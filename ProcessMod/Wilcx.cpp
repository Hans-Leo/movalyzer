
/*************************************************
    Wilcx - version 1.0 - hlt - 23May95
**************************************************
/*
Copyright 1995 Teulings

Mann-Whitney-Wilcoxon U, Student t, sign test for 2 samples

ARGUMENTS: Infile

FORMAT INPUT: Run without arguments

PURPOSES:
    (1) Mann-Whitney-Wilcoxon T or U for 2 independent samples
    (2) Sign test per sample
    (3) Student t test between samples
    (4) Sign test of sample1-sample2 (if equal size) as if paired samples
    (5) Student t test of sample1-sample2 (if equal size)
    (6) Correlation

TEST:
  wilcx.tst

REFERENCE:
  J.D. Gibbons (1985). Nonparametric methods for quantitative analysis (2nd Ed.).
  Columbus: American Sciences Press.

UPDATES:
  21Jun95: Indices did not make use of indx,y,xy, Output improved.
  23Jun95: Both set names (shortened) in each line for easy grepping if significances
  12Jul95: Warning if mean and median have different signs; medxyunpaired = medx-medy
           Log and exp transformation (not useful)
  27Feb96: Updated help info
  22Mar96: Correlation included
   5Jun96: zsignificance() also for positive z, also t (biased, though)
   2Jul96: Bug: overlap of y set used nx, corresponding output desambiguated.
             tsignificance() added
  28Aug96: sqrt(neg value) protection
   5May97: two-sided p values instead of one-sided (~ if marginal)
   8May97: Format adjusted (no tabs anymore)
  23May97: Bug in tpaired: mxy/(sdxy*sqrt(nxy)) instead of mxy/(sdxy/sqrt(nxy))
   5Sep97: Bug in tpaired: if unequal missing data: t=0; if zero difference: data dropped
  13apr01: Added mean,sd,n to output summary
  8Oct03: Problems with identical data sets resolved
  02Dec03: Raji: Reformatted the output to be written to statistics file
  18Sep06: GMB: Converted deprecated string/file functions for MSVS7
  12Nov08: GMB: Removed constant for missing data value. Added settable value.

BUGS:
  No tie correction in U

TESTSET
 20 set1
0.001 0.001 0.001 0.001 1 1 1 1 1 1 2 2 2 2 2 2 3 3 4 4
 20 set2
8 10 6 7 8 11 9 7 10 6 3 4 8 0.001 3 6 1 6 2 3
r=-.66, t(18)=-3.73 p<0.01
From: Slavin (1984), p.199

15 set1
10 9 9 8 8 7 7 7 7 6 6 6 6 5 4
15 set2
8 7 7 6 6 6 5 5 5 5 4 4 3 3 1
t(28)=3.21 p<0.01
From Slavin (1984), p.179

20 set1
28 26 25 24 24 23 22 21 21 21 20 20 20 20 19 18 18 17 16 14
20 set2
29 30 27 25 22 29 21 22 29 20 29 25 27 18 24 16 26 21 22 20
t(19)=-3.94
From: Slavin (1984), p. 182


16 Group2_Level2
1.273     1.76    1.311    2.122    1.906    1.987    1.713    1.846    1.675
2.087    1.438    1.959        0     2.16    1.806    2.336
16 Group2_Level3
1.224    1.463    1.538    1.766    1.513    1.368    1.627    1.298    1.247
2.087    1.237    1.654    1.851    1.899    1.919    2.076

Output:
Group2_Level2:
  n=15
  m=  1.83 s=0.309
  t(14)=  22.9
  med=  1.85
  min[ 1]=  1.27 max [16]=  2.34
  sign(15)= 0,z= -3.61+++
Group2_Level3:
  n=16
  m=  1.61 s=0.297
  t(15)=  21.7
  med=  1.58
  min[ 1]=  1.22 max [10]=  2.09
  sign(16)= 0,z= -3.75+++
G2_L2   o  :
  x[1]=1.27 x[2]=1.76 x[3]=1.31 x[5]=1.91 x[6]=1.99 x[7]=1.71 x[8]=1.85 x[9]=1.67 x[11]=1.44 x[12]=1.96 x[15]=1.81
G2_L3   o  :
  y[2]=1.46 y[3]=1.54 y[4]=1.77 y[5]=1.51 y[6]=1.37 y[7]=1.63 y[8]=1.3 y[10]=2.09 y[12]=1.65 y[13]=1.85 y[14]=1.9 y[15]=1.92 y[16]=2.08
G2_L2-G2_L3 U  :
  n=31
  m= 0.215 s=0.303
  t(29)=  1.97~
  med= 0.263
  U(15,16)=207/289,z= -1.92~
G2_L2-G2_L3 P e:
  [3]=-0.227 [15]=-0.113
G2_L2-G2_L3 P  :
  n=15
  m= 0.247 s=0.236
  t(14)=  4.06**
  med= 0.261
  min[ 3]=-0.227 max[ 6]= 0.619
  sign(14)= 2,z= -2.58**
G2_L2-G2_L3 P  :
  corr=   0.7
  t(13)=  3.54 ##

*/

/************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <ctype.h>
#include <math.h>
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"

#define	MODULE		_T("WILCX")

#define NSMAXW 200
#define NODECIMATE 1
#define ASCENDING +1
//#define XMIS 0.
#define NCHARTXT 40
#define LOGUPSCALE 100.


int wilct_(double *x, int *nx, double *y, int *ny, int a [], double b [], int *n, double *u, double *urank, double *z);
double sumxq (double data [], int n);
void stest (double y [], int ny, int indout [], int *nposnegy,  int *nonzero, double *zsigny);
int remmis (double x [], int indx [], int nx, double xmis);
void zsignificance (double z, char pz [], char marker);
void tsignificance (double t, int df, char pz [], char marker);
double betai (double a, double b, double x);
double gammln(double xx);
void namebrake (CString inname, CString& outname);
double betacf (double a, double b, double x);

/************************************************************************/
bool Wilcx( CString szGRPIn, CString szLSTOut, bool fLogTransform )
/************************************************************************/
{
	bool fRet = TRUE;

	/* System generated locals */
	double r__1 = 0., r__2 = 0.;

	/* Local variables */
	double* x = new double[ NSMAXW ];
	double* y = new double[ NSMAXW ];
	double* xy = new double[ NSMAXW ];
	double* xyunpaired = new double[ 2 * NSMAXW ];
	double* xpair = new double[ NSMAXW ];
	double* ypair = new double[ NSMAXW ];
	int* indx = new int[ NSMAXW ];
	int* indy = new int[ NSMAXW ];
	int* indxy = new int[ NSMAXW ];
	int* indxpair = new int[ NSMAXW ];
	int* indypair = new int[ NSMAXW ];
	int* indxout = new int[ NSMAXW ];
	int* indyout = new int[ NSMAXW ];
	int* indxyout = new int[ NSMAXW ];
	int* indxout2 = new int[ NSMAXW ];
	int* indyout2 = new int[ NSMAXW ];
	int* ranks = new int[ 2* NSMAXW ];
	char pz [4], pzsignx [4], pzsigny [4], pzsignxy [4];
	char ptcorr [4], ptxy [4], ptxyunpaired [4];
	char pwarnunpaired [4], pwarn [4];
	int jx = 0, jy = 0, jx2 = 0, jy2 = 0;
	double gemx = 0., gemy = 0., gemxy = 0., gemxyunpaired = 0.;
	double varx = 0., vary = 0., varxy = 0.;
	double sdx = 0., sdy = 0., sdxy = 0., sdxyunpaired = 0.;
	double rmedx = 0., rmedy = 0., rmedxy = 0.;
	double corr = 0., tcorr = 0., tcorrdf = 0.;
	int n = 0;
	double txy = 0., txyunpaired = 0., tx = 0., ty = 0., u = 0., urank = 0., z = 0.;
	double sq = 0.;
	int nx = 0, ny = 0, nxy = 0;
	int nxpair = 0, nypair = 0;
	int idf = 0;
	int nposnegx = 0, nposnegy = 0, nposnegxy = 0;
	int nonzerox = 0, nonzeroy = 0, nonzeroxy = 0;
	double zsignx = 0., zsigny = 0., zsignxy = 0.;
	int ixmin = 0, iymin = 0, ixymin = 0;
	int ixmax = 0, iymax = 0, ixymax = 0;
	int i = 0;
	bool nodata = 0, insuffdata = 0;
	CStdioFile fIn, fOut;
	CString szMsg, txtx, txty;
	CString txtxshort, txtyshort, txhead;

	/***********/
	/* heading */
	/***********/
	/*
	printf ("Mann-Whitney-Wilcoxon U, Student t, Sign test, Correlation test\n");
	printf ("Normal z approximation (biased for t values, two-tailed tests\n");
	printf ("  * = p<0.05, ** = p<0.01, *** = p<0.001, ~ = p<0.1 between sets, +... within sets, #... corr\n");
	printf ("  ? = inconsistency between means and medians\n");
	printf ("  U = Unpaired tests are done at any rate.\n");
	printf ("  P = Paired tests are done when set sizes are equal.\n");
	printf ("  o = Overlap between two sets of unpaired cases.\n");
	printf ("  e = Exceptions in two sets of paired cases.\n");
	printf ("  n = Number; m = Mean; s = SD; med = Median; min = Minimum; max = Maximum.\n");
	printf ("  corr = Correlation\n");
	printf ("  [1] = First case in input, etc.\n");
	printf ("  Warning: Ties are not corrected for in U.\n");
	*/
	/***************************/
	/* Open input/output files */
	/***************************/
	if( !fIn.Open( szGRPIn, CFile::modeRead ) )
	{
		szMsg.Format( "%s: Error; In_infile=%s; Open failed.", MODULE, szGRPIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	if( !fOut.Open( szLSTOut, CFile::modeWrite | CFile::modeCreate ) )
	{
		szMsg.Format( "%s: Error; Out_outfile=%s; Open failed.", MODULE, szLSTOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	szMsg.Format( _T("%s: INFO; Reading in_file=%s."), MODULE, szGRPIn );
	OutputMessage( szMsg, LOG_PROC );
	/***********/
	/* Header */
	/***********/
	/*02Dec03: Raji: Reformatted Header*/
	fOut.WriteString(_T("Exhaustive Pairwise Statistical Tests\n"));
	fOut.WriteString(_T("-------------+--------------+---------------------------------------\n"));
	fOut.WriteString(_T("             | Parametric   | NonParametric                         \n"));
    fOut.WriteString(_T("-------------+--------------+---------------------------------------\n"));
	fOut.WriteString(_T("NonPaired    | Student's t  | Mann-Whitney-Wilcoxon U (1)           \n"));
	fOut.WriteString(_T("-------------+--------------+---------------------------------------\n"));
    fOut.WriteString(_T("Paired(2)    | Student's t  | Sign                                  \n"));
	fOut.WriteString(_T("             | Correlation  |                                       \n"));
	fOut.WriteString(_T("-------------+--------------+---------------------------------------\n"));
	fOut.WriteString(_T("(1) Ties are not corrected for.\n"));
    fOut.WriteString(_T("(2) If equal number of observations per sample (could be conincedence in which case pairwise testing is senseless.\n\n"));
	fOut.WriteString(_T("-------------------+--------+-------+-------+------+-----------------\n"));
	fOut.WriteString(_T("p                  | <0.001 | <0.01 | <0.05 | <0.1 | Inconsistent (3)\n"));
	fOut.WriteString(_T("-------------------+--------+-------+-------+------+-----------------\n"));
	fOut.WriteString(_T("Between samples    |  ***   |  **   |  *    |  ~   |     ?           \n"));
	fOut.WriteString(_T("Within sample      |  +++   |  ++   |  +    |  ~   |     ?           \n"));
	fOut.WriteString(_T("LinearCorrelations |  ###   |  ##   |  #    |  ~   |     ?           \n"));
	fOut.WriteString(_T("-------------------+--------+-------+-------+------+-----------------\n"));
	fOut.WriteString(_T("(3) Mean and median show different trends\n\n"));
	fOut.WriteString(_T("Additional statistics summarizing all N observations per sample\n"));
	fOut.WriteString(_T("-----------------+------------------------------------------\n"));
	fOut.WriteString(_T("Name             | Calculation                              \n"));
	fOut.WriteString(_T("-----------------+------------------------------------------\n"));
	fOut.WriteString(_T("Mean             | Sum Data(i) / N                          \n"));
	fOut.WriteString(_T("SD               | SQRT Sum (Data(i)-Mean)**2/(N-1)         \n"));
	fOut.WriteString(_T("SD/Mean          | Relative SD                              \n"));
	fOut.WriteString(_T("Range            | Max (all Data (i)) - Min (all Data (i))  \n"));
	fOut.WriteString(_T("Regression       | LinearRegression = dData(i)/di           \n"));
	fOut.WriteString(_T("-----------------+------------------------------------------\n"));
	/*09Dec03: Write the y-axis label at beginning of data*/
	fIn.ReadString(txhead);
	szMsg.Format( _T("%-11s\n"),txhead);
	fOut.WriteString(szMsg);

	while (TRUE)
	{
		/*********/
		/* Input */
		/*********/
		nx = getdata (fIn.m_pStream, x, NSMAXW, txtx, NODECIMATE);
		if (nx == 0) break;
		ny = getdata (fIn.m_pStream, y, NSMAXW, txty, NODECIMATE);

		/* Get shorter output */
		namebrake (txtx, txtxshort);
		namebrake (txty, txtyshort);


		/******************/
		/* Transformation */
		/******************/
		/* Prevents negative values if dy<0.01cm dt<0.01s peakay<0.01cm/s2 vy<0.01cm/s */
		/* Actually irrelevant here */
		if (fLogTransform)
		{
			for (i = 0; i < nx; i++)
			{
				if (x [i] != ::GetMissingDataValue())
					x [i] = log (LOGUPSCALE * fabs ((double) x [i]));
			}
			for (i = 0; i < ny; i++)
			{
				if (y [i] != ::GetMissingDataValue())
					y [i] = log (LOGUPSCALE * fabs ((double) y [i]));
			}
		}


		/*****************/
		/* Pair the data */
		/*****************/
		/* Difference between data */
		/* Must be before removing missing data */
		/* Guess that paired difference is needed if nx is ny */
		if (nx == ny)
		{
			/* Paired data */
			for (i = 0; i < nx; i++)
			{
				if (x [i] != ::GetMissingDataValue() && y [i] != ::GetMissingDataValue())
				{
					xpair [i] = x [i];
					ypair [i] = y [i];
				}
				else
				{
					xpair [i] = ::GetMissingDataValue();
					ypair [i] = ::GetMissingDataValue();
				}
			}
		}

		/*********************************/
		/* List of indices in input file */
		/*********************************/
		/* After removing missing data the indices tell which input data used */
		for (i = 0; i < nx; i++)
			indx [i] = i;
		for (i = 0; i < ny; i++)
			indy [i] = i;
		if (nx == ny )
		{
			for (i = 0; i < nx; i++)
				indxpair [i] = i;
			/* In fact indypair should be same as indxpair */
			for (i = 0; i < ny; i++)
				indypair [i] = i;
		}

		/***********************/
		/* Remove missing data */
		/***********************/
		/* Remove pairs with at least one missing for paired tests */
		nxy = 0;
		if (nx == ny)
		{
			nxpair = remmis (xpair, indxpair, nx, ::GetMissingDataValue());
			nypair = remmis (ypair, indypair, ny, ::GetMissingDataValue());
			for (i = 0; i < nx; i++)
				indxy [i] = indxpair [i];
			/* Test; Also indxpair and indypair should be the same */
			if (nxpair != nypair)
			{
				szMsg.Format( _T("%s: nxpair=%d != nypair=%d."), MODULE, nxpair, nypair );
				OutputMessage( szMsg, LOG_PROC );
				fRet = FALSE;
				goto Cleanup;
			}

			/* differences of the pairs */
			/* Coincidental missing data values are accepted */
			nxy = nxpair;
			for (i = 0; i < nxy; i++)
			{
				xy [i] = xpair [i] - ypair [i];
			}
		}

		/* Remove per set separately for unpiared tests */
		/* nx, ny, nxy have now reduced values */
		nx = remmis (x, indx, nx, ::GetMissingDataValue());
		ny = remmis (y, indy, ny, ::GetMissingDataValue());


		/*****************/
		/* Range of data */
		/*****************/
		/* Unpaired data */
		indrmm (x, nx, &ixmin, &ixmax);
		indrmm (y, ny, &iymin, &iymax);
		/* Paired data */
		if (nxy > 0)
			indrmm (xy, nxy, &ixymin, &ixymax);

		/***********************/
		/* Test for empty data */
		/***********************/
		if (nx == 0 || ny == 0)
		{
			szMsg.Format( _T("%s: INFO; nx=%d or ny=%d =0."), MODULE, nx, ny );
			OutputMessage( szMsg, LOG_PROC );
			nodata = 1;
			goto L10;
		}

		/*********************/
		/* Statistical tests */
		/*********************/

		/***************/
		/* Correlation */
		/***************/
		/* Nothing for unpaired data */
		/* Paired data */
		tcorr = 0.;
		tcorrdf = 0;
		if (nxy > 0)
		{
			/* xpair, ypair should not contain any missing data */
			corr = correl (xpair, ypair, nxy, ::GetMissingDataValue());
			tcorrdf = nxy - 2.;
			if (tcorrdf > 0)
			{
				if (corr != 1.)
					tcorr = corr * sqrt (tcorrdf / (1. - SQR (corr)));
				else
					tcorr = 999.; /* Large value */
			}
		}


		/*************/
		/* Sign test */
		/*************/
		/* Unpaired data */
		/* These exceptions in indxout,y are not so interesting and will be overruled */
		stest (x, nx, indxout, &nposnegx, &nonzerox, &zsignx);
		stest (y, ny, indyout, &nposnegy, &nonzeroy, &zsigny);

		/* Paired data */
		/* Get list of paired exceptions here */
		stest (xy, nxy, indxyout, &nposnegxy, &nonzeroxy, &zsignxy);


		/***************************/
		/* Wilcoxon Mann Whitney U */
		/***************************/
		/* Unpaired data */
		/* Nothing for paired data */
		/* Get list of unpaired exceptions here */
		/* xyunpaired is created here */
		n = nx + ny;
		wilct_(x, &nx, y, &ny, ranks, xyunpaired, &n, &u, &urank, &z);

		/***********/
		/* Overlap */
		/***********/
		/* Cases of x greater than min of y */
		/*              xmax                */
		/*   xxxxxxxxxxxxxxx                */
		/*          yyyyyyyyyyyyyyyy        */
		/*          ymin                    */
		/* 08Oct03: Hlt: Identical sequence of data has to have all the values of x and y in the output similar*/
		jx = 0;
		for (i = 0; i < nx; i++)
			if (x [i] >= y [iymin])
				indxout [jx++] = i;

		/* Cases of y less than max of x */
		jy = 0;
		for (i = 0; i < ny; i++)
			if (y [i] <= x [ixmax])
				indyout [jy++] = i;

		/* Cases of x greater than max of y */
		/*               xmin               */
		/*               xxxxxxxxxxxxxxx    */
		/* yyyyyyyyyyyyyyyyyyyyy            */
		/*                  ymax            */
		jx2 = 0;
		for (i = 0; i < nx; i++)
			if (x [i] <= y [iymax])
				indxout2 [jx2++] = i;

		/* Cases of y smaller than min of x */
		jy2 = 0;
		for (i = 0; i < ny; i++)
			if (y [i] >= x [ixmin])
				indyout2 [jy2++] = i;

		/**************************************/
		/* Take smallest number of exceptions */
		/**************************************/
		if (jx2 + jy2 < jx + jy)
		{
			for (i = 0; i < jx2; i++)
				indxout [i] = indxout2 [i];
			jx = jx2;

			for (i = 0; i < jy2; i++)
				indyout [i] = indyout2 [i];
			jy = jy2;
		}


		/**************/
		/* Means, sds */
		/**************/
		/* unpaired data */
		/* Should not contain missing data */
		gemx  = means (x,  nx,  ::GetMissingDataValue());
		gemy  = means (y,  ny,  ::GetMissingDataValue());
		varx  = vars (x,  nx,  gemx,  ::GetMissingDataValue());
		vary  = vars (y,  ny,  gemy,  ::GetMissingDataValue());
		sdx  = sqrt (varx);
		sdy  = sqrt (vary);
		gemxy = means (xy, nxy, ::GetMissingDataValue());
		varxy = vars (xy, nxy, gemxy, ::GetMissingDataValue());
		sdxy = sqrt (varxy);


		/********************/
		/* Student's t test */
		/********************/
		/* Unpaired data */
		/* DF */
		idf = nx + ny - 2;
		/* Test for insufficient data */
		if (idf <= 0)
		{
			szMsg.Format( _T("%s: INFO; idf=%d<=0."), MODULE, idf );
			OutputMessage( szMsg, LOG_PROC );
			insuffdata = 1;
			goto L10;
		}

		/* May differ from paired because of missing data */
		gemxyunpaired = gemx - gemy;

		/* Computing 2nd power */
		r__1 = gemx;
		r__2 = gemy;
		sq = (sumxq (x, nx) - r__1 * r__1 * nx + sumxq (y, ny) - r__2 * r__2 * ny) / idf;
		/* About? */
		sdxyunpaired = sqrt (sq);

		if (sdxyunpaired > 0. && nx > 0 && ny > 0)
			txyunpaired = (gemx - gemy) / (sdxyunpaired * sqrt ((double) 1. / nx + 1. / ny));
		else
			txyunpaired = 0.;


		/* t for difference from zero */
		if (nx > 0 && sdx > 0.)
			tx = gemx / (sdx / sqrt ((double) nx));
		else
			tx = 0;

		/* t for difference from zero */
		if (ny > 0 && sdy > 0.)
			ty = gemy / (sdy / sqrt ((double) ny));
		else
			ty = 0;


		/* Paired data */
		if (nxy > 0 && sdxy > 0.)
			txy = gemxy / (sdxy * sqrt ((double) 1. / nxy));
		else
			txy = 0;

		/**************************/
		/* Reverse Transformation */
		/**************************/
		/* Operations after this are indepdendent of the transformation */
		if (fLogTransform)
		{
			for (i = 0; i < nx; i++)
				x [i] = (1. / LOGUPSCALE) * exp ((double) x [i]);
			for (i = 0; i < ny; i++)
				y [i] = (1. / LOGUPSCALE) * exp ((double) y [i]);
			for (i = 0; i < n; i++)
				xyunpaired [i] = (1. / LOGUPSCALE) * exp ((double) xyunpaired [i]);
			for (i = 0; i < nxy; i++)
				xy [i] = (1. / LOGUPSCALE) * exp ((double) xy [i]);

			/* Correct numerical data calculated during the transformation */
			gemx  = (1. / LOGUPSCALE) * exp ((double) gemx);
			gemy  = (1. / LOGUPSCALE) * exp ((double) gemy);
			gemxy = (1. / LOGUPSCALE) * exp ((double) gemxy);
			gemxyunpaired = (1. / LOGUPSCALE) * exp ((double) gemxyunpaired);
			sdx  = (1. / LOGUPSCALE) * exp ((double) sdx);
			sdy  = (1. / LOGUPSCALE) * exp ((double) sdy);
			sdxy = (1. / LOGUPSCALE) * exp ((double) sdxy);
			sdxyunpaired = (1. / LOGUPSCALE) * exp ((double) sdxyunpaired);
		}


		/***********/
		/* Medians */
		/***********/
		/* Unpaired data */
		shell_sortr (x, nx, ranks, ASCENDING);
		if (nx < 1)
			rmedx = ::GetMissingDataValue();
		else
		/* Pick the one or two middle ones */
			rmedx = 0.5 * (x [ranks [(nx - 1) / 2] ] + x [ranks [nx / 2] ]);

		shell_sortr (y, ny, ranks, ASCENDING);
		if (ny < 1)
			rmedy = ::GetMissingDataValue();
		else
		/* Pick the one or two middle ones */
			rmedy = 0.5 * (y [ranks [(ny - 1) / 2] ] + y [ranks [ny / 2] ]);

		/* Paired data */
		shell_sortr (xy, nxy, ranks, ASCENDING);
		if (nxy < 1)
			rmedxy = ::GetMissingDataValue();
		else
		/* Pick the one or two middle ones */
			rmedxy = 0.5 * (xy [ranks [(nxy - 1) / 2] ] + xy [ranks [nxy / 2] ]);

		/**********/
		/* Output */
		/**********/
		/* Significance marks */
		zsignificance (zsignx,  pzsignx,  '+');
		zsignificance (zsigny,  pzsigny,  '+');
		zsignificance (z,       pz,       '*');
		zsignificance (zsignxy, pzsignxy, '*');
		tsignificance (txyunpaired, idf, ptxyunpaired, '*');
		tsignificance (txy, nxy - 1, ptxy, '*');
		tsignificance (tcorr, (int)tcorrdf, ptcorr, '#');

		/* Warnings for inconsistencies */
		strcpy_s (pwarnunpaired, " ");
		if (strcmp (pz, "   ") != 0 && gemxyunpaired * (rmedx - rmedy) < 0)
			strcpy_s (pwarnunpaired, "?");
		strcpy_s (pwarn, " ");
		if (strcmp (pzsignxy, "   ") != 0 && gemxy * rmedxy < 0)
			strcpy_s (pwarn, "?");

		/**********/
		/* Output */
		/**********/
        /* Set 1 */
		/* 02Dec03: Raji: Reformatted the output to be written to statistics file*/
		//fOut.WriteString(_T("__________________________________________________________________________________________________________________________________\n\n"));
		fOut.WriteString(_T("\n"));
		szMsg.Format( _T("%-11s\n - Parametric Mean=%.3g SD=%.3g N=%2d t(%d)=%.3g\n"),
			          txtx, gemx, sdx, nx, nx - 1, tx);
		fOut.WriteString( szMsg );
		szMsg.Format( _T(" - NonParametric Median=%.3g Minimum[%d]=%.3g Maximum[%d]=%.3g SignTest(%d)=%d z=%.3g%s\n"),
		   		      rmedx, indx [ixmin]+1, x [ixmin], indx [ixmax]+1, x [ixmax], nonzerox, nposnegx, zsignx, pzsignx);
		fOut.WriteString( szMsg );
		/* Set 2 */
		szMsg.Format( _T("\n%-11s\n - Parametric Mean=%.3g SD=%.3g N=%d t(%d)=%.3g\n"),
					  txty, gemy, sdy,  ny, ny - 1, ty);
		fOut.WriteString( szMsg );
		szMsg.Format( _T(" - NonParametric Median=%.3g Minimum[%d]=%.3g Maximum[%d]=%.3g SignTest(%d)=%d z=%.3g%s\n"),
					 rmedy, indy [iymin]+1, y [iymin], indx [iymax]+1, y [iymax], nonzeroy, nposnegy, zsigny, pzsigny);
		fOut.WriteString( szMsg );
		/*02Dec03: Raji: Not writing extrema data to output file*/
		//for (i = 0; i < jx; i++)
		//{
		//	szMsg.Format( _T(" x[%d]=%.3g"), indx [indxout [i]]+1, x [indxout [i]]);
		//	fOut.WriteString( szMsg );
		//}
		//szMsg.Format( _T("\n%-7s: Overlap:"), txtyshort);
		//fOut.WriteString( szMsg );
		//for (i = 0; i < jy; i++)
		//{
		//	szMsg.Format( _T(" y[%d]=%.3g"), indy [indyout [i]]+1, y [indyout [i]]);
		//	fOut.WriteString( szMsg );
		//}

		/* Unpaired test summary */
        szMsg.Format( _T("\n%-3s VERSUS %-3s\n"),txtx,txty);
		fOut.WriteString( szMsg );
		szMsg.Format( _T(" Unpaired Parametric Mean=%.3g SD=%.3g N=%d t(%d)=%.3g%s\n"),
					  gemxyunpaired, sdxyunpaired, n, idf, txyunpaired, ptxyunpaired);
		fOut.WriteString( szMsg );
		szMsg.Format( _T(" Unpaired NonParametric Median=%.3g - - U(%d,%d)=%g/%g z=%.3g%s%s?\n"),
					  rmedx - rmedy, nx, ny, urank, u, z, pz, pwarnunpaired);
		fOut.WriteString( szMsg );

		/*02Dec03: Raji:Not writing extrema data to output file*/
		//if (nxy != 0)
		//{
			//szMsg.Format( _T("%-3s-%-3s Paired: Extrema:"), txtxshort, txtyshort);
		//	fOut.WriteString( szMsg );
		//	for (i = 0; i < nposnegxy; i++)
		//	{
		//		szMsg.Format( _T(" [%d]=%.3g"), indxy [indxyout [i]]+1, xy [indxyout [i]]);
		//		fOut.WriteString( szMsg );
		//	}

			/* Paired test summary (special case) */
			szMsg.Format( _T(" Paired Parametric Mean=%.3g SD=%.3g N=%d t(%d)=%.3g%s\n"),
						  gemxy, sdxy, nxy, nxy - 1, txy, ptxy);
			fOut.WriteString( szMsg );
			szMsg.Format( _T(" Paired NonParametric Median=%.3g Minimum[%d]=%.3g Maximum[%d]=%.3g Signtest(%d)=%d z=%.3g%s%s\n"),
						  rmedxy, indxy [ixymin]+1, xy [ixymin], indxy [ixymax]+1, xy [ixymax], nonzeroxy, nposnegxy, zsignxy, pzsignxy, pwarn);
			fOut.WriteString( szMsg );
			szMsg.Format( _T(" Paired Parametric Correlation=%.3g - - t(%g)=%.3g %s\n"),
					corr, tcorrdf, tcorr, ptcorr);
			fOut.WriteString( szMsg );
		//}
		//else
		//{
		//	szMsg.Format( _T("%-3s--%-3s P e: N/A\n"), txtx, txty);
		//	fOut.WriteString( szMsg );
		//	szMsg.Format( _T("%-3s--%-3s P  : N/A\n"), txtx, txty);
		//	fOut.WriteString( szMsg );
		//}
	}
L10:;
	/* Not sufficient data: Write message to output file, otherwise empty*/
	if (insuffdata)
		szMsg.Format(_T("Insufficient data -- #parameters: xaxis = %d yaxis = %d\nTry Averaging per subject & per trial"), nx, ny);
	if (nodata)
		szMsg.Format(_T("Empty data -- #parameters: xaxis = %d yaxis = %d\n"), nx, ny);
	fOut.WriteString(szMsg);

Cleanup:
	fIn.Close();
	fOut.Close();

	if( x ) delete [] x;
	if( y ) delete [] y;
	if( xy ) delete [] xy;
	if( xyunpaired ) delete [] xyunpaired;
	if( xpair ) delete [] xpair;
	if( ypair ) delete [] ypair;
	if( indx ) delete [] indx;
	if( indy ) delete [] indy;
	if( indxy ) delete [] indxy;
	if( indxpair ) delete [] indxpair;
	if( indypair ) delete [] indypair;
	if( indxout ) delete [] indxout;
	if( indyout ) delete [] indyout;
	if( indxyout ) delete [] indxyout;
	if( indxout2 ) delete [] indxout2;
	if( indyout2 ) delete [] indyout2;
	if( ranks ) delete [] ranks;

	return fRet;
} /* MAIN__ */

/***************************************************************************/
int wilct_(double *x, int *nx, double *y, int *ny, int ranks [], double b [], int *n, double *u, double *urank, double *z)
/***************************************************************************/

/* wilct.ftn */

/* toets van wilcoxon (mann-whitney-u) voor */
/* 2 onafhankelijke steekproeven */

/* benodigde subroutine rank */
{
	/* System generated locals */
	int i__1;

	/* Local variables */
	int i;
	double an, anx, any;
	double urankx, uranky;
	double tiecorrection;
	int nxory;

	/* Parameter adjustments */
	--b;
	--y;
	--x;

	/* Function Body */
	if (*n != *nx + *ny)
	{
		CString szMsg;
		szMsg.Format( _T("%s: ERROR; n unequal nx+ny."), MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}

	/* Load x into values array */
	i__1 = *nx;
	for (i = 1; i <= i__1; ++i)
		b[i] = x[i];

	/* Append y */
	i__1 = *ny;
	for (i = 1; i <= i__1; ++i)
		b[i + *nx] = y[i];

	/* Get ranks sorted values */
	shell_sortr (&b[1], *n, ranks, ASCENDING);

	/* Cumulate the positions belonging to the x sample */
	/* u is actually Wilcoxon T and not Mann-Whitney U */
	*u = 0;
	for (i = 0; i < *n; i++)
		if (ranks [i] < *nx)
	*u += i + 1;


	/* Some precalulations */
	an = (*n);
	anx = (*nx);
	any = (*ny);

	/* Make list of exceptions here */
	/* Take the smallest of the x or y rank */
	urankx = *u;
	uranky = an * (an + 1.) * 0.5 - urankx;
	if (urankx < uranky)
	{
		*urank = urankx;
		nxory = *nx;
		*u = uranky;
	}
	else
	{
		*urank = uranky;
		nxory = *ny;
		*u = urankx;
	}

	/* With continuity correction assuming */
	/* No tie correction */
	tiecorrection = 0.;

	*z = (*urank + 0.5 - nxory * (an + 1.) * 0.5) /
		 sqrt (anx * any * (an + 1.) / 12. - tiecorrection);

  return 0;
} /* wilct_ */



/*********************************************/
double sumxq (double data [], int n)
/*********************************************/
/* Sum of squares */
{
	double sumxq;
	int i;

	sumxq = 0.;
	for (i = 0; i < n; i++)
	{
		sumxq += data [i] * data [i];
	}

	return sumxq;
}



/**********************************************************/
void stest (double y [], int ny,  int indout [],
  int *nposnegy,  int *nonzero, double *zsigny)
/**********************************************************/
/*

Sign test under the assumption of equal chances for positive or negative y [0...ny].

Output:
indout [0...ny] contains the few cases that form exceptions.
nposnegy is number of positives or negatives whatever is smallest.
nonzero is the number of nonzeroes
zsigny is the z-score

*/

{
	int i, j, nposnegy2;

	*nposnegy = 0;
	*nonzero = ny;
	for (i = 0; i < ny; i++)
	{
		/* Count negatives */
		if (y [i] < 0.)
			*nposnegy += 1;
		else if (y [i] == 0.)
			*nonzero -= 1;
	}

	/* Make list of exceptions here */
	nposnegy2 = *nonzero - *nposnegy;
	if (nposnegy2 < *nposnegy)
	{
		/* Least are positive */
		*nposnegy = nposnegy2;
		j = 0;
		for (i = 0; i < ny; i++)
		{
			if (y [i] > 0.)
				indout [j++] = i;
		}
	}
	else
	{
		/* Least are negative */
		*nposnegy = *nposnegy;
		j = 0;
		for (i = 0; i < ny; i++)
		{
			if (y [i] < 0.)
			indout [j++] = i;
		}
	}


	/* z value for large ny using continuity correction */
	/* Since nposnegy is the smallest of npos and nneg correction should be +0.5 */
	/* 8oct03: Raji: nonzero used instead of ny to get rid of a problem in identical data sets*/
	if (*nonzero > 0)
		*zsigny = (*nposnegy + 0.5 - 0.5 * *nonzero) / (0.5 * sqrt ((double) *nonzero));
	else
		*zsigny = 0.;
}

/**************************************************************/
int remmis (double x [], int ind [], int nx, double xmis)
/**************************************************************/
{
	int i, j;

	j = 0;
	for (i = 0; i < nx; i++)
	{
		if (x [i] != xmis)
		{
			/* Copy data */
			x [j] = x [i];
			/* Copy indices */
			ind [j] = ind [i];
			/* Augment counter */
			j++;
		}
	}

	return j;
}

/*******************************************************/
void zsignificance (double z, char pz [], char marker)
/*******************************************************/
{
	/* two-tailed probabilities */
	//if (z < -3.090 || z > +3.090)  /* .999 */
	if (z < -3.291 || z > +3.291)    /* .9995 */
		/* p<0.001 */
		snprintf(pz, sizeof( pz ), "%c%c%c", marker, marker, marker);
	else if (z < -2.576 || z > +2.576)    /* .995 */
		/* p<0.01 */
		snprintf (pz, sizeof( pz ), "%c%c", marker, marker);
	else if (z < -1.960 || z > +1.960)    /* .975 */
		/* p<0.05 */
		snprintf (pz, sizeof( pz ), "%c", marker);
	else if (z < -1.645 || z > +1.645)    /* .95 */
		/* p<0.1 */
		snprintf (pz, sizeof( pz ), "%c", '~');
	else
		/* p>=0.1 */
		snprintf (pz, sizeof( pz ), " ");
}


/*******************************************************/
void tsignificance (double t, int df, char pt [], char marker)
/*******************************************************/
{

	double tprob;

/*
Numerical recepies

#include <math.h>
void ttest(data1,n1,data2,n2,t,prob)

int n1,n2;
double data1[],data2[],*t,*prob;
{
	double var1,var2,svar,df,ave1,ave2;
	void avevar();
	double betai();

	avevar(data1,n1,&ave1,&var1);
	avevar(data2,n2,&ave2,&var2);
	df=n1+n2-2;
	svar=((n1-1)*var1+(n2-1)*var2)/df;
	*t=(ave1-ave2)/sqrt(svar*(1.0/n1+1.0/n2));
	*prob=betai(0.5*df,0.5,df/(df+(*t)*(*t)));
}
#include <math.h>
double betai(a,b,x)

double a,b,x;
{
	double bt;
	double gammln(),betacf();
	void nrerror();

	if (x < 0.0 || x > 1.0) nrerror("Bad x in routine BETAI");
	if (x == 0.0 || x == 1.0) bt=0.0;
	else
		bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.0-x));
	if (x < (a+1.0)/(a+b+2.0))
		return bt*betacf(a,b,x)/a;
	else
		return 1.0-bt*betacf(b,a,1.0-x)/b;
}
#include <math.h>

double gammln(xx)
double xx;
{
	double x,tmp,ser;
	static double cof[6]={76.18009173,-86.50532033,24.01409822,
		-1.231739516,0.120858003e-2,-0.536382e-5};
	int j;

	x=xx-1.0;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.0;
	for (j=0;j<=5;j++) {
		x += 1.0;
		ser += cof[j]/x;
	}
	return -tmp+log(2.50662827465*ser);
}

*/
  /* two-tailed probabilities! */
	if (df + SQR (t) != 0.)
		tprob = 0.5 * betai (0.5 * df, 0.5, df / (df + SQR (t)));
	else
		tprob = 1.;
	if (tprob < 0.0005)   /*  was 0.001 */
		snprintf (pt, sizeof( pt ), "%c%c%c", marker, marker, marker);
	else if (tprob < 0.005)  /* was 0.01 */
		snprintf (pt, sizeof( pt ), "%c%c ", marker, marker);
	else if (tprob < 0.025)  /* was 0.05 */
		snprintf (pt, sizeof( pt ), "%c  ", marker);
	else if (tprob < 0.05)  /* was 0.1 */
		snprintf (pt, sizeof( pt ), "%c  ", '~');
	else
		snprintf (pt, sizeof( pt ), "   ");
}

/************************************/
double betai (double a, double b, double x)
/************************************/
{
	double bt;

	if (x < 0.0 || x > 1.0)
	{
		CString szMsg;
		szMsg.Format( _T("%s: Bad x in routine BETAI."), MODULE );
		OutputMessage( szMsg, LOG_PROC );
		return -999999.;	// Will this work all the time?
	}
	if (x == 0.0 || x == 1.0)
		bt = 0.0;
	else
		bt = exp(gammln(a + b) - gammln(a) - gammln(b) + a * log(x) + b * log(1.0 - x));
	if (x < (a + 1.0) / (a + b + 2.0))
		return bt * betacf(a, b, x) / a;
	else
		return 1.0 - bt * betacf(b, a, 1.0 - x) / b;
}

/**************************/
double gammln(double xx)
/**************************/
{
	double x,tmp,ser;
	static double cof[6]={76.18009173,-86.50532033,24.01409822,
						  -1.231739516,0.120858003e-2,-0.536382e-5};
	int j;

	x = xx - 1.0;
	tmp = x + 5.5;
	tmp -= (x + 0.5) * log(tmp);
	ser = 1.0;
	for (j = 0; j <= 5; j++)
	{
		x += 1.0;
		ser += cof[j] / x;
	}
	return -tmp + log(2.50662827465*ser);
}

#define ITMAX 100
#define EPS 3.0e-7
/**************************************/
double betacf(double a, double b, double x)
/**************************************/
{
	double qap, qam, qab, em, tem, d;
	double bz, bm = 1.0, bp, bpp;
	double az = 1.0, am = 1.0, ap, app, aold;
	int m;

	qab = a + b;
	qap = a + 1.0;
	qam = a - 1.0;
	bz = 1.0 - qab * x / qap;
	for (m = 1; m <= ITMAX; m++)
	{
		em = (double) m;
		tem = em + em;
		d = em * (b - em) * x / ((qam + tem) * (a + tem));
		ap = az + d * am;
		bp = bz + d * bm;
		d = -(a + em) * (qab + em) * x / ((qap + tem) * (a + tem));
		app = ap + d * az;
		bpp = bp + d * bz;
		aold = az;
		am = ap / bpp;
		bm = bp / bpp;
		az = app / bpp;
		bz = 1.0;
		if (fabs(az - aold) < (EPS * fabs(az))) return az;
	}

	CString szMsg;
	szMsg.Format( _T("%s: a or b too big, or ITMAX too small in BETACF."), MODULE );
	OutputMessage( szMsg, LOG_PROC );

	return 1;
}
#undef ITMAX
#undef EPS

/****************************************************/
void namebrake (CString inname, CString& outname)
/****************************************************/
{
}

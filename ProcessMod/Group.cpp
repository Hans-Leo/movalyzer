
/*************************************************
  Group - version 1.0 - hlt - 22 May 1995
*************************************************
  Copyright 1995 Teulings

ARGUMENTS: Infile where column 1 signifies group level
  columnd 2 is subject and the following
  columns are data levels of that subject, mostly created by an awk procedure.

INPUT FILE
  Groupnr Subjnr Data for all conditions...
  Groupnr Subjnr Data for all conditions...
  Groupnr Subjnr Data for all conditions...


PURPOSES:
  (1) Reads lines of data. Each line represents all analysis conditions per group and subj
  (2) Groups columns belonging to one group level into one package (record)
  (3) Forms all pairs of groups within the same column
  (4) Forms all pairs of columns (data) within the same group
  (5) Adds extra data levels after given data:
         range, mean, sd, sd/mean per subject, linear trend

UPDATES:
  18Jul95: /d = nobetweenDatalevels
  26Sep95: Additional data has new sequence: Range was first; is now last
             So: mean, ds, sd/mean, range.
           Furthermore, Group.._Data.. is changed into Group.._Level..
           Levels are couting from 1, 2, 3, instead of Data from 3, 4, 5, ...
  20Aug96: Linear trend as extra datanr 5 added.
  15May97: /t = testadded levels of data also
  7Oct03:Raji: New labels to accomodate the extra columns due to ftestAdded always true
  20Oct03:Raji: Read the header from the EVA file and write the third column to output to be read by wilcx for the statistics file
  10Dec03:Raji: Changes made to modify the output GRP file.
  12Nov08: GMB: Removed constant for missing data value. Added settable value.
*/

/************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"
#include "../Common/macros.h"

#define	MODULE		_T("GROUP")

#define NINDEX 2
#define NDATA 14
#define NDATACOR 6
#define NDATAXMEAN 1
#define NDATAMAX 40
#define NC 40
#define NLINE 320 /* = npoints * ncurves */
#define LINELEN 400

#define NGROUP 50
#define GROUP 0
#define NDATAADDED 5
//#define XMIS 0.

void rmima0 (double arr [], int n, double *min, double *max, double xmis);

/************************************************************************/
bool Group( CString szEVAIn, CString szGRPOut, bool fBetweenData, bool fTestAdded )
/************************************************************************/
{
	bool fRet = TRUE;

	CString szMsg, szTmp;
	CStdioFile fIn, fOut;
	double data [NDATAMAX] [NLINE];
	double datain [NDATAMAX];
	int idata = 0, idata2 = 0, ndatain = 0, ndata = 0;
	int iline = 0, nline = 0;
	int iline1 [NGROUP+1];
	int igroupid [NGROUP];
	int igroup = 0, igroup2 = 0, igroupold = 0, ngroup = 0;
	double datamin = 0., datamax = 0.;
	double databuf [NDATAMAX];
	double gemx = 0., varx = 0., sdx = 0., nsdx = 0.;
	double b = 0., a = 0., w = 0., r = 0.;
	double x [NDATAMAX];
	int testnot = 0;
	CString szHeaders, szValue, szColumnLabel1, szColumnLabel2, szColumnLabel3, szLineNew;
	CStringArray Name;
	int nItem = 0;
	int chfind1, chfind2;
	int pos = 0, nPos = 0;

	/***************************/
	/* Open input/output files */
	/***************************/
	if( !fIn.Open( szEVAIn, CFile::modeRead ) )
	{
		szMsg.Format( "%s: Error; Eva_infile=%s; Open failed.", MODULE, szEVAIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	if( !fOut.Open( szGRPOut, CFile::modeWrite | CFile::modeCreate ) )
	{
		szMsg.Format( "%s: Error; Grp_outfile=%s; Open failed.", MODULE, szGRPOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	/********/
	/*Header*/
	/********/
 	/* 19Nov03:Raji: Read column headers from .eva file and extract the labels*/
	fIn.ReadString( szHeaders );
	nItem = 0;
	szLineNew = szHeaders;
	nPos = szLineNew.Find( _T(" ") );
	/* Split the header in to individual columns seperated by a space*/
	while( nPos != -1 )
	{
		szValue = szLineNew.Left( nPos );
		TRIM( szValue );
		szLineNew = szLineNew.Mid( nPos + 1 );
		TRIM( szLineNew );
		nItem++;
		/* 05Dec03: Raji: If the word number is 3 for 'Yaxis name' OR 4 for 'Xaxisname', OR 1 for 'groupname' store it*/
		if (nItem == 3)
		{
			/* Yaxis*/
			szColumnLabel1 = szValue;
		}
		if (nItem == 1)
		{
			/*Group*/
			szColumnLabel3 = szValue;
			/* Remove the avgof- or SDof-*/
            chfind1= szColumnLabel3.Find( _T("-") );
			szColumnLabel3 = szColumnLabel3.Right(szColumnLabel3.GetLength()- (chfind1 + 1));
		}
		if (nItem == 4)
		{
			/*Xaxis*/
			szColumnLabel2 = szLineNew;
			/* Remove the avgof- or SDof-*/
			chfind2 = szColumnLabel2.Find( _T("-") );
			szColumnLabel2 = szColumnLabel2.Right(szColumnLabel2.GetLength()- (chfind2 + 1));
		}
		nPos = szLineNew.Find( _T(" ") );
	}

	 /*****************/
	 /* Read till eof */
	/*****************/
	/* Read rest of the data lines */
	for (iline = 0; iline < NLINE; iline++)
	{
		/* Get line of data */
		ndatain = getlinefloat (fIn, datain, NDATAMAX);
		/* At EOF */
		if (ndatain == 0) break;
		/* Save in array */
		for (idata = 0; idata < ndatain; idata++)
		{
			data [idata] [iline] = datain [idata];
		}

		/* Test constancy of ndata */
		if (iline > 0)
		{
			if (ndatain != ndata)
			/* line nr plus heading +1 */
			szMsg.Format( _T("%s: ERROR; Xme_infile=%s; Iline=%d; #Data/line changed from %d to %d."),
						  MODULE, szEVAIn, iline + 2, ndata, ndatain );
			OutputMessage( szMsg, LOG_PROC );
		}
		ndata = ndatain;
	}

	/********/
	/* Test */
	/********/
	/* Test whether a file line was read */
	if (iline == 0)
	{
		szMsg.Format( _T("%s: ERROR; Xme_file=%s has zero=%d lines."),
					  MODULE, szEVAIn, iline );
		OutputMessage( szMsg, LOG_PROC );
	}
	if (iline == NLINE)
	{
		szMsg.Format( _T("%s: ERROR; Xme_file=%s has >NLINE=%d lines."),
					  MODULE, szEVAIn, NLINE );
		OutputMessage( szMsg, LOG_PROC );
	}
	nline = iline;


	/*********************/
	/* Split into groups */
	/*********************/
	/* Using column 1 */
	for (iline = 0; iline < nline; iline++)
	{
		igroup = (int)data [GROUP] [iline];
		if (igroup != igroupold)
		{
			/* First line number of new group */
			if (igroup >= NGROUP || igroup < 1)
			{
				szMsg.Format( _T("%s: ERROR; igroup=%d >=NGROUP=%d or <1."),
							  MODULE, igroup, NGROUP );
				OutputMessage( szMsg, LOG_PROC );
				fRet = FALSE;
				goto Cleanup;
			}
			iline1 [ngroup] = iline;
			igroupid [ngroup] = igroup;
			ngroup++;
			if (ngroup > NGROUP)
			{
				szMsg.Format( _T("%s: ERROR; ngroup=%d >NGROUP=%d."),
							  MODULE, ngroup, NGROUP );
				OutputMessage( szMsg, LOG_PROC );
				fRet = FALSE;
				goto Cleanup;
			}
		}
		igroupold = igroup;
	}
	iline1 [ngroup] = nline;

	/****************/
	/* Calculations */
	/****************/
	/* Each calculation is appended as additional columns */
	/* Calculations are across data within groups and subjects */
	if (fBetweenData)
	{
		/* New number of data */
		if (ndata + NDATAADDED >= NDATAMAX)
		{
			szMsg.Format( _T("%s: ERROR; #Data=%d + Added data=%d <NDATAMAX=%d."),
						  MODULE, ndata, NDATAADDED, NDATAMAX );
			OutputMessage( szMsg, LOG_PROC );
			fRet = FALSE;
			goto Cleanup;
		}

		/* Per line containing all subjects */
		for (iline = 0; iline < nline; iline++)
		{
			/* Estimate data range per subject */
			/* For compatibility with other programs iline is running fastest */
			/* Do not include indices in the range */
			for (idata = NINDEX; idata < ndata; idata++)
			{
				databuf [idata] = data [idata] [iline];
			}
			rmima0 (&databuf [NINDEX], ndata - NINDEX, &datamin, &datamax, ::GetMissingDataValue());
			gemx = means (&databuf [NINDEX], ndata - NINDEX, ::GetMissingDataValue());
			varx = vars (&databuf [NINDEX], ndata - NINDEX, gemx, ::GetMissingDataValue());
			sdx = sqrt ((double) varx);
			if (gemx != 0.)
				nsdx = sdx / gemx;
			else
				nsdx = 0.;

			/* y variance / x variance */
			w=10000.;
			/* Linear horizontal scale */
			for (idata = NINDEX; idata < ndata; idata++)
			{
				x [idata] = idata - NINDEX + 1;
			}
			rregr (&x [NINDEX], &databuf [NINDEX], ndata - NINDEX, w, &a, &b, &r);

			/* Append the extra data */
			data [ndata + 0] [iline] = gemx;
			/*10Dec03: Raji: Create a Label for each of calculated data to be written to output file*/
			Name.Add("Mean");
			data [ndata + 1] [iline] = sdx;
			Name.Add("SD");
			data [ndata + 2] [iline] = nsdx;
			Name.Add("SD/Mean");
			data [ndata + 3] [iline] = datamax - datamin;
			Name.Add("Range");
			data [ndata + 4] [iline] = b;
			Name.Add("Regression");
		}
		ndata += NDATAADDED;
	}

	/*********/
	/* write */
	/*********/
    /* 07OCt03: fTestAdded forced to 1 when group.cpp is called*/
	if (fTestAdded)
		testnot = 0;
	else
		testnot = NDATAADDED;
    /* Write the Y axis plotted label to the .grp file*/
	szMsg.Format( _T("%s%d\n"),szColumnLabel1, igroupid [igroup2]);
	fOut.WriteString( szMsg );
	/*09Dec03:Hlt: Perform every combination of sequence for group and group2 including doubles*/
	/* Each pair of groups including the same */
	//for (igroup = 0; igroup < ngroup; igroup++)
	//{
	//	for (igroup2 = 0; igroup2 <= igroup; igroup2++)
	//	{
	/* Need to consider upper limit as ngroup for the first two loops*/
	for (igroup = 0; igroup < ngroup; igroup++)
	{
		for (igroup2 = 0; igroup2 < ngroup; igroup2++)
		{
		/* Each pair of data or columns including the same */
			for (idata = NINDEX; idata < ndata; idata++)
			{
				for (idata2 = NINDEX; idata2 < ndata; idata2++)
				{
					if (fBetweenData || idata == idata2)
					{
						/* Xor: Do only those where only group or data differ */
						/* 07Oct03: Raji: Check for identical data within a group with ||(igroup == igroup2)&&(idata == idata2)*/
						if ((igroup != igroup2) ^ (idata != idata2)||(igroup == igroup2)&&(idata == idata2))
						{
							/* Added data: not compared with other data */
							if (idata < ndata - testnot || idata == idata2)
							{
								/* One package */
								szMsg.Format( _T("%4d %s%d_%s%d\n"),
											  iline1 [igroup + 1] - iline1 [igroup],
											  szColumnLabel3, igroup+1, szColumnLabel2, idata+1 - NINDEX);
								/* 07Oct03: Raji: Overwrite the label for all the extra columns*/
								/*10Dec03:Raji: Replaced multiple loops with single loop to write the labels for output*/
								if (fTestAdded)
								{
									pos = 0;
									for (int i = 0; i <=4; i++)
									{
										if(idata == ndata - NDATAADDED + i)
											szMsg.Format( _T("%4d %s%d_%s%d\n"),
												iline1 [igroup + 1] - iline1 [igroup],szColumnLabel3,
												igroup+1, Name.GetAt(pos + i), idata+1 - NINDEX );
									}
								}

								for (iline = iline1 [igroup]; iline < iline1 [igroup + 1]; iline++)
								{
									szTmp.Format( _T("%8.4g "), data [idata] [iline]);
									szMsg += szTmp;
								}
								szMsg += _T("\n");
								fOut.WriteString( szMsg );
								/* One package */
								szMsg.Format( _T("%4d %s%d_%s%d\n"),
											  iline1 [igroup2+ 1] - iline1 [igroup2],
											  szColumnLabel3, igroup2+1, szColumnLabel2, idata2+1 - NINDEX);
								/* 07Oct03: Raji: Overwrite the label for all the extra columns*/
								/*10Dec03:Raji: Replaced multiple loops with single loop to write the labels for output*/
								if (fTestAdded)
								{
									pos = 0;
									for (int i = 0; i <=4; i++)
									{
										if(idata2 == ndata - NDATAADDED + i)
											szMsg.Format( _T("%4d %s%d_%s%d\n"),
												iline1 [igroup2 + 1] - iline1 [igroup2],szColumnLabel3,
												igroup2+1, Name.GetAt(pos + i),idata2+1 - NINDEX );
									}
								}

								for (iline = iline1 [igroup2]; iline < iline1 [igroup2 + 1]; iline++)
								{
									szTmp.Format( _T("%8.4g "), data [idata2] [iline]);
									szMsg += szTmp;
								}
								szMsg += _T("\n");
								fOut.WriteString( szMsg );
							}
						}
					}
				}
			}
		}
	}

Cleanup:

  return fRet;
}

/********************************************************/
void rmima0 (double arr [], int n, double *min, double *max, double xmis)
/********************************************************/
/* Range of data array */
/* Real data */
{
  int i;

  *min = arr [0];
  *max = arr [0];

  for (i = 1; i < n; i++)
  {
    if (arr [i] != xmis)
	{
      if (arr [i] > *max)
        *max = arr [i];
      else if (arr [i] < *min)
        *min = arr [i];
    }
  }
} /* End rmima */

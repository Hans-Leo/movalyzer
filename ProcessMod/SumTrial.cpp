@/*************************************************
SumTial - version 1.2 - hlt - 18Jan1995
**************************************************

ARGUMENTS: 1/ ext_infile or con_file
2/ Inc_outfile
3/ ini_infile
4-6/ group, subj, condition nrs
7/ max #strokes/trial
8/ /z/t=Output also zero strokes/trials; /r=ratios to means

OUTPUT:
Each line contains numerical columns:
first some indices: group subj cond, pattern, ...
then data: #trials, #segments, data...

PURPOSES:
(1) Compiling all raw .ext or .con files
(2) Optional transformation: Absolute
(3) Optional normalization by dividing by average across replications
(4) Optional outputting of zero-strokes and zero-trials
(5) Splits the condition factor into two factors (customizable).

HISTORY:
6Oct94: Numerical arguments & data explicit and in fixed format.
Standardizing fixed nr of strokes option
7Oct94: Storing all info per condition in one array.
12Oct94: Customizable: all or only nonzero strokes
Divide by means across replications
Arguments /z /z/t and /r
17Nov94: Adapted for exp kp and exp hp.
Argument analysis added.
22Dec94: Added exp wr and exp qp.
23Dec94: Added absolute values (yields otherwise odd results in neg+pos values).
29Dec94: Added STOPPOS and VY for compatibility.
9 Jan95: More factors for Expwr
17Jan95: Renamed from include.c
18Jan95: ini_infile implemented instead of analysisnr
28Mar95: nsegmmax added; now compatible output with sumsubj.c
5May95: Output format was %8.3f becomes %8.4g
9Aug97: Output format becomes %.4g because files may get big
04Dec99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
removal of statics
26Jul01: hlt: Increased NTRIAL to 99
02May03: raji: nsegmmax was pointing to the next trial instead of the current trial
23Sep03: GMB: Added column headers to inc; removed nsegmax from output
20Apr04: Raji: Write only non-excluded columns to the output
18Sep06: GMB: Converted deprecated string/file functions for MSVS7
07May08: GMB: Increased # of data in. Adjusted column headers to exclude spaces.
09Jul08: GMB: Added extra columns for primary & secondary strokes when submovement
analysis is on (secondary var). Use FeatureData object in FeatureData.h
to determine which columns use only 1, 2 or all 3: primary, secondary, total
NOTE: Also increased column limite (was not sufficient)
13Aug08: GMB: Subm. Anal. added complexity when writing to one line...so it is now managed
by class objects defined in FeatureData.h/cpp
23Aug08: GMB: Added collapsing for up/down strokes & all strokes for each and all trials
22May09: GMB: Added inclusion/exclusion of strokes with/without pen-lifts using threshold
			  of RelativePenDownDuration
			  Combined above with collapsing.
26May09: GMB: Added include file ProcSettings.h for consistent use of defines such as NSEGMAX
09Jul09: GMB: Added timers for tracking duration of summarization steps.
			  Improved efficiency/speed with initializations and absolute value setting.
13Jul09: GMB: System did not handle substitution in cases of no consistent trials - now does
24Jul09: GMB: Subst. missing strokes was never implemented. Now implemented.
11Jun10: GMB: Fixed array initialization. Changed data input sscanf (which was limited to 35 items) to dynamic.
*/

/************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include "../Common/deffun.h"
#include "../Common/ProcSettings.h"
#include "../NSShared/iolib.h"
#include "FeatureData.h"
#include <math.h>
#include <winbase.h>

#define STARTT  0
#define DELTAT  1
// 14Aug08: GMB: These do not appear to be used
// #define STARTY  2
// #define DELTAY  3
// #define PEAKAY  4
// 07May08: GMB: Increased # of data columns from 25 to 35
//#define NDATAIN 25 /* Number of data columns excluding trial and segment columns*/
// 09Jul08: GMB: Increased # of data columns from 35 to 70 (32 columns added for submovement)
//#define NDATAIN 35 /* Number of data columns excluding trial and segment columns*/
#define NDATAIN 70 /* Number of data columns excluding trial and segment columns*/
#define NDATAEXT 10 /* External Apps may add columns to the EXT file. Allow 10 new columns(arbitrarily) to be added */
#define NINDEX 2 /* Number of index columns */
#define NINDEXFD 6 // number of index columns for feature data
#define NDATAMAX NDATAIN + NINDEX + NDATAEXT /* Total number of data columns*/

//#define NTRIAL 16   //24May00: hlt: was 12 which is too low. This may eventually be 100 or so.
//#define NTRIAL 25	  //07Sep00: gmb: changed this & nsegmmax to 25 until we can test with higher (was 16 for both)
#define NTRIAL 99	  //26jul01: hlt: 99 trials has already occurred
#define NCONDMAX 31   /* Max nr of conditions */
//#define NSEGMMAX 99   /* Max nr of strokes */
#define NSEGMMAX NSEGMAX	//26May09: gmb: now matches nsegmax of segmen, etc.

#define	MODULE		_T("SUMTRIAL")

struct _ArrayDataST
{
	float data [NTRIAL] [NSEGMMAX] [NDATAMAX];
	float meantrialdata [NSEGMMAX] [NDATAMAX];
	int col [ NDATAMAX ];
};

// PARAMETERS
bool _fCollapseUDStrokes = FALSE;		// collapse up & down strokes for each trial
bool _fCollapseStrokes = FALSE;			// collapse all strokes for each trial
bool _fCollapseTrials = TRUE;			// collapse all trials to strokes
// Trials + UDStrokes = collapse all trials to up/down strokes
// Trials + Strokes = collapse all trials to a stroke
bool _fMedian = FALSE;					// use median instead of average
void SetAveraging( bool fUDStrokes, bool fStrokes, bool fTrials, bool fMedian )
{
	_fCollapseUDStrokes = fUDStrokes;
	_fCollapseStrokes = fStrokes;
	_fCollapseTrials = fTrials;
	_fMedian = fMedian;
}
// include only trials with/out pen-lifts using threshold
bool _fWithoutPenLifts = FALSE;
bool _fWithPenLifts = FALSE;
double	_dPenDownDurThreshold = 0.5;
void SetIncludeOnlyStrokesPenlifts( bool fWithoutLifts, bool fWithLifts, double dThreshold )
{
	_fWithoutPenLifts = fWithoutLifts;
	_fWithPenLifts = fWithLifts;
	_dPenDownDurThreshold = dThreshold;
}

#pragma warning( disable: 4996 )	// disable deprecated warning

/************************************************************************/

/***********************************/
bool SumTrial( CString szDataIn, CString szIncOut, CString szErrOut,
			   CString szGroup, CString szSubj, CString szCond, unsigned int nSwitch,
			   int nTrialCount, int nDiscardAfter, int nDiscardAfter2,
			   bool fIgnoreExclude )
/***********************************/
{
	_ArrayDataST* pad = new _ArrayDataST;
	bool fRet = TRUE;
	CStdioFile fp_ext;
	FILE* fp_ini = NULL;
	FILE* fp_inc = NULL;
	CStdioFile fp_err, fp_err2;
	CString line, szMsg, szTemp, szTmpLine, szErrIn, szLastTrial,
		szColLine, szMask, szSumIn, szExp, szTemp2, szLineTemp;
	CStringList lstExt;
	int itrial = 0, ntrial = 0;
	int idata = 0;
	bool zerostrokes = FALSE, zerotrials = FALSE, ratiotomean = FALSE, absolute = FALSE,
		substtrials = FALSE, discafter = FALSE, discafter2 = FALSE;
	int ncondout = 0, ncondin = 0, nsegmout = 0, nDataTemp = 0;
	int idataout1 = STARTT;
	int idataout2 = NDATAMAX - NINDEX - 1;
	int isegmout = 0, isegmcode = 0, isegm = 0;
	int nsegmmax [NTRIAL];
	POSITION pos = NULL;
	int nLastTrial = 0, h = 0, i = 0, j = 0, nNextTrial = 1, nLastSeg = 0;
	int nFirstTrial = 0, nFirstTrialSegCount = 0, nPrevSeg = 0;
	int nPos = 0;
	bool fFirstSet = FALSE;
	CFileFind ff;
	bool fAlreadyWroteHeader = FALSE;
	int nLine = 0, ncol = 0, nCount = 0;
	static int nCountCompare = 0;
	bool writeoneline = false;	// for submovement analysis with one line ONLY
	double dVal = 0.;
	// Somesh: 12Jun09: Replace insertion of zeros with insertion of Missing data value instead
	double dMissing = ::GetMissingDataValue();
	NSProc::Feature* pFeature = NULL;
	NSProc::Stroke* pStroke = NULL;
	NSProc::Segment* pSegment = NULL;
	NSProc::Summary* pSummary = new NSProc::Summary();

	// PROCESS TIMING (added 09Jul09: GMB)
	LARGE_INTEGER freq, startAll, stopAll;
	double dTimeSetup = 0., dTimeInput = 0., dTimeSubst = 0., dTimeOut = 0.,
		   dTimeCollapse = 0., dTimeErr = 0., dTimeRatio = 0.;
	unsigned int nLines = 0, nWritten = 0;
	CString szTimeMsg;
	// get frequency of high-resolution performance counter
	::QueryPerformanceFrequency( &freq );
	// start timer
	StopWatch( 0, startAll, stopAll, freq );

	/************************************************************************/
	/* Initialize                                                           */
	/************************************************************************/
// 09Jul09: GMB: Looping to initialize is ridiculously inefficient
	ZeroMemory( pad->data, NTRIAL * NSEGMAX * NDATAMAX * sizeof( float ) );
	ZeroMemory( nsegmmax, NTRIAL * sizeof( int ) );
// 	for (itrial = 0; itrial < NTRIAL; itrial++)
// 	{
// 		for (isegm = 0; isegm < NSEGMMAX; isegm++)
// 		{
// 			for (idata = 0; idata < NDATAMAX; idata++)
// 			{
// 				pad->data [itrial] [isegm] [idata] = 0.; /* NDATAMAX columns*/
// 			}
// 		}
// 	}
// 	for (itrial = 0; itrial < NTRIAL; itrial++)
// 		nsegmmax [itrial] = 0;

	/***************************************@*********************************/
	/* Switches                                                             */
	/************************************************************************/
	if( ( nSwitch & ST_SWITCH_A ) != 0 )
	{
		absolute = TRUE;
		szMsg.Format( IDS_ST_ABS, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_SM ) != 0 )
	{
		pSummary->SetIsSMA( TRUE );
		szMsg.Format( IDS_EXT_SEC, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_ONE ) != 0 )
	{
		pSummary->SetIsOneLine( TRUE );
	}
	if( ( nSwitch & ST_SWITCH_R ) != 0 )
	{
		ratiotomean = TRUE;
		szMsg.Format( IDS_ST_MEAN, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_Z ) != 0 )
	{
		zerostrokes = TRUE;
		szMsg.Format( IDS_ST_ZEROSTROKES, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_T ) != 0 )
	{
		zerotrials = TRUE;
		szMsg.Format( IDS_ST_ZEROTRIALS, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_S ) != 0 )
	{
		substtrials = TRUE;
		zerotrials = FALSE;
		szMsg.Format( IDS_ST_SUBSTTRIALS, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_D ) != 0 )
	{
		discafter = TRUE;
		szMsg.Format( IDS_ST_DISC, MODULE, nDiscardAfter );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & ST_SWITCH_D2 ) != 0 )
	{
		discafter2 = TRUE;
		szMsg.Format( IDS_ST_DISC2, MODULE, nDiscardAfter2 );
		OutputMessage( szMsg, LOG_PROC );
	}

	/************************************************************************/
	/* if INC file does not exist, we shall write headers                   */
	/************************************************************************/
	if( ff.FindFile( szIncOut ) )
	{
		fAlreadyWroteHeader = TRUE;		// flag to write headers
	}
	else
	{
		nCountCompare = 0;				// reinitialize input count static var
	}

	/************************************************************************/
	// if not submovement analysis or specified one line, set to true (ie, ignore)
	/************************************************************************/
	if( !pSummary->IsSMA() || !pSummary->IsSMAOneLine() ) writeoneline = true;


	/************************************************************************/
	/* Open ext_file and read till end of file (CONSIS FILE)				*/
	/************************************************************************/
	if( !fp_ext.Open( szDataIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_ST_EXTERR, MODULE, szDataIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	szMsg.Format( IDS_ST_EXTREAD, MODULE, szDataIn );
	OutputMessage( szMsg, LOG_PROC );


	/************************************************************************/
	// set pen-lift inclusion/exclusion
	/************************************************************************/
	pSummary->SetPenLiftCriteria( _fWithoutPenLifts, _fWithPenLifts, _dPenDownDurThreshold );

	/************************************************************************/
	/* add group, subject, & condition info to main summary object			*/
	/************************************************************************/
	pSummary->SetGroup( szGroup );
	pSummary->SetSubject( szSubj );
	pSummary->SetCondition( szCond );


	/************************************************************************/
	/* Read Excluded Features File                                          */
	/*																		*/
	/* If none read, we ignore exclusion                                    */
	/************************************************************************/
	// extract exclude file from INC out file
	nPos = szIncOut.ReverseFind( '.' );
	szMask = szIncOut.Left( nPos );
	szSumIn = szMask;
	szSumIn += _T("-what.SUM");
	fIgnoreExclude = pSummary->ReadExcludes( szSumIn );
	// get count of excluded features
	ncol = fIgnoreExclude ? 0 : pSummary->GetExcludeCount();


	/************************************************************************/
	/* Read all the column labels in the con file and store					*/
	/************************************************************************/
	pSummary->SetDataMax( NDATAMAX );
	nCount = pSummary->ReadFeatures( &fp_ext );
	// compare the count to the last (unless first)
	if( nCountCompare != 0 )
	{
		if( nCountCompare != nCount )
		{
			CString szFileName = szDataIn;
			nPos = szDataIn.ReverseFind( '\\' );
			if( nPos != -1 ) szFileName = szDataIn.Mid( nPos + 1 );
			szMsg.Format( _T("%s: WARNING: # of columns in %s is inconsistent. This is likely due to processing from an older application version. Please reprocess all trials and repeat summarize."), MODULE, szFileName );
			OutputMessage( szMsg, LOG_PROC );
			fRet = END_FALSE;
			goto Cleanup;
		}
	}
	nCountCompare = nCount;


	/************************************************************************/
	/* Set the status to not show if in the exclude list                    */
	/************************************************************************/
	pSummary->UpdateForExclusions();

	// get setup time
	dTimeSetup = StopWatch( 1, startAll, stopAll, freq );
	// reset timer
	StopWatch( 0, startAll, stopAll, freq );

	/************************************************************************/
	/* Input data															*/
	/************************************************************************/
	itrial = 0;
	while( !discafter2 || ( discafter2 && ( nDiscardAfter2 >= itrial ) ) )
	{
		if( !fp_ext.ReadString( line ) )
		{
			if( substtrials || zerotrials )
			{
				// Test to ensure we have all trials

				if( ( nLastTrial < nTrialCount ) && ( nLastTrial > 0 ) )
				{
					pos = NULL;

					while( ( nLastTrial < nTrialCount ) &&
						( !discafter || ( discafter &&
						( nDiscardAfter > nLastTrial ) ) ) )
					{
						nLastTrial++;

						if( substtrials )
						{
							szMsg.Format( IDS_ST_SUBSTMSG, MODULE, szGroup, szSubj,
								szCond, nLastTrial - 1, nLastTrial );
							OutputMessage( szMsg, LOG_PROC );
						}

						int nSegs = substtrials ? nLastSeg : ( pSummary->IsSMA() ? 6 : 2 );
						for( i = 0; i < nSegs; i++ )
						{
							szTemp.Format( _T("%d %d"), nLastTrial, i + 1 );
							if( !pos ) pos = lstExt.Find( szLastTrial );
							if( pos ) pos = lstExt.InsertAfter( pos, szTemp );

							for( j = 0; j < NDATAIN; j++ )
							{
								if( substtrials )
									pad->data[ nLastTrial - 1 ][ i ][ j ] = pad->data[ nLastTrial - 2 ][ i ][ j ];
								else if( zerotrials )
									pad->data[ nLastTrial - 1 ][ i ][ j ] = (float)dMissing;
							}
						}
					}

				}
			}

			break;
		}

		sscanf (line, "%d %d", &i, &isegm);
		if( i != itrial )
		{
			nNextTrial++;
			itrial = i;
			pos = NULL;
			nPrevSeg = 0;

			if( nFirstTrial == 0 )
			{
				nFirstTrial = itrial;
			}
			else if( !fFirstSet )
			{
				nFirstTrialSegCount = nLastSeg;
				fFirstSet = TRUE;
			}

			if( zerotrials || substtrials )
			{
				// Check for previous trial(s) existence
				while( ( ( nNextTrial - itrial ) <= 0 ) &&
					( !discafter || ( discafter &&
					( nDiscardAfter > ( nNextTrial - 1 ) ) ) ) )
				{
					if( substtrials && ( nLastTrial > 0 ) )
					{
						szMsg.Format( IDS_ST_SUBSTMSG, MODULE, szGroup, szSubj,
							szCond, nLastTrial, nNextTrial - 1 );
						OutputMessage( szMsg, LOG_PROC );
					}

					int nSegs = substtrials ? nFirstTrialSegCount : ( pSummary->IsSMA() ? 6 : 2 );
					for( i = 0; i < nSegs; i++ )
					{
						szTemp.Format( _T("%d %d"), nNextTrial - 1, i + 1 );
						if( !pos ) pos = lstExt.Find( szLastTrial );
						if( pos ) pos = lstExt.InsertAfter( pos, szTemp );

						for( j = 0; j < NDATAIN; j++ )
						{
							if( substtrials && ( nLastTrial > 0 ) )
								pad->data[ nNextTrial - 2 ][ i ][ j ] = pad->data[ nLastTrial - 1 ][ i ][ j ];
							else if( zerotrials )
								pad->data[ nNextTrial - 2 ][ i ][ j ] = (float)dMissing;
						}
					}

					nNextTrial++;
				}
			}

			nLastSeg = isegm;
			if( ( nNextTrial - itrial ) > 0 ) nLastTrial = itrial;
		}
		else nLastSeg++;

		// 24Jul09: GMB: Handle missing segments
		if( zerostrokes )
		{
			// if the last read segment is not +1 from previously read, we have missing strokes
			if( isegm > ( nPrevSeg + 1 ) )
			{
				// Insert missing strokes: From previously read segment to current segment
				for( int nSeg = nPrevSeg + 1; nSeg < isegm; nSeg++ )
				{
					if( !discafter2 || ( discafter2 && ( nDiscardAfter2 >= itrial ) ) )
					{
						szLastTrial.Format( _T("%d %d"), itrial, nSeg );
						lstExt.AddTail( szLastTrial );
						nsegmmax [itrial-1] = MAX (nsegmmax [itrial-1], nSeg);

						for( int nFeature = 0; nFeature < NDATAIN; nFeature++ )
						{
							pad->data[ itrial - 1 ][ nSeg - 1 ][ nFeature ] = (float)dMissing;
						}

						ncondin++;
					}
				}
				nPrevSeg = isegm;
			}
			else nPrevSeg++;
		}
		else nPrevSeg++;

		/* Only now itrial and isegm are available */
		if (itrial > NTRIAL)
		{
			szMsg.Format( IDS_ST_TRIALDISC, MODULE, itrial, NTRIAL );
			OutputMessage( szMsg, LOG_PROC );
		}
		else if (isegm > NSEGMMAX)
		{
			szMsg.Format( IDS_ST_STROKEDISC, MODULE, isegm, NSEGMMAX );
			OutputMessage( szMsg, LOG_PROC );
		}
		else if (itrial < 1 || isegm < 1)
		{
			szMsg.Format( IDS_ST_RANGE, MODULE, itrial, isegm );
			OutputMessage( szMsg, LOG_PROC );
			fRet = FALSE;
			goto Cleanup;
		}
		else if( !discafter2 || ( discafter2 && ( nDiscardAfter2 >= itrial ) ) )
		{
			szLastTrial.Format( _T("%d %d"), itrial, isegm );
			lstExt.AddTail( szLastTrial );

			/* Cumulate nsegmmax */
			nsegmmax [itrial-1] = MAX (nsegmmax [itrial-1], isegm);

			// extract columnar data...skip first two data items (trial # & segment #)
			szLineTemp = line;
			nDataTemp = 0;
			int nPos = szLineTemp.Find( _T(" ") );
			if( nPos != -1 ) {
				szLineTemp = szLineTemp.Mid( nPos + 1 );
				nPos = szLineTemp.Find( _T(" " ) );
				if( nPos != -1 ) {
					szLineTemp = szLineTemp.Mid( nPos + 1 );
					nPos = szLineTemp.Find( _T(" " ) );
					while( nPos != -1 ) {
						if( nDataTemp >= NDATAMAX ) {
							break;
						}
						pad->data[ itrial - 1][ isegm - 1][ nDataTemp++ ] = (float)atof( szLineTemp.Left( nPos ) );
						szLineTemp = szLineTemp.Mid( nPos + 1 );
						nPos = szLineTemp.Find( _T(" " ) );
					}
					// last point
					if( nDataTemp < NDATAMAX ) {
						szLineTemp.Trim();
						pad->data[ itrial - 1 ][ isegm - 1 ][ nDataTemp ] = (float)atof( szLineTemp );
					}
				}
			}

			/**columns ******1   2  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35**/
/*			sscanf (line, "%*d %*d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
				&pad->data [itrial-1] [isegm-1] [0],
				&pad->data [itrial-1] [isegm-1] [1],
				&pad->data [itrial-1] [isegm-1] [2],
				&pad->data [itrial-1] [isegm-1] [3],
				&pad->data [itrial-1] [isegm-1] [4],
				&pad->data [itrial-1] [isegm-1] [5],
				&pad->data [itrial-1] [isegm-1] [6],
				&pad->data [itrial-1] [isegm-1] [7],
				&pad->data [itrial-1] [isegm-1] [8],
				&pad->data [itrial-1] [isegm-1] [9],
				&pad->data [itrial-1] [isegm-1] [10],
				&pad->data [itrial-1] [isegm-1] [11],
				&pad->data [itrial-1] [isegm-1] [12],
				&pad->data [itrial-1] [isegm-1] [13],
				&pad->data [itrial-1] [isegm-1] [14],
				&pad->data [itrial-1] [isegm-1] [15],
				&pad->data [itrial-1] [isegm-1] [16],
				&pad->data [itrial-1] [isegm-1] [17],
				&pad->data [itrial-1] [isegm-1] [18],
				&pad->data [itrial-1] [isegm-1] [19],
				&pad->data [itrial-1] [isegm-1] [20],
				&pad->data [itrial-1] [isegm-1] [21],
				&pad->data [itrial-1] [isegm-1] [22],
				&pad->data [itrial-1] [isegm-1] [23],
				&pad->data [itrial-1] [isegm-1] [24],
				&pad->data [itrial-1] [isegm-1] [25],
				&pad->data [itrial-1] [isegm-1] [26],
				&pad->data [itrial-1] [isegm-1] [27],
				&pad->data [itrial-1] [isegm-1] [28],
				&pad->data [itrial-1] [isegm-1] [29],
				&pad->data [itrial-1] [isegm-1] [30],
				&pad->data [itrial-1] [isegm-1] [31],
				&pad->data [itrial-1] [isegm-1] [32],
				&pad->data [itrial-1] [isegm-1] [33],
				&pad->data [itrial-1] [isegm-1] [34],
				&pad->data [itrial-1] [isegm-1] [35],
				&pad->data [itrial-1] [isegm-1] [36]);
*/
			ncondin++;
		}
	}
	fp_ext.Close();

	// get input time
	dTimeInput = StopWatch( 1, startAll, stopAll, freq );
	// reset timer
	StopWatch( 0, startAll, stopAll, freq );

	// Substitution/output missing trials option (missing initial trials and no trials)
	if( zerotrials || substtrials )
	{
		// Check for missing initial trials
		if( fFirstSet && ( nFirstTrial > 1 ) )
		{
			pos = NULL;

			for( h = 1; h < nFirstTrial; h++ )
			{
				if( !discafter || ( discafter && ( nDiscardAfter > h ) ) )
				{
					if( substtrials )
					{
						szMsg.Format( IDS_ST_SUBSTMSG, MODULE, szGroup, szSubj,
							szCond, nFirstTrial, h );
						OutputMessage( szMsg, LOG_PROC );
					}
// 13JUL09: GMB: i < nFirstTrialSegCount ... will miss the last segment (so, use <=)
//					for( i = 1; i < nFirstTrialSegCount; i++ )
					int nSegs = substtrials ? nFirstTrialSegCount : ( pSummary->IsSMA() ? 6 : 2 );
					for( i = 1; i <= nSegs; i++ )
					{
						szTemp.Format( _T("%d %d"), h, i );
						if( !pos ) pos = lstExt.AddHead( szTemp );
						else pos = lstExt.InsertAfter( pos, szTemp );

						for( j = 0; j < NDATAIN; j++ )
						{
							if( substtrials )
								pad->data[ h - 1 ][ i - 1 ][ j ] = pad->data[ nFirstTrial - 1 ][ i - 1 ][ j ];
							else
								pad->data[ h - 1 ][ i - 1 ][ j ] = (float)dMissing;
						}
					}
				}
			}
		}
// 13JUL09: GMB: We must handle cases of no first trial (i.e. no trials for the condition)
		else if( !fFirstSet && zerotrials )
		{
			// 1 for each trial...
			for( h = 1; h <= nTrialCount; h++ )
			{
				// we still are aware of discarding after trial #
				if( !discafter || ( discafter && ( nDiscardAfter > h ) ) )
				{
					// # of segments = 2 (1 upstroke + 1 downstroke) for non-sma
					//                 6 ( up/down strokes per segment: primary, secondary, total)
					// this choice is rather arbitrary, but 2 strokes was chosen to support
					// up/down stroke collapsing
					int nSegs = pSummary->IsSMA() ? 6 : 2;
					for( i = 1; i <= nSegs; i++ )
					{
						szTemp.Format( _T("%d %d"), h, i );
						if( !pos ) pos = lstExt.AddHead( szTemp );
						else pos = lstExt.InsertAfter( pos, szTemp );

						for( j = 0; j < NDATAIN; j++ )
						{
							pad->data[ h - 1 ][ i - 1 ][ j ] = (float)dMissing;
						}
					}
				}
			}
		}
	}

	// get subst/zerot time
	dTimeSubst = StopWatch( 1, startAll, stopAll, freq );
	// reset timer
	StopWatch( 0, startAll, stopAll, freq );

	/************************************************************************/
	/* Processing															*/
	/************************************************************************/
// 09Jul09: GMB: This was a bottleneck. Moved setting to absolute when value is used.
// 	if (absolute)
// 	{
// 		szMsg.Format( IDS_STABS, MODULE );
// 		OutputMessage( szMsg, LOG_PROC );
//
// 		for (isegm = 0; isegm < NSEGMMAX; isegm++)
// 		{
// 			for (idata = 0; idata < NDATAMAX; idata++)
// 			{
// 				for (itrial = 0; itrial < NTRIAL; itrial++)
// 				{
// 					pad->data [itrial] [isegm] [idata] = (float)fabs ((double) pad->data [itrial] [isegm] [idata]);
// 				}
// 			}
// 		}
// 	}

	if (ratiotomean)
	{
		szMsg.Format( IDS_STMEAN, MODULE );
		OutputMessage( szMsg, LOG_PROC );

		/* Mean across trials */
		for (isegm = 0; isegm < NSEGMMAX; isegm++)
		{
			for (idata = 0; idata < NDATAMAX; idata++)
			{
				ntrial = 0;
				pad->meantrialdata [isegm] [idata] = 0.;
				for (itrial = 0; itrial < NTRIAL; itrial++)
				{
					pad->meantrialdata [isegm] [idata] += pad->data [itrial] [isegm] [idata];
					if (pad->data [itrial] [isegm] [DELTAT] != 0.) ntrial++;
				}
				if (ntrial > 0) pad->meantrialdata [isegm] [idata] /= ntrial;
			}
		}

		/* Divide mean */
		for (isegm = 0; isegm < NSEGMMAX; isegm++)
		{
			for (idata = 0; idata < NDATAMAX; idata++)
			{
				if (pad->meantrialdata [isegm] [idata] != 0.)
				{
					for (itrial = 0; itrial < NTRIAL; itrial++)
					{
						pad->data [itrial] [isegm] [idata] /= pad->meantrialdata [isegm] [idata];
					}
				}
			}
		}
	}

	// get ratio-mean time
	dTimeRatio = StopWatch( 1, startAll, stopAll, freq );
	// reset timer
	StopWatch( 0, startAll, stopAll, freq );

	/************************************************************************/
	/* Open and write output file											*/
	/************************************************************************/
	if (( fopen_s (&fp_inc, szIncOut, "a")) != 0)
	{
		szMsg.Format( IDS_ST_INCERR, MODULE, szIncOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	szMsg.Format( IDS_ST_INCOUT, MODULE, szIncOut, szGroup, szSubj, szCond );
	if (zerotrials)
	{
		szTemp.LoadString( IDS_ST_INCOUT1 );
		szMsg += szTemp;
	}
	if (zerostrokes)
	{
		szTemp.LoadString( IDS_ST_INCOUT2 );
		szMsg += szTemp;
	}
	OutputMessage( szMsg, LOG_PROC );

	/************************************************************************/
	/* OUTPUT																*/
	/************************************************************************/
	// Write Headers (if not already have)
	if( !fAlreadyWroteHeader )
	{
		line = _T("Group Subject Condition Trial Segment");
		pSummary->OutputHeaders( line, _fCollapseTrials || _fCollapseUDStrokes || _fCollapseStrokes );
		line += _T("\n");
		fprintf( fp_inc, line );
	}

	// Scan data into summary object and output
	pos = lstExt.GetHeadPosition();
	itrial = -1;
	while( pos )
	{
		// create segment object if not already created
		if( !pSegment )
		{
			pSegment = new NSProc::Segment;
			pSummary->AddSegment( pSegment );
		}
		// if subm anal & selected
		if( pSummary->IsSMA() && pSummary->IsSMAOneLine() )
		{
			// next line
			nLine++;
			// if not yet 3rd line (total movement), do not write to file
			if( nLine < 3 ) writeoneline = false;
			// else write to file
			else writeoneline = true;
		}
		// if we can write, write the constant data
		szTemp = lstExt.GetNext( pos );
		szTemp.TrimRight();
		sscanf( szTemp, _T("%d %d"), &itrial, &isegmout );
		if( ( itrial < 0 ) || ( isegmout < 0 ) ) break;
		if( writeoneline )
		{
			pSegment->SetTrial( itrial );
			pSegment->SetSegment( isegmout );
		}
		// loop through non-constant data and write or store (if subm anal)
		for( idata = idataout1; idata <= idataout2; idata++ )
		{
			// get stroke object (depends on sma)
			pFeature = pSegment->FindFeature( idata );

			// if sub=movement anal & selected..
			if( pFeature && pSummary->IsSMA() && pSummary->IsSMAOneLine() )
			{
				// collect the data & place in temp array
				if( ( nLine <= 3 ) && ( idata < NDATAMAX ) )
				{
					// get value
					dVal = pad->data[ itrial - 1 ][ isegmout - 1 ][ idata ];
					// set absolute if specified and NOT missing data value
					if( absolute && ( dVal != dMissing ) ) dVal = fabs( dVal );
					// check for range
					if( ( abs( dVal ) < 0.00001 ) || ( abs( dVal ) > MAXDWORD ) ) dVal = 0.;
					// get appropriate stroke
					if( nLine == 1 )
						pStroke = pFeature->GetMovements().GetStrokeByType( NSProc::Stroke::StrokeType_Primary );
					else if( nLine == 2 )
						pStroke = pFeature->GetMovements().GetStrokeByType( NSProc::Stroke::StrokeType_Secondary );
					else
						pStroke = pFeature->GetMovements().GetStrokeByType( NSProc::Stroke::StrokeType_Total );
					// set stroke value
					if( pStroke ) pStroke->SetValue( dVal );
				}
			}
			// else just assign total to object
			else if( pFeature )
			{
				// get stroke
				pStroke = pFeature->GetMovements().GetStrokeByType( NSProc::Stroke::StrokeType_Total );
				// set stroke value
				if( pStroke ) pStroke->SetValue( absolute ? fabs( pad->data[ itrial - 1 ][ isegmout - 1 ][ idata ] ) :
															pad->data[ itrial - 1 ][ isegmout - 1 ][ idata ] );
			}
		}

		// write data if 'writeoneline'
		if( writeoneline )
		{
			line = _T(" ");
			// if not collapsing, output
			if( !_fCollapseUDStrokes && !_fCollapseStrokes && !_fCollapseTrials )
			{
				if( pSummary->OutputData( line, pSegment, TRUE ) )
				{
					pSegment->OutputData( line, TRUE );
					fprintf( fp_inc, line );
					fprintf( fp_inc, _T("\n") );
				}
			}

			// nullify segment object for next iteration
			pSegment = NULL;

			// next while iteration will start fresh with next stroke
			nLine = 0;
		}
	}

	// get output/associate time
	dTimeOut = StopWatch( 1, startAll, stopAll, freq );
	// reset timer
	StopWatch( 0, startAll, stopAll, freq );

	//////////////////////////////////////////////////////////////////////////
	// COLLAPSING DATA
	//////////////////////////////////////////////////////////////////////////
	if( _fCollapseTrials || _fCollapseUDStrokes || _fCollapseStrokes )
	{
		// Collapsing
		if( _fCollapseTrials && _fCollapseUDStrokes ) pSummary->CollapseAllUD( _fMedian );
		else if( _fCollapseTrials && _fCollapseStrokes ) pSummary->CollapseAll( _fMedian );
		else if( _fCollapseUDStrokes ) pSummary->CollapseUDStrokes( _fMedian );
		else if( _fCollapseStrokes ) pSummary->CollapseStrokes( _fMedian );
		else if( _fCollapseTrials ) pSummary->CollapseTrials( _fMedian );
		// Output
		pSummary->OutputAllData( fp_inc );
	}

	// get collapse time
	dTimeCollapse = StopWatch( 1, startAll, stopAll, freq );
	// reset timer
	StopWatch( 0, startAll, stopAll, freq );

	//////////////////////////////////////////////////////////////////////////
	// CONCATENATE ERR FILES
	//////////////////////////////////////////////////////////////////////////
	// Open IN File
	szErrIn = szDataIn.Left( szDataIn.GetLength() - 3 );	// This was EXT file
	szErrIn += _T("ERR");
	if( !fp_err.Open( szErrIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_ST_ERRERR, MODULE, szErrIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	szMsg.Format( IDS_ST_ERRREAD, MODULE, szErrIn );
	OutputMessage( szMsg, LOG_PROC );
	// Open OUT File
	if( !fp_err2.Open( szErrOut, CFile::modeWrite | CFile::modeCreate |
		CFile::shareDenyNone | CFile::modeNoTruncate ) )
	{
		szMsg.Format( IDS_ST_ERROUTERR, MODULE, szErrOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	// Write/Concatenate
	fp_err2.SeekToEnd();
	while( fp_err.ReadString( szTemp ) )
	{
		CString szTemp2, szT;
		// extract trial
		nPos = szTemp.Find( _T(" ") );
		if( nPos != -1 )
		{
			szT = szTemp.Left( nPos );
			szTemp = szTemp.Mid( nPos + 1 );
		}
		else szT = _T("?");

		szTemp2.Format( _T("%s %s %s %s ;"), szGroup, szSubj, szCond, szT );
		fp_err2.WriteString( szTemp2 );
		fp_err2.WriteString( szTemp );
		fp_err2.WriteString( _T("\n") );
	}


Cleanup:

	/* Close files */
	if( fp_ext.m_pStream ) fp_ext.Close();
	if( fp_inc ) fclose( fp_inc );
	if( fp_ini ) fclose( fp_ini );
	if( fp_err.m_pStream ) fp_err.Close();
	if( fp_err2.m_pStream ) fp_err2.Close();
	if( pad ) delete pad;
	if( pSummary ) delete pSummary;

	// get finalization/cleanup time
	dTimeErr = StopWatch( 1, startAll, stopAll, freq );
	// output final time info
	szTimeMsg.Format( _T("%s: PROCESS TIME: Setup=%.4f s, Input=%.4f s, Zero/Subst=%.4f s, RatioToMean=%.4f s, Output=%.4f s, Collapse=%.4f s, Err/Cleanup=%.4f s, Total=%.4f s"),
		MODULE, dTimeSetup, dTimeInput, dTimeSubst, dTimeRatio, dTimeOut, dTimeCollapse, dTimeErr,
		dTimeSetup + dTimeInput + dTimeSubst + dTimeRatio + dTimeOut + dTimeCollapse + dTimeErr );
	::OutputMessage( szTimeMsg, LOG_PROC );

	return fRet;
}

#pragma warning( default: 4996 )

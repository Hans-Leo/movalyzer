/************************************************
   Unrot - version 1.4 - hlt - 22 July 1994
**************************************************

PURPOSE:
     (1) Rotates patterns of straight strokes to vertical orientation.
     (2) Orientation is regression of velocity.
     (3) Rotates only when velocity direction between +-BMAX = dy/dx.
     (4) Rotates specified angle in counterclockwise direction (overrules estimate).
     (5) Strips penup part estimating angle.
     (6) Allows testing linearity of the tablet: rotate to vertical and display y axis.

ARGUMENTS: hwr_infile hwr_outfile [angle in radians]

UPDATES:
  14Jun94: Not rotating counterclockwise to vertical but rotating so that
             flexion is down.
           Not: Rotating counterclockwise to first vertical:
             1 o'clock becomes 12 ;  5 o'clock becomes 12 ;
             7 o'clock becomes  6 ; 11 o'clock becomes  6 ;
           But: 4, 5, 6, 7, 8 o'clock becomes 6 AND 9 becomes also 6.
  20Jul94: gethwr() included.
  22Jul94: mdist instead of mqdist
  22Dec94: minpres=20->1 as it is not read from .def file
   7Jun95: Text of clockwise changed into counterclockwise
  14May97: Add options differy and velocity (was default before)
  20Jun97: Accept .def files; 'differentiate' relative by dmax/dmin
  14Jul97: Extended output with straightness err, etc. Bug: no rotation if specified restored
   8Dec98: Block-by-block read for very long datasets.
           Keep_DC option removed
  19Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		   removal of statics
  16Nov06  Yi: Change the windows results output
  30Nov06  Yi: Implement the algirothm for the rejection of bad data before isBegin and after isEnd

************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include <algorithm>
#include "../Common/ProcSettings.h"
#include "../NSShared/hlib.h"
#include "../NSShared/iolib.h"

//Yi : change windows results output
//#define	MODULE		_T("UNROT")
#define	MODULE		_T("Linearity Test")

#define BMAX			1000.0	// 0.3     /* Which range of b should be transformed */

/*************************************************************************/
bool Unrotate( CString szHWRIn, CString szHWROut, bool fVelocity,
			   bool fDifferentiate, double dRotationBeta )
/*************************************************************************/
{
	bool fRet = TRUE;

	double* x = new double[ NSMAX ];
	double* y = new double[ NSMAX ];
	double* z = new double[ NSMAX ];
	double* vx = new double[ NSMAX ];
	double* vy = new double[ NSMAX ];
	double xlength = 0.0, ylength = 0.0;

	int is = 0, ns = 0, nsin = 0;
	CStdioFile fIn, fOut;
	double sinb, cosb, beta, mdist;
	double w, a, b, r, ds;
	/* Do not decimate as the decimation factor will not be transmitted */
	/* Larger files can only be handled with preset angle */
	int decimate = 1;
	int keep_dc = 0, nread = 0;
	double cm = 0.0, sec = 0.0, prsmin = 0.0;
	CString szMsg;
	double ymin = 0.0, ymax = 0.0;
	bool fnextblock = FALSE;
	int blocknr = 0;
	int *isBegin = new int[NSEGMAX];
	int *isEnd = new int[NSEGMAX];
	double xmean = 0.0, strokelength = 0.0;
	double percentagemissing = 0.;
	double percentageextra = 0.;

	double mfactor = 0.5;
	double ravg = 0.;
	int iseg;
	double MissingSampleCounter;
	int b0=0, b1=0, b2=0, b3=0, b4=0, b5=0, b6=0, bb=0;

	double dVelocityAverage=0;
	double dVelocityMin = 1.33 * dVelocityAverage;
	int iCounterThld=3;
	double dVelocity=0;
	int iCounterVelocity = 0;
	int nseg = 0;
	int nsout = 0;
	int movementrange = 0;

	/********************************/
	/*** Read dynamic definitions ***/
	/********************************/
	ReadDynamicDefs( cm, sec, prsmin );

	//Linearity test results
	//|--------------------------------------------
	szMsg.Format( IDS_UDD5, MODULE);
	OutputMessage( szMsg, LOG_PROC );
	//output
	szMsg.Format( IDS_UDD, MODULE);
	OutputMessage( szMsg, LOG_PROC );
	//Resolutuion
	szMsg.Format( IDS_UDD2,MODULE,cm);
	OutputMessage( szMsg, LOG_PROC );
    //Sampling Rate
	szMsg.Format( IDS_UDD3,MODULE,(1/sec));
	OutputMessage( szMsg, LOG_PROC );
    //Minimum Pen Pressure
	szMsg.Format( IDS_UDD4, MODULE, prsmin);
	OutputMessage( szMsg, LOG_PROC );

	//|-----------------------------------------
	szMsg.Format( IDS_UDD5, MODULE);
	OutputMessage( szMsg, LOG_PROC );

	/*********************/
	/*** Open Input    ***/
	/*********************/
	if( !fIn.Open( szHWRIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_UINP_ERR, MODULE, szHWRIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/* Open now already because if repeated reads and writes */
	/*********************/
	/*** Open Output    ***/
	/*********************/
	if( !fOut.Open( szHWROut, CFile::modeWrite ) )
	{
		if( !fOut.Open( szHWROut, CFile::modeCreate | CFile::modeWrite ) )
		{
			szMsg.Format( IDS_UOUT_ERR, MODULE, szHWROut );
			OutputMessage( szMsg, LOG_PROC );
			fRet = FALSE;
			goto Cleanup;
		}
	}

	/***************/
	/*** Reading ***/
	/***************/
	/* Next block of NSMAX data */
	blocknr = 0;
nextblock:
	blocknr++;
	szMsg.Format( IDS_UINP_READ, MODULE, szHWRIn);
	OutputMessage( szMsg, LOG_PROC );

	// Read one block of NSMAX samples
	nsin = gethwr( fIn, x, y, z, NSMAX, NSMAX, 0, decimate, prsmin, TRUE, ::GetRemoveTrailingPenlift() );
	fIn.Close();

	/*** input checks ***/
	fnextblock = FALSE;
	if (nsin == NSMAX)
	{
		szMsg.Format( IDS_UINP_CNT, MODULE, nsin, decimate);
		OutputMessage( szMsg, LOG_PROC );

		if (z [nsin - 1] >= prsmin)
		{
			szMsg.Format( IDS_UINP_TRUNC, MODULE, z [nsin-1], prsmin);
			OutputMessage( szMsg, LOG_PROC );
		}

		/* Probably there are more blocks */
		fnextblock = TRUE;
	}

	// If no more samples are left to be read, remove temporary arrays and return
	if (nsin == 0)
	{
		szMsg.Format( IDS_UINP_0, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/****************************/
	/*** skip trailing penup  ***/
	/****************************/
	/* Only for last block because if more blocks are to follow there may be pen lifts embedded in a long trial */
	/* POTENTIAL PROBLEM: If the last blcok contains ONLY penlifts then you could have removed the penlifts of the previous block */
	/* However, this is not a big problem because trailing pen lifts are limited by the pen lift time out */

	// If you are finished reading all blocks of data
	if (!fnextblock)
	{
		/* Search backwards from end of trial (of the last block) */
		// ns is going to be the actual number of samples
		//for (ns = nsin; ns > 1; ns--) 27Jan10: Somesh
		for (ns = nsin; ns >= 1; ns--)
		/* Find first pen press point when moving backwards */
			if (z [ns-1] >= prsmin) break;
		// Message if trailing penlift removed
		if (ns != nsin)
		{
			//# of samples trailing penlift samples removed and total number of remaining samples
			// Removed: nsin - ns
			// Remaining in total: (blocknr - 1) * NSMAX + ns
			/*szMsg.Format( IDS_UPL3,MODULE, ns);
			OutputMessage( szMsg, LOG_PROC );*/
		}
		// Message us no samples left in this block
		// 20Nov06 YI: TODO
		if (ns == 0)
		{
			szMsg.Format( IDS_UPL_0, MODULE );
			OutputMessage( szMsg, LOG_PROC );
			fRet = FALSE;
			goto Cleanup;
		}
	}
	// ns is the number of samples
	else ns = nsin;

	//SAMPLES
	//--------------------------------
	szMsg.Format (IDS_UDD5, MODULE);
	OutputMessage (szMsg, LOG_PROC);
	szMsg.Format( IDS_UPL, MODULE);
	OutputMessage( szMsg, LOG_PROC );
	szMsg.Format( IDS_UPL3, MODULE, nsin);
	OutputMessage( szMsg, LOG_PROC );
	//-------------------------------
	szMsg.Format (IDS_UDD5, MODULE);
	OutputMessage (szMsg, LOG_PROC);
	// OutputMessage: Samples remaining after removing trailing penlift
	szMsg.Format( IDS_UPL3a, MODULE, ns);
	OutputMessage( szMsg, LOG_PROC );

	/************************/
	/* Some simple features */
	/************************/
	strokelength = sqrt ( SQR(x [ns-1] - x [0]) + SQR (y [ns-1] - y [0]));
	/*szMsg.Format( IDS_UF_0, MODULE, ns, ns * sec);
	OutputMessage( szMsg, LOG_PROC );*/
	////ADD Nov.16 Yi : change results window output
	////First sample coordinate
	//szMsg.Format( IDS_UF_1a,MODULE, x [0], y [0], z [0]);
	//OutputMessage( szMsg, LOG_PROC );
	////Last sample coordinate
	//szMsg.Format(IDS_UF_1a2,MODULE, x [ns-1], y [ns-1], z [ns-1]);
	//OutputMessage( szMsg, LOG_PROC );
	//xlength, ylength, absolute stroke length
	szMsg.Format( IDS_UF_1b,MODULE, x [ns-1] - x [0], (x [ns-1] - x [0]) * cm, y [ns-1] - y [0],(y [ns-1] - y [0]) * cm, strokelength, strokelength * cm);
	OutputMessage( szMsg, LOG_PROC );
	if (ns-1 > 0)
	{
		szMsg.Format( IDS_UF_VEL,MODULE, strokelength / (ns-1), strokelength * cm / ((ns-1) * sec));
		OutputMessage( szMsg, LOG_PROC );
	}

	/**************************/
	/* Find isBegin and isEnd */
	/**************************/
	/*13Aug04: Raji: Find isBegin and isEnd here: Consider only samples that have a higher velocity */
	/* This is needed for the linearity test */
	/* Hence the new ns becomes isEnd-isBegin+1; ns - 1 becomes isEnd-isBegin; 0 becomes isBegin for all calculations */
	/* isBegin is the first sample with a larger distance with the following example */
	/* Example */
	/* Sample  0  1  2  3  4  5  6  7  8 */
	/* Value  12 14 24 44 54 64 74 76 80 */
	/* Diff    2 10 10 20 10 10  2  4  * */
	/* isBegin/2     isBegin            isEnd      */
	/* Lag1    5  1  2  2  1  5  2  *  * */

	// ADDED Nov.30.2006 by Yi
	// Find the average velocity
	for (int i = 0; i < ns - 1; i++)
	{
		dVelocityAverage += sqrt ( (y[i+1]-y[i])*(y[i+1]-y[i]) + (x[i+1]-x[i])*(x[i+1]-x[i]) );
	}
	if (ns!=0)
		dVelocityAverage /= ns;

	dVelocityMin = 1.33 * dVelocityAverage;
	memset (isBegin, 0	 , NSEGMAX*sizeof(int));
#pragma warning( disable: 4996 )
	std::fill_n( isEnd, NSEGMAX, ns-1 );
#pragma warning( default: 4996 )

	// 25Jan10: Somesh: This was one loop searching from beginning and from the end. Thus low velocity points were included in isBegin and isEnd.
	// Now we search forward in the same way and continue searching forward for the first point with iCounterThld points < dVelocityMin.
	// This is repeated for the entire recording, yielding 1 or more segments, each with its begin and end.
	while (nsout < ns-1 && nseg < NSEGMAX)
	{
		// Find the beginning of good samples
		while (nsout < ns-1)
		{
			dVelocity = sqrt((y[nsout+1]-y[nsout])*(y[nsout+1]-y[nsout])+(x[nsout+1]-x[nsout])*(x[nsout+1]-x[nsout]));

			if (dVelocity > dVelocityMin)
			{
				iCounterVelocity++;
				if (iCounterVelocity >= iCounterThld)
				{
					isBegin[nseg] = nsout - iCounterThld;
					//in case of the beginning of the sample sequence, isBegin - iCounterThld could be negative
					if (isBegin[nseg] < 0)
						isBegin[nseg] = 0;
					break;
				}
			}
			else
			{
				iCounterVelocity=0;
			}

			// Goto next sample
			nsout++;
		}

		iCounterVelocity = 0;
		// Find the ending of good samples
		while (nsout < ns-1)
		{
			dVelocity = sqrt((y[nsout+1]-y[nsout])*(y[nsout+1]-y[nsout])+(x[nsout+1]-x[nsout])*(x[nsout+1]-x[nsout]));

			if (dVelocity < dVelocityMin)
			{
				iCounterVelocity++;
				if (iCounterVelocity >= iCounterThld)
				{
					isEnd[nseg] = nsout - iCounterThld;
					//in case of the end of the sample sequence, isEnd + iCounterThld could be larger than number of samples
					if (isEnd[nseg] > ns -1)
						isEnd[nseg] = ns - 1;

					// Increment segment count for every pair of (isBegin, isEnd)
					nseg++;
					break;
				}
			}
			else
			{
				iCounterVelocity=0;
			}

			// Goto next sample
			nsout++;
		}
	}

	// If no segments found, make an artificial segment from begin to end
	// By default: isBegin[0] = 0, isEnd[0] = ns-1
	if (nseg == 0)
		nseg = 1;

	// OutputMessage: Criteria for choosing isBegin and isEnd
	szMsg.Format( IDS_UROT_ISBEGIN, MODULE, iCounterThld);
	OutputMessage( szMsg, LOG_PROC );
	/*szMsg.Format( IDS_UROT_ISEND, MODULE, iCounterThld);
	OutputMessage( szMsg, LOG_PROC );*/

	// OutputMessage: Begin and End samples of each segment
	for ( iseg = 0; iseg<nseg; iseg++ )
	{
		szMsg.Format( IDS_UROT_ISBEGEND, MODULE, iseg+1, isBegin[iseg], isEnd[iseg]);
		OutputMessage( szMsg, LOG_PROC );
	}
	//szMsg.Format( IDS_UROT_IS1,MODULE, isBegin[0]);
	//OutputMessage( szMsg, LOG_PROC );
	//szMsg.Format( IDS_UROT_IS2,MODULE, isEnd[nseg-1]);
	//OutputMessage( szMsg, LOG_PROC );

	/***************************/
	/*** Find rotation angle ***/
	/***************************/
	// Rotate the regression line to vertical.
	// Therefore y = tangential movemetn component and x is the orthogonal movement component which noise in the linearity test
	/* Linearity test only for first block because we do not record very long straight lines */
	if (blocknr == 1)
	{
		beta = dRotationBeta;
		if (dRotationBeta > 0.0)
		{
			szMsg.Format( IDS_UROT, MODULE, beta );
			OutputMessage( szMsg, LOG_PROC );
		}
		else
		{
			/*****************************/
			/* Automatic angle detection */
			/*****************************/
			/*** Calculate velocity in a simple and quick way but less accurate ***/
			if (fVelocity)
			{
				OutputMessage( IDS_UVEL, LOG_PROC );
				/* Not copied to timfun */
				/* Differentiate in space domain quickly to get predominant direction */
				/* This is mainly needed to detect predominant up and down strokes */
				// Velocity points = 1 less than coordinate points. Thus (isEnd[nseg-1]-isBegin[0]) and not (isEnd[nseg-1]-isBegin[0]+1)
				for (is = isBegin[0]; is < isEnd[nseg-1] - isBegin[0]; is++)
				{
					vx [is] = x [is + 1] - x [is];
					vy [is] = y [is + 1] - y [is];
				}
				/* regression and error (ns reduced by one due to differentiation) */
				w = 1.;
				// Velocity points = 1 less than coordinate points. Thus (isEnd[nseg-1]-isBegin[0]) and not (isEnd[nseg-1]-isBegin[0]+1)
				mdist = rregr (&vx[isBegin[0]], &vy[isBegin[0]], isEnd[nseg-1] - isBegin[0], w, &a, &b, &r);
				szMsg.Format( IDS_UVEL_1, MODULE, r, a, b);
				OutputMessage( szMsg, LOG_PROC );
				szMsg.Format( IDS_UVEL_2, MODULE, mdist, sec);
				OutputMessage( szMsg, LOG_PROC );
			}
			else
			{
				/* regression and orthogonal error (ns reduced by one due to differentiation) */
				w = 1.;
				/* RMS sample error from fitted line in points */
				/* Fit the line y = a + bx */
				/* r is the correction */
				mdist = rregr (&x [isBegin[0]], &y [isBegin[0]], isEnd[nseg-1] - isBegin[0] + 1, w, &a, &b, &r);
				szMsg.Format( IDS_UNVEL_1,MODULE, r, a, b);
				OutputMessage( szMsg, LOG_PROC );

				/* Length of the stroke */
                ds = sqrt (SQR (x [isEnd[nseg-1]-isBegin[0]] - x [isBegin[0]]) + SQR (y [isEnd[nseg-1]-isBegin[0]] - y [isBegin[0]]));
				if (ds > 0.)
				{
					szMsg.Format( IDS_UNVEL_4, MODULE, ds, ds * cm, mdist / ds);
					OutputMessage( szMsg, LOG_PROC );
				}

				//ADDED Nov.17 Yi: Change the results window output
				//|-------------------
				szMsg.Format( IDS_UDD5, MODULE);
				OutputMessage( szMsg, LOG_PROC );
				/* RMS error in cm*/
				szMsg.Format( IDS_UNVEL_2, MODULE);
				OutputMessage( szMsg, LOG_PROC );
				szMsg.Format( IDS_UNVEL_2a,MODULE, mdist, mdist * cm);
				OutputMessage( szMsg, LOG_PROC );
				//|-------------------
				szMsg.Format( IDS_UDD5, MODULE);
				OutputMessage( szMsg, LOG_PROC );

				/* RMS quantization noise in points, cm and RMS noise relative to quantization error*/
				/* White noise between -.5 and +.5 yields var=Int(-.5,+.5) x**2 dx = (-.5,+.5)x**3/3 = 1/12. */
				szMsg.Format( IDS_UNVEL_3, MODULE, sqrt (1./12.), sqrt (1./12.) * cm);
				OutputMessage( szMsg, LOG_PROC );
				/*szMsg.Format( IDS_UNVEL_3a,MODULE, mdist / sqrt (1./12.));
				OutputMessage( szMsg, LOG_PROC );*/
				/* RMS noise without quantization noise*/
				szMsg.Format( IDS_UNVEL_5,MODULE,sqrt ((MAX(0,(mdist)*(mdist)) - 1./12.)), sqrt ((MAX(0,(mdist)*(mdist)) - 1./12.))*cm);
				OutputMessage( szMsg, LOG_PROC );
				/*szMsg.Format( IDS_UNVEL_6,MODULE);
				OutputMessage( szMsg, LOG_PROC );*/
			}

			/* estimate angle; high values of b refer to almost vertical */
			if (-BMAX < b && b < BMAX)
			{
				beta = PI / 2. - atan (b);
				if (beta > (5./8.) * PI)
				beta -= PI;

				// Convert to degrees
				szMsg.Format( IDS_U_ROT, MODULE, beta, 180*beta/3.141592 );
				OutputMessage( szMsg, LOG_PROC );
			}
			else
			{
				szMsg.Format( IDS_U_NROT, MODULE, b, BMAX);
				OutputMessage( szMsg, LOG_PROC );
				beta = 0.;
			}

		}
	}

	/****************/
	/*** rotation ***/
	/****************/
	// Make x the orthogonal (noise in the linearity test) component and y the tangential component
	if (beta != 0.)
	{
		sinb = sin (beta);
		cosb = cos (beta);
		/* Do not keep DC based on each segment */
		/* Rather keep position first point untouched */
		keep_dc = FALSE;
		ratfor (&x[isBegin[0]], &y[isBegin[0]], isEnd[nseg-1]-isBegin[0]+1, cosb, -sinb, sinb, cosb, keep_dc);
	}

	// For the linearity test you calculate again the sample differences of y (or y velocity) and store in z
	// because we are not using z (=pressure) for the linearity test
	if (fDifferentiate)
	{
		/* The y contains the tangential movement. It is more instructive to */
		/* view the differences per sample in the y direction */
		/*szMsg.Format( IDS_UDIFF_1, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_UDIFF_2,MODULE,  LOG_PROC );
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_UDIFF_2a,MODULE,  LOG_PROC );
		OutputMessage( szMsg, LOG_PROC );*/

		double sum = 0., avg = 0.;
		int icount = 0;
		for (is = isBegin[0]; is < isEnd[nseg-1]; is++)
		{
			z [is] = y [is+1] - y [is];
		}
		//// Estimate average velocity (just for output?)
		//// (potential error) Only work for the straight line
		//if (isEnd[nseg-1]*isBegin[0])
		//	avg = fabs ((y[isEnd[nseg-1]] - y[isBegin[0]]) / (isEnd[nseg-1]-isBegin[0]));
		//else
		//	avg = 0.;

		//szMsg.Format( IDS_UF_VEL1, MODULE, avg, (ds*cm)/((isEnd[nseg-1]-isBegin[0]+1)*sec));
		//OutputMessage( szMsg, LOG_PROC );

		// Clear y array
		for (is = 0; is < ns - 1; is++)
			y [is] = 0.;

		// Calculation of missing/extra samples
		movementrange = 0;
		for (iseg = 0; iseg < nseg; iseg++)
		{
			ravg = z [isBegin [iseg]];
			for (is = isBegin [iseg]; is < isEnd [iseg] ; is++)
			{
				// Ravg is the running average of z which are the differences
				// 21Jan2010: HL: Only update running average if no missing or extra samples to be detected, otherwise, leave running average as is.
				MissingSampleCounter = fabs (z [is]/ravg);
				// Counter value when no missing or double data
				if (MissingSampleCounter > 0.75 && MissingSampleCounter < 1.5)
					ravg = (mfactor * z[is]) + ((1. - mfactor) * ravg);
					// 1st-order approximation
					//ravg1 = (mfactor * z[is+1]) + ((1. - mfactor) * ravg);
				// The quotient of the difference and the running average difference tells you whether samples are missing or double.
				// For example if the running average is 20 and we find a sample difference of 40, then apparently
				// Store the quotients into y
				if (ravg == 0)
					y [is] = 0.;
				else
				{
					// Missing samples. 0th-order approximation: Do not change
					y [is] = MissingSampleCounter;
					// 1st-order approximation: Change lineraly by interpolating with next one
					//y [is] = fabs (z [is]/ravg1)
				}
				if (y [is] > 4.)
					y [is] = 4.;

				// For debugging of running average. Output it to x. Disable filling x with departure from straight line
				//x[is] = ravg;

				// Estimate missing or multiple samples //

				// Identical samples = double sample
				if (y [is] == 0.)
					bb++;
				// Suddenly a distance implies a quadruple sample: x  x  xxxx  x  x
				else if (y [is] <= 0.43)
					b0++;
				// Suddenly a third distance implies a triple sample: x  x  xxx  x  x
				else if (y [is] <= 0.58)
					b1++;
				// Suddenly a half distance implies a double sample: x x xx x x
				else if (y [is] <= 0.75)
					b2++;
				// Ok sample difference: x x x x x x
				else if (y [is] <= 1.5)
					b3++;
				// Suddenly a double distance implies one missing sample: x x x _ x x x
				else if (y [is] <= 2.5)
					b4++;
				// Suddenly a triple distance implies two missing samples: x x x _ _ x x x
				else if (y [is] <= 3.5)
					b5++;
				else if (y [is] > 3.5)
					b6++;
			}
			movementrange += isEnd[iseg]-isBegin[iseg]+1;
		}

		// Temporal error //
		szMsg.Format( IDS_UDD5, MODULE);
		OutputMessage( szMsg, LOG_PROC );
		// Temporal error header
		szMsg.Format( IDS_UROT_M1, MODULE);
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_UROT_M1a, MODULE, 100.0*(double)( b4+b2+bb + 2*(b5+b1) + 3*(b6+b0) )/movementrange );
		OutputMessage( szMsg, LOG_PROC );
		//|-------------------
		szMsg.Format( IDS_UDD5, MODULE);
		OutputMessage( szMsg, LOG_PROC );

		//# Samples=Movement Range Samples above average velocity to be used in the linearity test
		szMsg.Format( IDS_UROT_M3_1, MODULE, movementrange);
		OutputMessage( szMsg, LOG_PROC );
		//1 missing sample
		szMsg.Format( IDS_UROT_M2a, MODULE, b4);
		OutputMessage( szMsg, LOG_PROC );
		//2 missing samples
		szMsg.Format( IDS_UROT_M2a_1, MODULE, b5);
		OutputMessage( szMsg, LOG_PROC );
		//3 or more missing samples
		szMsg.Format( IDS_UROT_M2a_2, MODULE, b6);
		OutputMessage( szMsg, LOG_PROC );
		//Total number/percentage of missing samples
		percentagemissing = 0;
		if ( movementrange != 0 )
			percentagemissing = (double) 100.0 * ( b4 + 2 * b5 + 3 * b6 ) /(double) movementrange;
		szMsg.Format( IDS_UROT_M2a_3, MODULE, ( b4 + 2 * b5 + 3 * b6 ), percentagemissing);
		OutputMessage( szMsg, LOG_PROC );

		//1 extra sample
		szMsg.Format( IDS_UROT_M2b, MODULE, b2 + bb);
		OutputMessage( szMsg, LOG_PROC );
		//2 extra samples
		szMsg.Format( IDS_UROT_M3, MODULE, b1);
		OutputMessage( szMsg, LOG_PROC );
		//3 or more extra samples
		szMsg.Format( IDS_UROT_M3_2, MODULE, b0);
		OutputMessage( szMsg, LOG_PROC );
		if ( movementrange != 0 )
			percentageextra = (double) 100.0 * ( b2 + bb + 2 * b1 + 3 * b0 ) / (double) movementrange;
		szMsg.Format( IDS_UROT_M3_3, MODULE, ( b2 + bb + 2 * b1 + 3 * b0 ), percentageextra);
		OutputMessage( szMsg, LOG_PROC );

		// OutputMessage: Refer to DIS file for further missing sample information
		szMsg.Format( IDS_UROT_REFERDIS, MODULE );
		OutputMessage( szMsg, LOG_PROC );

		/*
		szMsg.Format( IDS_U_CLIP, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_U_SHIFT, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		*/

		/* Missing value is 0 may cause artifact */
		//xmean = means (x, isEnd - isBegin - 1, 0.);
		xmean = means (&x [isBegin[0]], isEnd[nseg-1] - isBegin[0] + 1, 0.);
		/* deduct across the whole array */
        for (is = isBegin[0]; is < isEnd[nseg-1]; is++)
		{
			// Disable this when filling this array with ravg
			x [is] -= xmean;
		}

		/* Fill the x, y and z values that have not been calculated with zeros (implies indeterminate)*/

		// Fill zeroes before first isBegin
		for (is = 0; is < isBegin[0]; is++)
		{
			x [is] = 0.;
			y [is] = 0.;
			z [is] = 0.;
		}

		// Fill zeroes between isEnd and next isBegin
		for (iseg = 0; iseg < nseg - 1; iseg++)
		{
			for (is = isEnd[iseg]; is < isBegin[iseg+1]; is++)
			{
				x [is] = 0.;
				y [is] = 0.;
				z [is] = 0.;
			}
		}

		// Fill zeroes after last isEnd
		for (is = isEnd[nseg-1]; is < ns; is++)
		{
			x [is] = 0.;
			y [is] = 0.;
			z [is] = 0.;
		}

		/*************/
		/*** write ***/
		/*************/
		szMsg.Format( IDS_UINP_OUT, MODULE, szHWROut, ns);
		OutputMessage( szMsg, LOG_PROC );

		/* Not required as the numbers are written out already*/
		//szMsg.Format( IDS_U_OUT, MODULE, ns, isBegin, isEnd);
		//OutputMessage( szMsg, LOG_PROC );
		for (is = 0; is < ns; is++)
		{
			/* x array is Distance of X value from the best fitted line */
			/* z array is Distance between Consecutive Y samples (Velocity)*/
			/* y array is Number of missing samples (Write this in the last column)*/
			szMsg.Format( _T("%.3g %.3g %.3g\n"), x[ is ], z[ is ] , y[ is ] );
			fOut.WriteString( szMsg );
		}
	}
	else
	{
		for (is = 0; is < ns; is++)
		{
			szMsg.Format( _T("%g %g %g\n"), x[ is ], z[ is ] , y[ is ] );
			fOut.WriteString( szMsg );
		}
	}

	// ADDED Yi Nov.17.06
	//+-------------------
	szMsg.Format( IDS_UDD5, MODULE);
	OutputMessage( szMsg, LOG_PROC );


  /* Repeat blocks of NSMAX samples if needed */
	if (fnextblock)
		goto nextblock;


Cleanup:
	if( fOut.m_pStream ) fOut.Close();
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;
	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( isBegin ) delete [] isBegin;
	if( isEnd ) delete [] isEnd;

	return fRet;
}

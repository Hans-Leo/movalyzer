/*************************************************
   SegmenG - version 2.4 - hlt - 14 Feb 1995
**************************************************
   Copyright 1998 Teulings

ARGUMENTS: tf_infile seg_outfile [Debug/addFirst/addLast/...]

WARNING: If debug: no tests for max # segments

PURPOSES:
3    (1) Segments tf_file into separate up and down strokes
    (2) Adds significant first/last stroke if addFirst/addLast

COMPILATION:
    (1) Borland BC under DOS

UPDATES:
    27Apr94: Tvyzero in seconds instead of sample nrs.
    22Jul94: addFirst, addLast introduced. By default off.
    10Aug94: Infinite loop when interval[0]=stop corrected.
    14Sep94: Updating debugging output in remzerocross/2
    15Sep94: Update debuggin of STOP. Changed RDISTMIN from 0.2 to 0.1
    14Feb94: Bug solved: In very short artifacts first and last segmentation
               could be same. Solved by requiring TIMEMIN
    20Mar95: Simplified begin and end detection; messages improved.
               Small bug in remzerocross2 removed (all but last were shifted if first was stop).
     5Jun95: Screen info of switch settings; little debug mode format problem solved.
             Bug in arguments solved: arguments undetermined because:
             if(arc>=3) if(strstr(argv[3],"d")!=NULL) debug=TRUE; else debug=FALSE;
             Needed changed into:
             debug=FALSE; if(argc>3) if(strstr(argv[3],"d")!=NULL) debug=TRUE;
    18Mar96: RVYMIN2 increased from .1 to .8 for exp WR
     4Sep96: Better diagnostics in small stroke sizes
     6Jan97: Segment primary phase and secondary phases
    16Dec97: vmax based on MAX(-vmin,vmax) - needed in single down stroke
             No ntvyzero reduction if only 2 points left
    18Mar98: Do not segment into strokes with /1
    16Sep98: Included beta from timfun
    19Nov97: Zero data are now ok
    19Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		     removal of statics
	29May00: hlt: Added Tvypeak after which to search for the start of secondary submovement according to Meyer et al.
	05Dec01: Bug fix: Tvypeak corresponds nearly with one Tayzero point Added code to snap in
				so that the subsegmentation point is always one zerocrossing after.
	14jun01 hlt disable debug output to seg file
			hlt option(Verbosity for now) to include ALL tayzero points
	10Jun03:Raji: While forwarding/backwarding for tvyzero [ntvyzero] and tvyzero [ntvyzero + 2] segmentation points
	              use two different vylev values
	18Sep06: GMB: Converted deprecated string/file functions for MSVS7

************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"
#include "../Common/siglib.h"

// 26mar08 hlt
#include <math.h>

// 02Sep10: Somesh: Was 1700
#define NSMAXG 3300    /*(4096/1.25)*/ //2048 not enough mem //1200 /*2000 causes problems when running in debug mode */
//#define NSEGMAXG NSMAXG/20      /* good for 200 Hz * 0.1 s strokes */
#define NSEGMAXG 200            /* must be high because of undescarded zero crossings */
#define NSEGMAXOUT 60         /* because of remstop */
#define NFREQMAXG 2048/2+1

/* relative vy level w.r.t. peak */
//#define RVYMIN 0.1 /* 0.1 too small; 0.2 is too big */

/* the smallest senseful stroke size in cm */
#define ABSDISTMIN 0.05 /* 0.05 from remstop */
/* the smallest sensful stroke duration in sec */
#define TIMEMIN 0.04 /* 0.05 from remstop */


/* Minimum stroke size (cm) */
#define ABSDISTMIN 0.05 /* 0.05 from remstop */

/* Minimum stroke size (relative to the max stroke size per trial) */
/* distav 0.2; distmax 0.1 sometimes too short 0.05 0.03 seems ok? */
/* 0.2 too short for hpmibr00 */
#define RDISTMIN 0.1

/* Minimum stroke duration (s) */
#define TIMEMIN 0.04 /* 0.05 from remstop */


/* 26Mar08 hlt */
/* Minimum vertical velocity of the coarse beginning/end of the first/last stroke (relative to the vertical peak velocity per trial) */
#define RVYMIN 0.3 /* 0.1 too small; 0.2 is too big */

double tvyzerocoarse = 0.0, tvabsmincoarse = 0.0, tvyzerolastcoarse = 0.0, tvabslastcoarse = 0.0;

	/*Definitions for new routine*/
	bool segvabs = 0;
	bool segacc = 0;


#define VLEVMIN 0.1
#define ROUNDUP 0.999
#define NODECIMATE 1



// 26mar08 hlt
/* specific routines - supporting */
int indup (double* vy, int nsvy, double tstart, double vylev, bool segvabs);
int indupe (double* vy, int nsvy, double tstart, double vylev, bool segvabs);
double rindmin0 (double* vy, int nsvy, double tstart, bool lastzero);
double rindmin0e (double* vy, int nsvy, double tstart);
double rind0 (double* vy, int nsvy, double tstart);
double rind0e (double* vy, int nsvy, double tstart);
double rindlev (double* vy, int nsvy, double tstart, double tvlevmin);
double rindelev (double* vy, int nsvy, double tstart, double tvlevmin);
int nzerocross (double* vy, int nsvy, double* tvyzero, int ntvyzeromax);
double distmax (double* x, double* y, int ns, double* tvyzero, int ntvyzero, int itimemin);
int remzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
				  double distmin, int debug);
int remzerocross2 (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
				   double distmin, int itimemin, int debug);
int addlastzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
					  double distmin, int itimemin);
int addfirstzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
					   double distmin, int itimemin);
int remayzerocross1 (double* tayzero, int ntayzero, double* tvyzero, double* tvypeak, int ntvyzero,
					double* tayzeroout, int ntayzeroout);
void segmov2z(double *tvyzero, double *z, int ntvyzero, double prsmin);
void segmov2min(double *tvyzero, double *vabs, double *x, double *y, int ntvyzero, double distmin, int itimemin);
/*functions for new routine*/
int inddip(double *x, int n, int nstops, int idt, double *tvabsbot, int nsegmax);
double rnpolq(double *y, int ns, double rind, double *rinde, double *yrinde);
int remmin (double* x, double* y, double* vabs, double* tvabsmin, int ntvabsmin, double vlev, double* vabspeaks, double distmin);


/* specific routines */
int induponly (double* vy, int nsvy, double tstart, double vylev);
int induponlye (double* vy, int nsvy, double tstart, double vylev);
double rindminonly (double* vy, int nsvy, double tstart);
double rindminonlye (double* vy, int nsvy, double tstart);
int indpeak (double* z, int ns, int isam1, int isam2);

#define	MODULE		_T("GRIPPERSEGMENT")

/************************************************************************/
bool SegmentG( CString szTFIn, CString szSegOut, unsigned int nSwitch, unsigned int nTask )
/************************************************************************/
{
	bool fRet = TRUE;

	double vlevmin = VLEVMIN;
	double distmax1 = 0;
	double absdistmin = ABSDISTMIN;
	double rdistmin = RDISTMIN;
	double timemin = TIMEMIN;
	double rvymin = RVYMIN;
	// segmentation points is NSEGMAXG+1 and addlastzerocross may want to add one point
	double tvyzerolast = 0.0;
	double tvyzerofirst = 0.0;
	int ntayzero = 0;
	int itvyzero = 0, itayzero = 0;
	int nsvabs = 0;
	int nsax = 0, nsay = 0;
	static double vylev2 = 0.0;
	double vabsmin = 0.0, vabsmax = 0.0;
	double distmin = 0.0;
	int itimemin = 0;
	bool debug = FALSE, addlast = FALSE, addfirst = FALSE, secondary = FALSE, onestroke = FALSE;
	int isam = 0;
	double vypeakneg = 0.0, vypeakpos = 0.0;
	double tvypeakneg = 0.0, tvypeakpos = 0.0;
	double* x = new double[ NSMAXG ];
	double* y = new double[ NSMAXG ];
	double* z = new double[ NSMAXG ];
	double* vx = new double[ NSMAXG ];
	double* vy = new double[ NSMAXG ];
	double* vz = new double[ NSMAXG ];
	double* tvyzero = new double[ NSEGMAXG + 2 ];
	double* tvypeak = new double[ NSEGMAXG + 1 ];
	double* tayzero = new double[ NSEGMAXG + 1 ];
	double* tayzeroout = new double[ NSEGMAXG + 1 ];
	bool segvabs = 0;
	bool segacc = 0;
	int ntvabsmin = 0;
	double vabslev = 0.0, vabslev2 = 0.0, rvabsmin = 0.0, rvabsmin2 = 0.0, vabspeakpos = 0.0, tvabspeakpos = 0.0;
	int itvabsmin = 0, ntaymin = 0, itaymin = 0, index = 0;
	int ntvabssec = 0;
	double tvabslast = 0.0;
    int isampl_begin = 0, isampl_end = 0;
    double cblength = 0.0;
	double tvyzerocoarse = 0.0, tvabsmincoarse = 0.0, tvyzerolastcoarse = 0.0, tvabslastcoarse = 0.0;
	double tvlevmin = 0.0;
	bool segmovtoz = FALSE;
	bool vlevtest = FALSE;
	bool vylastzero = FALSE;
	int itz = 0;
	bool segz = FALSE;
	int ntz = 0;
	bool segmovtomin = FALSE;
	int ntvyzero = 0;
	int nsx = 0, nsy = 0, nsz = 0, nsec = 0;
	int nsvx = 0, nsvy = 0, nsvz = 0;
	int nprsmin = 0, nbeta = 0;		// Not used in calculations
	double prsmin = 0.0, beta = 0.0;// ""
	CString szMsg, szTxt;
	FILE *fTF = NULL, *fSeg = NULL;
	static double vylev = 0.0;
	double vymin = 0.0, vymax = 0.0, sec = 0.0;
	int isam1 = 0, isam2 = 0;
	int intro = 1, trail = 1;
	// 30Oct08: Somesh: Variables to hold max value of force channel (x/y/z)
	double maxx = 0.0, maxy = 0.0, maxz = 0.0;


	/************/
	/* Switches */
	/************/
	if( ( nSwitch & SEG_SWITCH_I ) != 0 )
	{
		intro = FALSE;
		szMsg.Format( _T("%s: INFO: No intro segment."), MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_T ) != 0 )
	{
		trail = FALSE;
		szMsg.Format( _T("%s: INFO: No trail segment."), MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}

	/**************/
	/* Input file */
	/**************/
	if( ( fopen_s( &fTF, szTFIn, "r") ) != 0 )
	{
		szMsg.Format( IDS_SEGINP_ERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/***  read  ***/
	//x, y, z, V-spectrumXY, V_spectrumZ, V_spectrumFiltered, vx, vy, vz
	nsec = getdata (fTF, &sec, 1, szTxt, NODECIMATE);
	nprsmin = getdata (fTF, &prsmin, 1, szTxt, NODECIMATE);
	nbeta = getdata (fTF, &beta, 1, szTxt, NODECIMATE);
	nsx = getdata (fTF, x, NSMAXG, szTxt, NODECIMATE);
	nsy = getdata (fTF, y, NSMAXG, szTxt, NODECIMATE);
	nsz = getdata (fTF, z, NSMAXG, szTxt, NODECIMATE);

	/* 26Mar08 hlt */
	// Skip 3 spectra plus their filtered version
	// x, y, z spectrum
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	//vx, vy, vz
	nsvx = getdata (fTF, vx, NSMAXG, szTxt, NODECIMATE);
	nsvy = getdata (fTF, vy, NSMAXG, szTxt, NODECIMATE);
	nsvz = getdata (fTF, vz, NSMAXG, szTxt, NODECIMATE);

	// close input file
	fclose( fTF );
	fTF = NULL;

	/* Test (also done in Extract)*/
	/* 28Dec04: Raji: If the data file is empty, gives a crash in Extract*/
	/* Now instead, the seg file is not created, and error written to results file*/
	if( (nsx == 0) || (nsy == 0) || (nsz == 0))
	{
		szMsg.Format( IDS_SEG_TFERR1, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/* tests */
	if (nsx != nsy || nsx != nsz || nsec != 1 )
	{
		szMsg.Format( IDS_SEGLEN_ERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/*********************/
	/*** Open Output    ***/
	/*********************/
	/* Put here to allow writing debug info */
	if( (  fopen_s( &fSeg, szSegOut, "w" ) ) != 0 )
	{
		szMsg.Format( IDS_SEGOUT_ERR, MODULE, szSegOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/**************************************/
	/* Segmentation procedures for resp., */
	/* Standard Gripper task              */
	/* and repetitive pressure task       */
	/*  and pressure measuring tasks      */
	/**************************************/
	if( nTask == GRIPPER_TASK_DEFAULT ) nTask = GRIPPER_TASK_STANDARD;
	/*******************************************/
	/* Standard gripper segmentation procedure */
	/*******************************************/
	if( nTask == GRIPPER_TASK_STANDARD )
	{
		/********************************/
		/* (0) first segmentation point */
		/********************************/
		ntvyzero = 0;
		if (intro)
		{
			tvyzero [ntvyzero] = 0.;
			ntvyzero++;
		}

		//02Dec08: Somesh: New algorithm

		// Index of peak
		tvyzero [ntvyzero + 1] = indpeak (z, nsz, 0, nsz-1);
		ntvyzero++;
		// Valleys on the two sides of peak
		tvyzero [ntvyzero - 1] = rindminonlye (z, nsz, tvyzero [ntvyzero] );
		ntvyzero++;
		tvyzero [ntvyzero	 ] = rindminonly  (z, nsz, tvyzero [ntvyzero - 1] + 1);
		ntvyzero++;

		// Check if the segment points 2 and 4 are local valleys. If yes, search lower than vlev
		rmima (z, nsz, &vymin, &vymax);

		// 24Aug10: Somesh: A 'very' negative vymin was pulling vylev down. Set vymin to 0.
		vymin = 0;
		vylev = 0.6 * (vymax - vymin) + vymin;
		if (z [ (int) tvyzero [1] ] > vylev)
		{
			tvyzero [1] = induponly (z, nsz, 0., vylev);
			tvyzero [1] = rindminonlye (z, nsz, tvyzero [1]);
		}

		if (z [ (int) tvyzero [3] ] > vylev)
		{
			tvyzero [3] = induponlye (z, nsz, nsz-1, vylev);
			tvyzero [3] = rindminonly (z, nsz, tvyzero [3]);
		}


		/*******************************/
		/* (4) last segmentation point */
		/*******************************/
		if (trail)
		{
			tvyzero [ntvyzero] = nsz - 1;
			ntvyzero++;
		}
	}
	/***********************************************************************/
	/* Segmentation of the repetitive pressure tasks, borrowed from Segmen */
	/***********************************************************************/
	else if( nTask == GRIPPER_TASK_REPETITIVE )
	{

		// 20Oct08:Somesh: Segmentation based on signal with maximum amplitude. tvyzero used as dummy.
		minmax ( x, nsx, tvyzero, &maxx);
		minmax ( y, nsy, tvyzero, &maxy);
		minmax ( z, nsz, tvyzero, &maxz);

		//Choose segmentation to be carried out on array with higher magnitude
		if ( maxx >= maxy && maxx >= maxz )
		{

			/* Dynamic range and z level */
			rmima (x, nsx, &vymin, &vymax);
			vylev = 0.2 * (vymax - vymin) + vymin;

			/* All signal crossings going above or below vylev*/
			// Always start with upgoing stroke.
			tvyzero [0] = induponly (x, nsx, 0., vylev);
			ntvyzero = 1;
			for (isam1 = (int) tvyzero [0]; isam1 < nsx - 1; isam1++)
			{
				if (x [isam1+1] > vylev && x [isam1] < vylev )
				{
					tvyzero [ntvyzero] = isam1-1;
					ntvyzero++;
				}
				if (x [isam1+1] < vylev && x [isam1] > vylev )
				{
					tvyzero [ntvyzero] = isam1+1;
					ntvyzero++;
				}
			}

			// 28Oct08: Somesh: Assume that all even indices are upgoing and odd indices are downgoing crossings
			// of tvyzero.
			/* backwards till zero crossing or minimum */
			for (isam1=0; isam1 < ntvyzero; isam1++)
			{
				if(isam1%2 == 0)
					tvyzero [isam1] = rindminonlye (x, nsx, tvyzero [isam1]);
				else
					tvyzero [isam1] = rindminonly (x, nsx, tvyzero [isam1]);
			}
		}
		else if (maxy >= maxx && maxy >= maxz)
		{

			/* Dynamic range and z level */
			rmima (y, nsy, &vymin, &vymax);
			vylev = 0.2 * (vymax - vymin) + vymin;

			/* All signal crossings going above or below vylev*/
			// Always start with upgoing stroke.
			tvyzero [0] = induponly (y, nsy, 0., vylev);
			ntvyzero = 1;
			for (isam1 = (int) tvyzero [0]; isam1 < nsx - 1; isam1++)
			{
				if (y [isam1+1] > vylev && y [isam1] < vylev )
				{
					tvyzero [ntvyzero] = isam1-1;
					ntvyzero++;
				}
				if (y [isam1+1] < vylev && y [isam1] > vylev )
				{
					tvyzero [ntvyzero] = isam1+1;
					ntvyzero++;
				}
			}

			// 28Oct08: Somesh: Assume that all even indices are upgoing and odd indices are downgoing crossings
			// of tvyzero.
			/* backwards till zero crossing or minimum */
			for (isam1=0; isam1 < ntvyzero; isam1++)
			{
				if(isam1%2 == 0)
					tvyzero [isam1] = rindminonlye (y, nsy, tvyzero [isam1]);
				else
					tvyzero [isam1] = rindminonly (y, nsy, tvyzero [isam1]);
			}
		}
		else
		{

			/* Dynamic range and z level */
			rmima (z, nsz, &vymin, &vymax);
			vylev = 0.2 * (vymax - vymin) + vymin;

			/* All signal crossings going above or below vylev*/
			// Always start with upgoing stroke.
			tvyzero [0] = induponly (z, nsz, 0., vylev);
			ntvyzero = 1;
			for (isam1 = (int) tvyzero [0]; isam1 < nsx - 1; isam1++)
			{
				if (z [isam1+1] > vylev && z [isam1] < vylev )
				{
					tvyzero [ntvyzero] = isam1-1;
					ntvyzero++;
				}
				if (z [isam1+1] < vylev && z [isam1] > vylev )
				{
					tvyzero [ntvyzero] = isam1+1;
					ntvyzero++;
				}
			}

			// 28Oct08: Somesh: Assume that all even indices are upgoing and odd indices are downgoing crossings
			// of tvyzero.
			/* backwards till zero crossing or minimum */
			for (isam1=0; isam1 < ntvyzero; isam1++)
			{
				if(isam1%2 == 0)
					tvyzero [isam1] = rindminonlye (z, nsz, tvyzero [isam1]);
				else
					tvyzero [isam1] = rindminonly (z, nsz, tvyzero [isam1]);
			}
		}
	}
	else if( nTask == GRIPPER_TASK_MAXIMUM )
	{

		// 20Oct08:Somesh: Segmentation based on signal with maximum amplitude. isam1 used as dummy.
		minmax ( x, nsx, tvyzero, &maxx);
		minmax ( y, nsy, tvyzero, &maxy);
		minmax ( z, nsz, tvyzero, &maxz);

		//Decide segmentation to be carried out on which array (x or y)
		if ( maxx > maxy && maxx > maxz )
		{

			/* Dynamic range and z level */
			rmima (x, nsx, &vymin, &vymax);
			vylev = 0.2 * (vymax - vymin) + vymin;

			// First segment begins at first point of signal
			ntvyzero = 0;
			tvyzero [0] = 0;

			ntvyzero++;
#if 0
			tvabsmincoarse = indup (vabs, nsvabs, 0., vabslev, segvabs);
			/* Backwards till minimum */
			tvabsmin [0] = rindmin0e (vabs, nsvabs, tvabsmincoarse);
#endif

			/* All signal crossings going above vylev*/
			for (isam1 = (int) tvyzero [0]; isam1 < nsx - 1; isam1++)
			{
				if (x [isam1+1] > vylev && x [isam1] < vylev )
				{
					tvyzero [ntvyzero] = isam1-1;
				}
			}

			/* backwards till zero crossing or minimum */
			tvyzero [ntvyzero] = rindminonlye (x, nsx, tvyzero [ntvyzero]);
			ntvyzero++;

			// Point where upward race ends
			for (isam1 = (int) tvyzero [ntvyzero - 1] + 1; isam1 < nsx - 1; isam1++)
			{
				if ( x [isam1+1] < x [isam1])
				{
					tvyzero [ntvyzero] = isam1;
					ntvyzero++;
					break;
				}
			}

			// Last segment ends at last point of signal
			tvyzero [ntvyzero] = nsx - 1;
			ntvyzero++;
		}
#if 0
		else if ( maxy > maxx && maxy > maxz )
		{

			/* Dynamic range and z level */
			rmima (y, nsy, &vymin, &vymax);
			vylev = 0.2 * (vymax - vymin) + vymin;

			// First segment begins at first point of signal
			ntvyzero = 0;
			tvyzero [0] = 0;
			ntvyzero++;

			/* All signal crossings going above vylev*/
			for (isam1 = (int) tvyzero [0]; isam1 < nsy - 1; isam1++)
			{
				if (y [isam1+1] > vylev && y [isam1] < vylev )
				{
					tvyzero [ntvyzero] = isam1-1;
				}
			}

			/* backwards till zero crossing or minimum */
			tvyzero [ntvyzero] = rindminonlye (y, nsy, tvyzero [ntvyzero]);
			ntvyzero++;

			// Point where upward race ends
			for (isam1 = (int) tvyzero [ntvyzero - 1] + 1; isam1 < nsy - 1; isam1++)
			{
				if ( y [isam1+1] < y [isam1])
				{
					tvyzero [ntvyzero] = isam1;
					ntvyzero++;
					break;
				}
			}

			// Last segment ends at last point of signal
			tvyzero [ntvyzero] = nsy - 1;
			ntvyzero++;
		}
		else
		{

			/* Dynamic range and z level */
			rmima (z, nsz, &vymin, &vymax);
			vylev = 0.2 * (vymax - vymin) + vymin;

			// First segment begins at first point of signal
			ntvyzero = 0;
			tvyzero [0] = 0;
			ntvyzero++;

			/* All signal crossings going above vylev*/
			for (isam1 = (int) tvyzero [0]; isam1 < nsz - 1; isam1++)
			{
				if ( z [isam1+1] > vylev && z [isam1] < vylev )
				{
					tvyzero [ntvyzero] = isam1-1;
				}
			}

			/* backwards till zero crossing or minimum */
			tvyzero [ntvyzero] = rindminonlye (z, nsz, tvyzero [ntvyzero]);
			ntvyzero++;

			// Point where upward race ends
			for (isam1 = (int) tvyzero [ntvyzero - 1] + 1; isam1 < nsz - 1; isam1++)
			{
				if ( z [isam1+1] < z [isam1])
				{
					tvyzero [ntvyzero] = isam1;
					ntvyzero++;
					break;
				}
			}

			// Last segment ends at last point of signal
			tvyzero [ntvyzero] = nsz - 1;
			ntvyzero++;
		}
#endif
	}

	/**********/
	/* Output */
	/**********/
	szMsg.Format( IDS_SEG_OUT, MODULE, szSegOut, ntvyzero - 1);
	OutputMessage( szMsg, LOG_PROC );
	putdata (fSeg, tvyzero, ntvyzero, "tseg (sec)", (double) sec);


//26mar08 hlt
	if (secondary)
	{
		/* It is possible that the second tayzero is outside the interval of the stroke */
		/* In that case tayzeroout receives the last tvyzero so that the secondary interval is exactly zero */
		if (ntayzero <= 0 && ntvyzero > 0)
		{
			tayzeroout [0] = tvyzero [ntvyzero - 1];
			ntayzero = 1;
		}
		putdata (fSeg, tayzeroout, ntayzero, IDS_SEGOUT_AY, (double) sec);
	}

Cleanup:
	if( fTF ) fclose( fTF );
	if( fSeg ) fclose( fSeg );
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;

	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( vz ) delete [] vz;
	if( tvyzero ) delete [] tvyzero;
	if( tvypeak ) delete [] tvypeak;
	if( tayzero ) delete [] tayzero;
	if( tayzeroout ) delete [] tayzeroout;

	return fRet;
}

/* The following routines maybe too specific for segmen for being in hlib */


/* See also induponlye */
/********************************************************/
int induponly (double* vy, int nsvy, double tstart, double vlev)
/********************************************************/
/* index of up going jump where for the first time (v [i]) > vlev */

{
  int is0, is1, is2;


  /* Round down */
  is0 = (int)tstart;

  /* test */
  if (is0 < 0 || (is0 >= nsvy && is0 != 0)) {
    printf ("Induponly(): ERROR; is0=%d <0 or >=nsvy=%d\n", is0, nsvy);
    exit (2);
  }

  /* The problem with this check is that when vlev is very low */
  /* a first stroke may not at all be detected */
  is1 = is0;

  // 26Mar08 skip if starting high
  for (is2 = is1; is2 < nsvy - 1; is2++)
    if (vy [is2] < vlev) break;
  is1 = is2;

  /* Prevent that is2 gets nsvy */
  for (is2 = is1; is2 < nsvy - 1; is2++)
    if (vy [is2] > vlev) break;

  return (is2);
}

/* See also induponly */
/********************************************************/
int induponlye (double* vy, int nsvy, double tstart, double vylev)
/********************************************************/
/* index of up or down going jump searching backwards */
/* where for the first time abs (v [i]) > vlev */

{
  int is0, is1, is2;

  /* Round down */
  is0 = (int)tstart;

  /* test */
  if (is0 < 0 || (is0 >= nsvy && is0 != 0)) {
    printf ("Indupe(): ERROR; is0=%d <0 or >=nsvy=%d\n", is0, nsvy);
    exit (2);
  }

  is1 = is0;

  // 26Mar08 skip if starting high
  for (is2 = is1; is2 > 0; is2--)
    if (vy [is2] < vylev) break;
  is1 = is2;

  /* Search backwards for vy far from zero */
  /* Prevent that is2 gets less than 0 */
  for (is2 = is1; is2 > 0; is2--)
    if (vy [is2] > vylev) break;

  return (is2);
}

/* See also rindminonly */
/************************************************/
double rindminonlye (double* vy, int nsvy, double tstart)
/************************************************/
/* Index of first zero crossing starting from tstart backwards. */
/* If no zero crossing exists, select the point closest to zero */

{
  int is0, is, indmin, indmax;

  /* Round down */
  is0 = (int)tstart;

  /* test */
  if (is0 < 0 || (is0 >= nsvy && is0 != 0)) {
    printf ("Rindmin0e(): ERROR; is0=%d <0 or >=nsvy=%d\n", is0, nsvy);
    exit (2);
  }

  /* Find zero grossing or bouncing from zero, whatever comes first */
  for (is = is0; is > 0; --is) {
    if (vy [is - 1] > vy [is])
      return ((double) is);
  }

  /* no (close) zero crossing found: select point closest to zero */
  indrmm (vy, is0, &indmin, &indmax);
  if (vy [indmax] < -vy [indmin]) return ((double) indmax);
  else return ((double) indmin);

}

/* See also rindminonlye */
/************************************************/
double rindminonly (double* vy, int nsvy, double tstart)
/************************************************/
/* Index of first zero crossing starting from tstart. */
/* If no zero crossing exists, select the point closest to zero */

{
  int is0, is, indmin, indmax;

  /* Round down */
  is0 = (int)tstart;

  /* test */
  if (is0 < 0 || (is0 >= nsvy && is0 != 0)) {
    printf ("Rinmin0(): ERROR; is0=%d <0 or >=nsvy=%d\n", is0, nsvy);
    exit (2);
  }

  /* find zero grossing or moving away from zero, whatever comes first */
  for (is = is0; is < nsvy; ++is) {
    if (vy [is - 1] < vy [is])
      return ((double) (is - 1));
  }

  /* no zero crossing found: select point closest to zero */
  indrmm (&vy [is0], nsvy - is0, &indmin, &indmax);
  if (vy [indmax + is0] < -vy [indmin + is0]) return ((double) indmax + is0);
  else return ((double) indmin + is0);

}

/**********************************************************/
int indpeak (double* z, int ns, int isam1, int isam2)
/**********************************************************/
{
  int isampeak, isam;
  int isam1a, isam2a;
  double peak;

  /* Find positive peak between isam1 and isam2 */
  isampeak = isam1;
  peak = z [isampeak];

  isam1a = MAX (0, isam1);
  isam2a = MIN (isam2, ns-1);

  for (isam = isam1a; isam < isam2a; isam++)
  {
          if (peak < z [isam])
          {
                  peak = z [isam];
                  isampeak = isam;
          }
  }
  return (isampeak);

}

// 26mar08 hlt
// Derived from similar Segmen routine. Difference is only: Detect downgoing zero crossing.
/************************************************************************************************************************/
int remayzerocross1 (double* tayzero, int ntayzero, double* tvyzero, double* tvypeak, int ntvyzero,
					double* tayzeroout, int ntayzeroout)
/************************************************************************************************************************/
/* Remove first ay zero crossing, leave second, and remove all others within a stroke */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( tayzero != NULL );
	ASSERT( tvypeak != NULL );
	ASSERT( tvyzero != NULL );
	ASSERT( tayzeroout != NULL );

	int itvy = 0, itay = 0, itayout = 0;
	if (ntayzero == 0)
		return 0;

	itay = 0;    /* points at tayzero */
	itvy = 0;    /* points at tvyzero */
	itayout = 0; /* points at tayzeroout */
//


	// For each segmentation point, find the closest
	double tdistance = 0, tdistancemin = 0;
	int itaydistancemin = 0;
	for (itvy = 0; itvy < ntvyzero; itvy++) {

		// Some largest tdistance
		tdistancemin = 1.e10;

		for (itay = 0; itay < ntayzero; itay++ ) {
			tdistance = abs (tayzero [itay] - tvyzero [itvy]);
			// TODO qualify point
			if (tdistance < tdistancemin) {
				tdistancemin = tdistance;
				itaydistancemin = itay;
			}
		}
		// Closest found
		// Not further away then between sample and half second before
		if (tayzero [itaydistancemin] < tvyzero [itvy] - 0.500)
			tayzeroout [itvy] = tvyzero [itvy];
		else if (tayzero [itaydistancemin] > tvyzero [itvy] + 0.500)
			tayzeroout [itvy] = tvyzero [itvy];
		else
		{
			tayzeroout [itvy] = tayzero [itaydistancemin];
			// Some remote value to prvent the same tayzero point is selected twice.
			tayzero [itaydistancemin] = 0.;
		}
	}
	return ntvyzero;
}


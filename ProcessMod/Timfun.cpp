/*************************************************
TimFun - version 2.5 - hlt - 10 Aug 1994
**************************************************
Copyright 1994 Teulings

ARGUMENTS: hwr_infile tf_outfile [filterfreq (default=23.5)] [decimate (default=1)]

PURPOSES:
(1) Calculates time functions and spectra
(2) Lowpass filtering
(3) Decimate allows to read only every decimate-th sample
(4) Decimation occurs automatically if not specified.
Suppress decimation by decimation = 1

COMPILATION:
(1) MS Visual Studio .NET 2003

UPDATES:
13Jul94: gethwr() included, which does decimation
20Jul94: Do not output spectrum point nfft/2+1
22Jul94: Spectra calibrated.
27Jul94: Automatic decimation.
10Aug94: Automatic decimation only if decimation not specified.
30Sep94: Prsmin = 1 (was 20).
21Mar95: Message on trailing  pen lift shorter; decimate message only if not 1
30Mar95: Preventing overflow processing files>32000; Message when >900,000,000.
20Apr95: Jerk added; Decimate = 0 means automatic.
31May95: readhwr() separated from main()
09Jun95: Unit of jerk multiplied wih sec**3 instead of sec**2.
12Dec95: Bug if optdecimate>1 but trailing part is penup, then
         optdecimate can be reduced. decimate was not set = optdecimate
14Jan97: Better output on deffile and warnings for filter aliasing
		 Bug in deffile read if no comment is given
07May97: Removed spaces from putdata labels
13Jun97: No filtering (4 times) if filter freq > fsample/2; extra warning
		 Spectrum of raw signal or acceleration optional
16Sep98: Include unrotate
19Nov98: Zero input file caused zero devide
19Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		 removal of statics
19May00: New option: Automatic unrotation with whole trial is one stroke yielding slants in 4 quadrants.
		 TODO: New Switch in automatic unrotation: One-stroke trial rotated to +vertical
23May00: Bug in Automitc unrotatiton
25May00: Fold Beta back to with -PI and +PI
21jun01; Improved Beta in Automatic vs non-automatic. In Automatic it rotates to HORIZONTAL now.
09Aug05: GMB: Tests run: modified the 4 main #defines (NSMAX, NFREQMAX, DEFAULTFILTF)
		 individually to extremely low values to test for crashes
		 RESULTS: no crashes
18Sep06: GMB: Converted deprecated string/file functions for MSVS7
22Oct07: GMB: Added global var for # of samples reading HWR file ('ns')
08Jul08: GMB: Changed position OR velocity OR accel. spectrum to all 3 - filtered & not
07Oct08: Somesh: filtf, tranbh passed as double(floating point) to rlowps
27Sep09: GMB: Added "Remove Trailing Penlift" option. Passed to 'readhwr'

CALIBRATION:
If the sampling rate and resolution differ from the default
Wacom PL-100V setting at ASU.
Create a file "cmsec.def" in the current directory or or specify
any other dev:\path\name by:
set DEFFILE=....    (in Unix: setenv DEFFILE ...)
Example of a cmsec.def file:
0.00508 cm Comments ...
0.005 sec Comments ...
1 prsmin COmments ...

REQUIRES:
Deffun.h
Siglib.c
Iolib.c

COMPILATION:
(1) Borland BC under DOS
(2) Borland BC for MSWindows
Choose Options Application... WindowsApps (See (1))
(3) Unix/Xwindows
#!/bin/csh
cc -c -D__platform__=__unx_x11__ timfun.c
cc timfun.o -lm -o timfun

************************************************************************/

#include "../MFC/mfc.h"

#include <math.h>
#include "ProcMod.h"
#include "../Common/siglib.h"
#include "../Common/ProcSettings.h"
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"

#define DEFAULTFILTF	10

#define	MODULE		_T("TIME FUNCTION")

//28Jul08: Somesh: Define direction of FFT for function fftc
#define FORWARD +1
#define INVERSE -1

// FFT Low Pass vs Butterworth Low Pass
bool fftlw = false;
void SetFFT( bool fVal ) { fftlw = fVal; }
#define SHARP 1.75
double sharpness = SHARP;
void SetSharpness( double dVal ) { sharpness = dVal; }

// Remove Trailing Penlift
bool fRemTrailLift = true;
void RemoveTrailingPenlift( bool fVal ) { fRemTrailLift = fVal; }
bool GetRemoveTrailingPenlift() { return fRemTrailLift; }

// Up-Sample Factor
int upsamplefactor = 1;
void SetUpSampleFactor( int nVal ) { upsamplefactor = nVal; }

// global for # of samples read for current raw
long _Samples = 0;
void SetSampleCount( long nVal ) { _Samples = nVal; }
long GetSampleCount() { return _Samples; }

/*************************************************************************/
bool TimeFunction( CString szHWRIn, CString szTFOut, double dFilterFreq,
				   int nDecimate, unsigned int nSwitch, double dRotationBeta )
/*************************************************************************/
{
	bool fRet = true;

	double* x = new double[ NSMAX ];
	double* y = new double[ NSMAX ];
	double* xout = new double[ NSMAX ];
	double* yout = new double[ NSMAX ];
	double* zout = new double[ NSMAX ];
	double* z = new double[ NSMAX ];
	double* xbuf = new double[ NSMAX ];
	double* ybuf = new double[ NSMAX ];
	double* zbuf = new double[ NSMAX ];
	double* xin = new double[ NSMAX ];
	double* yin = new double[ NSMAX ];
	double* zin = new double[ NSMAX ];
	double* vaspectrum = new double[ NSMAX ];
	double* vx = new double[ NSMAX ];
	double* vy = new double[ NSMAX ];

	double cm = 0.0, sec = 0.0, prsmin = 0.0;
	double fsampl = 0.0;
	double filtf = dFilterFreq, tranbh = 0.0;
	double fact = 0.0;
	int is = 0, ns = 0, nsout = 0, nread = 0;
	int nfft = 0;
	int nw = 0;
	int nfiltershift = 0;
	double alpha = 0.0, rf = 0.0;
	CString szMsg, szTxtSpec;
	CStdioFile fIn;
	FILE* fOut = NULL;

	int decimate = nDecimate, decimatearg = 0;
	int diffspec = 1;
	double beta_man = dRotationBeta, beta_auto = 0.0, beta = 0.0;
	double sinb = 0.0, cosb = 0.0, mdist = 0.0;
	double w = 0.0, a = 0.0, b = 0.0, r = 0.0, ds = 0.0;
	int keep_dc = 0;
	bool autounrotate = false, outputjerk = true;
	/* 19May00: hlt */
	bool trialis1stroke = false;
	double slant2 = 0.0;
	//25Jul08: Somesh: nsshifted = ns + 2*nfiltershift bounded by NSMAX*0.8.
	//				   nscyclic is num of relevant signal samples (including nfiltershift) after sine wave is appended.
	int nsshifted = 0; int nscyclic = 0;

	if( decimate == 0 ) decimate = 1;
	else decimatearg = decimate;


	// Initialize Arrays
	// 10Sep03: GMB - Changed initialization from loop to memset
	memset( x, 0, NSMAX * sizeof( double ) );
	memset( y, 0, NSMAX * sizeof( double ) );
	memset( z, 0, NSMAX * sizeof( double ) );
	memset( xout, 0, NSMAX * sizeof( double ) );
	memset( yout, 0, NSMAX * sizeof( double ) );
	memset( zout, 0, NSMAX * sizeof( double ) );
	memset( xbuf, 0, NSMAX * sizeof( double ) );
	memset( ybuf, 0, NSMAX * sizeof( double ) );
	memset( zbuf, 0, NSMAX * sizeof( double ) );
	memset( xin, 0, NSMAX * sizeof( double ) );
	memset( yin, 0, NSMAX * sizeof( double ) );
	memset( zin, 0, NSMAX * sizeof( double ) );
	memset( vx, 0, NSMAX * sizeof( double ) );
	memset( vy, 0, NSMAX * sizeof( double ) );
	memset( vaspectrum, 0, NSMAX * sizeof( double ) );

	/********************************/
	/*** Read dynamic definitions ***/
	/********************************/
	ReadDynamicDefs( cm, sec, prsmin );
	// 10Sep03: GMB - Added assert to ensure validity of values
	ASSERT( sec > 0 );
	ASSERT( cm > 0 );

	/** lowpass filter frequencies not yet known here: take safe values **/
	if( filtf <= 0 )
	{
		szMsg.Format( IDS_TF_FF, MODULE, filtf);
		OutputMessage( szMsg, LOG_PROC );
		return false;
    }

	/* Decimation number; Take every decimate sample */
	if( decimate < 0 )
	{
		szMsg.Format( IDS_TF_DEC, MODULE, decimate);
		OutputMessage( szMsg, LOG_PROC );
		return false;
	}

	/************/
	/* Switches */
	/************/
	// 05/28/03: GMB - We swapped the logic from "use" jerk to "supress"
	if( ( nSwitch & TF_SWITCH_J ) != 0 )
	{
		outputjerk = false;
	}
	else
	{
// 		szMsg.Format( IDS_TF_JERK, MODULE );
// 		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & TF_SWITCH_R ) != 0 )
	{
		diffspec = 0;
		szMsg.Format( IDS_TF_DS, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & TF_SWITCH_A ) != 0 )
	{
		diffspec = 2;
		szMsg.Format( IDS_TF_DS2, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & TF_SWITCH_U ) != 0 )
	{
		autounrotate = true;
		szMsg.Format( IDS_TF_AUTO, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & TF_SWITCH_O ) != 0 )
	{
        trialis1stroke = true;
		szMsg.Format( IDS_TF_ONE, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	/*********************/
	/*** Open Input    ***/
	/*********************/
	if( !fIn.Open( szHWRIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_TFINP_ERR, MODULE, szHWRIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	/* Read */
	/* Quick patch to make arguments consistent again */
	/* Trailing pen ups are discarded */
	// TODO: nswanted (=NSMAX/1.25 is fixed. May be settable in future
	// TODO: nsskip (=0) is fixed. May be settable in future
	// TODO: TestDiscontinuity (=true) is fixed. Should become settable by the user
	// The factor of 1.25 means that you need at least 1/5 of the signal to append a half sign wave to make the signal cyclc so 4/5 is the signal.
	// Also, the FFT requries an array of size equal to a power of 2 so we mostly need to expand the array anyway.
	// EXAMPLE:
	// If you have an array of 128, then you can have maximally 128/1.25 samples = 102 samples which is nice as it is just exceeds 100 which is the sampling rate so it corresponds with 1 s.
	// So you need successively larger FFT arrays with 1,2,4,8,16,32 seconds for arrays of 128, 256, 512, 1024, 2048, 4096, resp.
	// Remember that the FFT requires array sizes of a power of 2.
	// Since NSMAX was 4096, when you get recordings with more than 32 seconds you will need to set decimate=2 to fit data +
	// TODO: This factor of 1.25 occurs on many placed in Timfun and the functions used by Timfun. Needs replaced by one variable. And needs to become user settable.
	// 27Sep09: GMB: Passing in "Remove Trailing Penlift" variable to readhwr (it is now a settable option)
	decimate = readhwr( fIn, szHWRIn, xin, yin, zin, (int)(NSMAX / 1.25), (int) (NSMAX / 1.25),
						ns, 0, decimate, decimatearg, prsmin, true, fRemTrailLift );
	// 22Oct07: GMB: Added global setting for # of samples read from current raw
	::SetSampleCount( ns );
    /* Close */
	fIn.Close();

	if (ns == 0)
	{
		szMsg.Format( IDS_TFINP_SAM, MODULE, szHWRIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/*********************************/
	/* correct units due to decimate */
	/*********************************/
	sec *= decimate;
	fsampl = 1. / sec;

	/*********************/
	/*** Open Output    ***/
	/*********************/
	if( ( fopen_s( &fOut, szTFOut, "w" ) ) != 0 )
	{
		szMsg.Format( IDS_TFOUT_ERR, MODULE, szTFOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	//28Jul04: Raji: New method to manipulate data adding samples at beginning & end
	//For usable filtered data (beginning samples) in Butterworth and to eliminate noise in FFT at begin and end
	//	25Jul08: Somesh-
	//	filtf = filter frequency
	//	tranbh = half of the transition bandwidth of the filter
	//	nfiltershift = estimate of number of samples where the butterworth filter's width has effect during convolution
	if (sharpness < 1.1)
		sharpness = 1.1;
	if (!fftlw)
		sharpness = 1.33;
	tranbh = filtf / sharpness;
	if ((filtf - tranbh) <= 0.0)
		nfiltershift = 0;
	else
		nfiltershift = (int) (fsampl/(filtf-tranbh));

	//28Jul08: Somesh {remark only}: Limit nfiltershift for very low filter frequencies
	if (nfiltershift > (0.2*ns))
		nfiltershift = (int)(0.2*ns);

	//25Jul08: Somesh: nfiltershift when appended to ns should not exceed NSMAX
	nfiltershift = MIN ( nfiltershift, (int)(0.5 * (0.8 * NSMAX - ns)) );

	//28Jul08: Somesh: assign nsshifted. The bound on nfiltershift above ensures nsshifted doesn't exceed NSMAX*0.8
	nsshifted = ns + 2*nfiltershift;

	if (nfiltershift)
	{
		//ns = original number of samples
		//nsshifted = ns+2*nfiltershift

		for (is = nfiltershift; is <= nsshifted - nfiltershift; is++)
		{
			x [is] = xin [is - nfiltershift];
			y [is] = yin [is - nfiltershift];
			z [is] = zin [is - nfiltershift];
		}

		for (is = 0; is < nfiltershift; is++)
		{
			x [is] = x [nfiltershift];
			y [is] = y [nfiltershift];
			z [is] = z [nfiltershift];
		}

		for (is = 0; is <= nfiltershift; is++)
		{
			// 06Nov08: Somesh: The last index was nsshifted - 2 * nfiltershift
			x [is + nsshifted - nfiltershift] = xin [nsshifted - 2 * nfiltershift - 1];
			y [is + nsshifted - nfiltershift] = yin [nsshifted - 2 * nfiltershift - 1];
			z [is + nsshifted - nfiltershift] = zin [nsshifted - 2 * nfiltershift - 1];
		}

	}
	else
	{
		for (is = 0; is < ns; is++)
		{
			x [is] = xin [is];
			y [is] = yin [is];
			z [is] = zin [is];
		}
	}

	/****************************************************************************************/
	/****************************************************************************************/
	/*** processing																		  ***/
	/****************************************************************************************/
	/****************************************************************************************/
	/************/
	/* Rotation */
	/************/
	if (autounrotate)
	{
		w = 1.;

		//25Jul08: Somesh: Regression only on actual data points
		mdist = rregr ( &x [nfiltershift], &y [nfiltershift], (nsshifted-nfiltershift) , w, &a, &b, &r);
		/* White noise between -.5 and +.5 yields var=Int(-.5,+.5) x**2 dx = (-.5,+.5)x**3/3 = 1/12. */
		szMsg.Format( IDS_TFAUTO_Y, MODULE, r, a, b);
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_TFAUTO_RMS, MODULE, mdist, mdist * cm );
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_TFAUTO_INFO, MODULE, sqrt (1./12.), sqrt (1./12.) * cm, mdist / sqrt (1./12.) );
		OutputMessage( szMsg, LOG_PROC );
		// 10Sep03: GMB - Added assert to ensure validity of indices

		//25Jul08: Somesh: Calculate for first and last sample of real data (no appended data)
		ASSERT( ( nsshifted -nfiltershift - 1 ) >= 0 );
		ds = sqrt (SQR (x [nsshifted - nfiltershift - 1] - x [nfiltershift])
					+
					SQR (y [nsshifted - nfiltershift - 1] - y [nfiltershift]));
		if (ds > 0.)
		{
			szMsg.Format( IDS_TFAUTO_LEN, MODULE, ds, ds * cm, mdist / ds);
			OutputMessage( szMsg, LOG_PROC );
		}
		/* estimate angle; high values of b refer to almost vertical */
		beta_auto = atan (b);

		/* 19May00: hlt: TODO: One-stroke trial rotated to +vertical under automatic rotation */
		/* 05 Jun 03: raji: This option is set using the OneStroke Analysis switch*/
		/* If the switch is not set then this whole block is not executed*/
		//if (trialis1stroke)
		//{
		/* 19May00: hlt: Make beta in 4 quadrants just like pad->slant2 in extract.cpp */
		/* Orientation of the connection between begin and end of the trial */
		/* Potential BUG: This works only if the trial is one stroke */
		/* 23May00: hlt: BUG: Was [ns] instead of [ns-1]; [1] instead of [0] */

		/* Slant2 is the vectorial slant between begin and end */
		// 10Sep03: GMB - Added assert to ensure validity of indices
		ASSERT( ( nsshifted - nfiltershift - 1 ) >= 0 );
		if (y [nsshifted-nfiltershift-1] - y [nfiltershift] != 0. && x [nsshifted-nfiltershift-1] - x [nfiltershift] != 0.)
			slant2 = (double) atan2 ((double) (y [nsshifted-nfiltershift-1] - y [nfiltershift]),
									 (double) (x [nsshifted-nfiltershift-1] - x [nfiltershift]));
		else
			slant2 = 0.;

		/* Find beta_auto that is close to slant2 by adding or subtracting PI */
		if (fabs ((double) (beta_auto - PI - slant2)) <
			fabs ((double) (beta_auto      - slant2)))
			beta_auto  -= PI;
		if (fabs ((double) (beta_auto + PI - slant2)) <
			fabs ((double) (beta_auto      - slant2)))
			beta_auto  += PI;

		ASSERT (beta_auto <= 2.*PI);
		ASSERT (beta_auto >= -2.*PI);

		szMsg.Format( IDS_TFAUTO_RAD, MODULE, beta_auto, b );
		OutputMessage( szMsg, LOG_PROC );
	}

	/* The rotation angle is such that at the end the direction will be PI/2 */
	/* hlt 21jun01 rotate to horizontal (0 deg) is now default */
	//beta = PI / 2. + beta_man - beta_auto;
	beta = beta_man - beta_auto;

	/* 25May00: hlt: Fold beta back into the range between -PI and +PI */
	if (beta >= PI)
		beta -= 2.*PI;
	else if (beta <= -PI)
		beta += 2.*PI;

	if ((beta) != 0.)
	{
		/****************/
		/*** rotation ***/
		/****************/
		sinb = sin (beta);
		cosb = cos (beta);
		keep_dc = true;
		ratfor (x, y, nsshifted, cosb, -sinb, sinb, cosb, keep_dc);

		szMsg.Format( IDS_TFRATFOR, MODULE, beta );
		OutputMessage( szMsg, LOG_PROC );
	}


	/*************/
	/* FFT filter*/
	/*************/
	if(fftlw)
	{
		/**********/
		/* cyclic */
		/**********/
		/* Make also optimal ns and nfft */
		alpha = (double) 0.5;
		nw = 0;

		// TODO nsshifted = nsshifted is returned, change into nscyclic because after making cyclic
		//we may have to sacrifice signal when decimate is held at 1. If it were 0 it would find the
		//optimal decimate to store the entire array.

		//29Jul08: Somesh: nsshifted may be truncated to nscyclic. Modify nsout accordingly to yield correct no of O/P points
		rwin (x, y, nsshifted, NSMAX, &nscyclic, &nfft, nw, alpha);

		// 10Sep03: GMB - Added assert to ensure validity of value
		ASSERT( nfft != 0 );
		double dIntFactor = 2. * PI * fsampl / nfft;
		/*******/
		/* fft */
		/*******/
		// The only forward fft
		int nfftForward;
		fftc (x, y, nfft, FORWARD);
        nfftForward = nfft;

		if (upsamplefactor*nfft <= NSMAX)
		{
			// Upsample z
			// 28Jul08: Somesh: Implemented z interpolation
			// 11Oct10: Somesh: This was wrong! Made corrections.
			int istemp, isout;
			for (is = nfiltershift; is <= nsshifted-nfiltershift; is++)
			{
				for (istemp = 0; istemp < upsamplefactor; istemp++)
				{
					isout = nfiltershift * upsamplefactor + (is - nfiltershift)*upsamplefactor + istemp;
					zbuf [is * upsamplefactor + istemp] = z [is] + (int)( (z [is+1] - z [is]) * (double)istemp / (double)upsamplefactor );
				}
			}

			// Upsample x,y
			rpolw (x, y, nfft, upsamplefactor * nfft);

			nfft *= upsamplefactor;
			ns *= upsamplefactor;
			nscyclic *= upsamplefactor;
			nsshifted *= upsamplefactor;
			sec /= upsamplefactor;
			fsampl *= upsamplefactor;
			nfiltershift *= upsamplefactor;
		}

		//29Jul08: Somesh: nsout has relevant number of samples of ns
		if (nsshifted - nscyclic >= nfiltershift)
			nsout = nscyclic - nfiltershift;
		else
			nsout = ns;

		// 11Oct10: Somesh: z interpolation moved above
		//for (istemp = 0; istemp < upsamplefactor; istemp++)
		//z [is + istemp] = z [is];

		/*** x, y -> buffer ***/
		/* Copy and scale */
		//20Jun08: Somesh: Scaling dependent upon forward fft only
		fact = cm / nfftForward;
 		for (is = 0; is < nfft; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
		}

		//20Jun08: Somesh: moved from above
		rf = nfft / fsampl;  /* relative frequencies for rlowps */

		/******************/
		/* lowpass filter */
		/******************/
		szMsg.Format( IDS_TFLPF_0, MODULE, filtf, filtf - tranbh,
					filtf + tranbh, fsampl);
		OutputMessage( szMsg, LOG_PROC );
		if (filtf + tranbh > fsampl * 0.5)
		{
			szMsg.Format( IDS_TFLPF_1, MODULE );
			OutputMessage( szMsg, LOG_PROC );
		}
		if (filtf > fsampl * 0.5)
		{
			szMsg.Format( IDS_TFLPF_2, MODULE );
			OutputMessage( szMsg, LOG_PROC );
		}
		else
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));

		/*****************/
		/* x, y filtered */
		/*****************/
		fftc (xbuf, ybuf, nfft, INVERSE);

		/*** output ***/
		szMsg.Format( IDS_TF_OUT, MODULE, szTFOut );
		OutputMessage( szMsg, LOG_PROC );
		putdata (fOut, &sec, 1, "sec", 1.);
		putdata (fOut, &prsmin, 1, "prsmin", 1.);
		putdata (fOut, &beta, 1, "beta", 1.);
		putdata (fOut, &xbuf [nfiltershift], nsout, "x(cm)", 1.);
		putdata (fOut, &ybuf [nfiltershift], nsout, "y(cm)", 1.);
		// 11Oct10: Somesh: Was z [nfiltershift]
		putdata (fOut, &zbuf [nfiltershift], nsout, "z", 1.);

		// Compute all 3 spectra: Pos, Vel, Accel
		// - POSITION
		fact = cm / nfft;
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < nfft; is++ )
		{
			xbuf[ is ] = x[ is ] * fact;
			ybuf[ is ] = y[ is ] * fact;
		}
		// 24Jul08: Somesh: Was nfftout
		powerspectrum( xbuf, ybuf, nfft, vaspectrum );
		// --- out position
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("Spectrum"), (double)nfft / MAX( 1, nsout ) );
		// - VELOCITY
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out velocity
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("V_Spectrum"), (double)nfft / MAX( 1, nsout ) );
		// - ACCELERATION
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out acceleration
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("A_Spectrum"), (double)nfft / MAX( 1, nsout ) );

		/******************/
		/* lowpass filter */
		/******************/
		if( filtf <= ( fsampl * 0.5 ) )
			rlowps( xbuf, ybuf, nfft, ( filtf * rf ), ( tranbh * rf ) );

		/***************************/
		/* power spectrum filtered */
		/***************************/
		// Compute all 3 filtered spectra: Pos, Vel, Accel
		// - POSITION
		// 24Jul08: Somesh: Was nfftout
		powerspectrum( xbuf, ybuf, nfft, vaspectrum );
		// --- out position
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("Spectrum_Filtered"), (double)nfft / MAX( 1, nsout ) );
		// - VELOCITY
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out velocity
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("V_Spectrum_Filtered"), (double)nfft / MAX( 1, nsout ) );
		// - ACCELERATION
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out acceleration
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("A_Spectrum_Filtered"), (double)nfft / MAX( 1, nsout ) );

		/****************************/
		/* differentiate unfiltered */
		/****************************/
		//20jun08: Somesh: Was nfftout
		rdif (x, y, nfft);
		//20Jun08: Somesh: Scaling dependent upon forward fft only
		/* Copy and scale */
		//20Jun08: Somesh: Was nfftout
		fact = cm / (sec * nfftForward);
		for (is = 0; is < nfft; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
		}

		/******************/
		/* lowpass filter */
		/******************/
		if (filtf <= fsampl * 0.5)
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));

		/*******************/
		/* vx, vy filtered */
		//20jun08: Somesh: Was nfftout
		fftc (xbuf, ybuf, nfft, INVERSE);
		/*** output ***/
		//25Jul08: Somesh: Was nsout
		putdata (fOut, &xbuf [nfiltershift], nsout, "vx(cm/s)", 1.);
		putdata (fOut, &ybuf [nfiltershift], nsout, "vy(cm/s)", 1.);

		/*****************/
		/* vabs filtered */
		/*****************/
		for (is = 0; is < nsshifted; ++is)
			yout [is] = sqrt (xbuf [is] * xbuf [is] + ybuf [is] * ybuf [is]);
		/*** output ***/
		//25Jul08: Somesh: Was nsout
		putdata (fOut, &yout [nfiltershift], nsout, "vabs(cm/s)", 1.);

		/*************************************/
		/* Differentiate 2nd time unfiltered */
		/*************************************/
		/*** ax, ay -> buffer ***/
		//20jun08: Somesh: Was nfftout
		rdif (x, y, nfft);

		/* Copy and scale */
		//20Jun08: Somesh: Scaling dependent upon forward fft only
		fact = cm / (sec * sec * nfftForward);
		//20Jun08: Somesh: Was nfftout
		for (is = 0; is < nfft; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
		}

		/******************/
		/* Lowpass filter */
		/******************/
		if (filtf <= fsampl * 0.5)
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));

		/*******************/
		/* ax, ay filtered */
		/*******************/
		//20jun08: Somesh: Was nfftout
		fftc (xbuf, ybuf, nfft, INVERSE);

		/*** output ***/
		//25Jul08: Somesh: Was nsout
		putdata (fOut, &xbuf [nfiltershift], nsout, "ax(cm/s**2)", 1.);
		putdata (fOut, &ybuf [nfiltershift], nsout, "ay(cm/s**2)", 1.);

		///** Absolute Acceleration **/
		//25Jul08: Somesh: Was ns
		for (is = 0; is < nsshifted; is++ )
			yout [is] = sqrt (xbuf [is] * xbuf [is] + ybuf [is] * ybuf [is]);
		//25Jul08: Somesh: Was nsout
		putdata (fOut, &yout [nfiltershift], nsout, "aabs(cm/s**2)", 1.);

		if (outputjerk)
		{
			szMsg.Format( IDS_TFJERK, MODULE );
			OutputMessage( szMsg, LOG_PROC );

			/*************************************/
			/* Differentiate 3rd time unfiltered */
			/*************************************/
			/*** jx, jy -> buffer ***/
			//20jun08: Somesh: Was nfftout
			rdif (x, y, nfft);

			/* Copy and scale */
			//20Jun08: Somesh: Scaling dependent upon forward fft only
			fact = cm / (sec * sec * sec * nfftForward);
			//20jun08: Somesh: Was nfftout
			for (is = 0; is < nfft; is++)
			{
				xbuf [is] = x [is] * fact;
				ybuf [is] = y [is] * fact;
			}

			/******************/
			/* Lowpass filter */
			/******************/
			if (filtf <= fsampl * 0.5)
				rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));

			/*******************/
			/* jx, jy filtered */
			/*******************/
			//20jun08: Somesh: Was nfftout
			fftc (xbuf, ybuf, nfft, INVERSE);

			/*** output ***/
			//25Jul08: Somesh: Was nsout
			putdata (fOut, &xbuf [nfiltershift], nsout, "jx(cm/s**3)", 1.);
			putdata (fOut, &ybuf [nfiltershift], nsout, "jy(cm/s**3)", 1.);

			/*****************/
			/* jabs filtered */
			/*****************/
			//25Jul08: Somesh: Was ns
			for (is = 0; is < nsshifted; ++is)
				yout [is] = sqrt (xbuf [is] * xbuf [is] + ybuf [is] * ybuf [is]);
			/*** output ***/
			//25Jul08: Somesh: Was nsout
			putdata (fOut, &yout [nfiltershift], nsout, "jabs(cm/s**3)", 1.);
		}

	/*** end of processing *************************************************/
	}
	/********************/
	/*Butterworth Filter*/
	/*******************/
	else
	{

		// In the FFT method processing is on nfft samples which includes nfiltershift + ns + nfiltershift samples
		// In the Butterworth filter we need to process the same i.e. (nfiltershift + ns + nfiltershift = nsshifted) samples

		//24Jul08: Somesh: defined nsbutt to include the two nsfiltershift data
		//Note: nsshifted = ns + 2* nfiltershift --- Line 347
		int nsbutt = 0;
		// TODO: Make warning that data are cut at the end if close NSMAX
		nsbutt = MIN ( NSMAX, nsshifted );

		int nstmp = 0;
		/*** x, y -> buffer ***/
		for (is = 0; is < nsshifted; is++)
		{
			xbuf [is] = x [is];
			ybuf [is] = y [is];
		}

		/********************************************************/
		/* Butterworth filter (4th order) dual pass for x and y */
		/********************************************************/
		//24Aug06: Yi: Generally the filter frequency is less than 1/2 of the sampling rate
		if (filtf <= fsampl * 0.5)
		{
			//24Jul08: Somesh: changed from ns to nsbutt here and everywhere
			butterworth(x, nsbutt, fsampl, filtf);
			butterworth(y, nsbutt, fsampl, filtf);

			//23Aug06: Yi: inverse the x, y data in oder to get dual pass in opposite direction for Butterworth filter.
			InverseData(x, nsbutt, NSMAX);
			InverseData(y, nsbutt, NSMAX);

			//do the butterworth in current direction
			butterworth(x, nsbutt, fsampl, filtf);
			butterworth(y, nsbutt, fsampl, filtf);

			//restore the data sequence
			InverseData(x, nsbutt, NSMAX);
			InverseData(y, nsbutt, NSMAX);
		}

		/*** output filtered x and y***/
		szMsg.Format( IDS_TF_OUT, MODULE, szTFOut );
		OutputMessage( szMsg, LOG_PROC );

		putdata (fOut, &sec, 1, "sec", 1.);
		putdata (fOut, &prsmin, 1, "prsmin", 1.);
		putdata (fOut, &beta, 1, "beta", 1.);

		// 25Jul08: Somesh: Output ns samples beginning from nfiltershift
		putdata (fOut, &x [nfiltershift], ns, "x(cm)", cm);
		putdata (fOut, &y [nfiltershift], ns, "y(cm)", cm);
		putdata (fOut, &z [nfiltershift], ns, "z", 1.);

		// Is this needed?
		/* prepend and append again for filtered data*/
		for (is = 0; is < nfiltershift; is++)
		{
			x [is] = x [nfiltershift];
			y [is] = y [nfiltershift];
			z [is] = z [nfiltershift];
		}
		//25Jul08: Somesh: Was ns
		for (is = (nsshifted - nfiltershift); is < nsshifted; is++)
		{
			x [is] = x [nsshifted - nfiltershift];
			y [is] = y [nsshifted - nfiltershift];
			z [is] = z [nsshifted - nfiltershift];
		}

		/**********************/
		/* UNFILTERED SPECTRUM*/
		/**********************/
		/* Make data CYCLIC */
		/* Make also optimal ns and nfft */
		alpha = (double) 0.5;
		nw = 0;
		// 24Jul08: Somesh: Was nsout which was ns - 2*nfiltershift. Changed into ns.
		// nstmp is the effective number of samples
		rwin (xbuf, ybuf, nsbutt, NSMAX, &nstmp, &nfft, nw, alpha);
		// 10Sep03: GMB - Added assert to ensure validity of value
		ASSERT( nfft != 0 );
		double dIntFactor = 2. * PI * fsampl / nfft;

		/*******/
		/* fft */
		/*******/
		fftc (xbuf, ybuf, nfft, FORWARD);

		/*** no interpolation of x, y, z implemented for Butterworth ***/

		//yi add a line of zero check on Aug.24.2006
		if (fsampl !=0)
		{
			rf = nfft / fsampl;  /* relative frequencies for rlowps */
		}
		else
		{
			return 0;
		}

		//	08Jul08: GMB: Change selected spectrum to all 3 spectra (P/V/A) - filtered and not

		// Compute all 3 spectra: Pos, Vel, Accel
		// - POSITION
		fact = cm / nfft;
		for( is = 0; is < nfft; is++ )
		{
			xbuf[ is ] = xbuf [ is ] * fact;
			ybuf[ is ] = ybuf [ is ] * fact;
		}
		powerspectrum( xbuf, ybuf, nfft, vaspectrum );
		// --- out position
		putdata( fOut, vaspectrum, nfft / 2, _T("Spectrum"), (double)nfft / MAX( 1, nstmp ) );
		// - VELOCITY
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out velocity
		putdata( fOut, vaspectrum, nfft / 2, _T("V_Spectrum"), (double)nfft / MAX( 1, nstmp ) );
		// - ACCELERATION
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out acceleration
		putdata( fOut, vaspectrum, nfft / 2, _T("A_Spectrum"), (double)nfft / MAX( 1, nstmp ) );

		/******************/
		/*SPECTRUM FILTERED*/
		/******************/

		// copy the filtered data to buffer
		for (is = 0; is < nsbutt; is++)
		{
			xbuf [is] = x [is];
			ybuf [is] = y [is];
		}

		/**********/
		/* cyclic */
		/**********/
		/* Make also optimal ns and nfft */
		alpha = (double) 0.5;
		nw = 0;
		rwin (xbuf, ybuf, nsbutt, NSMAX, &nstmp, &nfft, nw, alpha);
		// 10Sep03: GMB - Added assert to ensure validity of value
		ASSERT( nfft != 0 );

		/*****************/
		/* FFT xbuf ybuf*/
		/*****************/
		//20jun08: Somesh: Was nfftout
		fftc (xbuf, ybuf, nfft, FORWARD);

		rf = nfft / fsampl;  /* relative frequencies for rlowps */

		// Compute all 3 spectra: Pos, Vel, Accel
		// - POSITION
		fact = cm / nfft;
		// 22Oct08: Somesh: Was nfft/2!!
		for( is = 0; is < nfft; is++ )
		{
			// 21Oct2008: Somesh: was x [ is ] * fact;
			xbuf[ is ] = xbuf[ is ] * fact;
			ybuf[ is ] = ybuf[ is ] * fact;
		}
		// 24Jul08: Somesh: Was nfftout
		powerspectrum( xbuf, ybuf, nfft, vaspectrum );
		// --- out position
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("Spectrum_Filtered"), (double)nfft / MAX( 1, nstmp ) );
		// - VELOCITY
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out velocity
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("V_Spectrum_Filtered"), (double)nfft / MAX( 1, nstmp ) );
		// - ACCELERATION
		// 24Jul08: Somesh: Was nfftout
		for( is = 0; is < ( nfft / 2 ); is++ )
		{
			vaspectrum[ is ] *= ( is * dIntFactor );
		}
		// --- out acceleration
		// 24Jul08: Somesh: Was nfftout
		putdata( fOut, vaspectrum, nfft / 2, _T("A_Spectrum_Filtered"), (double)nfft / MAX( 1, nstmp ) );

		/****************/
		/* vx and vy */
		/****************/
		/* Tried the regular diff method in this new function - works fine*/
		/* could extend it based on reviewing the results*/
		bdif (x, nsbutt, sec, vx);
		bdif (y, nsbutt, sec, vy);

		/*** output vx and vy ***/
		//25Jul08: Somesh: Was nsout
		putdata (fOut, &vx [nfiltershift], ns, "vx(cm/s)", cm);
		putdata (fOut, &vy [nfiltershift], ns, "vy(cm/s)", cm);

		/*****************/
		/* vabs filtered */
		/*****************/
		for (is = 0; is < nsbutt; ++is)
			yout [is] = sqrt (vx [is] * vx [is] + vy [is] * vy [is]);
		/*** output ***/
		putdata (fOut, &yout [nfiltershift], ns, "vabs(cm/s)", cm);


		/*************/
		/* ax and ay */
		/*************/
		/*** ax, ay -> buffer ***/
		bdif (vx, nsbutt, sec, x);
		bdif (vy, nsbutt, sec, y);

		/*** output ax and ay***/
		putdata (fOut, &x [nfiltershift], ns, "ax(cm/s**2)", cm);
		putdata (fOut, &y [nfiltershift], ns, "ay(cm/s**2)", cm);

		///** Absolute Acceleration **/
		for (is = 0; is < nsshifted; is++ )
			yout [is] = sqrt (x [is] * x [is] + y [is] * y [is]);
		putdata (fOut, &yout [nfiltershift], ns, "aabs(cm/s**2)", cm);


		/**************/
		/* jx and jy */
		/************/
		if (outputjerk)
		{
			szMsg.Format( IDS_TFJERK, MODULE );
			OutputMessage( szMsg, LOG_PROC );

			/*** jx, jy -> buffer ***/
			bdif (x, nsbutt, sec, vx);
			bdif (y, nsbutt, sec, vy);

			/*** output jx, jy***/
			putdata (fOut, &vx [nfiltershift], ns, "jx(cm/s**3)", cm);
			putdata (fOut, &vy [nfiltershift], ns, "jy(cm/s**3)", cm);

			/*****************/
			/* jabs          */
			/*****************/
			for (is = 0; is < nsbutt; ++is)
				yout [is] = sqrt (vx [is] * vx [is] + vy [is] * vy [is]);
			/*** output ***/
			putdata (fOut, &yout [nfiltershift], ns, "jabs(cm/s**3)", cm);
		}

	/*** end of processing *************************************************/
	}

Cleanup:
	if( fOut ) fclose( fOut );
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( xout ) delete [] xout;
	if( yout ) delete [] yout;
	if( zout ) delete [] zout;
	if( z ) delete [] z;
	if( xbuf ) delete [] xbuf;
	if( ybuf ) delete [] ybuf;
	if( zbuf ) delete [] zbuf;
	if( xin ) delete [] xin;
	if( yin ) delete [] yin;
	if( zin ) delete [] zin;
	if( vx ) delete [] vx;
	if( vy) delete [] vy;
	if( vaspectrum ) delete [] vaspectrum;

	return fRet;
}
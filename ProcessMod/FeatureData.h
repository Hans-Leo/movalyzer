// FeatureData.h
// includes the column information of whether to include/exclude submovements for summarize
// and classes for sumamrization

#ifndef __FEATUREDATA_H__
#define __FEATUREDATA_H__

#include "cobject.h"
#include "coblist.h"

struct FeatureData
{
	int		nColumn;
	char	szLabel[ 50 ];
	bool	fStroke1;
	bool	fStroke2;
	bool	fStroke;
	char	szAlt[ 50 ];
};

FeatureData* FindFeatureData( CString szLabel );
FeatureData* FindFeatureData( int nIndex );

namespace NSProc
{

// STROKE Class
// Defines a stroke, it's value, and whether it's to be displayed
class Movements;	// forward-declare parent
class Stroke
{
	friend class Movements;
public:
	Stroke();
	const Stroke& operator = ( Stroke &ref );
	~Stroke() {}

	void		SetIndex( int nIndex )	{ m_nIndex = nIndex; }
	int			GetIndex()				{ return m_nIndex; }
	void		SetValue( double dVal )	{ m_dVal = dVal; }
	double		GetValue()				{ return m_dVal; }
	void		SetIsShown( bool fVal )	{ m_fShow = fVal; }
	bool		IsShown()				{ return m_fShow; }
	CString&	GetName();

	enum StrokeType
	{
		StrokeType_Unknown,
		StrokeType_Primary,
		StrokeType_Secondary,
		StrokeType_Total
	};

protected:
	int			m_nIndex;
	double		m_dVal;
	bool		m_fShow;
	CString		m_szName;
	Movements*	m_pParent;
	void		SetParent( Movements* pParent ) { m_pParent = pParent; }
	StrokeType	m_eType;
	void		SetType( StrokeType eType )		{ m_eType = eType; }
};

// MOVEMENT Class
// Defines a movement's strokes
class Feature;	// forward-declare parent
class Movements
{
	friend class Stroke;
	friend class Feature;
public:
	Movements();
	const Movements& operator = ( Movements &ref );
	~Movements() {}

	Stroke& GetStroke()				{ return m_strokeTotal; }
	Stroke& GetPrimaryStroke()		{ return m_strokePrimary; }
	Stroke& GetSecondaryStroke()	{ return m_strokeSecondary; }
	Stroke* GetStrokeByType( Stroke::StrokeType eType );

protected:
	Stroke		m_strokeTotal;
	Stroke		m_strokePrimary;
	Stroke		m_strokeSecondary;
	Feature*	m_pParent;
	void		SetParent( Feature* pParent )	{ m_pParent = pParent; }
	Feature*	GetParent()						{ return m_pParent; }
};

// FEATURE Class
// Defines a feature with it's name, movements and input index (in EXTRACT file)
class Feature : public CObject
{
public:
	Feature( CString szName, int nIndex );
	Feature( Feature& ref );
	~Feature() {}

	void		SetName( CString szName )	{ m_szName = szName; }
	CString&	GetName()					{ return m_szName; }
	void		SetIndex( int nIndex )		{ m_nIndex = nIndex; }
	int			GetIndex()					{ return m_nIndex; }
	Movements&	GetMovements()				{ return m_movements; }

protected:
	CString		m_szName;
	int			m_nIndex;
	Movements	m_movements;
};

typedef	CObList	FeatureSet;

// SEGMENT Class
// Segment object for summarization
class Summary;
class Segment : public CObject
{
	friend class Summary;
public:
	Segment() : CObject(), m_nTrial( 0 ), m_nSegment( 0 ), m_pParent( NULL ) {}
	~Segment();

	// datafile specific
	void		SetTrial( int nVal )		{ m_nTrial = nVal; }
	int			GetTrial()					{ return m_nTrial; }
	void		SetSegment( int nVal )		{ m_nSegment = nVal; }
	int			GetSegment()				{ return m_nSegment; }

protected:
	// set the features - set by parent
	void		SetFeatures( FeatureSet* );
public:
	// FIND a feature by name
	Feature*	FindFeature( CString szName );
	// FIND feature by input index
	Feature*	FindFeature( int nIndex );
	// FIND a stroke by output index
	Stroke*		FindStroke( int nIndex );
	// Does segment meet inclusion criteria?
	bool		Include();
	// Output the data
	void		OutputData( CString& szOut, bool fCheckForInclusion = false );

protected:
	int			m_nTrial;
	int			m_nSegment;
	FeatureSet	m_features;
	Summary*	m_pParent;
	void		SetParent( Summary* pParent )	{ m_pParent = pParent; }
	Summary*	GetParent()						{ return m_pParent; }
};

typedef CObList SegmentSet;

// SUMMARY Class
// Object for groups of summary objects for a trial
class Summary
{
	friend class Segment;
public:
	Summary() : m_fSMA( false ), m_fOneLine( true ), m_fWithoutLifts( false ),
				m_fWithLifts( false ), m_dLiftThreshold( 0.0 ) {}
	~Summary();

	// maximum # of data
	void		SetDataMax( int nVal )		{ m_nDataMax = nVal; }
	// is sub-movement analysis
	void		SetIsSMA( bool fVal )		{ m_fSMA = fVal; }
	bool		IsSMA()						{ return m_fSMA; }
	// is summary file one line of sub-movements
	void		SetIsOneLine( bool fVal )	{ m_fOneLine = fVal; }
	bool		IsSMAOneLine()				{ return m_fOneLine; }
	// datafile specific
	void		SetGroup( CString szVal )	{ m_szGroup = szVal; }
	void		SetSubject( CString szVal )	{ m_szSubj = szVal; }
	void		SetCondition( CString szVal )	{ m_szCond = szVal; }
	// air-stroke inclusion/exclusion
	void		SetPenLiftCriteria( bool fWithout, bool fWith, double dThreshold )
	{
		m_fWithoutLifts = fWithout;
		m_fWithLifts = fWith;
		m_dLiftThreshold = dThreshold;
	}
	bool		IsWithoutPenLifts()			{ return m_fWithoutLifts; }
	bool		IsWithPenLifts()			{ return m_fWithLifts; }
	double		GetLiftThreshold()			{ return m_dLiftThreshold; }

public:
	// Get the excluded features from the file parameter
	// ... returns whether there were any features to exclude
	bool	ReadExcludes( CString szFile );
	// Count of excluded features
	int64_t	GetExcludeCount()			{ return m_aExcludes.GetCount(); }
	// Get the extracted features (returns # of features)
	int64_t	ReadFeatures( CStdioFile* pFile );

public:
	// ADD a feature
	Feature*	AddFeature( CString szName, int nIndex );
protected:
	// INTERNAL: Add feature object & check for sub strokes based upon featuredata list
	// ...nFeature is the feature index
	// ...nCount is the # of data items added thus far
	// ...fTotalOnly will only add the total stroke
	// ...return false if add failure or exceeds # of maximum features
	bool		AddFeature( FeatureData* pData, int nFeature, int& nCount, bool fTotalOnly = false );
	// FIND a feature by name
	Feature*	FindFeature( CString szName );
	// FIND feature by input index
	Feature*	FindFeature( int nIndex );

public:
	// Go through feature set and set to hide all excluded features
	// ...return # of visible features
	int64_t		UpdateForExclusions();
	// Count of visible features
	int64_t		GetVisibleCount();
protected:
	// Locate an excluded feature by name
	bool		FindExcluded( CString szName );

public:
	// Add a summary
	void		AddSegment( Segment* pSeg );

public:
	// Collapse across up- and down-strokes
	void		CollapseUDStrokes( bool fMedian = false );
	// Collapse across all strokes
	void		CollapseStrokes( bool fMedian = false );
	// Collapse across trials
	void		CollapseTrials( bool fMedian = false );
	// Collapse across strokes & trials
	void		CollapseAllUD( bool fMedian = false );
	// Collapse across all strokes of all trials
	void		CollapseAll( bool fMedian = false );

public:
	// Output the headers
	void		OutputHeaders( CString& szOut, bool fCollapsing );
	// Output the general data (returns false if stroke is not to be written)
	bool		OutputData( CString& szOut, Segment* pSegment, bool fCheckForInclusion = false );
	// Output all data
	void		OutputAllData( FILE* fp );

protected:
	CString			m_szGroup;
	CString			m_szSubj;
	CString			m_szCond;
	SegmentSet		m_segments;
	bool			m_fSMA;			// sub-movement analysis
	bool			m_fOneLine;		// if SMA, to output all sub-movements on one line
	int				m_nDataMax;		// max # of data
	FeatureSet		m_features;
	CStringArray	m_aExcludes;	// list of excluded features
	bool			m_fWithoutLifts;
	bool			m_fWithLifts;
	double			m_dLiftThreshold;
};

};	// end of namespace

#endif	// __FEATUREDATA_H__

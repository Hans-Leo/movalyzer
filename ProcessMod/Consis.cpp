/*************************************************
    Consis - version 2.0 - hlt - 20 Mar 1995
**************************************************
    Copyright 1995 Teulings

ARGUMENTS: ext_infile con_outfile lexicon_infile cond err_outfile /Nohead

PURPOSES:
    (1) Consistency check of trials with entry "cond" in lexicon_infile
    (2) Only consistent trials are written to con_outfile while keeping record number
    (3) Errors are cumulated in a separate file error_file.

FORMAT
  lexicon_file
  Lines with: condcode #min strokes, #maxstrokes, #strokes to remove from begin,
    target size, size range, target direction, direction range.

UPDATES:
    22Aug94: #trials accepted shown; Messages streamlined.
    14Sep94: Rmove first down stroke (not yet needed)
    13Feb95: DATAMAX increased; some small changes.
    20Mar95: Checks of relative stroke sizes and loop sizes specified in lex file.
             Checks extended in case j is preceeding; Also end stroke included.
             End stroke has irrelevant length (in all letters but in j LONG);
             Letter h added (not yet used).
     5Jun95: First stroke of j changed from SHORT to SHORTLONG
     6Jun95: Actually the first stroke of j.... has undetermined length
    18Jul95: Test also angles of back and forth patterns
     6Sep95: Test all letters before first unsupported letter.
             Messages shorter; first interprete lexicon; then do all trials (one loop)
     7Feb96: Trials performed/accepted per condition to error file
             Streamlined test conditions so that all tests can in principle be done for all trials
     7Mar95: 60 trials only 14 strokes, clip when more strokes needed.
    15Mar95: Spiral @ testing, Nstroke testing moved to end because Nstroke can be reduced.
             Warning if nsegmin exceeds NSEGMAX
             NTRIALMAX from 60 to 40 (50 causes system halts). and NSEGMAX from 12 to 18.
    17Jun96: Bug: If nr of segments was already zero it was not tested whether
               trial had enough strokes so that discraded trial counter was not augmented.
    27Jun96: Trials could be discarded more times, underestimating #output.
             Empty trial now separate error; not like insufficient strokes.
     1Jul96: Bug occurring when spiral was added restored. Spiral truncates
               #strokes so should be first, then general truncation, then rest.
    15Aug96: printandbuffer() compiles discard reasons
     7Feb97: Letter a and v added. /nohead added. Missing trial error removed
    10Feb97: First stroke no loop test (artifact after removal of initial stroke)
             Removal of [itrial] index because everything is done per trial.
    28Feb97: Bug: if iseg==NSEGMAX no points should be added in profile.
    14Jul97: Improved angle criterion feedback.
    17Jul97: Added nsegskip
    29Jul97: Straightness test to discriminate strokes from circles.
    22Apr98: WordLex in output, One error report was missing
    15Sep09: Dirtarget, dirrange, lentarget, lenrange in lexicon /noUpstroketest
             More general reading of whole line and buffering it using getlinefloat()
             Dynamic compilation for normal (2 indices) and extended (9 indices) ext files.
    25Sep98: Err file like .ext file - no summaries for all trials per cond
    30Sep98: Straightness criterion from .5 to .1
             Include anticipation error /c, Write OK if no errors found
             Include filename after list of indices.
    27Oct98: Substitute missing trials option
     5Nov98: Always check nstrokes
    19Nov98: Ok for empty data.
    24Nov98: Ouput reflects normalized looparea
    10Dec98: Introduced . as arbitrary pattern
    20Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		     removal of statics
	18May00: Stroke length testing
	22May00: Primary + secondary submovement length testing
	TODO: Switch for secondary submovement analysis just like in segment, extract
	Eventually we MAY need these switches for the 3 modules combined.
	23May00: Removed overwrite of: dirrange=0.4 cm
	24May00: Prevented infinite loop by searching for last trial instead of highest-numbered trial when fLastOnly
	05May03: Minimum Loop area reduced to 0.001 from 0.01 to consider smaller loops in handwriting like movements
	08May03: Extra processing step for secondary analysis has eliminated in the block for stroke size limitation.
	09May03: New procedure for storing the segments in the case of secondary submovement analysis
	15May03: Max number of segments NSEGMAX increased to 3 times number of strokes in order to allow the processing of
		     substrokes in the case of secondary submovement analysis
	20May03  Raji: Assignment of the cumulated nr of segments nsegpattern to iseg minus 1 instead of iseg
	         Process of discarding the trials if not consistent with min/max strokes of condition altered
			 Now,the error will be written in ERR file without the further processing individual strokes,
			 to prevent the overwritting of that error in case an error occurs in processing individual strokes
	20May03: hlt: At any rate: Reduce the analysis to the min (was max) number of strokes
	24May03 GMB: Variabalized some constants for use as parameters
	30May03: Raji:Store the number of segments for each trial from the ext file before going to the next trial
	2 Jun03: Raji:While writing the output file, the number of segments nseg should be restored to the original number of segments
			 and use the input array to write the output file instead of the array for segments to be processed.
    29 Sept03: Raji: A statement 'goto notest'was in the wrong place in testing minimum and maximum number of strokes- updated
			   So, when the # of strokes was > maxstrokes, it used to skip and go to notest and hence no consistency checking was done
	30Sep03: HLT: Extended the output file columns to fit new data columns from extract file, by making
			the number of columns dependant on the number of items in the header rather than the NDATAMAX
	08Oct03: Raji: Range for length and direction in consistency checking can be specified in condition properties exactly instead of twice the value.
	09Oct03: Raji: The targetslant should be stored in an array for all the segments and checked during consistency checking. Previously it was a variable
	16Apr03: Raji: Output and process new columns of data
	May03: Raji: Procedure to check for target errors
	Jun03: Raji: check for min/max reaction times
	Sep05.2006:  Yi: apply an algirothm to recoginize an circle, l and tell the difference between circle and l,
	Sep12.2006:  Yi: apply an algirothm to tell the difference between the sequences of lll... and circles
	18Sep06: GMB: Converted deprecated string/file functions for MSVS7
	Sep28.2006   Yi: Define and modify most of the error messgaers for consistancy checking, we need to make it much clear to our customers.
	Oct17.2006:  Yi: Updated the parameters, error message for the LLL and Circles problem for the GUI usage.
	18Oct07: GMB: Added discontinuity check via a DIS file (if exists)...adds to ERR file and fails if disc.
	20Oct07: GMB: Added warning if strokes to skip >= min strokes
	06Aug08: Somesh: Fixed removal of downstroke. The incorrect array was being shifted.
	11Aug08: Somesh: datatmp is now called dataSubM (submovement)
	05Feb09: GMB: Added settable discontinuity relative intersample period error
	16Mar09: GMB: Changed out consis handles stimuli target sequences - no longer cares if targets are missing
	30Jul09: GMB: Data structures were already being initialized with memset. No need to loop through to set to 0.
	29Oct09: GMB: If CON file exists, now open for appending.
*/

/************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include "../NSShared/iolib.h"
#include "../Common/ProcSettings.h"
#include "../Common/DefFun.h"

/******************/
/* Column Indices */
/******************/
#define NINDEX	2
#define ITRIAL  0
#define ISEG    1

/* data items */
#define NDATA  52 /* Number of data */ /* last updated 18Jul08*/
#define STARTT		2
#define DELTAT		3
#define STARTY		4
#define DELTAY		5
#define DELTAX		9
#define STRAIGHTERR 10
#define SLANT		11
#define SURFACE		12
#define PENUP		15

// 10Nov08: Somesh: Get other extract data to check for unusual values
# define VYPEAK		6
# define AYPEAK		7
# define STARTX		8
# define SLANT80	13
# define RTVYPEAK	14
# define RDELTAT	16
# define RDELTAY	17
# define FREQ		18
# define DELTAS		19
# define ABSVEL		20
# define RDLENGTH	21
# define JABSQ		25
# define JABSQN		26
# define JABSQNMEAN	27
# define SCORE		28
# define NDECEL		29
# define ZAVG		30
# define NTRIALSEQ	31

/*******************/
/* Stroke features */
/*******************/
#define ASCENDING +1  /* Upward strokes have postitive vertical size: EXAMPLE: Stroke 1 of letter e */
#define DESCENDING -1 /* Downward strokes have negativee vertical size: EXAMPLE: Stroke 2 of letter e */

/* Connection feature: How strokes are connected with the previous stroke */
#define NOLOOP 1    /* No loop as stroke is fluent. EXAMPLE: Strokes 2 and 4 of letter n assuming Stroke 1 is the initial upstroke */
#define SHARP 2     /* Stroke starts with a reversal.EXAMPLE: Stroke 3 of letter n */
#define LOOP 4      /* Stroke forms loop with previous one. EXAMPLE: Stroke 2 of letter l */
#define LOOPORNOLOOP 0 /* Irrelevant loop (after removal of first).  */

/* Vertical size feature: How the length is relative to other strokes */
/* For example, all letters e shoudl be smaller than all letters l */
#define SHORT 8     /* EXAMPLE: Both strokes of letter e */
#define LONG 16     /* EXAMPLE: Bot strokes of letter l */
#define SHORTLONG 0 /* Irrelevant stroke length as end stroke of l, which may be followed by another l */

/* Stroke features */
#define NOTHING 0
#define UPSTROKE 1
#define NSTROKES 2
#define SIZES 4
#define LOOPS 8
#define ORIENTATION 16
#define SPIRAL 32
#define STRAIGHTNESS 64
#define STRAIGHT 1
#define CURVED 2
#define ANTICIPATIONTESTCRIT 0.1

/* Bounds checking for values read from ext file*/
#define DATABOUNDMAX	 10E6
#define DATABOUNDMIN	-10E6
#define BIG 1.e32

/****************/
/* Buffer sizes */
/****************/
#define NDATAMAX NINDEX + NDATA
/* hlt 21Jun 01 Extended to cope with longer words */
//#define NSEGMAX 18+1  /* one more than minimum because 1st may be removed */
/* 15 May 03 Raji: In the case of secondary analysis the total number of segments = 3* number of strokes */
/* Hence NSEGMAX should be three times max number of strokes, changed from 24 to 24*3 = 72*/
//#define NSEGMAX 72+1  /* one more than minimum because 1st may be removed */
// 26Nov08: Somesh: NSEGMAX defined in ProcSettings.h
#define NTRIALMAX 99	  /**/

#define NCONDMAX    36 /* max number of conditions to be verified */
#define NCHARLEXMAX 40 /* max length of the patterns */
#define NCHARLEXLINEMAX (40 + NCHARLEXMAX)

#define ERRBUFLEN 1024 /* #chars for error texts of all trials */
#define ERRLEN 256 /* #chars for error texts of one trial */


/************/
/* Criteria */
/************/
// Yi Sep.05.2006 recoginize the circle, lll and tell the difference between circle and lll

// gmb: 18Oct06 - variable names should not be in all caps (this is reserved for defines and macros)
//					 - also, variable names should be preceded by the variable type
//					 - bool changed to bool to conform to COM/DCOM interface
// gmb: 18Oct06 - functions for setting the variables below (added to procmod.h, process.h/cpp, & IDL as well)

// The control of whether we want to tell the difference between the circle and lll
bool fCircleLLL = true;
void SetCheckCircleLLL( bool fVal ) { fCircleLLL = fVal; }
//the threshhold for the ratio of correct number of circles in the sequence
double dThldRatioCircleSequence = 0.125;
void SetThresholdRatioCircleSequence( double dVal ) { dThldRatioCircleSequence = dVal; }
// the threshhold for the ratio of relative distance of one circle
double dThldHorizRelCircle = 0.4;
void SetThresholdHorizRelCircle( double dVal ) { dThldHorizRelCircle = dVal; }
//the threshhold for the ratio of correct number of Ls in the sequence
double dThldRatioLSequence = 0.25;
void SetThresholdRatioLSequence( double dVal ) { dThldRatioLSequence = dVal; }
// the threshhold for the ratio of relative distance of one L
double dThldHorizRelL = 0.1;
void SetThresholdHorizRelL( double dVal ) { dThldHorizRelL = dVal; }
// decide whether ingore the error of the last or first stroke of cirle and loop
bool fCircleIgnoreFirst = true;
bool fCircleIgnoreLast = true;
bool fLLLIgnoreFirst = true;
bool fLLLIgnoreLast = true;
void SetCircleLLLIgnore( bool fCIF, bool fCIL, bool fLIF, bool fLIL )
{
	fCircleIgnoreFirst = fCIF;
	fCircleIgnoreLast = fCIL;
	fLLLIgnoreFirst = fLIF;
	fLLLIgnoreLast = fLIL;
}

/* Maximum slant error for strokes of a certain direction */
#define SLANTERROR PI/8.  /* Back to the smaller range. The range could actually be set in the .lex file.; PI/4.;  PI/8. is a bit too small */
double	slanterror = SLANTERROR;
void SetSlantError( double dVal ) { slanterror = dVal; }

/* Minimum vertical stroke size increase ratio between succesive strokes in outward spiral */
#define SPIRALINCREASEFACTOR 1.1
double spiralincreasefactor = SPIRALINCREASEFACTOR;
void SetSpiralIncreaseFactor( double dVal ) { spiralincreasefactor = dVal; }

/* Minimum loop area (cm2) */
/*05 May 03: Raji: MINLOOPAREAN= 0.001 as the loops are very small in the handwriting like movements Eg. Experiment WRI*/
#define MINLOOPAREAN 0.001 /* In cm2 to get rid of quantization and noise errors */
double	minlooparean = MINLOOPAREAN;
void SetMinLoopArea( double dVal ) { minlooparean = dVal; }

//#define MINLOOPAREAN 0.01 /* In units to get rid of quantization and noise errors */
/* Maximum straightness error for straight segments */
#define STRAIGHTCRITSTRAIGHT 0.1
double	straightcritstraight = STRAIGHTCRITSTRAIGHT;
void SetMaxStraightErrorStraight( double dVal ) { straightcritstraight = dVal; }

/* Minimum straightness departure for curved segments */
//#define STRAIGHTCRITSTRAIGHT 0.5
#define STRAIGHTCRITCURVED 0.01
double	straightcritcurved = STRAIGHTCRITCURVED;
void SetMinStraightErrorCurved( double dVal ) { straightcritcurved = dVal; }

bool anticipationtestmin = false, anticipationtestmax = false;
double RTmin = 0.0, RTmax = 0.0;
// Discard if less than Minimum Reaction Time
void SetDiscardLTMin( bool fVal ) { anticipationtestmin = fVal; SetDiscardLTMinG( fVal ); }
// Minimum Reaction Time
void SetMinReactionTime( double dVal ) { RTmin = dVal; SetMinReactionTimeG( dVal ); }
// Discard if greather than Maximum Reaction Time
void SetDiscardGTMax( bool fVal ) { anticipationtestmax = fVal; SetDiscardGTMaxG( fVal ); }
// Maximum Reaction Time
void SetMaxReactionTime( double dVal ) { RTmax = dVal; SetMaxReactionTimeG( dVal ); }

// Discard trial if target order is not maintainted
bool chktgt = false;
void SetFlagBadTarget( bool fVal ) { chktgt = fVal; }

// Flag as failed for discontinuity
bool chkdis = true;
void SetFlagDiscontinuity( bool fVal ) { chkdis = fVal; }

// Discontinuity relative intersample period error
double dDisError = 0.3;
void SetDisError( double dVal ) { dDisError = dVal; }

#define NONSPECIFIED 0
#define IDLEN 5 /* sufficient for e1/2, etc. plus nul byte */
#define ISEG0 0
#define EMPTY -1
#define ICONDPATTERN ICOND2
#define NCONDDEF 6

#define	MODULE		_T("CONSIS")
//Yi Sep.28-29.2006 Define some strings for the error messgaers of consistancy checking, this file should be consistant with ProcessMod.rc file.
#define CON					_T("Consistency")
#define CON_PROCESSING		_T("Processing Consistency")
#define CON_ERROR			_T("Error")
#define CON_WARNING			_T("WARNING")
#define CON_SUMMARY			_T("SUMMARY")
#define CON_MESSAGE			_T("")
#define CON_PERFORMANCE		_T("Performance")
#define CON_SYSTEM			_T("System")
#define CON_LOCATION		_T("")
#define CON_CONTACTUS		_T("(Contact us for more help)")

struct _ArrayDataC
{
	double data [NDATAMAX] [NSEGMAX];
	double dataSubM [NDATAMAX] [NSEGMAX];
	double datalasttrial [NDATAMAX] [NSEGMAX];
	double dataline [NDATAMAX];
	int nsegminlex [NCONDMAX], nsegmaxlex [NCONDMAX], nsegskip [NCONDMAX];
	double dirtarget [NCONDMAX], dirrange [NCONDMAX], lentarget [NCONDMAX], lenrange [NCONDMAX];

	// Stroke features: whether it's straightness or curved
	int straightness [NCONDMAX];
	// STRAIGHT 1
	// CURVED 2

	// Vertical size feature: How the length is relative to other strokes
	int strokelen [NSEGMAX];
	// SHORT 8     /* EXAMPLE: Both strokes of letter e */
	// LONG 16     /* EXAMPLE: Bot strokes of letter l */
	// SHORTLONG 0 /* Irrelvant stroke length as end stroke of l, which may be followed by another l */

	//Connection feature: How strokes are connected with the previous stroke
	int	strokeloop [NSEGMAX];
	// NOLOOP 1    /* No loop as stroke is fluent. EXAMPLE: Strokes 2 and 4 of letter n assuming Stroke 1 is the initial upstroke */
	// SHARP 2     /* Stroke starts with a reversal.EXAMPLE: Stroke 3 of letter n */
	// LOOP 4      /* Stroke forms loop with previous one. EXAMPLE: Stroke 2 of letter l */
	// LOOPORNOLOOP 0 /* Irrelevant loop (after removal of first).  */

};

static _ArrayDataC padC;
CStdioFile*	_pErr = NULL;

bool printandbuffer (CString err, CString errbuf, bool toErr = false);

#pragma warning( disable: 4996 )	// disable deprecated warning

/************************************************************************/

/***********************************/
bool Consis( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			 short nStrokeMax, double dStrokeLength, double dRangeLength,
			 double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			 CString szCondition, CString szErrOut, unsigned int nSwitch,
			 CString szTrials, bool fLastOnly, bool* fPassed )
/***********************************/
{
	CStringList lst;
	CString szTemp = szTrials, szTrial;

	if( !szTrials.IsEmpty() )
	{
		int nPos = szTrials.Find( _T(" ") );
		while( nPos != -1 )
		{
			szTrial = szTemp.Left( nPos );
			lst.AddTail( szTrial );
			szTemp = szTemp.Mid( nPos + 1 );
		}
	}

	return Consis( szExtIn, szConOut, szLexIn, nStrokeMin, nStrokeMax, dStrokeLength,
				   dRangeLength, dStrokeDirection, dRangeDirection, nStrokeSkip,
				   szCondition, szErrOut, nSwitch, &lst, fLastOnly, fPassed );
}

/***********************************/
bool Consis( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			 short nStrokeMax, double dStrokeLength, double dRangeLength,
			 double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			 CString szCondition, CString szErrOut, unsigned int nSwitch,
			 CStringList* pszlTrials, bool fLastOnly, bool* fPassed )
/***********************************/
{
	bool fRet = true;
	int ndata = 0;
	char szOpenMode[ 2 ];
	POSITION pos = NULL;
	CString szMsg, errbuf, err, szTemp, szIn, szMade;
	CString err1 = "";
	int iseg = 0, nseg = 0, nseglasttrial = 0;
	int nsegpattern = 0, idata = 0, itrial = 0;
	int ntrialmax = 0, ntrialdiscarded = 0, ntrialin = 0;
	int icond = 0, ncond = 0;
	CStdioFile fExt;
	CStdioFile fTgt;
	FILE *fCon = NULL;
	/* Maximum size of each column label is 40 characters */
	CStringArray datatxt;
	CString szLine;
	char condcode [NCONDMAX] [4], wordlex [NCONDMAX] [NCHARLEXMAX];
	char condcodearg [4];  /* Presently only one byte used */
	unsigned int ichar = 0;
	char strokeid [NSEGMAX] [IDLEN];
	double minlong = 0.0, maxshort = 0.0;
	double minposloop = 0.0, maxnegloop = 0.0;
	double minsharp = 0.0, maxsharp = 0.0;
	double maxnoloop = 0.0;
	double targetslant [NSEGMAX]; /* Some strange value in case it is not set */
	int minposloopstroke = 0, maxnegloopstroke = 0, minsharpstroke = 0;
	int maxsharpstroke = 0, maxnoloopstroke = 0;
	int minlongstroke = 0, maxshortstroke = 0;
	int consistencytest = 0, discardtrial = 0;
	int nexttrial = 0, nextsegment = 0, iseg1 = 0;
	double angle [3];
	int i = 0, newcond = 0, ntrialcond = 0, itrial1 = 0, itrial2 = 0;
	int itrial1prev = 0, itrial2prev = 0, missingtrials = 0;
	int nLastTrial = 0;
	int nextstroke = 0;
	CFileFind ff;
	CString szMask, szTgtIn;
	CStringArray szSeqIn,szSeqMade;
	memset( &padC, 0, sizeof( _ArrayDataC ) );
	CStdioFile fErr;

	// Defaults for all switches
	bool absolute = false, nohead = false, morethanmaxiserror = false;
	bool testupstroke = false, anticipationtest = false, substitutemissing = false;
	bool create = false;
	/* 22May00: hlt */
	bool secondary = false;

	int ntgt = 0, itgt = 0;
	double horizontal_relative=0.0;

	/*****************/
	/*** Switches  ***/
	/*****************/
	/* From Experiment Settings >Processing >Consistency > Flags */

	// Use absolute measures instead of relative measures
	if( ( nSwitch & CONSIS_SWITCH_A ) != 0 )
	{
		absolute = true;
		szMsg.Format( IDS_CON_ABS, CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// CHECK: Is this one used?
	// Do not output column header in .con file
	if( ( nSwitch & CONSIS_SWITCH_N ) != 0 )
	{
		nohead = true;
		szMsg.Format( IDS_CON_NOHEAD,CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// Discard trial if # strokes too high
	if( ( nSwitch & CONSIS_SWITCH_M ) != 0 )
	{
		morethanmaxiserror = true;
		szMsg.Format( IDS_CON_MAX,CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// Remove initial downstroke
	if( ( nSwitch & CONSIS_SWITCH_U ) != 0 )
	{
		testupstroke = true;
		OutputMessage( szMsg, LOG_PROC );
	}
	// Discard trial if first stroke start within the reaction time
	//else szMsg.Format( IDS_CON_UPTEST, MODULE );
	else szMsg.Format( IDS_CON_UPTEST, CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi

	if( ( nSwitch & CONSIS_SWITCH_C ) != 0 )
	{
		anticipationtest = true;
		szMsg.Format( IDS_CON_ANTTEST, CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// CHECK: Used?
	// Subsitute discarded trials by emtpy trials
	if( ( nSwitch & CONSIS_SWITCH_S ) != 0 )
	{
		substitutemissing = true;
		szMsg.Format( IDS_CON_SUB1, CON_PROCESSING, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );

		if( pszlTrials )
		{
			int nTrial = 0;
			pos = pszlTrials->GetHeadPosition();
			while( pos )
			{
				CString szTrial = pszlTrials->GetNext( pos );
				szMsg.Format( IDS_CON_SUB2, ++nTrial, szTrial, CON_PROCESSING, CON_MESSAGE); //yi
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}
	/* 22May00: hlt: Adding a switch for Submovement analysis */
	/* Primary+Secondary Submovement Optimization all refer to subanalysis within a stroke */
	/* Default is false */
	if( ( nSwitch & CONSIS_SWITCH_O ) != 0 )
	{
	    secondary = true;
		szMsg.Format( IDS_EXT_SEC, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}

	/*********************/
	/*** input lexicon ***/
	/*********************/
    /* In the project: Right click the trial->Condition properties > Condition Checking */
	/* Input Consis condition properites for ALL conditions */
	/* NCONDMAX: max number of conditions to be verified */

	//********************************************************//
	// Yi Aug.28.2006
	// We may only need the loop number of 1 for the for statement below.
	// We may need to consider to remove it.
	//********************************************************//
	for (icond = 0; icond < NCONDMAX; icond++)
	{
		/* Read complete line */
		if( icond > 0 ) break;
		/* Parse the line into features */
		/* For the time being there is fixed sequence of features */
		/* Reset to neutral value to be sure */
		// Minimum Strokes
		padC.nsegminlex [icond] = nStrokeMin;
		// Maximum strokes
		padC.nsegmaxlex [icond] = nStrokeMax;
		// Strokes to skip
		padC.nsegskip [icond] = nStrokeSkip;
		// Stroke length (cm)
		padC.lentarget [icond] = dStrokeLength;
		// +/- Length Error (cm)
		padC.lenrange [icond] = dRangeLength;
		// Stroke Dir (rad)
		padC.dirtarget [icond] = dStrokeDirection;
		// +/- Dir. Erorr (rad)
		padC.dirrange [icond] = dRangeDirection;
		// Condition ID
		strcpy_s( condcode[ icond ], szCondition );

		// Stroke Pattern (= sequence of letters and each letter has a specific sequence of properties
		szLexIn.TrimRight();
		strcpy_s( wordlex[ icond ], szLexIn );
		szMsg.Format( IDS_CON_LEX1, CON_PROCESSING, CON_MESSAGE, icond+1, condcode [icond], wordlex [icond]); //yi

		// Error messages in the Results window and the log file
		if (padC.nsegminlex [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX2, padC.nsegminlex [icond] );
			szMsg += szTemp;
		}
		if (padC.nsegmaxlex [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX3, padC.nsegmaxlex [icond] );
			szMsg += szTemp;
		}
		if (padC.nsegskip [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX4, padC.nsegskip [icond] );
			szMsg += szTemp;
		}
		if (padC.lentarget [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX5, padC.lentarget [icond] );
			szMsg += szTemp;
		}
		if (padC.lenrange [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX6, padC.lenrange [icond] );
			szMsg += szTemp;
		}
		if (padC.dirtarget [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX7, padC.dirtarget [icond] );
			szMsg += szTemp;
		}
		if (padC.dirrange [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX6, padC.dirrange [icond] );
			szMsg += szTemp;
		}

		if( !szMsg.IsEmpty() ) OutputMessage( szMsg, LOG_PROC );

		// 20Oct07: GMB: Added checking (+ message) for strokes to skip >= min strokes
		if( padC.nsegskip[ icond ] >= padC.nsegminlex[ icond ] )
		{
			szMsg.Format( _T("%s %s: Condition#%d Strokes to discard(=%d)>Min. strokes(=%d). Strokes to discard must be less than Min. strokes."),
			CON_PROCESSING, CON_WARNING, icond, padC.nsegskip[ icond ], padC.nsegminlex[ icond ] );
			OutputMessage( szMsg, LOG_PROC );
		}

		/* Tests */
   		if (padC.nsegminlex [icond] > NSEGMAX)
		{
			szMsg.Format( IDS_CON_LEX8, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, padC.nsegminlex [icond], NSEGMAX);
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}
	}

	/*********/
	/* Tests */
	/*********/
	/* Test whether number of conditions icond is within range */
	if (icond == NCONDMAX)
	{
		szMsg.Format( IDS_CON_LEXTEST1, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szLexIn, icond+1, NCONDMAX); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	else if (icond == 0)
	{
		szMsg.Format( IDS_CON_LEXTEST2, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szLexIn); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	/* The number of conditions in lexicon */
	ncond = icond;

	/*******************/
	/* open input data */
	/*******************/
	/* Open .ext file created by extract */
	if( !fExt.Open( szExtIn, CFile::modeRead ) )
	{
		//zMsg.Format( IDS_CON_EXTERR, MODULE, szExtIn);
		szMsg.Format( IDS_CON_EXTERR, CON, CON_WARNING,CON_MESSAGE, szExtIn); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	// Determine last trial # if specified
	/* 24May99: hlt: Do not assume this is the highest trial number */
	if( fLastOnly )
	{
		char pszTemp[ 1024 ];
		int nTemp = 0;

		while( fExt.ReadString( szLine ) )
		{
			//sscanf( szLine, _T("%d%[^\n]"), &nTemp, pszTemp );
			//if( nTemp > nLastTrial ) nLastTrial = nTemp;
			sscanf( szLine, _T("%d%[^\n]"), &nLastTrial, pszTemp );
		}

		fExt.SeekToBegin();
	}

	szMsg.Format( IDS_CON_EXTREAD, CON_PROCESSING, CON_MESSAGE, szExtIn, szCondition); //yi
	OutputMessage( szMsg, LOG_PROC );
	/*** Read header ***/
    /* 13Oct03: hlt: Do not skip line as it is not there */
	/* Skip file name line */
	//fExt.ReadString( szLine );

	/* Read data names */
	fExt.ReadString( szLine );
	idata = 0;
	while( idata < NDATAMAX )
	{
		int nPos = szLine.Find( _T(" ") );
		/*16Dec03: Raji: Extract individual labels in to CStringArray datatxt*/
		while( nPos != -1 )
		{
			idata ++;
			datatxt.Add( szLine.Left( nPos));
			szLine = szLine.Mid( nPos + 1 );
			nPos = szLine.Find( _T(" ") );
		}
		//Nov.09.06 Yi : not sure whether it's a bug here
		idata ++;

		datatxt.Add(szLine);
		/* 30Sep03: hlt: break after detecting no more spaces */
		/* If you do not break, the last label will be repeated for N
 times */
		break;
	}
	/* 30Sep03: hlt: Count of number of labels. The number of columns will be ndatamax */
    int ndatamax; /* Max number of columns in the input .ext file */
	ndatamax = idata;

	/************************/
	/*** open output file ***/
	/************************/
	create = !ff.FindFile( szConOut );

	// 29Oct09: GMB: We want to open the file for appending if it exists
	if( create ) strcpy_s( szOpenMode, "w" );
	else strcpy_s( szOpenMode, "a" );
	/* Open for appending and creating */
	if( ( fopen_s( &fCon, szConOut, szOpenMode ) ) != 0 )
	{
		szMsg.Format( IDS_CON_CONERR,  CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szConOut); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	// 29Oct09: GMB: Only write header if file was created (not appended)
	/* Write header consisting of column headers */
	if( create )
	{
		szMsg.Format( IDS_CON_CONCREATE, CON_PROCESSING, CON_MESSAGE, szConOut); //yi
		OutputMessage( szMsg, LOG_PROC );
		if (!nohead)
		{
			/* 30Sep03: hlt: Was till NDATAMAX, now till ndatamax which is dynamic */
			for (idata = 0; idata < ndatamax; idata++)
			{
				szMsg.Format( _T("%s "), datatxt.GetAt(idata));
				fprintf( fCon, szMsg );
			}
			fprintf( fCon, _T("\n") );
		}
	}
	/**********************/
	/* Open error Outfile */
	/**********************/
	szMsg.Format( IDS_CON_ERROUT, CON_PROCESSING, CON_MESSAGE, szErrOut); //yi
	OutputMessage( szMsg, LOG_PROC );
	/* Open */
	if( !fErr.Open( szErrOut, CFile::modeWrite ) )
	{
		if( !fErr.Open( szErrOut, CFile::modeCreate | CFile::modeWrite ) )
		{
			szMsg.Format( IDS_CON_ERRERR, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szErrOut); //yi
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}
	}
	else fErr.SeekToEnd();
	_pErr = &fErr;

	/********************************/
	/* initialize input data arrays */
	/********************************/
	nseg = 0;
	ntrialmax = 0;
	ntrialdiscarded = 0;
	ntrialin = 0;
	ntrialcond = 0;
	// 30Jul09: GMB: The memset of the padC object initializes all of the arrays. This loop is not necessary.
#if 0
	for (idata = 0; idata < NDATAMAX; idata++)
	{
		for (iseg = 0; iseg < NSEGMAX; iseg++)
		{
			/* 09 May 03 Raji: This array will store the segments 3,6,9...to 3 times nstroke; in the case of secondary analysis*/
			/* and 1,2,3...to nstroke otherwise*/
			padC.data [idata] [iseg] = 0.;
			/* 09 May 03: Raji: A new array datatmp(Somesh: changed name to padC.dataSubM) added*/
			/* In order to store all the segments 1.2,3...to 3 times nstroke; in the case of secondary analysis for the output file*/
			padC.dataSubM [idata] [iseg] = 0.;
		}
	}
#endif
	/* Some impossible trial number */
	padC.datalasttrial [0] [ISEG0] = EMPTY;
	itrial = EMPTY;
	nexttrial = true;
	nextsegment = true;

	/***********************/
	/* Read trial by trial */
	/***********************/
	/* Read the .ext file created by extract */
	/* Keep on getting trials unless EOF detected last time */
	while (nexttrial)
	{
		/* Cumulative counter of number of segments */
		nseg = 0;
		/* raji: 9May03:*/
			/* Select only the full strokes (iseg=1,2,3 if no-secondary and */
			/* 3,6,9,... is secondary submovement analysis */
			/* The number of strokes needs to be reduced in the latter case */
			if (secondary)
				nextstroke=3;
			else
				nextstroke=1;
		/* Keep on getting segments for that trial*/
		while (true)
		{
			/* Read segment if not already done */
			if (nextsegment)
			{
				/* 24May00: hlt: Skip all trials with a trial number different from the last appended trial number */
				/* TODO: POTENTIAL BUG: If the same trial occurs more than once in the .ext file earlier ones are overwritten */
				/* when consistency checking the last file although, when summarizing the results of all */
				/* trials in the condition, the last trial with the targeted trial number is used */
				/* If the < statement is used we may get an infinite loop if nLastTrial < all trial nrs in the .ext file */
				//Yi, ndata is the number of data in the line of data readin from fExt
				// GMB: 17Aug07: while did not check for end of file (causing infinite loop in cases of 1 trial)
				if( fLastOnly )
				{
					do ndata = getlinefloat( fExt, padC.dataline, NDATAMAX );
					while( ( padC.dataline[ 0 ] != nLastTrial ) && ( ndata != 0 ) );
//					while( padC.dataline[ 0 ] != nLastTrial );
//					while( padC.dataline[ 0 ] < nLastTrial );
				}
				else ndata = getlinefloat (fExt, padC.dataline, NDATAMAX);

				/*************************/
				/* End of file criterion */
				/*************************/
				if (ndata == 0)
				{
					/* Remember next time not to start a next trial */
					nexttrial = false;
					/* Do not get more segments */
					break;
				}

				/* Check for incomplete line; May mean wrong stroke data file */
				/* 30sep03: hlt: Was NDATAMAX */
				if (ndata != ndatamax)
				{
					szMsg.Format( IDS_CON_EXTLEN, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, ndata, szExtIn, NDATAMAX); //yi

					for (idata = 0; idata < ndata; idata++)
					{
						szTemp.Format( _T("%.3g "), padC.dataline [idata]);
						szMsg += szTemp;
					}
					OutputMessage( szMsg, LOG_PROC );
					fRet = false;
					goto Cleanup;
				}
				/***********************/
				/* New trial criterion */
				/***********************/
				//ITRIAL is defined as 0
				//ISEG is defined as  1
				if (itrial != EMPTY && padC.dataline [ITRIAL] != itrial)
				{
					/* Stop reading more segments and remember to skip the first read next time */
					nextsegment = false;
				    break;
				}
			}//end of if (nextsegment)

			/**********************/
			/* Store this trial */
			/**********************/
			/* Here some space can be saved: Skipped strokes do not need to be stored */
			/* Also Indices may not need to be stored */
			itrial = (int) padC.dataline [ITRIAL];
			/* redefine iseg here */
			iseg = (int) padC.dataline [ISEG];
			// 10Sep03: GMB - Added asserts to ensure validity of indices

			ASSERT( iseg >= 1 );
			if (iseg <= NSEGMAX )
			{
				for (idata = 0; idata < ndata; idata++)
				{
					// All data, ALL segments of ONE trial
					/* raji: 9May03: Store the strokes in datatmp and */
					/* datatmp is output after each trial */
					/* If nextstroke=3: other wise it is just a copy */
					/* So take iseg=3,6,9, .. in positions 2, 5, 8, ... (iseg-1) */
					/* and put it into new positions 0, 1, 2, ...  (iseg/nextstroke -1) */
					padC.dataSubM [idata] [iseg - 1] = padC.dataline [idata];
					if (int(iseg/nextstroke)*nextstroke==iseg)
							padC.data [idata] [iseg/nextstroke - 1] = padC.dataline [idata];
                }

				/* 30 May 03: Raji:IF trial nr read is lesser from current value then we are still in the current trial*/
				/* the number of full strokes is 3 times less if secondary analysis */
				/* ELSE when the trial number reads equal to the current value it will point to the next trial*/
				/* Store the number of segments from the ext file before going to the next trial*/
				/* This is necessary while reprocessing a single trial, and the ext file */
				/* Has a different number of segments for each reprocessing of trial due to appending*/
				if (nseg < iseg / nextstroke)
					nseg = iseg / nextstroke;
				else
					nseg = (int)padC.dataline[ISEG]/nextstroke;
			}

			/* Max trialnr */
			if (ntrialmax < itrial)
				ntrialmax = itrial;

			/* Since segement has been stored allow next time to read following segment */
			nextsegment = true;
		}//end of while (true)

		/*******************/
		/* New trial found */
		/*******************/
		/* Count nr of trials read */
		ntrialin++;

		/******************/
		/* Condition code */
		/******************/
		sscanf (szCondition, "%s", &condcodearg);

		/* Find cond nr from cond code in lex file */
		for (icond = 0; icond < ncond; icond++)
			if (strcmp (condcodearg, condcode [icond]) == 0)
				break;
		/* Test if found or not */
		if (icond == ncond)
		{
			/* Not found */
			szMsg.Format( IDS_CON_LEXERR, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szLexIn, condcodearg, NINDEX); //yi
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}

		/************************/
		/* Skip initial strokes */
		/************************/
		// As specified in Condition properties >Condition checking
		szMsg.Format( IDS_CON_SKIP,CON_PROCESSING, CON_MESSAGE, itrial, itrial - 1, nseg, padC.nsegskip [icond]); //yi
		OutputMessage( szMsg, LOG_PROC );
		if (nseg > NSEGMAX)
		{
			szMsg.Format( IDS_CON_TRUNC, CON_PROCESSING, CON_MESSAGE, NSEGMAX); //yi
			OutputMessage( szMsg, LOG_PROC );
		}

		/* Feedback of pattern selected */
		szMsg.Format( IDS_CON_FEED, CON_PROCESSING, CON_MESSAGE, condcodearg, wordlex [icond], nseg, padC.nsegminlex [icond], padC.nsegmaxlex [icond]); //yi

		OutputMessage( szMsg, LOG_PROC );

		/*****************************************************/
		/* Interpret lexicon in terms of features of letters */
		/*****************************************************/
		/* Once for all trials: */
		/* Eventually this information should be stored in a letter file */
		/* Initialize */
		consistencytest = NOTHING;
		iseg1 = padC.nsegskip [icond];
		iseg = iseg1;

		/* All characters */
		if( !szLexIn.IsEmpty() )
		{
			for (ichar = 0; ichar < strlen (wordlex [icond]); ichar++)
			{
				/* Quit filling strokes if all NSEGMAX strokes specified before doing all ichars */
				if (iseg >= NSEGMAX-1)
					break;

				/* Pattern for each character */
				switch (wordlex [icond] [ichar])
				{
					case 'a': // Define each stroke of letter a
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "a1/4"); // Short upstroke fluently connected with previous letter
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "a2/4"); // Short downstroke starting with a sharp direction reversal
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "a3/4"); // Short upstroke fluently connected with previous stroke
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "a4/4"); // Short downtroke fluently starting with a sharp direction reversal
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke: Add last stroke of word */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "a5/4");
							padC.strokelen [iseg] = +SHORT; // Shor
							padC.strokeloop [iseg] = NOLOOP; // Short upstroke fluently connected
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					/* 23Dec04: Raji: More letters */
					case 'd':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "d1/4");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "d2/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "d3/4");
						padC.strokelen [iseg] = +LONG;
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "d4/4");
						padC.strokelen [iseg] = -LONG;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;
						case 'c':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "c1/2");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "c2/2");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "c3/2");
							padC.strokelen [iseg] = +SHORT;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case 'e':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "e1/2");
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokelen [iseg] = +LONG;
						else
							padC.strokelen [iseg] = +SHORT;
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "e2/2");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = +LOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "e3/2");
							padC.strokelen [iseg] = +SHORTLONG;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case 'h':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "h1/4");
						padC.strokelen [iseg] = +LONG;
						if (ichar > 0 && ((wordlex [icond] [ichar-1] == 'j')||(wordlex [icond] [ichar-1] == 'j')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "h2/4");
						padC.strokelen [iseg] = -LONG;
						padC.strokeloop [iseg] = +LOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "h3/4");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "h4/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = +NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "h5/4");
							padC.strokelen [iseg] = +SHORTLONG;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case 'i':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "i1/2");
						if (ichar > 0 && wordlex [icond] [ichar - 1] == 'j')
							padC.strokelen [iseg] = +LONG;
						else
							padC.strokelen [iseg] = +SHORT;
						if (ichar > 0 && wordlex [icond] [ichar-1] == 'j')
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "i2/2");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "i3/2");
							padC.strokelen [iseg] = +SHORTLONG;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case 'j':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "j1/2");

						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokelen [iseg] = +LONG;
						else
						/* This is not good as it is not known whether the first stroke is removed in jjj sequences */
							padC.strokelen [iseg] = +SHORTLONG;

						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "j2/2");
						padC.strokelen [iseg] = -LONG;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "j3/2");
							padC.strokelen [iseg] = +LONG;
							padC.strokeloop [iseg] = -LOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case 'l':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "l1/2");
						padC.strokelen [iseg] = +LONG;
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "l2/2");
						padC.strokelen [iseg] = -LONG;
						padC.strokeloop [iseg] = +LOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "l3/2");
							padC.strokelen [iseg] = +SHORTLONG;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					/* 23Dec04: Raji: More letters */
					case 'm':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "m1/6");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "m2/6");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "m3/6");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "m4/6");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "m5/6");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "m6/6");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					/* 23Dec04: Raji: More letters */
					case 'n' :
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "n1/4");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "n2/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "n3/4");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "n4/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;

						break;

					case 'o': //This is a repetitive circle, or captial O, not zero 0.
						consistencytest = NOTHING + UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* No tests are possible */
						/* Circles are not well defined */

						//Yi Sep.05.2006, common the break;
						padC.straightness [iseg] = CURVED;
						//break;


						//Yi Sep.05.2006, define more about circle
						/* Up stroke */
						strcpy_s (strokeid [iseg], "o1/2");
						padC.strokelen [iseg] = +LONG;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "o2/2");
						padC.strokelen [iseg] = -LONG;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "o3/2");
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;


					case 't':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "t1/2");
						padC.strokelen [iseg] = +LONG;
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "t2/2");
						padC.strokelen [iseg] = -LONG;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "t3/2");
							padC.strokelen [iseg] = +SHORTLONG;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					/* 23Dec04: Raji: More letters */
					case 'u':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "u1/4");
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokelen [iseg] = +LONG;
						else
							padC.strokelen [iseg] = +SHORT;
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "u2/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "u3/4");
						if (ichar > 0 && wordlex [icond] [ichar - 1] == 'j')
							padC.strokelen [iseg] = +LONG;
						else
							padC.strokelen [iseg] = +SHORT;
						if (ichar > 0 && wordlex [icond] [ichar-1] == 'j')
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "u4/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "u5/4");
							padC.strokelen [iseg] = +SHORTLONG;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case 'v':
						consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "v1/4");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "v2/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "v3/4");
						padC.strokelen [iseg] = +SHORT;
						padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "v4/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* End stroke */
						/* 23Dec04: Raji: Ignore the end stroke*/
						/*if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "v5/4");
							padC.strokelen [iseg] = +SHORT;
							padC.strokeloop [iseg] = NOLOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}*/
						break;

					case 'y':
					   	consistencytest = SIZES + LOOPS + UPSTROKE + NSTROKES;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "y1/4");
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokelen [iseg] = +LONG;
						else
							padC.strokelen [iseg] = +SHORT;
						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "y2/4");
						padC.strokelen [iseg] = -SHORT;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Up stroke */
						strcpy_s (strokeid [iseg], "y3/4");

						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokelen [iseg] = +LONG;
						else
						/* This is not good as it is not known whether the first stroke is removed in jjj sequences */
							padC.strokelen [iseg] = +SHORTLONG;

						if (ichar > 0 && ((wordlex [icond] [ichar - 1] == 'j') || (wordlex [icond] [ichar - 1] == 'y')))
							padC.strokeloop [iseg] = -LOOP;
						else
							padC.strokeloop [iseg] = NOLOOP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						/* Down stroke */
						strcpy_s (strokeid [iseg], "y4/4");
						padC.strokelen [iseg] = -LONG;
						padC.strokeloop [iseg] = SHARP;
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						///* End stroke */
						if (ichar == strlen (wordlex [icond]) - 1)
						{
							strcpy_s (strokeid [iseg], "y5/4");
							padC.strokelen [iseg] = +LONG;
							padC.strokeloop [iseg] = -LOOP;
							if (iseg >= NSEGMAX-1)
								break;
							iseg++;
						}
						break;

					case '-': // Minus sign
						targetslant [iseg] = 0. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
/* Potential BUG: Orientation will be tested also for the other characters, e.g., ee-ee */
/* To resolve this bug you want to make the orientation testing only on or off per stroke */
/* or better: define per stroke a directory and directory range */
/* As long as cursive letters and stroke directions are not mixed there will be no problem */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;
					case '/':
						targetslant [iseg] = 1. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;
					case '|':
						targetslant [iseg] = 2. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					case '\\':
						targetslant [iseg] = 3. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					case '_': // Underscore
						targetslant [iseg]= 4. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					case '?':
						targetslant [iseg] = -3. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					case '}':
						targetslant [iseg]= -2. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					case ']':
						targetslant [iseg] = -1. * PI / 4.;
						padC.straightness [iseg] = STRAIGHT;
						/* Only first stroke */
						consistencytest = UPSTROKE + NSTROKES + STRAIGHTNESS;
						/* If no dirrange given test coarse orientation */
						if (padC.dirrange [icond] == 0.)
							consistencytest = consistencytest | ORIENTATION;
						/* This character counts as one stroke & Test for stroke overflow */
						if (iseg >= NSEGMAX-1)
							break;
						iseg++;
						break;

					case '@':
						/* Spiral of multiple strokes */
						consistencytest = SPIRAL + UPSTROKE + NSTROKES;
						break;

					case '.':
						/* Nothing specific */
						consistencytest = UPSTROKE + NSTROKES;
						break;
					default:
						szMsg.Format( IDS_CON_LEX_NO1, CON, CON_WARNING, CON_MESSAGE, szLexIn, icond+1, condcodearg, wordlex [icond]);
						OutputMessage( szMsg, LOG_PROC );
						szMsg.Format( IDS_CON_LEX_NO2, CON_PROCESSING, CON_MESSAGE, wordlex [icond] [ichar], ichar+1, iseg+1); //yi
						OutputMessage( szMsg, LOG_PROC );
						/* Do not change consistencytest */
						break;
				}//end of switch /* Pattern for each character */

				/* First stroke can never have a loop */
				padC.strokeloop [0] = LOOPORNOLOOP;
				/* iseg range */
				/* Iseg may become NSEGMAX after inserting last stroke */
				if (iseg >= NSEGMAX)
				{
					szMsg.Format( IDS_CON_LEX_SEG, CON, CON_WARNING, CON_MESSAGE, szLexIn, icond+1, condcodearg, wordlex [icond], iseg, NSEGMAX); //yi
					OutputMessage( szMsg, LOG_PROC );
					break;
				}
			}//end of for (ichar = 0; ichar < strlen (wordlex [icond]); ichar++)
		}//end of if( !szLexIn.IsEmpty())

		/* cumulative number of segments*/
		nsegpattern = iseg;

		/***************/
		/* COMPARISONS */
		/***************/
		/*********************************************/
		/* Assume trial is ok and try to find errors */
		/*********************************************/
		discardtrial = false;

		/*************************************/
		/* Indices of stroke 1 to err buffer */
		/*************************************/
		errbuf = _T("");
		err.Format( _T("%d "), itrial );
		errbuf += err;

		/***********************/
		/* Test for empty data */
		/***********************/
		if (nseg <= 0 || padC.data [DELTAT][ISEG0] == 0.)
		{
			szMsg = CON;
			szMsg += _T(": ");
			err.Format( IDS_CON_EMPTYEXT, szExtIn);
			szMsg += err;
//			discardtrial = printandbuffer (err, errbuf);
			discardtrial = printandbuffer( szMsg, errbuf );
			/* further tests are not needed */
			goto notest;
		}

		/*************************************************/
   		/* Test if reached targets in the right sequence*/
		/*************************************************/
		if (chktgt)
		{
			/*****************/
			/* Open tgt_file */
			/*****************/
			/* There is seperate .tgt file for every trial (like hwr), so we have to open it here, rather than begin of consis*/
			int nPos = szExtIn.ReverseFind( '.' );
			szMask = szExtIn.Left( nPos );
			szTgtIn = szMask;
			CString szTrial;
			szTrial.Format(_T("%02d.TGT"),itrial);
			szTgtIn += szTrial;

			if( !fTgt.Open( szTgtIn, CFile::modeRead ) )
			{
				szMsg.Format( IDS_CON_TGTERR, CON,CON_MESSAGE, szTgtIn);//yi
				OutputMessage( szMsg, LOG_PROC );
				err.Format(IDS_CON_TGTERR,CON,CON_MESSAGE, szTgtIn);//yi
				discardtrial = false;
				/* There is no .tgt file if no stimulus is added or if they are older trials, as it is created during recording! */
				/* In this case, cannot go to cleanup as we have to perform other tests, as before*/
				goto test;
			}

			/****************************************************/
			/* Store the correct and actual sequence of targets */
			/****************************************************/
			fTgt.ReadString( szLine );
			sscanf (szLine, "%d", &ntgt);
			if (ntgt == 0)
			{
				err.Format(IDS_CON_TGTERR3);
				discardtrial = false;
				goto ftgtclose;
			}
			for (itgt = 0; itgt < ntgt; itgt++)
			{
				fTgt.ReadString( szLine );
    			szSeqIn.Add(szLine);
			}
			fTgt.ReadString( szLine );
			for (itgt = 0; itgt < ntgt; itgt++)
			{
				// 16Mar09: GMB: Since the # of reached targets can be less than # of available,
				// we need to add an empty string for the missing
				if( fTgt.ReadString( szLine ) ) szSeqMade.Add( szLine );
				else szSeqMade.Add( _T("") );
			}
			for (itgt = 0; itgt < ntgt; itgt++)
			{
				// 16Mar09: GMB: If the recorded sequence item is empty, we are not comparing it
				szIn = szSeqIn.GetAt( itgt );
				szMade = szSeqMade.GetAt( itgt );

				// 04Oct10: Somesh: There are 2 kinds of tasks
				// (1) fixed no. of targets and fixed no. of strokes
				// (2) fixed no. of targets and variable no. of strokes
				// We want to enforce that the targets specified in the target list be reached. Once that condition
				// is satisfied, we don't care (for now) which 'extra' targets are reached. The user may always
				// specify a "max no. of strokes" to limit extra strokes such as in a Fitts' task
				// To go back to the previous behavior described by GMB on 16Mar09, simply switch the if statements below

				//if( ( szMade != _T("") ) && ( szIn != szMade ) )
				if (szSeqIn.GetAt(itgt) != szSeqMade.GetAt(itgt))
				{
					err.Format(IDS_CON_TGTERR4, CON, CON_PERFORMANCE, CON_ERROR, CON_MESSAGE, itgt + 1, szSeqMade.GetAt(itgt), szSeqIn.GetAt(itgt));//yi
					discardtrial = printandbuffer (err, errbuf);
					// close file first
					if( fTgt.m_pStream ) fTgt.Close();
					/* further tests are not needed */
					goto notest;
				}
			}

ftgtclose:
			if( fTgt.m_pStream ) fTgt.Close();
		}

		/**************************************************************************/
		// 18Oct07: GMB: Discontinuity testing (by time)
		//		if EEEGGGSSSCCC##.DIS file exists, then there is a discontinuity
		//		file contains lines with sample # and interval from previous sample
		//		writing (first?)last instance of disc. to err file and failing trial
		// 05Feb09: GMB: Added relative intersample period error
		/**************************************************************************/
		if (chkdis)
		{
			/*****************/
			/* Open dis_file */
			/*****************/
			/* There is a seperate .dis file for every trial (like hwr), so we have to open it here, rather than begin of consis*/
			int nPos = szExtIn.ReverseFind( '.' );
			szMask = szExtIn.Left( nPos );
			CString szDisIn = szMask;
			CString szTrial;
			szTrial.Format(_T("%02d"), itrial);
			szDisIn += szTrial;
			szDisIn += _T(".DIS");
			CStdioFile fDis;
			if( fDis.Open( szDisIn, CFile::modeRead ) )
			{
				// get last line of file
				CString szErr;
//				while( fDis.ReadString( szLine ) ) szData = szLine;
				//  first line of file
				fDis.ReadString( szLine );
				// construct message
				double interval = 0.;
				int sample = 0;
				sscanf( szLine, "%d %lf", &sample, &interval );
				// only discard/show error if discontinuity is before the trailing pen lift (if exists)
				int nSampCount = ::GetSampleCount();
				if( sample < nSampCount )
				{
					int nRate = ::GetSamplingRate();
					double dSec = ( nRate != 0 ) ? 1. / nRate : 0.;

					szErr.Format( "%s: %s: Extra or missing sample in trial=%d. Sample %u has intersample period =%.4f s departing more than %g * 1 / sampling rate (%.4f) = %g s.",
								  CON_PROCESSING, CON_WARNING, nseg, sample, interval, dDisError, dSec, dDisError * dSec );
					// write to err file & output
					discardtrial = printandbuffer( szErr, errbuf, true );
					OutputMessage( szErr, LOG_PROC );
					fDis.Close();
					goto Finished;
				}
				fDis.Close();
			}
		}
test:

		/************************************/
		/* Test for unusual data in ext file*/
		/************************************/
		// 10Nov08: Somesh: Test for unusual values in ext file
		for (iseg = iseg1 + nextstroke; iseg <= nseg; iseg++)
		{
			for (idata = STARTT; idata < ndatamax; idata++)
			{
				if (padC.dataSubM [idata] [iseg] > DATABOUNDMAX || padC.dataSubM [idata] [iseg] < DATABOUNDMIN ||
					padC.data [idata] [iseg] > DATABOUNDMAX || padC.data [idata] [iseg] < DATABOUNDMIN)
				{
					szMsg = CON;
					szMsg += _T(": ");
					err.Format( IDS_CON_UNUSUALEXTVAL, szExtIn, iseg, idata);
					szMsg += err;
//					printandbuffer (err, errbuf);
					printandbuffer( szMsg, errbuf );
					goto notest;
				}
			}
		}

		/* see if reaction time is lesser/greater than maximum reaction time*/
		if (anticipationtestmin)
		{
			if (nseg > 0 && padC.data [STARTT] [ISEG0] < RTmin)
			{
				err.Format( IDS_CON_ANT1,CON_PROCESSING, CON_MESSAGE, padC.data [STARTT] [ISEG0], RTmin); //yi
				discardtrial = printandbuffer (err, errbuf);
				goto notest;
			}
		}
		if (anticipationtestmax)
		{
			if (nseg > 0 && padC.data [STARTT] [ISEG0] > RTmax)
			{
				err.Format( IDS_CON_ANT2, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, padC.data [STARTT] [ISEG0], RTmax); //yi
				discardtrial = printandbuffer (err, errbuf);
				goto notest;
			}
		}

		// Somesh: 6Aug08: If you have not set any other consistency checking
		if (testupstroke)
		//if ((consistencytest & UPSTROKE) && testupstroke)
		{
			/* Do this first */
			/*********************************/
			/* (1) Check start with upstroke */
			/*********************************/
			/* Test for initial downstroke */
			if (padC.data [DELTAY] [ISEG0] < 0.)
			{
				/* NOT A REAL ERROR SO DISCARD TRIAL SHOULD NOT BE CHANGED*/
				/* THIS MESSAGE IS GIVEN AS IT IS STILL USEFUL*/
				err.Format( IDS_CON_1REM, CON_PROCESSING, CON_MESSAGE, nseg, nseg - 1);//yi
				printandbuffer (err, errbuf);
				/* Start iseg from 1 to nseg instead of 0 to nseg-1, because we are shifting arrays*/
				// 03Nov08: Somesh: The shift of data was improper
				for (iseg = iseg1 + nextstroke; iseg <= nseg * nextstroke; iseg ++)
				{
					/* Leave trial nr and segment nr the same */
					/* Should limit the data by ndatamax not ndata because at this point ndata becomes 0*/
					for (idata = STARTT; idata < ndatamax; idata++)
					{
						//Somesh: 06Aug08: dataSubM used to be called datatmp.
					    padC.dataSubM [idata] [iseg - nextstroke] = padC.dataSubM [idata] [iseg];
						if ( iseg % nextstroke == 0)
						padC.data [idata] [int(iseg/nextstroke)-1] = padC.data [idata] [int(iseg/nextstroke)];
					}
				}
				nseg--;
			}
		}

		/* Do this always for now */
		if (true)
		{
			/*****************************************/
			/* (3) Check number of remaining strokes */
			/*****************************************/
			/* Do this last */
			/* Too few strokes */
			/* The number of strokes may already be zero, though */
			/* but not due to #strokes set to zero */
			/* Need to check that there are real data */

			/* 20May03: Raji: Loops discarding the trials Streamlined*/
			/* To prevent the overwriting of this error jump without processing individual strokes*/
			/* Also we already know that it is not a good trial*/
			iseg = iseg1;
			/* Insufficient strokes */
			if (nseg < padC.nsegminlex [icond])
			{
				if (morethanmaxiserror)
				{
					err.Format( IDS_CON_ERR1,CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, nseg, padC.nsegminlex [icond]); //yi
					discardtrial = printandbuffer (err, errbuf);
					goto notest;
				}
				// 10Nov08: Somesh: If user switch "Discard if number of strokes out of range" is OFF, then earlier code issued a warning.
				// Now it has been made an error and the trial is discarded.
				// 15Jan09: GMB: We WANT this to be a warning when OFF. It should only be an error if it is set to ON.
				else
				{
					err.Format( IDS_CON_ERR2A, CON, CON_PERFORMANCE, CON_WARNING, CON_LOCATION, CON_MESSAGE, nseg, padC.nsegminlex [icond]); //yi
//					discardtrial = printandbuffer (err, errbuf);
//					goto notest;
					printandbuffer (err, errbuf);
				}
			}
			/* Too many strokes */
			else if(nseg > padC.nsegmaxlex [icond])
			{
				/* 29 Sept03: Raji: There was a mistake here, goto notest in the wrong loop*/
				/* If user switch "Discard if more strokes than required" is ON,then it is an error,Discard trial*/
				if (morethanmaxiserror)
				{
					err.Format( IDS_CON_ERR3, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, nseg, padC.nsegmaxlex [icond]); //yi
					discardtrial = printandbuffer (err, errbuf);
					goto notest;
				}
				/* If user switch "Discard if number of strokes out of range" is OFF, then issue a warning, Don't Discard trial*/
				else
				{
					err.Format( IDS_CON_ERR2B, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, nseg, padC.nsegmaxlex [icond]); //yi
				    discardtrial = false;
				}
			    /* 20May03: hlt: At any rate: Reduce the analysis to the min (was max) number of strokes */
				/* TODO You could include an option to leave as many strokes as needed: Bad option */
				nseg = padC.nsegminlex [icond];
			}
			/* Number of strokes within range; but reduce to the minimum number to have equal trials */
			else
			{
				/* 20May03: hlt: Reduce the analysis to the min number of strokes */
				/* TODO You could include an option to leave as many strokes as needed: Bad option */
				nseg = padC.nsegminlex [icond];
			}

		}

		/********************/
		/* Size limitations */
		/********************/
		if (padC.lenrange [icond] > 0.)
		{

			/* 08 May 03: raji: replaced by the following block hlt: 22 May 00*/
			/* And now we dont need a part of it within #if0 #endif that is for secondary submovement*/
			/* As in any case all the strokes are processed similarly */
			/* Absolute sizes specified */
			for (iseg = iseg1; iseg < nseg; iseg++)
			{
				/***********/
				/* Testing */
				/***********/
				if ( fabs (fabs(padC.data [DELTAY] [iseg]) - padC.lentarget [icond]) > padC.lenrange [icond])
				{
					err.Format( IDS_CON_ERR4, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, icond+1, condcodearg, wordlex [icond],
								iseg+1, padC.data [DELTAY] [iseg], padC.lentarget [icond],fabs(padC.data [DELTAY] [iseg]) - padC.lentarget [icond], padC.lenrange [icond]); //yi
					discardtrial = printandbuffer (err, errbuf);
					/*02Feb04: Raji: need to exit the loop after finding the first error*/
					goto notest;
				}
			}
		}
		/* No absolute sizes specified */
		/* Absolute (=across whole pattern) or relative (=only between successive pairs) comparisons */
		else if (consistencytest & SIZES)
		{
			/****************************************/
			/* (4) Recognize long and short strokes */
			/****************************************/
			/* Match measured and expected pattern */

			if (absolute)
			{
				/*****************************/
				/* (4a) Absolute comparisons */
				/*****************************/
				/* are less appropriate in very micrographic writing */
				/***********/
				/* Scoring */
				/***********/
				/* Initialize per trial */
				minlong = +BIG;
				maxshort = -BIG;
				/* Check only the first nsegpattern segments */
				for (iseg = iseg1; iseg < MIN (nseg, nsegpattern); iseg++)
				{
					/* Get the smallest long stroke */
					if (abs (padC.strokelen [iseg]) == LONG)
					{
						if (minlong > fabs ((double) padC.data [DELTAY] [iseg]))
						{
							minlong = fabs ((double) padC.data [DELTAY] [iseg]);
							minlongstroke = iseg;
						}
					}
					/* Get the largest short stroke */
					else if (abs (padC.strokelen [iseg]) == SHORT)
					{
						if (maxshort < fabs ((double) padC.data [DELTAY] [iseg]))
						{
							maxshort = fabs ((double) padC.data [DELTAY] [iseg]);
							maxshortstroke = iseg;
						}
					}
				}

				/***********/
				/* Testing */
				/***********/
				if (minlong <= maxshort)
				{
					err.Format( IDS_CON_ERR5,CON, CON_PERFORMANCE, CON_ERROR, CON_MESSAGE, minlongstroke+1, strokeid [minlongstroke], minlong, maxshortstroke+1, strokeid [maxshortstroke], maxshort); //yi
					discardtrial = printandbuffer (err, errbuf);
					/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
					goto notest;
				}
			}
			else
			{
				/*************************************/
				/* (4b) Relative scoring and testing */
				/*************************************/
				//07Nov08: Somesh: Added nextstroke to initialization and loop counter
				for (iseg = iseg1 + nextstroke; iseg < MIN (nseg, nsegpattern); iseg+=nextstroke)
				{
					if (abs (padC.strokelen [iseg - 1]) != SHORTLONG &&
						abs (padC.strokelen [iseg    ]) != SHORTLONG &&
						(abs (padC.strokelen [iseg - 1]) -
						 abs (padC.strokelen [iseg    ])) *
						 (fabs ((double) padC.data [DELTAY] [iseg - 1]) -
						 fabs ((double) padC.data [DELTAY] [iseg])) < 0.)
					{
						err.Format( IDS_CON_ERR6, CON, CON_PERFORMANCE, CON_ERROR, CON_MESSAGE,
									iseg - 1+1, strokeid [iseg - 1],
									padC.strokelen [iseg - 1], padC.data [DELTAY] [iseg - 1],
									iseg+1, strokeid [iseg], padC.strokelen [iseg],
									padC.data [DELTAY] [iseg]); //yi
						discardtrial = printandbuffer (err, errbuf);
						/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
						goto notest;
					}
				}
			}
		}

		/* Special case of length testing: Spiral */
		if (consistencytest & SPIRAL)
		{
			/*******************************************************/
			/* (2) Cut strokes after nonincrease in outward spiral */
			/*******************************************************/
			for (iseg = iseg1; iseg < nseg; iseg++)
			{
				double dummy1 = fabs (padC.data [DELTAY] [iseg - 1]);
				double dummy2 = fabs (padC.data [DELTAY] [iseg] * spiralincreasefactor);

				if (fabs (padC.data [DELTAY] [iseg - 1]) >
					fabs (padC.data [DELTAY] [iseg] * spiralincreasefactor))
				{
					// 26Mar2010: Somesh: Was szMsg.Format
					err.Format( IDS_CON_ERR7, CON, CON_WARNING, CON_MESSAGE,
								  icond+1,condcodearg, wordlex [icond],
								  iseg-1, padC.data [DELTAY] [iseg-1],
								  iseg, padC.data [DELTAY] [iseg], spiralincreasefactor,
								  nseg, iseg); //yi
					//OutputMessage( szMsg, LOG_PROC );
					discardtrial = printandbuffer(err, errbuf);
					/* Cut trial */
					nseg = iseg;
					goto notest;
				}
			}
		}

		if (padC.dirrange [icond] > 0.)
		{
			/* Absolute directions specified */
			for (iseg = iseg1; iseg < nseg; iseg++)
			{
				/***********/
				/* Testing */
				/***********/
#define ANGLEDATA 0
#define ANGLEMIN 1
#define ANGLEMAX 2
				angle [ANGLEDATA] = padC.data [SLANT] [iseg];
				/* 08Oct03: Raji: Check for the actual range specified instead of including a 0.5 factor*/
				angle [ANGLEMIN] = padC.dirtarget [icond] - padC.dirrange [icond];
				angle [ANGLEMAX] = padC.dirtarget [icond] + padC.dirrange [icond];

				// Somesh: 08Aug08: Check if angle +- 2*PI is within boundaries
				if ( (	angle [ANGLEDATA]		 < angle [ANGLEMIN] ||
						angle [ANGLEDATA]		 > angle [ANGLEMAX]	) &&
					 (	angle [ANGLEDATA] + 2*PI < angle [ANGLEMIN] ||
						angle [ANGLEDATA] + 2*PI > angle [ANGLEMAX]	) &&
					 (	angle [ANGLEDATA] - 2*PI < angle [ANGLEMIN] ||
						angle [ANGLEDATA] - 2*PI > angle [ANGLEMAX]	)
					)

				{
					err.Format( IDS_CON_ERR8, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,
								icond+1, condcodearg, wordlex [icond],
								iseg+1, padC.data [SLANT] [iseg],
								padC.dirtarget [icond], padC.dirrange [icond]); //yi
					discardtrial = printandbuffer (err, errbuf);
					/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
					goto notest;
				}
			}
		}
		/* No absolute directions specified */
		/* Absolute (=across whole pattern) or relative (=only between successive pairs) comparisons */
		else if (consistencytest & ORIENTATION)
		{
			/***************************************/
			/* (6) Orientation of straight strokes */
			/***************************************/

			///* Test all strokes */
			//for (iseg = iseg1; iseg < nseg; iseg++)
			/* Test only the segments specified in the lexical pattern */
			for (iseg = iseg1; iseg < MIN (nseg, nsegpattern); iseg++)
			{
                // We test now the direction and not only the orientation
                // We want this eventually optional.
				if (fabs (padC.data [SLANT] [iseg] - targetslant [iseg] ) > slanterror &&
					fabs (padC.data [SLANT] [iseg] - targetslant [iseg] - 2. * PI) > slanterror &&
					fabs (padC.data [SLANT] [iseg] - targetslant[iseg]+ 2. * PI) > slanterror)
				{
					err.Format( IDS_CON_ERR9, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,
								icond+1,condcodearg, wordlex [icond],iseg+1, padC.data [SLANT] [iseg], targetslant [iseg], slanterror); //yi
					discardtrial = printandbuffer (err, errbuf);
					/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
					goto notest;
				}
			}
		}

		/* Absolute (=across whole pattern) or relative (=only between successive pairs) comparisons */
		if (consistencytest & LOOPS)
		{
			/********************************************/
			/* (5) Loops clockwise and counterclockwise */
			/********************************************/
			/* Match measured and expected pattern */
			/* Also descarded patterns will be checked */

			/* Not yet implemented: compare only equal sized loops */
			/* Relative testing */

			/***********/
			/* Scoring */
			/***********/
			/* Initialize per trial */
			minposloop = +BIG;
			maxnegloop = -BIG;
			minsharp   = -minlooparean;
			maxsharp   = +minlooparean;
			maxnoloop  = +minlooparean;
			/* Check only the first nsegpattern segments */
			for (iseg = iseg1; iseg < MIN(nseg, nsegpattern); iseg++)
			{
				/* Get the clockwise loop */
				if (padC.strokeloop [iseg] == +LOOP)
				{
					if (minposloop > padC.data [SURFACE] [iseg])
					{
						minposloop = padC.data [SURFACE] [iseg];
						int value = SURFACE;
						minposloopstroke = iseg;
					}
				}
				/* Get the counter clockwise loop */
				else if (padC.strokeloop [iseg] == -LOOP)
				{
					if (maxnegloop < padC.data [SURFACE] [iseg])
					{
					maxnegloop = padC.data [SURFACE] [iseg];
					maxnegloopstroke = iseg;
					}
				}
				/* Get the range of sharp reversals */
				else if (padC.strokeloop [iseg] == SHARP)
				{
					if (minsharp > padC.data [SURFACE] [iseg])
					{
						minsharp = padC.data [SURFACE] [iseg];
						minsharpstroke = iseg;
					}
					if (maxsharp < padC.data [SURFACE] [iseg])
					{
						maxsharp = padC.data [SURFACE] [iseg];
						maxsharpstroke = iseg;
					}
				}
				else if (padC.strokeloop [iseg] == NOLOOP)
				{
					if (maxnoloop < fabs ((double) padC.data [SURFACE] [iseg]))
					{
						maxnoloop = fabs ((double) padC.data [SURFACE] [iseg]);
						maxnoloopstroke = iseg;
					}
				}
			/* else irrelevant loop */
			}

			/* Potential bug: ntrialout can be deducted more than once per trial */
			/* Solution: goto nextrial */

			/***********/
			/* Testing */
			/***********/
			/* Check for the first error to be found */
			/* otherwise ntrialout is reduced too often, hence elseif */
			/* Initial settings are such that no error conditions are satisfied */
			/* so no messages are generated if certain features do not occur */

			/* Negative loops (i.e., even max) should be negative */
			if (maxnegloop > (double) -minlooparean)
			{
				err.Format( IDS_CON_ERR10, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, maxnegloopstroke+1, strokeid [maxnegloopstroke],maxnegloop, -minlooparean); //yi
				discardtrial = printandbuffer (err, errbuf);
				/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
				goto notest;
			}

			/* Negative loops (i.e., even max) should still be less than the most negative sharp reversal */
			else if (minsharp < maxnegloop)
			{
				err.Format( IDS_CON_ERR11, CON, CON_PERFORMANCE, CON_ERROR, CON_MESSAGE,
							maxnegloopstroke+1, strokeid [maxnegloopstroke],
							maxnegloop, minsharpstroke+1, strokeid [minsharpstroke],
							minsharp); //yi
				discardtrial = printandbuffer (err, errbuf);
				/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
				goto notest;
			}

			/* Positive loops (i.e., even min) should be positive */
			else if (minposloop < (double) +minlooparean)
			{
				err.Format( IDS_CON_ERR12, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,
							minposloopstroke+1, strokeid [minposloopstroke],minposloop, +minlooparean); //yi
				discardtrial = printandbuffer (err, errbuf);
				/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
				goto notest;
			}

			/* The largest or most positive sharp reversal should be less than the smallest positive loop */
			else if (minposloop < maxsharp)
			{
				err.Format( IDS_CON_ERR13, CON, CON_PERFORMANCE, CON_ERROR, CON_MESSAGE,
							minposloopstroke+1, strokeid [minposloopstroke], minposloop,
							maxsharpstroke+1, strokeid [maxsharpstroke], maxsharp);//yi
				discardtrial = printandbuffer (err, errbuf);
				/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
				goto notest;
			}

			/* There should be no loop at all */
			else if (maxnoloop > (double) minlooparean)
			{
				err.Format( IDS_CON_ERR14, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,
							maxnoloopstroke+1, strokeid [maxnoloopstroke], maxnoloop, +minlooparean); //yi
				discardtrial = printandbuffer (err, errbuf);
				/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
				goto notest;
			}
		}


		if (consistencytest & STRAIGHTNESS)
		{
			/* Scoring & Testing */
			for (iseg = iseg1; iseg < nseg; iseg++)
			{
				if (padC.straightness [iseg] == STRAIGHT)
				{
					if (padC.data [STRAIGHTERR] [iseg] > straightcritstraight)
					{
						err.Format( IDS_CON_ERR15, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,
									icond+1, condcodearg, wordlex [icond],iseg+1, padC.data [STRAIGHTERR] [iseg], straightcritstraight); //yi
						discardtrial = printandbuffer (err, errbuf);
						/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
						goto notest;
					}
				}
				else if (padC.straightness [iseg] == CURVED)
				{
					if (padC.data [STRAIGHTERR] [iseg] < straightcritcurved)
					{
						err.Format( IDS_CON_ERR15, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,
									icond+1,condcodearg, wordlex [icond],iseg+1, padC.data [STRAIGHTERR] [iseg], straightcritcurved); //yi
						discardtrial = printandbuffer (err, errbuf);
						/*02Feb04: Raji: need to stop consistency testing after finding the first error*/
						goto notest;
					}
				}
			}
		}


		//Yi Sep.06-12.2006 for recognization of Circle and lll,and tell the difference between the two.
		// The ratio of relative distance is calculated by
		// ||f(x)|-|f(x+1)||/|f(x)|+|f(x+1)|, x is the horizontal size between the adjacent segmentation points

		// gmb: 18Oct06 - incorrect mix of int and bool -- nonconsistent logic
		// NOTE: there should be another bit mask for "constencytest" :: #define CIRCLE_LLL 128
		//		Above, where consistencytest is set per letter (or based upon a determination of the stroke pattern),
		//		consistencytest should be or'd with CIRCLE_LLL for use in the logic below when it is appropriate.
		//		Then, the appropriate statement below would be:
		//			if( ( consistencytest & CIRCLE_LLL ) & fCircleLLL )
//		if(consistencytest & fCircleLLL)
		/*if( ( consistencytest & CIRCLE_LLL ) & fCircleLLL )
		{
			for (iseg = iseg1; iseg < nseg; iseg++)
			{*/


//		if( ( consistencytest > 0 ) && fCircleLLL )	// this is not appropriate "logic" but is appropriate syntax
//		{
//
//			double counter_lll=0;
//			double counter_linkcircle=0;
//			double counter_linklll=0;
//			double counter_lll_2=0;
//
//			int error_o=0;
//			int error_l=0;
//
//
//			/* Scoring & Testing */
//			for (iseg = iseg1; iseg < nseg; iseg++)
//			{
//				//tell circle
//				if (!(strcmp(strokeid [iseg], "o1/2")))
//				{
//					//the rule for a single 'o'.
//					if (nseg<=5)
//					{
//						// ||f(x)|-|f(x+1)||/|f(x)|+|f(x+1)|, x is the horizontal size between the adjance segmentation points
//						horizontal_relative=fabs(padC.data [PENUP] [iseg]) + fabs(padC.data [PENUP] [iseg+1]);
//						horizontal_relative=1/horizontal_relative;
//						horizontal_relative=horizontal_relative*fabs(fabs(padC.data [PENUP] [iseg]) - fabs(padC.data [PENUP] [iseg+1]));
//						// for 'o', the ratio should be smaller than a threthold
//						if (horizontal_relative> dThldHorizRelCircle)
//							error_o=1;
//					}
//
//					//if it's an 'o' sequence like 'ooooooooo', look at the condition of sequence
//					if ((iseg>=2) && !(strcmp(strokeid [iseg-2], "o1/2")))
//					{
//						counter_linkcircle++;
//
//						if ((padC.data[DELTAX][iseg]>padC.data[DELTAX][iseg-2]) && (padC.data[DELTAX][iseg+1]>padC.data[DELTAX][iseg-1]))
//							counter_lll++;
//					}
//
//					//if the last two stork and not a single 'o',then ignore the error
//					if ((nseg>6) && (iseg>=nseg-3) && fCircleIgnoreLast)
//					{
//						error_o=0;
//					}
//					//if the first two stork, then ignore the error
//					if ((iseg==0) && fCircleIgnoreFirst)
//						error_o=0;
//
//				}
//
//
//				//tell l
//				if (!(strcmp(strokeid [iseg], "l1/2")))
//				{
//
//					//the horizontal size of odd number stroke should larger than even number stroke when they are in the opposite direction
//					if ((!(fabs(padC.data [PENUP] [iseg]) > fabs(padC.data [PENUP] [iseg+1]))) && (padC.data [PENUP] [iseg]*padC.data [PENUP] [iseg+1]<0 ))
//					{
//						error_l++;
//						//if the last two stork and not a single 'l',then ignore the error
//						if ((nseg>6) && (iseg>=nseg-3) && fLLLIgnoreLast)
//						{
//							error_l--;
//						}
//						//if the first two stork, then ignore the error
//						if ((iseg==0) && fLLLIgnoreFirst)
//						{
//							error_l--;
//						}
//					}
//
//
//					// ||f(x)|-|f(x+1)||/|f(x)|+|f(x+1)|, x is the horizontal size between the adjance segmentation points
//					horizontal_relative=fabs(padC.data [PENUP] [iseg]) + fabs(padC.data [PENUP] [iseg+1]);
//					horizontal_relative=1/horizontal_relative;
//					horizontal_relative=horizontal_relative*fabs(fabs(padC.data [PENUP] [iseg]) - fabs(padC.data [PENUP] [iseg+1]));
//					// for 'l', the ratio should be larger than a threthold
//					if (horizontal_relative< dThldHorizRelL)
//					{
//						error_l++;
//						//if the last two stork and not a single 'l',then ignore the error
//						if ((nseg>6) && (iseg>=nseg-3) && fLLLIgnoreLast)
//						{
//							error_l--;
//						}
//						//if the first two stork, then ignore the error
//						if ((iseg==0) && fLLLIgnoreFirst)
//						{
//							error_l--;
//						}
//					}
//
//					//if it's a 'l' sequence like 'llllllll', look at the condition of sequence
//					if ((iseg>=2) && !(strcmp(strokeid [iseg-2], "l1/2")))
//					{
//						counter_linklll++;
//
//						if ((padC.data[DELTAX][iseg]>padC.data[DELTAX][iseg-2]) && (padC.data[DELTAX][iseg+1]>padC.data[DELTAX][iseg-1]))
//							counter_lll_2++;
//					}
//
//				}
//
//			}//end of for (iseg = iseg1; iseg < nseg; iseg++)
//
//			//detection of the wrong sequence 'o', like 'oooooooo'
//			if (counter_linkcircle!=0)
//			{
//				if ((((counter_linkcircle-counter_lll)*1.0/counter_linkcircle)<=1/8)  &&(counter_linkcircle!=0) && (counter_lll!=0))
//				{
//					double circle_sequence_ratio=(counter_linkcircle-counter_lll)*1.0/counter_linkcircle;
//					err.Format( IDS_CON_ERR16, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, icond+1, condcodearg, wordlex [icond],iseg+1, circle_sequence_ratio, dThldRatioCircleSequence); //yi
//					discardtrial = printandbuffer (err, errbuf);
//					// need to stop consistency testing after finding the first error
//					//TODO: hlt 24mar08: Get rid of all these goto notest; things!
//					goto notest;
//				}
//			}
//
//			//detection of 'o'
//			if (error_o>0)
//			{
//				err.Format( IDS_CON_ERR17, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, icond+1, condcodearg, wordlex [icond],iseg+1, horizontal_relative, dThldHorizRelCircle); //yi
//				discardtrial = printandbuffer (err, errbuf);
//				// need to stop consistency testing after finding the first error
//				goto notest;
//			}
//
//			//detection of the wrong squence 'l', like 'llllllll'
//			//rejection if most l are wrong in the sequence
//			if (counter_linklll!=0)
//			{
//				if ( (((counter_linklll-counter_lll_2)*1.0/counter_linklll)>1/4) && (counter_linklll!=0) && (counter_lll_2!=0) )
//				{
//					double l_sequence_ratio=(counter_linklll-counter_lll_2)*1.0/counter_linklll;
//					err.Format( IDS_CON_ERR18, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, icond+1,condcodearg, wordlex [icond],iseg+1, l_sequence_ratio,dThldRatioLSequence); //yi
//					discardtrial = printandbuffer (err, errbuf);
//					// need to stop consistency testing after finding the first error
//					goto notest;
//				}
//			}
//
//			//not rejection if it's a sequence l and the error is not much
//			if ((error_l>0 && error_l<=3) && (counter_linklll!=0))
//			{
//				if ((((counter_linklll-counter_lll_2)*1.0/counter_linklll)<1/8)  &&(counter_linklll!=0) && (counter_lll_2!=0))
//				{
//					;
//				}
//			}
//			//rejection of 'l'
//			else if (error_l>0)
//			{
//				err.Format( IDS_CON_ERR19, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE,icond+1, condcodearg, wordlex [icond],iseg+1, SetThresholdHorizRelL, horizontal_relative); //yi
//				discardtrial = printandbuffer (err, errbuf);
//				// need to stop consistency testing after finding the first error
//				goto notest;
//			}
//		}//end of if (consistencytest & fCircleLLL)
//

notest:;

		/***************************/
		/* (Finally) Discard trial */
		/***************************/
		if (discardtrial)
		{
			/* Discard trial by making the number of segments 0 */
			nseg = 0;
			ntrialdiscarded++;
		}

		/*******************/
		/* Or accept trial */
		/*******************/
		else
		{
			/* 2 Jun 03 Raji: nseg now has the number of segments processed, which is lesser in secondary analysis*/
			/* The original number of strokes should be restored while writing the file*/
            /* Use the input array datatmp[][] array for output instead of using data[][] array*/
			/* which stores segments to be processed*/
			/* changes in every line marked as %%%%%%%%%*/
			nseg = nseg * nextstroke;
			szMsg.Format( IDS_CON_GOOD, CON_PROCESSING, CON_MESSAGE, itrial, itrial-1); //yi
			OutputMessage( szMsg, RGB( 0, 255, 0 ) );
			if( fLastOnly ) *fPassed = true;
			else *fPassed |= true;

			/************************/
			/* Output current trial */
			/************************/

		    for (iseg = iseg1; iseg < nseg; iseg++)
			{
				for (idata = 0; idata < ndatamax; idata++)
				{
					if( idata == 0 )
					{
						szMsg.Format( _T("%g"), padC.dataSubM [idata] [iseg]);
						fprintf( fCon, szMsg );
					}
					else
					{
						szMsg.Format( _T(" %g"), padC.dataSubM [idata] [iseg]);
						fprintf( fCon, szMsg );
					}
				}
				fprintf( fCon, _T("\n") );
			}
			/* Write ok if trial is all ok */
			err = _T("OK");
			discardtrial = printandbuffer (err, errbuf);
		}
		/**************************/
		/* Feedback of discarding */
		/**************************/
		discardtrial = printandbuffer( err, errbuf, true );
		fErr.WriteString(err1);
	}//end if while(nextTrial)

Finished:

	/* Summary info at the end */
	szMsg.Format( IDS_CON_SUMMARY, CON, CON_SUMMARY, CON_MESSAGE, szExtIn, ntrialin, ntrialmax, szErrOut, ntrialdiscarded,
				  100.* ntrialdiscarded / (MAX (ntrialin,1)), ntrialin - ntrialdiscarded); //yi
	OutputMessage( szMsg, LOG_PROC );

Cleanup:

	/* Close files */
	if( fExt.m_pStream ) fExt.Close();
	if( fCon ) fclose( fCon );
	if( fErr.m_pStream ) fErr.Close();

	return fRet;
}

/************************************************/
bool printandbuffer (CString szErr, CString szErrbuf, bool toErr)
/************************************************/
/* 19 May 03: raji: The function now returns true or false depending on whether the trial has to be discarded or not*/
/* If there is an error message then the trial has always to be discarded*/
{
	/* No need of a semicolon in error file*/
	szErrbuf += szErr;

	// eliminate new line
	int nLen = szErrbuf.GetLength();
	if( nLen <= 0 ) return false;
	if( szErrbuf[ nLen - 1 ] == '\n' )
		szErrbuf = szErrbuf.Left( nLen - 1 );

	if( !toErr )
		OutputMessage( szErrbuf, LOG_PROC );
	szErrbuf += _T("\n");
	if( toErr && _pErr && _pErr->m_pStream )
		_pErr->WriteString( szErrbuf );
	return true;
}

#pragma warning( default: 4996 )

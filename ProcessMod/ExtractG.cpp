/*************************************************
   ExtractG - version 2.6 - hlt/ka - 10 Mar 1995
**************************************************
   Copyright 1995 Teulings
		2
ARGUMENTS: infile_timefunctions infile_segments output_append_file trialnr

PURPOSES:
	(1) Extracts features

UPDATES:
  27Apr94: Segmentation data now in seconds
  19Aug94: File names in header; Write header only if creating file.
  26Jan95: New features: padEG->startx, padEG->deltax, rnprsmin, padEG->slant, padEG->surface, padEG->t1loop, padEG->t2loop
  14Feb95: Revised and tested ok; Bugs due to zero intervals solved.
  23Feb94: V features added
  31Mar95: Drop T1Loop and T2Loop; Include Vypeak and Rtvypeak (relative);
  19Apr95: Added padEG->ndecel, padEG->ayeff
   2May95: Reading also Jerk. Jabsq overrules Yefficiency. padEG->starty added.
   5May95: Output format was %7.3f becomes %8.4g
  16May95: Normalize StraightErr and JabsQ but not NDecel
  26Jun95: Bug in normalized jabs: Integrated across first and second interval.
           Loop search speedded up: first 0 or last loop; now
             two strokes back or last loop whatever is closest.
  21Jul95: Jabs factor 1/2 included.
  13Jan96: Relative Jabs by Jabs-of-phase1/Jabs-total
   9Apr97: Relative phase1/total: DeltaT, DeltaX, DeltaY. Variable names updated: DeltaTR, etc.
   2Jul97: Arbitrary alphanumeric args for trialindex; /s moves to arg4.
   7Jul97: Slant during first 80 msp; Slant80 instead of padEG->ndecel
  16Jul97: MIN/MAXLOOPTIME in seconds instead of sample nrs
  21Jul97: MaxLoop time from 2.5 to 5s, minlooptime from 0.02 to 0.05
           Start loop search not 2 strokes back but only one
  15Dec97: Onestroke analysis included
  20Dec97: Startt instead of padEG->startx
  21Apr98: Startx instead of padEG->startt
  16Sep98: Included beta from timfun to correct angles for rotation.
  28Sep98: Vypeak replaced by Slant80, Slant80 replaced by StartT
  20Nov98: Insert empty stroke if empty data
  24Nov98: Looparea was NOT normalized.
  28Jan99: PeakVy instead of StartX
  20Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		   removal of statics
	18May00: Replaced rpenup by startx. Did NOT upte the header line.
	 5Jun00: Prevent values (except startt,x,y) when deltat=0
	21Jun01: hlt: Disabled normlization of looparea feature
	26Jul01: hlt: Appended total stroke data summarizing the substrokes (quick solution)
	12Jun03 Raji : Added New parameters: excerpted from gripnew\extract.c
	9Oct03: Raji: Changed the header message in processmod.rc to get new expanded headings
	18Sep06: GMB: Converted deprecated string/file functions for MSVS7
	09Nov06: Yi: Add more output and change the header message in processmod.rc to output more titles
	17Aug07: GMB: Added segmentation verification

FUTURE EXTENSIONS:
   Change: data [STARTT][], ...  instead of padEG->startt [], ...


************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"
#include "../Common/ProcSettings.h"
#include "../Common/DefFun.h"
#include "../Common/siglib.h"


// 02Sep10: Somesh: Was 1700
#define NSMAXG 3300 /*(4096/1.25)*/ // 1400 abnormal end; 1700 too much global data
#define NSEGMAXG 60
// 02Sep10: Somesh: Was 2048
#define NSMAXGTIMFUN 4096
#define NFREQMAXG NSMAXGTIMFUN / 2
#define ROUNDUP 0.999
#define NODECIMATE 1
#define INITIALPART 0.080 /* in s */
#define INDEXARGS 4
#define MINLOOPAREA 0.0001 /* In cm2 to get rid of quantization and noise errors */

#define	MODULE		_T("GRIPPEREXTRACT")

struct _ArrayDataEG
{
	double tseg [NSEGMAXG];
	// 26Mar08 hlt
	double tsegsecondary [NSEGMAXG];
	double startt [NSEGMAXG];
	double startx [NSEGMAXG];
	double starty [NSEGMAXG];
	double startz [NSEGMAXG];
	double deltat [NSEGMAXG];
	double deltax [NSEGMAXG];
	double deltay [NSEGMAXG];
	double deltaz [NSEGMAXG];
	double deltas [NSEGMAXG];
/* 12 Jun 03 Raji: The following definitions are added for new parameters' calculation*/
	double deltasxy [NSEGMAXG];
  	double deltasxz [NSEGMAXG];
  	double deltasyz [NSEGMAXG];
  	double deltas3 [NSEGMAXG];
  	double rxy [NSEGMAXG];
	double rxz [NSEGMAXG];
	double ryz [NSEGMAXG];
	double rvxvy [NSEGMAXG];
	double rvxvz [NSEGMAXG];
	double rvyvz [NSEGMAXG];
	double bxy [NSEGMAXG];
	double bxz [NSEGMAXG];
	double byz [NSEGMAXG];
	double straighterrxy [NSEGMAXG];
  	double straighterrxz [NSEGMAXG];
 	double straighterryz [NSEGMAXG];

/* 12 Jun 03 Raji: The following definitions are added for new parameters' calculation*/
	double straighterr [NSEGMAXG];
	double straighterr3 [NSEGMAXG];
	//29Oct08: Somesh: Not needed for now
	/*double slant [NSEGMAXG];
	double slant2 [NSEGMAXG];
	double slant80 [NSEGMAXG];*/

	// 10Nov06: Yi: These 3 are not needed for output
	//double surface [NSEGMAXG];  /* LoopArea */
	//double t1loop [NSEGMAXG];
	//double t2loop [NSEGMAXG];

	// 29Oct08: Somesh: New parameters added
	double meanx [NSEGMAXG]; double sdx [NSEGMAXG];
	double meany [NSEGMAXG]; double sdy [NSEGMAXG];
	double meanz [NSEGMAXG]; double sdz [NSEGMAXG];

	double minx [NSEGMAXG]; double maxx [NSEGMAXG];
	double miny [NSEGMAXG]; double maxy [NSEGMAXG];
	double minz [NSEGMAXG]; double maxz [NSEGMAXG];

};

double loopareag (double* x, double* y, int ifirst, int ilast,
				 double *t1loop, double *t2loop, double sec);
/* Hack for 3d straightness */
double rmsdist3g (double* x, double* y, double* z, int n, double a,
				 double b, double az, double bz);

/*************************************************************************/
bool ExtractG( CString szTFIn, CString szSegIn, CString szExtOut, unsigned int nSwitch, unsigned int nTask )
/*************************************************************************/
{
	bool fRet = true;
	CString szMsg, txt;
	char szOpenMode[ 2 ];

	double *x = NULL, *y = NULL, *z = NULL;
	double *vx = NULL, *vy = NULL, *vz = NULL;/*%%%%%%%%%%%%%%*/

	int endoflastloop = 0, i = 0;
	int nArgCount = 1; // TODO: Trial/group/part/exp...etc.
	int ns = 0, nsy = 0, nsz = 0;
	int nsvx = 0, nsvy = 0, nsvz = 0;/*%%%%%%%%%%%%%%*/

	//26Mar08 hlt
	int istroke = 0, nstroke = 0, nstrokesecondary = 0, nstrokeprocess = 0;
	bool secondary = false;

	int isam1 = 0, isam2 = 0;
	CFileFind ff;
	FILE *fTF = NULL, *fSeg = NULL, *fExt = NULL;
	double a = 0.0, b = 0.0, r = 0.0, w = 0.0;
	double a2 = 0.0, b2 = 0.0, r2 = 0.0;
	double sec = 0.0;
	int nsec = 0;
	int nprsmin = 0, nbeta = 0;			// not used
	double prsmin = 0., beta = 0.;		// ""
	bool create = false;
	int iarg = 0;
	double maxxtemp = -99999.9, maxytemp = -99999.9, maxztemp = 99999.9;
	int largestForceChan = 0;
	double curseg = 0.0;


#define TOTAL_FEATURES		38
	double* pFeatures = new double[ TOTAL_FEATURES ];

	_ArrayDataEG* padEG = new _ArrayDataEG;
	if( !padEG )
	{
		szMsg.Format( IDS_EXT_MEM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		return false;
	}
	memset( (LPVOID)padEG, 0, sizeof( _ArrayDataEG ) );

	/*****************/
	/*** Switches  ***/
	/*****************/


	/*************************/
	/*** Open segment file ***/
	/*************************/
	if( ( fopen_s( &fSeg, szSegIn, "r" ) ) != 0 )
	{
		szMsg.Format( IDS_EXT_SEGIN, MODULE, szSegIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	szMsg.Format( IDS_EXT_SEGREAD, MODULE, szSegIn );
	OutputMessage( szMsg, LOG_PROC );
	/* no. of strokes is one less than no. of segmentations */

	/*****************************/
	/* Input segmentation points */
	/*****************************/

	/* Normal ballistic strokes are default */
	/* Subtract one */
	nstroke = -1 + getdata (fSeg, padEG->tseg, NSEGMAXG, txt, NODECIMATE);


//26Mar08 hlt
	/* 22Sep03: HLT; Test in case there are no strokes, e.g., one or no segmentation points */
	if (nstroke <= 0)
	{
		szMsg.Format( "%s: WARNING; No strokes", MODULE );
		OutputMessage( szMsg, LOG_PROC );

		/* Output at least one stroke which will have zero duration */
		nstroke = 1;

		/* Forget about secondary submovement analysis */
		secondary = false;
	}


// Needed for repetitive pressure task
#if 0
	secondary = true;
	/* Secondary stroke analysis */
	if (secondary)
	{
		/* Inside each stroke there should be a segmentation between primary and secondary substrokes */
		nstrokesecondary = getdata (fSeg, padEG->tsegsecondary, NSEGMAXG, txt, NODECIMATE);
		/* If it turns out that there are no substrokes, set Secondary OFF */
		if (nstrokesecondary == 0)
		{
			szMsg.Format( IDS_EXTSEC_1, MODULE );
			OutputMessage( szMsg, LOG_PROC );
			secondary = false;
		}
		/* If there are substrokes but due to ??? fewer than strokes, set Secondary OFF */
		else if (nstrokesecondary != nstroke)
		{
			szMsg.Format( IDS_EXTSEC_2, MODULE );
			OutputMessage( szMsg, LOG_PROC );
			secondary = false;
		}
		/* 02Feb04: Raji: Dont switch secondary off, atleast process till NSEGMAXG*/
		/* If there are so many strokes that there is no space to zip in the substroke segmentations */
		/* Set Secondary OFF */
		/*else if (3 * nstroke >= NSEGMAXG)
		{
			szMsg.Format( IDS_EXTSEC_3, MODULE, NSEGMAXG );
			OutputMessage( szMsg, LOG_PROC );
			secondary = false;
		}
		*/
	}

#endif

	fclose( fSeg );
	fSeg = NULL;

	/************************/
	/*** Open timfun file ***/
	/************************/
	if( ( fopen_s( &fTF, szTFIn, "r" ) ) != 0 )
	{
		szMsg.Format( IDS_EXT_TFIN, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

    /*******************************/
    /* Input heading of timfun file */
    /*******************************/
	/* Read data */
	szMsg.Format( IDS_EXT_TFREAD, MODULE, szTFIn );
	OutputMessage( szMsg, LOG_PROC );
	nsec = getdata (fTF, &sec, 1, txt, NODECIMATE);
	nprsmin = getdata (fTF, &prsmin, 1, txt, NODECIMATE);
	nbeta = getdata (fTF, &beta, 1, txt, NODECIMATE);

	/* Test */
	if (nsec != 1)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/*******************************/
	/* Convert segmentation points */
	/*******************************/
	/* Including last stroke */
	for (istroke = 0; istroke <= nstroke; istroke++)
	{
		padEG->tseg [istroke] /= sec;
	}

	/*****************/
	/* Input x, y, z */
	/*****************/
	/* Allocate */
	x = new double[ NSMAXG ];
	y = new double[ NSMAXG ];
	z = new double[ NSMAXG ];
	/* 12Jun03 read the vx, vy, vz to plot in the velocity domain*/
	vx = new double[ NSMAXG ];
	vy = new double[ NSMAXG ];
	vz = new double[ NSMAXG ];

	if( !x || !y || !z || !vx || !vy || !vz )
	{
		szMsg.Format( IDS_EXT_MEM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/* Read */
	ns = getdata (fTF, x, NSMAXG, txt, NODECIMATE);
	nsy = getdata (fTF, y, NSMAXG, txt, NODECIMATE);
	nsz = getdata (fTF, z, NSMAXG, txt, NODECIMATE);
	/* 26Mar08 hlt */
	// Skip 2 spectra plus filter spectrum
	// x, y, z spectrum
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	// x,y,z filtered spectrum
	// 05Nov08: Somesh: Incorrect number of data segments were being skipped
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	//vx, vy, vz
	nsvx = getdata (fTF, vx, NSMAXG, txt, NODECIMATE);
	nsvy = getdata (fTF, vy, NSMAXG, txt, NODECIMATE);
	nsvz = getdata (fTF, vz, NSMAXG, txt, NODECIMATE);

	/* Test (also done in segmen*/
	/* 28Dec04: Raji: If the data file is empty, gives a crash in Extract*/
	/* Now instead, the seg file is not created, and error written to results file*/
	if( (ns == 0) || (nsy == 0) || (nsz == 0))
	{
		szMsg.Format( IDS_SEG_TFERR1, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/* Test */
	if( (nsy != ns) || (nsz != ns))
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/*********************************************/
	/* Intermezzo: Test padEG->tseg[] input data */
	/*********************************************/
	// 17Aug07: GMB: Added improved segmentation validation
	curseg = padEG->tseg[ 0 ];
	if( curseg < 0. ) curseg = 0.;
	if( curseg > ns ) curseg = ns;
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		if (padEG->tseg [istroke] > ns)
		{
			szMsg.Format( IDS_EXT_INTMZZO, MODULE, szSegIn, istroke + 1,
						  padEG->tseg [istroke], ns, nstroke, istroke);
			OutputMessage( szMsg, LOG_PROC );
			/* One less than nr of segmentation points */
			nstroke = istroke - 1;
			break;
		}
		if( padEG->tseg[ istroke ] < curseg )
			padEG->tseg[ istroke ] = curseg;
		curseg = padEG->tseg[ istroke ];
	}

	if( nstroke > NSEGMAXG ) nstroke = NSEGMAXG;

    /****************/
	/* Process x, y */
    /****************/
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		/********/
		/* Time */
		/********/
		padEG->startt [istroke] = padEG->tseg [istroke] * sec;

		/*******************/
		/* Stroke duration */
		/*******************/
		padEG->deltat [istroke] = (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) * sec;

		/*************/
		/* Positions */
		/*************/
		padEG->startx [istroke] = x [(int) padEG->tseg [istroke]];
		padEG->starty [istroke] = y [(int) padEG->tseg [istroke]];
		//23mar08 hlt; Error that y was used instead of z for startz
		padEG->startz [istroke] = z [(int) padEG->tseg [istroke]];

		/*********/
		/* Sizes */
		/*********/
		/* Needed for padEG->deltas which is needed for normalizing straightness, padEG->jabsq, ... */
		/* Deltax and y are overwritten if relative primary movement is needed */
		padEG->deltax [istroke] = x [(int) padEG->tseg [istroke + 1]] - x [(int) padEG->tseg [istroke]];
		padEG->deltay [istroke] = y [(int) padEG->tseg [istroke + 1]] - y [(int) padEG->tseg [istroke]];
		padEG->deltaz [istroke] = z [(int) padEG->tseg [istroke + 1]] - z [(int) padEG->tseg [istroke]];

		/*****************/
		/* Absolute size */
		/*****************/
		/* 12Jun03 Raji : New parameters: excerpted from gripnew\extract.c*/
		padEG->deltasxy [istroke] = sqrt (SQR (padEG->deltax [istroke]) + SQR (padEG->deltay [istroke]));
		padEG->deltasxz [istroke] = sqrt (SQR (padEG->deltax [istroke]) + SQR (padEG->deltaz [istroke]));
		padEG->deltasyz [istroke] = sqrt (SQR (padEG->deltay [istroke]) + SQR (padEG->deltaz [istroke]));
		padEG->deltas3 [istroke] = sqrt (SQR (padEG->deltax [istroke]) + SQR (padEG->deltay [istroke]) + SQR (padEG->deltaz [istroke]));


		/************************/
		/* Area under a segment */
		/************************/
		// 10Nov06: Yi: Calculate area under a segment.
		// Starting at tseg [istroke] till tseg [istroke + 1], add all positive force values in the z * sec: PositiveAreaZ in Ns
		// Starting at tseg [istroke] till tseg [istroke + 1], add all negative force values in the z * sec: NegativeAreaZ in Ns


		/**************/
		/* Forces     */
		/**************/
		// 10Nov06: Yi: Calculate Maximum and minimum and average and sd forces per segment and component
		// Starting at tseg [istroke] till tseg [istroke + 1], find the highest force: MaxX in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the highest force: MaxY in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the highest force: MaxZ in N

		// Starting at tseg [istroke] till tseg [istroke + 1], find the lowest force: MinX in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the lowest force: MinY in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the lowest force: MinZ in N

		// See if you can use gemx() to calculate mean and sd
		// Starting at tseg [istroke] till tseg [istroke + 1], find the average force: MeanX in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the average force: MeanY in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the average force: MeanZ in N

		// Starting at tseg [istroke] till tseg [istroke + 1], find the sd force: SdX in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the sd force: SdY in N
		// Starting at tseg [istroke] till tseg [istroke + 1], find the sd force: SdZ in N

		// Signal-to-Noise Ratio and Coefficient of variation
		// Calculate per segment and component: Signal-to-Noice Ratio (SNR) Mean/sd or if sd=0, then 0 for x: SNRX
		// Calculate per segment and component: Signal-to-Noice Ratio (SNR) Mean/sd or if sd=0, then 0 for y: SNRY
		// Calculate per segment and component: Signal-to-Noice Ratio (SNR) Mean/sd or if sd=0, then 0 for z: SNRZ
		// Calculate per segment and component: Coefficient of variation (CV) sd/Mean or if mean=0, then 0 for x: CVX.
		// Calculate per segment and component: Coefficient of variation (CV) sd/Mean or if mean=0, then 0 for y: CVY.
		// Calculate per segment and component: Coefficient of variation (CV) sd/Mean or if mean=0, then 0 for z: CVZ.


		// Lags between zx and zy and xy (Phase3)
		// tseg [istroke] - tsegx [istroke]: LagZX in s  EXAMPLE: 1 s means that the pulling force starts 1 s after the x force started (In Segment 2)
		// tseg [istroke] - tsegy [istroke]; Lag ZY in s
		// tsegx [istroke] - tsegy [istroke]: LagXY in s


		/*********************************/
		/* Straightness error normalized */
		/*********************************/
		isam1 = (int)(padEG->tseg [istroke] + ROUNDUP);
		isam2 = (int)(padEG->tseg [istroke + 1] + ROUNDUP);
		w = 1.;

		/* RMS distance is independent of stroke duration */
		padEG->straighterrxy [istroke] = (double) rregr (&x[isam1], &y[isam1], isam2 - isam1, w, &a, &b, &r);
		padEG->bxy [istroke] = b;
		padEG->rxy [istroke] = r;
		padEG->straighterrxz [istroke] = (double) rregr (&x[isam1], &z[isam1], isam2 - isam1, w, &a, &b, &r);
		padEG->bxz [istroke] = b;
		padEG->rxz [istroke] = r;
		padEG->straighterryz [istroke] = (double) rregr (&y[isam1], &z[isam1], isam2 - isam1, w, &a, &b, &r);
		padEG->byz [istroke] = b;
		padEG->ryz [istroke] = r;
		/* Overwrite the Straighterr values to get them in the vx, vy and vz domains for analysis purposes*/
		padEG->straighterrxy [istroke] = (double) rregr (&vx[isam1], &vy[isam1], isam2 - isam1, w, &a, &b, &r);
	    padEG->rvxvy [istroke] = r;
		padEG->straighterrxz [istroke] = (double) rregr (&vx[isam1], &vz[isam1], isam2 - isam1, w, &a, &b, &r);
		padEG->rvxvz [istroke] = r;
		padEG->straighterryz [istroke] = (double) rregr (&vy[isam1], &vz[isam1], isam2 - isam1, w, &a, &b, &r);
		padEG->rvyvz[istroke] = r;

		/* Normalize by length of stroke */
		/* Normalize by /padEG->deltas */
		/* Becomes unitless */
		/* The following pattern (if not filtered) gets a=-0.5, b=+1.
		   The crossing points are typically (0.25,-0.25)
		   The average distance would be 0.25*sqrt(2)=.35
		   The length is 12.04 (Begin t=0; end t=17)
		   Normalized straightness error is .35/12.4=0.029
		0 0 1
		1 0 1
		1 1 1
		2 1 1
		2 2 1
		3 2 1
		3 3 1
		4 3 1
		4 4 1
		5 4 1
		5 5 1
		6 5 1
		6 6 1
		7 6 1
		7 7 1
		8 7 1
		8 8 1
		9 8 1
		9 9 1
		*/

		if (padEG->deltas [istroke] != 0.)
			padEG->straighterr [istroke] = padEG->straighterr [istroke] / padEG->deltas [istroke];

		/*******************/
		/* 3d straightness */
		/*******************/
		/* Heck for 3d straightness */
		rregr (&x[isam1], &y[isam1], isam2 - isam1, w, &a, &b, &r);
		rregr (&z[isam1], &y[isam1], isam2 - isam1, w, &a2, &b2, &r2);
		padEG->straighterr3 [istroke] = rmsdist3g (&x[isam1], &y[isam1], &z[isam1], isam2 - isam1, a, b, a2, b2);

		/* 12Jun03 Raji : Adjusting the straightness error parameters: excerpted from gripnew\extract.c*/
		if (padEG->deltasxy [istroke] != 0.) padEG->straighterrxy [istroke] /= padEG->deltasxy [istroke];
		if (padEG->deltasxz [istroke] != 0.) padEG->straighterrxz [istroke] /= padEG->deltasxz [istroke];
		if (padEG->deltasyz [istroke] != 0.) padEG->straighterryz [istroke] /= padEG->deltasyz [istroke];
		if (padEG->deltas3 [istroke] != 0.) padEG->straighterr3 [istroke] /= padEG->deltas3 [istroke];

		// 28Oct08: Somesh: Following parameters not needed for now
		///*******************************/
		///* Slant of the straight line  */
		///*******************************/
		///* in upper grip force, upper load force domain */
		///* Minimum squares fitted line */
		//padEG->slant [istroke]  = (double) atan (b);

		///* Orientation of the connection between begin and end */
		//if (padEG->deltay [istroke] != 0. && padEG->deltaz [istroke] != 0.)
		//	padEG->slant2 [istroke] = (float) atan2 ((double) (padEG->deltay [istroke]),
		//										   (double) (padEG->deltax [istroke]));
		//else
		//	padEG->slant2 [istroke] = 0.;

		////There may be a problem, e.g., hp-mhd-t yields -2 and +2 rad.
		///* Make padEG->slant in 4 quadrants just like padEG->slant2 */
		//if (fabs ((double) (padEG->slant [istroke] - PI - padEG->slant2 [istroke])) <
		//	fabs ((double) (padEG->slant [istroke]      - padEG->slant2 [istroke])))
		//	padEG->slant [istroke]  -= PI;
		//if (fabs ((double) (padEG->slant [istroke] + PI - padEG->slant2 [istroke])) <
		//	fabs ((double) (padEG->slant [istroke]      - padEG->slant2 [istroke])))
		//	padEG->slant [istroke]  += PI;


		///*************************************************/
		///* Slant of the straight line of the first 80ms  */
		///*************************************************/
		//isam1 = (int)(padEG->tseg [istroke] + ROUNDUP);
		//isam2 = (int)(padEG->tseg [istroke] + ROUNDUP + INITIALPART / sec);
		///* Artifact if isam1=isam2 */
		//if (isam1 == isam2)
		//	isam2++;
		//if (isam2 > padEG->tseg [istroke + 1] + ROUNDUP)
		//	isam2 = (int)(padEG->tseg [istroke + 1] + ROUNDUP);
		//w = 1.;
		//rregr (&x[isam1], &y[isam1], isam2 - isam1, w, &a, &b, &r);

		///* Minimum squares fitted line */
		//padEG->slant80 [istroke]  = (double) atan (b);

		///* Make padEG->slant in 4 quadrants just like padEG->slant2 */
		//if (fabs ((double) (padEG->slant80 [istroke] - PI - padEG->slant2 [istroke])) <
		//	fabs ((double) (padEG->slant80 [istroke]      - padEG->slant2 [istroke])))
		//	padEG->slant80 [istroke]  -= PI;
		//if (fabs ((double) (padEG->slant80 [istroke] + PI - padEG->slant2 [istroke])) <
		//	fabs ((double) (padEG->slant80 [istroke]      - padEG->slant2 [istroke])))
		//	padEG->slant80 [istroke]  += PI;

		//28Oct08: Somesh: New parameters added (max,mean,sd of x,y,z)
		meansd( &x [(int) padEG->tseg [istroke]], (int) (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) , &padEG->meanx [istroke], &padEG->sdx [istroke]);
		meansd( &y [(int) padEG->tseg [istroke]], (int) (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) , &padEG->meany [istroke], &padEG->sdy [istroke]);
		meansd( &z [(int) padEG->tseg [istroke]], (int) (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) , &padEG->meanz [istroke], &padEG->sdz [istroke]);

		minmax (&x [(int) padEG->tseg [istroke]], (int) (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) , &padEG->minx [istroke], &padEG->maxx [istroke]);
		minmax (&y [(int) padEG->tseg [istroke]], (int) (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) , &padEG->miny [istroke], &padEG->maxy [istroke]);
		minmax (&z [(int) padEG->tseg [istroke]], (int) (padEG->tseg [istroke + 1] - padEG->tseg [istroke]) , &padEG->minz [istroke], &padEG->maxz [istroke]);

		//30Oct08: Somesh: largestForceChan is common for all strokes (test based on segmeng.cpp to determine signal with max amplitude)
		if( padEG->maxx [istroke] > maxxtemp)
			maxxtemp = padEG->maxx [istroke];
		if( padEG->maxy [istroke] > maxytemp)
			maxytemp = padEG->maxy [istroke];
		if( padEG->maxz [istroke] > maxztemp)
			maxztemp = padEG->maxz [istroke];

		if ( maxxtemp >= maxytemp && maxxtemp >= maxztemp)
			largestForceChan = 1;
		else if ( maxytemp >= maxxtemp && maxytemp >= maxztemp)
			largestForceChan = 2;
		else
			largestForceChan = 3;
    }

    /********/
	/* Free */
    /********/
	if( x ) delete [] x;
	x = NULL;
	if( y ) delete [] y;
	y = NULL;

	/* Close input file */
	fclose( fTF );
	fTF = NULL;


	/************************/
	/*** Open Output file ***/
	/************************/
	create = !ff.FindFile( szExtOut );

	/* Open for appending and creating */
	if( create ) strcpy_s( szOpenMode, "w" );
	else strcpy_s( szOpenMode, "a" );
	if( ( fopen_s( &fExt, szExtOut, szOpenMode ) ) != 0 )
	{
		szMsg.Format( IDS_EXT_OUT, MODULE, szExtOut);
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/* File header */
	if (create)
	{

		szMsg.Format( IDS_EXT_OUTCREATE, MODULE, szExtOut);
		OutputMessage( szMsg, LOG_PROC );
		szMsg.Format( IDS_EXT_HDRGRIP2 );
		fprintf( fExt, szMsg );
	}

	/***************************************/
	/* Write stroke features */
	/***************************************/
	szTFIn = szTFIn.Left( szTFIn.GetLength() - 3 );	// Remove extension
	txt = szTFIn.Right( 2 );
	iarg = atoi( txt );
	szMsg.Format( IDS_EXT_EXTOUT, MODULE, szExtOut, iarg );
	OutputMessage( szMsg, LOG_PROC );

	/* It is problematic if a missing or empty trial just disappears */
	/* Therefore, zero the output (already done) and output an empty stroke */
	if (nstroke == 0)
		nstroke = 1;

	// 12Jun03 Raji : writing new parameters: excerpted from gripnew\extract.c
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		/**************************/
		// Output data per stroke
		/**************************/

		// 08Dec08: Somesh: Reflect code from Extract.cpp
		pFeatures[ 0 ] = iarg;
		pFeatures[ 1 ] = istroke + 1;
		pFeatures[ 2 ] = nstroke;
		pFeatures[ 3 ] = padEG->startt[ istroke ];
		pFeatures[ 4 ] = padEG->deltat[ istroke ];
		pFeatures[ 5 ] = padEG->bxy[ istroke ];
		pFeatures[ 6 ] = padEG->bxz[ istroke ];
		pFeatures[ 7 ] = padEG->byz[ istroke ];
		pFeatures[ 8 ] = padEG->rxy[ istroke ];
		pFeatures[ 9 ] = padEG->rxz[ istroke ];
		pFeatures[ 10 ] = padEG->ryz[ istroke ];
		pFeatures[ 11 ] = padEG->rvxvy[ istroke ];
		pFeatures[ 12 ] = padEG->rvxvz[ istroke ];
		pFeatures[ 13 ] = padEG->rvyvz[ istroke ];
		pFeatures[ 14 ] = padEG->straighterrxy[ istroke ];
		pFeatures[ 15 ] = padEG->straighterrxz[ istroke ];
		pFeatures[ 16 ] = padEG->straighterryz[ istroke ];
		pFeatures[ 17 ] = padEG->straighterr3[ istroke ];
		pFeatures[ 18 ] = padEG->startx[ istroke ];
		pFeatures[ 19 ] = padEG->starty[ istroke ];
		pFeatures[ 20 ] = padEG->startz[ istroke ];
		pFeatures[ 21 ] = padEG->deltax[ istroke ];
		pFeatures[ 22 ] = padEG->deltay[ istroke ];
		pFeatures[ 23 ] = padEG->deltaz[ istroke ];
		pFeatures[ 24 ] = padEG->deltas3[ istroke ];
		pFeatures[ 25 ] = padEG->meanx[ istroke ];
		pFeatures[ 26 ] = padEG->meany[ istroke ];
		pFeatures[ 27 ] = padEG->meanz[ istroke ];
		pFeatures[ 28 ] = padEG->sdx[ istroke ];
		pFeatures[ 29 ] = padEG->sdy[ istroke ];
		pFeatures[ 30 ] = padEG->sdz[ istroke ];
		pFeatures[ 31 ] = padEG->maxx[ istroke ];
		pFeatures[ 32 ] = padEG->maxy[ istroke ];
		pFeatures[ 33 ] = padEG->maxz[ istroke ];
		pFeatures[ 34 ] = padEG->minx[ istroke ];
		pFeatures[ 35 ] = padEG->miny[ istroke ];
		pFeatures[ 36 ] = padEG->minz[ istroke ];
		pFeatures[ 37 ] = largestForceChan;

		szMsg.Format( _T("%d %d %d %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %d\n"),
					  (int)(pFeatures[ 0 ]), (int)(pFeatures[ 1 ]), (int)(pFeatures[ 2 ]),
					  pFeatures[ 3 ], pFeatures[ 4 ],
					  pFeatures[ 5 ], pFeatures[ 6 ], pFeatures[ 7 ],
					  pFeatures[ 8 ], pFeatures[ 9 ], pFeatures[ 10 ],
					  pFeatures[ 11 ], pFeatures[ 12 ], pFeatures[ 13 ],
					  pFeatures[ 14 ], pFeatures[ 15 ], pFeatures[ 16 ], pFeatures[ 17 ],
					  pFeatures[ 18 ], pFeatures[ 19 ], pFeatures[ 20 ],
					  pFeatures[ 21 ], pFeatures[ 22 ], pFeatures[ 23 ], pFeatures[ 24 ],
					  pFeatures[ 25 ], pFeatures[ 26 ], pFeatures[ 27 ],
					  pFeatures[ 28 ], pFeatures[ 29 ], pFeatures[ 30 ],
					  pFeatures[ 31 ], pFeatures[ 32 ], pFeatures[ 33 ],
					  pFeatures[ 34 ], pFeatures[ 35 ], pFeatures[ 36 ],
					  (int)(pFeatures[ 37 ]) );

/*szMsg.Format( _T("%d %d %d %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %d \n"),
					  iarg,
					  istroke + 1,
					  nstroke,
					  padEG->startt [istroke], padEG->deltat [istroke],
					  padEG->bxy [istroke], padEG->bxz [istroke], padEG->byz [istroke],
					  padEG->rxy [istroke], padEG->rxz [istroke], padEG->ryz[istroke],
					  padEG->rvxvy [istroke], padEG->rvxvz [istroke],padEG->rvyvz [istroke],
					  padEG->straighterrxy [istroke],padEG->straighterrxz[istroke], padEG->straighterryz [istroke],
					  padEG->straighterr3 [istroke],
					  padEG->startx [istroke],padEG->starty [istroke],padEG->startz [istroke],
					  padEG->deltax [istroke],padEG->deltay [istroke],padEG->deltaz [istroke],
					  padEG->deltas3 [istroke],
					  padEG->meanx [istroke],padEG->meany [istroke],padEG->meanz [istroke],
					  padEG->sdx [istroke],padEG->sdy [istroke],padEG->sdz [istroke],
					  padEG->maxx [istroke],padEG->maxy [istroke],padEG->maxz [istroke],
					  padEG->minx [istroke],padEG->miny [istroke],padEG->minz [istroke],
					  largestForceChan
					  );*/
		fprintf( fExt, szMsg );
	}

Cleanup:

    /*****************/
    /* Free the rest */
    /*****************/
	if( fTF ) fclose ( fTF );
	if( fSeg ) fclose( fSeg );
	if( fExt ) fclose( fExt );
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;
	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( vz ) delete [] vz;

	if( padEG ) delete padEG;

	return fRet;
}


/****************************************************************************/
double loopareag (double* x, double* y, int ifirst, int ilast, double *t1loop,
				 double *t2loop, double sec)
/****************************************************************************/

{
/* Max loop time is actually senseless because you do not know the time of begin of loop */
/* until you found also the end of loop */

/* BUG: If msxlooptime too short the loop is detected in the wrong stroke */

#define MAXLOOPTIME 5.  /* in Sec: Max time for 3 strokes; Max inter-sample periods after previous loop = 2 s = 400,200 points at 200,100Hz */
                         /* A large value slows the program down dramatically */
/* However it is very important to have maxlooptime not too small */
/* because otherwise the loop padEG->surface is detected one stroke later */
#define MINLOOPTIME 0.05    /* in Sec: Min loop time in inter-sample periods = 200ms =40,20 points at 200,100 Hz */
#define MAXSPEED 25       /* Maximum pen speed in cm/s */

	double x1 = 0.0, y1 = 0.0, x2 = 0.0, y2 = 0.0;
	double dx1 = 0.0, dy1 = 0.0, dx2 = 0.0, dy2 = 0.0;
	double alpha1 = 0.0, alpha2 = 0.0;   /* Relative sample nrs between 0 (=isample) and 1 (=isample+1) */
	double alpha1c = 0.0, alpha2c = 0.0; /* Counter to calculate alpha */
	double alpha1d = 0.0, alpha2d = 0.0; /* Denominator to calculate alpha */
	int isample1 = 0, isample2 = 0;
	int deltasample1 = 0;
	double looparea = 0.0, loopa = 0.0;
	int isample = 0;
	double unitspersample = 0.0;
	int isample2previousloop = 0;

	/*******************************************************************/
	/* Do not check consecutive parts as they may be parallel */
	/* Use extra margin of MINLOOPTIME for convenience (gets rid of noise) */
	/* Use maxlooptime to speed up */
	/* isample2=last sample before closing of loop
	/* isample1=last sample before start of loop
	/* Do */
	/* isample2 = isample2previousloop +1 +MINLOOPTIME; isample1 = isample2previousloop +0      */
	/* isample2 = isample2previousloop +2 +MINLOOPTIME; isample1 = isample2previousloop +0,1,   */
	/* isample2 = isample2previousloop +3 +MINLOOPTIME; isample1 = isample2previousloop +0,1,2  */
	/* ... */
	/* isample2 = isample2previousloop +MAXLOOPTIME; isample1 = isample2previousloop + 0,1,2,...,MAXLOOPTIME - 1 */

	/* Where isample2 can be as small as ifirst and as large as <ilast

	/* Thus at any time all adjecent pairs have been tested */

	/* Max number of coordinate units crossed per intersample period */
	unitspersample = 1. / (2. * MAXSPEED * sec);

	/* Default if no loop found */
	/* Initialize */
	*t1loop = 0.;
	*t2loop = 0.;
	isample2previousloop = ifirst;


	/* BUG DOES NOT FIND FIRST LOOP IN WREHKA01.hwr */

	/* End of loop test runs from first to last sample */
	/* and jumps to the sample after each loop */
	isample2 = isample2previousloop;
	deltasample1 = 1;
	while (isample2 < MIN (ilast - 1, isample2previousloop + MAXLOOPTIME / sec))
	{
		/* Take every sample as it is unclear whether there may be a sample in the neighborhood */
		isample2 += 1;

		x2 = x [isample2];
		y2 = y [isample2];
		dx2 = x [isample2 + 1] - x2;
		dy2 = y [isample2 + 1] - y2;

		/* Search for begin and end of loop sample pairs */
		/* Begin of loop test runs from isample2previousloop till end of loop sample upto MINLOOPTIME */
		isample1 = isample2previousloop;
		/* Neighboring samples automatically touch with alpha1=1 and alpha2=0 */
		while (isample1 < MIN (isample2 - MINLOOPTIME / sec, isample2 - 1 - 1))
		{
			/* Sample increment increases with distance so that at least deltasample1 samples are */
			/* needed to let both points meet */
			/* Here the data are still in device coordinates and not in cm */
			isample1++;

			/* Discard if rectangles do not overlap */
			if (MAX (x [isample1], x [isample1 + 1]) <
				MIN (x [isample2], x [isample2 + 1]))
				goto trynextsample;
			if (MIN (x [isample1], x [isample1 + 1]) >
				MAX (x [isample2], x [isample2 + 1]))
				goto trynextsample;
			if (MAX (y [isample1], y [isample1 + 1]) <
				MIN (y [isample2], y [isample2 + 1]))
				goto trynextsample;
			if (MIN (y [isample1], y [isample1 + 1]) >
				MAX (y [isample2], y [isample2 + 1]))
				goto trynextsample;

			x1 = x [isample1];
			y1 = y [isample1];
			dx1 = x [isample1 + 1] - x1;
			dy1 = y [isample1 + 1] - y1;

			alpha1c = (x2 - x1) * dy2 - dx2 * (y2 - y1);
			alpha1d = dx1 * dy2 - dx2 * dy1;
			if (alpha1d == 0.)
				goto trynextsample;
			alpha1 = alpha1c / alpha1d;
			if (alpha1 < -0.001 || alpha1 > 1.001)
				goto trynextsample;

			/* Substitute in other formula */
			if (dx2 != 0.)
			{
				alpha2c = x1 + alpha1 * dx1 - x2;
				alpha2d = dx2;
			}
			else if (dy2 != 0.)
			{
				alpha2c = y1 + alpha1 * dy1 - y2;
				alpha2d = dy2;
			}
			else goto trynextsample;

			if (alpha2d == 0.)
				goto trynextsample;
			alpha2 = alpha2c / alpha2d;
			if (alpha2 < -0.001 || alpha2 > 1.001)
				goto trynextsample;

			/**************/
			/* Loop found */
			/**************/
			/* Real sample numbers of the two crossing points */
			*t1loop = isample1 + alpha1;
			*t2loop = isample2 + alpha2;

			/*************/
			/* Loop area */
			/*************/
			/* Estimate padEG->surface by integrating innerproduct of the direction */
			/* vector with a 0.5*(y,-x) field, which has curl 1 */
			/* According to Stokes, the curve integral equals the padEG->surface */
			/* integral of the curl */
			/* To reduce addition of large positive and negative numbers x,y at isample1 is deducted */
			/* The loop will thus be translated to the origin */
			/* For the differences between isample+1 and isample this is not needed */
			looparea = 0.;
			for (isample = isample1; isample < isample2; isample++)
			{
				loopa = (-(x [isample + 1] - x [isample]) * (y [isample] - y [isample1]) +
						(y [isample + 1] - y [isample]) * (x [isample] - x [isample1]));
				looparea += loopa;
			}
			/* Correct for 0.5 factor omitted in inner product */
			looparea *= 0.5;

			if (fabs ((double) looparea ) > MINLOOPAREA)
			{
				/**********************************************/
				/* Start searching next loop after found loop */
				/**********************************************/
				isample2previousloop = isample2;
				return looparea;
			}

trynextsample:;    /* label because of deep nesting */

		}
	}

  /* Return zero loop area if no loop found */
  return 0.;
}

/*******************************************************************/
double rmsdist3g (double* x, double* y, double* z, int n, double a,
				 double b, double az, double bz)
/*******************************************************************/
/*
RMS distance of array of (x, y) points from line
  y = a + b * x

The perpendicular line through (x1, y1) or (x[is],y[is]) is
  (y - y1) = -1/b * (x - x1)

Solving y yields the x of the corssing point of both lines
  a + b * x = y1 - 1/b * (x - x1)
or
  x = (y1 - a - 1/b * (x - x1)) / b

*/
{
	int is = 0;
	double qdist = 0.0, xis0 = 0.0, yis0 = 0.0;
	double xis0z = 0.0, yis0z = 0.0;

	if (n == 0) return (0.);

	for (is = 0; is < n; is++)
	{
		xis0 = (x [is] + (y [is] - a) * b) / (SQR (b) + 1.);
		yis0 = a + b * xis0;
		xis0z = (z [is] + (y [is] - az) * bz) / (SQR (bz) + 1.);
		yis0z = az + bz * xis0z;
		qdist += SQR (xis0 - x [is]) + SQR (yis0 - y [is]) +
				 SQR (xis0z - z [is]) + SQR (yis0z - y [is]);
	}

	return sqrt (qdist / n);
}

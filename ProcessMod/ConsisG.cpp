/*************************************************
    Consis - version 2.0 - hlt - 20 Mar 1995
**************************************************
    Copyright 1995 Teulings

ARGUMENTS: ext_infile con_outfile lexicon_infile cond err_outfile /Nohead

PURPOSES:
    Consistency check of trials for grip-force experiments

FORMAT
  lexicon_file
  Lines with: condcode #min strokes, #maxstrokes, #strokes to remove from begin,
    target size, size range, target direction, direction range.

UPDATES:
	20Oct07: GMB: Created
	9Nov2010: HLT: Extended some criteria
*/

/************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include "../NSShared/iolib.h"
#include "../Common/DefFun.h"


/******************/
/* Column Indices */
/******************/
// 29Oct08: Somesh: Make sure ConsisG reads the right columns from Ext files if extract output is modified.
#define NINDEX 2
#define ITRIAL 0
#define ISEG   1
/* data items */
#define NDATA		50 /* Number of data */ /* last updated 16Feb05*/
#define STARTT		 3
#define DELTATG		 4
#define UPLOGRIPCOR  9
#define STRAIGHTERR 17
#define DELTAXG		21
#define DELTAYG		22
#define DELTAZG		23
#define MAXZG		33

#define ISEG0 0
#define ISEG2 1
#define ISEG3 2

/*******************/
/* Stroke features */
/*******************/
#define ASCENDING +1  /* Upward strokes have postitive vertical size: EXAMPLE: Stroke 1 of letter e */
#define DESCENDING -1 /* Downward strokes have negative vertical size: EXAMPLE: Stroke 2 of letter e */

/* Connection feature: How strokes are connected with the previous stroke */
#define NOLOOP 1    /* No loop as stroke is fluent. EXAMPLE: Strokes 2 and 4 of letter n assuming Stroke 1 is the initial upstroke */
#define SHARP 2     /* Stroke starts with a reversal.EXAMPLE: Stroke 3 of letter n */
#define LOOP 4      /* Stroke forms loop with previous one. EXAMPLE: Stroke 2 of letter l */
#define LOOPORNOLOOP 0 /* Irrelevant loop (after removal of first).  */

/* Vertical size feature: How the length is relative to other strokes */
/* For example, all letters e shoudl be smaller than all letters l */
#define SHORT 8     /* EXAMPLE: Both strokes of letter e */
#define LONG 16     /* EXAMPLE: Bot strokes of letter l */
#define SHORTLONG 0 /* Irrelvant stroke length as end stroke of l, which may be followed by another l */

/* Stroke features */
#define NOTHING 0
#define UPSTROKE 1
#define NSTROKES 2
#define SIZES 4
#define LOOPS 8
#define ORIENTATION 16
#define SPIRAL 32
#define STRAIGHTNESS 64
#define STRAIGHT 1
#define CURVED 2
#define ANTICIPATIONTESTCRIT 0.1


/****************/
/* Buffer sizes */
/****************/
#define NDATAMAX NINDEX + NDATA
#define NSEGMAXG 72+1  /* one more than minimum because 1st may be removed */
#define NTRIALMAX 99	  /**/
#define NCONDMAX    36 /* max number of conditions to be verified */
#define NCHARLEXMAX 40 /* max length of the patterns */
#define NCHARLEXLINEMAX (40 + NCHARLEXMAX)
#define ERRBUFLEN 1024 /* #chars for error texts of all trials */
#define ERRLEN 256 /* #chars for error texts of one trial */


/************/
/* Criteria */
/************/
bool anticipationtestminG = false, anticipationtestmaxG = false;
double RTminG = 0.0, RTmaxG = 0.0;
// Discard if less than Minimum Reaction Time
void SetDiscardLTMinG( bool fVal ) { anticipationtestminG = fVal; }
// Minimum Reaction Time
void SetMinReactionTimeG( double dVal ) { RTminG = dVal; }
// Discard if greather than Maximum Reaction Time
void SetDiscardGTMaxG( bool fVal ) { anticipationtestmaxG = fVal; }
// Maximum Reaction Time
void SetMaxReactionTimeG( double dVal ) { RTmaxG = dVal; }
// Magnet Force
double _dMagnetForce = 0.;
void SetMagnetForce( double dVal ) { _dMagnetForce = dVal; }


#define NONSPECIFIED 0
#define IDLEN 5 /* sufficient for e1/2, etc. plus nul byte */
#define EMPTY -1
#define ICONDPATTERN ICOND2
#define NCONDDEF 6

#define	MODULE				_T("CONSIS")
#define CON					_T("Consistency")
#define CON_PROCESSING		_T("Processing Consistency")
#define CON_ERROR			_T("Error")
#define CON_WARNING			_T("WARNING")
#define CON_SUMMARY			_T("SUMMARY")
#define CON_MESSAGE			_T("")
#define CON_PERFORMANCE		_T("Performance")
#define CON_SYSTEM			_T("System")
#define CON_LOCATION		_T("")
#define CON_CONTACTUS		_T("(Contact us for more help)")

struct _ArrayDataCG
{
	double data [NDATAMAX] [NSEGMAXG];
	double datatmp [NDATAMAX] [NSEGMAXG];
	double datalasttrial [NDATAMAX] [NSEGMAXG];
	double dataline [NDATAMAX];
	int nsegskip [NCONDMAX];
};

static _ArrayDataCG padC;
CStdioFile*	_pErrG = NULL;

bool printandbufferg (CString err, CString errbuf, bool toErr = false);

#pragma warning( disable: 4996 )	// disable deprecated warning

/************************************************************************/

/***********************************/
bool ConsisG( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			 short nStrokeMax, double dStrokeLength, double dRangeLength,
			 double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			 CString szCondition, CString szErrOut, unsigned int nSwitch,
			 CString szTrials, bool fLastOnly, bool* fPassed, unsigned int nTask )
/***********************************/
{
	CStringList lst;
	CString szTemp = szTrials, szTrial;

	if( !szTrials.IsEmpty() )
	{
		int nPos = szTrials.Find( _T(" ") );
		while( nPos != -1 )
		{
			szTrial = szTemp.Left( nPos );
			lst.AddTail( szTrial );
			szTemp = szTemp.Mid( nPos + 1 );
		}
	}

	return ConsisG( szExtIn, szConOut, szLexIn, nStrokeMin, nStrokeMax, dStrokeLength,
				   dRangeLength, dStrokeDirection, dRangeDirection, nStrokeSkip,
				   szCondition, szErrOut, nSwitch, &lst, fLastOnly, fPassed, nTask );
}

/***********************************/
bool ConsisG( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			 short nStrokeMax, double dStrokeLength, double dRangeLength,
			 double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			 CString szCondition, CString szErrOut, unsigned int nSwitch,
			 CStringList* pszlTrials, bool fLastOnly, bool* fPassed, unsigned int nTask )
/***********************************/
{
	bool fRet = true;
	int ndata = 0;
	POSITION pos = NULL;
	CString szMsg, errbuf, err, szTemp;
	CString err1 = "";
	int iseg = 0, nseg = 0;
	int idata = 0, itrial = 0;
	int ntrialmax = 0, ntrialdiscarded = 0, ntrialin = 0;
	int icond = 0, ncond = 0;
	CStdioFile fExt;
	FILE *fCon = NULL;
	/* Maximum size of each column label is 40 characters */
	CStringArray datatxt;
	CString szLine;
	char condcode [NCONDMAX] [4];
	char condcodearg [4];  /* Presently only one byte used */
	int consistencytest = 0, discardtrial = 0;
	int nexttrial = 0, nextsegment = 0, iseg1 = 0;
	int i = 0;
	int nLastTrial = 0;
	int nextstroke = 0;
	CStdioFile fErr;
	double tttt = 0.;

	// initialize data array
	memset( &padC, 0, sizeof( _ArrayDataCG ) );

	// Defaults for all switches
	bool absolute = false, nohead = false, morethanmaxiserror = false;
	bool testupstroke = false, anticipationtest = false, substitutemissing = false;
	bool create = false;
	bool secondary = false;


	/*****************/
	/*** Switches  ***/
	/*****************/
	/* From Experiment Settings >Processing >Consistency > Flags */

	// Use absolute measures instead of relative meaures
	if( ( nSwitch & CONSIS_SWITCH_A ) != 0 )
	{
		absolute = true;
		szMsg.Format( IDS_CON_ABS, CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// CHECK: Is this one used?
	// Do not output column header in .con file
	if( ( nSwitch & CONSIS_SWITCH_N ) != 0 )
	{
		nohead = true;
		szMsg.Format( IDS_CON_NOHEAD,CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// Discard trial if # strokes too high
	if( ( nSwitch & CONSIS_SWITCH_M ) != 0 )
	{
		morethanmaxiserror = true;
		szMsg.Format( IDS_CON_MAX,CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// Remove initial downstroke
	if( ( nSwitch & CONSIS_SWITCH_U ) != 0 )
	{
		testupstroke = true;
		OutputMessage( szMsg, LOG_PROC );
	}
	// Discard trial if first stroke start within the reaction time
	//else szMsg.Format( IDS_CON_UPTEST, MODULE );
	else szMsg.Format( IDS_CON_UPTEST, CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi

	if( ( nSwitch & CONSIS_SWITCH_C ) != 0 )
	{
		anticipationtest = true;
		szMsg.Format( IDS_CON_ANTTEST, CON_PROCESSING, CON_LOCATION, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );
	}
	// CHECK: Used?
	// Subsitute discarded trials by emtpy trials
	if( ( nSwitch & CONSIS_SWITCH_S ) != 0 )
	{
		substitutemissing = true;
		szMsg.Format( IDS_CON_SUB1, CON_PROCESSING, CON_MESSAGE); //yi
		OutputMessage( szMsg, LOG_PROC );

		if( pszlTrials )
		{
			int nTrial = 0;
			pos = pszlTrials->GetHeadPosition();
			while( pos )
			{
				CString szTrial = pszlTrials->GetNext( pos );
				szMsg.Format( IDS_CON_SUB2, ++nTrial, szTrial, CON_PROCESSING, CON_MESSAGE); //yi
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}
	/* Primary+Secondary Submovement Optimization all refer to subanalysis within a stroke */
	/* Default is false */
	if( ( nSwitch & CONSIS_SWITCH_O ) != 0 )
	{
	    secondary = true;
		szMsg.Format( IDS_EXT_SEC, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}

	//********************************************************//
	// Yi Aug.28.2006
	// We may only need the loop number of 1 for the for statement below
	// We may need to consider to remove it.
	//********************************************************//
	for (icond = 0; icond < NCONDMAX; icond++)
	{
		/* Read complete line */
		if( icond > 0 ) break;
		/* Parse the line into features */
		/* For the time being there is fixed sequence of features */
		/* Reset to neutral value to be sure */

		// Strokes to skip
		padC.nsegskip [icond] = nStrokeSkip;
		// Condition ID
		strcpy_s( condcode[ icond ], szCondition );

		if (padC.nsegskip [icond] != NONSPECIFIED)
		{
			szTemp.Format( IDS_CON_LEX4, padC.nsegskip [icond] );
			szMsg += szTemp;
		}

		if( !szMsg.IsEmpty() ) OutputMessage( szMsg, LOG_PROC );
	}

	/*********/
	/* Tests */
	/*********/
	/* Test whether number of conditions icond is within range */
	if (icond == NCONDMAX)
	{
		szMsg.Format( IDS_CON_LEXTEST1, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szLexIn, icond+1, NCONDMAX); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	else if (icond == 0)
	{
		szMsg.Format( IDS_CON_LEXTEST2, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szLexIn); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	/* The number of conditions in lexicon */
	ncond = icond;

	/*******************/
	/* open input data */
	/*******************/
	/* Open .ext file created by extract */
	if( !fExt.Open( szExtIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_CON_EXTERR, CON, CON_WARNING,CON_MESSAGE, szExtIn); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	// Determine last trial # if specified
	/* 24May99: hlt: Do not assume this is the highest trial number */
	if( fLastOnly )
	{
		char pszTemp[ 1024 ];
		int nTemp = 0;

		while( fExt.ReadString( szLine ) )
		{
			sscanf( szLine, _T("%d%[^\n]"), &nLastTrial, pszTemp );
		}

		fExt.SeekToBegin();
	}

	szMsg.Format( IDS_CON_EXTREAD, CON_PROCESSING, CON_MESSAGE, szExtIn, szCondition); //yi
	OutputMessage( szMsg, LOG_PROC );

	/* Read data names */
	fExt.ReadString( szLine );
	idata = 0;
	while( idata < NDATAMAX )
	{
		int nPos = szLine.Find( _T(" ") );
		/*16Dec03: Raji: Extract individual labels in to CStringArray datatxt*/
		while( nPos != -1 )
		{
			idata ++;
			datatxt.Add( szLine.Left( nPos));
			szLine = szLine.Mid( nPos + 1 );
			nPos = szLine.Find( _T(" ") );
		}
		//Nov.09.06 Yi : not sure whether it's a bug here
		idata ++;

		datatxt.Add(szLine);
		/* 30Sep03: hlt: break after detecting no more spaces */
		/* If you do not break, the last label will be repeated for N times */
		break;
	}
	/* 30Sep03: hlt: Count of number of labels. The number of columns will be ndatamax */
    int ndatamax; /* Max number of columns in the input .ext file */
	ndatamax = idata;

	/************************/
	/*** open output file ***/
	/************************/
	/* Open */
	if( ( fopen_s( &fCon, szConOut, "w" ) ) != 0 )
	{
		szMsg.Format( IDS_CON_CONERR,  CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szConOut); //yi
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/* Write header consisting of column headers */
	szMsg.Format( IDS_CON_CONCREATE, CON_PROCESSING, CON_MESSAGE, szConOut); //yi
	OutputMessage( szMsg, LOG_PROC );
	if (!nohead)
	{
		/* 30Sep03: hlt: Was till NDATAMAX, now till ndatamax which is dynamic */
		for (idata = 0; idata < ndatamax; idata++)
		{
			szMsg.Format( _T("%s "), datatxt.GetAt(idata));
			fprintf( fCon, szMsg );
		}
		fprintf( fCon, _T("\n") );
	}
	/**********************/
	/* Open error Outfile */
	/**********************/
	szMsg.Format( IDS_CON_ERROUT, CON_PROCESSING, CON_MESSAGE, szErrOut); //yi
	OutputMessage( szMsg, LOG_PROC );
	/* Open */
	if( !fErr.Open( szErrOut, CFile::modeWrite ) )
	{
		if( !fErr.Open( szErrOut, CFile::modeCreate | CFile::modeWrite ) )
		{
			szMsg.Format( IDS_CON_ERRERR, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szErrOut); //yi
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}
	}
	else fErr.SeekToEnd();
	_pErrG = &fErr;

	/********************************/
	/* initialize input data arrays */
	/********************************/
	nseg = 0;
	ntrialmax = 0;
	ntrialdiscarded = 0;
	ntrialin = 0;
	for (idata = 0; idata < NDATAMAX; idata++)
	{
		for (iseg = 0; iseg < NSEGMAXG; iseg++)
		{
			/* 09 May 03 Raji: This array will store the segments 3,6,9...to 3 times nstroke; in the case of secondary analysis*/
			/* and 1,2,3...to nstroke otherwise*/
			padC.data [idata] [iseg] = 0.;
			/* 09 May 03: Raji: A new array datatmp added*/
			/* In order to store all the segments 1.2,3...to 3 times nstroke; in the case of secondary analysis for the output file*/
			padC.datatmp [idata] [iseg] = 0.;
		}
	}
	/* Some impossible trial number */
	padC.datalasttrial [0] [ISEG0] = EMPTY;
	itrial = EMPTY;
	nexttrial = true;
	nextsegment = true;

	/***********************/
	/* Read trial by trial */
	/***********************/
	/* Read the .ext file created by extract */
	/* Keep on getting trials unless EOF detected last time */
	while (nexttrial)
	{
		/* Cumulative counter of number of segments */
		nseg = 0;
		/* raji: 9May03:*/
			/* Select only the full strokes (iseg=1,2,3 if no-secondary and */
			/* 3,6,9,... is secondary submovement analysis */
			/* The number of strokes needs to be reduced in the latter case */
			if (secondary)
				nextstroke=3;
			else
				nextstroke=1;
		/* Keep on getting segments for that trial*/
		while (true)
		{
			/* Read segment if not already done */
			if (nextsegment)
			{
				/* 24May00: hlt: Skip all trials with a trial number different from the last appended trial number */
				/* TODO: POTENTIAL BUG: If the same trial occurs more than once in the .ext file earlier ones are overwritten */
				/* when consistency checking the last file although, when summarizing the results of all */
				/* trials in the condition, the last trial with the targeted trial number is used */
				/* If the < statement is used we may get an infinite loop if nLastTrial < all trial nrs in the .ext file */
				//Yi, ndata is the number of data in the line of data readin from fExt
				// GMB: 17Aug07: while did not check for end of file (causing infinite loop in cases of 1 trial)
				if( fLastOnly )
				{
					do ndata = getlinefloat( fExt, padC.dataline, NDATAMAX );
					while( ( padC.dataline[ 0 ] != nLastTrial ) && ( ndata != 0 ) );
				}
				else ndata = getlinefloat (fExt, padC.dataline, NDATAMAX);

				/*************************/
				/* End of file criterion */
				/*************************/
				if (ndata == 0)
				{
					/* Remember next time not to start a next trial */
					nexttrial = false;
					/* Do not get more segments */
					break;
				}

				/* Check for incomplete line; May mean wrong stroke data file */
				/* 30sep03: hlt: Was NDATAMAX */
				if (ndata != ndatamax)
				{
					szMsg.Format( IDS_CON_EXTLEN, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, ndata, szExtIn, NDATAMAX); //yi

					for (idata = 0; idata < ndata; idata++)
					{
						szTemp.Format( _T("%.3g "), padC.dataline [idata]);
						szMsg += szTemp;
					}
					OutputMessage( szMsg, LOG_PROC );
					fRet = false;
					goto Cleanup;
				}
				/***********************/
				/* New trial criterion */
				/***********************/
				//ITRIAL is defined as 0
				//ISEG is defined as  1
				if (itrial != EMPTY && padC.dataline [ITRIAL] != itrial)
				{
					/* Stop reading more segments and remember to skip the first read next time */
					nextsegment = false;
				    break;
				}
			}//end of if (nextsegment)

			/**********************/
			/* Store this trial */
			/**********************/
			/* Here some space can be saved: Skipped strokes do not need to be stored */
			/* Also Indices may not need to be stored */
			itrial = (int) padC.dataline [ITRIAL];
			/* redefine iseg here */
			iseg = (int) padC.dataline [ISEG];
			// 10Sep03: GMB - Added asserts to ensure validity of indices

			ASSERT( iseg >= 1 );
			if (iseg <= NSEGMAXG )
			{
				for (idata = 0; idata < ndata; idata++)
				{
					// All data, ALL segments of ONE trial
					/* raji: 9May03: Store the strokes in datatmp and */
					/* datatmp is output after each trial */
					/* If nextstroke=3: other wise it is just a copy */
					/* So take iseg=3,6,9, .. in positions 2, 5, 8, ... (iseg-1) */
					/* and put it into new positions 0, 1, 2, ...  (iseg/nextstroke -1) */
					 //padC.data [idata] [iseg - 1] = padC.dataline [idata];
					padC.datatmp [idata] [iseg - 1] = padC.dataline [idata];
					if (int(iseg/nextstroke)*nextstroke==iseg)
							padC.data [idata] [iseg/nextstroke - 1] = padC.dataline [idata];
                }

				/* 30 May 03: Raji:IF trial nr read is lesser from current value then we are still in the current trial*/
				/* the number of full strokes is 3 times less if secondary analysis */
				/* ELSE when the trial number reads equal to the current value it will point to the next trial*/
				/* Store the number of segments from the ext file before going to the next trial*/
				/* This is necessary while reprocessing a single trial, and the ext file */
				/* Has a different number of segments for each reprocessing of trial due to appending*/
				if (nseg < iseg / nextstroke)
					nseg = iseg / nextstroke;
				else
					nseg = (int)padC.dataline[ISEG]/nextstroke;
			}

			/* Max trialnr */
			if (ntrialmax < itrial)
				ntrialmax = itrial;

			/* Since segement has been stored allow next time to read following segment */
			nextsegment = true;
		}//end of while (true)

		/*******************/
		/* New trial found */
		/*******************/
		/* Count nr of trials read */
		ntrialin++;

		/******************/
		/* Condition code */
		/******************/
		sscanf (szCondition, "%s", &condcodearg);

		/* Find cond nr from cond code in lex file */
		for (icond = 0; icond < ncond; icond++)
			if (strcmp (condcodearg, condcode [icond]) == 0)
				break;
		/* Test if found or not */
		if (icond == ncond)
		{
			/* Not found */
			szMsg.Format( IDS_CON_LEXERR, CON, CON_SYSTEM, CON_ERROR, CON_CONTACTUS, CON_MESSAGE, szLexIn, condcodearg, NINDEX); //yi
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}

		/************************/
		/* Skip initial strokes */
		/************************/
		// As specified in Condition properties >Condition checking
		szMsg.Format( IDS_CON_SKIP,CON_PROCESSING, CON_MESSAGE, itrial, itrial - 1, nseg, padC.nsegskip [icond]); //yi
		OutputMessage( szMsg, LOG_PROC );
		if (nseg > NSEGMAXG)
		{
			szMsg.Format( IDS_CON_TRUNC, CON_PROCESSING, CON_MESSAGE, NSEGMAXG); //yi
			OutputMessage( szMsg, LOG_PROC );
		}

		/*****************************************************/
		/* Interpret lexicon in terms of features of letters */
		/*****************************************************/
		/* Once for all trials: */
		/* Eventually this information should be stored in a letter file */
		/* Initialize */
		consistencytest = NOTHING;
		iseg1 = padC.nsegskip [icond];
		iseg = iseg1;

		/***************/
		/* COMPARISONS */
		/***************/
		/*********************************************/
		/* Assume trial is ok and try to find errors */
		/*********************************************/
		discardtrial = false;

		/*************************************/
		/* Indices of stroke 1 to err buffer */
		/*************************************/
		errbuf = _T("");
		err.Format( _T("%d "), itrial );
		errbuf += err;

		/***********************/
		/* Test for empty data */
		/***********************/
		if (nseg <= 0 || padC.data [DELTATG][ISEG0] == 0.)
		{
			err.LoadString( IDS_CON_EMPTY );
			discardtrial = printandbufferg (err, errbuf);
		}

		/* see if reaction time is lesser/greater than maximum reaction time*/
		if (anticipationtestminG)
		{
			if (nseg > 0 && padC.data [STARTT] [ISEG0] < RTminG)
			{
				err.Format( IDS_CON_ANT1,CON_PROCESSING, CON_MESSAGE, padC.data [STARTT] [ISEG0], RTminG); //yi
				discardtrial = printandbufferg (err, errbuf);
			}
		}
		if (anticipationtestmaxG)
		{
			if (nseg > 0 && padC.data [STARTT] [ISEG0] > RTmaxG)
			{
				err.Format( IDS_CON_ANT2, CON, CON_PERFORMANCE, CON_ERROR, CON_LOCATION, CON_MESSAGE, padC.data [STARTT] [ISEG0], RTmaxG); //yi
				discardtrial = printandbufferg (err, errbuf);
			}
		}

		// Special cases for Standard gripper tasks, Repetitive pressure task, Max pressure task
		if( nTask == GRIPPER_TASK_DEFAULT ) nTask = GRIPPER_TASK_STANDARD;

		////////////////////////////////////
		// Tests for standard gripper task
		////////////////////////////////////
		if( nTask == GRIPPER_TASK_STANDARD )
		{
			if (nseg > 0)
			{
				// Standard Gripper Task: Duration Lift Phase (segment 2) = 0
				if (padC.data [DELTATG] [ISEG2] == 0.)
				{
					err.Format( IDS_CON_ZEROLIFTDURATION, padC.data [DELTATG] [ISEG2]);
					discardtrial = printandbufferg (err, errbuf);
				}

				// Standard Gripper Task: Upper vs Lower Grip Correlation in Lift Phase (Segment=2) = 0
				if (padC.data [UPLOGRIPCOR] [ISEG2] == 0.)
				{
					err.Format( IDS_CON_UPLOWGRIPCORNEG);
					discardtrial = printandbufferg (err, errbuf);
				}

				// 31Aug10: Somesh: Was _dMagnetForce+-20%
				// Standard Gripper Task: Maximum lift force in Lift Phase (Segment=2) = %.3g should lie within _dMagnetForce+-60%
				if (padC.data [MAXZG] [ISEG2] > (1 +.70) * _dMagnetForce || padC.data [MAXZG] [ISEG2] < (1 - .6) * _dMagnetForce)
				{
					err.Format( IDS_CON_LIFTHIGH, padC.data [MAXZG] [ISEG2], _dMagnetForce);
					discardtrial = printandbufferg (err, errbuf);
				}

				// Standard Gripper Task: Drop in Lift force in Disconnect Phase (Segment=3) = absolute(%.3g) < 1.5 N
				if ( - padC.data [DELTAZG] [ISEG3] < 1.5)
				{
					err.Format( IDS_CON_DISCONNECTLOW, - padC.data [DELTAZG] [ISEG3] );
					discardtrial = printandbufferg (err, errbuf);
				}

				// 02Sep10: Somesh: Was 0.4 s
				// Standard Gripper Task: Drop time of Lift force in Disconnect Phase (Segment=3) = %.3g > 0.7 s
				if (padC.data [DELTATG] [ISEG3] > 1.2)
				{
					err.Format( IDS_CON_DISCONNECTSLOW, padC.data [DELTATG] [ISEG3]);
					discardtrial = printandbufferg (err, errbuf);
				}

				// Standard Gripper Task: Increase in Lower Grip force in Lift Phase (Segment=2) = %.3g < 1 N
				if (padC.data [DELTAXG] [ISEG2] < 1.)
				{
					err.Format( IDS_CON_LOWERGRIPLOW, padC.data [DELTAXG] [ISEG2]);
					discardtrial = printandbufferg (err, errbuf);
				}

				// Standard Gripper Task: Increase in Upper Grip force in Lift Phase (Segment=2) = %.3g < 1 N
				if (padC.data [DELTAYG] [ISEG2] < 1.)
				{
					err.Format( IDS_CON_UPPERGRIPLOW, padC.data [DELTAYG] [ISEG2]);
					discardtrial = printandbufferg (err, errbuf);
				}

				// Standard Gripper Task: Ma
			}
		}

		// Repetitive pressure task
		else if( nTask == GRIPPER_TASK_REPETITIVE )
		{
			// TODO
		}

		// Maximum pressure task
		else if( nTask == GRIPPER_TASK_MAXIMUM )
		{
			// TODO
		}


		/***************************/
		/* (Finally) Discard trial */
		/***************************/
		if (discardtrial)
		{
			/* Discard trial by making the number of segments 0 */
			nseg = 0;
			ntrialdiscarded++;
		}

		/*******************/
		/* Or accept trial */
		/*******************/
		else
		{
			/* 2 Jun 03 Raji: nseg now has the number of segments processed, which is lesser in secondary analysis*/
			/* The original number of strokes should be restored while writing the file*/
            /* Use the input array datatmp[][] array for output instead of using data[][] array*/
			/* which stores segments to be processed*/
			/* changes in every line marked as %%%%%%%%%*/
			nseg = nseg * nextstroke;
			szMsg.Format( IDS_CON_GOOD, CON_PROCESSING, CON_MESSAGE, itrial, itrial-1); //yi
			OutputMessage( szMsg, RGB( 0, 255, 0 ) );
			if( fLastOnly ) *fPassed = true;
			else *fPassed |= true;

			/************************/
			/* Output current trial */
			/************************/

		    for (iseg = iseg1; iseg < nseg; iseg++)
			{
				for (idata = 0; idata < ndatamax; idata++)
				{
					if( idata == 0 )
					{
						szMsg.Format( _T("%g"), padC.datatmp [idata] [iseg]);
						fprintf( fCon, szMsg );
					}
					else
					{
						szMsg.Format( _T(" %g"), padC.datatmp [idata] [iseg]);
						fprintf( fCon, szMsg );
					}
				}
				fprintf( fCon, _T("\n") );
			}
			/* Write ok if trial is all ok */
			err = _T("OK");
			discardtrial = printandbufferg (err, errbuf);
		}
		/**************************/
		/* Feedback of discarding */
		/**************************/
		discardtrial = printandbufferg( err, errbuf, true );
		fErr.WriteString(err1);
	}//end if while(nextTrial)


	/* Summary info at the end */
	szMsg.Format( IDS_CON_SUMMARY, CON, CON_SUMMARY, CON_MESSAGE, szExtIn, ntrialin, ntrialmax, szErrOut, ntrialdiscarded,
				  100.* ntrialdiscarded / (MAX (ntrialin,1)), ntrialin - ntrialdiscarded); //yi
	OutputMessage( szMsg, LOG_PROC );

Cleanup:

	/* Close files */
	if( fExt.m_pStream ) fExt.Close();
	if( fCon ) fclose( fCon );
	if( fErr.m_pStream ) fErr.Close();

	return fRet;
}

/************************************************/
bool printandbufferg (CString szErr, CString szErrbuf, bool toErr)
/************************************************/
/* 19 May 03: raji: The function now returns true or false depending on whether the trial has to be discarded or not*/
/* If there is an error message then the trial has always to be discarded*/
{
	/* No need of a semicolon in error file*/
	szErrbuf += szErr;

	// eliminate new line
	int nLen = szErrbuf.GetLength();
	if( nLen <= 0 ) return false;
	if( szErrbuf[ nLen - 1 ] == '\n' )
		szErrbuf = szErrbuf.Left( nLen - 1 );

	if( !toErr ) OutputMessage( szErrbuf, LOG_PROC );
	szErrbuf += _T("\n");
	if( toErr && _pErrG && _pErrG->m_pStream ) _pErrG->WriteString( szErrbuf );
	return true;
}

#pragma warning( default: 4996 )

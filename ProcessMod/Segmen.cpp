/*************************************************
   Segmen - version 2.4 - hlt - 14 Feb 1995
**************************************************
   Copyright 1998 Teulings

ARGUMENTS: tf_infile seg_outfile [Debug/addFirst/addLast/...]

WARNING: If debug: no tests for max # segments

PURPOSES:
    (1) Segments tf_file into separate up and down strokes
    (2) Adds significant first/last stroke if addFirst/addLast

COMPILATION:
    (1) MS Visual Studio .NET 2003

UPDATES:
    27Apr94: Tvyzero in seconds instead of sample nrs.
    22Jul94: addFirst, addLast introduced. By default off.
    10Aug94: Infinite loop when interval[0]=stop corrected.
    14Sep94: Updating debugging output in remzerocross/2
    15Sep94: Update debuggin of STOP. Changed RDISTMIN from 0.2 to 0.1
    14Feb94: Bug solved: In very short artifacts first and last segmentation
               could be same. Solved by requiring TIMEMIN
    20Mar95: Simplified begin and end detection; messages improved.
               Small bug in remzerocross2 removed (all but last were shifted if first was stop).
     5Jun95: Screen info of switch settings; little debug mode format problem solved.
             Bug in arguments solved: arguments undetermined because:
             if(arc>=3) if(strstr(argv[3],"d")!=NULL) debug=TRUE; else debug=FALSE;
             Needed changed into:
             debug=FALSE; if(argc>3) if(strstr(argv[3],"d")!=NULL) debug=TRUE;
    18Mar96: RVYMIN2 increased from .1 to .8 for exp WR
     4Sep96: Better diagnostics in small stroke sizes
     6Jan97: Segment primary phase and secondary phases
    16Dec97: vmax based on MAX(-vmin,vmax) - needed in single down stroke
             No ntvyzero reduction if only 2 points left
    18Mar98: Do not segment into strokes with /1
    16Sep98: Included beta from timfun
    19Nov97: Zero data are now ok
    19Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		     removal of statics
	29May00: hlt: Added Tvypeak after which to search for the start of secondary submovement according to Meyer et al.
	05Dec01: Bug fix: Tvypeak corresponds nearly with one Tayzero point Added code to snap in
				so that the subsegmentation point is always one zerocrossing after.
	14jun01 hlt disable debug output to seg file
			hlt option(Verbosity for now) to include ALL tayzero points
	14May03 Raji: Subroutine remayzerocross replaced with a modified block with	Additional condition checking
	        to prevent premature exiting of the loop
	24May03 GMB: Variabalized some constants for use as parameters
	29May03:Changed the debug switch to Consider only one primary and secondary submovement points instead of all zero-crossings
	08Jan03: Raji: New segmentation routine to segment by absolute velocity minima
	09Aug05: GMB: changed [NSEGMAX + 2] to [NSEGMAX +1] because there is only one segmentation point more than # of strokes
				  Also added index checking in the loop to find minima from all the points that are at the threshold level
				  Tests run: modified the 4 main #defines (NSMAX, MSEGMAX, MSEGMAXOUT, MFREQMAX)
							 individually to extremely low values to test for crashes
						RESULTS: no crashes
	18Sep06: GMB: Converted deprecated string/file functions for MSVS7
	08Jul08: GMB: Changed position OR velocity OR accel. spectrum to all 3 - filtered & not
				  TF file now has 6 spectra as opposed to 2
	28Jan09: Aditya & HLT: Absolute velocity segmentation, first stroke detection, discard pen lift
	29Jan09: Aditya & HLT: Move to nearest pendown limited to half a stroke.

************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include <algorithm>
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"
#include "../Common/ProcSettings.h"

/* Minimum stroke size (cm) */
#define ABSDISTMIN 0.05 /* 0.05 from remstop */
double	absdistmin = ABSDISTMIN;
void SetMinStrokeSize( double dVal ) { absdistmin = dVal; }

/* Minimum stroke size (relative to the max stroke size per trial) */
/* distav 0.2; distmax 0.1 sometimes too short 0.05 0.03 seems ok? */
/* 0.2 too short for hpmibr00 */
#define RDISTMIN 0.1
double	rdistmin = RDISTMIN;
/* 18Nov04: hlt: to store max stroke size */
double distmax1 = 0;
void SetMinStrokeSizeRelativeToMax( double dVal ) { rdistmin = dVal; }

/* Minimum stroke duration (s) */
#define TIMEMIN 0.04 /* 0.05 from remstop */
double	timemin = TIMEMIN;
void SetMinStrokeDuration( double dVal ) { timemin = dVal; }

/* Minimum vertical velocity of the coarse beginning/end of the first/last stroke (relative to the vertical peak velocity per trial) */
#define RVYMIN 0.05 /* 0.1 too small; 0.2 is too big */
double	rvymin = RVYMIN;
void SetMinVertVelocity( double dVal ) { rvymin = dVal; }

/* Maximum vertical velocity of the beginning/end of the first/last stroke at the beginning/end of a trial (relative to the vy peak per trial) */
/* 0.1 seems very small. Loosing the first stroke is a big problem                   */
/* when the pattern has a specific sequence and all replications could be ruled out. */
/* Till 18Mar96 0.10 was used. However, some strokes seem long enough but start from the air */
/* Used to use 0.8 which seems high */
#define RVYMIN2 0.1
double	rvymin2 = RVYMIN2;
//void SetMaxVertVelocity( double dVal ) { rvymin2 = dVal; }
/* now we use the setting from experiment settings for the absolute noise level*/
#define VLEVMIN 0.1
double vlevmin = VLEVMIN;
void SetMaxVertVelocity( double dVal ) { vlevmin = dVal; }

#define	NODECIMATE	1

#define	NODECIMATE	1
#define UP +1
#define DOWN -1
#define STOP 0

/* specific routines - supporting */
int indup (double* vy, int nsvy, double tstart, double vylev, bool segvabs);
int indupe (double* vy, int nsvy, double tstart, double vylev, bool segvabs);
double rindmin0 (double* vy, int nsvy, double tstart, bool lastzero);
double rindmin0e (double* vy, int nsvy, double tstart);
double rind0 (double* vy, int nsvy, double tstart);
double rind0e (double* vy, int nsvy, double tstart);
double rindlev (double* vy, int nsvy, double tstart, double tvlevmin);
double rindelev (double* vy, int nsvy, double tstart, double tvlevmin);
int nzerocross (double* vy, int nsvy, double* tvyzero, int ntvyzeromax);
double distmax (double* x, double* y, int ns, double* tvyzero, int ntvyzero, int itimemin);
int remzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
				  double distmin, int debug);
int remzerocross2 (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
				   double distmin, int itimemin, int debug);
int addlastzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
					  double distmin, int itimemin);
int addfirstzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
					   double distmin, int itimemin);
int remayzerocross (double* tayzero, int ntayzero, double* tvyzero, double* tvypeak, int ntvyzero,
					double* tayzeroout, int ntayzeroout);
void segmov2z(double *tvyzero, double *z, int ntvyzero, double prsmin);
void segmov2min(double *tvyzero, int ntvyzero, double *vabs, double *x, double *y, int ns, double distmin, int itimemin);
/*functions for new routine*/
int inddip(double *x, int n, int nstops, int idt, double *tvabsbot, int nsegmax);
double rnpolq(double *y, int ns, double rind, double *rinde, double *yrinde);
int remmin (double* x, double* y, double* vabs, double* tvabsmin, int ntvabsmin, double vlev, double* vabspeaks, double distmin);

#define	MODULE		_T("SEGMENT")

/************************************************************************/
bool Segment( CString szTFIn, CString szSegOut, unsigned int nSwitch )
/************************************************************************/
{
	bool fRet = TRUE;
	/* # segmentation points is NSEGMAX+1 and addlastzerocross may want to add
	one point */
	double tvyzerolast = 0.0;
	double tvyzerofirst = 0.0;
	int ntvyzero = 0, ntayzero = 0;
	/* 21jun01 hlt Added itayzero */
	int itvyzero = 0, itayzero = 0;
	int nsx = 0, nsy = 0, nsz = 0;
	int nsaspec = 0, nsaspecfilt = 0;
	int nsvx = 0,nsvy = 0, nsvabs = 0;
	int nsax = 0, nsay = 0;
	int nsec = 0, nprsmin = 0;
	int nbeta = 0;
	CString szMsg, szTxt;
	FILE *fTF = NULL, *fSeg = NULL;
	static double vylev = 0.0, vylev2 = 0.0;
	double vymin = 0.0, vymax = 0.0;
	double vabsmin = 0.0, vabsmax = 0.0;
	double distmin = 0.0;
	int itimemin = 0;
	double sec = 0.0, prsmin = 0.0;
	bool debug = FALSE, addlast = FALSE, addfirst = FALSE,
		 secondary = FALSE, onestroke = FALSE;
	double beta = 0.0;
	/* 29May00: hlt: Added tvypeak */
	int isam = 0, isam1 = 0, isam2 = 0;
	double vypeakneg = 0.0, vypeakpos = 0.0;
	double tvypeakneg = 0.0, tvypeakpos = 0.0;

	// [ NSEGMAX + 1 ] was formerly [ NSEGMAX + 2 ]
	// this makes no sense as we only have 1 segmentation point more than strokes
	double* x = new double[ NSMAX ];
	double* y = new double[ NSMAX ];
	double* z = new double[ NSMAX ];
	double* aspec = new double[ NFREQMAX ];
	double* aspecfilt = new double[ NFREQMAX ];
	double* vx = new double[ NSMAX ];
	double* vy = new double[ NSMAX ];
	double* vabs = new double[ NSMAX ];
	double* ax = new double[ NSMAX ];
	double* ay = new double[ NSMAX ];
	double* tvyzero = new double[ NSEGMAX + 1 ];
	/* 29May00: hlt: Added tvypeak */
	double* tvypeak = new double[ NSEGMAX + 1 ];
	double* tayzero = new double[ NSEGMAX + 1 ];
	double* tayzeroout = new double[ NSEGMAX + 1 ];

	/*Definitions for new routine*/
	bool segvabs = 0;
	bool segacc = 0;
	//bool vlevtest = 1;
	//double vlevmin = 0.0;
	int ntvabsmin = 0;
	double vabslev = 0.0, vabslev2 = 0.0, rvabsmin = 0.0, rvabsmin2 = 0.0, vabspeakpos = 0.0, tvabspeakpos = 0.0;
	int itvabsmin = 0, ntaymin = 0, itaymin = 0, index = 0;
	int ntvabssec = 0;
	double *tvabsmin= new double[ NSEGMAX + 1 ];
	double* tvabspeak = new double[ NSEGMAX + 1 ];
	double* tvabssec = new double[ NSEGMAX + 1 ];
	double* tvabsminout= new double[ NSEGMAX + 1 ];
	double* tvabssecout = new double[ NSEGMAX + 1 ];
	double* tz = new double[ NSEGMAX + 1 ];
	double tvabslast = 0.0;
    int isampl_begin = 0, isampl_end = 0;
    double cblength = 0.0;
	double tvyzerocoarse = 0.0, tvabsmincoarse = 0.0, tvyzerolastcoarse = 0.0, tvabslastcoarse = 0.0;
	double tvlevmin = 0.0;
	bool segmovtoz = FALSE;
	bool vlevtest = FALSE;
	bool vylastzero = FALSE;
	/**************/
	int itz = 0;
	bool segz = FALSE;
	int ntz = 0;
	bool segmovtomin = FALSE;
	/*************/

	/* 07Jan03: gmb: failure to initalize resulted in differing data */
	memset( x, 0, sizeof( double ) * NSMAX );
	memset( y, 0, sizeof( double ) * NSMAX );
	memset( z, 0, sizeof( double ) * NSMAX );
	memset( aspec, 0, sizeof( double ) * NFREQMAX );
	memset( aspecfilt, 0, sizeof( double ) * NFREQMAX );
	memset( vx, 0, sizeof( double ) * NSMAX );
	memset( vy, 0, sizeof( double ) * NSMAX );
	memset( vabs, 0, sizeof( double ) * NSMAX );
	memset( ax, 0, sizeof( double ) * NSMAX );
	memset( ay, 0, sizeof( double ) * NSMAX );
	memset( tvyzero, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tvypeak, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tayzero, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tayzeroout, 0, sizeof( double ) * ( NSEGMAX + 1 ) );

	/*Initializations for new routine*/
	memset( tvabsmin, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tvabspeak, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tvabssec, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tvabsminout, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tvabssecout, 0, sizeof( double ) * ( NSEGMAX + 1 ) );
	memset( tz, 0, sizeof( double ) * ( NSEGMAX + 1 ) );

/* 18Jun04: gmb: added debug-version ini file for features not yet in db or gui */
/* file format: varName (SPACE) value (SPACE) comments */
	/************/
	/* Switches */
	/************/
	/* 29May00: hlt: can this switch be controlled via the Processing Settings? */
	if( ( nSwitch & SEG_SWITCH_D ) != 0 )
	{
		debug = TRUE;
		szMsg.Format( IDS_SEG_DEBUG, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_F ) != 0 )
	{
		addfirst = TRUE;
		szMsg.Format( IDS_SEG_AF, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_L ) != 0 )
	{
		addlast = TRUE;
		szMsg.Format( IDS_SEG_AL, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_S ) != 0 )
	{
		secondary = TRUE;
		szMsg.Format( IDS_SEG_SEC, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_O ) != 0 )
	{
		onestroke = TRUE;
		szMsg.Format( IDS_SEG_ONE, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_M ) != 0 )
	{
		segvabs = TRUE;
		szMsg.Format( IDS_SEG_AM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_A ) != 0 )
	{
		segacc = TRUE;
		szMsg.Format( IDS_SEG_AZ, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_V ) != 0 )	// Move segmentation point to nearest pendown if on a penlift
	{
		segmovtoz = 1;
		// TODO: Move to resources
		szMsg.Format( _T(IDS_SEG_AV), MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_MM ) != 0 )
	{
		segmovtomin = TRUE;
		// TODO: Move to resources
		szMsg.Format( IDS_SEG_AMM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & SEG_SWITCH_J ) != 0 )
	{
		segz = TRUE;
		// TODO: Move to resources
		szMsg.Format( IDS_SEG_AJ, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	/* Enable the noise level switch if the noise level setting is set to some value other than 0*/
 	if (vlevmin > 0.)
        vlevtest = TRUE;
	/*if acceleration by zerocrossings, make to default*/
	if (segacc)
	{
		segacc = FALSE;
		segvabs = FALSE;
		/* now it will only vertical velocity zero crossings*/
	}

    /**************/
	/* Input file */
	/**************/
	if( ( fopen_s( &fTF, szTFIn, "r") ) != 0 )
	{
		szMsg.Format( IDS_SEGINP_ERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/***  read  ***/
	nsec = getdata (fTF, &sec, 1, szTxt, NODECIMATE);
	// 10Sep03: GMB - Added assert to ensure validity of value
	ASSERT( sec > 0 );
	nprsmin = getdata (fTF, &prsmin, 1, szTxt, NODECIMATE);
	nbeta = getdata (fTF, &beta, 1, szTxt, NODECIMATE);
	nsx = getdata (fTF, x, NSMAX, szTxt, NODECIMATE);
	nsy = getdata (fTF, y, NSMAX, szTxt, NODECIMATE);
	nsz = getdata (fTF, z, NSMAX, szTxt, NODECIMATE);
	nsaspec = getdata (fTF, aspec, NFREQMAX, szTxt, NODECIMATE);
	nsaspecfilt = getdata (fTF, aspecfilt, NFREQMAX, szTxt, NODECIMATE);
// 08Jul08: GMB: above 2 are for position spectra, next 4 are for velocity & accel
	nsaspec = getdata (fTF, aspec, NFREQMAX, szTxt, NODECIMATE);
	nsaspecfilt = getdata (fTF, aspecfilt, NFREQMAX, szTxt, NODECIMATE);
	nsaspec = getdata (fTF, aspec, NFREQMAX, szTxt, NODECIMATE);
	nsaspecfilt = getdata (fTF, aspecfilt, NFREQMAX, szTxt, NODECIMATE);
	nsvx = getdata (fTF, vx, NSMAX, szTxt, NODECIMATE);
	nsvy = getdata (fTF, vy, NSMAX, szTxt, NODECIMATE);
	/* needed for secondary */
	nsvabs = getdata (fTF, vabs, NSMAX, szTxt, NODECIMATE);
	nsax = getdata (fTF, ax, NSMAX, szTxt, NODECIMATE);
	nsay = getdata (fTF, ay, NSMAX, szTxt, NODECIMATE);

	fclose( fTF );
	fTF = NULL;

	/* tests */
	if (nsx != nsy || nsx != nsz || nsx != nsvx || nsx != nsvy || nsx != nsvabs ||
		nsx != nsax || nsx != nsay || nsec != 1 ||
		nprsmin != 1 || nbeta != 1)
	{
		szMsg.Format( IDS_SEGLEN_ERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/*********************/
	/*** Open Output    ***/
	/*********************/
	/* Put here to allow writing debug info */
	if( ( fopen_s( &fSeg, szSegOut, "w" ) ) != 0 )
	{
		szMsg.Format( IDS_SEGOUT_ERR, MODULE, szSegOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}

	/* Test & Make all segmentation parameters positive*/
	if(absdistmin < 0.)
		absdistmin = fabs (absdistmin);
	if(rdistmin < 0.)
		rdistmin = fabs (rdistmin);
	if(rvymin < 0.)
		rvymin = fabs (rvymin);
	if(rvymin2 < 0.)
		rvymin2 = fabs (rvymin2);
	if(timemin < 0.)
		timemin = fabs (timemin);

	/* Guess the minimum relevant stroke length */
	itimemin = (int)(timemin / sec);

	/********************************************************************************/
	if (!segvabs && !segz)/*Segment by zero crossings of vertical velocity*/
	{
		/******************/
		/*** Segmenting ***/
		/******************/
		/* dynamic range and v level */
		rmima (vy, nsvy, &vymin, &vymax);
		/* vmax is the absolute max. Important in single down stroke */
		vymax = MAX (-vymin, vymax);
		vylev = rvymin * vymax;
		//vylev2 = rvymin2 * vymax;

		/****************************/
		/* Begin of first up stroke */
		/****************************/
		/* first upward/downward velocity */
		tvyzerocoarse  = indup (vy, nsvy, 0., vylev, segvabs);
		/* backwards till zero crossing or minimum */
		tvyzero [0] = rindmin0e (vy, nsvy, tvyzerocoarse);
		/* If switch enabled, backwards until reaching minimum velocity requirement vlevmin*/
		/* Compare with the prev begin point and choose the max value*/
		/* Dont check for minimum velocity level for now*/
		if (vlevtest)
		{
			tvlevmin = rindelev (vy, nsvy, tvyzerocoarse, vlevmin);
			if (tvlevmin > tvyzero [0])
				tvyzero [0] = tvlevmin;
		}
		/* If acc. switch enabled, backwards until acceleration zero crossing*/
		/* Compare with the prev begin point and choose the max value*/

		/**********************/
		/* End of last stroke */
		/**********************/
		/* first upward/downward velocity from the end sample*/
 		tvyzerolastcoarse = (double) indupe (vy, nsvy, (double) (MAX (0, nsvy - 1)), vylev, segvabs);
		/* Forwards till zero crossing or minimum */
		tvyzerolast = rindmin0 (vy, nsvy, tvyzerolastcoarse, vylastzero);
		/* Never let the last point go beyond the first point while searching backwards*/
		if (tvyzerolast <= tvyzero [0])
			tvyzerolast = nsvy - 1;
		/* If switch enabled, forwards until reaching minimum velocity requirement vlevmin*/
		/* Compare with the prev end point and choose the min value*/
		/*Dont check for minimum velocity level for now*/
		if (vlevtest)
		{
			tvlevmin = rindlev (vy, nsvy, tvyzerolastcoarse, vlevmin);
			if ((tvlevmin < tvyzerolast))
				tvyzerolast = tvlevmin;
		}

		/**********************/
		/* All zero crossings */
		/**********************/
		if (debug) OutputMessage( IDS_SEG0_VY, LOG_PROC );
		ntvyzero = nzerocross (vy, (int) tvyzerolast, tvyzero, NSEGMAX);

		/* I do not know why nzerocross does not find the last point in the array, only necessary if we are not yet at the end */
		// It is ununderstandable why in kqghmb06, kqghmd04, kqghmg05 with equal values the unequality is true.
        /* 18Nov04: hlt: Store distmax1 for output lines */
		distmax1 = distmax (x, y, nsvy, tvyzero, ntvyzero, itimemin);
		distmin = rdistmin * distmax1;
		if (debug)
		{
			szMsg.Format( IDS_SEG_MIN1, timemin, distmin, rdistmin);
			OutputMessage( szMsg, LOG_PROC );
		}
		/* raji: divided by operation leads to crash if rdistmin = 0*/
        /* 18Nov04: hlt: we had distmin/rdistmin to reconstruct distmax(), which gives problems when rdistmin=0 */
        /* Solution implemented: remember distmax() in distmax1 */
		if (distmin < absdistmin)
		{
			szMsg.Format( IDS_SEG_MIN2, ntvyzero, distmax1, distmin, absdistmin);
			OutputMessage( szMsg, LOG_PROC );
			distmin = absdistmin;
		}
		/************************/
		/* Assign the last point*/
		/************************/
		/* add last point at tvabslast*/
		if (ntvyzero > 0 && ntvyzero < NSEGMAX)
		{
			/* Add last only if it is a zero crossing*/
			// 27Oct08: Somesh: If statement, quick fix for erroneous case when O/P segpoint1 > segpoint2
			// The problem is actually in indupe where end of last stroke is calculated.
			// indupe may be rewritten cleanly later.
			if (tvyzerolast > tvyzero [ ntvyzero - 1] )
			{
			tvyzero [ntvyzero] = tvyzerolast;
			ntvyzero++;
			}
		}
		/* Remove clusters of zero crossings with stroke lengths less than distmin */
		if (debug) OutputMessage( IDS_SEGRZ, LOG_PROC );
		if (ntvyzero > 2)
			ntvyzero = remzerocross (x, y, nsvy, tvyzero, ntvyzero, distmin, debug);


		/* 21jun01 hlt Disable writing debug data to .seg file */
		//if (debug)
		//	putdata (fSeg, tvyzero, ntvyzero, IDS_SEGRZ_REM, (double) sec);

		/* add last one if sufficiently long, though it did not really reach zero */
		/* this is a potential problem if zero is by no ways reached */
		if (addlast)
			ntvyzero = addlastzerocross (x, y, nsvy, tvyzero, ntvyzero, distmin, itimemin);

		/* 19Feb04:Raji: Disable writing points to fSeg if Verbosity (debug) is ON*/
		//if (debug)
		//	putdata (fSeg, tvyzero, ntvyzero, IDS_SEGRZ_AL, (double) sec);

		/* remove last stroke if nonsignificant but the previous one was significant */
		/* this could also be done in remzerocross */

		/* add first segment if sufficiently long, though it did not really reach zero */
		if (addfirst)
			ntvyzero = addfirstzerocross (x, y, nsvy, tvyzero, ntvyzero, distmin, itimemin);
		/* 21jun01 hlt Disable writing debug data to .seg file */
		//if (debug)
		//	putdata (fSeg, tvyzero, ntvyzero, IDS_SEGRZ_AF, (double) sec);

		/****************/
		/* Remove stops */
		/****************/
		/* remove segments with stop by adding them to the right stroke */
		if (debug) OutputMessage( IDS_SEGRS, LOG_PROC );
		if (ntvyzero > 2)
			ntvyzero = remzerocross2 (x, y, nsvy, tvyzero, ntvyzero, distmin, itimemin, debug);

		// 10Sep03: GMB - Added assert to ensure validity of indices
		/* 01sep04: raji: this causes a crash if there are no segmentation points found which could very well happen*/
		/* disabling doesnot cause any troubles as such*/
		//ASSERT( ntvyzero >= 1 );

		/* 21jun01 hlt Disable writing debug data to .seg file */
		//if (debug)
		//	putdata (fSeg, tvyzero, ntvyzero, IDS_SEGRS_REM, (double) sec);

		/*****************************************/
		/* Combine all approved strokes into one */
		/*****************************************/
		/* Handier to do here because half finished strokes need to be excluded */
		// e.g.: In case of sixty degree visual feedback rotation, you sometimes get many strokes
		// to reach the target. These strokes need to be combined into one.

		// 13Oct10: Somesh: If num of seg points is just 1, there is no stroke
		//if (onestroke)
		if (onestroke && ntvyzero > 1)
		{
			szMsg.Format( IDS_SEG_OS, ntvyzero - 1);
			OutputMessage( szMsg, LOG_PROC );
			// 27Oct08: Somesh: Treat entire recording till end as one stroke
			tvyzero [1] = tvyzero [ntvyzero - 1];
			// 01Oct10: Somesh: The following change was made on 27 Oct 08. This is wrong. Reverting back to old code.
			//tvyzero [1] = nsvy - 1;
			ntvyzero = 2;
			/* Just take the first ayzero because that is done within a stroke anyway */
			ntayzero = 1;
		}
		/***Moving of points****/
		/*1. Move to the nearest pendown point*/
		if (segmovtoz)
		{
			 segmov2z(tvyzero, z, ntvyzero, prsmin);
		}
		/*2. Move to the nearest minima*/
		if (segmovtomin)
		{
            segmov2min(tvyzero, ntvyzero, vabs, x, y, nsx, distmin, itimemin);
		}

		/**************************************/
		/*** Secondary submovement analysis ***/
		/**************************************/
		/* 29May00: hlt: All submovement segmentaiton stuff concentrated here */
		/* and updated with the vypeak criterion */
		/* Find all times of maximum absolute velocity within the stroke */
		/**********************************************************************/
		if (secondary)
		{
			/* Do for each stroke */
			for (itvyzero = 0; itvyzero < ntvyzero - 1; itvyzero++)
			{
				/************************************/
				/* Times of peakvelocity per stroke */
				/************************************/
	#define ROUNDUP 0.999
				/* First and last sample */
				isam1 = (int)(tvyzero [itvyzero] + ROUNDUP);
				isam2 = (int)(tvyzero [itvyzero + 1] + ROUNDUP);

				// 10Sep03: GMB - Added assert to ensure validity of indices
				ASSERT( isam1 >= 0 );

				/* Find positive and negative peaks */
				vypeakneg = vy [isam1];
				vypeakpos = vy [isam1];

				for (isam = isam1; isam < isam2; isam++)
				{
					if (vypeakneg > vy [isam])
					{
						vypeakneg = vy [isam];
						tvypeakneg = isam;
					}

					if (vypeakpos < vy [isam])
					{
						vypeakpos = vy [isam];
						tvypeakpos = isam;
					}
				}
				/* Take the largest peak */
				if (isam2 - isam1 > 0)
				{
					if (y [(int)tvyzero[itvyzero+1]] - y [(int)tvyzero[itvyzero]]> 0.)
					{
						tvypeak [itvyzero] = tvypeakpos;
					}
					else
					{
						tvypeak [itvyzero] = tvypeakneg;
					}
				}
				else
				{
					tvypeak [itvyzero] = 0.;
				}
			}

			/* 21jun01 hlt Disable writing debug data to .seg file */
			//if (debug)
			//{
	//// 29May00: Not yet in processmod.rc
	//#define IDS_SEG_VYPEAK "tvypeak (sec)"
			//	putdata (fSeg, tvypeak, ntvyzero - 1, IDS_SEG_VYPEAK, (double) sec);
			//}

			/* Set first crossing at tvyzero and find all following ones */
			/* This is actually not a real tayzero point */
			tayzero [0] = tvyzero [0];
			if (debug) OutputMessage( IDS_SEG0_AY, LOG_PROC );
			ntayzero = nzerocross (ay, (int) tvyzerolast, tayzero, NSEGMAX);

			/* 21jun01 hlt Disable writing debug data to .seg file */
			//if (debug)
			//{
	//// 29May00: Not yet in processmod.rc
	//#define IDS_SEG_TAYZERO "tayzero (sec)"
			//	putdata (fSeg, tayzero, ntayzero, IDS_SEG_TAYZERO, (double) sec);
			//}
			/*******************************/
			/* Remove zero crossings of ay */
			/*******************************/
			/* Remove first ay zero crossing, leave second, and remove all others within a stroke */
			if (debug) OutputMessage( IDS_SEGZC_AY, LOG_PROC );
			// 21jun01 hlt I found it too risky to do the removal in tayzero */
			/* so if we want to see all ayzero crossings we need to copy the array */
			/* TODO: Instead of the debug switch we want one in the segmen processing settings */
			/*19Feb04:Raji: disable calculating as we dont write this to results window anyway
			/* All submovements instead of one primary and one secondary submovement */
			//if (debug)
				//for (itayzero = 0; itayzero < ntayzero; itayzero++)
				//tayzeroout [itayzero] = tayzero [itayzero];
			//else
				ntayzero = remayzerocross (tayzero, ntayzero, tvyzero, tvypeak, ntvyzero, tayzeroout, NSEGMAX);
				/* 14 May 03 hlt: PATCH: If ntayzero turns out less than one less than ntvyzero, then most likely this is caused */
				/* by the last stroke not being a stroke with a ay zero point and should be dropped */
				ntvyzero = ntayzero + 1;
		}
		/**********/
		/* Output */
		/**********/
		szMsg.Format( IDS_SEG_OUT, MODULE, szSegOut, ntvyzero - 1);
		OutputMessage( szMsg, LOG_PROC );
		putdata (fSeg, tvyzero, ntvyzero, IDS_SEGOUT_VY, (double) sec);
		if (secondary)
		{
			/* It is possible that the second tayzero is outside the interval of the stroke */
			/* In that case tayzeroout receives the last tvyzero so that the secondary interval is exactly zero */
			if (ntayzero <= 0 && ntvyzero > 0)
			{
				tayzeroout [0] = tvyzero [ntvyzero - 1];
				ntayzero = 1;
			}
			putdata (fSeg, tayzeroout, ntayzero, IDS_SEGOUT_AY, (double) sec);
		}
	}
	/********************************************************************************/
	else if(segvabs)/*Segment by minima of absolute velocity*/
	{
		/******************/
		/*** Segmenting ***/
		/******************/
		/* The range for vabs specified by same variables as for vy, from the experiment settings*/
		rvabsmin = rvymin;
		//rvabsmin2 = rvymin2;
		/* dynamic range and v level */
		rmima (vabs, nsvabs, &vabsmin, &vabsmax);
		/* vmax is the absolute max. Important in single down stroke */
		vabsmax = MAX (-vabsmin, vabsmax);
		vabslev = rvabsmin * vabsmax;
		//vabslev2 = rvabsmin2 * vabsmax;

		/* Guess the minimum relevant stroke length duration */
		itimemin = (int)(timemin / sec);
		/* segstop = 1: segmentation points at stops by using a threshold velocity, else, include all minima (old method)*/
		//bool segstop = TRUE;
		bool segstop = FALSE; //hlt:21Jan09 added to test old method again
		if (!segstop)
		{
			/****************************/
			/* Begin of first stroke */
			/****************************/
			/* first upward/downward velocity */
			// TODO Why is segvabs needed?
			tvabsmincoarse = indup (vabs, nsvabs, 0., vabslev, segvabs);
			/* Backwards till minimum */
			tvabsmin [0] = rindmin0e (vabs, nsvabs, tvabsmincoarse);

			/**********************/
			/* End of last stroke */
			/**********************/
			/* first upward/downward velocity */
			// TODO Why is segvabs needed?
			tvabslastcoarse = (double) indupe (vabs, nsvabs, (double) (MAX (0, nsvabs - 1)), vabslev, segvabs);

			/* Forwards till minimum */
			tvabslast = rindmin0 (vabs, nsvabs, tvabslastcoarse, vylastzero);

			/* Never let the last point go beyond the first point while searching backwards*/
			if (tvabslast <= tvabsmin [0])
				tvabslast = nsvabs - 1;

			/* Get number of local minima and the array of points*/
			ntvabsmin = inddip(vabs, nsvabs, (int)tvabslast, itimemin, tvabsmin, NSEGMAX);
		}
		else
		{
			/****************************/
			/* Stroke Segmentations     */
			/****************************/
			tvabsmincoarse = 0.;
			tvabslastcoarse = 0.;
			/*Find minima from all the points that are at the threshold level*/

			while( true )
			{
				/* Searching from begin*/
				/* first upward/downward velocity */
				tvabsmincoarse = indup (vabs, nsvabs, tvabslastcoarse, vabslev, segvabs);
				if ((int)(tvabsmincoarse) >= nsvabs)
					break;
				/* Backwards till minimum */
				tvabsmin [ntvabsmin] = rindmin0e (vabs, nsvabs, tvabsmincoarse);
				ntvabsmin++;
				// 09Aug05 - GMB: Added check for index
				if( ( ntvabsmin < 0 ) || ( ntvabsmin > NSEGMAX ) )
					break;
				for (int i = (int)(tvabsmincoarse); i < nsvabs; i++)
				{
					if (vabs [i] < vabslev)
					{
						tvabslastcoarse = (double)(i);
						break;
					}
					else
						tvabslastcoarse = nsvabs;
				}
				if ((int)(tvabslastcoarse) >= nsvabs)
					break;
				/* Forwards till minimum */
				tvabsmin [ntvabsmin] = rindmin0 (vabs, nsvabs, tvabslastcoarse, vylastzero);
				ntvabsmin++;
				// 09Aug05 - GMB: Added check for index
				if( ( ntvabsmin < 0 ) || ( ntvabsmin > NSEGMAX ) )
					break;
			}
		}

		/******************/
		/* Add last point*/
		/*****************/
		if (ntvabsmin  > 0 && ntvabsmin  < NSEGMAX)
		{
			tvabsmin [ ntvabsmin ] = tvabslast;
			ntvabsmin ++;
		}

		/* Never let the last point go beyond the first point while searching backwards*/
		if ((ntvabsmin > 2) && (tvabsmin [ntvabsmin - 1] <= tvabsmin [0]))
			tvabsmin [ntvabsmin - 1] = nsvabs - 1;

		/*****************************************/
		/* Add first segment and/or Last segment */
		/*****************************************/
		// hlt: 28Jan09: moved. Was before Remove Stops
		/* add last one if sufficiently long, though it did not really reach zero */
		/* this is a potential problem if zero is by no ways reached */
		if (addlast)
			ntvabsmin = addlastzerocross (x, y, nsvabs, tvabsmin, ntvabsmin, distmin, itimemin);
		/* remove last stroke if nonsignificant but the previous one was significant */
		/* this could also be done in remzerocross */
		/* add first segment if sufficiently long, though it did not really reach zero */
		if (addfirst)
			ntvabsmin = addfirstzerocross (x, y, nsvabs, tvabsmin, ntvabsmin, distmin, itimemin);



		/*****************************/
		/* Remove some segmentations */
		/*****************************/

		// TODO Check this is doing what it says
		/*********************************************************************************/
		/* Remove first segment if starting with higher than threshold absolute velocity */
		/*********************************************************************************/
		/* No point removing points if there is only begin and end */
		/* 18Nov04: hlt: Store distmax1 for output lines */
		// TODO: Why is this not based on tvabsmin[] ?  BUG?
		//distmax1 = distmax (x, y, nsvy, tvyzero, ntvyzero, itimemin);

		// 02Feb09: Somesh: Use vabs parameters instead of vy
		distmax1 = distmax (x, y, nsvabs, tvabsmin, ntvabsmin, itimemin);
		distmin = rdistmin * distmax1;
		if (debug)
		{
			szMsg.Format( IDS_SEG_MIN1, timemin, distmin, rdistmin);
			OutputMessage( szMsg, LOG_PROC );
		}
		/* raji: divided by operation leads to crash if rdistmin = 0*/
        /* 18Nov04: hlt: we had distmin/rdistmin to reconstruct distmax(), which gives problems when rdistmin=0 */
        /* Solution implemented: remember distmax() in distmax1 */
		if (distmin < absdistmin)
		{
            szMsg.Format( IDS_SEG_MIN2, ntvyzero, distmax1, distmin, absdistmin);
			OutputMessage( szMsg, LOG_PROC );
			distmin = absdistmin;
		}
		/*********************************************************************/
		/* Remove clusters of segments with stroke lengths less than distmin */
		/*********************************************************************/
		if (debug) OutputMessage( IDS_SEGRZ, LOG_PROC );
		ntvabsmin = remzerocross (x, y, nsvabs, tvabsmin, ntvabsmin, distmin, itimemin);
		//ntvabsmin = remmin(x, y, vabs, tvabsmin, ntvabsmin, vabslev, vabspeaks, distmin);

		/****************/
		/* Remove stops */
		/****************/
		/* remove segments with stop by adding them to the right stroke */
		if (debug) OutputMessage( IDS_SEGRS, LOG_PROC );
			//ntvabsmin = remzerocross2 (x, y, nsvabs, tvabsmin, ntvabsmin, distmin, itimemin,debug);


		/*****************************************/
		/* Combine all approved strokes into one */
		/*****************************************/
		/* Handier to do here because half finished strokes need to be excluded */
		if (onestroke)
		{
			//szMsg.Format( IDS_SEG_OS, ntvyzero - 1);
			//OutputMessage( szMsg, LOG_PROC );
			tvabsmin [1] = tvabsmin [ntvabsmin - 1];
			ntvabsmin = 2;
		}

		//02Feb09: Somesh: Moving WAS after removal of strokes
		/********************/
		/* Moving of points */
		/********************/
		/**************************************/
		/*1. Move to the nearest pendown point*/
		/**************************************/
		if (segmovtoz)
		{
			 segmov2z(tvabsmin, z, ntvabsmin, prsmin);
		}
		/*******************************/
		/*2. Move to the nearest minima*/
		/*******************************/
		/* Not applicable*/

		/************************/
		/* Submovement analysis */
		/************************/
		ntvabssec = 0;
		if (secondary) /* Secondary Analysis*/
		{
			/* Do for each stroke */
			for (itvabsmin = 0; itvabsmin < ntvabsmin; itvabsmin++)
			{
				/*Number of secondary points per segment*/
				int isec = 0;
				/************************************/
				/* Times of peakvelocity per stroke */
				/************************************/
	#define ROUNDUP 0.999
				/* First and last sample */
				isam1 = (int)(tvabsmin [itvabsmin] + ROUNDUP);
				isam2 = (int)(tvabsmin [itvabsmin + 1] + ROUNDUP);

				// 10Sep03: GMB - Added assert to ensure validity of indices
				ASSERT( isam1 >= 0 );

				/* Find positive peaks */
				vabspeakpos = vabs [isam1];

				for (isam = isam1; isam < isam2; isam++)
				{
					if (vabspeakpos < vabs[isam])
					{
						vabspeakpos = vabs [isam];
						tvabspeakpos = isam;
					}
				}
				/* Take the largest peak */
				if (isam2 - isam1 > 0)
					tvabspeak [itvabsmin] = tvabspeakpos;
				else
					tvabspeak [itvabsmin] = 0.;

				/***secondary point = the next min point after max peak */
				for (isam = (int)tvabspeakpos; isam < isam2; isam++)
				{
					if(vabs[isam + 1] > vabs [isam]&& isec < 1)
					{
						tvabssec [itvabsmin] = isam;
						isec++;
					}
				}
				/* If no min point found after peak, assign the secondary point to minima*/
				if(!tvabssec [itvabsmin])
				{
					tvabssec [itvabsmin] = tvabsmin [itvabsmin + 1];
   				}
				ntvabssec++;
			}
		}

		/************************************/
		/* Remove points where pressure = 0 */
		/************************************/
		// This needs done after points have been moved to points with pressure.
		// hlt: 28Jan09: Added
		bool no0pressure = TRUE;
		int itvabsminnew = 0;
		if (no0pressure) {
			itvabsminnew = 0;
			for (itvabsmin = 0; itvabsmin < ntvabsmin; itvabsmin++)
			{
				// TODO check it is <= prsmin or < prsmin
				if (z [(int) (tvabsmin [itvabsmin] + .5)] > prsmin) // penlift
				{
					tvabsmin [itvabsminnew] = tvabsmin [itvabsmin];
					itvabsminnew++;
				}
			}
			// TODO: Results output showing how many (or even which) points were removed and why
			ntvabsmin = itvabsminnew;
		}

		/**********/
		/* Output */
		/**********/
		int iseg = 0;
		bool write = 0;
		putdata (fSeg, tvabsmin, ntvabsmin, IDS_SEGOUT_VABS, (double) sec);
		if(secondary)
			putdata (fSeg, tvabssec, ntvabssec - 1, IDS_SEGOUT_VABSSEC, (double) sec);

	}
	/********************************************************************************/
	else if (segz)
	{
		// 22Feb10: Somesh: Add first point at any rate
		/* First point */
		if (nsz > 0)
		{
			tz[ntz] = 0.;
			ntz++;
		}

		/****************************************/
		/* Segmentation based on pen pressure z */
		/****************************************/
		// 09Jun09: Somesh: Was itz <= nsz
		// 22Feb10: First point already assigned as segment. Loop beginning changed from itz=0 to itz=1
		//for (itz = 0; itz < nsz; itz++)
		for (itz = 1; itz < nsz; itz++)
		{
			/* First point has been found*/
			//if (ntz > 0)
			//{
				// 30jan09: hlt: Switch to saying min or less = penlift
				//if (((z[itz-1] < prsmin) && (z[itz] >= prsmin)) || ((z[itz] >= prsmin) && (z[itz+1] < prsmin)))

				// 22Feb10: Somesh
				// Consider SampleNumber PendownPD or PenupPU
				// 1PD 2PD 3PD 4PU 5PU 6PU 7PU 8PD 9PD 10PD
				// Segmentation points: 4PU, 8PD
				// i.e. begin point of Penup or begin point of Pendown is the segmentation point
				// --NOTE: 1PD, 10 PD i.e. First and last segment points added later
				if (((z[itz-1] <= prsmin) && (z[itz] > prsmin)) || ((z[itz-1] > prsmin) && (z[itz] <= prsmin)))
				{
					tz [ntz] = (double) itz;
					ntz++;
				}
			//}

			// 22Feb10: Somesh: Moved to add first point at any rate
			// 09Jun09: Somesh: Interchanged position of first point and no first point loops
			// so that if first point found, itz is incremented and z [itz-1] does not baulk
			/* First point*/
			//if ((ntz == 0) && (z [itz] > prsmin)) //pendown
			//{
			//	tz [ntz] = (double) itz;
			//	ntz++;
			//}
		}

		// 22Feb10: Somesh: Add last point at any rate
		/* Last point */
		if ( (ntz >= 0) && (tz [ntz] != (double) (nsz-1)) )
		{
			tz [ntz] = (double) (nsz-1);
			ntz++;
		}

		/***Moving of points****/
		/*1. Move to the nearest pendown point*/
		if (segmovtoz)
		{
			//for (itz = 1; itz< ntz; itz++)
				segmov2z(tz, z, ntz, prsmin);
		}
		/*2. Move to the nearest minima*/
		if (segmovtomin)
		{
			//for (itz = 1; itz< ntz; itz++)
				segmov2min(tz, ntz, vabs, x, y, nsx, distmin, itimemin);
		}

		/**********/
		/* Output */
		/**********/
		putdata (fSeg, tz, ntz, "Z_segmentation_points", (double) sec);
	}

Cleanup:
	if( fTF ) fclose( fTF );
	if( fSeg ) fclose( fSeg );
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;
	if( aspec ) delete [] aspec;
	if( aspecfilt ) delete [] aspecfilt;
	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( vabs ) delete [] vabs;
	if( ax ) delete [] ax;
	if( ay ) delete [] ay;
	if( tvyzero ) delete [] tvyzero;
	/* 29May00: hlt: Added */
	if( tvyzero ) delete [] tvypeak;
	if( tayzero ) delete [] tayzero;
	if( tayzeroout ) delete [] tayzeroout;
	/* 22Jan04: gmb: Added the missing cleanup for these new arrays */
	if( tvabsmin ) delete [] tvabsmin;
	if( tvabspeak ) delete [] tvabspeak;
	if( tvabssec ) delete [] tvabssec;
	if( tvabsminout ) delete [] tvabsminout;
	if( tvabssecout ) delete [] tvabssecout;
	if( tz ) delete [] tz;

	return fRet;
}

/* The following routines maybe too specific for segmen for being in hlib */


/****************************************************************************/
int remzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
				  double distmin, int debug)
/****************************************************************************/
/* remove zero crossings if not one adjecent stroke is longer than distmin */

{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( tvyzero != NULL );

	CString szMsg;
	int ntvyzeronew = 0;
	int itvyzeronew = 0, itvyzeroold = 0, it = 0, itdone = 0;
	int isampl_begin = 0, isampl_end = 0, isampl_prev = 0;
	double cblength = 0.0, cblength_prev = 0.0;
	double min = 0.0, max = 0.0;
	double vel = 0.0;

	/* tests */
	rmima (tvyzero, ntvyzero, &min, &max);
	if (min < 0 || max > ns)
	{
		OutputMessage( IDS_SEG_RZ1, LOG_PROC );
		return 0;
	}

	ntvyzeronew = ntvyzero;
	itdone = 1;
	itvyzeronew = 0;
	itvyzeroold = 0;

	/* First is always ok */
	if (debug)
	{
		szMsg.Format( IDS_SEG_RZ2, itvyzeroold, itvyzeronew, tvyzero [itvyzeronew]);
		OutputMessage( szMsg, LOG_PROC );
	}

	while (itdone < ntvyzeronew - 1)
	{
		for (itvyzeronew = itdone; itvyzeronew < ntvyzeronew-1; itvyzeronew++)
		{
			/* City block distances with previous and following sample */
			isampl_prev  = (int)(tvyzero [itvyzeronew - 1] + 0.5);
			isampl_begin = (int)(tvyzero [itvyzeronew    ] + 0.5);
			isampl_end   = (int)(tvyzero [itvyzeronew + 1] + 0.5);

			// 10Sep03: GMB - Added asserts to ensure validity of indices
			ASSERT( isampl_prev >= 0 );
			ASSERT( isampl_begin >= 0 );
			ASSERT( isampl_end >= 0 );

			cblength_prev = 0.5 * fabs ((double) (x [isampl_begin] - x [isampl_prev])) +
							fabs ((double) (y [isampl_begin] - y [isampl_prev]));
			cblength =      0.5 * fabs ((double) (x [isampl_begin] - x [isampl_end])) +
							fabs ((double) (y [isampl_begin] - y [isampl_end]));

			if (cblength_prev < distmin && cblength < distmin)
			{
				if (debug)
				{
					szMsg.Format( IDS_SEG_RZ3, itvyzeroold, itvyzeronew, tvyzero [itvyzeronew],
								  cblength_prev, cblength, distmin);
					OutputMessage( szMsg, LOG_PROC );
				}
				/* (remove/overwrite and) merge */
				for (it = itvyzeronew + 1; it < ntvyzeronew; it++)
					tvyzero [it - 1] = tvyzero [it];
				ntvyzeronew--;
				itvyzeroold++;
				/* ugly but cheap: the same test has to be repeated */
				/* can be prevented with break and one extra test */
				goto testagain;

			}
			else
			{
				if (debug)
				{
					szMsg.Format( IDS_SEG_RZ4, itvyzeroold, itvyzeronew, tvyzero [itvyzeronew],
								  cblength_prev, cblength, distmin);
					OutputMessage( szMsg, LOG_PROC );
				}
				itdone++;
				itvyzeroold++;
			}
		}

testagain:;
	}

	if (debug)
	{
		szMsg.Format( IDS_SEG_RZ2, itvyzeroold, itvyzeronew, tvyzero [itvyzeronew]);
		OutputMessage( szMsg, LOG_PROC );
	}
	/* If there are only two points and the distance between is smaller than distmin, return 0 points*/
	if (ntvyzeronew == 2)
	{
		isampl_begin = (int)(tvyzero [0] + 0.5);
		isampl_end   = (int)(tvyzero [1] + 0.5);
		cblength =      0.5 * fabs ((double) (x [isampl_begin] - x [isampl_end])) +
							fabs ((double) (y [isampl_begin] - y [isampl_end]));
		if (cblength < distmin)
			ntvyzeronew = 0;
	}
	return (ntvyzeronew);
}
/****************************************************************************/
int remmin (double* x, double* y, double* vabs, double* tvabsmin, int ntvabsmin, double vlev, double* vabspeaks, double distmin)
/****************************************************************************/
/* remove zero crossings if not one adjecent stroke is longer than distmin */

{
	ASSERT( tvabsmin != NULL );
		int itvabsmin = 0, isam = 0, isam1 = 0, isam2= 0, it = 0;
		int ntvabsminnew = ntvabsmin;
		int tvabspeakpos = 0;
		double cblength = 0.0;
		/* Gives artifacts: if one stroke present it may be a zero stroke. */
		if (ntvabsmin <= 2) return (ntvabsmin);

		for (itvabsmin = 0; itvabsmin < ntvabsmin; itvabsmin++)
		{
			/*Number of secondary points per segment*/
			int isec = 0;
			/************************************/
			/* Times of peakvelocity per stroke */
			/************************************/
#define ROUNDUP 0.999
			/* First and last sample */
			isam1 = (int)(tvabsmin [itvabsmin] + ROUNDUP);
			isam2 = (int)(tvabsmin [itvabsmin + 1] + ROUNDUP);

			// 10Sep03: GMB - Added assert to ensure validity of indices
			ASSERT( isam1 >= 0 );

			/* Find positive peak */
			vabspeaks [itvabsmin] = vabs [isam1];

			for (isam = isam1; isam < isam2; isam++)
			{
				if (vabspeaks [itvabsmin] < vabs[isam])
				{
					vabspeaks [itvabsmin] = vabs [isam];
					tvabspeakpos = isam;
				}
			}
		}
		for (itvabsmin = 0; itvabsmin < ntvabsmin; itvabsmin++)
		{
			isam1 = (int)(tvabsmin [itvabsmin] + ROUNDUP);
			isam2 = (int)(tvabsmin [itvabsmin + 1] + ROUNDUP);

			cblength = 0.5 * fabs ((double) (x [isam1] - x [isam2])) + fabs ((double) (y [isam1] - y [isam2]));

			if (cblength < distmin)
			{
				if (vabspeaks [itvabsmin] < vlev && vabspeaks [itvabsmin + 1] < vlev)
				{
					 for (it = itvabsmin + 1; it < ntvabsmin; itvabsmin++)
						 tvabsmin [it] = tvabsmin [it + 1];
					 ntvabsmin--;
				}
			}
		}

	return (ntvabsmin);
}

/************************************************************************************************/
int remzerocross2 (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
				   double distmin, int itimemin, int debug)
/************************************************************************************************/
/* Remove sequences not consisting of up-down sequences. */

{
// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( tvyzero != NULL );

	CString szMsg;
	int ntvyzeronew = 0, itvyzero = 0;
	int isampl_begin = 0, isampl_end = 0;
	double cblength = 0.0;
	double min = 0.0, max = 0.0;
	int updown [NSEGMAX];
	int updown_debug [NSEGMAX];
	double tvyzero0 = tvyzero [0];
	double tvyzeron = tvyzero [ntvyzero - 2];

	/* Gives artifacts: if one stroke present it may be a zero stroke. */
	if (ntvyzero <= 2) return (ntvyzero);

	/* Tests. */
	rmima (tvyzero, ntvyzero, &min, &max);
	if (min < 0 || max >= ns)
	{
		OutputMessage( IDS_SEG_RZ1_2, LOG_PROC );
		return 0;
	}

	/* Look at sign of dy and categorize sign + or - */
	/* and if length (not dy!) <min: 0. */
	for (itvyzero = 0; itvyzero < ntvyzero - 1; itvyzero++)
	{
		isampl_begin = (int)(tvyzero [itvyzero    ] + 0.5);
		isampl_end   = (int)(tvyzero [itvyzero + 1] + 0.5);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isampl_begin >= 0 );
		ASSERT( isampl_end >= 0 );

		/* Kind of city block distance. */
		cblength  = 0.5 * fabs ((double) (x [isampl_end] - x [isampl_begin])) +
					fabs ((double) (y [isampl_end] - y [isampl_begin]));
		if (cblength > distmin || (isampl_end - isampl_begin) > itimemin)
		{
			if ((y [isampl_end] - y [isampl_begin]) > 0.)
				updown [itvyzero] = UP;
			else
				updown [itvyzero] = DOWN;
		}
		else
		{
			updown [itvyzero] = STOP;
			if (debug)
			{
				szMsg.Format( IDS_SEG_RZ2_2, itvyzero, tvyzero [itvyzero], cblength, distmin,
							  isampl_end - isampl_begin, itimemin);
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}

	/* Assume that the series never starts or ends with 0. */
	/* Do not know why this is not satisfied in wild handwriting */
	while (updown [0] == STOP && ntvyzero > 0)
	{
		OutputMessage( IDS_SEG_RZ3_2, LOG_PROC );
		for (itvyzero = 1; itvyzero < ntvyzero; itvyzero++)
		{
			updown [itvyzero - 1] = updown [itvyzero];
			tvyzero [itvyzero - 1] = tvyzero [itvyzero];
		}
		ntvyzero--;
	}

	while (updown [ntvyzero - 2] == STOP)
	{
		szMsg.Format( IDS_SEG_RZ4_2, ntvyzero);
		OutputMessage( szMsg, LOG_PROC );
		ntvyzero--;
		if (ntvyzero <= 2) break;
	}

	/***************************************************/
	/* Here is the key for grouping strokes.           */
	/***************************************************/
	/* DOWN STOP* DOWN  => DOWN DOWN* DOWN */
	/* DOWN STOP* UP    => DOWN UP*   UP */
	/* UP   STOP* DOWN  => UP   DOWN* DOWN */
	/* UP   STOP* UP    => UP   UP*   UP */

	/* Astrisk (*) means one or more repetitions. */
	/* Each stop gets the direction of the intended following stroke. */
	/* Therefore, move backwards through updown. */
	/* This is not so nice as the last noisy stroke may determine the beginning. */


	if (debug)
	{
		for (itvyzero = 0; itvyzero < ntvyzero; itvyzero++)
			updown_debug [itvyzero] = updown [itvyzero];
	}

	for (itvyzero = ntvyzero - 2; itvyzero >= 0; itvyzero--)
	{
		if (updown [itvyzero] == STOP && updown [itvyzero + 1] == UP)
			updown [itvyzero] = UP;
		if (updown [itvyzero] == STOP && updown [itvyzero + 1] == DOWN)
			updown [itvyzero] = DOWN;
	}

	ntvyzeronew = 0;
	itvyzero = 0;
	/* First point is always ok. */
	/* Copy first point */
	if (debug)
	{
		szMsg.Format( IDS_SEG_RZ2, itvyzero, ntvyzeronew, tvyzero [itvyzero]);
		OutputMessage( szMsg, LOG_PROC );
	}
	tvyzero [ntvyzeronew] = tvyzero [itvyzero];
	ntvyzeronew++;

	/* Copy second and following points only if different from previous. */
	for (itvyzero = 1; itvyzero < ntvyzero - 1; itvyzero++)
	{
		if (updown [itvyzero - 1] != updown [itvyzero])
		{
			if (debug)
			{
				szMsg.Format( IDS_SEG_RZ6_2, itvyzero, ntvyzeronew, tvyzero [itvyzero],
							  updown_debug [itvyzero - 1], updown_debug [itvyzero]);
				OutputMessage( szMsg, LOG_PROC );
			}
			tvyzero [ntvyzeronew] = tvyzero [itvyzero];
			ntvyzeronew++;
		}
		else
		{
			if (debug)
			{
				szMsg.Format( IDS_SEG_RZ7_2, itvyzero, ntvyzeronew, tvyzero [itvyzero],
							  updown_debug [itvyzero - 1], updown_debug [itvyzero]);
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}

	/* Copy last one at any rate as it cannot be verified */
	itvyzero = ntvyzero - 1;
	if (debug)
	{
		szMsg.Format( IDS_SEG_RZ2, itvyzero, ntvyzeronew, tvyzero [itvyzero]);
		OutputMessage( szMsg, LOG_PROC );
	}
	tvyzero [ntvyzeronew] = tvyzero [itvyzero];
	ntvyzeronew++;
	return (ntvyzeronew);
}
/*********************************************************************************/
int addlastzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
					  double distmin, int itimemin)
/*********************************************************************************/
/* See also addfirstzerocross. */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( tvyzero != NULL );

	int ntvyzeronew = 0;
	double cblength = 0.0;
	int isampl_begin = 0, isampl_end = 0;

		tvyzero [ntvyzero] = ns - 1;
		ntvyzero++;
		if (ntvyzero >= 2)
		{
			isampl_begin = (int)(tvyzero [ntvyzero - 2] + 0.5); /* last segmentation point */
			isampl_end   = (int)(tvyzero [ntvyzero - 1] + 0.5);

			// 10Sep03: GMB - Added asserts to ensure validity of indices
			ASSERT( isampl_begin >= 0 );
			ASSERT( isampl_end >= 0 );
			/* kind of city block distance */
			cblength  = 0.5 * fabs ((double) (x [isampl_end] - x [isampl_begin])) +
					fabs ((double) (y [isampl_end] - y [isampl_begin]));

			/* big enough ? */
			if (cblength < distmin || (isampl_end - isampl_begin) < itimemin)
				ntvyzero--;
		}
	return (ntvyzero);
}

/*********************************************************************************/
int addfirstzerocross (double* x, double* y, int ns, double* tvyzero, int ntvyzero,
					   double distmin, int itimemin)
/*********************************************************************************/
/* See also addlastzerocross. */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( tvyzero != NULL );

	int ntvyzeronew = 0;
	double cblength = 0.0;
	int isampl_begin = 0, isampl_end = 0, it = 0;

	/* Is not made repetative because of all the shifting necessary. */
	/* Should be constructed with pointers and shifting afterwards. */
	ntvyzeronew = ntvyzero;

	/* city block distance */
	isampl_begin = 0;
	isampl_end = (int)(tvyzero [0] + 0.5);

	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( isampl_begin >= 0 );
	ASSERT( isampl_end >= 0 );

	if (isampl_end == 0) return (ntvyzeronew);
	if (ns == 0) return (ntvyzeronew);  /* to use ns to prevent warning */
	cblength  = 0.5 * fabs ((double) (x [isampl_end] - x [isampl_begin])) +
				fabs ((double) (y [isampl_end] - y [isampl_begin]));

	/* big enough ? */
	if (cblength > distmin && (isampl_end - isampl_begin) > itimemin)
	{
		/* shift zeroes one place behind */
		for (it = ntvyzero; it > 0; --it)
			tvyzero [it] = tvyzero [it - 1];
		/* substute first point */
		tvyzero [0] = isampl_begin;
		ntvyzeronew++;
	}

	return (ntvyzeronew);
}


/* See also indupe */
/********************************************************/
int indup (double* vy, int nsvy, double tstart, double vlev, bool segvabs)
/********************************************************/
/* index of up or down going jump where for the first time abs (v [i]) > vlev */

{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is1 = 0, is2 = 0;
	bool furthertest = 0;

	/* Round down */
	is0 = (int)tstart;

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
	{
		CString szMsg;
		szMsg.Format( IDS_SEG_IND, is0, nsvy);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	/* The problem with this check is that when vlev is very low */
	/* a first stroke may not at all be detected */
	is1 = is0;
		/* For segmentation by zero crossing*/
	/* Two different methods required for movement ending with upswing and downswing*/
	/* To prevent jumping of samples for trials where there is no sample closer to zero within vylev range*/

#if 1
	// hlt: 28jan09: if it starts bigger than vlev, find where it is smaller than vlev.
	// This pat can also be used for vy segmentation
	if (abs (vy [is1]) > vlev)
	{
		for (is2 = is1; is2< nsvy; is2++)
		{
			if ((double) abs (vy [is2]) <= vlev)
			{
				is1 = is2;
				break;
			}
		}
	}
#else
	// hlt: 28Jan09: Disabled because segvabs means whether vabs or vy is used
	/* 1. If the seg point (last sample) is already greater than vylev*/
	/* Search backwards to find a point lower than vylev*/
	if (!segvabs)
	{
		if (vy [is1] > vlev)
		{
			for (is2 = is1; is2< nsvy; is2++)
			{
				if ((double) vy [is2] <= vlev)
				{
					is1 = is2;
					break;
				}
			}
		}
		/* 2. If the seg point (last sample) is already lower than vylev*/
		/* Search backwards to find a point higher than vylev*/
		else if (vy [is1] < -vlev)
		{
			for (is2 = is1; is2 < nsvy; is2++)
			{
				if ((double) vy [is2] >= -vlev)
				{
					is1 = is2;
					break;
				}

			}
		}
	}
#endif

	/* Now find the actual coarse point*/
	for (is2 = is1; is2 < nsvy; is2++)
	{
		if (fabs((double) vy [is2]) > vlev)
		{
			is1 = is2;
			break;
		}
	}
	//}
	return (is2);
}

/* See also indup */
// TODO make vylev like vlev in undup()
/********************************************************/
int indupe (double* vy, int nsvy, double tstart, double vylev, bool segvabs)
/********************************************************/
/* index of up or down going jump searching backwards */
/* where for the first time abs (v [i]) > vlev */

{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is1 = 0, is2 = 0;
	bool furthertest = 0;

	/* Round down */
	is0 = (int)tstart;

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
	{
		CString szMsg;
		szMsg.Format( IDS_SEG_INDE, is0, nsvy);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	is1 = is0;

	/* Search backwards for vy far from zero */
	/* Prevent that is2 gets less than 0 */
	//for (is2 = is1; is2 > 0; is2--)
	//	if (fabs ((double) vy [is2]) > vylev) break;
	/* For segmentation by zero crossing*/
	/* Two different methods required for movement ending with upswing and downswing*/
	/* To prevent jumping of samples for trials where there is no sample closer to zero within vylev range*/

#if 1
	// hlt: 28jan09: if it starts bigger than vlev, find where it is smaller than vlev.
	// This pat can also be used for vy segmentation
	if (abs (vy [is1]) > vylev)
	{
		for (is2 = is1; is2 > 0; is2--)
		{
			if ((double) abs (vy [is2]) <= vylev)
			{
				is1 = is2;
				break;
			}
		}
	}
#else
	// hlt: 28Jan09: Disabled because segvabs means whether vabs or vy is used
	/* 1. If the seg point (last sample) is already greater than vylev*/
	/* Search backwards to find a point lower than vylev*/
	if (!segvabs)
	{
		if (vy [is1] > vylev)
		{
			for (is2 = is1; is2 > 0; is2--)
			{
				if ((double) vy [is2] <= vylev)
				{
					is1 = is2;
					break;
				}
			}
		}
		/* 2. If the seg point (last sample) is already lower than vylev*/
		/* Search backwards to find a point higher than vylev*/
		else if (vy [is1] < -vylev)
		{
			for (is2 = is1; is2 > 0; is2--)
			{
				if ((double) vy [is2] >= -vylev)
				{
					is1 = is2;
					break;
				}

			}
		}
	}
#endif

	/* Now find the actual coarse point*/
	for (is2 = is1; is2 > 0; is2--)
	{
		if (fabs((double) vy [is2]) > vylev)
		{
			is1 = is2;
			break;
		}
	}

	return (is2);
}

/* See also rindmin0 */
/************************************************/
double rindmin0e (double* vy, int nsvy, double tstart)
/************************************************/
/* Index of first zero crossing starting from tstart backwards. */
/* If no zero crossing exists, select the point closest to zero */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is = 0, indmin = 0, indmax = 0;
	double vs1 = 0.0, vs2 = 0.0;

	/* Round down changed to Round up*/
	/* To start from one sample below vylev irrespective of curve going up or down*/
	is0 = (int)(tstart + 0.999);

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
	{
		CString szMsg;
		szMsg.Format( IDS_SEG_RIND, is0, nsvy);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	/* Find zero grossing or bouncing from zero, whatever comes first */
	for (is = is0; is > 0; --is)
	{
		vs1 = vy [is - 1];
		vs2 = vy [is];
		/* zero crossing */
		if ((vs1 < 0. && vs2 >= 0.) || (vs1 > 0. && vs2 <= 0.) || (vs1 <= 0. && vs2 > 0.) || (vs1 >= 0. && vs2 < 0.))
			return ((double) (is + vy [is] / ( vy [is - 1] - vy [is])));
		/* close by zero crossing */
		if (fabs ((double) vy [is - 1]) > fabs ((double) vy [is]))
			return ((double) is);
	}

/* TO DO: Check if this block is necessary in all the four rind routines*/
	/* no (close) zero crossing found: select point closest to zero */
	indrmm (vy, is0, &indmin, &indmax);

	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( indmin >= 0 );
	ASSERT( indmax >= 0 );

	if (vy [indmax] < -vy [indmin])
		return ((double) indmax);
	else
		return ((double) indmin);
}

/* See also rindmin0e */
/************************************************/
double rindmin0 (double* vy, int nsvy, double tstart, bool lastzero)
/************************************************/
/* Index of first zero crossing starting from tstart. */
/* If no zero crossing exists, select the point closest to zero */

{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is = 0, indmin = 0, indmax = 0;
	double vs1 = 0.0, vs2 = 0.0;

	lastzero = 0;
	/* Round up and go to the next sample while searching forward*/
	is0 = (int)(tstart + 0.999)+ 1;
	if (is0 >= (nsvy - 1))
		is0 = nsvy - 1;

	/* test */
	if (is0 < 0 || (is0 >= nsvy  && is0 != 0))
	{
		CString szMsg;
		szMsg.Format( IDS_SEG_RIND0, is0, nsvy);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	/* find zero grossing or moving away from zero, whatever comes first */
	for (is = is0; is < nsvy; ++is)
	{
		vs1 = vy [is - 1];
		vs2 = vy [is];
		/* zero crossing */
 		if ((vs1 < 0. && vs2 >= 0.) || (vs1 > 0. && vs2 <= 0.) || (vs1 <= 0. && vs2 > 0.) || (vs1 >= 0. && vs2 < 0.))
		{
			lastzero = 1;
			return ((double) (is + vy [is] / ( vy [is - 1] - vy [is])));
		}
		/* close by zero crossing */
		if (fabs ((double) vy [is - 1]) < fabs ((double) vy [is]))
			return ((double) (is - 1));
	}

	/* no zero crossing found: select point closest to zero */
	indrmm (&vy [is0], nsvy - is0, &indmin, &indmax);

	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( indmin >= 0 );
	ASSERT( indmax >= 0 );

	if (vy [indmax + is0] < -vy [indmin + is0])
		return ((double) indmax + is0);
	else
		return ((double) indmin + is0);
}
/* See also rind0 */
/************************************************/
double rind0e (double* vy, int nsvy, double tstart)
/************************************************/
/* Index of first zero crossing starting from tstart backwards. */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is = 0, indmin = 0, indmax = 0;

	/* Round down changed to Round up*/
	/* To start from one sample below vylev irrespective of curve going up or down*/
	is0 = (int)(tstart + 0.999);

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
	{
		CString szMsg;
		szMsg.Format( IDS_SEG_RIND, is0, nsvy);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	/* Find zero grossing or bouncing from zero, whatever comes first */
	for (is = is0; is > 0; --is)
	{
		/* zero crossing */
		if ((vy [is - 1] < 0) ^ (vy [is] < 0))
			return ((double) (is + vy [is] / ( vy [is - 1] - vy [is])));
	}

	/* no (close) zero crossing found: select point closest to zero */
	indrmm (vy, is0, &indmin, &indmax);

	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( indmin >= 0 );
	ASSERT( indmax >= 0 );

	if (vy [indmax] < -vy [indmin])
		return ((double) indmax);
	else
		return ((double) indmin);
}
/************************************************/
double rind0 (double* vy, int nsvy, double tstart)
/************************************************/
/* Index of first zero crossing starting from tstart. */
{
  // 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is = 0, indmin = 0, indmax = 0;

	/* Round up and go to the next sample while looking forward */
	is0 = (int)(tstart + 0.999) + 1;

	if (is0  >= (nsvy -1))
		is0 = nsvy - 1;

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
	{
		CString szMsg;
		szMsg.Format( IDS_SEG_RIND0, is0, nsvy);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	/* find zero grossing or moving away from zero, whatever comes first */
	for (is = is0; is < nsvy; ++is)
	{
		/* zero crossing */
		if ((vy [is - 1] < 0) ^ (vy [is] < 0))
			return ((double) (is + vy [is] / ( vy [is - 1] - vy [is])));
	}

	/* no zero crossing found: select point closest to zero */
	indrmm (&vy [is0], nsvy - is0, &indmin, &indmax);

	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( indmin >= 0 );
	ASSERT( indmax >= 0 );

	if (vy [indmax + is0] < -vy [indmin + is0])
		return ((double) indmax + is0);
	else
		return ((double) indmin + is0);
}
/* See also rind */
/************************************************/
double rindelev(double* vy, int nsvy, double tstart, double vlevmin)
/************************************************/
/* search backwards to get the point where vy reaches minimum vy requirement */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is = 0, indmin = 0, indmax = 0;

	/* Round down changed to Round up*/
	/* To start from one sample below vylev irrespective of curve going up or down*/
	is0 = (int)(tstart + 0.999);

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
		return 0;
	for (is = is0; is > 0; --is)
	{
		/* test if lesser than vlevmin, return previous point*/
		if (fabs ((double) vy [is]) < vlevmin)
			break;
	}
	return ((double)is);
}
/********************************************************************************/
double rindlev (double* vy, int nsvy, double tstart, double vlevmin)
/********************************************************************************/
{
	/* search forwards to get the point where vy reaches minimum vy requirement */
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );

	int is0 = 0, is = 0, is2 = 0,indmin = 0, indmax = 0;

	/* Round down changed to Round up*/
	/* To start from one sample below vylev irrespective of curve going up or down*/
	is0 = (int)(tstart + 0.999) + 1;

	/* test */
	if (is0 < 0 || (is0 >= nsvy && is0 != 0))
		return 0;
	for (is = is0; is < nsvy; ++is)
	{
		/* test if lesser than vlevmin, return next point*/
		if (fabs ((double) vy [is]) < vlevmin)
			break;
    }
	return ((double)is);
}
/************************************************************************/
int nzerocross (double* vy, int nsvy, double* tvyzero, int ntvyzeromax)
/************************************************************************/
/* All zero crossings after index tvyzero [0] */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( vy != NULL );
	ASSERT( tvyzero != NULL );

	int is = 0, is0 = 0, ntvyzero = 0;
	double vs1 = 0.0, vs2 = 0.0;

	/* rounding up plus look one place beyond the previous is */
	/* which was found by looking backward */
	/* keep the strategy of testing is-1 and is */
	/*02Aug04: Raji: Definitely need to jump 2 samples from current point, as it looks at is-1 and is*/
	/*Otherwise it repeats finding the first point, already found*/
	is0 = (int) tvyzero [ntvyzero] + 2;

	ntvyzero++;
	for (is = is0; is < nsvy; is++)
	{
		vs1 = vy [is - 1];
		vs2 = vy [is];

		//if ((vy [is - 1] <= 0.) ^ (vy [is] < 0.))
		if ((vs1 < 0. && vs2 >= 0.) || (vs1 > 0. && vs2 <= 0.) || (vs1 <= 0. && vs2 > 0.) || (vs1 >= 0. && vs2 < 0.))
		{
			if (ntvyzero >= ntvyzeromax)
			{
				CString szMsg;
				szMsg.Format( IDS_SEG_ZC, ntvyzero, is, nsvy);
				OutputMessage( szMsg, LOG_PROC );
				break;
			}
			/* zero crossing at index "is" */
			/* however, check for closeness in time and space of other zero crossings */
			/* but this requires knowledge of past and future points. Therefore done in separate routine */
			/* linear interpolation */
			if (vy [is - 1] - vy [is] == 0.)
				tvyzero [ntvyzero] = is;
			else
                tvyzero [ntvyzero] = is + (vy [is] / ( vy [is - 1] - vy [is]));

			ntvyzero++;
		}

	}
	return (ntvyzero);
}

/***********************************************************************/
double distmax (double* x, double* y, int ns, double* tvyzero, int ntvyzero, int itimemin)
/***********************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( tvyzero != NULL );

	double distmax = 0.0, dprev = 0.0;
	int is = 0, isprev = 0, itvyzero = 0;
	double min = 0.0, max = 0.0;

	/* tests */
	rmima (tvyzero, ntvyzero, &min, &max);
	if (min < 0 || max > ns)
	{
		OutputMessage( IDS_SEG_DM1, LOG_PROC );
		return 0;
	}

	/* if there are many short strokes it is only to be hoped that the longest */\
	/* stroke is a relevant one, which is not the case if it is a sampling artefact */
	/* or trailing penlift: so strip trailing penlifts */
	/* largest city block distances */
	distmax = 0.;

	for (itvyzero = 1; itvyzero < ntvyzero; itvyzero++)
	{
		is     = (int)(tvyzero [itvyzero    ] + 0.5);
		isprev = (int)(tvyzero [itvyzero - 1] + 0.5);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( is >= 0 );
		ASSERT( isprev >= 0 );

		dprev  = 0.5 * fabs ((double) (x [is] - x [isprev])) +
				fabs ((double) (y [is] - y [isprev]));
		if (dprev > distmax)
		{
			if (is - isprev > itimemin) distmax = dprev;
			/* may generate to many harmless messages */
			else OutputMessage( IDS_SEG_DM2, LOG_PROC );

		}
	}

	return distmax;
}


/************************************************************************************************************************/
int remayzerocross (double* tayzero, int ntayzero, double* tvyzero, double* tvypeak, int ntvyzero,
					double* tayzeroout, int ntayzeroout)
/************************************************************************************************************************/
/* Remove first ay zero crossing, leave second, and remove all others within a stroke */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( tayzero != NULL );
	ASSERT( tvypeak != NULL );
	ASSERT( tvyzero != NULL );
	ASSERT( tayzeroout != NULL );

	int itvy = 0, itay = 0, itayout = 0;
	if (ntayzero == 0)
		return 0;

	itay = 0;    /* points at tayzero */
	itvy = 0;    /* points at tvyzero */
	itayout = 0; /* points at tayzeroout */
	while (TRUE)
	{
		/* 29May00: hlt: Added tvypeak criterion */
		/* Search forward for the first tayzero in the next stroke after the peak velocity */
		/* The specified event after which the submovement segmentation point must be searched */
		/* There is only one peak velocity point per stroke */
			while (tayzero [itay] <= tvypeak [itvy])
			{
				itay++;
				if(itay >= ntayzero)
				{
					/* 14 May 03 Raji: Additional condition checking is required to prevent premature exiting of the loop*/
					/* In cases where the last segmentation point happens to be after the last zero crossing point*/
					/* This loop should be exited only after processing the submovement point for the last segment*/
					/* Now itayout will return the correct number of submovement points and it is assigned the value*/
					/*of the last zero crossing point*/
					if(itayout >= ntayzero)
						return itayout;
					else
					{
						tayzeroout [itayout] = tvyzero [itvy + 1];
						itayout++;
						return itayout;
					}
				}
			}

		/* 5Jan01: hlt: Added to prevent the problem that the peak can just be beyond the zero crossing */
		/* The peak velocity point should be close to one of the acceleration zero crossings */
		/* Check whether this or the next one is the closest point */
		if (itay-1 >= 0)
		{
			if (tayzero [itay] - tvypeak [itvy] > tvypeak [itvy] - tayzero [itay-1])
			{
				itay--;
			}
		}

		/* The submovement segmentation point is the next zero crossing.*/
		/* If there is a second ayzero take that one */
		if (itay + 1 < ntayzero &&
			itvy + 1 < ntvyzero &&
			tayzero [itay + 1] < tvyzero [itvy+1])
		{
			tayzeroout [itayout] = tayzero [itay + 1];
		}
		/* Otherwise use the stroke end point tvyzero (no secondary movement) */
		else
		{
			tayzeroout [itayout] = tvyzero [itvy+1];
		}

		/* 05Jan01: hlt: Moved this to the end of the loop -- cleaner */
		/* Ready for next stroke end point */
		itvy++;
		if (itvy >= ntvyzero)
			return itayout;

		/* At any rate, get ready for the next stroke */
		itayout++;
		if (itayout >= ntayzeroout)
			return itayout;

	}
}

double rnpolq(double *y, int ns, double rind, double *rinde, double *yrinde)
{
    double ret_val;
    double a, b, c;
	int ind;
    double tmp, tmp1;

    ind = (int) (rind + 0.5);

    if (!(ind <= 1 || ind >= ns))
	{
		/*  a * (i-ind) * * 2 + b * (i - ind) + c = y(i) */

        a = (y[ind + 1] - y[ind] * 2.0 + y[ind - 1]) * 0.5;
        b = (y[ind + 1] - y[ind - 1]) * 0.5;
        c = y[ind];

        if (!(a == 0.0))
		{
            tmp = b / (a * -2.0);
			tmp1 = rind - ind;
			ret_val = (a * tmp1 + b) * tmp1 + c;
            *yrinde = (a * tmp + b) * tmp + c;
            *rinde = ind + tmp;
             return ret_val;

        }
    }

    *rinde = std::min(std::max(1.0,rind), (double) ns);
    ret_val = rnpol(&y[0], ns, (double)*rinde);
    *yrinde = ret_val;
    return ret_val;
}
/* Find local minima */
int inddip(double *x, int n, int nstop, int idt, double *tvabsbot, int nsegmax)
{
	// This is an algorithm to find all local minima that have no lower minimum
	//vabs = array of absolute velocity data
	/*ns = size of array

	Ifirst = first location to look for a velocity dip
	Ifirst must be between/including 0 and ns-1

	Ilast = last location to look for a velocity dip
	Ilast must be between/including 0 and ns-1*/

	int ibot = 0, itop = 0, itoptest = 0, ibottest = 0, ndip = 0, is = 0;
	double vabstdip = 0.0, temp = 0.0;
	/* starting sample*/
	ibot = (int)tvabsbot [ndip];
	ndip++;
	itop = 0;
	//Search from ifirst to ilast
	while (itop < nstop && ibot < nstop)
	{
		// start location for finding local top
		itop = ibot;
		//stop processing after current sample equals nstop; Apply for all steps
		if (itop >= nstop)
			goto ret;
		itoptest = itop + 1;
		//stop processing after sample we test the current sample with goes beyond the number of samples; Apply for all steps
		if (itoptest >= n)
			goto ret;

		// Search for a top that has no higher top within idt samples
		while (true)
		{
			// Check for an uptrend
			if(x[itop] < x[itoptest])
			{
				// Uptrend found
				//itop++;
				itop = itoptest;
				if (itop >= nstop)
					goto ret;
				itoptest++;
				if (itoptest >= n)
					goto ret;
			}
			else
			{
				itoptest++;
				if (itoptest >= n)
					goto ret;
				if ((itoptest - itop) > idt)
					break;
			}
		}

		// local top found
		// start location for finding local bot
		ibot = itop;
		if (ibot >= nstop)
			goto ret;
		ibottest = ibot+1;
		if (ibottest >= n)
			goto ret;

		// Search for a bot that has no lower bot within idt samples
		while (true)
		{
			// Check for a downtrend
			if( x[ibot] > x[ibottest] )
			{
				// Downtrend found
				//ibot++;
				ibot = ibottest;
				if (ibot >= nstop)
					goto ret;
				ibottest++;
				if (ibottest >= n)
					goto ret;
			}
			else
			{
				ibottest++;
				if (ibottest >= n)
					goto ret;
				if ((ibottest - ibot) > idt)
					break;
			}
		}

	// Next local bot found
	//Assign the current point as minima (tvabsbot[ndip])after interpolation
	rnpolq(x, n, (double)ibot, &tvabsbot[ndip], &vabstdip);
	ndip++;
	if(ndip >= nsegmax - 1)
		break;
	}
ret:
	return (ndip);
}

// TODO: This routine does not know how big array z is so we cannot check for out of boundaries
// Using NSMAX as max array size for now.
void segmov2z(double *tvyzero, double *z, int ntvyzero, double prsmin)
{
	int itvyzero = 0, is = 0, isprev = 0, isnext = 0, i = 0, ff = 0, rr = 0;
	// 30Jan09: hlt: Last point cannot be done as we do not know how long the sampled array is.
	// First point can also nto be done.
	for (itvyzero = 1; itvyzero < ntvyzero - 1; itvyzero++)
	{
		/*23Apr04: Raji: Move the segmentation points to the nearest pen-down point*/
		/* for all segmentation points except first and last*/
		/* Including the first and last point in this interferes greatly with the regular segmentation process, so ignore for now*/
		// 29Jan09: hlt: Only points NEAR a pen down (=closer than 1/2 stroke) should be moved
		// Other points should stay where they are

		is = (int)(tvyzero [itvyzero] + 0.5);
		// 29Jan09: hlt: Go maximally half the #samples to previous or next segmentation point
		// To give both backwards and forward shift a chance, the max shifts must be equal
		// as otherwise, is a
		//isprev = (int)(tvyzero [itvyzero - 1] + 0.5);
		//isnext = (int)(tvyzero [itvyzero + 1] + 0.5);
		isprev = int( is - (is - (int)(tvyzero [itvyzero - 1] + 0.5)) * 0.5 );
		// TODO We can exceed the nr of samples for the last point
		// as we do not know them in this routine (argument forgotten).
		// So we reduce the loop to NOT move the last point.
		// TODO: We need to add the number of samples to the argument list and make a better correction
		isnext = int( is - (is - (int)(tvyzero [itvyzero + 1] + 0.5)) * 0.5 );
		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( is >= 0 );
		ASSERT( isprev >= 0 );
		ASSERT( isnext >= 0 );

		ff = 0;
		rr = 0;
		/* If the current segmentation point is in air*/
		if (z [is] <= prsmin) //pen lift
		{
			/* Find the next pen-down point forward*/
			for (i = is; i < isnext; i++)
			{
				if (z[i] > prsmin) //pendown
					break;
				ff++;
			}
			// 29Jan09: hlt: Added. If not near pendown point found during loop, do not shift (half a stroke)
			if (i == isnext)
				ff = NSMAX; // carries it outside the array of samples and becomes not eligible

			/* Find the next pen-down point backward*/
			for (i = is; i >= isprev; i--)
			{
				if (z[i] > prsmin) //pendown
					break;
				rr++;
			}
			// 29Jan09: hlt: Added. If not near pendown point found during loop, do not shift (half a stroke)
			if (i == isprev)
				rr = NSMAX; // carries it outside the array of samples and becomes not eligible

			/* Assign whichever is closer*/
			// 29Jan09: hlt: But do not change with these large values that will bring them outside [0,NSMAX)
			// The if .. else brackets are essential (?)
			if (ff < rr)
			{
				if (is + ff < NSMAX)
					tvyzero [itvyzero] = is + ff;
			}
			else
			{
				if (is - rr >= 0)
					tvyzero [itvyzero] = is - rr;
			}
		}
	}
}

#if 0
// 28Apr09: Somesh:
// This routine was working till v5.2
// Problems: several tests could reach out of data range and possibly array bound;
// Rounding and min distance 0 for non-existing min was taken for closest min
// Too much code
void segmov2min(double *tvyzero, double *vabs, double *x, double *y, int ntvyzero, double distmin, int itimemin)
{
	int itvyzero = 0, is = 0, isprev = 0, isnext = 0, i = 0, ff = 0, rr = 0;
	double cblength;
	for (itvyzero = 1; itvyzero < ntvyzero; itvyzero++)
	{
		isprev = (int)(tvyzero [itvyzero - 1] + 0.5);
		is = (int)(tvyzero [itvyzero] + 0.5);
		isnext = (int)(tvyzero [itvyzero + 1] + 0.5);
		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( is >= 0 );
		ASSERT( isprev >= 0 );
		ASSERT( isnext >= 0 );

		ff = 0;
		rr = 0;
		i = 0;

		/* Find the next minima forward*/
		for (i = is; i < (is + itimemin); i++)
		{
			cblength = 0.5 * fabs ((double) (x [i] - x [is])) +
							fabs ((double) (y [i] - y [is]));
			if (cblength > distmin)
				break;
			if ((vabs [i + 1] > vabs [i]) && (vabs [i - 1] > vabs [i]))
			{
				ff = i - is;
				break;
			}

		}
		/* Find the next minima backward*/
		for (i = is; i >= (is - itimemin); i--)
		{
			cblength = 0.5 * fabs ((double) (x [i] - x [is])) +
							fabs ((double) (y [i] - y [is]));
			if (cblength > distmin)
				break;
		is1 = (int)(tvyzero [itvyzero] + 0.5) - itimemin;
			{
				rr = is - i;
				break;
			}
		}
		/* If no minima is found in both directions, take the current point*/
		if ((ff == 0) && (rr == 0))
			tvyzero [itvyzero] = (double)is;
		/* One of them is non-zero*/
		else
		{
            /* If in one of the directions there is no minima found, take the other one*/
			if (ff == 0)
				tvyzero [itvyzero] = (double)is - rr;
			else if (rr == 0)
				tvyzero [itvyzero]= (double)is + ff;

			/* Assign whichever is closer*/
			if (ff < rr)
				tvyzero [itvyzero]= (double)is + ff;
			else if (rr < ff)
				tvyzero [itvyzero] = (double)is - rr;
		}
	}
}
#endif

// Updated for v5.3
// Improved by minimizing conversion to integer
// added ns check
void segmov2min(double *tvyzero, int ntvyzero, double *vabs, double *x, double *y, int ns, double distmin, int itimemin)
{
	int itvyzero = 0, is = 0, is1 = 0, is2 = 0, i = 0;
	double cblength, vabsmin;
	// for all points, including beginning and end
	for (itvyzero = 0; itvyzero < ntvyzero; itvyzero++)
	{
		/* Round to closest sample where we have vabs data and select the interval +-itimemin within the x, y, vabs arrays */
		/* TODO: The interval of 2 times itimemin is maybe too large. Make it half? */
		is  = (int)(tvyzero [itvyzero] + 0.5);
		is1 = (int)(tvyzero [itvyzero] + 0.5) - itimemin;
		is2 = (int)(tvyzero [itvyzero] + 0.5) + itimemin;
		// TODO should not iS, is1, is2 bedelimited to 0 and ns-1?
		if (is1 < 1)
			is1 = 1;
		if (is2 > ns - 2)
			is2 = ns - 2;

		/* Find the lowest vabs within the +-itimemin interval and within distmin */
		/* The surest vabs min is around tvyzero [itvyzero] which is hopefully within distmin as it may be up to 0.5 inter sample time from the vy zero crossing */
		/* TODO (low prio) quadratic interpolation of vabs around its min. */
		/* TODO (low prio) Eucldian distance instead of city block distance */
		/* TODO (low prio) interpolated x and y instead of at integer samples nrs */

		vabsmin = vabs [is];
		for (i = is1; i < is2; i++)
		{
			// Only look for the lowest vabs which have a city block distance less than distmin
			cblength = fabs (x [i] - x [is]) + fabs (y [i] - y [is]);
			// Only within distmin distance
			if (cblength < distmin)
			{
				// Only for lower and lower vabs
				// Find a minimum
				if ((vabs [i + 1] > vabs [i]) && (vabs [i - 1] > vabs [i]))
				{
					//Ensure the minimum is the lowest minimum
					if (vabs [i] <= vabsmin)
					{
						// Remember lower vabsmin
						vabsmin = vabs [i];
						// Update the tvyzero array each time you find a lower vabs within range
						tvyzero [itvyzero] = i;
					}

				}
			}
		}
	}
}

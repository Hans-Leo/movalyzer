/*************************************************
   Extract - version 2.6 - hlt/ka - 10 Mar 1995
**************************************************
   Copyright 1995 Teulings
		2
ARGUMENTS: infile_timefunctions infile_segments output_append_file trialnr

PURPOSES:
	(1) Extracts features

COMPILATION:
    (1) MS Visual Studio .NET 2003

UPDATES:
	27Apr94: Segmentation data now in seconds
	19Aug94: File names in header; Write header only if creating file.
	26Jan95: New features: padE->startx, padE->deltax, rnprsmin, padE->slant, padE->surface, padE->t1loop, padE->t2loop
	14Feb95: Revised and tested ok; Bugs due to zero intervals solved.
	23Feb94: V features added
	31Mar95: Drop T1Loop and T2Loop; Include Vypeak and Rtvypeak (relative);
	19Apr95: Added padE->ndecel, padE->ayeff
	2May95: Reading also Jerk. Jabsq overrules Yefficiency. padE->starty added.
	5May95: Output format was %7.3f becomes %8.4g
	16May95: Normalize StraightErr and JabsQ but not NDecel
	26Jun95: Bug in normalized jabs: Integrated across first and second interval.
           Loop search speedded up: first 0 or last loop; now
             two strokes back or last loop whatever is closest.
	21Jul95: Jabs factor 1/2 included.
	13Jan96: Relative Jabs by Jabs-of-phase1/Jabs-total
	9Apr97: Relative phase1/total: DeltaT, DeltaX, DeltaY. Variable names updated: DeltaTR, etc.
	2Jul97: Arbitrary alphanumeric args for trialindex; /s moves to arg4.
	7Jul97: Slant during first 80 msp; Slant80 instead of padE->ndecel
	16Jul97: MIN/MAXLOOPTIME in seconds instead of sample nrs
	21Jul97: MaxLoop time from 2.5 to 5s, minlooptime from 0.02 to 0.05
           Start loop search not 2 strokes back but only one
	15Dec97: Onestroke analysis included
	20Dec97: Startt instead of padE->startx
	21Apr98: Startx instead of padE->startt
	16Sep98: Included beta from timfun to correct angles for rotation.
	28Sep98: Vypeak replaced by Slant80, Slant80 replaced by StartT
	20Nov98: Insert empty stroke if empty data
	24Nov98: Looparea was NOT normalized.
	28Jan99: PeakVy instead of StartX
	20Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		   removal of statics
	18May00: Replaced rpenup by startx. Did NOT upte the header line.
	5Jun00: Prevent values (except startt,x,y) when deltat=0
	21Jun01: hlt: Disabled normlization of looparea feature
	26Jul01: hlt: Appended total stroke data summarizing the substrokes (quick solution)
	5May03: hlt: Now multistroke sequences can have a summary stroke appended in Secondary Submovement analysis
	05May03 Hlt: Combined the onestroke changes after getting the inputs from the segmentation points from the .seg file
	07May03 hlt: Change one dimensional segmentaiton array into 2 dimensional so that
              making a uniform processing of substrokes 1,2 and the total stroke 3, etc.
	08May03 Raji: New code blocks added to replace the blocks involving the processing of the strokes to calculate the output parameters
			   and to make it compatible with the new 2-D array system used to store the segment data.
	08May03 Raji: The block 'Append Stroke data' has been eliminated as all the strokes are processed similarly Irrespective of
               whether it is a secondary submovement analysis or not
	24May03 GMB: Variabalized some constants for use as parameters
	05Jun03:Raji:Correction of padE->slant for performed rotation-Fold slant value back into the range between -PI and +PI
	22Sep03: HLT; Test in case there are no strokes, e.g., one or no segmentation points
	16Apr04: Raji: New columns for special data and modified the secondary data.
	09Aug05: GMB: Tests run: modified the 4 main #defines (NSMAX, NSEGMAX, NFREQMAX, NSMAXTIMFUN)
							 individually to extremely low values to test for crashes
						RESULTS: no crashes
	18Sep06: GMB: Converted deprecated string/file functions for MSVS7
	21Apr08: GMB: Cleanup up code. Added roadlength. Fixed isam2 calculations to round down, not up.
	Ensured initialization occurred while calculating
	08Jul08: GMB: Changed position OR velocity OR accel. spectrum to all 3 - filtered & not
				  TF file now has 6 spectra as opposed to 2
	19Aug08: GMB: Added feedback information ( 2 of them ). Input for column & stroke. Output for value.
				  Returns value of index "_nFBColumn" for stroke "_nFBStroke"
    03Oct08: GMB: Added new feature "TrialSeq" for the actual sequence of the trial within the entire
				  subject's experiment. Data is derived from the sequence file (.SEQ) created
				  when the experiment is run. If does not exist, value is 0 for each trial replication.
	25Jun10: Somesh: aabs array was not being read and discarded. As a result, jx array was incorrectly
					getting aabs data .Added getdata() to do that.

 *************************************************************************/

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include <math.h>
#include "../NSShared/iolib.h"
#include "../NSShared/hlib.h"
#include "../Common/ProcSettings.h"
#include "../Common/DefFun.h"
#include "../Common/siglib.h"


#define ROUNDUP					0.999
#define NODECIMATE				1

/* Beginning of the stroke to base the slant upon (s) */
/* In the settings there was reference of 80 ms. This can be made adjustable */
#define INITIALPART 0.080 /* in s */
double	initialpart = INITIALPART;
void SetStrokeBeginning( double dVal ) { initialpart = dVal; }

/* Maximum frequency of acceleration and deceleration */
/* This feature is calculated but not output to the .ext file */
// 20May09: Somesh: Changed default to 20
#define MAXFREQ 20.
double	maxfreq = MAXFREQ;
void SetMaxFrequency( double dVal ) { maxfreq = dVal; }

#define INDEXARGS 5

#define	MODULE		_T("EXTRACT")

#define TSEGBEGIN 0
#define TSEGEND   1

struct _ArrayDataE
{
	double tseg [NSEGMAX][2];
	double tseginput [NSEGMAX];
	double tsegsecondary [NSEGMAX];
	double startt [NSEGMAX];
	double deltat [NSEGMAX];
	double startx [NSEGMAX];
	double deltax [NSEGMAX];
	double starty [NSEGMAX];
	double deltay [NSEGMAX];
	double deltas [NSEGMAX];
	double vypeak [NSEGMAX];
	double rtvypeak [NSEGMAX];
	double aypeak [NSEGMAX];
	double rpenup [NSEGMAX];
	double zavg [NSEGMAX];
	double maxz [NSEGMAX];
	double minz [NSEGMAX];
	double linz [NSEGMAX];
	double varz [NSEGMAX];
	double straighterr [NSEGMAX];
	double slant [NSEGMAX];
	double slant2 [NSEGMAX];
	double slant80 [NSEGMAX];
	double surface [NSEGMAX];  /* LoopArea */
	double t1loop [NSEGMAX];
	double t2loop [NSEGMAX];
	double ndecel [NSEGMAX];
	double ayeff [NSEGMAX];
	double jabsq [NSEGMAX];
	double jabsqN [NSEGMAX];
	/* new columns for yJerk and NormalizedyJerk */
	double jyq [NSEGMAX];
	double jyqN [NSEGMAX];
	/* new columns with Relative duration of primary, Relative size of primary, Frequency of Secondary*/
	double rdeltat [NSEGMAX];
	double rdeltay [NSEGMAX];
	double freq [NSEGMAX];
	double absvel [NSEGMAX];
	double param [NSEGMAX];
	double roadlength[NSEGMAX];
};

// first trial
bool _FirstTrial = false;
void SetIsFirstTrial() { _FirstTrial = true; }

// score & statistic
double NJMeanPrev = 0.;
double scoreprev = 0.;
double GetTheScore() { return scoreprev; }
double GetTheStatistic() { return NJMeanPrev; }
void ResetTheScore( double dScore, double dNJMeanPrev )
{ scoreprev = dScore; NJMeanPrev = dNJMeanPrev; }

//#define TOTAL_FEATURES		28
// 04Oct08: GMB: Added new feature TrialSeq
// 25May09: Somesh: Added NumofStrokes
// 29June10: Somesh: Added yJerk, NormalizedyJerk, AverageNormalizedyJerk
#define TOTAL_FEATURES		33

// feedback
int _nFBColumn1 = -1, _nFBStroke1 = -1;
int _nFBColumn2 = -1, _nFBStroke2 = -1;
void SetTheFeedbackParams( short nCol, short nStroke, bool fSecond )
{
	if( !fSecond )
	{
		_nFBColumn1 = nCol;
		_nFBStroke1 = nStroke;
	}
	else
	{
		_nFBColumn2 = nCol;
		_nFBStroke2 = nStroke;
	}
}
#define IGNOREME			-99999.0
double _dFeedback1 = IGNOREME, _dFeedback2 = IGNOREME;
double GetTheFeedback( bool fSecond ) { return fSecond ? _dFeedback2 : _dFeedback1; }

// loop area
double looparea (double* x, double* y, int ifirst, int ilast, int imid1, int imid2,
				 double *t1loop, double *t2loop, double sec);

// Hack for 3d straightness
double rmsdist3 (double* x, double* y, double* z, int n, double a,
				 double b, double az, double bz);

/*************************************************************************/
bool Extract( CString szTFIn, CString szSegIn, CString szExtOut, unsigned int nSwitch )
/*************************************************************************/
{
	bool fRet = true;
	CString szMsg, txt;
	char szOpenMode[ 2 ];

	//11Jun08 : somesh: added vx and vy
	double *x = NULL, *y = NULL, *z = NULL, *aspec = NULL, *vx = NULL, *vy = NULL;

	int endoflastloop = 0, i = 0;
	int nArgCount = 1; // TODO: Trial/group/part/exp...etc.
	double aypeakpos = 0.0, aypeakneg = 0.0;
	double vypeakpos = 0.0, vypeakneg = 0.0;
	double tvypeakpos = 0.0, tvypeakneg = 0.0;
	int idecel = 0;
	int upordown = 0, upordownlim = 0;
	double ayold = 0.0;
	int ns = 0, ns2 = 0, ns2jabs = 0;
	int nsaspec = 0, nsaspecfilt = 0;
	int istroke = 0, nstroke = 0, nstrokesecondary = 0, nstrokeprocess = 0;
	int previousstroke = 0;
	int nprsmin = 0;
	int isam = 0, isam1 = 0, isam2 =0, isammid1, isammid2;
	CFileFind ff;
	FILE *fTF = NULL, *fSeg = NULL, *fExt = NULL;
	double a = 0.0, b = 0.0, r = 0.0, w = 0.0;
	/* Hack for 3d strokes */
	double az = 0.0, bz = 0.0, rz = 0.0;
	double sec = 0.0, prsmin = 0.0, beta = 0.0;
	int nsec = 0, penup = 0, nbeta = 0;
	bool create = false;
	int iarg = 0;
	bool secondary = false, threed = false, onestroke = false;
	double vabssum = 0.0;
	double jabsqNsum = 0.0, jabsqNmean = 0.0;
	double jyqNsum = 0.0, jyqNmean = 0.0;
	double score = 0.0;
	bool loop2stroke = false;
	double zsum = 0.0;
	// array for simplifying feedback value retrieval
	double* pFeatures = new double[ TOTAL_FEATURES ];
	_dFeedback1 = IGNOREME;
	_dFeedback2 = IGNOREME;
	// vars for trial sequence determination
	CStdioFile fSeq;
	CString szSeqFile, szLine, szItem, szCurCond;
	int nTrialSeq = 0, nTrial = 0, nRep = 0, nPos = -1;
	// 18May09: Somesh: variables for Number of Peak accel points algorithm
	double ayold_primary = 0.;
	int upordown_primary = 0, idecel_primary = 0;
	double dMissing = ::GetMissingDataValue();
	int stroken = 0;
	double thesqrt = 0.;
	double delta_temp = 0.0;
	int strokeinc = 0, strokefirst = 0;
	double s21 = 0.0, s23 = 0.0;
	double interp = 0.0;
	double seg_start_area = 0.0;
	double seg_end_area = 0.0;

	_ArrayDataE* padE = new _ArrayDataE;
	if( !padE )
	{
		szMsg.Format( IDS_EXT_MEM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		return false;
	}
	memset( (LPVOID)padE, 0, sizeof( _ArrayDataE ) );

	/*****************/
	/*** Switches  ***/
	/*****************/
	if( ( nSwitch & EXTRACT_SWITCH_S ) != 0 )
	{
		secondary = true;
		szMsg.Format( IDS_EXT_SEC, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & EXTRACT_SWITCH_3 ) != 0 )
	{
		threed = true;
		szMsg.Format( IDS_EXT_3, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & EXTRACT_SWITCH_2 ) != 0 )
	{
		loop2stroke = true;
		szMsg.Format( IDS_EXT_2, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & EXTRACT_SWITCH_O ) != 0 )
	{
		onestroke = true;
		szMsg.Format( IDS_EXT_ONE, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}

	/*************************/
	/*** Open segment file ***/
	/*************************/
	if( ( fopen_s( &fSeg, szSegIn, "r" ) ) != 0 )
	{
		szMsg.Format( IDS_EXT_SEGIN, MODULE, szSegIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	szMsg.Format( IDS_EXT_SEGREAD, MODULE, szSegIn );
	OutputMessage( szMsg, LOG_PROC );

	/******************************************/
	/* Input segmentation points */
	/*****************************************/

	/* Normal ballistic strokes are default */
	/* Nstroke is the number of strokes which is one less than the number of segmentation points */
	/* Thus if there are 4 segmentation points (0,1,2,3) */
	/* there are 3 strokes (0,1,2) and nstroke=3 */
	/* Therefore tseginput[nstroke] is the end of stroke [nstroke-1] */
	nstroke = -1 + getdata (fSeg, padE->tseginput, NSEGMAX, txt, NODECIMATE);

	/* 22Sep03: HLT; Test in case there are no strokes, e.g., one or no segmentation points */
	if (nstroke <= 0)
	{
		szMsg.Format( "%s: WARNING; No strokes", MODULE );
		OutputMessage( szMsg, LOG_PROC );

		/* Output at least one stroke which will have zero duration */
		nstroke = 1;

		/* Forget about secondary submovement analysis */
		secondary = false;
	}

	/* Secondary stroke analysis */
	if (secondary)
	{
		/* Inside each stroke there should be a segmentation between primary and secondary substrokes */
		nstrokesecondary = getdata (fSeg, padE->tsegsecondary, NSEGMAX, txt, NODECIMATE);
		/* If it turns out that there are no substrokes, set Secondary OFF */
		if (nstrokesecondary == 0)
		{
			szMsg.Format( IDS_EXTSEC_1, MODULE );
			OutputMessage( szMsg, LOG_PROC );
			secondary = false;
		}
		/* If there are substrokes but due to ??? fewer than strokes, set Secondary OFF */
		else if (nstrokesecondary != nstroke)
		{
			szMsg.Format( IDS_EXTSEC_2, MODULE );
			OutputMessage( szMsg, LOG_PROC );
			secondary = false;
		}
	}

	fclose( fSeg );
	fSeg = NULL;


	/* Make table with begin times and end times of */
	/* primary, secondary submovement if required */
	/* and total movement */
	/* Thus tim=2.4345 seconds at 100Hz becomes sample nr 243.45 */
	/* There are nstroke +1 tseginput data so the last one is istroke [nstroke] */
	/* Leave the data in seconds at this point as we don't have the value of sec*/
	nstrokeprocess = 0;
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		if (secondary)
		{
			padE->tseg [nstrokeprocess][TSEGBEGIN] = padE->tseginput [istroke];
			padE->tseg [nstrokeprocess][TSEGEND  ] = padE->tsegsecondary [istroke];
			nstrokeprocess++;
			padE->tseg [nstrokeprocess][TSEGBEGIN] = padE->tsegsecondary [istroke];
			padE->tseg [nstrokeprocess][TSEGEND  ] = padE->tseginput [istroke + 1];
			nstrokeprocess++;
		}
		padE->tseg [nstrokeprocess][TSEGBEGIN] = padE->tseginput [istroke];
		padE->tseg [nstrokeprocess][TSEGEND  ] = padE->tseginput [istroke + 1];//tsegsecondary [istroke];
		nstrokeprocess++;

		/* Stop collecting more input if the number of strokes exceeds NSEGMAX = 99*/
		if (nstrokeprocess > NSEGMAX)
			break;
	}

	/* New number of strokes can be the same or triple */
	nstroke = nstrokeprocess;

	/*05May03 Hlt: Combined the onestroke changes after getting the inputs from the segmentation points from the .seg file*/
	/* Onstroke analysis forces one stroke. */
	/* So use only first and last segmentation point combining all segment into one stroke */
    /* If Secondary submovement analysis, include the FIRST segmentation as well */
	/* EXAMPLE: */
	/* Location           0      1      2      3
	/* Segments:          T1     T2     T3     T4  (or 4 points or 3 strokes) */
	/* Location:          0  1   2  3   4  5   6
	/* In case of Sec=ON: T1 ST1 T2 ST2 T3 ST3 T4  (or 7 points or 6 strokes) */
	/* With Onstroke we move T4 to T2 which is in Location 1 and #strokes=1 (if Secondary=OFF) or */
	/* to Location 2 and #strokes=2 (if Secondary=ON) */
	/* NOTE: ST1 remains where it was as it will always be BEFORE the last segmentation point (T4 in the example). */
	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( nstroke >= 1 );
	if (onestroke)
	{
		if (secondary )
		{
			/* Leave the beginning of the first stroke */
			/* and make the end of the last stroke the end of the first stroke */
			/* So if the substrokes were: */
			/* 0:T1-ST1, 1:ST1-T2, 2:T1-T2, 3:T2-ST2, 4:ST2-T3, 5:T2-T3 (nstroke=6), */
			/* then we get */
			/* 0:T1-ST1, 1:ST1-T3, 2:T1-T3.*/
			/* We just say that ST1 remains whihc is correct if there are really no more than one stroke */
			/* otherwise you have to check in which stroke the peak occurs and take the secondary submovement */
			/* after that peak -- The assumption that the primary submovement is ballistic is then violated */
			/* This imply does not work in multiple real strokes */
			padE->tseg [1][TSEGEND  ] = padE->tseg [nstroke-1][TSEGEND  ];
			padE->tseg [2][TSEGEND  ] = padE->tseg [nstroke-1][TSEGEND  ];
			nstroke = 3;
		}
		else
		{
			/* Leave the beginning of the first stroke */
			/* and make the end of the last stroke the end of the first stroke */
			/* So if the strokes were 0:T1-T2, 1:T2-T3, 2:T3-T4 (nstroke=3), then we get 0:T1-T4.*/
			padE->tseg [0][TSEGEND  ] = padE->tseg [nstroke - 1][TSEGEND  ];
			nstroke = 1;

		}
	}

	/************************/
	/*** Open timfun file ***/
	/************************/
	if( ( fopen_s( &fTF, szTFIn, "r" ) ) != 0 )
	{
		szMsg.Format( IDS_EXT_TFIN, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

    /*******************************/
    /* Input heading of timfun file */
    /*******************************/
	/* Read data */
	szMsg.Format( IDS_EXT_TFREAD, MODULE, szTFIn );
	OutputMessage( szMsg, LOG_PROC );
	nsec = getdata (fTF, &sec, 1, txt, NODECIMATE);
	nprsmin = getdata (fTF, &prsmin, 1, txt, NODECIMATE);
	nbeta = getdata (fTF, &beta, 1, txt, NODECIMATE);

	// 10Sep03: GMB - Added asserts to ensure validity of indices
	ASSERT( sec > 0 );

	/* Test */
	if (nsec != 1 || nprsmin != 1 || nbeta != 1)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/*******************************/
	/* Convert segmentation points */
	/*******************************/
	/* Convert from seconds back to (real) sample numbers */
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		padE->tseg [istroke][TSEGBEGIN] /= sec;
		padE->tseg [istroke][TSEGEND  ] /= sec;

	}

	/**************/
	/* Input x, y */
	/**************/
	/* Allocate */
	x = new double[ NSMAX ];
	y = new double[ NSMAX ];
	if( !x || !y )
	{
		szMsg.Format( IDS_EXT_MEM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	/* Read */
	ns = getdata (fTF, x, NSMAX, txt, NODECIMATE);
	ns2 = getdata (fTF, y, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}


	/***********/
	/* Input z */
	/***********/
	/* Allocate */
	z = new double[ NSMAX ];
	if( !z )
	{
		szMsg.Format( IDS_EXT_MEM, MODULE );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
	/* Read */
	ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}


	/*******************************************/
	/* Intermezzo: Test padE->tseg[] input data */
	/*******************************************/
	/* Truncate nstroke so that all times are within the data range of ns */
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		if (padE->tseg [istroke][TSEGBEGIN] >= ns || padE->tseg [istroke][TSEGEND  ] >= ns)
		{
			szMsg.Format( IDS_EXT_INTMZZO, MODULE, szSegIn, istroke + 1,
						  padE->tseg [istroke], ns, nstroke, istroke);
			OutputMessage( szMsg, LOG_PROC );
			/* One less than nr of segmentation points */
			nstroke = istroke - 1;
			break;
		}
	}
	if( nstroke > NSEGMAX ) nstroke = NSEGMAX;

    /****************/
	/* Process x, y */
    /****************/
	// 23Jun08: Somesh: Temporary variable for interpolation
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		/********/
		/* Time */
		/********/
		/* Make them back into seconds */
		padE->startt [istroke] = padE->tseg [istroke][TSEGBEGIN] * sec;

		// 23Jun2008: Somesh: Combined horizontal and vertical position interpolation
		//*********************************************/
		//* Horizontal position and Vertical position */
		//*********************************************/
		delta_temp = padE->tseg [istroke][TSEGBEGIN] - (int) padE->tseg [istroke][TSEGBEGIN];
		if ( delta_temp != 0. )
		{
			padE->startx [istroke] = x [ (int) padE->tseg [istroke][TSEGBEGIN]	    ] * ( 1. - delta_temp)
								   + x [ (int)(padE->tseg [istroke][TSEGBEGIN]) + 1 ] * delta_temp ;
			padE->starty [istroke] = y [(int) padE->tseg [istroke][TSEGBEGIN]]
								+ delta_temp * (y [(int) padE->tseg [istroke][TSEGBEGIN] +1 ] - y [(int) padE->tseg [istroke][TSEGBEGIN]])
								+ 0.5 * delta_temp * ( delta_temp -1) * (y [(int) padE->tseg [istroke][TSEGBEGIN] + 2] - 2 * y [(int) padE->tseg [istroke][TSEGBEGIN] + 1] + y [(int) padE->tseg [istroke][TSEGBEGIN]])	;
		}
		else
		{
			padE->startx [istroke] = x [(int) padE->tseg [istroke][TSEGBEGIN]];
			padE->starty [istroke] = y [(int) padE->tseg [istroke][TSEGBEGIN]];
		}

		// 23Jun2008: Somesh: Combined deltax and deltay, start and end segment points interpolated
		/*************************************/
		/* Horizontal size and Vertical size */
		/*************************************/
		/* Needed for padE->deltas which is needed for normalizing straightness, padE->jabsq, ... */
		/* Deltax and y are overwritten if relative primary movement is needed */
		delta_temp = padE->tseg [istroke][TSEGEND] - (int) padE->tseg [istroke][TSEGEND];

		// 02Feb2009: Somesh: Don't interpolate for last stroke
		if ( delta_temp != 0. && istroke < nstroke - 1)
		{
			padE->deltax [istroke] = x [ (int) padE->tseg [istroke][TSEGEND]	  ] * ( 1. - delta_temp)
								   + x [ (int)(padE->tseg [istroke][TSEGEND]) + 1 ] * delta_temp
								   - padE->startx [istroke];
			delta_temp = padE->tseg [istroke][TSEGEND] - (int) padE->tseg [istroke][TSEGEND] + 1;
			padE->deltay [istroke] = y [(int) padE->tseg [istroke][TSEGEND] - 1 ]
									+ delta_temp * (y [(int) padE->tseg [istroke][TSEGEND ]] - y [(int) padE->tseg [istroke][TSEGEND ] - 1 ])
									+ 0.5 * delta_temp * ( delta_temp - 1 ) * (y [(int) padE->tseg [istroke][TSEGEND] + 1]
									- 2 * y [(int) padE->tseg [istroke][TSEGEND]] + y [(int) padE->tseg [istroke][TSEGEND] - 1])
									- padE->starty [istroke];
		}
		else
		{
			padE->deltax [istroke] = x [(int) padE->tseg [istroke][TSEGEND  ]] - padE->startx [istroke];
			padE->deltay [istroke] = y [(int) padE->tseg [istroke][TSEGEND  ]] - padE->starty [istroke];
		}

		/*****************/
		/* Absolute size */
		/*****************/
		padE->deltas [istroke] = sqrt (SQR (padE->deltax [istroke]) + SQR (padE->deltay [istroke]));

		/*******************/
		/* Stroke duration */
		/*******************/
		/* Make them back into seconds */
		/* added fabs to prevent any negative values for duration whatsoever*/
		padE->deltat [istroke] = fabs((padE->tseg [istroke][TSEGEND  ] - padE->tseg [istroke][TSEGBEGIN])) * sec;

		/*********************************/
		/* Straightness error normalized */
		/*********************************/
		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);
		w = 1.;

		if (threed)
		{
			/* Heck for 3d straightness */
			rregr (&x[isam1], &y[isam1], isam2 - isam1, w, &a, &b, &r);
			rregr (&z[isam1], &y[isam1], isam2 - isam1, w, &az, &bz, &rz);
			padE->straighterr [istroke] = rmsdist3 (&x[isam1], &y[isam1], &z[isam1],
											  isam2 - isam1, a, b, az, bz);
		}
		else
		{
			/* RMS distance is independent of stroke duration */
			padE->straighterr [istroke] = (double) rregr (&x[isam1], &y[isam1], isam2 - isam1,
													w, &a, &b, &r);
		}

		/* Normalize by length of stroke */
		/* Normalize by /padE->deltas */
		/* Becomes unitless */
		/* The following pattern (if not filtered) gets a=-0.5, b=+1.
		   The crossing points are typically (0.25,-0.25)
		   The average distance would be 0.25*sqrt(2)=.35
		   The length is 12.04 (Begin t=0; end t=17)
		   Normalized straightness error is .35/12.4=0.029
		0 0 1
		1 0 1
		1 1 1
		2 1 1
		2 2 1
		3 2 1
		3 3 1
		4 3 1
		4 4 1
		5 4 1
		5 5 1
		6 5 1
		6 6 1
		7 6 1
		7 7 1
		8 7 1
		8 8 1
		9 8 1
		9 9 1
		*/

		if (padE->deltas [istroke] != 0.)
			padE->straighterr [istroke] = padE->straighterr [istroke] / padE->deltas [istroke];


		/*******************************/
		/* Slant of the straight line  */
		/*******************************/
		/* Minimum squares fitted line */
		padE->slant [istroke]  = (double) atan (b);
		/* Orientation of the connection between begin and end */
		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isam1 >= 0 );
		ASSERT( isam2 >= 0 );

		if (y [isam2] - y [isam1] != 0. && x [isam2] - x [isam1] != 0.)
			padE->slant2 [istroke] = (double) atan2 ((double) (y [isam2] - y [isam1]),
											   (double) (x [isam2] - x [isam1]));
		else
			padE->slant2 [istroke] = 0.;

		// 23Jun08: Somesh: Fix for case x[isam2] - x[isam1] == 0.
		if (x [isam2] - x [isam1] == 0. && y [isam2] - y [isam1] > 0.)
			padE->slant2 [istroke] = PI/2;
		else
			if (x [isam2] - x [isam1] == 0. && y [isam2] - y [isam1] < 0.)
			padE->slant2 [istroke] = -PI/2;

		//There may be a problem, e.g., hp-mhd-t yields -2 and +2 rad.
		/* Make padE->slant in 4 quadrants just like padE->slant2 */
		if (fabs ((double) (padE->slant [istroke] - PI - padE->slant2 [istroke])) <
			fabs ((double) (padE->slant [istroke]      - padE->slant2 [istroke])))
			padE->slant [istroke]  -= PI;
		if (fabs ((double) (padE->slant [istroke] + PI - padE->slant2 [istroke])) <
			fabs ((double) (padE->slant [istroke]      - padE->slant2 [istroke])))
			padE->slant [istroke]  += PI;

		/***************************************************/
		/* Slant of the straight line of the initial 80ms  */
		/***************************************************/
		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP + initialpart / sec);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isam1 >= 0 );
		ASSERT( isam2 >= 0 );

		/* Artifact if isam1=isam2 */
		if (isam1 == isam2)
			isam2++;
		if (isam2 > padE->tseg [istroke][TSEGEND  ] + ROUNDUP)
			isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);
		w = 1.;
		rregr (&x[isam1], &y[isam1], isam2 - isam1, w, &a, &b, &r);

		/* Minimum squares fitted line */
		padE->slant80 [istroke]  = (double) atan (b);

		/* Make padE->slant in 4 quadrants just like padE->slant2 */
		if (fabs ((double) (padE->slant80 [istroke] - PI - padE->slant2 [istroke])) <
			fabs ((double) (padE->slant80 [istroke]      - padE->slant2 [istroke])))
			padE->slant80 [istroke]  -= PI;
		if (fabs ((double) (padE->slant80 [istroke] + PI - padE->slant2 [istroke])) <
			fabs ((double) (padE->slant80 [istroke]      - padE->slant2 [istroke])))
			padE->slant80 [istroke]  += PI;

		/* Difference between initial and total stroke */
		padE->slant80 [istroke] = padE->slant80 [istroke] - padE->slant [istroke];


		/***************************************************/
		/* Correction of padE->slant for performed rotation */
		/***************************************************/
		/* 05 Jun 03: Raji: Fold slant value back into the range between -PI and +PI */
		padE->slant [istroke] -= beta;
		if (padE->slant [istroke] >= PI)
			padE->slant [istroke] -= 2. *PI;
		else if (padE->slant [istroke] <= -PI)
			padE->slant [istroke] += 2. *PI;

		/* 5Jun00: hlt: Prevent strange values if secondary submovement is 0 */
		if (padE->deltat [istroke] == 0)
			padE->slant [istroke] = 0.;

	}
	if (secondary)
	{
		strokeinc = 3;
		strokefirst = 2;
	}
	else
	{
		strokeinc = 1;
		strokefirst = 0;
	}
	for (istroke = strokefirst; istroke < nstroke; istroke+= strokeinc)
	{
		/*************/
		/* Loop area */
		/*************/
		/* Calculate area of loop enclosed by previous and current stroke */
		/* In the case of secondary analysis, calculate the loop area between the present stroke and the previous complete stroke*/
//		loop2stroke = 0;	// now set via a parameter

		if (istroke  <= strokefirst)  /* Start at begin of first stroke */
		{
			isam1 = (int) padE->tseg [0][TSEGBEGIN];
			isammid1 = (int) padE->tseg [0][TSEGEND];
		}
		else
		{
			/* For stroke 3 and following: DO NOT go back 2 strokes but only */
			/* to the beginning of the previous stroke */
			/* isam1: consider 2 strokes ahead instead of 1 total stroke (= previoustroke)if the switch is set in extract*/
			if (loop2stroke && istroke >= 2 * strokeinc)
			{
				isam1 = (int) padE->tseg [istroke - strokeinc - strokeinc][TSEGBEGIN];
				isammid1 = (int) padE->tseg [istroke - strokeinc - strokeinc][TSEGEND];
			}
			else
			{
				isam1 = (int) padE->tseg [istroke - strokeinc][TSEGBEGIN];
				isammid1 = (int) padE->tseg [istroke - strokeinc][TSEGEND];
			}
			/* or end of last loop if that is closer */
			// 07Nov08: Somesh: was surface [istroke-1]
			if (padE->surface [istroke - strokeinc] != 0.)
			{
				endoflastloop = (int)(padE->t2loop [istroke - strokeinc] + ROUNDUP); /* Round up */
				if (isam1 < endoflastloop)
				{
					isam1 = endoflastloop;
					isammid1 = (int) padE->tseg [istroke][TSEGBEGIN];
				}
			}
		}

		// 13Jul09: Somesh: isammid2 serves same purpose as isammid1, except when loop2stroke is set it compensates for the extra segment involved
		isammid2 = (int) padE->tseg [istroke][TSEGBEGIN];
		/* End of the present stroke */
		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP); /* do not risk missing last point; can never exceed array boundary */

		// 13Jul09: Somesh: Added isammid1, isammid2 to eliminate small loops found within loops.
		// Only loops across 2 strokes but not within a stroke is calculated.
		/* Calculate the area of loop by calling the looparea function */
		padE->surface [istroke] = looparea (x, y, isam1, isam2, isammid1, isammid2, &padE->t1loop [istroke], &padE->t2loop [istroke], sec);

		/* 21Jun01 hlt Disabled normalization of looparea because small but round loops could have large loopareas */
		/* Normalize */
		//if (padE->deltay [istroke] != 0.)
		//	padE->surface [istroke] /= SQR (padE->deltay [istroke]);

		if (padE->surface [istroke] > 0)
		{
			szMsg.Format( IDS_EXT_STROKE1, MODULE, istroke + strokeinc);
			OutputMessage( szMsg, LOG_PROC );
		}
		else
		{
			szMsg.Format( IDS_EXT_STROKE2, MODULE, istroke + strokeinc);
			OutputMessage( szMsg, LOG_PROC );
		}
		if (secondary)
			padE->surface [istroke - 2] = padE->surface [istroke - 1] = 0.0;
	}

    /********/
	/* Free */
    /********/
	if( x ) delete [] x;
	x = NULL;
	if( y ) delete [] y;
	y = NULL;

	/*************/
	/* Process z */
	/*************/
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		/*****************************************************/
		/* Relative time or nr of samples that the pen is up */
		/*****************************************************/
		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isam1 >= 0 );
		ASSERT( isam2 >= 0 );

		penup = 0;
		for (isam = isam1; isam < isam2; isam++)
		{
			if (z [isam] > prsmin)
				penup++;
		}
		if ((isam2 - isam1) != 0.)
			padE->rpenup [istroke] = (double) penup / (isam2 - isam1);
		else
			padE->rpenup [istroke] = 0.;

		zsum = 0.;
		/* Avg. Pen pressure */
		for (isam = isam1; isam < isam2; isam++)
			zsum += z [isam];
		if ((isam2 - isam1) != 0.)
			padE->zavg [istroke] = zsum / (isam2 - isam1);
		else
			padE->zavg [istroke] = 0.;
	}

	// Clear z...we'll use again
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );

	/****************************/
	/* Input amplitude spectrum */
	/****************************/
	/* Allocate */
	aspec = new double[ NFREQMAX ];
	// Initialize aspec
	for( i = 0; i < NFREQMAX; aspec[ i++ ] = 0.0 );

	/* Read */
	nsaspec = getdata (fTF, aspec, NFREQMAX, txt, NODECIMATE);
	/**************************************/
	/* Input velocity spectra */
	/**************************************/
	/* Read into same aspec array */
	nsaspec = getdata (fTF, aspec, NFREQMAX, txt, NODECIMATE);
	/**************************************/
	/* Input accel spectra */
	/**************************************/
	/* Read into same aspec array */
	nsaspec = getdata (fTF, aspec, NFREQMAX, txt, NODECIMATE);
	/**************************************/
	/* Input amplitude spectra (filtered) */
	/**************************************/
	/* Read into same aspec array */
	nsaspecfilt = getdata (fTF, aspec, NFREQMAX, txt, NODECIMATE);
	/**************************************/
	/* Input velocity spectra (filtered) */
	/**************************************/
	/* Read into same aspec array */
	nsaspecfilt = getdata (fTF, aspec, NFREQMAX, txt, NODECIMATE);
	/**************************************/
	/* Input accel spectra (filtered) */
	/**************************************/
	/* Read into same aspec array */
	nsaspecfilt = getdata (fTF, aspec, NFREQMAX, txt, NODECIMATE);

	/************/
	/* Input vx */
	/************/
	/* Read */
	//11Jun08: Somesh: vx is loaded into its own variable for use in vabs calculation
	vx = new double [ NSMAX ];
	for( i = 0; i < NSMAX; vx[ i++ ] = 0.0 );
	ns2 = getdata (fTF, vx, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/************/
	/* Input vy */
	/************/
	/* Read */
	//19Jun08: Somesh: vy is loaded into its own variable for use in vabs calculation
	vy = new double [ NSMAX ];
	ns2 = getdata (fTF, vy, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}
    /**************/
	/* Process vy */
    /**************/
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		/*****************************************/
		/* Velocity, time of peak, peak, average */
		/*****************************************/
		// isam1 must be 1st sample inside istroke
		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		// isam2 must be last sample inside istroke
		// 21Apr08: gmb: we should have floored, not rounded up
//		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGEND]);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isam1 >= 0 );
		ASSERT( isam2 >= 0 );

		vypeakneg = 0.;
		vypeakpos = 0.;

		// 21Apr08: gmb: we need to initialize tvypeakneg/pos to start time of stroke
		// such that computation of rtvypeak will zero if end sample <= start sample
		tvypeakneg = padE->startt[ istroke ];
		tvypeakpos = padE->startt[ istroke ];

		// 25Sep08: Somesh: Loop runs upto <=isam2 and not <isam2
		for (isam = isam1; isam <= isam2; isam++)
		{
			if (vypeakneg > vy [isam])
			{
				vypeakneg = vy [isam];
				// 21Apr08: gmb: move sec multi. from rtvypeak calc to tvypeak calc
				//tvypeakneg = isam * sec;
				// 27Jun08: Somesh: moved sec multi. back to rtvypeak calculation, will use tvypeakneg sample value soon
				tvypeakneg = isam;
			}

			if (vypeakpos < vy [isam])
			{
				vypeakpos = vy [isam];
				// 21Apr08: gmb: move sec multi. from rtvypeak calc to tvypeak calc
				// tvypeakpos = isam * sec;
				// 27Jun08: Somesh: moved sec multi. back to rtvypeak calculation, use tvypeakpos sample value in next few lines
				tvypeakpos = isam;
			}
		}

		/* 29May00: hlt: Separated deltat>0 test and set 0 for 0 duration segments */
		if (padE->deltat [istroke] > 0.)
		{
			if (padE->deltay [istroke] > 0.)
			{
				//27Jun08: Somesh: Find maximum point of quadratic curve around estimated tvypeakpos
				isam = (int)tvypeakpos;
				//25Sep08: Somesh: Check for isam>0, if false set vypeak and rtvypeak to 0
				if (isam > 0)
				{
					s21 = vy [isam] - vy [isam-1];
					s23 = vy [isam+1] - vy[isam];
					if (s23 - s21 != 0)
						tvypeakpos = ( 2 * isam - 1 - (2 * s21 / (s23 - s21)) ) * 0.5;
					else
						tvypeakpos = isam;

				//Estimate actual vypeak
				delta_temp = tvypeakpos - isam + 1;
				vypeakpos = vy [isam - 1] + delta_temp * (vy [isam] - vy [isam-1])
							+ 0.5 * delta_temp * (delta_temp -1) * (vy [isam+1] - 2 * vy [isam] + vy [isam-1]);

				padE->vypeak [istroke] = vypeakpos;
				// 21Apr08: gmb: move sec multi. from rtvypeak calc to tvypeak calc
				// padE->rtvypeak [istroke] = (tvypeakpos - padE->startt [istroke]) / padE->deltat [istroke];

				// 27Jun08: Somesh: moved sec multiplication back to rtvypeak calculation from tvypeak calc
				padE->rtvypeak [istroke] = (tvypeakpos * sec - padE->startt [istroke]) / padE->deltat [istroke];
				}
				else
				{
					padE->vypeak [istroke] = 0;
					padE->rtvypeak [istroke] = 0;
				}
			}
			else
			{
				//27Jun08: Somesh: Find maximum point of quadratic curve around estimated tvypeakpos
				isam = (int)tvypeakneg;
				//25Sep08: Somesh: Check for isam>0, if false set vypeak and rtvypeak to 0
				if (isam > 0)
				{
					s21 = vy [isam] - vy [isam-1];
					s23 = vy [isam+1] - vy[isam];
					tvypeakneg = ( 2 * isam - 1 - (2 * s21 / (s23 - s21)) ) * 0.5;

					//Estimate actual vypeak
					delta_temp = tvypeakneg - isam + 1;
					vypeakneg = vy [isam - 1] + delta_temp * (vy [isam] - vy [isam-1])
							+ 0.5 * delta_temp * (delta_temp -1) * (vy [isam+1] - 2 * vy [isam] + vy [isam-1]);

					padE->vypeak [istroke] = vypeakneg;
					// 21Apr08: gmb: move sec multi. from rtvypeak calc to tvypeak calc
					//padE->rtvypeak [istroke] = (tvypeakneg - padE->startt [istroke]) / padE->deltat [istroke];

					// 27Jun08: Somesh: moved sec multiplication back to rtvypeak calculation from tvypeak calc
					padE->rtvypeak [istroke] = (tvypeakneg * sec - padE->startt [istroke]) / padE->deltat [istroke];
				}
				else
				{
					padE->vypeak [istroke] = 0;
					padE->rtvypeak [istroke] = 0;
				}
			}
		}

		// to avoid division by zero
		else
		{
			padE->vypeak [istroke] = 0.;
			padE->rtvypeak [istroke] = 0.;
		}
	}


	/**************/
	/* Input vabs */
	/**************/

	// Clear z
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );

	/* Read */
	ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/**************/
	/*process vabs*/
	/**************/
	/*calculation of average Vabs per stroke*/
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		//11Jun08: Somesh: Implemented Simpson's rule to integrate vabs across strokes
		isam1 = (int)floor(padE->tseg [istroke][TSEGBEGIN]);
		ASSERT( isam1 >= 0 );
		isam2 = (int)ceil(padE->tseg [istroke][TSEGEND  ]);
		ASSERT( isam2 >= 0 );

		//Estimate vabs at begin and end of segment, vabs = sqrt(vx^2+vy^2). Then find area.
		if ( padE->tseg [istroke][TSEGBEGIN] - isam1 != 0. )
		{
			if (isam1 >= 0)
			{
			delta_temp = padE->tseg [istroke][TSEGBEGIN] - isam1;
			interp = sqrt ( SQR ( vx [isam1] * ( 1. - delta_temp ) + vx [isam1+1] * delta_temp )
								+
							SQR ( vy [isam1] * ( 1. - delta_temp ) + vy [isam1+1] * delta_temp )
							);
			seg_start_area = 0.5 * ( interp + z [isam1+1] ) * ( 1. - delta_temp );
			isam1++;
			}
		}
		else
			seg_start_area = 0.0;

		// 02Feb09: Somesh: TODO- Extrapolate for last stroke, for now make interpolation zero
		if ( isam2 - padE->tseg [istroke][TSEGEND] != 0. && istroke < nstroke - 1)
		{
			if (isam2 > 0)
			{
				delta_temp = isam2 - padE->tseg [istroke][TSEGEND];
				interp = sqrt ( SQR ( vx [isam2-1] * delta_temp + vx [isam2] * ( 1. - delta_temp ) )
									+
								SQR ( vy [isam2-1] * delta_temp + vy [isam2] * ( 1. - delta_temp ) )
									);
				seg_end_area = 0.5 * ( interp + z [isam2-1] ) * ( 1. - delta_temp );
				isam2--;
			}
		}
		else
			seg_end_area = 0.0;

		vabssum = 0.0;
		for (isam = isam1; isam < isam2-1; isam+=2)
		{
			vabssum += z [isam] + 4 * z [isam+1] + z [isam+2];
		}

		vabssum = vabssum / 3;

		if ( (isam2-isam1+1) % 2 == 0. )
			vabssum += 0.5 * ( z [isam] + z [isam+1] );

		if (isam2 - isam1 == 0.)
			padE->absvel[istroke]= 0.0;
		else
			if (padE->tseg [istroke][TSEGEND] - padE->tseg [istroke][TSEGBEGIN] != 0.0 )
			padE->absvel[istroke] = ( vabssum + seg_start_area + seg_end_area ) / (padE->tseg [istroke][TSEGEND] - padE->tseg [istroke][TSEGBEGIN]);
			else padE->absvel[istroke] = 0.0;

	}

	/********/
	/* Free */
    /********/
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );

	/************************/
	/* calculate roadlength */
	/************************/
	for (istroke = 0; istroke < nstroke; istroke++)
	{
//		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
//		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);

		padE->roadlength[ istroke ] = padE->absvel[ istroke ] * padE->deltat[ istroke ];
	}

	/************/
	/* Input ax */
	/************/
	// Clear z
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );

	/* Read */
	ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/************/
	/* Input ay */
	/************/
	// Clear z
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );

	/* Read ay into z array */
	ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);
	/* Test */
	if (ns2 != ns)
	{
		szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/**************/
	/* Process ay */
	/**************/
	for (istroke = 0; istroke < nstroke; istroke++)
	{
		/*********************/
		/* Peak Acceleration */
		/*********************/
		if (istroke == 0)
			isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		else
			isam1 = (int)(padE->tseg [istroke][TSEGBEGIN]+ ROUNDUP);
		// 1Jul08: Somesh: Changed isam2 to Round-down
		//isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGEND  ]);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isam1 >= 0 );
		ASSERT( isam2 >= 0 );

		// 16Jul08: GMB - Changed initializers to extreme outlier values
		aypeakneg = 999999.;
		aypeakpos = -999999.;
		// NOTE: in theory, this should not be necessary, but it seems it is
		// ...there must be some condition below where these are not getting set
		tvypeakneg = 0;
		tvypeakpos = 0;
//		aypeakneg = 0.;
//		aypeakpos = 0.;

		// 07July08: Somesh: Implemented quadratic interpolation (identical to RelativeTimetoPeakVerticalVelocity)
		for (isam = isam1; isam < isam2; isam++)
		{
			if (aypeakneg > z [isam])
			{
				aypeakneg = z [isam];
				tvypeakneg = isam;
			}
			if (aypeakpos < z [isam])
			{
				aypeakpos = z [isam];
				tvypeakpos = isam;
			}
		}
		ASSERT( ( tvypeakneg >= 0 ) && ( tvypeakneg < NSMAX ) );
		ASSERT( ( tvypeakpos >= 0 ) && ( tvypeakpos < NSMAX ) );

		if (padE->deltay [istroke] > 0.)
		{
				isam = (int)tvypeakpos;
				//25Sep2008: Somesh: Check if isam>0, if not then set aypeak to 0
				if (isam > 0)
				{
					s21 = z [isam] - z [isam-1];
					s23 = z [isam+1] - z [isam];
					tvypeakpos = ( 2 * isam - 1 - (2 * s21 / (s23 - s21)) ) * 0.5;

					//Estimate actual aypeak
					delta_temp = tvypeakpos - isam + 1;
					aypeakpos = z [isam - 1] + delta_temp * (z [isam] - z [isam-1])
								+ 0.5 * delta_temp * (delta_temp -1) * (z [isam+1] - 2 * z [isam] + z [isam-1]);
					padE->aypeak [istroke] = aypeakpos;
				}
				else if (isam == 0 )
					padE->aypeak [istroke] = z [isam];
				else
					padE->aypeak [istroke] = 0;
		}
		else
		{
				isam = (int)tvypeakneg;
				//25Sep2008: Somesh: Check if isam>0, if not then set aypeak to 0
				if (isam > 0)
				{
					s21 = z [isam] - z [isam-1];
					s23 = z [isam+1] - z [isam];
					tvypeakneg = ( 2 * isam - 1 - (2 * s21 / (s23 - s21)) ) * 0.5;

					//Estimate actual aypeak
					delta_temp = tvypeakneg - isam + 1;
					aypeakneg = z [isam - 1] + delta_temp * (z [isam] - z [isam-1])
								+ 0.5 * delta_temp * (delta_temp -1) * (z [isam+1] - 2 * z [isam] + z [isam-1]);
					padE->aypeak [istroke] = aypeakneg;
				}
				else if (isam == 0)
					padE->aypeak [istroke] = z [isam];
				else
					padE->aypeak [istroke] = 0;

		}

		/* 5Jun00: hlt: Prevent strange values if secondary submovement is 0 */
		if (padE->deltat [istroke] == 0)
			padE->aypeak [istroke] = 0.;

		/*******************************************/
		/* Number of acceleration peaks per stroke */
		/*******************************************/
		/* Not per time unit as PDs get then very low scores */
		/* due to their slow movements */
		/* Prevent missing data by adding 1. at any rate */
		//padE->ndecel [istroke] = 1.;
		// 12May09: Somesh: Number of Peak acceleration points may be zero
		padE->ndecel [istroke] = 0.;
		isam1 = (int)(padE->tseg [istroke][TSEGBEGIN] + ROUNDUP);
		isam2 = (int)(padE->tseg [istroke][TSEGEND  ] + ROUNDUP);

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isam1 >= 0 );
		ASSERT( isam2 >= 0 );

		if (istroke == 0)
		{
			idecel = 0;
			upordown = 0;
			ayold = z [isam1];
			upordownlim = (int)(0.5 / (maxfreq * sec));
		}

		// 12May09: Somesh: If secondary analysis is enabled, values of idecel, ayold and upordown for the
		// total movement must be remembered from the primary submovement. If not, these are incorrectly
		// carried over from the secondary submovement.

		// Remember idecel, ayold and upordown for primary submovement
		if (secondary && istroke%3 == 0)
		{
			ayold_primary = ayold;
			upordown_primary = upordown;
			idecel_primary = idecel;
		}
		// Retrieve idecel, ayold and upordown for total movement from corresponding primary submovement
		if (secondary && istroke%3 == 2)
		{
			ayold = ayold_primary;
			upordown = upordown_primary;
			idecel = idecel_primary;
		}

		for (isam = isam1 + 1; isam < isam2; isam++)
		{
			if (z [isam] > ayold)
			{
				/* Upgoing */
				upordown++;
				if (upordown < 0)
				{
					upordown = 0;
				}
				else if (upordown > +upordownlim && idecel != +1)
				{
					idecel = +1;
					padE->ndecel [istroke] += 1.;
				}
			}
			else
			{
				/* Downgoing */
				upordown--;
				if (upordown > 0)
				{
					upordown = 0;
				}
				else if (upordown < -upordownlim && idecel != -1)
				{
					idecel = -1;
					padE->ndecel [istroke] += 1.;
				}
			}
			ayold = z [isam];
		}

		/********************/
		/* Force efficiency */
		/********************/
		/* Only used if jabs not available */
		if ((padE->aypeak [istroke] * SQR (padE->deltat [istroke])) != 0.)
			padE->ayeff [istroke] = padE->deltay [istroke] / (padE->aypeak [istroke] * SQR (padE->deltat [istroke]));
		else
			padE->ayeff [istroke] = 0.;

	}

	// Clear z
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );
	// Somesh: 25Jun2010: Read and discard aabs
	ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);

	// 29Jun10: Somesh: Jerk calculation now includes yJerk and absJerk.
	// Modified code according to posting: http://www.neuroscript.net/forum/showthread.php?3023-Normalized-Jerk-in-Exp-E01-is-wrong&p=7879#post7879

	/**************/
	/* Input jy */
	/**************/
	/* Read */
	/* jx (discard), jy */

	// Clear z...we'll use again
	for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );
	/* jx */
	ns2jabs = getdata (fTF, z, NSMAX, txt, NODECIMATE);
	/*******************************/
	/* Process jy only if exists */
	/*******************************/
	if (ns2jabs > 0)
	{
		/************/
		/* Input jy */
		/************/
		ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);

		/* Test (may be missing totally) */
		if (ns2jabs != ns)
		{
			szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}

		/**************/
		/* Process jy */
		/**************/
		for (istroke = 0; istroke < nstroke; istroke++)
		{
			/*********************************/
			/* Normalized integrated jy**2 */
			/*********************************/
			isam1 = (int)floor(padE->tseg [istroke][TSEGBEGIN]);
			isam2 = (int)ceil(padE->tseg [istroke][TSEGEND  ] );

			// 10Sep03: GMB - Added asserts to ensure validity of indices
			ASSERT( isam1 >= 0 );
			ASSERT( isam2 >= 0 );

			// 24Jun08: Somesh: Implemented Simpson's rule for integration, refer vabs document in forum for details
			/* Integrate j squared across stroke */
			/* The unit of jabs is cm/s**3 */
			if ( padE->tseg [istroke][TSEGBEGIN] - isam1 != 0. )
			{
				delta_temp = padE->tseg [istroke][TSEGBEGIN] - isam1;
				interp = SQR (z [isam1]) * ( 1. - delta_temp ) + SQR (z [isam1+1]) * delta_temp;
				seg_start_area = 0.5 * ( interp + SQR (z [isam1+1]) ) * ( 1. - delta_temp );
				isam1++;
			}
			else
				seg_start_area = 0.0;

			if ( isam2 - padE->tseg [istroke][TSEGEND] != 0. )
			{
				if (isam2 > 0)
				{
					delta_temp = isam2 - padE->tseg [istroke][TSEGEND];
					interp = SQR (z [isam2-1]) * delta_temp + SQR (z [isam2]) * ( 1. - delta_temp );
					seg_end_area = 0.5 * ( interp + SQR (z [isam2-1]) ) * ( 1. - delta_temp );
					isam2--;
				}
			}
			else
				seg_end_area = 0.0;

			padE->jyq [istroke] = 0.;
			for (isam = isam1; isam < isam2-1; isam+=2)
			{
				padE->jyq [istroke] += SQR (z [isam]) + 4 * SQR (z [isam+1]) + SQR (z [isam+2]);
			}

			padE->jyq [istroke] = padE->jyq [istroke] / 3;

			if ( (isam2-isam1+1) % 2 == 0. )
				padE->jyq [istroke] += 0.5 * ( SQR (z [isam]) + SQR (z [isam+1]) );

			/* The unit is now cm**2/s**6 */
			/* 1/sec times too many additions per stroke */
			/* The unit is now cm**2/s**5 */
			padE->jyq [istroke] = ( padE->jyq [istroke] + seg_start_area + seg_end_area ) * sec;

			padE->jyq[istroke] = sqrt ( padE->jyq [istroke] );
		}

		stroken = 0;

		if (!secondary)
			stroken = 1;
		else
			stroken = 3;

		thesqrt = 0.;
		for (istroke = stroken - 1; istroke < nstroke; istroke+=stroken)
		{
			/* Normalized Jerk*/
			if (padE->deltas [istroke] != 0.)
			{
				thesqrt = 0.5 * ( padE->deltat [ istroke ] / padE->deltas [ istroke ] ) *
					( padE->deltat [ istroke ] / padE->deltas [ istroke ] ) *
					padE->deltat [ istroke ] * padE->deltat [ istroke ] *
					padE->deltat [ istroke ];
				if( thesqrt < 0. ) thesqrt = 0.;
				padE->jyqN [istroke] =  padE->jyq [istroke] * sqrt( thesqrt );
			}
		}
		if (secondary)
		{
			for (istroke = stroken - 1; istroke < nstroke; istroke+=stroken)
			{
				padE->jyqN [istroke - 2] = padE->jyqN [istroke - 1] = padE->jyqN [istroke];
			}
		}
	}

	for (istroke = 0; istroke < nstroke; istroke++)
		jyqNsum += padE->jyqN [istroke];
	jyqNmean = jyqNsum/nstroke;


	/*******************************/
	/* Process jabs only if exists */
	/*******************************/
	if (ns2jabs > 0)
	{
		// Clear z...we'll use again
		for( i = 0; i < NSMAX; z[ i++ ] = 0.0 );

		/**************/
		/* Input jabs */
		/**************/
		ns2 = getdata (fTF, z, NSMAX, txt, NODECIMATE);

		/* Test (may be missing totally) */
		if (ns2jabs != ns)
		{
			szMsg.Format( IDS_EXT_TFERR, MODULE, szTFIn );
			OutputMessage( szMsg, LOG_PROC );
			fRet = false;
			goto Cleanup;
		}

		/****************/
		/* Process jabs */
		/****************/
		for (istroke = 0; istroke < nstroke; istroke++)
		{
			/*********************************/
			/* Normalized integrated jabs**2 */
			/*********************************/

			/* Here was a bug: based on two intervals, as borrowed from efficiency  */
			/* Watch out: integration without time interpolation */
			// 23Jun2008: Somesh: Changed rounding to floor and ceil
			isam1 = (int)floor(padE->tseg [istroke][TSEGBEGIN]);
			isam2 = (int)ceil(padE->tseg [istroke][TSEGEND  ] );

			// 10Sep03: GMB - Added asserts to ensure validity of indices
			ASSERT( isam1 >= 0 );
			ASSERT( isam2 >= 0 );

			// 24Jun08: Somesh: Implemented Simpson's rule for integration, refer vabs document in forum for details
			/* Integrate j squared across stroke */
			/* The unit of jabs is cm/s**3 */

			if ( padE->tseg [istroke][TSEGBEGIN] - isam1 != 0. )
			{
				delta_temp = padE->tseg [istroke][TSEGBEGIN] - isam1;
				interp = SQR (z [isam1]) * ( 1. - delta_temp ) + SQR (z [isam1+1]) * delta_temp;
				seg_start_area = 0.5 * ( interp + SQR (z [isam1+1]) ) * ( 1. - delta_temp );
				isam1++;
			}
			else
				seg_start_area = 0.0;

			if ( isam2 - padE->tseg [istroke][TSEGEND] != 0. )
			{
				if (isam2 > 0)
				{
					delta_temp = isam2 - padE->tseg [istroke][TSEGEND];
					interp = SQR (z [isam2-1]) * delta_temp + SQR (z [isam2]) * ( 1. - delta_temp );
					seg_end_area = 0.5 * ( interp + SQR (z [isam2-1]) ) * ( 1. - delta_temp );
					isam2--;
				}
			}
			else
				seg_end_area = 0.0;

			padE->jabsq [istroke] = 0.;
			for (isam = isam1; isam < isam2-1; isam+=2)
			{
				padE->jabsq [istroke] += SQR (z [isam]) + 4 * SQR (z [isam+1]) + SQR (z [isam+2]);
			}

			padE->jabsq [istroke] = padE->jabsq [istroke] / 3;

			if ( (isam2-isam1+1) % 2 == 0. )
				padE->jabsq [istroke] += 0.5 * ( SQR (z [isam]) + SQR (z [isam+1]) );

			/* The unit is now cm**2/s**6 */
			/* 1/sec times too many additions per stroke */
			/* The unit is now cm**2/s**5 */
			padE->jabsq [istroke] = ( padE->jabsq [istroke] + seg_start_area + seg_end_area ) * sec;

			padE->jabsq[istroke] = sqrt ( padE->jabsq [istroke] );
		}
	}
	//****Previous code****
	// /****************/
	//	/* Process jabs */
	//  /****************/
	//	for (istroke = 0; istroke < nstroke; istroke++)
	//	{
	//		/*********************************/
	//		/* Normalized integrated jabs**2 */
	//		/*********************************/

	//		/* Here was a bug: based on two intervals, as borrowed from efficiency  */
	//		/* Watch out: integration without time interpolation */
	//		isam1 = floor(padE->tseg [istroke][TSEGBEGIN]);
	//		isam2 = ceil(padE->tseg [istroke][TSEGEND  ]);

	//		// 10Sep03: GMB - Added asserts to ensure validity of indices
	//		ASSERT( isam1 >= 0 );
	//		ASSERT( isam2 >= 0 );
	//
	//		/* Integrate j squared across stroke */
	//		/* The unit of jabs is cm/s**3 */
	//		padE->jabsq [istroke] = 0.;

	//		/* Absolute Jerk for all strokes*/
	//		for (isam = isam1; isam < isam2; isam++)
	//		{
	//			/* The unit is now cm**2/s**6 */
	//			padE->jabsq [istroke] += SQR (z [isam]);
	//		}
	//		/* 1/sec times too many additions per stroke */
	//		/* The unit is now cm**2/s**5 */
	//		padE->jabsq [istroke] += interp;
	//		padE->jabsq [istroke] *= sec;

	//		/* jabsq is the absolute jerk*/
	//		padE->jabsq [istroke] = sqrt(padE->jabsq [istroke]);
	//	}
	//}

	/* Special data in case of secondary strokes are put in the total stroke data */
	/* WATCH OUT: Is this going well in Consis? */
	//	if (secondary)
	{
		stroken = 0;

		if (!secondary)
			stroken = 1;
		else
			stroken = 3;

		thesqrt = 0.;
		for (istroke = stroken - 1; istroke < nstroke; istroke+=stroken)
		{
			/* Normalized Jerk*/
			if (padE->deltas [istroke] != 0.)
			{
				thesqrt = 0.5 * ( padE->deltat [ istroke ] / padE->deltas [ istroke ] ) *
					( padE->deltat [ istroke ] / padE->deltas [ istroke ] ) *
					padE->deltat [ istroke ] * padE->deltat [ istroke ] *
					padE->deltat [ istroke ];
				if( thesqrt < 0. ) thesqrt = 0.;
				padE->jabsqN [istroke] =  padE->jabsq [istroke] * sqrt( thesqrt );
			}
		}
		if (secondary)
		{
			for (istroke = stroken - 1; istroke < nstroke; istroke+=stroken)
			{
				/* New column for Special data: Relative duration of primary submovement */
				if (padE->deltat [istroke] == 0.)
					padE->rdeltat [istroke] = 0.0001;   //Do not use 0 which may be omitted in averaging.
				else
					padE->rdeltat [istroke] = padE->deltat [istroke-2] / padE->deltat [istroke];
				/* New column for Special data: Relative size of primary submovement*/
				if (padE->deltay [istroke] == 0.)
					padE->rdeltay [istroke] = 0.0001;   //Do not use 0 which may be omitted in averaging.
				else
					padE->rdeltay [istroke] = padE->deltay [istroke-2] / padE->deltay [istroke];
				/* New Column for Special data: Frequency of secondary submovement*/
				if (padE->deltat [istroke-1] == 0.)
					padE->freq [istroke] = 0.0001;   //Do not use 0 which may be omitted in averaging.
				else
					padE->freq [istroke] = 1.;       //Count secondary submovements

				/* copy the same also for primary and secondary*/
				padE->rdeltat [istroke - 2] = padE->rdeltat [istroke - 1] = padE->rdeltat [istroke];
				padE->rdeltay [istroke - 2] = padE->rdeltay [istroke - 1] = padE->rdeltay [istroke];
				padE->freq [istroke - 2] = padE->freq [istroke - 1] = padE->freq [istroke];
				padE->jabsqN [istroke - 2] = padE->jabsqN [istroke - 1] = padE->jabsqN [istroke];
			}
		}
	}

	for (istroke = 0; istroke < nstroke; istroke++)
		jabsqNsum += padE->jabsqN [istroke];
	jabsqNmean = jabsqNsum/nstroke;


	/**********************/
	/* Calculate Scores   */
	/**********************/
	/* perform calculations only if a non-zero value, during reprocessing zerovalues will be encountered*/
	if (jabsqNmean > NJMeanPrev)
	/* deduct % increase in absolute jerk from previous score*/
		score = scoreprev - 0.5;
	else if (jabsqNmean < NJMeanPrev)
		score = scoreprev + 0.5;
	else
		score = scoreprev;
	if (score < 0.5)
		score = 0.0;
	if (score > 5.0)
		score = 5.0;
	szMsg.Format( IDS_EXT_SCORE, MODULE, scoreprev, NJMeanPrev, score, jabsqNmean);
	OutputMessage( szMsg, LOG_PROC );


	/* Close input file */
	fclose( fTF );
	fTF = NULL;

	/************************/
	/*** Open Output file ***/
	/************************/
	create = !ff.FindFile( szExtOut );

	/* Open for appending and creating */
	if( create ) strcpy_s( szOpenMode, "w" );
	else strcpy_s( szOpenMode, "a" );
	if( ( fopen_s( &fExt, szExtOut, szOpenMode ) ) != 0 )
	{
		szMsg.Format( IDS_EXT_OUT, MODULE, szExtOut);
		OutputMessage( szMsg, LOG_PROC );
		fRet = false;
		goto Cleanup;
	}

	/* File header */
	if (create)
	{
		szMsg.Format( IDS_EXT_OUTCREATE, MODULE, szExtOut);
		OutputMessage( szMsg, LOG_PROC );
		/* 10Oct03: Raji: Remove header for the new format of data*/
		//szMsg.Format( IDS_EXT_HDR1, szTFIn, szSegIn, szExtOut );
		//fprintf( fExt, szMsg );

		for (iarg = INDEXARGS; iarg < ( nArgCount + INDEXARGS ); iarg++)
			/* 29May00: hlt */
			/* Extract can accept more column names prepended in the .ext files */
			/* However, for now, we only use the Trial Nr. We want to see how this will work */
			//fprintf( fExt, _T("I%d "), iarg - INDEXARGS + 1 );
			fprintf( fExt, _T("Trial "));

		// 02Jul10: Somesh: There is a common header to EXT file
		/* Jerk is always included in TF file so we need not check for ns2jabs
		Submovement analysis does not change the header of EXT file, we just output 0 for the submovement data */
		szMsg.Format( IDS_EXT_HDR );
		fprintf( fExt, szMsg );

//		if (ns2jabs == 0)
//		{
//// 09Jul08: GMB: The 2 conditions yield the same result
////			if( secondary ) szMsg.Format( IDS_EXT_HDR2B );
////			else szMsg.Format( IDS_EXT_HDR2B );
//			szMsg.Format( IDS_EXT_HDR2B );
//			fprintf( fExt, szMsg );
//		}
//		else
//		{
//			// 09Jul08: GMB: The 2 conditions yield the same result
////			if( secondary ) szMsg.Format( IDS_EXT_HDR3B );
////			else szMsg.Format( IDS_EXT_HDR3B );
//			szMsg.Format( IDS_EXT_HDR3B );
//			fprintf( fExt, szMsg );
//		}
	}

	/***************************************/
	/* Write stroke features */
	/***************************************/
	szLine = szTFIn.Left( szTFIn.GetLength() - 3 );	// Remove extension
	txt = szLine.Right( 2 );
	iarg = atoi( txt );
	szMsg.Format( IDS_EXT_EXTOUT, MODULE, szExtOut, iarg );
	OutputMessage( szMsg, LOG_PROC );

	/* It is problematic if a missing or empty trial just disappears */
	/* Therefore, zero the output (already done) and output an empty stroke */
	if (nstroke == 0)
		nstroke = 1;

	/* for first trial the score is always 3*/
	if( _FirstTrial )
	{
		_FirstTrial = false;
		score = 3.0;
	}

	// 04Oct08: GMB: Added trial sequence calculation.
	// ...Derive the information from the SEQ file associated with the input TF file.
	// ...If the file does not exist, the value is 0.
	// ...calculate the actual trial sequence
	// Construct file name
	szSeqFile = szTFIn.Left( szTFIn.GetLength() - 8 );	// less CCC##.TF
	szSeqFile += _T(".SEQ");
	if( fSeq.Open( szSeqFile, CFile::modeRead ) )
	{
		// this condition
		szCurCond = szTFIn.Mid( szTFIn.GetLength() - 8, 3 );
		// this replication
		nRep = atoi( szTFIn.Mid( szTFIn.GetLength() - 5, 2 ) );

		// read through the file: each line grab trial #
		// if cond from file line = cur cond and rep# = this rep, then that is the TrialSeq
		while( fSeq.ReadString( szLine ) )
		{
			// if comment line or empty line, skip
			if( ( szLine == _T("") ) || ( szLine.Left( 2 ) == _T("//") ) )
				continue;

			// get this trial #
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 ) continue;
			szLine = szLine.Mid( nPos + 1 );
			szLine.Trim();
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 ) continue;
			szItem = szLine.Left( nPos );
			szItem.Trim();
			nTrial = atoi( szItem );

			// cleanup to next section
			nPos = szLine.Find( _T("Condition") );
			if( nPos == -1 ) continue;
			szLine = szLine.Mid( nPos );

			// get this cond id
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 ) continue;
			szLine = szLine.Mid( nPos + 1 );
			szLine.Trim();
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 ) continue;
			szItem = szLine.Left( nPos );
			szItem.Trim();
			// if not this cond, skip
			if( szItem.CompareNoCase( szCurCond ) != 0 ) continue;

			// cleanup to next section
			nPos = szLine.Find( _T("Replication") );
			if( nPos == -1 ) continue;
			szLine = szLine.Mid( nPos );

			// get this replication #
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 ) continue;
			szLine = szLine.Mid( nPos + 1 );
			szLine.Trim();
			nPos = szLine.Find( _T(" ") );
			if( nPos != -1 )
			{
				szItem = szLine.Left( nPos );
				szItem.Trim();
			}
			else szItem = szLine;
			// if not this rep #, skip
			if( nRep != atoi( szItem ) ) continue;

			// we've found our item
			nTrialSeq = nTrial;
			break;
		}
	}
	else
	{
		szMsg.Format( _T("%s: Unable to open trial sequence file (%s)"), MODULE, szSeqFile );
		OutputMessage( szMsg, LOG_PROC );
	}

    /* hlt: 5May03: Do for each stroke or substroke */
	/* If secondary submovement analysis is on then we do not have strokes but substrokes */
	/* in which case there are already twice as many substrokes as the number of strokes read */
    /* For each pair of substrokes we want to output a thrid "stroke" which is Substroke1+2 */
    /* The output storke counter is istroke3 which is going 3 times the number of strokes read */

	for (istroke = 0; istroke < nstroke; istroke++)
	{
       	/**************************/
		/* Output data per stroke */
		/**************************/
		/* Just to make it simple */
		if( ns2jabs > 0 ) padE->ayeff [istroke] = padE->jabsqN [istroke];

		// 19Aug08: GMB: Assign the values previously used to write the line to this array
		// 29Jun10: Somesh: Added yJerk, NormalizedyJerk and AverageNormalizedyJerk
		// IMPORTANT: Be sure TOTAL_FEATURES is in synch with increased/decreased # of features
		pFeatures[ 0 ] = iarg;
		pFeatures[ 1 ] = istroke + 1;
		pFeatures[ 2 ] = padE->startt[ istroke ];
		pFeatures[ 3 ] = padE->deltat[ istroke ];
		pFeatures[ 4 ] = padE->starty[ istroke ];
		pFeatures[ 5 ] = padE->deltay[ istroke ];
		pFeatures[ 6 ] = padE->vypeak[ istroke ];
		pFeatures[ 7 ] = padE->aypeak[ istroke ];
		pFeatures[ 8 ] = padE->startx[ istroke ];
		pFeatures[ 9 ] = padE->deltax[ istroke ];
		pFeatures[ 10 ] = padE->straighterr[ istroke ];
		pFeatures[ 11 ] = padE->slant[ istroke ];
		pFeatures[ 12 ] = padE->surface[ istroke ];
		pFeatures[ 13 ] = padE->slant80[ istroke ];
		pFeatures[ 14 ] = padE->rtvypeak[ istroke ];
		pFeatures[ 15 ] = padE->rpenup[ istroke ];
		pFeatures[ 16 ] = padE->rdeltat[ istroke ];
		pFeatures[ 17 ] = padE->rdeltay[ istroke ];
		pFeatures[ 18 ] = padE->freq[ istroke ];
		pFeatures[ 19 ] = padE->deltas[ istroke ];
		pFeatures[ 20 ] = padE->absvel[ istroke ];
		pFeatures[ 21 ] = padE->roadlength[ istroke ];
		pFeatures[ 22 ] = padE->jyq[ istroke ];
		pFeatures[ 23 ] = padE->jyqN[ istroke ];
		pFeatures[ 24 ] = jyqNmean;
		pFeatures[ 25 ] = padE->jabsq[ istroke ];
		pFeatures[ 26 ] = padE->jabsqN[ istroke ];
		pFeatures[ 27 ] = jabsqNmean;
		pFeatures[ 28 ] = score;
		pFeatures[ 29 ] = padE->ndecel[ istroke ];
		pFeatures[ 30 ] = padE->zavg[ istroke ];
		pFeatures[ 31 ] = nTrialSeq;
		pFeatures[ 32 ] = nstroke;

		// 19Aug08: GMB: If feedback column 1 and stroke 1 are specified, set feedback 1 value
		if( ( _nFBColumn1 > 0 ) && ( _nFBColumn1 <= TOTAL_FEATURES ) &&
			( _nFBStroke1 > 0 ) && ( _nFBStroke1 <= nstroke ) &&
			( istroke == ( _nFBStroke1 - 1 ) ) )
		{
			_dFeedback1 = pFeatures[ _nFBColumn1 - 1 ];
		}
		// 19Aug08: GMB: If feedback column 2 and stroke 2 are specified, set feedback 2 value
		if( ( _nFBColumn2 > 0 ) && ( _nFBColumn2 <= TOTAL_FEATURES ) &&
			( _nFBStroke2 > 0 ) && ( _nFBStroke2 <= nstroke ) &&
			( istroke == ( _nFBStroke2 - 1 ) ) )
		{
			_dFeedback2 = pFeatures[ _nFBColumn2 - 1 ];
		}

		/* 18May00: HLT: rpenup replaced by startx */
		/* Did NOT update the header line reflecting the new feature */
        /* hlt: 5May03: istroke3 instead of istroke+1 */
		/* raji:23Jul04: rearranged columns to accommodate new features, including rpenup*/
		// 19Aug08: GMB: Using new array filled above
		// 29Jun10: Somesh: Added yJerk, NormalizedyJerk AverageNormalizedyJerk
// 		szMsg.Format( _T("%d %d %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %f\n"),
// 						iarg, istroke + 1, padE->startt [istroke], padE->deltat [istroke],
// 						padE->starty [istroke], padE->deltay [istroke], padE->vypeak [istroke],
// 						padE->aypeak [istroke], padE->startx [istroke], padE->deltax [istroke],
// 						padE->straighterr [istroke], padE->slant [istroke], padE->surface [istroke],
// 						padE->slant80 [istroke], padE->rtvypeak [istroke], padE->rpenup [istroke],
// 						padE->rdeltat [istroke], padE->rdeltay [istroke], padE->freq [istroke],
// 						padE->deltas[istroke], padE->absvel[istroke], padE->roadlength[istroke],
// 						padE->jabsq[istroke], padE->jabsqN[istroke], jabsqNmean, score,
// 						padE->ndecel [istroke], padE->zavg [istroke]);
		szMsg.Format( _T("%d %d %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf %f %d %d\n"),
					  (int)(pFeatures[ 0 ]),	(int)(pFeatures[ 1 ]),
					  pFeatures[ 2 ],	pFeatures[ 3],
					  pFeatures[ 4 ],	pFeatures[ 5 ],
					  pFeatures[ 6 ],	pFeatures[ 7 ],
					  pFeatures[ 8 ],	pFeatures[ 9 ],
					  pFeatures[ 10 ],	pFeatures[ 11 ],
					  pFeatures[ 12 ],	pFeatures[ 13 ],
					  pFeatures[ 14 ],	pFeatures[ 15 ],
					  pFeatures[ 16 ],	pFeatures[ 17 ],
					  pFeatures[ 18 ],	pFeatures[ 19 ],
					  pFeatures[ 20 ],	pFeatures[ 21 ],
					  pFeatures[ 22 ],	pFeatures[ 23 ],
					  pFeatures[ 24 ],	pFeatures[ 25 ],
					  pFeatures[ 26 ],	pFeatures[ 27 ],
					  pFeatures[ 28 ],	pFeatures[ 29 ],
					  pFeatures[ 30 ],
					  (int)pFeatures[ 31 ],	(int)pFeatures[ 32 ]);

			/*
			[0]Trial								[1]Segment
			[2]StartTime							[3]Duration
			[4]StartVerticalPosition				[5]VerticalSize
			[6]PeakVerticalVelocity					[7]PeakVerticalAcceleration
			[8]StartHorizontalPosition				[9]HorizontalSize
			[10]StraightnessError					[11]Slant
			[12]LoopSurface							[13]RelativeInitalSlant
			[14]RelativeTimeToPeakVerticalVelocity	[15]RelativePenDownDuration
			[16]RelativeDurationofPrimary			[17]RelativeSizeofPrimary
			[18]FrequencyofSecondary				[19]AbsoluteSize
			[20]AverageAbsoluteVelocity				[21]Roadlength
			[22]yJerk								[23]NormalizedyJerk
			[24]AverageNormalizedyJerkPerTrial		[25]AbsoluteJerk
			[26]NormalizedJerk						[27]AverageNormalizedJerkPerTrial
			[28]Score								[29]NumberOfPeakAccelerationPoints
			[30]AveragePenPressure
			[31]NTrialSeq							[32]NumofStrokes
			*/

		fprintf( fExt, szMsg );
	}

	NJMeanPrev = jabsqNmean;
	scoreprev = score;

Cleanup:

    /****************/
    /* Free the rest */
    /****************/
	if( fTF ) fclose ( fTF );
	if( fSeg ) fclose( fSeg );
	if( fExt ) fclose( fExt );
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;
	if( aspec ) delete [] aspec;
	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( padE ) delete padE;
	if( pFeatures ) delete [] pFeatures;

	return fRet;
}


/****************************************************************************/
double looparea (double* x, double* y, int ifirst, int ilast, int imid1, int imid2, double *t1loop,
				 double *t2loop, double sec)
/****************************************************************************/

{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers & values
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( t1loop != NULL );
	ASSERT( t2loop != NULL );
	ASSERT( sec != 0 );

/* Max loop time is actually senseless because you do not know the time of begin of loop */
/* until you found also the end of loop */

/* BUG: If msxlooptime too short the loop is detected in the wrong stroke */

#define MAXLOOPTIME 5.  /* in Sec: Max time for 3 strokes; Max inter-sample periods after previous loop = 2 s = 400,200 points at 200,100Hz */
                         /* A large value slows the program down dramatically */
/* However it is very important to have maxlooptime not too small */
/* because otherwise the loop padE->surface is detected one stroke later */
#define MINLOOPTIME 0.05    /* in Sec: Min loop time in inter-sample periods = 200ms =40,20 points at 200,100 Hz */
#define MAXSPEED 25       /* Maximum pen speed in cm/s */

	double x1 = 0.0, y1 = 0.0, x2 = 0.0, y2 = 0.0;
	double dx1 = 0.0, dy1 = 0.0, dx2 = 0.0, dy2 = 0.0;
	double alpha1 = 0.0, alpha2 = 0.0;   /* Relative sample nrs between 0 (=isample) and 1 (=isample+1) */
	double alpha1c = 0.0, alpha2c = 0.0; /* Counter to calculate alpha */
	double alpha1d = 0.0, alpha2d = 0.0; /* Denominator to calculate alpha */
	int isample1 = 0, isample2 = 0;
	int deltasample1 = 0;
	double looparea = 0.0, loopa = 0.0;
	int isample = 0;
	double unitspersample = 0.0;
	int isample2previousloop = 0;

	/*******************************************************************/
	/* Do not check consecutive parts as they may be parallel */
	/* Use extra margin of MINLOOPTIME for convenience (gets rid of noise) */
	/* Use maxlooptime to speed up */
	/* isample2=last sample before closing of loop
	/* isample1=last sample before start of loop
	/* Do */
	/* isample2 = isample2previousloop +1 +MINLOOPTIME; isample1 = isample2previousloop +0      */
	/* isample2 = isample2previousloop +2 +MINLOOPTIME; isample1 = isample2previousloop +0,1,   */
	/* isample2 = isample2previousloop +3 +MINLOOPTIME; isample1 = isample2previousloop +0,1,2  */
	/* ... */
	/* isample2 = isample2previousloop +MAXLOOPTIME; isample1 = isample2previousloop + 0,1,2,...,MAXLOOPTIME - 1 */

	/* Where isample2 can be as small as ifirst and as large as <ilast

	/* Thus at any time all adjecent pairs have been tested */

	/* Max number of coordinate units crossed per intersample period */
	unitspersample = 1. / (2. * MAXSPEED * sec);

	/* Default if no loop found */
	/* Initialize */
	*t1loop = 0.;
	*t2loop = 0.;
	isample2previousloop = ifirst;


	/* BUG DOES NOT FIND FIRST LOOP IN WREHKA01.hwr */

	/* End of loop test runs from first to last sample */
	/* and jumps to the sample after each loop */
	isample2 = isample2previousloop;
	deltasample1 = 1;
	while (isample2 < MIN (ilast - 1, isample2previousloop + MAXLOOPTIME / sec))
	{
		/* Take every sample as it is unclear whether there may be a sample in the neighborhood */
		isample2 += 1;

		// 10Sep03: GMB - Added asserts to ensure validity of indices
		ASSERT( isample2 >= 0 );

		x2 = x [isample2];
		y2 = y [isample2];
		dx2 = x [isample2 + 1] - x2;
		dy2 = y [isample2 + 1] - y2;

		/* Search for begin and end of loop sample pairs */
		/* Begin of loop test runs from isample2previousloop till end of loop sample upto MINLOOPTIME */
		isample1 = isample2previousloop;
		/* Neighboring samples automatically touch with alpha1=1 and alpha2=0 */
		while (isample1 < MIN (isample2 - MINLOOPTIME / sec, isample2 - 1 - 1))
		{
			/* Sample increment increases with distance so that at least deltasample1 samples are */
			/* needed to let both points meet */
			/* Here the data are still in device coordinates and not in cm */
			isample1++;

			// 10Sep03: GMB - Added asserts to ensure validity of indices
			ASSERT( isample1 >= 0 );

			/* Discard if rectangles do not overlap */
			if (MAX (x [isample1], x [isample1 + 1]) <
				MIN (x [isample2], x [isample2 + 1]))
				goto trynextsample;
			if (MIN (x [isample1], x [isample1 + 1]) >
				MAX (x [isample2], x [isample2 + 1]))
				goto trynextsample;
			if (MAX (y [isample1], y [isample1 + 1]) <
				MIN (y [isample2], y [isample2 + 1]))
				goto trynextsample;
			if (MIN (y [isample1], y [isample1 + 1]) >
				MAX (y [isample2], y [isample2 + 1]))
				goto trynextsample;

			x1 = x [isample1];
			y1 = y [isample1];
			dx1 = x [isample1 + 1] - x1;
			dy1 = y [isample1 + 1] - y1;

			alpha1c = (x2 - x1) * dy2 - dx2 * (y2 - y1);
			alpha1d = dx1 * dy2 - dx2 * dy1;
			if (alpha1d == 0.)
				goto trynextsample;
			alpha1 = alpha1c / alpha1d;
			if (alpha1 < -0.001 || alpha1 > 1.001)
				goto trynextsample;

			/* Substitute in other formula */
			if (dx2 != 0.)
			{
				alpha2c = x1 + alpha1 * dx1 - x2;
				alpha2d = dx2;
			}
			else if (dy2 != 0.)
			{
				alpha2c = y1 + alpha1 * dy1 - y2;
				alpha2d = dy2;
			}
			else goto trynextsample;

			if (alpha2d == 0.)
				goto trynextsample;
			alpha2 = alpha2c / alpha2d;
			if (alpha2 < -0.001 || alpha2 > 1.001)
				goto trynextsample;

			// 13Jul09: Somesh: Check if the loop crossing is between points from different segments.
			// This is to eliminate small loops caused by sudden stops in movement to be detected instead of the original big loop.
			// eg: Small loops can be seen in EP6/PH2SZ3003C0401.HWR
			// If crossing points are in the same segment, move on
			// isample1 = start of first stroke in suspected loop
			// isample2 = end of last stroke in suspected loop
			// imid1 = end of first stroke
			// imid2 = begin of last stroke (this is imp when Calculate loop area between consecutive 2 strokes is checked)
			if ( ( (isample1 < imid1 && isample2 < imid1) || ((isample1 > imid1 && isample2 > imid1)) ) &&
				 ( (isample1 < imid2 && isample2 < imid2) || ((isample1 > imid2 && isample2 > imid2)) ) )
				goto trynextsample;

			/**************/
			/* Loop found */
			/**************/
			/* Real sample numbers of the two crossing points */
			*t1loop = isample1 + alpha1;
			*t2loop = isample2 + alpha2;

			/*************/
			/* Loop area */
			/*************/
			/* Estimate padE->surface by integrating innerproduct of the direction */
			/* vector with a 0.5*(y,-x) field, which has curl 1 */
			/* According to Stokes, the curve integral equals the padE->surface */
			/* integral of the curl */
			/* To reduce addition of large positive and negative numbers x,y at isample1 is deducted */
			/* The loop will thus be translated to the origin */
			/* For the differences between isample+1 and isample this is not needed */
			looparea = 0.;
			for (isample = isample1; isample < isample2; isample++)
			{
				loopa = (-(x [isample + 1] - x [isample]) * (y [isample] - y [isample1]) +
						(y [isample + 1] - y [isample]) * (x [isample] - x [isample1]));
				looparea += loopa;
			}
			/* Correct for 0.5 factor omitted in inner product */
			looparea *= 0.5;

#define MINLOOPAREA 0.0001 /* In cm2 to get rid of quantization and noise errors */

			if (fabs ((double) looparea ) > MINLOOPAREA)
			{
				/**********************************************/
				/* Start searching next loop after found loop */
				/**********************************************/
				isample2previousloop = isample2;
				return looparea;
			}

trynextsample:;    /* label because of deep nesting */

		}
	}

  /* Return zero loop area if no loop found */
  return 0.;
}

/*******************************************************************/
double rmsdist3 (double* x, double* y, double* z, int n, double a,
				 double b, double az, double bz)
/*******************************************************************/
/*
RMS distance of array of (x, y) points from line
  y = a + b * x

The perpendicular line through (x1, y1) or (x[is],y[is]) is
  (y - y1) = -1/b * (x - x1)

Solving y yields the x of the corssing point of both lines
  a + b * x = y1 - 1/b * (x - x1)
or
  x = (y1 - a - 1/b * (x - x1)) / b

*/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( z != NULL );

	int is = 0;
	double qdist = 0.0, xis0 = 0.0, yis0 = 0.0;
	double xis0z = 0.0, yis0z = 0.0;

	if (n == 0) return (0.);

	for (is = 0; is < n; is++)
	{
		xis0 = (x [is] + (y [is] - a) * b) / (SQR (b) + 1.);
		yis0 = a + b * xis0;
		xis0z = (z [is] + (y [is] - az) * bz) / (SQR (bz) + 1.);
		yis0z = az + bz * xis0z;
		qdist += SQR (xis0 - x [is]) + SQR (yis0 - y [is]) +
				 SQR (xis0z - z [is]) + SQR (yis0z - y [is]);
	}

	return sqrt (qdist / n);
}

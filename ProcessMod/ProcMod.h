#pragma once

#include "../Common/DefFun.h"
#include "../Common/ProcSettings.h"

#include "Resource.h"

#if _MSC_VER < 1400
#define	strcpy_s	strcpy
int fopen_s( FILE** fp, const char * f, const char *a );
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// Missing data values
void SetMissingDataValue( double dVal );
double GetMissingDataValue();

///////////////////////////////////////////////////////////////////////////////

bool Sentence2Word( CString szHWRIn, CString szHWROut, bool fDebug = false );
// PARAMETER SETTINGS
// Minimum leftward penlift movement between words (cm)
void SetMinLeftPenup( double dVal );
// Minimum leftward distance between words (cm)
void SetMinLeftSpacing( double dVal );
// Minimum word width (cm)
void SetMinWordWidth( double dVal );
// Minimum downward distance between word beginnings in successive lines (s)
void SetMinDownSpacing( double dVal );
// Minimum duration of penlifts between words (s)
void SetMinTimePenup( double dVal );
// Maximum duration of penlifts within a word (s)
void SetMaxTimePenup( double dVal );

///////////////////////////////////////////////////////////////////////////////

bool Unrotate( CString szHWRIn, CString szHWROut, bool fVelocity = false,
			   bool fDifferentiate = false, double dRotationBeta = 0.0 );

///////////////////////////////////////////////////////////////////////////////

#define	TF_SWITCH_NONE	0x0000	// Nothing
#define	TF_SWITCH_J		0x0001	// Calculate x, y, abs jerk and append to .tf file
#define	TF_SWITCH_R		0x0010	// Spectrum of raw signal instead of velocity
#define	TF_SWITCH_A		0x0020	// Spectrum of acceleration instead of velocity
#define	TF_SWITCH_U		0x0100	// Unrotate automatically
#define	TF_SWITCH_O		0x0400	// One-stroke trial rotated to +vertical under automatic rotation

bool TimeFunction( CString szHWRIn, CString szTFOut, double dFilterFreq = 23.5,
				   int nDecimate = 1, unsigned int nSwitch /*see above*/ = TF_SWITCH_NONE,
				   double dRotationBeta = 0.0 );

bool TimeFunctionG( CString szHWRIn, CString szTFOut, double dFilterFreq = 23.5,
					int nDecimate = 1, unsigned int nSwitch /*see above*/ = TF_SWITCH_NONE );

// PARAMETER SETTINGS
// FFT Low pass vs Butterworth Low pass for TIMFUN
void SetFFT( bool fVal );
// Sharpness for FFT
void SetSharpness( double dVal );
// Up-Sample Factor
void SetUpSampleFactor( int nVal );
// Remove Trailing Penlift
void RemoveTrailingPenlift( bool fVal );
bool GetRemoveTrailingPenlift();
// FFT Low Pass vs Butterworth Low Pass for TIMFUNG
void SetFFTG( bool fVal );
void SetSharpnessG( double dVal );
// global setting for # of samples for current HWR (for discontinuity)
// ... shades of a new ProcessMod with global vars instead of I/O (TODO)
void SetSampleCount( long nVal );
long GetSampleCount();

///////////////////////////////////////////////////////////////////////////////

#define	SEG_SWITCH_NONE		0x0000	// Nothing
#define	SEG_SWITCH_D		0x0001	// Debug mode
#define	SEG_SWITCH_F		0x0002	// Add first segment at any rate
#define	SEG_SWITCH_L		0x0004	// Add last segment at any rate
#define	SEG_SWITCH_S		0x0008	// Split each stroke into primary & secondary phases
#define	SEG_SWITCH_O		0x0010	// Do not split into strokes but just identify begin and end as one segment
#define	SEG_SWITCH_I		0x0020	// Drop segment between begin of record and load phase
#define	SEG_SWITCH_T		0x0040	// Drop segment between postload phase and end of record
#define	SEG_SWITCH_M		0x0080	// Segment at absolute velocity minima instead of vertical velocity zero crossings
#define	SEG_SWITCH_A		0x0100	// Segment at vertical acceleration zero crossings
#define	SEG_SWITCH_V		0x0200	// Move segmentation point to nearest pendown if on a penlift
#define	SEG_SWITCH_MM		0x0400	// Move segmentation point to the nearest absolute velocity minima within stroke
#define	SEG_SWITCH_J		0x0800	// Segmentation method: at pen down trajectories

#define	GRIPPER_TASK_DEFAULT		0
#define	GRIPPER_TASK_STANDARD		1	// standard gripper task
#define	GRIPPER_TASK_REPETITIVE		2	// repetitive pressure task
#define	GRIPPER_TASK_MAXIMUM		3	// maximum pressure task

bool Segment( CString szTFIn, CString szSegOut, unsigned int nSwitch /*see above*/ = SEG_SWITCH_NONE );
bool SegmentG( CString szTFIn, CString szSegOut,
			   unsigned int nSwitch /*see above*/ = SEG_SWITCH_NONE,
			   unsigned int nTask = GRIPPER_TASK_DEFAULT );
// PARAMETER SETTINGS
// Min stroke size (cm)
void SetMinStrokeSize( double dVal );
// Min stroke size relative to max stroke per trial
void SetMinStrokeSizeRelativeToMax( double dVal );
// Min stroke duration (s)
void SetMinStrokeDuration( double dVal );
// Min vertical velocity of coarse beginning/end of first/last stroke (relative to vertical peak velocity/trial)
void SetMinVertVelocity( double dVal );
// Max vertical velocity
void SetMaxVertVelocity( double dVal );

///////////////////////////////////////////////////////////////////////////////

#define	EXTRACT_SWITCH_NONE	0x0000	// Nothing
#define	EXTRACT_SWITCH_S	0x0001	// Secondary analysis splits every stroke into 2 substrokes
									// - primary and secondary submovements
#define	EXTRACT_SWITCH_3	0x0010	// 3D estimation of straightness : y=long axis, x & z=short axes
#define	EXTRACT_SWITCH_O	0x1000	// Onestroke analysis combines all strokes as if it were one
#define	EXTRACT_SWITCH_2	0x2000	// CCalculate loop area between every consecutive 2 strokes, instead of every consecutive stroke
				// NOTE: Onestroke & Secondary analysis implies only Primary segment

bool Extract( CString szTFIn, CString szSegIn, CString szExtOut,
			  unsigned int nSwitch /*see above*/ = EXTRACT_SWITCH_NONE );
bool ExtractG( CString szTFIn, CString szSegIn, CString szExtOut,
			   unsigned int nSwitch /*see above*/ = EXTRACT_SWITCH_NONE,
			   unsigned int nTask = GRIPPER_TASK_DEFAULT );
// PARAMETER SETTINGS
// Beginning of the stroke to base the slant upon (s)
void SetStrokeBeginning( double dVal );
// Maximum frequency of acceleration and deceleration
void SetMaxFrequency( double dVal );
// Indicates this is the first trial of a given subject
void SetIsFirstTrial();
// Scoring
double GetTheScore();
double GetTheStatistic();
void ResetTheScore( double dScore, double dNJMeanPrev );
// Feedback information
void SetTheFeedbackParams( short nCol, short nStroke, bool fSecond );
double GetTheFeedback( bool fSecond );

///////////////////////////////////////////////////////////////////////////////

#define	CONSIS_SWITCH_NONE	0x0000	// Nothing
#define	CONSIS_SWITCH_A		0x0001	// Absolute = Use absolute measures instead of relative measures
#define	CONSIS_SWITCH_N		0x0004	// Nohead = Suppress header in output file (handy when appending)
#define	CONSIS_SWITCH_M		0x0010	// More_than_max_is_err = Discard also if more strokes than required
#define	CONSIS_SWITCH_U		0x0040	// noUpstroketest = Do not remove initial downstrokes (default=remove)
#define	CONSIS_SWITCH_C		0x0100	// antiCipationtest = Test for too short RT
#define	CONSIS_SWITCH_S		0x0400	// Substitutemissingtrials = Missing trials are copied from previous or following trial
#define	CONSIS_SWITCH_O		0x1000	// secOndary analysis splits every stroke into 2 substrokes
									// - primary and secondary submovements
//  Number of trials for conditions 1,2,3,... are appended

// NOTE: Trials are placed in the last input parameter; each trial should be a string and
//		 added to a stringlist; pass the reference of the list to the call below
// OR alternatively call second method with trials in a single string separated by spaces

bool Consis( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			 short nStrokeMax, double dStrokeLength, double dRangeLength,
			 double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			 CString szCondition, CString szErrOut, unsigned int nSwitch /*see above*/,
			 CStringList* pszlTrials /*see NOTE above*/, bool fLastOnly,
			 bool* fPassed );
bool Consis( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			 short nStrokeMax, double dStrokeLength, double dRangeLength,
			 double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			 CString szCondition, CString szErrOut, unsigned int nSwitch /*see above*/,
			 CString szTrials /*see NOTE above*/, bool fLastOnly,
			 bool* fPassed );
bool ConsisG( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			short nStrokeMax, double dStrokeLength, double dRangeLength,
			double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			CString szCondition, CString szErrOut, unsigned int nSwitch /*see above*/,
			CStringList* pszlTrials /*see NOTE above*/, bool fLastOnly,
			bool* fPassed, unsigned int nTask = GRIPPER_TASK_DEFAULT );
bool ConsisG( CString szExtIn, CString szConOut, CString szLexIn, short nStrokeMin,
			  short nStrokeMax, double dStrokeLength, double dRangeLength,
			  double dStrokeDirection, double dRangeDirection, int nStrokeSkip,
			  CString szCondition, CString szErrOut, unsigned int nSwitch /*see above*/,
			  CString szTrials /*see NOTE above*/, bool fLastOnly,
			  bool* fPassed, unsigned int nTask = GRIPPER_TASK_DEFAULT );

// PARAMETER SETTINGS
// Maximum slant error for strokes of a certain direction
void SetSlantError( double dVal );
// Minimum vertical stroke size increase ratio between succesive strokes in outward spiral
void SetSpiralIncreaseFactor( double dVal );
// Minimum loop area (cm2)
void SetMinLoopArea( double dVal );
// Maximum straightness error for straight segments
void SetMaxStraightErrorStraight( double dVal );
// Minimum straightness departure for curved segments
void SetMinStraightErrorCurved( double dVal );
// Discard if less than Minimum Reaction Time
void SetDiscardLTMin( bool fVal );
void SetDiscardLTMinG( bool fVal );
// Minimum Reaction Time
void SetMinReactionTime( double dVal );
void SetMinReactionTimeG( double dVal );
// Discard if greather than Maximum Reaction Time
void SetDiscardGTMax( bool fVal );
void SetDiscardGTMaxG( bool fVal );
// Maximum Reaction Time
void SetMaxReactionTime( double dVal );
void SetMaxReactionTimeG( double dVal );
// Discard trial if target order is not maintainted
void SetFlagBadTarget( bool newVal );
// The control of whether we want to tell the difference between the circle and lll
void SetCheckCircleLLL( bool fVal );
//the threshhold for the ratio of correct number of circles in the sequence
void SetThresholdRatioCircleSequence( double dVal );
// the threshhold for the ratio of relative distance of one circle
void SetThresholdHorizRelCircle( double dVal );
//the threshhold for the ratio of correct number of Ls in the sequence
void SetThresholdRatioLSequence( double dVal );
// the threshhold for the ratio of relative distance of one L
void SetThresholdHorizRelL( double dVal );
// ignore first/last error of circle/l
void SetCircleLLLIgnore( bool fCIF, bool fCIL, bool fLIF, bool fLIL );
// flag as failed for discontinuity
void SetFlagDiscontinuity( bool fVal );
// discontinuity relative intersample period error
void SetDisError( double dVal );
// magnetic force (gripper)
void SetMagnetForce( double dVal );

///////////////////////////////////////////////////////////////////////////////

#define	ST_SWITCH_NONE	0x0000	// Nothing
#define	ST_SWITCH_A		0x0001	// Absolute values
#define ST_SWITCH_SM	0x0002	// Submovement analysis
#define ST_SWITCH_ONE	0x0004	// Use one line for submovement data
#define	ST_SWITCH_R		0x0010	// Relative data = data / mean_across_strokes; May require /a
#define	ST_SWITCH_Z		0x0100	// Output missing strokes as zeroes
#define	ST_SWITCH_T		0x1000	// Output missing trials as zeroes (Use only with /z)
#define	ST_SWITCH_S		0x2000	// Substitute missing trials
#define	ST_SWITCH_D		0x4000	// Discard substitutions after trial#
#define	ST_SWITCH_D2	0x8000	// Discard after trial#

// Return values
// true - There was 1+ consistent trials processed
// false - There were no consistent trials processed
#define END_false	2	// We need to stop processing
// Summarize
bool SumTrial( CString szDataIn, CString szIncOut, CString szErrOut,
			   CString szGroup, CString szSubj, CString szCond,
			   unsigned int nSwitch /*see above*/, int nTrialCount, int nDiscardAfter,
			   int nDiscardAfter2, bool fIgnoreExclude );

// PARAMETER SETTINGS
void SetAveraging( bool fUDStrokes, bool fStrokes, bool fTrials, bool fMedian );
void SetIncludeOnlyStrokesPenlifts( bool fWithoutLifts, bool fWithLifts, double dThreshold );

///////////////////////////////////////////////////////////////////////////////

bool Group( CString szEVAIn, CString szGRPOut, bool fBetweenData, bool fTestAdded );

///////////////////////////////////////////////////////////////////////////////

bool Wilcx( CString szGRPIn, CString szLSTOut, bool fLogTransform );

///////////////////////////////////////////////////////////////////////////////

#define	X_SWITCH_NONE	0x0000	// Nothing
#define	X_SWITCH_S		0x0001	// Read, avg., and output per subject as indicated by column 2
#define	X_SWITCH_A		0x0010	// Abs. transformation for metric difference data
#define	X_SWITCH_L		0x0100	// Log/Exp transformation for metric difference data
#define	X_SWITCH_N		0x1000	// No header line in output file
#define	X_SWITCH_I		0x4000	// Input file is not .inc file

typedef enum eXSwitch
{
	eX_None		= 0x0000,	// No switch selected
	eX_S		= 0x0001,	// Read, avg., and output per subject as indicated by column 2
	eX_A		= 0x0010,	// Abs. transformation for metric difference data
	eX_L		= 0x0100,	// Log/Exp transformation for metric difference data
	eX_N		= 0x1000,	// No header line in output file
	eX_I		= 0x4000	// Input file is not .inc file
} XSwitch;

// nfactor1, doffset1, dscale1 related to x-axis for analysis graph
// nfactor2, doffset2, dscale2 related to grouping for analysis graph
bool XMean( CString szIncIn, CString szXmeOut, int nFactor1, int nFactor2,
			double dOffset1, double dScale1, double dOffset2, double dScale2,
		    unsigned int nSwitch, double& dInMin1, double& dInMax1, double& dInMin2,
			double& dInMax2, double& dMax1, double& dMax2, bool fSeparateThread = TRUE );

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#define SHARPNESS 1.75
// TODO: This will require a lot of work.
// All variables/values that are used throughout the various modules should be here.
class NSProcModData
{
public:
	NSProcModData()
	{
		Reset();
	}

	~NSProcModData()
	{
// 		if( x ) delete [] x;
// 		if( y ) delete [] y;
// 		if( xout ) delete [] xout;
// 		if( yout ) delete [] yout;
// 		if( zout ) delete [] zout;
// 		if( z ) delete [] z;
// 		if( xbuf ) delete [] xbuf;
// 		if( ybuf ) delete [] ybuf;
// 		if( zbuf ) delete [] zbuf;
// 		if( xin ) delete [] xin;
// 		if( yin ) delete [] yin;
// 		if( zin ) delete [] zin;
// 		if( vx ) delete [] vx;
// 		if( vy) delete [] vy;
// 		if( vaspectrum ) delete [] vaspectrum;
	}

	void Setup()
	{
// 		x = new double[ NSMAX ];
// 		y = new double[ NSMAX ];
// 		xout = new double[ NSMAX ];
// 		yout = new double[ NSMAX ];
// 		zout = new double[ NSMAX ];
// 		z = new double[ NSMAX ];
// 		xbuf = new double[ NSMAX ];
// 		ybuf = new double[ NSMAX ];
// 		zbuf = new double[ NSMAX ];
// 		xin = new double[ NSMAX ];
// 		yin = new double[ NSMAX ];
// 		zin = new double[ NSMAX ];
// 		vaspectrum = new double[ NSMAX ];
// 		vx = new double[ NSMAX ];
// 		vy = new double[ NSMAX ];
	}

	void Reset()
	{
// 		if( !fSetup ) Setup();
//
// 		fftlw = false;
// 		sharpness = SHARPNESS;
// 		upsamplefactor = 1;
// 		_Samples = 0;
// 		memset( x, 0, NSMAX * sizeof( double ) );
// 		memset( y, 0, NSMAX * sizeof( double ) );
// 		memset( z, 0, NSMAX * sizeof( double ) );
// 		memset( xout, 0, NSMAX * sizeof( double ) );
// 		memset( yout, 0, NSMAX * sizeof( double ) );
// 		memset( zout, 0, NSMAX * sizeof( double ) );
// 		memset( xbuf, 0, NSMAX * sizeof( double ) );
// 		memset( ybuf, 0, NSMAX * sizeof( double ) );
// 		memset( zbuf, 0, NSMAX * sizeof( double ) );
// 		memset( xin, 0, NSMAX * sizeof( double ) );
// 		memset( yin, 0, NSMAX * sizeof( double ) );
// 		memset( zin, 0, NSMAX * sizeof( double ) );
// 		memset( vx, 0, NSMAX * sizeof( double ) );
// 		memset( vy, 0, NSMAX * sizeof( double ) );
// 		memset( vaspectrum, 0, NSMAX * sizeof( double ) );
	}

	// TIME FUNCTION VARS
	bool fSetup;
	// FFT Low Pass vs Butterworth Low Pass
	bool fftlw;
	double sharpness;
	// Up-Sample Factor
	int upsamplefactor;
	// global for # of samples read for current raw
	long _Samples;
	// data arrays
	double* x;
	double* y;
	double* xout;
	double* yout;
	double* zout;
	double* z;
	double* xbuf;
	double* ybuf;
	double* zbuf;
	double* xin;
	double* yin;
	double* zin;
	double* vaspectrum;
	double* vx;
	double* vy;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// FeatureData.cpp

#include "../MFC/mfc.h"
#include "ProcMod.h"
#include "FeatureData.h"
#include "../Common/DefFun.h"
#include "../Common/ProcSettings.h"


// maximum # of trials (TODO: where else is this defined?)
#define MAX_TRIALS 99

// DEFINE to warn of over limit of columns
#define WARN_OVER_LIMIT() { \
	szMsg = _T("SUMTRIAL: WARNING: Column limit exceeded."); \
	OutputMessage( szMsg, LOG_PROC ); \
	return false; }
#define WARN_OVER_LIMIT_BREAK() { \
	szMsg = _T("SUMTRIAL: WARNING: Column limit exceeded."); \
	OutputMessage( szMsg, LOG_PROC ); \
	break; }

// IMPORTANT: If any labels of features changes, this file must be updated
//			  That includes the sequence, as well.
static FeatureData featureData[] =
{
	{ 0, "Group", false, false, true, "\0" },
	{ 1, "Subject", false, false, true, "\0" },
	{ 2, "Condition", false, false, true, "\0" },
	{ 3, "Trial", false, false, true, "\0" },
	{ 4, "Segment", false, false, true, "\0" },
	{ 5, "StartTime", false, false, true, "\0" },
	{ 6, "Duration", true, true, true, "\0" },
	{ 7, "StartVerticalPosition", false, false, true, "\0" },
	{ 8, "VerticalSize", true, true, true, "\0" },
	{ 9, "PeakVerticalVelocity", true, true, true, "\0" },
	{ 10, "PeakVerticalAcceleration", true, true, true, "\0" },
	{ 11, "StartHorizontalPosition", false, false, true, "\0" },
	{ 12, "HorizontalSize", true, true, true, "\0" },
	{ 13, "StraightnessError", true, true, true, "\0" },
	{ 14, "Slant", true, true, true, "\0" },
	{ 15, "LoopSurface", false, false, true, "\0" },
	{ 16, "RelativeInitialSlant", false, false, true, "\0" },
	{ 17, "RelativeTimeToPeakVerticalVelocity", true, true, true, "\0" },
	{ 18, "RelativePenDownDuration", true, true, true, "\0" },
	{ 19, "RelativeDurationofPrimary", true, true, false, "RelativeDuration" },
	{ 20, "RelativeSizeofPrimary", true, true, false, "RelativeSize" },
	{ 21, "FrequencyofSecondary", false, true, false, "\0" },
	{ 22, "AbsoluteSize", true, true, true, "\0" },
	{ 23, "AverageAbsoluteVelocity", true, true, true, "\0" },
	{ 24, "Roadlength", true, true, true, "\0" },
	{ 25, "AbsoluteyJerk", true, true, true, "\0" },
	{ 26, "NormalizedyJerk", false, false, true, "\0" },
	{ 27, "AverageNormalizedyJerkPerTrial", false, false, true, "\0" },
	{ 28, "AbsoluteJerk", true, true, true, "\0" },
	{ 29, "NormalizedJerk", false, false, true, "\0" },
	{ 30, "AverageNormalizedJerkPerTrial", false, false, true, "\0" },
	{ 31, "Score", false, false, true, "\0" },
	{ 32, "NumberOfPeakAccelerationPoints", true, true, true, "\0" },
	{ 33, "AveragePenPressure", true, true, true, "\0" }
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    DoSort
// FullName:  DoSort
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: double * d
// Parameter: int nCount
// Parameter: double * dRes
// Parameter: int & nResCount
//************************************
void DoSort( double* d, int nCount, double* dRes, int& nResCount )
{
	if( !d || !dRes || ( nCount <= 0 ) ) return;

	int j = 0;
	double dValue = 0.;
	for( int i = 1; i < nCount; i++ )
	{
		dValue = d[ i ];
		j = i - 1;
		while( ( j >= 0 ) && ( d[ j ] > dValue ) )
		{
			d[ j + 1 ] = d[ j ];
			j--;
		}
		d[ j + 1 ] = dValue;
	}
	// now place into output array excluding any missing values
	double dMissing = ::GetMissingDataValue();
	nResCount = 0;
	j = 0;
	for( int i = 0; i < nCount; i++ )
	{
		if( d[ i ] != dMissing )
		{
			dRes[ j++ ] = d[ i ];
			nResCount++;
		}
	}
}

//************************************
// Method:    GetAverage
// FullName:  GetAverage
// Access:    public
// Returns:   double
// Qualifier:
// Parameter: double * d
// Parameter: int nCount
// Parameter: int & nCounted
//************************************
double GetAverage( double* d, int nCount, int& nCounted )
{
	if( !d || ( nCount <= 0 ) ) return 0.;

	nCounted = nCount;
	double dRet = 0., dMissing = ::GetMissingDataValue();
	for( int i = 0; i < nCount; i++ )
	{
		if( d[ i ] != dMissing ) dRet += d[ i ];
		else nCounted--;
	}
	if( nCounted <= 0 ) dRet = ::GetMissingDataValue();
	else dRet /= ( nCounted * 1. );

	return dRet;
}

//************************************
// Method:    GetMedian
// FullName:  GetMedian
// Access:    public
// Returns:   double
// Qualifier:
// Parameter: double * d
// Parameter: int nCount
// Parameter: int & nCounted
//************************************
double GetMedian( double* d, int nCount, int& nCounted )
{
	if( !d || ( nCount <= 0 ) ) return 0.;

	// sort array first into output array and get actual count
	double* sd = new double[ nCount ];
	ZeroMemory( sd, nCount * sizeof( double ) );
	DoSort( d, nCount, sd, nCounted );
	if( nCounted <= 0 ) { delete [] sd; return ::GetMissingDataValue(); }

	double dRet = 0.;
	int nMid = (int)( nCounted / 2 ) - 1;
	if( ( nMid < 0 ) || ( nMid >= nCounted ) ) { dRet = sd[ 0 ]; delete [] sd; return dRet; }
	// if count is odd, median is (nCount/2 + 1)th element
	if( ( nCounted % 2 ) != 0 ) dRet = sd[ nMid + 1 ];
	// else it is average of nCount/2 & nCount/2+1
	else dRet = ( sd[ nMid ] + sd[ nMid + 1 ] ) / 2.;

	delete [] sd;

	return dRet;
}

//************************************
// Method:    HasMissing
// FullName:  HasMissing
// Access:    public
// Returns:   bool
// Qualifier:
// Parameter: double * d
// Parameter: int nCount
// Parameter: int & nIdx
//************************************
bool HasMissing( double* d, int nCount, int& nIdx )
{
	if( !d || ( nCount <= 0 ) ) return true;
	for( int i = 0; i < nCount; i++ )
	{
		nIdx = i;
		if( d[ i ] == ::GetMissingDataValue() ) return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    FindFeatureData
// FullName:  FindFeatureData
// Access:    public
// Returns:   FeatureData*
// Qualifier:
// Parameter: CString szLabel
//************************************
FeatureData* FindFeatureData( CString szLabel )
{
	int nCount = sizeof( featureData ) / sizeof( FeatureData );
	FeatureData* pData = NULL;
	for( int i = 0; i < nCount; i++ )
	{
		pData = &(featureData[ i ]);
		if( pData && ( szLabel.CompareNoCase( pData->szLabel ) == 0 ) )
			return pData;
	}
	return NULL;
}

//************************************
// Method:    FindFeatureData
// FullName:  FindFeatureData
// Access:    public
// Returns:   FeatureData*
// Qualifier:
// Parameter: int nIndex
//************************************
FeatureData* FindFeatureData( int nIndex )
{
	int nCount = sizeof( featureData ) / sizeof( FeatureData );
	if( ( nIndex < 0 ) || ( nIndex >= nCount ) ) return NULL;
	return &( featureData[ nIndex ] );
}

//////////////////////////////////////////////////////////////////////////
// STROKE
//////////////////////////////////////////////////////////////////////////

NSProc::Stroke::Stroke()
{
	m_nIndex = -1;
	m_dVal = 0.;
	m_fShow = false;
	m_pParent = NULL;
	m_eType = StrokeType_Unknown;
}

//************************************
// Method:    operator=
// FullName:  NSProc::Stroke::operator=
// Access:    public
// Returns:   const NSProc::Stroke&
// Qualifier:
// Parameter: Stroke & ref
//************************************
const NSProc::Stroke& NSProc::Stroke::operator = ( Stroke &ref )
{
	m_nIndex = ref.m_nIndex;
	m_dVal = ref.m_dVal;
	m_fShow = ref.m_fShow;
	// m_pParent is not copied
	m_eType = ref.m_eType;

	return *this;
}

//************************************
// Method:    GetName
// FullName:  NSProc::Stroke::GetName
// Access:    public
// Returns:   CString&
// Qualifier:
//************************************
CString& NSProc::Stroke::GetName()
{
	// if name is empty, get parent feature name, and add extension based on type
	if( ( m_szName == _T("") ) && m_pParent )
	{
		CString szName;
		Feature* pFeature = m_pParent->GetParent();
		if( pFeature )
		{
			szName = pFeature->GetName();
			switch( m_eType )
			{
				case StrokeType_Primary:	szName += _T("1"); break;
				case StrokeType_Secondary:	szName += _T("2"); break;
			}
		}
		m_szName = szName;
	}
	return m_szName;
}

//////////////////////////////////////////////////////////////////////////
// MOVEMENTS
//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    Movements
// FullName:  NSProc::Movements::Movements
// Access:    public
// Returns:
// Qualifier:
//************************************
NSProc::Movements::Movements()
{
	m_pParent = NULL;
	m_strokeTotal.SetParent( this );
	m_strokeTotal.SetType( NSProc::Stroke::StrokeType_Total );
	m_strokePrimary.SetParent( this );
	m_strokePrimary.SetType( NSProc::Stroke::StrokeType_Primary );
	m_strokeSecondary.SetParent( this );
	m_strokeSecondary.SetType( NSProc::Stroke::StrokeType_Secondary );
}

//************************************
// Method:    operator=
// FullName:  NSProc::Movements::operator=
// Access:    public
// Returns:   const NSProc::Movements&
// Qualifier:
// Parameter: Movements & ref
//************************************
const NSProc::Movements& NSProc::Movements::operator = ( Movements &ref )
{
	// m_pParent is not copied
	m_strokeTotal = ref.m_strokeTotal;
	m_strokePrimary = ref.m_strokePrimary;
	m_strokeSecondary = ref.m_strokeSecondary;

	return *this;
}

NSProc::Stroke* NSProc::Movements::GetStrokeByType( Stroke::StrokeType eType )
{
	Stroke* pStroke = NULL;

	switch( eType )
	{
		case Stroke::StrokeType_Total:		pStroke = &m_strokeTotal;		break;
		case Stroke::StrokeType_Primary:	pStroke = &m_strokePrimary;		break;
		case Stroke::StrokeType_Secondary:	pStroke = &m_strokeSecondary;	break;
	}

	return pStroke;
}

//////////////////////////////////////////////////////////////////////////
// FEATURE
//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    Feature
// FullName:  NSProc::Feature::Feature
// Access:    public
// Returns:
// Qualifier: : CObject()
// Parameter: CString szName
// Parameter: int nIndex
//************************************
NSProc::Feature::Feature( CString szName, int nIndex ) : CObject()
{
	m_szName = szName;
	m_nIndex = nIndex;
	m_movements.SetParent( this );
}

//************************************
// Method:    Feature
// FullName:  NSProc::Feature::Feature
// Access:    public
// Returns:
// Qualifier:
// Parameter: Feature & ref
//************************************
NSProc::Feature::Feature( Feature& ref )
{
	m_szName = ref.m_szName;
	m_nIndex = ref.m_nIndex;
	m_movements = ref.m_movements;
}

//////////////////////////////////////////////////////////////////////////
// SEGMENT
//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ~Segment
// FullName:  NSProc::Segment::~Segment
// Access:    public
// Returns:
// Qualifier:
//************************************
NSProc::Segment::~Segment()
{
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pFeature = (Feature*)m_features.GetNext( pos );
		if( pFeature ) delete pFeature;
	}
	m_features.RemoveAll();
}

//************************************
// Method:    SetFeatures
// FullName:  NSProc::Segment::SetFeatures
// Access:    protected
// Returns:   void
// Qualifier:
// Parameter: FeatureSet * pFeatures
//************************************
void NSProc::Segment::SetFeatures( FeatureSet* pFeatures )
{
	// copy features to our local feature set
	POSITION pos = pFeatures ? pFeatures->GetHeadPosition() : NULL;
	while( pos )
	{
		Feature* pFeature = (Feature*)pFeatures->GetNext( pos );
		if( pFeature )
		{
			CString szName = pFeature->GetName();
			int nIdx = pFeature->GetIndex();
			m_features.AddTail(new Feature( *pFeature ));
		}
	}
}

//************************************
// Method:    FindFeature
// FullName:  NSProc::Segment::FindFeature
// Access:    public
// Returns:   Feature*
// Qualifier:
// Parameter: CString szName
//************************************
NSProc::Feature* NSProc::Segment::FindFeature( CString szName )
{
	Feature* pFeature = NULL;

	// locate feature by name
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pF = (Feature*)m_features.GetNext( pos );
		if( pF && ( pF->GetName() == szName ) )
		{
			pFeature = pF;
			break;
		}
	}

	return pFeature;
}

//************************************
// Method:    FindFeature
// FullName:  NSProc::Segment::FindFeature
// Access:    public
// Returns:   NSProc::Feature*
// Qualifier:
// Parameter: int nIndex
//************************************
NSProc::Feature* NSProc::Segment::FindFeature( int nIndex )
{
	Feature* pFeature = NULL;

	// locate feature by name
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pF = (Feature*)m_features.GetNext( pos );
		if( pF && ( pF->GetIndex() == nIndex ) )
		{
			pFeature = pF;
			break;
		}
	}

	return pFeature;
}

//************************************
// Method:    FindStroke
// FullName:  NSProc::Segment::FindStroke
// Access:    public
// Returns:   NSProc::Stroke*
// Qualifier:
// Parameter: int nIndex
//************************************
NSProc::Stroke* NSProc::Segment::FindStroke( int nIndex )
{
	Stroke* pStroke = NULL;

	// locate feature by input index

	// loop through each feature
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		// get feature
		Feature* pF = (Feature*)m_features.GetNext( pos );
		if( pF )
		{
			// loop through each stroke (noting sma & oneline)
			Stroke stroke = pF->GetMovements().GetStroke();
			if( stroke.IsShown() && ( stroke.GetIndex() == nIndex ) )
			{
				pStroke = &stroke;
				break;
			}
		}
	}

	return pStroke;
}

//************************************
// Method:    Include
// FullName:  NSProc::Segment::Include
// Access:    public
// Returns:   bool
// Qualifier:
//************************************
bool NSProc::Segment::Include()
{
	Feature* pFeature = NULL;
	bool fWithoutPenLift = m_pParent ? m_pParent->IsWithoutPenLifts() : false;
	bool fWithPenLift = m_pParent ? m_pParent->IsWithPenLifts() : false;
	double dThreshold = m_pParent ? m_pParent->GetLiftThreshold() : 0.0;
	double dCurThreshold = 0.0;
	bool fInclude = true;

	// if pen-lift inclusion/exclusion specified, we need to determine whether this segment/stroke
	// fits the criteria of inclusion
	if( fWithoutPenLift || fWithPenLift )
	{
		pFeature = FindFeature( _T("RelativePenDownDuration") );
		if( pFeature )
		{
			// Same function will cover both sub-movement analysis and not
			dCurThreshold = pFeature->GetMovements().GetStroke().GetValue();
			if( ( fWithoutPenLift && ( dCurThreshold < dThreshold ) ) ||
				( fWithPenLift && ( dCurThreshold > dThreshold ) ) )
				fInclude = false;
		}
	}

	return fInclude;
}

//************************************
// Method:    OutputData
// FullName:  NSProc::Segment::OutputData
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: CString & szOut
// Parameter: bool fCheckForInclusion
//************************************
void NSProc::Segment::OutputData( CString& szOut, bool fCheckForInclusion )
{
	CString szTemp, szName;
	double dVal = 0.;
	bool fSpecial = false;		// for special handling of relative duration and relative size
	Feature* pFeature = NULL;

	// if we're not including, exit function (do not write)
	if( fCheckForInclusion && !Include() ) return;

	// output trial & segment
	szTemp.Format( _T(" %d %d "), m_nTrial, m_nSegment );
	szOut += szTemp;

	// loop through each feature set item, and check for "shown" flag
	// if SMA and oneline, check sub-movements as well
	double dMissing = ::GetMissingDataValue();
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		// feature
		pFeature = (Feature*)m_features.GetNext( pos );
		if( !pFeature ) return;
		szName = pFeature->GetName();

		// special handling?
		if( ( szName == _T("RelativeDuration") ) || ( szName == _T("RelativeSize") ) )
			fSpecial = true;
		else fSpecial = false;

		// total
		if( pFeature->GetMovements().GetStroke().IsShown() )
		{
#if defined _DEBUG
			szName = pFeature->GetMovements().GetStroke().GetName();
#endif
			dVal = pFeature->GetMovements().GetStroke().GetValue();
			szTemp.Format( _T("%g "), dVal );
			szOut += szTemp;
		}

		// Primary & Secondary if SMA & oneline
		if( m_pParent && m_pParent->IsSMA() && m_pParent->IsSMAOneLine() )
		{
			// primary
			if( pFeature->GetMovements().GetPrimaryStroke().IsShown() )
			{
#if defined _DEBUG
				szName = pFeature->GetMovements().GetPrimaryStroke().GetName();
#endif
				dVal = pFeature->GetMovements().GetPrimaryStroke().GetValue();
				szTemp.Format( _T("%g "), dVal );
				szOut += szTemp;
			}
			// secondary
			if( pFeature->GetMovements().GetSecondaryStroke().IsShown() )
			{
#if defined _DEBUG
				szName = pFeature->GetMovements().GetSecondaryStroke().GetName();
#endif
				if( fSpecial )
				{
					dVal = pFeature->GetMovements().GetPrimaryStroke().GetValue();
					if( dVal != dMissing ) dVal = 1. - dVal;
				}
				else dVal = pFeature->GetMovements().GetSecondaryStroke().GetValue();
				szTemp.Format( _T("%g "), dVal );
				szOut += szTemp;
			}
		}
	}

	// trim for any excess whitespace
	szOut.TrimRight();
}

//////////////////////////////////////////////////////////////////////////
// SUMMARY
//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ~Summary
// FullName:  NSProc::Summary::~Summary
// Access:    public
// Returns:
// Qualifier:
//************************************
NSProc::Summary::~Summary()
{
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pFeature = (Feature*)m_features.GetNext( pos );
		if( pFeature ) delete pFeature;
	}
	m_features.RemoveAll();

	pos = m_segments.GetHeadPosition();
	while( pos )
	{
		Segment* pSum = (Segment*)m_segments.GetNext( pos );
		if( pSum ) delete pSum;
	}
	m_segments.RemoveAll();
}

//************************************
// Method:    ReadExcludes
// FullName:  NSProc::Summary::ReadExcludes
// Access:    public
// Returns:   bool
// Qualifier:
// Parameter: CString szFile
//************************************
bool NSProc::Summary::ReadExcludes( CString szFile )
{
	// if file cannot be opened (or does not exist), return false
	CStdioFile f;
	if( !f.Open( szFile, CFile::modeRead ) ) return false;

	// read the features to exclude and store
	CString szColLine;
	while( f.ReadString( szColLine ) )
	{
		if( szColLine != _T("") ) m_aExcludes.Add( szColLine );
	}

	return ( m_aExcludes.GetCount() > 0 );
}

//************************************
// Method:    FindFeature
// FullName:  NSProc::Summary::FindFeature
// Access:    public
// Returns:   Feature*
// Qualifier:
// Parameter: CString szName
//************************************
NSProc::Feature* NSProc::Summary::FindFeature( CString szName )
{
	Feature* pFeature = NULL;

	// locate feature by name
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pF = (Feature*)m_features.GetNext( pos );
		if( pF && ( pF->GetName() == szName ) )
		{
			pFeature = pF;
			break;
		}
	}

	return pFeature;
}

//************************************
// Method:    FindFeature
// FullName:  NSProc::Summary::FindFeature
// Access:    public
// Returns:   NSProc::Feature*
// Qualifier:
// Parameter: int nIndex
//************************************
NSProc::Feature* NSProc::Summary::FindFeature( int nIndex )
{
	Feature* pFeature = NULL;

	// locate feature by name
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pF = (Feature*)m_features.GetNext( pos );
		if( pF && ( pF->GetIndex() == nIndex ) )
		{
			pFeature = pF;
			break;
		}
	}

	return pFeature;
}

//************************************
// Method:    AddFeature
// FullName:  NSProc::Summary::AddFeature
// Access:    public
// Returns:   Feature*
// Qualifier:
// Parameter: CString szName
// Parameter: int nIndex
//************************************
NSProc::Feature* NSProc::Summary::AddFeature( CString szName, int nIndex )
{
	// check to see if feature exists, first...if so, do nothing
	if( FindFeature( szName ) != NULL ) return NULL;

	// create new feature and add to list
	Feature* pFeature = new Feature( szName, nIndex );
	if( !pFeature ) return NULL;
	m_features.AddTail( pFeature );

	return pFeature;
}

//************************************
// Method:    AddFeature
// FullName:  NSProc::Summary::AddFeature
// Access:    protected
// Returns:   bool
// Qualifier:
// Parameter: FeatureData * pData
// Parameter: int nFeature
// Parameter: int & nCount
// Parameter: bool fTotalOnly
//************************************
bool NSProc::Summary::AddFeature( FeatureData* pData, int nFeature, int& nCount, bool fTotalOnly )
{
	// verify pointer
	if( !pData ) return false;

	// if this feature data has an alternate name, use it (sma only)
	CString szTemp;
	szTemp = pData->szLabel;
	if( m_fSMA && ( strcmp( pData->szAlt, "" ) != 0 ) ) szTemp = pData->szAlt;

	// total stroke
	Feature* pFeature = NULL;
	CString szMsg;
	if( !m_fSMA || !m_fOneLine || pData->fStroke )
	{
		// add feature
		pFeature = AddFeature( szTemp, nFeature );
		if( !pFeature ) return false;
		// set as visible
		pFeature->GetMovements().GetStroke().SetIsShown( true );
		// increment feature column count
		nCount++;
		// verify column count
		if( nCount >= m_nDataMax ) WARN_OVER_LIMIT();
	}
	// primary stroke
	if( !fTotalOnly && pData->fStroke1 )
	{
		// add main feature name if not already established
		if( !pFeature ) pFeature = AddFeature( szTemp, nFeature );
		if( !pFeature ) return false;
		// set as visible
		pFeature->GetMovements().GetPrimaryStroke().SetIsShown( true );
		// increment feature column count
		nCount++;
		// verify column count
		if( nCount >= m_nDataMax ) WARN_OVER_LIMIT();
	}
	if( !fTotalOnly && pData->fStroke2 )
	{
		// add main feature name if not already established
		if( !pFeature ) pFeature = AddFeature( szTemp, nFeature );
		if( !pFeature ) return false;
		// set as visible
		pFeature->GetMovements().GetSecondaryStroke().SetIsShown( true );
		// increment feature column count
		nCount++;
		// verify column count
		if( nCount >= m_nDataMax ) WARN_OVER_LIMIT();
	}

	return true;
}

//************************************
// Method:    ReadFeatures
// FullName:  NSProc::Summary::ReadFeatures
// Access:    public
// Returns:   int64_t
// Qualifier:
// Parameter: CStdioFile * pFile
//************************************
int64_t NSProc::Summary::ReadFeatures( CStdioFile* pFile )
{
	// verify file
	if( !pFile || !pFile->m_pStream ) return 0;

	// Read column header line from file
	CString szHeaderLine;
	pFile->ReadString( szHeaderLine );

	// Extract Column Headers (Features)
	CString szTemp, szTemp2, szMsg;
	FeatureData* pData = NULL;
	int nItem = 0;
	bool fSkip = true;			// we are skipping the 1st two features (trial & segment)
	int nPos = szHeaderLine.Find( _T(" ") ), ndata = 0, nFeature = -1;
	while( nPos != -1 )
	{
		nItem++;
		if( nItem > 2 )
		{
			fSkip = false;
			// next feature
			nFeature++;
		}

		// extract feature name
		szTemp = szHeaderLine.Left( nPos );
		szTemp.Trim();

		// only if not skipping
		if( !fSkip )
		{
			// Locate the feature in the featuredata list
			pData = FindFeatureData( szTemp );

			// Add the feature & movements (if not sma, then we do total only)
			if( !AddFeature( pData, nFeature, ndata, ( !m_fSMA && !m_fOneLine ) ) )
			{
				// 1st confirm failure was not due to # of points
				if( ndata >= m_nDataMax ) break;
				Feature* pFeature = AddFeature( szTemp, nFeature );
				if( !pFeature ) break;
				// set as visible
				pFeature->GetMovements().GetStroke().SetIsShown( true );
				// increment feature column count
				ndata++;
				// verify column count
				if( ndata >= m_nDataMax ) WARN_OVER_LIMIT_BREAK();
			}
		}

		// Next header
		szHeaderLine = szHeaderLine.Mid( nPos + 1 );
		nPos = szHeaderLine.Find( _T(" ") );
		// If no more, handle this column header
		if( nPos == -1 )
		{
			nFeature++;

			// get last column
			szHeaderLine.Trim();
			if( szHeaderLine == _T("") ) break;
			else szTemp = szHeaderLine;

			// Locate the feature in the featuredata list
			pData = FindFeatureData( szTemp );

			// Add the feature & movements (if not sma, then we do total only)
			if( !AddFeature( pData, nFeature, ndata, ( !m_fSMA && !m_fOneLine ) ) )
			{
				// 1st confirm failure was not due to # of points
				if( ndata >= m_nDataMax ) break;
				Feature* pFeature = AddFeature( szTemp, nFeature );
				if( !pFeature ) break;
				// set as visible
				pFeature->GetMovements().GetStroke().SetIsShown( true );
				// increment feature column count
				ndata++;
				// verify column count
				if( ndata >= m_nDataMax ) WARN_OVER_LIMIT_BREAK();
			}
		}
	}

	return ndata;
}

//************************************
// Method:    FindExcluded
// FullName:  NSProc::Summary::FindExcluded
// Access:    protected
// Returns:   bool
// Qualifier:
// Parameter: CString szName
//************************************
bool NSProc::Summary::FindExcluded( CString szName )
{
	CString szN;
	int64_t nCount = GetExcludeCount();
	for( int64_t i = 0; i < nCount; i++ )
	{
		if( szName.CompareNoCase( m_aExcludes.GetAt( i ) ) == 0 ) return true;
	}

	return false;
}

//************************************
// Method:    UpdateForExclusions
// FullName:  NSProc::Summary::UpdateForExclusions
// Access:    public
// Returns:   int64_t
// Qualifier:
//************************************
int64_t NSProc::Summary::UpdateForExclusions()
{
	// if we have no features or no exclusions, return
	if( ( m_features.GetCount() == 0 ) || ( m_aExcludes.GetCount() == 0 ) )
		return GetVisibleCount();

	// loop through features (including submovements) to see if on exclude list
	// if so, mark as such
	CString szName, szNameS;
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		Feature* pFeature = (Feature*)m_features.GetNext( pos );
		if( !pFeature ) return 0;

		// total
		szName = pFeature->GetName();
		if( FindExcluded( szName ) )
			pFeature->GetMovements().GetStroke().SetIsShown( false );

		// primary & secondary strokes ONLY if SMA && oneline
		if( m_fSMA && m_fOneLine )
		{
			// primary
			szNameS = szName;
			szNameS += _T("1");
			if( FindExcluded( szNameS ) )
				pFeature->GetMovements().GetPrimaryStroke().SetIsShown( false );

			// secondary
			szNameS = szName;
			szNameS += _T("2");
			if( FindExcluded( szNameS ) )
				pFeature->GetMovements().GetSecondaryStroke().SetIsShown( false );
		}
	}

	// return count of visible items
	return GetVisibleCount();
}

//************************************
// Method:    GetVisibleCount
// FullName:  NSProc::Summary::GetVisibleCount
// Access:    public
// Returns:   int64_t
// Qualifier:
//************************************
int64_t NSProc::Summary::GetVisibleCount()
{
	// loop through each feature set item, and check for "shown" flag
	// if SMA and oneline, check sub-movements as well
	int nCount = 0;
	Feature* pFeature = NULL;
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		// feature
		pFeature = (Feature*)m_features.GetNext( pos );
		if( !pFeature ) return 0;

		// total
		if( pFeature->GetMovements().GetStroke().IsShown() ) nCount++;

		// Primary & Secondary if SMA & oneline
		if( m_fSMA && m_fOneLine )
		{
			// primary
			if( pFeature->GetMovements().GetPrimaryStroke().IsShown() ) nCount++;
			// secondary
			if( pFeature->GetMovements().GetSecondaryStroke().IsShown() ) nCount++;
		}
	}

	return nCount;
}

//************************************
// Method:    AddSegment
// FullName:  NSProc::Summary::AddSegment
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: Segment * pSeg
//************************************
void NSProc::Summary::AddSegment( Segment* pSeg )
{
	if( pSeg )
	{
		pSeg->SetParent( this );
		pSeg->SetFeatures( &m_features );
		m_segments.AddTail( pSeg );
	}
}

//************************************
// Method:    CollapseUDStrokes
// FullName:  NSProc::Summary::CollapseUDStrokes
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: bool fMedian
//************************************
void NSProc::Summary::CollapseUDStrokes( bool fMedian )
{
	// if no data, do nothing
	if( m_segments.GetCount() <= 0 ) return;

	// Create a new segment list
	SegmentSet segments;
	// loop through each segment to get the #s of all trials and add to
	Segment* pSegment = NULL;
	int nTrials[ MAX_TRIALS ];
	int nCurTrial = 0, nLastTrial = 0, nTrialCount = 0;
	memset( nTrials, 0, sizeof( int ) * MAX_TRIALS );
	POSITION pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment )
		{
			nCurTrial = pSegment->GetTrial();
			if( nCurTrial != nLastTrial )
			{
				nTrials[ nTrialCount ] = nCurTrial;
				nLastTrial = nCurTrial;
				nTrialCount++;
				if( nTrialCount >= MAX_TRIALS ) break;
			}
		}
	}
	// loop through each trial, then loop through each segment of that trial,
	// and loop through each feature to accumulate the values
	Feature* pFeature = NULL;
	int nFeatureCount = m_features.GetCount();
	int nPos = 0, nCollapsed = -1;
	double dPS[ NSEGMAX ], dSS[ NSEGMAX ], dTS[ NSEGMAX ];
	double dValPS = 0., dValSS = 0., dValTS = 0.;
	for( int t = 0; t < nTrialCount; t++ )
	{
		// current trial
		nCurTrial = nTrials[ t ];

		// loop through twice, once for upstroke (odd), other for downstroke (even)
		for( int s = 0; s <= 1; s++ )
		{
			// new segment per trial
			Segment* pSeg = new Segment;
			pSeg->SetFeatures( &m_features );
			pSeg->SetParent( this );
			pSeg->SetTrial( nCurTrial );
			pSeg->SetSegment( s + 1 );

			// loop through each feature...
			for( int f = 0; f < nFeatureCount; f++ )
			{
				// initialize
				memset( dPS, 0, sizeof( double ) * NSEGMAX );
				memset( dSS, 0, sizeof( double ) * NSEGMAX );
				memset( dTS, 0, sizeof( double ) * NSEGMAX );
				nPos = 0;

				// loop through each segment, and choose up or down stroke accordingly
				pos = m_segments.GetHeadPosition();
				while( pos )
				{
					pSegment = (Segment*)m_segments.GetNext( pos );
					if( ( s == 0 ) &&		// upstrokes
						( ( pSegment->GetSegment() % 2 ) == 0 ) )
						continue;
					else if( ( s == 1 )	&&	// downstrokes
						( ( pSegment->GetSegment() % 2 ) != 0 ) )
						continue;

					// if this segment is of current trial, get current feature and add to list
					if( pSegment &&
						( pSegment->GetTrial() == nCurTrial ) &&
						( pFeature = pSegment->FindFeature( f ) ) )
					{
						dPS[ nPos ] = pFeature->GetMovements().GetPrimaryStroke().GetValue();
						dSS[ nPos ] = pFeature->GetMovements().GetSecondaryStroke().GetValue();
						dTS[ nPos ] = pFeature->GetMovements().GetStroke().GetValue();
						nPos++;
					}
				}

				// process arrays
				if( fMedian )
				{
					dValPS = GetMedian( dPS, nPos, nCollapsed );
					dValSS = GetMedian( dSS, nPos, nCollapsed );
					dValTS = GetMedian( dTS, nPos, nCollapsed );
				}
				else
				{
					dValPS = GetAverage( dPS, nPos, nCollapsed );
					dValSS = GetAverage( dSS, nPos, nCollapsed );
					dValTS = GetAverage( dTS, nPos, nCollapsed );
				}

				// set new feature value to new segment
				pFeature = pSeg->FindFeature( f );
				if( pFeature )
				{
					pFeature->GetMovements().GetPrimaryStroke().SetValue( dValPS );
					pFeature->GetMovements().GetSecondaryStroke().SetValue( dValSS );
					pFeature->GetMovements().GetStroke().SetValue( dValTS );
				}
			}

			// Add num-items-collapsed feature to segments
			pFeature = new Feature( _T("NumCollapsed"), nFeatureCount );
			pFeature->GetMovements().GetStroke().SetIsShown( true );
			pFeature->GetMovements().GetStroke().SetValue( nPos );
			pSeg->m_features.AddTail( pFeature );

			// add new segment to new segment list
			segments.AddTail( pSeg );
		}
	}

	// delete original segment list
	pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment ) delete pSegment;
	}
	m_segments.RemoveAll();

	// create new segment list
	pos = segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)segments.GetNext( pos );
		if( pSegment ) m_segments.AddTail( pSegment );
	}
}

//************************************
// Method:    CollapseStrokes
// FullName:  NSProc::Summary::CollapseStrokes
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: bool fMedian
//************************************
void NSProc::Summary::CollapseStrokes( bool fMedian )
{
	// if no data, do nothing
	if( m_segments.GetCount() <= 0 ) return;

	// Create a new segment list
	SegmentSet segments;
	// loop through each segment to get the #s of all trials and add to
	Segment* pSegment = NULL;
	int nTrials[ MAX_TRIALS ];
	int nCurTrial = 0, nLastTrial = 0, nTrialCount = 0;
	memset( nTrials, 0, sizeof( int ) * MAX_TRIALS );
	POSITION pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment )
		{
			if( pSegment->Include() )
			{
				nCurTrial = pSegment->GetTrial();
				if( nCurTrial != nLastTrial )
				{
					nTrials[ nTrialCount ] = nCurTrial;
					nLastTrial = nCurTrial;
					nTrialCount++;
					if( nTrialCount >= MAX_TRIALS ) break;
				}
			}
			else
			{
				CString szMsg;
				szMsg.Format( _T("SUMTRIAL: INFO: Excluding segment # %d of trial # %d."), pSegment->m_nSegment, pSegment->m_nTrial );
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}
	// loop through each trial, then loop through each segment of that trial,
	// and loop through each feature to accumulate the values
	Feature* pFeature = NULL;
	int nFeatureCount = m_features.GetCount();
	int nPos = 0, nCollapsed = -1;
	double dPS[ NSEGMAX ], dSS[ NSEGMAX ], dTS[ NSEGMAX ];
	double dValPS = 0., dValSS = 0., dValTS = 0.;
	for( int t = 0; t < nTrialCount; t++ )
	{
		// current trial
		nCurTrial = nTrials[ t ];

		// new segment per trial
		Segment* pSeg = new Segment;
		pSeg->SetFeatures( &m_features );
		pSeg->SetParent( this );
		pSeg->SetTrial( nCurTrial );
		pSeg->SetSegment( 1 );

		// loop through each feature...
		for( int f = 0; f < nFeatureCount; f++ )
		{
			// initialize
			memset( dPS, 0, sizeof( double ) * NSEGMAX );
			memset( dSS, 0, sizeof( double ) * NSEGMAX );
			memset( dTS, 0, sizeof( double ) * NSEGMAX );
			nPos = 0;

			// loop through each segment, and choose up or down stroke accordingly
			pos = m_segments.GetHeadPosition();
			while( pos )
			{
				pSegment = (Segment*)m_segments.GetNext( pos );
				// if this segment is of current trial, get current feature and add to list
				if( pSegment && pSegment->Include() &&
					( pSegment->GetTrial() == nCurTrial ) &&
					( pFeature = pSegment->FindFeature( f ) ) )
				{
					dPS[ nPos ] = pFeature->GetMovements().GetPrimaryStroke().GetValue();
					dSS[ nPos ] = pFeature->GetMovements().GetSecondaryStroke().GetValue();
					dTS[ nPos ] = pFeature->GetMovements().GetStroke().GetValue();
					nPos++;
				}
			}

			// process arrays
			if( fMedian )
			{
				dValPS = GetMedian( dPS, nPos, nCollapsed );
				dValSS = GetMedian( dSS, nPos, nCollapsed );
				dValTS = GetMedian( dTS, nPos, nCollapsed );
			}
			else
			{
				dValPS = GetAverage( dPS, nPos, nCollapsed );
				dValSS = GetAverage( dSS, nPos, nCollapsed );
				dValTS = GetAverage( dTS, nPos, nCollapsed );
			}

			// set new feature value to new segment
			pFeature = pSeg->FindFeature( f );
			if( pFeature )
			{
				pFeature->GetMovements().GetPrimaryStroke().SetValue( dValPS );
				pFeature->GetMovements().GetSecondaryStroke().SetValue( dValSS );
				pFeature->GetMovements().GetStroke().SetValue( dValTS );
			}
		}

		// Add num-items-collapsed feature to segments
		pFeature = new Feature( _T("NumCollapsed"), nFeatureCount );
		pFeature->GetMovements().GetStroke().SetIsShown( true );
		pFeature->GetMovements().GetStroke().SetValue( nPos );
		pSeg->m_features.AddTail( pFeature );

		// add new segment to new segment list
		segments.AddTail( pSeg );
	}

	// delete original segment list
	pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment ) delete pSegment;
	}
	m_segments.RemoveAll();

	// create new segment list
	pos = segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)segments.GetNext( pos );
		if( pSegment ) m_segments.AddTail( pSegment );
	}
}

//************************************
// Method:    GetIndexFromArray
// FullName:  GetIndexFromArray
// Access:    public
// Returns:   int
// Qualifier:
// Parameter: int * pList
// Parameter: int nCount
// Parameter: int nIdx
//************************************
int GetIndexFromArray( int* pList, int nCount, int nIdx )
{
	if( !pList || ( nIdx < 0 ) ) return -1;
	for( int i = 0; i < nCount; i++ )
	{
		if( pList[ i ] == nIdx ) return i;
	}
	return -1;
}

//************************************
// Method:    CollapseTrials
// FullName:  NSProc::Summary::CollapseTrials
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: bool fMedian
//************************************
void NSProc::Summary::CollapseTrials( bool fMedian )
{
	// if no data, do nothing
	if( m_segments.GetCount() <= 0 ) return;

	// Create a new segment list
	SegmentSet segments;
	// loop through each segment to get the #s of all trials and add to
	Segment* pSegment = NULL;
	int nTrials[ MAX_TRIALS ];
	int nCurTrial = 0, nLastTrial = 0, nTrialCount = 0, nMaxSeg = 0;
	memset( nTrials, 0, sizeof( int ) * MAX_TRIALS );
	POSITION pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment )
		{
			if( pSegment->Include() )
			{
				nCurTrial = pSegment->GetTrial();
				if( nCurTrial != nLastTrial )
				{
					nTrials[ nTrialCount ] = nCurTrial;
					nLastTrial = nCurTrial;
					nTrialCount++;
					if( nTrialCount >= MAX_TRIALS ) break;
				}
				if( pSegment->GetSegment() > nMaxSeg ) nMaxSeg = pSegment->GetSegment();
			}
			else
			{
				CString szMsg;
				szMsg.Format( _T("SUMTRIAL: INFO: Excluding segment # %d of trial # %d."), pSegment->m_nSegment, pSegment->m_nTrial );
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}
	// loop through each trial, then loop through each segment of that trial,
	// and loop through each feature to accumulate the values
	// initialize first
	Feature* pFeature = NULL;
	int nFeatureCount = m_features.GetCount();
	double*** dPS = new double**[ nFeatureCount ];
	double*** dSS = new double**[ nFeatureCount ];
	double*** dTS = new double**[ nFeatureCount ];
	for( int i = 0; i < nFeatureCount; i++ )
	{
		dPS[ i ] = new double*[ nMaxSeg ];
		dSS[ i ] = new double*[ nMaxSeg ];
		dTS[ i ] = new double*[ nMaxSeg ];
		for( int j = 0; j < nMaxSeg; j++ )
		{
			dPS[ i ][ j ] = new double[ nTrialCount ];
			dSS[ i ][ j ] = new double[ nTrialCount ];
			dTS[ i ][ j ] = new double[ nTrialCount ];
			for( int t = 0; t < nTrialCount; t++ )
			{
				dPS[ i ][ j ][ t ] = ::GetMissingDataValue();
				dSS[ i ][ j ][ t ] = ::GetMissingDataValue();
				dTS[ i ][ j ][ t ] = ::GetMissingDataValue();
			}
		}
	}
	int nTrial = 0, nSeg = 0, nIdx = 0;
	double dValPS = 0., dValSS = 0., dValTS = 0.;
	// loop through each feature...
	for( int f = 0; f < nFeatureCount; f++ )
	{
		// loop through each segment, and choose stroke accordingly
		pos = m_segments.GetHeadPosition();
		while( pos )
		{
			pSegment = (Segment*)m_segments.GetNext( pos );
			if( pSegment && pSegment->Include() && ( pFeature = pSegment->FindFeature( f ) ) )
			{
				nTrial = GetIndexFromArray( nTrials, nTrialCount, pSegment->GetTrial() ) + 1;
				if( ( nTrial > 0 ) && ( nTrial <= nTrialCount ) )
				{
					nSeg = pSegment->GetSegment() - 1;
					dPS[ f ][ nSeg ][ nTrial - 1 ] = pFeature->GetMovements().GetPrimaryStroke().GetValue();
					dSS[ f ][ nSeg ][ nTrial - 1 ] = pFeature->GetMovements().GetSecondaryStroke().GetValue();
					dTS[ f ][ nSeg ][ nTrial - 1 ] = pFeature->GetMovements().GetStroke().GetValue();
				}
			}
		}
	}

	// loop through each segment (up to nMaxSeg)
	// for each segment, we create a new one to combine all trials
	int nCollapsed = -1, nSegIndex = 0, nCounted = -1;
	for( int s = 0; s < nMaxSeg; s++ )
	{
		nCollapsed = -1;

		// if sub-movement analysis, we are only accessing the 3rd sub-movement (index)
		nSegIndex = s;
		if( IsSMA() ) nSegIndex = s * 3 + 2;
		if( nSegIndex > nMaxSeg ) break;

		// new segment
		Segment* pSeg = new Segment;
		pSeg->SetFeatures( &m_features );
		pSeg->SetParent( this );
		pSeg->SetTrial( 1 );
		pSeg->SetSegment( s + 1 );

		// loop through each feature...
		for( int f = 0; f < nFeatureCount; f++ )
		{
			// process arrays
			if( fMedian )
			{
				dValPS = GetMedian( dPS[ f ][ nSegIndex ], nTrialCount, nCounted );
				dValSS = GetMedian( dSS[ f ][ nSegIndex ], nTrialCount, nCounted );
				dValTS = GetMedian( dTS[ f ][ nSegIndex ], nTrialCount, nCounted );
			}
			else
			{
				dValPS = GetAverage( dPS[ f ][ nSegIndex ], nTrialCount, nCounted );
				dValSS = GetAverage( dSS[ f ][ nSegIndex ], nTrialCount, nCounted );
				dValTS = GetAverage( dTS[ f ][ nSegIndex ], nTrialCount, nCounted );
			}

			// verify the # of items collapsed. If last count != current count,
			// that means the # of items collapsed per column/feature is not the same
			// which is NOT GOOD. Warn user with option to end
			if( ( nCollapsed != -1 ) && ( nCounted != 0 ) && ( nCounted != nCollapsed ) )
			{
				CString szMsg = _T("SUMMARIZE: ERROR: The number of collapsed items per feature are not consistent. Results are not predictable." );
				OutputMessage( szMsg, LOG_PROC );
			}
			else if( nCounted != 0 ) nCollapsed = nCounted;

			// set new feature value to new segment
			pFeature = pSeg->FindFeature( f );
			if( pFeature )
			{
				pFeature->GetMovements().GetPrimaryStroke().SetValue( dValPS );
				pFeature->GetMovements().GetSecondaryStroke().SetValue( dValSS );
				pFeature->GetMovements().GetStroke().SetValue( dValTS );
			}
		}

		// Add num-items-collapsed feature to segments
		pFeature = new Feature( _T("NumCollapsed"), nFeatureCount );
		pFeature->GetMovements().GetStroke().SetIsShown( true );
		pFeature->GetMovements().GetStroke().SetValue( nCollapsed );
		pSeg->m_features.AddTail( pFeature );

		// add new segment to new segment list
		segments.AddTail( pSeg );
	}

	// cleanup
	for( int i = 0; i < nFeatureCount; i++ )
	{
		for( int j = 0; j < nMaxSeg; j++ )
		{
			delete [] dPS[ i ][ j ];
			delete [] dSS[ i ][ j ];
			delete [] dTS[ i ][ j ];
		}
		delete [] dPS[ i ];
		delete [] dSS[ i ];
		delete [] dTS[ i ];
	}
	delete [] dPS;
	delete [] dSS;
	delete [] dTS;

	// delete original segment list
	pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment ) delete pSegment;
	}
	m_segments.RemoveAll();

	// create new segment list
	pos = segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)segments.GetNext( pos );
		if( pSegment ) m_segments.AddTail( pSegment );
	}
}

//************************************
// Method:    CollapseAllUD
// FullName:  NSProc::Summary::CollapseAllUD
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: bool fMedian
//************************************
void NSProc::Summary::CollapseAllUD( bool fMedian )
{
	// if no data, do nothing
	if( m_segments.GetCount() <= 0 ) return;

	// Create a new segment list
	SegmentSet segments;

	// new segments (up & down strokes)
	Segment* pSeg = new Segment;
	pSeg->SetFeatures( &m_features );
	pSeg->SetParent( this );
	pSeg->SetTrial( 1 );
	pSeg->SetSegment( 1 );
	Segment* pSeg2 = new Segment;
	pSeg2->SetFeatures( &m_features );
	pSeg2->SetParent( this );
	pSeg2->SetTrial( 1 );
	pSeg2->SetSegment( 2 );

	// loop through each segment (data size = # of segments)
	int nMaxSeg = m_segments.GetCount();
	double* dPS = new double[ nMaxSeg ];
	double* dSS = new double[ nMaxSeg ];
	double* dTS = new double[ nMaxSeg ];
	double* dPS2 = new double[ nMaxSeg ];
	double* dSS2 = new double[ nMaxSeg ];
	double* dTS2 = new double[ nMaxSeg ];
	double dValPS = 0., dValSS = 0., dValTS = 0.;
	double dValPS2 = 0., dValSS2 = 0., dValTS2 = 0.;
	Segment* pSegment = NULL;
	Feature* pFeature = NULL;
	int nFeatureCount = m_features.GetCount();
	int nPos = 0, nPos2 = 0, nCollapsed = -1, nCounted = -1, nCollapsed2 = -1, nCounted2 = -1;
	POSITION pos = NULL;
	// loop through each feature...
	for( int i = 0; i < nFeatureCount; i++ )
	{
		// initialize
		memset( dPS, 0, sizeof( double ) * nMaxSeg );
		memset( dSS, 0, sizeof( double ) * nMaxSeg );
		memset( dTS, 0, sizeof( double ) * nMaxSeg );
		memset( dPS2, 0, sizeof( double ) * nMaxSeg );
		memset( dSS2, 0, sizeof( double ) * nMaxSeg );
		memset( dTS2, 0, sizeof( double ) * nMaxSeg );
		nPos = 0;
		nPos2 = 0;

		// loop through each segment...
		pos = m_segments.GetHeadPosition();
		while( pos )
		{
			pSegment = (Segment*)m_segments.GetNext( pos );

			// if this segment is of current trial, get current feature and add to list
			if( pSegment && pSegment->Include() && ( pFeature = pSegment->FindFeature( i ) ) )
			{
				if( ( pSegment->GetSegment() % 2 ) != 0 )		// upstrokes
				{
					dPS[ nPos ] = pFeature->GetMovements().GetPrimaryStroke().GetValue();
					dSS[ nPos ] = pFeature->GetMovements().GetSecondaryStroke().GetValue();
					dTS[ nPos ] = pFeature->GetMovements().GetStroke().GetValue();
					nPos++;
				}
				else if( ( pSegment->GetSegment() % 2 ) == 0 )	// downstrokes
				{
					dPS2[ nPos2 ] = pFeature->GetMovements().GetPrimaryStroke().GetValue();
					dSS2[ nPos2 ] = pFeature->GetMovements().GetSecondaryStroke().GetValue();
					dTS2[ nPos2 ] = pFeature->GetMovements().GetStroke().GetValue();
					nPos2++;
				}
			}
		}

		// process arrays
		if( fMedian )
		{
			dValPS = GetMedian( dPS, nPos, nCounted );
			dValSS = GetMedian( dSS, nPos, nCounted );
			dValTS = GetMedian( dTS, nPos, nCounted );
			dValPS2 = GetMedian( dPS2, nPos2, nCounted2 );
			dValSS2 = GetMedian( dSS2, nPos2, nCounted2 );
			dValTS2 = GetMedian( dTS2, nPos2, nCounted2 );
		}
		else
		{
			dValPS = GetAverage( dPS, nPos, nCounted );
			dValSS = GetAverage( dSS, nPos, nCounted );
			dValTS = GetAverage( dTS, nPos, nCounted );
			dValPS2 = GetAverage( dPS2, nPos2, nCounted2 );
			dValSS2 = GetAverage( dSS2, nPos2, nCounted2 );
			dValTS2 = GetAverage( dTS2, nPos2, nCounted2 );
		}

		// verify the # of items collapsed. If last count != current count,
		// that means the # of items collapsed per column/feature is not the same
		// which is NOT GOOD. Warn user with option to end
		if( ( nCollapsed != -1 ) && ( nCollapsed2 != -1 ) && ( nCounted != 0 ) && ( nCounted2 != 0 ) &&
			( ( nCounted != nCollapsed ) || ( nCounted2 != nCollapsed2 ) ) )
		{
			CString szMsg = _T("SUMMARIZE: ERROR: The number of collapsed items per feature are not consistent. Results are not predictable." );
			OutputMessage( szMsg, LOG_PROC );
		}
		else if( ( nCounted != 0 ) && ( nCounted2 != 0 ) )
		{
			nCollapsed = nCounted;
			nCollapsed2 = nCounted2;
		}

		// set new feature value to new segment (upstrokes)
		pFeature = pSeg->FindFeature( i );
		if( pFeature )
		{
			pFeature->GetMovements().GetPrimaryStroke().SetValue( dValPS );
			pFeature->GetMovements().GetSecondaryStroke().SetValue( dValSS );
			pFeature->GetMovements().GetStroke().SetValue( dValTS );
		}

		// set new feature value to new segment (downstrokes)
		pFeature = pSeg2->FindFeature( i );
		if( pFeature )
		{
			pFeature->GetMovements().GetPrimaryStroke().SetValue( dValPS2 );
			pFeature->GetMovements().GetSecondaryStroke().SetValue( dValSS2 );
			pFeature->GetMovements().GetStroke().SetValue( dValTS2 );
		}
	}

	// Add num-items-collapsed feature to segments
	pFeature = new Feature( _T("NumCollapsed"), nFeatureCount );
	pFeature->GetMovements().GetStroke().SetIsShown( true );
	pFeature->GetMovements().GetStroke().SetValue( nCollapsed );
	pSeg->m_features.AddTail( pFeature );
	pFeature = new Feature( _T("NumCollapsed"), nFeatureCount );
	pFeature->GetMovements().GetStroke().SetIsShown( true );
	pFeature->GetMovements().GetStroke().SetValue( nCollapsed2 );
	pSeg2->m_features.AddTail( pFeature );

	// add new segments to new segment list
	segments.AddTail( pSeg );
	segments.AddTail( pSeg2 );

	// cleanup
	delete [] dPS;
	delete [] dSS;
	delete [] dTS;
	delete [] dPS2;
	delete [] dSS2;
	delete [] dTS2;

	// delete original segment list
	pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment ) delete pSegment;
	}
	m_segments.RemoveAll();

	// create new segment list
	pos = segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)segments.GetNext( pos );
		if( pSegment ) m_segments.AddTail( pSegment );
	}
}

//************************************
// Method:    CollapseAll
// FullName:  NSProc::Summary::CollapseAll
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: bool fMedian
//************************************
void NSProc::Summary::CollapseAll( bool fMedian )
{
	// if no data, do nothing
	if( m_segments.GetCount() <= 0 ) return;

	Feature* pFeature = NULL;
	// Create a new segment list
	SegmentSet segments;
	// loop through each segment to get the #s of all trials and add to list
	// also get max segment/stroke #
	Segment* pSegment = NULL;
	int nTrials[ MAX_TRIALS ];
	int nCurTrial = 0, nLastTrial = 0, nTrialCount = 0, nMaxSeg = 0;
	memset( nTrials, 0, sizeof( int ) * MAX_TRIALS );
	POSITION pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment )
		{
			if( pSegment->Include() )
			{
				nCurTrial = pSegment->GetTrial();
				if( nCurTrial != nLastTrial )
				{
					nTrials[ nTrialCount ] = nCurTrial;
					nLastTrial = nCurTrial;
					nTrialCount++;
					if( nTrialCount >= MAX_TRIALS ) break;
				}
				if( pSegment->GetSegment() > nMaxSeg ) nMaxSeg = pSegment->GetSegment();
			}
			else
			{
				CString szMsg;
				szMsg.Format( _T("SUMTRIAL: INFO: Excluding segment # %d of trial # %d."), pSegment->m_nSegment, pSegment->m_nTrial );
				OutputMessage( szMsg, LOG_PROC );
			}
		}
	}
	// new segment per condition
	Segment* pSeg = new Segment;
	pSeg->SetFeatures( &m_features );
	pSeg->SetParent( this );
	pSeg->SetTrial( 1 );
	pSeg->SetSegment( 1 );
	// loop through each trial, then loop through each segment of that trial,
	// and loop through each feature to accumulate the values
	int nFeatureCount = m_features.GetCount();
	int nPos = 0, nCollapsed = -1, nCounted = -1;
	double* dPS = new double[ nTrialCount * nMaxSeg ];
	double* dSS = new double[ nTrialCount * nMaxSeg ];
	double* dTS = new double[ nTrialCount * nMaxSeg ];
	double dValPS = 0., dValSS = 0., dValTS = 0.;
	// loop through each feature...
	for( int i = 0; i < nFeatureCount; i++ )
	{
		// initialize
		memset( dPS, 0, sizeof( double ) * nTrialCount * nMaxSeg );
		memset( dSS, 0, sizeof( double ) * nTrialCount * nMaxSeg );
		memset( dTS, 0, sizeof( double ) * nTrialCount * nMaxSeg );
		nPos = 0;

		// loop through each segment...
		pos = m_segments.GetHeadPosition();
		while( pos )
		{
			pSegment = (Segment*)m_segments.GetNext( pos );

			// if this segment is of current trial, get current feature and add to list
			if( pSegment && pSegment->Include() && ( pFeature = pSegment->FindFeature( i ) ) &&
				( nPos <= ( nTrialCount * nMaxSeg ) ) )
			{
				dPS[ nPos ] = pFeature->GetMovements().GetPrimaryStroke().GetValue();
				dSS[ nPos ] = pFeature->GetMovements().GetSecondaryStroke().GetValue();
				dTS[ nPos ] = pFeature->GetMovements().GetStroke().GetValue();
				nPos++;
			}
		}

		// process arrays
		if( fMedian )
		{
			dValPS = GetMedian( dPS, nPos, nCounted );
			dValSS = GetMedian( dSS, nPos, nCounted );
			dValTS = GetMedian( dTS, nPos, nCounted );
		}
		else
		{
			dValPS = GetAverage( dPS, nPos, nCounted );
			dValSS = GetAverage( dSS, nPos, nCounted );
			dValTS = GetAverage( dTS, nPos, nCounted );
		}

		// verify the # of items collapsed. If last count != current count,
		// that means the # of items collapsed per column/feature is not the same
		// which is NOT GOOD. Warn user with option to end
		if( ( nCollapsed != -1 ) && ( nCounted != 0 ) && ( nCounted != nCollapsed ) )
		{
			CString szMsg = _T("SUMMARIZE: ERROR: The number of collapsed items per feature are not consistent. Results are not predictable." );
			OutputMessage( szMsg, LOG_PROC );
		}
		else if( nCounted != 0 ) nCollapsed = nCounted;

		// set new feature value to new segment
		pFeature = pSeg->FindFeature( i );
		if( pFeature )
		{
			pFeature->GetMovements().GetPrimaryStroke().SetValue( dValPS );
			pFeature->GetMovements().GetSecondaryStroke().SetValue( dValSS );
			pFeature->GetMovements().GetStroke().SetValue( dValTS );
		}
	}

	// Add num-items-collapsed feature to segment
	pFeature = new Feature( _T("NumCollapsed"), nFeatureCount );
	pFeature->GetMovements().GetStroke().SetValue( nCollapsed );
	pFeature->GetMovements().GetStroke().SetIsShown( true );
	pSeg->m_features.AddTail( pFeature );

	// add new segment to new segment list
	segments.AddTail( pSeg );

	// cleanup
	delete [] dPS;
	delete [] dSS;
	delete [] dTS;

	// delete original segment list
	pos = m_segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)m_segments.GetNext( pos );
		if( pSegment ) delete pSegment;
	}
	m_segments.RemoveAll();

	// create new segment list
	pos = segments.GetHeadPosition();
	while( pos )
	{
		pSegment = (Segment*)segments.GetNext( pos );
		if( pSegment ) m_segments.AddTail( pSegment );
	}
}

//************************************
// Method:    OutputHeaders
// FullName:  NSProc::Summary::OutputHeaders
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: CString & szOut
// Parameter: bool fCollapsing
//************************************
void NSProc::Summary::OutputHeaders( CString& szOut, bool fCollapsing )
{
	// loop through each feature set item, and check for "shown" flag
	// if SMA and oneline, check sub-movements as well
	// also, assign index based upon output headers
	int nIndex = 0;
	CString szName;
	Feature* pFeature = NULL;
	POSITION pos = m_features.GetHeadPosition();
	while( pos )
	{
		// feature
		pFeature = (Feature*)m_features.GetNext( pos );
		if( !pFeature ) return;

		// total
		if( pFeature->GetMovements().GetStroke().IsShown() )
		{
			szName = pFeature->GetMovements().GetStroke().GetName();
			if( szName != _T("") )
			{
				szOut += _T(" ");
				szOut += szName;
				pFeature->GetMovements().GetStroke().SetIndex( nIndex );
				nIndex++;
			}
		}

		// Primary & Secondary if SMA & oneline
		if( m_fSMA && m_fOneLine )
		{
			// primary
			if( pFeature->GetMovements().GetPrimaryStroke().IsShown() )
			{
				szName = pFeature->GetMovements().GetPrimaryStroke().GetName();
				if( szName != _T("") )
				{
					szOut += _T(" ");
					szOut += szName;
					pFeature->GetMovements().GetPrimaryStroke().SetIndex( nIndex );
					nIndex++;
				}
			}
			// secondary
			if( pFeature->GetMovements().GetSecondaryStroke().IsShown() )
			{
				szName = pFeature->GetMovements().GetSecondaryStroke().GetName();
				if( szName != _T("") )
				{
					szOut += _T(" ");
					szOut += szName;
					pFeature->GetMovements().GetSecondaryStroke().SetIndex( nIndex );
					nIndex++;
				}
			}
		}
	}

	// if we're collapsing, add column for num-collapsed
	if( fCollapsing )
	{
		szOut += _T(" ");
		szOut += _T("NumCollapsed");
	}

	// trim for any excess whitespace
	szOut.Trim();
}

//************************************
// Method:    OutputData
// FullName:  NSProc::Summary::OutputData
// Access:    public
// Returns:   bool
// Qualifier:
// Parameter: CString & szOut
// Parameter: Segment * pSegment
// Parameter: bool fCheckForInclusion
//************************************
bool NSProc::Summary::OutputData( CString& szOut, Segment* pSegment, bool fCheckForInclusion )
{
	if( !pSegment ) return false;

	if( !fCheckForInclusion || pSegment->Include() )
	{
		// output the group, subject & condition
		szOut.Format( _T("%s %s %s"), m_szGroup, m_szSubj, m_szCond );
		return true;
	}
	return false;
}

//************************************
// Method:    OutputAllData
// FullName:  NSProc::Summary::OutputAllData
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: FILE * fp
//************************************
void NSProc::Summary::OutputAllData( FILE* fp )
{
	if( !fp ) return;

	CString szOut;
	// loop through each segment and write data
	POSITION pos = m_segments.GetHeadPosition();
	while( pos )
	{
		Segment* pSeg = (Segment*)m_segments.GetNext( pos );
		if( pSeg )
		{
			OutputData( szOut, pSeg );
			pSeg->OutputData( szOut );
			fprintf( fp, szOut );
			fprintf( fp, _T("\n") );
			szOut.Empty();
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
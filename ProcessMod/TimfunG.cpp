/*************************************************
   TimFunG - version 2.5 - hlt - 10 Aug 1994
**************************************************
   Copyright 1994 Teulings

ARGUMENTS: hwr_infile tf_outfile [filterfreq (default=23.5)] [decimate (default=1)]

PURPOSES:
    (1) Calculates time functions and spectra
    (2) Lowpass filtering
    (3) Decimate allows to read only every decimate-th sample
    (4) Decimation occurs automatically if not specified.
          Suppress decimation by decimation = 1

UPDATES:
    13Jul94: gethwr() included, which does decimation
    20Jul94: Do not output spectrum point nfft/2+1
    22Jul94: Spectra calibrated.
    27Jul94: Automatic decimation.
    10Aug94: Automatic decimation only if decimation not specified.
    30Sep94: Prsmin = 1 (was 20).
    21Mar95: Message on trailing  pen lift shorter; decimate message only if not 1
    30Mar95: Preventing overflow processing files>32000; Message when >900,000,000.
    20Apr95: Jerk added; Decimate = 0 means automatic.
    31May95: readhwr() separated from main()
     9Jun95: Unit of jerk multiplied wih sec**3 instead of sec**2.
    12Dec95: Bug if optdecimate>1 but trailing part is penup, then
               optdecimate can be reduced. decimate was not set = optdecimate
    14Jan97: Better output on deffile and warnings for filter aliasing
             Bug in deffile read if no comment is given
     7May97: Removed spaces from putdata labels
    13Jun97: No filtering (4 times) if filter freq > fsample/2; extra warning
             Spectrum of raw signal or acceleration optional
    16Sep98: Include unrotate
    19Nov98: Zero input file caused zero devide
    19Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		     removal of statics
	19May00: New option: Automatic unrotation with whole trial is one stroke yielding slants in 4 quadrants.
	TODO: New Switch in automatic unrotation: One-stroke trial rotated to +vertical
	23May00: Bug in Automitc unrotatiton
	25May00: Fold Beta back to with -PI and +PI
	21jun01; Improved Beta in Automatic vs non-automatic. In Automatic it rotates to HORIZONTAL now.
	18Sep06: GMB: Converted deprecated string/file functions for MSVS7
	28Apr08: GMB: Removing jerk calculations & output
	07Oct08: Somesh: filtf, tranbh passed as double(floating point) to rlowps

CALIBRATION:
    If the sampling rate and resolution differ from the default
    Wacom PL-100V setting at ASU.
    Create a file "forceunitsec.def" in the current directory or or specify
    any other dev:\path\name by:
      set DEFFILE=....    (in Unix: setenv DEFFILE ...)
    Example of a forceunitsec.def file:
      0.00508 forceunit Comments ...
      0.005 sec Comments ...
      1 prsmin COmments ...

REQUIRES:
    Deffun.h
    Siglib.c
    Iolib.c

COMPILATION:
    (1) Borland BC under DOS
    (2) Borland BC for MSWindows
      Choose Options Application... WindowsApps (See (1))
    (3) Unix/Xwindows
      #!/bin/csh
      cc -c -D__platform__=__unx_x11__ timfun.c
      cc timfun.o -lm -o timfun

************************************************************************/
#include "../MFC/mfc.h"

#include <math.h>
#include "Procmod.h"
#include "..\Common\siglib.h"
#include "..\Common\ProcSettings.h"
#include "..\NSShared\iolib.h"
#include "..\NSShared\hlib.h"

// 02Sep10: Was 2048
#define NSMAXG			4096   /* should be power of 2 and larger than 1.25 * NS */
#define NFREQMAXG		NSMAXG/2
#define DEFAULTFILTF	23.5

#define	MODULE		_T("GRIPPERTIMFUN")

// FFT Low Pass vs Butterworth Low Pass
bool fftlwpass = 1;
void SetFFTG( bool fVal ) { fftlwpass = fVal; }
#define SHARP 1.75
double sharpnessg = SHARP;
void SetSharpnessG( double dVal ) { sharpnessg = dVal; }

/*************************************************************************/
bool TimeFunctionG( CString szHWRIn, CString szTFOut, double dFilterFreq,
					int nDecimate, unsigned int nSwitch )
/*************************************************************************/
{
	bool fRet = TRUE;

	/* Since FFT is 2 dimensional it can process an even nr of data although zz may not be used */
	double* x = new double[ NSMAXG ];
	double* y = new double[ NSMAXG ];
	double* z = new double[ NSMAXG ];
	double* xin = new double[ NSMAXG ];
	double* yin = new double[ NSMAXG ];
	double* zin = new double[ NSMAXG ];
	double* zz = new double[ NSMAXG ];	/* not actually used */
// 28Apr08: GMB: removing jerk output
// 	double* xout = new double[ NSMAXG ];
// 	double* yout = new double[ NSMAXG ];
// 	double* zout = new double[ NSMAXG ];
	double* xbuf = new double[ NSMAXG ];
	double* ybuf = new double[ NSMAXG ];
	double* zbuf = new double[ NSMAXG ];
	double* zzbuf = new double[ NSMAXG ];
	double* vaspectrum = new double[ NSMAXG ];
	double* vx = new double[ NSMAXG ];
	double* vy = new double[ NSMAXG ];

	// 04Nov08: Somesh: New placeholders for powerspectrum2 output
	double* vaspectrumreal = new double[ NSMAXG ];
	double* vaspectrumimag = new double[ NSMAXG ];

	double forceunit = 0.0, sec = 0.0;
	double fsampl = 0.0, fsamplout = 0.0;
	double filtf = dFilterFreq, tranbh = 0.0;
	double fact = 0.0;
	int is = 0, ns = 0, nsout = 0, nread = 0;
	int nfft = 0, nfftout = 0;
	int ns1 = 0, nfft1 = 0; /* Nonused output */
	int nw = 0;
	double alpha = 0.0, rf = 0.0;
	int decimate = nDecimate, decimatearg = 0;
	int diffspec = 1;

	CString szMsg, szTxtSpec;
	CStdioFile fIn;
	FILE* fOut = NULL;


	if( decimate == 0 ) decimate = 1;
	else decimatearg = decimate;


	// Initialize Arrays
	for( int i = 0; i < NSMAXG; i++ )
	{
		x[ i ] = 0.0;
		y[ i ] = 0.0;
		z[ i ] = 0.0;
		zz[ i ] = 0.0;
		xin[ i ] = 0.0;
		yin[ i ] = 0.0;
		zin[ i ] = 0.0;
// 28Apr08: GMB: removing jerk output
// 		xout[ i ] = 0.0;
// 		yout[ i ] = 0.0;
// 		zout[ i ] = 0.0;
		vx[ i ] = 0.0;
		vy[ i ] = 0.0;
		xbuf[ i ] = 0.0;
		ybuf[ i ] = 0.0;
		vaspectrum[ i ] = 0.0;
		zbuf[ i ] = 0.0;
		zzbuf[ i ] = 0.0;
	}

	/********************************/
	/*** Read dynamic definitions ***/
	/********************************/
//	::ReadDynamicGripperDefs( sec, forceunit );
	double junk = 0.0;
	::ReadDynamicDefs( forceunit, sec, junk );

	/** lowpass filter frequencies not yet known here: take safe values **/
	if( filtf <= 0 )
	{
		szMsg.Format( IDS_TF_FF, MODULE, filtf);
		OutputMessage( szMsg, LOG_PROC );
		return FALSE;
    }

	/* Decimation number; Take every decimate sample */
	if( decimate < 0 )
	{
		szMsg.Format( IDS_TF_DEC, MODULE, decimate);
		OutputMessage( szMsg, LOG_PROC );
		return FALSE;
	}

	/************/
	/* Switches */
	/************/
	if( ( nSwitch & TF_SWITCH_R ) != 0 )
	{
		diffspec = 0;
		szMsg.Format( IDS_TF_DS, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & TF_SWITCH_A ) != 0 )
	{
		diffspec = 2;
		szMsg.Format( IDS_TF_DS2, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	/*********************/
	/*** Open Input    ***/
	/*********************/
	if( !fIn.Open( szHWRIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_TFINP_ERR, MODULE, szHWRIn );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	/* Read */
	/* Quick patch to make arguments consistent again */
	/* Trailing pen ups are discarded */
	decimate = readhwr( fIn, szHWRIn, xin, yin, zin, (int)(NSMAXG / 1.25), (int) (NSMAXG / 1.25),
						ns, 0, decimate, decimatearg, -1000, TRUE, TRUE );
	/* Close */
	fIn.Close();


	/*********************************/
	/* correct units due to decimate */
	/*********************************/
	sec *= decimate;
	fsampl = 1. / sec;


	/*********************/
	/*** Open Output    ***/
	/*********************/
	if( ( fopen_s( &fOut, szTFOut, "w" ) ) != 0 )
	{
		szMsg.Format( IDS_TFOUT_ERR, MODULE, szTFOut );
		OutputMessage( szMsg, LOG_PROC );
		fRet = FALSE;
		goto Cleanup;
	}
	/*Raji: 28Jul04: New method to manipulate data adding samples at beginning & end*/
	/* For usable filtered data (beginning samples) in Butterworth and to eliminate noise in FFT at begin and end*/
	int nfiltershift = 0;
	if (sharpnessg < 1.1)
		sharpnessg = 1.1;
	if (!fftlwpass)
		sharpnessg = 1.33;
	tranbh = filtf / sharpnessg;
	if ((filtf - tranbh) <= 0.0)
		nfiltershift = 0;
	else
		nfiltershift = (int) (fsampl/(filtf-tranbh));

	if (nfiltershift > (0.2*ns))
		nfiltershift = (int)(0.2*ns);
	if (nfiltershift)
	{
		int nsnewtmp = 0;
		int nsnew = nfiltershift;
		for (is = 0; is < ns; is++)
		{
			x [is + nfiltershift] = xin [is];
			y [is + nfiltershift] = yin [is];
			z [is + nfiltershift] = zin [is];
			nsnew++;
			if (nsnew >= (int)(NSMAXG/1.25 - 1))
				break;
   		}
		nsnewtmp = nsnew;
		for (is = 0; is < nfiltershift; is++)
		{
			x [is] = x [nfiltershift];
			y [is] = y [nfiltershift];
			z [is] = z [nfiltershift];
		}

		for (is = 0; is < nfiltershift; is++)
		{
			x [nsnew + is] = x [nsnew - 1];
			y [nsnew + is] = y [nsnew - 1];
			z [nsnew + is] = z [nsnew - 1];
			nsnewtmp++;
			if (nsnew >= (int)(NSMAXG/1.25 - 1))
				break;
		}
		ns = nsnewtmp;
	}
	else
	{
		for (is = 0; is < ns; is++)
		{
			x [is] = xin [is];
			y [is] = yin [is];
			z [is] = zin [is];
		}
	}
	/*****************/
	/***Filtering*****/
	/*****************/

	/* FFT filter*/
	if (fftlwpass)
	{
		/**********/
		/* cyclic */
		/**********/
		/* Make also optimal ns and nfft */
		alpha = (double) 0.5;
		nw = 0;
		/* This routine suggests an optimal ns and nfft. The first time this information is not used */
		// 04Nov08: Somesh: Changed both to ns1 and nfft
		rwin (x, y, ns, NSMAXG, &ns1, &nfft, nw, alpha);
		rwin (z, zz, ns, NSMAXG, &ns1, &nfft, nw, alpha);

		/*******/
		/* fft */
		/*******/
		fftc (x, y, nfft, 1);
		fftc (z, zz, nfft, 1);

		/*** no interpolation of x, y, z implemented ***/
		nfftout = nfft;
		fsamplout = fsampl;
		nsout = ns;
		rf = nfft / fsamplout;  /* relative frequencies for rlowps */

		/*** x, y -> buffer ***/
		/* Copy and scale */
		fact = forceunit / nfft;
		for (is = 0; is < nfftout; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
			zbuf [is] = z [is] * fact;
			zzbuf [is] = zz [is] * fact;
		}

		/******************/
		/* lowpass filter */
		/******************/
		tranbh = filtf / 1.75;  //TODO
		szMsg.Format( IDS_TFLPF_0, MODULE, filtf, filtf - tranbh,
					filtf + tranbh, fsampl);
		OutputMessage( szMsg, LOG_PROC );
		if (filtf + tranbh > fsampl * 0.5)
		{
			szMsg.Format( IDS_TFLPF_1, MODULE );
			OutputMessage( szMsg, LOG_PROC );
		}
		if (filtf > fsampl * 0.5)
		{
			szMsg.Format( IDS_TFLPF_2, MODULE );
			OutputMessage( szMsg, LOG_PROC );
		}
		else
		{
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));
			rlowps (zbuf, zzbuf, nfft, (filtf * rf), (tranbh * rf));
		}


		/*****************/
		/* x, y filtered */
		/*****************/
		fftc (xbuf, ybuf, nfftout, -1);
		fftc (zbuf, zzbuf, nfftout, -1);

		//04Nov08: Somesh: nsout has relevant number of samples to output
		if ( ns - ns1 >= nfiltershift )
			nsout = ns - (2 * nfiltershift);
		else
			nsout = ns1 - nfiltershift;

		/*** output ***/
		szMsg.Format( IDS_TF_OUT, MODULE, szTFOut );
		OutputMessage( szMsg, LOG_PROC );

		putdata (fOut, &sec, 1, "sec", 1.);
		putdata (fOut, &sec, 1, "prsmin", 0.);	// added for consistency with hwr tf (prsmin)
		putdata (fOut, &sec, 1, "beta", 0.);		// "" (beta)
		putdata (fOut, &xbuf [nfiltershift], nsout, "x=LowerGrip(N)", 1.);
		putdata (fOut, &ybuf [nfiltershift], nsout, "y=UpperGrip(N)", 1.);
		putdata (fOut, &zbuf [nfiltershift], nsout, "z=Lift(N)", 1.);


		/*****************************/
		/* power spectrum unfiltered */
		/*****************************/
		// Unlinke Timfun.cpp where we calculate all 3 derivatives and their filtered results
		// use in TimfunG.cpp one derivative (specified in exp settings) for the x, y, and z signals and their filtered results.
		/* Copy and scale */
		if (diffspec == 0)
		{
			fact = forceunit / (nfft);
			szTxtSpec = _T("Spectrum");
		}
		else if (diffspec == 1)
		{
			fact = forceunit / (sec * nfft);
			szTxtSpec = _T("VSpectrum");
		}
		else if (diffspec == 2)
		{
			fact = forceunit / (sec *sec * nfft);
			szTxtSpec = _T("ASpectrum");
		}

		// Differentiate the FFT spectra
		// x and y channels together
		for (is = 0; is < nfftout; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
			zbuf  [is] = z  [is] * fact;
			zzbuf [is] = zz [is] * fact;
		}
		/* differentiate */
		if (diffspec >= 1) {
			rdif (xbuf, ybuf, nfftout);
			rdif (zbuf, zzbuf, nfftout);
		}
		if (diffspec >= 2) {
			rdif (xbuf, ybuf, nfftout);
			rdif (zbuf, zzbuf, nfftout);
		}
		// 10Nov06: Yi: Calculate power spectra per component.
		// Power spectrum per component alone
		powerspectrum2 (xbuf, ybuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale factor takes energy into account */
		// create similarly to szTxtSpec: Xspectrum
		putdata (fOut, vaspectrumreal, nfftout/2, _T("X_") + szTxtSpec, (double) nfftout / MAX (1, nsout));
		putdata (fOut, vaspectrumimag, nfftout/2, _T("Y_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		// Power spectrum per component alone
		powerspectrum2 (zbuf, zzbuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale factor takes energy into account */
		// create similarly to szTxtSpec: Zspectrum
		putdata (fOut, vaspectrumreal, nfftout/2, _T("Z_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		/* Left 2-dimensional */
		/***************************/
		/* power spectrum filtered */
		/***************************/
		szTxtSpec += _T("_Filtered");

		/******************/
		/* lowpass filter */
		/******************/
		if (filtf <= fsampl * 0.5)
		{
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));
			rlowps (zbuf, zzbuf, nfft, (filtf * rf), (tranbh * rf));
		}

		// Power spectrum per component alone
		powerspectrum2 (xbuf, ybuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale facter takes energy into account */
		putdata (fOut, vaspectrumreal, nfftout/2, _T("X_") + szTxtSpec, (double) nfftout / MAX (1, nsout));
		putdata (fOut, vaspectrumimag, nfftout/2, _T("Y_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		// Power spectrum per component alone
		powerspectrum2 (zbuf, zzbuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale facter takes energy into account */
		putdata (fOut, vaspectrumreal, nfftout/2, _T("Z_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		/****************************/
		/* differentiate unfiltered */
		/****************************/
		rdif (x, y, nfftout);
		rdif (z, zz, nfftout);

		/* Copy and scale */
		fact = forceunit / (sec * nfft);
		for (is = 0; is < nfftout; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
			zbuf [is] = z [is] * fact;
			zzbuf [is] = zz [is] * fact;
		}

		/******************/
		/* lowpass filter */
		/******************/
		if (filtf <= fsampl * 0.5)
		{
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));
			rlowps (zbuf, zzbuf, nfft, (filtf * rf), (tranbh * rf));
		}

		/*******************/
		/* vx, vy filtered */
		/*******************/
		fftc (xbuf, ybuf, nfftout, -1);
		fftc (zbuf, zzbuf, nfftout, -1);
		/*** output ***/
		putdata (fOut, &xbuf [nfiltershift], nsout, "vx(N/s)", 1.);
		putdata (fOut, &ybuf [nfiltershift], nsout, "vy(N/s)", 1.);
		putdata (fOut, &zbuf [nfiltershift], nsout, "vz(N/s)", 1.);		// norm timfun this section in file is vabs


		/*************************************/
		/* Differentiate 2nd time unfiltered */
		/*************************************/
		/*** ax, ay -> buffer ***/
		rdif (x, y, nfftout);
		rdif (z, zz, nfftout);

		/* Copy and scale */
		fact = forceunit / (sec * sec * nfft);
		for (is = 0; is < nfftout; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
			zbuf [is] = z [is] * fact;
			zzbuf [is] = zz [is] * fact;
		}

		/******************/
		/* Lowpass filter */
		/******************/
		if (filtf <= fsampl * 0.5)
		{
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));
			rlowps (zbuf, zzbuf, nfft, (filtf * rf), (tranbh * rf));
		}

		/*******************/
		/* ax, ay filtered */
		/*******************/
		fftc (xbuf, ybuf, nfftout, -1);
		fftc (zbuf, zzbuf, nfftout, -1);

		/*** output ***/
		putdata (fOut, &xbuf [nfiltershift], nsout, "ax(N/s**2)", 1.);
		putdata (fOut, &ybuf [nfiltershift], nsout, "ay(N/s**2)", 1.);
		putdata (fOut, &zbuf [nfiltershift], nsout, "az(N/s**2)", 1.);

	}
	else
	{
		int nstmp = 0;
		/*** x, y -> buffer ***/
		for (is = 0; is < ns; is++)
		{
			xbuf [is] = x [is];
			ybuf [is] = y [is];
		}


		/**********************************************/
		/* Butterworth filter (4th order) for x and y */
		/**********************************************/
		butterworth(x, ns, (int) fsampl,(int)(filtf));
		butterworth(y, ns, (int) fsampl,(int)(filtf));

		nsout = ns - (2 * nfiltershift);

		/*** output x and y***/
		szMsg.Format( IDS_TF_OUT, MODULE, szTFOut );
		OutputMessage( szMsg, LOG_PROC );

		putdata (fOut, &sec, 1, "sec", 1.);
		putdata (fOut, &sec, 1, "prsmin", 0.);	// added for consistency with hwr tf (prsmin)
		putdata (fOut, &sec, 1, "beta", 0.);		// "" (beta)
		putdata (fOut, &x [nfiltershift], nsout, "x=LowerGrip(N)=Grip", forceunit);
		putdata (fOut, &y [nfiltershift], nsout, "y=UpperGrip(N)=Grip", forceunit);
		putdata (fOut, &z [nfiltershift], nsout, "z=Lift(N)=Load", forceunit);

		/* prepend and append again for filtered spectrum*/
		for (is = 0; is < nfiltershift; is++)
		{
			x [is] = x [nfiltershift];
			y [is] = y [nfiltershift];
			z [is] = z [nfiltershift];
		}
		for (is = (ns - nfiltershift); is < ns; is++)
		{
			x [is] = x [ns - nfiltershift];
			y [is] = y [ns - nfiltershift];
			z [is] = z [ns - nfiltershift];
		}
		//TODO: Same code as in FFT method. Make routine?
		/*****************************/
		/* power spectrum unfiltered */
		/*****************************/
		// Unlinke Timfun.cpp where we calculate all 3 derivatives and their filtered results
		// use in TimfunG.cpp one derivative (specified in exp settings) for the x, y, and z signals and their filtered results.
		/* Copy and scale */
		if (diffspec == 0)
		{
			fact = forceunit / (nfft);
			szTxtSpec = _T("Spectrum");
		}
		else if (diffspec == 1)
		{
			fact = forceunit / (sec * nfft);
			szTxtSpec = _T("VSpectrum");
		}
		else if (diffspec == 2)
		{
			fact = forceunit / (sec *sec * nfft);
			szTxtSpec = _T("ASpectrum");
		}

		// Differentiate the FFT spectra
		// x and y channels together
		for (is = 0; is < nfftout; is++)
		{
			xbuf [is] = x [is] * fact;
			ybuf [is] = y [is] * fact;
			zbuf  [is] = z  [is] * fact;
			zzbuf [is] = zz [is] * fact;
		}
		/* differentiate */
		if (diffspec >= 1) {
			rdif (xbuf, ybuf, nfftout);
			rdif (zbuf, zzbuf, nfftout);
		}
		if (diffspec >= 2) {
			rdif (xbuf, ybuf, nfftout);
			rdif (zbuf, zzbuf, nfftout);
		}
		// 10Nov06: Yi: Calculate power spectra per component.
		// Power spectrum per component alone
		powerspectrum2 (xbuf, ybuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale factor takes energy into account */
		// create similarly to szTxtSpec: Xspectrum
		putdata (fOut, vaspectrumreal, nfftout/2, _T("X_") + szTxtSpec, (double) nfftout / MAX (1, nsout));
		putdata (fOut, vaspectrumimag, nfftout/2, _T("Y_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		// Power spectrum per component alone
		powerspectrum2 (zbuf, zzbuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale factor takes energy into account */
		// create similarly to szTxtSpec: Zspectrum
		putdata (fOut, vaspectrumreal, nfftout/2, _T("Z_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		/* Left 2-dimensional */
		/***************************/
		/* power spectrum filtered */
		/***************************/
		szTxtSpec += _T("_Filtered");

		/******************/
		/* lowpass filter */
		/******************/
		if (filtf <= fsampl * 0.5)
		{
			rlowps (xbuf, ybuf, nfft, (filtf * rf), (tranbh * rf));
			rlowps (zbuf, zzbuf, nfft, (filtf * rf), (tranbh * rf));
		}

		// Power spectrum per component alone
		powerspectrum2 (xbuf, ybuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale facter takes energy into account */
		putdata (fOut, vaspectrumreal, nfftout/2, _T("X_") + szTxtSpec, (double) nfftout / MAX (1, nsout));
		putdata (fOut, vaspectrumreal, nfftout/2, _T("Y_") + szTxtSpec, (double) nfftout / MAX (1, nsout));

		// Power spectrum per component alone
		powerspectrum2 (zbuf, zzbuf, nfftout, vaspectrumreal, vaspectrumimag);
		/* Scale facter takes energy into account */
		putdata (fOut, vaspectrumreal, nfftout/2, _T("Z_") + szTxtSpec, (double) nfftout / MAX (1, nsout));


		/****************/
		/* vx and vy */
		/****************/
	/* Tried the regular diff method in this new function - works fine*/
		/* could extend it based on reviewing the results*/
		bdif (x, ns, sec, vx);
		bdif (y, ns, sec, vy);
		bdif (z, ns, sec, zz);
		/*** output vx and vy ***/
		putdata (fOut, &vx [nfiltershift], nsout, "vx(N/s)", forceunit);
		putdata (fOut, &vy [nfiltershift], nsout, "vy(N/s)", forceunit);
		putdata (fOut, &zz [nfiltershift], nsout, "vz(N/s)", forceunit);


		/*************/
		/* ax and ay */
		/*************/
		/*** ax, ay -> buffer ***/
		bdif (vx, ns, sec, x);
		bdif (vy, ns, sec, y);
		bdif (zz, ns, sec, z);

		/*** output ax and ay***/
		putdata (fOut, &x [nfiltershift], nsout, "ax(N/s**2)", forceunit);
		putdata (fOut, &y [nfiltershift], nsout, "ay(N/s**2)", forceunit);
		putdata (fOut, &z [nfiltershift], nsout, "az(N/s**2)", forceunit);

	}

  /*** end of processing *************************************************/

Cleanup:
	if( fOut ) fclose( fOut );
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;
	if( xin ) delete [] xin;
	if( yin ) delete [] yin;
	if( zin ) delete [] zin;
// 28Apr08: GMB: removing jerk output
// 	if( xout ) delete [] xout;
// 	if( yout ) delete [] yout;
// 	if( zout ) delete [] zout;
	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( zz ) delete [] zz;
	if( xbuf ) delete [] xbuf;
	if( ybuf ) delete [] ybuf;
	if( vaspectrum ) delete [] vaspectrum;
	if( zbuf ) delete [] zbuf;
	if( zzbuf ) delete [] zzbuf;

	return fRet;
}




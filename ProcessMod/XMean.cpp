/*************************************************
   Xmean - version 1.0 - hlt - 16 Feb 1995
**************************************************
    Copyright 1994 Teulings

ARGUMENTS: 1/ inc_infile
           Each line contains numerical columns:
             first some indices: group subj cond, pattern, ...
             then data: #trials, #segments, data...
           2/ xme outfile
           3/ 1 or 2 factor numbers (1, 2, ...)
           4/ Transformations

PURPOSES:
    (1) Mean across any factor
    (2) Counts number of lines averaged (Appended per line).
    (3) Allows to make histograms of data in loew integer range.
    (4) Allows two factors: first factor has many levels, second has few.
    (5) Log, average, exp transformation
    (6) And SD appended (doubling line length).
          get SD of mean by dividing by sqrt(n)).

HISTORY:
  30Aug94: Updated for 2 factors
  31Aug94: Standardizes # output columns, putting NDATA in last columns
           Diffeent formats for integer and real.
           No blank line separators
  22Jan95: Skip missing data Missing data: DELTAT=0
  25Jan95: getline(): 15% slower and 25% larger (due to optotrack in iolib?)
  16Feb95: Adapted for 38 columns; Not appanding anymore.
  24Mar95: Automatic detection of integer indices; tests for unequal line lengths
   5May95: Output format changed from %7.3f to %8.4g
  16May95: Only Exp transformation (not yet dynamic)
  30Oct95: Error message if index = 0 not detected
   7Feb96: Log, average, exp transformation plus SD of mean appended
  27Feb96: Variance, log transform streamlined.
  24Apr96: /a transformation added. / arguments ingerated with numerical ones.
  29Apr96: No SD of mean but just SD.
   3Jul96: /a and /s can now be appanded at last argument (otherwise artifacts in xmean)
           sd calcul now based on abs transformed data, not on log transformed.
  27Aug96: Info if infile empty.
   6Mar97: Skip lines with out of range data and print them.
   6Aug97: Swtich /s added to average within subjects for wilcx tests
  15Sep97: Large (probably unreal numbers) warning
   2Oct98: /n noheader option added
   6Oct98: /i noincfile option added; Large numbers are considered missing
   1Jun00: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		   removal of statics
  29jul01: hlt: Rescaling of factor1 and 2 to allow for
  10Oct01: hlt: Output only non-zero lines histograms of averages

POTENTIAL BUG:
  The number of indices is sensed by the last non-zero column.

*/

/************************************************************************/

#include "../MFC/mfc.h"

#include "ProcMod.h"
#include <math.h>
#include "../NSShared/iolib.h"
#include "../Common/ProcSettings.h"

float varsqm (float sumsq, float sum, int n);


/* Movalyzer .inc files */
#define NINDEX 6
/* .msc files */
//#define NINDEX   9
/* merged .con files */
//#define NINDEX   3

#define NDATA    14
#define NDATACOR  6
#define NDATAMAX 2 * NDATA + NDATACOR          /* #Real data */
/* Number of output columns will be one higher because of counter */
#define NCOL     NINDEX + 2 * NDATA + NDATACOR /* #Columns = #Indices + #Data */



/* Input data */
//    #define DELTAT 11


#define NDECEL      ( 0 + NINDEX)
#define DELTAT      ( 1 + NINDEX)
#define STARTY      ( 2 + NINDEX)
#define DELTAY      ( 3 + NINDEX)
#define PEAKAY      ( 4 + NINDEX)
#define STARTX      ( 5 + NINDEX)
#define DELTAX      ( 6 + NINDEX)
#define PENUP       ( 7 + NINDEX)
#define STRAIGHTERR ( 8 + NINDEX)
#define SLANT       ( 9 + NINDEX)
#define JABSQ       (10 + NINDEX)
#define SURFACE     (11 + NINDEX)
#define VYPEAK      (12 + NINDEX)
#define RTVYPEAK    (13 + NINDEX)


#define SDNDECEL      ( 0 + NDATA + NINDEX)
#define SDDELTAT      ( 1 + NDATA + NINDEX)
#define SDSTARTY      ( 2 + NDATA + NINDEX)
#define SDDELTAY      ( 3 + NDATA + NINDEX)
#define SDPEAKAY      ( 4 + NDATA + NINDEX)
#define SDSTARTX      ( 5 + NDATA + NINDEX)
#define SDDELTAX      ( 6 + NDATA + NINDEX)
#define SDPENUP       ( 7 + NDATA + NINDEX)
#define SDSTRAIGHTERR ( 8 + NDATA + NINDEX)
#define SDSLANT       ( 9 + NDATA + NINDEX)
#define SDJABSQ       (10 + NDATA + NINDEX)
#define SDSURFACE     (11 + NDATA + NINDEX)
#define SDVYPEAK      (12 + NDATA + NINDEX)
#define SDRTVYPEAK    (13 + NDATA + NINDEX)

#define NDATACOR 6
#define CORDTDY   (0 + 2 * NDATA + NINDEX)
#define CORDTAP   (1 + 2 * NDATA + NINDEX)
#define CORDT1    (2 + 2 * NDATA + NINDEX)
#define CORDY1    (3 + 2 * NDATA + NINDEX)
#define CORDT2    (4 + 2 * NDATA + NINDEX)
#define CORDY2    (5 + 2 * NDATA + NINDEX)


//#define NLEVEL1  19
//#define NLEVEL2  19

//#define NLEVEL1  10
//#define NLEVEL2  38

//#define NLEVEL1  16
//#define NLEVEL2  23

//#define NLEVEL1  24
//#define NLEVEL2  15

#define NLEVEL1  36
//#define NLEVEL2  6
// 12Jun00: hlt
//#define NLEVEL2  10
#define NLEVEL2  36

//#define NLEVEL1  60
//#define NLEVEL2  3
#define NFACTOR  2

#define	MODULE		_T("XMEAN")

/************************************************************************/

/***********************************/
bool XMean( CString szIncIn, CString szXmeOut, int nFactor1, int nFactor2,
		    double dOffset1, double dScale1, double dOffset2, double dScale2,
			unsigned int nSwitch )
/***********************************/
{
	FILE *fp_xme = NULL;
	CStdioFile fp_inc;
	double* datain = new double[ NCOL ];
	static float datasum [NLEVEL2] [NLEVEL1] [NCOL];
	/* Sum */
	static float datas [NLEVEL2] [NLEVEL1] [NCOL];   /* transformed sum */
	static float dataq [NLEVEL2] [NLEVEL1] [NCOL];   /* sum of squares */
	static int   count [NLEVEL2] [NLEVEL1];
	int ilevel1 = 0, ilevel2 = 0;
	int nlevel1 = 0, nlevel2 = 0;
	int icol = 0, icol1 = 0, iline = 0;
	int ncol = 0;
	int ifactor = 0, nfactor = 0;
	int factor [NFACTOR];
	int nlevelmax = 0;
	int ndata = 0;
	int nindex = 0;
	int ndatamin = 0, ndatamax = 0;
	int iarg = 0;
	double datainold = 0.0;
	bool logtransformation = FALSE, abstransformation = FALSE;
	bool eof = FALSE, notyetread = TRUE, persubject = FALSE, afterfirstsubject = FALSE;
	bool nohead = FALSE, incfile = FALSE;
	CString szMsg, szTemp;

	/*****************/
	/*** Switches  ***/
	/*****************/
	if( ( nSwitch & X_SWITCH_S ) != 0 )
	{
		persubject = TRUE;
		szMsg.Format( IDS_X_PERSUBJECT, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & X_SWITCH_A ) != 0 )
	{
		abstransformation = TRUE;
		szMsg.Format( IDS_X_ABS, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & X_SWITCH_L ) != 0 )
	{
		logtransformation = TRUE;
		szMsg.Format( IDS_X_LOG, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & X_SWITCH_N ) != 0 )
	{
		nohead = TRUE;
		szMsg.Format( IDS_X_NOHEADER, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}
	if( ( nSwitch & X_SWITCH_I ) != 0 )
	{
		incfile = TRUE;
		szMsg.Format( IDS_X_NOTINC, MODULE );
		OutputMessage( szMsg, LOG_PROC );
	}

	// 12Jun00: hlt
	double missing = 0.0;

	/*************/
	/* Factors   */
	/*************/
	/* BUG if the /l switch is used with one dimension  it expects a second one */
	factor[ 0 ] = factor[ 1 ] = 0;
	if( nFactor1 > 0 )
	{
		if( ( nFactor1 < 1 ) || ( nFactor1 >= NCOL ) )
		{
			szMsg.Format( IDS_X_FACTORERR, MODULE, nFactor1, NCOL );
			OutputMessage( szMsg, LOG_PROC );
			if( datain ) delete [] datain;
			return FALSE;
		}
		else
		{
			nfactor++;
			factor[ 0 ] = nFactor1;
			/* Go from external range 1 - n to range 0 - n-1 */
			factor[ 0 ]--;
		}
	}
	if( nFactor2 > 0 )
	{
		if( ( nFactor2 < 1 ) || ( nFactor2 >= NCOL ) )
		{
			szMsg.Format( IDS_X_FACTORERR, MODULE, nFactor2, NCOL );
			OutputMessage( szMsg, LOG_PROC );
			if( datain ) delete [] datain;
			return FALSE;
		}
		else
		{
			nfactor++;
			factor[ 1 ] = nFactor2;
			/* Go from external range 1 - n to range 0 - n-1 */
			factor[ 1 ]--;
		}
	}

	/*******************/
	/* Open input file */
	/*******************/
	if( !fp_inc.Open( szIncIn, CFile::modeRead ) )
	{
		szMsg.Format( IDS_X_INCERR, MODULE, szIncIn );
		OutputMessage( szMsg, LOG_PROC );
		if( datain ) delete [] datain;
		return FALSE;
	}
	szMsg.Format( IDS_X_INCREAD, MODULE, szIncIn );
	OutputMessage( szMsg, LOG_PROC );

	/********************/
	/* Open output file */
	/********************/
	if( ( fp_xme = fopen( szXmeOut, "w" ) ) == NULL)
	{
		szMsg.Format( IDS_X_XMEERR, MODULE, szXmeOut );
		OutputMessage( szMsg, LOG_PROC );
		if( datain ) delete [] datain;
		return FALSE;
	}
	szMsg.Format( IDS_X_XMEWRITE, MODULE, szXmeOut );
	OutputMessage( szMsg, LOG_PROC );

	/* Header to output */
	if( !nohead )
	{
		fprintf( fp_xme, "%s ", MODULE );
		fprintf( fp_xme, "%s ", szIncIn );
		fprintf( fp_xme, "%s ", szXmeOut );
		if( nFactor1 >= 1 ) fprintf( fp_xme, "%d ", nFactor1 );
		if( nFactor2 >= 1 ) fprintf( fp_xme, "%d ", nFactor2 );
		if( persubject ) fprintf( fp_xme, "/s" );
		if( abstransformation ) fprintf( fp_xme, "/a" );
		if( logtransformation ) fprintf( fp_xme, "/l" );
		if( nohead ) fprintf( fp_xme, "/n" );
		if( incfile ) fprintf( fp_xme, "/i" );
		fprintf( fp_xme, "\n" );
	}

	szMsg.Empty();
	if( nfactor == 1 ) szMsg.Format( IDS_X_FACTOR1, MODULE, factor[ 0 ] + 1 );
	else if( nfactor = 2 ) szMsg.Format( IDS_X_FACTOR2, MODULE, factor[ 0 ] + 1, factor[ 1 ] + 1 );
	OutputMessage( szMsg, LOG_PROC );


	/*****************************************************************************/
	/* Repeat reading and writing for each of the subjects as marked in column 2 */
	/*****************************************************************************/
#define EMPTY 0. /* nonexisting subj nr */
#define SUBJ 1
	datain[ SUBJ ] = EMPTY;

	for( icol = 0; icol < NCOL; icol++ )
		datain[ icol ] = 0.;

	while( !eof )
	{
		/**************/
		/* Initialize */
		/**************/
		ncol = 0;

		for (ilevel2 = 0; ilevel2 < NLEVEL2; ilevel2++)
		{
			for (ilevel1 = 0; ilevel1 < NLEVEL1; ilevel1++)
			{
				count [ilevel2] [ilevel1] = 0;
				for (icol = 0; icol < NCOL; icol++)
				{
					datasum [ilevel2] [ilevel1] [icol] = 0.;
					datas [ilevel2] [ilevel1] [icol] = 0.;
					dataq [ilevel2] [ilevel1] [icol] = 0.;
				}
			}
		}
		nindex = NINDEX;
		ndatamin = NCOL;
		ndatamax = 0;

		/**********************************************/
		/*** Read stroke by stroke till end of file ***/
		/**********************************************/
		/* And process each line */
		iline = 0;
		while (TRUE)
		{
nextline:;
			/* Read of not yet read */
			datainold = datain [SUBJ];
			if (notyetread)
				ndata = getlinefloat( fp_inc, datain, NCOL );
			notyetread = TRUE;
			/* End of file: quit reading */
			if (ndata == 0)
			{
				eof = TRUE;
				break;
			}
			/* New subject: suspend reading and remember that line already read */
			if (persubject && datain [SUBJ] != datainold && afterfirstsubject)
			{
				notyetread = FALSE;
				/* Remember current subject */
				break;
			}

			afterfirstsubject = TRUE;

			iline++;
			/* Number of data per line */
			ndatamin = MIN (ndatamin, ndata);
			ndatamax = MAX (ndatamax, ndata);
			/* Test for change of number of data per line */
			if (ndatamin != ndatamax)
			{
				szMsg.Format( IDS_X_INCWHERE, MODULE, szIncIn, nindex, ndatamin, ndatamax );
				OutputMessage( szMsg, LOG_PROC );
				szMsg.Format( _T("Line#=%d: "), iline );
				for (icol = 0; icol < ndata; icol++)
				{
					szTemp.Format( _T("%.3g "), datain[ icol ] );
					szMsg += szTemp;
				}
				szMsg += _T("\n");
				OutputMessage( szMsg, LOG_PROC );
				goto nextline;
			}

// 12Jun00: hlt: Do not test ranges as we may also use data columns
#if 0
			/* Check number of indices */
			if (incfile)
			{
				for (icol = 0; icol < MIN (NINDEX, ndata); icol++)
					if ((int) datain [icol] != datain [icol])
						break;
				nindex = MIN (nindex, icol);
			}
#endif
			/* Update actual number of columns */
			ncol = MAX (ncol, ndata);

			/* 29jul01: hlt: Rescaling data from, say from 0.2 to 0.5 */
			/* to an appropriate integer range, say 0-10 */
			/* so that each unique integer can identify a class within which data are collapsed */
			//ilevel1 = (int)( datain [factor [0]] - 1 );
			//ilevel2 = (int)( datain [factor [1]] - 1 );
			/* The following values should be settable for each of the column numbers separately */
			/* Similar to setting dispersion, etc.) */
			/* Unlike dispersion, which is only for display purposes */
			/* The offset and scaling are used in xmean to add data into the appropriate bins */
			/* When the data are output none of the other modules need to know about the rescaling */
			/* I tested this idea in my dos software but could not test it in the Movalyzer */
			/* because I expected xmean to be called each time you change x, y, or grouping */
			/* Xmean should also be called each time you change offsets and scales 1 and 2 */

double offset [2];
double scale [2];
int ilevel;

			offset [0] = dOffset1;
			scale [0] = dScale1;
			offset [1] = dOffset2;
			scale [1] = dScale2;


			/* Test for illegal index values */
			/* 29jul01: hlt: This test needs the same rescaling */
			/* This test is crucial as it prevents storing data outside array boundaries */
			for (ifactor = 0; ifactor < nfactor; ifactor++)
			{
				if (ifactor == 0) nlevelmax = NLEVEL1;
				else if (ifactor == 1) nlevelmax = NLEVEL2;
				/* 29 Jul 01 hlt: Patch added to assure that the rescaled indices stay within array boundaries */
				/* If they fall outside we eventually want to know the number and percentage */
				/* and the data range that falls outside the array boundaries */
				/* Either the array boundaries have to be scaled up (may be disaster) */
				/* or the scales and offsets have to be chosen better */
				ilevel = (int)(( datain [factor [0]] + offset [ifactor]) * scale [ifactor] - 1.);


				/* Test */
				/* 29 Jul 01: hlt replaced datain[[]] by ilevel */
				//if (datain [factor [ifactor]] > nlevelmax || datain [factor [ifactor]] < 1)
				if (ifactor > nlevelmax || ifactor < 1)
				{
					/* Output the original data but we should eventually get a message how to change scale and offest */
					szMsg.Format( IDS_X_INCTEST, MODULE, szIncIn, factor[ ifactor ] + 1,
								  datain[ factor[ ifactor ] ], nlevelmax, 1, iline );
					for (icol = 0; icol < ndata; icol++)
					{
						szTemp.Format( _T(" %.3g"), datain[ icol ] );
						szMsg += szTemp;
					}
					szMsg += _T("\n");
					// 12jun00: hlt When out of range occurs, clip data instead of leaving loop
					// Here a lot of messages may be generated
					OutputMessage( szMsg, LOG_PROC );
					//goto nextline;
					/* 29 Jul 01: hlt: replaced datain[[]] by ilevel */
					/* However, datain's range remains 1-nlevelmax (there will be 1 -1 operation down the road I hope) */
					//if (datain [factor [ifactor]] > nlevelmax) datain [factor [ifactor]] = nlevelmax;
					//if (datain [factor [ifactor]] < 1) datain [factor [ifactor]] = 1;
					if (ilevel > nlevelmax) datain [factor [ifactor]] = nlevelmax;
					if (ilevel < 1) datain [factor [ifactor]] = 1;
				}
			}

			/* Datain #factor determines the level of the index */
			/* Outside data with ranges 1 - n */
//ASSERT(0);
			ilevel1 = (int)(( datain [factor [0]] + offset [0]) * scale [0] - 1.);
			ilevel2 = (int)(( datain [factor [1]] + offset [1]) * scale [1] - 1.);
			/* Keep ilevel2 constant, otherwise ilevel2 would act als factor 1 */
			if (nfactor == 1) ilevel2 = 0;
			nlevel1 = MAX (nlevel1, ilevel1 + 1);
			nlevel2 = MAX (nlevel2, ilevel2 + 1);

			/*********************************************/
			/* Replace excessive values by missing value */
			/*********************************************/
			// 12jun00: missing is now dynamic
			//#define MISSING -1000. //This is an arbitrary stupid value, used to be 0.
#define LARGE 1.e10
			for (icol = 0; icol < NCOL; icol++)
			{
				/* Detect exessive values */
				// 25apr03: gmb: added checks to ensure indinces to arrays were within bounds
				if( ( ( nlevel1 < NLEVEL1 ) && ( nlevel2 < NLEVEL2 ) ) &&
					( ( datain [icol] > LARGE ) || ( datain [icol] < -LARGE ) ) )
				{
					/* Warning */
					szMsg.Format( IDS_X_MISSING, MODULE, szIncIn, datain[ icol ],
								  ilevel1, ilevel2, icol );
					for (icol1 = 0; icol1 < MIN (NINDEX, ndata); icol1++)
					{
						szTemp.Format( _T("%g "), datain[ icol1 ] );
						szMsg += szTemp;
					}
					szMsg += _T("\n");
					OutputMessage( szMsg, LOG_PROC );
					/* Replace by MISSING */
					// 12jun00: hlt missing instead of MISSING
					datain [icol] = missing;
				}
			}

			/****************************/
			/* Cumulate input and count */
			/****************************/
			/* It may be wrong to cumulate nonmissing data */
			/* The alternative would be: there are no data if one is missing */
			/* Better is to test more data for missing */
			/* Do at any rate if not inc_file */
			// 12jun00: hlt: missing instead of MISSING
			// 25apr03: gmb: added checks to ensure indinces to arrays were within bounds
			if( ( ( nlevel1 < NLEVEL1 ) && ( nlevel2 < NLEVEL2 ) ) &&
				( !incfile || ( datain [DELTAT] != missing ) ) )
			{
				count [ilevel2] [ilevel1]++;
				for (icol = 0; icol < NCOL; icol++)
				{
					/***************************/
					/* Transformation of input */
					/***************************/
					/* Inverse of log abs transformation */
					/* Only if it is an inc_file */
					if (incfile && abstransformation)
					{
						/* Only cm and s difference data */
						if (icol >= NINDEX   && icol != NDECEL   && icol != SDNDECEL &&
							icol != STARTX   && icol != SDSTARTX && icol != STARTY &&
							icol != SDSTARTY && icol != PENUP    && icol != SDPENUP  &&
							icol != SLANT    && icol != SDSLANT  && icol != RTVYPEAK &&
							icol != SDRTVYPEAK)
						{
							/* Transform only if not missing */
							if (datain [icol] != missing)
							// 12jun00: hlt: missing instead of MISSING
								datain [icol] = (float)fabs ((double) datain [icol]);
						}
					}

					dataq [ilevel2] [ilevel1] [icol]    += (float)SQR (datain [icol]);
					datasum [ilevel2] [ilevel1] [icol]    += (float)datain [icol];

					if (incfile && logtransformation)
					{
						/* Only cm and s difference data */
						if (icol >= NINDEX &&
							icol != NDECEL   && icol != SDNDECEL &&
							icol != STARTX   && icol != SDSTARTX &&
							icol != STARTY   && icol != SDSTARTY &&
							icol != PENUP    && icol != SDPENUP  &&
							icol != SLANT    && icol != SDSLANT  &&
							icol != RTVYPEAK && icol != SDRTVYPEAK)
						{
							/* Transform only if not missing */
							// 12jun00: hlt: missing instead of MISSING
							if (datain [icol] != missing)
								datain [icol] = (float)log ((double) datain [icol]);
						}
					}

					/* This may give rounding errors because not -mean and not double */
					/* No transformation for sd estimate */
					datas [ilevel2] [ilevel1] [icol]    += (float)datain [icol];
				}
			}
		}

		/* Only first time because data
		/* BUG: if first group has only one subjnr things may go wrong. */
#define GROUP 0
		if (persubject)
		{
			szMsg.Format( IDS_X_SUBJ, MODULE, datain[ GROUP ], datainold );
			OutputMessage( szMsg, LOG_PROC );
		}

		/* Feedback */
		if (incfile && nindex != NINDEX)
		{
			szMsg.Format( IDS_X_DELTATERR, MODULE, nindex, NINDEX, NINDEX - nindex, ndatamax );
			OutputMessage( szMsg, LOG_PROC );
			/* Sometimes it is false alarm */
			nindex = NINDEX;
		}
		/* When processing data of only one group over all subjects */
		/* there must be a number of subjects not belonging to that group */
		/* and they result in empty data which is ok */
		if (iline == 0)
		{
			szMsg.Format( _T("%s: INFO: #lines=%d."), MODULE, iline );
			OutputMessage( szMsg, LOG_PROC );
			fclose( fp_xme );
			if( datain ) delete [] datain;
			return FALSE;
		}

		/****************/
		/*** Calculus ***/
		/****************/
		for (ilevel2 = 0; ilevel2 < nlevel2; ilevel2++)
		{
			for (ilevel1 = 0; ilevel1 < nlevel1; ilevel1++)
			{
				for (icol = 0; icol < NCOL; icol++)
				{
					if (count [ilevel2] [ilevel1] != 0)
					{
						/* mean */
						datas [ilevel2] [ilevel1] [icol] /= count [ilevel2] [ilevel1];

						/* var */
						dataq [ilevel2] [ilevel1] [icol] =
							varsqm (dataq [ilevel2] [ilevel1] [icol],
									datasum [ilevel2] [ilevel1] [icol],
									count [ilevel2] [ilevel1]);

						/* sd */
						if (dataq [ilevel2] [ilevel1] [icol] > 0.)
							//dataq [ilevel2] [ilevel1] [icol] =
								//sqrt (dataq [ilevel2] [ilevel1] [icol] / count [ilevel2] [ilevel1]);
							/* no 1/sqrt(n) for now */
							dataq [ilevel2] [ilevel1] [icol] = (float)
								sqrt (dataq [ilevel2] [ilevel1] [icol]);
						else
							dataq [ilevel2] [ilevel1] [icol] = 0.;
					}
					else
					{
						datas [ilevel2] [ilevel1] [icol] = 0.;
						dataq [ilevel2] [ilevel1] [icol] = 0.;
					}
				}
			}
		}

		/**************************/
		/* Inverse Transformation */
		/**************************/
		/* Inverse of log transformation */
		if (incfile && logtransformation)
		{
			szMsg.Format( IDS_X_LOGTRANS, MODULE );
			OutputMessage( szMsg, LOG_PROC );
			for (icol = NINDEX; icol < NINDEX + 2 * NDATA; icol++)
			{
				/* Only cm and s difference data */
				if (icol != NDECEL   && icol != SDNDECEL &&
					icol != STARTX   && icol != SDSTARTX &&
					icol != STARTY   && icol != SDSTARTY &&
					icol != PENUP    && icol != SDPENUP  &&
					icol != SLANT    && icol != SDSLANT  &&
					icol != RTVYPEAK && icol != SDRTVYPEAK)
				{
					for (ilevel2 = 0; ilevel2 < nlevel2; ilevel2++)
					{
						for (ilevel1 = 0; ilevel1 < nlevel1; ilevel1++)
						{
							if (count [ilevel2] [ilevel1] != 0)
							{
								datas [ilevel2] [ilevel1] [icol] = (float)
									exp ((double) datas [ilevel2] [ilevel1] [icol]);
							}
						}
					}
				}
			}
		}

		/**************/
		/*** Output ***/
		/**************/
		/* Data to output */
		for (ilevel2 = 0; ilevel2 < nlevel2; ilevel2++)
		{
			for (ilevel1 = 0; ilevel1 < nlevel1; ilevel1++)
			{

				/* 10Oct01: hlt: Output only non-zero lines */
				/* Non-zero lines are those where the #data>0 */
				if (count [ilevel2] [ilevel1] > 0)
				{
					for (icol = 0; icol < ndatamax; icol++)
					{
						/* Insert columns of zeroes */
						if (incfile && icol == nindex)
						{
							for (icol1 = nindex; icol1 < NINDEX; icol1++)
							{
								/* zero */
//								fprintf (fp_xme, " %3d    ", 0);
								fprintf (fp_xme, " %d", 0);
							}
						}
						if ((int) datas [ilevel2] [ilevel1] [icol] == datas [ilevel2] [ilevel1] [icol])
							/* integer */
//							fprintf (fp_xme, " %3d    ", (int) datas [ilevel2] [ilevel1] [icol]);
							fprintf (fp_xme, " %d", (int) datas [ilevel2] [ilevel1] [icol]);
						else
							/* real */
//							fprintf (fp_xme, " %8.4g", datas [ilevel2] [ilevel1] [icol]);
							fprintf (fp_xme, " %.4g", datas [ilevel2] [ilevel1] [icol]);
					}
					/******************/
					/* #data averaged */
					/******************/
//					fprintf (fp_xme, " %3d", count [ilevel2] [ilevel1]);
					fprintf (fp_xme, " %d", count [ilevel2] [ilevel1]);

					/* append sd data */
					for (icol = 0; icol < ndatamax; icol++)
					{
						/* Insert columns of zeroes */
						if (icol == nindex)
						{
							for (icol1 = nindex; icol1 < NINDEX; icol1++)
							{
								/* zero */
//								fprintf (fp_xme, " %3d    ", 0);
								fprintf (fp_xme, " %d", 0);
							}
						}
						if ((int) dataq [ilevel2] [ilevel1] [icol] == dataq [ilevel2] [ilevel1] [icol])
							/* integer */
//							fprintf (fp_xme, " %3d    ", (int) dataq [ilevel2] [ilevel1] [icol]);
							fprintf (fp_xme, " %d", (int) dataq [ilevel2] [ilevel1] [icol]);
						else
							/* real */
//							fprintf (fp_xme, " %8.4g", dataq [ilevel2] [ilevel1] [icol]);
							fprintf (fp_xme, " %.4g", dataq [ilevel2] [ilevel1] [icol]);
					}
					/* #data averaged */
					// 12jun00: hlt: The SD of #ndata averages = 0 would be more systematic here
					/* SD #data averaged */
//					fprintf (fp_xme, " %d", count [ilevel2] [ilevel1]);
//					fprintf (fp_xme, " %3d", count [ilevel2] [ilevel1]);
					fprintf (fp_xme, " %3d", 0);
					fprintf (fp_xme, "\n");

				}
			}
		}
	}

	// cleanup
	fclose (fp_xme);
	if( datain ) delete [] datain;

	return TRUE;
}

/***********************************************/
float varsqm (float sumsq, float sum, int n)
/***********************************************/
/* Variance from sum of squares and sum */
{
	float var = 0.;

	if (n > 1) var = (float)((sumsq - SQR (sum) / n) / (n - 1.));
	else var = 0.;

	return var;
}
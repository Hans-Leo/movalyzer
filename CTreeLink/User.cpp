#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBUser.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szUserFile[ _MAX_PATH ] = _T("user");
static char _UDataPath[ _MAX_PATH ] = _T("");

// global data object
USER user;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBUser::SetDataPath( CString szPath )
{
	strcpy_s( _UDataPath, szPath );
}

///////////////////////////////////////////////////////////////////////////////

char* DBUser::GetDataPath()
{
	return _UDataPath;
}

///////////////////////////////////////////////////////////////////////////////

void DBUser::GetObjectName( CString& szName )
{
	szName = _T("user");
}

///////////////////////////////////////////////////////////////////////////////

void DBUser::GetFileName( CTString& szFile )
{
	char* pszDataPath = GetDataPath();
	m_table->SetPath( pszDataPath );
	szFile = szUserFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBUser::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Description", CT_FSTRING, szUSER_DESC + 1 );
		m_table->AddField( "RootPath", CT_FSTRING, MAX_PATH + 1 );
		m_table->AddField( "BackupPath", CT_FSTRING, MAX_PATH + 1 );
		m_table->AddField( "Delim", CT_FSTRING, szUSER_DELIM + 1 );
		m_table->AddField( "InputType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TabletMode", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EntireTablet", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Log", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "LogUI", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "LogDB", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "LogProc", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "LogGraph", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "LogTablet", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Alpha", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SubjectPass", CT_FSTRING, szUSER_PASS_ENC + 1 );
		m_table->AddField( "TabletWidth", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "TabletHeight", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "DisplayWidth", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "DisplayHeight", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "AspectRatioWidth", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "AspectRatioHeight", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "CommPort", CT_FSTRING, szCOMM_PORT + 1 );
		m_table->AddField( "AutoUpdate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Private", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "WinUser", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "GenSubjID", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SubjStartID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "SubjEndID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "SubjCurID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Encrypted", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EncryptionMethod", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SiteID", CT_FSTRING, szCODE + 1 );
		m_table->AddField( "SiteDesc", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Signature", CT_FSTRING, szSIGNATURE + 1 );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "UserID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBUser::GetData( pDBDATA pData, CString& szErr )
{
	pUSER p = (pUSER)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->UserID, m_record->GetFieldAsString( ++i ).c_str(), szIDS + 1 );
		strncpy_s( p->Desc, m_record->GetFieldAsString( ++i ).c_str(), szUSER_DESC + 1 );
		strncpy_s( p->RootPath, m_record->GetFieldAsString( ++i ).c_str(), MAX_PATH + 1 );
		strncpy_s( p->BackupPath, m_record->GetFieldAsString( ++i ).c_str(), MAX_PATH + 1 );
		strncpy_s( p->Delim, m_record->GetFieldAsString( ++i ).c_str(), szUSER_DELIM + 1 );
		p->InputType = m_record->GetFieldAsSigned( ++i );
		p->TabletMode = m_record->GetFieldAsSigned( ++i );
		p->EntireTablet = m_record->GetFieldAsSigned( ++i );
		p->Log = m_record->GetFieldAsSigned( ++i );
		p->LogUI = m_record->GetFieldAsSigned( ++i );
		p->LogDB = m_record->GetFieldAsSigned( ++i );
		p->LogProc = m_record->GetFieldAsSigned( ++i );
		p->LogGraph = m_record->GetFieldAsSigned( ++i );
		p->LogTablet = m_record->GetFieldAsSigned( ++i );
		p->Alpha = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->Pass, m_record->GetFieldAsString( ++i ).c_str(), szUSER_PASS_ENC + 1 );
		p->TabletWidth = m_record->GetFieldAsFloat( ++i );
		p->TabletHeight = m_record->GetFieldAsFloat( ++i );
		p->DisplayWidth = m_record->GetFieldAsFloat( ++i );
		p->DisplayHeight = m_record->GetFieldAsFloat( ++i );
		p->AspectRatioWidth = m_record->GetFieldAsFloat( ++i );
		p->AspectRatioHeight = m_record->GetFieldAsFloat( ++i );
		strncpy_s( p->CommPort, m_record->GetFieldAsString( ++i ).c_str(), szCOMM_PORT + 1 );
		p->AutoUpdate = m_record->GetFieldAsSigned( ++i );
		p->Private = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->WinUser, m_record->GetFieldAsString( ++i ).c_str(), szDESC + 1 );
		p->GenSubjID = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SubjStartID, m_record->GetFieldAsString( ++i ).c_str(), szIDS + 1 );
		strncpy_s( p->SubjEndID, m_record->GetFieldAsString( ++i ).c_str(), szIDS + 1 );
		strncpy_s( p->SubjCurID, m_record->GetFieldAsString( ++i ).c_str(), szIDS + 1 );
		p->Encrypted = m_record->GetFieldAsSigned( ++i );
		p->EncryptionMethod = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SiteID, szCODE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->SiteDesc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Signature, szSIGNATURE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBUser::SetData( pDBDATA pData, CString& szErr )
{
	pUSER p = (pUSER)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->UserID );
		m_record->SetFieldAsString( ++i, p->Desc );
		m_record->SetFieldAsString( ++i, p->RootPath );
		m_record->SetFieldAsString( ++i, p->BackupPath );
		m_record->SetFieldAsString( ++i, p->Delim );
		m_record->SetFieldAsSigned( ++i, p->InputType );
		m_record->SetFieldAsSigned( ++i, p->TabletMode );
		m_record->SetFieldAsSigned( ++i, p->EntireTablet );
		m_record->SetFieldAsSigned( ++i, p->Log );
		m_record->SetFieldAsSigned( ++i, p->LogUI );
		m_record->SetFieldAsSigned( ++i, p->LogDB );
		m_record->SetFieldAsSigned( ++i, p->LogProc );
		m_record->SetFieldAsSigned( ++i, p->LogGraph );
		m_record->SetFieldAsSigned( ++i, p->LogTablet );
		m_record->SetFieldAsSigned( ++i, p->Alpha );
		m_record->SetFieldAsString( ++i, p->Pass );
		m_record->SetFieldAsFloat( ++i, p->TabletWidth );
		m_record->SetFieldAsFloat( ++i, p->TabletHeight );
		m_record->SetFieldAsFloat( ++i, p->DisplayWidth );
		m_record->SetFieldAsFloat( ++i, p->DisplayHeight );
		m_record->SetFieldAsFloat( ++i, p->AspectRatioWidth );
		m_record->SetFieldAsFloat( ++i, p->AspectRatioHeight );
		m_record->SetFieldAsString( ++i, p->CommPort );
		m_record->SetFieldAsSigned( ++i, p->AutoUpdate );
		m_record->SetFieldAsSigned( ++i, p->Private );
		m_record->SetFieldAsString( ++i, p->WinUser );
		m_record->SetFieldAsSigned( ++i, p->GenSubjID );
		m_record->SetFieldAsString( ++i, p->SubjStartID );
		m_record->SetFieldAsString( ++i, p->SubjEndID );
		m_record->SetFieldAsString( ++i, p->SubjCurID );
		m_record->SetFieldAsSigned( ++i, p->Encrypted );
		m_record->SetFieldAsSigned( ++i, p->EncryptionMethod );
		m_record->SetFieldAsString( ++i, p->SiteID );
		m_record->SetFieldAsString( ++i, p->SiteDesc );
		m_record->SetFieldAsString( ++i, p->Signature );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pUSER DBUser::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pUSER p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &user, szErr );
			p = &user;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBUser::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBUser::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f300 = false;
	bool f350 = false;
	bool f410 = false;
	bool f430 = false;
	bool f431 = false;	// internal
	bool fDoSize = false;

	// check for field size of old var (everyone should have)
	CTField fld = m_table->GetField( "SubjectPass" );
	if( fld.GetLength() != ( szUSER_PASS_ENC + 1 ) )
	{
		fMod = true;
		fDoSize = true;
	}

	// version 3.00 new
	try
	{
		m_table->GetField( _T("CommPort") );
	}
	catch( ... )
	{
		fMod = true;
		f300 = true;
	}

	// version 3.50
	try
	{
		m_table->GetField( _T("AutoUpdate") );
	}
	catch( ... )
	{
		fMod = true;
		f350 = true;
	}

	// version 4.10
	try
	{
		m_table->GetField( _T("WinUser") );
	}
	catch( ... )
	{
		fMod = true;
		f410 = true;
	}

	// version 4.30
	try
	{
		m_table->GetField( _T("GenSubjID") );
	}
	catch( ... )
	{
		fMod = true;
		f430 = true;
	}
	// version 4.31 (internal)
	try
	{
		m_table->GetField( _T("Encrypted") );
	}
	catch( ... )
	{
		fMod = true;
		f431 = true;
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 3.00
		if( f300 )
		{
			m_table->GetField( "AspectRatioHeight" );
			m_table->AddField( "CommPort", CT_FSTRING, szCOMM_PORT + 1 );
		}
		// version 3.50
		if( f300 || f350 )
		{
			m_table->AddField( "AutoUpdate", CT_INT4, sizeof( CT_INT4 ) );
		}
		// version 4.10
		if( f300 || f350 || f410 )
		{
			m_table->AddField( "Private", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "WinUser", CT_FSTRING, szDESC + 1 );
		}
		// version 4.30
		if( f300 || f350 || f410 || f430 )
		{
			CTField fld = m_table->AddField( "GenSubjID", CT_INT4, sizeof( CT_INT4 ) );
			str = _T("0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "SubjStartID", CT_FSTRING, szIDS + 1 );
			str = _T("000");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "SubjEndID", CT_FSTRING, szIDS + 1 );
			str = _T("ZZZ");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SubjCurID", CT_FSTRING, szIDS + 1 );
		}
		// version 4.31
		if( f300 || f350 || f410 || f430 || f431 )
		{
			CTField fld = m_table->AddField( "Encrypted", CT_INT4, sizeof( CT_INT4 ) );
			str = _T("0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "EncryptionMethod", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), ENC_NONE );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SiteID", CT_FSTRING, szCODE + 1 );
			m_table->AddField( "SiteDesc", CT_FSTRING, szDESC + 1 );
			m_table->AddField( "Signature", CT_FSTRING, szSIGNATURE + 1 );
		}
		// subject password size change
		if( fDoSize )
		{
			fld = m_table->GetField( "SubjectPass" );
			fld.SetLength( szUSER_PASS_ENC + 1 );
		}

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

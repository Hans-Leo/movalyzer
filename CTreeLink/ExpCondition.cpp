#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBExpCondition.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szExpCondFile[ _MAX_PATH ] = _T("expcond");

// global data object
EXPERIMENTCONDITION expcondition;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBExperimentCondition::GetObjectName( CString& szName )
{
	szName = _T("experimentcondition");
}

///////////////////////////////////////////////////////////////////////////////

void DBExperimentCondition::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		m_table->SetPath( pszDataPath );
		strcpy_s( pszFile, szExpCondFile );
	}
	else strcpy_s( pszFile, szExpCondFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentCondition::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ExpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx2 = m_table->AddField( "CondID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Replications", CT_INT4, sizeof( CT_INT4 ) );

		// add indices
		// experiment only
		CTIndex idx1 = m_table->AddIndex( "Exp", CTINDEX_FIXED, YES, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
		// exp + cond
		CTIndex idx2 = m_table->AddIndex( "ExpCond", CTINDEX_FIXED, NO, NO );
		idx2.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx2.AddSegment( fldIdx2, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentCondition::GetData( pDBDATA pData, CString& szErr )
{
	pEXPERIMENTCONDITION p = (pEXPERIMENTCONDITION)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ExpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->CondID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Replications = m_record->GetFieldAsSigned( ++i );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentCondition::SetData( pDBDATA pData, CString& szErr )
{
	pEXPERIMENTCONDITION p = (pEXPERIMENTCONDITION)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ExpID );
		m_record->SetFieldAsString( ++i, p->CondID );
		m_record->SetFieldAsSigned( ++i, p->Replications );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pEXPERIMENTCONDITION DBExperimentCondition::Find( CString szExpID, CString szCondID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pEXPERIMENTCONDITION p = NULL;
	TEXT eid[ szIDS + 1 ], cid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( cid, szCondID, szIDS );
	cid[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// select the second index (exp + cond)
		m_record->SetDefaultIndex( 1 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, cid );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &expcondition, szErr );
			p = &expcondition;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pEXPERIMENTCONDITION DBExperimentCondition::Find( CString szExpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pEXPERIMENTCONDITION p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szExpID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// select the first index (exp)
		m_record->SetDefaultIndex( 0 );
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &expcondition, szErr );
			p = &expcondition;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentCondition::Remove( CString szExpID, CString szCondID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szCondID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			// cleanup deleted items by compacting
			m_table->Rebuild( CTREBUILD_NONE );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

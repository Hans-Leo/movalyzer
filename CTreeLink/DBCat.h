#ifndef __DB_CAT__
#define	__DB_CAT__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Cat
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct CATA : public DBDATA
{
	FLD_char( CatID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_INT( Type );
};

typedef CATA* pCAT;

// DB class
class AFX_EXT_CLASS DBCat : public DBData
{
public:
	DBCat() {}
	~DBCat() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pCAT Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBCat* pDBCat;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_CAT__

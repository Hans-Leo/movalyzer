#ifndef __DB_PROCSETTING__
#define	__DB_PROCSETTING__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// ProcSetting
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct PROCSETTING : public DBDATA
{
	FLD_char( ExpID, szIDS );
	// time function settings
	FLD_BOOL( Velocity );
	FLD_BOOL( Differentiate );
	FLD_DOUBLE( RotationBeta );
	FLD_DOUBLE( FilterFrequency );
	FLD_INT( Decimate );
	// input device settings
	FLD_INT( SamplingRate );
	FLD_INT( MinPenPressure );
	FLD_DOUBLE( DeviceResolution );
	// time function flags
	FLD_BOOL( TF_J );
	FLD_BOOL( TF_R );
	FLD_BOOL( TF_A );
	FLD_BOOL( TF_U );
	FLD_BOOL( TF_O );
	// segmentation flags
	FLD_BOOL( SEG_F );
	FLD_BOOL( SEG_L );
	FLD_BOOL( SEG_S );
	FLD_BOOL( SEG_O );
	FLD_BOOL( SEG_M );
	FLD_BOOL( SEG_V );
	// extraction flags
	FLD_BOOL( EXT_S );
	FLD_BOOL( EXT_3 );
	FLD_BOOL( EXT_O );
	// consistency checking flags
	FLD_BOOL( CON_A );
	FLD_BOOL( CON_N );
	FLD_BOOL( CON_M );
	FLD_BOOL( CON_U );
	FLD_BOOL( CON_C );
	FLD_BOOL( CON_S );
	FLD_BOOL( CON_O );
	FLD_BOOL( CON_D );	// discontinuity check
	// recording options
	FLD_BOOL( SpecifySequence );
	FLD_char( CondSeq, szTRIALS );
	FLD_BOOL( Randomize );
	FLD_BOOL( RandomizeTrials );
	// summarization flags
	FLD_BOOL( ST_A );
	FLD_BOOL( ST_R );
	FLD_BOOL( ST_Z );
	FLD_BOOL( ST_T );
	FLD_BOOL( ST_S );
	FLD_BOOL( ST_D );
	FLD_INT( DiscardAfter );
	FLD_BOOL( ST_D2 );
	FLD_BOOL( DiscardAfter2 );
	FLD_BOOL( DiscardPenDownDurMin );
	FLD_DOUBLE( PenDownDurMin );
	// recording options
	FLD_BOOL( ShowInstr );
	// sen2word settings
	FLD_DOUBLE( minleftpenup );
	FLD_DOUBLE( minleftsp );
	FLD_DOUBLE( minwidth );
	FLD_DOUBLE( mindownsp );
	FLD_DOUBLE( mintimepenup );
	FLD_DOUBLE( maxtimepenup );
	// timfun settings
	FLD_BOOL( FFT );
	FLD_DOUBLE( sharpness );
	FLD_INT( upsamplefactor );
	FLD_BOOL( remtraillift );
	// segmentation settings
	FLD_DOUBLE( absdistmin );
	FLD_DOUBLE( rdistmin );
	FLD_DOUBLE( timemin );
	FLD_DOUBLE( rvymin );
	FLD_DOUBLE( rvymin2 );
	// extraction settings
	FLD_DOUBLE( initialpart );
	FLD_DOUBLE( maxfreq );
	// consis settings
	FLD_DOUBLE( slanterror );
	FLD_DOUBLE( spiralincreasefactor );
	FLD_DOUBLE( minlooparean );
	FLD_DOUBLE( straightcritstraight );
	FLD_DOUBLE( straightcritcurved );
	FLD_BOOL( discardLTmin );
	FLD_DOUBLE( minreactiontime );
	FLD_BOOL( discardGTmax );
	FLD_DOUBLE( maxreactiontime );
	// run experiment options
	FLD_BOOL( DoQuestionnaire );
	FLD_BOOL( QuestAtEnd );
	FLD_DOUBLE( TimeoutStart );
	FLD_DOUBLE( TimeoutRecord );
	FLD_DOUBLE( TimeoutPenlift );
	FLD_DOUBLE( TimeoutLatency );
	FLD_BOOL( MaxRecArea );
	FLD_BOOL( FullScreen );
	FLD_BOOL( ProcImmediately );
	FLD_BOOL( DisplayCharts );
	FLD_BOOL( DisplayRaw );
	FLD_BOOL( DisplayTF );
	FLD_BOOL( DisplaySeg );
	FLD_BOOL( Summarize );
	FLD_BOOL( SummarizeOnly );
	FLD_BOOL( SummarizeOnlyGroup );
	FLD_BOOL( SummarizeNormDB );
	FLD_BOOL( SummarizeSelect );
	FLD_BOOL( Analyze );
	FLD_BOOL( ViewSubjExt );
	FLD_BOOL( ViewSubjCon );
	FLD_BOOL( ViewExpExt );
	FLD_BOOL( ViewExpCon );
	// in-between-trial mode (no delay, return to start, instruction)
	FLD_INT( TrialMode );
	FLD_BOOL( SEG_A );
	FLD_BOOL( SEG_MM );		// move segmentation point to the nearest absolute velocity minima within stroke
	FLD_BOOL( SEG_J );		// segmentation method: at pen down trajectories
	FLD_BOOL( EXT_2 );		// Calculate loop area between every consecutive 2 strokes, instead of every consecutive stroke
	// drawing options in recording window
	FLD_INT( LineSize );
	FLD_INT( EllipseSize );
	FLD_DWORD( BGColor );
	FLD_DWORD( LineColor1 );
	FLD_DWORD( LineColor2 );
	FLD_DWORD( LineColor3 );
	FLD_DWORD( EllipseColor );
	FLD_DWORD( MPPColor );	// min pen-pressure color
	FLD_BOOL( VaryLineThickness );
	FLD_INT( MaxLineThickness );
	// new consis parameters for comparing a circle to multiple L's
//	FLD_BOOL( CircleLLL );
//	FLD_DOUBLE( ThldRatioCircleSequence );
//	FLD_DOUBLE( ThldHorizRelCircle );
//	FLD_DOUBLE( ThldRatioLSequence );
//	FLD_DOUBLE( ThldHorizRelL );
//	FLD_BOOL( CircleIgnoreFirst );
//	FLD_BOOL( CircleIgnoreLast );
//	FLD_BOOL( LLLIgnoreFirst );
//	FLD_BOOL( LLLIgnoreLast );
	// timfun(iolib) discontinuity
	FLD_INT( DisCorrection );
	FLD_DOUBLE( DisFactor );
	FLD_DOUBLE( DisZInsertPoint );
	FLD_DOUBLE( DisRelError );
	FLD_DOUBLE( DisAbsError );
	// Randomization with Rules vars
	FLD_BOOL( RandWithRules );
	FLD_char( RandRules, szRULES );
	FLD_char( RandBlockCounts, szRULES );
	FLD_char( RandSelChars, szRULES );
	FLD_char( RandSelTrials, szTRIALS );
	// Summarization collapsing
	FLD_BOOL( CollapseUDStrokes );		// collapse each trial to up/down (2 strokes)
	FLD_BOOL( CollapseStrokes );		// collapse each trial to 1 stroke
	FLD_BOOL( CollapseTrials );			// collapse all trials to strokes (n strokes)
	FLD_BOOL( CollapseMedian );			// collapse using median instead of average
	// to use or not - tilt
	FLD_BOOL( UseTilt );
	// Max Pen Pressure
	FLD_INT( MaxPenPressure );			// max pressure before notification
	// Discontinuity Detection
	FLD_BOOL( DisNotify );				// notify if dis. during recording
	FLD_DOUBLE( DisError );				// % of sampling rate deviation before detection
	// Image experiment settings
	FLD_INT( SmoothingIter );
	FLD_INT( InkThreshold );
	FLD_INT( ImgResolution );
	// External application settings
	FLD_INT( ExtTypeRaw );					// external app type before TimeFunction call (values defined in DataMod interface)
	FLD_char( ExtScriptRaw, szFILELENGTH );	// script file for pre-TF call
	FLD_INT( ExtTypeTF );					// external app type before Segmentation call
	FLD_char( ExtScriptTF, szFILELENGTH );	// script file for pre-Seg call
	FLD_INT( ExtTypeSeg );					// external app type before Extraction call
	FLD_char( ExtScriptSeg, szFILELENGTH );	// script file for pre-Ext call
	FLD_INT( ExtTypeExt );					// external app type after Extraction call
	FLD_char( ExtScriptExt, szFILELENGTH );	// script file for post-Ext call
	// Norm DB
	FLD_BOOL( NormDBCalcScore );
	FLD_BOOL( NormDBCalcScoreGlobal );
	FLD_BOOL( NormDBNoUpdate );
	FLD_DOUBLE( NormDBThreshold );
};

typedef PROCSETTING* pPROCSETTING;

// DB class
class AFX_EXT_CLASS DBProcSetting : public DBData
{
public:
	DBProcSetting() {}
	~DBProcSetting() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pPROCSETTING Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBProcSetting* pDBProcSetting;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_PROCSETTING__

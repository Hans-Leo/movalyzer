#ifndef __DB_SUBJECT__
#define	__DB_SUBJECT__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Subject
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct SUBJECT : public DBDATA
{
	FLD_char( SubjID, szIDS );
	FLD_char( LastName, szSUBJECT_LAST_ENC );
	FLD_char( FirstName, szSUBJECT_FIRST_ENC );
	FLD_char( Notes, szNOTES );
	FLD_char( PrvNotes, szNOTES_ENC );
	FLD_DATE( DateAdded );
	FLD_char( DefExp, szIDS );	// subject's default experiment
	FLD_BOOL( Active );			// user could be selected as inactive if no longer participating
	FLD_INT( ExpCount );		// # of times subject has run an experiment
	FLD_BOOL( Encrypted );		// has this data been encrypted yet?
	FLD_char( SubjCode, szCODE );
	FLD_INT( EncryptionMethod );
	FLD_char( SiteID, szCODE );				// (optional) identifier for where subject was created
	FLD_char( SiteDesc, szDESC );			// (optional) desc. for where subject was created
	FLD_char( Signature, szSIGNATURE );		// unique identifier for creator of subject
	FLD_char( Password, szSUBJECT_PASS );		// password to access this data (encrypted info)
};

typedef SUBJECT* pSUBJECT;

// DB class
class AFX_EXT_CLASS DBSubject : public DBData
{
public:
	DBSubject() {}
	~DBSubject() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pSUBJECT Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBSubject* pDBSubject;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_SUBJECT__

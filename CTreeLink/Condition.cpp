#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBCondition.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szCondFile[ _MAX_PATH ] = _T("condition");
static char szCondTempFile[ _MAX_PATH ] = _T("conditiontemp");

// global data object
CONDITION condition;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBCondition::GetObjectName( CString& szName )
{
	szName = _T("condition");
}

///////////////////////////////////////////////////////////////////////////////

void DBCondition::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szCondFile );
		else strcpy_s( pszFile, szCondTempFile );
	}
	else strcpy_s( pszFile, szCondFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBCondition::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Description", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Instr", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Lex", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "StrokeMin", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "StrokeMax", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "StrokeLength", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "RangeLength", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "StrokeDirection", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "RangeDirection", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "StrokeSkip", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Notes", CT_FSTRING, szNOTES + 1 );
		m_table->AddField( "Record", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Process", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Parent", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Word", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "UseStimulus", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "StimulusW", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "StimulusP", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Stimulus", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "PrecueDuration", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "PrecueLatency", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "RecordImmediately", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "StopWrongTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "WarningDuration", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "WarningLatency", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "MagnetForce", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "SoundStart", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundEnd", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundEndTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundEndToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundEndToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundEndMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundStimPStart", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStartTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStart", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStartTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStartToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStartToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStartMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundStimWStop", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStopTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStopToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStopToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStopMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundStimPStartToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStartToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStartMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundStimPStop", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStopTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStopToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStopToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStopMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundTargetCorrect", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetCorrectTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetCorrectToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetCorrectToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetCorrectMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundTargetWrong", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetWrongTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetWrongToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetWrongToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetWrongMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundPenup", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPenupTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPenupToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPenupToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPenupMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "SoundPendown", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPendownTone", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPendownToneFreq", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPendownToneDur", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPendownMedia", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "CountStrokes", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideFeedback", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideShowAfter", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideShowAfterShow", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideShowAfterStrokes", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideShowCount", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Transform", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Xgain", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Xrotation", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Ygain", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Yrotation", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "flagbadtarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundEndTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStartTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStopTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStartTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStopTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetCorrectTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetWrongTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPenupTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPendownTrigger", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Feedback", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "StartFirstTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "StopLastTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "GripperTask", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TrialFeedback1", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TrialFeedback2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FBColumn1", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FBColumn2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FBStroke1", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FBStroke2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FBMin1", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "FBMin2", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "FBMax1", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "FBMax2", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "FBSwapColors1", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FBSwapColors2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStartScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundEndScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundEndScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundStimWStartScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStartScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundStimWStopScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimWStopScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundStimPStartScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStartScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundStimPStopScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundStimPStopScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundTargetCorrectScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetCorrectScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundTargetWrongScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundTargetWrongScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundPenupScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPenupScript", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SoundPendownScriptType", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SoundPendownScript", CT_FSTRING, szFILELENGTH + 1 );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "CondID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBCondition::GetData( pDBDATA pData, CString& szErr )
{
	pCONDITION p = (pCONDITION)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->CondID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Desc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Instr, szCONDITION_INSTR + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Lex, szCONDITION_LEX + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->StrokeMin = m_record->GetFieldAsSigned( ++i );
		p->StrokeMax = m_record->GetFieldAsSigned( ++i );
		p->StrokeLength = m_record->GetFieldAsFloat( ++i );
		p->RangeLength = m_record->GetFieldAsFloat( ++i );
		p->StrokeDirection = m_record->GetFieldAsFloat( ++i );
		p->RangeDirection = m_record->GetFieldAsFloat( ++i );
		p->StrokeSkip = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->Notes, szNOTES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Record = m_record->GetFieldAsSigned( ++i );
		p->Process = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->Parent, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Word = m_record->GetFieldAsSigned( ++i );
		p->UseStimulus = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->StimulusW, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->StimulusP, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Stimulus, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->PrecueDuration = m_record->GetFieldAsFloat( ++i );
		p->PrecueLatency = m_record->GetFieldAsFloat( ++i );
		p->RecordImmediately = m_record->GetFieldAsSigned( ++i );
		p->StopWrongTarget = m_record->GetFieldAsSigned( ++i );
		p->WarningDuration = m_record->GetFieldAsFloat( ++i );
		p->WarningLatency = m_record->GetFieldAsFloat( ++i );
		p->MagnetForce = m_record->GetFieldAsFloat( ++i );
		p->SoundStart = m_record->GetFieldAsSigned( ++i );
		p->SoundStartTone = m_record->GetFieldAsSigned( ++i );
		p->SoundStartToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundStartToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStartMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundEnd = m_record->GetFieldAsSigned( ++i );
		p->SoundEndTone = m_record->GetFieldAsSigned( ++i );
		p->SoundEndToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundEndToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundEndMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimPStart = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStartTone = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStart = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStartTone = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStartToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStartToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimWStartMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimWStop = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStopTone = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStopToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStopToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimWStopMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimPStartToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStartToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimPStartMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimPStop = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStopTone = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStopToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStopToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimPStopMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundTargetCorrect = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetCorrectTone = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetCorrectToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetCorrectToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundTargetCorrectMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundTargetWrong = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetWrongTone = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetWrongToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetWrongToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundTargetWrongMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundPenup = m_record->GetFieldAsSigned( ++i );
		p->SoundPenupTone = m_record->GetFieldAsSigned( ++i );
		p->SoundPenupToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundPenupToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundPenupMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundPendown = m_record->GetFieldAsSigned( ++i );
		p->SoundPendownTone = m_record->GetFieldAsSigned( ++i );
		p->SoundPendownToneFreq = m_record->GetFieldAsSigned( ++i );
		p->SoundPendownToneDur = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundPendownMedia, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->CountStrokes = m_record->GetFieldAsSigned( ++i );
		p->HideFeedback = m_record->GetFieldAsSigned( ++i );
		p->HideShowAfter = m_record->GetFieldAsSigned( ++i );
		p->HideShowAfterShow = m_record->GetFieldAsSigned( ++i );
		p->HideShowAfterStrokes = m_record->GetFieldAsSigned( ++i );
		p->HideShowCount = m_record->GetFieldAsFloat( ++i );
		p->Transform = m_record->GetFieldAsSigned( ++i );
		p->Xgain = m_record->GetFieldAsFloat( ++i );
		p->Xrotation = m_record->GetFieldAsFloat( ++i );
		p->Ygain = m_record->GetFieldAsFloat( ++i );
		p->Yrotation = m_record->GetFieldAsFloat( ++i );
		p->flagbadtarget = m_record->GetFieldAsSigned( ++i );
		p->SoundStartTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundEndTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStartTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundStimWStopTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStartTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundStimPStopTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetCorrectTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundTargetWrongTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundPenupTrigger = m_record->GetFieldAsSigned( ++i );
		p->SoundPendownTrigger = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->Feedback, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->StartFirstTarget = m_record->GetFieldAsSigned( ++i );
		p->StopLastTarget = m_record->GetFieldAsSigned( ++i );
		p->GripperTask = m_record->GetFieldAsSigned( ++i );
		p->TrialFeedback1 = m_record->GetFieldAsSigned( ++i );
		p->TrialFeedback2 = m_record->GetFieldAsSigned( ++i );
		p->FBColumn1 = m_record->GetFieldAsSigned( ++i );
		p->FBColumn2 = m_record->GetFieldAsSigned( ++i );
		p->FBStroke1 = m_record->GetFieldAsSigned( ++i );
		p->FBStroke2 = m_record->GetFieldAsSigned( ++i );
		p->FBMin1 = m_record->GetFieldAsFloat( ++i );
		p->FBMin2 = m_record->GetFieldAsFloat( ++i );
		p->FBMax1 = m_record->GetFieldAsFloat( ++i );
		p->FBMax2 = m_record->GetFieldAsFloat( ++i );
		p->FBSwapColors1 = m_record->GetFieldAsSigned( ++i );
		p->FBSwapColors2 = m_record->GetFieldAsSigned( ++i );
		p->SoundStartScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStartScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundEndScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundEndScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimWStartScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimWStartScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimWStopScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimWStopScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimPStartScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimPStartScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundStimPStopScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundStimPStopScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundTargetCorrectScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundTargetCorrectScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundTargetWrongScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundTargetWrongScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundPenupScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundPenupScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SoundPendownScriptType = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SoundPendownScript, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBCondition::SetData( pDBDATA pData, CString& szErr )
{
	pCONDITION p = (pCONDITION)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->CondID );
		m_record->SetFieldAsString( ++i, p->Desc );
		m_record->SetFieldAsString( ++i, p->Instr );
		m_record->SetFieldAsString( ++i, p->Lex );
		m_record->SetFieldAsSigned( ++i, p->StrokeMin );
		m_record->SetFieldAsSigned( ++i, p->StrokeMax );
		m_record->SetFieldAsFloat( ++i, p->StrokeLength );
		m_record->SetFieldAsFloat( ++i, p->RangeLength );
		m_record->SetFieldAsFloat( ++i, p->StrokeDirection );
		m_record->SetFieldAsFloat( ++i, p->RangeDirection );
		m_record->SetFieldAsSigned( ++i, p->StrokeSkip );
		m_record->SetFieldAsString( ++i, p->Notes );
		m_record->SetFieldAsSigned( ++i, p->Record );
		m_record->SetFieldAsSigned( ++i, p->Process );
		m_record->SetFieldAsString( ++i, p->Parent );
		m_record->SetFieldAsSigned( ++i, p->Word );
		m_record->SetFieldAsSigned( ++i, p->UseStimulus );
		m_record->SetFieldAsString( ++i, p->StimulusW );
		m_record->SetFieldAsString( ++i, p->StimulusP );
		m_record->SetFieldAsString( ++i, p->Stimulus );
		m_record->SetFieldAsFloat( ++i, p->PrecueDuration );
		m_record->SetFieldAsFloat( ++i, p->PrecueLatency );
		m_record->SetFieldAsSigned( ++i, p->RecordImmediately );
		m_record->SetFieldAsSigned( ++i, p->StopWrongTarget );
		m_record->SetFieldAsFloat( ++i, p->WarningDuration );
		m_record->SetFieldAsFloat( ++i, p->WarningLatency );
		m_record->SetFieldAsFloat( ++i, p->MagnetForce );
		m_record->SetFieldAsSigned( ++i, p->SoundStart );
		m_record->SetFieldAsSigned( ++i, p->SoundStartTone );
		m_record->SetFieldAsSigned( ++i, p->SoundStartToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundStartToneDur );
		m_record->SetFieldAsString( ++i, p->SoundStartMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundEnd );
		m_record->SetFieldAsSigned( ++i, p->SoundEndTone );
		m_record->SetFieldAsSigned( ++i, p->SoundEndToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundEndToneDur );
		m_record->SetFieldAsString( ++i, p->SoundEndMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStart );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStartTone );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStart );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStartTone );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStartToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStartToneDur );
		m_record->SetFieldAsString( ++i, p->SoundStimWStartMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStop );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStopTone );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStopToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStopToneDur );
		m_record->SetFieldAsString( ++i, p->SoundStimWStopMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStartToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStartToneDur );
		m_record->SetFieldAsString( ++i, p->SoundStimPStartMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStop );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStopTone );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStopToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStopToneDur );
		m_record->SetFieldAsString( ++i, p->SoundStimPStopMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetCorrect );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetCorrectTone );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetCorrectToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetCorrectToneDur );
		m_record->SetFieldAsString( ++i, p->SoundTargetCorrectMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetWrong );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetWrongTone );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetWrongToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetWrongToneDur );
		m_record->SetFieldAsString( ++i, p->SoundTargetWrongMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundPenup );
		m_record->SetFieldAsSigned( ++i, p->SoundPenupTone );
		m_record->SetFieldAsSigned( ++i, p->SoundPenupToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundPenupToneDur );
		m_record->SetFieldAsString( ++i, p->SoundPenupMedia );
		m_record->SetFieldAsSigned( ++i, p->SoundPendown );
		m_record->SetFieldAsSigned( ++i, p->SoundPendownTone );
		m_record->SetFieldAsSigned( ++i, p->SoundPendownToneFreq );
		m_record->SetFieldAsSigned( ++i, p->SoundPendownToneDur );
		m_record->SetFieldAsString( ++i, p->SoundPendownMedia );
		m_record->SetFieldAsSigned( ++i, p->CountStrokes );
		m_record->SetFieldAsSigned( ++i, p->HideFeedback );
		m_record->SetFieldAsSigned( ++i, p->HideShowAfter );
		m_record->SetFieldAsSigned( ++i, p->HideShowAfterShow );
		m_record->SetFieldAsSigned( ++i, p->HideShowAfterStrokes );
		m_record->SetFieldAsFloat( ++i, p->HideShowCount );
		m_record->SetFieldAsSigned( ++i, p->Transform );
		m_record->SetFieldAsFloat( ++i, p->Xgain );
		m_record->SetFieldAsFloat( ++i, p->Xrotation );
		m_record->SetFieldAsFloat( ++i, p->Ygain );
		m_record->SetFieldAsFloat( ++i, p->Yrotation );
		m_record->SetFieldAsSigned( ++i, p->flagbadtarget );
		m_record->SetFieldAsSigned( ++i, p->SoundStartTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundEndTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStartTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStopTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStartTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStopTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetCorrectTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetWrongTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundPenupTrigger );
		m_record->SetFieldAsSigned( ++i, p->SoundPendownTrigger );
		m_record->SetFieldAsString( ++i, p->Feedback );
		m_record->SetFieldAsSigned( ++i, p->StartFirstTarget );
		m_record->SetFieldAsSigned( ++i, p->StopLastTarget );
		m_record->SetFieldAsSigned( ++i, p->GripperTask );
		m_record->SetFieldAsSigned( ++i, p->TrialFeedback1 );
		m_record->SetFieldAsSigned( ++i, p->TrialFeedback2 );
		m_record->SetFieldAsSigned( ++i, p->FBColumn1 );
		m_record->SetFieldAsSigned( ++i, p->FBColumn2 );
		m_record->SetFieldAsSigned( ++i, p->FBStroke1 );
		m_record->SetFieldAsSigned( ++i, p->FBStroke2 );
		m_record->SetFieldAsFloat( ++i, p->FBMin1 );
		m_record->SetFieldAsFloat( ++i, p->FBMin2 );
		m_record->SetFieldAsFloat( ++i, p->FBMax1 );
		m_record->SetFieldAsFloat( ++i, p->FBMax2 );
		m_record->SetFieldAsSigned( ++i, p->FBSwapColors1 );
		m_record->SetFieldAsSigned( ++i, p->FBSwapColors2 );
		m_record->SetFieldAsSigned( ++i, p->SoundStartScriptType );
		m_record->SetFieldAsString( ++i, p->SoundStartScript );
		m_record->SetFieldAsSigned( ++i, p->SoundEndScriptType );
		m_record->SetFieldAsString( ++i, p->SoundEndScript );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStartScriptType );
		m_record->SetFieldAsString( ++i, p->SoundStimWStartScript );
		m_record->SetFieldAsSigned( ++i, p->SoundStimWStopScriptType );
		m_record->SetFieldAsString( ++i, p->SoundStimWStopScript );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStartScriptType );
		m_record->SetFieldAsString( ++i, p->SoundStimPStartScript );
		m_record->SetFieldAsSigned( ++i, p->SoundStimPStopScriptType );
		m_record->SetFieldAsString( ++i, p->SoundStimPStopScript );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetCorrectScriptType );
		m_record->SetFieldAsString( ++i, p->SoundTargetCorrectScript );
		m_record->SetFieldAsSigned( ++i, p->SoundTargetWrongScriptType );
		m_record->SetFieldAsString( ++i, p->SoundTargetWrongScript );
		m_record->SetFieldAsSigned( ++i, p->SoundPenupScriptType );
		m_record->SetFieldAsString( ++i, p->SoundPenupScript );
		m_record->SetFieldAsSigned( ++i, p->SoundPendownScriptType );
		m_record->SetFieldAsString( ++i, p->SoundPendownScript );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pCONDITION DBCondition::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pCONDITION p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &condition, szErr );
			p = &condition;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBCondition::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBCondition::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f300 = false;
	bool f310 = false;
	bool f340 = false;
	bool fHack = false;
	bool f440 = false;
	bool f480 = false;
	bool f600 = false;

	// version 3.00 new
	try
	{
		if( !fMod ) m_table->GetField( _T("flagbadtarget") );
		if( !fMod )
		{
			// internal hack since employees already have change from before but not users
			fHack = true;
			m_table->GetField( _T("SoundStartTrigger") );
		}
	}
	catch( ... )
	{
		fMod = true;
		f300 = true;
	}

	// version 3.10
	if( !fMod )
	{
		try
		{
			m_table->GetField( _T("Feedback") );
		}
		catch( ... )
		{
			fMod = true;
			f310 = true;
		}
	}

	// version 3.40
	if( !fMod )
	{
		try
		{
			m_table->GetField( _T("StartFirstTarget") );
		}
		catch( ... )
		{
			fMod = true;
			f340 = true;
		}
	}

	// version 4.40
	if( !fMod )
	{
		try
		{
			m_table->GetField( _T("GripperTask") );
		}
		catch( ... )
		{
			fMod = true;
			f440 = true;
		}
	}

	// version 4.80
	if( !fMod )
	{
		try
		{
			m_table->GetField( _T("TrialFeedback1") );
		}
		catch( ... )
		{
			fMod = true;
			f480 = true;
		}
	}

	// version 6.00
	if( !fMod ) {
		try {
			m_table->GetField( _T("SoundStartScriptType") );
		}
		catch(...) {
			fMod = true;
			f600 = true;
		}
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 3.00
		if( f300 )
		{
			m_table->GetField( "Yrotation" );
			if( !fHack ) m_table->AddField( "flagbadtarget", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundStartTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundEndTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundStimWStartTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundStimWStopTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundStimPStartTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundStimPStopTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundTargetCorrectTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundTargetWrongTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundPenupTrigger", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SoundPendownTrigger", CT_INT4, sizeof( CT_INT4 ) );
		}
		// version 3.10
		if( f300 || f310 )
		{
			m_table->AddField( "Feedback", CT_FSTRING, szIDS + 1 );
		}
		// version 3.40
		if( f300 || f310 || f340 )
		{
			m_table->AddField( "StartFirstTarget", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "StopLastTarget", CT_INT4, sizeof( CT_INT4 ) );
		}
		// version 4.40
		if( f300 || f310 || f340 || f440 )
		{
			m_table->AddField( "GripperTask", CT_INT4, sizeof( CT_INT4 ) );
		}
		// version 4.80
		if( f300 || f310 || f340 || f440 || f480 )
		{
			CTField fld = m_table->AddField( "TrialFeedback1", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "TrialFeedback2", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBColumn1", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBColumn2", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBStroke1", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBStroke2", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBMin1", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 0.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBMin2", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 0.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBMax1", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 1.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBMax2", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 1.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBSwapColors1", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FBSwapColors2", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 6.00
		if( f300 || f310 || f340 || f440 || f480 || f600 ) {
			CTField fld = m_table->AddField( "SoundStartScriptType", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundStartScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundEndScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundEndScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundStimWStartScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundStimWStartScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundStimWStopScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundStimWStopScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundStimPStartScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundStimPStartScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundStimPStopScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundStimPStopScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundTargetCorrectScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundTargetCorrectScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundTargetWrongScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundTargetWrongScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundPenupScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundPenupScript", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "SoundPendownScriptType", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SoundPendownScript", CT_FSTRING, szFILELENGTH + 1 );
		}

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

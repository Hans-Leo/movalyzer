#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBProcSetting.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szProcSetFile[ _MAX_PATH ] = _T("procset");
static char szProcSetTempFile[ _MAX_PATH ] = _T("procsettemp");

// global data object
PROCSETTING procset;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBProcSetting::GetObjectName( CString& szName )
{
	szName = _T("experimentsettings");
}

///////////////////////////////////////////////////////////////////////////////

void DBProcSetting::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szProcSetFile );
		else strcpy_s( pszFile, szProcSetTempFile );
	}
	else strcpy_s( pszFile, szProcSetFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBProcSetting::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Velocity", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Differentiate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "RotationBeta", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "FilterFrequency", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Decimate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SamplingRate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "MinPenPressure", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DeviceResolution", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "TF_J", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TF_R", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TF_A", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TF_U", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TF_O", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_F", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_L", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_S", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_O", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_M", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EXT_S", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EXT_3", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EXT_O", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_A", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_N", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_M", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_U", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_C", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_S", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CON_O", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Randomize", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "RandomizeTrials", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_A", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_R", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_Z", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_T", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_S", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_D", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DiscardAfter", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ST_D2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DiscardAfter2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ShowInstr", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "minleftpenup", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "minleftsp", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "minwidth", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "mindownsp", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "mintimepenup", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "maxtimepenup", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "absdistmin", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "rdistmin", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "timemin", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "rvymin", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "rvymin2", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "initialpart", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "maxfreq", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "slanterror", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "spiralincreasefactor", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "minlooparean", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "straightcritstraight", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "straightcritcurved", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "DoQuestionnaire", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TimeoutStart", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "TimeoutRecord", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "TimeoutPenlift", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "TimeoutLatency", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "MaxRecArea", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ProcImmediately", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisplayCharts", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisplayRaw", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisplayTF", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisplaySeg", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Summarize", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SummarizeOnly", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SummarizeSelect", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Analyze", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ViewSubjExt", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ViewSubjCon", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ViewExpExt", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ViewExpCon", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "TrialMode", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_A", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "minreactiontime", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "maxreactiontime", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "SEG_V", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "discardLTmin", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "discardGTmax", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FFT", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "sharpness", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "SEG_MM", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SEG_J", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EXT_2", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "LineSize", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "EllipseSize", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "BGColor", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "LineColor1", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "LineColor2", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "LineColor3", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "EllipseColor", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "MPPColor", CT_INT4U, sizeof( CT_INT4U ) );
//		m_table->AddField( "CircleLLL", CT_INT4, sizeof( CT_INT4 ) );
//		m_table->AddField( "ThldRatioCircleSequence", CT_DFLOAT, sizeof( CT_DFLOAT ) );
//		m_table->AddField( "ThldHorizRelCircle", CT_DFLOAT, sizeof( CT_DFLOAT ) );
//		m_table->AddField( "ThldRatioLSequence", CT_DFLOAT, sizeof( CT_DFLOAT ) );
//		m_table->AddField( "ThldHorizRelL", CT_DFLOAT, sizeof( CT_DFLOAT ) );
//		m_table->AddField( "CircleIgnoreFirst", CT_INT4, sizeof( CT_INT4 ) );
//		m_table->AddField( "CircleIgnoreLast", CT_INT4, sizeof( CT_INT4 ) );
//		m_table->AddField( "LLLIgnoreFirst", CT_INT4, sizeof( CT_INT4 ) );
//		m_table->AddField( "LLLIgnoreLast", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisCorrection", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisFactor", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "DisZInsertPoint", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "DisRelError", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "DisAbsError", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "CON_D", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "VaryLineThickness", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "MaxLineThickness", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "RandWithRules", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "RandRules", CT_FSTRING, szRULES + 1 );
		m_table->AddField( "RandBlockCounts", CT_FSTRING, szRULES + 1 );
		m_table->AddField( "RandSelChars", CT_FSTRING, szRULES + 1 );
		m_table->AddField( "RandSelTrials", CT_FSTRING, szTRIALS + 1 );
		m_table->AddField( "UpSampleFactor", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "QuestAtEnd", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "FullScreen", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CollapseUDStrokes", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CollapseStrokes", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CollapseTrials", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CollapseMedian", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "UseTilt", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "MaxPenPressure", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisNotify", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DisError", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "SmoothingIter", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "InkThreshold", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ImgResolution", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "DiscardPenDownDurMin", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "PenDownDurMin", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "ExtTypeRaw", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ExtScriptRaw", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "ExtTypeTF", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ExtScriptTF", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "ExtTypeSeg", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ExtScriptSeg", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "ExtTypeExt", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ExtScriptExt", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "SpecifySequence", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "CondSeq", CT_FSTRING, szTRIALS + 1 );
		m_table->AddField( "remtraillift", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SummarizeOnlyGroup", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SummarizeNormDB", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "NormDBCalcScore", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "NormDBCalcScoreGlobal", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "NormDBNoUpdate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "NormDBThreshold", CT_DFLOAT, sizeof( CT_DFLOAT ) );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "ProcSetID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBProcSetting::GetData( pDBDATA pData, CString& szErr )
{
	pPROCSETTING p = (pPROCSETTING)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ExpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Velocity = m_record->GetFieldAsSigned( ++i );
		p->Differentiate = m_record->GetFieldAsSigned( ++i );
		p->RotationBeta = m_record->GetFieldAsFloat( ++i );
		p->FilterFrequency = m_record->GetFieldAsFloat( ++i );
		p->Decimate = m_record->GetFieldAsSigned( ++i );
		p->SamplingRate = m_record->GetFieldAsSigned( ++i );
		p->MinPenPressure = m_record->GetFieldAsSigned( ++i );
		p->DeviceResolution = m_record->GetFieldAsFloat( ++i );
		p->TF_J = m_record->GetFieldAsSigned( ++i );
		p->TF_R = m_record->GetFieldAsSigned( ++i );
		p->TF_A = m_record->GetFieldAsSigned( ++i );
		p->TF_U = m_record->GetFieldAsSigned( ++i );
		p->TF_O = m_record->GetFieldAsSigned( ++i );
		p->SEG_F = m_record->GetFieldAsSigned( ++i );
		p->SEG_L = m_record->GetFieldAsSigned( ++i );
		p->SEG_S = m_record->GetFieldAsSigned( ++i );
		p->SEG_O = m_record->GetFieldAsSigned( ++i );
		p->SEG_M = m_record->GetFieldAsSigned( ++i );
		p->EXT_S = m_record->GetFieldAsSigned( ++i );
		p->EXT_3 = m_record->GetFieldAsSigned( ++i );
		p->EXT_O = m_record->GetFieldAsSigned( ++i );
		p->CON_A = m_record->GetFieldAsSigned( ++i );
		p->CON_N = m_record->GetFieldAsSigned( ++i );
		p->CON_M = m_record->GetFieldAsSigned( ++i );
		p->CON_U = m_record->GetFieldAsSigned( ++i );
		p->CON_C = m_record->GetFieldAsSigned( ++i );
		p->CON_S = m_record->GetFieldAsSigned( ++i );
		p->CON_O = m_record->GetFieldAsSigned( ++i );
		p->Randomize = m_record->GetFieldAsSigned( ++i );
		p->RandomizeTrials = m_record->GetFieldAsSigned( ++i );
		p->ST_A = m_record->GetFieldAsSigned( ++i );
		p->ST_R = m_record->GetFieldAsSigned( ++i );
		p->ST_Z = m_record->GetFieldAsSigned( ++i );
		p->ST_T = m_record->GetFieldAsSigned( ++i );
		p->ST_S = m_record->GetFieldAsSigned( ++i );
		p->ST_D = m_record->GetFieldAsSigned( ++i );
		p->DiscardAfter = m_record->GetFieldAsSigned( ++i );
		p->ST_D2 = m_record->GetFieldAsSigned( ++i );
		p->DiscardAfter2 = m_record->GetFieldAsSigned( ++i );
		p->ShowInstr = m_record->GetFieldAsSigned( ++i );
		p->minleftpenup = m_record->GetFieldAsFloat( ++i );
		p->minleftsp = m_record->GetFieldAsFloat( ++i );
		p->minwidth = m_record->GetFieldAsFloat( ++i );
		p->mindownsp = m_record->GetFieldAsFloat( ++i );
		p->mintimepenup = m_record->GetFieldAsFloat( ++i );
		p->maxtimepenup = m_record->GetFieldAsFloat( ++i );
		p->absdistmin = m_record->GetFieldAsFloat( ++i );
		p->rdistmin = m_record->GetFieldAsFloat( ++i );
		p->timemin = m_record->GetFieldAsFloat( ++i );
		p->rvymin = m_record->GetFieldAsFloat( ++i );
		p->rvymin2 = m_record->GetFieldAsFloat( ++i );
		p->initialpart = m_record->GetFieldAsFloat( ++i );
		p->maxfreq = m_record->GetFieldAsFloat( ++i );
		p->slanterror = m_record->GetFieldAsFloat( ++i );
		p->spiralincreasefactor = m_record->GetFieldAsFloat( ++i );
		p->minlooparean = m_record->GetFieldAsFloat( ++i );
		p->straightcritstraight = m_record->GetFieldAsFloat( ++i );
		p->straightcritcurved = m_record->GetFieldAsFloat( ++i );
		p->DoQuestionnaire = m_record->GetFieldAsSigned( ++i );
		p->TimeoutStart = m_record->GetFieldAsFloat( ++i );
		p->TimeoutRecord = m_record->GetFieldAsFloat( ++i );
		p->TimeoutPenlift = m_record->GetFieldAsFloat( ++i );
		p->TimeoutLatency = m_record->GetFieldAsFloat( ++i );
		p->MaxRecArea = m_record->GetFieldAsSigned( ++i );
		p->ProcImmediately = m_record->GetFieldAsSigned( ++i );
		p->DisplayCharts = m_record->GetFieldAsSigned( ++i );
		p->DisplayRaw = m_record->GetFieldAsSigned( ++i );
		p->DisplayTF = m_record->GetFieldAsSigned( ++i );
		p->DisplaySeg = m_record->GetFieldAsSigned( ++i );
		p->Summarize = m_record->GetFieldAsSigned( ++i );
		p->SummarizeOnly = m_record->GetFieldAsSigned( ++i );
		p->SummarizeSelect = m_record->GetFieldAsSigned( ++i );
		p->Analyze = m_record->GetFieldAsSigned( ++i );
		p->ViewSubjExt = m_record->GetFieldAsSigned( ++i );
		p->ViewSubjCon = m_record->GetFieldAsSigned( ++i );
		p->ViewExpExt = m_record->GetFieldAsSigned( ++i );
		p->ViewExpCon = m_record->GetFieldAsSigned( ++i );
		p->TrialMode = m_record->GetFieldAsSigned( ++i );
		p->SEG_A = m_record->GetFieldAsSigned( ++i );
		p->minreactiontime = m_record->GetFieldAsFloat( ++i );
		p->maxreactiontime = m_record->GetFieldAsFloat( ++i );
		p->SEG_V = m_record->GetFieldAsSigned( ++i );
		p->discardLTmin = m_record->GetFieldAsSigned( ++i );
		p->discardGTmax = m_record->GetFieldAsSigned( ++i );
		p->FFT = m_record->GetFieldAsSigned( ++i );
		p->sharpness = m_record->GetFieldAsFloat( ++i );
		p->SEG_MM = m_record->GetFieldAsSigned( ++i );
		p->SEG_J = m_record->GetFieldAsSigned( ++i );
		p->EXT_2 = m_record->GetFieldAsSigned( ++i );
		p->LineSize = m_record->GetFieldAsSigned( ++i );
		p->EllipseSize = m_record->GetFieldAsSigned( ++i );
		p->BGColor = m_record->GetFieldAsUnsigned( ++i );
		p->LineColor1 = m_record->GetFieldAsUnsigned( ++i );
		p->LineColor2 = m_record->GetFieldAsUnsigned( ++i );
		p->LineColor3 = m_record->GetFieldAsUnsigned( ++i );
		p->EllipseColor = m_record->GetFieldAsUnsigned( ++i );
		p->MPPColor = m_record->GetFieldAsUnsigned( ++i );
//		p->CircleLLL = m_record->GetFieldAsSigned( ++i );
//		p->ThldRatioCircleSequence = m_record->GetFieldAsFloat( ++i );
//		p->ThldHorizRelCircle = m_record->GetFieldAsFloat( ++i );
//		p->ThldRatioLSequence = m_record->GetFieldAsFloat( ++i );
//		p->ThldHorizRelL = m_record->GetFieldAsFloat( ++i );
//		p->CircleIgnoreFirst = m_record->GetFieldAsSigned( ++i );
//		p->CircleIgnoreLast = m_record->GetFieldAsSigned( ++i );
//		p->LLLIgnoreFirst = m_record->GetFieldAsSigned( ++i );
//		p->LLLIgnoreLast = m_record->GetFieldAsSigned( ++i );
		p->DisCorrection = m_record->GetFieldAsSigned( ++i );
		p->DisFactor = m_record->GetFieldAsFloat( ++i );
		p->DisZInsertPoint = m_record->GetFieldAsFloat( ++i );
		p->DisRelError = m_record->GetFieldAsFloat( ++i );
		p->DisAbsError = m_record->GetFieldAsFloat( ++i );
		p->CON_D = m_record->GetFieldAsSigned( ++i );
		p->VaryLineThickness = m_record->GetFieldAsSigned( ++i );
		p->MaxLineThickness = m_record->GetFieldAsSigned( ++i );
		p->RandWithRules = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->RandRules, szRULES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->RandBlockCounts, szRULES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->RandSelChars, szRULES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->RandSelTrials, szTRIALS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->upsamplefactor = m_record->GetFieldAsSigned( ++i );
		p->QuestAtEnd = m_record->GetFieldAsSigned( ++i );
		p->FullScreen = m_record->GetFieldAsSigned( ++i );
		p->CollapseUDStrokes = m_record->GetFieldAsSigned( ++i );
		p->CollapseStrokes = m_record->GetFieldAsSigned( ++i );
		p->CollapseTrials = m_record->GetFieldAsSigned( ++i );
		p->CollapseMedian = m_record->GetFieldAsSigned( ++i );
		p->UseTilt = m_record->GetFieldAsSigned( ++i );
		p->MaxPenPressure = m_record->GetFieldAsSigned( ++i );
		p->DisNotify = m_record->GetFieldAsSigned( ++i );
		p->DisError = m_record->GetFieldAsFloat( ++i );
		p->SmoothingIter = m_record->GetFieldAsSigned( ++i );
		p->InkThreshold = m_record->GetFieldAsSigned( ++i );
		p->ImgResolution = m_record->GetFieldAsSigned( ++i );
		p->DiscardPenDownDurMin = m_record->GetFieldAsSigned( ++i );
		p->PenDownDurMin = m_record->GetFieldAsFloat( ++i );
		p->ExtTypeRaw = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->ExtScriptRaw, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->ExtTypeTF = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->ExtScriptTF, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->ExtTypeSeg = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->ExtScriptSeg, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->ExtTypeExt = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->ExtScriptExt, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->SpecifySequence = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->CondSeq, szTRIALS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->remtraillift = m_record->GetFieldAsSigned( ++i );
		p->SummarizeOnlyGroup = m_record->GetFieldAsSigned( ++i );
		p->SummarizeNormDB = m_record->GetFieldAsSigned( ++i );
		p->NormDBCalcScore = m_record->GetFieldAsSigned( ++i );
		p->NormDBCalcScoreGlobal = m_record->GetFieldAsSigned( ++i );
		p->NormDBNoUpdate = m_record->GetFieldAsSigned( ++i );
		p->NormDBThreshold = m_record->GetFieldAsFloat( ++i );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBProcSetting::SetData( pDBDATA pData, CString& szErr )
{
	pPROCSETTING p = (pPROCSETTING)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ExpID );
		m_record->SetFieldAsSigned( ++i, p->Velocity );
		m_record->SetFieldAsSigned( ++i, p->Differentiate );
		m_record->SetFieldAsFloat( ++i, p->RotationBeta );
		m_record->SetFieldAsFloat( ++i, p->FilterFrequency );
		m_record->SetFieldAsSigned( ++i, p->Decimate );
		m_record->SetFieldAsSigned( ++i, p->SamplingRate );
		m_record->SetFieldAsSigned( ++i, p->MinPenPressure );
		m_record->SetFieldAsFloat( ++i, p->DeviceResolution );
		m_record->SetFieldAsSigned( ++i, p->TF_J );
		m_record->SetFieldAsSigned( ++i, p->TF_R );
		m_record->SetFieldAsSigned( ++i, p->TF_A );
		m_record->SetFieldAsSigned( ++i, p->TF_U );
		m_record->SetFieldAsSigned( ++i, p->TF_O );
		m_record->SetFieldAsSigned( ++i, p->SEG_F );
		m_record->SetFieldAsSigned( ++i, p->SEG_L );
		m_record->SetFieldAsSigned( ++i, p->SEG_S );
		m_record->SetFieldAsSigned( ++i, p->SEG_O );
		m_record->SetFieldAsSigned( ++i, p->SEG_M );
		m_record->SetFieldAsSigned( ++i, p->EXT_S );
		m_record->SetFieldAsSigned( ++i, p->EXT_3 );
		m_record->SetFieldAsSigned( ++i, p->EXT_O );
		m_record->SetFieldAsSigned( ++i, p->CON_A );
		m_record->SetFieldAsSigned( ++i, p->CON_N );
		m_record->SetFieldAsSigned( ++i, p->CON_M );
		m_record->SetFieldAsSigned( ++i, p->CON_U );
		m_record->SetFieldAsSigned( ++i, p->CON_C );
		m_record->SetFieldAsSigned( ++i, p->CON_S );
		m_record->SetFieldAsSigned( ++i, p->CON_O );
		m_record->SetFieldAsSigned( ++i, p->Randomize );
		m_record->SetFieldAsSigned( ++i, p->RandomizeTrials );
		m_record->SetFieldAsSigned( ++i, p->ST_A );
		m_record->SetFieldAsSigned( ++i, p->ST_R );
		m_record->SetFieldAsSigned( ++i, p->ST_Z );
		m_record->SetFieldAsSigned( ++i, p->ST_T );
		m_record->SetFieldAsSigned( ++i, p->ST_S );
		m_record->SetFieldAsSigned( ++i, p->ST_D);
		m_record->SetFieldAsSigned( ++i, p->DiscardAfter );
		m_record->SetFieldAsSigned( ++i, p->ST_D2 );
		m_record->SetFieldAsSigned( ++i, p->DiscardAfter2 );
		m_record->SetFieldAsSigned( ++i, p->ShowInstr );
		m_record->SetFieldAsFloat( ++i, p->minleftpenup );
		m_record->SetFieldAsFloat( ++i, p->minleftsp );
		m_record->SetFieldAsFloat( ++i, p->minwidth );
		m_record->SetFieldAsFloat( ++i, p->mindownsp );
		m_record->SetFieldAsFloat( ++i, p->mintimepenup );
		m_record->SetFieldAsFloat( ++i, p->maxtimepenup );
		m_record->SetFieldAsFloat( ++i, p->absdistmin );
		m_record->SetFieldAsFloat( ++i, p->rdistmin );
		m_record->SetFieldAsFloat( ++i, p->timemin );
		m_record->SetFieldAsFloat( ++i, p->rvymin );
		m_record->SetFieldAsFloat( ++i, p->rvymin2 );
		m_record->SetFieldAsFloat( ++i, p->initialpart );
		m_record->SetFieldAsFloat( ++i, p->maxfreq );
		m_record->SetFieldAsFloat( ++i, p->slanterror );
		m_record->SetFieldAsFloat( ++i, p->spiralincreasefactor );
		m_record->SetFieldAsFloat( ++i, p->minlooparean );
		m_record->SetFieldAsFloat( ++i, p->straightcritstraight );
		m_record->SetFieldAsFloat( ++i, p->straightcritcurved );
		m_record->SetFieldAsSigned( ++i, p->DoQuestionnaire );
		m_record->SetFieldAsFloat( ++i, p->TimeoutStart );
		m_record->SetFieldAsFloat( ++i, p->TimeoutRecord );
		m_record->SetFieldAsFloat( ++i, p->TimeoutPenlift );
		m_record->SetFieldAsFloat( ++i, p->TimeoutLatency );
		m_record->SetFieldAsSigned( ++i, p->MaxRecArea );
		m_record->SetFieldAsSigned( ++i, p->ProcImmediately );
		m_record->SetFieldAsSigned( ++i, p->DisplayCharts );
		m_record->SetFieldAsSigned( ++i, p->DisplayRaw );
		m_record->SetFieldAsSigned( ++i, p->DisplayTF );
		m_record->SetFieldAsSigned( ++i, p->DisplaySeg );
		m_record->SetFieldAsSigned( ++i, p->Summarize );
		m_record->SetFieldAsSigned( ++i, p->SummarizeOnly );
		m_record->SetFieldAsSigned( ++i, p->SummarizeSelect );
		m_record->SetFieldAsSigned( ++i, p->Analyze );
		m_record->SetFieldAsSigned( ++i, p->ViewSubjExt );
		m_record->SetFieldAsSigned( ++i, p->ViewSubjCon );
		m_record->SetFieldAsSigned( ++i, p->ViewExpExt );
		m_record->SetFieldAsSigned( ++i, p->ViewExpCon );
		m_record->SetFieldAsSigned( ++i, p->TrialMode );
		m_record->SetFieldAsSigned( ++i, p->SEG_A );
		m_record->SetFieldAsFloat( ++i, p->minreactiontime );
		m_record->SetFieldAsFloat( ++i, p->maxreactiontime );
		m_record->SetFieldAsSigned( ++i, p->SEG_V );
		m_record->SetFieldAsSigned( ++i, p->discardLTmin );
		m_record->SetFieldAsSigned( ++i, p->discardGTmax );
		m_record->SetFieldAsSigned( ++i, p->FFT );
		m_record->SetFieldAsFloat( ++i, p->sharpness );
		m_record->SetFieldAsSigned( ++i, p->SEG_MM );
		m_record->SetFieldAsSigned( ++i, p->SEG_J );
		m_record->SetFieldAsSigned( ++i, p->EXT_2 );
		m_record->SetFieldAsSigned( ++i, p->LineSize );
		m_record->SetFieldAsSigned( ++i, p->EllipseSize );
		m_record->SetFieldAsUnsigned( ++i, p->BGColor );
		m_record->SetFieldAsUnsigned( ++i, p->LineColor1 );
		m_record->SetFieldAsUnsigned( ++i, p->LineColor2 );
		m_record->SetFieldAsUnsigned( ++i, p->LineColor3 );
		m_record->SetFieldAsUnsigned( ++i, p->EllipseColor );
		m_record->SetFieldAsUnsigned( ++i, p->MPPColor );
//		m_record->SetFieldAsSigned( ++i, p->CircleLLL );
//		m_record->SetFieldAsFloat( ++i, p->ThldRatioCircleSequence );
//		m_record->SetFieldAsFloat( ++i, p->ThldHorizRelCircle );
//		m_record->SetFieldAsFloat( ++i, p->ThldRatioLSequence );
//		m_record->SetFieldAsFloat( ++i, p->ThldHorizRelL );
//		m_record->SetFieldAsSigned( ++i, p->CircleIgnoreFirst );
//		m_record->SetFieldAsSigned( ++i, p->CircleIgnoreLast );
//		m_record->SetFieldAsSigned( ++i, p->LLLIgnoreFirst );
//		m_record->SetFieldAsSigned( ++i, p->LLLIgnoreLast );
		m_record->SetFieldAsSigned( ++i, p->DisCorrection );
		m_record->SetFieldAsFloat( ++i, p->DisFactor );
		m_record->SetFieldAsFloat( ++i, p->DisZInsertPoint );
		m_record->SetFieldAsFloat( ++i, p->DisRelError );
		m_record->SetFieldAsFloat( ++i, p->DisAbsError );
		m_record->SetFieldAsSigned( ++i, p->CON_D );
		m_record->SetFieldAsSigned( ++i, p->VaryLineThickness );
		m_record->SetFieldAsSigned( ++i, p->MaxLineThickness );
		m_record->SetFieldAsSigned( ++i, p->RandWithRules );
		m_record->SetFieldAsString( ++i, p->RandRules );
		m_record->SetFieldAsString( ++i, p->RandBlockCounts );
		m_record->SetFieldAsString( ++i, p->RandSelChars );
		m_record->SetFieldAsString( ++i, p->RandSelTrials );
		m_record->SetFieldAsSigned( ++i, p->upsamplefactor );
		m_record->SetFieldAsSigned( ++i, p->QuestAtEnd );
		m_record->SetFieldAsSigned( ++i, p->FullScreen );
		m_record->SetFieldAsSigned( ++i, p->CollapseUDStrokes );
		m_record->SetFieldAsSigned( ++i, p->CollapseStrokes );
		m_record->SetFieldAsSigned( ++i, p->CollapseTrials );
		m_record->SetFieldAsSigned( ++i, p->CollapseMedian );
		m_record->SetFieldAsSigned( ++i, p->UseTilt );
		m_record->SetFieldAsSigned( ++i, p->MaxPenPressure );
		m_record->SetFieldAsSigned( ++i, p->DisNotify );
		m_record->SetFieldAsFloat( ++i, p->DisError );
		m_record->SetFieldAsSigned( ++i, p->SmoothingIter );
		m_record->SetFieldAsSigned( ++i, p->InkThreshold );
		m_record->SetFieldAsSigned( ++i, p->ImgResolution );
		m_record->SetFieldAsSigned( ++i, p->DiscardPenDownDurMin );
		m_record->SetFieldAsFloat( ++i, p->PenDownDurMin );
		m_record->SetFieldAsSigned( ++i, p->ExtTypeRaw );
		m_record->SetFieldAsString( ++i, p->ExtScriptRaw );
		m_record->SetFieldAsSigned( ++i, p->ExtTypeTF );
		m_record->SetFieldAsString( ++i, p->ExtScriptTF );
		m_record->SetFieldAsSigned( ++i, p->ExtTypeSeg );
		m_record->SetFieldAsString( ++i, p->ExtScriptSeg );
		m_record->SetFieldAsSigned( ++i, p->ExtTypeExt );
		m_record->SetFieldAsString( ++i, p->ExtScriptExt );
		m_record->SetFieldAsSigned( ++i, p->SpecifySequence );
		m_record->SetFieldAsString( ++i, p->CondSeq );
		m_record->SetFieldAsSigned( ++i, p->remtraillift );
		m_record->SetFieldAsSigned( ++i, p->SummarizeOnlyGroup );
		m_record->SetFieldAsSigned( ++i, p->SummarizeNormDB );
		m_record->SetFieldAsSigned( ++i, p->NormDBCalcScore );
		m_record->SetFieldAsSigned( ++i, p->NormDBCalcScoreGlobal );
		m_record->SetFieldAsSigned( ++i, p->NormDBNoUpdate );
		m_record->SetFieldAsFloat( ++i, p->NormDBThreshold );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pPROCSETTING DBProcSetting::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pPROCSETTING p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &procset, szErr );
			p = &procset;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBProcSetting::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBProcSetting::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f300 = false;
	bool f330 = false;
	bool f393 = false;
	bool f400 = false;
	bool f401 = false;	// internal only
	bool f420 = false;
	bool f430 = false;
	bool f460 = false;
	bool f470 = false;
	bool f480 = false;
	bool f490 = false;
	bool f510 = false;
	bool f511 = false;	// internal only
	bool f540 = false;
	bool f560 = false;
	bool f570 = false;
	bool f571 = false;	// internal only
	bool f610 = false;

	// version 3.00 new
	try {
		m_table->GetField( _T("minreactiontime") );
	}
	catch( ... ) {
		fMod = true;
		f300 = true;
	}

	// version 3.30
	if( !fMod ) {
		try {
			m_table->GetField( _T("SEG_J") );
		}
		catch( ... ) {
			fMod = true;
			f330 = true;
		}
	}

	// version 3.93
	if( !fMod ) {
		try {
			m_table->GetField( _T("EXT_2") );
		}
		catch( ... ) {
			fMod = true;
			f393 = true;
		}
	}

	// version 4.00
	if( !fMod ) {
		try {
			m_table->GetField( _T("LineSize") );
		}
		catch( ... ) {
			fMod = true;
			f400 = true;
		}
	}

	// version 4.01 (4.00 not released but coding done, then these are needed -- internal version)
	if( !fMod ) {
		try {
			m_table->GetField( _T("DisCorrection") );
		}
		catch( ... ) {
			fMod = true;
			f401 = true;
		}
	}

	// version 4.20
	if( !fMod ) {
		try {
			m_table->GetField( _T("CON_D") );
		}
		catch( ... ) {
			fMod = true;
			f420 = true;
		}
	}

	// version 4.30
	if( !fMod ) {
		try {
			m_table->GetField( _T("VaryLineThickness") );
		}
		catch( ... ) {
			fMod = true;
			f430 = true;
		}
	}

	// version 4.60
	if( !fMod ) {
		try {
			m_table->GetField( _T("RandWithRules") );
		}
		catch( ... ) {
			fMod = true;
			f460 = true;
		}
	}

	// version 4.70
	if( !fMod ) {
		try {
			m_table->GetField( _T("UpSampleFactor") );
		}
		catch( ... ) {
			fMod = true;
			f470 = true;
		}
	}

	// version 4.80
	if( !fMod ) {
		try {
			m_table->GetField( _T("CollapseStrokes") );
		}
		catch( ... ) {
			fMod = true;
			f480 = true;
		}
	}

	// version 4.90
	if( !fMod ) {
		try {
			m_table->GetField( _T("UseTilt") );
		}
		catch( ... ) {
			fMod = true;
			f490 = true;
		}
	}

	// version 5.10
	if( !fMod ) {
		try {
			m_table->GetField( _T("MaxPenPressure") );
		}
		catch( ... ) {
			fMod = true;
			f510 = true;
		}
	}

	// version 5.11 (internal version -- pre-5.10 release)
	if( !fMod ) {
		try {
			m_table->GetField( _T("SmoothingIter") );
		}
		catch( ... ) {
			fMod = true;
			f511 = true;
		}
	}

	// version 5.40
	if( !fMod ) {
		try {
			m_table->GetField( _T("DiscardPenDownDurMin") );
		}
		catch( ... ) {
			fMod = true;
			f540 = true;
		}
	}

	// version 5.60
	if( !fMod ) {
		try {
			m_table->GetField( _T("ExtTypeRaw") );
		}
		catch( ... ) {
			fMod = true;
			f560 = true;
		}
	}

	// version 5.70
	if( !fMod ) {
		try {
			m_table->GetField( _T("SpecifySequence") );
		}
		catch( ... ) {
			fMod = true;
			f570 = true;
		}
	}

	// version 5.71 (internal)
	if( !fMod ) {
		try {
			m_table->GetField( _T("remtraillift") );
		}
		catch( ... ) {
			fMod = true;
			f571 = true;
		}
	}

	// version 6.10
	if( !fMod ) {
		try {
			m_table->GetField( _T("SummarizeOnlyGroup") );
		}
		catch( ... ) {
			fMod = true;
			f610 = true;
		}
	}
	// version 6.20
	// No change

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 3.00
		if( f300 )
		{
			m_table->AddField( "minreactiontime", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			m_table->AddField( "maxreactiontime", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			m_table->AddField( "SEG_V", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "discardLTmin", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "discardGTmax", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "FFT", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "sharpness", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		}
		// version 3.30
		if( f300 || f330 )
		{
			m_table->AddField( "SEG_MM", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "SEG_J", CT_INT4, sizeof( CT_INT4 ) );
		}
		// version 3.93
		if( f300 || f330 || f393 )
		{
			m_table->AddField( "EXT_2", CT_INT4, sizeof( CT_INT4 ) );
		}
		// version 4.00
		if( f300 || f330 || f393 || f400 )
		{
			CTField fld = m_table->AddField( "LineSize", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( "2", 1 );
			fld = m_table->AddField( "EllipseSize", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( "2", 1 );
			fld = m_table->AddField( "BGColor", CT_INT4U, sizeof( CT_INT4U ) );
			str.Format( _T("%u"), RGB( 255, 255, 255 ) );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "LineColor1", CT_INT4U, sizeof( CT_INT4U ) );
			str.Format( _T("%u"), RGB( 0, 0, 255 ) );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "LineColor2", CT_INT4U, sizeof( CT_INT4U ) );
			str.Format( _T("%u"), RGB( 0, 255, 0 ) );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "LineColor3", CT_INT4U, sizeof( CT_INT4U ) );
			str.Format( _T("%u"), RGB( 255, 0, 0 ) );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "EllipseColor", CT_INT4U, sizeof( CT_INT4U ) );
			str.Format( _T("%u"), RGB( 255, 0, 0 ) );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "MPPColor", CT_INT4U, sizeof( CT_INT4U ) );
// 16Apr09: GMB: Changed default penup color
			str.Format( _T("%u"), RGB( 224, 224, 224 ) );
//			str.Format( _T("%u"), RGB( 229, 240, 247 ) );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// version 4.01 (internal version)
		if( f300 || f330 || f393 || f400 || f401 )
		{
			CTField fld = m_table->AddField( "DisCorrection", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "DisFactor", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 0.2 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "DisZInsertPoint", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), -1.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "DisRelError", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 20.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "DisAbsError", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 100.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// version 4.20
		if( f300 || f330 || f393 || f400 || f401 || f420 )
		{
			CTField fld = m_table->AddField( "CON_D", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// version 4.30
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 )
		{
			CTField fld = m_table->AddField( "VaryLineThickness", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "MaxLineThickness", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 20 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// version 4.60
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 )
		{
			CTField fld = m_table->AddField( "RandWithRules", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "RandRules", CT_FSTRING, szRULES + 1 );
			m_table->AddField( "RandBlockCounts", CT_FSTRING, szRULES + 1 );
			m_table->AddField( "RandSelChars", CT_FSTRING, szRULES + 1 );
			m_table->AddField( "RandSelTrials", CT_FSTRING, szTRIALS + 1 );
		}

		// version 4.70
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 )
		{
			CTField fld = m_table->AddField( "UpSampleFactor", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "QuestAtEnd", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "FullScreen", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// version 4.80
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 || f480 )
		{
			CTField fld = m_table->AddField( "CollapseUDStrokes", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "CollapseStrokes", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "CollapseTrials", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "CollapseMedian", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 4.90
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 || f480 || f490 )
		{
			CTField fld = m_table->AddField( "UseTilt", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 5.10
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 || f480 || f490 || f510 )
		{
			CTField fld = m_table->AddField( "MaxPenPressure", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1013 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "DisNotify", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "DisError", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 0.3 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 5.10
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 ||
			f480 || f490 || f510 || f511 )
		{
			str.Format( _T("%d"), 0 );
			tstr = str;

			CTField fld = m_table->AddField( "SmoothingIter", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "InkThreshold", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "ImgResolution", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
		}
		// version 5.40
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 ||
			f480 || f490 || f510 || f511 || f540 )
		{
			CTField fld = m_table->AddField( "DiscardPenDownDurMin", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "PenDownDurMin", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 0.0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 5.60
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 ||
			f480 || f490 || f510 || f511 || f540 || f560 )
		{
			CTField fld = m_table->AddField( "ExtTypeRaw", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "ExtScriptRaw", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "ExtTypeTF", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "ExtScriptTF", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "ExtTypeSeg", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "ExtScriptSeg", CT_FSTRING, szFILELENGTH + 1 );
			fld = m_table->AddField( "ExtTypeExt", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "ExtScriptExt", CT_FSTRING, szFILELENGTH + 1 );
		}
		// version 5.70
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 ||
			f480 || f490 || f510 || f511 || f540 || f560 || f570 )
		{
			CTField fld = m_table->AddField( "SpecifySequence", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "CondSeq", CT_FSTRING, szTRIALS + 1 );
		}
		// version 5.71
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 ||
			f480 || f490 || f510 || f511 || f540 || f560 || f570 || f571 )
		{
			CTField fld = m_table->AddField( "remtraillift", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 6.10
		if( f300 || f330 || f393 || f400 || f401 || f420 || f430 || f460 || f470 ||
			f480 || f490 || f510 || f511 || f540 || f560 || f570 || f571 || f610 )
		{
			CTField fld = m_table->AddField( "SummarizeOnlyGroup", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 1 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "SummarizeNormDB", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "NormDBCalcScore", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "NormDBCalcScoreGlobal", CT_INT4, sizeof( CT_INT4 ) );
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "NormDBNoUpdate", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), 0 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "NormDBThreshold", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), 1. );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}
		// version 6.20
		// No change

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_table->Alter( CTDB_ALTER_NORMAL );
			m_fConverted = TRUE;
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

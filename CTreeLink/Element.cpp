#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBElement.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szElementFile[ _MAX_PATH ] = _T("element");
static char szElementTempFile[ _MAX_PATH ] = _T("elementtemp");

// global data object
ELEMENT element;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBElement::GetObjectName( CString& szName )
{
	szName = _T("element");
}

///////////////////////////////////////////////////////////////////////////////

void DBElement::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szElementFile );
		else strcpy_s( pszFile, szElementTempFile );
	}
	else strcpy_s( pszFile, szElementFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBElement::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Description", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Pattern", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "Shape", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "CenterX", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "CenterY", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "WidthX", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "WidthY", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "IsTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ErrorX", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "ErrorY", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Line1Size", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "Line2Size", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "Line3Size", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "Line1Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Line2Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Line3Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Bk1Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Bk2Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Bk3Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Text1", CT_FSTRING, szELEMENT_TEXT + 1 );
		m_table->AddField( "Text2", CT_FSTRING, szELEMENT_TEXT + 1 );
		m_table->AddField( "Text3", CT_FSTRING, szELEMENT_TEXT + 1 );
		m_table->AddField( "Text1Size", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "Text2Size", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "Text3Size", CT_INT2, sizeof( CT_INT2 ) );
		m_table->AddField( "Text1Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Text2Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "Text3Color", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "lf1", CT_BINARY, sizeof( LOGFONT ) );
		m_table->AddField( "lf2", CT_BINARY, sizeof( LOGFONT ) );
		m_table->AddField( "lf3", CT_BINARY, sizeof( LOGFONT ) );
		m_table->AddField( "IsImage", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Start", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "Duration", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "HideRightTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideWrongTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideIfAnyRightTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "HideIfAnyWrongTarget", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "RightTarget", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "WrongTarget", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "CatID", CT_FSTRING, szELEMENT_TEXT + 1 );
		m_table->AddField( "Animate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "AnimationFile", CT_FSTRING, szFILELENGTH + 1 );
		m_table->AddField( "AnimationRandom", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "AnimationGenerate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "AnimationSubjID", CT_FSTRING, szIDS + szIDS + szIDS + 1 );
		m_table->AddField( "AnimationRate", CT_DFLOAT, sizeof( CT_DFLOAT ) );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "ElmtID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBElement::GetData( pDBDATA pData, CString& szErr )
{
	pELEMENT p = (pELEMENT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ElmtID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Desc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Pattern, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Shape = m_record->GetFieldAsShort( ++i );
		p->CenterX = m_record->GetFieldAsFloat( ++i );;
		p->CenterY = m_record->GetFieldAsFloat( ++i );;
		p->WidthX = m_record->GetFieldAsFloat( ++i );;
		p->WidthY = m_record->GetFieldAsFloat( ++i );;
		p->IsTarget = m_record->GetFieldAsSigned( ++i );
		p->ErrorX = m_record->GetFieldAsFloat( ++i );;
		p->ErrorY = m_record->GetFieldAsFloat( ++i );;
		p->Line1Size = m_record->GetFieldAsShort( ++i );
		p->Line2Size = m_record->GetFieldAsShort( ++i );
		p->Line3Size = m_record->GetFieldAsShort( ++i );
		p->Line1Color = m_record->GetFieldAsUnsigned( ++i );
		p->Line2Color = m_record->GetFieldAsUnsigned( ++i );
		p->Line3Color = m_record->GetFieldAsUnsigned( ++i );
		p->Bk1Color = m_record->GetFieldAsUnsigned( ++i );
		p->Bk2Color = m_record->GetFieldAsUnsigned( ++i );
		p->Bk3Color = m_record->GetFieldAsUnsigned( ++i );
		strncpy_s( p->Text1, szELEMENT_TEXT + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Text2, szELEMENT_TEXT + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Text3, szELEMENT_TEXT + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Text1Size = m_record->GetFieldAsShort( ++i );
		p->Text2Size = m_record->GetFieldAsShort( ++i );
		p->Text3Size = m_record->GetFieldAsShort( ++i );
		p->Text1Color = m_record->GetFieldAsUnsigned( ++i );
		p->Text2Color = m_record->GetFieldAsUnsigned( ++i );
		p->Text3Color = m_record->GetFieldAsUnsigned( ++i );

		ctdbGetFieldAsBinary( m_record->GetHandle(), ++i, (LPVOID)&p->lf1, sizeof( LOGFONT ) );
		ctdbGetFieldAsBinary( m_record->GetHandle(), ++i, (LPVOID)&p->lf2, sizeof( LOGFONT ) );
		ctdbGetFieldAsBinary( m_record->GetHandle(), ++i, (LPVOID)&p->lf3, sizeof( LOGFONT ) );

		p->IsImage = m_record->GetFieldAsSigned( ++i );
		p->Start = m_record->GetFieldAsFloat( ++i );;
		p->Duration = m_record->GetFieldAsFloat( ++i );;
		p->HideRightTarget = m_record->GetFieldAsSigned( ++i );
		p->HideWrongTarget = m_record->GetFieldAsSigned( ++i );
		p->HideIfAnyRightTarget = m_record->GetFieldAsSigned( ++i );
		p->HideIfAnyWrongTarget = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->RightTarget, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->WrongTarget, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->CatID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Animate = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->AnimationFile, szFILELENGTH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->AnimationRandom = m_record->GetFieldAsSigned( ++i );
		p->AnimationGenerate = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->AnimationSubjID, szIDS + szIDS + szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->AnimationRate = m_record->GetFieldAsFloat( ++i );;

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBElement::SetData( pDBDATA pData, CString& szErr )
{
	pELEMENT p = (pELEMENT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ElmtID );
		m_record->SetFieldAsString( ++i, p->Desc );
		m_record->SetFieldAsString( ++i, p->Pattern );
		m_record->SetFieldAsShort( ++i, p->Shape );
		m_record->SetFieldAsFloat( ++i, p->CenterX );
		m_record->SetFieldAsFloat( ++i, p->CenterY );
		m_record->SetFieldAsFloat( ++i, p->WidthX );
		m_record->SetFieldAsFloat( ++i, p->WidthY );
		m_record->SetFieldAsSigned( ++i, p->IsTarget );
		m_record->SetFieldAsFloat( ++i, p->ErrorX );
		m_record->SetFieldAsFloat( ++i, p->ErrorY );
		m_record->SetFieldAsShort( ++i, p->Line1Size );
		m_record->SetFieldAsShort( ++i, p->Line2Size );
		m_record->SetFieldAsShort( ++i, p->Line3Size );
		m_record->SetFieldAsUnsigned( ++i, p->Line1Color );
		m_record->SetFieldAsUnsigned( ++i, p->Line2Color );
		m_record->SetFieldAsUnsigned( ++i, p->Line3Color );
		m_record->SetFieldAsUnsigned( ++i, p->Bk1Color );
		m_record->SetFieldAsUnsigned( ++i, p->Bk2Color );
		m_record->SetFieldAsUnsigned( ++i, p->Bk3Color );
		m_record->SetFieldAsString( ++i, p->Text1 );
		m_record->SetFieldAsString( ++i, p->Text2 );
		m_record->SetFieldAsString( ++i, p->Text3 );
		m_record->SetFieldAsShort( ++i, p->Text1Size );
		m_record->SetFieldAsShort( ++i, p->Text2Size );
		m_record->SetFieldAsShort( ++i, p->Text3Size );
		m_record->SetFieldAsUnsigned( ++i, p->Text1Color );
		m_record->SetFieldAsUnsigned( ++i, p->Text2Color );
		m_record->SetFieldAsUnsigned( ++i, p->Text3Color );

		ctdbSetFieldAsBinary( m_record->GetHandle(), ++i, (LPVOID)&p->lf1, sizeof( LOGFONT ) );
		ctdbSetFieldAsBinary( m_record->GetHandle(), ++i, (LPVOID)&p->lf2, sizeof( LOGFONT ) );
		ctdbSetFieldAsBinary( m_record->GetHandle(), ++i, (LPVOID)&p->lf3, sizeof( LOGFONT ) );

		m_record->SetFieldAsSigned( ++i, p->IsImage );
		m_record->SetFieldAsFloat( ++i, p->Start );
		m_record->SetFieldAsFloat( ++i, p->Duration );
		m_record->SetFieldAsSigned( ++i, p->HideRightTarget );
		m_record->SetFieldAsSigned( ++i, p->HideWrongTarget );
		m_record->SetFieldAsSigned( ++i, p->HideIfAnyRightTarget );
		m_record->SetFieldAsSigned( ++i, p->HideIfAnyWrongTarget );
		m_record->SetFieldAsString( ++i, p->RightTarget );
		m_record->SetFieldAsString( ++i, p->WrongTarget );
		m_record->SetFieldAsString( ++i, p->CatID );
		m_record->SetFieldAsSigned( ++i, p->Animate );
		m_record->SetFieldAsString( ++i, p->AnimationFile );
		m_record->SetFieldAsSigned( ++i, p->AnimationRandom );
		m_record->SetFieldAsSigned( ++i, p->AnimationGenerate );
		m_record->SetFieldAsString( ++i, p->AnimationSubjID );
		m_record->SetFieldAsFloat( ++i, p->AnimationRate );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pELEMENT DBElement::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pELEMENT p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &element, szErr );
			p = &element;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBElement::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBElement::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool fDoSize = false;

	// check for field size of old var (everyone should have) - 3 text fields, just check 1
	CTField fld = m_table->GetField( "Text1" );
	if( fld.GetLength() != ( szELEMENT_TEXT + 1 ) )
	{
		fMod = true;
		fDoSize = true;
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// element text size changes
		if( fDoSize )
		{
			fld = m_table->GetField( "Text1" );
			fld.SetLength( szELEMENT_TEXT + 1 );
			fld = m_table->GetField( "Text2" );
			fld.SetLength( szELEMENT_TEXT + 1 );
			fld = m_table->GetField( "Text3" );
			fld.SetLength( szELEMENT_TEXT + 1 );
		}

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

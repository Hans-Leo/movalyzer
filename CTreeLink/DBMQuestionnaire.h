#ifndef __DB_MQUESTIONNAIRE__
#define	__DB_MQUESTIONNAIRE__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// MQuestionnaire
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct MQUESTIONNAIRE : public DBDATA
{
	FLD_char( ID, szIDS );
	FLD_char( ItemNum, szQUESTIONNAIRE_ITEM );
	FLD_BOOL( Header );
	FLD_BOOL( Private );
	FLD_char( Question, szQUESTIONNAIRE_Q );
	FLD_BOOL( Numeric );
	FLD_char( ColHeader, szCODE );
};

typedef MQUESTIONNAIRE* pMQUESTIONNAIRE;

// DB class
class AFX_EXT_CLASS DBMQuestionnaire : public DBData
{
public:
	DBMQuestionnaire() {}
	~DBMQuestionnaire() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pMQUESTIONNAIRE Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBMQuestionnaire* pDBMQuestionnaire;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_MQUESTIONNAIRE__

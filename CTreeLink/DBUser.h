#ifndef __DB_USER__
#define	__DB_USER__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// User
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct USER : public DBDATA
{
	FLD_char( UserID, szIDS );
	FLD_char( Desc, szUSER_DESC );
	FLD_char( RootPath, MAX_PATH );
	FLD_char( BackupPath, MAX_PATH );
	FLD_char( Delim, szUSER_DELIM );
	FLD_INT( InputType );
	FLD_INT( TabletMode );
	FLD_BOOL( EntireTablet );
	FLD_BOOL( Log );
	FLD_BOOL( LogUI );
	FLD_BOOL( LogDB );
	FLD_BOOL( LogProc );
	FLD_BOOL( LogGraph );
	FLD_BOOL( LogTablet );
	FLD_BOOL( Alpha );
	FLD_char( Pass, szUSER_PASS_ENC );
	FLD_DOUBLE( TabletWidth );
	FLD_DOUBLE( TabletHeight );
	FLD_DOUBLE( DisplayWidth );
	FLD_DOUBLE( DisplayHeight );
	FLD_DOUBLE( AspectRatioWidth );
	FLD_DOUBLE( AspectRatioHeight );
	FLD_char( CommPort, szCOMM_PORT );
	FLD_BOOL( AutoUpdate );
	FLD_BOOL( Private );
	FLD_char( WinUser, szDESC + 1 );
	FLD_BOOL( GenSubjID );
	FLD_char( SubjStartID, szIDS );
	FLD_char( SubjEndID, szIDS );
	FLD_char( SubjCurID, szIDS );
	FLD_BOOL( Encrypted );			// has this data been encrypted yet?
	FLD_INT( EncryptionMethod );
	FLD_char( SiteID, szCODE );				// (optional) identifier for where was created
	FLD_char( SiteDesc, szDESC );			// (optional) desc. for where was created
	FLD_char( Signature, szSIGNATURE );		// unique identifier for user
};

typedef USER* pUSER;

// User DB class
class AFX_EXT_CLASS DBUser : public DBData
{
public:
	DBUser() {}
	~DBUser() {}

// overrides
protected:
	// obtain pointer to current session
	virtual CTSession* GetSession( CString& szErr, int nForceMode )
	{ return DBData::GetSession( szErr, CTOP_LOCAL ); }	// user must always be local
	virtual void ForceOperationMode( short nMode ) {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pUSER Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );

// table-specific functions
public:
	virtual char* GetDataPath();
	virtual void SetDataPath( CString szPath );
};

typedef DBUser* pDBUser;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_USER__

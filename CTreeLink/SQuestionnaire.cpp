#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBSQuestionnaire.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szSQuestFile[ _MAX_PATH ] = _T("squest");
static char szSQuestTempFile[ _MAX_PATH ] = _T("squesttemp");

// global data object
SQUESTIONNAIRE squest;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBSQuestionnaire::GetObjectName( CString& szName )
{
	szName = _T("squest");
}

///////////////////////////////////////////////////////////////////////////////

void DBSQuestionnaire::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szSQuestFile );
		else strcpy_s( pszFile, szSQuestTempFile );
	}
	else strcpy_s( pszFile, szSQuestFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ExpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx2 = m_table->AddField( "GrpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx3 = m_table->AddField( "SubjID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx4 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Answer", CT_FSTRING, szQUESTIONNAIRE_A + 1 );

		// add indices
		// exp only
		CTIndex idx1 = m_table->AddIndex( "Exp", CTINDEX_FIXED, YES, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
		// exp/grp
		CTIndex idx2 = m_table->AddIndex( "ExpGrp", CTINDEX_FIXED, YES, NO );
		idx2.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx2.AddSegment( fldIdx2, CTSEG_UREGSEG );
		// exp/grp/subj
		CTIndex idx3 = m_table->AddIndex( "ExpGrpSubj", CTINDEX_FIXED, YES, NO );
		idx3.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx3.AddSegment( fldIdx2, CTSEG_UREGSEG );
		idx3.AddSegment( fldIdx3, CTSEG_UREGSEG );
		// exp/grp/subj/quest
		CTIndex idx4 = m_table->AddIndex( "ExpGrpSubjQuest", CTINDEX_FIXED, NO, NO );
		idx4.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx4.AddSegment( fldIdx2, CTSEG_UREGSEG );
		idx4.AddSegment( fldIdx3, CTSEG_UREGSEG );
		idx4.AddSegment( fldIdx4, CTSEG_UREGSEG );
		// grp
		CTIndex idx5 = m_table->AddIndex( "Grp", CTINDEX_FIXED, YES, NO );
		idx5.AddSegment( fldIdx2, CTSEG_UREGSEG );
		// subj
		CTIndex idx6 = m_table->AddIndex( "Subj", CTINDEX_FIXED, YES, NO );
		idx6.AddSegment( fldIdx3, CTSEG_UREGSEG );
		// quest
		CTIndex idx7 = m_table->AddIndex( "Quest", CTINDEX_FIXED, YES, NO );
		idx7.AddSegment( fldIdx4, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::GetData( pDBDATA pData, CString& szErr )
{
	pSQUESTIONNAIRE p = (pSQUESTIONNAIRE)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ExpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->GrpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->SubjID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->ID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Answer, szQUESTIONNAIRE_A + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::SetData( pDBDATA pData, CString& szErr )
{
	pSQUESTIONNAIRE p = (pSQUESTIONNAIRE)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ExpID );
		m_record->SetFieldAsString( ++i, p->GrpID );
		m_record->SetFieldAsString( ++i, p->SubjID );
		m_record->SetFieldAsString( ++i, p->ID );
		m_record->SetFieldAsString( ++i, p->Answer );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pSQUESTIONNAIRE DBSQuestionnaire::Find( CString szExpID, CString szGrpID, CString szSubjID, CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSQUESTIONNAIRE p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ], sid[ szIDS + 1 ], id[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';
	strncpy_s( sid, szSubjID, szIDS );
	sid[ szIDS ] = '\0';
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 3 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->SetFieldAsString( 3, sid );
		m_record->SetFieldAsString( 4, id );
		m_record->RecordSetOn( ( szIDS + 1 ) * 4 );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &squest, szErr );
			p = &squest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pSQUESTIONNAIRE DBSQuestionnaire::Find( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSQUESTIONNAIRE p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ], sid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';
	strncpy_s( sid, szSubjID, szIDS );
	sid[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 2 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->SetFieldAsString( 3, sid );
		m_record->RecordSetOn( ( szIDS + 1 ) * 3 );
		// search
		if( m_record->First() )
		{
			GetData( &squest, szErr );
			p = &squest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pSQUESTIONNAIRE DBSQuestionnaire::Find( CString szExpID, CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSQUESTIONNAIRE p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 1 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->RecordSetOn( ( szIDS + 1 ) * 2 );
		// search
		if( m_record->First() )
		{
			GetData( &squest, szErr );
			p = &squest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pSQUESTIONNAIRE DBSQuestionnaire::FindForGroup( CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szGrpID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 4 );
		// set search criteria
		m_record->SetFieldAsString( 2, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &squest, szErr );
			p = &squest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pSQUESTIONNAIRE DBSQuestionnaire::FindForSubject( CString szSubjID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szSubjID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 5 );
		// set search criteria
		m_record->SetFieldAsString( 3, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &squest, szErr );
			p = &squest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pSQUESTIONNAIRE DBSQuestionnaire::FindForQuestion( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 6 );
		// set search criteria
		m_record->SetFieldAsString( 3, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &squest, szErr );
			p = &squest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::Remove( CString szExpID, CString szGrpID, CString szSubjID, CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szGrpID, szSubjID, szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::Remove( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr )
{
	return ClearQuestionnaire( szExpID, szGrpID, szSubjID, szErr );
}

BOOL DBSQuestionnaire::ClearQuestionnaire( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szGrpID, szSubjID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::RemoveGroup( CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( FindForGroup( szGrpID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::RemoveSubject( CString szSubjID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( FindForSubject( szSubjID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSQuestionnaire::RemoveQuestion( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( FindForQuestion( szID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

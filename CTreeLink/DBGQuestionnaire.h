#ifndef __DB_GQUESTIONNAIRE__
#define	__DB_GQUESTIONNAIRE__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// GQuestionnaire
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct GQUESTIONNAIRE : public DBDATA
{
	FLD_char( ExpID, szIDS );
	FLD_char( GrpID, szIDS );
	FLD_char( ID, szIDS );
	FLD_char( ItemNum, szQUESTIONNAIRE_ITEM );
};

typedef GQUESTIONNAIRE* pGQUESTIONNAIRE;

// DB class
class AFX_EXT_CLASS DBGQuestionnaire : public DBData
{
public:
	DBGQuestionnaire() {}
	~DBGQuestionnaire() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pGQUESTIONNAIRE Find( CString szExpID, CString szGrpID, CString szID, CString& szErr );
	pGQUESTIONNAIRE Find( CString szExpID, CString szGrpID, CString& szErr );
	pGQUESTIONNAIRE Find( CString szExpID, CString& szErr );
	pGQUESTIONNAIRE FindForGroup( CString szGrp, CString& szErr );
	pGQUESTIONNAIRE FindForQuestion( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szExpID, CString szGrpID, CString szID, CString& szErr );
	BOOL Remove( CString szExpID, CString szGrpID, CString& szErr );
	BOOL Remove( CString szExpID, CString& szErr );
	BOOL RemoveGroup( CString szGrpID, CString& szErr );
	BOOL RemoveQuestion( CString szID, CString& szErr );
};

typedef DBGQuestionnaire* pDBGQuestionnaire;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_GQUESTIONNAIRE__

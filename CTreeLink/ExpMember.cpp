#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBExpMember.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szExpMemFile[ _MAX_PATH ] = _T("expmember");

// global data object
EXPERIMENTMEMBER expmember;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBExperimentMember::GetObjectName( CString& szName )
{
	szName = _T("experimentmember");
}

///////////////////////////////////////////////////////////////////////////////

void DBExperimentMember::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		m_table->SetPath( pszDataPath );
		strcpy_s( pszFile, szExpMemFile );
	}
	else strcpy_s( pszFile, szExpMemFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentMember::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ExpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx2 = m_table->AddField( "GrpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx3 = m_table->AddField( "SubjID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "DeviceRes", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "SamplingRate", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Exported", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ExpWhen", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "ExpWhere", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "Uploaded", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "UpWhen", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		m_table->AddField( "UpWhere", CT_FSTRING, _MAX_PATH + 1 );

		// add indices
		// exp only
		CTIndex idx1 = m_table->AddIndex( "Exp", CTINDEX_FIXED, YES, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
		// exp/grp
		CTIndex idx2 = m_table->AddIndex( "ExpGrp", CTINDEX_FIXED, YES, NO );
		idx2.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx2.AddSegment( fldIdx2, CTSEG_UREGSEG );
		// exp/grp/subj
		CTIndex idx3 = m_table->AddIndex( "ExpGrpSubj", CTINDEX_FIXED, NO, NO );
		idx3.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx3.AddSegment( fldIdx2, CTSEG_UREGSEG );
		idx3.AddSegment( fldIdx3, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentMember::GetData( pDBDATA pData, CString& szErr )
{
	pEXPERIMENTMEMBER p = (pEXPERIMENTMEMBER)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ExpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->GrpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->SubjID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->DeviceRes = m_record->GetFieldAsFloat( ++i );
		p->SamplingRate = m_record->GetFieldAsSigned( ++i );
		p->Exported = m_record->GetFieldAsSigned( ++i );
		p->ExpWhen = m_record->GetFieldAsFloat( ++i );
		strncpy_s( p->ExpWhere, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Uploaded = m_record->GetFieldAsSigned( ++i );
		p->UpWhen = m_record->GetFieldAsFloat( ++i );
		strncpy_s( p->UpWhere, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentMember::SetData( pDBDATA pData, CString& szErr )
{
	pEXPERIMENTMEMBER p = (pEXPERIMENTMEMBER)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ExpID );
		m_record->SetFieldAsString( ++i, p->GrpID );
		m_record->SetFieldAsString( ++i, p->SubjID );
		m_record->SetFieldAsFloat( ++i, p->DeviceRes );
		m_record->SetFieldAsSigned( ++i, p->SamplingRate );
		m_record->SetFieldAsSigned( ++i, p->Exported );
		m_record->SetFieldAsFloat( ++i, p->ExpWhen );
		m_record->SetFieldAsString( ++i, p->ExpWhere );
		m_record->SetFieldAsSigned( ++i, p->Uploaded );
		m_record->SetFieldAsFloat( ++i, p->UpWhen );
		m_record->SetFieldAsString( ++i, p->UpWhere );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pEXPERIMENTMEMBER DBExperimentMember::Find( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pEXPERIMENTMEMBER p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ], sid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';
	strncpy_s( sid, szSubjID, szIDS );
	sid[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 2 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->SetFieldAsString( 3, sid );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &expmember, szErr );
			p = &expmember;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pEXPERIMENTMEMBER DBExperimentMember::Find( CString szExpID, CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pEXPERIMENTMEMBER p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 1 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->RecordSetOn( 2* ( szIDS + 1 ) );
		// search
		if( m_record->First() )
		{
			GetData( &expmember, szErr );
			p = &expmember;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pEXPERIMENTMEMBER DBExperimentMember::Find( CString szExpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pEXPERIMENTMEMBER p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szExpID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 0 );
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &expmember, szErr );
			p = &expmember;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentMember::Remove( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szGrpID, szSubjID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentMember::Remove( CString szExpID, CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szGrpID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperimentMember::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f400 = false;
	bool f410 = false;

	// version 4.00 new
	try
	{
		m_table->GetField( _T("DeviceRes") );
	}
	catch( ... )
	{
		fMod = true;
		f400 = true;
	}

	// version 4.10
	try
	{
		m_table->GetField( _T("Exported") );
	}
	catch( ... )
	{
		fMod = true;
		f410 = true;
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 4.00
		if( f400 )
		{
			CTField fld = m_table->AddField( "DeviceRes", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str = _T("0.0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "SamplingRate", CT_INT4, sizeof( CT_INT4 ) );
			str = _T("0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// version 4.10
		if( f400 || f410 )
		{
			CTField fld = m_table->AddField( "Exported", CT_INT4, sizeof( CT_INT4 ) );
			str = _T("0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "ExpWhen", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str = _T("0.0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "ExpWhere", CT_FSTRING, _MAX_PATH + 1 );
			fld = m_table->AddField( "Uploaded", CT_INT4, sizeof( CT_INT4 ) );
			str = _T("0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			fld = m_table->AddField( "UpWhen", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str = _T("0.0");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "UpWhere", CT_FSTRING, _MAX_PATH + 1 );
		}
		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

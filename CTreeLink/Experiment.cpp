#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBExperiment.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szExpFile[ _MAX_PATH ] = _T("experiment");
static char szExpTempFile[ _MAX_PATH ] = _T("experimenttemp");

// global data object
EXPERIMENT experiment;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBExperiment::GetObjectName( CString& szName )
{
	szName = _T("experiment");
}

///////////////////////////////////////////////////////////////////////////////

void DBExperiment::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szExpFile );
		else strcpy_s( pszFile, szExpTempFile );
	}
	else strcpy_s( pszFile, szExpFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperiment::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Description", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Notes", CT_FSTRING, szNOTES + 1 );
		m_table->AddField( "Type", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "MissingDataValue", CT_DFLOAT, sizeof( CT_DFLOAT ) );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "ExpID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperiment::GetData( pDBDATA pData, CString& szErr )
{
	pEXPERIMENT p = (pEXPERIMENT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ExpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Desc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Notes, szNOTES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Type = m_record->GetFieldAsSigned( ++i );
		p->MissingDataValue = m_record->GetFieldAsFloat( ++i );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperiment::SetData( pDBDATA pData, CString& szErr )
{
	pEXPERIMENT p = (pEXPERIMENT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ExpID );
		m_record->SetFieldAsString( ++i, p->Desc );
		m_record->SetFieldAsString( ++i, p->Notes );
		m_record->SetFieldAsSigned( ++i, p->Type );
		m_record->SetFieldAsFloat( ++i, p->MissingDataValue );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pEXPERIMENT DBExperiment::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pEXPERIMENT p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &experiment, szErr );
			p = &experiment;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperiment::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBExperiment::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f500 = false;

	// version 5.00 new
	try
	{
		m_table->GetField( _T("MissingDataValue") );
	}
	catch( ... )
	{
		fMod = true;
		f500 = true;
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 5.00
		if( f500 )
		{
			CTField fld = m_table->AddField( "MissingDataValue", CT_DFLOAT, sizeof( CT_DFLOAT ) );
			str.Format( _T("%f"), -1.e6 );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
		}

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

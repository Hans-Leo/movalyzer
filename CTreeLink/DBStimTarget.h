#ifndef __DB_STIMTARGET__
#define	__DB_STIMTARGET__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Stimulus Target
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct STIMULUSTARGET : public DBDATA
{
	FLD_char( StimID, szIDS );
	FLD_char( ElmtID, szIDS );
	FLD_char( Sequence, szTARGET_SEQUENCE );
};

typedef STIMULUSTARGET* pSTIMULUSTARGET;

// DB class
class AFX_EXT_CLASS DBStimulusTarget : public DBData
{
public:
	DBStimulusTarget() {}
	~DBStimulusTarget() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pSTIMULUSTARGET Find( CString szStimID, CString szElmtID, CString szSequence, CString& szErr );
	pSTIMULUSTARGET Find( CString szStimID, CString szElmtID, CString& szErr );
	pSTIMULUSTARGET Find( CString szStimID, CString& szErr );
//	pSTIMULUSTARGET NextSet( CString& szErr );
	// remove current record
	BOOL Remove( CString szStimID, CString szElmtID, CString szSequence, CString& szErr );
};

typedef DBStimulusTarget* pDBStimulusTarget;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_STIMTARGET__

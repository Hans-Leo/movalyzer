#ifndef __DB_BACKUP__
#define	__DB_BACKUP__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// BACKUP1
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct BACKUP : public DBDATA
{
	FLD_char( UserID, szIDS );		// Who
	FLD_char( BackupDate, szDATE );	// When
	FLD_char( Exp, szIDS );			// What
	FLD_char( Grp, szIDS );			// ...
	FLD_char( Subj, szIDS );		// ...
	FLD_char( Path, _MAX_PATH );	// Where
	FLD_INT( Mode );				// How
	FLD_char( Notes, szNOTES );		// Why
};

typedef BACKUP* pBACKUP;

// DB class
class AFX_EXT_CLASS DBBackup : public DBData
{
public:
	DBBackup() {}
	~DBBackup() {}

// overrides
protected:
	// obtain pointer to current session
	virtual CTSession* GetSession( CString& szErr, int nForceMode )
	{ return DBData::GetSession( szErr, CTOP_LOCAL ); }	// backup always local
	virtual void ForceOperationMode( short nMode ) {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pBACKUP Find( CString szID, CString szDate, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString szDate, CString& szErr );
};

typedef DBBackup* pDBBACKUP;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_BACKUP1__

#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBMQuestionnaire.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szMQuestFile[ _MAX_PATH ] = _T("mquest");
static char szMQuestTempFile[ _MAX_PATH ] = _T("mquesttemp");

// global data object
MQUESTIONNAIRE mquest;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBMQuestionnaire::GetObjectName( CString& szName )
{
	szName = _T("mquestionnaire");
}

///////////////////////////////////////////////////////////////////////////////

void DBMQuestionnaire::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szMQuestFile );
		else strcpy_s( pszFile, szMQuestTempFile );
	}
	else strcpy_s( pszFile, szMQuestFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBMQuestionnaire::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "ItemNum", CT_FSTRING, szQUESTIONNAIRE_ITEM + 1 );
		m_table->AddField( "Header", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Private", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Question", CT_FSTRING, szQUESTIONNAIRE_Q + 1 );
		m_table->AddField( "Numeric", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ColHeader", CT_FSTRING, szCODE + 1 );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "MQuestID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBMQuestionnaire::GetData( pDBDATA pData, CString& szErr )
{
	pMQUESTIONNAIRE p = (pMQUESTIONNAIRE)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->ItemNum, szQUESTIONNAIRE_ITEM + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Header = m_record->GetFieldAsSigned( ++i );
		p->Private = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->Question, szQUESTIONNAIRE_Q + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Numeric = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->ColHeader, szCODE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBMQuestionnaire::SetData( pDBDATA pData, CString& szErr )
{
	pMQUESTIONNAIRE p = (pMQUESTIONNAIRE)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ID );
		m_record->SetFieldAsString( ++i, p->ItemNum );
		m_record->SetFieldAsSigned( ++i, p->Header );
		m_record->SetFieldAsSigned( ++i, p->Private );
		m_record->SetFieldAsString( ++i, p->Question );
		m_record->SetFieldAsSigned( ++i, p->Numeric );
		m_record->SetFieldAsString( ++i, p->ColHeader );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pMQUESTIONNAIRE DBMQuestionnaire::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pMQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &mquest, szErr );
			p = &mquest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBMQuestionnaire::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBMQuestionnaire::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f450 = false;

	// version 4.50
	try
	{
		m_table->GetField( _T("Numeric") );
	}
	catch( ... )
	{
		fMod = true;
		f450 = true;
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 4.50
		if( f450 )
		{
			CTField fld = m_table->AddField( "Numeric", CT_INT4, sizeof( CT_INT4 ) );
			str = _T("1");
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "ColHeader", CT_FSTRING, szCODE + 1 );
		}

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

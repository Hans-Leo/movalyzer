#ifndef __DB_EXPCONDITION__
#define	__DB_EXPCONDITION__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Experiment Condition
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct EXPERIMENTCONDITION : public DBDATA
{
	FLD_char( ExpID, szIDS );
	FLD_char( CondID, szIDS );
	FLD_INT( Replications );
};

typedef EXPERIMENTCONDITION* pEXPERIMENTCONDITION;

// DB class
class AFX_EXT_CLASS DBExperimentCondition : public DBData
{
public:
	DBExperimentCondition() {}
	~DBExperimentCondition() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pEXPERIMENTCONDITION Find( CString szExpID, CString szCondID, CString& szErr );
	pEXPERIMENTCONDITION Find( CString szExpID, CString& szErr );
//	pEXPERIMENTCONDITION NextSet( CString& szErr );
	// remove current record
	BOOL Remove( CString szExpID, CString szCondID, CString& szErr );
};

typedef DBExperimentCondition* pDBExperimentCondition;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_EXPCONDITION__

#ifndef __DB_CONDITION__
#define	__DB_CONDITION__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Condition
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct CONDITION : public DBDATA
{
	FLD_char( CondID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_char( Instr, szCONDITION_INSTR );
	FLD_char( Lex, szCONDITION_LEX );
	FLD_INT( StrokeMin );
	FLD_INT( StrokeMax );
	FLD_DOUBLE( StrokeLength );
	FLD_DOUBLE( RangeLength );
	FLD_DOUBLE( StrokeDirection );
	FLD_DOUBLE( RangeDirection );
	FLD_INT( StrokeSkip );
	FLD_char( Notes, szNOTES );
	FLD_BOOL( Record );
	FLD_BOOL( Process );
	FLD_char( Parent, szIDS + 1 );
	FLD_INT( Word );
	FLD_BOOL( UseStimulus );
	FLD_char( StimulusW, szIDS );
	FLD_char( StimulusP, szIDS );
	FLD_char( Stimulus, szIDS );
	FLD_DOUBLE( PrecueDuration );
	FLD_DOUBLE( PrecueLatency );
	FLD_BOOL( RecordImmediately );
	FLD_BOOL( StopWrongTarget );
	FLD_BOOL( StartFirstTarget );
	FLD_BOOL( StopLastTarget );
	FLD_DOUBLE( WarningDuration );
	FLD_DOUBLE( WarningLatency );
	FLD_DOUBLE( MagnetForce );
	// sounds
	FLD_BOOL( SoundStart );
	FLD_BOOL( SoundStartTone );
	FLD_INT( SoundStartToneFreq );
	FLD_INT( SoundStartToneDur );
	FLD_char( SoundStartMedia, _MAX_PATH );
	FLD_BOOL( SoundEnd );
	FLD_BOOL( SoundEndTone );
	FLD_INT( SoundEndToneFreq );
	FLD_INT( SoundEndToneDur );
	FLD_char( SoundEndMedia, _MAX_PATH );
	FLD_BOOL( SoundStimPStart );
	FLD_BOOL( SoundStimPStartTone );
	FLD_BOOL( SoundStimWStart );
	FLD_BOOL( SoundStimWStartTone );
	FLD_INT( SoundStimWStartToneFreq );
	FLD_INT( SoundStimWStartToneDur );
	FLD_char( SoundStimWStartMedia, _MAX_PATH );
	FLD_BOOL( SoundStimWStop );
	FLD_BOOL( SoundStimWStopTone );
	FLD_INT( SoundStimWStopToneFreq );
	FLD_INT( SoundStimWStopToneDur );
	FLD_char( SoundStimWStopMedia, _MAX_PATH );
	FLD_INT( SoundStimPStartToneFreq );
	FLD_INT( SoundStimPStartToneDur );
	FLD_char( SoundStimPStartMedia, _MAX_PATH );
	FLD_BOOL( SoundStimPStop );
	FLD_BOOL( SoundStimPStopTone );
	FLD_INT( SoundStimPStopToneFreq );
	FLD_INT( SoundStimPStopToneDur );
	FLD_char( SoundStimPStopMedia, _MAX_PATH );
	FLD_BOOL( SoundTargetCorrect );
	FLD_BOOL( SoundTargetCorrectTone );
	FLD_INT( SoundTargetCorrectToneFreq );
	FLD_INT( SoundTargetCorrectToneDur );
	FLD_char( SoundTargetCorrectMedia, _MAX_PATH );
	FLD_BOOL( SoundTargetWrong );
	FLD_BOOL( SoundTargetWrongTone );
	FLD_INT( SoundTargetWrongToneFreq );
	FLD_INT( SoundTargetWrongToneDur );
	FLD_char( SoundTargetWrongMedia, _MAX_PATH );
	FLD_BOOL( SoundPenup );
	FLD_BOOL( SoundPenupTone );
	FLD_INT( SoundPenupToneFreq );
	FLD_INT( SoundPenupToneDur );
	FLD_char( SoundPenupMedia, _MAX_PATH );
	FLD_BOOL( SoundPendown );
	FLD_BOOL( SoundPendownTone );
	FLD_INT( SoundPendownToneFreq );
	FLD_INT( SoundPendownToneDur );
	FLD_char( SoundPendownMedia, _MAX_PATH );
	// feedback
	FLD_BOOL( CountStrokes );
	FLD_BOOL( HideFeedback );
	FLD_BOOL( HideShowAfter );
	FLD_BOOL( HideShowAfterShow );		// if false, hide
	FLD_BOOL( HideShowAfterStrokes );	// if false, seconds
	FLD_DOUBLE( HideShowCount );
	FLD_BOOL( Transform );
	FLD_DOUBLE( Xgain );
	FLD_DOUBLE( Xrotation );
	FLD_DOUBLE( Ygain );
	FLD_DOUBLE( Yrotation );
	FLD_BOOL( flagbadtarget );
	// trigger pulses (being grouped with sound)
	FLD_BOOL( SoundStartTrigger );
	FLD_BOOL( SoundEndTrigger );
	FLD_BOOL( SoundStimWStartTrigger );
	FLD_BOOL( SoundStimWStopTrigger );
	FLD_BOOL( SoundStimPStartTrigger );
	FLD_BOOL( SoundStimPStopTrigger );
	FLD_BOOL( SoundTargetCorrectTrigger );
	FLD_BOOL( SoundTargetWrongTrigger );
	FLD_BOOL( SoundPenupTrigger );
	FLD_BOOL( SoundPendownTrigger );
	// event scripts - external apps (being grouped with sound)
	FLD_INT( SoundStartScriptType );
	FLD_char( SoundStartScript, szFILELENGTH );
	FLD_INT( SoundEndScriptType );
	FLD_char( SoundEndScript, szFILELENGTH );
	FLD_INT( SoundStimWStartScriptType );
	FLD_char( SoundStimWStartScript, szFILELENGTH );
	FLD_INT( SoundStimWStopScriptType );
	FLD_char( SoundStimWStopScript, szFILELENGTH );
	FLD_INT( SoundStimPStartScriptType );
	FLD_char( SoundStimPStartScript, szFILELENGTH );
	FLD_INT( SoundStimPStopScriptType );
	FLD_char( SoundStimPStopScript, szFILELENGTH );
	FLD_INT( SoundTargetCorrectScriptType );
	FLD_char( SoundTargetCorrectScript, szFILELENGTH );
	FLD_INT( SoundTargetWrongScriptType );
	FLD_char( SoundTargetWrongScript, szFILELENGTH );
	FLD_INT( SoundPenupScriptType );
	FLD_char( SoundPenupScript, szFILELENGTH );
	FLD_INT( SoundPendownScriptType );
	FLD_char( SoundPendownScript, szFILELENGTH );
	// feedback for charting (min-max color range based on tf feature)
	FLD_char( Feedback, szIDS );
	// gripper task type
	FLD_INT( GripperTask );
	// recording window visual feedback
	FLD_BOOL( TrialFeedback1 );
	FLD_BOOL( TrialFeedback2 );
	FLD_INT( FBColumn1 );
	FLD_INT( FBColumn2 );
	FLD_INT( FBStroke1 );
	FLD_INT( FBStroke2 );
	FLD_DOUBLE( FBMin1 );
	FLD_DOUBLE( FBMin2 );
	FLD_DOUBLE( FBMax1 );
	FLD_DOUBLE( FBMax2 );
	FLD_BOOL( FBSwapColors1 );
	FLD_BOOL( FBSwapColors2 );
};

typedef CONDITION* pCONDITION;

// DB class
class AFX_EXT_CLASS DBCondition : public DBData
{
public:
	DBCondition() {}
	~DBCondition() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pCONDITION Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBCondition* pDBCondition;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_CONDITION__

#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBSubject.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szSubjFile[ _MAX_PATH ] = _T("subject");
static char szSubjTempFile[ _MAX_PATH ] = _T("subjecttemp");

// global data object
SUBJECT subject;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBSubject::GetObjectName( CString& szName )
{
	szName = _T("subject");
}

///////////////////////////////////////////////////////////////////////////////

void DBSubject::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szSubjFile );
		else strcpy_s( pszFile, szSubjTempFile );
	}
	else strcpy_s( pszFile, szSubjFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSubject::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "LastName", CT_FSTRING, szSUBJECT_LAST_ENC + 1 );
		m_table->AddField( "FirstName", CT_FSTRING, szSUBJECT_FIRST_ENC + 1 );
		m_table->AddField( "Notes", CT_FSTRING, szNOTES + 1 );
		m_table->AddField( "PrvNotes", CT_FSTRING, szNOTES_ENC + 1 );
		m_table->AddField( "DateAdded", CT_DFLOAT, sizeof( CT_DFLOAT ) );
		//NOTE: Currently DATE is a double
		m_table->AddField( "DefExp", CT_FSTRING, szSUBJECT_LAST + 1 );
		m_table->AddField( "Active", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "ExpCount", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Encrypted", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SubjCode", CT_FSTRING, szCODE + 1 );
		m_table->AddField( "EncryptionMethod", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "SiteID", CT_FSTRING, szCODE + 1 );
		m_table->AddField( "SiteDesc", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Signature", CT_FSTRING, szSIGNATURE + 1 );
		m_table->AddField( "Password", CT_FSTRING, szSUBJECT_PASS + 1 );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "SubjID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSubject::GetData( pDBDATA pData, CString& szErr )
{
	pSUBJECT p = (pSUBJECT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->SubjID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->LastName, szSUBJECT_LAST_ENC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->FirstName, szSUBJECT_FIRST_ENC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Notes, szNOTES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->PrvNotes, szNOTES_ENC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->DateAdded = m_record->GetFieldAsFloat( ++i );
		strncpy_s( p->DefExp, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Active = m_record->GetFieldAsSigned( ++i );
		p->ExpCount = m_record->GetFieldAsSigned( ++i );
		p->Encrypted = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SubjCode, szCODE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->EncryptionMethod = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->SiteID, szCODE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->SiteDesc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Signature, szSIGNATURE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Password, szSUBJECT_PASS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSubject::SetData( pDBDATA pData, CString& szErr )
{
	pSUBJECT p = (pSUBJECT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->SubjID );
		m_record->SetFieldAsString( ++i, p->LastName );
		m_record->SetFieldAsString( ++i, p->FirstName );
		m_record->SetFieldAsString( ++i, p->Notes );
		m_record->SetFieldAsString( ++i, p->PrvNotes );
		m_record->SetFieldAsFloat( ++i, p->DateAdded );
		m_record->SetFieldAsString( ++i, p->DefExp );
		m_record->SetFieldAsSigned( ++i, p->Active );
		m_record->SetFieldAsSigned( ++i, p->ExpCount );
		m_record->SetFieldAsSigned( ++i, p->Encrypted );
		m_record->SetFieldAsString( ++i, p->SubjCode );
		m_record->SetFieldAsSigned( ++i, p->EncryptionMethod );
		m_record->SetFieldAsString( ++i, p->SiteID );
		m_record->SetFieldAsString( ++i, p->SiteDesc );
		m_record->SetFieldAsString( ++i, p->Signature );
		m_record->SetFieldAsString( ++i, p->Password );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pSUBJECT DBSubject::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSUBJECT p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &subject, szErr );
			p = &subject;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSubject::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBSubject::Verify( CString& szErr )
{
	// No DB verifications prior to 2.90 are required
	// verifications are done from oldest to newest

	CTString tstr;
	CString str;
	BOOL fNull = FALSE;
	// version mod flags (one for each modification)
	// all flags for later updates are forced true if any older are flagged
	bool fMod = false;
	bool f310 = false;
	bool f430 = false;
	bool fDoSize = false;
	bool fInternal = false;

	// check for field size of old var (everyone should have)
	CTField fld = m_table->GetField( "LastName" );
	if( fld.GetLength() != ( szSUBJECT_LAST_ENC + 1 ) )
	{
		fMod = true;
		fDoSize = true;
	}

	// version 3.10 new
	try
	{
		m_table->GetField( _T("DefExp") );
	}
	catch( ... )
	{
		fMod = true;
		f310 = true;
	}

	// version 4.30
	try
	{
		m_table->GetField( _T("Encrypted") );
		fInternal = true;
		m_table->GetField( _T("EncryptionMethod") );
	}
	catch( ... )
	{
		fMod = true;
		f430 = true;
	}

	// MODIFICATIONS (FROM OLDEST TO NEWEST) -- $NULFLD$ first
	try
	{
		// close & reopen in exclusive mode if any flags set
		if( fMod )
		{
			// warn
			if( !ConversionWarning( this ) )
			{
				Close( szErr );
				return FALSE;
			}
			// verify structure of doda
			if( !DODACleanup( szErr, fNull ) ) return FALSE;
		}

		// version 3.10
		if( f310 )
		{
			m_table->AddField( "DefExp", CT_FSTRING, szSUBJECT_LAST + 1 );
			m_table->AddField( "Active", CT_INT4, sizeof( CT_INT4 ) );
			m_table->AddField( "ExpCount", CT_INT4, sizeof( CT_INT4 ) );
		}

		// version 4.30
		if( f310 || f430 )
		{
			CTField fld;
			if( !fInternal )
			{
				fld = m_table->AddField( "Encrypted", CT_INT4, sizeof( CT_INT4 ) );
				str = _T("0");
				tstr = str;
				fld.SetFieldDefaultValue( tstr );
				m_table->AddField( "SubjCode", CT_FSTRING, szCODE + 1 );
			}
			fld = m_table->AddField( "EncryptionMethod", CT_INT4, sizeof( CT_INT4 ) );
			str.Format( _T("%d"), ENC_NONE );
			tstr = str;
			fld.SetFieldDefaultValue( tstr );
			m_table->AddField( "SiteID", CT_FSTRING, szCODE + 1 );
			m_table->AddField( "SiteDesc", CT_FSTRING, szDESC + 1 );
			m_table->AddField( "Signature", CT_FSTRING, szSIGNATURE + 1 );
			m_table->AddField( "Password", CT_FSTRING, szSUBJECT_PASS + 1 );
		}

		// private info fields size changes
		if( fDoSize )
		{
			fld = m_table->GetField( "LastName" );
			fld.SetLength( szSUBJECT_LAST_ENC + 1 );
			fld = m_table->GetField( "FirstName" );
			fld.SetLength( szSUBJECT_FIRST_ENC + 1 );
			fld = m_table->GetField( "PrvNotes" );
			fld.SetLength( szNOTES_ENC + 1 );
		}

		// we're done adding (check for $NULFLD$)....now alter/close/reopen
		if( fMod )
		{
			// older tables (pre ctpp) do not have a null field (required for default values)
			// and now with DODA cleanup, 
			// $NULFLD$ - bitmap image w/1 bit per field
			if( fNull )
			{
				NINT numFields = m_table->GetFieldCount();
				NINT fldSize = numFields / 8;
				if( ( fldSize * 8 ) < numFields ) fldSize++;
				m_table->AddField( "$NULFLD$", CT_ARRAY, fldSize );
			}

			// alter/commit - close/reopen
			m_fConverted = TRUE;
			m_table->Alter( CTDB_ALTER_NORMAL );
			Close( szErr );
			return Open( szErr );
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error altering table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

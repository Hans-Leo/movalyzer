#ifndef __DB_EXPERIMENT__
#define	__DB_EXPERIMENT__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Experiment
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct EXPERIMENT : public DBDATA
{
	FLD_char( ExpID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_char( Notes, szNOTES );
	FLD_INT( Type );
	FLD_DOUBLE( MissingDataValue );
};

typedef EXPERIMENT* pEXPERIMENT;

// DB class
class AFX_EXT_CLASS DBExperiment : public DBData
{
public:
	DBExperiment() {}
	~DBExperiment() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pEXPERIMENT Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBExperiment* pDBExperiment;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_EXPERIMENT__

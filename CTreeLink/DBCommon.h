#ifndef	__DBCOMMON_H__
#define	__DBCOMMON_H__

#ifndef _CTDBSDK_HPP_
typedef CString CTString;
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Encryption Methods
///////////////////////////////////////////////////////////////////////////////
#define	ENC_NONE		0
#define	ENC_DES			1
#define	ENC_AES			2
#define	ENC_BLOWFISH	3

///////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////

#ifndef COUNT
typedef short int COUNT;
#endif
#ifndef	DATE
typedef double DATE;
#endif
#ifndef	CTOPEN_NORMAL
#define	CTOPEN_NORMAL	0
#endif

#define	FLD_char(fld, sz)	char	fld[ sz + 1 ];
#define	FLD_DOUBLE(fld)		double	fld;
#define	FLD_LONG(fld)		long	fld;
#define	FLD_INT(fld)		int		fld;
#define	FLD_SHORT(fld)		COUNT	fld;
#define	FLD_BOOL(fld)		BOOL	fld;
#define	FLD_DATE(fld)		DATE	fld;
#define	FLD_DWORD(fld)		DWORD	fld;

#define	DEL_FLAG			FLD_SHORT(delete_flag);

///////////////////////////////////////////////////////////////////////////////
// Common Data Structure
///////////////////////////////////////////////////////////////////////////////

struct DBDATA
{
	DEL_FLAG;
};

typedef DBDATA* pDBDATA;

///////////////////////////////////////////////////////////////////////////////
// Common DB Class (Ctree C++)
///////////////////////////////////////////////////////////////////////////////

// ctree operation modes
#define	CTOP_DEFAULT	0
#define	CTOP_LOCAL		1
#define	CTOP_REMOTE		2
#define	CTOP_BOTH		3

// defines for record traversal for shared function
#define	MOVE_FIRST	1
#define	MOVE_LAST	2
#define	MOVE_PREV	3
#define	MOVE_NEXT	4

// some useful macros
#define	REPORT_ERROR( msg ) \
	CString szName; \
	GetObjectName( szName ); \
	szErr.Format( msg, szName ); \
	DoError( &E, szErr );

// CTPP forward declares
class CTException;
class CTSession;
class CTTable;
class CTRecord;

class AFX_EXT_CLASS DBData
{
// construction/destruction
public:
	DBData();
	~DBData();

// reference counts to track starting/ending a session
public:
	BOOL AddReference( CString& szErr );
	BOOL ReleaseReference( CString& szErr );

// info functions
public:
	// data & backup path for associated table
	virtual void SetDataPath( CString szPath );
	virtual void SetBackupPath( CString szPath );
	virtual char* GetDataPath();
	virtual char* GetBackupPath();
	// global application window for dialogs
	void SetAppWindow( HWND hWnd );
	static HWND GetAppWindow();
	// error reporting
	void DoError( CTException* E, CString& szErr );

// session & table management (NOTE: handled internally)
protected:
	// obtain pointer to current session
	virtual CTSession* GetSession( CString& szErr, int nForceMode = CTOP_DEFAULT );
	// for opening/creating the table
	// ...the creation of the fields is managed by the inheriting class
	BOOL Create( CString& szErr, int mode, int nForceMode = CTOP_DEFAULT );
	// are we in remote mode?
	BOOL IsRemoteMode();
	// cleanup the DODA ($NULFLD$)
	BOOL DODACleanup( CString& szErr, BOOL& fFound );
public:
	// set db modes to local and/or remote
	virtual void SetOperationMode( short nMode, CString szServer, CString szUser,
								   CString szPassword, CString& szErr );
	virtual void ForceOperationMode( short nMode );
	// open the table (create does the open/create)
	BOOL Open( CString& szErr, int mode = CTOPEN_NORMAL )
		{ return Create( szErr, mode ); }
	// close the table
	BOOL Close( CString& szErr );
	// repair table
	BOOL Repair( CString& szErr );
	// reset warning status for db conversions
	void ResetWarningStatus();
	// is this a virgin table (created)?
	BOOL IsVirgin() { return m_fVirgin; }
	// set this as a temporary table
	void SetTemp( BOOL fVal );
	// dump a record to a file
	BOOL DumpRecord( CString szID, CString szFile );
	// remove temporary table
	void RemoveTemp();
	// record count
	ULONGLONG GetNumRecords();

// record traversal functions
public:
	BOOL FirstItem( pDBDATA pData, CString& szErr );
	BOOL PreviousItem( pDBDATA pData, CString& szErr );
	BOOL NextItem( pDBDATA pData, CString& szErr );
	BOOL LastItem( pDBDATA pData, CString& szErr );
protected:
	// the private shared version for record traversal
	BOOL GoToItem( UINT nMode, pDBDATA pData, CString& szErr );

// record manipulation functions
public:
	// add a new record
	BOOL AddItem( pDBDATA pData, CString& szErr );
	// modify current record
	BOOL ModifyItem( pDBDATA pData, CString& szErr );
	// was the database converted to a newer version
	BOOL WasConverted() { return m_fConverted; }

// pure virtual functions that MUST be implemented by inheriting class
public:
	// table file name and location
	virtual void GetObjectName( CString& szName ) = 0;
	virtual void GetFileName( CTString& szFile ) = 0;
protected:
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr ) = 0;
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr ) { return TRUE; }
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr ) = 0;
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr ) = 0;
public:
	// Find can be different for all inherits...therefore, not virtual (but required)
	// Remove can be different for all classes...therefore, not virtual (but required)

// shared class member vars
protected:
	BOOL		m_fOpen;
	CTTable*	m_table;
	CTRecord*	m_record;
	short		m_nForceMode;
	BOOL		m_fConverted;
	BOOL		m_fVirgin;
	BOOL		m_fTemp;
};

// conversion warning/backup functions
BOOL ConversionWarning( DBData* );

///////////////////////////////////////////////////////////////////////////////
// Field sizes
///////////////////////////////////////////////////////////////////////////////

#define	szIDS					3
#define	szCODE					25
#define	szDESC					50
#define	szNOTES					100
#define	szNOTES_ENC				szNOTES * 3
#define	szFILELENGTH			MAX_PATH
#define	szFONTNAME				50
#define	szSIGNATURE				150
#define szRULES					10
#define szTRIALS				1000

#define	szSUBJECT_LAST			25
#define	szSUBJECT_LAST_ENC		szSUBJECT_LAST * 3
#define	szSUBJECT_FIRST			20
#define	szSUBJECT_FIRST_ENC		szSUBJECT_FIRST * 3
#define	szSUBJECT_PASS			50
#define	szCONDITION_INSTR		50
#define	szCONDITION_LEX			50
#define	szCONDITION_STIMULUS	szFILELENGTH
#define	szQUESTIONNAIRE_ITEM	5
#define	szQUESTIONNAIRE_Q		100
#define	szQUESTIONNAIRE_A		200
#define	szUSER_DESC				25
#define	szUSER_ROOTPATH			szFILELENGTH
#define	szUSER_BACKUPPATH		szFILELENGTH
#define	szUSER_DELIM			2
#define	szUSER_PASS				10
#define	szUSER_PASS_ENC			szUSER_PASS * 3
#define	szDATE					20
#define	szELEMENT_TEXT			120
#define	szTARGET_SEQUENCE		3
#define	szCOMM_PORT				31

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#ifndef _RESOURCE_H_
#include "Resource.h"
#define _RESOURCE_H_
#endif

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#endif	// __DBCOMMON_H__

#ifndef __DB_EXPMEMBER__
#define	__DB_EXPMEMBER__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Experiment Member
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct EXPERIMENTMEMBER : public DBDATA
{
	FLD_char( ExpID, szIDS );
	FLD_char( GrpID, szIDS );
	FLD_char( SubjID, szIDS );
	FLD_DOUBLE( DeviceRes );
	FLD_INT( SamplingRate );
	FLD_BOOL( Exported );
	FLD_DATE( ExpWhen );
	FLD_char( ExpWhere, _MAX_PATH );
	FLD_BOOL( Uploaded );
	FLD_DATE( UpWhen );
	FLD_char( UpWhere, _MAX_PATH );
};

typedef EXPERIMENTMEMBER* pEXPERIMENTMEMBER;

// DB class
class AFX_EXT_CLASS DBExperimentMember : public DBData
{
public:
	DBExperimentMember() {}
	~DBExperimentMember() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pEXPERIMENTMEMBER Find( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr );
	pEXPERIMENTMEMBER Find( CString szExpID, CString szGrpID, CString& szErr );
	pEXPERIMENTMEMBER Find( CString szExpID, CString& szErr );
//	pEXPERIMENTMEMBER NextSet( CString& szErr );
	// remove current record
	BOOL Remove( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr );
	BOOL Remove( CString szExpID, CString szGrpID, CString& szErr );
};

typedef DBExperimentMember* pDBExperimentMember;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_EXPMEMBER__

#ifndef __DB_SQUESTIONNAIRE__
#define	__DB_SQUESTIONNAIRE__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// SQuestionnaire
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct SQUESTIONNAIRE : public DBDATA
{
	FLD_char( ExpID, szIDS );
	FLD_char( GrpID, szIDS );
	FLD_char( SubjID, szIDS );
	FLD_char( ID, szIDS );
	FLD_char( Answer, szQUESTIONNAIRE_A );
};

typedef SQUESTIONNAIRE* pSQUESTIONNAIRE;

// DB class
class AFX_EXT_CLASS DBSQuestionnaire : public DBData
{
public:
	DBSQuestionnaire() {}
	~DBSQuestionnaire() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pSQUESTIONNAIRE Find( CString szExpID, CString szGrpID, CString szSubjID, CString szID, CString& szErr );
	pSQUESTIONNAIRE Find( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr );
	pSQUESTIONNAIRE Find( CString szExpID, CString szGrpID, CString& szErr );
	pSQUESTIONNAIRE FindForGroup( CString szGrp, CString& szErr );
	pSQUESTIONNAIRE FindForSubject( CString szSubjID, CString& szErr );
	pSQUESTIONNAIRE FindForQuestion( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szExpID, CString szGrpID, CString szSubjID, CString szID, CString& szErr );
	BOOL Remove( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr );
	BOOL RemoveSubject( CString szSubjID, CString& szErr );
	BOOL RemoveGroup( CString szGrpID, CString& szErr );
	BOOL RemoveQuestion( CString szID, CString& szErr );
	BOOL ClearQuestionnaire( CString szExpID, CString szGrpID, CString szSubjID, CString& szErr );
};

typedef DBSQuestionnaire* pDBSQuestionnaire;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_SQUESTIONNAIRE__

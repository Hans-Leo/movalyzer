#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBStimElement.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szStimElmtFile[ _MAX_PATH ] = _T("stimelmt");

// global data object
STIMULUSELEMENT stimelement;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBStimulusElement::GetObjectName( CString& szName )
{
	szName = _T("stimuluselement");
}

///////////////////////////////////////////////////////////////////////////////

void DBStimulusElement::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		m_table->SetPath( pszDataPath );
		strcpy_s( pszFile, szStimElmtFile );
	}
	else strcpy_s( pszFile, szStimElmtFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBStimulusElement::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "StimID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx2 = m_table->AddField( "ElmtID", CT_FSTRING, szIDS + 1 );

		// add indices
		// stim only
		CTIndex idx1 = m_table->AddIndex( "Stim", CTINDEX_FIXED, YES, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
		// stim/elmt
		CTIndex idx2 = m_table->AddIndex( "StimElmt", CTINDEX_FIXED, NO, NO );
		idx2.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx2.AddSegment( fldIdx2, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBStimulusElement::GetData( pDBDATA pData, CString& szErr )
{
	pSTIMULUSELEMENT p = (pSTIMULUSELEMENT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->StimID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->ElmtID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBStimulusElement::SetData( pDBDATA pData, CString& szErr )
{
	pSTIMULUSELEMENT p = (pSTIMULUSELEMENT)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->StimID );
		m_record->SetFieldAsString( ++i, p->ElmtID );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pSTIMULUSELEMENT DBStimulusElement::Find( CString szStimID, CString szElmtID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSTIMULUSELEMENT p = NULL;
	TEXT sid[ szIDS + 1 ], eid[ szIDS + 1 ];
	strncpy_s( sid, szStimID, szIDS );
	sid[ szIDS ] = '\0';
	strncpy_s( eid, szElmtID, szIDS );
	eid[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 1 );
		// set search criteria
		m_record->SetFieldAsString( 1, sid );
		m_record->SetFieldAsString( 2, eid );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &stimelement, szErr );
			p = &stimelement;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pSTIMULUSELEMENT DBStimulusElement::Find( CString szStimID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pSTIMULUSELEMENT p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szStimID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 0 );
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &stimelement, szErr );
			p = &stimelement;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBStimulusElement::Remove( CString szStimID, CString szElmtID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szStimID, szElmtID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

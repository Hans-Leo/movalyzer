#ifndef __DB_STIMULUS__
#define	__DB_STIMULUS__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Stimulus
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct STIMULUS : public DBDATA
{
	FLD_char( StimID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_BOOL( Demo );	// not used
};

typedef STIMULUS* pSTIMULUS;

// DB class
class AFX_EXT_CLASS DBStimulus : public DBData
{
public:
	DBStimulus() {}
	~DBStimulus() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pSTIMULUS Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBStimulus* pDBStimulus;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_STIMULUS__

#include "StdAfx.h"
#include "ctreep.h"
#include "ctdbsdk.hpp"
#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////

static int		_RefCount = 0;
static char		_DataPath[ _MAX_PATH ] = _T("");
static char		_BackupPath[ _MAX_PATH ] = _T("");
static BOOL		_DidConvWarning = FALSE;
static BOOL		_Backup = FALSE;
static short	_nMode = CTOP_LOCAL;
static CString	_szServer;
static CString	_szUser;
static CString	_szPassword;
static BOOL		_NeedConvertSaidNo = FALSE;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

CTSession sessionLocal( CTSESSION_CTREE );
CTSession sessionRemote( CTSESSION_CTREE );

HWND	_appWnd	= NULL;
void DBData::SetAppWindow( HWND hWnd ) { _appWnd = hWnd; }
HWND DBData::GetAppWindow() { return _appWnd; }
BOOL ConvertLabel( CTString szLbl, CString& szNewLbl );

///////////////////////////////////////////////////////////////////////////////

DBData::DBData()
{
	m_fOpen = FALSE;
	m_table = NULL;
	m_record = NULL;
	m_nForceMode = CTOP_DEFAULT;
	m_fConverted = FALSE;
	m_fVirgin = FALSE;
}

///////////////////////////////////////////////////////////////////////////////

DBData::~DBData()
{
	if( m_table ) delete m_table;
	if( m_record ) delete m_record;

	if( sessionLocal.IsActive() == YES ) sessionLocal.Logout();
	if( sessionRemote.IsActive() == YES ) sessionRemote.Logout();
}

///////////////////////////////////////////////////////////////////////////////

void DBData::DoError( CTException* E, CString& szErr )
{
	if( !E ) return;

	CString szTemp;
	szTemp.Format( _T("ERROR: [%d] - %s"), E->GetErrorCode(), E->GetErrorMsg() );
	szErr += szTemp;
}

///////////////////////////////////////////////////////////////////////////////

CTSession* DBData::GetSession( CString& szErr, int nForceMode )
{
	CString szName;
	GetObjectName( szName );

	try
	{
		if( ( m_nForceMode == CTOP_LOCAL ) || ( nForceMode == CTOP_LOCAL ) ||
			( ( _nMode == CTOP_LOCAL ) && ( nForceMode != CTOP_REMOTE ) && ( m_nForceMode != CTOP_REMOTE ) ) )
		{
			if( sessionLocal.IsActive() == NO )
			{
				// local library (non-server) parameters
				sessionLocal.SetParam( CT_USERPROF, USERPRF_LOCLIB );
				// login to server
				sessionLocal.Logon();
			}
			return &sessionLocal;
		}
		else if( ( m_nForceMode == CTOP_REMOTE ) || ( nForceMode == CTOP_REMOTE ) ||
				 ( ( _nMode == CTOP_REMOTE ) && ( nForceMode != CTOP_LOCAL ) && ( m_nForceMode != CTOP_LOCAL ) ) )
		{
			if( sessionRemote.IsActive() == NO )
			{
				CTString szServer, szUser, szPassword;
				char pszTemp[ _MAX_PATH ];

				strcpy_s( pszTemp, _MAX_PATH, _szServer );
				szServer = pszTemp;
				strcpy_s( pszTemp, _MAX_PATH, _szUser );
				szUser = pszTemp;
				strcpy_s( pszTemp, _MAX_PATH, _szPassword );
				szPassword = pszTemp;

				// default parameters
				sessionRemote.SetParam( CT_USERPROF, ( USERPRF_NTKEY | USERPRF_CLRCHK ) );
				// login to server
				sessionRemote.Logon( szServer, szUser, szPassword );
			}
			return &sessionRemote;
		}
		else return NULL;
	}
	catch( CTException E )
	{
		szErr = _T("Unable to login to ctree server.");
		DoError( &E, szErr );
		_RefCount--;
		return NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////

void DBData::SetOperationMode( short nMode, CString szServer, CString szUser,
							   CString szPassword, CString& szErr )
{
	_nMode = nMode;
	_szServer = szServer;
	_szUser = szUser;
	_szPassword = szPassword;

	// next call to open a table should set to new mode
}

///////////////////////////////////////////////////////////////////////////////

void DBData::ForceOperationMode( short nMode )
{
	m_nForceMode = nMode;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::IsRemoteMode()
{
	if( m_nForceMode == CTOP_DEFAULT )
		return( _nMode == CTOP_REMOTE );
	else
		return( m_nForceMode == CTOP_REMOTE );
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::AddReference( CString& szErr )
{
	_RefCount++;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::ReleaseReference( CString& szErr )
{
	_RefCount--;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void DBData::SetDataPath( CString szPath )
{
	strcpy_s( _DataPath, _MAX_PATH, szPath );
}

///////////////////////////////////////////////////////////////////////////////

char* DBData::GetDataPath()
{
	return _DataPath;
}

///////////////////////////////////////////////////////////////////////////////

void DBData::SetBackupPath( CString szPath )
{
	strcpy_s( _BackupPath, _MAX_PATH, szPath );
}

///////////////////////////////////////////////////////////////////////////////

char* DBData::GetBackupPath()
{
	return _BackupPath;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::Repair( CString& szErr )
{
	try
	{
		// if open, close
		if( m_fOpen )
		{
			Close( szErr );
			m_fOpen = FALSE;
		}
		CTString szFile;
		GetFileName( szFile );
		// repair
		m_table->Open( szFile, CTOPEN_CORRUPT );
		m_table->Rebuild( CTREBUILD_NONE );
		m_table->Close();
		return TRUE;
	}
	catch( CTException E )
	{
		CString szName;
		szName.GetBufferSetLength( 25 );
		GetObjectName( szName );
		szErr.Format( _T("Unable to open/repair table %s."), szName );
		DoError( &E, szErr );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::Create( CString& szErr, int mode, int nForceMode )
{
	// if a conversion was needed and they said no...then never return true
	// any legitimate tables before this that were already scanned will show up in the tree
	// however any refresh or editing attempts will fail
//	if( _NeedConvertSaidNo ) return FALSE;

	if( m_fOpen ) return TRUE;

	// get current session
	CTSession* pSession = GetSession( szErr, nForceMode );
	if( !pSession ) return FALSE;

	// delete table/record if exist
	if( m_table ) delete m_table; m_table = NULL;
	if( m_record ) delete m_record; m_record = NULL;

	// create & attach tables/records
	m_table = new CTTable( pSession );
	m_record = new CTRecord( m_table );

	// mode must be CTOPEN_NORMAL or CTOPEN_EXCLUSIVE
	if( ( mode != CTOPEN_NORMAL ) && ( mode != CTOPEN_EXCLUSIVE ) ) return FALSE;

	// current object/table name
	CString szName;
	szName.GetBufferSetLength( 25 );
	GetObjectName( szName );
	// file/table name/location
	CTString szFile;
	GetFileName( szFile );
	if( ( szFile.c_str() == NULL ) || ( szFile == _T("") ) ) {
		if( m_table ) {
			delete m_table;
			m_table = NULL;
		}
		if( m_record ) {
			delete m_record;
			m_record = NULL;
		}
		return FALSE;
	}

	// needs to be created flag
	CTBOOL fCreate = NO;

	try
	{
		// has session connected?
		if( pSession->IsActive() == NO )
		{
			szErr = _T("CTree Session not active.");
			return FALSE;
		}

		// open table (attempt)
		try
		{
			m_table->Open( szFile, mode );
			m_fOpen = TRUE;
			// if opened successfully, check for any table updates
			// changes will require a table close/open in exclusive
			// and reclose & reopen
			// verify calls open again in exclusive mode (do not verify)
			if( ( mode != CTOPEN_EXCLUSIVE ) && !Verify( szErr ) ) return FALSE;
		}
		catch( CTException E )
		{
			NINT ec = E.GetErrorCode();
			if( ec == 12 ) fCreate = YES;	// table does not exist
			else if( ec == 14 )				// table failed to open (corrupt)
			{
				// repair & reopen if repaired
				if( Repair( szErr ) )
				{
					return Create( szErr, mode, nForceMode );
				}
				else
				{
					m_fOpen = FALSE;
					return FALSE;
				}
			}
		}

		// create table if necessary
		if( fCreate )
		{
			if( CreateTable( szErr ) )
			{
				m_table->Create( szFile, CTCREATE_NORMAL );
				// seems the subsequent call immediately after a creation was failing (dunno why)
				// since there is constant checking to ensure the table is open, removing this will not harm
//				m_table->Open( szFile, CTOPEN_NORMAL );
//				m_fOpen = TRUE;
				m_fVirgin = TRUE;
			}
			else return FALSE;
		}
	}
	catch( CTException E )
	{
		szErr.Format( _T("Unable to open table %s."), szName );
		DoError( &E, szErr );
		m_fOpen = FALSE;
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::Close( CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;

	try
	{
		if( m_fOpen )
		{
			m_table->Close();
			m_fOpen = FALSE;
		}
	}
	catch( CTException E )
	{
		DoError( &E, szErr );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::AddItem( pDBDATA pData, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !pData ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	// table/object name
	CString szName;
	szName.GetBufferSetLength( 25 );
	GetObjectName( szName );

	try
	{
		// clear record of any association
		m_record->Clear();
		// Set data to fields
		if( SetData( pData, szErr ) )
		{
			// write data
			m_record->Write();

			return TRUE;
		}
	}
	catch( CTException E )
	{
		szErr.Format( _T("Unable to write to table %s."), szName );
		DoError( &E, szErr );
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::ModifyItem( pDBDATA pData, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !pData ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	// table/object name
	CString szName;
	szName.GetBufferSetLength( 25 );
	GetObjectName( szName );

	try
	{
		// Set data to fields
		if( SetData( pData, szErr ) )
		{
			// write data
			m_record->Write();

			return TRUE;
		}
	}
	catch( CTException E )
	{
		szErr.Format( _T("Unable to write to table %s."), szName );
		DoError( &E, szErr );
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::GoToItem( UINT nMode, pDBDATA pData, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !pData || !Open( szErr ) ) return FALSE;

	// table/object name
	CString szName;
	szName.GetBufferSetLength( 25 );
	GetObjectName( szName );

	try
	{
		// go to appropriate record (if any)
		if( ( ( nMode == MOVE_FIRST ) && m_record->First() ) ||
			( ( nMode == MOVE_LAST ) && m_record->Last() ) ||
			( ( nMode == MOVE_PREV ) && m_record->Prev() ) ||
			( ( nMode == MOVE_NEXT ) && m_record->Next() ) )
		{
			// get data
			BOOL fRet = GetData( pData, szErr );

			return fRet;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		szErr.Format( _T("Cannot move to record of table %s."), szName );
		DoError( &E, szErr );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::FirstItem( pDBDATA pData, CString& szErr )
{
	return GoToItem( MOVE_FIRST, pData, szErr );
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::PreviousItem( pDBDATA pData, CString& szErr )
{
	return GoToItem( MOVE_PREV, pData, szErr );
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::NextItem( pDBDATA pData, CString& szErr )
{
	return GoToItem( MOVE_NEXT, pData, szErr );
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::LastItem( pDBDATA pData, CString& szErr )
{
	return GoToItem( MOVE_LAST, pData, szErr );
}

///////////////////////////////////////////////////////////////////////////////

void DBData::ResetWarningStatus()
{
	_DidConvWarning = FALSE;
}

///////////////////////////////////////////////////////////////////////////////

void DBData::SetTemp( BOOL fVal )
{
	m_fTemp = fVal;
	m_fOpen = FALSE;
}

///////////////////////////////////////////////////////////////////////////////

void DBData::RemoveTemp()
{
	
	// do nothing if table object isnt valid or this isnt a temporary DB
	if( !m_table || !m_fTemp ) return;
	// if open, close the table first (otherwise cant delete - sharing violation)
	if( m_fOpen ) m_table->Close();
	// get the temporary table name
	CTString szTable;
	GetFileName( szTable );
	// path to table
	CTString szPath = GetDataPath();
	szPath += _T("\\");
	szPath += szTable;
	// idx & dat files
	CTString szIdx = szPath, szDat = szPath;
	szIdx += _T(".idx");
	szDat += _T(".dat");
	// if they exists, delete them
	CFileFind ff;
	if( ff.FindFile( szIdx.c_str() ) ) CFile::Remove( szIdx.c_str() );
	if( ff.FindFile( szDat.c_str() ) ) CFile::Remove( szDat.c_str() );
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG DBData::GetNumRecords()
{
	// if this function was called before a recordset was created (reset to start or find)
	// it will throw an exception

	unsigned __int64 val = 0;
	try
	{
		if( m_record ) val = m_record->GetRecordCount();
	}
	catch( ... ) {}	// do nothing

	return val;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBData::DumpRecord( CString szID, CString szFile )
{
	// do nothing if table object isnt valid or not open (TODO w/ID for find)
	if( !m_table || !m_fOpen ) return FALSE;
	// file to write
	CStdioFile f;
	if( !f.Open( szFile, CFile::modeCreate | CFile::modeWrite ) ) return FALSE;
	// loop through the fields, get names & vals, & dump to file
	CTField fld;
	CTDBTYPE fldType;
	CTString szName, szVal;
	CString szLine, szTemp, szNewName;
	double dVal = 0.;
	int nVal = 0;
	short sVal = 0;
	DWORD dwVal = 0;
	LOGFONT lf;
	VRLEN numFields = m_table->GetFieldCount();
	for( VRLEN i = 1; i < numFields; i++ )
	{
		fld = m_table->GetField( i );
		szName = fld.GetName();
		if( ConvertLabel( szName, szNewName ) ) szName = szNewName.GetBuffer();
		fldType = fld.GetType();
		switch( fldType )
		{
			case CT_FSTRING:
				szVal = m_record->GetFieldAsString( i );
				szLine = szName.c_str();
				szLine += _T(" : ");
				szLine += szVal.c_str();
				break;
			case CT_DFLOAT:
				dVal = m_record->GetFieldAsFloat( i );
				szLine.Format( _T("%s : %g"), szName.c_str(), dVal );
				break;
			case CT_INT2:
				sVal = m_record->GetFieldAsShort( i );
				szLine.Format( _T("%s : %d"), szName.c_str(), sVal );
				break;
			case CT_INT4:
				nVal = m_record->GetFieldAsSigned( i );
				szLine.Format( _T("%s : %d"), szName.c_str(), nVal );
				break;
			case CT_INT4U:
				dwVal = m_record->GetFieldAsUnsigned( i );
				szLine.Format( _T("%s : %u"), szName.c_str(), dwVal );
				break;
			case CT_BINARY: case 128:
				ctdbGetFieldAsBinary( m_record->GetHandle(), i, (LPVOID)&lf, sizeof( LOGFONT ) );
				szTemp.Format( _T("%u%u%u%u%u%d%d%d%d%d%d%d%d%s"),
							  lf.lfHeight, lf.lfWidth, lf.lfEscapement, lf.lfOrientation,
							  lf.lfWeight, lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut,
							  lf.lfCharSet, lf.lfOutPrecision, lf.lfClipPrecision,
							  lf.lfQuality, lf.lfPitchAndFamily, lf.lfFaceName );
				if( szName == _T("lf1") ) szLine.Format( _T("LOGFONT1 : %s"), szTemp );
				else if( szName == _T("lf2") ) szLine.Format( _T("LOGFONT2 : %s"), szTemp );
				else if( szName == _T("lf3") ) szLine.Format( _T("LOGFONT3 : %s"), szTemp );
				break;
			default:
				szLine = _T("Unknown data type");
		}
		szLine += _T("\n");
		f.WriteString( szLine );
	}

	return TRUE;
}

BOOL ConvertLabel( CTString szLbl, CString& szNewLbl )
{
	BOOL fRet = TRUE;
	if( szLbl == _T("TF_J") ) szNewLbl = _T("SuppressJerk");
	else if( szLbl == _T("TF_R") ) szNewLbl = _T("SpectrumPos");
	else if( szLbl == _T("TF_A") ) szNewLbl = _T("SpectrumAccel");
	else if( szLbl == _T("TF_U") ) szNewLbl = _T("UnrotAuto");
	else if( szLbl == _T("TF_O") ) szNewLbl = _T("TFOneStroke");
	else if( szLbl == _T("SEG_F") ) szNewLbl = _T("AddFirstSeg");
	else if( szLbl == _T("SEG_L") ) szNewLbl = _T("AddLastSeg");
	else if( szLbl == _T("SEG_S") ) szNewLbl = _T("SplitStrokes");
	else if( szLbl == _T("SEG_O") ) szNewLbl = _T("SegOneStroke");
	else if( szLbl == _T("SEG_M") ) szNewLbl = _T("SegAbsVelMin");
	else if( szLbl == _T("SEG_A") ) szNewLbl = _T("SegVerAccel");
	else if( szLbl == _T("SEG_V") ) szNewLbl = _T("MoveSegPenDown");
	else if( szLbl == _T("SEG_MM") ) szNewLbl = _T("MoveSegAbsVelMin");
	else if( szLbl == _T("SEG_J") ) szNewLbl = _T("SegPenDownTraj");
	else if( szLbl == _T("EXT_S") ) szNewLbl = _T("ExtSubMvmtAna");
	else if( szLbl == _T("EXT_3") ) szNewLbl = _T("3DEstStraight");
	else if( szLbl == _T("EXT_O") ) szNewLbl = _T("ExtOneStroke");
	else if( szLbl == _T("EXT_2") ) szNewLbl = _T("LoopArea2Strokes");
	else if( szLbl == _T("CON_A") ) szNewLbl = _T("ConUseAbs");
	else if( szLbl == _T("CON_N") ) szNewLbl = _T("NoHdr");
	else if( szLbl == _T("CON_M") ) szNewLbl = _T("DiscardAboveMaxStrokes");
	else if( szLbl == _T("CON_U") ) szNewLbl = _T("NoUpStrokeTest");
	else if( szLbl == _T("CON_C") ) szNewLbl = _T("AnticipationTest");
	else if( szLbl == _T("CON_S") ) szNewLbl = _T("SubstMissingTrials");
	else if( szLbl == _T("CON_O") ) szNewLbl = _T("ConSubMvmtAna");
	else if( szLbl == _T("ST_A") ) szNewLbl = _T("SummUseAbs");
	else if( szLbl == _T("ST_R") ) szNewLbl = _T("SummRelData");
	else if( szLbl == _T("ST_Z") ) szNewLbl = _T("MissingStrokesAsZeros");
	else if( szLbl == _T("ST_T") ) szNewLbl = _T("MissingTrialsAsZeros");
	else if( szLbl == _T("ST_S") ) szNewLbl = _T("SubstMissingTrials");
	else if( szLbl == _T("ST_D") ) szNewLbl = _T("DiscardSubstAfter");
	else if( szLbl == _T("ST_D2") ) szNewLbl = _T("DiscardAfter");
	else if( szLbl == _T("ItemNum") ) szNewLbl = _T("Sequence");
	else fRet = FALSE;

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

#define	HEADER_FIELDS	3
BOOL DBData::DODACleanup( CString& szErr, BOOL& fFound )
{
	try
	{
		fFound = FALSE;

		// close
		Close( szErr );
		// reopen in exclusive
		if( !Open( szErr, CTOPEN_EXCLUSIVE ) ) return FALSE;

		// rewrite DODA & remove $NULFLD$ if found
		// read original field definitions from data file
		VRLEN maplen = 0 ;
		if( !( maplen = GETDODA( m_table->GetDatno(), 0, NULL, SCHEMA_MAP ) ) )
		{
			szErr.Format( _T("ERROR: Failed to get schema map size: %d"), uerr_cod );
			return FALSE;
		}
		pConvMap pschmap = NULL;
		if( !( pschmap = (pConvMap)malloc( (size_t)maplen ) ) )
		{
			szErr.Format( _T("ERROR: Failed to allocate memory for schema map") );
			return FALSE;
		}
		if( !GETDODA( m_table->GetDatno(), maplen, pschmap, SCHEMA_MAP ) )
		{
			szErr.Format( _T("ERROR: Failed to retrieve schema map: %d"), uerr_cod );
			free( pschmap );
			return FALSE;
		}
		VRLEN dodalen = 0;
		if( !( dodalen = GETDODA( m_table->GetDatno(), 0, NULL, SCHEMA_DODA ) ) )
		{
			szErr.Format( _T("ERROR: Failed to get DODA size: %d"), uerr_cod );
			free( pschmap );
			return FALSE;
		}
		pDATOBJ pdoda = NULL;
		if( !( pdoda = (pDATOBJ)malloc( (size_t)dodalen ) ) )
		{
			szErr.Format( _T("ERROR: Failed to allocate memory for DODA") );
			free( pschmap );
			return FALSE;
		}
		if( !GETDODA( m_table->GetDatno(), dodalen, pdoda, SCHEMA_DODA ) )
		{
			szErr.Format( _T("ERROR: Failed to read DODA: %d"), uerr_cod );
			free( pschmap );
			free( pdoda );
			return FALSE;
		}
		COUNT numflds = (COUNT)( pschmap->nbrflds );
		if( numflds <= 0 )
		{
			free( pschmap );
			free( pdoda );
			return FALSE;
		}
		for( NINT i = HEADER_FIELDS; i < numflds; i++ )
		{
			if( !strcmp( pdoda[ i ].fsymb, "$NULFLD$" ) )
			{
				pdoda[ i ].fsymb[ 0 ] = '_';
				fFound = TRUE;
				break;
			}
		}
		if( !fFound )
		{
			free( pschmap );
			free( pdoda );
			return TRUE;
		}
		// put doda back with renamed $NULFLD$ to _NULFLD$
		NINT rc = 0;
		if( ( rc = PUTDODA( m_table->GetDatno(), pdoda, numflds ) ) )
		{
			szErr.Format( _T("ERROR: Failed to update DODA: %d"), rc );
			free( pschmap );
			free( pdoda );
			return FALSE;
		}
		free( pschmap );
		free( pdoda );

		// close and reopen (to ensure doda changes are finalized)
		Close( szErr );
		if( !Open( szErr, CTOPEN_EXCLUSIVE ) ) return FALSE;

		// delete formerly hidden, renamed null field
		m_table->DelField( _T("_NULFLD$") );
	}
	catch( CTException E )
	{
		szErr = _T("Unable to cleanup DODA");
		DoError( &E, szErr );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ConversionWarning( DBData* pDB )
{
	if( _NeedConvertSaidNo ) return FALSE;

	// Warning (if havent already --- and if not user table)
	CString szName;
	pDB->GetObjectName( szName );
	if( szName == _T("user") ) return TRUE;

	if( !_DidConvWarning )
	{
		_DidConvWarning = TRUE;

		CString szMsg = _T("Database data has been found to be in an older format. It must be converted to the new format to continue.");
		::MessageBox( _appWnd, szMsg, _T("MovAlyzeR - Data Conversion Notice"), MB_OK );

		if( ::MessageBox( _appWnd, _T("Do you wish to convert the database files?"), _T("MovAlyzeR - Data Conversion Notice"), MB_YESNO ) == IDNO )
		{
			::MessageBox( _appWnd, _T("The application cannot be run properly without a conversion.\nYou must exit the program or accept the conversion to continue."), _T("MovAlyzeR - Unable To Continue"), MB_ICONWARNING );
			if( pDB )
			{
				CString szErr;
				szErr.GetBufferSetLength( 100 );
			}
			_NeedConvertSaidNo = TRUE;
			return FALSE;
		}

		szMsg = _T("Do you want to backup your database files first?");
		int nRes = ::MessageBox( _appWnd, szMsg, _T("MovAlyzeR - Data Conversion Notice"), MB_YESNO );
		if( nRes == IDYES ) _Backup = TRUE;
		else _Backup = FALSE;

		// backup datafiles if need be
		if( _Backup )
		{
			CTString szSrc, szDest, szName;
			pDB->GetFileName( szName );

			// .DAT file
			szSrc = _DataPath;
			szSrc += _T("\\");
			szSrc += szName;
			szSrc += _T(".dat");
			szDest = _BackupPath;
			szDest += _T("\\");
			szDest += szName;
			szDest += _T(".dat");
			CopyFile( szSrc.c_str(), szDest.c_str(), FALSE );

			// .IDX file
			szSrc = _DataPath;
			szSrc += _T("\\");
			szSrc += szName;
			szSrc += _T(".idx");
			szDest = _BackupPath;
			szDest += _T("\\");
			szDest += szName;
			szDest += _T(".idx");
			CopyFile( szSrc.c_str(), szDest.c_str(), FALSE );
		}
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#ifndef __DB_GROUP__
#define	__DB_GROUP__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Group
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct GROUPA : public DBDATA
{
	FLD_char( GrpID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_char( Notes, szNOTES );
};

typedef GROUPA* pGROUP;

// DB class
class AFX_EXT_CLASS DBGroup : public DBData
{
public:
	DBGroup() {}
	~DBGroup() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pGROUP Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBGroup* pDBGroup;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_GROUP__

#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBFeedback.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szFeedbackFile[ _MAX_PATH ] = _T("feedback");
static char szFeedbackTempFile[ _MAX_PATH ] = _T("feedbacktemp");

// global data object
FEEDBACK feedback;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBFeedback::GetObjectName( CString& szName )
{
	szName = _T("feedback");
}

///////////////////////////////////////////////////////////////////////////////

void DBFeedback::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szFeedbackFile );
		else strcpy_s( pszFile, szFeedbackTempFile );
	}
	else strcpy_s( pszFile, szFeedbackFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBFeedback::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Description", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Feature", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "MinColor", CT_INT4U, sizeof( CT_INT4U ) );
		m_table->AddField( "MaxColor", CT_INT4U, sizeof( CT_INT4U ) );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "FeedbackID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBFeedback::GetData( pDBDATA pData, CString& szErr )
{
	pFEEDBACK p = (pFEEDBACK)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->FeedbackID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Desc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Feature, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->MinColor = m_record->GetFieldAsUnsigned( ++i );
		p->MaxColor = m_record->GetFieldAsUnsigned( ++i );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBFeedback::SetData( pDBDATA pData, CString& szErr )
{
	pFEEDBACK p = (pFEEDBACK)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->FeedbackID );
		m_record->SetFieldAsString( ++i, p->Desc );
		m_record->SetFieldAsString( ++i, p->Feature );
		m_record->SetFieldAsUnsigned( ++i, p->MinColor );
		m_record->SetFieldAsUnsigned( ++i, p->MaxColor );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pFEEDBACK DBFeedback::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pFEEDBACK p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &feedback, szErr );
			p = &feedback;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBFeedback::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

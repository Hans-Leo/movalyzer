#ifndef __DB_FEEDBACK__
#define	__DB_FEEDBACK__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Feedback
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct FEEDBACK : public DBDATA
{
	FLD_char( FeedbackID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_char( Feature, szDESC );
	FLD_DWORD( MinColor );
	FLD_DWORD( MaxColor );
};

typedef FEEDBACK* pFEEDBACK;

// DB class
class AFX_EXT_CLASS DBFeedback : public DBData
{
public:
	DBFeedback() {}
	~DBFeedback() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pFEEDBACK Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBFeedback* pDBFeedback;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_FEEDBACK__

#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBBackup.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szBackupFile[ _MAX_PATH ] = _T("backup");

// global data object
BACKUP backup;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBBackup::GetObjectName( CString& szName )
{
	szName = _T("backup");
}

///////////////////////////////////////////////////////////////////////////////

void DBBackup::GetFileName( CTString& szFile )
{
	char* pszDataPath = GetDataPath();
	m_table->SetPath( pszDataPath );
	szFile = szBackupFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBBackup::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "UserID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx2 = m_table->AddField( "BackupDate", CT_FSTRING, szDATE + 1 );
		m_table->AddField( "Exp", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Grp", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Subj", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Path", CT_FSTRING, _MAX_PATH + 1 );
		m_table->AddField( "Mode", CT_INT4, sizeof( CT_INT4 ) );
		m_table->AddField( "Notes", CT_FSTRING, szNOTES + 1 );

		// add indices
		// user only
		CTIndex idx1 = m_table->AddIndex( "Backup", CTINDEX_FIXED, YES, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
		// user/date
		CTIndex idx2 = m_table->AddIndex( "BackupDate", CTINDEX_FIXED, NO, NO );
		idx2.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx2.AddSegment( fldIdx2, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBBackup::GetData( pDBDATA pData, CString& szErr )
{
	pBACKUP p = (pBACKUP)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->UserID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->BackupDate, szDATE + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Exp, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Grp, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Subj, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Path, _MAX_PATH + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		p->Mode = m_record->GetFieldAsSigned( ++i );
		strncpy_s( p->Notes, szNOTES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBBackup::SetData( pDBDATA pData, CString& szErr )
{
	pBACKUP p = (pBACKUP)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->UserID );
		m_record->SetFieldAsString( ++i, p->BackupDate );
		m_record->SetFieldAsString( ++i, p->Exp );
		m_record->SetFieldAsString( ++i, p->Grp );
		m_record->SetFieldAsString( ++i, p->Subj );
		m_record->SetFieldAsString( ++i, p->Path );
		m_record->SetFieldAsSigned( ++i, p->Mode );
		m_record->SetFieldAsString( ++i, p->Notes );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pBACKUP DBBackup::Find( CString szUserID, CString szBackupDate, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pBACKUP p = NULL;
	TEXT eid[ szIDS + 1 ], cid[ szIDS + 1 ];
	strncpy_s( eid, szUserID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( cid, szBackupDate, szIDS );
	cid[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 1 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, cid );
		// search
		if( m_record->First() )
		{
			GetData( &backup, szErr );
			p = &backup;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBBackup::Remove( CString szUserID, CString szBackupDate, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szUserID, szBackupDate, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBGroup.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szGroupFile[ _MAX_PATH ] = _T("group");
static char szGroupTempFile[ _MAX_PATH ] = _T("grouptemp");

// global data object
GROUPA group;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBGroup::GetObjectName( CString& szName )
{
	szName = _T("group");
}

///////////////////////////////////////////////////////////////////////////////

void DBGroup::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szGroupFile );
		else strcpy_s( pszFile, szGroupTempFile );
	}
	else strcpy_s( pszFile, szGroupFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGroup::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "Description", CT_FSTRING, szDESC + 1 );
		m_table->AddField( "Notes", CT_FSTRING, szNOTES + 1 );

		// add indices
		CTIndex idx1 = m_table->AddIndex( "GrpID", CTINDEX_FIXED, NO, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGroup::GetData( pDBDATA pData, CString& szErr )
{
	pGROUP p = (pGROUP)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->GrpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Desc, szDESC + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->Notes, szNOTES + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGroup::SetData( pDBDATA pData, CString& szErr )
{
	pGROUP p = (pGROUP)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->GrpID );
		m_record->SetFieldAsString( ++i, p->Desc );
		m_record->SetFieldAsString( ++i, p->Notes );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pGROUP DBGroup::Find( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pGROUP p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &group, szErr );
			p = &group;
		}
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGroup::Remove( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

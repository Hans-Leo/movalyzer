// CTreeLink.h : main header file for the CTREELINK DLL
//

#if !defined(AFX_CTREELINK_H__CF9610D8_B194_11D3_8A6A_000000000000__INCLUDED_)
#define AFX_CTREELINK_H__CF9610D8_B194_11D3_8A6A_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCTreeLinkApp
// See CTreeLink.cpp for the implementation of this class
//

class CCTreeLinkApp : public CWinApp
{
public:
	CCTreeLinkApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCTreeLinkApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCTreeLinkApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTREELINK_H__CF9610D8_B194_11D3_8A6A_000000000000__INCLUDED_)

#ifndef __DB_ELEMENT__
#define	__DB_ELEMENT__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Element
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct ELEMENT : public DBDATA
{
	FLD_char( ElmtID, szIDS );
	FLD_char( Desc, szDESC );
	FLD_char( Pattern, szFILELENGTH );
	FLD_SHORT( Shape );
	FLD_DOUBLE( CenterX );
	FLD_DOUBLE( CenterY );
	FLD_DOUBLE( WidthX );
	FLD_DOUBLE( WidthY );
	FLD_BOOL( IsTarget );
	FLD_DOUBLE( ErrorX );
	FLD_DOUBLE( ErrorY );
	FLD_SHORT( Line1Size );
	FLD_SHORT( Line2Size );
	FLD_SHORT( Line3Size );
	FLD_DWORD( Line1Color );
	FLD_DWORD( Line2Color );
	FLD_DWORD( Line3Color );
	FLD_DWORD( Bk1Color );
	FLD_DWORD( Bk2Color );
	FLD_DWORD( Bk3Color );
	FLD_char( Text1, szELEMENT_TEXT );
	FLD_char( Text2, szELEMENT_TEXT );
	FLD_char( Text3, szELEMENT_TEXT );
	FLD_SHORT( Text1Size );
	FLD_SHORT( Text2Size );
	FLD_SHORT( Text3Size );
	FLD_DWORD( Text1Color );
	FLD_DWORD( Text2Color );
	FLD_DWORD( Text3Color );
	LOGFONT		lf1;
	LOGFONT		lf2;
	LOGFONT		lf3;
	FLD_BOOL( IsImage );
	FLD_DOUBLE( Start );
	FLD_DOUBLE( Duration );
	FLD_BOOL( HideRightTarget );
	FLD_BOOL( HideWrongTarget );
	FLD_BOOL( HideIfAnyRightTarget );
	FLD_BOOL( HideIfAnyWrongTarget );
	FLD_char( RightTarget, szIDS );	// stimulus to be displayed if correct target reached
	FLD_char( WrongTarget, szIDS );	// blah blah
	FLD_char( CatID, szIDS );
	// animation related
	FLD_BOOL( Animate );		// animated means that the center x,y mean nothing
								// only the radius has meaning (dimension-wise)
	FLD_char( AnimationFile, szFILELENGTH );		// selected hwr file
	FLD_BOOL( AnimationRandom );					// random hwr file
	FLD_BOOL( AnimationGenerate );					// generate random hwr file
	// random hwr file from subject
	FLD_char( AnimationSubjID, szIDS + szIDS + szIDS );
	FLD_DOUBLE( AnimationRate );
};

typedef ELEMENT* pELEMENT;

// DB class
class AFX_EXT_CLASS DBElement : public DBData
{
public:
	DBElement() {}
	~DBElement() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// verify that the table is the latest version
	virtual BOOL Verify( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pELEMENT Find( CString szID, CString& szErr );
	// remove current record
	BOOL Remove( CString szID, CString& szErr );
};

typedef DBElement* pDBElement;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_ELEMENT__

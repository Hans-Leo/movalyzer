// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#pragma once

#define _BIND_TO_CURRENT_CRT_VERSION	1
#define _BIND_TO_CURRENT_MFC_VERSION	1

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components

extern "C" 
{
	#include "ctreep.h"
}

#include "StdAfx.h"
#include "ctdbsdk.hpp"
#include "DBGQuestionnaire.h"

///////////////////////////////////////////////////////////////////////////////

// File names (base)
static char szGQuestFile[ _MAX_PATH ] = _T("gquest");
static char szGQuestTempFile[ _MAX_PATH ] = _T("gquesttemp");

// global data object
GQUESTIONNAIRE gquest;

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void DBGQuestionnaire::GetObjectName( CString& szName )
{
	szName = _T("gquest");
}

///////////////////////////////////////////////////////////////////////////////

void DBGQuestionnaire::GetFileName( CTString& szFile )
{
	char pszFile[ _MAX_PATH ];
	if( !IsRemoteMode() )
	{
		char* pszDataPath = GetDataPath();
		if( !m_fOpen ) m_table->SetPath( pszDataPath );
		if( !m_fTemp ) strcpy_s( pszFile, szGQuestFile );
		else strcpy_s( pszFile, szGQuestTempFile );
	}
	else strcpy_s( pszFile, szGQuestFile );

	szFile = pszFile;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::CreateTable( CString& szErr )
{
	try
	{
		// add fields
		m_table->AddField( "DF", CT_INT2, sizeof( CT_INT2 ) );
		CTField fldIdx1 = m_table->AddField( "ExpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx2 = m_table->AddField( "GrpID", CT_FSTRING, szIDS + 1 );
		CTField fldIdx3 = m_table->AddField( "ID", CT_FSTRING, szIDS + 1 );
		m_table->AddField( "ItemNum", CT_FSTRING, szQUESTIONNAIRE_ITEM + 1 );

		// add indices
		// exp only
		CTIndex idx1 = m_table->AddIndex( "Exp", CTINDEX_FIXED, YES, NO );
		idx1.AddSegment( fldIdx1, CTSEG_UREGSEG );
		// exp/grp
		CTIndex idx2 = m_table->AddIndex( "ExpGrp", CTINDEX_FIXED, YES, NO );
		idx2.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx2.AddSegment( fldIdx2, CTSEG_UREGSEG );
		// exp/grp/quest
		CTIndex idx3 = m_table->AddIndex( "ExpGrpQuest", CTINDEX_FIXED, NO, NO );
		idx3.AddSegment( fldIdx1, CTSEG_UREGSEG );
		idx3.AddSegment( fldIdx2, CTSEG_UREGSEG );
		idx3.AddSegment( fldIdx3, CTSEG_UREGSEG );
		// grp only
		CTIndex idx4 = m_table->AddIndex( "Grp", CTINDEX_FIXED, YES, NO );
		idx4.AddSegment( fldIdx2, CTSEG_UREGSEG );
		// quest only
		CTIndex idx5 = m_table->AddIndex( "Quest", CTINDEX_FIXED, YES, NO );
		idx5.AddSegment( fldIdx3, CTSEG_UREGSEG );
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error in creating table %s.") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::GetData( pDBDATA pData, CString& szErr )
{
	pGQUESTIONNAIRE p = (pGQUESTIONNAIRE)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		strncpy_s( p->ExpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->GrpID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->ID, szIDS + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );
		strncpy_s( p->ItemNum, szQUESTIONNAIRE_ITEM + 1, m_record->GetFieldAsString( ++i ).c_str(), _TRUNCATE );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error reading record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::SetData( pDBDATA pData, CString& szErr )
{
	pGQUESTIONNAIRE p = (pGQUESTIONNAIRE)pData;
	if( !p ) return FALSE;

	int i = 0;

	try
	{
		m_record->SetFieldAsString( ++i, p->ExpID );
		m_record->SetFieldAsString( ++i, p->GrpID );
		m_record->SetFieldAsString( ++i, p->ID );
		m_record->SetFieldAsString( ++i, p->ItemNum );

		return TRUE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error setting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

pGQUESTIONNAIRE DBGQuestionnaire::Find( CString szExpID, CString szGrpID, CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pGQUESTIONNAIRE p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ], sid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';
	strncpy_s( sid, szID, szIDS );
	sid[ szIDS ] = '\0';

	try
	{
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 2 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->SetFieldAsString( 3, sid );
		// search
		if( m_record->Find( CTFIND_EQ ) )
		{
			GetData( &gquest, szErr );
			p = &gquest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pGQUESTIONNAIRE DBGQuestionnaire::Find( CString szExpID, CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pGQUESTIONNAIRE p = NULL;
	TEXT eid[ szIDS + 1 ], gid[ szIDS + 1 ];
	strncpy_s( eid, szExpID, szIDS );
	eid[ szIDS ] = '\0';
	strncpy_s( gid, szGrpID, szIDS );
	gid[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 1 );
		// set search criteria
		m_record->SetFieldAsString( 1, eid );
		m_record->SetFieldAsString( 2, gid );
		m_record->RecordSetOn( szIDS + 1 + szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &gquest, szErr );
			p = &gquest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pGQUESTIONNAIRE DBGQuestionnaire::Find( CString szExpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pGQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szExpID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 0 );
		// set search criteria
		m_record->SetFieldAsString( 1, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &gquest, szErr );
			p = &gquest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pGQUESTIONNAIRE DBGQuestionnaire::FindForGroup( CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pGQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szGrpID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 3 );
		// set search criteria
		m_record->SetFieldAsString( 2, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &gquest, szErr );
			p = &gquest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

pGQUESTIONNAIRE DBGQuestionnaire::FindForQuestion( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return NULL;
	if( !Open( szErr ) ) return NULL;

	pGQUESTIONNAIRE p = NULL;
	TEXT id[ szIDS + 1 ];
	strncpy_s( id, szID, szIDS );
	id[ szIDS ] = '\0';

	try
	{
		m_record->RecordSetOff();
		// clear record from any existing ties
		m_record->Clear();
		// set appropriate index
		m_record->SetDefaultIndex( 4 );
		// set search criteria
		m_record->SetFieldAsString( 3, id );
		m_record->RecordSetOn( szIDS + 1 );
		// search
		if( m_record->First() )
		{
			GetData( &gquest, szErr );
			p = &gquest;
		}
	}
	catch( CTException E )
	{
//		REPORT_ERROR( _T("Error finding record in table %s.") );
	}

	return p;
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::Remove( CString szExpID, CString szGrpID, CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szGrpID, szID, szErr ) )
		{
			// lock & delete
			m_record->LockRecord( CTLOCK_WRITE );
			m_record->Delete();

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::Remove( CString szExpID, CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szGrpID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::Remove( CString szExpID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( Find( szExpID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::RemoveGroup( CString szGrpID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( FindForGroup( szGrpID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL DBGQuestionnaire::RemoveQuestion( CString szID, CString& szErr )
{
	if( !m_table || !m_record ) return FALSE;
	if( !Open( szErr ) ) return FALSE;

	try
	{
		// locate record
		if( FindForQuestion( szID, szErr ) )
		{
			do
			{
                // lock & delete
				m_record->LockRecord( CTLOCK_WRITE );
				m_record->Delete();
			}
			while( m_record->Next() );

			return TRUE;
		}
		else return FALSE;
	}
	catch( CTException E )
	{
		REPORT_ERROR( _T("Error deleting record from table %s.") );
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

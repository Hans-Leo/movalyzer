#ifndef __DB_STIMELEMENT__
#define	__DB_STIMELEMENT__

#include "DBCommon.h"

///////////////////////////////////////////////////////////////////////////////
// Stimulus Element
///////////////////////////////////////////////////////////////////////////////

// Data structure
struct STIMULUSELEMENT : public DBDATA
{
	FLD_char( StimID, szIDS );
	FLD_char( ElmtID, szIDS );
};

typedef STIMULUSELEMENT* pSTIMULUSELEMENT;

// DB class
class AFX_EXT_CLASS DBStimulusElement : public DBData
{
public:
	DBStimulusElement() {}
	~DBStimulusElement() {}

// required virtual functions
protected:
	// table file name and location
	virtual void GetObjectName( CString& szName );
	virtual void GetFileName( CTString& szFile );
	// code to add fields, segments, indexes to table (do not call create)
	virtual BOOL CreateTable( CString& szErr );
	// code to fill in the fields from record traversal/adding/modifying
	virtual BOOL GetData( pDBDATA pData, CString& szErr );
	// code to set the fields from record traversal/adding/modifying
	virtual BOOL SetData( pDBDATA pData, CString& szErr );
public:
	// find a record
	pSTIMULUSELEMENT Find( CString szStimID, CString szElmtID, CString& szErr );
	pSTIMULUSELEMENT Find( CString szStimID, CString& szErr );
//	pSTIMULUSELEMENT NextSet( CString& szErr );
	// remove current record
	BOOL Remove( CString szStimID, CString szElmtID, CString& szErr );
};

typedef DBStimulusElement* pDBStimulusElement;

///////////////////////////////////////////////////////////////////////////////

#endif	// __DB_STIMELEMENT__

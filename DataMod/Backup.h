// Backup.h: Definition of the Backup class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BACKUP_H__F6AD3938_D6AD_413E_96E3_60B52363A1AE__INCLUDED_)
#define AFX_BACKUP_H__F6AD3938_D6AD_413E_96E3_60B52363A1AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// Backup

class BackupDB;

class Backup : 
	public IDispatchImpl<IBackup, &IID_IBackup, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Backup,&CLSID_Backup>
{
public:
	Backup();
	~Backup();

BEGIN_COM_MAP(Backup)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IBackup)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Backup) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Backup)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IBackup
public:
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR UserID, /*[in]*/ BSTR BackupDate);
	STDMETHOD(get_Notes)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Notes)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Subj)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Subj)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Grp)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Grp)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Exp)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Exp)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_BackupType)(/*[out, retval]*/ BackupType *pVal);
	STDMETHOD(put_BackupType)(/*[in]*/ BackupType newVal);
	STDMETHOD(get_Path)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Path)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_BackupDate)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_BackupDate)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_UserID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_UserID)(/*[in]*/ BSTR newVal);
protected:
	BackupDB*	m_pDB;
};

#endif // !defined(AFX_BACKUP_H__F6AD3938_D6AD_413E_96E3_60B52363A1AE__INCLUDED_)

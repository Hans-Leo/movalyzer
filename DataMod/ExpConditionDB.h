#if !defined(AFX_EXPCONDITIONDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_EXPCONDITIONDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExpConditionDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBExpCondition.h"

/////////////////////////////////////////////////////////////////////////////
// ExpConditionDB recordset

class ExpConditionDB : public CTDataDB
{
public:
	ExpConditionDB();

// Field/Param Data
	CString	m_ExpID;
	CString	m_CondID;
	int		m_Replications;

// Implementation
public:
	BOOL Find( CString szExpID, CString szCondID );
	BOOL Find( CString szExpID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pEXPERIMENTCONDITION );
	BOOL SetAll( pEXPERIMENTCONDITION );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPCONDITIONDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)

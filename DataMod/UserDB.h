#if !defined(AFX_USERDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_USERDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBUser.h"

/////////////////////////////////////////////////////////////////////////////
// UserDB recordset

class UserDB : public CTDataDB
{
public:
	UserDB();

// Field/Param Data
	CString			m_UserID;
	CString			m_Desc;
	CString			m_RootPath;
	CString			m_BackupPath;
	CString			m_Delim;
	int				m_InputType;
	int				m_TabletMode;
	BOOL			m_EntireTablet;
	BOOL			m_Log;
	BOOL			m_LogUI;
	BOOL			m_LogDB;
	BOOL			m_LogProc;
	BOOL			m_LogGraph;
	BOOL			m_LogTablet;
	BOOL			m_Alpha;
	CString			m_Pass;
	double			m_TabletWidth;
	double			m_TabletHeight;
	double			m_DisplayWidth;
	double			m_DisplayHeight;
	double			m_AspectRatioWidth;
	double			m_AspectRatioHeight;
	CString			m_CommPort;
	BOOL			m_AutoUpdate;
	BOOL			m_Private;
	CString			m_WinUser;
	BOOL			m_GenSubjID;
	CString			m_SubjStartID;
	CString			m_SubjEndID;
	CString			m_SubjCurID;
	BOOL			m_Encrypted;
	short			m_EncryptionMethod;
	CString			m_SiteID;
	CString			m_SiteDesc;
	CString			m_Signature;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ResetWarningStatus();
	BOOL SetOperationMode(BOOL ClientServerON, CString CSServer, CString CSUser, CString CSPassword);
protected:
	BOOL GetAll( pUSER );
	BOOL SetAll( pUSER );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)

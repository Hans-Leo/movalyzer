// Experiment.h: Definition of the Experiment class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols
#include "ExperimentDB.h"

/////////////////////////////////////////////////////////////////////////////
// Experiment

class Experiment : 
	public IDispatchImpl<IExperiment, &IID_IExperiment, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Experiment,&CLSID_Experiment>,
	public NSDataObject
{
public:
	Experiment();
	~Experiment();

BEGIN_COM_MAP(Experiment)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IExperiment)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Experiment) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Experiment)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IExperiment
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(put_MissingDataValue)(/*[in]*/double newVal);
	STDMETHOD(get_MissingDataValue)(/*[out,retval]*/double* pVal);
	STDMETHOD(SetAppWindow)(/*[in]*/ DWORD AppWindow);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(TestLogin)();
	STDMETHOD(SetBackupPath)(/*[in]*/ BSTR Path);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);
	STDMETHOD(SetOutputWindow)(/*[in]*/ VARIANT* MsgWindow);
	STDMETHOD(AddGroupSubject)(/*[in]*/ IGroup* pGroup, /*[in]*/ ISubject* pSubject);
	STDMETHOD(SetProcSetting)(/*[in]*/ IProcSetting* PS);
	STDMETHOD(GetProcSettings)(/*[out,retval]*/ IProcSetting* PS);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_Type)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Type)(/*[in]*/ short newVal);
	STDMETHOD(get_Notes)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Notes)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	ExperimentDB*	m_pDB;
};

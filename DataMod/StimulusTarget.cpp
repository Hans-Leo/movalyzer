// StimulusTarget.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "StimTargetDB.h"
#include "StimulusTarget.h"

/////////////////////////////////////////////////////////////////////////////
//

StimulusTarget::StimulusTarget()  : m_pDB( NULL )
{
	m_pDB = new StimTargetDB;
}

StimulusTarget::~StimulusTarget()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP StimulusTarget::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IStimulusTarget,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP StimulusTarget::get_StimulusID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StimID.AllocSysString();

	return S_OK;
}

STDMETHODIMP StimulusTarget::put_StimulusID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StimID = newVal;

	return S_OK;
}

STDMETHODIMP StimulusTarget::get_ElementID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ElmtID.AllocSysString();

	return S_OK;
}

STDMETHODIMP StimulusTarget::put_ElementID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ElmtID = newVal;

	return S_OK;
}

STDMETHODIMP StimulusTarget::Find(BSTR StimID, BSTR ElmtID, short Seq)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = StimID;
	CString szElmtID = ElmtID;
	if( !m_pDB->Find( szID, szElmtID, Seq ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::FindForStimElmt(BSTR StimID, BSTR ElmtID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = StimID;
	CString szElmtID = ElmtID;
	if( !m_pDB->Find( szID, szElmtID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::FindForStimulus(BSTR StimID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = StimID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_StimID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusTarget::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP StimulusTarget::get_Sequence(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Sequence;

	return S_OK;
}

STDMETHODIMP StimulusTarget::put_Sequence(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Sequence = newVal;

	return S_OK;
}

STDMETHODIMP StimulusTarget::ReleaseData()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ReleaseDB();

	return S_OK;
}

STDMETHODIMP StimulusTarget::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP StimulusTarget::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP StimulusTarget::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

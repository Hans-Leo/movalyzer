// StimulusElement.h: Definition of the StimulusElement class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STIMULUSELEMENT_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_STIMULUSELEMENT_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// StimulusElement

class StimElementDB;

class StimulusElement : 
	public IDispatchImpl<IStimulusElement, &IID_IStimulusElement, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<StimulusElement,&CLSID_StimulusElement>
{
public:
	StimulusElement();
	~StimulusElement();

BEGIN_COM_MAP(StimulusElement)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IStimulusElement)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(StimulusElement) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_StimulusElement)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IStimulusElement
public:
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(FindForStimulus)(/*[in]*/ BSTR StimID);
	STDMETHOD(Find)(/*[in]*/ BSTR StimID, /*[in]*/ BSTR ElmtID);
	STDMETHOD(get_ElementID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ElementID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_StimulusID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_StimulusID)(/*[in]*/ BSTR newVal);
protected:
	StimElementDB*	m_pDB;
};

#endif // !defined(AFX_STIMULUSELEMENT_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_)

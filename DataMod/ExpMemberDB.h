#if !defined(AFX_EXPMEMBERDB_H__DCB05ED6_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_EXPMEMBERDB_H__DCB05ED6_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExpMemberDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBExpMember.h"

/////////////////////////////////////////////////////////////////////////////
// ExpMemberDB recordset

class ExpMemberDB : public CTDataDB
{
public:
	ExpMemberDB();

// Field/Param Data
	CString			m_ExpID;
	CString			m_GrpID;
	CString			m_SubjID;
	double			m_DeviceRes;
	int				m_SamplingRate;
	BOOL			m_Exported;
	COleDateTime	m_ExpWhen;
	CString			m_ExpWhere;
	BOOL			m_Uploaded;
	COleDateTime	m_UpWhen;
	CString			m_UpWhere;

// Implementation
public:
	BOOL Find( CString szExpID, CString szGrpID, CString szSubjID );
	BOOL Find( CString szExpID, CString szGrpID );
	BOOL Find( CString szExpID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	BOOL Remove( CString szExpID, CString szGrpID );
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pEXPERIMENTMEMBER );
	BOOL SetAll( pEXPERIMENTMEMBER );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPMEMBERDB_H__DCB05ED6_A758_11D3_8A58_000000000000__INCLUDED_)

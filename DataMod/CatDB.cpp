// CatDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CatDB.h"

DBCat _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CatDB

CatDB::CatDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(CatDB)
	m_CatID = _T("");
	m_Desc = _T("");
	m_Type = 0;
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL CatDB::GetAll( pCAT pCat )
{
	if( !pCat ) return FALSE;

	m_CatID = pCat->CatID;
	m_Desc = pCat->Desc;
	m_Type = pCat->Type;

	return TRUE;
}

BOOL CatDB::SetAll( pCAT pCat )
{
	if( !pCat ) return FALSE;

	strncpy_s( pCat->CatID, szIDS + 1, m_CatID, _TRUNCATE );
	strncpy_s( pCat->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	pCat->Type = m_Type;

	return TRUE;
}

BOOL CatDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pCAT pCat = _db.Find( szID, szErr );
	if( !pCat )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pCat );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CatDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CATA grp;
	SetAll( &grp );
	if( !AddItem( (pDBDATA)&grp, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CatDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CATA grp;
	SetAll( &grp );
	if( !ModifyItem( (pDBDATA)&grp, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CatDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_CatID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void CatDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CATA grp;
	if( GetFirstItem( (pDBDATA)&grp, szErr ) )
		GetAll( &grp );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CatDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CATA grp;
	if( GetNextItem( (pDBDATA)&grp, szErr ) )
	{
		GetAll( &grp );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CatDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CATA grp;
	if( GetPreviousItem( (pDBDATA)&grp, szErr ) )
	{
		GetAll( &grp );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void CatDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void CatDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void CatDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

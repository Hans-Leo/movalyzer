//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DataMod.rc
//
#define IDR_Backup                      300
#define IDR_CAT                         301
#define IDR_Condition                   302
#define IDR_Element                     303
#define IDR_Experiment                  304
#define IDR_ExperimentCondition         305
#define IDR_ExperimentMember            306
#define IDR_FEEDBACK                    307
#define IDR_GQuestionnaire              308
#define IDR_Group                       309
#define IDR_MQuestionnaire              310
#define IDR_NSMUser                     311
#define IDR_ProcSetting                 312
#define IDR_SQuestionnaire              313
#define IDR_Stimulus                    314
#define IDR_StimulusElement             315
#define IDR_StimulusTarget              316
#define IDR_Subject                     317
#define IDS_ADDSUBJ_ERR                 318
#define IDS_BACKUP_DESC                 319
#define IDS_CONDITION_DESC              320
#define IDS_ELEMENT_DESC                321
#define IDS_EXPERIMENT_DESC             322
#define IDS_EXPERIMENTCONDITION_DESC    323
#define IDS_EXPERIMENTCONDITIONLIST_DESC 324
#define IDS_EXPERIMENTMEMBER_DESC       325
#define IDS_EXPERIMENTMEMBERLIST_DESC   326
#define IDS_GQUESTIONNAIRE_DESC         327
#define IDS_GROUP_DESC                  328
#define IDS_NSMUSER_DESC                329
#define IDS_PROCSETTING_DESC            330
#define IDS_PROJNAME                    331
#define IDS_QUESTIONNAIRE_DESC          332
#define IDS_SQUESTIONNAIRE_DESC         333
#define IDS_STIMULUS_DESC               334
#define IDS_STIMULUSELEMENT_DESC        335
#define IDS_STIMULUSELEMENTLIST_DESC    336
#define IDS_STIMULUSTARGET_DESC         337
#define IDS_STIMULUSTARGETLIST_DESC     338
#define IDS_SUBJECT_DESC                339

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        340
#define _APS_NEXT_COMMAND_VALUE         32767
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           1100
#endif
#endif

// ExperimentMember.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "ExpMemberDB.h"
#include "ExperimentMember.h"

/////////////////////////////////////////////////////////////////////////////
//

ExperimentMember::ExperimentMember()  : m_pDB( NULL )
{
	m_pDB = new ExpMemberDB;
}

ExperimentMember::~ExperimentMember()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP ExperimentMember::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IExperimentMember,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP ExperimentMember::get_ExperimentID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_ExperimentID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpID = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_GroupID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_GrpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_GroupID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_GrpID = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_SubjectID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjID.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_SubjectID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjID = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_DeviceRes(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DeviceRes;

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_DeviceRes(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DeviceRes = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_SamplingRate(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SamplingRate;

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_SamplingRate(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SamplingRate = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_Exported(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Exported;

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_Exported(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Exported = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_ExpWhen(DATE *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpWhen;

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_ExpWhen(DATE newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpWhen = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_ExpWhere(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpWhere.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_ExpWhere(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpWhere = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_Uploaded(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Uploaded;

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_Uploaded(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Uploaded = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_UpWhen(DATE *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_UpWhen;

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_UpWhen(DATE newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_UpWhen = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::get_UpWhere(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_UpWhere.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentMember::put_UpWhere(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_UpWhere = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentMember::Find(BSTR ExpID, BSTR GrpID, BSTR SubjID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	CString szSubjID = SubjID;
	if( !m_pDB->Find( szExpID, szGrpID, szSubjID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::FindForGroup(BSTR ExpID, BSTR GrpID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	if( !m_pDB->Find( szExpID, szGrpID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::FindForExperiment(BSTR ExpID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ExpID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::RemoveGroup(BSTR ExpID, BSTR GrpID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	if( !m_pDB->Remove( szExpID, szGrpID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ExpID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentMember::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP ExperimentMember::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetDBLogOn( fOn );

	return S_OK;
}

STDMETHODIMP ExperimentMember::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP ExperimentMember::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP ExperimentMember::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

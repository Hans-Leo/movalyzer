// Group.h: Definition of the Group class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GROUP_H__DCB05EE1_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_GROUP_H__DCB05EE1_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "GroupDB.h"

/////////////////////////////////////////////////////////////////////////////
// Group

class GroupDB;

class Group : 
	public IDispatchImpl<IGroup, &IID_IGroup, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Group,&CLSID_Group>,
	public NSDataObject
{
public:
	Group();
	~Group();

BEGIN_COM_MAP(Group)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IGroup)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Group) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Group)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IGroup
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_Notes)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Notes)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	GroupDB*	m_pDB;
};

#endif // !defined(AFX_GROUP_H__DCB05EE1_A758_11D3_8A58_000000000000__INCLUDED_)

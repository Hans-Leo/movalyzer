// StimElementDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "StimElementDB.h"

DBStimulusElement _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StimElementDB

StimElementDB::StimElementDB() : CTDataDB(&_db)
{
	//{{AFX_FIELD_INIT(StimElementDB)
	m_StimID = _T("");
	m_ElmtID = _T("");
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::GetAll( pSTIMULUSELEMENT pSE )
{
	if( !pSE ) return FALSE;

	m_StimID = pSE->StimID;
	m_ElmtID = pSE->ElmtID;

	return TRUE;
}

BOOL StimElementDB::SetAll( pSTIMULUSELEMENT pSE )
{
	if( !pSE ) return FALSE;

	strncpy_s( pSE->StimID, szIDS + 1, m_StimID, _TRUNCATE );
	strncpy_s( pSE->ElmtID, szIDS + 1, m_ElmtID, _TRUNCATE );

	return TRUE;
}

BOOL StimElementDB::Find( CString szStimID, CString szElmtID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSTIMULUSELEMENT pSE = _db.Find( szStimID, szElmtID, szErr );
	if( !pSE )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pSE );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::Find( CString szStimID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSTIMULUSELEMENT pSE = _db.Find( szStimID, szErr );
	if( !pSE )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pSE );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSELEMENT se;
	SetAll( &se );
	if( !AddItem( (pDBDATA)&se, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSELEMENT se;
	SetAll( &se );
	if( !ModifyItem( (pDBDATA)&se, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_StimID, m_ElmtID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void StimElementDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSELEMENT se;
	if( GetFirstItem( (pDBDATA)&se, szErr ) )
		GetAll( &se );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSELEMENT se;
	if( GetNextItem( (pDBDATA)&se, szErr ) )
	{
		GetAll( &se );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimElementDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSELEMENT se;
	if( GetPreviousItem( (pDBDATA)&se, szErr ) )
	{
		GetAll( &se );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimElementDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimElementDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimElementDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

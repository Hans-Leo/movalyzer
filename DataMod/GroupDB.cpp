// GroupDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GroupDB.h"

DBGroup _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GroupDB

GroupDB::GroupDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(GroupDB)
	m_GrpID = _T("");
	m_Desc = _T("");
	m_Notes = _T("");
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL GroupDB::GetAll( pGROUP pGroup )
{
	if( !pGroup ) return FALSE;

	m_GrpID = pGroup->GrpID;
	m_Desc = pGroup->Desc;
	m_Notes = pGroup->Notes;

	return TRUE;
}

BOOL GroupDB::SetAll( pGROUP pGroup )
{
	if( !pGroup ) return FALSE;

	strcpy_s( pGroup->GrpID, szIDS + 1, m_GrpID );
	strcpy_s( pGroup->Desc, szDESC + 1, m_Desc );
	strcpy_s( pGroup->Notes, szNOTES + 1, m_Notes );

	return TRUE;
}

BOOL GroupDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pGROUP pGroup = _db.Find( szID, szErr );
	if( !pGroup )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pGroup );
}

/////////////////////////////////////////////////////////////////////////////

BOOL GroupDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GROUPA grp;
	SetAll( &grp );
	if( !AddItem( (pDBDATA)&grp, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GroupDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GROUPA grp;
	SetAll( &grp );
	if( !ModifyItem( (pDBDATA)&grp, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GroupDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_GrpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void GroupDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GROUPA grp;
	if( GetFirstItem( (pDBDATA)&grp, szErr ) )
		GetAll( &grp );
}

/////////////////////////////////////////////////////////////////////////////

BOOL GroupDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GROUPA grp;
	if( GetNextItem( (pDBDATA)&grp, szErr ) )
	{
		GetAll( &grp );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GroupDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GROUPA grp;
	if( GetPreviousItem( (pDBDATA)&grp, szErr ) )
	{
		GetAll( &grp );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void GroupDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void GroupDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void GroupDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

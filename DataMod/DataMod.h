

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sat Apr 02 18:54:48 2016
 */
/* Compiler settings for .\DataMod.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __DataMod_h__
#define __DataMod_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __INSDataObject_FWD_DEFINED__
#define __INSDataObject_FWD_DEFINED__
typedef interface INSDataObject INSDataObject;
#endif 	/* __INSDataObject_FWD_DEFINED__ */


#ifndef __IExperiment_FWD_DEFINED__
#define __IExperiment_FWD_DEFINED__
typedef interface IExperiment IExperiment;
#endif 	/* __IExperiment_FWD_DEFINED__ */


#ifndef __ISubject_FWD_DEFINED__
#define __ISubject_FWD_DEFINED__
typedef interface ISubject ISubject;
#endif 	/* __ISubject_FWD_DEFINED__ */


#ifndef __IGroup_FWD_DEFINED__
#define __IGroup_FWD_DEFINED__
typedef interface IGroup IGroup;
#endif 	/* __IGroup_FWD_DEFINED__ */


#ifndef __INSCondition_FWD_DEFINED__
#define __INSCondition_FWD_DEFINED__
typedef interface INSCondition INSCondition;
#endif 	/* __INSCondition_FWD_DEFINED__ */


#ifndef __IProcSetting_FWD_DEFINED__
#define __IProcSetting_FWD_DEFINED__
typedef interface IProcSetting IProcSetting;
#endif 	/* __IProcSetting_FWD_DEFINED__ */


#ifndef __IProcSetFlags_FWD_DEFINED__
#define __IProcSetFlags_FWD_DEFINED__
typedef interface IProcSetFlags IProcSetFlags;
#endif 	/* __IProcSetFlags_FWD_DEFINED__ */


#ifndef __INSConditionSound_FWD_DEFINED__
#define __INSConditionSound_FWD_DEFINED__
typedef interface INSConditionSound INSConditionSound;
#endif 	/* __INSConditionSound_FWD_DEFINED__ */


#ifndef __IProcSetRunExp_FWD_DEFINED__
#define __IProcSetRunExp_FWD_DEFINED__
typedef interface IProcSetRunExp IProcSetRunExp;
#endif 	/* __IProcSetRunExp_FWD_DEFINED__ */


#ifndef __IExperimentMember_FWD_DEFINED__
#define __IExperimentMember_FWD_DEFINED__
typedef interface IExperimentMember IExperimentMember;
#endif 	/* __IExperimentMember_FWD_DEFINED__ */


#ifndef __IExperimentCondition_FWD_DEFINED__
#define __IExperimentCondition_FWD_DEFINED__
typedef interface IExperimentCondition IExperimentCondition;
#endif 	/* __IExperimentCondition_FWD_DEFINED__ */


#ifndef __IMQuestionnaire_FWD_DEFINED__
#define __IMQuestionnaire_FWD_DEFINED__
typedef interface IMQuestionnaire IMQuestionnaire;
#endif 	/* __IMQuestionnaire_FWD_DEFINED__ */


#ifndef __IGQuestionnaire_FWD_DEFINED__
#define __IGQuestionnaire_FWD_DEFINED__
typedef interface IGQuestionnaire IGQuestionnaire;
#endif 	/* __IGQuestionnaire_FWD_DEFINED__ */


#ifndef __ISQuestionnaire_FWD_DEFINED__
#define __ISQuestionnaire_FWD_DEFINED__
typedef interface ISQuestionnaire ISQuestionnaire;
#endif 	/* __ISQuestionnaire_FWD_DEFINED__ */


#ifndef __INSMUser_FWD_DEFINED__
#define __INSMUser_FWD_DEFINED__
typedef interface INSMUser INSMUser;
#endif 	/* __INSMUser_FWD_DEFINED__ */


#ifndef __IBackup_FWD_DEFINED__
#define __IBackup_FWD_DEFINED__
typedef interface IBackup IBackup;
#endif 	/* __IBackup_FWD_DEFINED__ */


#ifndef __IElement_FWD_DEFINED__
#define __IElement_FWD_DEFINED__
typedef interface IElement IElement;
#endif 	/* __IElement_FWD_DEFINED__ */


#ifndef __IStimulus_FWD_DEFINED__
#define __IStimulus_FWD_DEFINED__
typedef interface IStimulus IStimulus;
#endif 	/* __IStimulus_FWD_DEFINED__ */


#ifndef __IStimulusElement_FWD_DEFINED__
#define __IStimulusElement_FWD_DEFINED__
typedef interface IStimulusElement IStimulusElement;
#endif 	/* __IStimulusElement_FWD_DEFINED__ */


#ifndef __IStimulusTarget_FWD_DEFINED__
#define __IStimulusTarget_FWD_DEFINED__
typedef interface IStimulusTarget IStimulusTarget;
#endif 	/* __IStimulusTarget_FWD_DEFINED__ */


#ifndef __ICat_FWD_DEFINED__
#define __ICat_FWD_DEFINED__
typedef interface ICat ICat;
#endif 	/* __ICat_FWD_DEFINED__ */


#ifndef __IFeedback_FWD_DEFINED__
#define __IFeedback_FWD_DEFINED__
typedef interface IFeedback IFeedback;
#endif 	/* __IFeedback_FWD_DEFINED__ */


#ifndef __Experiment_FWD_DEFINED__
#define __Experiment_FWD_DEFINED__

#ifdef __cplusplus
typedef class Experiment Experiment;
#else
typedef struct Experiment Experiment;
#endif /* __cplusplus */

#endif 	/* __Experiment_FWD_DEFINED__ */


#ifndef __Subject_FWD_DEFINED__
#define __Subject_FWD_DEFINED__

#ifdef __cplusplus
typedef class Subject Subject;
#else
typedef struct Subject Subject;
#endif /* __cplusplus */

#endif 	/* __Subject_FWD_DEFINED__ */


#ifndef __Group_FWD_DEFINED__
#define __Group_FWD_DEFINED__

#ifdef __cplusplus
typedef class Group Group;
#else
typedef struct Group Group;
#endif /* __cplusplus */

#endif 	/* __Group_FWD_DEFINED__ */


#ifndef __NSCondition_FWD_DEFINED__
#define __NSCondition_FWD_DEFINED__

#ifdef __cplusplus
typedef class NSCondition NSCondition;
#else
typedef struct NSCondition NSCondition;
#endif /* __cplusplus */

#endif 	/* __NSCondition_FWD_DEFINED__ */


#ifndef __ProcSetting_FWD_DEFINED__
#define __ProcSetting_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProcSetting ProcSetting;
#else
typedef struct ProcSetting ProcSetting;
#endif /* __cplusplus */

#endif 	/* __ProcSetting_FWD_DEFINED__ */


#ifndef __ExperimentMember_FWD_DEFINED__
#define __ExperimentMember_FWD_DEFINED__

#ifdef __cplusplus
typedef class ExperimentMember ExperimentMember;
#else
typedef struct ExperimentMember ExperimentMember;
#endif /* __cplusplus */

#endif 	/* __ExperimentMember_FWD_DEFINED__ */


#ifndef __ExperimentCondition_FWD_DEFINED__
#define __ExperimentCondition_FWD_DEFINED__

#ifdef __cplusplus
typedef class ExperimentCondition ExperimentCondition;
#else
typedef struct ExperimentCondition ExperimentCondition;
#endif /* __cplusplus */

#endif 	/* __ExperimentCondition_FWD_DEFINED__ */


#ifndef __MQuestionnaire_FWD_DEFINED__
#define __MQuestionnaire_FWD_DEFINED__

#ifdef __cplusplus
typedef class MQuestionnaire MQuestionnaire;
#else
typedef struct MQuestionnaire MQuestionnaire;
#endif /* __cplusplus */

#endif 	/* __MQuestionnaire_FWD_DEFINED__ */


#ifndef __GQuestionnaire_FWD_DEFINED__
#define __GQuestionnaire_FWD_DEFINED__

#ifdef __cplusplus
typedef class GQuestionnaire GQuestionnaire;
#else
typedef struct GQuestionnaire GQuestionnaire;
#endif /* __cplusplus */

#endif 	/* __GQuestionnaire_FWD_DEFINED__ */


#ifndef __SQuestionnaire_FWD_DEFINED__
#define __SQuestionnaire_FWD_DEFINED__

#ifdef __cplusplus
typedef class SQuestionnaire SQuestionnaire;
#else
typedef struct SQuestionnaire SQuestionnaire;
#endif /* __cplusplus */

#endif 	/* __SQuestionnaire_FWD_DEFINED__ */


#ifndef __NSMUser_FWD_DEFINED__
#define __NSMUser_FWD_DEFINED__

#ifdef __cplusplus
typedef class NSMUser NSMUser;
#else
typedef struct NSMUser NSMUser;
#endif /* __cplusplus */

#endif 	/* __NSMUser_FWD_DEFINED__ */


#ifndef __Backup_FWD_DEFINED__
#define __Backup_FWD_DEFINED__

#ifdef __cplusplus
typedef class Backup Backup;
#else
typedef struct Backup Backup;
#endif /* __cplusplus */

#endif 	/* __Backup_FWD_DEFINED__ */


#ifndef __Element_FWD_DEFINED__
#define __Element_FWD_DEFINED__

#ifdef __cplusplus
typedef class Element Element;
#else
typedef struct Element Element;
#endif /* __cplusplus */

#endif 	/* __Element_FWD_DEFINED__ */


#ifndef __Stimulus_FWD_DEFINED__
#define __Stimulus_FWD_DEFINED__

#ifdef __cplusplus
typedef class Stimulus Stimulus;
#else
typedef struct Stimulus Stimulus;
#endif /* __cplusplus */

#endif 	/* __Stimulus_FWD_DEFINED__ */


#ifndef __StimulusElement_FWD_DEFINED__
#define __StimulusElement_FWD_DEFINED__

#ifdef __cplusplus
typedef class StimulusElement StimulusElement;
#else
typedef struct StimulusElement StimulusElement;
#endif /* __cplusplus */

#endif 	/* __StimulusElement_FWD_DEFINED__ */


#ifndef __StimulusTarget_FWD_DEFINED__
#define __StimulusTarget_FWD_DEFINED__

#ifdef __cplusplus
typedef class StimulusTarget StimulusTarget;
#else
typedef struct StimulusTarget StimulusTarget;
#endif /* __cplusplus */

#endif 	/* __StimulusTarget_FWD_DEFINED__ */


#ifndef __Cat_FWD_DEFINED__
#define __Cat_FWD_DEFINED__

#ifdef __cplusplus
typedef class Cat Cat;
#else
typedef struct Cat Cat;
#endif /* __cplusplus */

#endif 	/* __Cat_FWD_DEFINED__ */


#ifndef __Feedback_FWD_DEFINED__
#define __Feedback_FWD_DEFINED__

#ifdef __cplusplus
typedef class Feedback Feedback;
#else
typedef struct Feedback Feedback;
#endif /* __cplusplus */

#endif 	/* __Feedback_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_DataMod_0000_0000 */
/* [local] */ 

#pragma once





extern RPC_IF_HANDLE __MIDL_itf_DataMod_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_DataMod_0000_0000_v0_0_s_ifspec;

#ifndef __INSDataObject_INTERFACE_DEFINED__
#define __INSDataObject_INTERFACE_DEFINED__

/* interface INSDataObject */
/* [object][unique][helpstring][dual][uuid] */ 


EXTERN_C const IID IID_INSDataObject;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("ACB05ED9-A758-11D3-8A58-000000000000")
    INSDataObject : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetNumRecords( 
            /* [retval][out] */ ULONGLONG *Cnt) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSDataObjectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSDataObject * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSDataObject * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSDataObject * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSDataObject * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSDataObject * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSDataObject * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSDataObject * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            INSDataObject * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        END_INTERFACE
    } INSDataObjectVtbl;

    interface INSDataObject
    {
        CONST_VTBL struct INSDataObjectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSDataObject_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSDataObject_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSDataObject_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSDataObject_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSDataObject_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSDataObject_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSDataObject_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSDataObject_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INSDataObject_INTERFACE_DEFINED__ */


#ifndef __IExperiment_INTERFACE_DEFINED__
#define __IExperiment_INTERFACE_DEFINED__

/* interface IExperiment */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IExperiment;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05ED9-A758-11D3-8A58-000000000000")
    IExperiment : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetProcSettings( 
            /* [retval][out] */ IProcSetting *PS) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetProcSetting( 
            /* [in] */ IProcSetting *PS) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddGroupSubject( 
            /* [in] */ IGroup *pGroup,
            /* [in] */ ISubject *pSubject) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetOutputWindow( 
            /* [in] */ VARIANT *MsgWindow) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Notes( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Notes( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Type( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Type( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBackupPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE TestLogin( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAppWindow( 
            /* [in] */ DWORD AppWindow) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MissingDataValue( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MissingDataValue( 
            /* [in] */ double newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IExperimentVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IExperiment * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IExperiment * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IExperiment * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IExperiment * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IExperiment * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IExperiment * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IExperiment * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            IExperiment * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IExperiment * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IExperiment * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IExperiment * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            IExperiment * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IExperiment * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetProcSettings )( 
            IExperiment * This,
            /* [retval][out] */ IProcSetting *PS);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetProcSetting )( 
            IExperiment * This,
            /* [in] */ IProcSetting *PS);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddGroupSubject )( 
            IExperiment * This,
            /* [in] */ IGroup *pGroup,
            /* [in] */ ISubject *pSubject);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetOutputWindow )( 
            IExperiment * This,
            /* [in] */ VARIANT *MsgWindow);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IExperiment * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IExperiment * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Notes )( 
            IExperiment * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Notes )( 
            IExperiment * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Type )( 
            IExperiment * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Type )( 
            IExperiment * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBackupPath )( 
            IExperiment * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TestLogin )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IExperiment * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IExperiment * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IExperiment * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAppWindow )( 
            IExperiment * This,
            /* [in] */ DWORD AppWindow);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MissingDataValue )( 
            IExperiment * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MissingDataValue )( 
            IExperiment * This,
            /* [in] */ double newVal);
        
        END_INTERFACE
    } IExperimentVtbl;

    interface IExperiment
    {
        CONST_VTBL struct IExperimentVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IExperiment_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IExperiment_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IExperiment_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IExperiment_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IExperiment_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IExperiment_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IExperiment_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IExperiment_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define IExperiment_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IExperiment_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IExperiment_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define IExperiment_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define IExperiment_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IExperiment_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IExperiment_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IExperiment_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IExperiment_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IExperiment_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IExperiment_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IExperiment_GetProcSettings(This,PS)	\
    ( (This)->lpVtbl -> GetProcSettings(This,PS) ) 

#define IExperiment_SetProcSetting(This,PS)	\
    ( (This)->lpVtbl -> SetProcSetting(This,PS) ) 

#define IExperiment_AddGroupSubject(This,pGroup,pSubject)	\
    ( (This)->lpVtbl -> AddGroupSubject(This,pGroup,pSubject) ) 

#define IExperiment_SetOutputWindow(This,MsgWindow)	\
    ( (This)->lpVtbl -> SetOutputWindow(This,MsgWindow) ) 

#define IExperiment_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IExperiment_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IExperiment_get_Notes(This,pVal)	\
    ( (This)->lpVtbl -> get_Notes(This,pVal) ) 

#define IExperiment_put_Notes(This,newVal)	\
    ( (This)->lpVtbl -> put_Notes(This,newVal) ) 

#define IExperiment_get_Type(This,pVal)	\
    ( (This)->lpVtbl -> get_Type(This,pVal) ) 

#define IExperiment_put_Type(This,newVal)	\
    ( (This)->lpVtbl -> put_Type(This,newVal) ) 

#define IExperiment_SetBackupPath(This,Path)	\
    ( (This)->lpVtbl -> SetBackupPath(This,Path) ) 

#define IExperiment_TestLogin(This)	\
    ( (This)->lpVtbl -> TestLogin(This) ) 

#define IExperiment_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IExperiment_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IExperiment_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IExperiment_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IExperiment_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IExperiment_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#define IExperiment_SetAppWindow(This,AppWindow)	\
    ( (This)->lpVtbl -> SetAppWindow(This,AppWindow) ) 

#define IExperiment_get_MissingDataValue(This,pVal)	\
    ( (This)->lpVtbl -> get_MissingDataValue(This,pVal) ) 

#define IExperiment_put_MissingDataValue(This,newVal)	\
    ( (This)->lpVtbl -> put_MissingDataValue(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IExperiment_INTERFACE_DEFINED__ */


#ifndef __ISubject_INTERFACE_DEFINED__
#define __ISubject_INTERFACE_DEFINED__

/* interface ISubject */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ISubject;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05EDC-A758-11D3-8A58-000000000000")
    ISubject : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LastName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LastName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FirstName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FirstName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Notes( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Notes( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DateAdded( 
            /* [retval][out] */ DATE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DateAdded( 
            /* [in] */ DATE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PrvNotes( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PrvNotes( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DefaultExperiment( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DefaultExperiment( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Active( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Active( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExperimentCount( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExperimentCount( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Encrypted( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Encrypted( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Code( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Code( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EncryptionMethod( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EncryptionMethod( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SiteID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SiteID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SiteDesc( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SiteDesc( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Signature( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Signature( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Password( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Password( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISubjectVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISubject * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISubject * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISubject * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISubject * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISubject * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISubject * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISubject * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            ISubject * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LastName )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LastName )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FirstName )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FirstName )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Notes )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Notes )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            ISubject * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            ISubject * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DateAdded )( 
            ISubject * This,
            /* [retval][out] */ DATE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DateAdded )( 
            ISubject * This,
            /* [in] */ DATE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PrvNotes )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PrvNotes )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            ISubject * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            ISubject * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DefaultExperiment )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DefaultExperiment )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Active )( 
            ISubject * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Active )( 
            ISubject * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExperimentCount )( 
            ISubject * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExperimentCount )( 
            ISubject * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            ISubject * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            ISubject * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            ISubject * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Encrypted )( 
            ISubject * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Encrypted )( 
            ISubject * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Code )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Code )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EncryptionMethod )( 
            ISubject * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EncryptionMethod )( 
            ISubject * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SiteID )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SiteID )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SiteDesc )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SiteDesc )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Signature )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Signature )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Password )( 
            ISubject * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Password )( 
            ISubject * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ISubjectVtbl;

    interface ISubject
    {
        CONST_VTBL struct ISubjectVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISubject_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISubject_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISubject_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISubject_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ISubject_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ISubject_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ISubject_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ISubject_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define ISubject_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define ISubject_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define ISubject_get_LastName(This,pVal)	\
    ( (This)->lpVtbl -> get_LastName(This,pVal) ) 

#define ISubject_put_LastName(This,newVal)	\
    ( (This)->lpVtbl -> put_LastName(This,newVal) ) 

#define ISubject_get_FirstName(This,pVal)	\
    ( (This)->lpVtbl -> get_FirstName(This,pVal) ) 

#define ISubject_put_FirstName(This,newVal)	\
    ( (This)->lpVtbl -> put_FirstName(This,newVal) ) 

#define ISubject_get_Notes(This,pVal)	\
    ( (This)->lpVtbl -> get_Notes(This,pVal) ) 

#define ISubject_put_Notes(This,newVal)	\
    ( (This)->lpVtbl -> put_Notes(This,newVal) ) 

#define ISubject_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define ISubject_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define ISubject_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define ISubject_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define ISubject_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define ISubject_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define ISubject_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define ISubject_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define ISubject_get_DateAdded(This,pVal)	\
    ( (This)->lpVtbl -> get_DateAdded(This,pVal) ) 

#define ISubject_put_DateAdded(This,newVal)	\
    ( (This)->lpVtbl -> put_DateAdded(This,newVal) ) 

#define ISubject_get_PrvNotes(This,pVal)	\
    ( (This)->lpVtbl -> get_PrvNotes(This,pVal) ) 

#define ISubject_put_PrvNotes(This,newVal)	\
    ( (This)->lpVtbl -> put_PrvNotes(This,newVal) ) 

#define ISubject_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define ISubject_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define ISubject_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define ISubject_get_DefaultExperiment(This,pVal)	\
    ( (This)->lpVtbl -> get_DefaultExperiment(This,pVal) ) 

#define ISubject_put_DefaultExperiment(This,newVal)	\
    ( (This)->lpVtbl -> put_DefaultExperiment(This,newVal) ) 

#define ISubject_get_Active(This,pVal)	\
    ( (This)->lpVtbl -> get_Active(This,pVal) ) 

#define ISubject_put_Active(This,newVal)	\
    ( (This)->lpVtbl -> put_Active(This,newVal) ) 

#define ISubject_get_ExperimentCount(This,pVal)	\
    ( (This)->lpVtbl -> get_ExperimentCount(This,pVal) ) 

#define ISubject_put_ExperimentCount(This,newVal)	\
    ( (This)->lpVtbl -> put_ExperimentCount(This,newVal) ) 

#define ISubject_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define ISubject_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define ISubject_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#define ISubject_get_Encrypted(This,pVal)	\
    ( (This)->lpVtbl -> get_Encrypted(This,pVal) ) 

#define ISubject_put_Encrypted(This,newVal)	\
    ( (This)->lpVtbl -> put_Encrypted(This,newVal) ) 

#define ISubject_get_Code(This,pVal)	\
    ( (This)->lpVtbl -> get_Code(This,pVal) ) 

#define ISubject_put_Code(This,newVal)	\
    ( (This)->lpVtbl -> put_Code(This,newVal) ) 

#define ISubject_get_EncryptionMethod(This,pVal)	\
    ( (This)->lpVtbl -> get_EncryptionMethod(This,pVal) ) 

#define ISubject_put_EncryptionMethod(This,newVal)	\
    ( (This)->lpVtbl -> put_EncryptionMethod(This,newVal) ) 

#define ISubject_get_SiteID(This,pVal)	\
    ( (This)->lpVtbl -> get_SiteID(This,pVal) ) 

#define ISubject_put_SiteID(This,newVal)	\
    ( (This)->lpVtbl -> put_SiteID(This,newVal) ) 

#define ISubject_get_SiteDesc(This,pVal)	\
    ( (This)->lpVtbl -> get_SiteDesc(This,pVal) ) 

#define ISubject_put_SiteDesc(This,newVal)	\
    ( (This)->lpVtbl -> put_SiteDesc(This,newVal) ) 

#define ISubject_get_Signature(This,pVal)	\
    ( (This)->lpVtbl -> get_Signature(This,pVal) ) 

#define ISubject_put_Signature(This,newVal)	\
    ( (This)->lpVtbl -> put_Signature(This,newVal) ) 

#define ISubject_get_Password(This,pVal)	\
    ( (This)->lpVtbl -> get_Password(This,pVal) ) 

#define ISubject_put_Password(This,newVal)	\
    ( (This)->lpVtbl -> put_Password(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISubject_INTERFACE_DEFINED__ */


#ifndef __IGroup_INTERFACE_DEFINED__
#define __IGroup_INTERFACE_DEFINED__

/* interface IGroup */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IGroup;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05EDF-A758-11D3-8A58-000000000000")
    IGroup : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Notes( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Notes( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGroupVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGroup * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGroup * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGroup * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGroup * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGroup * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGroup * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGroup * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            IGroup * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IGroup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IGroup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IGroup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            IGroup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IGroup * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IGroup * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Notes )( 
            IGroup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Notes )( 
            IGroup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IGroup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IGroup * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IGroup * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IGroup * This);
        
        END_INTERFACE
    } IGroupVtbl;

    interface IGroup
    {
        CONST_VTBL struct IGroupVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGroup_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGroup_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGroup_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGroup_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGroup_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGroup_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGroup_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGroup_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define IGroup_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IGroup_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IGroup_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define IGroup_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define IGroup_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IGroup_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IGroup_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IGroup_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IGroup_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IGroup_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IGroup_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IGroup_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IGroup_get_Notes(This,pVal)	\
    ( (This)->lpVtbl -> get_Notes(This,pVal) ) 

#define IGroup_put_Notes(This,newVal)	\
    ( (This)->lpVtbl -> put_Notes(This,newVal) ) 

#define IGroup_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IGroup_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IGroup_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IGroup_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IGroup_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IGroup_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGroup_INTERFACE_DEFINED__ */


#ifndef __INSCondition_INTERFACE_DEFINED__
#define __INSCondition_INTERFACE_DEFINED__

/* interface INSCondition */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_INSCondition;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05EE2-A758-11D3-8A58-000000000000")
    INSCondition : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Instruction( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Instruction( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Lex( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Lex( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeMin( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeMin( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeMax( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeMax( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeLength( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeLength( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RangeLength( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RangeLength( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeDirection( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeDirection( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RangeDirection( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RangeDirection( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeSkip( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeSkip( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Notes( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Notes( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Record( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Record( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Process( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Process( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Parent( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Parent( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Word( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Word( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseStimulus( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseStimulus( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Stimulus( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Stimulus( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StimulusWarning( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StimulusWarning( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StimulusPrecue( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StimulusPrecue( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PrecueDuration( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PrecueDuration( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PrecueLatency( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PrecueLatency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RecordImmediately( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RecordImmediately( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StopWrongTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopWrongTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetBackupPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WarningDuration( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WarningDuration( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WarningLatency( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WarningLatency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MagnetForce( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MagnetForce( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CountStrokes( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CountStrokes( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideFeedback( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideFeedback( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideShowAfter( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideShowAfter( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideShowAfterStrokes( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideShowAfterStrokes( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideShowCount( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideShowCount( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Transform( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Transform( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Xgain( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Xgain( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Xrotation( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Xrotation( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Ygain( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Ygain( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Yrotation( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Yrotation( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideShowAfterShow( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideShowAfterShow( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagBadTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagBadTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Feedback( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Feedback( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StartFirstTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartFirstTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StopLastTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopLastTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GripperTask( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GripperTask( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetFeedbackInfo( 
            /* [in] */ short Which,
            /* [in] */ BOOL Use,
            /* [in] */ short Column,
            /* [in] */ short Stroke,
            /* [in] */ double Min,
            /* [in] */ double Max,
            /* [in] */ BOOL SwapColors) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetFeedbackInfo( 
            /* [in] */ short Which,
            /* [out] */ BOOL *Use,
            /* [out] */ short *Column,
            /* [out] */ short *Stroke,
            /* [out] */ double *Min,
            /* [out] */ double *Max,
            /* [out] */ BOOL *SwapColors) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSConditionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSCondition * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSCondition * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSCondition * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSCondition * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSCondition * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSCondition * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSCondition * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            INSCondition * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            INSCondition * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            INSCondition * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Instruction )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Instruction )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Lex )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Lex )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeMin )( 
            INSCondition * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeMin )( 
            INSCondition * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeMax )( 
            INSCondition * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeMax )( 
            INSCondition * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeLength )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeLength )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RangeLength )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RangeLength )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeDirection )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeDirection )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RangeDirection )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RangeDirection )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeSkip )( 
            INSCondition * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeSkip )( 
            INSCondition * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            INSCondition * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Notes )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Notes )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Record )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Record )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Process )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Process )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Parent )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Parent )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Word )( 
            INSCondition * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Word )( 
            INSCondition * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseStimulus )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseStimulus )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Stimulus )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Stimulus )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StimulusWarning )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StimulusWarning )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StimulusPrecue )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StimulusPrecue )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PrecueDuration )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PrecueDuration )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PrecueLatency )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PrecueLatency )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RecordImmediately )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecordImmediately )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StopWrongTarget )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopWrongTarget )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetBackupPath )( 
            INSCondition * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WarningDuration )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WarningDuration )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WarningLatency )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WarningLatency )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MagnetForce )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MagnetForce )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CountStrokes )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CountStrokes )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideFeedback )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideFeedback )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideShowAfter )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideShowAfter )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideShowAfterStrokes )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideShowAfterStrokes )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideShowCount )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideShowCount )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Transform )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Transform )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Xgain )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Xgain )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Xrotation )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Xrotation )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Ygain )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Ygain )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Yrotation )( 
            INSCondition * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Yrotation )( 
            INSCondition * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideShowAfterShow )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideShowAfterShow )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagBadTarget )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagBadTarget )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            INSCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            INSCondition * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Feedback )( 
            INSCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Feedback )( 
            INSCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StartFirstTarget )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartFirstTarget )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StopLastTarget )( 
            INSCondition * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopLastTarget )( 
            INSCondition * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            INSCondition * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            INSCondition * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            INSCondition * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GripperTask )( 
            INSCondition * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GripperTask )( 
            INSCondition * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetFeedbackInfo )( 
            INSCondition * This,
            /* [in] */ short Which,
            /* [in] */ BOOL Use,
            /* [in] */ short Column,
            /* [in] */ short Stroke,
            /* [in] */ double Min,
            /* [in] */ double Max,
            /* [in] */ BOOL SwapColors);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetFeedbackInfo )( 
            INSCondition * This,
            /* [in] */ short Which,
            /* [out] */ BOOL *Use,
            /* [out] */ short *Column,
            /* [out] */ short *Stroke,
            /* [out] */ double *Min,
            /* [out] */ double *Max,
            /* [out] */ BOOL *SwapColors);
        
        END_INTERFACE
    } INSConditionVtbl;

    interface INSCondition
    {
        CONST_VTBL struct INSConditionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSCondition_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSCondition_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSCondition_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSCondition_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSCondition_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSCondition_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSCondition_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSCondition_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define INSCondition_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define INSCondition_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define INSCondition_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define INSCondition_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define INSCondition_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define INSCondition_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define INSCondition_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define INSCondition_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define INSCondition_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define INSCondition_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define INSCondition_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define INSCondition_get_Instruction(This,pVal)	\
    ( (This)->lpVtbl -> get_Instruction(This,pVal) ) 

#define INSCondition_put_Instruction(This,newVal)	\
    ( (This)->lpVtbl -> put_Instruction(This,newVal) ) 

#define INSCondition_get_Lex(This,pVal)	\
    ( (This)->lpVtbl -> get_Lex(This,pVal) ) 

#define INSCondition_put_Lex(This,newVal)	\
    ( (This)->lpVtbl -> put_Lex(This,newVal) ) 

#define INSCondition_get_StrokeMin(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeMin(This,pVal) ) 

#define INSCondition_put_StrokeMin(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeMin(This,newVal) ) 

#define INSCondition_get_StrokeMax(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeMax(This,pVal) ) 

#define INSCondition_put_StrokeMax(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeMax(This,newVal) ) 

#define INSCondition_get_StrokeLength(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeLength(This,pVal) ) 

#define INSCondition_put_StrokeLength(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeLength(This,newVal) ) 

#define INSCondition_get_RangeLength(This,pVal)	\
    ( (This)->lpVtbl -> get_RangeLength(This,pVal) ) 

#define INSCondition_put_RangeLength(This,newVal)	\
    ( (This)->lpVtbl -> put_RangeLength(This,newVal) ) 

#define INSCondition_get_StrokeDirection(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeDirection(This,pVal) ) 

#define INSCondition_put_StrokeDirection(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeDirection(This,newVal) ) 

#define INSCondition_get_RangeDirection(This,pVal)	\
    ( (This)->lpVtbl -> get_RangeDirection(This,pVal) ) 

#define INSCondition_put_RangeDirection(This,newVal)	\
    ( (This)->lpVtbl -> put_RangeDirection(This,newVal) ) 

#define INSCondition_get_StrokeSkip(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeSkip(This,pVal) ) 

#define INSCondition_put_StrokeSkip(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeSkip(This,newVal) ) 

#define INSCondition_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define INSCondition_get_Notes(This,pVal)	\
    ( (This)->lpVtbl -> get_Notes(This,pVal) ) 

#define INSCondition_put_Notes(This,newVal)	\
    ( (This)->lpVtbl -> put_Notes(This,newVal) ) 

#define INSCondition_get_Record(This,pVal)	\
    ( (This)->lpVtbl -> get_Record(This,pVal) ) 

#define INSCondition_put_Record(This,newVal)	\
    ( (This)->lpVtbl -> put_Record(This,newVal) ) 

#define INSCondition_get_Process(This,pVal)	\
    ( (This)->lpVtbl -> get_Process(This,pVal) ) 

#define INSCondition_put_Process(This,newVal)	\
    ( (This)->lpVtbl -> put_Process(This,newVal) ) 

#define INSCondition_get_Parent(This,pVal)	\
    ( (This)->lpVtbl -> get_Parent(This,pVal) ) 

#define INSCondition_put_Parent(This,newVal)	\
    ( (This)->lpVtbl -> put_Parent(This,newVal) ) 

#define INSCondition_get_Word(This,pVal)	\
    ( (This)->lpVtbl -> get_Word(This,pVal) ) 

#define INSCondition_put_Word(This,newVal)	\
    ( (This)->lpVtbl -> put_Word(This,newVal) ) 

#define INSCondition_get_UseStimulus(This,pVal)	\
    ( (This)->lpVtbl -> get_UseStimulus(This,pVal) ) 

#define INSCondition_put_UseStimulus(This,newVal)	\
    ( (This)->lpVtbl -> put_UseStimulus(This,newVal) ) 

#define INSCondition_get_Stimulus(This,pVal)	\
    ( (This)->lpVtbl -> get_Stimulus(This,pVal) ) 

#define INSCondition_put_Stimulus(This,newVal)	\
    ( (This)->lpVtbl -> put_Stimulus(This,newVal) ) 

#define INSCondition_get_StimulusWarning(This,pVal)	\
    ( (This)->lpVtbl -> get_StimulusWarning(This,pVal) ) 

#define INSCondition_put_StimulusWarning(This,newVal)	\
    ( (This)->lpVtbl -> put_StimulusWarning(This,newVal) ) 

#define INSCondition_get_StimulusPrecue(This,pVal)	\
    ( (This)->lpVtbl -> get_StimulusPrecue(This,pVal) ) 

#define INSCondition_put_StimulusPrecue(This,newVal)	\
    ( (This)->lpVtbl -> put_StimulusPrecue(This,newVal) ) 

#define INSCondition_get_PrecueDuration(This,pVal)	\
    ( (This)->lpVtbl -> get_PrecueDuration(This,pVal) ) 

#define INSCondition_put_PrecueDuration(This,newVal)	\
    ( (This)->lpVtbl -> put_PrecueDuration(This,newVal) ) 

#define INSCondition_get_PrecueLatency(This,pVal)	\
    ( (This)->lpVtbl -> get_PrecueLatency(This,pVal) ) 

#define INSCondition_put_PrecueLatency(This,newVal)	\
    ( (This)->lpVtbl -> put_PrecueLatency(This,newVal) ) 

#define INSCondition_get_RecordImmediately(This,pVal)	\
    ( (This)->lpVtbl -> get_RecordImmediately(This,pVal) ) 

#define INSCondition_put_RecordImmediately(This,newVal)	\
    ( (This)->lpVtbl -> put_RecordImmediately(This,newVal) ) 

#define INSCondition_get_StopWrongTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_StopWrongTarget(This,pVal) ) 

#define INSCondition_put_StopWrongTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_StopWrongTarget(This,newVal) ) 

#define INSCondition_SetBackupPath(This,Path)	\
    ( (This)->lpVtbl -> SetBackupPath(This,Path) ) 

#define INSCondition_get_WarningDuration(This,pVal)	\
    ( (This)->lpVtbl -> get_WarningDuration(This,pVal) ) 

#define INSCondition_put_WarningDuration(This,newVal)	\
    ( (This)->lpVtbl -> put_WarningDuration(This,newVal) ) 

#define INSCondition_get_WarningLatency(This,pVal)	\
    ( (This)->lpVtbl -> get_WarningLatency(This,pVal) ) 

#define INSCondition_put_WarningLatency(This,newVal)	\
    ( (This)->lpVtbl -> put_WarningLatency(This,newVal) ) 

#define INSCondition_get_MagnetForce(This,pVal)	\
    ( (This)->lpVtbl -> get_MagnetForce(This,pVal) ) 

#define INSCondition_put_MagnetForce(This,newVal)	\
    ( (This)->lpVtbl -> put_MagnetForce(This,newVal) ) 

#define INSCondition_get_CountStrokes(This,pVal)	\
    ( (This)->lpVtbl -> get_CountStrokes(This,pVal) ) 

#define INSCondition_put_CountStrokes(This,newVal)	\
    ( (This)->lpVtbl -> put_CountStrokes(This,newVal) ) 

#define INSCondition_get_HideFeedback(This,pVal)	\
    ( (This)->lpVtbl -> get_HideFeedback(This,pVal) ) 

#define INSCondition_put_HideFeedback(This,newVal)	\
    ( (This)->lpVtbl -> put_HideFeedback(This,newVal) ) 

#define INSCondition_get_HideShowAfter(This,pVal)	\
    ( (This)->lpVtbl -> get_HideShowAfter(This,pVal) ) 

#define INSCondition_put_HideShowAfter(This,newVal)	\
    ( (This)->lpVtbl -> put_HideShowAfter(This,newVal) ) 

#define INSCondition_get_HideShowAfterStrokes(This,pVal)	\
    ( (This)->lpVtbl -> get_HideShowAfterStrokes(This,pVal) ) 

#define INSCondition_put_HideShowAfterStrokes(This,newVal)	\
    ( (This)->lpVtbl -> put_HideShowAfterStrokes(This,newVal) ) 

#define INSCondition_get_HideShowCount(This,pVal)	\
    ( (This)->lpVtbl -> get_HideShowCount(This,pVal) ) 

#define INSCondition_put_HideShowCount(This,newVal)	\
    ( (This)->lpVtbl -> put_HideShowCount(This,newVal) ) 

#define INSCondition_get_Transform(This,pVal)	\
    ( (This)->lpVtbl -> get_Transform(This,pVal) ) 

#define INSCondition_put_Transform(This,newVal)	\
    ( (This)->lpVtbl -> put_Transform(This,newVal) ) 

#define INSCondition_get_Xgain(This,pVal)	\
    ( (This)->lpVtbl -> get_Xgain(This,pVal) ) 

#define INSCondition_put_Xgain(This,newVal)	\
    ( (This)->lpVtbl -> put_Xgain(This,newVal) ) 

#define INSCondition_get_Xrotation(This,pVal)	\
    ( (This)->lpVtbl -> get_Xrotation(This,pVal) ) 

#define INSCondition_put_Xrotation(This,newVal)	\
    ( (This)->lpVtbl -> put_Xrotation(This,newVal) ) 

#define INSCondition_get_Ygain(This,pVal)	\
    ( (This)->lpVtbl -> get_Ygain(This,pVal) ) 

#define INSCondition_put_Ygain(This,newVal)	\
    ( (This)->lpVtbl -> put_Ygain(This,newVal) ) 

#define INSCondition_get_Yrotation(This,pVal)	\
    ( (This)->lpVtbl -> get_Yrotation(This,pVal) ) 

#define INSCondition_put_Yrotation(This,newVal)	\
    ( (This)->lpVtbl -> put_Yrotation(This,newVal) ) 

#define INSCondition_get_HideShowAfterShow(This,pVal)	\
    ( (This)->lpVtbl -> get_HideShowAfterShow(This,pVal) ) 

#define INSCondition_put_HideShowAfterShow(This,newVal)	\
    ( (This)->lpVtbl -> put_HideShowAfterShow(This,newVal) ) 

#define INSCondition_get_FlagBadTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagBadTarget(This,pVal) ) 

#define INSCondition_put_FlagBadTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagBadTarget(This,newVal) ) 

#define INSCondition_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define INSCondition_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define INSCondition_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define INSCondition_get_Feedback(This,pVal)	\
    ( (This)->lpVtbl -> get_Feedback(This,pVal) ) 

#define INSCondition_put_Feedback(This,newVal)	\
    ( (This)->lpVtbl -> put_Feedback(This,newVal) ) 

#define INSCondition_get_StartFirstTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_StartFirstTarget(This,pVal) ) 

#define INSCondition_put_StartFirstTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_StartFirstTarget(This,newVal) ) 

#define INSCondition_get_StopLastTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_StopLastTarget(This,pVal) ) 

#define INSCondition_put_StopLastTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_StopLastTarget(This,newVal) ) 

#define INSCondition_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define INSCondition_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define INSCondition_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#define INSCondition_get_GripperTask(This,pVal)	\
    ( (This)->lpVtbl -> get_GripperTask(This,pVal) ) 

#define INSCondition_put_GripperTask(This,newVal)	\
    ( (This)->lpVtbl -> put_GripperTask(This,newVal) ) 

#define INSCondition_SetFeedbackInfo(This,Which,Use,Column,Stroke,Min,Max,SwapColors)	\
    ( (This)->lpVtbl -> SetFeedbackInfo(This,Which,Use,Column,Stroke,Min,Max,SwapColors) ) 

#define INSCondition_GetFeedbackInfo(This,Which,Use,Column,Stroke,Min,Max,SwapColors)	\
    ( (This)->lpVtbl -> GetFeedbackInfo(This,Which,Use,Column,Stroke,Min,Max,SwapColors) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INSCondition_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_DataMod_0000_0005 */
/* [local] */ 




extern RPC_IF_HANDLE __MIDL_itf_DataMod_0000_0005_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_DataMod_0000_0005_v0_0_s_ifspec;

#ifndef __IProcSetting_INTERFACE_DEFINED__
#define __IProcSetting_INTERFACE_DEFINED__

/* interface IProcSetting */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eTrialsMode
    {	eTM_None	= 0x1,
	eTM_Delay	= 0x2,
	eTM_First	= 0x4,
	eTM_FirstDelay	= 0x8,
	eTM_All	= 0x10,
	eTM_NoneAD	= 0x100,
	eTM_DelayAD	= 0x101,
	eTM_FirstAD	= 0x102,
	eTM_FirstDelayAD	= 0x104,
	eTM_AllAD	= 0x108,
	eTM_NoneADErr	= 0x200,
	eTM_DelayADErr	= 0x201,
	eTM_FirstADErr	= 0x202,
	eTM_FirstDelayADErr	= 0x204,
	eTM_AllADErr	= 0x208,
	eTM_NoneErr	= 0x400,
	eTM_DelayErr	= 0x401,
	eTM_FirstErr	= 0x402,
	eTM_FirstDelayErr	= 0x404,
	eTM_AllErr	= 0x408
    } 	TrialsMode;


EXTERN_C const IID IID_IProcSetting;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05EE5-A758-11D3-8A58-000000000000")
    IProcSetting : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExperimentID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExperimentID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Velocity( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Velocity( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Differentiate( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Differentiate( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RotationBeta( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RotationBeta( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FilterFrequency( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FilterFrequency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Decimate( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Decimate( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Randomize( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Randomize( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandomizeTrials( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandomizeTrials( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SamplingRate( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SamplingRate( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinPenPressure( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinPenPressure( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DeviceResolution( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DeviceResolution( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InstructionAlwaysVisible( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InstructionAlwaysVisible( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinLeftPenup( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinLeftPenup( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinLeftSpacing( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinLeftSpacing( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinWordWidth( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinWordWidth( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinDownSpacing( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinDownSpacing( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinTimePenup( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinTimePenup( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxTimePenup( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxTimePenup( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinStrokeSize( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinStrokeSize( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinStrokeSizeRelativeToMax( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinStrokeSizeRelativeToMax( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinStrokeDuration( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinStrokeDuration( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinVertVelocity( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinVertVelocity( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxVertVelocity( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxVertVelocity( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeBeginning( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeBeginning( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxFrequency( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxFrequency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SlantError( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SlantError( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SpiralIncreaseFactor( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SpiralIncreaseFactor( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinLoopArea( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinLoopArea( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxStraightErrorStraight( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxStraightErrorStraight( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinStraightErrorCurved( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinStraightErrorCurved( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DiscardLTMin( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DiscardLTMin( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DiscardGTMax( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DiscardGTMax( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinReactionTime( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinReactionTime( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxReactionTime( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxReactionTime( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FFT( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FFT( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Sharpness( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Sharpness( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrialMode( 
            /* [retval][out] */ TrialsMode *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrialMode( 
            /* [in] */ TrialsMode newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisCorrection( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisCorrection( 
            /* [retval][out] */ short *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisFactor( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisFactor( 
            /* [retval][out] */ double *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisZInsertPoint( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisZInsertPoint( 
            /* [retval][out] */ double *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisRelError( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisRelError( 
            /* [retval][out] */ double *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisAbsError( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisAbsError( 
            /* [retval][out] */ double *newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandWithRules( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandWithRules( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandRules( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandRules( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandBlockCounts( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandBlockCounts( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandSelectionChar( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandSelectionChar( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RandSelectionTrials( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RandSelectionTrials( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UpSampleFactor( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UpSampleFactor( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CollapseUDStrokes( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CollapseUDStrokes( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CollapseStrokes( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CollapseStrokes( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CollapseTrials( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CollapseTrials( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CollapseMedian( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CollapseMedian( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseTilt( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseTilt( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxPenPressure( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxPenPressure( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisNotify( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisNotify( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisError( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisError( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SmoothingIter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SmoothingIter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InkThreshold( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InkThreshold( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ImgResolution( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ImgResolution( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SpecifySequence( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SpecifySequence( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ConditionSequence( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ConditionSequence( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RemoveTrailingPenlift( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RemoveTrailingPenlift( 
            /* [in] */ BOOL newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProcSettingVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProcSetting * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProcSetting * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProcSetting * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProcSetting * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProcSetting * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProcSetting * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProcSetting * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExperimentID )( 
            IProcSetting * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExperimentID )( 
            IProcSetting * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Velocity )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Velocity )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Differentiate )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Differentiate )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RotationBeta )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RotationBeta )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FilterFrequency )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FilterFrequency )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Decimate )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Decimate )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Randomize )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Randomize )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandomizeTrials )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandomizeTrials )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IProcSetting * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IProcSetting * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SamplingRate )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SamplingRate )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinPenPressure )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinPenPressure )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DeviceResolution )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DeviceResolution )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InstructionAlwaysVisible )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InstructionAlwaysVisible )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IProcSetting * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinLeftPenup )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinLeftPenup )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinLeftSpacing )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinLeftSpacing )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinWordWidth )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinWordWidth )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinDownSpacing )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinDownSpacing )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinTimePenup )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinTimePenup )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxTimePenup )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxTimePenup )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinStrokeSize )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinStrokeSize )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinStrokeSizeRelativeToMax )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinStrokeSizeRelativeToMax )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinStrokeDuration )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinStrokeDuration )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinVertVelocity )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinVertVelocity )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxVertVelocity )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxVertVelocity )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeBeginning )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeBeginning )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxFrequency )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxFrequency )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SlantError )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SlantError )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SpiralIncreaseFactor )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SpiralIncreaseFactor )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinLoopArea )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinLoopArea )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxStraightErrorStraight )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxStraightErrorStraight )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinStraightErrorCurved )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinStraightErrorCurved )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DiscardLTMin )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DiscardLTMin )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DiscardGTMax )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DiscardGTMax )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinReactionTime )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinReactionTime )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxReactionTime )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxReactionTime )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FFT )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FFT )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Sharpness )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Sharpness )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TrialMode )( 
            IProcSetting * This,
            /* [retval][out] */ TrialsMode *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TrialMode )( 
            IProcSetting * This,
            /* [in] */ TrialsMode newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IProcSetting * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IProcSetting * This);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisCorrection )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisCorrection )( 
            IProcSetting * This,
            /* [retval][out] */ short *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisFactor )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisFactor )( 
            IProcSetting * This,
            /* [retval][out] */ double *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisZInsertPoint )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisZInsertPoint )( 
            IProcSetting * This,
            /* [retval][out] */ double *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisRelError )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisRelError )( 
            IProcSetting * This,
            /* [retval][out] */ double *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisAbsError )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisAbsError )( 
            IProcSetting * This,
            /* [retval][out] */ double *newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IProcSetting * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IProcSetting * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IProcSetting * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandWithRules )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandWithRules )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandRules )( 
            IProcSetting * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandRules )( 
            IProcSetting * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandBlockCounts )( 
            IProcSetting * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandBlockCounts )( 
            IProcSetting * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandSelectionChar )( 
            IProcSetting * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandSelectionChar )( 
            IProcSetting * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RandSelectionTrials )( 
            IProcSetting * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RandSelectionTrials )( 
            IProcSetting * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UpSampleFactor )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UpSampleFactor )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CollapseUDStrokes )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CollapseUDStrokes )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CollapseStrokes )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CollapseStrokes )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CollapseTrials )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CollapseTrials )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CollapseMedian )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CollapseMedian )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseTilt )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseTilt )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxPenPressure )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxPenPressure )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisNotify )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisNotify )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisError )( 
            IProcSetting * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisError )( 
            IProcSetting * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SmoothingIter )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SmoothingIter )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InkThreshold )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InkThreshold )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ImgResolution )( 
            IProcSetting * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ImgResolution )( 
            IProcSetting * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SpecifySequence )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SpecifySequence )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConditionSequence )( 
            IProcSetting * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConditionSequence )( 
            IProcSetting * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RemoveTrailingPenlift )( 
            IProcSetting * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RemoveTrailingPenlift )( 
            IProcSetting * This,
            /* [in] */ BOOL newVal);
        
        END_INTERFACE
    } IProcSettingVtbl;

    interface IProcSetting
    {
        CONST_VTBL struct IProcSettingVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProcSetting_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IProcSetting_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IProcSetting_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IProcSetting_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IProcSetting_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IProcSetting_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IProcSetting_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IProcSetting_get_ExperimentID(This,pVal)	\
    ( (This)->lpVtbl -> get_ExperimentID(This,pVal) ) 

#define IProcSetting_put_ExperimentID(This,newVal)	\
    ( (This)->lpVtbl -> put_ExperimentID(This,newVal) ) 

#define IProcSetting_get_Velocity(This,pVal)	\
    ( (This)->lpVtbl -> get_Velocity(This,pVal) ) 

#define IProcSetting_put_Velocity(This,newVal)	\
    ( (This)->lpVtbl -> put_Velocity(This,newVal) ) 

#define IProcSetting_get_Differentiate(This,pVal)	\
    ( (This)->lpVtbl -> get_Differentiate(This,pVal) ) 

#define IProcSetting_put_Differentiate(This,newVal)	\
    ( (This)->lpVtbl -> put_Differentiate(This,newVal) ) 

#define IProcSetting_get_RotationBeta(This,pVal)	\
    ( (This)->lpVtbl -> get_RotationBeta(This,pVal) ) 

#define IProcSetting_put_RotationBeta(This,newVal)	\
    ( (This)->lpVtbl -> put_RotationBeta(This,newVal) ) 

#define IProcSetting_get_FilterFrequency(This,pVal)	\
    ( (This)->lpVtbl -> get_FilterFrequency(This,pVal) ) 

#define IProcSetting_put_FilterFrequency(This,newVal)	\
    ( (This)->lpVtbl -> put_FilterFrequency(This,newVal) ) 

#define IProcSetting_get_Decimate(This,pVal)	\
    ( (This)->lpVtbl -> get_Decimate(This,pVal) ) 

#define IProcSetting_put_Decimate(This,newVal)	\
    ( (This)->lpVtbl -> put_Decimate(This,newVal) ) 

#define IProcSetting_get_Randomize(This,pVal)	\
    ( (This)->lpVtbl -> get_Randomize(This,pVal) ) 

#define IProcSetting_put_Randomize(This,newVal)	\
    ( (This)->lpVtbl -> put_Randomize(This,newVal) ) 

#define IProcSetting_get_RandomizeTrials(This,pVal)	\
    ( (This)->lpVtbl -> get_RandomizeTrials(This,pVal) ) 

#define IProcSetting_put_RandomizeTrials(This,newVal)	\
    ( (This)->lpVtbl -> put_RandomizeTrials(This,newVal) ) 

#define IProcSetting_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IProcSetting_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IProcSetting_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IProcSetting_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IProcSetting_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IProcSetting_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IProcSetting_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IProcSetting_get_SamplingRate(This,pVal)	\
    ( (This)->lpVtbl -> get_SamplingRate(This,pVal) ) 

#define IProcSetting_put_SamplingRate(This,newVal)	\
    ( (This)->lpVtbl -> put_SamplingRate(This,newVal) ) 

#define IProcSetting_get_MinPenPressure(This,pVal)	\
    ( (This)->lpVtbl -> get_MinPenPressure(This,pVal) ) 

#define IProcSetting_put_MinPenPressure(This,newVal)	\
    ( (This)->lpVtbl -> put_MinPenPressure(This,newVal) ) 

#define IProcSetting_get_DeviceResolution(This,pVal)	\
    ( (This)->lpVtbl -> get_DeviceResolution(This,pVal) ) 

#define IProcSetting_put_DeviceResolution(This,newVal)	\
    ( (This)->lpVtbl -> put_DeviceResolution(This,newVal) ) 

#define IProcSetting_get_InstructionAlwaysVisible(This,pVal)	\
    ( (This)->lpVtbl -> get_InstructionAlwaysVisible(This,pVal) ) 

#define IProcSetting_put_InstructionAlwaysVisible(This,newVal)	\
    ( (This)->lpVtbl -> put_InstructionAlwaysVisible(This,newVal) ) 

#define IProcSetting_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IProcSetting_get_MinLeftPenup(This,pVal)	\
    ( (This)->lpVtbl -> get_MinLeftPenup(This,pVal) ) 

#define IProcSetting_put_MinLeftPenup(This,newVal)	\
    ( (This)->lpVtbl -> put_MinLeftPenup(This,newVal) ) 

#define IProcSetting_get_MinLeftSpacing(This,pVal)	\
    ( (This)->lpVtbl -> get_MinLeftSpacing(This,pVal) ) 

#define IProcSetting_put_MinLeftSpacing(This,newVal)	\
    ( (This)->lpVtbl -> put_MinLeftSpacing(This,newVal) ) 

#define IProcSetting_get_MinWordWidth(This,pVal)	\
    ( (This)->lpVtbl -> get_MinWordWidth(This,pVal) ) 

#define IProcSetting_put_MinWordWidth(This,newVal)	\
    ( (This)->lpVtbl -> put_MinWordWidth(This,newVal) ) 

#define IProcSetting_get_MinDownSpacing(This,pVal)	\
    ( (This)->lpVtbl -> get_MinDownSpacing(This,pVal) ) 

#define IProcSetting_put_MinDownSpacing(This,newVal)	\
    ( (This)->lpVtbl -> put_MinDownSpacing(This,newVal) ) 

#define IProcSetting_get_MinTimePenup(This,pVal)	\
    ( (This)->lpVtbl -> get_MinTimePenup(This,pVal) ) 

#define IProcSetting_put_MinTimePenup(This,newVal)	\
    ( (This)->lpVtbl -> put_MinTimePenup(This,newVal) ) 

#define IProcSetting_get_MaxTimePenup(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxTimePenup(This,pVal) ) 

#define IProcSetting_put_MaxTimePenup(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxTimePenup(This,newVal) ) 

#define IProcSetting_get_MinStrokeSize(This,pVal)	\
    ( (This)->lpVtbl -> get_MinStrokeSize(This,pVal) ) 

#define IProcSetting_put_MinStrokeSize(This,newVal)	\
    ( (This)->lpVtbl -> put_MinStrokeSize(This,newVal) ) 

#define IProcSetting_get_MinStrokeSizeRelativeToMax(This,pVal)	\
    ( (This)->lpVtbl -> get_MinStrokeSizeRelativeToMax(This,pVal) ) 

#define IProcSetting_put_MinStrokeSizeRelativeToMax(This,newVal)	\
    ( (This)->lpVtbl -> put_MinStrokeSizeRelativeToMax(This,newVal) ) 

#define IProcSetting_get_MinStrokeDuration(This,pVal)	\
    ( (This)->lpVtbl -> get_MinStrokeDuration(This,pVal) ) 

#define IProcSetting_put_MinStrokeDuration(This,newVal)	\
    ( (This)->lpVtbl -> put_MinStrokeDuration(This,newVal) ) 

#define IProcSetting_get_MinVertVelocity(This,pVal)	\
    ( (This)->lpVtbl -> get_MinVertVelocity(This,pVal) ) 

#define IProcSetting_put_MinVertVelocity(This,newVal)	\
    ( (This)->lpVtbl -> put_MinVertVelocity(This,newVal) ) 

#define IProcSetting_get_MaxVertVelocity(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxVertVelocity(This,pVal) ) 

#define IProcSetting_put_MaxVertVelocity(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxVertVelocity(This,newVal) ) 

#define IProcSetting_get_StrokeBeginning(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeBeginning(This,pVal) ) 

#define IProcSetting_put_StrokeBeginning(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeBeginning(This,newVal) ) 

#define IProcSetting_get_MaxFrequency(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxFrequency(This,pVal) ) 

#define IProcSetting_put_MaxFrequency(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxFrequency(This,newVal) ) 

#define IProcSetting_get_SlantError(This,pVal)	\
    ( (This)->lpVtbl -> get_SlantError(This,pVal) ) 

#define IProcSetting_put_SlantError(This,newVal)	\
    ( (This)->lpVtbl -> put_SlantError(This,newVal) ) 

#define IProcSetting_get_SpiralIncreaseFactor(This,pVal)	\
    ( (This)->lpVtbl -> get_SpiralIncreaseFactor(This,pVal) ) 

#define IProcSetting_put_SpiralIncreaseFactor(This,newVal)	\
    ( (This)->lpVtbl -> put_SpiralIncreaseFactor(This,newVal) ) 

#define IProcSetting_get_MinLoopArea(This,pVal)	\
    ( (This)->lpVtbl -> get_MinLoopArea(This,pVal) ) 

#define IProcSetting_put_MinLoopArea(This,newVal)	\
    ( (This)->lpVtbl -> put_MinLoopArea(This,newVal) ) 

#define IProcSetting_get_MaxStraightErrorStraight(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxStraightErrorStraight(This,pVal) ) 

#define IProcSetting_put_MaxStraightErrorStraight(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxStraightErrorStraight(This,newVal) ) 

#define IProcSetting_get_MinStraightErrorCurved(This,pVal)	\
    ( (This)->lpVtbl -> get_MinStraightErrorCurved(This,pVal) ) 

#define IProcSetting_put_MinStraightErrorCurved(This,newVal)	\
    ( (This)->lpVtbl -> put_MinStraightErrorCurved(This,newVal) ) 

#define IProcSetting_get_DiscardLTMin(This,pVal)	\
    ( (This)->lpVtbl -> get_DiscardLTMin(This,pVal) ) 

#define IProcSetting_put_DiscardLTMin(This,newVal)	\
    ( (This)->lpVtbl -> put_DiscardLTMin(This,newVal) ) 

#define IProcSetting_get_DiscardGTMax(This,pVal)	\
    ( (This)->lpVtbl -> get_DiscardGTMax(This,pVal) ) 

#define IProcSetting_put_DiscardGTMax(This,newVal)	\
    ( (This)->lpVtbl -> put_DiscardGTMax(This,newVal) ) 

#define IProcSetting_get_MinReactionTime(This,pVal)	\
    ( (This)->lpVtbl -> get_MinReactionTime(This,pVal) ) 

#define IProcSetting_put_MinReactionTime(This,newVal)	\
    ( (This)->lpVtbl -> put_MinReactionTime(This,newVal) ) 

#define IProcSetting_get_MaxReactionTime(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxReactionTime(This,pVal) ) 

#define IProcSetting_put_MaxReactionTime(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxReactionTime(This,newVal) ) 

#define IProcSetting_get_FFT(This,pVal)	\
    ( (This)->lpVtbl -> get_FFT(This,pVal) ) 

#define IProcSetting_put_FFT(This,newVal)	\
    ( (This)->lpVtbl -> put_FFT(This,newVal) ) 

#define IProcSetting_get_Sharpness(This,pVal)	\
    ( (This)->lpVtbl -> get_Sharpness(This,pVal) ) 

#define IProcSetting_put_Sharpness(This,newVal)	\
    ( (This)->lpVtbl -> put_Sharpness(This,newVal) ) 

#define IProcSetting_get_TrialMode(This,pVal)	\
    ( (This)->lpVtbl -> get_TrialMode(This,pVal) ) 

#define IProcSetting_put_TrialMode(This,newVal)	\
    ( (This)->lpVtbl -> put_TrialMode(This,newVal) ) 

#define IProcSetting_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IProcSetting_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IProcSetting_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IProcSetting_put_DisCorrection(This,newVal)	\
    ( (This)->lpVtbl -> put_DisCorrection(This,newVal) ) 

#define IProcSetting_get_DisCorrection(This,newVal)	\
    ( (This)->lpVtbl -> get_DisCorrection(This,newVal) ) 

#define IProcSetting_put_DisFactor(This,newVal)	\
    ( (This)->lpVtbl -> put_DisFactor(This,newVal) ) 

#define IProcSetting_get_DisFactor(This,newVal)	\
    ( (This)->lpVtbl -> get_DisFactor(This,newVal) ) 

#define IProcSetting_put_DisZInsertPoint(This,newVal)	\
    ( (This)->lpVtbl -> put_DisZInsertPoint(This,newVal) ) 

#define IProcSetting_get_DisZInsertPoint(This,newVal)	\
    ( (This)->lpVtbl -> get_DisZInsertPoint(This,newVal) ) 

#define IProcSetting_put_DisRelError(This,newVal)	\
    ( (This)->lpVtbl -> put_DisRelError(This,newVal) ) 

#define IProcSetting_get_DisRelError(This,newVal)	\
    ( (This)->lpVtbl -> get_DisRelError(This,newVal) ) 

#define IProcSetting_put_DisAbsError(This,newVal)	\
    ( (This)->lpVtbl -> put_DisAbsError(This,newVal) ) 

#define IProcSetting_get_DisAbsError(This,newVal)	\
    ( (This)->lpVtbl -> get_DisAbsError(This,newVal) ) 

#define IProcSetting_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IProcSetting_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IProcSetting_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#define IProcSetting_get_RandWithRules(This,pVal)	\
    ( (This)->lpVtbl -> get_RandWithRules(This,pVal) ) 

#define IProcSetting_put_RandWithRules(This,newVal)	\
    ( (This)->lpVtbl -> put_RandWithRules(This,newVal) ) 

#define IProcSetting_get_RandRules(This,pVal)	\
    ( (This)->lpVtbl -> get_RandRules(This,pVal) ) 

#define IProcSetting_put_RandRules(This,newVal)	\
    ( (This)->lpVtbl -> put_RandRules(This,newVal) ) 

#define IProcSetting_get_RandBlockCounts(This,pVal)	\
    ( (This)->lpVtbl -> get_RandBlockCounts(This,pVal) ) 

#define IProcSetting_put_RandBlockCounts(This,newVal)	\
    ( (This)->lpVtbl -> put_RandBlockCounts(This,newVal) ) 

#define IProcSetting_get_RandSelectionChar(This,pVal)	\
    ( (This)->lpVtbl -> get_RandSelectionChar(This,pVal) ) 

#define IProcSetting_put_RandSelectionChar(This,newVal)	\
    ( (This)->lpVtbl -> put_RandSelectionChar(This,newVal) ) 

#define IProcSetting_get_RandSelectionTrials(This,pVal)	\
    ( (This)->lpVtbl -> get_RandSelectionTrials(This,pVal) ) 

#define IProcSetting_put_RandSelectionTrials(This,newVal)	\
    ( (This)->lpVtbl -> put_RandSelectionTrials(This,newVal) ) 

#define IProcSetting_get_UpSampleFactor(This,pVal)	\
    ( (This)->lpVtbl -> get_UpSampleFactor(This,pVal) ) 

#define IProcSetting_put_UpSampleFactor(This,newVal)	\
    ( (This)->lpVtbl -> put_UpSampleFactor(This,newVal) ) 

#define IProcSetting_get_CollapseUDStrokes(This,pVal)	\
    ( (This)->lpVtbl -> get_CollapseUDStrokes(This,pVal) ) 

#define IProcSetting_put_CollapseUDStrokes(This,newVal)	\
    ( (This)->lpVtbl -> put_CollapseUDStrokes(This,newVal) ) 

#define IProcSetting_get_CollapseStrokes(This,pVal)	\
    ( (This)->lpVtbl -> get_CollapseStrokes(This,pVal) ) 

#define IProcSetting_put_CollapseStrokes(This,newVal)	\
    ( (This)->lpVtbl -> put_CollapseStrokes(This,newVal) ) 

#define IProcSetting_get_CollapseTrials(This,pVal)	\
    ( (This)->lpVtbl -> get_CollapseTrials(This,pVal) ) 

#define IProcSetting_put_CollapseTrials(This,newVal)	\
    ( (This)->lpVtbl -> put_CollapseTrials(This,newVal) ) 

#define IProcSetting_get_CollapseMedian(This,pVal)	\
    ( (This)->lpVtbl -> get_CollapseMedian(This,pVal) ) 

#define IProcSetting_put_CollapseMedian(This,newVal)	\
    ( (This)->lpVtbl -> put_CollapseMedian(This,newVal) ) 

#define IProcSetting_get_UseTilt(This,pVal)	\
    ( (This)->lpVtbl -> get_UseTilt(This,pVal) ) 

#define IProcSetting_put_UseTilt(This,newVal)	\
    ( (This)->lpVtbl -> put_UseTilt(This,newVal) ) 

#define IProcSetting_get_MaxPenPressure(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxPenPressure(This,pVal) ) 

#define IProcSetting_put_MaxPenPressure(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxPenPressure(This,newVal) ) 

#define IProcSetting_get_DisNotify(This,pVal)	\
    ( (This)->lpVtbl -> get_DisNotify(This,pVal) ) 

#define IProcSetting_put_DisNotify(This,newVal)	\
    ( (This)->lpVtbl -> put_DisNotify(This,newVal) ) 

#define IProcSetting_get_DisError(This,pVal)	\
    ( (This)->lpVtbl -> get_DisError(This,pVal) ) 

#define IProcSetting_put_DisError(This,newVal)	\
    ( (This)->lpVtbl -> put_DisError(This,newVal) ) 

#define IProcSetting_get_SmoothingIter(This,pVal)	\
    ( (This)->lpVtbl -> get_SmoothingIter(This,pVal) ) 

#define IProcSetting_put_SmoothingIter(This,newVal)	\
    ( (This)->lpVtbl -> put_SmoothingIter(This,newVal) ) 

#define IProcSetting_get_InkThreshold(This,pVal)	\
    ( (This)->lpVtbl -> get_InkThreshold(This,pVal) ) 

#define IProcSetting_put_InkThreshold(This,newVal)	\
    ( (This)->lpVtbl -> put_InkThreshold(This,newVal) ) 

#define IProcSetting_get_ImgResolution(This,pVal)	\
    ( (This)->lpVtbl -> get_ImgResolution(This,pVal) ) 

#define IProcSetting_put_ImgResolution(This,newVal)	\
    ( (This)->lpVtbl -> put_ImgResolution(This,newVal) ) 

#define IProcSetting_get_SpecifySequence(This,pVal)	\
    ( (This)->lpVtbl -> get_SpecifySequence(This,pVal) ) 

#define IProcSetting_put_SpecifySequence(This,newVal)	\
    ( (This)->lpVtbl -> put_SpecifySequence(This,newVal) ) 

#define IProcSetting_get_ConditionSequence(This,pVal)	\
    ( (This)->lpVtbl -> get_ConditionSequence(This,pVal) ) 

#define IProcSetting_put_ConditionSequence(This,newVal)	\
    ( (This)->lpVtbl -> put_ConditionSequence(This,newVal) ) 

#define IProcSetting_get_RemoveTrailingPenlift(This,pVal)	\
    ( (This)->lpVtbl -> get_RemoveTrailingPenlift(This,pVal) ) 

#define IProcSetting_put_RemoveTrailingPenlift(This,newVal)	\
    ( (This)->lpVtbl -> put_RemoveTrailingPenlift(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_MaxPenPressure_Proxy( 
    IProcSetting * This,
    /* [in] */ short newVal);


void __RPC_STUB IProcSetting_put_MaxPenPressure_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_DisNotify_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB IProcSetting_get_DisNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_DisNotify_Proxy( 
    IProcSetting * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB IProcSetting_put_DisNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_DisError_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ double *pVal);


void __RPC_STUB IProcSetting_get_DisError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_DisError_Proxy( 
    IProcSetting * This,
    /* [in] */ double newVal);


void __RPC_STUB IProcSetting_put_DisError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_SmoothingIter_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB IProcSetting_get_SmoothingIter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_SmoothingIter_Proxy( 
    IProcSetting * This,
    /* [in] */ short newVal);


void __RPC_STUB IProcSetting_put_SmoothingIter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_InkThreshold_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB IProcSetting_get_InkThreshold_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_InkThreshold_Proxy( 
    IProcSetting * This,
    /* [in] */ short newVal);


void __RPC_STUB IProcSetting_put_InkThreshold_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_ImgResolution_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ short *pVal);


void __RPC_STUB IProcSetting_get_ImgResolution_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_ImgResolution_Proxy( 
    IProcSetting * This,
    /* [in] */ short newVal);


void __RPC_STUB IProcSetting_put_ImgResolution_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_SpecifySequence_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB IProcSetting_get_SpecifySequence_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_SpecifySequence_Proxy( 
    IProcSetting * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB IProcSetting_put_SpecifySequence_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_ConditionSequence_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IProcSetting_get_ConditionSequence_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_ConditionSequence_Proxy( 
    IProcSetting * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IProcSetting_put_ConditionSequence_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IProcSetting_get_RemoveTrailingPenlift_Proxy( 
    IProcSetting * This,
    /* [retval][out] */ BOOL *pVal);


void __RPC_STUB IProcSetting_get_RemoveTrailingPenlift_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IProcSetting_put_RemoveTrailingPenlift_Proxy( 
    IProcSetting * This,
    /* [in] */ BOOL newVal);


void __RPC_STUB IProcSetting_put_RemoveTrailingPenlift_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProcSetting_INTERFACE_DEFINED__ */


#ifndef __IProcSetFlags_INTERFACE_DEFINED__
#define __IProcSetFlags_INTERFACE_DEFINED__

/* interface IProcSetFlags */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IProcSetFlags;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DFE1ED9D-E374-4499-95FD-F3AF7F2ED5E0")
    IProcSetFlags : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_D( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_D( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DiscardAfter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DiscardAfter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_D2( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_D2( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DiscardAfter2( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DiscardAfter2( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TF_O( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TF_O( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_O( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_O( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TF_J( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TF_J( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TF_R( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TF_R( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TF_A( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TF_A( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TF_U( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TF_U( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_F( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_F( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_L( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_L( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_S( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_S( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_O( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_O( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EXT_S( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EXT_S( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EXT_3( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EXT_3( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EXT_O( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EXT_O( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_A( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_A( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_N( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_N( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_M( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_M( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_U( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_U( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_C( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_C( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_S( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_S( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_A( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_A( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_R( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_R( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_Z( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_Z( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_T( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_T( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ST_S( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ST_S( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_M( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_M( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_A( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_A( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_V( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_V( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_MM( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_MM( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SEG_J( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_J( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EXT_2( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EXT_2( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CON_D( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CON_D( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DiscardPenDownDurMin( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DiscardPenDownDurMin( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_PenDownDurMin( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PenDownDurMin( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NormDBCalcScore( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NormDBCalcScore( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NormDBCalcScoreGlobal( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NormDBCalcScoreGlobal( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NormDBNoUpdate( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NormDBNoUpdate( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NormDBThreshold( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_NormDBThreshold( 
            /* [retval][out] */ double *newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProcSetFlagsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProcSetFlags * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProcSetFlags * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProcSetFlags * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProcSetFlags * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProcSetFlags * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProcSetFlags * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProcSetFlags * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_D )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_D )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DiscardAfter )( 
            IProcSetFlags * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DiscardAfter )( 
            IProcSetFlags * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_D2 )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_D2 )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DiscardAfter2 )( 
            IProcSetFlags * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DiscardAfter2 )( 
            IProcSetFlags * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TF_O )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TF_O )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_O )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_O )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TF_J )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TF_J )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TF_R )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TF_R )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TF_A )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TF_A )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TF_U )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TF_U )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_F )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_F )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_L )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_L )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_S )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_S )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_O )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_O )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EXT_S )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EXT_S )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EXT_3 )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EXT_3 )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EXT_O )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EXT_O )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_A )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_A )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_N )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_N )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_M )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_M )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_U )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_U )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_C )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_C )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_S )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_S )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_A )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_A )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_R )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_R )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_Z )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_Z )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_T )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_T )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ST_S )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ST_S )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_M )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_M )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_A )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_A )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_V )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_V )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_MM )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_MM )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SEG_J )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_J )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EXT_2 )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EXT_2 )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CON_D )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CON_D )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DiscardPenDownDurMin )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DiscardPenDownDurMin )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_PenDownDurMin )( 
            IProcSetFlags * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PenDownDurMin )( 
            IProcSetFlags * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NormDBCalcScore )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NormDBCalcScore )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NormDBCalcScoreGlobal )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NormDBCalcScoreGlobal )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NormDBNoUpdate )( 
            IProcSetFlags * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NormDBNoUpdate )( 
            IProcSetFlags * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NormDBThreshold )( 
            IProcSetFlags * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NormDBThreshold )( 
            IProcSetFlags * This,
            /* [retval][out] */ double *newVal);
        
        END_INTERFACE
    } IProcSetFlagsVtbl;

    interface IProcSetFlags
    {
        CONST_VTBL struct IProcSetFlagsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProcSetFlags_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IProcSetFlags_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IProcSetFlags_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IProcSetFlags_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IProcSetFlags_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IProcSetFlags_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IProcSetFlags_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IProcSetFlags_get_ST_D(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_D(This,pVal) ) 

#define IProcSetFlags_put_ST_D(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_D(This,newVal) ) 

#define IProcSetFlags_get_DiscardAfter(This,pVal)	\
    ( (This)->lpVtbl -> get_DiscardAfter(This,pVal) ) 

#define IProcSetFlags_put_DiscardAfter(This,newVal)	\
    ( (This)->lpVtbl -> put_DiscardAfter(This,newVal) ) 

#define IProcSetFlags_get_ST_D2(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_D2(This,pVal) ) 

#define IProcSetFlags_put_ST_D2(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_D2(This,newVal) ) 

#define IProcSetFlags_get_DiscardAfter2(This,pVal)	\
    ( (This)->lpVtbl -> get_DiscardAfter2(This,pVal) ) 

#define IProcSetFlags_put_DiscardAfter2(This,newVal)	\
    ( (This)->lpVtbl -> put_DiscardAfter2(This,newVal) ) 

#define IProcSetFlags_get_TF_O(This,pVal)	\
    ( (This)->lpVtbl -> get_TF_O(This,pVal) ) 

#define IProcSetFlags_put_TF_O(This,newVal)	\
    ( (This)->lpVtbl -> put_TF_O(This,newVal) ) 

#define IProcSetFlags_get_CON_O(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_O(This,pVal) ) 

#define IProcSetFlags_put_CON_O(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_O(This,newVal) ) 

#define IProcSetFlags_get_TF_J(This,pVal)	\
    ( (This)->lpVtbl -> get_TF_J(This,pVal) ) 

#define IProcSetFlags_put_TF_J(This,newVal)	\
    ( (This)->lpVtbl -> put_TF_J(This,newVal) ) 

#define IProcSetFlags_get_TF_R(This,pVal)	\
    ( (This)->lpVtbl -> get_TF_R(This,pVal) ) 

#define IProcSetFlags_put_TF_R(This,newVal)	\
    ( (This)->lpVtbl -> put_TF_R(This,newVal) ) 

#define IProcSetFlags_get_TF_A(This,pVal)	\
    ( (This)->lpVtbl -> get_TF_A(This,pVal) ) 

#define IProcSetFlags_put_TF_A(This,newVal)	\
    ( (This)->lpVtbl -> put_TF_A(This,newVal) ) 

#define IProcSetFlags_get_TF_U(This,pVal)	\
    ( (This)->lpVtbl -> get_TF_U(This,pVal) ) 

#define IProcSetFlags_put_TF_U(This,newVal)	\
    ( (This)->lpVtbl -> put_TF_U(This,newVal) ) 

#define IProcSetFlags_get_SEG_F(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_F(This,pVal) ) 

#define IProcSetFlags_put_SEG_F(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_F(This,newVal) ) 

#define IProcSetFlags_get_SEG_L(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_L(This,pVal) ) 

#define IProcSetFlags_put_SEG_L(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_L(This,newVal) ) 

#define IProcSetFlags_get_SEG_S(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_S(This,pVal) ) 

#define IProcSetFlags_put_SEG_S(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_S(This,newVal) ) 

#define IProcSetFlags_get_SEG_O(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_O(This,pVal) ) 

#define IProcSetFlags_put_SEG_O(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_O(This,newVal) ) 

#define IProcSetFlags_get_EXT_S(This,pVal)	\
    ( (This)->lpVtbl -> get_EXT_S(This,pVal) ) 

#define IProcSetFlags_put_EXT_S(This,newVal)	\
    ( (This)->lpVtbl -> put_EXT_S(This,newVal) ) 

#define IProcSetFlags_get_EXT_3(This,pVal)	\
    ( (This)->lpVtbl -> get_EXT_3(This,pVal) ) 

#define IProcSetFlags_put_EXT_3(This,newVal)	\
    ( (This)->lpVtbl -> put_EXT_3(This,newVal) ) 

#define IProcSetFlags_get_EXT_O(This,pVal)	\
    ( (This)->lpVtbl -> get_EXT_O(This,pVal) ) 

#define IProcSetFlags_put_EXT_O(This,newVal)	\
    ( (This)->lpVtbl -> put_EXT_O(This,newVal) ) 

#define IProcSetFlags_get_CON_A(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_A(This,pVal) ) 

#define IProcSetFlags_put_CON_A(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_A(This,newVal) ) 

#define IProcSetFlags_get_CON_N(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_N(This,pVal) ) 

#define IProcSetFlags_put_CON_N(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_N(This,newVal) ) 

#define IProcSetFlags_get_CON_M(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_M(This,pVal) ) 

#define IProcSetFlags_put_CON_M(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_M(This,newVal) ) 

#define IProcSetFlags_get_CON_U(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_U(This,pVal) ) 

#define IProcSetFlags_put_CON_U(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_U(This,newVal) ) 

#define IProcSetFlags_get_CON_C(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_C(This,pVal) ) 

#define IProcSetFlags_put_CON_C(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_C(This,newVal) ) 

#define IProcSetFlags_get_CON_S(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_S(This,pVal) ) 

#define IProcSetFlags_put_CON_S(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_S(This,newVal) ) 

#define IProcSetFlags_get_ST_A(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_A(This,pVal) ) 

#define IProcSetFlags_put_ST_A(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_A(This,newVal) ) 

#define IProcSetFlags_get_ST_R(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_R(This,pVal) ) 

#define IProcSetFlags_put_ST_R(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_R(This,newVal) ) 

#define IProcSetFlags_get_ST_Z(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_Z(This,pVal) ) 

#define IProcSetFlags_put_ST_Z(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_Z(This,newVal) ) 

#define IProcSetFlags_get_ST_T(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_T(This,pVal) ) 

#define IProcSetFlags_put_ST_T(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_T(This,newVal) ) 

#define IProcSetFlags_get_ST_S(This,pVal)	\
    ( (This)->lpVtbl -> get_ST_S(This,pVal) ) 

#define IProcSetFlags_put_ST_S(This,newVal)	\
    ( (This)->lpVtbl -> put_ST_S(This,newVal) ) 

#define IProcSetFlags_get_SEG_M(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_M(This,pVal) ) 

#define IProcSetFlags_put_SEG_M(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_M(This,newVal) ) 

#define IProcSetFlags_get_SEG_A(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_A(This,pVal) ) 

#define IProcSetFlags_put_SEG_A(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_A(This,newVal) ) 

#define IProcSetFlags_get_SEG_V(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_V(This,pVal) ) 

#define IProcSetFlags_put_SEG_V(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_V(This,newVal) ) 

#define IProcSetFlags_get_SEG_MM(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_MM(This,pVal) ) 

#define IProcSetFlags_put_SEG_MM(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_MM(This,newVal) ) 

#define IProcSetFlags_get_SEG_J(This,pVal)	\
    ( (This)->lpVtbl -> get_SEG_J(This,pVal) ) 

#define IProcSetFlags_put_SEG_J(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_J(This,newVal) ) 

#define IProcSetFlags_get_EXT_2(This,pVal)	\
    ( (This)->lpVtbl -> get_EXT_2(This,pVal) ) 

#define IProcSetFlags_put_EXT_2(This,newVal)	\
    ( (This)->lpVtbl -> put_EXT_2(This,newVal) ) 

#define IProcSetFlags_get_CON_D(This,pVal)	\
    ( (This)->lpVtbl -> get_CON_D(This,pVal) ) 

#define IProcSetFlags_put_CON_D(This,newVal)	\
    ( (This)->lpVtbl -> put_CON_D(This,newVal) ) 

#define IProcSetFlags_get_DiscardPenDownDurMin(This,pVal)	\
    ( (This)->lpVtbl -> get_DiscardPenDownDurMin(This,pVal) ) 

#define IProcSetFlags_put_DiscardPenDownDurMin(This,newVal)	\
    ( (This)->lpVtbl -> put_DiscardPenDownDurMin(This,newVal) ) 

#define IProcSetFlags_get_PenDownDurMin(This,pVal)	\
    ( (This)->lpVtbl -> get_PenDownDurMin(This,pVal) ) 

#define IProcSetFlags_put_PenDownDurMin(This,newVal)	\
    ( (This)->lpVtbl -> put_PenDownDurMin(This,newVal) ) 

#define IProcSetFlags_get_NormDBCalcScore(This,pVal)	\
    ( (This)->lpVtbl -> get_NormDBCalcScore(This,pVal) ) 

#define IProcSetFlags_put_NormDBCalcScore(This,newVal)	\
    ( (This)->lpVtbl -> put_NormDBCalcScore(This,newVal) ) 

#define IProcSetFlags_get_NormDBCalcScoreGlobal(This,pVal)	\
    ( (This)->lpVtbl -> get_NormDBCalcScoreGlobal(This,pVal) ) 

#define IProcSetFlags_put_NormDBCalcScoreGlobal(This,newVal)	\
    ( (This)->lpVtbl -> put_NormDBCalcScoreGlobal(This,newVal) ) 

#define IProcSetFlags_get_NormDBNoUpdate(This,pVal)	\
    ( (This)->lpVtbl -> get_NormDBNoUpdate(This,pVal) ) 

#define IProcSetFlags_put_NormDBNoUpdate(This,newVal)	\
    ( (This)->lpVtbl -> put_NormDBNoUpdate(This,newVal) ) 

#define IProcSetFlags_put_NormDBThreshold(This,newVal)	\
    ( (This)->lpVtbl -> put_NormDBThreshold(This,newVal) ) 

#define IProcSetFlags_get_NormDBThreshold(This,newVal)	\
    ( (This)->lpVtbl -> get_NormDBThreshold(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IProcSetFlags_INTERFACE_DEFINED__ */


#ifndef __INSConditionSound_INTERFACE_DEFINED__
#define __INSConditionSound_INTERFACE_DEFINED__

/* interface INSConditionSound */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eSoundType
    {	eSndT_None	= 0,
	eSndT_Start	= 1,
	eSndT_Stop	= 2,
	eSndT_StimWStart	= 3,
	eSndT_StimWStop	= 4,
	eSndT_StimPStart	= 5,
	eSndT_StimPStop	= 6,
	eSndT_TargetCorrect	= 7,
	eSndT_TargetWrong	= 8,
	eSndT_Penup	= 9,
	eSndT_Pendown	= 10
    } 	SoundType;

typedef 
enum eSoundCat
    {	eSC_None	= 0,
	eSC_Use	= 1,
	eSC_Tone	= 2,
	eSC_Frequency	= 3,
	eSC_Duration	= 4,
	eSC_Media	= 5,
	eSC_Trigger	= 6
    } 	SoundCat;


EXTERN_C const IID IID_INSConditionSound;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B7BB2789-A991-453B-B922-24FC5547B6E8")
    INSConditionSound : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundUse( 
            /* [in] */ SoundType type,
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundUse( 
            /* [in] */ SoundType type,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundTone( 
            /* [in] */ SoundType type,
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundTone( 
            /* [in] */ SoundType type,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundToneFreq( 
            /* [in] */ SoundType type,
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundToneFreq( 
            /* [in] */ SoundType type,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundToneDur( 
            /* [in] */ SoundType type,
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundToneDur( 
            /* [in] */ SoundType type,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundMedia( 
            /* [in] */ SoundType type,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundMedia( 
            /* [in] */ SoundType type,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundTrigger( 
            /* [in] */ SoundType type,
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundTrigger( 
            /* [in] */ SoundType type,
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundInfo( 
            /* [in] */ SoundCat cat,
            /* [in] */ SoundType type,
            /* [retval][out] */ VARIANT *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SoundInfo( 
            /* [in] */ SoundCat cat,
            /* [in] */ SoundType type,
            /* [in] */ VARIANT newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SoundCount( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE get_SoundScript( 
            /* [in] */ SoundType type,
            /* [out] */ short *ScriptType,
            /* [out] */ BSTR *pScript) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE put_SoundScript( 
            /* [in] */ SoundType type,
            /* [in] */ short ScriptType,
            /* [in] */ BSTR Script) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSConditionSoundVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSConditionSound * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSConditionSound * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSConditionSound * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSConditionSound * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSConditionSound * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSConditionSound * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSConditionSound * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundUse )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundUse )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundTone )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundTone )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundToneFreq )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundToneFreq )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundToneDur )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundToneDur )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundMedia )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundMedia )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundTrigger )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundTrigger )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundInfo )( 
            INSConditionSound * This,
            /* [in] */ SoundCat cat,
            /* [in] */ SoundType type,
            /* [retval][out] */ VARIANT *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SoundInfo )( 
            INSConditionSound * This,
            /* [in] */ SoundCat cat,
            /* [in] */ SoundType type,
            /* [in] */ VARIANT newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SoundCount )( 
            INSConditionSound * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *get_SoundScript )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [out] */ short *ScriptType,
            /* [out] */ BSTR *pScript);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *put_SoundScript )( 
            INSConditionSound * This,
            /* [in] */ SoundType type,
            /* [in] */ short ScriptType,
            /* [in] */ BSTR Script);
        
        END_INTERFACE
    } INSConditionSoundVtbl;

    interface INSConditionSound
    {
        CONST_VTBL struct INSConditionSoundVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSConditionSound_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSConditionSound_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSConditionSound_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSConditionSound_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSConditionSound_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSConditionSound_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSConditionSound_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSConditionSound_get_SoundUse(This,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundUse(This,type,pVal) ) 

#define INSConditionSound_put_SoundUse(This,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundUse(This,type,newVal) ) 

#define INSConditionSound_get_SoundTone(This,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundTone(This,type,pVal) ) 

#define INSConditionSound_put_SoundTone(This,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundTone(This,type,newVal) ) 

#define INSConditionSound_get_SoundToneFreq(This,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundToneFreq(This,type,pVal) ) 

#define INSConditionSound_put_SoundToneFreq(This,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundToneFreq(This,type,newVal) ) 

#define INSConditionSound_get_SoundToneDur(This,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundToneDur(This,type,pVal) ) 

#define INSConditionSound_put_SoundToneDur(This,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundToneDur(This,type,newVal) ) 

#define INSConditionSound_get_SoundMedia(This,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundMedia(This,type,pVal) ) 

#define INSConditionSound_put_SoundMedia(This,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundMedia(This,type,newVal) ) 

#define INSConditionSound_get_SoundTrigger(This,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundTrigger(This,type,pVal) ) 

#define INSConditionSound_put_SoundTrigger(This,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundTrigger(This,type,newVal) ) 

#define INSConditionSound_get_SoundInfo(This,cat,type,pVal)	\
    ( (This)->lpVtbl -> get_SoundInfo(This,cat,type,pVal) ) 

#define INSConditionSound_put_SoundInfo(This,cat,type,newVal)	\
    ( (This)->lpVtbl -> put_SoundInfo(This,cat,type,newVal) ) 

#define INSConditionSound_get_SoundCount(This,pVal)	\
    ( (This)->lpVtbl -> get_SoundCount(This,pVal) ) 

#define INSConditionSound_get_SoundScript(This,type,ScriptType,pScript)	\
    ( (This)->lpVtbl -> get_SoundScript(This,type,ScriptType,pScript) ) 

#define INSConditionSound_put_SoundScript(This,type,ScriptType,Script)	\
    ( (This)->lpVtbl -> put_SoundScript(This,type,ScriptType,Script) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INSConditionSound_INTERFACE_DEFINED__ */


#ifndef __IProcSetRunExp_INTERFACE_DEFINED__
#define __IProcSetRunExp_INTERFACE_DEFINED__

/* interface IProcSetRunExp */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eExternalAppType
    {	eEAT_None	= 0,
	eEAT_Matlab	= 0x1,
	eEAT_Batch	= 0x2
    } 	ExternalAppType;


EXTERN_C const IID IID_IProcSetRunExp;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C03A1C19-9D10-4950-96FE-CBBBFACC9672")
    IProcSetRunExp : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DoQuestionnaire( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DoQuestionnaire( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TimeoutStart( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TimeoutStart( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TimeoutRecord( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TimeoutRecord( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TimeoutPenlift( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TimeoutPenlift( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TimeoutLatency( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TimeoutLatency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxRecArea( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxRecArea( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ProcImmediately( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ProcImmediately( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplayCharts( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayCharts( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplayRaw( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayRaw( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplayTF( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayTF( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplaySeg( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplaySeg( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Summarize( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Summarize( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SummarizeOnly( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SummarizeOnly( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SummarizeSelect( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SummarizeSelect( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Analyze( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Analyze( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ViewSubjExt( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ViewSubjExt( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ViewSubjCon( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ViewSubjCon( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ViewExpExt( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ViewExpExt( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ViewExpCon( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ViewExpCon( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDrawingOptions( 
            /* [in] */ short LineSize,
            /* [in] */ short EllipseSize,
            /* [in] */ DWORD BGColor,
            /* [in] */ DWORD LineColor1,
            /* [in] */ DWORD LineColor2,
            /* [in] */ DWORD LineColor3,
            /* [in] */ DWORD EllipseColor,
            /* [in] */ DWORD MPPColor) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDrawingOptions( 
            /* [out] */ short *LineSize,
            /* [out] */ short *EllipseSize,
            /* [out] */ DWORD *BGColor,
            /* [out] */ DWORD *LineColor1,
            /* [out] */ DWORD *LineColor2,
            /* [out] */ DWORD *LineColor3,
            /* [out] */ DWORD *EllipseColor,
            /* [out] */ DWORD *MPPColor) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_VaryLineThickness( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_VaryLineThickness( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxLineThickness( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxLineThickness( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_QuestAtEnd( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_QuestAtEnd( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FullScreen( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FullScreen( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppTypeRaw( 
            /* [retval][out] */ ExternalAppType *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppTypeRaw( 
            /* [in] */ ExternalAppType newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppScriptRaw( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppScriptRaw( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppTypeTF( 
            /* [retval][out] */ ExternalAppType *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppTypeTF( 
            /* [in] */ ExternalAppType newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppScriptTF( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppScriptTF( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppTypeSeg( 
            /* [retval][out] */ ExternalAppType *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppTypeSeg( 
            /* [in] */ ExternalAppType newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppScriptSeg( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppScriptSeg( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppTypeExt( 
            /* [retval][out] */ ExternalAppType *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppTypeExt( 
            /* [in] */ ExternalAppType newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExtAppScriptExt( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExtAppScriptExt( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SummarizeOnlyGroup( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SummarizeOnlyGroup( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SummarizeNormDB( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SummarizeNormDB( 
            /* [in] */ BOOL newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProcSetRunExpVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProcSetRunExp * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProcSetRunExp * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProcSetRunExp * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProcSetRunExp * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProcSetRunExp * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProcSetRunExp * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProcSetRunExp * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DoQuestionnaire )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DoQuestionnaire )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TimeoutStart )( 
            IProcSetRunExp * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TimeoutStart )( 
            IProcSetRunExp * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TimeoutRecord )( 
            IProcSetRunExp * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TimeoutRecord )( 
            IProcSetRunExp * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TimeoutPenlift )( 
            IProcSetRunExp * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TimeoutPenlift )( 
            IProcSetRunExp * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TimeoutLatency )( 
            IProcSetRunExp * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TimeoutLatency )( 
            IProcSetRunExp * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxRecArea )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxRecArea )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProcImmediately )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ProcImmediately )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayCharts )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayCharts )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayRaw )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayRaw )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayTF )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayTF )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplaySeg )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplaySeg )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Summarize )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Summarize )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SummarizeOnly )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SummarizeOnly )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SummarizeSelect )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SummarizeSelect )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Analyze )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Analyze )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ViewSubjExt )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ViewSubjExt )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ViewSubjCon )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ViewSubjCon )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ViewExpExt )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ViewExpExt )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ViewExpCon )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ViewExpCon )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDrawingOptions )( 
            IProcSetRunExp * This,
            /* [in] */ short LineSize,
            /* [in] */ short EllipseSize,
            /* [in] */ DWORD BGColor,
            /* [in] */ DWORD LineColor1,
            /* [in] */ DWORD LineColor2,
            /* [in] */ DWORD LineColor3,
            /* [in] */ DWORD EllipseColor,
            /* [in] */ DWORD MPPColor);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDrawingOptions )( 
            IProcSetRunExp * This,
            /* [out] */ short *LineSize,
            /* [out] */ short *EllipseSize,
            /* [out] */ DWORD *BGColor,
            /* [out] */ DWORD *LineColor1,
            /* [out] */ DWORD *LineColor2,
            /* [out] */ DWORD *LineColor3,
            /* [out] */ DWORD *EllipseColor,
            /* [out] */ DWORD *MPPColor);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_VaryLineThickness )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_VaryLineThickness )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxLineThickness )( 
            IProcSetRunExp * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxLineThickness )( 
            IProcSetRunExp * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_QuestAtEnd )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_QuestAtEnd )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FullScreen )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FullScreen )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppTypeRaw )( 
            IProcSetRunExp * This,
            /* [retval][out] */ ExternalAppType *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppTypeRaw )( 
            IProcSetRunExp * This,
            /* [in] */ ExternalAppType newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppScriptRaw )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppScriptRaw )( 
            IProcSetRunExp * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppTypeTF )( 
            IProcSetRunExp * This,
            /* [retval][out] */ ExternalAppType *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppTypeTF )( 
            IProcSetRunExp * This,
            /* [in] */ ExternalAppType newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppScriptTF )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppScriptTF )( 
            IProcSetRunExp * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppTypeSeg )( 
            IProcSetRunExp * This,
            /* [retval][out] */ ExternalAppType *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppTypeSeg )( 
            IProcSetRunExp * This,
            /* [in] */ ExternalAppType newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppScriptSeg )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppScriptSeg )( 
            IProcSetRunExp * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppTypeExt )( 
            IProcSetRunExp * This,
            /* [retval][out] */ ExternalAppType *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppTypeExt )( 
            IProcSetRunExp * This,
            /* [in] */ ExternalAppType newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExtAppScriptExt )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExtAppScriptExt )( 
            IProcSetRunExp * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SummarizeOnlyGroup )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SummarizeOnlyGroup )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SummarizeNormDB )( 
            IProcSetRunExp * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SummarizeNormDB )( 
            IProcSetRunExp * This,
            /* [in] */ BOOL newVal);
        
        END_INTERFACE
    } IProcSetRunExpVtbl;

    interface IProcSetRunExp
    {
        CONST_VTBL struct IProcSetRunExpVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProcSetRunExp_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IProcSetRunExp_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IProcSetRunExp_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IProcSetRunExp_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IProcSetRunExp_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IProcSetRunExp_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IProcSetRunExp_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IProcSetRunExp_get_DoQuestionnaire(This,pVal)	\
    ( (This)->lpVtbl -> get_DoQuestionnaire(This,pVal) ) 

#define IProcSetRunExp_put_DoQuestionnaire(This,newVal)	\
    ( (This)->lpVtbl -> put_DoQuestionnaire(This,newVal) ) 

#define IProcSetRunExp_get_TimeoutStart(This,pVal)	\
    ( (This)->lpVtbl -> get_TimeoutStart(This,pVal) ) 

#define IProcSetRunExp_put_TimeoutStart(This,newVal)	\
    ( (This)->lpVtbl -> put_TimeoutStart(This,newVal) ) 

#define IProcSetRunExp_get_TimeoutRecord(This,pVal)	\
    ( (This)->lpVtbl -> get_TimeoutRecord(This,pVal) ) 

#define IProcSetRunExp_put_TimeoutRecord(This,newVal)	\
    ( (This)->lpVtbl -> put_TimeoutRecord(This,newVal) ) 

#define IProcSetRunExp_get_TimeoutPenlift(This,pVal)	\
    ( (This)->lpVtbl -> get_TimeoutPenlift(This,pVal) ) 

#define IProcSetRunExp_put_TimeoutPenlift(This,newVal)	\
    ( (This)->lpVtbl -> put_TimeoutPenlift(This,newVal) ) 

#define IProcSetRunExp_get_TimeoutLatency(This,pVal)	\
    ( (This)->lpVtbl -> get_TimeoutLatency(This,pVal) ) 

#define IProcSetRunExp_put_TimeoutLatency(This,newVal)	\
    ( (This)->lpVtbl -> put_TimeoutLatency(This,newVal) ) 

#define IProcSetRunExp_get_MaxRecArea(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxRecArea(This,pVal) ) 

#define IProcSetRunExp_put_MaxRecArea(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxRecArea(This,newVal) ) 

#define IProcSetRunExp_get_ProcImmediately(This,pVal)	\
    ( (This)->lpVtbl -> get_ProcImmediately(This,pVal) ) 

#define IProcSetRunExp_put_ProcImmediately(This,newVal)	\
    ( (This)->lpVtbl -> put_ProcImmediately(This,newVal) ) 

#define IProcSetRunExp_get_DisplayCharts(This,pVal)	\
    ( (This)->lpVtbl -> get_DisplayCharts(This,pVal) ) 

#define IProcSetRunExp_put_DisplayCharts(This,newVal)	\
    ( (This)->lpVtbl -> put_DisplayCharts(This,newVal) ) 

#define IProcSetRunExp_get_DisplayRaw(This,pVal)	\
    ( (This)->lpVtbl -> get_DisplayRaw(This,pVal) ) 

#define IProcSetRunExp_put_DisplayRaw(This,newVal)	\
    ( (This)->lpVtbl -> put_DisplayRaw(This,newVal) ) 

#define IProcSetRunExp_get_DisplayTF(This,pVal)	\
    ( (This)->lpVtbl -> get_DisplayTF(This,pVal) ) 

#define IProcSetRunExp_put_DisplayTF(This,newVal)	\
    ( (This)->lpVtbl -> put_DisplayTF(This,newVal) ) 

#define IProcSetRunExp_get_DisplaySeg(This,pVal)	\
    ( (This)->lpVtbl -> get_DisplaySeg(This,pVal) ) 

#define IProcSetRunExp_put_DisplaySeg(This,newVal)	\
    ( (This)->lpVtbl -> put_DisplaySeg(This,newVal) ) 

#define IProcSetRunExp_get_Summarize(This,pVal)	\
    ( (This)->lpVtbl -> get_Summarize(This,pVal) ) 

#define IProcSetRunExp_put_Summarize(This,newVal)	\
    ( (This)->lpVtbl -> put_Summarize(This,newVal) ) 

#define IProcSetRunExp_get_SummarizeOnly(This,pVal)	\
    ( (This)->lpVtbl -> get_SummarizeOnly(This,pVal) ) 

#define IProcSetRunExp_put_SummarizeOnly(This,newVal)	\
    ( (This)->lpVtbl -> put_SummarizeOnly(This,newVal) ) 

#define IProcSetRunExp_get_SummarizeSelect(This,pVal)	\
    ( (This)->lpVtbl -> get_SummarizeSelect(This,pVal) ) 

#define IProcSetRunExp_put_SummarizeSelect(This,newVal)	\
    ( (This)->lpVtbl -> put_SummarizeSelect(This,newVal) ) 

#define IProcSetRunExp_get_Analyze(This,pVal)	\
    ( (This)->lpVtbl -> get_Analyze(This,pVal) ) 

#define IProcSetRunExp_put_Analyze(This,newVal)	\
    ( (This)->lpVtbl -> put_Analyze(This,newVal) ) 

#define IProcSetRunExp_get_ViewSubjExt(This,pVal)	\
    ( (This)->lpVtbl -> get_ViewSubjExt(This,pVal) ) 

#define IProcSetRunExp_put_ViewSubjExt(This,newVal)	\
    ( (This)->lpVtbl -> put_ViewSubjExt(This,newVal) ) 

#define IProcSetRunExp_get_ViewSubjCon(This,pVal)	\
    ( (This)->lpVtbl -> get_ViewSubjCon(This,pVal) ) 

#define IProcSetRunExp_put_ViewSubjCon(This,newVal)	\
    ( (This)->lpVtbl -> put_ViewSubjCon(This,newVal) ) 

#define IProcSetRunExp_get_ViewExpExt(This,pVal)	\
    ( (This)->lpVtbl -> get_ViewExpExt(This,pVal) ) 

#define IProcSetRunExp_put_ViewExpExt(This,newVal)	\
    ( (This)->lpVtbl -> put_ViewExpExt(This,newVal) ) 

#define IProcSetRunExp_get_ViewExpCon(This,pVal)	\
    ( (This)->lpVtbl -> get_ViewExpCon(This,pVal) ) 

#define IProcSetRunExp_put_ViewExpCon(This,newVal)	\
    ( (This)->lpVtbl -> put_ViewExpCon(This,newVal) ) 

#define IProcSetRunExp_SetDrawingOptions(This,LineSize,EllipseSize,BGColor,LineColor1,LineColor2,LineColor3,EllipseColor,MPPColor)	\
    ( (This)->lpVtbl -> SetDrawingOptions(This,LineSize,EllipseSize,BGColor,LineColor1,LineColor2,LineColor3,EllipseColor,MPPColor) ) 

#define IProcSetRunExp_GetDrawingOptions(This,LineSize,EllipseSize,BGColor,LineColor1,LineColor2,LineColor3,EllipseColor,MPPColor)	\
    ( (This)->lpVtbl -> GetDrawingOptions(This,LineSize,EllipseSize,BGColor,LineColor1,LineColor2,LineColor3,EllipseColor,MPPColor) ) 

#define IProcSetRunExp_get_VaryLineThickness(This,pVal)	\
    ( (This)->lpVtbl -> get_VaryLineThickness(This,pVal) ) 

#define IProcSetRunExp_put_VaryLineThickness(This,newVal)	\
    ( (This)->lpVtbl -> put_VaryLineThickness(This,newVal) ) 

#define IProcSetRunExp_get_MaxLineThickness(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxLineThickness(This,pVal) ) 

#define IProcSetRunExp_put_MaxLineThickness(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxLineThickness(This,newVal) ) 

#define IProcSetRunExp_get_QuestAtEnd(This,pVal)	\
    ( (This)->lpVtbl -> get_QuestAtEnd(This,pVal) ) 

#define IProcSetRunExp_put_QuestAtEnd(This,newVal)	\
    ( (This)->lpVtbl -> put_QuestAtEnd(This,newVal) ) 

#define IProcSetRunExp_get_FullScreen(This,pVal)	\
    ( (This)->lpVtbl -> get_FullScreen(This,pVal) ) 

#define IProcSetRunExp_put_FullScreen(This,newVal)	\
    ( (This)->lpVtbl -> put_FullScreen(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppTypeRaw(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppTypeRaw(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppTypeRaw(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppTypeRaw(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppScriptRaw(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppScriptRaw(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppScriptRaw(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppScriptRaw(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppTypeTF(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppTypeTF(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppTypeTF(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppTypeTF(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppScriptTF(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppScriptTF(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppScriptTF(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppScriptTF(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppTypeSeg(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppTypeSeg(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppTypeSeg(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppTypeSeg(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppScriptSeg(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppScriptSeg(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppScriptSeg(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppScriptSeg(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppTypeExt(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppTypeExt(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppTypeExt(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppTypeExt(This,newVal) ) 

#define IProcSetRunExp_get_ExtAppScriptExt(This,pVal)	\
    ( (This)->lpVtbl -> get_ExtAppScriptExt(This,pVal) ) 

#define IProcSetRunExp_put_ExtAppScriptExt(This,newVal)	\
    ( (This)->lpVtbl -> put_ExtAppScriptExt(This,newVal) ) 

#define IProcSetRunExp_get_SummarizeOnlyGroup(This,pVal)	\
    ( (This)->lpVtbl -> get_SummarizeOnlyGroup(This,pVal) ) 

#define IProcSetRunExp_put_SummarizeOnlyGroup(This,newVal)	\
    ( (This)->lpVtbl -> put_SummarizeOnlyGroup(This,newVal) ) 

#define IProcSetRunExp_get_SummarizeNormDB(This,pVal)	\
    ( (This)->lpVtbl -> get_SummarizeNormDB(This,pVal) ) 

#define IProcSetRunExp_put_SummarizeNormDB(This,newVal)	\
    ( (This)->lpVtbl -> put_SummarizeNormDB(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IProcSetRunExp_INTERFACE_DEFINED__ */


#ifndef __IExperimentMember_INTERFACE_DEFINED__
#define __IExperimentMember_INTERFACE_DEFINED__

/* interface IExperimentMember */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IExperimentMember;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05EE8-A758-11D3-8A58-000000000000")
    IExperimentMember : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExperimentID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExperimentID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubjectID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubjectID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForGroup( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForExperiment( 
            /* [in] */ BSTR ExpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveGroup( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DeviceRes( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DeviceRes( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SamplingRate( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SamplingRate( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Exported( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Exported( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExpWhen( 
            /* [retval][out] */ DATE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExpWhen( 
            /* [in] */ DATE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExpWhere( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExpWhere( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Uploaded( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Uploaded( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UpWhen( 
            /* [retval][out] */ DATE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UpWhen( 
            /* [in] */ DATE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UpWhere( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UpWhere( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IExperimentMemberVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IExperimentMember * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IExperimentMember * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IExperimentMember * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IExperimentMember * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IExperimentMember * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IExperimentMember * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IExperimentMember * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExperimentID )( 
            IExperimentMember * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExperimentID )( 
            IExperimentMember * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupID )( 
            IExperimentMember * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupID )( 
            IExperimentMember * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubjectID )( 
            IExperimentMember * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubjectID )( 
            IExperimentMember * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IExperimentMember * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForGroup )( 
            IExperimentMember * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForExperiment )( 
            IExperimentMember * This,
            /* [in] */ BSTR ExpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveGroup )( 
            IExperimentMember * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IExperimentMember * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IExperimentMember * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IExperimentMember * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IExperimentMember * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DeviceRes )( 
            IExperimentMember * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DeviceRes )( 
            IExperimentMember * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SamplingRate )( 
            IExperimentMember * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SamplingRate )( 
            IExperimentMember * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Exported )( 
            IExperimentMember * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Exported )( 
            IExperimentMember * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExpWhen )( 
            IExperimentMember * This,
            /* [retval][out] */ DATE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExpWhen )( 
            IExperimentMember * This,
            /* [in] */ DATE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExpWhere )( 
            IExperimentMember * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExpWhere )( 
            IExperimentMember * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Uploaded )( 
            IExperimentMember * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Uploaded )( 
            IExperimentMember * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UpWhen )( 
            IExperimentMember * This,
            /* [retval][out] */ DATE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UpWhen )( 
            IExperimentMember * This,
            /* [in] */ DATE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UpWhere )( 
            IExperimentMember * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UpWhere )( 
            IExperimentMember * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } IExperimentMemberVtbl;

    interface IExperimentMember
    {
        CONST_VTBL struct IExperimentMemberVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IExperimentMember_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IExperimentMember_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IExperimentMember_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IExperimentMember_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IExperimentMember_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IExperimentMember_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IExperimentMember_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IExperimentMember_get_ExperimentID(This,pVal)	\
    ( (This)->lpVtbl -> get_ExperimentID(This,pVal) ) 

#define IExperimentMember_put_ExperimentID(This,newVal)	\
    ( (This)->lpVtbl -> put_ExperimentID(This,newVal) ) 

#define IExperimentMember_get_GroupID(This,pVal)	\
    ( (This)->lpVtbl -> get_GroupID(This,pVal) ) 

#define IExperimentMember_put_GroupID(This,newVal)	\
    ( (This)->lpVtbl -> put_GroupID(This,newVal) ) 

#define IExperimentMember_get_SubjectID(This,pVal)	\
    ( (This)->lpVtbl -> get_SubjectID(This,pVal) ) 

#define IExperimentMember_put_SubjectID(This,newVal)	\
    ( (This)->lpVtbl -> put_SubjectID(This,newVal) ) 

#define IExperimentMember_Find(This,ExpID,GrpID,SubjID)	\
    ( (This)->lpVtbl -> Find(This,ExpID,GrpID,SubjID) ) 

#define IExperimentMember_FindForGroup(This,ExpID,GrpID)	\
    ( (This)->lpVtbl -> FindForGroup(This,ExpID,GrpID) ) 

#define IExperimentMember_FindForExperiment(This,ExpID)	\
    ( (This)->lpVtbl -> FindForExperiment(This,ExpID) ) 

#define IExperimentMember_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IExperimentMember_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IExperimentMember_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IExperimentMember_RemoveGroup(This,ExpID,GrpID)	\
    ( (This)->lpVtbl -> RemoveGroup(This,ExpID,GrpID) ) 

#define IExperimentMember_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IExperimentMember_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IExperimentMember_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IExperimentMember_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IExperimentMember_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IExperimentMember_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IExperimentMember_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IExperimentMember_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IExperimentMember_get_DeviceRes(This,pVal)	\
    ( (This)->lpVtbl -> get_DeviceRes(This,pVal) ) 

#define IExperimentMember_put_DeviceRes(This,newVal)	\
    ( (This)->lpVtbl -> put_DeviceRes(This,newVal) ) 

#define IExperimentMember_get_SamplingRate(This,pVal)	\
    ( (This)->lpVtbl -> get_SamplingRate(This,pVal) ) 

#define IExperimentMember_put_SamplingRate(This,newVal)	\
    ( (This)->lpVtbl -> put_SamplingRate(This,newVal) ) 

#define IExperimentMember_get_Exported(This,pVal)	\
    ( (This)->lpVtbl -> get_Exported(This,pVal) ) 

#define IExperimentMember_put_Exported(This,newVal)	\
    ( (This)->lpVtbl -> put_Exported(This,newVal) ) 

#define IExperimentMember_get_ExpWhen(This,pVal)	\
    ( (This)->lpVtbl -> get_ExpWhen(This,pVal) ) 

#define IExperimentMember_put_ExpWhen(This,newVal)	\
    ( (This)->lpVtbl -> put_ExpWhen(This,newVal) ) 

#define IExperimentMember_get_ExpWhere(This,pVal)	\
    ( (This)->lpVtbl -> get_ExpWhere(This,pVal) ) 

#define IExperimentMember_put_ExpWhere(This,newVal)	\
    ( (This)->lpVtbl -> put_ExpWhere(This,newVal) ) 

#define IExperimentMember_get_Uploaded(This,pVal)	\
    ( (This)->lpVtbl -> get_Uploaded(This,pVal) ) 

#define IExperimentMember_put_Uploaded(This,newVal)	\
    ( (This)->lpVtbl -> put_Uploaded(This,newVal) ) 

#define IExperimentMember_get_UpWhen(This,pVal)	\
    ( (This)->lpVtbl -> get_UpWhen(This,pVal) ) 

#define IExperimentMember_put_UpWhen(This,newVal)	\
    ( (This)->lpVtbl -> put_UpWhen(This,newVal) ) 

#define IExperimentMember_get_UpWhere(This,pVal)	\
    ( (This)->lpVtbl -> get_UpWhere(This,pVal) ) 

#define IExperimentMember_put_UpWhere(This,newVal)	\
    ( (This)->lpVtbl -> put_UpWhere(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IExperimentMember_INTERFACE_DEFINED__ */


#ifndef __IExperimentCondition_INTERFACE_DEFINED__
#define __IExperimentCondition_INTERFACE_DEFINED__

/* interface IExperimentCondition */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IExperimentCondition;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCB05EEB-A758-11D3-8A58-000000000000")
    IExperimentCondition : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExperimentID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExperimentID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ConditionID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ConditionID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Replications( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Replications( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR CondID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForExperiment( 
            /* [in] */ BSTR ExpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IExperimentConditionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IExperimentCondition * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IExperimentCondition * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IExperimentCondition * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IExperimentCondition * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IExperimentCondition * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IExperimentCondition * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IExperimentCondition * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExperimentID )( 
            IExperimentCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExperimentID )( 
            IExperimentCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConditionID )( 
            IExperimentCondition * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConditionID )( 
            IExperimentCondition * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Replications )( 
            IExperimentCondition * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Replications )( 
            IExperimentCondition * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IExperimentCondition * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR CondID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForExperiment )( 
            IExperimentCondition * This,
            /* [in] */ BSTR ExpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IExperimentCondition * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IExperimentCondition * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IExperimentCondition * This);
        
        END_INTERFACE
    } IExperimentConditionVtbl;

    interface IExperimentCondition
    {
        CONST_VTBL struct IExperimentConditionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IExperimentCondition_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IExperimentCondition_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IExperimentCondition_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IExperimentCondition_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IExperimentCondition_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IExperimentCondition_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IExperimentCondition_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IExperimentCondition_get_ExperimentID(This,pVal)	\
    ( (This)->lpVtbl -> get_ExperimentID(This,pVal) ) 

#define IExperimentCondition_put_ExperimentID(This,newVal)	\
    ( (This)->lpVtbl -> put_ExperimentID(This,newVal) ) 

#define IExperimentCondition_get_ConditionID(This,pVal)	\
    ( (This)->lpVtbl -> get_ConditionID(This,pVal) ) 

#define IExperimentCondition_put_ConditionID(This,newVal)	\
    ( (This)->lpVtbl -> put_ConditionID(This,newVal) ) 

#define IExperimentCondition_get_Replications(This,pVal)	\
    ( (This)->lpVtbl -> get_Replications(This,pVal) ) 

#define IExperimentCondition_put_Replications(This,newVal)	\
    ( (This)->lpVtbl -> put_Replications(This,newVal) ) 

#define IExperimentCondition_Find(This,ExpID,CondID)	\
    ( (This)->lpVtbl -> Find(This,ExpID,CondID) ) 

#define IExperimentCondition_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IExperimentCondition_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IExperimentCondition_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IExperimentCondition_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IExperimentCondition_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IExperimentCondition_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IExperimentCondition_FindForExperiment(This,ExpID)	\
    ( (This)->lpVtbl -> FindForExperiment(This,ExpID) ) 

#define IExperimentCondition_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IExperimentCondition_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IExperimentCondition_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IExperimentCondition_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IExperimentCondition_INTERFACE_DEFINED__ */


#ifndef __IMQuestionnaire_INTERFACE_DEFINED__
#define __IMQuestionnaire_INTERFACE_DEFINED__

/* interface IMQuestionnaire */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IMQuestionnaire;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6BC7F957-1612-11D4-8B4C-00104BC7E2C8")
    IMQuestionnaire : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ItemNum( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ItemNum( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsHeader( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IsHeader( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Question( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Question( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsPrivate( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IsPrivate( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsNumeric( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IsNumeric( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ColumnHeader( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ColumnHeader( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMQuestionnaireVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMQuestionnaire * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMQuestionnaire * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMQuestionnaire * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMQuestionnaire * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMQuestionnaire * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMQuestionnaire * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMQuestionnaire * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            IMQuestionnaire * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IMQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IMQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ItemNum )( 
            IMQuestionnaire * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ItemNum )( 
            IMQuestionnaire * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsHeader )( 
            IMQuestionnaire * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IsHeader )( 
            IMQuestionnaire * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Question )( 
            IMQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Question )( 
            IMQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IMQuestionnaire * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IMQuestionnaire * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsPrivate )( 
            IMQuestionnaire * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IsPrivate )( 
            IMQuestionnaire * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IMQuestionnaire * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IMQuestionnaire * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IMQuestionnaire * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsNumeric )( 
            IMQuestionnaire * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IsNumeric )( 
            IMQuestionnaire * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ColumnHeader )( 
            IMQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ColumnHeader )( 
            IMQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } IMQuestionnaireVtbl;

    interface IMQuestionnaire
    {
        CONST_VTBL struct IMQuestionnaireVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMQuestionnaire_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMQuestionnaire_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMQuestionnaire_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMQuestionnaire_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMQuestionnaire_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMQuestionnaire_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMQuestionnaire_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMQuestionnaire_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define IMQuestionnaire_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IMQuestionnaire_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IMQuestionnaire_get_ItemNum(This,pVal)	\
    ( (This)->lpVtbl -> get_ItemNum(This,pVal) ) 

#define IMQuestionnaire_put_ItemNum(This,newVal)	\
    ( (This)->lpVtbl -> put_ItemNum(This,newVal) ) 

#define IMQuestionnaire_get_IsHeader(This,pVal)	\
    ( (This)->lpVtbl -> get_IsHeader(This,pVal) ) 

#define IMQuestionnaire_put_IsHeader(This,newVal)	\
    ( (This)->lpVtbl -> put_IsHeader(This,newVal) ) 

#define IMQuestionnaire_get_Question(This,pVal)	\
    ( (This)->lpVtbl -> get_Question(This,pVal) ) 

#define IMQuestionnaire_put_Question(This,newVal)	\
    ( (This)->lpVtbl -> put_Question(This,newVal) ) 

#define IMQuestionnaire_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IMQuestionnaire_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IMQuestionnaire_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IMQuestionnaire_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IMQuestionnaire_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IMQuestionnaire_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IMQuestionnaire_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IMQuestionnaire_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IMQuestionnaire_get_IsPrivate(This,pVal)	\
    ( (This)->lpVtbl -> get_IsPrivate(This,pVal) ) 

#define IMQuestionnaire_put_IsPrivate(This,newVal)	\
    ( (This)->lpVtbl -> put_IsPrivate(This,newVal) ) 

#define IMQuestionnaire_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IMQuestionnaire_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IMQuestionnaire_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IMQuestionnaire_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IMQuestionnaire_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IMQuestionnaire_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#define IMQuestionnaire_get_IsNumeric(This,pVal)	\
    ( (This)->lpVtbl -> get_IsNumeric(This,pVal) ) 

#define IMQuestionnaire_put_IsNumeric(This,newVal)	\
    ( (This)->lpVtbl -> put_IsNumeric(This,newVal) ) 

#define IMQuestionnaire_get_ColumnHeader(This,pVal)	\
    ( (This)->lpVtbl -> get_ColumnHeader(This,pVal) ) 

#define IMQuestionnaire_put_ColumnHeader(This,newVal)	\
    ( (This)->lpVtbl -> put_ColumnHeader(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMQuestionnaire_INTERFACE_DEFINED__ */


#ifndef __IGQuestionnaire_INTERFACE_DEFINED__
#define __IGQuestionnaire_INTERFACE_DEFINED__

/* interface IGQuestionnaire */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IGQuestionnaire;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B37E09B3-2516-11D4-8B62-00104BC7E2C8")
    IGQuestionnaire : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ItemNum( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ItemNum( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForGroup( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveGroup( 
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveQuestion( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveForGroup( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExperimentID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExperimentID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGQuestionnaireVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGQuestionnaire * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGQuestionnaire * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGQuestionnaire * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGQuestionnaire * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGQuestionnaire * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGQuestionnaire * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGQuestionnaire * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupID )( 
            IGQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupID )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IGQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ItemNum )( 
            IGQuestionnaire * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ItemNum )( 
            IGQuestionnaire * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForGroup )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IGQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IGQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IGQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveGroup )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveQuestion )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IGQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IGQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IGQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveForGroup )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExperimentID )( 
            IGQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExperimentID )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IGQuestionnaire * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IGQuestionnaire * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IGQuestionnaire * This);
        
        END_INTERFACE
    } IGQuestionnaireVtbl;

    interface IGQuestionnaire
    {
        CONST_VTBL struct IGQuestionnaireVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGQuestionnaire_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGQuestionnaire_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGQuestionnaire_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGQuestionnaire_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGQuestionnaire_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGQuestionnaire_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGQuestionnaire_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IGQuestionnaire_get_GroupID(This,pVal)	\
    ( (This)->lpVtbl -> get_GroupID(This,pVal) ) 

#define IGQuestionnaire_put_GroupID(This,newVal)	\
    ( (This)->lpVtbl -> put_GroupID(This,newVal) ) 

#define IGQuestionnaire_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IGQuestionnaire_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IGQuestionnaire_get_ItemNum(This,pVal)	\
    ( (This)->lpVtbl -> get_ItemNum(This,pVal) ) 

#define IGQuestionnaire_put_ItemNum(This,newVal)	\
    ( (This)->lpVtbl -> put_ItemNum(This,newVal) ) 

#define IGQuestionnaire_FindForGroup(This,ExpID,GrpID)	\
    ( (This)->lpVtbl -> FindForGroup(This,ExpID,GrpID) ) 

#define IGQuestionnaire_Find(This,ExpID,GrpID,ID)	\
    ( (This)->lpVtbl -> Find(This,ExpID,GrpID,ID) ) 

#define IGQuestionnaire_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IGQuestionnaire_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IGQuestionnaire_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IGQuestionnaire_RemoveGroup(This,GrpID)	\
    ( (This)->lpVtbl -> RemoveGroup(This,GrpID) ) 

#define IGQuestionnaire_RemoveQuestion(This,ID)	\
    ( (This)->lpVtbl -> RemoveQuestion(This,ID) ) 

#define IGQuestionnaire_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IGQuestionnaire_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IGQuestionnaire_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IGQuestionnaire_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IGQuestionnaire_RemoveForGroup(This,ExpID,GrpID)	\
    ( (This)->lpVtbl -> RemoveForGroup(This,ExpID,GrpID) ) 

#define IGQuestionnaire_get_ExperimentID(This,pVal)	\
    ( (This)->lpVtbl -> get_ExperimentID(This,pVal) ) 

#define IGQuestionnaire_put_ExperimentID(This,newVal)	\
    ( (This)->lpVtbl -> put_ExperimentID(This,newVal) ) 

#define IGQuestionnaire_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IGQuestionnaire_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IGQuestionnaire_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IGQuestionnaire_INTERFACE_DEFINED__ */


#ifndef __ISQuestionnaire_INTERFACE_DEFINED__
#define __ISQuestionnaire_INTERFACE_DEFINED__

/* interface ISQuestionnaire */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ISQuestionnaire;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D62EF4A6-25BC-11D4-8B63-00104BC7E2C8")
    ISQuestionnaire : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubjectID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubjectID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Answer( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Answer( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForSubject( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID,
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveGroup( 
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveSubject( 
            /* [in] */ BSTR SubjID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ClearQuestionnaire( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveForSubject( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExperimentID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExperimentID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveQuestion( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForGroup( 
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISQuestionnaireVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISQuestionnaire * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISQuestionnaire * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISQuestionnaire * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISQuestionnaire * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISQuestionnaire * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISQuestionnaire * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISQuestionnaire * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupID )( 
            ISQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupID )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubjectID )( 
            ISQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubjectID )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            ISQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Answer )( 
            ISQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Answer )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForSubject )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            ISQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            ISQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            ISQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveGroup )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveSubject )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR SubjID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            ISQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            ISQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            ISQuestionnaire * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ClearQuestionnaire )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveForSubject )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID,
            /* [in] */ BSTR SubjID);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExperimentID )( 
            ISQuestionnaire * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExperimentID )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveQuestion )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForGroup )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ExpID,
            /* [in] */ BSTR GrpID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            ISQuestionnaire * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            ISQuestionnaire * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            ISQuestionnaire * This);
        
        END_INTERFACE
    } ISQuestionnaireVtbl;

    interface ISQuestionnaire
    {
        CONST_VTBL struct ISQuestionnaireVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISQuestionnaire_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ISQuestionnaire_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ISQuestionnaire_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ISQuestionnaire_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ISQuestionnaire_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ISQuestionnaire_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ISQuestionnaire_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ISQuestionnaire_get_GroupID(This,pVal)	\
    ( (This)->lpVtbl -> get_GroupID(This,pVal) ) 

#define ISQuestionnaire_put_GroupID(This,newVal)	\
    ( (This)->lpVtbl -> put_GroupID(This,newVal) ) 

#define ISQuestionnaire_get_SubjectID(This,pVal)	\
    ( (This)->lpVtbl -> get_SubjectID(This,pVal) ) 

#define ISQuestionnaire_put_SubjectID(This,newVal)	\
    ( (This)->lpVtbl -> put_SubjectID(This,newVal) ) 

#define ISQuestionnaire_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define ISQuestionnaire_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define ISQuestionnaire_get_Answer(This,pVal)	\
    ( (This)->lpVtbl -> get_Answer(This,pVal) ) 

#define ISQuestionnaire_put_Answer(This,newVal)	\
    ( (This)->lpVtbl -> put_Answer(This,newVal) ) 

#define ISQuestionnaire_FindForSubject(This,ExpID,GrpID,SubjID)	\
    ( (This)->lpVtbl -> FindForSubject(This,ExpID,GrpID,SubjID) ) 

#define ISQuestionnaire_Find(This,ExpID,GrpID,SubjID,ID)	\
    ( (This)->lpVtbl -> Find(This,ExpID,GrpID,SubjID,ID) ) 

#define ISQuestionnaire_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define ISQuestionnaire_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define ISQuestionnaire_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define ISQuestionnaire_RemoveGroup(This,GrpID)	\
    ( (This)->lpVtbl -> RemoveGroup(This,GrpID) ) 

#define ISQuestionnaire_RemoveSubject(This,SubjID)	\
    ( (This)->lpVtbl -> RemoveSubject(This,SubjID) ) 

#define ISQuestionnaire_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define ISQuestionnaire_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define ISQuestionnaire_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define ISQuestionnaire_ClearQuestionnaire(This,ExpID,GrpID,SubjID)	\
    ( (This)->lpVtbl -> ClearQuestionnaire(This,ExpID,GrpID,SubjID) ) 

#define ISQuestionnaire_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define ISQuestionnaire_RemoveForSubject(This,ExpID,GrpID,SubjID)	\
    ( (This)->lpVtbl -> RemoveForSubject(This,ExpID,GrpID,SubjID) ) 

#define ISQuestionnaire_get_ExperimentID(This,pVal)	\
    ( (This)->lpVtbl -> get_ExperimentID(This,pVal) ) 

#define ISQuestionnaire_put_ExperimentID(This,newVal)	\
    ( (This)->lpVtbl -> put_ExperimentID(This,newVal) ) 

#define ISQuestionnaire_RemoveQuestion(This,ID)	\
    ( (This)->lpVtbl -> RemoveQuestion(This,ID) ) 

#define ISQuestionnaire_FindForGroup(This,ExpID,GrpID)	\
    ( (This)->lpVtbl -> FindForGroup(This,ExpID,GrpID) ) 

#define ISQuestionnaire_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define ISQuestionnaire_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define ISQuestionnaire_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ISQuestionnaire_INTERFACE_DEFINED__ */


#ifndef __INSMUser_INTERFACE_DEFINED__
#define __INSMUser_INTERFACE_DEFINED__

/* interface INSMUser */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_INSMUser;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("17A683B5-F309-456D-8ADC-8A96EC8A4843")
    INSMUser : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UserID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UserID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RootPath( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RootPath( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BackupPath( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BackupPath( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Delimiter( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Delimiter( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_InputType( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_InputType( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletMode( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TabletMode( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EntireTablet( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EntireTablet( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Log( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Log( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Alpha( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Alpha( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletWidth( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TabletWidth( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletHeight( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TabletHeight( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplayWidth( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayWidth( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DisplayHeight( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayHeight( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LogUI( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LogUI( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LogDB( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LogDB( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LogProc( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LogProc( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LogGraph( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LogGraph( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_LogTablet( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LogTablet( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SysPass( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SysPass( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetWarningStatus( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AspectRatioWidth( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AspectRatioWidth( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AspectRatioHeight( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AspectRatioHeight( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetOperationMode( 
            /* [in] */ BOOL ClientServerON,
            /* [in] */ BSTR CSServer,
            /* [in] */ BSTR CSUser,
            /* [in] */ BSTR CSPassword) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CommPort( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CommPort( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AutoUpdate( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AutoUpdate( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Private( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Private( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WinUser( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WinUser( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Virgin( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GenerateSubjectIDs( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GenerateSubjectIDs( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubjectStartID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubjectStartID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubjectEndID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubjectEndID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubjectCurID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubjectCurID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Encrypted( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Encrypted( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_EncryptionMethod( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_EncryptionMethod( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SiteID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SiteID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SiteDesc( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SiteDesc( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Signature( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Signature( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INSMUserVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INSMUser * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INSMUser * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INSMUser * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INSMUser * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INSMUser * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INSMUser * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INSMUser * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            INSMUser * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserID )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserID )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RootPath )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RootPath )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BackupPath )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BackupPath )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Delimiter )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Delimiter )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_InputType )( 
            INSMUser * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_InputType )( 
            INSMUser * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletMode )( 
            INSMUser * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TabletMode )( 
            INSMUser * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EntireTablet )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EntireTablet )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Log )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Log )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Alpha )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Alpha )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletWidth )( 
            INSMUser * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TabletWidth )( 
            INSMUser * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletHeight )( 
            INSMUser * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TabletHeight )( 
            INSMUser * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayWidth )( 
            INSMUser * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayWidth )( 
            INSMUser * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DisplayHeight )( 
            INSMUser * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayHeight )( 
            INSMUser * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            INSMUser * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            INSMUser * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            INSMUser * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            INSMUser * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            INSMUser * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            INSMUser * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            INSMUser * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            INSMUser * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogUI )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogUI )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogDB )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogDB )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogProc )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogProc )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogGraph )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogGraph )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LogTablet )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LogTablet )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SysPass )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SysPass )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetWarningStatus )( 
            INSMUser * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AspectRatioWidth )( 
            INSMUser * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AspectRatioWidth )( 
            INSMUser * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AspectRatioHeight )( 
            INSMUser * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AspectRatioHeight )( 
            INSMUser * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetOperationMode )( 
            INSMUser * This,
            /* [in] */ BOOL ClientServerON,
            /* [in] */ BSTR CSServer,
            /* [in] */ BSTR CSUser,
            /* [in] */ BSTR CSPassword);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommPort )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CommPort )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AutoUpdate )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AutoUpdate )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Private )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Private )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WinUser )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WinUser )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Virgin )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GenerateSubjectIDs )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GenerateSubjectIDs )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubjectStartID )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubjectStartID )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubjectEndID )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubjectEndID )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubjectCurID )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubjectCurID )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Encrypted )( 
            INSMUser * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Encrypted )( 
            INSMUser * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_EncryptionMethod )( 
            INSMUser * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_EncryptionMethod )( 
            INSMUser * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SiteID )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SiteID )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SiteDesc )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SiteDesc )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Signature )( 
            INSMUser * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Signature )( 
            INSMUser * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } INSMUserVtbl;

    interface INSMUser
    {
        CONST_VTBL struct INSMUserVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INSMUser_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INSMUser_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INSMUser_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INSMUser_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INSMUser_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INSMUser_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INSMUser_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INSMUser_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define INSMUser_get_UserID(This,pVal)	\
    ( (This)->lpVtbl -> get_UserID(This,pVal) ) 

#define INSMUser_put_UserID(This,newVal)	\
    ( (This)->lpVtbl -> put_UserID(This,newVal) ) 

#define INSMUser_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define INSMUser_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define INSMUser_get_RootPath(This,pVal)	\
    ( (This)->lpVtbl -> get_RootPath(This,pVal) ) 

#define INSMUser_put_RootPath(This,newVal)	\
    ( (This)->lpVtbl -> put_RootPath(This,newVal) ) 

#define INSMUser_get_BackupPath(This,pVal)	\
    ( (This)->lpVtbl -> get_BackupPath(This,pVal) ) 

#define INSMUser_put_BackupPath(This,newVal)	\
    ( (This)->lpVtbl -> put_BackupPath(This,newVal) ) 

#define INSMUser_get_Delimiter(This,pVal)	\
    ( (This)->lpVtbl -> get_Delimiter(This,pVal) ) 

#define INSMUser_put_Delimiter(This,newVal)	\
    ( (This)->lpVtbl -> put_Delimiter(This,newVal) ) 

#define INSMUser_get_InputType(This,pVal)	\
    ( (This)->lpVtbl -> get_InputType(This,pVal) ) 

#define INSMUser_put_InputType(This,newVal)	\
    ( (This)->lpVtbl -> put_InputType(This,newVal) ) 

#define INSMUser_get_TabletMode(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletMode(This,pVal) ) 

#define INSMUser_put_TabletMode(This,newVal)	\
    ( (This)->lpVtbl -> put_TabletMode(This,newVal) ) 

#define INSMUser_get_EntireTablet(This,pVal)	\
    ( (This)->lpVtbl -> get_EntireTablet(This,pVal) ) 

#define INSMUser_put_EntireTablet(This,newVal)	\
    ( (This)->lpVtbl -> put_EntireTablet(This,newVal) ) 

#define INSMUser_get_Log(This,pVal)	\
    ( (This)->lpVtbl -> get_Log(This,pVal) ) 

#define INSMUser_put_Log(This,newVal)	\
    ( (This)->lpVtbl -> put_Log(This,newVal) ) 

#define INSMUser_get_Alpha(This,pVal)	\
    ( (This)->lpVtbl -> get_Alpha(This,pVal) ) 

#define INSMUser_put_Alpha(This,newVal)	\
    ( (This)->lpVtbl -> put_Alpha(This,newVal) ) 

#define INSMUser_get_TabletWidth(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletWidth(This,pVal) ) 

#define INSMUser_put_TabletWidth(This,newVal)	\
    ( (This)->lpVtbl -> put_TabletWidth(This,newVal) ) 

#define INSMUser_get_TabletHeight(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletHeight(This,pVal) ) 

#define INSMUser_put_TabletHeight(This,newVal)	\
    ( (This)->lpVtbl -> put_TabletHeight(This,newVal) ) 

#define INSMUser_get_DisplayWidth(This,pVal)	\
    ( (This)->lpVtbl -> get_DisplayWidth(This,pVal) ) 

#define INSMUser_put_DisplayWidth(This,newVal)	\
    ( (This)->lpVtbl -> put_DisplayWidth(This,newVal) ) 

#define INSMUser_get_DisplayHeight(This,pVal)	\
    ( (This)->lpVtbl -> get_DisplayHeight(This,pVal) ) 

#define INSMUser_put_DisplayHeight(This,newVal)	\
    ( (This)->lpVtbl -> put_DisplayHeight(This,newVal) ) 

#define INSMUser_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define INSMUser_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define INSMUser_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define INSMUser_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define INSMUser_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define INSMUser_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define INSMUser_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define INSMUser_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define INSMUser_get_LogUI(This,pVal)	\
    ( (This)->lpVtbl -> get_LogUI(This,pVal) ) 

#define INSMUser_put_LogUI(This,newVal)	\
    ( (This)->lpVtbl -> put_LogUI(This,newVal) ) 

#define INSMUser_get_LogDB(This,pVal)	\
    ( (This)->lpVtbl -> get_LogDB(This,pVal) ) 

#define INSMUser_put_LogDB(This,newVal)	\
    ( (This)->lpVtbl -> put_LogDB(This,newVal) ) 

#define INSMUser_get_LogProc(This,pVal)	\
    ( (This)->lpVtbl -> get_LogProc(This,pVal) ) 

#define INSMUser_put_LogProc(This,newVal)	\
    ( (This)->lpVtbl -> put_LogProc(This,newVal) ) 

#define INSMUser_get_LogGraph(This,pVal)	\
    ( (This)->lpVtbl -> get_LogGraph(This,pVal) ) 

#define INSMUser_put_LogGraph(This,newVal)	\
    ( (This)->lpVtbl -> put_LogGraph(This,newVal) ) 

#define INSMUser_get_LogTablet(This,pVal)	\
    ( (This)->lpVtbl -> get_LogTablet(This,pVal) ) 

#define INSMUser_put_LogTablet(This,newVal)	\
    ( (This)->lpVtbl -> put_LogTablet(This,newVal) ) 

#define INSMUser_get_SysPass(This,pVal)	\
    ( (This)->lpVtbl -> get_SysPass(This,pVal) ) 

#define INSMUser_put_SysPass(This,newVal)	\
    ( (This)->lpVtbl -> put_SysPass(This,newVal) ) 

#define INSMUser_ResetWarningStatus(This)	\
    ( (This)->lpVtbl -> ResetWarningStatus(This) ) 

#define INSMUser_get_AspectRatioWidth(This,pVal)	\
    ( (This)->lpVtbl -> get_AspectRatioWidth(This,pVal) ) 

#define INSMUser_put_AspectRatioWidth(This,newVal)	\
    ( (This)->lpVtbl -> put_AspectRatioWidth(This,newVal) ) 

#define INSMUser_get_AspectRatioHeight(This,pVal)	\
    ( (This)->lpVtbl -> get_AspectRatioHeight(This,pVal) ) 

#define INSMUser_put_AspectRatioHeight(This,newVal)	\
    ( (This)->lpVtbl -> put_AspectRatioHeight(This,newVal) ) 

#define INSMUser_SetOperationMode(This,ClientServerON,CSServer,CSUser,CSPassword)	\
    ( (This)->lpVtbl -> SetOperationMode(This,ClientServerON,CSServer,CSUser,CSPassword) ) 

#define INSMUser_get_CommPort(This,pVal)	\
    ( (This)->lpVtbl -> get_CommPort(This,pVal) ) 

#define INSMUser_put_CommPort(This,newVal)	\
    ( (This)->lpVtbl -> put_CommPort(This,newVal) ) 

#define INSMUser_get_AutoUpdate(This,pVal)	\
    ( (This)->lpVtbl -> get_AutoUpdate(This,pVal) ) 

#define INSMUser_put_AutoUpdate(This,newVal)	\
    ( (This)->lpVtbl -> put_AutoUpdate(This,newVal) ) 

#define INSMUser_get_Private(This,pVal)	\
    ( (This)->lpVtbl -> get_Private(This,pVal) ) 

#define INSMUser_put_Private(This,newVal)	\
    ( (This)->lpVtbl -> put_Private(This,newVal) ) 

#define INSMUser_get_WinUser(This,pVal)	\
    ( (This)->lpVtbl -> get_WinUser(This,pVal) ) 

#define INSMUser_put_WinUser(This,newVal)	\
    ( (This)->lpVtbl -> put_WinUser(This,newVal) ) 

#define INSMUser_get_Virgin(This,pVal)	\
    ( (This)->lpVtbl -> get_Virgin(This,pVal) ) 

#define INSMUser_get_GenerateSubjectIDs(This,pVal)	\
    ( (This)->lpVtbl -> get_GenerateSubjectIDs(This,pVal) ) 

#define INSMUser_put_GenerateSubjectIDs(This,newVal)	\
    ( (This)->lpVtbl -> put_GenerateSubjectIDs(This,newVal) ) 

#define INSMUser_get_SubjectStartID(This,pVal)	\
    ( (This)->lpVtbl -> get_SubjectStartID(This,pVal) ) 

#define INSMUser_put_SubjectStartID(This,newVal)	\
    ( (This)->lpVtbl -> put_SubjectStartID(This,newVal) ) 

#define INSMUser_get_SubjectEndID(This,pVal)	\
    ( (This)->lpVtbl -> get_SubjectEndID(This,pVal) ) 

#define INSMUser_put_SubjectEndID(This,newVal)	\
    ( (This)->lpVtbl -> put_SubjectEndID(This,newVal) ) 

#define INSMUser_get_SubjectCurID(This,pVal)	\
    ( (This)->lpVtbl -> get_SubjectCurID(This,pVal) ) 

#define INSMUser_put_SubjectCurID(This,newVal)	\
    ( (This)->lpVtbl -> put_SubjectCurID(This,newVal) ) 

#define INSMUser_get_Encrypted(This,pVal)	\
    ( (This)->lpVtbl -> get_Encrypted(This,pVal) ) 

#define INSMUser_put_Encrypted(This,newVal)	\
    ( (This)->lpVtbl -> put_Encrypted(This,newVal) ) 

#define INSMUser_get_EncryptionMethod(This,pVal)	\
    ( (This)->lpVtbl -> get_EncryptionMethod(This,pVal) ) 

#define INSMUser_put_EncryptionMethod(This,newVal)	\
    ( (This)->lpVtbl -> put_EncryptionMethod(This,newVal) ) 

#define INSMUser_get_SiteID(This,pVal)	\
    ( (This)->lpVtbl -> get_SiteID(This,pVal) ) 

#define INSMUser_put_SiteID(This,newVal)	\
    ( (This)->lpVtbl -> put_SiteID(This,newVal) ) 

#define INSMUser_get_SiteDesc(This,pVal)	\
    ( (This)->lpVtbl -> get_SiteDesc(This,pVal) ) 

#define INSMUser_put_SiteDesc(This,newVal)	\
    ( (This)->lpVtbl -> put_SiteDesc(This,newVal) ) 

#define INSMUser_get_Signature(This,pVal)	\
    ( (This)->lpVtbl -> get_Signature(This,pVal) ) 

#define INSMUser_put_Signature(This,newVal)	\
    ( (This)->lpVtbl -> put_Signature(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INSMUser_INTERFACE_DEFINED__ */


#ifndef __IBackup_INTERFACE_DEFINED__
#define __IBackup_INTERFACE_DEFINED__

/* interface IBackup */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eBackupType
    {	eB_None	= 0,
	eB_All	= 1,
	eB_Experiment	= 2,
	eB_Group	= 3,
	eB_Subject	= 4,
	eB_DB	= 5
    } 	BackupType;


EXTERN_C const IID IID_IBackup;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C8C0CF86-1C9D-4D0A-8AF8-AC1B03B56BFA")
    IBackup : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UserID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UserID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BackupDate( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BackupDate( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Path( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Path( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_BackupType( 
            /* [retval][out] */ BackupType *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BackupType( 
            /* [in] */ BackupType newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Exp( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Exp( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Grp( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Grp( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Subj( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Subj( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Notes( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Notes( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR UserID,
            /* [in] */ BSTR BackupDate) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IBackupVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IBackup * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IBackup * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IBackup * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IBackup * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IBackup * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IBackup * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IBackup * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserID )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserID )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BackupDate )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BackupDate )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Path )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Path )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_BackupType )( 
            IBackup * This,
            /* [retval][out] */ BackupType *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BackupType )( 
            IBackup * This,
            /* [in] */ BackupType newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Exp )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Exp )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Grp )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Grp )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Subj )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Subj )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Notes )( 
            IBackup * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Notes )( 
            IBackup * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IBackup * This,
            /* [in] */ BSTR UserID,
            /* [in] */ BSTR BackupDate);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IBackup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IBackup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IBackup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IBackup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IBackup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IBackup * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IBackup * This,
            /* [in] */ BSTR Path);
        
        END_INTERFACE
    } IBackupVtbl;

    interface IBackup
    {
        CONST_VTBL struct IBackupVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IBackup_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IBackup_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IBackup_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IBackup_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IBackup_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IBackup_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IBackup_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IBackup_get_UserID(This,pVal)	\
    ( (This)->lpVtbl -> get_UserID(This,pVal) ) 

#define IBackup_put_UserID(This,newVal)	\
    ( (This)->lpVtbl -> put_UserID(This,newVal) ) 

#define IBackup_get_BackupDate(This,pVal)	\
    ( (This)->lpVtbl -> get_BackupDate(This,pVal) ) 

#define IBackup_put_BackupDate(This,newVal)	\
    ( (This)->lpVtbl -> put_BackupDate(This,newVal) ) 

#define IBackup_get_Path(This,pVal)	\
    ( (This)->lpVtbl -> get_Path(This,pVal) ) 

#define IBackup_put_Path(This,newVal)	\
    ( (This)->lpVtbl -> put_Path(This,newVal) ) 

#define IBackup_get_BackupType(This,pVal)	\
    ( (This)->lpVtbl -> get_BackupType(This,pVal) ) 

#define IBackup_put_BackupType(This,newVal)	\
    ( (This)->lpVtbl -> put_BackupType(This,newVal) ) 

#define IBackup_get_Exp(This,pVal)	\
    ( (This)->lpVtbl -> get_Exp(This,pVal) ) 

#define IBackup_put_Exp(This,newVal)	\
    ( (This)->lpVtbl -> put_Exp(This,newVal) ) 

#define IBackup_get_Grp(This,pVal)	\
    ( (This)->lpVtbl -> get_Grp(This,pVal) ) 

#define IBackup_put_Grp(This,newVal)	\
    ( (This)->lpVtbl -> put_Grp(This,newVal) ) 

#define IBackup_get_Subj(This,pVal)	\
    ( (This)->lpVtbl -> get_Subj(This,pVal) ) 

#define IBackup_put_Subj(This,newVal)	\
    ( (This)->lpVtbl -> put_Subj(This,newVal) ) 

#define IBackup_get_Notes(This,pVal)	\
    ( (This)->lpVtbl -> get_Notes(This,pVal) ) 

#define IBackup_put_Notes(This,newVal)	\
    ( (This)->lpVtbl -> put_Notes(This,newVal) ) 

#define IBackup_Find(This,UserID,BackupDate)	\
    ( (This)->lpVtbl -> Find(This,UserID,BackupDate) ) 

#define IBackup_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IBackup_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IBackup_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IBackup_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IBackup_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IBackup_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IBackup_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IBackup_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_DataMod_0000_0016 */
/* [local] */ 




extern RPC_IF_HANDLE __MIDL_itf_DataMod_0000_0016_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_DataMod_0000_0016_v0_0_s_ifspec;

#ifndef __IElement_INTERFACE_DEFINED__
#define __IElement_INTERFACE_DEFINED__

/* interface IElement */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IElement;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("70B12C87-0CCD-463D-A855-5445D5AD4B74")
    IElement : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Pattern( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Pattern( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Shape( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Shape( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CenterX( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CenterX( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CenterY( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CenterY( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WidthX( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WidthX( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WidthY( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WidthY( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Line1Size( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Line1Size( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Line1Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Line1Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text1( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text1( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text1Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text1Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text1Size( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text1Size( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Background1Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Background1Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IsTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Line2Size( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Line2Size( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Line2Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Line2Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text2( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text2( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text2Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text2Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text2Size( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text2Size( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Background2Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Background2Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Line3Size( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Line3Size( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Line3Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Line3Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text3( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text3( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text3Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text3Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Text3Size( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Text3Size( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Background3Color( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Background3Color( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorX( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ErrorX( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ErrorY( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ErrorY( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Font1( 
            /* [retval][out] */ VARIANT **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Font1( 
            /* [in] */ VARIANT *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Font2( 
            /* [retval][out] */ VARIANT **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Font2( 
            /* [in] */ VARIANT *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Font3( 
            /* [retval][out] */ VARIANT **pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Font3( 
            /* [in] */ VARIANT *newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_IsImage( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IsImage( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Start( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Start( 
            /* [in] */ DOUBLE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Duration( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Duration( 
            /* [in] */ DOUBLE newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideRightTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideRightTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_CatID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CatID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Animate( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Animate( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AnimationFile( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AnimationFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AnimationRandom( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AnimationRandom( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AnimationGenerate( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AnimationGenerate( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AnimationSubjID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AnimationSubjID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideWrongTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideWrongTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideIfAnyRightTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideIfAnyRightTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_HideIfAnyWrongTarget( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HideIfAnyWrongTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_RightTarget( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RightTarget( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_WrongTarget( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_WrongTarget( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_AnimationRate( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_AnimationRate( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IElementVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IElement * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IElement * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IElement * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IElement * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IElement * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IElement * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IElement * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            IElement * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IElement * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IElement * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Pattern )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Pattern )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Shape )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Shape )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CenterX )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CenterX )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CenterY )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CenterY )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WidthX )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WidthX )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WidthY )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WidthY )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Line1Size )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Line1Size )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Line1Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Line1Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text1 )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text1 )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text1Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text1Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text1Size )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text1Size )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Background1Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Background1Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsTarget )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IsTarget )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Line2Size )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Line2Size )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Line2Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Line2Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text2 )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text2 )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text2Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text2Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text2Size )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text2Size )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Background2Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Background2Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Line3Size )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Line3Size )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Line3Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Line3Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text3 )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text3 )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text3Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text3Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Text3Size )( 
            IElement * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Text3Size )( 
            IElement * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Background3Color )( 
            IElement * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Background3Color )( 
            IElement * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorX )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ErrorX )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ErrorY )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ErrorY )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Font1 )( 
            IElement * This,
            /* [retval][out] */ VARIANT **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Font1 )( 
            IElement * This,
            /* [in] */ VARIANT *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Font2 )( 
            IElement * This,
            /* [retval][out] */ VARIANT **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Font2 )( 
            IElement * This,
            /* [in] */ VARIANT *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Font3 )( 
            IElement * This,
            /* [retval][out] */ VARIANT **pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Font3 )( 
            IElement * This,
            /* [in] */ VARIANT *newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsImage )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IsImage )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Start )( 
            IElement * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Start )( 
            IElement * This,
            /* [in] */ DOUBLE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Duration )( 
            IElement * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Duration )( 
            IElement * This,
            /* [in] */ DOUBLE newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideRightTarget )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideRightTarget )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CatID )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CatID )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Animate )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Animate )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationFile )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationFile )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationRandom )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationRandom )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationGenerate )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationGenerate )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationSubjID )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationSubjID )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideWrongTarget )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideWrongTarget )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideIfAnyRightTarget )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideIfAnyRightTarget )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HideIfAnyWrongTarget )( 
            IElement * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HideIfAnyWrongTarget )( 
            IElement * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RightTarget )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RightTarget )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WrongTarget )( 
            IElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_WrongTarget )( 
            IElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AnimationRate )( 
            IElement * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_AnimationRate )( 
            IElement * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IElement * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IElement * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IElement * This);
        
        END_INTERFACE
    } IElementVtbl;

    interface IElement
    {
        CONST_VTBL struct IElementVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IElement_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IElement_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IElement_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IElement_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IElement_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IElement_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IElement_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IElement_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define IElement_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IElement_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IElement_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define IElement_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define IElement_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IElement_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IElement_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IElement_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IElement_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IElement_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IElement_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IElement_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IElement_get_Pattern(This,pVal)	\
    ( (This)->lpVtbl -> get_Pattern(This,pVal) ) 

#define IElement_put_Pattern(This,newVal)	\
    ( (This)->lpVtbl -> put_Pattern(This,newVal) ) 

#define IElement_get_Shape(This,pVal)	\
    ( (This)->lpVtbl -> get_Shape(This,pVal) ) 

#define IElement_put_Shape(This,newVal)	\
    ( (This)->lpVtbl -> put_Shape(This,newVal) ) 

#define IElement_get_CenterX(This,pVal)	\
    ( (This)->lpVtbl -> get_CenterX(This,pVal) ) 

#define IElement_put_CenterX(This,newVal)	\
    ( (This)->lpVtbl -> put_CenterX(This,newVal) ) 

#define IElement_get_CenterY(This,pVal)	\
    ( (This)->lpVtbl -> get_CenterY(This,pVal) ) 

#define IElement_put_CenterY(This,newVal)	\
    ( (This)->lpVtbl -> put_CenterY(This,newVal) ) 

#define IElement_get_WidthX(This,pVal)	\
    ( (This)->lpVtbl -> get_WidthX(This,pVal) ) 

#define IElement_put_WidthX(This,newVal)	\
    ( (This)->lpVtbl -> put_WidthX(This,newVal) ) 

#define IElement_get_WidthY(This,pVal)	\
    ( (This)->lpVtbl -> get_WidthY(This,pVal) ) 

#define IElement_put_WidthY(This,newVal)	\
    ( (This)->lpVtbl -> put_WidthY(This,newVal) ) 

#define IElement_get_Line1Size(This,pVal)	\
    ( (This)->lpVtbl -> get_Line1Size(This,pVal) ) 

#define IElement_put_Line1Size(This,newVal)	\
    ( (This)->lpVtbl -> put_Line1Size(This,newVal) ) 

#define IElement_get_Line1Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Line1Color(This,pVal) ) 

#define IElement_put_Line1Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Line1Color(This,newVal) ) 

#define IElement_get_Text1(This,pVal)	\
    ( (This)->lpVtbl -> get_Text1(This,pVal) ) 

#define IElement_put_Text1(This,newVal)	\
    ( (This)->lpVtbl -> put_Text1(This,newVal) ) 

#define IElement_get_Text1Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Text1Color(This,pVal) ) 

#define IElement_put_Text1Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Text1Color(This,newVal) ) 

#define IElement_get_Text1Size(This,pVal)	\
    ( (This)->lpVtbl -> get_Text1Size(This,pVal) ) 

#define IElement_put_Text1Size(This,newVal)	\
    ( (This)->lpVtbl -> put_Text1Size(This,newVal) ) 

#define IElement_get_Background1Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Background1Color(This,pVal) ) 

#define IElement_put_Background1Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Background1Color(This,newVal) ) 

#define IElement_get_IsTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_IsTarget(This,pVal) ) 

#define IElement_put_IsTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_IsTarget(This,newVal) ) 

#define IElement_get_Line2Size(This,pVal)	\
    ( (This)->lpVtbl -> get_Line2Size(This,pVal) ) 

#define IElement_put_Line2Size(This,newVal)	\
    ( (This)->lpVtbl -> put_Line2Size(This,newVal) ) 

#define IElement_get_Line2Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Line2Color(This,pVal) ) 

#define IElement_put_Line2Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Line2Color(This,newVal) ) 

#define IElement_get_Text2(This,pVal)	\
    ( (This)->lpVtbl -> get_Text2(This,pVal) ) 

#define IElement_put_Text2(This,newVal)	\
    ( (This)->lpVtbl -> put_Text2(This,newVal) ) 

#define IElement_get_Text2Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Text2Color(This,pVal) ) 

#define IElement_put_Text2Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Text2Color(This,newVal) ) 

#define IElement_get_Text2Size(This,pVal)	\
    ( (This)->lpVtbl -> get_Text2Size(This,pVal) ) 

#define IElement_put_Text2Size(This,newVal)	\
    ( (This)->lpVtbl -> put_Text2Size(This,newVal) ) 

#define IElement_get_Background2Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Background2Color(This,pVal) ) 

#define IElement_put_Background2Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Background2Color(This,newVal) ) 

#define IElement_get_Line3Size(This,pVal)	\
    ( (This)->lpVtbl -> get_Line3Size(This,pVal) ) 

#define IElement_put_Line3Size(This,newVal)	\
    ( (This)->lpVtbl -> put_Line3Size(This,newVal) ) 

#define IElement_get_Line3Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Line3Color(This,pVal) ) 

#define IElement_put_Line3Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Line3Color(This,newVal) ) 

#define IElement_get_Text3(This,pVal)	\
    ( (This)->lpVtbl -> get_Text3(This,pVal) ) 

#define IElement_put_Text3(This,newVal)	\
    ( (This)->lpVtbl -> put_Text3(This,newVal) ) 

#define IElement_get_Text3Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Text3Color(This,pVal) ) 

#define IElement_put_Text3Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Text3Color(This,newVal) ) 

#define IElement_get_Text3Size(This,pVal)	\
    ( (This)->lpVtbl -> get_Text3Size(This,pVal) ) 

#define IElement_put_Text3Size(This,newVal)	\
    ( (This)->lpVtbl -> put_Text3Size(This,newVal) ) 

#define IElement_get_Background3Color(This,pVal)	\
    ( (This)->lpVtbl -> get_Background3Color(This,pVal) ) 

#define IElement_put_Background3Color(This,newVal)	\
    ( (This)->lpVtbl -> put_Background3Color(This,newVal) ) 

#define IElement_get_ErrorX(This,pVal)	\
    ( (This)->lpVtbl -> get_ErrorX(This,pVal) ) 

#define IElement_put_ErrorX(This,newVal)	\
    ( (This)->lpVtbl -> put_ErrorX(This,newVal) ) 

#define IElement_get_ErrorY(This,pVal)	\
    ( (This)->lpVtbl -> get_ErrorY(This,pVal) ) 

#define IElement_put_ErrorY(This,newVal)	\
    ( (This)->lpVtbl -> put_ErrorY(This,newVal) ) 

#define IElement_get_Font1(This,pVal)	\
    ( (This)->lpVtbl -> get_Font1(This,pVal) ) 

#define IElement_put_Font1(This,newVal)	\
    ( (This)->lpVtbl -> put_Font1(This,newVal) ) 

#define IElement_get_Font2(This,pVal)	\
    ( (This)->lpVtbl -> get_Font2(This,pVal) ) 

#define IElement_put_Font2(This,newVal)	\
    ( (This)->lpVtbl -> put_Font2(This,newVal) ) 

#define IElement_get_Font3(This,pVal)	\
    ( (This)->lpVtbl -> get_Font3(This,pVal) ) 

#define IElement_put_Font3(This,newVal)	\
    ( (This)->lpVtbl -> put_Font3(This,newVal) ) 

#define IElement_get_IsImage(This,pVal)	\
    ( (This)->lpVtbl -> get_IsImage(This,pVal) ) 

#define IElement_put_IsImage(This,newVal)	\
    ( (This)->lpVtbl -> put_IsImage(This,newVal) ) 

#define IElement_get_Start(This,pVal)	\
    ( (This)->lpVtbl -> get_Start(This,pVal) ) 

#define IElement_put_Start(This,newVal)	\
    ( (This)->lpVtbl -> put_Start(This,newVal) ) 

#define IElement_get_Duration(This,pVal)	\
    ( (This)->lpVtbl -> get_Duration(This,pVal) ) 

#define IElement_put_Duration(This,newVal)	\
    ( (This)->lpVtbl -> put_Duration(This,newVal) ) 

#define IElement_get_HideRightTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_HideRightTarget(This,pVal) ) 

#define IElement_put_HideRightTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_HideRightTarget(This,newVal) ) 

#define IElement_get_CatID(This,pVal)	\
    ( (This)->lpVtbl -> get_CatID(This,pVal) ) 

#define IElement_put_CatID(This,newVal)	\
    ( (This)->lpVtbl -> put_CatID(This,newVal) ) 

#define IElement_get_Animate(This,pVal)	\
    ( (This)->lpVtbl -> get_Animate(This,pVal) ) 

#define IElement_put_Animate(This,newVal)	\
    ( (This)->lpVtbl -> put_Animate(This,newVal) ) 

#define IElement_get_AnimationFile(This,pVal)	\
    ( (This)->lpVtbl -> get_AnimationFile(This,pVal) ) 

#define IElement_put_AnimationFile(This,newVal)	\
    ( (This)->lpVtbl -> put_AnimationFile(This,newVal) ) 

#define IElement_get_AnimationRandom(This,pVal)	\
    ( (This)->lpVtbl -> get_AnimationRandom(This,pVal) ) 

#define IElement_put_AnimationRandom(This,newVal)	\
    ( (This)->lpVtbl -> put_AnimationRandom(This,newVal) ) 

#define IElement_get_AnimationGenerate(This,pVal)	\
    ( (This)->lpVtbl -> get_AnimationGenerate(This,pVal) ) 

#define IElement_put_AnimationGenerate(This,newVal)	\
    ( (This)->lpVtbl -> put_AnimationGenerate(This,newVal) ) 

#define IElement_get_AnimationSubjID(This,pVal)	\
    ( (This)->lpVtbl -> get_AnimationSubjID(This,pVal) ) 

#define IElement_put_AnimationSubjID(This,newVal)	\
    ( (This)->lpVtbl -> put_AnimationSubjID(This,newVal) ) 

#define IElement_get_HideWrongTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_HideWrongTarget(This,pVal) ) 

#define IElement_put_HideWrongTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_HideWrongTarget(This,newVal) ) 

#define IElement_get_HideIfAnyRightTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_HideIfAnyRightTarget(This,pVal) ) 

#define IElement_put_HideIfAnyRightTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_HideIfAnyRightTarget(This,newVal) ) 

#define IElement_get_HideIfAnyWrongTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_HideIfAnyWrongTarget(This,pVal) ) 

#define IElement_put_HideIfAnyWrongTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_HideIfAnyWrongTarget(This,newVal) ) 

#define IElement_get_RightTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_RightTarget(This,pVal) ) 

#define IElement_put_RightTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_RightTarget(This,newVal) ) 

#define IElement_get_WrongTarget(This,pVal)	\
    ( (This)->lpVtbl -> get_WrongTarget(This,pVal) ) 

#define IElement_put_WrongTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_WrongTarget(This,newVal) ) 

#define IElement_get_AnimationRate(This,pVal)	\
    ( (This)->lpVtbl -> get_AnimationRate(This,pVal) ) 

#define IElement_put_AnimationRate(This,newVal)	\
    ( (This)->lpVtbl -> put_AnimationRate(This,newVal) ) 

#define IElement_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IElement_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IElement_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IElement_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IElement_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IElement_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IElement_INTERFACE_DEFINED__ */


#ifndef __IStimulus_INTERFACE_DEFINED__
#define __IStimulus_INTERFACE_DEFINED__

/* interface IStimulus */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStimulus;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4554F8A4-3333-4EE9-9075-EFF024080D61")
    IStimulus : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Demo( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Demo( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStimulusVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStimulus * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStimulus * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStimulus * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStimulus * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStimulus * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStimulus * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStimulus * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            IStimulus * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IStimulus * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IStimulus * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IStimulus * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            IStimulus * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IStimulus * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IStimulus * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Demo )( 
            IStimulus * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Demo )( 
            IStimulus * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IStimulus * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IStimulus * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IStimulus * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IStimulus * This);
        
        END_INTERFACE
    } IStimulusVtbl;

    interface IStimulus
    {
        CONST_VTBL struct IStimulusVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStimulus_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IStimulus_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IStimulus_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IStimulus_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IStimulus_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IStimulus_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IStimulus_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IStimulus_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define IStimulus_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IStimulus_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IStimulus_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define IStimulus_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define IStimulus_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IStimulus_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IStimulus_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IStimulus_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IStimulus_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IStimulus_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IStimulus_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IStimulus_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IStimulus_get_Demo(This,pVal)	\
    ( (This)->lpVtbl -> get_Demo(This,pVal) ) 

#define IStimulus_put_Demo(This,newVal)	\
    ( (This)->lpVtbl -> put_Demo(This,newVal) ) 

#define IStimulus_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IStimulus_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IStimulus_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IStimulus_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IStimulus_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IStimulus_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IStimulus_INTERFACE_DEFINED__ */


#ifndef __IStimulusElement_INTERFACE_DEFINED__
#define __IStimulusElement_INTERFACE_DEFINED__

/* interface IStimulusElement */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStimulusElement;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("387F41B2-4C02-4D85-889C-1DA7FACD5F5E")
    IStimulusElement : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StimulusID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StimulusID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR StimID,
            /* [in] */ BSTR ElmtID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForStimulus( 
            /* [in] */ BSTR StimID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStimulusElementVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStimulusElement * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStimulusElement * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStimulusElement * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStimulusElement * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStimulusElement * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStimulusElement * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStimulusElement * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StimulusID )( 
            IStimulusElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StimulusID )( 
            IStimulusElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ElementID )( 
            IStimulusElement * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ElementID )( 
            IStimulusElement * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IStimulusElement * This,
            /* [in] */ BSTR StimID,
            /* [in] */ BSTR ElmtID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForStimulus )( 
            IStimulusElement * This,
            /* [in] */ BSTR StimID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IStimulusElement * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IStimulusElement * This,
            /* [in] */ BSTR Path);
        
        END_INTERFACE
    } IStimulusElementVtbl;

    interface IStimulusElement
    {
        CONST_VTBL struct IStimulusElementVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStimulusElement_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IStimulusElement_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IStimulusElement_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IStimulusElement_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IStimulusElement_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IStimulusElement_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IStimulusElement_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IStimulusElement_get_StimulusID(This,pVal)	\
    ( (This)->lpVtbl -> get_StimulusID(This,pVal) ) 

#define IStimulusElement_put_StimulusID(This,newVal)	\
    ( (This)->lpVtbl -> put_StimulusID(This,newVal) ) 

#define IStimulusElement_get_ElementID(This,pVal)	\
    ( (This)->lpVtbl -> get_ElementID(This,pVal) ) 

#define IStimulusElement_put_ElementID(This,newVal)	\
    ( (This)->lpVtbl -> put_ElementID(This,newVal) ) 

#define IStimulusElement_Find(This,StimID,ElmtID)	\
    ( (This)->lpVtbl -> Find(This,StimID,ElmtID) ) 

#define IStimulusElement_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IStimulusElement_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IStimulusElement_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IStimulusElement_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IStimulusElement_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IStimulusElement_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IStimulusElement_FindForStimulus(This,StimID)	\
    ( (This)->lpVtbl -> FindForStimulus(This,StimID) ) 

#define IStimulusElement_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IStimulusElement_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IStimulusElement_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IStimulusElement_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IStimulusElement_INTERFACE_DEFINED__ */


#ifndef __IStimulusTarget_INTERFACE_DEFINED__
#define __IStimulusTarget_INTERFACE_DEFINED__

/* interface IStimulusTarget */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStimulusTarget;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6F5FE8F9-40EC-4714-A9EC-DB2668FF6B63")
    IStimulusTarget : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StimulusID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StimulusID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ElementID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ElementID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR StimID,
            /* [in] */ BSTR ElmtID,
            /* [in] */ short Seq) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForStimulus( 
            /* [in] */ BSTR StimID) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Sequence( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Sequence( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReleaseData( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FindForStimElmt( 
            /* [in] */ BSTR StimID,
            /* [in] */ BSTR ElmtID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStimulusTargetVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStimulusTarget * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStimulusTarget * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStimulusTarget * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStimulusTarget * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStimulusTarget * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStimulusTarget * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStimulusTarget * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StimulusID )( 
            IStimulusTarget * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StimulusID )( 
            IStimulusTarget * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ElementID )( 
            IStimulusTarget * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ElementID )( 
            IStimulusTarget * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IStimulusTarget * This,
            /* [in] */ BSTR StimID,
            /* [in] */ BSTR ElmtID,
            /* [in] */ short Seq);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForStimulus )( 
            IStimulusTarget * This,
            /* [in] */ BSTR StimID);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Sequence )( 
            IStimulusTarget * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Sequence )( 
            IStimulusTarget * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ReleaseData )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FindForStimElmt )( 
            IStimulusTarget * This,
            /* [in] */ BSTR StimID,
            /* [in] */ BSTR ElmtID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IStimulusTarget * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IStimulusTarget * This,
            /* [in] */ BSTR Path);
        
        END_INTERFACE
    } IStimulusTargetVtbl;

    interface IStimulusTarget
    {
        CONST_VTBL struct IStimulusTargetVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStimulusTarget_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IStimulusTarget_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IStimulusTarget_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IStimulusTarget_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IStimulusTarget_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IStimulusTarget_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IStimulusTarget_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IStimulusTarget_get_StimulusID(This,pVal)	\
    ( (This)->lpVtbl -> get_StimulusID(This,pVal) ) 

#define IStimulusTarget_put_StimulusID(This,newVal)	\
    ( (This)->lpVtbl -> put_StimulusID(This,newVal) ) 

#define IStimulusTarget_get_ElementID(This,pVal)	\
    ( (This)->lpVtbl -> get_ElementID(This,pVal) ) 

#define IStimulusTarget_put_ElementID(This,newVal)	\
    ( (This)->lpVtbl -> put_ElementID(This,newVal) ) 

#define IStimulusTarget_Find(This,StimID,ElmtID,Seq)	\
    ( (This)->lpVtbl -> Find(This,StimID,ElmtID,Seq) ) 

#define IStimulusTarget_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IStimulusTarget_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IStimulusTarget_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IStimulusTarget_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IStimulusTarget_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IStimulusTarget_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IStimulusTarget_FindForStimulus(This,StimID)	\
    ( (This)->lpVtbl -> FindForStimulus(This,StimID) ) 

#define IStimulusTarget_get_Sequence(This,pVal)	\
    ( (This)->lpVtbl -> get_Sequence(This,pVal) ) 

#define IStimulusTarget_put_Sequence(This,newVal)	\
    ( (This)->lpVtbl -> put_Sequence(This,newVal) ) 

#define IStimulusTarget_ReleaseData(This)	\
    ( (This)->lpVtbl -> ReleaseData(This) ) 

#define IStimulusTarget_FindForStimElmt(This,StimID,ElmtID)	\
    ( (This)->lpVtbl -> FindForStimElmt(This,StimID,ElmtID) ) 

#define IStimulusTarget_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IStimulusTarget_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IStimulusTarget_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IStimulusTarget_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IStimulusTarget_INTERFACE_DEFINED__ */


#ifndef __ICat_INTERFACE_DEFINED__
#define __ICat_INTERFACE_DEFINED__

/* interface ICat */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eCatType
    {	eCT_None	= 0,
	eCT_Experiment	= 1,
	eCT_Group	= 2,
	eCT_Subject	= 3,
	eCT_Condition	= 4,
	eCT_Stimulus	= 5,
	eCT_Element	= 6
    } 	CatType;


EXTERN_C const IID IID_ICat;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B4A54E2A-1BBA-4EBB-B308-305B15277E4E")
    ICat : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Type( 
            /* [retval][out] */ CatType *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Type( 
            /* [in] */ CatType newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICatVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICat * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICat * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICat * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICat * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICat * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICat * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICat * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            ICat * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            ICat * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            ICat * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            ICat * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            ICat * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Type )( 
            ICat * This,
            /* [retval][out] */ CatType *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Type )( 
            ICat * This,
            /* [in] */ CatType newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            ICat * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            ICat * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            ICat * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            ICat * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            ICat * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            ICat * This);
        
        END_INTERFACE
    } ICatVtbl;

    interface ICat
    {
        CONST_VTBL struct ICatVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICat_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICat_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICat_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICat_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICat_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICat_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICat_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICat_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define ICat_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define ICat_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define ICat_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define ICat_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define ICat_get_Type(This,pVal)	\
    ( (This)->lpVtbl -> get_Type(This,pVal) ) 

#define ICat_put_Type(This,newVal)	\
    ( (This)->lpVtbl -> put_Type(This,newVal) ) 

#define ICat_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define ICat_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define ICat_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define ICat_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define ICat_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define ICat_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define ICat_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define ICat_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define ICat_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define ICat_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define ICat_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define ICat_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define ICat_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define ICat_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICat_INTERFACE_DEFINED__ */


#ifndef __IFeedback_INTERFACE_DEFINED__
#define __IFeedback_INTERFACE_DEFINED__

/* interface IFeedback */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IFeedback;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8ADE1096-0495-479A-BC49-6B02F92CBB59")
    IFeedback : public INSDataObject
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ID( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Description( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Find( 
            /* [in] */ BSTR ID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Modify( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ResetToStart( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetNext( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetPrevious( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetDataPath( 
            /* [in] */ BSTR Path) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Feature( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Feature( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MinColor( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxColor( 
            /* [retval][out] */ DWORD *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceRemote( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceLocal( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ForceDefault( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetTemp( 
            /* [in] */ BOOL Temp) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DumpRecord( 
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveTemp( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IFeedbackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IFeedback * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IFeedback * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IFeedback * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IFeedback * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IFeedback * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IFeedback * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IFeedback * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *GetNumRecords )( 
            IFeedback * This,
            /* [retval][out] */ ULONGLONG *Cnt);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ID )( 
            IFeedback * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ID )( 
            IFeedback * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IFeedback * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Description )( 
            IFeedback * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Find )( 
            IFeedback * This,
            /* [in] */ BSTR ID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Modify )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ResetToStart )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetNext )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetPrevious )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetDataPath )( 
            IFeedback * This,
            /* [in] */ BSTR Path);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Feature )( 
            IFeedback * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Feature )( 
            IFeedback * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MinColor )( 
            IFeedback * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinColor )( 
            IFeedback * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxColor )( 
            IFeedback * This,
            /* [retval][out] */ DWORD *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxColor )( 
            IFeedback * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceRemote )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceLocal )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ForceDefault )( 
            IFeedback * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetTemp )( 
            IFeedback * This,
            /* [in] */ BOOL Temp);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DumpRecord )( 
            IFeedback * This,
            /* [in] */ BSTR ID,
            /* [in] */ BSTR OutFile);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveTemp )( 
            IFeedback * This);
        
        END_INTERFACE
    } IFeedbackVtbl;

    interface IFeedback
    {
        CONST_VTBL struct IFeedbackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IFeedback_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IFeedback_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IFeedback_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IFeedback_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IFeedback_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IFeedback_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IFeedback_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IFeedback_GetNumRecords(This,Cnt)	\
    ( (This)->lpVtbl -> GetNumRecords(This,Cnt) ) 


#define IFeedback_get_ID(This,pVal)	\
    ( (This)->lpVtbl -> get_ID(This,pVal) ) 

#define IFeedback_put_ID(This,newVal)	\
    ( (This)->lpVtbl -> put_ID(This,newVal) ) 

#define IFeedback_get_Description(This,pVal)	\
    ( (This)->lpVtbl -> get_Description(This,pVal) ) 

#define IFeedback_put_Description(This,newVal)	\
    ( (This)->lpVtbl -> put_Description(This,newVal) ) 

#define IFeedback_Find(This,ID)	\
    ( (This)->lpVtbl -> Find(This,ID) ) 

#define IFeedback_Add(This)	\
    ( (This)->lpVtbl -> Add(This) ) 

#define IFeedback_Modify(This)	\
    ( (This)->lpVtbl -> Modify(This) ) 

#define IFeedback_Remove(This)	\
    ( (This)->lpVtbl -> Remove(This) ) 

#define IFeedback_ResetToStart(This)	\
    ( (This)->lpVtbl -> ResetToStart(This) ) 

#define IFeedback_GetNext(This)	\
    ( (This)->lpVtbl -> GetNext(This) ) 

#define IFeedback_GetPrevious(This)	\
    ( (This)->lpVtbl -> GetPrevious(This) ) 

#define IFeedback_SetDataPath(This,Path)	\
    ( (This)->lpVtbl -> SetDataPath(This,Path) ) 

#define IFeedback_get_Feature(This,pVal)	\
    ( (This)->lpVtbl -> get_Feature(This,pVal) ) 

#define IFeedback_put_Feature(This,newVal)	\
    ( (This)->lpVtbl -> put_Feature(This,newVal) ) 

#define IFeedback_get_MinColor(This,pVal)	\
    ( (This)->lpVtbl -> get_MinColor(This,pVal) ) 

#define IFeedback_put_MinColor(This,newVal)	\
    ( (This)->lpVtbl -> put_MinColor(This,newVal) ) 

#define IFeedback_get_MaxColor(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxColor(This,pVal) ) 

#define IFeedback_put_MaxColor(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxColor(This,newVal) ) 

#define IFeedback_ForceRemote(This)	\
    ( (This)->lpVtbl -> ForceRemote(This) ) 

#define IFeedback_ForceLocal(This)	\
    ( (This)->lpVtbl -> ForceLocal(This) ) 

#define IFeedback_ForceDefault(This)	\
    ( (This)->lpVtbl -> ForceDefault(This) ) 

#define IFeedback_SetTemp(This,Temp)	\
    ( (This)->lpVtbl -> SetTemp(This,Temp) ) 

#define IFeedback_DumpRecord(This,ID,OutFile)	\
    ( (This)->lpVtbl -> DumpRecord(This,ID,OutFile) ) 

#define IFeedback_RemoveTemp(This)	\
    ( (This)->lpVtbl -> RemoveTemp(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IFeedback_INTERFACE_DEFINED__ */



#ifndef __DATAMODLib_LIBRARY_DEFINED__
#define __DATAMODLib_LIBRARY_DEFINED__

/* library DATAMODLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_DATAMODLib;

EXTERN_C const CLSID CLSID_Experiment;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EDA-A758-11D3-8A58-000000000000")
Experiment;
#endif

EXTERN_C const CLSID CLSID_Subject;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EDD-A758-11D3-8A58-000000000000")
Subject;
#endif

EXTERN_C const CLSID CLSID_Group;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EE0-A758-11D3-8A58-000000000000")
Group;
#endif

EXTERN_C const CLSID CLSID_NSCondition;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EE3-A758-11D3-8A58-000000000000")
NSCondition;
#endif

EXTERN_C const CLSID CLSID_ProcSetting;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EE6-A758-11D3-8A58-000000000000")
ProcSetting;
#endif

EXTERN_C const CLSID CLSID_ExperimentMember;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EE9-A758-11D3-8A58-000000000000")
ExperimentMember;
#endif

EXTERN_C const CLSID CLSID_ExperimentCondition;

#ifdef __cplusplus

class DECLSPEC_UUID("DCB05EEC-A758-11D3-8A58-000000000000")
ExperimentCondition;
#endif

EXTERN_C const CLSID CLSID_MQuestionnaire;

#ifdef __cplusplus

class DECLSPEC_UUID("6BC7F958-1612-11D4-8B4C-00104BC7E2C8")
MQuestionnaire;
#endif

EXTERN_C const CLSID CLSID_GQuestionnaire;

#ifdef __cplusplus

class DECLSPEC_UUID("B37E09B4-2516-11D4-8B62-00104BC7E2C8")
GQuestionnaire;
#endif

EXTERN_C const CLSID CLSID_SQuestionnaire;

#ifdef __cplusplus

class DECLSPEC_UUID("D62EF4A7-25BC-11D4-8B63-00104BC7E2C8")
SQuestionnaire;
#endif

EXTERN_C const CLSID CLSID_NSMUser;

#ifdef __cplusplus

class DECLSPEC_UUID("171877A8-42A1-4F1B-80F0-6F4B99D26CCB")
NSMUser;
#endif

EXTERN_C const CLSID CLSID_Backup;

#ifdef __cplusplus

class DECLSPEC_UUID("F58B4531-C9CD-4AB1-863C-DB4B1A83543C")
Backup;
#endif

EXTERN_C const CLSID CLSID_Element;

#ifdef __cplusplus

class DECLSPEC_UUID("2A8C9549-9C71-4054-B331-364A8F5FB2D3")
Element;
#endif

EXTERN_C const CLSID CLSID_Stimulus;

#ifdef __cplusplus

class DECLSPEC_UUID("70F4BE0C-C5BA-4FF5-93F7-6864A65E9BC9")
Stimulus;
#endif

EXTERN_C const CLSID CLSID_StimulusElement;

#ifdef __cplusplus

class DECLSPEC_UUID("811A8821-CD38-40EB-A033-2A2397FF9CAF")
StimulusElement;
#endif

EXTERN_C const CLSID CLSID_StimulusTarget;

#ifdef __cplusplus

class DECLSPEC_UUID("2FBBC252-21ED-4E87-8C6C-C2A581EC811D")
StimulusTarget;
#endif

EXTERN_C const CLSID CLSID_Cat;

#ifdef __cplusplus

class DECLSPEC_UUID("81A56385-FFF2-4FCD-B81C-1089EB8FC848")
Cat;
#endif

EXTERN_C const CLSID CLSID_Feedback;

#ifdef __cplusplus

class DECLSPEC_UUID("047760A1-7540-49DF-B8EC-775F4753AF62")
Feedback;
#endif
#endif /* __DATAMODLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



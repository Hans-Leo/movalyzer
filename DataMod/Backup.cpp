// Backup.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "BackupDB.h"
#include "Backup.h"

/////////////////////////////////////////////////////////////////////////////
//

Backup::Backup() : m_pDB( NULL )
{
	m_pDB = new BackupDB;
}

Backup::~Backup()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP Backup::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IBackup,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP Backup::get_UserID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_UserID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_UserID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_UserID = newVal;

	return S_OK;
}

STDMETHODIMP Backup::get_BackupDate(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_BackupDate.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_BackupDate(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_BackupDate = newVal;

	return S_OK;
}

STDMETHODIMP Backup::get_Path(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Path.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_Path(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Path = newVal;

	return S_OK;
}

STDMETHODIMP Backup::get_BackupType(BackupType *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (BackupType)m_pDB->m_Mode;

	return S_OK;
}

STDMETHODIMP Backup::put_BackupType(BackupType newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Mode = newVal;

	return S_OK;
}

STDMETHODIMP Backup::get_Exp(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Exp.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_Exp(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Exp = newVal;

	return S_OK;
}

STDMETHODIMP Backup::Find(BSTR UserID, BSTR BackupDate)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = UserID;
	CString szDt = BackupDate;
	if( !m_pDB->Find( szID, szDt ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_UserID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Backup::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP Backup::get_Notes(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Notes.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_Notes(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Notes = newVal;

	return S_OK;
}

STDMETHODIMP Backup::get_Grp(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Grp.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_Grp(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Grp = newVal;

	return S_OK;
}

STDMETHODIMP Backup::get_Subj(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Subj.AllocSysString();

	return S_OK;
}

STDMETHODIMP Backup::put_Subj(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Subj = newVal;

	return S_OK;
}

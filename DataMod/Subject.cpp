// Subject.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "SubjectDB.h"
#include "Subject.h"

/////////////////////////////////////////////////////////////////////////////
//

Subject::Subject()  : m_pDB( NULL )
{
	m_pDB = new SubjectDB;
}

Subject::~Subject()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP Subject::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ISubject,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP Subject::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP Subject::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP Subject::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjID = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_LastName(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_LastName.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_LastName(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_LastName = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_FirstName(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_FirstName.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_FirstName(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_FirstName = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_Notes(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Notes.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_Notes(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Notes = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_PrvNotes(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_PrvNotes.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_PrvNotes(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_PrvNotes = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_DateAdded(DATE *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DateAdded;

	return S_OK;
}

STDMETHODIMP Subject::put_DateAdded(DATE newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DateAdded = newVal;

	return S_OK;
}

STDMETHODIMP Subject::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_SubjID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Subject::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP Subject::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP Subject::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP Subject::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP Subject::get_DefaultExperiment(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DefExp.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_DefaultExperiment(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DefExp = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_Active(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Active;

	return S_OK;
}

STDMETHODIMP Subject::put_Active(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Active = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_ExperimentCount(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpCount;

	return S_OK;
}

STDMETHODIMP Subject::put_ExperimentCount(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpCount = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_Encrypted(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Encrypted;

	return S_OK;
}

STDMETHODIMP Subject::put_Encrypted(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Encrypted = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_Code(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjCode.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_Code(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjCode = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_EncryptionMethod(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EncryptionMethod;

	return S_OK;
}

STDMETHODIMP Subject::put_EncryptionMethod(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_EncryptionMethod = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_SiteID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SiteID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_SiteID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SiteID = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_SiteDesc(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SiteDesc.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_SiteDesc(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SiteDesc = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_Signature(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Signature.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_Signature(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Signature = newVal;

	return S_OK;
}

STDMETHODIMP Subject::get_Password(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Password.AllocSysString();

	return S_OK;
}

STDMETHODIMP Subject::put_Password(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Password = newVal;

	return S_OK;
}

STDMETHODIMP Subject::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*Cnt = GetRecordCount( m_pDB );
	return S_OK;
}

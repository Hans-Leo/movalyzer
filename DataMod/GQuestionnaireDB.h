#if !defined(AFX_GQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_GQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GQuestionnaireDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBGQuestionnaire.h"

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireDB recordset

class GQuestionnaireDB : public CTDataDB
{
public:
	GQuestionnaireDB();

// Field/Param Data
	CString	m_ExpID;
	CString	m_GrpID;
	CString	m_ID;
	short	m_ItemNum;

// Implementation
public:
	BOOL Find( CString szExpID, CString szGrpID );
	BOOL Find( CString szExpID, CString szGrpID, CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove( CString szExpID, CString szGrpID );
	BOOL Remove();
	BOOL RemoveQuestion( CString szID );
	BOOL RemoveGroup( CString szGrpID );
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
protected:
	BOOL GetAll( pGQUESTIONNAIRE );
	BOOL SetAll( pGQUESTIONNAIRE );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)

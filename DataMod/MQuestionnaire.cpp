// MQuestionnaire.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "MQuestionnaireDB.h"
#include "MQuestionnaire.h"

/////////////////////////////////////////////////////////////////////////////
//

MQuestionnaire::MQuestionnaire()  : m_pDB( NULL )
{
	m_pDB = new MQuestionnaireDB;
}

MQuestionnaire::~MQuestionnaire()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP MQuestionnaire::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IMQuestionnaire,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP MQuestionnaire::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP MQuestionnaire::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ID.AllocSysString();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ID = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_Question(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Question.AllocSysString();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_Question(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Question = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_IsHeader(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Header;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_IsHeader(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Header = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_IsPrivate(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Private;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_IsPrivate(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Private = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_ItemNum(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ItemNum;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_ItemNum(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ItemNum = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP MQuestionnaire::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_IsNumeric(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

		*pVal = m_pDB->m_Numeric;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_IsNumeric(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

		m_pDB->m_Numeric = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::get_ColumnHeader(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

		*pVal = m_pDB->m_ColHeader.AllocSysString();

	return S_OK;
}

STDMETHODIMP MQuestionnaire::put_ColumnHeader(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

		m_pDB->m_ColHeader = newVal;

	return S_OK;
}

STDMETHODIMP MQuestionnaire::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*Cnt = GetRecordCount( m_pDB );
	return S_OK;
}

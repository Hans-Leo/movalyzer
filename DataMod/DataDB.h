#if !defined(AFX_DATADB_H__DCB05ED2_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_DATADB_H__DCB05ED2_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataDB.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DataDB recordset

class DataDB : public CRecordset
{
	DECLARE_DYNAMIC(DataDB)
public:
	DataDB(CDatabase* pDatabase = NULL);
	~DataDB();

// Field/Param Data
	//{{AFX_FIELD(DataDB, CRecordset)
	//}}AFX_FIELD

// Overrides
	//{{AFX_VIRTUAL(DataDB)
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL Find();
	void ReleaseDB() { if( IsOpen() ) Close(); }
	virtual BOOL Add();
	virtual BOOL Modify();
	virtual BOOL Remove();

	DEFINE_MOVEMENT()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATADB_H__DCB05ED2_A758_11D3_8A58_000000000000__INCLUDED_)

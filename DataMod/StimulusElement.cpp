// StimulusElement.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "StimElementDB.h"
#include "StimulusElement.h"

/////////////////////////////////////////////////////////////////////////////
//

StimulusElement::StimulusElement()  : m_pDB( NULL )
{
	m_pDB = new StimElementDB;
}

StimulusElement::~StimulusElement()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP StimulusElement::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IStimulusElement,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP StimulusElement::get_StimulusID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StimID.AllocSysString();

	return S_OK;
}

STDMETHODIMP StimulusElement::put_StimulusID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StimID = newVal;

	return S_OK;
}

STDMETHODIMP StimulusElement::get_ElementID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ElmtID.AllocSysString();

	return S_OK;
}

STDMETHODIMP StimulusElement::put_ElementID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ElmtID = newVal;

	return S_OK;
}

STDMETHODIMP StimulusElement::Find(BSTR StimID, BSTR ElmtID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = StimID;
	CString szElmtID = ElmtID;
	if( !m_pDB->Find( szID, szElmtID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::FindForStimulus(BSTR StimID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = StimID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_StimID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP StimulusElement::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP StimulusElement::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP StimulusElement::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP StimulusElement::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

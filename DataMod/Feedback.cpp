// Feedback.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "FeedbackDB.h"
#include "Feedback.h"

/////////////////////////////////////////////////////////////////////////////
//

Feedback::Feedback()  : m_pDB( NULL )
{
	m_pDB = new FeedbackDB;
}

Feedback::~Feedback()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP Feedback::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IFeedback,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP Feedback::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP Feedback::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP Feedback::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_FeedbackID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Feedback::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_FeedbackID = newVal;

	return S_OK;
}

STDMETHODIMP Feedback::get_Description(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Desc.AllocSysString();

	return S_OK;
}

STDMETHODIMP Feedback::put_Description(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Desc = newVal;

	return S_OK;
}

STDMETHODIMP Feedback::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_FeedbackID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Feedback::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP Feedback::get_Feature(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Feature.AllocSysString();

	return S_OK;
}

STDMETHODIMP Feedback::put_Feature(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Feature = newVal;

	return S_OK;
}

STDMETHODIMP Feedback::get_MinColor(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_MinColor;

	return S_OK;
}

STDMETHODIMP Feedback::put_MinColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_MinColor = newVal;

	return S_OK;
}

STDMETHODIMP Feedback::get_MaxColor(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_MaxColor;

	return S_OK;
}

STDMETHODIMP Feedback::put_MaxColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_MaxColor = newVal;

	return S_OK;
}

STDMETHODIMP Feedback::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP Feedback::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP Feedback::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP Feedback::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*Cnt = GetRecordCount( m_pDB );
	return S_OK;
}

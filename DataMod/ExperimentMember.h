// ExperimentMember.h: Definition of the ExperimentMember class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXPERIMENTMEMBER_H__DCB05EEA_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_EXPERIMENTMEMBER_H__DCB05EEA_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// ExperimentMember

class ExpMemberDB;

class ExperimentMember : 
	public IDispatchImpl<IExperimentMember, &IID_IExperimentMember, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<ExperimentMember,&CLSID_ExperimentMember>
{
public:
	ExperimentMember();
	~ExperimentMember();

BEGIN_COM_MAP(ExperimentMember)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IExperimentMember)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(ExperimentMember) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_ExperimentMember)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IExperimentMember
public:
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(RemoveGroup)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID);
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(FindForExperiment)(/*[in]*/ BSTR ExpID);
	STDMETHOD(FindForGroup)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID);
	STDMETHOD(Find)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID, /*[in]*/ BSTR SubjID);
	STDMETHOD(put_UpWhere)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_UpWhere)(/*[out,retval]*/ BSTR *pVal);
	STDMETHOD(put_UpWhen)(/*[in]*/ DATE newVal);
	STDMETHOD(get_UpWhen)(/*[out,retval]*/ DATE *pVal);
	STDMETHOD(put_Uploaded)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Uploaded)(/*[out,retval]*/ BOOL *pVal);
	STDMETHOD(put_ExpWhere)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ExpWhere)(/*[out,retval]*/ BSTR *pVal);
	STDMETHOD(put_ExpWhen)(/*[in]*/ DATE newVal);
	STDMETHOD(get_ExpWhen)(/*[out,retval]*/ DATE *pVal);
	STDMETHOD(put_Exported)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Exported)(/*[out,retval]*/ BOOL *pVal);
	STDMETHOD(put_SamplingRate)(/*[in]*/ short newVal);
	STDMETHOD(get_SamplingRate)(/*[out,retval]*/ short *pVal);
	STDMETHOD(put_DeviceRes)(/*[in]*/ double newVal);
	STDMETHOD(get_DeviceRes)(/*[out,retval]*/ double *pVal);
	STDMETHOD(put_SubjectID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SubjectID)(/*[out,retval]*/ BSTR *pVal);
	STDMETHOD(put_GroupID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_GroupID)(/*[out,retval]*/ BSTR *pVal);
	STDMETHOD(put_ExperimentID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ExperimentID)(/*[out,retval]*/ BSTR *pVal);
protected:
	ExpMemberDB*	m_pDB;
};

#endif // !defined(AFX_EXPERIMENTMEMBER_H__DCB05EEA_A758_11D3_8A58_000000000000__INCLUDED_)

#if !defined(AFX_BACKUPDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_BACKUPDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BackupDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBBackup.h"

#define	BT_UNKNOWN		0
#define	BT_ALL			1
#define	BT_EXPERIMENT	2
#define	BT_GROUP		3
#define	BT_SUBJECT		4
#define BT_DB			5

/////////////////////////////////////////////////////////////////////////////
// BackupDB recordset

class BackupDB : public CTDataDB
{
public:
	BackupDB();

// Field/Param Data
	CString	m_UserID;
	CString	m_BackupDate;
	CString	m_Path;
	int		m_Mode;
	CString	m_Exp;
	CString	m_Grp;
	CString	m_Subj;
	CString	m_Notes;

// Implementation
public:
	BOOL Find( CString szUserID, CString szBackupDate );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
protected:
	BOOL GetAll( pBACKUP );
	BOOL SetAll( pBACKUP );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BACKUPDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)

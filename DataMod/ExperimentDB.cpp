// ExperimentDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ExperimentDB.h"

DBExperiment _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExperimentDB

ExperimentDB::ExperimentDB() : CTDataDB(&_db)
{
	m_ExpID = _T("");
	m_Desc = _T("");
	m_Notes = _T("");
	Type = 0;		// EXP_TYPE_HANDWRITING (default)
	MissingDataValue = -1.e6;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExperimentDB::GetAll( pEXPERIMENT pExperiment )
{
	if( !pExperiment ) return FALSE;

	m_ExpID = pExperiment->ExpID;
	m_Desc = pExperiment->Desc;
	m_Notes = pExperiment->Notes;
	Type = pExperiment->Type;
	MissingDataValue = pExperiment->MissingDataValue;

	return TRUE;
}

BOOL ExperimentDB::SetAll( pEXPERIMENT pExperiment )
{
	if( !pExperiment ) return FALSE;

	strncpy_s( pExperiment->ExpID, szIDS + 1, m_ExpID, _TRUNCATE );
	strncpy_s( pExperiment->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	strncpy_s( pExperiment->Notes, szNOTES + 1, m_Notes, _TRUNCATE );
	pExperiment->Type = Type;
	pExperiment->MissingDataValue = MissingDataValue;

	return TRUE;
}

BOOL ExperimentDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pEXPERIMENT pExp = _db.Find( szID, szErr );
	if( !pExp )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pExp );
}

BOOL ExperimentDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENT exp;
	SetAll( &exp );
	if( !AddItem( (pDBDATA)&exp, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExperimentDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENT exp;
	SetAll( &exp );
	if( !ModifyItem( (pDBDATA)&exp, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExperimentDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_ExpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void ExperimentDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENT exp;
	if( GetFirstItem( (pDBDATA)&exp, szErr ) )
		GetAll( &exp );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExperimentDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENT exp;
	if( GetNextItem( (pDBDATA)&exp, szErr ) )
	{
		GetAll( &exp );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExperimentDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENT exp;
	if( GetPreviousItem( (pDBDATA)&exp, szErr ) )
	{
		GetAll( &exp );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExperimentDB::TestLogin()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	return CheckOpen( szErr );
}

/////////////////////////////////////////////////////////////////////////////

void ExperimentDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExperimentDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExperimentDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

// SQuestionnaire.h: Definition of the SQuestionnaire class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SQUESTIONNAIRE_H__D62EF4A8_25BC_11D4_8B63_00104BC7E2C8__INCLUDED_)
#define AFX_SQUESTIONNAIRE_H__D62EF4A8_25BC_11D4_8B63_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaire

class SQuestionnaireDB;

class SQuestionnaire : 
	public IDispatchImpl<ISQuestionnaire, &IID_ISQuestionnaire, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<SQuestionnaire,&CLSID_SQuestionnaire>
{
public:
	SQuestionnaire();
	~SQuestionnaire();

BEGIN_COM_MAP(SQuestionnaire)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISQuestionnaire)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(SQuestionnaire) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_SQuestionnaire)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ISQuestionnaire
public:
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(RemoveForSubject)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID, /*[in]*/ BSTR SubjID);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(RemoveQuestion)(/*[in]*/ BSTR ID);
	STDMETHOD(RemoveSubject)(/*[in]*/ BSTR SubjID);
	STDMETHOD(RemoveGroup)(/*[in]*/ BSTR GrpID);
	STDMETHOD(ClearQuestionnaire)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID, /*[in]*/ BSTR SubjID);
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(FindForGroup)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID);
	STDMETHOD(FindForSubject)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID, /*[in]*/ BSTR SubjID);
	STDMETHOD(Find)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID, /*[in]*/ BSTR SubjID, /*[in]*/ BSTR ID);
	STDMETHOD(get_Answer)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Answer)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SubjectID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SubjectID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_GroupID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_GroupID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ExperimentID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ExperimentID)(/*[in]*/ BSTR newVal);
protected:
	SQuestionnaireDB*	m_pDB;
};

#endif // !defined(AFX_SQUESTIONNAIRE_H__D62EF4A8_25BC_11D4_8B63_00104BC7E2C8__INCLUDED_)

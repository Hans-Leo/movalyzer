#if !defined(AFX_STIMULUSDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_STIMULUSDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// STIMULUSDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBStimulus.h"

/////////////////////////////////////////////////////////////////////////////
// STIMULUSDB recordset

class StimulusDB : public CTDataDB
{
public:
	StimulusDB();

// Field/Param Data
	CString	m_StimID;
	CString	m_Desc;
	// temp var for demo..remove next version
	BOOL	m_Demo;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pSTIMULUS );
	BOOL SetAll( pSTIMULUS );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STIMULUSDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)

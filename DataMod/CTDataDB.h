#pragma once

// CTDataDB.h : header file
//

#include "..\Common\DefFun.h"
#include "..\Common\Macros.h"

/////////////////////////////////////////////////////////////////////////////
// CTDataDB recordset

struct DBDATA;
class DBData;

class CTDataDB
{
public:
	CTDataDB( DBData* pDB ) : m_fOpen( FALSE ), m_pDB( pDB ) {}
	~CTDataDB();

// Implementation
public:
	void SetDataPath( CString szPath );
	void SetBackupPath( CString szPath );
	void SetAppWindow( HWND hWnd );
	BOOL IsVirgin();
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
	ULONGLONG GetNumRecords();
	BOOL RepairTable( CString& );
protected:
	BOOL CheckOpen( CString& szErr );
	BOOL AddItem( DBDATA* pData, CString& szErr );
	BOOL ModifyItem( DBDATA* pData, CString& szErr );
	BOOL GetFirstItem( DBDATA* pData, CString& szErr );
	BOOL GetNextItem( DBDATA* pData, CString& szErr );
	BOOL GetPreviousItem( DBDATA* pData, CString& szErr );

	BOOL	m_fOpen;
	DBData*	m_pDB;
};

class NSDataObject
{
public:
	NSDataObject() {}
	virtual ~NSDataObject() {}
	ULONGLONG GetRecordCount( CTDataDB* pDB );
};

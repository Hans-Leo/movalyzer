// DataDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DataDB.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DataDB

IMPLEMENT_DYNAMIC(DataDB, CRecordset)
IMPLEMENT_MOVEMENT(DataDB)

DataDB::DataDB(CDatabase* pdb) : CRecordset(pdb)
{
	m_pDatabase = GetMasterDB();

	//{{AFX_FIELD_INIT(DataDB)
	//}}AFX_FIELD_INIT

	m_nDefaultType = dynaset;
}

DataDB::~DataDB()
{
	::ReleaseMasterDB();
}

/////////////////////////////////////////////////////////////////////////////

BOOL DataDB::Find()
{
	try
	{
		if( !IsOpen() && !Open() ) return FALSE;

		BOOL fRet = Requery();
		if( fRet ) fRet = ( GetRecordCount() != 0 );

		m_strFilter = _T("");
	}
	catch( CDBException* e )
	{
		OutputMessage( e->m_strError );
		return FALSE;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DataDB::Add()
{
	return FALSE;
}

BOOL DataDB::Modify()
{
	try
	{
		if( !IsOpen() ) return FALSE;

		Edit();
		SetFieldDirty( NULL );
		Update();
	}
	catch( CDBException* e )
	{
		OutputMessage( e->m_strError );
		return FALSE;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL DataDB::Remove()
{
	try
	{
		if( !IsOpen() ) return FALSE;

		Delete();
		ResetToStart();
	}
	catch( CDBException* e )
	{
		OutputMessage( e->m_strError );
		return FALSE;
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

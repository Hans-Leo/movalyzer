#if !defined(AFX_SUBJECTDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_SUBJECTDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SubjectDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBSubject.h"

/////////////////////////////////////////////////////////////////////////////
// SubjectDB recordset

class SubjectDB : public CTDataDB
{
public:
	SubjectDB();

// Field/Param Data
	CString			m_SubjID;
	CString			m_LastName;
	CString			m_FirstName;
	CString			m_Notes;
	CString			m_PrvNotes;
	COleDateTime	m_DateAdded;
	CString			m_DefExp;
	BOOL			m_Active;
	short			m_ExpCount;
	BOOL			m_Encrypted;
	CString			m_SubjCode;
	short			m_EncryptionMethod;
	CString			m_SiteID;
	CString			m_SiteDesc;
	CString			m_Signature;
	CString			m_Password;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pSUBJECT );
	BOOL SetAll( pSUBJECT );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUBJECTDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)

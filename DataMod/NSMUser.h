// NSMUser.h: Definition of the NSMUser class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// NSMUser

class UserDB;

class NSMUser : 
	public IDispatchImpl<INSMUser, &IID_INSMUser, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<NSMUser,&CLSID_NSMUser>,
	public NSDataObject
{
public:
	NSMUser();
	~NSMUser();

BEGIN_COM_MAP(NSMUser)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(INSMUser)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(NSMUser) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_NSMUser)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// INSMUser
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(get_Signature)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Signature)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SiteDesc)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SiteDesc)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SiteID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SiteID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_EncryptionMethod)(short *pVal);
	STDMETHOD(put_EncryptionMethod)(short newVal);
	STDMETHOD(get_Encrypted)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Encrypted)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SubjectCurID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SubjectCurID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SubjectEndID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SubjectEndID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SubjectStartID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SubjectStartID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_GenerateSubjectIDs)(/*[out,retval]*/ BOOL *pVal);
	STDMETHOD(put_GenerateSubjectIDs)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Virgin)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(get_WinUser)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_WinUser)(/*[in]*/BSTR newVal);
	STDMETHOD(get_Private)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Private)(/*[in]*/BOOL newVal);
	STDMETHOD(get_AutoUpdate)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_AutoUpdate)(/*[in]*/BOOL newVal);
	STDMETHOD(get_CommPort)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_CommPort)(/*[in]*/ BSTR newVal);
	STDMETHOD(SetOperationMode)(BOOL ClientServerON, BSTR CSServer, BSTR CSUser, BSTR CSPassword);
	STDMETHOD(ResetWarningStatus)();
	STDMETHOD(get_SysPass)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SysPass)(/*[in]*/ BSTR newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_AspectRatioHeight)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_AspectRatioHeight)(/*[in]*/ double newVal);
	STDMETHOD(get_AspectRatioWidth)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_AspectRatioWidth)(/*[in]*/ double newVal);
	STDMETHOD(get_DisplayHeight)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_DisplayHeight)(/*[in]*/ double newVal);
	STDMETHOD(get_DisplayWidth)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_DisplayWidth)(/*[in]*/ double newVal);
	STDMETHOD(get_TabletHeight)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TabletHeight)(/*[in]*/ double newVal);
	STDMETHOD(get_TabletWidth)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TabletWidth)(/*[in]*/ double newVal);
	STDMETHOD(get_Alpha)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Alpha)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_LogTablet)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_LogTablet)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_LogGraph)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_LogGraph)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_LogProc)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_LogProc)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_LogDB)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_LogDB)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_LogUI)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_LogUI)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Log)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Log)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_EntireTablet)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_EntireTablet)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TabletMode)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_TabletMode)(/*[in]*/ short newVal);
	STDMETHOD(get_InputType)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_InputType)(/*[in]*/ short newVal);
	STDMETHOD(get_Delimiter)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Delimiter)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_BackupPath)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_BackupPath)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RootPath)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_RootPath)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_UserID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_UserID)(/*[in]*/ BSTR newVal);
protected:
	UserDB*	m_pDB;
};

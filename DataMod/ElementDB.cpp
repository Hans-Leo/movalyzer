// ElementDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ElementDB.h"

DBElement _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ElementDB

ElementDB::ElementDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(ElementDB)
	m_ElmtID = _T("");
	m_Desc = _T("");
	m_Pattern = _T("");
	m_Shape = 0;
	m_CenterX = 3.;
	m_CenterY = 5.;
	m_WidthX = 0.2;
	m_WidthY = 0.2;
	m_Line1Size = 2;
	m_Line1Color = 0;
	m_Background1Color = RGB(255,255,255);
	m_Text1 = _T("");
	m_Text1Color = 0;
	m_Text1Size = 10;
	m_IsTarget = FALSE;
	m_ErrorX = 0.;
	m_ErrorY = 0.;
	m_Line2Size = 2;
	m_Line2Color = 0;
	m_Background2Color = RGB(0,255,0);
	m_Text2 = _T("");
	m_Text2Color = 0;
	m_Text2Size = 10;
	m_Line3Size = 2;
	m_Line3Color = 0;
	m_Background3Color = RGB(255,0,0);
	m_Text3 = _T("X");
	m_Text3Color = 0;
	m_Text3Size = 10;
	m_IsImage = FALSE;
	m_Start = 0.;
	m_Duration = 0.;
	m_HideRightTarget = FALSE;
	m_HideWrongTarget = FALSE;
	m_HideIfAnyRightTarget = FALSE;
	m_HideIfAnyWrongTarget = FALSE;
	m_RightTarget = _T("");
	m_WrongTarget = _T("");
	m_CatID = _T("");
	m_Animate = FALSE;
	m_AnimationFile = _T("");
	m_AnimationRandom = FALSE;
	m_AnimationGenerate = FALSE;
	m_AnimationSubjID = _T("");
	m_AnimationRate = 120.;
	//}}AFX_FIELD_INIT
	memset( &m_lf1, 0, sizeof( LOGFONT ) );
	strncpy_s(m_lf1.lfFaceName, LF_FACESIZE, "Arial", _TRUNCATE);
	memset( &m_lf2, 0, sizeof( LOGFONT ) );
	strncpy_s(m_lf2.lfFaceName, LF_FACESIZE, "Arial", _TRUNCATE);
	memset( &m_lf3, 0, sizeof( LOGFONT ) );
	strncpy_s(m_lf3.lfFaceName, LF_FACESIZE, "Arial", _TRUNCATE);
}

/////////////////////////////////////////////////////////////////////////////

BOOL ElementDB::GetAll( pELEMENT pElement )
{
	if( !pElement ) return FALSE;

	m_ElmtID = pElement->ElmtID;
	m_Desc = pElement->Desc;
	m_Pattern = pElement->Pattern;
	m_Shape = pElement->Shape;
	m_CenterX = pElement->CenterX;
	m_CenterY = pElement->CenterY;
	m_WidthX = pElement->WidthX;
	m_WidthY = pElement->WidthY;
	m_Line1Size = pElement->Line1Size;
	m_Line1Color = pElement->Line1Color;
	m_Text1 = pElement->Text1;
	m_Text1Color = pElement->Text1Color;
	m_Text1Size = pElement->Text1Size;
	m_IsTarget = pElement->IsTarget;
	m_ErrorX = pElement->ErrorX;
	m_ErrorY = pElement->ErrorY;
	m_Line2Size = pElement->Line2Size;
	m_Line2Color = pElement->Line2Color;
	m_Text2 = pElement->Text2;
	m_Text2Color = pElement->Text2Color;
	m_Text2Size = pElement->Text2Size;
	m_Line3Size = pElement->Line3Size;
	m_Line3Color = pElement->Line3Color;
	m_Text3 = pElement->Text3;
	m_Text3Color = pElement->Text3Color;
	m_Text3Size = pElement->Text3Size;
	m_Background1Color = pElement->Bk1Color;
	m_Background2Color = pElement->Bk2Color;
	m_Background3Color = pElement->Bk3Color;
	m_lf1.lfHeight = pElement->lf1.lfHeight;
	m_lf1.lfWidth = pElement->lf1.lfWidth;
	m_lf1.lfEscapement = pElement->lf1.lfEscapement;
	m_lf1.lfOrientation = pElement->lf1.lfOrientation;
	m_lf1.lfWeight = pElement->lf1.lfWeight;
	m_lf1.lfItalic = pElement->lf1.lfItalic;
	m_lf1.lfUnderline = pElement->lf1.lfUnderline;
	m_lf1.lfStrikeOut = pElement->lf1.lfStrikeOut;
	m_lf1.lfCharSet = pElement->lf1.lfCharSet;
	m_lf1.lfOutPrecision = pElement->lf1.lfOutPrecision;
	m_lf1.lfClipPrecision = pElement->lf1.lfClipPrecision;
	m_lf1.lfQuality = pElement->lf1.lfQuality;
	m_lf1.lfPitchAndFamily = pElement->lf1.lfPitchAndFamily;
	strncpy_s( m_lf1.lfFaceName, LF_FACESIZE, pElement->lf1.lfFaceName, _TRUNCATE );
	m_lf2.lfHeight = pElement->lf2.lfHeight;
	m_lf2.lfWidth = pElement->lf2.lfWidth;
	m_lf2.lfEscapement = pElement->lf2.lfEscapement;
	m_lf2.lfOrientation = pElement->lf2.lfOrientation;
	m_lf2.lfWeight = pElement->lf2.lfWeight;
	m_lf2.lfItalic = pElement->lf2.lfItalic;
	m_lf2.lfUnderline = pElement->lf2.lfUnderline;
	m_lf2.lfStrikeOut = pElement->lf2.lfStrikeOut;
	m_lf2.lfCharSet = pElement->lf2.lfCharSet;
	m_lf2.lfOutPrecision = pElement->lf2.lfOutPrecision;
	m_lf2.lfClipPrecision = pElement->lf2.lfClipPrecision;
	m_lf2.lfQuality = pElement->lf2.lfQuality;
	m_lf2.lfPitchAndFamily = pElement->lf2.lfPitchAndFamily;
	strncpy_s( m_lf2.lfFaceName, LF_FACESIZE, pElement->lf2.lfFaceName, _TRUNCATE );
	m_lf3.lfHeight = pElement->lf3.lfHeight;
	m_lf3.lfWidth = pElement->lf3.lfWidth;
	m_lf3.lfEscapement = pElement->lf3.lfEscapement;
	m_lf3.lfOrientation = pElement->lf3.lfOrientation;
	m_lf3.lfWeight = pElement->lf3.lfWeight;
	m_lf3.lfItalic = pElement->lf3.lfItalic;
	m_lf3.lfUnderline = pElement->lf3.lfUnderline;
	m_lf3.lfStrikeOut = pElement->lf3.lfStrikeOut;
	m_lf3.lfCharSet = pElement->lf3.lfCharSet;
	m_lf3.lfOutPrecision = pElement->lf3.lfOutPrecision;
	m_lf3.lfClipPrecision = pElement->lf3.lfClipPrecision;
	m_lf3.lfQuality = pElement->lf3.lfQuality;
	m_lf3.lfPitchAndFamily = pElement->lf3.lfPitchAndFamily;
	strncpy_s( m_lf3.lfFaceName, LF_FACESIZE, pElement->lf3.lfFaceName, _TRUNCATE );
	m_IsImage = pElement->IsImage;
	m_Start = pElement->Start;
	m_Duration = pElement->Duration;
	m_HideRightTarget = pElement->HideRightTarget;
	m_HideWrongTarget = pElement->HideWrongTarget;
	m_HideIfAnyRightTarget = pElement->HideIfAnyRightTarget;
	m_HideIfAnyWrongTarget = pElement->HideIfAnyWrongTarget;
	m_RightTarget = pElement->RightTarget;
	m_WrongTarget = pElement->WrongTarget;
	m_CatID = pElement->CatID;
	m_Animate = pElement->Animate;
	m_AnimationFile = pElement->AnimationFile;
	m_AnimationRandom = pElement->AnimationRandom;
	m_AnimationGenerate = pElement->AnimationGenerate;
	m_AnimationSubjID = pElement->AnimationSubjID;
	m_AnimationRate = pElement->AnimationRate;

	return TRUE;
}

BOOL ElementDB::SetAll( pELEMENT pElement )
{
	if( !pElement ) return FALSE;

	strncpy_s( pElement->ElmtID, szIDS + 1, m_ElmtID, _TRUNCATE );
	strncpy_s( pElement->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	strncpy_s( pElement->Pattern, szFILELENGTH + 1, m_Pattern, _TRUNCATE );
	pElement->Shape = m_Shape;
	pElement->CenterX = m_CenterX;
	pElement->CenterY = m_CenterY;
	pElement->WidthX = m_WidthX;
	pElement->WidthY = m_WidthY;
	pElement->Line1Size = m_Line1Size;
	pElement->Line1Color = m_Line1Color;
	strncpy_s( pElement->Text1, szELEMENT_TEXT + 1, m_Text1, _TRUNCATE );
	pElement->Text1Color = m_Text1Color;
	pElement->Text1Size = m_Text1Size;
	pElement->IsTarget = m_IsTarget;
	pElement->ErrorX = m_ErrorX;
	pElement->ErrorY = m_ErrorY;
	pElement->Line2Size = m_Line2Size;
	pElement->Line2Color = m_Line2Color;
	strncpy_s( pElement->Text2, szELEMENT_TEXT + 1, m_Text2, _TRUNCATE );
	pElement->Text2Color = m_Text2Color;
	pElement->Text2Size = m_Text2Size;
	pElement->Line3Size = m_Line3Size;
	pElement->Line3Color = m_Line3Color;
	strncpy_s( pElement->Text3, szELEMENT_TEXT + 1, m_Text3, _TRUNCATE );
	pElement->Text3Color = m_Text3Color;
	pElement->Text3Size = m_Text3Size;
	pElement->Bk1Color = m_Background1Color;
	pElement->Bk2Color = m_Background2Color;
	pElement->Bk3Color = m_Background3Color;
	pElement->lf1.lfHeight = m_lf1.lfHeight;
	pElement->lf1.lfWidth = m_lf1.lfWidth;
	pElement->lf1.lfEscapement = m_lf1.lfEscapement;
	pElement->lf1.lfOrientation = m_lf1.lfOrientation;
	pElement->lf1.lfWeight = m_lf1.lfWeight;
	pElement->lf1.lfItalic = m_lf1.lfItalic;
	pElement->lf1.lfUnderline = m_lf1.lfUnderline;
	pElement->lf1.lfStrikeOut = m_lf1.lfStrikeOut;
	pElement->lf1.lfCharSet = m_lf1.lfCharSet;
	pElement->lf1.lfOutPrecision = m_lf1.lfOutPrecision;
	pElement->lf1.lfClipPrecision = m_lf1.lfClipPrecision;
	pElement->lf1.lfQuality = m_lf1.lfQuality;
	pElement->lf1.lfPitchAndFamily = m_lf1.lfPitchAndFamily;
	strncpy_s( pElement->lf1.lfFaceName, LF_FACESIZE, m_lf1.lfFaceName, _TRUNCATE );
	pElement->lf2.lfHeight = m_lf2.lfHeight;
	pElement->lf2.lfWidth = m_lf2.lfWidth;
	pElement->lf2.lfEscapement = m_lf2.lfEscapement;
	pElement->lf2.lfOrientation = m_lf2.lfOrientation;
	pElement->lf2.lfWeight = m_lf2.lfWeight;
	pElement->lf2.lfItalic = m_lf2.lfItalic;
	pElement->lf2.lfUnderline = m_lf2.lfUnderline;
	pElement->lf2.lfStrikeOut = m_lf2.lfStrikeOut;
	pElement->lf2.lfCharSet = m_lf2.lfCharSet;
	pElement->lf2.lfOutPrecision = m_lf2.lfOutPrecision;
	pElement->lf2.lfClipPrecision = m_lf2.lfClipPrecision;
	pElement->lf2.lfQuality = m_lf2.lfQuality;
	pElement->lf2.lfPitchAndFamily = m_lf2.lfPitchAndFamily;
	strncpy_s( pElement->lf2.lfFaceName, LF_FACESIZE, m_lf2.lfFaceName, _TRUNCATE );
	pElement->lf3.lfHeight = m_lf3.lfHeight;
	pElement->lf3.lfWidth = m_lf3.lfWidth;
	pElement->lf3.lfEscapement = m_lf3.lfEscapement;
	pElement->lf3.lfOrientation = m_lf3.lfOrientation;
	pElement->lf3.lfWeight = m_lf3.lfWeight;
	pElement->lf3.lfItalic = m_lf3.lfItalic;
	pElement->lf3.lfUnderline = m_lf3.lfUnderline;
	pElement->lf3.lfStrikeOut = m_lf3.lfStrikeOut;
	pElement->lf3.lfCharSet = m_lf3.lfCharSet;
	pElement->lf3.lfOutPrecision = m_lf3.lfOutPrecision;
	pElement->lf3.lfClipPrecision = m_lf3.lfClipPrecision;
	pElement->lf3.lfQuality = m_lf3.lfQuality;
	pElement->lf3.lfPitchAndFamily = m_lf3.lfPitchAndFamily;
	strncpy_s( pElement->lf3.lfFaceName, LF_FACESIZE, m_lf3.lfFaceName, _TRUNCATE );
	pElement->IsImage = m_IsImage;
	pElement->Start = m_Start;
	pElement->Duration = m_Duration;
	pElement->HideRightTarget = m_HideRightTarget;
	pElement->HideWrongTarget = m_HideWrongTarget;
	pElement->HideIfAnyRightTarget = m_HideIfAnyRightTarget;
	pElement->HideIfAnyWrongTarget = m_HideIfAnyWrongTarget;
	strncpy_s( pElement->RightTarget, szIDS + 1, m_RightTarget, _TRUNCATE );
	strncpy_s( pElement->WrongTarget, szIDS + 1, m_WrongTarget, _TRUNCATE );
	strncpy_s( pElement->CatID, szIDS + 1, m_CatID, _TRUNCATE );
	pElement->Animate = m_Animate;
	strncpy_s( pElement->AnimationFile, szFILELENGTH + 1, m_AnimationFile, _TRUNCATE );
	pElement->AnimationRandom = m_AnimationRandom;
	pElement->AnimationGenerate = m_AnimationGenerate;
	strncpy_s( pElement->AnimationSubjID, szIDS + szIDS + szIDS + 1, m_AnimationSubjID, _TRUNCATE );
	pElement->AnimationRate = m_AnimationRate;

	return TRUE;
}

BOOL ElementDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pELEMENT pElement = _db.Find( szID, szErr );
	if( !pElement )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pElement );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ElementDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	ELEMENT elmt;
	SetAll( &elmt );
	SetAll( &elmt );
	if( !AddItem( (pDBDATA)&elmt, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ElementDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	ELEMENT elmt;
	SetAll( &elmt );
	if( !ModifyItem( (pDBDATA)&elmt, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ElementDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_ElmtID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void ElementDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	ELEMENT elmt;
	if( GetFirstItem( (pDBDATA)&elmt, szErr ) )
		GetAll( &elmt );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ElementDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	ELEMENT elmt;
	if( GetNextItem( (pDBDATA)&elmt, szErr ) )
	{
		GetAll( &elmt );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ElementDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	ELEMENT elmt;
	if( GetPreviousItem( (pDBDATA)&elmt, szErr ) )
	{
		GetAll( &elmt );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ElementDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ElementDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ElementDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

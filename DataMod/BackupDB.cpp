// BackupDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "BackupDB.h"

DBBackup _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BackupDB

BackupDB::BackupDB() : CTDataDB(&_db)
{
	//{{AFX_FIELD_INIT(BackupDB)
	m_UserID = _T("");
	m_BackupDate = _T("");
	m_Path = _T("");
	m_Mode = BT_UNKNOWN;
	m_Exp = _T("");
	m_Grp = _T("");
	m_Subj = _T("");
	m_Notes = _T("");
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL BackupDB::GetAll( pBACKUP pBackup )
{
	if( !pBackup ) return FALSE;

	m_UserID = pBackup->UserID;
	m_BackupDate = pBackup->BackupDate;
	m_Path = pBackup->Path;
	m_Mode = pBackup->Mode;
	m_Exp = pBackup->Exp;
	m_Grp = pBackup->Grp;
	m_Subj = pBackup->Subj;
	m_Notes = pBackup->Notes;

	return TRUE;
}

BOOL BackupDB::SetAll( pBACKUP pBackup )
{
	if( !pBackup ) return FALSE;

	strncpy_s( pBackup->UserID, szIDS + 1, m_UserID, _TRUNCATE );
	strncpy_s( pBackup->BackupDate, szDATE + 1, m_BackupDate, _TRUNCATE );
	strncpy_s( pBackup->Path, _MAX_PATH, m_Path, _TRUNCATE );
	pBackup->Mode = m_Mode;
	strncpy_s( pBackup->Exp, szIDS + 1, m_Exp, _TRUNCATE );
	strncpy_s( pBackup->Grp, szIDS + 1, m_Grp, _TRUNCATE );
	strncpy_s( pBackup->Subj, szIDS + 1, m_Subj, _TRUNCATE );
	strncpy_s( pBackup->Notes, szNOTES + 1, m_Notes, _TRUNCATE );

	return TRUE;
}

BOOL BackupDB::Find( CString szUserID, CString szBackupDate )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pBACKUP pB = _db.Find( szUserID, szBackupDate, szErr );
	if( !pB )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pB );
}

/////////////////////////////////////////////////////////////////////////////

BOOL BackupDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	BACKUP b;
	SetAll( &b );
	if( !AddItem( (pDBDATA)&b, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL BackupDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	BACKUP b;
	SetAll( &b );
	if( !ModifyItem( (pDBDATA)&b, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL BackupDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_UserID, m_BackupDate, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void BackupDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	BACKUP b;
	if( GetFirstItem( (pDBDATA)&b, szErr ) )
		GetAll( &b );
}

/////////////////////////////////////////////////////////////////////////////

BOOL BackupDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	BACKUP b;
	if( GetNextItem( (pDBDATA)&b, szErr ) )
	{
		GetAll( &b );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL BackupDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	BACKUP b;
	if( GetPreviousItem( (pDBDATA)&b, szErr ) )
	{
		GetAll( &b );
		return TRUE;
	}

	return FALSE;
}

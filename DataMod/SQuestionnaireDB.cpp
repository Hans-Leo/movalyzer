// SQuestionnaireDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SQuestionnaireDB.h"

DBSQuestionnaire _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireDB

SQuestionnaireDB::SQuestionnaireDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(SQuestionnaireDB)
	m_ExpID = _T("");
	m_GrpID = _T("");
	m_SubjID = _T("");
	m_ID = _T("");
	m_Answer = _T("");
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::GetAll( pSQUESTIONNAIRE pQuestionnaire )
{
	if( !pQuestionnaire ) return FALSE;

	m_ExpID = pQuestionnaire->ExpID;
	m_GrpID = pQuestionnaire->GrpID;
	m_SubjID = pQuestionnaire->SubjID;
	m_ID = pQuestionnaire->ID;
	m_Answer = pQuestionnaire->Answer;

	return TRUE;
}

BOOL SQuestionnaireDB::SetAll( pSQUESTIONNAIRE pQuestionnaire )
{
	if( !pQuestionnaire ) return FALSE;

	strncpy_s( pQuestionnaire->ExpID, szIDS + 1, m_ExpID, _TRUNCATE );
	strncpy_s( pQuestionnaire->GrpID, szIDS + 1, m_GrpID, _TRUNCATE );
	strncpy_s( pQuestionnaire->SubjID, szIDS + 1, m_SubjID, _TRUNCATE );
	strncpy_s( pQuestionnaire->ID, szIDS + 1, m_ID, _TRUNCATE );
	strncpy_s( pQuestionnaire->Answer, szQUESTIONNAIRE_A + 1, m_Answer, _TRUNCATE );

	return TRUE;
}

BOOL SQuestionnaireDB::Find( CString szExpID, CString szGrpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSQUESTIONNAIRE pQuestionnaire = _db.Find( szExpID, szGrpID, szErr );
	if( !pQuestionnaire )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pQuestionnaire );
}

BOOL SQuestionnaireDB::Find( CString szExpID, CString szGrpID, CString szSubjID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSQUESTIONNAIRE pQuestionnaire = _db.Find( szExpID, szGrpID, szSubjID, szErr );
	if( !pQuestionnaire )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pQuestionnaire );
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::Find( CString szExpID, CString szGrpID, CString szSubjID, CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSQUESTIONNAIRE pQuestionnaire = _db.Find( szExpID, szGrpID, szSubjID, szID, szErr );
	if( !pQuestionnaire )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pQuestionnaire );
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SQUESTIONNAIRE quest;
	SetAll( &quest );
	if( !AddItem( (pDBDATA)&quest, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SQUESTIONNAIRE quest;
	SetAll( &quest );
	if( !ModifyItem( (pDBDATA)&quest, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_GrpID, m_SubjID, m_ID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::Remove( CString szExpID, CString szGrpID, CString szSubjID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( szExpID, szGrpID, szSubjID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::ClearQuestionnaire( CString szExpID, CString szGrpID, CString szSubjID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.ClearQuestionnaire( szExpID, szGrpID, szSubjID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::RemoveGroup( CString szGrpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.RemoveGroup( szGrpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::RemoveSubject( CString szSubjID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.RemoveSubject( szSubjID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::RemoveQuestion( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.RemoveQuestion( szID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void SQuestionnaireDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SQUESTIONNAIRE quest;
	if( GetFirstItem( (pDBDATA)&quest, szErr ) )
		GetAll( &quest );
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SQUESTIONNAIRE quest;
	if( GetNextItem( (pDBDATA)&quest, szErr ) )
	{
		GetAll( &quest );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SQuestionnaireDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SQUESTIONNAIRE quest;
	if( GetPreviousItem( (pDBDATA)&quest, szErr ) )
	{
		GetAll( &quest );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

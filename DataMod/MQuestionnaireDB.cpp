// MQuestionnaireDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MQuestionnaireDB.h"

#include "..\CTreeLink\DBMQuestionnaire.h"

DBMQuestionnaire _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MQuestionnaireDB

MQuestionnaireDB::MQuestionnaireDB() : CTDataDB( &_db )
{
	m_ID = _T("");
	m_ItemNum = 0;
	m_Header = FALSE;
	m_Private = FALSE;
	m_Question = _T("");
	m_Numeric = TRUE;
	m_ColHeader = _T("");
}

/////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaireDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pMQUESTIONNAIRE pQuestionnaire = _db.Find( szID, szErr );
	if( !pQuestionnaire )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	m_ID = pQuestionnaire->ID;
	m_Header = pQuestionnaire->Header;
	m_Private = pQuestionnaire->Private;
	m_ItemNum = atoi( pQuestionnaire->ItemNum );
	m_Question = pQuestionnaire->Question;
	m_Numeric = pQuestionnaire->Numeric;
	m_ColHeader = pQuestionnaire->ColHeader;
	if( ( m_ColHeader == _T("") ) && ( m_ID != _T("") ) )
	{
		m_ColHeader = _T("Q_");
		m_ColHeader += m_ID;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaireDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	MQUESTIONNAIRE quest;
	strncpy_s( quest.ID, szIDS + 1, m_ID, _TRUNCATE );
	quest.Header = m_Header;
	quest.Private = m_Private;
	sprintf_s( quest.ItemNum, _T("%d"), m_ItemNum );
	strncpy_s( quest.Question, szQUESTIONNAIRE_Q + 1, m_Question, _TRUNCATE );
	quest.Numeric = m_Numeric;
	strncpy_s( quest.ColHeader, szCODE + 1, m_ColHeader, _TRUNCATE );
	if( !AddItem( (pDBDATA)&quest, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaireDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	MQUESTIONNAIRE quest;
	strncpy_s( quest.ID, szIDS + 1, m_ID, _TRUNCATE );
	quest.Header = m_Header;
	quest.Private = m_Private;
	sprintf_s( quest.ItemNum, _T("%d"), m_ItemNum );
	strncpy_s( quest.Question, szQUESTIONNAIRE_Q + 1, m_Question, _TRUNCATE );
	quest.Numeric = m_Numeric;
	strncpy_s( quest.ColHeader, szCODE + 1, m_ColHeader, _TRUNCATE );
	if( !ModifyItem( (pDBDATA)&quest, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaireDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_ID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void MQuestionnaireDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	MQUESTIONNAIRE quest;
	if( GetFirstItem( (pDBDATA)&quest, szErr ) )
	{
		m_ID = quest.ID;
		m_Header = quest.Header;
		m_Private = quest.Private;
		m_ItemNum = atoi( quest.ItemNum );
		m_Question = quest.Question;
		m_Numeric = quest.Numeric;
		m_ColHeader = quest.ColHeader;
		if( ( m_ColHeader == _T("") ) && ( m_ID != _T("") ) )
		{
			m_ColHeader = _T("Q_");
			m_ColHeader += m_ID;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaireDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	MQUESTIONNAIRE quest;
	if( GetNextItem( (pDBDATA)&quest, szErr ) )
	{
		m_ID = quest.ID;
		m_Header = quest.Header;
		m_Private = quest.Private;
		m_ItemNum = atoi( quest.ItemNum );
		m_Question = quest.Question;
		m_Numeric = quest.Numeric;
		m_ColHeader = quest.ColHeader;
		if( ( m_ColHeader == _T("") ) && ( m_ID != _T("") ) )
		{
			m_ColHeader = _T("Q_");
			m_ColHeader += m_ID;
		}

		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaireDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	MQUESTIONNAIRE quest;
	if( GetPreviousItem( (pDBDATA)&quest, szErr ) )
	{
		m_ID = quest.ID;
		m_Header = quest.Header;
		m_Private = quest.Private;
		m_ItemNum = atoi( quest.ItemNum );
		m_Question = quest.Question;
		m_Numeric = quest.Numeric;
		m_ColHeader = quest.ColHeader;
		if( ( m_ColHeader == _T("") ) && ( m_ID != _T("") ) )
		{
			m_ColHeader = _T("Q_");
			m_ColHeader += m_ID;
		}

		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void MQuestionnaireDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void MQuestionnaireDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void MQuestionnaireDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

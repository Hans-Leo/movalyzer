// ExpMemberDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ExpMemberDB.h"

DBExperimentMember _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExpMemberDB

ExpMemberDB::ExpMemberDB() : CTDataDB(&_db)
{
	m_ExpID = _T("");
	m_GrpID = _T("");
	m_SubjID = _T("");
	m_DeviceRes = 0.;
	m_SamplingRate = 0;
	m_Exported = 0;
	m_ExpWhen = (DATE)0;
	m_ExpWhere = _T("");
	m_Uploaded = 0;
	m_UpWhen = (DATE)0;
	m_UpWhere = _T("");
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::GetAll( pEXPERIMENTMEMBER pEM )
{
	if( !pEM ) return FALSE;

	m_ExpID = pEM->ExpID;
	m_GrpID = pEM->GrpID;
	m_SubjID = pEM->SubjID;
	m_DeviceRes = pEM->DeviceRes;
	m_SamplingRate = pEM->SamplingRate;
	m_Exported = pEM->Exported;
	m_ExpWhen = pEM->ExpWhen;
	m_ExpWhere = pEM->ExpWhere;
	m_Uploaded = pEM->Uploaded;
	m_UpWhen = pEM->UpWhen;
	m_UpWhere = pEM->UpWhere;

	return TRUE;
}

BOOL ExpMemberDB::SetAll( pEXPERIMENTMEMBER pEM )
{
	if( !pEM ) return FALSE;

	strncpy_s( pEM->ExpID, szIDS + 1, m_ExpID, _TRUNCATE );
	strncpy_s( pEM->GrpID, szIDS + 1, m_GrpID, _TRUNCATE );
	strncpy_s( pEM->SubjID, szIDS + 1, m_SubjID, _TRUNCATE );
	pEM->DeviceRes = m_DeviceRes;
	pEM->SamplingRate = m_SamplingRate;
	pEM->Exported = m_Exported;
	pEM->ExpWhen = m_ExpWhen;
	strncpy_s( pEM->ExpWhere, _MAX_PATH + 1, m_ExpWhere, _TRUNCATE );
	pEM->Uploaded = m_Uploaded;
	pEM->UpWhen = m_UpWhen;
	strncpy_s( pEM->UpWhere, _MAX_PATH + 1, m_UpWhere, _TRUNCATE );

	return TRUE;
}

BOOL ExpMemberDB::Find( CString szExpID, CString szGrpID, CString szSubjID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pEXPERIMENTMEMBER pEM = _db.Find( szExpID, szGrpID, szSubjID, szErr );
	if( !pEM )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pEM );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::Find( CString szExpID, CString szGrpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pEXPERIMENTMEMBER pEM = _db.Find( szExpID, szGrpID, szErr );
	if( !pEM )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pEM );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::Find( CString szExpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pEXPERIMENTMEMBER pEM = _db.Find( szExpID, szErr );
	if( !pEM )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pEM );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTMEMBER em;
	SetAll( &em );
	if( !AddItem( (pDBDATA)&em, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTMEMBER em;
	SetAll( &em );
	if( !ModifyItem( (pDBDATA)&em, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_ExpID, m_GrpID, m_SubjID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::Remove( CString szExpID, CString szGrpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( szExpID, szGrpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpMemberDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTMEMBER em;
	if( GetFirstItem( (pDBDATA)&em, szErr ) )
		GetAll( &em );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTMEMBER em;
	if( GetNextItem( (pDBDATA)&em, szErr ) )
	{
		GetAll( &em );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpMemberDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTMEMBER em;
	if( GetPreviousItem( (pDBDATA)&em, szErr ) )
	{
		GetAll( &em );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpMemberDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpMemberDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpMemberDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

// SubjectDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SubjectDB.h"

DBSubject _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SubjectDB

SubjectDB::SubjectDB() : CTDataDB( &_db )
{
	m_SubjID = _T("");
	m_LastName = _T("-");
	m_FirstName = _T("-");
	m_Notes = _T("");
	m_PrvNotes = _T("");
	m_DefExp = _T("");
	m_Active = TRUE;
	m_ExpCount = 0;
	m_DateAdded = COleDateTime::GetCurrentTime();
	m_Encrypted = FALSE;
	m_SubjCode = _T("");
	m_EncryptionMethod = ENC_NONE;
	m_SiteID = _T("");
	m_SiteDesc = _T("");
	m_Signature = _T("");
	m_Password = _T("");
}

/////////////////////////////////////////////////////////////////////////////

BOOL SubjectDB::GetAll( pSUBJECT pSubject )
{
	if( !pSubject ) return FALSE;

	m_SubjID = pSubject->SubjID;
	m_LastName = pSubject->LastName;
	m_FirstName = pSubject->FirstName;
	m_Notes = pSubject->Notes;
	m_PrvNotes = pSubject->PrvNotes;
	m_DateAdded = pSubject->DateAdded;
	m_DefExp = pSubject->DefExp;
	m_Active = pSubject->Active;
	m_ExpCount = pSubject->ExpCount;
	m_Encrypted = pSubject->Encrypted;
	m_SubjCode = pSubject->SubjCode;
	if( m_SubjCode == _T("") ) m_SubjCode = m_SubjID;
	m_EncryptionMethod = pSubject->EncryptionMethod;
	// if encrypted = 1 and encryption method = 0, then it was encrypted before this field and is DES
	if( m_Encrypted && ( m_EncryptionMethod == ENC_NONE ) ) m_EncryptionMethod = ENC_DES;
	m_SiteID = pSubject->SiteID;
	m_SiteDesc = pSubject->SiteDesc;
	m_Signature = pSubject->Signature;
	m_Password = pSubject->Password;

	return TRUE;
}

BOOL SubjectDB::SetAll( pSUBJECT pSubject )
{
	if( !pSubject ) return FALSE;

	strncpy_s( pSubject->SubjID, szIDS + 1, m_SubjID, _TRUNCATE );
	strncpy_s( pSubject->LastName, szSUBJECT_LAST_ENC + 1, m_LastName, _TRUNCATE );
	strncpy_s( pSubject->FirstName, szSUBJECT_FIRST_ENC + 1, m_FirstName, _TRUNCATE );
	strncpy_s( pSubject->Notes, szNOTES + 1, m_Notes, _TRUNCATE );
	strncpy_s( pSubject->PrvNotes, szNOTES_ENC + 1, m_PrvNotes, _TRUNCATE );
	pSubject->DateAdded = m_DateAdded;
	strncpy_s( pSubject->DefExp, szIDS + 1, m_DefExp, _TRUNCATE );
	pSubject->Active = m_Active;
	pSubject->ExpCount = m_ExpCount;
	pSubject->Encrypted = m_Encrypted;
	strncpy_s( pSubject->SubjCode, szCODE + 1, m_SubjCode, _TRUNCATE );
	pSubject->EncryptionMethod = m_EncryptionMethod;
	strncpy_s( pSubject->SiteID, szCODE + 1, m_SiteID, _TRUNCATE );
	strncpy_s( pSubject->SiteDesc, szDESC + 1, m_SiteDesc, _TRUNCATE );
	strncpy_s( pSubject->Signature, szSIGNATURE + 1, m_Signature, _TRUNCATE );
	strncpy_s( pSubject->Password, szSUBJECT_PASS + 1, m_Password, _TRUNCATE );

	return TRUE;
}

BOOL SubjectDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSUBJECT pSubject = _db.Find( szID, szErr );
	if( !pSubject )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pSubject );
}

/////////////////////////////////////////////////////////////////////////////

BOOL SubjectDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SUBJECT subj;
	SetAll( &subj );
	if( !AddItem( (pDBDATA)&subj, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SubjectDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SUBJECT subj;
	SetAll( &subj );
	if( !ModifyItem( (pDBDATA)&subj, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SubjectDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_SubjID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void SubjectDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SUBJECT subj;
	if( GetFirstItem( (pDBDATA)&subj, szErr ) )
		GetAll( &subj );
}

/////////////////////////////////////////////////////////////////////////////

BOOL SubjectDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SUBJECT subj;
	if( GetNextItem( (pDBDATA)&subj, szErr ) )
	{
		GetAll( &subj );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL SubjectDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	SUBJECT subj;
	if( GetPreviousItem( (pDBDATA)&subj, szErr ) )
	{
		GetAll( &subj );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void SubjectDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void SubjectDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void SubjectDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

#if !defined(AFX_GROUPDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_GROUPDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBGroup.h"

/////////////////////////////////////////////////////////////////////////////
// GroupDB recordset

class GroupDB : public CTDataDB
{
public:
	GroupDB();

// Field/Param Data
	CString	m_GrpID;
	CString	m_Desc;
	CString	m_Notes;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pGROUP );
	BOOL SetAll( pGROUP );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)

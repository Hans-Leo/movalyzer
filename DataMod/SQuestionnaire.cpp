// SQuestionnaire.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "SQuestionnaireDB.h"
#include "SQuestionnaire.h"

/////////////////////////////////////////////////////////////////////////////
//

SQuestionnaire::SQuestionnaire()  : m_pDB( NULL )
{
	m_pDB = new SQuestionnaireDB;
}

SQuestionnaire::~SQuestionnaire()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP SQuestionnaire::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ISQuestionnaire,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP SQuestionnaire::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP SQuestionnaire::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP SQuestionnaire::get_ExperimentID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP SQuestionnaire::put_ExperimentID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpID = newVal;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::get_GroupID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_GrpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP SQuestionnaire::put_GroupID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_GrpID = newVal;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::get_SubjectID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjID.AllocSysString();

	return S_OK;
}

STDMETHODIMP SQuestionnaire::put_SubjectID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjID = newVal;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ID.AllocSysString();

	return S_OK;
}

STDMETHODIMP SQuestionnaire::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ID = newVal;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::get_Answer(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Answer.AllocSysString();

	return S_OK;
}

STDMETHODIMP SQuestionnaire::put_Answer(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Answer = newVal;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::Find(BSTR ExpID, BSTR GrpID, BSTR SubjID, BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	CString szSubjID = SubjID;
	CString szID = ID;
	if( !m_pDB->Find( szExpID, szGrpID, szSubjID, szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::FindForGroup(BSTR ExpID, BSTR GrpID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	if( !m_pDB->Find( szExpID, szGrpID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::FindForSubject(BSTR ExpID, BSTR GrpID, BSTR SubjID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	CString szSubjID = SubjID;
	if( !m_pDB->Find( szExpID, szGrpID, szSubjID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::RemoveForSubject( BSTR ExpID, BSTR GrpID, BSTR SubjID )
{
	return ClearQuestionnaire( ExpID, GrpID, SubjID );
}

STDMETHODIMP SQuestionnaire::ClearQuestionnaire( BSTR ExpID, BSTR GrpID, BSTR SubjID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	CString szSubjID = SubjID;
	if( !m_pDB->ClearQuestionnaire( szExpID, szGrpID, szSubjID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::RemoveGroup( BSTR GrpID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szGrpID = GrpID;
	if( !m_pDB->RemoveGroup( szGrpID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::RemoveSubject( BSTR SubjID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szSubjID = SubjID;
	if( !m_pDB->RemoveSubject( szSubjID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::RemoveQuestion( BSTR ID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->RemoveQuestion( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP SQuestionnaire::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

// Condition.h: Definition of the Condition class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols
#include "ConditionDB.h"

/////////////////////////////////////////////////////////////////////////////
// Condition

class NSCondition : 
	public IDispatchImpl<INSCondition, &IID_INSCondition, &LIBID_DATAMODLib>, 
	public IDispatchImpl<INSConditionSound, &IID_INSConditionSound, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<NSCondition,&CLSID_NSCondition>,
	public NSDataObject
{
public:
	NSCondition();
	~NSCondition();

BEGIN_COM_MAP(NSCondition)
	COM_INTERFACE_ENTRY2(IDispatch, INSCondition)
	COM_INTERFACE_ENTRY(INSCondition)
	COM_INTERFACE_ENTRY(INSConditionSound)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

DECLARE_REGISTRY_RESOURCEID(IDR_Condition)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ICondition
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
// IProcSetSound
	STDMETHOD(SetFeedbackInfo)(short Which, BOOL Use, short Column, short Stroke,
							   double Min, double Max, BOOL SwapColors);
	STDMETHOD(GetFeedbackInfo)(short Which, BOOL* Use, short* Column, short* Stroke,
							   double* Min, double* Max, BOOL* SwapColors);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(get_GripperTask)(/*[out, retval]*/short *pVal);
	STDMETHOD(put_GripperTask)(/*[in]*/short newVal);
	STDMETHOD(get_Feedback)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Feedback)(/*[in]*/ BSTR newVal);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(get_FlagBadTarget)(/*[in]*/ BOOL *pVal);
	STDMETHOD(put_FlagBadTarget)(/*[out, retval]*/ BOOL newVal);
	STDMETHOD(get_SoundCount)(/*[out, retval]*/ short *pVal);
	STDMETHOD(get_SoundInfo)(/*[in]*/ SoundCat cat, /*[in]*/ SoundType type, /*[out, retval]*/ VARIANT *pVal);
	STDMETHOD(put_SoundInfo)(/*[in]*/ SoundCat cat, /*[in]*/ SoundType type, /*[in]*/ VARIANT newVal);
	STDMETHOD(get_SoundTrigger)(/*[in]*/ SoundType type, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SoundTrigger)(/*[in]*/ SoundType type, /*[in]*/ BOOL newVal);
	STDMETHOD(get_SoundScript)(/*[in]*/ SoundType type, short* ScriptType, BSTR* pScript);
	STDMETHOD(put_SoundScript)(/*[in]*/ SoundType type, /*[in]*/ short ScripType, /*[in]*/ BSTR Script);
	STDMETHOD(get_SoundMedia)(/*[in]*/ SoundType type, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SoundMedia)(/*[in]*/ SoundType type, /*[in]*/ BSTR newVal);
	STDMETHOD(get_SoundToneDur)(/*[in]*/ SoundType type, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_SoundToneDur)(/*[in]*/ SoundType type, /*[in]*/ short newVal);
	STDMETHOD(get_SoundToneFreq)(/*[in]*/ SoundType type, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_SoundToneFreq)(/*[in]*/ SoundType type, /*[in]*/ short newVal);
	STDMETHOD(get_SoundTone)(/*[in]*/ SoundType type, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SoundTone)(/*[in]*/ SoundType type, /*[in]*/ BOOL newVal);
	STDMETHOD(get_SoundUse)(/*[in]*/ SoundType type, /*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SoundUse)(/*[in]*/ SoundType type, /*[in]*/ BOOL newVal);
	STDMETHOD(get_CountStrokes)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CountStrokes)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_HideFeedback)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_HideFeedback)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_HideShowAfter)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_HideShowAfter)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_HideShowAfterShow)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_HideShowAfterShow)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_HideShowAfterStrokes)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_HideShowAfterStrokes)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_HideShowCount)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_HideShowCount)(/*[in]*/ double newVal);
	STDMETHOD(get_Transform)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Transform)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Xgain)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_Xgain)(/*[in]*/ double newVal);
	STDMETHOD(get_Xrotation)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_Xrotation)(/*[in]*/ double newVal);
	STDMETHOD(get_Ygain)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_Ygain)(/*[in]*/ double newVal);
	STDMETHOD(get_Yrotation)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_Yrotation)(/*[in]*/ double newVal);
	STDMETHOD(get_MagnetForce)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MagnetForce)(/*[in]*/ double newVal);
	STDMETHOD(get_WarningLatency)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_WarningLatency)(/*[in]*/ double newVal);
	STDMETHOD(get_WarningDuration)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_WarningDuration)(/*[in]*/ double newVal);
	STDMETHOD(SetBackupPath)(/*[in]*/ BSTR Path);
	STDMETHOD(get_StopLastTarget)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_StopLastTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_StartFirstTarget)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_StartFirstTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_StopWrongTarget)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_StopWrongTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_RecordImmediately)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_RecordImmediately)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_PrecueLatency)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_PrecueLatency)(/*[in]*/ double newVal);
	STDMETHOD(get_PrecueDuration)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_PrecueDuration)(/*[in]*/ double newVal);
	STDMETHOD(get_StimulusPrecue)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_StimulusPrecue)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_StimulusWarning)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_StimulusWarning)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Stimulus)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Stimulus)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_UseStimulus)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_UseStimulus)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Word)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Word)(/*[in]*/ short newVal);
	STDMETHOD(get_Parent)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Parent)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Process)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Process)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Record)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Record)(/*[in]*/ BOOL newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_Notes)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Notes)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_StrokeSkip)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_StrokeSkip)(/*[in]*/ short newVal);
	STDMETHOD(get_RangeDirection)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_RangeDirection)(/*[in]*/ double newVal);
	STDMETHOD(get_StrokeDirection)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_StrokeDirection)(/*[in]*/ double newVal);
	STDMETHOD(get_RangeLength)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_RangeLength)(/*[in]*/ double newVal);
	STDMETHOD(get_StrokeLength)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_StrokeLength)(/*[in]*/ double newVal);
	STDMETHOD(get_StrokeMax)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_StrokeMax)(/*[in]*/ short newVal);
	STDMETHOD(get_StrokeMin)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_StrokeMin)(/*[in]*/ short newVal);
	STDMETHOD(get_Lex)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Lex)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Instruction)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Instruction)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	ConditionDB*	m_pDB;
};

#pragma once

// ProcSettingDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBProcSetting.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSettingDB recordset

class ProcSettingDB : public CTDataDB
{
public:
	ProcSettingDB();

// Field/Param Data
	CString	m_ExpID;
	// time function settings
	BOOL	m_Velocity;
	BOOL	m_Differentiate;
	double	m_RotationBeta;
	double	m_FilterFrequency;
	int		m_Decimate;
	// time function flags
	BOOL	m_TF_J;
	BOOL	m_TF_R;
	BOOL	m_TF_A;
	BOOL	m_TF_U;
	BOOL	m_TF_O;
	// segmentation flags
	BOOL	m_SEG_F;
	BOOL	m_SEG_L;
	BOOL	m_SEG_S;
	BOOL	m_SEG_O;
	BOOL	m_SEG_M;
	BOOL	m_SEG_A;
	BOOL	m_SEG_V;
	BOOL	m_SEG_MM;
	BOOL	m_SEG_J;
	// extraction flags
	BOOL	m_EXT_S;
	BOOL	m_EXT_3;
	BOOL	m_EXT_O;
	BOOL	m_EXT_2;
	// consistency checking flags
	BOOL	m_CON_A;
	BOOL	m_CON_N;
	BOOL	m_CON_M;
	BOOL	m_CON_U;
	BOOL	m_CON_C;
	BOOL	m_CON_S;
	BOOL	m_CON_O;
	BOOL	m_CON_D;	// discontinuity
	// recording options
	BOOL	m_SpecifySequence;
	CString	CondSeq;
	BOOL	m_Randomize;
	BOOL	m_RandomizeTrials;
	// summarization flags
	BOOL	m_ST_A;
	BOOL	m_ST_R;
	BOOL	m_ST_Z;
	BOOL	m_ST_T;
	BOOL	m_ST_S;
	BOOL	m_ST_D;
	int		m_DiscardAfter;
	BOOL	m_ST_D2;
	int		m_DiscardAfter2;
	BOOL	DiscardPenDownDurMin;
	double	PenDownDurMin;
	// input device settings
	int		m_SamplingRate;
	int		m_MinPenPressure;
	double	m_DeviceResolution;
	// recording options
	BOOL	m_ShowInstr;
	// sen2word settings
	FLD_DOUBLE( minleftpenup );
	FLD_DOUBLE( minleftsp );
	FLD_DOUBLE( minwidth );
	FLD_DOUBLE( mindownsp );
	FLD_DOUBLE( mintimepenup );
	FLD_DOUBLE( maxtimepenup );
	// timfun settings
	FLD_BOOL( FFT );
	FLD_DOUBLE( sharpness );
	FLD_INT( upsamplefactor );
	FLD_BOOL( remtraillift );
	// segmentation settings
	FLD_DOUBLE( absdistmin );
	FLD_DOUBLE( rdistmin );
	FLD_DOUBLE( timemin );
	FLD_DOUBLE( rvymin );
	FLD_DOUBLE( rvymin2 );
	// extraction settings
	FLD_DOUBLE( initialpart );
	FLD_DOUBLE( maxfreq );
	// consis settings
	FLD_DOUBLE( slanterror );
	FLD_DOUBLE( spiralincreasefactor );
	FLD_DOUBLE( minlooparean );
	FLD_DOUBLE( straightcritstraight );
	FLD_DOUBLE( straightcritcurved );
	FLD_BOOL( discardLTmin );
	FLD_DOUBLE( minreactiontime );
	FLD_BOOL( discardGTmax );
	FLD_DOUBLE( maxreactiontime );
	// run experiment options
	FLD_BOOL( DoQuestionnaire );
	FLD_BOOL( QuestAtEnd );
	FLD_DOUBLE( TimeoutStart );
	FLD_DOUBLE( TimeoutRecord );
	FLD_DOUBLE( TimeoutPenlift );
	FLD_DOUBLE( TimeoutLatency );
	FLD_BOOL( MaxRecArea );
	FLD_BOOL( FullScreen );
	FLD_BOOL( ProcImmediately );
	FLD_BOOL( DisplayCharts );
	FLD_BOOL( DisplayRaw );
	FLD_BOOL( DisplayTF );
	FLD_BOOL( DisplaySeg );
	FLD_BOOL( Summarize );
	FLD_BOOL( SummarizeOnly );
	FLD_BOOL( SummarizeOnlyGroup );
	FLD_BOOL( SummarizeNormDB );
	FLD_BOOL( SummarizeSelect );
	FLD_BOOL( Analyze );
	FLD_BOOL( ViewSubjExt );
	FLD_BOOL( ViewSubjCon );
	FLD_BOOL( ViewExpExt );
	FLD_BOOL( ViewExpCon );
	// in-between trial delay mode
	FLD_INT( TrialMode );
	// drawing options in recording window
	FLD_INT( LineSize );
	FLD_INT( EllipseSize );
	FLD_DWORD( BGColor );
	FLD_DWORD( LineColor1 );
	FLD_DWORD( LineColor2 );
	FLD_DWORD( LineColor3 );
	FLD_DWORD( EllipseColor );
	FLD_DWORD( MPPColor );	// min pen-pressure color
	FLD_BOOL( VaryLineThickness );
	FLD_INT( MaxLineThickness );
	// new consis parameters for comparing a circle to multiple L's
//	FLD_BOOL( CircleLLL );
//	FLD_DOUBLE( ThldRatioCircleSequence );
//	FLD_DOUBLE( ThldHorizRelCircle );
//	FLD_DOUBLE( ThldRatioLSequence );
//	FLD_DOUBLE( ThldHorizRelL );
//	FLD_BOOL( CircleIgnoreFirst );
//	FLD_BOOL( CircleIgnoreLast );
//	FLD_BOOL( LLLIgnoreFirst );
//	FLD_BOOL( LLLIgnoreLast );
	// Timfun/iolib discontinuity
	FLD_INT( DisCorrection );
	FLD_DOUBLE( DisFactor );
	FLD_DOUBLE( DisZInsertPoint );
	FLD_DOUBLE( DisRelError );
	FLD_DOUBLE( DisAbsError );
	// Randomization with Rules vars
	FLD_BOOL( RandWithRules );
	CString RandRules;
	CString RandBlockCounts;
	CString RandSelChars;
	CString RandSelTrials;
	// Summarization collapsing
	FLD_BOOL( CollapseUDStrokes );
	FLD_BOOL( CollapseStrokes );
	FLD_BOOL( CollapseTrials );
	FLD_BOOL( CollapseMedian );
	// to use or not - tilt
	FLD_BOOL( UseTilt );
	// Max Pen Pressure
	FLD_INT( MaxPenPressure );			// max pressure before notification
	// Discontinuity Detection
	FLD_BOOL( DisNotify );				// notify if dis. during recording
	FLD_DOUBLE( DisError );				// % of sampling rate deviation before detection
	// Image experiment settings
	FLD_INT( SmoothingIter );
	FLD_INT( InkThreshold );
	FLD_INT( ImgResolution );
	// External application settings
	FLD_INT( ExtTypeRaw );					// external app type before TimeFunction call (values defined in DataMod interface)
	CString ExtScriptRaw;					// script file for pre-TF call
	FLD_INT( ExtTypeTF );					// external app type before Segmentation call
	CString ExtScriptTF;					// script file for pre-Seg call
	FLD_INT( ExtTypeSeg );					// external app type before Extraction call
	CString ExtScriptSeg;					// script file for pre-Ext call
	FLD_INT( ExtTypeExt );					// external app type after Extraction call
	CString ExtScriptExt;					// script file for post-Ext call
	// Norm DB
	FLD_BOOL( NormDBCalcScore );
	FLD_BOOL( NormDBCalcScoreGlobal );
	FLD_BOOL( NormDBNoUpdate );
	FLD_DOUBLE( NormDBThreshold );

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pPROCSETTING );
	BOOL SetAll( pPROCSETTING );
};

// Experiment.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "ExperimentDB.h"
#include "Experiment.h"
#include "ProcSetting.h"
#include "Group.h"
#include "Subject.h"
#include "ExpMemberDB.h"

/////////////////////////////////////////////////////////////////////////////
//

Experiment::Experiment()  : m_pDB( NULL )
{
	m_pDB = new ExperimentDB;
}

Experiment::~Experiment()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP Experiment::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IExperiment,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP Experiment::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP Experiment::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP Experiment::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Experiment::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpID = newVal;

	return S_OK;
}

STDMETHODIMP Experiment::get_Description(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Desc.AllocSysString();

	return S_OK;
}

STDMETHODIMP Experiment::put_Description(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Desc = newVal;

	return S_OK;
}

STDMETHODIMP Experiment::get_Notes(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Notes.AllocSysString();

	return S_OK;
}

STDMETHODIMP Experiment::put_Notes(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Notes = newVal;

	return S_OK;
}

STDMETHODIMP Experiment::get_Type(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Type;

	return S_OK;
}

STDMETHODIMP Experiment::put_Type(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Type = newVal;

	return S_OK;
}

STDMETHODIMP Experiment::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ExpID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Experiment::GetProcSettings(IProcSetting *PS)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	HRESULT hr = E_FAIL;

	if( !PS )
		CoCreateInstance( CLSID_ProcSetting, NULL,
						  CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&PS );
	if( PS ) hr = PS->Find( m_pDB->m_ExpID.AllocSysString() );

	return hr;
}

STDMETHODIMP Experiment::SetProcSetting(IProcSetting *PS)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	HRESULT hr = E_FAIL;

	if( PS )
	{
		BSTR bstrExpID = m_pDB->m_ExpID.AllocSysString();
		PS->put_ExperimentID( bstrExpID );

		IProcSetting* pPS = NULL;
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
							   IID_IProcSetting, (LPVOID*)&pPS );
		if( SUCCEEDED( hr ) )
		{
			hr = pPS->Find( bstrExpID );
			if( SUCCEEDED( hr ) ) pPS->Remove();
			pPS->Release();
			hr = PS->Add();
		}
	}

	return hr;
}

STDMETHODIMP Experiment::AddGroupSubject(IGroup *pGroup, ISubject *pSubject)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( ISNULL( pGroup ) || ISNULL( pSubject ) ) return E_FAIL;

	BOOL fCont = TRUE;
	BSTR bstrGrp = NULL, bstrSubj = NULL, bstrExp = NULL;

	// Check to see if group exists
	pGroup->get_ID( &bstrGrp );
	if( !pGroup->Find( bstrGrp ) )
		fCont = pGroup->Add();

	// Check to see if subject exists
	if( fCont )
	{
		pSubject->get_ID( &bstrSubj );
		if( !pSubject->Find( bstrSubj ) )
			fCont = pSubject->Add();
	}

	// Update expmember
	bstrExp = this->m_pDB->m_ExpID.AllocSysString();
	ExpMemberDB eDB;
	if( !eDB.Find( bstrExp, bstrGrp, bstrSubj ) )
	{
		eDB.m_ExpID = bstrExp;
		eDB.m_GrpID = bstrGrp;
		eDB.m_SubjID = bstrSubj;

		fCont = eDB.Add();
	}
	else
	{
		CString szTemp;
		szTemp.LoadString( IDS_ADDSUBJ_ERR );
		OutputMessage( szTemp, LOG_DB );
	}

	return( fCont ? S_OK : E_FAIL );
}

STDMETHODIMP Experiment::SetOutputWindow( VARIANT* MsgWindow )
{
	CListCtrl* pWnd = (CListCtrl*)MsgWindow;
	::SetOutputWindow( pWnd );

	return S_OK;
}

STDMETHODIMP Experiment::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetDBLogOn( fOn );

	return S_OK;
}

STDMETHODIMP Experiment::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP Experiment::SetBackupPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetBackupPath( Path );

	return S_OK;
}

STDMETHODIMP Experiment::TestLogin()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB )
		if( m_pDB->TestLogin() ) return S_OK;

	return E_FAIL;
}

STDMETHODIMP Experiment::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP Experiment::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP Experiment::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP Experiment::SetAppWindow( DWORD AppWindow )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetAppWindow( (HWND)AppWindow );

	return S_OK;
}

STDMETHODIMP Experiment::put_MissingDataValue(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->MissingDataValue = newVal;

	return S_OK;
}

STDMETHODIMP Experiment::get_MissingDataValue(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->MissingDataValue;

	return S_OK;
}

STDMETHODIMP Experiment::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*Cnt = GetRecordCount( m_pDB );
	return S_OK;
}

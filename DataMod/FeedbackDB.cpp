// FeedbackDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "FeedbackDB.h"

DBFeedback _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FeedbackDB

FeedbackDB::FeedbackDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(FeedbackDB)
	m_FeedbackID = _T("");
	m_Desc = _T("");
	m_Feature = _T("");
	m_MinColor = 16711680;
	m_MaxColor = 255;
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL FeedbackDB::GetAll( pFEEDBACK pFeedback )
{
	if( !pFeedback ) return FALSE;

	m_FeedbackID = pFeedback->FeedbackID;
	m_Desc = pFeedback->Desc;
	m_Feature = pFeedback->Feature;
	m_MinColor = pFeedback->MinColor;
	m_MaxColor = pFeedback->MaxColor;

	return TRUE;
}

BOOL FeedbackDB::SetAll( pFEEDBACK pFeedback )
{
	if( !pFeedback ) return FALSE;

	strncpy_s( pFeedback->FeedbackID, szIDS + 1, m_FeedbackID, _TRUNCATE );
	strncpy_s( pFeedback->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	strncpy_s( pFeedback->Feature, szDESC + 1, m_Feature, _TRUNCATE );
	pFeedback->MinColor = m_MinColor;
	pFeedback->MaxColor = m_MaxColor;

	return TRUE;
}

BOOL FeedbackDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pFEEDBACK pFeedback = _db.Find( szID, szErr );
	if( !pFeedback )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pFeedback );
}

/////////////////////////////////////////////////////////////////////////////

BOOL FeedbackDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	FEEDBACK fb;
	SetAll( &fb );
	SetAll( &fb );
	if( !AddItem( (pDBDATA)&fb, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL FeedbackDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	FEEDBACK fb;
	SetAll( &fb );
	if( !ModifyItem( (pDBDATA)&fb, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL FeedbackDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_FeedbackID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void FeedbackDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	FEEDBACK fb;
	if( GetFirstItem( (pDBDATA)&fb, szErr ) )
		GetAll( &fb );
}

/////////////////////////////////////////////////////////////////////////////

BOOL FeedbackDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	FEEDBACK fb;
	if( GetNextItem( (pDBDATA)&fb, szErr ) )
	{
		GetAll( &fb );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL FeedbackDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	FEEDBACK fb;
	if( GetPreviousItem( (pDBDATA)&fb, szErr ) )
	{
		GetAll( &fb );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void FeedbackDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void FeedbackDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void FeedbackDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

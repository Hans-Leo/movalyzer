#if !defined(AFX_SQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_SQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SQuestionnaireDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBSQuestionnaire.h"

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireDB recordset

class SQuestionnaireDB : public CTDataDB
{
public:
	SQuestionnaireDB();

// Field/Param Data
	CString	m_ExpID;
	CString	m_GrpID;
	CString	m_SubjID;
	CString	m_ID;
	CString	m_Answer;

// Implementation
public:
	BOOL Find( CString szExpID, CString szGrpID );
	BOOL Find( CString szExpID, CString szGrpID, CString szSubjID );
	BOOL Find( CString szExpID, CString szGrpID, CString szSubjID, CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL ClearQuestionnaire( CString szExpID, CString szGrpID, CString szSubjID );
	BOOL RemoveGroup( CString szGrpID );
	BOOL RemoveSubject( CString szSubjID );
	BOOL RemoveQuestion( CString szID );
	BOOL Remove();
	BOOL Remove( CString szExpID, CString szGrpID, CString szSubjID );
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
protected:
	BOOL GetAll( pSQUESTIONNAIRE );
	BOOL SetAll( pSQUESTIONNAIRE );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)

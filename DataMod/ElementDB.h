#if !defined(AFX_ELEMENTDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_ELEMENTDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ELEMENTDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBElement.h"

/////////////////////////////////////////////////////////////////////////////
// ELEMENTDB recordset

class ElementDB : public CTDataDB
{
public:
	ElementDB();

// Field/Param Data
	CString	m_ElmtID;
	CString	m_Desc;
	CString	m_Pattern;
	short	m_Shape;
	double	m_CenterX;
	double	m_CenterY;
	double	m_WidthX;
	double	m_WidthY;
	BOOL	m_IsTarget;
	double	m_ErrorX;
	double	m_ErrorY;
	short	m_Line1Size;
	short	m_Line2Size;
	short	m_Line3Size;
	DWORD	m_Line1Color;
	DWORD	m_Line2Color;
	DWORD	m_Line3Color;
	DWORD	m_Background1Color;
	DWORD	m_Background2Color;
	DWORD	m_Background3Color;
	CString	m_Text1;
	CString	m_Text2;
	CString	m_Text3;
	short	m_Text1Size;
	short	m_Text2Size;
	short	m_Text3Size;
	DWORD	m_Text1Color;
	DWORD	m_Text2Color;
	DWORD	m_Text3Color;
	LOGFONT	m_lf1;
	LOGFONT	m_lf2;
	LOGFONT	m_lf3;
	BOOL	m_IsImage;
	double	m_Start;
	double	m_Duration;
	BOOL	m_HideRightTarget;
	BOOL	m_HideWrongTarget;
	BOOL	m_HideIfAnyRightTarget;
	BOOL	m_HideIfAnyWrongTarget;
	CString	m_RightTarget;
	CString	m_WrongTarget;
	CString	m_CatID;
	BOOL	m_Animate;
	CString	m_AnimationFile;
	BOOL	m_AnimationRandom;
	BOOL	m_AnimationGenerate;
	CString	m_AnimationSubjID;
	double	m_AnimationRate;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pELEMENT );
	BOOL SetAll( pELEMENT );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ELEMENTDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)

// Feedback.h: Definition of the Feedback class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols
#include "FeedbackDB.h"

/////////////////////////////////////////////////////////////////////////////
// Feedback

class Feedback : 
	public IDispatchImpl<IFeedback, &IID_IFeedback, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Feedback,&CLSID_Feedback>,
	public NSDataObject
{
public:
	Feedback();
	~Feedback();

BEGIN_COM_MAP(Feedback)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IFeedback)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Feedback) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_FEEDBACK)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IFeedback
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(get_MaxColor)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_MaxColor)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_MinColor)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_MinColor)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Feature)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Feature)(/*[in]*/ BSTR newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	FeedbackDB*	m_pDB;
};

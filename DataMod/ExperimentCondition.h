// ExperimentCondition.h: Definition of the ExperimentCondition class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXPERIMENTCONDITION_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_EXPERIMENTCONDITION_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// ExperimentCondition

class ExpConditionDB;

class ExperimentCondition : 
	public IDispatchImpl<IExperimentCondition, &IID_IExperimentCondition, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<ExperimentCondition,&CLSID_ExperimentCondition>
{
public:
	ExperimentCondition();
	~ExperimentCondition();

BEGIN_COM_MAP(ExperimentCondition)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IExperimentCondition)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(ExperimentCondition) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_ExperimentCondition)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IExperimentCondition
public:
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(FindForExperiment)(/*[in]*/ BSTR ExpID);
	STDMETHOD(Find)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR CondID);
	STDMETHOD(get_Replications)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Replications)(/*[in]*/ short newVal);
	STDMETHOD(get_ConditionID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ConditionID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ExperimentID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ExperimentID)(/*[in]*/ BSTR newVal);
protected:
	ExpConditionDB*	m_pDB;
};

#endif // !defined(AFX_EXPERIMENTCONDITION_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_)

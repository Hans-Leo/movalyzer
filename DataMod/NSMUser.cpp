// NSMUser.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "UserDB.h"
#include "NSMUser.h"

/////////////////////////////////////////////////////////////////////////////
//

NSMUser::NSMUser() : m_pDB( NULL )
{
	m_pDB = new UserDB;
}

NSMUser::~NSMUser()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP NSMUser::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_INSMUser,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP NSMUser::get_Virgin(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->IsVirgin();

	return S_OK;
}

STDMETHODIMP NSMUser::get_UserID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_UserID.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_UserID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_UserID = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Description(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Desc.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_Description(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Desc = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_RootPath(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_RootPath.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_RootPath(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_RootPath = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_BackupPath(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_BackupPath.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_BackupPath(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_BackupPath = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Delimiter(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Delim.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_Delimiter(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Delim = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_InputType(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_InputType;

	return S_OK;
}

STDMETHODIMP NSMUser::put_InputType(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_InputType = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_TabletMode(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TabletMode;

	return S_OK;
}

STDMETHODIMP NSMUser::put_TabletMode(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_TabletMode = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_EntireTablet(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EntireTablet;

	return S_OK;
}

STDMETHODIMP NSMUser::put_EntireTablet(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_EntireTablet = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Log(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Log;

	return S_OK;
}

STDMETHODIMP NSMUser::put_Log(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Log = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Alpha(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Alpha;

	return S_OK;
}

STDMETHODIMP NSMUser::put_Alpha(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Alpha = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_TabletWidth(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TabletWidth;

	return S_OK;
}

STDMETHODIMP NSMUser::put_TabletWidth(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_TabletWidth = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_TabletHeight(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TabletHeight;

	return S_OK;
}

STDMETHODIMP NSMUser::put_TabletHeight(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_TabletHeight = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_DisplayHeight(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DisplayHeight;

	return S_OK;
}

STDMETHODIMP NSMUser::put_DisplayHeight(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DisplayHeight = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_DisplayWidth(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DisplayWidth;

	return S_OK;
}

STDMETHODIMP NSMUser::put_DisplayWidth(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DisplayWidth = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_AspectRatioWidth(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TabletWidth;

	return S_OK;
}

STDMETHODIMP NSMUser::put_AspectRatioWidth(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_TabletWidth = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_AspectRatioHeight(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TabletHeight;

	return S_OK;
}

STDMETHODIMP NSMUser::put_AspectRatioHeight(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_TabletHeight = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	m_pDB->ResetToStart();
	if( m_pDB->m_UserID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSMUser::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB ) m_pDB = new UserDB;
	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP NSMUser::get_LogUI(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_LogUI;

	return S_OK;
}

STDMETHODIMP NSMUser::put_LogUI(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_LogUI = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_LogDB(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_LogDB;

	return S_OK;
}

STDMETHODIMP NSMUser::put_LogDB(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_LogDB = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_LogProc(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_LogProc;

	return S_OK;
}

STDMETHODIMP NSMUser::put_LogProc(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_LogProc = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_LogGraph(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_LogGraph;

	return S_OK;
}

STDMETHODIMP NSMUser::put_LogGraph(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_LogGraph = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_LogTablet(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_LogTablet;

	return S_OK;
}

STDMETHODIMP NSMUser::put_LogTablet(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_LogTablet = newVal;

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

STDMETHODIMP NSMUser::get_SysPass(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szPass = m_pDB->m_Pass;
	Decode( szPass );
	*pVal = szPass.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_SysPass(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szPass = newVal;
	Decode( szPass );
	m_pDB->m_Pass = szPass;

	return S_OK;
}

STDMETHODIMP NSMUser::ResetWarningStatus()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetWarningStatus();

	return S_OK;
}
STDMETHODIMP NSMUser::SetOperationMode(BOOL ClientServerON, BSTR CSServer, BSTR CSUser, BSTR CSPassword)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB->SetOperationMode( ClientServerON, CSServer, CSUser, CSPassword ) )
		return S_OK;

	return E_FAIL;
}

STDMETHODIMP NSMUser::get_CommPort(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CommPort.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_CommPort(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_CommPort = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::put_AutoUpdate(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_AutoUpdate = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_AutoUpdate(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_AutoUpdate;

	return S_OK;
}

STDMETHODIMP NSMUser::put_Private(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Private = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Private(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Private;

	return S_OK;
}

STDMETHODIMP NSMUser::get_WinUser(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_WinUser.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_WinUser(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_WinUser = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::put_GenerateSubjectIDs(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_GenSubjID = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_GenerateSubjectIDs(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_GenSubjID;

	return S_OK;
}

STDMETHODIMP NSMUser::get_SubjectStartID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjStartID.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_SubjectStartID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjStartID = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_SubjectEndID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjEndID.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_SubjectEndID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjEndID = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_SubjectCurID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SubjCurID.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_SubjectCurID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SubjCurID = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Encrypted(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Encrypted;

	return S_OK;
}

STDMETHODIMP NSMUser::put_Encrypted(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Encrypted = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_EncryptionMethod(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EncryptionMethod;

	return S_OK;
}

STDMETHODIMP NSMUser::put_EncryptionMethod(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_EncryptionMethod = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_SiteID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SiteID.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_SiteID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SiteID = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_SiteDesc(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SiteDesc.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_SiteDesc(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SiteDesc = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::get_Signature(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Signature.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSMUser::put_Signature(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Signature = newVal;

	return S_OK;
}

STDMETHODIMP NSMUser::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*Cnt = GetRecordCount( m_pDB );
	return S_OK;
}

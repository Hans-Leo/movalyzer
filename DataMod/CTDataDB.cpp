// CTDataDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CTDataDB.h"

#include "..\CTreeLink\DBCommon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NSDataObject

ULONGLONG NSDataObject::GetRecordCount( CTDataDB* pDB )
{
	ULONGLONG val = 0;
	if( pDB ) val = pDB->GetNumRecords();
	return val;
}

/////////////////////////////////////////////////////////////////////////////
// CTDataDB

CTDataDB::~CTDataDB()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( m_fOpen )
	{
		if( !m_pDB->Close( szErr ) ) OutputMessage( szErr, LOG_DB );
		if( !m_pDB->ReleaseReference( szErr ) ) OutputMessage( szErr, LOG_DB );
	}
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::IsVirgin()
{
	return m_pDB ? m_pDB->IsVirgin() : FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void CTDataDB::SetTemp( BOOL fVal )
{
	if( m_pDB ) m_pDB->SetTemp( fVal );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::DumpRecord( CString szID, CString szFile )
{
	return m_pDB ? m_pDB->DumpRecord( szID, szFile ) : FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void CTDataDB::RemoveTemp()
{
	if( m_pDB ) m_pDB->RemoveTemp();
}

/////////////////////////////////////////////////////////////////////////////

ULONGLONG CTDataDB::GetNumRecords()
{
	if( m_pDB ) return m_pDB->GetNumRecords();
	else return 0;
}

/////////////////////////////////////////////////////////////////////////////

void CTDataDB::SetDataPath( CString szPath )
{
	if( m_pDB ) m_pDB->SetDataPath( szPath );
}

/////////////////////////////////////////////////////////////////////////////

void CTDataDB::SetBackupPath( CString szPath )
{
	if( m_pDB ) m_pDB->SetBackupPath( szPath );
}

/////////////////////////////////////////////////////////////////////////////

void CTDataDB::SetAppWindow( HWND hWnd )
{
	if( m_pDB ) m_pDB->SetAppWindow( hWnd );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::CheckOpen( CString& szErr )
{
	if( !m_pDB ) return FALSE;

	if( !m_fOpen )
	{
		szErr.GetBufferSetLength( 100 );

		if( !m_pDB->AddReference( szErr ) )
		{
			::OutputMessage( szErr, LOG_DB );
			return FALSE;
		}

		m_fOpen = m_pDB->Open( szErr );
		if( !m_fOpen )
		{
			::OutputMessage( szErr, LOG_DB );
			m_pDB->ReleaseReference( szErr );
			return FALSE;
		}
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::AddItem( DBDATA* pData, CString& szErr )
{
	if( !CheckOpen( szErr ) ) return FALSE;

	if( !m_pDB->AddItem( pData, szErr ) )
	{
		// try repairing
		CString szErrNew;
		szErrNew.GetBufferSetLength( 100 );
		if( !RepairTable( szErrNew ) ) return FALSE;
		// try reopening and re-adding
		if( !CheckOpen( szErrNew ) ) return FALSE;
		if( !m_pDB->AddItem( pData, szErrNew ) )
		{
			szErr = szErrNew;
			return FALSE;
		}
		else szErr = _T("");
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::ModifyItem( DBDATA* pData, CString& szErr )
{
	if( !CheckOpen( szErr ) ) return FALSE;

	if( !m_pDB->ModifyItem( pData, szErr ) ) return FALSE;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::GetFirstItem( DBDATA* pData, CString& szErr )
{
	if( !pData || !CheckOpen( szErr ) ) return FALSE;

	return( m_pDB->FirstItem( pData, szErr ) );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::GetNextItem( DBDATA* pData, CString& szErr )
{
	if( !pData || !CheckOpen( szErr ) ) return FALSE;

	return( m_pDB->NextItem( pData, szErr ) );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::GetPreviousItem( DBDATA* pData, CString& szErr )
{
	if( !pData || !CheckOpen( szErr ) ) return FALSE;

	return( m_pDB->PreviousItem( pData, szErr ) );
}

/////////////////////////////////////////////////////////////////////////////

BOOL CTDataDB::RepairTable( CString& szErr )
{
	return m_pDB ? m_pDB->Repair( szErr ) : FALSE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

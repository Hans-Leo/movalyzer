// GQuestionnaire.h: Definition of the GQuestionnaire class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GQUESTIONNAIRE_H__B37E09B5_2516_11D4_8B62_00104BC7E2C8__INCLUDED_)
#define AFX_GQUESTIONNAIRE_H__B37E09B5_2516_11D4_8B62_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// GQuestionnaire

class GQuestionnaireDB;

class GQuestionnaire : 
	public IDispatchImpl<IGQuestionnaire, &IID_IGQuestionnaire, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<GQuestionnaire,&CLSID_GQuestionnaire>
{
public:
	GQuestionnaire();
	~GQuestionnaire();

BEGIN_COM_MAP(GQuestionnaire)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IGQuestionnaire)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(GQuestionnaire) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_GQuestionnaire)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IGQuestionnaire
public:
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(RemoveForGroup)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(RemoveQuestion)(/*[in]*/ BSTR ID);
	STDMETHOD(RemoveGroup)(/*[in]*/ BSTR GrpID);
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(FindForGroup)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID);
	STDMETHOD(Find)(/*[in]*/ BSTR ExpID, /*[in]*/ BSTR GrpID, /*[in]*/ BSTR ID);
	STDMETHOD(get_ItemNum)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_ItemNum)(/*[in]*/ short newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_GroupID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_GroupID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ExperimentID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ExperimentID)(/*[in]*/ BSTR newVal);
protected:
	GQuestionnaireDB*	m_pDB;
};

#endif // !defined(AFX_GQUESTIONNAIRE_H__B37E09B5_2516_11D4_8B62_00104BC7E2C8__INCLUDED_)

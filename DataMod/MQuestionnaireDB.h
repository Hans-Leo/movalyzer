#if !defined(AFX_MQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_MQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MQuestionnaireDB.h : header file
//

#include "CTDataDB.h"

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireDB recordset

class MQuestionnaireDB : public CTDataDB
{
public:
	MQuestionnaireDB();

// Field/Param Data
	CString		m_ID;
	short		m_ItemNum;
	BOOL		m_Header;
	BOOL		m_Private;
	CString		m_Question;
	BOOL		m_Numeric;
	CString		m_ColHeader;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MQUESTIONNAIREDB_H__DCB05ED3_A758_11D3_8A58_000000000000__INCLUDED_)

// StimulusTarget.h: Definition of the StimulusTarget class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STIMULUSTARGET_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_STIMULUSTARGET_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// StimulusTarget

class StimTargetDB;

class StimulusTarget : 
	public IDispatchImpl<IStimulusTarget, &IID_IStimulusTarget, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<StimulusTarget,&CLSID_StimulusTarget>
{
public:
	StimulusTarget();
	~StimulusTarget();

BEGIN_COM_MAP(StimulusTarget)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IStimulusTarget)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(StimulusTarget) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_StimulusTarget)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IStimulusTarget
public:
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(get_Sequence)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Sequence)(/*[in]*/ short newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(ReleaseData)();
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(FindForStimulus)(/*[in]*/ BSTR StimID);
	STDMETHOD(FindForStimElmt)(/*[in]*/ BSTR StimID, /*[in]*/ BSTR ElmtID);
	STDMETHOD(Find)(/*[in]*/ BSTR StimID, /*[in]*/ BSTR ElmtID, /*[in]*/ short Seq);
	STDMETHOD(get_ElementID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ElementID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_StimulusID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_StimulusID)(/*[in]*/ BSTR newVal);
protected:
	StimTargetDB*	m_pDB;
};

#endif // !defined(AFX_STIMULUSTARGET_H__DCB05EED_A758_11D3_8A58_000000000000__INCLUDED_)

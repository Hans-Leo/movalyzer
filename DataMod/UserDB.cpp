// UserDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "UserDB.h"

DBUser _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UserDB

UserDB::UserDB() : CTDataDB( &_db )
{
	m_UserID = _T("");
	m_Desc = _T("");
	m_RootPath = _T("");
	m_BackupPath = _T("");
	m_Delim = _T(":");
	m_InputType = 1;
	m_TabletMode = 1;
	m_EntireTablet = TRUE;
	m_Log = FALSE;
	m_LogUI = TRUE;
	m_LogDB = TRUE;
	m_LogProc = TRUE;
	m_LogGraph = TRUE;
	m_LogTablet = TRUE;
	m_Alpha = FALSE;
	m_Pass = _T("userpass");
	m_TabletWidth = 8. * 2.54;
	m_TabletHeight = 6. * 2.54;
	m_DisplayWidth = 16. * 2.54;
	m_DisplayHeight = 12. * 2.54;
	m_AspectRatioWidth = 8. * 2.54;
	m_AspectRatioHeight = 6. * 2.54;
	m_CommPort = _T("");
	m_AutoUpdate = 1;
	m_Private = 0;
	m_WinUser = _T("");
	m_GenSubjID = TRUE;
	m_SubjStartID = _T("000");
	m_SubjEndID = _T("ZZZ");
	m_SubjCurID = m_SubjStartID;
	m_Encrypted = FALSE;
	m_EncryptionMethod = ENC_NONE;
	m_SiteID = _T("LOCAL");
	m_SiteDesc = _T("Independent Use");
	m_Signature = _T("");
}

/////////////////////////////////////////////////////////////////////////////

BOOL UserDB::GetAll( pUSER pUser )
{
	if( !pUser ) return FALSE;

	m_UserID = pUser->UserID;
	m_Desc = pUser->Desc;
	m_RootPath = pUser->RootPath;
	m_BackupPath = pUser->BackupPath;
	m_Delim = pUser->Delim;
	m_InputType = pUser->InputType;
	m_TabletMode = pUser->TabletMode;
	m_EntireTablet = pUser->EntireTablet;
	m_Log = pUser->Log;
	m_LogUI = pUser->LogUI;
	m_LogDB = pUser->LogDB;
	m_LogProc = pUser->LogProc;
	m_LogGraph = pUser->LogGraph;
	m_LogTablet = pUser->LogTablet;
	m_Alpha = pUser->Alpha;
	m_Pass = pUser->Pass;
	m_TabletHeight = pUser->TabletHeight;
	m_TabletWidth = pUser->TabletWidth;
	m_DisplayHeight = pUser->DisplayHeight;
	m_DisplayWidth = pUser->DisplayWidth;
	m_AspectRatioWidth = pUser->AspectRatioWidth;
	m_AspectRatioHeight = pUser->AspectRatioHeight;
	m_CommPort = pUser->CommPort;
	m_AutoUpdate = pUser->AutoUpdate;
	m_Private = pUser->Private;
	m_WinUser = pUser->WinUser;
	m_GenSubjID = pUser->GenSubjID;
	m_SubjStartID = pUser->SubjStartID;
	m_SubjEndID = pUser->SubjEndID;
	m_SubjCurID = pUser->SubjCurID;
	m_Encrypted = pUser->Encrypted;
	m_EncryptionMethod = pUser->EncryptionMethod;
	m_SiteID = pUser->SiteID;
	if( m_SiteID == _T("") ) m_SiteID = _T("LOCAL");
	m_SiteDesc = pUser->SiteDesc;
	if( m_SiteDesc == _T("") ) m_SiteDesc = _T("Independent Use");
	m_Signature = pUser->Signature;

	return TRUE;
}

BOOL UserDB::SetAll( pUSER pUser )
{
	if( !pUser ) return FALSE;

	strncpy_s( pUser->UserID, szIDS + 1, m_UserID, _TRUNCATE );
	strncpy_s( pUser->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	strncpy_s( pUser->RootPath, _MAX_PATH, m_RootPath, _TRUNCATE );
	strncpy_s( pUser->BackupPath, _MAX_PATH, m_BackupPath, _TRUNCATE );
	strncpy_s( pUser->Delim, szUSER_DELIM + 1, m_Delim, _TRUNCATE );
	pUser->InputType = m_InputType;
	pUser->TabletMode = m_TabletMode;
	pUser->EntireTablet = m_EntireTablet;
	pUser->Log = m_Log;
	pUser->LogUI = m_LogUI;
	pUser->LogDB = m_LogDB;
	pUser->LogProc = m_LogProc;
	pUser->LogGraph = m_LogGraph;
	pUser->LogTablet = m_LogTablet;
	pUser->Alpha = m_Alpha;
	strncpy_s( pUser->Pass, szUSER_PASS_ENC + 1, m_Pass, _TRUNCATE );
	pUser->TabletHeight = m_TabletHeight;
	pUser->TabletWidth = m_TabletWidth;
	pUser->DisplayHeight = m_DisplayHeight;
	pUser->DisplayWidth = m_DisplayWidth;
	pUser->AspectRatioWidth = m_AspectRatioWidth;
	pUser->AspectRatioHeight = m_AspectRatioHeight;
	strncpy_s( pUser->CommPort, szCOMM_PORT + 1, m_CommPort, _TRUNCATE );
	pUser->AutoUpdate = m_AutoUpdate;
	pUser->Private = m_Private;
	strncpy_s( pUser->WinUser, szDESC + 1, m_WinUser, _TRUNCATE );
	pUser->GenSubjID = m_GenSubjID;
	strncpy_s( pUser->SubjStartID, szIDS + 1, m_SubjStartID, _TRUNCATE );
	strncpy_s( pUser->SubjEndID, szIDS + 1, m_SubjEndID, _TRUNCATE );
	strncpy_s( pUser->SubjCurID, szIDS + 1, m_SubjCurID, _TRUNCATE );
	pUser->Encrypted = m_Encrypted;
	pUser->EncryptionMethod = m_EncryptionMethod;
	strncpy_s( pUser->SiteID, szCODE + 1, m_SiteID, _TRUNCATE );
	strncpy_s( pUser->SiteDesc, szDESC + 1, m_SiteDesc, _TRUNCATE );
	strncpy_s( pUser->Signature, szSIGNATURE + 1, m_Signature, _TRUNCATE );

	return TRUE;
}

BOOL UserDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pUSER pUser = _db.Find( szID, szErr );
	if( !pUser )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pUser );
}

/////////////////////////////////////////////////////////////////////////////

BOOL UserDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	USER user;
	SetAll( &user );
	if( !AddItem( (pDBDATA)&user, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL UserDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	USER user;
	SetAll( &user );
	if( !ModifyItem( (pDBDATA)&user, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL UserDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_UserID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void UserDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	USER user;
	if( GetFirstItem( (pDBDATA)&user, szErr ) )
		GetAll( &user );
}

/////////////////////////////////////////////////////////////////////////////

BOOL UserDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	USER user;
	if( GetNextItem( (pDBDATA)&user, szErr ) )
	{
		GetAll( &user );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL UserDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	USER user;
	if( GetPreviousItem( (pDBDATA)&user, szErr ) )
	{
		GetAll( &user );
		return TRUE;
	}

	return FALSE;
}

void UserDB::ResetWarningStatus()
{
	_db.ResetWarningStatus();
}

BOOL UserDB::SetOperationMode(BOOL ClientServerON, CString CSServer, CString CSUser, CString CSPassword)
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	_db.SetOperationMode( ClientServerON ? CTOP_REMOTE : CTOP_LOCAL,
						  CSServer, CSUser, CSPassword, szErr );

	return TRUE;
}

// ProcSettingDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ProcSettingDB.h"
#include "..\DataMod\DataMod.h"

DBProcSetting _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSettingDB

ProcSettingDB::ProcSettingDB() : CTDataDB( &_db )
{
	m_ExpID = _T("");
	m_Velocity = FALSE;
	m_Differentiate = FALSE;
	m_RotationBeta = 0.0;
	m_FilterFrequency = 12.0;
	m_Decimate = 1;
	m_TF_J = FALSE;
	m_TF_R = FALSE;
	m_TF_A = FALSE;
	m_TF_U = FALSE;
	m_TF_O = FALSE;
	m_SEG_F = FALSE;
	m_SEG_L = FALSE;
	m_SEG_S = FALSE;
	m_SEG_O = FALSE;
	m_SEG_M = FALSE;
	m_SEG_A = FALSE;
	m_SEG_MM = FALSE;
	m_SEG_J = FALSE;
	m_EXT_S = m_SEG_S;	// 05/28/03: GMB - we only need 1 def. to split (in seg)
	m_EXT_3 = FALSE;
	m_EXT_2	= TRUE;
	m_EXT_O = FALSE;
	m_CON_A = FALSE;
	m_CON_N = FALSE;
	m_CON_M = FALSE;
	m_CON_U = FALSE;
	m_CON_C = FALSE;
	m_CON_S = FALSE;
	m_CON_O = m_SEG_S;	// 05/28/03: GMB - we only need 1 def. to split (in seg)
	m_CON_D = FALSE;
	m_SpecifySequence = FALSE;
	m_Randomize = TRUE;
	m_RandomizeTrials = TRUE;
	m_ST_A = FALSE;
	m_ST_R = FALSE;
	m_ST_Z = FALSE;
	m_ST_T = FALSE;
	m_ST_S = FALSE;
	m_ST_D = FALSE;
	m_DiscardAfter = 0;
	m_ST_D2 = FALSE;
	m_DiscardAfter2 = 99;
	DiscardPenDownDurMin = FALSE;
	PenDownDurMin = 0.0;
	m_SamplingRate = 100;
	m_MinPenPressure = 0;
	m_DeviceResolution = 0.001;
	m_ShowInstr = FALSE;
	minleftpenup = 0.5;
	minleftsp = 0.25;
	minwidth = 0.1;
	mindownsp = 1.0;
	mintimepenup = 0.6;
	maxtimepenup = 0.01;
	absdistmin = 0.05;
	rdistmin = 0.1;
	timemin = 0.04;
	rvymin = 0.05;
	rvymin2 = 0.1;
	initialpart = 0.080;
	maxfreq = 20.;
	slanterror = PI/8.;
	spiralincreasefactor = 1.1;
	minlooparean = 0.001;
	straightcritstraight = 0.1;
	straightcritcurved = 0.01;
	DoQuestionnaire = FALSE;
// 06Aug08: GMB: Changed from 10 to 15 s to make program less "nervous"
//	TimeoutStart = 10.;
	TimeoutStart = 15.;
	TimeoutRecord = 10.;
// 06Aug08: GMB: Changed from 3 to 2 s to make program less "nervous"
//	TimeoutPenlift = 3.;
	TimeoutPenlift = 2.;
	TimeoutLatency = 0.;
	MaxRecArea = TRUE;
	ProcImmediately = TRUE;
	DisplayCharts = FALSE;
	DisplayRaw = TRUE;
	DisplayTF = FALSE;
	DisplaySeg = FALSE;
	Summarize = TRUE;
	SummarizeOnly = TRUE;
	SummarizeSelect = FALSE;
	Analyze = TRUE;
	ViewSubjExt = FALSE;
	ViewSubjCon = FALSE;
	ViewExpExt = FALSE;
	ViewExpCon = FALSE;
	TrialMode = eTM_NoneADErr;
	minreactiontime = 0.1;
	maxreactiontime = 3.0;
	m_SEG_V = FALSE;
	discardLTmin = FALSE;
	discardGTmax = FALSE;
	FFT = TRUE;
	sharpness = 1.75;
	LineSize = 2;
	EllipseSize = 2;
	BGColor = RGB( 255, 255, 255 );
	LineColor1 = RGB( 0, 0, 255 );
	LineColor2 = RGB( 0, 255, 0 );
	LineColor3 = RGB( 255, 0, 0 );
	EllipseColor = RGB( 255, 0, 0 );
	MPPColor = ::GetSoftColor();
	VaryLineThickness = FALSE;
	MaxLineThickness = 20;
	DisCorrection = 0;
	DisFactor = 0.2;
	DisZInsertPoint = -1.0;
	DisRelError = 20.0;
	DisAbsError = 100.0;
	RandWithRules = FALSE;
	RandRules = _T("");
	RandBlockCounts = _T("");
	RandSelChars = _T("");
	RandSelTrials = _T("");
	upsamplefactor = 1;
	QuestAtEnd = FALSE;
	FullScreen = FALSE;
	CollapseUDStrokes = FALSE;
	CollapseStrokes = FALSE;
	CollapseTrials = FALSE;
	CollapseMedian = FALSE;
	UseTilt = FALSE;
	MaxPenPressure = 1023 - 1;
	DisNotify = FALSE;
	DisError = 0.3;
	SmoothingIter = 0;
	InkThreshold = 0;
	ImgResolution = 0;
	ExtTypeRaw = 0;
	ExtScriptRaw = _T("");
	ExtTypeTF = 0;
	ExtScriptTF = _T("");
	ExtTypeSeg = 0;
	ExtScriptSeg = _T("");
	ExtTypeExt = 0;
	ExtScriptExt = _T("");
	CondSeq = _T("");
	remtraillift = TRUE;
	SummarizeOnlyGroup = TRUE;
	SummarizeNormDB = TRUE;
	NormDBCalcScore = TRUE;
	NormDBCalcScoreGlobal = TRUE;
	NormDBNoUpdate = FALSE;
	NormDBThreshold = 1.;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ProcSettingDB::GetAll( pPROCSETTING pPS )
{
	if( !pPS ) return FALSE;

	m_ExpID = pPS->ExpID;
	m_Velocity = pPS->Velocity;
	m_Differentiate = pPS->Differentiate;
	m_RotationBeta = pPS->RotationBeta;
	m_FilterFrequency = pPS->FilterFrequency;
	m_Decimate = pPS->Decimate;
	m_TF_J = pPS->TF_J;
	m_TF_R = pPS->TF_R;
	m_TF_A = pPS->TF_A;
	m_TF_U = pPS->TF_U;
	m_TF_O = pPS->TF_O;
	m_SEG_F = pPS->SEG_F;
	m_SEG_L = pPS->SEG_L;
	m_SEG_S = pPS->SEG_S;
	m_SEG_O = pPS->SEG_O;
	m_SEG_M = pPS->SEG_M;
	m_SEG_A = pPS->SEG_A;
	m_SEG_MM = pPS->SEG_MM;
	m_SEG_J = pPS->SEG_J;
	m_EXT_S = pPS->SEG_S;	// 05/28/03: GMB - we only need 1 def. to split (in seg)
	m_EXT_3 = pPS->EXT_3;
	m_EXT_2	= pPS->EXT_2;
	m_EXT_O = pPS->EXT_O;
	m_CON_A = pPS->CON_A;
	m_CON_N = pPS->CON_N;
	m_CON_M = pPS->CON_M;
	m_CON_U = pPS->CON_U;
	m_CON_C = pPS->CON_C;
	m_CON_S = pPS->CON_S;
	m_CON_O = pPS->SEG_S;	// 05/28/03: GMB - we only need 1 def. to split (in seg)
	m_CON_D = pPS->CON_D;
	m_SpecifySequence = pPS->SpecifySequence;
	m_Randomize = pPS->Randomize;
	m_RandomizeTrials = pPS->RandomizeTrials;
	m_ST_A = pPS->ST_A;
	m_ST_R = pPS->ST_R;
	m_ST_Z = pPS->ST_Z;
	m_ST_T = pPS->ST_T;
	m_ST_S = pPS->ST_S;
	m_ST_D = pPS->ST_D;
	m_DiscardAfter = pPS->DiscardAfter;
	m_ST_D2 = pPS->ST_D2;
	m_DiscardAfter2 = pPS->DiscardAfter2;
	DiscardPenDownDurMin = pPS->DiscardPenDownDurMin;
	PenDownDurMin = pPS->PenDownDurMin;
	m_SamplingRate = pPS->SamplingRate;
	m_MinPenPressure = pPS->MinPenPressure;
	m_DeviceResolution = pPS->DeviceResolution;
	m_ShowInstr = pPS->ShowInstr;
	minleftpenup = pPS->minleftpenup;
	minleftsp = pPS->minleftsp;
	minwidth = pPS->minwidth;
	mindownsp = pPS->mindownsp;
	mintimepenup = pPS->mintimepenup;
	maxtimepenup = pPS->maxtimepenup;
	absdistmin = pPS->absdistmin;
	rdistmin = pPS->rdistmin;
	timemin = pPS->timemin;
	rvymin = pPS->rvymin;
	rvymin2 = pPS->rvymin2;
	initialpart = pPS->initialpart;
	maxfreq = pPS->maxfreq;
	slanterror = pPS->slanterror;
	spiralincreasefactor = pPS->spiralincreasefactor;
	minlooparean = pPS->minlooparean;
	straightcritstraight = pPS->straightcritstraight;
	straightcritcurved = pPS->straightcritcurved;
	DoQuestionnaire = pPS->DoQuestionnaire;
	TimeoutStart = pPS->TimeoutStart;
	TimeoutRecord = pPS->TimeoutRecord;
	TimeoutPenlift = pPS->TimeoutPenlift;
	TimeoutLatency = pPS->TimeoutLatency;
	MaxRecArea = pPS->MaxRecArea;
	ProcImmediately = pPS->ProcImmediately;
	DisplayCharts = pPS->DisplayCharts;
	DisplayRaw = pPS->DisplayRaw;
	DisplayTF = pPS->DisplayTF;
	DisplaySeg = pPS->DisplaySeg;
	Summarize = pPS->Summarize;
	SummarizeOnly = pPS->SummarizeOnly;
	SummarizeSelect = pPS->SummarizeSelect;
	Analyze = pPS->Analyze;
	ViewSubjExt = pPS->ViewSubjExt;
	ViewSubjCon = pPS->ViewSubjCon;
	ViewExpExt = pPS->ViewExpExt;
	ViewExpCon = pPS->ViewExpCon;
	TrialMode = pPS->TrialMode;
	minreactiontime = pPS->minreactiontime;
	maxreactiontime = pPS->maxreactiontime;
	m_SEG_V = pPS->SEG_V;
	discardLTmin = pPS->discardLTmin;
	discardGTmax = pPS->discardGTmax;
	FFT = pPS->FFT;
	sharpness = pPS->sharpness;
	LineSize = pPS->LineSize;
	EllipseSize = pPS->EllipseSize;
	BGColor = pPS->BGColor;
	LineColor1 = pPS->LineColor1;
	LineColor2 = pPS->LineColor2;
	LineColor3 = pPS->LineColor3;
	EllipseColor = pPS->EllipseColor;
	MPPColor = pPS->MPPColor;
	VaryLineThickness = pPS->VaryLineThickness;
	MaxLineThickness = pPS->MaxLineThickness;
	DisCorrection = pPS->DisCorrection;
	DisFactor = pPS->DisFactor;
	DisZInsertPoint = pPS->DisZInsertPoint;
	DisRelError = pPS->DisRelError;
	DisAbsError = pPS->DisAbsError;
	RandWithRules = pPS->RandWithRules;
	RandRules = pPS->RandRules;
	RandBlockCounts = pPS->RandBlockCounts;
	RandSelChars = pPS->RandSelChars;
	RandSelTrials = pPS->RandSelTrials;
	upsamplefactor = pPS->upsamplefactor;
	if( upsamplefactor == 0 ) upsamplefactor = 1;
	QuestAtEnd = pPS->QuestAtEnd;
	FullScreen = pPS->FullScreen;
	CollapseUDStrokes = pPS->CollapseUDStrokes;
	CollapseStrokes = pPS->CollapseStrokes;
	CollapseTrials = pPS->CollapseTrials;
	CollapseMedian = pPS->CollapseMedian;
	UseTilt = pPS->UseTilt;
	MaxPenPressure = pPS->MaxPenPressure;
	DisNotify = pPS->DisNotify;
	DisError = pPS->DisError;
	SmoothingIter = pPS->SmoothingIter;
	InkThreshold = pPS->InkThreshold;
	ImgResolution = pPS->ImgResolution;
	ExtTypeRaw = pPS->ExtTypeRaw;
	ExtScriptRaw = pPS->ExtScriptRaw;
	ExtTypeTF = pPS->ExtTypeTF;
	ExtScriptTF = pPS->ExtScriptTF;
	ExtTypeSeg = pPS->ExtTypeSeg;
	ExtScriptSeg = pPS->ExtScriptSeg;
	ExtTypeExt = pPS->ExtTypeExt;
	ExtScriptExt = pPS->ExtScriptExt;
	CondSeq = pPS->CondSeq;
	remtraillift = pPS->remtraillift;
	SummarizeOnlyGroup = pPS->SummarizeOnlyGroup;
	SummarizeNormDB = pPS->SummarizeNormDB;
	NormDBCalcScore = pPS->NormDBCalcScore;
	NormDBCalcScoreGlobal = pPS->NormDBCalcScoreGlobal;
	NormDBNoUpdate = pPS->NormDBNoUpdate;
	NormDBThreshold = pPS->NormDBThreshold;

	return TRUE;
}

BOOL ProcSettingDB::SetAll( pPROCSETTING pPS )
{
	if( !pPS ) return FALSE;

	strncpy_s( pPS->ExpID, szIDS + 1, m_ExpID, _TRUNCATE );
	pPS->Velocity = m_Velocity;
	pPS->Differentiate = m_Differentiate;
	pPS->RotationBeta = m_RotationBeta;
	pPS->FilterFrequency = m_FilterFrequency;
	pPS->Decimate = m_Decimate;
	pPS->TF_J = m_TF_J;
	pPS->TF_R = m_TF_R;
	pPS->TF_A = m_TF_A;
	pPS->TF_U = m_TF_U;
	pPS->TF_O = m_TF_O;
	pPS->SEG_F = m_SEG_F;
	pPS->SEG_L = m_SEG_L;
	pPS->SEG_S = m_SEG_S;
	pPS->SEG_O = m_SEG_O;
	pPS->SEG_M = m_SEG_M;
	pPS->SEG_A = m_SEG_A;
	pPS->SEG_MM = m_SEG_MM;
	pPS->SEG_J = m_SEG_J;
	pPS->EXT_S = m_SEG_S;	// 05/28/03: GMB - we only need 1 def. to split (in seg)
	pPS->EXT_3 = m_EXT_3;
	pPS->EXT_2 = m_EXT_2;
	pPS->EXT_O = m_EXT_O;
	pPS->CON_A = m_CON_A;
	pPS->CON_N = m_CON_N;
	pPS->CON_M = m_CON_M;
	pPS->CON_U = m_CON_U;
	pPS->CON_C = m_CON_C;
	pPS->CON_S = m_CON_S;
	pPS->CON_O = m_SEG_S;	// 05/28/03: GMB - we only need 1 def. to split (in seg)
	pPS->CON_D = m_CON_D;
	pPS->SpecifySequence = m_SpecifySequence;
	pPS->Randomize = m_Randomize;
	pPS->RandomizeTrials = m_RandomizeTrials;
	pPS->ST_A = m_ST_A;
	pPS->ST_R = m_ST_R;
	pPS->ST_Z = m_ST_Z;
	pPS->ST_T = m_ST_T;
	pPS->ST_S = m_ST_S;
	pPS->ST_D = m_ST_D;
	pPS->DiscardAfter = m_DiscardAfter;
	pPS->ST_D2 = m_ST_D2;
	pPS->DiscardAfter2 = m_DiscardAfter2;
	pPS->DiscardPenDownDurMin = DiscardPenDownDurMin;
	pPS->PenDownDurMin = PenDownDurMin;
	pPS->SamplingRate = m_SamplingRate;
	pPS->MinPenPressure = m_MinPenPressure;
	pPS->DeviceResolution = m_DeviceResolution;
	pPS->ShowInstr = m_ShowInstr;
	pPS->minleftpenup = minleftpenup;
	pPS->minleftsp = minleftsp;
	pPS->minwidth = minwidth;
	pPS->mindownsp = mindownsp;
	pPS->mintimepenup = mintimepenup;
	pPS->maxtimepenup = maxtimepenup;
	pPS->absdistmin = absdistmin;
	pPS->rdistmin = rdistmin;
	pPS->timemin = timemin;
	pPS->rvymin = rvymin;
	pPS->rvymin2 = rvymin2;
	pPS->initialpart = initialpart;
	pPS->maxfreq = maxfreq;
	pPS->slanterror = slanterror;
	pPS->spiralincreasefactor = spiralincreasefactor;
	pPS->minlooparean = minlooparean;
	pPS->straightcritstraight = straightcritstraight;
	pPS->straightcritcurved = straightcritcurved;
	pPS->DoQuestionnaire = DoQuestionnaire;
	pPS->TimeoutStart = TimeoutStart;
	pPS->TimeoutRecord = TimeoutRecord;
	pPS->TimeoutPenlift = TimeoutPenlift;
	pPS->TimeoutLatency = TimeoutLatency;
	pPS->MaxRecArea = MaxRecArea;
	pPS->ProcImmediately = ProcImmediately;
	pPS->DisplayCharts = DisplayCharts;
	pPS->DisplayRaw = DisplayRaw;
	pPS->DisplayTF = DisplayTF;
	pPS->DisplaySeg = DisplaySeg;
	pPS->Summarize = Summarize;
	pPS->SummarizeOnly = SummarizeOnly;
	pPS->SummarizeSelect = SummarizeSelect;
	pPS->Analyze = Analyze;
	pPS->ViewSubjExt = ViewSubjExt;
	pPS->ViewSubjCon = ViewSubjCon;
	pPS->ViewExpExt = ViewExpExt;
	pPS->ViewExpCon = ViewExpCon;
	pPS->TrialMode = TrialMode;
	pPS->minreactiontime = minreactiontime;
	pPS->maxreactiontime = maxreactiontime;
	pPS->SEG_V = m_SEG_V;
	pPS->discardLTmin = discardLTmin;
	pPS->discardGTmax = discardGTmax;
	pPS->FFT = FFT;
	pPS->sharpness = sharpness;
	pPS->LineSize = LineSize;
	pPS->EllipseSize = EllipseSize;
	pPS->BGColor = BGColor;
	pPS->LineColor1 = LineColor1;
	pPS->LineColor2 = LineColor2;
	pPS->LineColor3 = LineColor3;
	pPS->EllipseColor = EllipseColor;
	pPS->MPPColor = MPPColor;
	pPS->VaryLineThickness = VaryLineThickness;
	pPS->MaxLineThickness = MaxLineThickness;
	pPS->DisCorrection = DisCorrection;
	pPS->DisFactor = DisFactor;
	pPS->DisZInsertPoint = DisZInsertPoint;
	pPS->DisRelError = DisRelError;
	pPS->DisAbsError = DisAbsError;
	pPS->RandWithRules = RandWithRules;
	strncpy_s( pPS->RandRules, szRULES + 1, RandRules, _TRUNCATE );
	strncpy_s( pPS->RandBlockCounts, szRULES + 1, RandBlockCounts, _TRUNCATE );
	strncpy_s( pPS->RandSelChars, szRULES + 1, RandSelChars, _TRUNCATE );
	strncpy_s( pPS->RandSelTrials, szTRIALS + 1, RandSelTrials, _TRUNCATE );
	pPS->upsamplefactor = upsamplefactor;
	if( pPS->upsamplefactor == 0 ) pPS->upsamplefactor = 1;
	pPS->QuestAtEnd = QuestAtEnd;
	pPS->FullScreen = FullScreen;
	pPS->CollapseUDStrokes = CollapseUDStrokes;
	pPS->CollapseStrokes = CollapseStrokes;
	pPS->CollapseTrials = CollapseTrials;
	pPS->CollapseMedian = CollapseMedian;
	pPS->UseTilt = UseTilt;
	pPS->MaxPenPressure = MaxPenPressure;
	pPS->DisNotify = DisNotify;
	pPS->DisError = DisError;
	pPS->SmoothingIter = SmoothingIter;
	pPS->InkThreshold = InkThreshold;
	pPS->ImgResolution = ImgResolution;
	pPS->ExtTypeRaw = ExtTypeRaw;
	strncpy_s( pPS->ExtScriptRaw, szFILELENGTH + 1, ExtScriptRaw, _TRUNCATE );
	pPS->ExtTypeTF = ExtTypeTF;
	strncpy_s( pPS->ExtScriptTF, szFILELENGTH + 1, ExtScriptTF, _TRUNCATE );
	pPS->ExtTypeSeg = ExtTypeSeg;
	strncpy_s( pPS->ExtScriptSeg, szFILELENGTH + 1, ExtScriptSeg, _TRUNCATE );
	pPS->ExtTypeExt = ExtTypeExt;
	strncpy_s( pPS->ExtScriptExt, szFILELENGTH + 1, ExtScriptExt, _TRUNCATE );
	strncpy_s( pPS->CondSeq, szTRIALS + 1, CondSeq, _TRUNCATE );
	pPS->remtraillift = remtraillift;
	pPS->SummarizeOnlyGroup = SummarizeOnlyGroup;
	pPS->SummarizeNormDB = SummarizeNormDB;
	pPS->NormDBCalcScore = NormDBCalcScore;
	pPS->NormDBCalcScoreGlobal = NormDBCalcScoreGlobal;
	pPS->NormDBNoUpdate = NormDBNoUpdate;
	pPS->NormDBThreshold = NormDBThreshold;

	return TRUE;
}

BOOL ProcSettingDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pPROCSETTING pPS = _db.Find( szID, szErr );
	if( !pPS )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pPS );
}

BOOL ProcSettingDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	PROCSETTING ps;
	SetAll( &ps );
	if( !AddItem( (pDBDATA)&ps, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ProcSettingDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	PROCSETTING ps;
	SetAll( &ps );
	if( !ModifyItem( (pDBDATA)&ps, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ProcSettingDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_ExpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void ProcSettingDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	PROCSETTING ps;
	if( GetFirstItem( (pDBDATA)&ps, szErr ) )
		GetAll( &ps );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ProcSettingDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	PROCSETTING ps;
	if( GetNextItem( (pDBDATA)&ps, szErr ) )
	{
		GetAll( &ps );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ProcSettingDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	PROCSETTING ps;
	if( GetPreviousItem( (pDBDATA)&ps, szErr ) )
	{
		GetAll( &ps );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ProcSettingDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ProcSettingDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ProcSettingDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

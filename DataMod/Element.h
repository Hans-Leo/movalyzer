// Element.h: Definition of the Element class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols
#include "ElementDB.h"

/////////////////////////////////////////////////////////////////////////////
// Element

class Element : 
	public IDispatchImpl<IElement, &IID_IElement, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Element,&CLSID_Element>,
	public NSDataObject
{
public:
	Element();
	~Element();

BEGIN_COM_MAP(Element)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IElement)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Element) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Element)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IElement
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(get_AnimationRate)(double* pVal);
	STDMETHOD(put_AnimationRate)(double newVal);
	STDMETHOD(get_Start)(DOUBLE* pVal);
	STDMETHOD(put_Start)(DOUBLE newVal);
	STDMETHOD(get_Duration)(DOUBLE* pVal);
	STDMETHOD(put_Duration)(DOUBLE newVal);
	STDMETHOD(get_HideRightTarget)(BOOL* pVal);
	STDMETHOD(put_HideRightTarget)(BOOL newVal);
	STDMETHOD(get_HideWrongTarget)(BOOL* pVal);
	STDMETHOD(put_HideWrongTarget)(BOOL newVal);
	STDMETHOD(get_HideIfAnyRightTarget)(BOOL* pVal);
	STDMETHOD(put_HideIfAnyRightTarget)(BOOL newVal);
	STDMETHOD(get_HideIfAnyWrongTarget)(BOOL* pVal);
	STDMETHOD(put_HideIfAnyWrongTarget)(BOOL newVal);
	STDMETHOD(get_RightTarget)(BSTR* pVal);
	STDMETHOD(put_RightTarget)(BSTR newVal);
	STDMETHOD(get_WrongTarget)(BSTR* pVal);
	STDMETHOD(put_WrongTarget)(BSTR newVal);
	STDMETHOD(get_CatID)(BSTR* pVal);
	STDMETHOD(put_CatID)(BSTR newVal);
	STDMETHOD(get_Animate)(BOOL* pVal);
	STDMETHOD(put_Animate)(BOOL newVal);
	STDMETHOD(get_AnimationFile)(BSTR* pVal);
	STDMETHOD(put_AnimationFile)(BSTR newVal);
	STDMETHOD(get_AnimationRandom)(BOOL* pVal);
	STDMETHOD(put_AnimationRandom)(BOOL newVal);
	STDMETHOD(get_AnimationGenerate)(BOOL* pVal);
	STDMETHOD(put_AnimationGenerate)(BOOL newVal);
	STDMETHOD(get_AnimationSubjID)(BSTR* pVal);
	STDMETHOD(put_AnimationSubjID)(BSTR newVal);
	STDMETHOD(get_IsImage)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_IsImage)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Font3)(/*[out, retval]*/ VARIANT* *pVal);
	STDMETHOD(put_Font3)(/*[in]*/ VARIANT *newVal);
	STDMETHOD(get_Font2)(/*[out, retval]*/ VARIANT* *pVal);
	STDMETHOD(put_Font2)(/*[in]*/ VARIANT *newVal);
	STDMETHOD(get_Font1)(/*[out, retval]*/ VARIANT* *pVal);
	STDMETHOD(put_Font1)(/*[in]*/ VARIANT *newVal);
	STDMETHOD(get_ErrorY)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_ErrorY)(/*[in]*/ double newVal);
	STDMETHOD(get_ErrorX)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_ErrorX)(/*[in]*/ double newVal);
	STDMETHOD(get_Text3Size)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Text3Size)(/*[in]*/ short newVal);
	STDMETHOD(get_Text3Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Text3Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Text3)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text3)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Background3Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Background3Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Line3Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Line3Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Line3Size)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Line3Size)(/*[in]*/ short newVal);
	STDMETHOD(get_Text2Size)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Text2Size)(/*[in]*/ short newVal);
	STDMETHOD(get_Text2Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Text2Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Text2)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text2)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Background2Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Background2Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Line2Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Line2Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Line2Size)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Line2Size)(/*[in]*/ short newVal);
	STDMETHOD(get_IsTarget)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_IsTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Text1Size)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Text1Size)(/*[in]*/ short newVal);
	STDMETHOD(get_Text1Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Text1Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Text1)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text1)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Background1Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Background1Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Line1Color)(/*[out, retval]*/ DWORD *pVal);
	STDMETHOD(put_Line1Color)(/*[in]*/ DWORD newVal);
	STDMETHOD(get_Line1Size)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Line1Size)(/*[in]*/ short newVal);
	STDMETHOD(get_WidthY)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_WidthY)(/*[in]*/ double newVal);
	STDMETHOD(get_WidthX)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_WidthX)(/*[in]*/ double newVal);
	STDMETHOD(get_CenterY)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_CenterY)(/*[in]*/ double newVal);
	STDMETHOD(get_CenterX)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_CenterX)(/*[in]*/ double newVal);
	STDMETHOD(get_Shape)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Shape)(/*[in]*/ short newVal);
	STDMETHOD(get_Pattern)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Pattern)(/*[in]*/ BSTR newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	ElementDB*	m_pDB;
};

// StimTargetDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "StimTargetDB.h"

DBStimulusTarget _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StimTargetDB

StimTargetDB::StimTargetDB() : CTDataDB(&_db)
{
	//{{AFX_FIELD_INIT(StimTargetDB)
	m_StimID = _T("");
	m_ElmtID = _T("");
	m_Sequence = 0;
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::GetAll( pSTIMULUSTARGET pST )
{
	if( !pST ) return FALSE;

	m_StimID = pST->StimID;
	m_ElmtID = pST->ElmtID;
	m_Sequence = atoi( pST->Sequence );

	return TRUE;
}

BOOL StimTargetDB::SetAll( pSTIMULUSTARGET pST )
{
	if( !pST ) return FALSE;

	strncpy_s( pST->StimID, szIDS + 1, m_StimID, _TRUNCATE );
	strncpy_s( pST->ElmtID, szIDS + 1, m_ElmtID, _TRUNCATE );
	if( m_Sequence < 10 ) sprintf_s( pST->Sequence, _T("00%d"), m_Sequence );
	else if( m_Sequence < 100 ) sprintf_s( pST->Sequence, _T("0%d"), m_Sequence );
//	else sprintf_s( pST->Sequence, _T("%d"), m_Sequence );

	return TRUE;
}

BOOL StimTargetDB::Find( CString szStimID, CString szElmtID, short Seq )
{
	CString szErr, szSeq;
	szErr.GetBufferSetLength( 100 );
	szSeq.GetBufferSetLength( 4 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( Seq < 10 ) szSeq.Format( _T("00%d"), Seq );
	else if( Seq < 100 ) szSeq.Format( _T("0%d"), Seq );
	else szSeq.Format( _T("%d"), Seq );

	pSTIMULUSTARGET pST = _db.Find( szStimID, szElmtID, szSeq, szErr );
	if( !pST )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pST );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::Find( CString szStimID, CString szElmtID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSTIMULUSTARGET pST = _db.Find( szStimID, szElmtID, szErr );
	if( !pST )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pST );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::Find( CString szStimID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSTIMULUSTARGET pST = _db.Find( szStimID, szErr );
	if( !pST )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pST );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSTARGET st;
	SetAll( &st );
	if( !AddItem( (pDBDATA)&st, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSTARGET st;
	SetAll( &st );
	if( !ModifyItem( (pDBDATA)&st, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	CString szSeq;
	if( m_Sequence < 10 ) szSeq.Format( _T("00%d"), m_Sequence );
	else if( m_Sequence < 100 ) szSeq.Format( _T("0%d"), m_Sequence );
	else szSeq.Format( _T("%d"), m_Sequence );
	if( !_db.Remove( m_StimID, m_ElmtID, szSeq, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void StimTargetDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSTARGET st;
	if( GetFirstItem( (pDBDATA)&st, szErr ) )
		GetAll( &st );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSTARGET st;
	if( GetNextItem( (pDBDATA)&st, szErr ) )
	{
		GetAll( &st );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimTargetDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUSTARGET st;
	if( GetPreviousItem( (pDBDATA)&st, szErr ) )
	{
		GetAll( &st );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimTargetDB::ReleaseDB()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !_db.Close( szErr ) )
		OutputMessage( szErr, LOG_DB );
}

/////////////////////////////////////////////////////////////////////////////

void StimTargetDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimTargetDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimTargetDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

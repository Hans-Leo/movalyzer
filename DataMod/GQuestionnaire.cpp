// GQuestionnaire.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "GQuestionnaireDB.h"
#include "GQuestionnaire.h"

/////////////////////////////////////////////////////////////////////////////
//

GQuestionnaire::GQuestionnaire()  : m_pDB( NULL )
{
	m_pDB = new GQuestionnaireDB;
}

GQuestionnaire::~GQuestionnaire()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP GQuestionnaire::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IGQuestionnaire,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP GQuestionnaire::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP GQuestionnaire::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP GQuestionnaire::get_ExperimentID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP GQuestionnaire::put_ExperimentID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpID = newVal;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::get_GroupID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_GrpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP GQuestionnaire::put_GroupID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_GrpID = newVal;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ID.AllocSysString();

	return S_OK;
}

STDMETHODIMP GQuestionnaire::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ID = newVal;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::get_ItemNum(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ItemNum;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::put_ItemNum(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ItemNum = newVal;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::Find(BSTR ExpID, BSTR GrpID, BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString	szExpID = ExpID;
	CString szGrpID = GrpID;
	CString szID = ID;
	if( !m_pDB->Find( szExpID, szGrpID, szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::FindForGroup(BSTR ExpID, BSTR GrpID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString	szExpID = ExpID;
	CString szGrpID = GrpID;
	if( !m_pDB->Find( szExpID, szGrpID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::RemoveGroup( BSTR GrpID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szGrpID = GrpID;
	if( !m_pDB->RemoveGroup( szGrpID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::RemoveQuestion( BSTR ID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->RemoveQuestion( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP GQuestionnaire::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP GQuestionnaire::RemoveForGroup( BSTR ExpID, BSTR GrpID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szExpID = ExpID;
	CString szGrpID = GrpID;
	if( !m_pDB->Remove( szExpID, szGrpID ) ) return E_FAIL;

	return S_OK;
}

// Subject.h: Definition of the Subject class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SUBJECT_H__DCB05EDE_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_SUBJECT_H__DCB05EDE_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "SubjectDB.h"

/////////////////////////////////////////////////////////////////////////////
// Subject

class Subject : 
	public IDispatchImpl<ISubject, &IID_ISubject, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Subject,&CLSID_Subject>,
	public NSDataObject
{
public:
	Subject();
	~Subject();

BEGIN_COM_MAP(Subject)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISubject)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Subject) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Subject)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// ISubject
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(get_Password)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Password)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Signature)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Signature)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SiteDesc)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SiteDesc)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SiteID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SiteID)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_EncryptionMethod)(short *pVal);
	STDMETHOD(put_EncryptionMethod)(short newVal);
	STDMETHOD(get_Code)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Code)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Encrypted)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Encrypted)(/*[in]*/ BOOL newVal);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(get_ExperimentCount)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_ExperimentCount)(/*[in]*/ short newVal);
	STDMETHOD(get_Active)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Active)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DefaultExperiment)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_DefaultExperiment)(/*[in]*/ BSTR newVal);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_PrvNotes)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_PrvNotes)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_DateAdded)(/*[out, retval]*/ DATE *pVal);
	STDMETHOD(put_DateAdded)(/*[in]*/ DATE newVal);
	STDMETHOD(get_Notes)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Notes)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_FirstName)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_FirstName)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_LastName)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_LastName)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	SubjectDB*	m_pDB;
};

#endif // !defined(AFX_SUBJECT_H__DCB05EDE_A758_11D3_8A58_000000000000__INCLUDED_)

// Element.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "ElementDB.h"
#include "Element.h"

/////////////////////////////////////////////////////////////////////////////
//

Element::Element()  : m_pDB( NULL )
{
	m_pDB = new ElementDB;
}

Element::~Element()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP Element::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IElement,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP Element::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP Element::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP Element::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ElmtID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ElmtID = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Description(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Desc.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_Description(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Desc = newVal;

	return S_OK;
}

STDMETHODIMP Element::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ElmtID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Element::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP Element::get_Pattern(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Pattern.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_Pattern(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Pattern = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Shape(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Shape;

	return S_OK;
}

STDMETHODIMP Element::put_Shape(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Shape = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_CenterX(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CenterX;

	return S_OK;
}

STDMETHODIMP Element::put_CenterX(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_CenterX = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_CenterY(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CenterY;

	return S_OK;
}

STDMETHODIMP Element::put_CenterY(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_CenterY = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_WidthX(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_WidthX;

	return S_OK;
}

STDMETHODIMP Element::put_WidthX(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_WidthX = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_WidthY(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_WidthY;

	return S_OK;
}

STDMETHODIMP Element::put_WidthY(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_WidthY = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Line1Size(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Line1Size;

	return S_OK;
}

STDMETHODIMP Element::put_Line1Size(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Line1Size = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Line1Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Line1Color;

	return S_OK;
}

STDMETHODIMP Element::put_Line1Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Line1Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text1(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text1.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_Text1(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text1 = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text1Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text1Color;

	return S_OK;
}

STDMETHODIMP Element::put_Text1Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text1Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text1Size(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text1Size;

	return S_OK;
}

STDMETHODIMP Element::put_Text1Size(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text1Size = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_IsTarget(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_IsTarget;

	return S_OK;
}

STDMETHODIMP Element::put_IsTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_IsTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Line2Size(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Line2Size;

	return S_OK;
}

STDMETHODIMP Element::put_Line2Size(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Line2Size = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Line2Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Line2Color;

	return S_OK;
}

STDMETHODIMP Element::put_Line2Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Line2Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text2(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text2.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_Text2(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text2 = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text2Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text2Color;

	return S_OK;
}

STDMETHODIMP Element::put_Text2Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text2Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text2Size(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text2Size;

	return S_OK;
}

STDMETHODIMP Element::put_Text2Size(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text2Size = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Line3Size(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Line3Size;

	return S_OK;
}

STDMETHODIMP Element::put_Line3Size(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Line3Size = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Line3Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Line3Color;

	return S_OK;
}

STDMETHODIMP Element::put_Line3Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Line3Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text3(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text3.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_Text3(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text3 = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text3Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text3Color;

	return S_OK;
}

STDMETHODIMP Element::put_Text3Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text3Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Text3Size(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Text3Size;

	return S_OK;
}

STDMETHODIMP Element::put_Text3Size(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Text3Size = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_ErrorX(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ErrorX;

	return S_OK;
}

STDMETHODIMP Element::put_ErrorX(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ErrorX = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_ErrorY(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ErrorY;

	return S_OK;
}

STDMETHODIMP Element::put_ErrorY(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ErrorY = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Background1Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Background1Color;

	return S_OK;
}

STDMETHODIMP Element::put_Background1Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Background1Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Background2Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Background2Color;

	return S_OK;
}

STDMETHODIMP Element::put_Background2Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Background2Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Background3Color(DWORD *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Background3Color;

	return S_OK;
}

STDMETHODIMP Element::put_Background3Color(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Background3Color = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Font1(VARIANT* *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (VARIANT*)&(m_pDB->m_lf1);

	return S_OK;
}

STDMETHODIMP Element::put_Font1(VARIANT* newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	LOGFONT* plf = (LOGFONT*)newVal;
	memcpy( &(m_pDB->m_lf1), plf, sizeof( LOGFONT ) );

	return S_OK;
}

STDMETHODIMP Element::get_Font2(VARIANT* *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (VARIANT*)&(m_pDB->m_lf2);

	return S_OK;
}

STDMETHODIMP Element::put_Font2(VARIANT* newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	LOGFONT* plf = (LOGFONT*)newVal;
	memcpy( &(m_pDB->m_lf2), plf, sizeof( LOGFONT ) );

	return S_OK;
}

STDMETHODIMP Element::get_Font3(VARIANT* *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (VARIANT*)&(m_pDB->m_lf3);

	return S_OK;
}

STDMETHODIMP Element::put_Font3(VARIANT* newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	LOGFONT* plf = (LOGFONT*)newVal;
	memcpy( &(m_pDB->m_lf3), plf, sizeof( LOGFONT ) );

	return S_OK;
}

STDMETHODIMP Element::get_IsImage(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_IsImage;

	return S_OK;
}

STDMETHODIMP Element::put_IsImage(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_IsImage = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Start(DOUBLE* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_Start;

	return S_OK;
}

STDMETHODIMP Element::put_Start(DOUBLE newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_Start = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Duration(DOUBLE* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_Duration;

	return S_OK;
}

STDMETHODIMP Element::put_Duration(DOUBLE newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_Duration = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_HideRightTarget(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_HideRightTarget;

	return S_OK;
}

STDMETHODIMP Element::put_HideRightTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_HideRightTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_HideWrongTarget(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_HideWrongTarget;

	return S_OK;
}

STDMETHODIMP Element::put_HideWrongTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_HideWrongTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_HideIfAnyRightTarget(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_HideIfAnyRightTarget;

	return S_OK;
}

STDMETHODIMP Element::put_HideIfAnyRightTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_HideIfAnyRightTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_HideIfAnyWrongTarget(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_HideIfAnyWrongTarget;

	return S_OK;
}

STDMETHODIMP Element::put_HideIfAnyWrongTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_HideIfAnyWrongTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_RightTarget(BSTR* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_RightTarget.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_RightTarget(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_RightTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_WrongTarget(BSTR* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_WrongTarget.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_WrongTarget(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_WrongTarget = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_CatID(BSTR* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_CatID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_CatID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_CatID = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_Animate(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_Animate;

	return S_OK;
}

STDMETHODIMP Element::put_Animate(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_Animate = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_AnimationFile(BSTR* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_AnimationFile.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_AnimationFile(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_AnimationFile = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_AnimationRandom(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_AnimationRandom;

	return S_OK;
}

STDMETHODIMP Element::put_AnimationRandom(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_AnimationRandom = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_AnimationGenerate(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_AnimationGenerate;

	return S_OK;
}

STDMETHODIMP Element::put_AnimationGenerate(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_AnimationGenerate = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_AnimationSubjID(BSTR* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_AnimationSubjID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Element::put_AnimationSubjID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_AnimationSubjID = newVal;

	return S_OK;
}

STDMETHODIMP Element::get_AnimationRate(double* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	*pVal = m_pDB->m_AnimationRate;

	return S_OK;
}

STDMETHODIMP Element::put_AnimationRate(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_pDB->m_AnimationRate = newVal;

	return S_OK;
}

STDMETHODIMP Element::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP Element::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP Element::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP Element::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*Cnt = GetRecordCount( m_pDB );
	return S_OK;
}

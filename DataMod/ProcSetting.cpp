// ProcSetting.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "ProcSettingDB.h"
#include "ProcSetting.h"

/////////////////////////////////////////////////////////////////////////////
//

ProcSetting::ProcSetting()  : m_pDB( NULL )
{
	m_pDB = new ProcSettingDB;
}

ProcSetting::~ProcSetting()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP ProcSetting::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IProcSetting,
		&IID_IProcSetRunExp,
		&IID_IProcSetFlags,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP ProcSetting::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP ProcSetting::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExperimentID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExperimentID(BSTR newVal)
{
	m_pDB->m_ExpID = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Velocity(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Velocity;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Velocity(BOOL newVal)
{
	m_pDB->m_Velocity = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Differentiate(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Differentiate;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Differentiate(BOOL newVal)
{
	m_pDB->m_Differentiate = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_RotationBeta(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_RotationBeta;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_RotationBeta(double newVal)
{
	m_pDB->m_RotationBeta = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_FilterFrequency(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_FilterFrequency;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_FilterFrequency(double newVal)
{
	m_pDB->m_FilterFrequency = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Decimate(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Decimate;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Decimate(short newVal)
{
	m_pDB->m_Decimate = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TF_J(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TF_J;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TF_J(BOOL newVal)
{
	m_pDB->m_TF_J = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TF_R(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TF_R;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TF_R(BOOL newVal)
{
	m_pDB->m_TF_R = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TF_A(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TF_A;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TF_A(BOOL newVal)
{
	m_pDB->m_TF_A = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TF_U(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TF_U;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TF_U(BOOL newVal)
{
	m_pDB->m_TF_U = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TF_O(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_TF_O;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TF_O(BOOL newVal)
{
	m_pDB->m_TF_O = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_F(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_F;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_F(BOOL newVal)
{
	m_pDB->m_SEG_F = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_L(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_L;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_L(BOOL newVal)
{
	m_pDB->m_SEG_L = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_S(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_S;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_S(BOOL newVal)
{
	m_pDB->m_SEG_S = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_O(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_O;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_O(BOOL newVal)
{
	m_pDB->m_SEG_O = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_M(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_M;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_M(BOOL newVal)
{
	m_pDB->m_SEG_M = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_A(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_A;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_A(BOOL newVal)
{
	m_pDB->m_SEG_A = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_MM(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_MM;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_MM(BOOL newVal)
{
	m_pDB->m_SEG_MM = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SEG_J(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_J;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_J(BOOL newVal)
{
	m_pDB->m_SEG_J = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_EXT_S(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EXT_S;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_EXT_S(BOOL newVal)
{
	m_pDB->m_EXT_S = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_EXT_3(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EXT_3;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_EXT_3(BOOL newVal)
{
	m_pDB->m_EXT_3 = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_EXT_O(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EXT_O;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_EXT_O(BOOL newVal)
{
	m_pDB->m_EXT_O = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_EXT_2(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_EXT_2;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_EXT_2(BOOL newVal)
{
	m_pDB->m_EXT_2 = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_A(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_A;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_A(BOOL newVal)
{
	m_pDB->m_CON_A = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_N(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_N;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_N(BOOL newVal)
{
	m_pDB->m_CON_N = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_M(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_M;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_M(BOOL newVal)
{
	m_pDB->m_CON_M = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_U(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_U;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_U(BOOL newVal)
{
	m_pDB->m_CON_U = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_C(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_C;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_C(BOOL newVal)
{
	m_pDB->m_CON_C = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_S(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_S;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_S(BOOL newVal)
{
	m_pDB->m_CON_S = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_O(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_O;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_O(BOOL newVal)
{
	m_pDB->m_CON_O = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_CON_D(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CON_D;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_CON_D(BOOL newVal)
{
	m_pDB->m_CON_D = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SpecifySequence(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

		*pVal = m_pDB->m_SpecifySequence;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SpecifySequence(BOOL newVal)
{
	m_pDB->m_SpecifySequence = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Randomize(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Randomize;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Randomize(BOOL newVal)
{
	m_pDB->m_Randomize = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_RandomizeTrials(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_RandomizeTrials;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_RandomizeTrials(BOOL newVal)
{
	m_pDB->m_RandomizeTrials = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ExpID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_A(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_A;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_A(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_A = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_R(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_R;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_R(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_R = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_Z(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_Z;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_Z(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_Z = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_T(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_T;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_T(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_T = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_S(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_S;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_S(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_S = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_D(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_D;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_D(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_D = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DiscardAfter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DiscardAfter;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DiscardAfter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DiscardAfter = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ST_D2(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ST_D2;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ST_D2(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ST_D2 = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DiscardAfter2(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DiscardAfter2;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DiscardAfter2(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DiscardAfter2 = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SamplingRate(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SamplingRate;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SamplingRate(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SamplingRate = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinPenPressure(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_MinPenPressure;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinPenPressure(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_MinPenPressure = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DeviceResolution(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_DeviceResolution;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DeviceResolution(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_DeviceResolution = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_InstructionAlwaysVisible(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ShowInstr;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_InstructionAlwaysVisible(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ShowInstr = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinLeftPenup(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->minleftpenup;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinLeftPenup(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->minleftpenup = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinLeftSpacing(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->minleftsp;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinLeftSpacing(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->minleftsp = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinWordWidth(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->minwidth;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinWordWidth(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->minwidth = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinDownSpacing(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->mindownsp;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinDownSpacing(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->mindownsp = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinTimePenup(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->mintimepenup;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinTimePenup(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->mintimepenup = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxTimePenup(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->maxtimepenup;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxTimePenup(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->maxtimepenup = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinStrokeSize(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->absdistmin;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinStrokeSize(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->absdistmin = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinStrokeSizeRelativeToMax(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->rdistmin;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinStrokeSizeRelativeToMax(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->rdistmin = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinStrokeDuration(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->timemin;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinStrokeDuration(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->timemin = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinVertVelocity(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->rvymin;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinVertVelocity(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->rvymin = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxVertVelocity(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->rvymin2;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxVertVelocity(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->rvymin2 = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_StrokeBeginning(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->initialpart;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_StrokeBeginning(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->initialpart = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxFrequency(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->maxfreq;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxFrequency(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->maxfreq = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SlantError(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->slanterror;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SlantError(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->slanterror = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SpiralIncreaseFactor(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->spiralincreasefactor;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SpiralIncreaseFactor(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->spiralincreasefactor = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinLoopArea(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->minlooparean;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinLoopArea(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->minlooparean = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxStraightErrorStraight(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->straightcritstraight;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxStraightErrorStraight(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->straightcritstraight = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinStraightErrorCurved(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->straightcritcurved;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinStraightErrorCurved(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->straightcritcurved = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DoQuestionnaire(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->DoQuestionnaire;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DoQuestionnaire(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DoQuestionnaire = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TimeoutStart(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->TimeoutStart;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TimeoutStart(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->TimeoutStart = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TimeoutRecord(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->TimeoutRecord;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TimeoutRecord(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->TimeoutRecord = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TimeoutPenlift(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->TimeoutPenlift;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TimeoutPenlift(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->TimeoutPenlift = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TimeoutLatency(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->TimeoutLatency;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TimeoutLatency(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->TimeoutLatency = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxRecArea(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->MaxRecArea;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxRecArea(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->MaxRecArea = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ProcImmediately(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->ProcImmediately;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ProcImmediately(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ProcImmediately = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisplayCharts(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->DisplayCharts;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisplayCharts(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DisplayCharts = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisplayRaw(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->DisplayRaw;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisplayRaw(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DisplayRaw = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisplayTF(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->DisplayTF;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisplayTF(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DisplayTF = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisplaySeg(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->DisplaySeg;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisplaySeg(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DisplaySeg = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Summarize(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Summarize;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Summarize(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Summarize = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SummarizeOnly(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->SummarizeOnly;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SummarizeOnly(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SummarizeOnly = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_SummarizeSelect(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->SummarizeSelect;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SummarizeSelect(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SummarizeSelect = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Analyze(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Analyze;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Analyze(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Analyze = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ViewSubjExt(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->ViewSubjExt;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ViewSubjExt(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ViewSubjExt = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ViewSubjCon(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->ViewSubjCon;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ViewSubjCon(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ViewSubjCon = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ViewExpExt(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->ViewExpExt;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ViewExpExt(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ViewExpExt = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_ViewExpCon(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->ViewExpCon;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_ViewExpCon(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ViewExpCon = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_TrialMode(TrialsMode *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (TrialsMode)m_pDB->TrialMode;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_TrialMode(TrialsMode newVal)
{
	m_pDB->TrialMode = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP ProcSetting::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP ProcSetting::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}
STDMETHODIMP ProcSetting::get_SEG_V(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_SEG_V;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_SEG_V(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_SEG_V = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DiscardLTMin(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->discardLTmin;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DiscardLTMin(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->discardLTmin = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DiscardGTMax(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->discardGTmax;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DiscardGTMax(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->discardGTmax = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_FFT(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->FFT;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_FFT(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->FFT = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MinReactionTime(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->minreactiontime;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MinReactionTime(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->minreactiontime = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxReactionTime(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->maxreactiontime;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxReactionTime(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->maxreactiontime = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_Sharpness(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->sharpness;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_Sharpness(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->sharpness = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_VaryLineThickness(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->VaryLineThickness;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_VaryLineThickness(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->VaryLineThickness = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxLineThickness(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->MaxLineThickness;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxLineThickness(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->MaxLineThickness = newVal;

	return S_OK;
}
STDMETHODIMP ProcSetting::GetDrawingOptions( /*[out]*/ short* LineSize, /*[out]*/ short* EllipseSize,
										/*[out]*/ DWORD* BGColor, /*[out]*/ DWORD* LineColor1,
										/*[out]*/ DWORD* LineColor2, /*[out]*/ DWORD* LineColor3,
										/*[out]*/ DWORD* EllipseColor, /*[out]*/ DWORD* MPPColor )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*LineSize = m_pDB->LineSize;
	*EllipseSize = m_pDB->EllipseSize;
	*BGColor = m_pDB->BGColor;
	*LineColor1 = m_pDB->LineColor1;
	*LineColor2 = m_pDB->LineColor2;
	*LineColor3 = m_pDB->LineColor3;
	*EllipseColor = m_pDB->EllipseColor;
	*MPPColor = m_pDB->MPPColor;

	return S_OK;
}

STDMETHODIMP ProcSetting::SetDrawingOptions( /*[in]*/ short LineSize, /*[in]*/ short EllipseSize,
										/*[in]*/ DWORD BGColor, /*[in]*/ DWORD LineColor1,
										/*[in]*/ DWORD LineColor2, /*[in]*/ DWORD LineColor3,
										/*[in]*/ DWORD EllipseColor, /*[in]*/ DWORD MPPColor )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->LineSize = LineSize;
	m_pDB->EllipseSize = EllipseSize;
	m_pDB->BGColor = BGColor;
	m_pDB->LineColor1 = LineColor1;
	m_pDB->LineColor2 = LineColor2;
	m_pDB->LineColor3 = LineColor3;
	m_pDB->EllipseColor = EllipseColor;
	m_pDB->MPPColor = MPPColor;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisCorrection(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DisCorrection = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisCorrection(short* newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*newVal = m_pDB->DisCorrection;

	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisFactor(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->DisFactor = newVal;

	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisFactor(double *newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*newVal = m_pDB->DisFactor;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisZInsertPoint(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->DisZInsertPoint = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisZInsertPoint(double *newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*newVal = m_pDB->DisZInsertPoint;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisRelError(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->DisRelError = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisRelError(double *newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*newVal = m_pDB->DisRelError;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisAbsError(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->DisAbsError = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisAbsError(double *newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*newVal = m_pDB->DisAbsError;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_RandWithRules(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->RandWithRules;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_RandWithRules(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->RandWithRules = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_RandRules(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->RandRules.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_RandRules(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->RandRules = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_RandBlockCounts(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->RandBlockCounts.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_RandBlockCounts(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->RandBlockCounts = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_RandSelectionChar(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->RandSelChars.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_RandSelectionChar(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->RandSelChars = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_RandSelectionTrials(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->RandSelTrials.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_RandSelectionTrials(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->RandSelTrials = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_UpSampleFactor(short* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->upsamplefactor;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_UpSampleFactor(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->upsamplefactor = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_QuestAtEnd(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->QuestAtEnd;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_QuestAtEnd(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->QuestAtEnd = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_FullScreen(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->FullScreen;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_FullScreen(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->FullScreen = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_CollapseUDStrokes( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->CollapseUDStrokes;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_CollapseUDStrokes( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->CollapseUDStrokes = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_CollapseStrokes( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->CollapseStrokes;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_CollapseStrokes( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->CollapseStrokes = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_CollapseTrials( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->CollapseTrials;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_CollapseTrials( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->CollapseTrials = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_CollapseMedian( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->CollapseMedian;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_CollapseMedian( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->CollapseMedian = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_UseTilt( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->UseTilt;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_UseTilt( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->UseTilt = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_MaxPenPressure( short* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->MaxPenPressure;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_MaxPenPressure( short newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->MaxPenPressure = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisNotify( BOOL* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->DisNotify;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisNotify( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->DisNotify = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_DisError( double* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->DisError;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_DisError( double newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->DisError = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_SmoothingIter( short* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->SmoothingIter;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_SmoothingIter( short newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->SmoothingIter = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_InkThreshold( short* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->InkThreshold;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_InkThreshold( short newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->InkThreshold = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ImgResolution( short* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->ImgResolution;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ImgResolution( short newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ImgResolution = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_DiscardPenDownDurMin( BOOL* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->DiscardPenDownDurMin;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_DiscardPenDownDurMin( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->DiscardPenDownDurMin = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_PenDownDurMin( double* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->PenDownDurMin;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_PenDownDurMin( double newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->PenDownDurMin = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExtAppTypeRaw(ExternalAppType *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = (ExternalAppType)m_pDB->ExtTypeRaw;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppTypeRaw(ExternalAppType newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtTypeRaw = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExtAppScriptRaw(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->ExtScriptRaw.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppScriptRaw(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtScriptRaw = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExtAppTypeTF(ExternalAppType *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = (ExternalAppType)m_pDB->ExtTypeTF;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppTypeTF(ExternalAppType newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtTypeTF = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExtAppScriptTF(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->ExtScriptTF.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppScriptTF(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtScriptTF = newVal;
	return S_OK;
}
STDMETHODIMP ProcSetting::get_ExtAppTypeSeg(ExternalAppType *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = (ExternalAppType)m_pDB->ExtTypeSeg;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppTypeSeg(ExternalAppType newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtTypeSeg = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExtAppScriptSeg(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->ExtScriptSeg.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppScriptSeg(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtScriptSeg = newVal;
	return S_OK;
}
STDMETHODIMP ProcSetting::get_ExtAppTypeExt(ExternalAppType *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = (ExternalAppType)m_pDB->ExtTypeExt;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppTypeExt(ExternalAppType newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtTypeExt = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ExtAppScriptExt(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->ExtScriptExt.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ExtAppScriptExt(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->ExtScriptExt = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_ConditionSequence(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->CondSeq.AllocSysString();
	return S_OK;
}

STDMETHODIMP ProcSetting::put_ConditionSequence(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->CondSeq = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_RemoveTrailingPenlift( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->remtraillift;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_RemoveTrailingPenlift( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->remtraillift = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_SummarizeOnlyGroup( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->SummarizeOnlyGroup;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_SummarizeOnlyGroup( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->SummarizeOnlyGroup = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_SummarizeNormDB( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->SummarizeNormDB;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_SummarizeNormDB( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->SummarizeNormDB = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_NormDBCalcScore( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->NormDBCalcScore;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_NormDBCalcScore( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->NormDBCalcScore = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_NormDBCalcScoreGlobal( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->NormDBCalcScoreGlobal;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_NormDBCalcScoreGlobal( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->NormDBCalcScoreGlobal = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_NormDBNoUpdate( BOOL *pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->NormDBNoUpdate;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_NormDBNoUpdate( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->NormDBNoUpdate = newVal;
	return S_OK;
}

STDMETHODIMP ProcSetting::get_NormDBThreshold( double* pVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_pDB->NormDBThreshold;
	return S_OK;
}

STDMETHODIMP ProcSetting::put_NormDBThreshold( double newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_pDB->NormDBThreshold = newVal;
	return S_OK;
}

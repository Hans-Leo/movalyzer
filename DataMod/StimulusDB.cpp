// StimulusDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "StimulusDB.h"

DBStimulus _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// StimulusDB

StimulusDB::StimulusDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(StimulusDB)
	m_StimID = _T("");
	m_Desc = _T("");
	m_Demo = FALSE;
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimulusDB::GetAll( pSTIMULUS pStimulus )
{
	if( !pStimulus ) return FALSE;

	m_StimID = pStimulus->StimID;
	m_Desc = pStimulus->Desc;
	m_Demo = pStimulus->Demo;

	return TRUE;
}

BOOL StimulusDB::SetAll( pSTIMULUS pStimulus )
{
	if( !pStimulus ) return FALSE;

	strncpy_s( pStimulus->StimID, szIDS + 1, m_StimID, _TRUNCATE );
	strncpy_s( pStimulus->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	pStimulus->Demo = m_Demo;

	return TRUE;
}

BOOL StimulusDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pSTIMULUS pStimulus = _db.Find( szID, szErr );
	if( !pStimulus )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pStimulus );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimulusDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUS stim;
	SetAll( &stim );
	if( !AddItem( (pDBDATA)&stim, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimulusDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUS stim;
	SetAll( &stim );
	if( !ModifyItem( (pDBDATA)&stim, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimulusDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_StimID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void StimulusDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUS stim;
	if( GetFirstItem( (pDBDATA)&stim, szErr ) )
		GetAll( &stim );
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimulusDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUS stim;
	if( GetNextItem( (pDBDATA)&stim, szErr ) )
	{
		GetAll( &stim );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL StimulusDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	STIMULUS stim;
	if( GetPreviousItem( (pDBDATA)&stim, szErr ) )
	{
		GetAll( &stim );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimulusDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimulusDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void StimulusDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

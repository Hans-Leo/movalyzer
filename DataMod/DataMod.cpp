// DataMod.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f DataModps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "DataMod.h"

#include "DataMod_i.c"
#include "Experiment.h"
#include "Subject.h"
#include "Group.h"
#include "Condition.h"
#include "ProcSetting.h"
#include "ExperimentMember.h"
#include "ExperimentCondition.h"
#include "MQuestionnaire.h"
#include "GQuestionnaire.h"
#include "SQuestionnaire.h"
#include "NSMUser.h"
#include "Backup.h"
#include "Element.h"
#include "Stimulus.h"
#include "StimulusElement.h"
#include "StimulusTarget.h"
#include "Cat.h"
#include "Feedback.h"

CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_Experiment, Experiment)
OBJECT_ENTRY(CLSID_Subject, Subject)
OBJECT_ENTRY(CLSID_Group, Group)
OBJECT_ENTRY(CLSID_NSCondition, NSCondition)
OBJECT_ENTRY(CLSID_ProcSetting, ProcSetting)
OBJECT_ENTRY(CLSID_ExperimentMember, ExperimentMember)
OBJECT_ENTRY(CLSID_ExperimentCondition, ExperimentCondition)
OBJECT_ENTRY(CLSID_MQuestionnaire, MQuestionnaire)
OBJECT_ENTRY(CLSID_GQuestionnaire, GQuestionnaire)
OBJECT_ENTRY(CLSID_SQuestionnaire, SQuestionnaire)
OBJECT_ENTRY(CLSID_NSMUser, NSMUser)
OBJECT_ENTRY(CLSID_Backup, Backup)
OBJECT_ENTRY(CLSID_Element, Element)
OBJECT_ENTRY(CLSID_Stimulus, Stimulus)
OBJECT_ENTRY(CLSID_StimulusElement, StimulusElement)
OBJECT_ENTRY(CLSID_StimulusTarget, StimulusTarget)
OBJECT_ENTRY(CLSID_Cat, Cat)
OBJECT_ENTRY(CLSID_Feedback, Feedback)
END_OBJECT_MAP()

class CDataModApp : public CWinApp
{
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataModApp)
	public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDataModApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CDataModApp, CWinApp)
	//{{AFX_MSG_MAP(CDataModApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CDataModApp theApp;

BOOL CDataModApp::InitInstance()
{
    _Module.Init(ObjectMap, m_hInstance, &LIBID_DATAMODLib);
    return CWinApp::InitInstance();
}

int CDataModApp::ExitInstance()
{
    _Module.Term();
    return CWinApp::ExitInstance();
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    return (AfxDllCanUnloadNow()==S_OK && _Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}

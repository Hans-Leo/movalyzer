// ProcSetting.h: Definition of the ProcSetting class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// ProcSetting

class ProcSettingDB;

class ProcSetting : 
	public IDispatchImpl<IProcSetting, &IID_IProcSetting, &LIBID_DATAMODLib>, 
	public IDispatchImpl<IProcSetRunExp, &IID_IProcSetRunExp, &LIBID_DATAMODLib>, 
	public IDispatchImpl<IProcSetFlags, &IID_IProcSetFlags, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<ProcSetting,&CLSID_ProcSetting>
{
public:
	ProcSetting();
	~ProcSetting();

BEGIN_COM_MAP(ProcSetting)
	COM_INTERFACE_ENTRY2(IDispatch, IProcSetting)
	COM_INTERFACE_ENTRY(IProcSetting)
	COM_INTERFACE_ENTRY(IProcSetRunExp)
	COM_INTERFACE_ENTRY(IProcSetFlags)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(ProcSetting) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_ProcSetting)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IProcSetting
public:
// IProcSetRunExp
	STDMETHOD(get_SummarizeOnlyGroup)(BOOL* pVal);
	STDMETHOD(put_SummarizeOnlyGroup)(BOOL newVal);
	STDMETHOD(get_SummarizeNormDB)(BOOL* pVal);
	STDMETHOD(put_SummarizeNormDB)(BOOL newVal);
	STDMETHOD(get_NormDBCalcScore)(BOOL* pVal);
	STDMETHOD(put_NormDBCalcScore)(BOOL newVal);
	STDMETHOD(get_NormDBCalcScoreGlobal)(BOOL* pVal);
	STDMETHOD(put_NormDBCalcScoreGlobal)(BOOL newVal);
	STDMETHOD(get_NormDBNoUpdate)(BOOL* pVal);
	STDMETHOD(put_NormDBNoUpdate)(BOOL newVal);
	STDMETHOD(get_NormDBThreshold)(double* pVal);
	STDMETHOD(put_NormDBThreshold)(double newVal);
	STDMETHOD(get_RemoveTrailingPenlift)(BOOL* pVal);
	STDMETHOD(put_RemoveTrailingPenlift)(BOOL newVal);
	STDMETHOD(get_ExtAppScriptExt)(BSTR* pVal);
	STDMETHOD(put_ExtAppScriptExt)(BSTR newVal);
	STDMETHOD(get_ExtAppTypeExt)(ExternalAppType* pVal);
	STDMETHOD(put_ExtAppTypeExt)(ExternalAppType newVal);
	STDMETHOD(get_ExtAppScriptSeg)(BSTR* pVal);
	STDMETHOD(put_ExtAppScriptSeg)(BSTR newVal);
	STDMETHOD(get_ExtAppTypeSeg)(ExternalAppType* pVal);
	STDMETHOD(put_ExtAppTypeSeg)(ExternalAppType newVal);
	STDMETHOD(get_ExtAppScriptTF)(BSTR* pVal);
	STDMETHOD(put_ExtAppScriptTF)(BSTR newVal);
	STDMETHOD(get_ExtAppTypeTF)(ExternalAppType* pVal);
	STDMETHOD(put_ExtAppTypeTF)(ExternalAppType newVal);
	STDMETHOD(get_ExtAppScriptRaw)(BSTR* pVal);
	STDMETHOD(put_ExtAppScriptRaw)(BSTR newVal);
	STDMETHOD(get_ExtAppTypeRaw)(ExternalAppType* pVal);
	STDMETHOD(put_ExtAppTypeRaw)(ExternalAppType newVal);
	STDMETHOD(get_DiscardPenDownDurMin)(BOOL* pVal);
	STDMETHOD(put_DiscardPenDownDurMin)(BOOL newVal);
	STDMETHOD(get_PenDownDurMin)(double* pVal);
	STDMETHOD(put_PenDownDurMin)(double newVal);
	STDMETHOD(get_ImgResolution)(short* pVal);
	STDMETHOD(put_ImgResolution)(short newVal);
	STDMETHOD(get_InkThreshold)(short* pVal);
	STDMETHOD(put_InkThreshold)(short newVal);
	STDMETHOD(get_SmoothingIter)(short* pVal);
	STDMETHOD(put_SmoothingIter)(short newVal);
	STDMETHOD(get_MaxPenPressure)(short* pVal);
	STDMETHOD(put_MaxPenPressure)(short newVal);
	STDMETHOD(get_DisNotify)(BOOL* pVal);
	STDMETHOD(put_DisNotify)(BOOL newVal);
	STDMETHOD(get_DisError)(double* pVal);
	STDMETHOD(put_DisError)(double newVal);
	STDMETHOD(get_UseTilt)(BOOL* pVal);
	STDMETHOD(put_UseTilt)(BOOL newVal);
	STDMETHOD(get_CollapseUDStrokes)(BOOL* pVal);
	STDMETHOD(put_CollapseUDStrokes)(BOOL newVal);
	STDMETHOD(get_CollapseStrokes)(BOOL* pVal);
	STDMETHOD(put_CollapseStrokes)(BOOL newVal);
	STDMETHOD(get_CollapseTrials)(BOOL* pVal);
	STDMETHOD(put_CollapseTrials)(BOOL newVal);
	STDMETHOD(get_CollapseMedian)(BOOL* pVal);
	STDMETHOD(put_CollapseMedian)(BOOL newVal);
	STDMETHOD(get_UpSampleFactor)(short* pVal);
	STDMETHOD(put_UpSampleFactor)(short newVal);
	STDMETHOD(get_QuestAtEnd)(BOOL* pVal);
	STDMETHOD(put_QuestAtEnd)(BOOL newVal);
	STDMETHOD(get_FullScreen)(BOOL* pVal);
	STDMETHOD(put_FullScreen)(BOOL newVal);
	STDMETHOD(get_RandSelectionTrials)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_RandSelectionTrials)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RandSelectionChar)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_RandSelectionChar)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RandBlockCounts)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_RandBlockCounts)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RandRules)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_RandRules)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RandWithRules)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_RandWithRules)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_MaxLineThickness)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_MaxLineThickness)(/*[in]*/ short newVal);
	STDMETHOD(get_VaryLineThickness)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_VaryLineThickness)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_D)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_D)(/*[in]*/ BOOL newVal);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(put_DisCorrection)(/*[in]*/ short newVal);
	STDMETHOD(get_DisCorrection)(/*[out, retval]*/ short* newVal);
	STDMETHOD(put_DisFactor)(/*[in]*/ double newVal);
	STDMETHOD(get_DisFactor)(/*[out, retval]*/ double* newVal);
	STDMETHOD(put_DisZInsertPoint)(/*[in]*/ double newVal);
	STDMETHOD(get_DisZInsertPoint)(/*[out, retval]*/ double* newVal);
	STDMETHOD(put_DisRelError)(/*[in]*/ double newVal);
	STDMETHOD(get_DisRelError)(/*[out, retval]*/ double* newVal);
	STDMETHOD(put_DisAbsError)(/*[in]*/ double newVal);
	STDMETHOD(get_DisAbsError)(/*[out, retval]*/ double* newVal);
	STDMETHOD(GetDrawingOptions)( /*[out]*/ short* LineSize, /*[out]*/ short* EllipseSize,
											/*[out]*/ DWORD* BGColor, /*[out]*/ DWORD* LineColor1,
											/*[out]*/ DWORD* LineColor2, /*[out]*/ DWORD* LineColor3,
											/*[out]*/ DWORD* EllipseColor, /*[out]*/ DWORD* MPPColor );
	STDMETHOD(SetDrawingOptions)( /*[in]*/ short LineSize, /*[in]*/ short EllipseSize,
											/*[in]*/ DWORD BGColor, /*[in]*/ DWORD LineColor1,
											/*[in]*/ DWORD LineColor2, /*[in]*/ DWORD LineColor3,
											/*[in]*/ DWORD EllipseColor, /*[in]*/ DWORD MPPColor );
	STDMETHOD(get_SEG_V)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_V)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DiscardLTMin)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DiscardLTMin)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DiscardGTMax)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DiscardGTMax)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_FFT)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_FFT)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_MinReactionTime)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinReactionTime)(/*[in]*/ double newVal);
	STDMETHOD(get_MaxReactionTime)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MaxReactionTime)(/*[in]*/ double newVal);
	STDMETHOD(get_Sharpness)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_Sharpness)(/*[in]*/ double newVal);
	STDMETHOD(get_TrialMode)(/*[out, retval]*/ TrialsMode *pVal);
	STDMETHOD(put_TrialMode)(/*[in]*/ TrialsMode newVal);
	STDMETHOD(get_ViewExpCon)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ViewExpCon)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ViewExpExt)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ViewExpExt)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ViewSubjCon)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ViewSubjCon)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ViewSubjExt)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ViewSubjExt)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Analyze)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Analyze)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SummarizeSelect)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SummarizeSelect)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SummarizeOnly)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SummarizeOnly)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Summarize)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Summarize)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DisplaySeg)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DisplaySeg)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DisplayTF)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DisplayTF)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DisplayRaw)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DisplayRaw)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DisplayCharts)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DisplayCharts)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ProcImmediately)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ProcImmediately)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_MaxRecArea)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_MaxRecArea)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TimeoutLatency)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TimeoutLatency)(/*[in]*/ double newVal);
	STDMETHOD(get_TimeoutPenlift)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TimeoutPenlift)(/*[in]*/ double newVal);
	STDMETHOD(get_TimeoutRecord)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TimeoutRecord)(/*[in]*/ double newVal);
	STDMETHOD(get_TimeoutStart)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TimeoutStart)(/*[in]*/ double newVal);
	STDMETHOD(get_DoQuestionnaire)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_DoQuestionnaire)(/*[in]*/ BOOL newVal);
// IProcSetting
	STDMETHOD(get_MinStraightErrorCurved)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinStraightErrorCurved)(/*[in]*/ double newVal);
	STDMETHOD(get_MaxStraightErrorStraight)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MaxStraightErrorStraight)(/*[in]*/ double newVal);
	STDMETHOD(get_MinLoopArea)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinLoopArea)(/*[in]*/ double newVal);
	STDMETHOD(get_SpiralIncreaseFactor)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_SpiralIncreaseFactor)(/*[in]*/ double newVal);
	STDMETHOD(get_SlantError)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_SlantError)(/*[in]*/ double newVal);
	STDMETHOD(get_MaxFrequency)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MaxFrequency)(/*[in]*/ double newVal);
	STDMETHOD(get_StrokeBeginning)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_StrokeBeginning)(/*[in]*/ double newVal);
	STDMETHOD(get_MaxVertVelocity)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MaxVertVelocity)(/*[in]*/ double newVal);
	STDMETHOD(get_MinVertVelocity)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinVertVelocity)(/*[in]*/ double newVal);
	STDMETHOD(get_MinStrokeDuration)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinStrokeDuration)(/*[in]*/ double newVal);
	STDMETHOD(get_MinStrokeSizeRelativeToMax)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinStrokeSizeRelativeToMax)(/*[in]*/ double newVal);
	STDMETHOD(get_MinStrokeSize)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinStrokeSize)(/*[in]*/ double newVal);
	STDMETHOD(get_MaxTimePenup)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MaxTimePenup)(/*[in]*/ double newVal);
	STDMETHOD(get_MinTimePenup)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinTimePenup)(/*[in]*/ double newVal);
	STDMETHOD(get_MinDownSpacing)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinDownSpacing)(/*[in]*/ double newVal);
	STDMETHOD(get_MinWordWidth)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinWordWidth)(/*[in]*/ double newVal);
	STDMETHOD(get_MinLeftSpacing)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinLeftSpacing)(/*[in]*/ double newVal);
	STDMETHOD(get_MinLeftPenup)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_MinLeftPenup)(/*[in]*/ double newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(get_InstructionAlwaysVisible)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_InstructionAlwaysVisible)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DeviceResolution)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_DeviceResolution)(/*[in]*/ double newVal);
	STDMETHOD(get_MinPenPressure)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_MinPenPressure)(/*[in]*/ short newVal);
	STDMETHOD(get_SamplingRate)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_SamplingRate)(/*[in]*/ short newVal);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_SpecifySequence)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SpecifySequence)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ConditionSequence)(/*[out, retval]*/ BSTR* pVal );
	STDMETHOD(put_ConditionSequence)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RandomizeTrials)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_RandomizeTrials)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Randomize)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Randomize)(/*[in]*/ BOOL newVal);
// IProcSetFlags
	STDMETHOD(get_DiscardAfter2)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_DiscardAfter2)(/*[in]*/ short newVal);
	STDMETHOD(get_ST_D2)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_D2)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_DiscardAfter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_DiscardAfter)(/*[in]*/ short newVal);
	STDMETHOD(get_ST_D)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_D)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ST_S)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_S)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ST_T)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_T)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ST_Z)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_Z)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ST_R)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_R)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ST_A)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ST_A)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_O)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_O)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_S)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_S)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_C)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_C)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_U)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_U)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_M)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_M)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_N)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_N)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_CON_A)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_CON_A)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_EXT_2)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_EXT_2)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_EXT_O)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_EXT_O)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_EXT_3)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_EXT_3)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_EXT_S)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_EXT_S)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_J)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_J)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_MM)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_MM)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_A)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_A)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_M)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_M)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_O)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_O)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_S)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_S)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_L)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_L)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SEG_F)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_SEG_F)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TF_O)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_TF_O)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TF_U)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_TF_U)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TF_A)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_TF_A)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TF_R)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_TF_R)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_TF_J)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_TF_J)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Decimate)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Decimate)(/*[in]*/ short newVal);
	STDMETHOD(get_FilterFrequency)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_FilterFrequency)(/*[in]*/ double newVal);
	STDMETHOD(get_RotationBeta)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_RotationBeta)(/*[in]*/ double newVal);
	STDMETHOD(get_Differentiate)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Differentiate)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Velocity)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_Velocity)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ExperimentID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ExperimentID)(/*[in]*/ BSTR newVal);
protected:
	ProcSettingDB*	m_pDB;
};

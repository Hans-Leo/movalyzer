// Stimulus.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "StimulusDB.h"
#include "Stimulus.h"

/////////////////////////////////////////////////////////////////////////////
//

Stimulus::Stimulus()  : m_pDB( NULL )
{
	m_pDB = new StimulusDB;
}

Stimulus::~Stimulus()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP Stimulus::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IStimulus,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP Stimulus::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP Stimulus::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP Stimulus::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StimID.AllocSysString();

	return S_OK;
}

STDMETHODIMP Stimulus::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StimID = newVal;

	return S_OK;
}

STDMETHODIMP Stimulus::get_Description(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Desc.AllocSysString();

	return S_OK;
}

STDMETHODIMP Stimulus::put_Description(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Desc = newVal;

	return S_OK;
}

STDMETHODIMP Stimulus::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_StimID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP Stimulus::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP Stimulus::get_Demo(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Demo;

	return S_OK;
}

STDMETHODIMP Stimulus::put_Demo(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Demo = newVal;

	return S_OK;
}

STDMETHODIMP Stimulus::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP Stimulus::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP Stimulus::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP Stimulus::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	if( m_pDB ) *Cnt = m_pDB->GetNumRecords();
	return S_OK;
}

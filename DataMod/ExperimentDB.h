#pragma once

// ExperimentDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBExperiment.h"

/////////////////////////////////////////////////////////////////////////////
// ExperimentDB recordset

class ExperimentDB : public CTDataDB
{
public:
	ExperimentDB();

// Field/Param Data
	CString	m_ExpID;
	CString	m_Desc;
	CString	m_Notes;
	FLD_INT( Type );
	FLD_DOUBLE( MissingDataValue );

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	BOOL TestLogin();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pEXPERIMENT );
	BOOL SetAll( pEXPERIMENT );
};

#if !defined(AFX_STIMTARGETDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_STIMTARGETDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StimTargetDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBStimTarget.h"

/////////////////////////////////////////////////////////////////////////////
// StimTargetDB recordset

class StimTargetDB : public CTDataDB
{
public:
	StimTargetDB();

// Field/Param Data
	CString	m_StimID;
	CString	m_ElmtID;
	int		m_Sequence;

// Implementation
public:
	BOOL Find( CString StimID, CString ElmtID, short Seq );
	BOOL Find( CString StimID, CString ElmtID );
	BOOL Find( CString StimID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ReleaseDB();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pSTIMULUSTARGET );
	BOOL SetAll( pSTIMULUSTARGET );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STIMTARGETDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)

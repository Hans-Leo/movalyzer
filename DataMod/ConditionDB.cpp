// ConditionDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ConditionDB.h"

DBCondition _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	SOUND_FREQ_MIN		20
#define	SOUND_FREQ_MAX		20000
#define	SOUND_DUR_MIN		0
#define	SOUND_DUR_MAX		50000

/////////////////////////////////////////////////////////////////////////////
// ConditionDB

ConditionDB::ConditionDB() : CTDataDB( &_db )
{
	m_CondID = _T("");
	m_Desc = _T("");
	m_Instr = _T("");
	m_Lex = _T("");
	m_StrokeMin = 30;
	m_StrokeMax = 33;
	m_StrokeLength = 0.0;
	m_RangeLength = 0.0;
	m_StrokeDirection = 0.0;
	m_RangeDirection = 0.0;
	m_StrokeSkip = 0;
	m_Notes = _T("");
	m_UseStimulus = FALSE;
	m_Stimulus = _T("");
	m_StimulusW = _T("");
	m_StimulusP = _T("");
	m_Record = TRUE;
	m_Process = TRUE;
	m_Parent = _T("");
	m_Word = 1;
	m_PrecueDuration = 2.5;
	m_PrecueLatency = 2.5;
	m_RecordImmediately = FALSE;
	m_StopWrongTarget = FALSE;
	m_StartFirstTarget = FALSE;
	m_StopLastTarget = FALSE;
	m_WarningDuration = 2.5;
	m_WarningLatency = 2.5;
	m_MagnetForce = 8.;
	SoundStart = TRUE;
	SoundStartTone = TRUE;
	SoundStartToneFreq = 800;
	SoundStartToneDur = 20;
	SoundEnd = TRUE;
	SoundEndTone = TRUE;
	SoundEndToneFreq = 800;
	SoundEndToneDur = 20;
	SoundStimPStart = FALSE;
	SoundStimPStartTone = TRUE;
	SoundStimPStartToneFreq = 800;
	SoundStimPStartToneDur = 20;
	SoundStimPStop = FALSE;
	SoundStimPStopTone = TRUE;
	SoundStimPStopToneFreq = 800;
	SoundStimPStopToneDur = 20;
	SoundStimWStart = FALSE;
	SoundStimWStartTone = TRUE;
	SoundStimWStartToneFreq = 800;
	SoundStimWStartToneDur = 20;
	SoundStimWStop = FALSE;
	SoundStimWStopTone = TRUE;
	SoundStimWStopToneFreq = 800;
	SoundStimWStopToneDur = 20;
	SoundTargetCorrect = FALSE;
	SoundTargetCorrectTone = TRUE;
	SoundTargetCorrectToneFreq = 800;
	SoundTargetCorrectToneDur = 20;
	SoundTargetWrong = TRUE;
	SoundTargetWrongTone = TRUE;
	SoundTargetWrongToneFreq = 800;
	SoundTargetWrongToneDur = 20;
	SoundPenup = FALSE;
	SoundPenupTone = TRUE;
	SoundPenupToneFreq = 800;
	SoundPenupToneDur = 20;
	SoundPendown = FALSE;
	SoundPendownTone = TRUE;
	SoundPendownToneFreq = 800;
	SoundPendownToneDur = 20;
	CountStrokes = FALSE;
	HideFeedback = FALSE;
	HideShowAfter = FALSE;
	HideShowAfterShow = FALSE;
	HideShowAfterStrokes = FALSE;
	HideShowCount = 0.;
	Transform = FALSE;
	Xgain = 1.;
	Xrotation = 0.;
	Ygain = 1.;
	Yrotation = 0.;
	flagbadtarget = FALSE;
	SoundStartTrigger = FALSE;
	SoundEndTrigger = FALSE;
	SoundStimWStartTrigger = FALSE;
	SoundStimWStopTrigger = FALSE;
	SoundStimPStartTrigger = FALSE;
	SoundStimPStopTrigger = FALSE;
	SoundTargetCorrectTrigger = FALSE;
	SoundTargetWrongTrigger = FALSE;
	SoundPenupTrigger = FALSE;
	SoundPendownTrigger = FALSE;
	SoundStartScriptType = 0;
	SoundEndScriptType = 0;
	SoundStimWStartScriptType = 0;
	SoundStimWStopScriptType = 0;
	SoundStimPStartScriptType = 0;
	SoundStimPStopScriptType = 0;
	SoundTargetCorrectScriptType = 0;
	SoundTargetWrongScriptType = 0;
	SoundPenupScriptType = 0;
	SoundPendownScriptType = 0;
	Feedback = _T("");
	GripperTask = 0;
	// recording window visual feedback
	TrialFeedback1 = FALSE;
	TrialFeedback2 = FALSE;
	FBColumn1 = 1;
	FBColumn2 = 1;
	FBStroke1 = 1;
	FBStroke2 = 1;
	FBMin1 = 0.;
	FBMin2 = 0.;
	FBMax1 = 1.;
	FBMax2 = 1.;
	FBSwapColors1 = FALSE;
	FBSwapColors2 = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ConditionDB::GetAll( pCONDITION pCondition )
{
	if( !pCondition ) return FALSE;

	m_CondID = pCondition->CondID;
	m_Desc = pCondition->Desc;
	m_Instr = pCondition->Instr;
	m_Lex = pCondition->Lex;
	m_StrokeMin = pCondition->StrokeMin;
	m_StrokeMax = pCondition->StrokeMax;
	m_StrokeLength = pCondition->StrokeLength;
	m_RangeLength = pCondition->RangeLength;
	m_StrokeDirection = pCondition->StrokeDirection;
	m_RangeDirection = pCondition->RangeDirection;
	m_StrokeSkip = pCondition->StrokeSkip;
	m_Notes = pCondition->Notes;
	m_UseStimulus = pCondition->UseStimulus;
	m_Stimulus = pCondition->Stimulus;
	m_StimulusW = pCondition->StimulusW;
	m_StimulusP = pCondition->StimulusP;
	m_Record = pCondition->Record;
	m_Process = pCondition->Process;
	m_Parent = pCondition->Parent;
	m_Word = pCondition->Word;
	m_PrecueDuration = pCondition->PrecueDuration;
	m_PrecueLatency = pCondition->PrecueLatency;
	m_RecordImmediately = pCondition->RecordImmediately;
	m_StopWrongTarget = pCondition->StopWrongTarget;
	m_StartFirstTarget = pCondition->StartFirstTarget;
	m_StopLastTarget = pCondition->StopLastTarget;
	m_WarningDuration = pCondition->WarningDuration;
	m_WarningLatency = pCondition->WarningLatency;
	m_MagnetForce = pCondition->MagnetForce;
	SoundStart = pCondition->SoundStart;
	SoundStartTone = pCondition->SoundStartTone;
	SoundStartToneFreq = pCondition->SoundStartToneFreq;
	SoundStartToneDur = pCondition->SoundStartToneDur;
	SoundStartMedia = pCondition->SoundStartMedia;
	SoundEnd = pCondition->SoundEnd;
	SoundEndTone = pCondition->SoundEndTone;
	SoundEndToneFreq = pCondition->SoundEndToneFreq;
	SoundEndToneDur = pCondition->SoundEndToneDur;
	SoundEndMedia = pCondition->SoundEndMedia;
	SoundStimPStart = pCondition->SoundStimPStart;
	SoundStimPStartTone = pCondition->SoundStimPStartTone;
	SoundStimPStartToneFreq = pCondition->SoundStimPStartToneFreq;
	SoundStimPStartToneDur = pCondition->SoundStimPStartToneDur;
	SoundStimPStartMedia = pCondition->SoundStimPStartMedia;
	SoundStimPStop = pCondition->SoundStimPStop;
	SoundStimPStopTone = pCondition->SoundStimPStopTone;
	SoundStimPStopToneFreq = pCondition->SoundStimPStopToneFreq;
	SoundStimPStopToneDur = pCondition->SoundStimPStopToneDur;
	SoundStimPStopMedia = pCondition->SoundStimPStopMedia;
	SoundStimWStart = pCondition->SoundStimWStart;
	SoundStimWStartTone = pCondition->SoundStimWStartTone;
	SoundStimWStartToneFreq = pCondition->SoundStimWStartToneFreq;
	SoundStimWStartToneDur = pCondition->SoundStimWStartToneDur;
	SoundStimWStartMedia = pCondition->SoundStimWStartMedia;
	SoundStimWStop = pCondition->SoundStimWStop;
	SoundStimWStopTone = pCondition->SoundStimWStopTone;
	SoundStimWStopToneFreq = pCondition->SoundStimWStopToneFreq;
	SoundStimWStopToneDur = pCondition->SoundStimWStopToneDur;
	SoundStimWStopMedia = pCondition->SoundStimWStopMedia;
	SoundTargetCorrect = pCondition->SoundTargetCorrect;
	SoundTargetCorrectTone = pCondition->SoundTargetCorrectTone;
	SoundTargetCorrectToneFreq = pCondition->SoundTargetCorrectToneFreq;
	SoundTargetCorrectToneDur = pCondition->SoundTargetCorrectToneDur;
	SoundTargetCorrectMedia = pCondition->SoundTargetCorrectMedia;
	SoundTargetWrong = pCondition->SoundTargetWrong;
	SoundTargetWrongTone = pCondition->SoundTargetWrongTone;
	SoundTargetWrongToneFreq = pCondition->SoundTargetWrongToneFreq;
	SoundTargetWrongToneDur = pCondition->SoundTargetWrongToneDur;
	SoundTargetWrongMedia = pCondition->SoundTargetWrongMedia;
	SoundPenup = pCondition->SoundPenup;
	SoundPenupTone = pCondition->SoundPenupTone;
	SoundPenupToneFreq = pCondition->SoundPenupToneFreq;
	SoundPenupToneDur = pCondition->SoundPenupToneDur;
	SoundPenupMedia = pCondition->SoundPenupMedia;
	SoundPendown = pCondition->SoundPendown;
	SoundPendownTone = pCondition->SoundPendownTone;
	SoundPendownToneFreq = pCondition->SoundPendownToneFreq;
	SoundPendownToneDur = pCondition->SoundPendownToneDur;
	SoundPendownMedia = pCondition->SoundPendownMedia;
	CountStrokes = pCondition->CountStrokes;
	HideFeedback = pCondition->HideFeedback;
	HideShowAfter = pCondition->HideShowAfter;
	HideShowAfterShow = pCondition->HideShowAfterShow;
	HideShowAfterStrokes = pCondition->HideShowAfterStrokes;
	HideShowCount = pCondition->HideShowCount;
	Transform = pCondition->Transform;
	Xgain = pCondition->Xgain;
	Xrotation = pCondition->Xrotation;
	Ygain = pCondition->Ygain;
	Yrotation = pCondition->Yrotation;
	flagbadtarget = pCondition->flagbadtarget;
	SoundStartTrigger = pCondition->SoundStartTrigger;
	SoundEndTrigger = pCondition->SoundEndTrigger;
	SoundStimWStartTrigger = pCondition->SoundStimWStartTrigger;
	SoundStimWStopTrigger = pCondition->SoundStimWStopTrigger;
	SoundStimPStartTrigger = pCondition->SoundStimPStartTrigger;
	SoundStimPStopTrigger = pCondition->SoundStimPStopTrigger;
	SoundTargetCorrectTrigger = pCondition->SoundTargetCorrectTrigger;
	SoundTargetWrongTrigger = pCondition->SoundTargetWrongTrigger;
	SoundPenupTrigger = pCondition->SoundPenupTrigger;
	SoundPendownTrigger = pCondition->SoundPendownTrigger;
	SoundStartScriptType = pCondition->SoundStartScriptType;
	SoundStartScript = pCondition->SoundStartScript;
	SoundEndScriptType = pCondition->SoundEndScriptType;
	SoundEndScript = pCondition->SoundEndScript;
	SoundStimWStartScriptType = pCondition->SoundStimWStartScriptType;
	SoundStimWStartScript = pCondition->SoundStimWStartScript;
	SoundStimWStopScriptType = pCondition->SoundStimWStopScriptType;
	SoundStimWStopScript = pCondition->SoundStimWStopScript;
	SoundStimPStartScriptType = pCondition->SoundStimPStartScriptType;
	SoundStimPStartScript = pCondition->SoundStimPStartScript;
	SoundStimPStopScriptType = pCondition->SoundStimPStopScriptType;
	SoundStimPStopScript = pCondition->SoundStimPStopScript;
	SoundTargetCorrectScriptType = pCondition->SoundTargetCorrectScriptType;
	SoundTargetCorrectScript = pCondition->SoundTargetCorrectScript;
	SoundTargetWrongScriptType= pCondition->SoundTargetWrongScriptType;
	SoundTargetWrongScript = pCondition->SoundTargetWrongScript;
	SoundPenupScriptType = pCondition->SoundPenupScriptType;
	SoundPenupScript = pCondition->SoundPenupScript;
	SoundPendownScriptType = pCondition->SoundPendownScriptType;
	SoundPendownScript = pCondition->SoundPendownScript;
	Feedback = pCondition->Feedback;
	GripperTask = pCondition->GripperTask;
	TrialFeedback1 = pCondition->TrialFeedback1;
	TrialFeedback2 = pCondition->TrialFeedback2;
	FBColumn1 = pCondition->FBColumn1;
	FBColumn2 = pCondition->FBColumn2;
	FBStroke1 = pCondition->FBStroke1;
	FBStroke2 = pCondition->FBStroke2;
	FBMin1 = pCondition->FBMin1;
	FBMin2 = pCondition->FBMin2;
	FBMax1 = pCondition->FBMax1;
	FBMax2 = pCondition->FBMax2;
	FBSwapColors1 = pCondition->FBSwapColors1;
	FBSwapColors2 = pCondition->FBSwapColors2;

	VerifyValues();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void ConditionDB::VerifyValues()
{
	if( SoundStartToneFreq < SOUND_FREQ_MIN ) SoundStartToneFreq = SOUND_FREQ_MIN;
	else if( SoundStartToneFreq > SOUND_FREQ_MAX ) SoundStartToneFreq = SOUND_FREQ_MAX;
	if( SoundStartToneDur < SOUND_DUR_MIN ) SoundStartToneDur = SOUND_DUR_MIN;
	else if( SoundStartToneDur > SOUND_DUR_MAX ) SoundStartToneDur = SOUND_DUR_MAX;

	if( SoundEndToneFreq < SOUND_FREQ_MIN ) SoundEndToneFreq = SOUND_FREQ_MIN;
	else if( SoundEndToneFreq > SOUND_FREQ_MAX ) SoundEndToneFreq = SOUND_FREQ_MAX;
	if( SoundEndToneDur < SOUND_DUR_MIN ) SoundEndToneDur = SOUND_DUR_MIN;
	else if( SoundEndToneDur > SOUND_DUR_MAX ) SoundEndToneDur = SOUND_DUR_MAX;

	if( SoundStimPStartToneFreq < SOUND_FREQ_MIN ) SoundStimPStartToneFreq = SOUND_FREQ_MIN;
	else if( SoundStimPStartToneFreq > SOUND_FREQ_MAX ) SoundStimPStartToneFreq = SOUND_FREQ_MAX;
	if( SoundStimPStartToneDur < SOUND_DUR_MIN ) SoundStimPStartToneDur = SOUND_DUR_MIN;
	else if( SoundStimPStartToneDur > SOUND_DUR_MAX ) SoundStimPStartToneDur = SOUND_DUR_MAX;

	if( SoundStimPStopToneFreq < SOUND_FREQ_MIN ) SoundStimPStopToneFreq = SOUND_FREQ_MIN;
	else if( SoundStimPStopToneFreq > SOUND_FREQ_MAX ) SoundStimPStopToneFreq = SOUND_FREQ_MAX;
	if( SoundStimPStopToneDur < SOUND_DUR_MIN ) SoundStimPStopToneDur = SOUND_DUR_MIN;
	else if( SoundStimPStopToneDur > SOUND_DUR_MAX ) SoundStimPStopToneDur = SOUND_DUR_MAX;

	if( SoundStimWStartToneFreq < SOUND_FREQ_MIN ) SoundStimWStartToneFreq = SOUND_FREQ_MIN;
	else if( SoundStimWStartToneFreq > SOUND_FREQ_MAX ) SoundStimWStartToneFreq = SOUND_FREQ_MAX;
	if( SoundStimWStartToneDur < SOUND_DUR_MIN ) SoundStimWStartToneDur = SOUND_DUR_MIN;
	else if( SoundStimWStartToneDur > SOUND_DUR_MAX ) SoundStimWStartToneDur = SOUND_DUR_MAX;

	if( SoundStimWStopToneFreq < SOUND_FREQ_MIN ) SoundStimWStopToneFreq = SOUND_FREQ_MIN;
	else if( SoundStimWStopToneFreq > SOUND_FREQ_MAX ) SoundStimWStopToneFreq = SOUND_FREQ_MAX;
	if( SoundStimWStopToneDur < SOUND_DUR_MIN ) SoundStimWStopToneDur = SOUND_DUR_MIN;
	else if( SoundStimWStopToneDur > SOUND_DUR_MAX ) SoundStimWStopToneDur = SOUND_DUR_MAX;

	if( SoundTargetCorrectToneFreq < SOUND_FREQ_MIN ) SoundTargetCorrectToneFreq = SOUND_FREQ_MIN;
	else if( SoundTargetCorrectToneFreq > SOUND_FREQ_MAX ) SoundTargetCorrectToneFreq = SOUND_FREQ_MAX;
	if( SoundTargetCorrectToneDur < SOUND_DUR_MIN ) SoundTargetCorrectToneDur = SOUND_DUR_MIN;
	else if( SoundTargetCorrectToneDur > SOUND_DUR_MAX ) SoundTargetCorrectToneDur = SOUND_DUR_MAX;

	if( SoundTargetWrongToneFreq < SOUND_FREQ_MIN ) SoundTargetWrongToneFreq = SOUND_FREQ_MIN;
	else if( SoundTargetWrongToneFreq > SOUND_FREQ_MAX ) SoundTargetWrongToneFreq = SOUND_FREQ_MAX;
	if( SoundTargetWrongToneDur < SOUND_DUR_MIN ) SoundTargetWrongToneDur = SOUND_DUR_MIN;
	else if( SoundTargetWrongToneDur > SOUND_DUR_MAX ) SoundTargetWrongToneDur = SOUND_DUR_MAX;

	if( SoundPenupToneFreq < SOUND_FREQ_MIN ) SoundPenupToneFreq = SOUND_FREQ_MIN;
	else if( SoundPenupToneFreq > SOUND_FREQ_MAX ) SoundPenupToneFreq = SOUND_FREQ_MAX;
	if( SoundPenupToneDur < SOUND_DUR_MIN ) SoundPenupToneDur = SOUND_DUR_MIN;
	else if( SoundPenupToneDur > SOUND_DUR_MAX ) SoundPenupToneDur = SOUND_DUR_MAX;

	if( SoundPendownToneFreq < SOUND_FREQ_MIN ) SoundPendownToneFreq = SOUND_FREQ_MIN;
	else if( SoundPendownToneFreq > SOUND_FREQ_MAX ) SoundPendownToneFreq = SOUND_FREQ_MAX;
	if( SoundPendownToneDur < SOUND_DUR_MIN ) SoundPendownToneDur = SOUND_DUR_MIN;
	else if( SoundPendownToneDur > SOUND_DUR_MAX ) SoundPendownToneDur = SOUND_DUR_MAX;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ConditionDB::SetAll( pCONDITION pCondition )
{
	if( !pCondition ) return FALSE;

	VerifyValues();

	strncpy_s( pCondition->CondID, szIDS + 1, m_CondID, _TRUNCATE );
	strncpy_s( pCondition->Desc, szDESC + 1, m_Desc, _TRUNCATE );
	strncpy_s( pCondition->Instr, szCONDITION_INSTR + 1, m_Instr, _TRUNCATE );
	strncpy_s( pCondition->Lex, szCONDITION_LEX + 1, m_Lex, _TRUNCATE );
	pCondition->StrokeMin = m_StrokeMin;
	pCondition->StrokeMax = m_StrokeMax;
	pCondition->StrokeLength = m_StrokeLength;
	pCondition->RangeLength = m_RangeLength;
	pCondition->StrokeDirection = m_StrokeDirection;
	pCondition->RangeDirection = m_RangeDirection;
	pCondition->StrokeSkip = m_StrokeSkip;
	strncpy_s( pCondition->Notes, szNOTES + 1, m_Notes, _TRUNCATE );
	pCondition->UseStimulus = m_UseStimulus;
	strncpy_s( pCondition->Stimulus, szIDS + 1, m_Stimulus, _TRUNCATE );
	strncpy_s( pCondition->StimulusW, szIDS + 1, m_StimulusW, _TRUNCATE );
	strncpy_s( pCondition->StimulusP, szIDS + 1, m_StimulusP, _TRUNCATE );
	pCondition->Record = m_Record;
	pCondition->Process = m_Process;
	strncpy_s( pCondition->Parent, szIDS + 1, m_Parent, _TRUNCATE );
	pCondition->Word = m_Word;
	pCondition->PrecueDuration = m_PrecueDuration;
	pCondition->PrecueLatency = m_PrecueLatency;
	pCondition->RecordImmediately = m_RecordImmediately;
	pCondition->StopWrongTarget = m_StopWrongTarget;
	pCondition->StartFirstTarget = m_StartFirstTarget;
	pCondition->StopLastTarget = m_StopLastTarget;
	pCondition->WarningDuration = m_WarningDuration;
	pCondition->WarningLatency = m_WarningLatency;
	pCondition->MagnetForce = m_MagnetForce;
	pCondition->SoundStart = SoundStart;
	pCondition->SoundStartTone = SoundStartTone;
	pCondition->SoundStartToneFreq = SoundStartToneFreq;
	pCondition->SoundStartToneDur = SoundStartToneDur;
	strncpy_s( pCondition->SoundStartMedia, _MAX_PATH, SoundStartMedia, _TRUNCATE );
	pCondition->SoundEnd = SoundEnd;
	pCondition->SoundEndTone = SoundEndTone;
	pCondition->SoundEndToneFreq = SoundEndToneFreq;
	pCondition->SoundEndToneDur = SoundEndToneDur;
	strncpy_s( pCondition->SoundEndMedia, _MAX_PATH, SoundEndMedia, _TRUNCATE );
	pCondition->SoundStimPStart = SoundStimPStart;
	pCondition->SoundStimPStartTone = SoundStimPStartTone;
	pCondition->SoundStimPStartToneFreq = SoundStimPStartToneFreq;
	pCondition->SoundStimPStartToneDur = SoundStimPStartToneDur;
	strncpy_s( pCondition->SoundStimPStartMedia, _MAX_PATH, SoundStimPStartMedia, _TRUNCATE );
	pCondition->SoundStimPStop = SoundStimPStop;
	pCondition->SoundStimPStopTone = SoundStimPStopTone;
	pCondition->SoundStimPStopToneFreq = SoundStimPStopToneFreq;
	pCondition->SoundStimPStopToneDur = SoundStimPStopToneDur;
	strncpy_s( pCondition->SoundStimPStopMedia, _MAX_PATH, SoundStimPStopMedia, _TRUNCATE );
	pCondition->SoundStimWStart = SoundStimWStart;
	pCondition->SoundStimWStartTone = SoundStimWStartTone;
	pCondition->SoundStimWStartToneFreq = SoundStimWStartToneFreq;
	pCondition->SoundStimWStartToneDur = SoundStimWStartToneDur;
	strncpy_s( pCondition->SoundStimWStartMedia, _MAX_PATH, SoundStimWStartMedia, _TRUNCATE );
	pCondition->SoundStimWStop = SoundStimWStop;
	pCondition->SoundStimWStopTone = SoundStimWStopTone;
	pCondition->SoundStimWStopToneFreq = SoundStimWStopToneFreq;
	pCondition->SoundStimWStopToneDur = SoundStimWStopToneDur;
	strncpy_s( pCondition->SoundStimWStopMedia, _MAX_PATH, SoundStimWStopMedia, _TRUNCATE );
	pCondition->SoundTargetCorrect = SoundTargetCorrect;
	pCondition->SoundTargetCorrectTone = SoundTargetCorrectTone;
	pCondition->SoundTargetCorrectToneFreq = SoundTargetCorrectToneFreq;
	pCondition->SoundTargetCorrectToneDur = SoundTargetCorrectToneDur;
	strncpy_s( pCondition->SoundTargetCorrectMedia, _MAX_PATH, SoundTargetCorrectMedia, _TRUNCATE );
	pCondition->SoundTargetWrong = SoundTargetWrong;
	pCondition->SoundTargetWrongTone = SoundTargetWrongTone;
	pCondition->SoundTargetWrongToneFreq = SoundTargetWrongToneFreq;
	pCondition->SoundTargetWrongToneDur = SoundTargetWrongToneDur;
	strncpy_s( pCondition->SoundTargetWrongMedia, _MAX_PATH, SoundTargetWrongMedia, _TRUNCATE );
	pCondition->SoundPenup = SoundPenup;
	pCondition->SoundPenupTone = SoundPenupTone;
	pCondition->SoundPenupToneFreq = SoundPenupToneFreq;
	pCondition->SoundPenupToneDur = SoundPenupToneDur;
	strncpy_s( pCondition->SoundPenupMedia, _MAX_PATH, SoundPenupMedia, _TRUNCATE );
	pCondition->SoundPendown = SoundPendown;
	pCondition->SoundPendownTone = SoundPendownTone;
	pCondition->SoundPendownToneFreq = SoundPendownToneFreq;
	pCondition->SoundPendownToneDur = SoundPendownToneDur;
	strncpy_s( pCondition->SoundPendownMedia, _MAX_PATH, SoundPendownMedia, _TRUNCATE );
	pCondition->CountStrokes = CountStrokes;
	pCondition->HideFeedback = HideFeedback;
	pCondition->HideShowAfter = HideShowAfter;
	pCondition->HideShowAfterShow = HideShowAfterShow;
	pCondition->HideShowAfterStrokes = HideShowAfterStrokes;
	pCondition->HideShowCount = HideShowCount;
	pCondition->Transform = Transform;
	pCondition->Xgain = Xgain;
	pCondition->Xrotation = Xrotation;
	pCondition->Ygain = Ygain;
	pCondition->Yrotation = Yrotation;
	pCondition->flagbadtarget = flagbadtarget;
	pCondition->SoundStartTrigger = SoundStartTrigger;
	pCondition->SoundEndTrigger = SoundEndTrigger;
	pCondition->SoundStimWStartTrigger = SoundStimWStartTrigger;
	pCondition->SoundStimWStopTrigger = SoundStimWStopTrigger;
	pCondition->SoundStimPStartTrigger = SoundStimPStartTrigger;
	pCondition->SoundStimPStopTrigger = SoundStimPStopTrigger;
	pCondition->SoundTargetCorrectTrigger = SoundTargetCorrectTrigger;
	pCondition->SoundTargetWrongTrigger = SoundTargetWrongTrigger;
	pCondition->SoundPenupTrigger = SoundPenupTrigger;
	pCondition->SoundPendownTrigger = SoundPendownTrigger;
	pCondition->SoundStartScriptType = SoundStartScriptType;
	strncpy_s( pCondition->SoundStartScript, szFILELENGTH + 1, SoundStartScript, _TRUNCATE );
	pCondition->SoundEndScriptType = SoundEndScriptType;
	strncpy_s( pCondition->SoundEndScript, szFILELENGTH + 1, SoundEndScript, _TRUNCATE );
	pCondition->SoundStimWStartScriptType = SoundStimWStartScriptType;
	strncpy_s( pCondition->SoundStimWStartScript, szFILELENGTH + 1, SoundStimWStartScript, _TRUNCATE );
	pCondition->SoundStimWStopScriptType = SoundStimWStopScriptType;
	strncpy_s( pCondition->SoundStimWStopScript, szFILELENGTH + 1, SoundStimWStopScript, _TRUNCATE );
	pCondition->SoundStimPStartScriptType = SoundStimPStartScriptType;
	strncpy_s( pCondition->SoundStimPStartScript, szFILELENGTH + 1, SoundStimPStartScript, _TRUNCATE );
	pCondition->SoundStimPStopScriptType = SoundStimPStopScriptType;
	strncpy_s( pCondition->SoundStimPStopScript, szFILELENGTH + 1, SoundStimPStopScript, _TRUNCATE );
	pCondition->SoundTargetCorrectScriptType = SoundTargetCorrectScriptType;
	strncpy_s( pCondition->SoundTargetCorrectScript, szFILELENGTH + 1, SoundTargetCorrectScript, _TRUNCATE );
	pCondition->SoundTargetWrongScriptType = SoundTargetWrongScriptType;
	strncpy_s( pCondition->SoundTargetWrongScript, szFILELENGTH + 1, SoundTargetWrongScript, _TRUNCATE );
	pCondition->SoundPenupScriptType = SoundPenupScriptType;
	strncpy_s( pCondition->SoundPenupScript, szFILELENGTH + 1, SoundPenupScript, _TRUNCATE );
	pCondition->SoundPendownScriptType = SoundPendownScriptType;
	strncpy_s( pCondition->SoundPendownScript, szFILELENGTH + 1, SoundPendownScript, _TRUNCATE );
	strncpy_s( pCondition->Feedback, szIDS + 1, Feedback, _TRUNCATE );
	pCondition->GripperTask = GripperTask;
	pCondition->TrialFeedback1 = TrialFeedback1;
	pCondition->TrialFeedback2 = TrialFeedback2;
	pCondition->FBColumn1 = FBColumn1;
	pCondition->FBColumn2 = FBColumn2;
	pCondition->FBStroke1 = FBStroke1;
	pCondition->FBStroke2 = FBStroke2;
	pCondition->FBMin1 = FBMin1;
	pCondition->FBMin2 = FBMin2;
	pCondition->FBMax1 = FBMax1;
	pCondition->FBMax2 = FBMax2;
	pCondition->FBSwapColors1 = FBSwapColors1;
	pCondition->FBSwapColors2 = FBSwapColors2;

	return TRUE;
}

BOOL ConditionDB::Find( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pCONDITION pCondition = _db.Find( szID, szErr );
	if( !pCondition )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pCondition );
}

BOOL ConditionDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CONDITION cond;
	SetAll( &cond );
	if( !AddItem( (pDBDATA)&cond, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

BOOL ConditionDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CONDITION cond;
	SetAll( &cond );
	if( !ModifyItem( (pDBDATA)&cond, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

BOOL ConditionDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_CondID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

void ConditionDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CONDITION cond;
	if( GetFirstItem( (pDBDATA)&cond, szErr ) )
		GetAll( &cond );
}

BOOL ConditionDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CONDITION cond;
	if( GetNextItem( (pDBDATA)&cond, szErr ) )
	{
		GetAll( &cond );
		return TRUE;
	}

	return FALSE;
}

BOOL ConditionDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	CONDITION cond;
	if( GetPreviousItem( (pDBDATA)&cond, szErr ) )
	{
		GetAll( &cond );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ConditionDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ConditionDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ConditionDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

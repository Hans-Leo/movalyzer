// Stimulus.h: Definition of the Stimulus class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STIMULUS_H__24855DF6_B863_4389_9FCA_B4CCC5025F3B__INCLUDED_)
#define AFX_STIMULUS_H__24855DF6_B863_4389_9FCA_B4CCC5025F3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// Stimulus

class StimulusDB;

class Stimulus : 
	public IDispatchImpl<IStimulus, &IID_IStimulus, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Stimulus,&CLSID_Stimulus>
{
public:
	Stimulus();
	~Stimulus();

BEGIN_COM_MAP(Stimulus)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IStimulus)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Stimulus) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Stimulus)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IStimulus
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(get_Demo)(/*[out,retval]*/ BOOL *pVal);
	STDMETHOD(put_Demo)(/*[in]*/ BOOL newVal);
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_Description)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Description)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	StimulusDB*	m_pDB;
};

#endif // !defined(AFX_STIMULUS_H__24855DF6_B863_4389_9FCA_B4CCC5025F3B__INCLUDED_)

#if !defined(AFX_STIMELEMENTDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_STIMELEMENTDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StimElementDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBStimElement.h"

/////////////////////////////////////////////////////////////////////////////
// StimElementDB recordset

class StimElementDB : public CTDataDB
{
public:
	StimElementDB();

// Field/Param Data
	CString	m_StimID;
	CString	m_ElmtID;

// Implementation
public:
	BOOL Find( CString m_StimID, CString m_ElmtID );
	BOOL Find( CString m_StimID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pSTIMULUSELEMENT );
	BOOL SetAll( pSTIMULUSELEMENT );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STIMELEMENTDB_H__DCB05ED7_A758_11D3_8A58_000000000000__INCLUDED_)

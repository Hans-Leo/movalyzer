#pragma once

// ConditionDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBCondition.h"

/////////////////////////////////////////////////////////////////////////////
// ConditionDB recordset

class ConditionDB : public CTDataDB
{
public:
	ConditionDB();

// Field/Param Data
	CString	m_CondID;
	CString	m_Desc;
	CString	m_Instr;
	CString	m_Lex;
	int		m_StrokeMin;
	int		m_StrokeMax;
	double	m_StrokeLength;
	double	m_RangeLength;
	double	m_StrokeDirection;
	double	m_RangeDirection;
	int		m_StrokeSkip;
	CString	m_Notes;
	BOOL	m_Record;
	BOOL	m_Process;
	CString	m_Parent;
	int		m_Word;
	BOOL	m_UseStimulus;
	CString	m_StimulusW;
	CString	m_StimulusP;
	CString	m_Stimulus;
	double	m_PrecueDuration;
	double	m_PrecueLatency;
	BOOL	m_RecordImmediately;
	BOOL	m_StopWrongTarget;
	BOOL	m_StartFirstTarget;
	BOOL	m_StopLastTarget;
	double	m_WarningDuration;
	double	m_WarningLatency;
	double	m_MagnetForce;
	// sounds
	FLD_BOOL( SoundStart );
	FLD_BOOL( SoundStartTone );
	FLD_INT( SoundStartToneFreq );
	FLD_INT( SoundStartToneDur );
	CString SoundStartMedia;
	FLD_BOOL( SoundEnd );
	FLD_BOOL( SoundEndTone );
	FLD_INT( SoundEndToneFreq );
	FLD_INT( SoundEndToneDur );
	CString SoundEndMedia;
	FLD_BOOL( SoundStimWStart );
	FLD_BOOL( SoundStimWStartTone );
	FLD_INT( SoundStimWStartToneFreq );
	FLD_INT( SoundStimWStartToneDur );
	CString SoundStimWStartMedia;
	FLD_BOOL( SoundStimWStop );
	FLD_BOOL( SoundStimWStopTone );
	FLD_INT( SoundStimWStopToneFreq );
	FLD_INT( SoundStimWStopToneDur );
	CString SoundStimWStopMedia;
	FLD_BOOL( SoundStimPStart );
	FLD_BOOL( SoundStimPStartTone );
	FLD_INT( SoundStimPStartToneFreq );
	FLD_INT( SoundStimPStartToneDur );
	CString SoundStimPStartMedia;
	FLD_BOOL( SoundStimPStop );
	FLD_BOOL( SoundStimPStopTone );
	FLD_INT( SoundStimPStopToneFreq );
	FLD_INT( SoundStimPStopToneDur );
	CString SoundStimPStopMedia;
	FLD_BOOL( SoundTargetCorrect );
	FLD_BOOL( SoundTargetCorrectTone );
	FLD_INT( SoundTargetCorrectToneFreq );
	FLD_INT( SoundTargetCorrectToneDur );
	CString SoundTargetCorrectMedia;
	FLD_BOOL( SoundTargetWrong );
	FLD_BOOL( SoundTargetWrongTone );
	FLD_INT( SoundTargetWrongToneFreq );
	FLD_INT( SoundTargetWrongToneDur );
	CString SoundTargetWrongMedia;
	FLD_BOOL( SoundPenup );
	FLD_BOOL( SoundPenupTone );
	FLD_INT( SoundPenupToneFreq );
	FLD_INT( SoundPenupToneDur );
	CString SoundPenupMedia;
	FLD_BOOL( SoundPendown );
	FLD_BOOL( SoundPendownTone );
	FLD_INT( SoundPendownToneFreq );
	FLD_INT( SoundPendownToneDur );
	CString SoundPendownMedia;
	FLD_BOOL( CountStrokes );
	FLD_BOOL( HideFeedback );
	FLD_BOOL( HideShowAfter );
	FLD_BOOL( HideShowAfterShow );		// if false, hide
	FLD_BOOL( HideShowAfterStrokes );	// if false, seconds
	FLD_DOUBLE( HideShowCount );
	FLD_BOOL( Transform );
	FLD_DOUBLE( Xgain );
	FLD_DOUBLE( Xrotation );
	FLD_DOUBLE( Ygain );
	FLD_DOUBLE( Yrotation );
	FLD_BOOL( flagbadtarget );
	FLD_BOOL( SoundStartTrigger );
	FLD_BOOL( SoundEndTrigger );
	FLD_BOOL( SoundStimWStartTrigger );
	FLD_BOOL( SoundStimWStopTrigger );
	FLD_BOOL( SoundStimPStartTrigger );
	FLD_BOOL( SoundStimPStopTrigger );
	FLD_BOOL( SoundTargetCorrectTrigger );
	FLD_BOOL( SoundTargetWrongTrigger );
	FLD_BOOL( SoundPenupTrigger );
	FLD_BOOL( SoundPendownTrigger );
	// event scripts - external apps (being grouped with sound)
	FLD_INT( SoundStartScriptType );
	CString SoundStartScript;
	FLD_INT( SoundEndScriptType );
	CString SoundEndScript;
	FLD_INT( SoundStimWStartScriptType );
	CString SoundStimWStartScript;
	FLD_INT( SoundStimWStopScriptType );
	CString SoundStimWStopScript;
	FLD_INT( SoundStimPStartScriptType );
	CString SoundStimPStartScript;
	FLD_INT( SoundStimPStopScriptType );
	CString SoundStimPStopScript;
	FLD_INT( SoundTargetCorrectScriptType );
	CString SoundTargetCorrectScript;
	FLD_INT( SoundTargetWrongScriptType );
	CString SoundTargetWrongScript;
	FLD_INT( SoundPenupScriptType );
	CString SoundPenupScript;
	FLD_INT( SoundPendownScriptType );
	CString SoundPendownScript;
	CString Feedback;
	FLD_INT( GripperTask );
	// recording window visual feedback
	FLD_BOOL( TrialFeedback1 );
	FLD_BOOL( TrialFeedback2 );
	FLD_INT( FBColumn1 );
	FLD_INT( FBColumn2 );
	FLD_INT( FBStroke1 );
	FLD_INT( FBStroke2 );
	FLD_DOUBLE( FBMin1 );
	FLD_DOUBLE( FBMin2 );
	FLD_DOUBLE( FBMax1 );
	FLD_DOUBLE( FBMax2 );
	FLD_BOOL( FBSwapColors1 );
	FLD_BOOL( FBSwapColors2 );

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pCONDITION );
	BOOL SetAll( pCONDITION );
	void VerifyValues();
};

// Condition.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "ConditionDB.h"
#include "Condition.h"

/////////////////////////////////////////////////////////////////////////////
//

NSCondition::NSCondition()  : m_pDB( NULL )
{
	m_pDB = new ConditionDB;
}

NSCondition::~NSCondition()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP NSCondition::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_INSCondition,
		&IID_INSConditionSound,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP NSCondition::SetTemp( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->SetTemp( fVal );

	return S_OK;
}

STDMETHODIMP NSCondition::DumpRecord( BSTR ID, BSTR OutFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->DumpRecord( ID, OutFile ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::RemoveTemp()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->RemoveTemp();

	return S_OK;
}

STDMETHODIMP NSCondition::get_ID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CondID.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_ID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_CondID = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Description(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Desc.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Description(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Desc = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Instruction(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Instr.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Instruction(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Instr = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Lex(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Lex.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Lex(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Lex = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StrokeMin(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StrokeMin;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StrokeMin(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StrokeMin = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StrokeMax(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StrokeMax;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StrokeMax(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StrokeMax = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StrokeLength(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StrokeLength;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StrokeLength(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StrokeLength = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_RangeLength(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_RangeLength;

	return S_OK;
}

STDMETHODIMP NSCondition::put_RangeLength(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_RangeLength = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StrokeDirection(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StrokeDirection;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StrokeDirection(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StrokeDirection = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_RangeDirection(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_RangeDirection;

	return S_OK;
}

STDMETHODIMP NSCondition::put_RangeDirection(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_RangeDirection = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StrokeSkip(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StrokeSkip;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StrokeSkip(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StrokeSkip = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Notes(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Notes.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Notes(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Notes = newVal;

	return S_OK;
}


STDMETHODIMP NSCondition::get_UseStimulus(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_UseStimulus;

	return S_OK;
}

STDMETHODIMP NSCondition::put_UseStimulus(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_UseStimulus = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Stimulus(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Stimulus.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Stimulus(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Stimulus = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StimulusWarning(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StimulusW.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_StimulusWarning(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StimulusW = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StimulusPrecue(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StimulusP.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_StimulusPrecue(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StimulusP = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::Find(BSTR ID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_CondID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP NSCondition::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP NSCondition::SetBackupPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetBackupPath( Path );

	return S_OK;
}

STDMETHODIMP NSCondition::get_Record(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Record;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Record(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Record = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Process(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Process;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Process(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Process = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Parent(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Parent.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Parent(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Parent = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Word(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Word;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Word(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Word = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_PrecueDuration(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_PrecueDuration;

	return S_OK;
}

STDMETHODIMP NSCondition::put_PrecueDuration(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_PrecueDuration = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_PrecueLatency(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_PrecueLatency;

	return S_OK;
}

STDMETHODIMP NSCondition::put_PrecueLatency(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_PrecueLatency = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_RecordImmediately(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_RecordImmediately;

	return S_OK;
}

STDMETHODIMP NSCondition::put_RecordImmediately(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_RecordImmediately = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StopWrongTarget(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StopWrongTarget;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StopWrongTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StopWrongTarget = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StartFirstTarget(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StartFirstTarget;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StartFirstTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StartFirstTarget = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_StopLastTarget(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_StopLastTarget;

	return S_OK;
}

STDMETHODIMP NSCondition::put_StopLastTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_StopLastTarget = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_WarningDuration(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_WarningDuration;

	return S_OK;
}

STDMETHODIMP NSCondition::put_WarningDuration(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_WarningDuration = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_WarningLatency(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_WarningLatency;

	return S_OK;
}

STDMETHODIMP NSCondition::put_WarningLatency(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_WarningLatency = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_MagnetForce(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_MagnetForce;

	return S_OK;
}

STDMETHODIMP NSCondition::put_MagnetForce(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_MagnetForce = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundUse(SoundType type, BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	BOOL fVal = FALSE;
	switch( type )
	{
		case eSndT_Start: fVal = m_pDB->SoundStart; break;
		case eSndT_Stop: fVal = m_pDB->SoundEnd;  break;
		case eSndT_TargetCorrect: fVal = m_pDB->SoundTargetCorrect;  break;
		case eSndT_TargetWrong: fVal = m_pDB->SoundTargetWrong;  break;
		case eSndT_Penup: fVal = m_pDB->SoundPenup;  break;
		case eSndT_Pendown: fVal = m_pDB->SoundPendown;  break;
		case eSndT_StimPStart: fVal = m_pDB->SoundStimPStart;  break;
		case eSndT_StimPStop: fVal = m_pDB->SoundStimPStop;  break;
		case eSndT_StimWStart: fVal = m_pDB->SoundStimWStart;  break;
		case eSndT_StimWStop: fVal = m_pDB->SoundStimWStop;  break;
	}

	*pVal = fVal;
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundUse(SoundType type, BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStart = newVal; break;
		case eSndT_Stop: m_pDB->SoundEnd = newVal;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrect = newVal;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrong = newVal;  break;
		case eSndT_Penup: m_pDB->SoundPenup = newVal;  break;
		case eSndT_Pendown: m_pDB->SoundPendown = newVal;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStart = newVal;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStop = newVal;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStart = newVal;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStop = newVal;  break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundTone(SoundType type, BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	BOOL fVal = FALSE;
	switch( type )
	{
		case eSndT_Start: fVal = m_pDB->SoundStartTone; break;
		case eSndT_Stop: fVal = m_pDB->SoundEndTone;  break;
		case eSndT_TargetCorrect: fVal = m_pDB->SoundTargetCorrectTone;  break;
		case eSndT_TargetWrong: fVal = m_pDB->SoundTargetWrongTone;  break;
		case eSndT_Penup: fVal = m_pDB->SoundPenupTone;  break;
		case eSndT_Pendown: fVal = m_pDB->SoundPendownTone;  break;
		case eSndT_StimPStart: fVal = m_pDB->SoundStimPStartTone;  break;
		case eSndT_StimPStop: fVal = m_pDB->SoundStimPStopTone;  break;
		case eSndT_StimWStart: fVal = m_pDB->SoundStimWStartTone;  break;
		case eSndT_StimWStop: fVal = m_pDB->SoundStimWStopTone;  break;
	}

	*pVal = fVal;
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundTone(SoundType type, BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStartTone = newVal; break;
		case eSndT_Stop: m_pDB->SoundEndTone = newVal;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrectTone = newVal;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrongTone = newVal;  break;
		case eSndT_Penup: m_pDB->SoundPenupTone = newVal;  break;
		case eSndT_Pendown: m_pDB->SoundPendownTone = newVal;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStartTone = newVal;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStopTone = newVal;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStartTone = newVal;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStopTone = newVal;  break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundToneFreq(SoundType type, short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	short nVal = 0;
	switch( type )
	{
		case eSndT_Start: nVal = m_pDB->SoundStartToneFreq; break;
		case eSndT_Stop: nVal = m_pDB->SoundEndToneFreq;  break;
		case eSndT_TargetCorrect: nVal = m_pDB->SoundTargetCorrectToneFreq;  break;
		case eSndT_TargetWrong: nVal = m_pDB->SoundTargetWrongToneFreq;  break;
		case eSndT_Penup: nVal = m_pDB->SoundPenupToneFreq;  break;
		case eSndT_Pendown: nVal = m_pDB->SoundPendownToneFreq;  break;
		case eSndT_StimPStart: nVal = m_pDB->SoundStimPStartToneFreq;  break;
		case eSndT_StimPStop: nVal = m_pDB->SoundStimPStopToneFreq;  break;
		case eSndT_StimWStart: nVal = m_pDB->SoundStimWStartToneFreq;  break;
		case eSndT_StimWStop: nVal = m_pDB->SoundStimWStopToneFreq;  break;
	}

	*pVal = nVal;
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundToneFreq(SoundType type, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStartToneFreq = newVal; break;
		case eSndT_Stop: m_pDB->SoundEndToneFreq = newVal;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrectToneFreq = newVal;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrongToneFreq = newVal;  break;
		case eSndT_Penup: m_pDB->SoundPenupToneFreq = newVal;  break;
		case eSndT_Pendown: m_pDB->SoundPendownToneFreq = newVal;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStartToneFreq = newVal;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStopToneFreq = newVal;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStartToneFreq = newVal;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStopToneFreq = newVal;  break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundToneDur(SoundType type, short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	short nVal = 0;
	switch( type )
	{
		case eSndT_Start: nVal = m_pDB->SoundStartToneDur; break;
		case eSndT_Stop: nVal = m_pDB->SoundEndToneDur;  break;
		case eSndT_TargetCorrect: nVal = m_pDB->SoundTargetCorrectToneDur;  break;
		case eSndT_TargetWrong: nVal = m_pDB->SoundTargetWrongToneDur;  break;
		case eSndT_Penup: nVal = m_pDB->SoundPenupToneDur;  break;
		case eSndT_Pendown: nVal = m_pDB->SoundPendownToneDur;  break;
		case eSndT_StimPStart: nVal = m_pDB->SoundStimPStartToneDur;  break;
		case eSndT_StimPStop: nVal = m_pDB->SoundStimPStopToneDur;  break;
		case eSndT_StimWStart: nVal = m_pDB->SoundStimWStartToneDur;  break;
		case eSndT_StimWStop: nVal = m_pDB->SoundStimWStopToneDur;  break;
	}

	*pVal = nVal;
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundToneDur(SoundType type, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStartToneDur = newVal; break;
		case eSndT_Stop: m_pDB->SoundEndToneDur = newVal;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrectToneDur = newVal;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrongToneDur = newVal;  break;
		case eSndT_Penup: m_pDB->SoundPenupToneDur = newVal;  break;
		case eSndT_Pendown: m_pDB->SoundPendownToneDur = newVal;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStartToneDur = newVal;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStopToneDur = newVal;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStartToneDur = newVal;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStopToneDur = newVal;  break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundMedia(SoundType type, BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szVal;
	switch( type )
	{
		case eSndT_Start: szVal = m_pDB->SoundStartMedia; break;
		case eSndT_Stop: szVal = m_pDB->SoundEndMedia;  break;
		case eSndT_TargetCorrect: szVal = m_pDB->SoundTargetCorrectMedia;  break;
		case eSndT_TargetWrong: szVal = m_pDB->SoundTargetWrongMedia;  break;
		case eSndT_Penup: szVal = m_pDB->SoundPenupMedia;  break;
		case eSndT_Pendown: szVal = m_pDB->SoundPendownMedia;  break;
		case eSndT_StimPStart: szVal = m_pDB->SoundStimPStartMedia;  break;
		case eSndT_StimPStop: szVal = m_pDB->SoundStimPStopMedia;  break;
		case eSndT_StimWStart: szVal = m_pDB->SoundStimWStartMedia;  break;
		case eSndT_StimWStop: szVal = m_pDB->SoundStimWStopMedia;  break;
	}

	*pVal = szVal.AllocSysString();
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundMedia(SoundType type, BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStartMedia = newVal; break;
		case eSndT_Stop: m_pDB->SoundEndMedia = newVal;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrectMedia = newVal;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrongMedia = newVal;  break;
		case eSndT_Penup: m_pDB->SoundPenupMedia = newVal;  break;
		case eSndT_Pendown: m_pDB->SoundPendownMedia = newVal;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStartMedia = newVal;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStopMedia = newVal;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStartMedia = newVal;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStopMedia = newVal;  break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundTrigger(SoundType type, BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	BOOL fVal = FALSE;
	switch( type )
	{
		case eSndT_Start: fVal = m_pDB->SoundStartTrigger; break;
		case eSndT_Stop: fVal = m_pDB->SoundEndTrigger;  break;
		case eSndT_TargetCorrect: fVal = m_pDB->SoundTargetCorrectTrigger;  break;
		case eSndT_TargetWrong: fVal = m_pDB->SoundTargetWrongTrigger;  break;
		case eSndT_Penup: fVal = m_pDB->SoundPenupTrigger;  break;
		case eSndT_Pendown: fVal = m_pDB->SoundPendownTrigger;  break;
		case eSndT_StimPStart: fVal = m_pDB->SoundStimPStartTrigger;  break;
		case eSndT_StimPStop: fVal = m_pDB->SoundStimPStopTrigger;  break;
		case eSndT_StimWStart: fVal = m_pDB->SoundStimWStartTrigger;  break;
		case eSndT_StimWStop: fVal = m_pDB->SoundStimWStopTrigger;  break;
	}

	*pVal = fVal;
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundTrigger(SoundType type, BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStartTrigger = newVal; break;
		case eSndT_Stop: m_pDB->SoundEndTrigger = newVal;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrectTrigger = newVal;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrongTrigger = newVal;  break;
		case eSndT_Penup: m_pDB->SoundPenupTrigger = newVal;  break;
		case eSndT_Pendown: m_pDB->SoundPendownTrigger = newVal;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStartTrigger = newVal;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStopTrigger = newVal;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStartTrigger = newVal;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStopTrigger = newVal;  break;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

STDMETHODIMP NSCondition::get_SoundScript(SoundType type, short* ScriptType, BSTR *pScript)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	short nVal = 0;
	CString szVal;
	switch( type )
	{
		case eSndT_Start: nVal = m_pDB->SoundStartScriptType; szVal = m_pDB->SoundStartScript; break;
		case eSndT_Stop: nVal = m_pDB->SoundEndScriptType; szVal = m_pDB->SoundEndScript;  break;
		case eSndT_TargetCorrect: nVal = m_pDB->SoundTargetCorrectScriptType; szVal = m_pDB->SoundTargetCorrectScript;  break;
		case eSndT_TargetWrong: nVal = m_pDB->SoundTargetWrongScriptType; szVal = m_pDB->SoundTargetWrongScript;  break;
		case eSndT_Penup: nVal = m_pDB->SoundPenupScriptType; szVal = m_pDB->SoundPenupScript;  break;
		case eSndT_Pendown: nVal = m_pDB->SoundPendownScriptType; szVal = m_pDB->SoundPendownScript;  break;
		case eSndT_StimPStart: nVal = m_pDB->SoundStimPStartScriptType; szVal = m_pDB->SoundStimPStartScript;  break;
		case eSndT_StimPStop: nVal = m_pDB->SoundStimPStopScriptType; szVal = m_pDB->SoundStimPStopScript;  break;
		case eSndT_StimWStart: nVal = m_pDB->SoundStimWStartScriptType; szVal = m_pDB->SoundStimWStartScript;  break;
		case eSndT_StimWStop: nVal = m_pDB->SoundStimWStopScriptType; szVal = m_pDB->SoundStimWStopScript;  break;
	}

	*ScriptType = nVal;
	*pScript = szVal.AllocSysString();
	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundScript(SoundType type, short ScriptType, BSTR Script)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( type )
	{
		case eSndT_Start: m_pDB->SoundStartScriptType = ScriptType; m_pDB->SoundStartScript = Script; break;
		case eSndT_Stop: m_pDB->SoundEndScriptType = ScriptType; m_pDB->SoundEndScript = Script;  break;
		case eSndT_TargetCorrect: m_pDB->SoundTargetCorrectScriptType = ScriptType; m_pDB->SoundTargetCorrectScript = Script;  break;
		case eSndT_TargetWrong: m_pDB->SoundTargetWrongScriptType = ScriptType; m_pDB->SoundTargetWrongScript = Script;  break;
		case eSndT_Penup: m_pDB->SoundPenupScriptType = ScriptType; m_pDB->SoundPenupScript = Script;  break;
		case eSndT_Pendown: m_pDB->SoundPendownScriptType = ScriptType; m_pDB->SoundPendownScript = Script;  break;
		case eSndT_StimPStart: m_pDB->SoundStimPStartScriptType = ScriptType; m_pDB->SoundStimPStartScript = Script;  break;
		case eSndT_StimPStop: m_pDB->SoundStimPStopScriptType = ScriptType; m_pDB->SoundStimPStopScript = Script;  break;
		case eSndT_StimWStart: m_pDB->SoundStimWStartScriptType = ScriptType; m_pDB->SoundStimWStartScript = Script;  break;
		case eSndT_StimWStop: m_pDB->SoundStimWStopScriptType = ScriptType; m_pDB->SoundStimWStopScript = Script;  break;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

STDMETHODIMP NSCondition::get_SoundInfo(SoundCat cat, SoundType type, VARIANT *pVal)
{
	BOOL fVal;
	short nVal;
	BSTR bstr;

	switch( cat )
	{
		case eSC_Use:
			get_SoundUse( type, &fVal );
			VS_BOOL( pVal, fVal );
			break;
		case eSC_Tone:
			get_SoundTone( type, &fVal );
			VS_BOOL( pVal, fVal );
			break;
		case eSC_Frequency:
			get_SoundToneFreq( type, &nVal );
			VS_I2( pVal, nVal );
			break;
		case eSC_Duration:
			get_SoundToneDur( type, &nVal );
			VS_I2( pVal, nVal );
			break;
		case eSC_Media:
			get_SoundMedia( type, &bstr );
			VS_BSTR( pVal, bstr );
			break;
		case eSC_Trigger:
			get_SoundTrigger( type, &fVal );
			VS_BOOL( pVal, fVal );
			break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::put_SoundInfo(SoundCat cat, SoundType type, VARIANT newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	switch( cat )
	{
		case eSC_Use: put_SoundUse( type, V_BOOL( &newVal ) ); break;
		case eSC_Tone: put_SoundTone( type, V_BOOL( &newVal ) ); break;
		case eSC_Frequency: put_SoundToneFreq( type, V_I2( &newVal ) ); break;
		case eSC_Duration: put_SoundToneDur( type, V_I2( &newVal ) ); break;
		case eSC_Media: put_SoundMedia( type, V_BSTR( &newVal ) ); break;
		case eSC_Trigger: put_SoundTrigger( type, V_BOOL( &newVal ) ); break;
	}

	return S_OK;
}

STDMETHODIMP NSCondition::get_SoundCount( short *pVal )
{
	*pVal = 10;
	return S_OK;
}

STDMETHODIMP NSCondition::get_CountStrokes(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->CountStrokes;

	return S_OK;
}

STDMETHODIMP NSCondition::put_CountStrokes(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->CountStrokes = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_HideFeedback(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->HideFeedback;

	return S_OK;
}

STDMETHODIMP NSCondition::put_HideFeedback(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->HideFeedback = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_HideShowAfter(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->HideShowAfter;

	return S_OK;
}

STDMETHODIMP NSCondition::put_HideShowAfter(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->HideShowAfter = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_HideShowAfterShow(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->HideShowAfterShow;

	return S_OK;
}

STDMETHODIMP NSCondition::put_HideShowAfterShow(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->HideShowAfterShow = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_HideShowAfterStrokes(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->HideShowAfterStrokes;

	return S_OK;
}

STDMETHODIMP NSCondition::put_HideShowAfterStrokes(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->HideShowAfterStrokes = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_HideShowCount(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->HideShowCount;

	return S_OK;
}

STDMETHODIMP NSCondition::put_HideShowCount(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->HideShowCount = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Transform(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Transform;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Transform(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Transform = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Xgain(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Xgain;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Xgain(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Xgain = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Xrotation(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Xrotation;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Xrotation(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Xrotation = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Ygain(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Ygain;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Ygain(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Ygain = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Yrotation(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Yrotation;

	return S_OK;
}

STDMETHODIMP NSCondition::put_Yrotation(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->Yrotation = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP NSCondition::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP NSCondition::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

STDMETHODIMP NSCondition::get_FlagBadTarget(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->flagbadtarget;

	return S_OK;
}

STDMETHODIMP NSCondition::put_FlagBadTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->flagbadtarget = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_Feedback(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->Feedback.AllocSysString();

	return S_OK;
}

STDMETHODIMP NSCondition::put_Feedback(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->Feedback = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::get_GripperTask(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->GripperTask;

	return S_OK;
}

STDMETHODIMP NSCondition::put_GripperTask(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->GripperTask = newVal;

	return S_OK;
}

STDMETHODIMP NSCondition::SetFeedbackInfo( short Which, BOOL Use, short Column,
										   short Stroke, double Min, double Max,
										   BOOL SwapColors )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB )
	{
		if( Which == 1 )
		{
			m_pDB->TrialFeedback1 = Use;
			m_pDB->FBColumn1 = Column;
			m_pDB->FBStroke1 = Stroke;
			m_pDB->FBMin1 = Min;
			m_pDB->FBMax1 = Max;
			m_pDB->FBSwapColors1 = SwapColors;
		}
		else if( Which == 2 )
		{
			m_pDB->TrialFeedback2 = Use;
			m_pDB->FBColumn2 = Column;
			m_pDB->FBStroke2 = Stroke;
			m_pDB->FBMin2 = Min;
			m_pDB->FBMax2 = Max;
			m_pDB->FBSwapColors2 = SwapColors;
		}
	}

	return S_OK;
}

STDMETHODIMP NSCondition::GetFeedbackInfo( short Which, BOOL* Use, short* Column,
										   short* Stroke, double* Min, double* Max,
										   BOOL* SwapColors )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if( m_pDB )
		{
			if( Which == 1 )
			{
				*Use = m_pDB->TrialFeedback1;
				*Column = m_pDB->FBColumn1;
				*Stroke = m_pDB->FBStroke1;
				*Min = m_pDB->FBMin1;
				*Max = m_pDB->FBMax1;
				*SwapColors = m_pDB->FBSwapColors1;
			}
			else if( Which == 2 )
			{
				*Use = m_pDB->TrialFeedback2;
				*Column = m_pDB->FBColumn2;
				*Stroke = m_pDB->FBStroke2;
				*Min = m_pDB->FBMin2;
				*Max = m_pDB->FBMax2;
				*SwapColors = m_pDB->FBSwapColors2;
			}
		}

	return S_OK;
}

STDMETHODIMP NSCondition::GetNumRecords( ULONGLONG* Cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	if( m_pDB ) *Cnt = m_pDB->GetNumRecords();
	return S_OK;
}

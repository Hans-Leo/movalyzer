#if !defined(AFX_FEEDBACKDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_FEEDBACKDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FEEDBACKDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBFeedback.h"

/////////////////////////////////////////////////////////////////////////////
// FEEDBACKDB recordset

class FeedbackDB : public CTDataDB
{
public:
	FeedbackDB();

// Field/Param Data
	CString	m_FeedbackID;
	CString	m_Desc;
	CString	m_Feature;
	DWORD	m_MinColor;
	DWORD	m_MaxColor;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pFEEDBACK );
	BOOL SetAll( pFEEDBACK );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FEEDBACKDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)

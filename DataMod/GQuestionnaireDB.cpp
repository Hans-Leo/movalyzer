// GQuestionnaireDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GQuestionnaireDB.h"

DBGQuestionnaire _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GQuestionnaireDB

GQuestionnaireDB::GQuestionnaireDB() : CTDataDB( &_db )
{
	//{{AFX_FIELD_INIT(GQuestionnaireDB)
	m_ExpID = _T("");
	m_GrpID = _T("");
	m_ID = _T("");
	m_ItemNum = 0;
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::GetAll( pGQUESTIONNAIRE pQuestionnaire )
{
	if( !pQuestionnaire ) return FALSE;

	m_ExpID = pQuestionnaire->ExpID;
	m_GrpID = pQuestionnaire->GrpID;
	m_ID = pQuestionnaire->ID;
	m_ItemNum = atoi( pQuestionnaire->ItemNum );

	return TRUE;
}

BOOL GQuestionnaireDB::SetAll( pGQUESTIONNAIRE pQuestionnaire )
{
	if( !pQuestionnaire ) return FALSE;

	strncpy_s( pQuestionnaire->ExpID, szIDS + 1, m_ExpID, _TRUNCATE );
	strncpy_s( pQuestionnaire->GrpID, szIDS + 1, m_GrpID, _TRUNCATE );
	strncpy_s( pQuestionnaire->ID, szIDS + 1, m_ID, _TRUNCATE );
	sprintf_s( pQuestionnaire->ItemNum, _T("%d"), m_ItemNum );;

	return TRUE;
}

BOOL GQuestionnaireDB::Find( CString szExpID, CString szGrpID )
{
	CString szErr;

	if( !CheckOpen( szErr ) ) return FALSE;

	pGQUESTIONNAIRE pQuestionnaire = _db.Find( szExpID, szGrpID, szErr );
	if( !pQuestionnaire )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pQuestionnaire );
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::Find( CString szExpID, CString szGrpID, CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pGQUESTIONNAIRE pQuestionnaire = _db.Find( szExpID, szGrpID, szID, szErr );
	if( !pQuestionnaire )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pQuestionnaire );
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GQUESTIONNAIRE quest;
	SetAll( &quest );
	if( !AddItem( (pDBDATA)&quest, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GQUESTIONNAIRE quest;
	SetAll( &quest );
	if( !ModifyItem( (pDBDATA)&quest, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_GrpID, m_ID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::Remove( CString szExpID, CString szGrpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( szExpID, szGrpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::RemoveQuestion( CString szID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.RemoveQuestion( szID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::RemoveGroup( CString szGrpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.RemoveGroup( szGrpID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void GQuestionnaireDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GQUESTIONNAIRE quest;
	if( GetFirstItem( (pDBDATA)&quest, szErr ) )
		GetAll( &quest );
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GQUESTIONNAIRE quest;
	if( GetNextItem( (pDBDATA)&quest, szErr ) )
	{
		GetAll( &quest );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL GQuestionnaireDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	GQUESTIONNAIRE quest;
	if( GetPreviousItem( (pDBDATA)&quest, szErr ) )
	{
		GetAll( &quest );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// MQuestionnaire.h: Definition of the Questionnaire class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MQUESTIONNAIRE_H__6BC7F959_1612_11D4_8B4C_00104BC7E2C8__INCLUDED_)
#define AFX_MQUESTIONNAIRE_H__6BC7F959_1612_11D4_8B4C_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "MQuestionnaireDB.h"

/////////////////////////////////////////////////////////////////////////////
// MQuestionnaire

class MQuestionnaire : 
	public IDispatchImpl<IMQuestionnaire, &IID_IMQuestionnaire, &LIBID_DATAMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<MQuestionnaire,&CLSID_MQuestionnaire>,
	public NSDataObject
{
public:
	MQuestionnaire();
	~MQuestionnaire();

BEGIN_COM_MAP(MQuestionnaire)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IMQuestionnaire)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(MQuestionnaire) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_MQuestionnaire)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IQuestionnaire
public:
	STDMETHOD(GetNumRecords)(/*[out,retval]*/ ULONGLONG* Cnt);
	STDMETHOD(RemoveTemp)();
	STDMETHOD(DumpRecord)(/*[in]*/ BSTR ID, /*[in]*/ BSTR OutFile);
	STDMETHOD(SetTemp)(/*[in]*/ BOOL Temp);
	STDMETHOD(ForceDefault)();
	STDMETHOD(ForceLocal)();
	STDMETHOD(ForceRemote)();
	STDMETHOD(SetDataPath)(/*[in]*/ BSTR Path);
	STDMETHOD(GetPrevious)();
	STDMETHOD(GetNext)();
	STDMETHOD(ResetToStart)();
	STDMETHOD(Remove)();
	STDMETHOD(Modify)();
	STDMETHOD(Add)();
	STDMETHOD(Find)(/*[in]*/ BSTR ID);
	STDMETHOD(get_ColumnHeader)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ColumnHeader)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_IsNumeric)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_IsNumeric)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_Question)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Question)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ItemNum)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_ItemNum)(/*[in]*/ short newVal);
	STDMETHOD(get_IsPrivate)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_IsPrivate)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_IsHeader)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_IsHeader)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ID)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ID)(/*[in]*/ BSTR newVal);
protected:
	MQuestionnaireDB*	m_pDB;
};

#endif // !defined(AFX_MQUESTIONNAIRE_H__6BC7F959_1612_11D4_8B4C_00104BC7E2C8__INCLUDED_)

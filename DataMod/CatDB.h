#if !defined(AFX_CATDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)
#define AFX_CATDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CatDB.h : header file
//

#include "CTDataDB.h"
#include "..\CTreeLink\DBCat.h"

/////////////////////////////////////////////////////////////////////////////
// CatDB recordset

class CatDB : public CTDataDB
{
public:
	CatDB();

// Field/Param Data
	CString	m_CatID;
	CString	m_Desc;
	int		m_Type;

// Implementation
public:
	BOOL Find( CString szID );
	BOOL Add();
	BOOL Modify();
	BOOL Remove();
	void ResetToStart();
	BOOL GetNext();
	BOOL GetPrevious();
	void ForceRemote();
	void ForceLocal();
	void ForceDefault();
protected:
	BOOL GetAll( pCAT );
	BOOL SetAll( pCAT );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CATDB_H__DCB05ED4_A758_11D3_8A58_000000000000__INCLUDED_)

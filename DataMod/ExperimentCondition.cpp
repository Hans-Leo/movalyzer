// ExperimentCondition.cpp : Implementation of CDataModApp and DLL registration.

#include "stdafx.h"
#include "DataMod.h"
#include "ExpConditionDB.h"
#include "ExperimentCondition.h"

/////////////////////////////////////////////////////////////////////////////
//

ExperimentCondition::ExperimentCondition()  : m_pDB( NULL )
{
	m_pDB = new ExpConditionDB;
}

ExperimentCondition::~ExperimentCondition()
{
	if( m_pDB ) delete m_pDB;
}

STDMETHODIMP ExperimentCondition::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IExperimentCondition,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP ExperimentCondition::get_ExperimentID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_ExpID.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentCondition::put_ExperimentID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_ExpID = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::get_ConditionID(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_CondID.AllocSysString();

	return S_OK;
}

STDMETHODIMP ExperimentCondition::put_ConditionID(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_CondID = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::get_Replications(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_pDB->m_Replications;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::put_Replications(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->m_Replications = newVal;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::Find(BSTR ExpID, BSTR CondID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ExpID;
	CString szCondID = CondID;
	if( !m_pDB->Find( szID, szCondID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::FindForExperiment(BSTR ExpID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString szID = ExpID;
	if( !m_pDB->Find( szID ) ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::Add()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Add() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::Modify()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Modify() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::Remove()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->Remove() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::ResetToStart()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_pDB->ResetToStart();
	if( m_pDB->m_ExpID.IsEmpty() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::GetNext()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetNext() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::GetPrevious()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pDB->GetPrevious() ) return E_FAIL;

	return S_OK;
}

STDMETHODIMP ExperimentCondition::SetDataPath(BSTR Path)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->SetDataPath( Path );

	return S_OK;
}

STDMETHODIMP ExperimentCondition::ForceRemote()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceRemote();

	return S_OK;
}

STDMETHODIMP ExperimentCondition::ForceLocal()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceLocal();

	return S_OK;
}

STDMETHODIMP ExperimentCondition::ForceDefault()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pDB ) m_pDB->ForceDefault();

	return S_OK;
}

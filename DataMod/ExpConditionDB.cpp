// ExpConditionDB.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ExpConditionDB.h"

DBExperimentCondition _db;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExpConditionDB

ExpConditionDB::ExpConditionDB() : CTDataDB(&_db)
{
	//{{AFX_FIELD_INIT(ExpConditionDB)
	m_ExpID = _T("");
	m_CondID = _T("");
	m_Replications = 3;
	//}}AFX_FIELD_INIT
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::GetAll( pEXPERIMENTCONDITION pEC )
{
	if( !pEC ) return FALSE;

	m_ExpID = pEC->ExpID;
	m_CondID = pEC->CondID;
	m_Replications = pEC->Replications;

	return TRUE;
}

BOOL ExpConditionDB::SetAll( pEXPERIMENTCONDITION pEC )
{
	if( !pEC ) return FALSE;

	strncpy_s( pEC->ExpID, szIDS + 1, m_ExpID, _TRUNCATE );
	strncpy_s( pEC->CondID, szIDS + 1, m_CondID, _TRUNCATE );
	pEC->Replications = m_Replications;

	return TRUE;
}

BOOL ExpConditionDB::Find( CString szExpID, CString szCondID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pEXPERIMENTCONDITION pEC = _db.Find( szExpID, szCondID, szErr );
	if( !pEC )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pEC );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::Find( CString szExpID )
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	pEXPERIMENTCONDITION pEC = _db.Find( szExpID, szErr );
	if( !pEC )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return GetAll( pEC );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::Add()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTCONDITION ec;
	SetAll( &ec );
	if( !AddItem( (pDBDATA)&ec, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::Modify()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTCONDITION ec;
	SetAll( &ec );
	if( !ModifyItem( (pDBDATA)&ec, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::Remove()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	if( !CheckOpen( szErr ) ) return FALSE;

	if( !_db.Remove( m_ExpID, m_CondID, szErr ) )
	{
		OutputMessage( szErr, LOG_DB );
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpConditionDB::ResetToStart()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTCONDITION ec;
	if( GetFirstItem( (pDBDATA)&ec, szErr ) )
		GetAll( &ec );
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::GetNext()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTCONDITION ec;
	if( GetNextItem( (pDBDATA)&ec, szErr ) )
	{
		GetAll( &ec );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

BOOL ExpConditionDB::GetPrevious()
{
	CString szErr;
	szErr.GetBufferSetLength( 100 );

	EXPERIMENTCONDITION ec;
	if( GetPreviousItem( (pDBDATA)&ec, szErr ) )
	{
		GetAll( &ec );
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpConditionDB::ForceRemote()
{
	_db.ForceOperationMode( CTOP_REMOTE );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpConditionDB::ForceLocal()
{
	_db.ForceOperationMode( CTOP_LOCAL );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

/////////////////////////////////////////////////////////////////////////////

void ExpConditionDB::ForceDefault()
{
	_db.ForceOperationMode( CTOP_DEFAULT );

	CString szErr;
	_db.Close( szErr );
	m_fOpen = FALSE;
}

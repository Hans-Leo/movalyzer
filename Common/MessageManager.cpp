// MessageManager.cpp

#include <stdafx.h>
#include "MessageManager.h"
#include "MessageTarget.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// global message manager
MessageManager*	_pMM = NULL;

///////////////////////////////////////////////////////////////////////////////

BOOL MessageManager::Create()
{
	if( _pMM ) return TRUE;
	_pMM = new MessageManager();
	return( _pMM != NULL );
}

void MessageManager::Destroy()
{
	if( _pMM ) delete _pMM;
}

MessageManager* MessageManager::GetMessageManager()
{
	if( !_pMM ) MessageManager::Create();
	return _pMM;
}

MessageManager::~MessageManager()
{
	m_lstItems.RemoveAll();
}

void MessageManager::PostMessage( LPARAM msg, WPARAM val )
{
	MessageTarget* pMT = NULL;
	POSITION pos = m_lstItems.GetHeadPosition();
	BOOL fStop = FALSE;
	while( pos && !fStop )
	{
		pMT = (MessageTarget*)m_lstItems.GetNext( pos );
		if( pMT ) pMT->Message( msg, val, &fStop );
	}
}

BOOL MessageManager::Add( MessageTarget* pMT )
{
	if( pMT )
	{
		m_lstItems.AddTail( (CObject*)pMT );
		return TRUE;
	}

	return FALSE;
}

BOOL MessageManager::Remove( MessageTarget* pMT )
{
	if( pMT )
	{
		MessageTarget* pMTCheck = NULL;
		POSITION pos = m_lstItems.GetHeadPosition(), lastPos = NULL;
		while( pos )
		{
			lastPos = pos;
			pMTCheck = (MessageTarget*)m_lstItems.GetNext( pos );
			if( pMTCheck == pMT )
			{
				m_lstItems.RemoveAt( lastPos );
				return TRUE;
			}
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

MessageTarget::MessageTarget()
{
	MessageManager* pMM = MessageManager::GetMessageManager();
	if( pMM ) pMM->Add( this );
}

void MessageTarget::Cleanup()
{
	MessageManager* pMM = MessageManager::GetMessageManager();
	if( pMM ) pMM->Remove( this );
}

///////////////////////////////////////////////////////////////////////////////

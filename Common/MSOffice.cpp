#include "StdAfx.h"
#include "MSOffice.h"

// Office Versions
#define	OFFICE_97		1
#define	OFFICE_2000		2
#define	OFFICE_2002		3
#define	OFFICE_2007		4

// Default Settings
#define OFFICE_VER OFFICE_2007

// Paths to required MS OFFICE files.
#if		OFFICE_VER == OFFICE_2007
#define _MSDLL_PATH "C:\Program Files\Common Files\Microsoft Shared\Office12\MSO.DLL"
#define	_MSVBA_PATH "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define	_MSEXL_PATH "C:\Program Files\Microsoft Office\Office12\EXCEL.EXE"
#elif	OFFICE_VER == OFFICE_2002
#define _MSDLL_PATH "C:\Program Files\Common Files\Microsoft Shared\Office11\MSO.DLL"
#define	_MSVBA_PATH "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define	_MSEXL_PATH "C:\Program Files\Microsoft Office\Office11\EXCEL.EXE"
#elif	OFFICE_VER == OFFICE_2000
#define _MSDLL_PATH "C:\Program Files\Microsoft Office\Office\MSO9.dll"
#define	_MSVBA_PATH "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define	_MSEXL_PATH "C:\Program Files\Microsoft Office\Office12\EXCEL9.olb"
#elif	OFFICE_VER == OFFICE_1997
#define _MSDLL_PATH "C:\Program Files\Microsoft Office\Office\MSO8.dll"
#define	_MSVBA_PATH "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define	_MSEXL_PATH "C:\Program Files\Microsoft Office\Office12\EXCEL8.olb"
#endif

// Delete the *.tlh files when changing import qualifiers
//#import _MSDLL_PATH rename("RGB", "MSRGB") rename("DocumentProperties", "WordDocumentProperties") raw_interfaces_only
#import _MSDLL_PATH auto_rename
#import _MSVBA_PATH auto_rename
#import _MSEXL_PATH exclude( "IFont", "IPicture" ) auto_rename


// EXCEL
BOOL Excel::OpenDelimitedText( CString szFile, UINT uiFormat, BOOL fTab, BOOL fSemicolon, BOOL fComma, BOOL fSpace )
{
	try
	{
		Excel::_ApplicationPtr app( __uuidof( Excel::Application ) );
		if( !app ) return FALSE;
		app->Workbooks->OpenText( szFile.AllocSysString(),
								  xlWindows,
								  (_variant_t)1,
								  xlDelimited,
								  xlTextQualifierDoubleQuote,
								  VARIANT_TRUE,
								  fTab ? VARIANT_TRUE : VARIANT_FALSE,
								  fSemicolon ? VARIANT_TRUE : VARIANT_FALSE,
								  fComma ? VARIANT_TRUE : VARIANT_FALSE,
								  fSpace ? VARIANT_TRUE : VARIANT_FALSE  );

		app->put_Visible( 0, VARIANT_TRUE );
		app->put_UserControl( TRUE );

		if( uiFormat == EXCEL_FORMAT_SUBJECTREPORT )
		{
			Excel::_WorksheetPtr ws = app->ActiveSheet;
			if( !ws ) return TRUE;
			Excel::RangePtr cell = ws->Cells->GetItem( 10, 10 );
			if( !cell ) return TRUE;
			cell->PutColumnWidth( 16 );
			cell = ws->Cells->GetItem( 12, 12 );
			cell->PutColumnWidth( 16 );
		}
		else if( uiFormat == EXCEL_FORMAT_QUESTIONNAIRE )
		{
			Excel::_WorksheetPtr ws = app->ActiveSheet;
			if( !ws ) return TRUE;
			Excel::RangePtr cell = ws->Cells->GetItem( 1, 1 );
			if( !cell ) return TRUE;
			cell->PutColumnWidth( 25 );
			cell = ws->Cells->GetItem( 2, 2 );
			cell->PutColumnWidth( 25 );
		}
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

#pragma once

// define version information for COM/DCOM DLL modules
#define	ADMINMOD_HELP		"AdminElevate 1.3 Type Library" // 1.3
#define	ADMINMOD_VER		1.3

#define	DATAMOD_HELP		"DataMod 5.2 Type Library" // 5.2
#define	DATAMOD_VER			4.5

#define	INPUTMOD_HELP		"InputMod 4.6 Type Library" //4.6
#define	INPUTMOD_VER		4.6

#define	PROCMOD_HELP		"ProcessMod 5.4 Type Library" // 5.4
#define	PROCMOD_VER			5.4

#define	SHOWMOD_HELP		"ShowMod 4.9 Type Library" // 4.9
#define	SHOWMOD_VER			4.9

#define SERVICES_HELP		"NSServices 1.0 Type Library"
#define SERVICES_VER		1.0

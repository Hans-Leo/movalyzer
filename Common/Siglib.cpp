/*************************************************
   Siglib - version 2.0 - hlt - 8 Sep 1993
**************************************************

   Purpose: Signal analysis routines

   Updates from vhs:
    - DC removal in rwin removed
    - Brakew improved, although still disabled as nw = 0

	 18Sep06: GMB: Converted deprecated string/file functions for MSVS7
************************************************************************/

#include "siglib.h"
#include "deffun.h"
#include <math.h> /* because of log, cos, sqrt, ... */

#if defined __PROCESSMOD__
#include "..\ProcessMod\Resource.h"
#elif defined __MOVALYZER__
#include "..\Test\Resource.h"
#endif

/*************************************************************************/
double zmedian( double* z,int i )
/*************************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( z != NULL );

	double o[3], tmp;
	int j, swap;

	for(j = 0; j < 3; ++j) {
	   o[j] = z[i+j-1];
	}
	swap = 1;
	while(swap) {
		swap = 0;
		for(j = 0; j < 2; ++j) {
			if(o[j] > o[j+1]) {
				swap = 1;
				tmp = o[j+1];
				o[j+1] = o[j];
				o[j] = tmp;
			}
		}
	}
	return(o[1]);
}

/*************************************************************************/
void rwin(double* x, double* y, int ns1, int ndecl, int* ns2, int* nfft,
		  int nw1, double alpha)
/*************************************************************************
  hans-leo teulings

  Append sine wave after ns1 samples till array element ndecl (should be power
  of 2) or if requested till element nfft which is the optimal number of
  points for the FFT.
  ndecl should be at least 20% larger than ns1 otherwise ns1 will be truncated
  till ns2.

  Input:
  x,y   = arrays
  ns1   = #relevant samples
  ndecl  = array size (should be power of 2)
  Output:
  ns2   = #relevant samples after clipping (optional)
  nfft  = optimal #points for FFT (optional; if not requested nfft =ndecl)
  Input:
  nw1   = number of samples at begin and end for brake window (optional;
	    default=0)
  alpha = braking factor (speed decreases by alpha, alpha**2, alpha**3 ... for
	    samples nw, nw-1, ... 1 and for samples ns-nw+1, ns-nw+2, ... ns)
	    (optional; default=.5)
******************************************************************************/
  {
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( ns2 != NULL );
	ASSERT( nfft != NULL );

    int ns, nw, i;
    double xd, yd, space, tw;

    /*  Optimal nfft */
    space=1.25;
    *nfft = (1 << (int) (log(space * MAX(ns1,1))/log(2.0)+0.9999));
    /* updated by hlt */
    if(*nfft>ndecl) {
		CString szMsg;
        // TODO: `IDS_RWIN_1` comes from processmod - redo w/string table
        // temporarily replacing with int 1
		szMsg.Format(1/*IDS_RWIN_1*/, ns1, *nfft, ndecl, *nfft/2);
		OutputMessage( szMsg, LOG_PROC );
		*nfft /= 2;
    }

    /*  Clip - 8 seconds at 105 Hz is still possible in 1024 samples */
    /* moved by hlt from above optimal fft */
    ns=MIN(ns1,(int)((double)*nfft/1.25));
    if (ns < ns1)
	{
		CString szMsg;
        // TODO: `IDS_RWIN_2` comes from processmod - redo w/string table
        // temporarily replacing with int 1
		szMsg.Format(1/*IDS_RWIN_2*/, ns1, *nfft, ns);
		OutputMessage( szMsg, LOG_PROC );
	}
    *ns2=ns;

/*  Brake movement at begin and end */

    nw=nw1;
    brakew(x,ns,nw,alpha);
    brakew(y,ns,nw,alpha);

/*  Append sine wave */

	// 10Sep03: GMB - Added asserts to ensure validity of indices
//	ASSERT( ( ns - 1 ) >= 0 );
	if( ns < 1 ) return;

	xd = x[ ns - 1 ] - x[ 0 ];
	yd = y[ ns - 1 ] - y[ 0 ];
    for (i=ns-1; i < *nfft; i++)
    {
      tw=0.5*(1.0+(double) cos(PI*(i+1-ns)/(*nfft-ns)));
      x[i] = x[0]+xd*tw;
      y[i] = y[0]+yd*tw;
    }

  } /* End rwin */


/*************************************************************************/
void brakew(double* x, int ns, int nw, double alpha)
/******************************************************************************
Rounds sharp velocity change where the signal and the sine meet.

25 Aug 93: hlt: For2c bug fixed: i was running from 1
******************************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );

    int i, ii;
    double f;


    if (ns > 2 * nw && nw > 0)
    {
      for (i = 0; i < nw; i++)
      {
		x [i]  = x [i+1] - x [i];
		ii     = ns - 1 - i;
		x [ii] = x [ii-1] - x [ii];
      }
      f = 1.0;
      for (i = 0; i < nw; i++)
      {
		f               *= alpha;
		x [nw - 1 - i]  *= f;
		x [ns - nw + i] *= f;
      }
      for (i = 0; i < nw; i++)
      {
		ii     = nw - 1 - i;
		x [ii] = x [ii+1] - x [ii];
		ii     = ns - nw + i;
		x [ii] = x [ii-1] - x [ii];
      }
    }
} /* End brakew */

/*************************************************************************/
double rcos[14] = { 0.0,
		   0.0,
		   0.70710683, 0.92387956, 0.98078537,
		   0.99518484, 0.99879545, 0.99969894,
		   0.99992478, 0.99998128, 0.99999535,
		   0.99999893, 0.99999976, 1.0 };
double rsin[14] = { 0.0,
		   1.0,
		   0.70710683e-0, 0.38268346e-0, 0.19509032e-0,
		   0.98017141e-1, 0.49067676e-1, 0.24541229e-1,
		   0.12271538e-1, 0.61358847e-2, 0.30679568e-2,
		   0.15339802e-2, 0.76699036e-3, 0.38349521e-3 };
/*************************************************************************/

/*************************************************************************/
void fftc (double* a, double* b, int n, int ind) {
/******************************************************************************
 fast fourier transform of complex signal.

 a     = real part of complex signal.
 b     = imaginair part of complex signal.
 n     = length of complex signal.
 ind   = indicates direction of transformation.
	 +1 : forward.
	 -1 : backward.

******************************************************************************/

	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( a != NULL );
	ASSERT( b != NULL );

    int nr, n2ou, kv, kr, iou, iin, n2oum2, iinb;
    double rind, bcos, bsin, rc, rs, ta, tb;

    --a;
    --b;

    /* shufle the data and initialise. */
    fftshf (a,b,n);
    rind = -ind;
    n2ou = 1;
    nr   = 1;
    bcos = -1.0;
    bsin = 0.0;

    /* 2log(n) stages. */
    do {
        n2oum2 = n2ou*2;
        rc     = 1.0;
        rs     = 0.0;
        kv     = 1;
        kr     = nr;

        /* combine n/2 pairs. */
        for (iou=1; iou<=n2ou; iou++) {
            for (iin=iou; iin<=n; iin+=n2oum2) {
                iinb     = iin+n2ou;
                ta       = rc * a[iinb] - rs * b[iinb];
                tb       = rc * b[iinb] + rs * a[iinb];
                a[iinb]  = a[iin]-ta;
                b[iinb]  = b[iin]-tb;
                a[iin]  += ta;
                b[iin]  += tb;
            }
    	    if(iou < n2ou) {
	            if(iou < kv) {
                    /* calculate new cosinus and sinus. */
	                tb = rc*bsin;
	                rc = rc*bcos-rs*bsin;
	                rs = tb+rs*bcos;
	            }
                else if (iou==kv) {
                    /* get new cosinus and sinus from table */
	                rc  = rcos[--kr];
                    rs  = rsin[kr]*rind;
                    kv *= 2;
                }
                else {
                    // TODO: `IDS_FFTC_1` comes from processmod - redo w/string table
                    // temporarily replacing with int 1
                    OutputMessage(1/*IDS_FFTC_1*/, LOG_PROC);
                    return;
                }
	        }
        }
        if (n2oum2 < n) {
            /* prepare next stage. */
	        n2ou  = n2oum2;
            bcos  = rcos[nr];
            bsin  = rsin[nr++]*rind;
        }
        else if (n2oum2 == n) {
	        return;
        }
        else {
            // TODO: `IDS_FFTC_2` comes from processmod - redo w/string table
            // temporarily replacing with int 1
	        OutputMessage(1/*IDS_FFTC_2*/, LOG_PROC);
	        return;
        }
    } while (TRUE);
} /* End fftc */


/*************************************************************************/
void fftshf (double* a, double* b, int n)
/******************************************************************************
 shufle for a fast fourier transform

 a     = real part of complex signal.
 b     = imaginair part of complex signal.
 n     = aantal punten van complex signal.

******************************************************************************/
  {
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( a != NULL );
	ASSERT( b != NULL );

    double tmp;
    int i, j, m;


/* check for power of two up to 15. */

    if (!ffttst(n))
    {
		CString szMsg;
        // TODO: `IDS_FFTC_3` comes from processmod - redo w/string table
        // temporarily replacing with int 1
		szMsg.Format(1/*IDS_FFTC_3*/, n);
		OutputMessage( szMsg, LOG_PROC );
		return;
    }

/* qruexi (4) puts the data in bit-reversed order. */

    j=1;
    for (i=1; i<=n; i++)
    {

/* at this point, i and j are a bit reversed pair (except for the
   displacement of +1) */

      if(i < j)
      {
/* exchange data. */

	tmp  = a[j];
	a[j] = a[i];
	a[i] = tmp;
	tmp  = b[j];
	b[j] = b[i];
	b[i] = tmp;
      }

/* implement j=j+1, bit-reversed counter. */

      m = n/2;
      while (j > m)
      {
	j -= m++;
	m /= 2;
      }
      j += m;
    }
  } /* End fftshf */


/*************************************************************************/
int ffttst (int n)
/*************************************************************************/
  {
    int i, nn;
    nn=1;
    for (i=4; i<=15; i++)
    {
      nn *= 2;
      if(nn==n) return(TRUE);
    }
    return(FALSE);
  } /* End ffttst */


/*************************************************************************/
void rlowps(double* a,double* b,int n,double nc,double nwh)
/******************************************************************************
 hlt

 low-pass frequency window with sinusiod transition band
 between nc-nwh and nc+nwh (unit=sampling rate/n)

  input/output:
  a,b   = double arrays with real and imaginary part of the spectrum
  n     = length of a and b
  nc,nwh= transition-band parameters
******************************************************************************/
  {
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( a != NULL );
	ASSERT( b != NULL );

	// 07Oct08: Somesh: nmh, nml, nc made floating point
	// nmh_index, nml_index truncated integer counterparts of nmh, nml.
	// FFT sample indexing fixed by Hans. Now filtering is more accurate.
    int i, n1, n2, nml_index, nmh_index, ni;
    double om, cosf, fl, nml, nmh;

    n1  = n+2;
    n2  = n/2;
	// 07Oct08: Somesh: The following is an implicit test for nc to be within 2 and n2
    nml = MIN(MAX(2,nc-nwh),n2);
    nmh = MIN(MAX(2,nc+nwh),n2);
	nml_index = (int) ceil(nml);
	nmh_index = (int) floor(nmh);

    if (nml_index < nmh_index)
    {
      om = PI/(nmh-nml);
      for (i=nml_index; i<=nmh_index; i++)
      {

		cosf   = cos(om*(i-nml));
		fl     = 0.5*(1.0+cosf);
	    // 10Sep03: GMB - Added assert to ensure validity of indices
	    ASSERT( ( i ) >= 0 );
		a[i]  *= fl;
		b[i]  *= fl;
		ni     = n-i;
	    // 10Sep03: GMB - Added assert to ensure validity of indices
		ASSERT( ( ni - 1 ) >= 0 );
		a[ni] *= fl;
		b[ni] *= fl;
      }
    }
    if (n-nmh_index > 0)
    {
      for (i=nmh_index; i<n - nmh_index; i++)
      {
		a[i] = 0;
		b[i] = 0;
      }
    }
  }

/* FILTER INFORMATION STRUCTURE FOR FILTER ROUTINES */
typedef struct
{
    unsigned int length;        /* size of filter */
    double *history;            /* pointer to history in filter */
    double *coef;               /* pointer to coefficients of filter */
} FILTER;

#define FILTER_SECTIONS   2   /* 2 filter sections */

typedef struct
{
    double a0, a1, a2;       /* numerator coefficients */
    double b0, b1, b2;       /* denominator coefficients */
} BIQUAD;

BIQUAD ProtoCoef[FILTER_SECTIONS];      /* Filter prototype coefficients,
                                                     1 for each filter section */
void bilinear(double a0, double a1, double a2, double b0, double b1, double b2, double *k, double fs, double *coef)
{
	if( !k || !coef ) return;

    double ad, bd;

    /* alpha (Numerator in s-domain) */
    ad = 4. * a2 * fs * fs + 2. * a1 * fs + a0;
    /* beta (Denominator in s-domain) */
    bd = 4. * b2 * fs * fs + 2. * b1* fs + b0;

    /* update gain constant for this section */
    *k *= ad/bd;

    /* Denominator */
    *coef++ = (2. * b0 - 8. * b2 * fs * fs)/ bd;         /* beta1 */
    *coef++ = (4. * b2 * fs * fs - 2. * b1 * fs + b0)/ bd; /* beta2 */

    /* Nominator */
    *coef++ = (2. * a0 - 8. * a2 * fs * fs)/ ad;         /* alpha1 */
    *coef = (4. * a2 * fs * fs - 2. * a1 * fs + a0)/ad;   /* alpha2 */
}
void prewarp(double *a0, double *a1, double *a2, double fc, double fs)
{
	if( !a0 || !a1 || !a2 ) return;

    double wp, pi;

    pi = 4.0 * atan(1.0);
    wp = 2.0 * fs * tan(pi * fc / fs);

    *a2 = (*a2) / (wp * wp);
    *a1 = (*a1) / wp;
}

void szxform( double *a0, double *a1, double *a2, double *b0, double *b1, double *b2, double fc, double fs, double *k, double *coef)
{
	if( !a0 || !a1 || !a2 || !b0 || !b1 || !b2 ) return;

    /* Calculate a1 and a2 and overwrite the original values */
    prewarp(a0, a1, a2, fc, fs);
    prewarp(b0, b1, b2, fc, fs);
    bilinear(*a0, *a1, *a2, *b0, *b1, *b2, k, fs, coef);
}
double iir_filter(double input, FILTER *iir)
{
	if( !iir ) return 0.;

    unsigned int i;
    double *hist1_ptr = NULL, *hist2_ptr = NULL, *coef_ptr = NULL;
    double output,new_hist,history1,history2;

/* allocate history array if different size than last call */

    if(!iir->history)
	{
		// caller will be responsible for cleanup if not initialized
		iir->history = new double[ 2 * iir->length ];
		memset( iir->history, 0, 2 * iir->length * sizeof( double ) );
        if(!iir->history) return 0.;
    }

    coef_ptr = iir->coef;                /* coefficient pointer */

    hist1_ptr = iir->history;            /* first history */
    hist2_ptr = hist1_ptr + 1;           /* next history */

        /* 1st number of coefficients array is overall input scale factor,
         * or filter gain */
    output = input * (*coef_ptr++);

    for (i = 0 ; i < iir->length; i++)
        {
        history1 = *hist1_ptr;           /* history values */
        history2 = *hist2_ptr;

        output = output - history1 * (*coef_ptr++);
        new_hist = output - history2 * (*coef_ptr++);    /* poles */

        output = new_hist + history1 * (*coef_ptr++);
        output = output + history2 * (*coef_ptr++);      /* zeros */

        *hist2_ptr++ = *hist1_ptr;
        *hist1_ptr++ = new_hist;
        hist1_ptr++;
        hist2_ptr++;
    }

    return(output);
}


//Yi Aug.23.2006 //change fs,fc from int to double type
void butterworth(double *x, int ns, double fs, double fc)
{

        FILTER   iir;
        double   *coef = NULL;
        double   Q;     /* Resonance > 1.0 < 1000 */
        unsigned nInd;
        double   a0, a1, a2, b0, b1, b2;
        double   k;           /* overall gain factor */

		iir.coef = NULL;
		iir.history = NULL;
/*
 * Setup filter s-domain coefficients
 */
                 /* Section 1 */
        ProtoCoef[0].a0 = 1.0;
        ProtoCoef[0].a1 = 0;
        ProtoCoef[0].a2 = 0;
        ProtoCoef[0].b0 = 1.0;
        ProtoCoef[0].b1 = 0.765367;
        ProtoCoef[0].b2 = 1.0;

                 /* Section 2 */
        ProtoCoef[1].a0 = 1.0;
        ProtoCoef[1].a1 = 0;
        ProtoCoef[1].a2 = 0;
        ProtoCoef[1].b0 = 1.0;
        ProtoCoef[1].b1 = 1.847759;
        ProtoCoef[1].b2 = 1.0;

        iir.length = FILTER_SECTIONS;         /* Number of filter sections */
		iir.history = 0;

/*
 * Allocate array of z-domain coefficients for each filter section
 * plus filter gain variable
 */
		iir.coef = new double[ 4 * iir.length + 1 ];
		memset( iir.coef, 0, ( 4 * iir.length + 1 ) * sizeof( double ) );
        if (!iir.coef) return;

        k = 1.0;          /* Set overall filter gain */
        coef = iir.coef + 1;     /* Skip k, or gain */

        Q = 1;                         /* Resonance */


/*
 * Compute z-domain coefficients for each biquad section
 * for new Cutoff Frequency and Resonance
 */
        for (nInd = 0; nInd < iir.length; nInd++)
        {
                 a0 = ProtoCoef[nInd].a0;
                 a1 = ProtoCoef[nInd].a1;
                 a2 = ProtoCoef[nInd].a2;

                 b0 = ProtoCoef[nInd].b0;
                 b1 = ProtoCoef[nInd].b1 / Q;      /* Divide by resonance or Q */
                 b2 = ProtoCoef[nInd].b2;
                 szxform(&a0, &a1, &a2, &b0, &b1, &b2, fc, fs, &k, coef);
                 coef += 4;                       /* Point to next filter section */
        }

        /* Update overall filter gain in coef array */
        iir.coef[0] = k;
/*
 * To process samples, call function iir_filter()
 * for each sample
 */
	for (int i = 0; i < ns; i++)
	{
	     x[i] = iir_filter(x[i],& iir);
	}

	// cleanup
	delete [] iir.coef;
	if( iir.history ) delete [] iir.history;
}

/*************************************************************************/
void rpolw(double* x, double* y, int n1, int n2)
/*************************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );

/*  subroutine  rpolw        hans-leo teulings
    interpolate via frequency domain by inserting zeroes */

    int inew, i, nlaag, icent, n21;


/*  input/output:
    x, y = arrays of length MAX(n1,n2)
    input:
    n1 = number of data (power of 2 for FFT)
    n2 = number of desired data (power of 2 for FFT) */


/*  shift up or down the negative-frequency part */


    n21 = n2 - n1;
    nlaag = n1 / 2 + 2;
    if (n21 < 0) {
	nlaag = n1 - n2 / 2 + 2;
    }
    for (i = nlaag-1; i < n1; ++i) {
	inew = n21 + i;
	x[inew] = x[i];
	y[inew] = y[i];
    }

/* zero if shifted upward */
	// 10Sep03: GMB - Added assert to ensure validity of index
	ASSERT( n1 >= 0 );
    for (i = n1 / 2;i < n2 - n1 / 2 + 1; ++i) {
	x[i] = (double)0.;
	y[i] = (double)0.;
    }

/* zero the highest-frequency component anyway */

    if (!(n21 >= 0)) {
	icent = n2 / 2;
	x[icent] = (double)0.;
	y[icent] = (double)0.;
    }
    return;
} /* End rpolw */


/*************************************************************************/
void rdif(double* x, double* y, int n)
/*************************************************************************/
{
/*  SUBROUTINE  RDIF                            HANS-LEO TEULINGS */
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );

    static double pi2 = (double)6.283185308;


    int i, n2, ii, nh1;
    static double r;
    static double dif, tmp;

	// 10Sep03: GMB - Added assert to ensure validity of index
	ASSERT( n >= 0 );

    nh1 = n / 2;
    x[0] = (double)0.;
    y[0] = (double)0.;
    x[nh1] = (double)0.;
    y[nh1] = (double)0.;

    n2 = n + 2;
    r = pi2 / n;

    for (i = 2; i <= nh1; ++i) {

	ii = n2 - i;
	dif = (i - 1) * r;

	tmp = dif * x[i-1];
	x[i-1] = -dif * y[i-1];
	y[i-1] = tmp;

	tmp = -dif * x[ii-1];
	x[ii-1] = dif * y[ii-1];
	y[ii-1] = tmp;
    }

    return;
} /* End rdif */
/*************************************************************************/
void bdif(double* x, int n, double sec, double *vx)
/*************************************************************************/
{
	for (int i = 0; i < n; i ++)
	{
		if (i == 0)
			vx [i] = x [i];
		else
			vx [i] = (x [i + 1] - x [i - 1])/(2*sec);
	}
	vx [n - 1] = 0.0000;
}

/****************************************************************************/
void powerspectrum (double* xbuf, double* ybuf, int nfftout, double* vaspectrum)
/****************************************************************************/
/* Calculate amplitude spectrum and store in vaspectrum*/
/* Does not calculate points nfftout/2+1 and higher */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( xbuf != NULL );
	ASSERT( ybuf != NULL );
	ASSERT( vaspectrum != NULL );

  int is, ik;

  /* use SQR next time */

  vaspectrum [0] = sqrt(xbuf [0] * xbuf [0] + ybuf [0] * ybuf [0]);
  for (is = 1; is <= nfftout/2; is++) {
    ik = nfftout - is;
    vaspectrum [is] = sqrt( xbuf [is] * xbuf [is] + ybuf [is] * ybuf [is] +
			                xbuf [ik] * xbuf [ik] + ybuf [ik] * ybuf [ik]);
  }
}

/****************************************************************************/
void powerspectrum2 (double* xbuf, double* ybuf, int nfftout, double* vaspectrumreal, double* vaspectrumimag)
/****************************************************************************/
/* Calculate amplitude spectrum and store in vaspectrum*/
/* Does not calculate points nfftout/2+1 and higher */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( xbuf != NULL );
	ASSERT( ybuf != NULL );
	ASSERT( vaspectrumreal != NULL );
	ASSERT( vaspectrumimag != NULL );

  int is, ik;

  /* use SQR next time */

  vaspectrumreal [0] = sqrt(xbuf [0] * xbuf [0]);
  vaspectrumimag [0] = sqrt(ybuf [0] * ybuf [0]);
  for (is = 1; is <= nfftout/2; is++) {
    ik = nfftout - is;
    vaspectrumreal [is] = sqrt( (0.5 * SQR(xbuf [is] + xbuf [ik])) +
								(0.5 * SQR(ybuf [is] - ybuf [ik])) );
    vaspectrumimag [is] = sqrt( (0.5 * SQR(xbuf [is] - xbuf [ik])) +
								(0.5 * SQR(ybuf [is] + ybuf [ik])) );
  }
}

/********************************/
//Inverse the data in the buffer
/*****************************/
// 23Aug2006: Yi: Added inversedata
// 17Nov06 - GMB: updated for pointer & size checking
void InverseData( double *pDataset, int nNs, int nSize )
{
	if( !pDataset ) return;
	if( nNs > nSize ) return;

	double temp = 0.;

	for( int i = 0; i < ( nNs / 2 ); i++ )
	{
		temp = pDataset[ i ];
		pDataset[ i ] = pDataset[ nNs - i - 1 ];
		pDataset[ nNs - i - 1] = temp;
	}
}

/*************************************/
// Mean and SD of real array
/*************************************/
void meansd (double* x, int n, double* mean, double* sd)
{
	ASSERT( x != NULL );

	int i = 0;

	*mean = 0.; *sd = 0.;

	// Mean
	for (i = 0; i < n; i++)
	{
		*mean += + x [i];
	}
	if (n > 0)
		*mean = *mean / n;
	else
		*mean = 0;

	// Standard Deviation
	for (i = 0; i < n; i++)
	{
		*sd += SQR ( x [i] - *mean );
	}
	if (n > 1)
		*sd = sqrt( *sd/(n-1) );
	else
		*sd = 0.;
}


/*************************************/
// Maximum and minimum of real array
/*************************************/
void minmax (double* x, int n, double* min, double* max)
{
	ASSERT( x != NULL );

	int i = 0;
	*min = 99999.9; *max = -99999.9;

	if (n == 0)
	{
		*max = x [0];
		*min = x [0];
	}
	else
	{
		// Min and Max
		for (i = 0; i < n; i++)
		{
			if (x [i] > *max)
				*max = x [i];
			if (x [i] < *min)
				*min = x [i];
		}
	}
}
/*
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the Shared Code
 * License (SCL) as published by www.IntellectualHeaven.com;
 * either version 1.0, or (at your option) any later version.
 *
 * You should have received a copy of the SCL 1.0 license,
 * look for file license.txt, along with this software; if not,
 * then please write to x_pankaj_x@intellectualheaven.com
 * for an electronic copy of the same.
 */

/* ++
History (in reverse order)

x_pankaj_x@intellectualheaven.com   2003-08-18
First version checked in CVS
-- */


/* des.cpp -- DES implementation source file */

//System Include Files
#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//User Include Files
#include "global_data.h"
#include "des.h"
#include "des_types.h"

CDes::CDes()
{
	for (int i = 0; i < 16; ++i)
		memset((void *)m_key_sched[i], 0, 6);
	m_input_text[7] = 0;
	m_output_text[7] = 0;
}

CDes::~CDes()
{
}


//Print text in Hex
void CDes::print_hex(const char *sz_title, const unsigned char *text, int n_length)
{
	printf("\n%s", sz_title);
	for( int i = 0; i < n_length; ++i)
	{
		
		unsigned char temp;
		for(int k = 0; k < 8; ++k)
		{
			copy_bit(&temp, k, &text[i], 8-k);
		}
		printf("%x ", temp);
	}
}


//Print text in Binary
void CDes::print_binary(const char *sz_title, const unsigned char *text, int n_length)
{
	printf("\n%s", sz_title);
	for( int i = 0; i < n_length; ++i)
	{
		for(int k = 7; k >= 0; --k)
		{
			printf("%1d ", text[i] & (1 << k));
		}
	}
}


//Copy a bit from src to dest
void CDes::copy_bit(	unsigned char *dest, //Destination
						int bit_pos_dest, //Position Dest (0->7)
						const unsigned char *src, //Source
						int bit_pos_src //Position Src (1->8)
					)
{
	/*
	bit_pos_src--;
	register s_bit = bit_pos_src%8;
	register d_bit = bit_pos_dest%8;
	register s_idx = bit_pos_src/8;
	register d_idx = bit_pos_dest/8;
	*/
	//My Method
	//dest[d_idx] = ((dest[d_idx] | (1 << d_bit)) &
	//				(((src[s_idx] >> s_bit) << d_bit) | (255 ^ (1 << d_bit))));

	//My Friend Karthik's Method
	//dest[d_idx] = ((dest[d_idx] | (1 << d_bit)) &
	//				(~(~((src[s_idx] >> s_bit) | 254) << d_bit)));

	//My Method again - about 2% faster
	bit_pos_src--;
	register int temp = (1 << (bit_pos_src%8));

	if (( src[bit_pos_src/8] & temp) == 0)
	{
		//The particular bit is off, so & with only that bit off
		dest[bit_pos_dest/8] &= (255 ^ (1 << bit_pos_dest%8));
	}
	else
	{
		//The particular bit is on, so | with only that bit on
		dest[bit_pos_dest/8] |= (1 << (bit_pos_dest%8));

	}
}


//Calculates Encrypt Key Schedule
void CDes::calculate_encrypt_key_schedule()
{
	unsigned char c[4], d[4];
	
	c[3] &= 0x0f;
	d[3] &= 0x0f;
	for (int i = 0; i < 28; ++i)
		copy_bit((unsigned char *)c, i, (unsigned char *)m_key, g_pc_1c[i]);
	
	for (int i = 0; i < 28; ++i)
		copy_bit((unsigned char *)d, i, (unsigned char *)m_key, g_pc_1d[i]);

	for (int i = 0; i < 16; ++i)
	{
		calculate_next_lt(c, d, g_l_shift[i]);
		for(int j = 0; j < 48; ++j)
		{
			if (g_pc_2[j] <= 28)
				copy_bit((unsigned char *)m_key_sched[i], j, (unsigned char *)c, g_pc_2[j]);
			else
				copy_bit((unsigned char *)m_key_sched[i], j, (unsigned char *)d, g_pc_2[j] - 28);
		}
	}
}

//Calculate Next Left Shift
void CDes::calculate_next_lt(unsigned char *c, unsigned char *d, int n_bits)
{
	if(n_bits == 2)
	{
		//We know our array has 32 bits even though we use 28
		//That's why 29 is not a problem in next lines
		copy_bit(c, 28, c, 1);
		copy_bit(c, 29, c, 2);
		copy_bit(d, 28, d, 1);
		copy_bit(d, 29, d, 2);
		for(int i = 0; i <= 25; ++i)
		{
			copy_bit(c, i, c, i+3);
			copy_bit(d, i, d, i+3);
		}
		copy_bit(c, 26, c, 29);
		copy_bit(c, 27, c, 30);
		c[3] &= 0x0f;
		copy_bit(d, 26, d, 29);
		copy_bit(d, 27, d, 30);
		d[3] &= 0x0f;
	}
	else if (n_bits == 1)
	{
		copy_bit(c, 28, c, 1);
		copy_bit(d, 28, d, 1);
		for(int i = 0; i <= 26; ++i)
		{
			copy_bit(c, i, c, i+2);
			copy_bit(d, i, d, i+2);
		}
		copy_bit(c, 27, c, 29);
		c[3] &= 0x0f;
		copy_bit(d, 27, d, 29);
		d[3] &= 0x0f;
	}
}


//Calculates Decrypt Key Schedule
void CDes::calculate_decrypt_key_schedule()
{
	unsigned char c[4], d[4];
	
	c[3] &= 0x0f;
	d[3] &= 0x0f;
	for (int i = 0; i < 28; ++i)
		copy_bit((unsigned char *)c, i, (unsigned char *)m_key, g_pc_1c[i]);
	
	for (int i = 0; i < 28; ++i)
		copy_bit((unsigned char *)d, i, (unsigned char *)m_key, g_pc_1d[i]);

	for (int i = 0; i < 16; ++i)
	{
		calculate_next_rt(c, d, g_r_shift[i]);
		for(int j = 0; j < 48; ++j)
		{
			if (g_pc_2[j] <= 28)
				copy_bit((unsigned char *)m_key_sched[i], j, (unsigned char *)c, g_pc_2[j]);
			else
				copy_bit((unsigned char *)m_key_sched[i], j, (unsigned char *)d, g_pc_2[j] - 28);
		}
	}
}

//Calculate Next Right Shift
void CDes::calculate_next_rt(unsigned char *c, unsigned char *d, int n_bits)
{
	if(n_bits == 2)
	{
		//We know our array has 32 bits even though we use 28
		for(int i = 29; i >= 2; --i)
		{
			copy_bit(c, i, c, i-1);
			copy_bit(d, i, d, i-1);
		}
		copy_bit(c, 0, c, 29);
		copy_bit(c, 1, c, 30);
		c[3] &= 0x0f;
		copy_bit(d, 0, d, 29);
		copy_bit(d, 1, d, 30);
		d[3] &= 0x0f;
	}
	else if (n_bits == 1)
	{
		//We know our array has 32 bits even though we use 28
		for(int i = 28; i >= 1; --i)
		{
			copy_bit(c, i, c, i);
			copy_bit(d, i, d, i);
		}
		copy_bit(c, 0, c, 29);
		c[3] &= 0x0f;
		copy_bit(d, 0, d, 29);
		d[3] &= 0x0f;
	}
}


//Calculate final cipher bits
void CDes::calculate_final_bits()
{
	U32bit left_0 = 0, right_0 = 0;
	U32bit left_1 = 0, right_1 = 0;

	int bit_pos = 58;
	for (int i = 0; i < 32; ++i)
	{
		copy_bit((unsigned char *)&left_0, i, (unsigned char *)m_input_text, bit_pos);
		bit_pos = (bit_pos - 8) >= 0 ? (bit_pos - 8) : (bit_pos + 58);
	}
	
	bit_pos = 57;
	for (int i = 0; i < 32; ++i)
	{
		copy_bit((unsigned char *)&right_0, i, (unsigned char *)m_input_text, bit_pos);
		bit_pos = (bit_pos - 8) >= 0 ? (bit_pos - 8) : (bit_pos + 58);
	}

	for (int i = 0; i < 16; ++i)
	{
		left_1 = right_0;

		unsigned char e_right[6]; //expanded right
		for (int j = 0; j < 48; ++j)
			copy_bit((unsigned char *)e_right, j, (unsigned char *)&right_0, g_expand[j]);

		for (int j = 0; j < 6; ++j)
			e_right[j] = (unsigned char)((int)e_right[j] ^ (int)m_key_sched[i][j]);

		unsigned char en_right[8]={0,0,0,0,0,0,0,0};
		for (int j = 0; j < 48; ++j)
			copy_bit(&en_right[j/6], j%6, e_right, 1+j);

		unsigned char temp_res[4];
		for (int j = 0; j < 8; ++j)
		{
			unsigned char row = 2*(en_right[j] & 1) + ((en_right[j] & 32) == 0 ? 0 : 1);
			unsigned char col = 0;
			col = ((en_right[j] & 2) << 2);
			col |= ((en_right[j] & 4));
			col |= ((en_right[j] & 8) >> 2);
			col |= ((en_right[j] & 16) >> 4);

			unsigned char s_val = S_Box[j][row][col];
			copy_bit(temp_res, j*4 + 0, &s_val, 4);
			copy_bit(temp_res, j*4 + 1, &s_val, 3);
			copy_bit(temp_res, j*4 + 2, &s_val, 2);
			copy_bit(temp_res, j*4 + 3, &s_val, 1);
		}
		
		U32bit result;
		for(int j = 0; j < 32; ++j)
		{
			copy_bit((unsigned char *)&result, j, temp_res, g_permute[j]);
		}

		right_1 = left_0 ^ result;
		left_0 = left_1;
		right_0 = right_1;
	}

	for(int i = 0; i < 64; ++i)
	{
		if (g_ini_permute_inv[i] <= 32)
			copy_bit((unsigned char *)m_output_text, i, (unsigned char *)&right_1, g_ini_permute_inv[i]);
		else
			copy_bit((unsigned char *)m_output_text, i, (unsigned char *)&left_1, g_ini_permute_inv[i]-32);
	}
}


//Encrypt
void CDes::encrypt(	const unsigned char *plain_text, 
					const unsigned char *key, 
					unsigned char *encrypted_text)
{
	//ReverseBits
	for (register int i = 0; i < 8; ++i)
	{
		unsigned char temp = key[i];
		for (register int j = 0; j < 8; ++j)
			copy_bit(&m_key[i], j, &temp, (8-j));
	}

	for (register int i = 0; i < 8; ++i)
	{
		unsigned char temp = plain_text[i];
		for (register int j = 0; j < 8; ++j)
			copy_bit(&m_input_text[i], j, &temp, (8-j));
	}
	
	calculate_encrypt_key_schedule();
	calculate_final_bits();

	//Reverse the bits in individual bytes
	//This is required for proper data representation
	for(register int i = 0; i < 8; ++i)
	{
		encrypted_text[i] = 0;
		for(register int j = 0; j < 8; ++j)
		{
			copy_bit(&encrypted_text[i], j, &m_output_text[i], (8-j));
		}
	}

	print_hex("Cipher Text = ", m_output_text, 8);
}


//Decrypt
void CDes::decrypt(	const unsigned char *encrypted_text, 
					const unsigned char *key, 
					unsigned char *plain_text)
{
	//ReverseBits
	for (register int i = 0; i < 8; ++i)
	{
		unsigned char temp = key[i];
		for (register int j = 0; j < 8; ++j)
			copy_bit(&m_key[i], j, &temp, (8-j));
	}

	for (register int i = 0; i < 8; ++i)
	{
		unsigned char temp = encrypted_text[i];
		for (register int j = 0; j < 8; ++j)
			copy_bit(&m_input_text[i], j, &temp, (8-j));
	}
	
	calculate_decrypt_key_schedule();
	calculate_final_bits();

	//Reverse the bits in individual bytes
	//This is required for proper data representation
	for(register int i = 0; i < 8; ++i)
	{
		plain_text[i] = 0;
		for(register int j = 0; j < 8; ++j)
		{
			copy_bit(&plain_text[i], j, &m_output_text[i], (8-j));
		}
	}

	print_hex("Plain Text = ", m_output_text, 8);
}

/*
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the Shared Code
 * License (SCL) as published by www.IntellectualHeaven.com;
 * either version 1.0, or (at your option) any later version.
 *
 * You should have received a copy of the SCL 1.0 license,
 * look for file license.txt, along with this software; if not,
 * then please write to x_pankaj_x@intellectualheaven.com
 * for an electronic copy of the same.
 */

/* ++
History (in reverse order)

x_pankaj_x@intellectualheaven.com   2003-08-18
First version checked in CVS
-- */


/* global_data.h */

#ifndef _GLOBAL_DATA_H_
#define _GLOBAL_DATA_H_

extern unsigned char g_pc_1c[];
extern unsigned char g_pc_1d[];
extern unsigned char g_pc_2[];
extern unsigned char g_l_shift[];
extern unsigned char g_r_shift[];
extern unsigned char g_expand[];
extern unsigned char g_permute[];
extern unsigned char g_ini_permute_inv[];
extern unsigned char S_Box[8][4][16];

#endif

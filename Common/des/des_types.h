/*
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the Shared Code
 * License (SCL) as published by www.IntellectualHeaven.com;
 * either version 1.0, or (at your option) any later version.
 *
 * You should have received a copy of the SCL 1.0 license,
 * look for file license.txt, along with this software; if not,
 * then please write to x_pankaj_x@intellectualheaven.com
 * for an electronic copy of the same.
 */

/* ++
History (in reverse order)

x_pankaj_x@intellectualheaven.com   2003-08-18
First version checked in CVS
-- */


/* des_types.cpp */

#ifndef _DES_TYPES_H_
#define _DES_TYPES_H_

typedef unsigned int U32bit;

#endif

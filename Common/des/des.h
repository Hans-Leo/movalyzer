/*
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the Shared Code
 * License (SCL) as published by www.IntellectualHeaven.com;
 * either version 1.0, or (at your option) any later version.
 *
 * You should have received a copy of the SCL 1.0 license,
 * look for file license.txt, along with this software; if not,
 * then please write to x_pankaj_x@intellectualheaven.com
 * for an electronic copy of the same.
 */

/* ++
History (in reverse order)

x_pankaj_x@intellectualheaven.com   2003-08-18
First version checked in CVS
-- */


/* des.h -- DES implementation header file */

#ifndef __C_DES_H__
#define __C_DES_H__

//System Include Files
#include <stdio.h>
#include <stdlib.h>

//User Include Files

class CDes
{
public:
	//Functions
	CDes();
	~CDes();
	void encrypt(	const unsigned char *plain_text, 
					const unsigned char *key, 
					unsigned char *encrypted_text);

	void decrypt(	const unsigned char *encrypted_text,
					const unsigned char *key,
					unsigned char *plain_text);

private:
	//Functions
	void calculate_encrypt_key_schedule();
	void calculate_decrypt_key_schedule();
	void calculate_next_lt(unsigned char *c, unsigned char *d, int n_bits);
	void calculate_next_rt(unsigned char *c, unsigned char *d, int n_bits);
	void calculate_final_bits();
	void copy_bit(	unsigned char *dest,
					int bit_pos_dest,
					const unsigned char *src,
					int bit_pos_src);
	
	void print_hex(const char *sz_title, const unsigned char *text, int n_length);
	void print_binary(const char *sz_title, const unsigned char *text, int n_length);


	//Data
	unsigned char m_key_sched[16][6];
	unsigned char m_key[8];
	unsigned char m_output_text[8];
	unsigned char m_input_text[8];
};

#endif

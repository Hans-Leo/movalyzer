// UserObj.cpp - Implementation of UserObj
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "UserObj.h"
#include "..\DataMod\DataMod.h"
#include "..\NSSharedGUI\Globals.h"


UserObj::UserObj( CString& szID, CString& szDesc, CString& szPass,
				  CString& szPath, CString& szBackup, CString& szSig,
				  CString& szDelim, BOOL fPrivate, BOOL fProperLoc,
				  BOOL fWriteable, CString& szSiteID, CString& szSiteDesc,
				  BOOL fPrivacy, CListCtrl* pMsgView )
: m_szID( szID ), m_szDesc( szDesc ), m_szPass( szPass ), m_szDataPath( szPath ),
  m_szBackupPath( szBackup ), m_szSig( szSig ), m_szDelim( szDelim ),
  m_fPrivate( fPrivate ), m_fProperLoc( fProperLoc ), m_fWriteable( fWriteable ),
  m_szSiteID( szSiteID ), m_szSiteDesc( szSiteDesc ), m_fPrivacy( fPrivacy ),
  m_szSubjID( _T("") ), m_szExpID( _T("") ), m_szTrial( _T("") ), m_pPS( NULL ),
  m_pMsgView( pMsgView ), m_nState( STATE_NONE ), m_pTRI( NULL )
{
	m_nInputType = 1;	// tablet
	m_fStimOnly = FALSE;
	m_fTrialOnly = FALSE;
	m_pPS = NULL;
	m_dMagnetForce = 0.;
	m_fRunTrial = FALSE;
	m_fRedoTrial = FALSE;
	m_fContinue = FALSE;
	m_dTabletX = 0.;
	m_dTabletY = 0.;
	m_dDisplayX = 0.;
	m_dDisplayY = 0.;
}

UserObj::~UserObj()
{
	if( m_pPS ) m_pPS->Release();
	if( m_pTRI )
	{
		if( m_pTRI->pEvents ) {
			delete m_pTRI->pEvents;
			m_pTRI->pEvents = NULL;
		}
		if( m_pTRI->pConditions ) {
			delete m_pTRI->pConditions;
			m_pTRI->pConditions = NULL;
		}
		delete m_pTRI;
		m_pTRI = NULL;
	}
}

void UserObj::Reset()
{
	m_nState = STATE_NONE;
	m_szTrial = _T("");
	if( m_pTRI )
	{
		if( m_pTRI->pEvents ) {
			delete m_pTRI->pEvents;
			m_pTRI->pEvents = NULL;
		}
		if( m_pTRI->pConditions )
		{
			delete m_pTRI->pConditions;
			m_pTRI->pConditions = NULL;
		}
		delete m_pTRI;
		m_pTRI = NULL;
	}
	m_pTRI = NULL;
	m_nInputType = 1;
	m_fStimOnly = FALSE;
	m_fTrialOnly = FALSE;

	m_dMagnetForce = 0.;
	m_fRunTrial = FALSE;
	m_fRedoTrial = FALSE;
	m_fContinue = FALSE;
}

BOOL UserObj::PrepareForExperiment( CStringList* pListConditions )
{
	// verify we can do this: exp id, subj id, grp id, IProcSetting
	if( !m_pPS )
	{
		OutputMessage( _T("ERROR: User Object: Invalid experiment settings pointer."), LOG_UI );
		return FALSE;
	}

	// create trialruninfo object (if not already created)
	if( !m_pTRI ) m_pTRI = new TrialRunInfo;
	if( !m_pTRI )
	{
		OutputMessage( _T("ERROR: User Object: Unable to create TrialRunInfo object."), LOG_UI );
		return FALSE;
	}

	m_fRunTrial = TRUE;
	m_fRedoTrial = FALSE;
	m_fContinue = TRUE;

	m_pTRI->szExpID = m_szExpID;
	m_pTRI->szExp = m_szExp;
	m_pTRI->szSubjID = m_szSubjID;
	m_pTRI->szSubj = m_szSubj;
	m_pTRI->szGrpID = m_szGrpID;
	if( m_pTRI->pConditions ) delete m_pTRI->pConditions;
	m_pTRI->pConditions = pListConditions;
	m_pTRI->szCurCond = _T("");
	IProcSetRunExp* m_pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&m_pPSRE );
	if( FAILED( hr ) )
	{
		OutputMessage( _T("ERROR: User Object: Invalid ProcSetRunExp interface."), LOG_UI );
		m_pPS->Release();
		return FALSE;
	}
	m_pPSRE->get_ProcImmediately( &m_pTRI->fProcess );
	m_pPSRE->get_DisplayCharts( &m_pTRI->fDisplay );
	m_pPSRE->get_DisplayRaw( &m_pTRI->fHWR );
	m_pPSRE->get_DisplayTF( &m_pTRI->fTF );
	m_pPSRE->get_DisplaySeg( &m_pTRI->fSeg );
	m_pPSRE->get_Summarize( &m_pTRI->fSumm );
	m_pPSRE->get_SummarizeSelect( &m_pTRI->fSelectSumm );
	m_pPSRE->get_SummarizeOnly( &m_pTRI->fOnlySumm );
	m_pPSRE->get_Analyze( &m_pTRI->fAnalysis );
	m_pPSRE->get_ViewSubjExt( &m_pTRI->fExt );
	m_pPSRE->get_ViewExpExt( &m_pTRI->fExtExp );
	m_pPSRE->get_ViewSubjCon( &m_pTRI->fConsis );
	m_pPSRE->get_ViewExpCon( &m_pTRI->fConsis2 );
	m_pPSRE->get_TimeoutStart( &m_pTRI->dStart );
	m_pPSRE->get_TimeoutRecord( &m_pTRI->dSecs );
	m_pPSRE->get_TimeoutPenlift( &m_pTRI->lTicks );
	m_pPSRE->get_TimeoutLatency( &m_pTRI->dClockSecsMax );
	m_pTRI->dClockSecsMax *= 1000.;
	m_pPSRE->get_DoQuestionnaire( &m_pTRI->fQuest );
	m_pPSRE->get_MaxRecArea( &m_pTRI->fMaximize );
	m_pPSRE->Release();
	TrialsMode tm;
	m_pPS->get_TrialMode( &tm );
	m_pTRI->nTrialMode = tm;
	m_pTRI->pPS = m_pPS;
	m_pTRI->nTrial = 1;
	m_pTRI->hLast = NULL;

	IProcSetFlags* m_pPSF = NULL;
	hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&m_pPSF );
	if( FAILED( hr ) ) {
		OutputMessage( _T("ERROR: User Object: Invalid ProcSetFlags interface."), LOG_UI );
		m_pPS->Release();
		return FALSE;
	}
	// TF parameters
	m_pPS->get_FilterFrequency( &m_pTRI->dFF );
	m_pPS->get_RotationBeta( &m_pTRI->dBeta );
	m_pPS->get_Decimate( &m_pTRI->nDec );
	m_pPSF->get_TF_A( &m_pTRI->fTFA );
	m_pPSF->get_TF_J( &m_pTRI->fTFJ );
	m_pPSF->get_TF_R( &m_pTRI->fTFR );
	m_pPSF->get_TF_U( &m_pTRI->fTFU );
	m_pPSF->get_TF_O( &m_pTRI->fTFO );
	// Seg parameters
	m_pPSF->get_SEG_F( &m_pTRI->fSegF );
	m_pPSF->get_SEG_L( &m_pTRI->fSegL );
	m_pPSF->get_SEG_O( &m_pTRI->fSegO );
	m_pPSF->get_SEG_S( &m_pTRI->fSegS );
	m_pPSF->get_SEG_M( &m_pTRI->fSegM );
	m_pPSF->get_SEG_A( &m_pTRI->fSegA );
	m_pPSF->get_SEG_V( &m_pTRI->fSegV );
	m_pPSF->get_SEG_MM( &m_pTRI->fSegMM );
	m_pPSF->get_SEG_J( &m_pTRI->fSegJ );
	// Extract parameters
	m_pPSF->get_EXT_S( &m_pTRI->fExtS );
	m_pPSF->get_EXT_3( &m_pTRI->fExt3 );
	m_pPSF->get_EXT_O( &m_pTRI->fExtO );
	m_pPSF->get_EXT_2( &m_pTRI->fExt2 );
	// Consis parameters
	m_pPSF->get_CON_A( &m_pTRI->fConA );
	m_pPSF->get_CON_C( &m_pTRI->fConC );
	m_pPSF->get_CON_M( &m_pTRI->fConM );
	m_pPSF->get_CON_N( &m_pTRI->fConN );
	m_pPSF->get_CON_S( &m_pTRI->fConS );
	m_pPSF->get_CON_U( &m_pTRI->fConU );
	m_pPSF->get_CON_O( &m_pTRI->fConO );
	m_pPSF->get_CON_D( &m_pTRI->fConD );
	m_pPSF->Release();

	// condition event matrix
	if( pListConditions ) {
		INSCondition* pCond = NULL;
		hr = CoCreateInstance( CLSID_NSCondition, NULL, CLSCTX_ALL, IID_INSCondition, (LPVOID*)&pCond );
		if( FAILED( hr ) ) {
			OutputMessage( _T("ERROR: User Object: Unable to create Condition object."), LOG_UI );
			m_pPS->Release();
			return FALSE;
		}
		INSConditionSound* pCondS = NULL;
		hr = pCond->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) {
			OutputMessage( _T("ERROR: User Object: Unable to access Condition sound interface."), LOG_UI );
			m_pPS->Release();
			pCond->Release();
			return FALSE;
		}
		m_pTRI->pEvents = new TrialRunEvents();
		CString szCondID;
		BSTR bstr = NULL;
		short nVal = 0;
		POSITION pos = pListConditions->GetHeadPosition();
		while( pos ) {
			szCondID = pListConditions->GetNext( pos );
			if( m_pTRI->pEvents->GetConditionEvents( szCondID ) == NULL ) {
				if( SUCCEEDED( pCond->Find( szCondID.AllocSysString() ) ) ) {
					CondEvents* ce = new CondEvents();
					ce->szCondID = szCondID;
					for( int i = 0; i < SOUND_COUNT; i++ ) {
						pCondS->get_SoundUse( (eSoundType)( i + 1 ), &(ce->pEvents[ i ].fUse) );
						pCondS->get_SoundTone( (eSoundType)( i + 1 ), &(ce->pEvents[ i ].fTone) );
						pCondS->get_SoundToneFreq( (eSoundType)( i + 1 ), &nVal );
						ce->pEvents[ i ].nFreq = nVal;
						pCondS->get_SoundToneDur( (eSoundType)( i + 1 ), &nVal );
						ce->pEvents[ i ].nDur = nVal;
						pCondS->get_SoundMedia( (eSoundType)( i + 1 ), &bstr );
						ce->pEvents[ i ].szSound = bstr;
						pCondS->get_SoundTrigger( (eSoundType)( i + 1 ), &(ce->pEvents[ i ].fTrigger) );
						pCondS->get_SoundScript( (eSoundType)( i + 1 ), &nVal, &bstr );
						ce->pEvents[ i ].nScriptType = nVal;
						ce->pEvents[ i ].szScript = bstr;
					}
					m_pTRI->pEvents->lstCondEvents.AddTail( ce );
				}
			}
		}
		pCondS->Release();
		pCond->Release();
	}

	return TRUE;
}

CondEvents::CondEvents() : pEvents( NULL ) {
	pEvents = new CondEvent[ SOUND_COUNT ];
}

CondEvents::~CondEvents() {
	if( pEvents ) delete [] pEvents;
}

TrialRunEvents::~TrialRunEvents() {
	POSITION pos = lstCondEvents.GetHeadPosition();
	while( pos ) {
		CondEvents* ce = (CondEvents*)lstCondEvents.GetNext( pos );
		if( ce ) delete ce;
	}
}

CondEvents* TrialRunEvents::GetConditionEvents( CString szCondID ) {
	CondEvents* pEvents = NULL;
	POSITION pos = lstCondEvents.GetHeadPosition();
	while( pos ) {
		CondEvents* ce = (CondEvents*)lstCondEvents.GetNext( pos );
		if( ce && ce->szCondID == szCondID ) {
			pEvents = ce;
			break;
		}
	}
	return pEvents;
}

// ErrorReport.h

#pragma once

#include <Windows.h>
#include <ErrorRep.h>

static char szPath[ MAX_PATH + 1 ];
static char szFR[] = _T("\\System32\\FaultRep.dll");

char* GetFullPathToFaultrepDll()
{
    CHAR *lpRet = NULL;
    UINT rc;

    rc = GetSystemWindowsDirectory( szPath, ARRAYSIZE( szPath ) );
    if( ( rc == 0 ) || ( rc > ( ARRAYSIZE( szPath ) - ARRAYSIZE( szFR ) - 1 ) ) )
        return NULL;

    strcat_s( szPath, MAX_PATH + 1, szFR );
    return szPath;
}

static LONG WINAPI NSUnhandledExceptionFilter( struct _EXCEPTION_POINTERS* pExceptionPointers  )
{
	// CRASH
	// File to write to indicate we've crashed
	CString szCrashFile;
	::GetDataPath( szCrashFile );
	szCrashFile += _T("mova.zzz");
	// Create file and write user to it if set
	CStdioFile CrashFile;
	if( CrashFile.Open( szCrashFile,
						CFile::modeWrite | CFile::modeCreate |
						CFile::shareDenyNone | CFile::modeNoTruncate ) )
	{
		CString szUser;
		::GetCurrentUser( szUser );
		CrashFile.WriteString( szUser );
		CrashFile.Close();
	}

	// now do normal handling
	LONG lRet = EXCEPTION_CONTINUE_SEARCH;
	char* psz = GetFullPathToFaultrepDll();
	if( psz )
	{
		HMODULE hFaultRepDll = LoadLibrary( psz ) ;
		if( hFaultRepDll )
		{
			pfn_REPORTFAULT pfn = (pfn_REPORTFAULT)GetProcAddress( hFaultRepDll, _T("ReportFault") ) ;
			if ( pfn )
			{
				EFaultRepRetVal rc = pfn( pExceptionPointers, 0) ;
				lRet = EXCEPTION_EXECUTE_HANDLER;
			}
			FreeLibrary( hFaultRepDll );
		}
	}
	return lRet;
}

// MessageTarget.h
//
// All classes of type MessageTarget that are added to the MessageManager
// list (MessageManager.h) will receive messages sent by others. This is similar (in concept)
// to microsoft's windows/system messages (with BEGIN_MESSAGE_MAP, etc). This
// was constructed to avoid MS's ever-changing methods and to allow more customization.

#if !defined( __MESSAGETARGET_H__ )
#define	__MESSAGETARGET_H__

// class
class AFX_EXT_CLASS MessageTarget
{
// construction/destruction
public:
	MessageTarget();
//	virtual ~MessageTarget();	/* STUPID BCG does not like destructors */
	void Cleanup();

// methods
public:
	// message: handles messages (needs to be overridden for functionality)
//		WORD wMsg = LOWORD( msg );
//		WORD wSubMsg = HIWORD( msg );
//		WORD wVal1 = LOWORD( val );
//		WORD wVal2 = HIWORD( val );
	virtual void Message( LPARAM msg, WPARAM val, BOOL* fStop ) { return; }
};

#endif

#ifndef	__PROCSETTINGS_H__
#define	__PROCSETTINGS_H__

#define NSMAX			4096			// should be power of 2 and larger than 1.25 * NS for TimFun
#define NSEGMAX			1000				// max # of segments
										// NOTE: we have tested this as 3 and 3000 to see what would happen
										//	RESULTS: no crashes, results as expected
										//			 for processmod and showmod
#define NFREQMAX		NSMAX / 2 + 1	// max # of frequencies in spectrum

#endif

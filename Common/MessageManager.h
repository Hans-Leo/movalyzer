// MessageManager.h
//
// All classes of type MessageTarget (MessageTarget.h) that are added to this
// class's list will receive messages sent by others. This is similar (in concept)
// to microsoft's windows/system messages (with BEGIN_MESSAGE_MAP, etc). This
// was constructed to avoid MS's ever-changing methods and to allow more customization.

#if !defined( __MESSAGEMANAGER_H__ )
#define	__MESSAGEMANAGER_H__

// message types (hiword)
#define	MSG_NONE							100	// should not be used
#define	MSG_RESULT							101
#define	MSG_ACTION							102
#define	MSG_PROPERTIES						103
#define	MSG_HELP							104
#define	MSG_CLEAR							105
#define	MSG_RUN_EXPERIMENT					106

// message subtypes (loword)
#define	MSG_SUB_NONE						100	// should not be used
#define	MSG_SUB_STATUS						101
#define	MSG_SUB_OUTPUT						102
#define	MSG_SUB_ADD_ELEMENT					103
#define	MSG_SUB_ELEMENT_ADDED				104
#define	MSG_SUB_ADD_TARGET					105
#define	MSG_SUB_TARGET_ADDED				106
#define	MSG_SUB_DEL_ELEMENT					107	// applies to targets as well
#define	MSG_SUB_DUP_ELEMENT					108	// ''
#define	MSG_SUB_HELP_GO						109
#define	MSG_SUB_HELP_CATEGORY				110
#define	MSG_SUB_USER_NONE					111
#define	MSG_SUB_USER_NEW					112
#define	MSG_SUB_USER_UPDATE					113
#define	MSG_SUB_SUBJECT_SELECT				114
#define	MSG_SUB_EXP_SELECT					115
#define	MSG_SUB_COND_SELECT					116
#define	MSG_SUB_GROUP_SELECT				117
#define	MSG_SUB_TRIAL_SELECT				118
#define	MSG_SUB_INITIALIAZE					119
#define	MSG_SUB_REFRESH						120
#define	MSG_SUB_NOEXPERIMENTS				121
#define	MSG_SUB_NOSUBJECTS					122
#define	MSG_SUB_SETUPTOOLTIPS				123
#define	MSG_SUB_SHOWTRIAL					124
#define	MSG_SUB_DEVICESETUP					125
#define	MSG_SUB_TESTINPUTDEVICE				200
#define	MSG_SUB_RUN_EXPERIMENT_INIT			201
#define	MSG_SUB_RUN_EXPERIMENT				202
#define	MSG_SUB_STOP_TEST					203
#define	MSG_SUB_STOP_EXP					204
#define	MSG_SUB_STOP_COND					205
#define	MSG_SUB_STOP_RECORD					206
#define	MSG_SUB_ENTER_HIT					207
#define	MSG_SUB_NEXT_TRIAL					208
#define	MSG_SUB_STOP_TRIAL					209
#define	MSG_SUB_RERUN_TRIAL					210
#define	MSG_SUB_START						211
#define	MSG_SUB_END							212
#define	MSG_SUB_RE_UPDATE_INSTRUCTION		213
#define	MSG_SUB_RE_UPDATE_TRIAL_NUM			214
#define	MSG_SUB_RE_UPDATE_SUBJECT			215
#define	MSG_SUB_RE_UPDATE_GROUP				216
#define	MSG_SUB_RE_UPDATE_EXP				217
#define	MSG_SUB_RE_NEW_TRIAL				218
#define MSG_SUB_RUNEXPERIMENT_START			219
#define MSG_SUB_RE_FEEDBACK					220
#define MSG_SUB_STIMEDIT					221

class MessageTarget;

// class
class AFX_EXT_CLASS MessageManager
{
// construction/destruction
public:
	MessageManager() {}
	~MessageManager();

// attributes
protected:
	CObList	m_lstItems;

// methods
public:
	// create: creates the global object from static function
	static BOOL Create();
	static void Destroy();
	// getmessagemanager: gets the global object from static function
	static MessageManager* GetMessageManager();

	// post message: hiword of msg = msg type, loword = sub-msg
	// val = content for message: can be value or pointer (dependant upon message)
	// examples
	//	PostMessage( MAKELPARAM( l, h ), MAKEWPARAM( l, h ) );
	//	PostMessage( MAKELPARAM( l, h ), (WPARAM)pObject );
	void PostMessage( LPARAM msg, WPARAM val );

	// add: adds MessageTarget to list
	BOOL Add( MessageTarget* pMT );

	// remove: removes item from list
	BOOL Remove( MessageTarget* pMT );
};

#endif

#ifndef	__BINARYTREE_H__
#define	__BINARYTREE_H__

///////////////////////////////////////////////////////////////////////////////
// BinaryTree Template Class
// 
// Mods:
//	27Oct99 - GMB:Initial Creation
//
// This template class represents a binary (red/black) tree structure.
// BinaryTree is used for managing data structures in a logical and
// quick manner (based on relative values of objects)...thus any object
// that is applied to BinaryTree must define the following
// .......operator <
// .......operator ==
// .......operator << (for DEBUGGING)
// This is for use with OODBMS ObjectSTore
//////////////////////////////////////////////////////////////////////////
template <class T>
class BinaryTree
{
public:
	// Attributes
	BinaryTree<T>	*m_pLeft;
	BinaryTree<T>	*m_pRight;
	T				&m_data;

public:
	// Construction/Destruction
	BinaryTree( T &d );
	~BinaryTree();

public:
	// Operations
	void Add( T &d );
	void Remove( T &d );
	T* Find( T &d );

	// Debugging
#ifdef _DEBUG
	void Print() const;
	void Dump() const;
#endif
};

#endif	// __BINARYTREE_H__

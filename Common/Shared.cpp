#include "StdAfx.h"
#include "Shared.h"
#include "..\Common\DefFun.h"
#include <WinNetWk.h>

// global vars
COLORREF bgColor = RGB( 255, 255, 255 );
int nLVMaxSize =  75;
BOOL IsHidden = FALSE;

// feedback settings
void SetBackgroundColor( COLORREF bg )
{
	bgColor = bg;
}

void SetIsHidden( BOOL fHide )
{
	IsHidden = fHide;
}

// LoadBMP		- Loads a BMP file and creates a logical palette for it.
// Returns		- TRUE for success
// sBMPFile		- Full path of the BMP file
// phDIB		- Pointer to a HGLOBAL variable to hold the loaded bitmap
//			  Memory is allocated by this function but should be 
//			  released by the caller.
// pPal			- Will hold the logical palette
BOOL LoadBMP( LPCTSTR sBMPFile, HGLOBAL *phDIB, CPalette *pPal )
{
	CFile file;
	if( !file.Open( sBMPFile, CFile::modeRead ) )
		return FALSE;

	BITMAPFILEHEADER bmfHeader;
	long nFileLen;

	nFileLen = (long)file.GetLength();


	// Read file header
	if( file.Read( (LPSTR)&bmfHeader, sizeof( bmfHeader ) ) != sizeof( bmfHeader ) )
		return FALSE;

	// File type should be 'BM'
	if( bmfHeader.bfType != ( (WORD) ('M' << 8) | 'B') )
		return FALSE;

	HGLOBAL hDIB = ::GlobalAlloc( GMEM_FIXED, nFileLen );
	if( hDIB == 0 )
		return FALSE;

	// Read the remainder of the bitmap file.
	if( file.Read(( LPSTR)hDIB, nFileLen - sizeof( BITMAPFILEHEADER ) ) !=
		( nFileLen - sizeof( BITMAPFILEHEADER ) ) )
	{
		::GlobalFree( hDIB );
		return FALSE;
	}

	
	BITMAPINFO &bmInfo = *(LPBITMAPINFO)hDIB ;

	int nColors = bmInfo.bmiHeader.biClrUsed ? bmInfo.bmiHeader.biClrUsed : 
						1 << bmInfo.bmiHeader.biBitCount;

	// Create the palette
	if( nColors <= 256 )
	{
		UINT nSize = sizeof( LOGPALETTE ) + ( sizeof( PALETTEENTRY ) * nColors );
		LOGPALETTE* pLP = (LOGPALETTE*) new BYTE[ nSize ];

		pLP->palVersion = 0x300;
		pLP->palNumEntries = nColors;

		for( int i=0; i < nColors; i++ )
		{
			pLP->palPalEntry[ i ].peRed = bmInfo.bmiColors[ i ].rgbRed;
			pLP->palPalEntry[ i ].peGreen = bmInfo.bmiColors[ i ].rgbGreen;
			pLP->palPalEntry[ i ].peBlue = bmInfo.bmiColors[ i ].rgbBlue;
			pLP->palPalEntry[ i ].peFlags = 0;
		}

		pPal->CreatePalette( pLP );

		delete [] pLP;
	}

	*phDIB = hDIB;
	return TRUE;
}

// draw a bitmap to the specified display device context
void DrawDIB( CDC* pDC, HGLOBAL hDIB, CPalette *pPal, long lX, long lY )
{
	LPVOID	lpDIBBits;			// Pointer to DIB bits
	BOOL	bSuccess=FALSE; 	// Success/fail flag

	BITMAPINFO &bmInfo = *(LPBITMAPINFO)hDIB ;
	int nColors = bmInfo.bmiHeader.biClrUsed ? bmInfo.bmiHeader.biClrUsed : 
						1 << bmInfo.bmiHeader.biBitCount;

	if( bmInfo.bmiHeader.biBitCount > 8 )
		lpDIBBits = (LPVOID)( (LPDWORD)( bmInfo.bmiColors + bmInfo.bmiHeader.biClrUsed ) +
							( ( bmInfo.bmiHeader.biCompression == BI_BITFIELDS ) ? 3 : 0 ) );
	else
		lpDIBBits = (LPVOID)( bmInfo.bmiColors + nColors );
	
	if( pPal && ( pDC->GetDeviceCaps( RASTERCAPS ) & RC_PALETTE ) )
	{
		pDC->SelectPalette( pPal, FALSE );
		pDC->RealizePalette();
	}

	long x = lX - ( bmInfo.bmiHeader.biWidth / 2 );
	long y = lY - ( bmInfo.bmiHeader.biHeight / 2 );

	::SetDIBitsToDevice( pDC->m_hDC, 				// hDC
				   x,								// DestX
				   y,								// DestY
				   bmInfo.bmiHeader.biWidth,		// nDestWidth
				   bmInfo.bmiHeader.biHeight,		// nDestHeight
				   0,								// SrcX
				   0,								// SrcY
				   0,								// nStartScan
				   bmInfo.bmiHeader.biHeight,		// nNumScans
				   lpDIBBits,						// lpBits
				   (LPBITMAPINFO)hDIB,				// lpBitsInfo
				   DIB_RGB_COLORS ); 				// wUsage
}

void DrawDIB( CDC* pDC, HGLOBAL hDIB, CPalette *pPal, RECT& rect )
{
	LPVOID	lpDIBBits;			// Pointer to DIB bits
	BOOL	bSuccess=FALSE; 	// Success/fail flag

	BITMAPINFO &bmInfo = *(LPBITMAPINFO)hDIB ;
	int nColors = bmInfo.bmiHeader.biClrUsed ? bmInfo.bmiHeader.biClrUsed : 
						1 << bmInfo.bmiHeader.biBitCount;

	if( bmInfo.bmiHeader.biBitCount > 8 )
		lpDIBBits = (LPVOID)( (LPDWORD)( bmInfo.bmiColors + bmInfo.bmiHeader.biClrUsed ) +
							( ( bmInfo.bmiHeader.biCompression == BI_BITFIELDS ) ? 3 : 0 ) );
	else
		lpDIBBits = (LPVOID)( bmInfo.bmiColors + nColors );
	
	if( pPal && ( pDC->GetDeviceCaps( RASTERCAPS ) & RC_PALETTE ) )
	{
		pDC->SelectPalette( pPal, FALSE );
		pDC->RealizePalette();
	}

	::SetDIBitsToDevice( pDC->m_hDC, 				// hDC
				   rect.left,						// DestX
				   rect.top,						// DestY
				   bmInfo.bmiHeader.biWidth,		// nDestWidth
				   bmInfo.bmiHeader.biHeight,		// nDestHeight
				   0,								// SrcX
				   0,								// SrcY
				   0,								// nStartScan
				   bmInfo.bmiHeader.biHeight,		// nNumScans
				   lpDIBBits,						// lpBits
				   (LPBITMAPINFO)hDIB,				// lpBitsInfo
				   DIB_RGB_COLORS ); 				// wUsage
}

// DDBToDIB		- Creates a DIB from a DDB
// bitmap		- Device dependent bitmap
// dwCompression	- Type of compression - see BITMAPINFOHEADER
// pPal			- Logical palette
HANDLE DDBToDIB( CBitmap& bitmap, DWORD dwCompression, CPalette* pPal ) 
{
	BITMAP			bm;
	BITMAPINFOHEADER	bi;
	LPBITMAPINFOHEADER 	lpbi;
	DWORD			dwLen;
	HANDLE			hDIB;
	HANDLE			handle;
	HDC 			hDC;
	HPALETTE		hPal;


	ASSERT( bitmap.GetSafeHandle() );

	// The function has no arg for bitfields
	if( dwCompression == BI_BITFIELDS )
		return NULL;

	// If a palette has not been supplied use defaul palette
	hPal = (HPALETTE) pPal->GetSafeHandle();
	if (hPal==NULL)
		hPal = (HPALETTE) GetStockObject(DEFAULT_PALETTE);

	// Get bitmap information
	bitmap.GetObject(sizeof(bm),(LPSTR)&bm);

	// Initialize the bitmapinfoheader
	bi.biSize		= sizeof(BITMAPINFOHEADER);
	bi.biWidth		= bm.bmWidth;
	bi.biHeight 		= bm.bmHeight;
	bi.biPlanes 		= 1;
	bi.biBitCount		= bm.bmPlanes * bm.bmBitsPixel;
	bi.biCompression	= dwCompression;
	bi.biSizeImage		= 0;
	bi.biXPelsPerMeter	= 0;
	bi.biYPelsPerMeter	= 0;
	bi.biClrUsed		= 0;
	bi.biClrImportant	= 0;

	// Compute the size of the  infoheader and the color table
	int nColors = (1 << bi.biBitCount);
	if( nColors > 256 ) 
		nColors = 0;
	dwLen  = bi.biSize + nColors * sizeof(RGBQUAD);

	// We need a device context to get the DIB from
	hDC = GetDC(NULL);
	hPal = SelectPalette(hDC,hPal,FALSE);
	RealizePalette(hDC);

	// Allocate enough memory to hold bitmapinfoheader and color table
	hDIB = GlobalAlloc(GMEM_FIXED,dwLen);

	if (!hDIB){
		SelectPalette(hDC,hPal,FALSE);
		ReleaseDC(NULL,hDC);
		return NULL;
	}

	lpbi = (LPBITMAPINFOHEADER)hDIB;

	*lpbi = bi;

	// Call GetDIBits with a NULL lpBits param, so the device driver 
	// will calculate the biSizeImage field 
	GetDIBits(hDC, (HBITMAP)bitmap.GetSafeHandle(), 0L, (DWORD)bi.biHeight,
			(LPBYTE)NULL, (LPBITMAPINFO)lpbi, (DWORD)DIB_RGB_COLORS);

	bi = *lpbi;

	// If the driver did not fill in the biSizeImage field, then compute it
	// Each scan line of the image is aligned on a DWORD (32bit) boundary
	if (bi.biSizeImage == 0){
		bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) 
						* bi.biHeight;

		// If a compression scheme is used the result may infact be larger
		// Increase the size to account for this.
		if (dwCompression != BI_RGB)
			bi.biSizeImage = (bi.biSizeImage * 3) / 2;
	}

	// Realloc the buffer so that it can hold all the bits
	dwLen += bi.biSizeImage;
	if (handle = GlobalReAlloc(hDIB, dwLen, GMEM_MOVEABLE))
		hDIB = handle;
	else{
		GlobalFree(hDIB);

		// Reselect the original palette
		SelectPalette(hDC,hPal,FALSE);
		ReleaseDC(NULL,hDC);
		return NULL;
	}

	// Get the bitmap bits
	lpbi = (LPBITMAPINFOHEADER)hDIB;

	// FINALLY get the DIB
	BOOL bGotBits = GetDIBits( hDC, (HBITMAP)bitmap.GetSafeHandle(),
				0L,				// Start scan line
				(DWORD)bi.biHeight,		// # of scan lines
				(LPBYTE)lpbi 			// address for bitmap bits
				+ (bi.biSize + nColors * sizeof(RGBQUAD)),
				(LPBITMAPINFO)lpbi,		// address of bitmapinfo
				(DWORD)DIB_RGB_COLORS);		// Use RGB for color table

	if( !bGotBits )
	{
		GlobalFree(hDIB);
		
		SelectPalette(hDC,hPal,FALSE);
		ReleaseDC(NULL,hDC);
		return NULL;
	}

	SelectPalette(hDC,hPal,FALSE);
	ReleaseDC(NULL,hDC);
	return hDIB;
}

// WriteDIB		- Writes a DIB to file
// Returns		- TRUE on success
// szFile		- Name of file to write to
// hDIB			- Handle of the DIB
BOOL WriteDIB( LPTSTR szFile, HANDLE hDIB)
{
	BITMAPFILEHEADER	hdr;
	LPBITMAPINFOHEADER	lpbi;

	if (!hDIB)
		return FALSE;

	CFile file;
	if( !file.Open( szFile, CFile::modeWrite|CFile::modeCreate) )
		return FALSE;

	lpbi = (LPBITMAPINFOHEADER)hDIB;

	int nColors = 1 << lpbi->biBitCount;

	// Fill in the fields of the file header 
	hdr.bfType		= ((WORD) ('M' << 8) | 'B');	// is always "BM"
	hdr.bfSize		= (DWORD)( GlobalSize (hDIB) + sizeof( hdr ) );
	hdr.bfReserved1 	= 0;
	hdr.bfReserved2 	= 0;
	hdr.bfOffBits		= (DWORD) (sizeof( hdr ) + lpbi->biSize +
						nColors * sizeof(RGBQUAD));

	// Write the file header 
	file.Write( &hdr, sizeof(hdr) );

	// Write the DIB header and the bits 
	file.Write( lpbi, (UINT)GlobalSize(hDIB) );

	return TRUE;
}

BOOL WriteWindowToDIB( LPTSTR szFile, CWnd *pWnd )
{
	CBitmap 	bitmap;
	CWindowDC	dc(pWnd);
	CDC 		memDC;
	CRect		rect;

	memDC.CreateCompatibleDC(&dc); 

	pWnd->GetWindowRect(rect);

	bitmap.CreateCompatibleBitmap(&dc, rect.Width(),rect.Height() );
	
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.BitBlt(0, 0, rect.Width(),rect.Height(), &dc, 0, 0, SRCCOPY); 

	// Create logical palette if device support a palette
	CPalette pal;
	if( dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE )
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];
		pLP->palVersion = 0x300;

		pLP->palNumEntries = 
			GetSystemPaletteEntries( dc, 0, 255, pLP->palPalEntry );

		// Create the palette
		pal.CreatePalette( pLP );

		delete[] pLP;
	}

	memDC.SelectObject(pOldBitmap);

	// Convert the bitmap to a DIB
	HANDLE hDIB = DDBToDIB( bitmap, BI_RGB, &pal );

	if( hDIB == NULL )
		return FALSE;

	// Write it to file
	WriteDIB( szFile, hDIB );

	// Free the memory allocated by DDBToDIB for the DIB
	GlobalFree( hDIB );
	return TRUE;
}

// LoadBMPImage	- Loads a BMP file and creates a bitmap GDI object
//		  also creates logical palette for it.
// Returns	- TRUE for success
// sBMPFile	- Full path of the BMP file
// bitmap	- The bitmap object to initialize
// pPal		- Will hold the logical palette. Can be NULL
BOOL LoadBMPImage( LPCTSTR sBMPFile, CBitmap& bitmap, CPalette *pPal )
{
	CFile file;
	if( !file.Open( sBMPFile, CFile::modeRead) )
		return FALSE;

	BITMAPFILEHEADER bmfHeader;

	// Read file header
	if (file.Read((LPSTR)&bmfHeader, sizeof(bmfHeader)) != sizeof(bmfHeader))
		return FALSE;

	// File type should be 'BM'
	if (bmfHeader.bfType != ((WORD) ('M' << 8) | 'B'))
		return FALSE;

	// Get length of the remainder of the file and allocate memory
	DWORD nPackedDIBLen = (DWORD)( file.GetLength() - sizeof(BITMAPFILEHEADER) );
	HGLOBAL hDIB = ::GlobalAlloc(GMEM_FIXED, nPackedDIBLen);
	if (hDIB == 0)
		return FALSE;

	// Read the remainder of the bitmap file.
	if (file.Read((LPSTR)hDIB, nPackedDIBLen) != nPackedDIBLen )
	{
		::GlobalFree(hDIB);
		return FALSE;
	}


	BITMAPINFOHEADER &bmiHeader = *(LPBITMAPINFOHEADER)hDIB ;
	BITMAPINFO &bmInfo = *(LPBITMAPINFO)hDIB ;

	// If bmiHeader.biClrUsed is zero we have to infer the number
	// of colors from the number of bits used to specify it.
	int nColors = bmiHeader.biClrUsed ? bmiHeader.biClrUsed : 
						1 << bmiHeader.biBitCount;

	LPVOID lpDIBBits;
	if( bmInfo.bmiHeader.biBitCount > 8 )
		lpDIBBits = (LPVOID)((LPDWORD)(bmInfo.bmiColors + bmInfo.bmiHeader.biClrUsed) + 
			((bmInfo.bmiHeader.biCompression == BI_BITFIELDS) ? 3 : 0));
	else
		lpDIBBits = (LPVOID)(bmInfo.bmiColors + nColors);

	// Create the logical palette
	if( pPal != NULL )
	{
		// Create the palette
		if( nColors <= 256 )
		{
			UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * nColors);
			LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];

			pLP->palVersion = 0x300;
			pLP->palNumEntries = nColors;

			for( int i=0; i < nColors; i++)
			{
				pLP->palPalEntry[i].peRed = bmInfo.bmiColors[i].rgbRed;
				pLP->palPalEntry[i].peGreen = bmInfo.bmiColors[i].rgbGreen;
				pLP->palPalEntry[i].peBlue = bmInfo.bmiColors[i].rgbBlue;
				pLP->palPalEntry[i].peFlags = 0;
			}

			pPal->CreatePalette( pLP );

			delete[] pLP;
		}
	}

	CClientDC dc(NULL);
	CPalette* pOldPalette = NULL;
	if( pPal )
	{
		pOldPalette = dc.SelectPalette( pPal, FALSE );
		dc.RealizePalette();
	}

	HBITMAP hBmp = CreateDIBitmap( dc.m_hDC,		// handle to device context 
				&bmiHeader,	// pointer to bitmap size and format data 
				CBM_INIT,	// initialization flag 
				lpDIBBits,	// pointer to initialization data 
				&bmInfo,	// pointer to bitmap color-format data 
				DIB_RGB_COLORS);		// color-data usage 
	bitmap.Attach( hBmp );

	if( pOldPalette )
		dc.SelectPalette( pOldPalette, FALSE );

	::GlobalFree(hDIB);
	return TRUE;
}

//************************************
// Method:    DIBToDDB
// FullName:  DIBToDDB
// Access:    public 
// Returns:   HBITMAP
// Qualifier:
// Parameter: HANDLE hDIB
//************************************
HBITMAP DIBToDDB( HANDLE hDIB )
{
	LPBITMAPINFOHEADER	lpbi;
	HBITMAP 		hbm;
	CPalette		pal;
	CPalette*		pOldPal;
	CClientDC		dc(NULL);

	if (hDIB == NULL)
		return NULL;

	lpbi = (LPBITMAPINFOHEADER)hDIB;

	int nColors = lpbi->biClrUsed ? lpbi->biClrUsed : 
						1 << lpbi->biBitCount;

	BITMAPINFO &bmInfo = *(LPBITMAPINFO)hDIB ;
	LPVOID lpDIBBits;
	if( bmInfo.bmiHeader.biBitCount > 8 )
		lpDIBBits = (LPVOID)((LPDWORD)(bmInfo.bmiColors + 
			bmInfo.bmiHeader.biClrUsed) + 
			((bmInfo.bmiHeader.biCompression == BI_BITFIELDS) ? 3 : 0));
	else
		lpDIBBits = (LPVOID)(bmInfo.bmiColors + nColors);

	// Create and select a logical palette if needed
	if( nColors <= 256 && dc.GetDeviceCaps(RASTERCAPS) & RC_PALETTE)
	{
		UINT nSize = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * nColors);
		LOGPALETTE *pLP = (LOGPALETTE *) new BYTE[nSize];

		pLP->palVersion = 0x300;
		pLP->palNumEntries = nColors;

		for( int i=0; i < nColors; i++)
		{
			pLP->palPalEntry[i].peRed = bmInfo.bmiColors[i].rgbRed;
			pLP->palPalEntry[i].peGreen = bmInfo.bmiColors[i].rgbGreen;
			pLP->palPalEntry[i].peBlue = bmInfo.bmiColors[i].rgbBlue;
			pLP->palPalEntry[i].peFlags = 0;
		}

		pal.CreatePalette( pLP );

		delete[] pLP;

		// Select and realize the palette
		pOldPal = dc.SelectPalette( &pal, FALSE );
		dc.RealizePalette();
	}

	
	hbm = CreateDIBitmap(dc.GetSafeHdc(),		// handle to device context
			(LPBITMAPINFOHEADER)lpbi,	// pointer to bitmap info header 
			(LONG)CBM_INIT,			// initialization flag
			lpDIBBits,			// pointer to initialization data 
			(LPBITMAPINFO)lpbi,		// pointer to bitmap info
			DIB_RGB_COLORS );		// color-data usage 

	if (pal.GetSafeHandle())
		dc.SelectPalette(pOldPal,FALSE);

	return hbm;
}

// aspect ratio for mouse to represent tablet (monitor)
double	_arX = 0., _arY = 0.;
// display dimensions
double _ddX = 0., _ddY = 0.;
// tablet dimesnions
double _dtX = 0., _dtY = 0.;

void SetAspectRatio( double arX, double arY ) { _arX = arX; _arY = arY; }
void GetAspectRatio( double& arX, double& arY ) { arX = _arX; arY = _arY; }
void SetDisplayDimension( double ddX, double ddY ) { _ddX = ddX; _ddY = ddY; }
void GetDisplayDimension( double& ddX, double& ddY ) { ddX = _ddX; ddY = _ddY; }
void SetTabletDimension( double dtX, double dtY ) { _dtX = dtX; _dtY = dtY; }
void GetTabletDimension( double& dtX, double& dtY ) { dtX = _dtX; dtY = _dtY; }

////////////////////////////////////////////////////////////////////////////////
// Drawing on screen for devices

#define ELLIPSESIZE		2		// Diameter
#define	ELLIPSETHICK	2		// line thickness
#define	ELLIPSECOLOR	RGB(255, 0, 0)
#define	LINETHICK		2		// line thickness
#define	LINECOLOR		RGB(0, 0, 255)
#define	LINECOLOR2		RGB(0, 255, 0)
#define	LINECOLOR3		RGB(255, 0, 0)

BOOL	__fLineFixed = TRUE;
short	__nLineSize = 2;
DWORD	__dwLineColor1 = LINECOLOR;
DWORD	__dwLineColor2 = LINECOLOR2;
DWORD	__dwLineColor3 = LINECOLOR3;
DWORD	__dwMPPColor = ::GetSoftColor();
DWORD	__dwEllipseColor = ELLIPSECOLOR;
short	__nEllipseSize = ELLIPSESIZE;

//************************************
// Method:    DrawSettings
// FullName:  DrawSettings
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL LineFixed
// Parameter: short LineSize
// Parameter: DWORD LineColor1
// Parameter: DWORD LineColor2
// Parameter: DWORD LineColor3
// Parameter: DWORD MPPColor
// Parameter: DWORD EllipseColor
// Parameter: short EllipseSize
//************************************
void DrawSettings( BOOL LineFixed, short LineSize, DWORD LineColor1,
				   DWORD LineColor2, DWORD LineColor3, DWORD MPPColor,
				   DWORD EllipseColor, short EllipseSize )
{
	__fLineFixed = LineFixed;
	__nLineSize = LineSize;
	__dwLineColor1 = LineColor1;
	__dwLineColor2 = LineColor2;
	__dwLineColor3 = LineColor3;
	__dwMPPColor = MPPColor;
	__dwEllipseColor = EllipseColor;
	__nEllipseSize = EllipseSize;
}

//************************************
// Method:    DrawPoint
// FullName:  DrawPoint
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CDC * pDC
// Parameter: POINT ptNew
// Parameter: POINT ptOld
// Parameter: BOOL fDown
// Parameter: BOOL fBeganRecording
// Parameter: BOOL fFirstPoint
// Parameter: BOOL fWasOutOfRange
// Parameter: int nPenPressure
//************************************
void DrawPoint( CDC* pDC, POINT ptNew, POINT ptOld, BOOL fDown, BOOL fBeganRecording,
			    BOOL fFirstPoint, BOOL fWasOutOfRange, int nPenPressure )
{
	DrawPoint( pDC->m_hDC, ptNew, ptOld, fDown, fBeganRecording,
			   fFirstPoint, fWasOutOfRange, nPenPressure );
	return;
}

//************************************
// Method:    DrawPoint
// FullName:  DrawPoint
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: HDC hDC
// Parameter: POINT ptNew
// Parameter: POINT ptOld
// Parameter: BOOL fDown
// Parameter: BOOL fBeganRecording
// Parameter: BOOL fFirstPoint
// Parameter: BOOL fWasOutOfRange
// Parameter: int nPenPressure
//************************************
void DrawPoint( HDC hDC, POINT ptNew, POINT ptOld, BOOL fDown, BOOL fBeganRecording,
			    BOOL fFirstPoint, BOOL fWasOutOfRange, int nPenPressure )
{
	if( !hDC || ( ptNew.x < 0 ) || ( ptNew.y < 0 ) ) return;

#ifdef	_LOGPOINTS_
	if( !fBeganRecording ) OutputMessage( _T("Recording has not begun. Point/line not drawn."), LOG_TABLET );
	if( fFirstPoint ) OutputMessage( _T("First poing being drawn."), LOG_TABLET );
#endif

	HPEN hPen, hPenOld;
	HBRUSH hbrFill, hbrOld;
	POINT point;

	// When the pen is on the tablet (or left mouse button down)
	if( fDown && !IsHidden )
	{
		// calculate left/right coord. of ellipse even if not varying by pen pressure
		int nMax = ::GetMaxPenPressure(), nMin = ::GetMinPenPressure();
		int nMaxLT = ::GetMaxLineThickness();
		int nLeft = 0, nRight = 0;

//		if( ( nPenPressure > -1 ) && ( nMax > 0 ) )
		if( ( nPenPressure > -1 ) && ( ( nMax - nMin ) > 0 ) )
		{
			// 15Apr10: GMB: Line thickness variation is now between settable MinPP and MaxPP instead of 1 and device MaxPP
// 			nLeft = (int)( .25 + .5 * ::GetMaxLineThickness() * nPenPressure / nMax );
// 			nRight = (int)( .748 + .5 * ::GetMaxLineThickness() * nPenPressure / nMax );
			nLeft = (int)( .25 + .5 * ::GetMaxLineThickness() * ( nPenPressure - nMin ) / ( nMax - nMin ) );
			nRight = (int)( .748 + .5 * ::GetMaxLineThickness() * ( nPenPressure - nMin ) / ( nMax - nMin ) );
		}
		else
		{
			nLeft = (int)( .25 + .5 * __nEllipseSize );
			nRight = (int)( .748 + .5 * __nEllipseSize );
		}


		// move to starting point
		::MoveToEx( hDC, ptOld.x, ptOld.y, &point );

		//-------------------------
		// Line

		int nLineSize = __nLineSize;
		if( nLineSize > 0 )
		{
			int nLineThickness = 1;
			if( nPenPressure != -1 ) nLineThickness = nMaxLT * ( nPenPressure - nMin ) / ( nMax - nMin );
			else nLineThickness = __nLineSize;
			if( nLineThickness < 1 ) nLineThickness = 1;
			else if( nLineThickness > nMaxLT ) nLineThickness = nMaxLT;
			hPen = CreatePen( PS_SOLID, nLineThickness, IsHidden ? bgColor : __dwLineColor1 );
			hPenOld = (HPEN)::SelectObject( hDC, hPen );

			if( !fFirstPoint && !fWasOutOfRange ) ::LineTo( hDC, ptNew.x, ptNew.y );

			::SelectObject( hDC, hPenOld );
			::DeleteObject( hPen );
		}

		//-------------------------
		// Ellipse

		hPen = CreatePen( PS_SOLID, ELLIPSETHICK, IsHidden ? bgColor : __dwEllipseColor );
		hPenOld = (HPEN)::SelectObject( hDC, hPen );
		hbrFill = CreateSolidBrush( IsHidden ? bgColor : __dwEllipseColor );
		hbrOld = (HBRUSH)::SelectObject( hDC, hbrFill );

		::Ellipse( hDC,
				   ptOld.x - nLeft, ptOld.y - nLeft,
				   ptOld.x + nRight, ptOld.y + nRight );

		::SelectObject( hDC, hPenOld );
		::DeleteObject( hPen );
		::SelectObject( hDC, hbrOld );
		::DeleteObject( hbrFill );

#ifdef	_LOGPOINTS_
		CString szMsg;
		szMsg.Format( _T("Drawing (PENDOWN) from: %d, %d to: %d, %d"), ptOld.x, ptOld.y, ptNew.x, ptNew.y );
		::OutputMessage( szMsg, LOG_TABLET );
#endif
	}
	else if( fBeganRecording && !IsHidden )	// When not on tablet but already started recording
	{
		::MoveToEx( hDC, ptOld.x, ptOld.y, &point );

		hPen = CreatePen( PS_DOT, 1, IsHidden ? bgColor : __dwMPPColor );
		hPenOld = (HPEN)::SelectObject( hDC, hPen );

		if( !fFirstPoint && !fWasOutOfRange ) ::LineTo( hDC, ptNew.x, ptNew.y );

		::SelectObject( hDC, hPenOld );
		::DeleteObject( hPen );

#ifdef	_LOGPOINTS_
		CString szMsg;
		szMsg.Format( _T("Drawing (PENUP) from: %d, %d to: %d, %d"), ptOld.x, ptOld.y, ptNew.x, ptNew.y );
		::OutputMessage( szMsg, LOG_TABLET );
#endif
	}
}

//************************************
// Method:    DrawGripper
// FullName:  DrawGripper
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CDC * pDC
// Parameter: int x
// Parameter: int y
// Parameter: int z
// Parameter: int xold
// Parameter: int yold
// Parameter: int zold
// Parameter: unsigned long samples
// Parameter: HWND hwnd
// Parameter: BOOL fBeganRecording
// Parameter: BOOL fFirstPoint
// Parameter: BOOL fWasOutOfRange
// Parameter: UINT nRange
//************************************
void DrawGripper( CDC* pDC, int x, int y, int z, int xold, int yold, int zold,
				  unsigned long samples, HWND hwnd, BOOL fBeganRecording,
				  BOOL fFirstPoint, BOOL fWasOutOfRange, UINT nRange )
{
	IsHidden = FALSE;
	DrawGripper( pDC->m_hDC, x, y, z, xold, yold, zold, samples, hwnd,
				 fBeganRecording, fFirstPoint, fWasOutOfRange, nRange );
	return;
}

//************************************
// Method:    DrawGripper
// FullName:  DrawGripper
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: HDC hDC
// Parameter: int x
// Parameter: int y
// Parameter: int z
// Parameter: int xold
// Parameter: int yold
// Parameter: int zold
// Parameter: unsigned long samples
// Parameter: HWND hwnd
// Parameter: BOOL fBeganRecording
// Parameter: BOOL fFirstPoint
// Parameter: BOOL fWasOutOfRange
// Parameter: UINT nRange
//************************************
void DrawGripper( HDC hDC, int x, int y, int z, int xold, int yold, int zold,
				  unsigned long samples, HWND hwnd, BOOL fBeganRecording,
				  BOOL fFirstPoint, BOOL fWasOutOfRange, UINT nRange )
{
	if( !hDC || !hwnd || ( samples <= 0 ) || ( x < 0 ) || ( y < 0 ) ) return;

	HPEN hPen, hPenOld;
	POINT point;
	static int nextx = 0;
	int nextxold = 0;
	static int curSample = 0;
	static int wndrange = 0;
	
	// if first point, reset wndrange & cursample & calculate horizontal scale
	if( fFirstPoint )
	{
		RECT rect;
		::GetWindowRect( hwnd, &rect );
		wndrange = rect.right - rect.left;

		curSample = -1;
		nextx = 0;
		nextxold = 0;
	}

	// next sample
	curSample++;

	// next x position
	nextxold = nextx;
	nextx = (int)( curSample * wndrange / samples );

	// DRAW X
	// move drawing cursor position to old point
	::MoveToEx( hDC, nextxold, xold, &point );
	//-------------------------
	// create & select line pen
	hPen = CreatePen( PS_SOLID, __nLineSize, __dwLineColor1 );
	hPenOld = (HPEN)::SelectObject( hDC, hPen );
	// draw line to next point if not 1st point
	if( !fFirstPoint && !fWasOutOfRange ) ::LineTo( hDC, nextx, x );
	// delete line pen
	::SelectObject( hDC, hPenOld );
	::DeleteObject( hPen );

	// DRAW Y
	// move drawing cursor position to old point
	::MoveToEx( hDC, nextxold, yold, &point );
	//-------------------------
	// create & select line pen
	hPen = CreatePen( PS_SOLID, __nLineSize, __dwLineColor2 );
	hPenOld = (HPEN)::SelectObject( hDC, hPen );
	// draw line to next point if not 1st point
	if( !fFirstPoint && !fWasOutOfRange ) ::LineTo( hDC, nextx, y );
	// delete line pen
	::SelectObject( hDC, hPenOld );
	::DeleteObject( hPen );

	// DRAW Z
	// move drawing cursor position to old point
	::MoveToEx( hDC, nextxold, zold, &point );
	//-------------------------
	// create & select line pen
	hPen = CreatePen( PS_SOLID, __nLineSize, __dwLineColor3 );
	hPenOld = (HPEN)::SelectObject( hDC, hPen );
	// draw line to next point if not 1st point
	if( !fFirstPoint && !fWasOutOfRange ) ::LineTo( hDC, nextx, z );
	// delete line pen
	::SelectObject( hDC, hPenOld );
	::DeleteObject( hPen );
}

//************************************
// Method:    CMToDisplay
// FullName:  CMToDisplay
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double cmx
// Parameter: double cmy
// Parameter: HWND hwndTarget
// Parameter: long & x
// Parameter: long & y
// Parameter: BOOL fDesktop
// Parameter: BOOL fFromTop
//************************************
void CMToDisplay( double cmx, double cmy, HWND hwndTarget, long& x, long& y,
				  BOOL fDesktop, BOOL fFromTop )
{
	// display dimensions
	double dWidth = 0., dHeight = 0.;
	::GetDisplayDimension( dWidth, dHeight );
	if( ( dWidth == 0. ) || ( dHeight == 0. ) ) return;

	// get window rects
	RECT window_rect, dt_rect;
	::GetWindowRect( hwndTarget, &window_rect );
	::GetWindowRect( ::GetDesktopWindow(), &dt_rect );

	// how many pixels per cm
	double dpX = ( dt_rect.right - dt_rect.left ) / dWidth;
	double dpY = ( dt_rect.bottom - dt_rect.top ) / dHeight;

	// convert cm to pixel position
	// # of cm * pixels-per-centimeter
	// handle for DC is for recording window, thus 0,0 is from recording window
	// horizontal is straightforward as we just shift xpos to right if tablet only
	// if desktop, derived from bottom of desktop as opposed to bottom of recording window
	// must always reduce by recording window top..since that is 0,0 origin
	// IF fFromTop is selected, we are just converting cy to pos from 0,0
	if( !fFromTop )
	{
		long lHBase = fDesktop ? window_rect.left : 0;
		x = (long)( ( cmx * dpX ) - lHBase );
		long lVBase = fDesktop ? dt_rect.bottom : window_rect.bottom;
		y = (long)( ( lVBase - ( cmy * dpY ) ) - window_rect.top );
	}
	else
	{
		x = (long)( cmx * dpX );
		y = (long)( cmy * dpY );
	}

	// negatives make no sense for drawing
	if( x < 0 ) x = 0;
	if( y < 0 ) y = 0;
}

//************************************
// Method:    GetLeftViewMaximizeSize
// FullName:  GetLeftViewMaximizeSize
// Access:    public 
// Returns:   int
// Qualifier:
//************************************
int GetLeftViewMaximizeSize()
{
	return nLVMaxSize;
}

//************************************
// Method:    SetLeftViewMaximizeSize
// FullName:  SetLeftViewMaximizeSize
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: int nSize
//************************************
void SetLeftViewMaximizeSize( int nSize )
{
	nLVMaxSize = nSize;
}

//************************************
// Method:    GetNetworkDrives
// FullName:  GetNetworkDrives
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CStringList * pList
//************************************
BOOL GetNetworkDrives( CStringList* pList )
{
	HANDLE h = NULL;
	DWORD res = 0;
	DWORD cbBuffer = 16384;
	DWORD cEntries = -1;
	DWORD i = 0;
	CString szItem;
	LPNETRESOURCE lpnrLocal = NULL;

	if( !pList ) return FALSE;

	res = WNetOpenEnum( RESOURCE_CONNECTED, RESOURCETYPE_DISK, 0, NULL, &h );
	if( res == NO_ERROR )
	{
		lpnrLocal = (LPNETRESOURCE)GlobalAlloc( GPTR, cbBuffer );
		if( lpnrLocal )
		{
			do 
			{
				ZeroMemory( lpnrLocal, cbBuffer );
				res = WNetEnumResource( h, &cEntries, lpnrLocal, &cbBuffer );
				if( res == NO_ERROR )
				{
					for( i = 0; i < cEntries; i++ )
					{
						szItem.Format( _T("%s"), lpnrLocal->lpLocalName );
						pList->AddTail( szItem );
					}
				}
			}
			while( res != ERROR_NO_MORE_ITEMS );
			GlobalFree( (HGLOBAL)lpnrLocal );
			WNetCloseEnum( h );

			return TRUE;
		}
	}

	return FALSE;
}


//************************************
// Method:    DoTextOut
// FullName:  DoTextOut
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CDC * pDC
// Parameter: long x
// Parameter: long y
// Parameter: int press
// Parameter: int rate
// Parameter: BOOL fTC
// Parameter: int stroke
// Parameter: BOOL fS
//************************************
void DoTextOut( CDC* pDC, long x, long y, int press, int rate, BOOL fTC, int stroke,
				BOOL fS, COLORREF bgColor )
{
	if( !pDC ) return;

	// text style
	pDC->SetTextColor( RGB( 0, 0, 0 ) );
	pDC->SetBkColor( bgColor );
	CFont* pOldFont = NULL;
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_NORMAL;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	if( font.CreateFontIndirect( &lf ) ) pOldFont = pDC->SelectObject( &font );

	// position
	CString szMsg;
	szMsg.Format( _T("X:%5u Y:%5u P:%4d "), x, y, press );
	pDC->TextOut( 2, 2, szMsg );
	// sampling rate
	szMsg.Format( _T("Sampling Rate: %3d Hz "), rate );
	if( fTC ) pDC->TextOut( 2, 16, szMsg );
	// stroke
	szMsg.Format( _T("Stroke: %1d "), stroke );
	if( fS ) pDC->TextOut( 2, 30, szMsg );

	pDC->SelectObject( pOldFont );
}

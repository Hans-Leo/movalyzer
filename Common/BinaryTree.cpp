// Refer to header file for description of object

#include "StdAfx.h"
#include <iostream.h>
#include <os_pse/ostore.hh>
#include "BinaryTree.h"

///////////////////////////////////////////////////////////////////////////////

template <class T>
BinaryTree<T>::BinaryTree( T &d )
	: m_data( d ), m_pLeft( NULL ), m_pRight( NULL )
{}

template <class T>
BinaryTree<T>::~BinaryTree()
{
	if( m_pLeft ) delete this->m_pLeft;
	if( m_pRight ) delete this->m_pRight;
	delete &m_data;
}

template <class T>
void BinaryTree<T>::Add( T &d )
{
	if( d < m_data )
	{
		if( !m_pLeft )
		{
//			m_pLeft = new( os_database::of( this ),
//						   os_ts<BinaryTree<T> >::get() ) BinaryTree<T>( d );
			m_pLeft = new BinaryTree<T>( d );
		}
		else
		{
			m_pLeft->Add( d );
		}
	}
	else
	{
		if( !m_pRight )
		{
			m_pRight = new BinaryTree<T>( d );
//			m_pRight = new( os_database::of( this ),
//							os_ts<BinaryTree<T> >::get() ) BinaryTree<T>( d );
		}
		else
		{
			m_pRight->Add( d );
		}
	}
}

template <class T>
void BinaryTree<T>::Remove( T &d )
{
	T* pData = Find( d );
	if( pData )
	{
		objectstore::free_memory( pData );
	}
}

template <class T>
T* BinaryTree<T>::Find( T &d )
{
	if( d == m_data ) return &m_data;

	if( d < m_data )
	{
		if( m_pLeft ) return m_pLeft->Find( d );
	}
	else
	{
		if( m_pRight ) return m_pRight->Find( d );
	}

	return NULL;
}

// Debugging
#ifdef _DEBUG

template <class T>
void BinaryTree<T>::Print() const
{
	cout << m_data << endl;
}

template <class T>
void BinaryTree<T>::Dump() const
{
	if( m_pLeft ) m_pLeft->Dump();

	Dump();

	if( m_pRight ) m_pRight->Dump();
}

#endif	// _DEBUG

#ifndef	__MACROS_H__
#define	__MACROS_H__

// CString trimming
#define	TRIM(x)			x.TrimLeft(); x.TrimRight();
#define	TRIMSET(x,y)	x = y; TRIM( x );



// Check for null
#define	ISNULL(x)	( x == NULL )
#define	NOTNULL(x)	( x != NULL )



// Record movement macros
#define	DEFINE_RESET() \
	void ResetToStart();

#define	IMPLEMENT_RESET(class_name) \
	void class_name::ResetToStart() \
	{ \
		try \
		{ \
			if( !IsOpen() && !Open() ) return; \
			if( !IsBOF() ) MoveFirst(); \
		} \
		catch( CDBException* e ) \
		{ \
			OutputMessage( e->m_strError ); \
		} \
		catch(...) \
		{ \
			OutputMessage( _T("Unknown error.") ); \
		} \
	}

#define	DEFINE_GETNEXT() \
	BOOL GetNext();

#define	IMPLEMENT_GETNEXT(class_name) \
	BOOL class_name::GetNext() \
	{ \
		try \
		{ \
			if( !IsOpen() ) \
			{ \
				if( !Open() ) return FALSE; \
				if( IsBOF() && IsEOF() ) return FALSE; \
			} \
			if( !IsEOF() ) \
			{ \
				MoveNext(); \
				if( IsEOF() ) return FALSE; \
			} \
			else return FALSE; \
		} \
		catch( CDBException* e ) \
		{ \
			OutputMessage( e->m_strError ); \
			return FALSE; \
		} \
		catch(...) \
		{ \
			return FALSE; \
		} \
		return TRUE; \
	}

#define	DEFINE_GETPREV() \
	BOOL GetPrevious();

#define	IMPLEMENT_GETPREV(class_name) \
	BOOL class_name::GetPrevious() \
	{ \
		try \
		{ \
			if( !IsOpen() ) \
			{ \
				if( !Open() ) return FALSE; \
				if( IsBOF() && IsEOF() ) return FALSE; \
			} \
			if( !IsBOF() ) \
			{ \
				MovePrev(); \
				if( IsBOF() ) return FALSE; \
			} \
			else return FALSE; \
		} \
		catch( CDBException* e ) \
		{ \
			OutputMessage( e->m_strError ); \
			return FALSE; \
		} \
		catch(...) \
		{ \
			return FALSE; \
		} \
		return TRUE; \
	}

#define	DEFINE_MOVEMENT() \
	DEFINE_RESET() \
	DEFINE_GETNEXT() \
	DEFINE_GETPREV()

#define	IMPLEMENT_MOVEMENT(class_name) \
	IMPLEMENT_RESET(class_name ) \
	IMPLEMENT_GETNEXT(class_name) \
	IMPLEMENT_GETPREV(class_name)


#endif	// __MACROS_H__

#include "../MFC/mfc.h"
#include "DefFun.h"
#include <math.h>
#include <ctime>
#include <Time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef WIN32
#include <unistd.h>
#endif
#ifdef WIN32
#define stat _stat
#endif

//#define	SOFT_COLOR	RGB( (int)(0.64*255), (int)(0.72*255), (int)(0.77*255) )
//#define	SOFT_COLOR	RGB( 206, 228, 241 )
//#define	SOFT_COLOR	RGB( 229, 240, 247 )
// 16Apr09: GMB: Changed default penup color
#define	SOFT_COLOR	RGB( 224, 224, 224 )

static void* _OutputListWindow = NULL;
static CStdioFile	_LogFile;
static bool _Logging = true;
static bool _Rapid = false;
static bool _LogUI = true;
static bool _LogDB = true;
static bool _LogProc = true;
static bool _LogGraph = true;
static bool _LogTab = true;
static CString	_AppPath;

#define MAX_OUTPUT_LINES	5000
#define LINE_DELETE_COUNT	500

#if _MSC_VER < 1400
#define	strcpy_s	strcpy
#define	_ftime64_s	_ftime
#endif

/*************************************************/
/*************************************************/

#define	XOR			^
#define	MASK		'\xC8'
void Decode( CString& szPass )
{
	CString szCode;
	char c, d;
	int nCnt = szPass.GetLength();

	for( int i = 0; i < nCnt; i++ )
	{
		c = szPass.GetAt( i );
		d = c XOR MASK;
		szCode += d;
	}

	szPass = szCode;
}

/*************************************************/

void ZeroMemory(void* d, size_t l) {
	memset(d, 0, l);
}

/*************************************************/

void SetOutputWindow( void* pWnd )
{
	_OutputListWindow = pWnd;
}

/*************************************************/

void* GetOutputWindow()
{
	return _OutputListWindow;
}

/*************************************************/

void SetRapidProcessing( bool fOn )
{
	_Rapid = fOn;
}

bool GetRapidProcessing()
{
	return _Rapid;
}

/*************************************************/

void SetUILogOn( bool fOn ) { _LogUI = fOn; }
void SetProcLogOn( bool fOn ) { _LogProc = fOn; }
void SetDBLogOn( bool fOn ) { _LogDB = fOn; }
void SetGraphLogOn( bool fOn ) { _LogGraph = fOn; }
void SetTabletLogOn( bool fOn ) { _LogTab = fOn; }

/*************************************************/

void OutputMessage( CString szMsg, unsigned int nType )
{
/*
	if( !_OutputListWindow ) return;
	if( szMsg.IsEmpty() || ( szMsg == _T("") ) ) return;

	if( !_Rapid )
	{
		// if we reach MAX_OUTPUT_LINES, delete LINE_DELETE_COUNT lines from start
		int nCount = _OutputListWindow->GetItemCount();
		if( nCount >= MAX_OUTPUT_LINES )
		{
			for( int i = 0; i < LINE_DELETE_COUNT; i++ )
			{
				_OutputListWindow->DeleteItem( 0 );
			}
			nCount -= LINE_DELETE_COUNT;
		}

		// insert new item
		int nItem = _OutputListWindow->InsertItem( nCount, szMsg );
		if( nItem >= 0 )
		{
			_OutputListWindow->SetItemState( nItem, LVIS_SELECTED, LVIS_SELECTED );
			_OutputListWindow->EnsureVisible( nItem, true );
			if( nItem >= 1 ) _OutputListWindow->SetItemState( nItem - 1, ~LVIS_SELECTED, LVIS_SELECTED );
		}
//		_OutputListWindow->InsertItem( 0, szMsg );
	}

	if( ( ( nType == LOG_UI ) && _LogUI ) ||
		( ( nType == LOG_PROC ) && _LogProc ) ||
		( ( nType == LOG_GRAPH ) && _LogGraph ) ||
		( ( nType == LOG_TABLET ) && _LogTab ) ||
		( ( nType == LOG_DB ) && _LogDB ) )
		LogToFile( szMsg, nType );
*/
}

void OutputMessage(const char* msg, unsigned int nType) {
	// TODO
}

/*************************************************/

void OutputMessage( unsigned int nResourceID, unsigned int nType )
{
	// TODO: Could make this logging for now

	// CString szTemp;
	// szTemp.LoadString( nResourceID );
	// OutputMessage( szTemp, nType );
}

/*************************************************/

void ReadDynamicDefs( double& cm, double& sec, double& prsmin )
{
	cm = GetDeviceResolution();
	sec = GetSeconds();
	prsmin = (double)GetMinPenPressure();
}

/*************************************************/

void SetLoggingOn( bool fOn, CString szAppPath )
{
	_Logging = fOn;
	_AppPath = szAppPath;
//	_AppPath += _T("Actions.log");
}

/*************************************************/

bool IsLoggingOn()
{
	return _Logging;
}

/*************************************************/

void LogToFile( CString szData, unsigned int nType )
{
	if( !IsLoggingOn() ) return;
	if( szData.IsEmpty() || ( szData == _T("") ) ) return;

	if( !_LogFile.Open( _AppPath,
						CFile::modeWrite | CFile::modeCreate |
						CFile::shareDenyNone | CFile::modeNoTruncate ) )
	{
//		AfxMessageBox( _T("Unable to create log file.") );
		return;
	}

	timeval curTime;
	gettimeofday(&curTime, 0);
	int millitm = curTime.tv_usec / 1000;
	std::time_t t = (std::time_t)curTime.tv_sec;
	std::tm* now = std::localtime(&t);

	CString szLine, szTemp;
	szLine.Format("%4d%2d%2d %2d:%2d:%2d.%03i", now->tm_year, now->tm_mon+1, now->tm_mday,
				  now->tm_hour, now->tm_min, now->tm_sec, millitm);

	if( nType == LOG_UI ) szLine += _T("USER INTERFACE: ");
	else if( nType == LOG_PROC ) szLine += _T("PROCCESSING: ");
	else if( nType == LOG_DB ) szLine += _T("DATABASE: ");
	else if( nType == LOG_GRAPH ) szLine += _T("GRAPHING: ");
	else if( nType == LOG_TABLET ) szLine += _T("DIGITIZER: ");

	szData += _T("\n");
	szLine += szData;

	_LogFile.SeekToEnd();
	_LogFile.WriteString( szLine );

	CloseLogFile();
}

/*************************************************/

void CloseLogFile()
{
	if( _LogFile.m_pStream ) _LogFile.Close();
}

/*************************************************/
/*************************************************/

void SortStringListAlpha( CStringList* pList )
{
	if( !pList ) return;
	if( pList->GetCount() <= 0 ) return;

	CStringList lstTemp;
	CString szItem, szCur;
	POSITION pos = NULL, posCur = NULL, posLast = NULL;
	bool fAdded = false;

	pos = pList->GetHeadPosition();
	while( pos )
	{
		szItem = pList->GetNext( pos );
		fAdded = false;

		posCur = lstTemp.GetHeadPosition();
		if( posCur )
		{
			while( posCur )
			{
				posLast = posCur;
				szCur = lstTemp.GetNext( posCur );

				if( szItem <= szCur )
				{
					lstTemp.InsertBefore( posLast, szItem );
					fAdded = true;
					break;
				}
			}

			if( !fAdded ) lstTemp.AddTail( szItem );
		}
		else lstTemp.AddTail( szItem );
	}

	pList->RemoveAll();
	pos = lstTemp.GetHeadPosition();
	while( pos )
	{
		szItem = lstTemp.GetNext( pos );
		pList->AddTail( szItem );
	}
}

/*************************************************/

void SortFilesAlpha( CStringList* pList, CString szMask )
{
	if( !pList ) return;

	CFileFind ff;
	if( !ff.FindFile( szMask ) ) return;

	CString szItem, szFile;
	POSITION pos = NULL, posLast = NULL;
	bool fAdded = false;

	while( ff.FindNextFile() )
	{
		szFile = ff.GetFileName();
		fAdded = false;

		pos = pList->GetHeadPosition();
		if( pos )
		{
			while( pos )
			{
				posLast = pos;
				szItem = pList->GetNext( pos );

				if( szFile <= szItem )
				{
					pList->InsertBefore( posLast, szFile );
					fAdded = true;
					break;
				}
			}

			if( !fAdded ) pList->AddTail( szFile );
		}
		else pList->AddTail( szFile );
	}
	// Last one
	szFile = ff.GetFileName();
	fAdded = false;

	pos = pList->GetHeadPosition();
	if( pos )
	{
		while( pos )
		{
			posLast = pos;
			szItem = pList->GetNext( pos );

			if( szFile <= szItem )
			{
				pList->InsertBefore( posLast, szFile );
				fAdded = true;
			}
		}

		if( !fAdded ) pList->AddTail( szFile );
	}
	else pList->AddTail( szFile );
}

/*************************************************/

void SortFilesChrono( CStringList* pList, CString szMask )
{
	if( !pList ) return;

	CFileFind ff;
	if( !ff.FindFile( szMask ) ) return;

	CStringList lstFiles, lstTimes;
	CString szItem, szFile, szTime;
	POSITION posFile = NULL, posTime = NULL, posFileLast = NULL, posTimeLast = NULL;
	bool fAdded = false;
	struct timeval modTime;

	while( ff.FindNextFile() )
	{
		szFile = ff.GetFileName();
		szFile.MakeUpper();

		ff.GetLastWriteTime(&modTime);
		std::time_t t = (std::time_t)modTime.tv_sec;
		int millitm = modTime.tv_usec / 1000;
		std::tm* now = std::localtime(&t);

		szTime.Format( _T("%04d%02d%02d %02d:%02d:%02d:%03d"), now->tm_year, now->tm_mon+1,
					   now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec, millitm);
		fAdded = false;

		posFile = lstFiles.GetHeadPosition();
		posTime = lstTimes.GetHeadPosition();
		if( posFile )
		{
			while( posFile )
			{
				posFileLast = posFile;
				posTimeLast = posTime;
				szItem = lstFiles.GetNext( posFile );
				szItem = lstTimes.GetNext( posTime );

				if( szTime < szItem )
				{
					lstFiles.InsertBefore( posFileLast, szFile );
					lstTimes.InsertBefore( posTimeLast, szTime );
					fAdded = true;
					break;
				}
			}

			if( !fAdded )
			{
				lstFiles.AddTail( szFile );
				lstTimes.AddTail( szTime );
			}
		}
		else
		{
			lstFiles.AddTail( szFile );
			lstTimes.AddTail( szTime );
		}
	}

	// Last one
	szFile = ff.GetFileName();
	ff.GetLastWriteTime(&modTime);
	std::time_t t = (std::time_t)modTime.tv_sec;
	int millitm = modTime.tv_usec / 1000;
	std::tm* now = std::localtime(&t);
	szTime.Format( _T("%d%d%d %d:%d:%d:%d"), now->tm_year, now->tm_mon+1, now->tm_mday,
				   now->tm_hour, now->tm_min, now->tm_sec, millitm );
	fAdded = false;

	posFile = lstFiles.GetHeadPosition();
	posTime = lstTimes.GetHeadPosition();
	if( posFile )
	{
		while( posFile )
		{
			posFileLast = posFile;
			posTimeLast = posTime;
			szItem = lstFiles.GetNext( posFile );
			szItem = lstTimes.GetNext( posTime );

			if( szTime <= szItem )
			{
				lstFiles.InsertBefore( posFileLast, szFile );
				lstTimes.InsertBefore( posTimeLast, szTime );
				fAdded = true;
				break;
			}
		}

		if( !fAdded )
		{
			lstFiles.AddTail( szFile );
			lstTimes.AddTail( szTime );
		}
	}
	else
	{
		lstFiles.AddTail( szFile );
		lstTimes.AddTail( szTime );
	}

	pList->RemoveAll();
	posFile = lstFiles.GetHeadPosition();
	while( posFile )
	{
		szItem = lstFiles.GetNext( posFile );
		pList->AddTail( szItem );
	}
}

/*************************************************/
/*************************************************/

// sampling rate for input device
int _SamplingRate = 200;
// min pen pressure for tablet
int _MinPenPressure = 8;
// max pen pressure for tablet
// initialize to max - 1 to ensure warning is triggered for max
int _MaxPenPressure = 1023 - 1;
// max line thickness/ellipse diameter
int _MaxLineThickness = 20;
// device resolution of input device
double _DeviceResolution = 0.001;

/*************************************************/

#define	GRAYTONE		90
#define	COLORCONTRAST	90
#define	GEN_COLOR( phase, idx, total ) \
	GRAYTONE + ( COLORCONTRAST * cos( ( phase * 2.0 * PI ) + ( 2.0 * PI * ( idx / total ) ) ) )

COLORREF GetColorByIndex( int nIndex /*0-based*/, int nNumTotalColors )
{
	int nR = (int)( GRAYTONE + COLORCONTRAST * cos( 0.00 * 2.0 * PI + 2.0 * PI * nIndex / nNumTotalColors ) );
	int nG = (int)( GRAYTONE + COLORCONTRAST * cos( 0.33 * 2.0 * PI + 2.0 * PI * nIndex / nNumTotalColors ) );
	int nB = (int)( GRAYTONE + COLORCONTRAST * cos( 0.66 * 2.0 * PI + 2.0 * PI * nIndex / nNumTotalColors ) );

	return RGB( nR, nG, nB );
}

/*************************************************/

void CopyFonts( LOGFONT* dest, LOGFONT* src )
{
	if( !src || !dest ) return;

	dest->lfHeight = src->lfHeight;
	dest->lfWidth = src->lfWidth;
	dest->lfEscapement = src->lfEscapement;
	dest->lfOrientation = src->lfOrientation;
	dest->lfWeight = src->lfWeight;
	dest->lfItalic = src->lfItalic;
	dest->lfUnderline = src->lfUnderline;
	dest->lfStrikeOut = src->lfStrikeOut;
	dest->lfCharSet = src->lfCharSet;
	dest->lfOutPrecision = src->lfOutPrecision;
	dest->lfClipPrecision = src->lfClipPrecision;
	dest->lfQuality = src->lfQuality;
	dest->lfPitchAndFamily = src->lfPitchAndFamily;
	strcpy_s( dest->lfFaceName, src->lfFaceName );
}

/*************************************************/

COLORREF GetSoftColor()
{
	return SOFT_COLOR;
}

/*************************************************/

void SetSamplingRate( int nVal ) { _SamplingRate = nVal; }
int GetSamplingRate() { return _SamplingRate; }
double GetSeconds() { return( ( _SamplingRate != 0 ) ? ( 1.0 / _SamplingRate ) : ( 1.0 / 200 ) ); }
void SetMinPenPressure( int nVal ) { _MinPenPressure = nVal; }
int GetMinPenPressure() { return _MinPenPressure; }
void SetMaxPenPressure( int nVal ) { _MaxPenPressure = nVal; }
int GetMaxPenPressure() { return _MaxPenPressure; }
void SetMaxLineThickness( int nVal ) { _MaxLineThickness = nVal; }
int GetMaxLineThickness() { return _MaxLineThickness; }
void SetDeviceResolution( double dVal ) { _DeviceResolution = dVal; }
double GetDeviceResolution() { return _DeviceResolution; }

/*************************************************/
/*************************************************/

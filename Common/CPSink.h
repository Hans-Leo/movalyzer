// CPSink.h - SoftwareShield Client Protector sink header file
#pragma once

class AFX_EXT_CLASS CPSink : public CCmdTarget
{
	DECLARE_DYNCREATE( CPSink )
// construction/destruction
public:
	CPSink();           // protected constructor used by dynamic creation
	virtual ~CPSink() {}

// Implementation
protected:
	afx_msg void OnMissingLicenseFile( LPCTSTR, BSTR FAR* );
	afx_msg void OnAttemptReleaseCP( LPCTSTR, BSTR FAR* );
	afx_msg void OnAttemptRecover( LPCTSTR, BSTR FAR* );
	afx_msg void OnAttemptDeactivate( short FAR* );

	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

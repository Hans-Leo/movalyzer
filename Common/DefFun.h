#ifndef __DEFFUN_H__
#define	__DEFFUN_H__

#include "../MFC/mfc.h"

class CString;
class CStringList;

/*************************************************
   DefFun - version 2.1 - hlt - 29 Oct 1993
**************************************************

   Purpose: Default functions and constants

***********************************************************************/


#define PI 3.141592654

#define HIGHFLOAT +1.e6
#define LOWFLOAT -1.e6

#define TRUE 1
#define FALSE 0

#define BEL 0x07
#define BS 0x08
#define TAB 0x09
#define LF 0x0a
#define FF 0x0c
#define CR 0x0d
#define ESC 0x1b
#define SP 0x20

#define SQR(x)		((x)*(x))
#define TOTHETHIRD(x)	((x)*(x)*(x))
#define TOTHEFOURTH(x)	((x)*(x)*(x)*(x))
#define TOTHEFIFTH(x)	((x)*(x)*(x)*(x)*(x))
#define ABS(x)		((x) > 0 ? (x) : -(x))
#define MAX(x,y)	(((x) > (y)) ? (x) : (y))
#define MIN(x,y)	(((x) < (y)) ? (x) : (y))
#define NINT(x)		((x) >= 0 ? ((int) (x+0.5)) : ((int) (x-0.5)))
#define ZERO(x)   memset(x,0,sizeof(x))

#define S_START				((HRESULT)0x00000010L)
#define	S_PENDOWN			((HRESULT)0x00000020L)
#define	S_PENUP				((HRESULT)0x00000040L)
#define	S_TARGETGOOD		((HRESULT)0x00000080L)
#define	S_TARGETBAD			((HRESULT)0x00000100L)
#define	S_INVALIDATE		((HRESULT)0x00000200L)
#define	S_MAXSTROKES		((HRESULT)0x00000400L)
#define	S_BADHANDLE			((HRESULT)0x00000800L)
#define	S_TARGETGOODFIRST	((HRESULT)0x00001000L)
#define	S_TARGETGOODLAST	((HRESULT)0x00002000L)

///////////////////////////////////////////////////////////////////////////////
// encode/decode (encryption)
void Decode( CString& szPass );

// `ZeroMemory` was a windows function for `memset`
void ZeroMemory(void* d, size_t l);

// Window* for output
class CListCtrl;
void SetOutputWindow( void* pWnd );
void* GetOutputWindow();

void SetRapidProcessing(  bool fOn );
 bool GetRapidProcessing();
void ReadDynamicDefs( double& cm, double& sec, double& prsmin );

// LOGGING
// defines
#define	LOG_UNKNOWN		0
#define	LOG_UI			1
#define	LOG_PROC		2
#define	LOG_DB			3
#define	LOG_GRAPH		4
#define	LOG_TABLET		5
#define	LOG_ADMIN		6
// flagging
void SetUILogOn(  bool fOn );
void SetProcLogOn(  bool fOn );
void SetDBLogOn(  bool fOn );
void SetGraphLogOn(  bool fOn );
void SetTabletLogOn(  bool fOn );
// functons
 bool IsLoggingOn();
void CloseLogFile();
void OutputMessage( CString szMsg, unsigned int nType );
void OutputMessage(const char* msg, unsigned int nType);
void OutputMessage( unsigned int nResourceID, unsigned int nType );
void SetLoggingOn(  bool fOn, CString szAppPath );
void LogToFile( CString szData, unsigned int nType );

///////////////////////////////////////////////////////////////////////////////

void SortStringListAlpha( CStringList* pList );
void SortFilesAlpha( CStringList* pList, CString szMask );
void SortFilesChrono( CStringList* pList, CString szMask );

///////////////////////////////////////////////////////////////////////////////

void SetSamplingRate( int nVal );
int GetSamplingRate();
double GetSeconds();
void SetMinPenPressure( int nVal );
int GetMinPenPressure();
void SetMaxPenPressure( int nVal );
int GetMaxPenPressure();
void SetMaxLineThickness( int nVal );
int GetMaxLineThickness();
void SetDeviceResolution( double dVal );
double GetDeviceResolution();

///////////////////////////////////////////////////////////////////////////////

COLORREF GetColorByIndex( int nIndex /*0-based*/, int nNumTotalColors );
COLORREF GetSoftColor();
void CopyFonts( LOGFONT* dest, LOGFONT* src );

///////////////////////////////////////////////////////////////////////////////
// Diagnostic method to determine how long it takes to do something
// StopWatch
//		nStart0Stop1: int			- starts or stops the stopwatch
//		start		: LARGE_INTEGER	- variable to receive start info (reference)
//									- variable to pass in previously received start
//		stop		: LARGE_INTEGER	- variable to receive stop info (reference)
//		freq		: LARGE_INTEGER - ***IMPORTANT*** QueryPerformanceFrequency MUST
//									- be called one time from your application before
//									- this can be called. QueryPerformanceFrequency is
//									- valid if your hardware supports high-resolution
//									- performance counters. If not, return value = -1
//		RETURN VALUE = 0 for start, duration for stop
//
//	NOTE: You must include a preproccessor definition of _USE_COUNTER for this to compile
#ifdef _USE_COUNTER
#include <winbase.h>
inline double StopWatch( int nStart0Stop1, LARGE_INTEGER& start, LARGE_INTEGER& stop, LARGE_INTEGER freq )
{
	double dRet = 0.;

	// are we starting?
	if( nStart0Stop1 == 0 )
	{
		QueryPerformanceCounter( &start );
	}
	else	// we are stopping..calculate duration
	{
		QueryPerformanceCounter( &stop );
		if( ( freq.LowPart != 0 ) || ( freq.HighPart != 0 ) )
		{
			dRet = (double)( stop.LowPart - start.LowPart );
			if( dRet < 0 ) dRet += 2^32;
			dRet /= ( freq.LowPart + freq.HighPart * 2^32 );
		}
		else dRet = -1.;
	}

	return dRet;
}
#endif	// _USE_COUNTER

///////////////////////////////////////////////////////////////////////////////

// OLE/COM/DCOM helpful macros (VS = VSET..kinda like VT = VType..where V = VARIANT)
// NOTE: shouldnt these friggin things already be defined? I cannot find
// NOTE: umm..lazy..only adding those i feel i could use

#define VS_ bool( x, val ) V_VT( x ) = VT_ bool; V_ bool( x ) = val;
#define VS_I2( x, val ) V_VT( x ) = VT_I2; V_I2( x ) = val;
#define VS_I4( x, val ) V_VT( x ) = VT_I4; V_I4( x ) = val;
#define VS_R8( x, val ) V_VT( x ) = VT_R8; V_R8( x ) = val;
#define VS_BSTR( x, val ) V_VT( x ) = VT_BSTR; V_BSTR( x ) = val;
#define VS_CSTRING( x, val ) V_VT( x ) = VT_BSTR; V_BSTR( x ) = val.AllocSysString();

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#endif	//__DEFFUN_H__

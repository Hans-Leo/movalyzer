// UserObj.h - Declaration of UserObj & TrialRunInfo
//
// Used to house the current user, subject, run-experiment etc
///////////////////////////////////////////////////////////////////////////////

#pragma once

// Tree item types
#define	TI_UNKNOWN					0
#define	TI_EXPERIMENT_FOLDER		1
#define	TI_EXPERIMENT				2
#define	TI_GROUP_FOLDER				3
#define	TI_GROUP					4
#define	TI_SUBJECT_FOLDER			5
#define	TI_SUBJECT					6
#define	TI_CONDITION_FOLDER			7
#define	TI_CONDITION				8
#define	TI_HWRTRIAL_FOLDER			9
#define	TI_FRDTRIAL_FOLDER			10
#define	TI_TRIAL					11
#define	TI_STIMULUS_FOLDER			12
#define	TI_STIMULUS					13
#define	TI_ELEMENT_FOLDER			14
#define	TI_ELEMENT					15
#define	TI_CATEGORY_FOLDER			16
#define	TI_CATEGORY					17
#define	TI_CATEGORY_HOLDER			18
#define	TI_FEEDBACK_FOLDER			19
#define	TI_FEEDBACK					20
#define	TI_IMAGE_FOLDER				21
#define	TI_IMAGE					22

// Actions/State
#define	STATE_NONE					0
#define	STATE_RUNEXPERIMENT			1
#define	STATE_REPROCESS				2
#define	STATE_SUMMARIZE				3
#define	STATE_TEST					4
#define	STATE_BACKUP				5
#define	STATE_STIMULUS				6
#define	STATE_IMPORTEXPORT			7
#define STATE_SHOWEXAMPLE			8

// Actions (LEGACY)
#define	ACTION_NONE					0
#define	ACTION_RUNEXPERIMENT		1
#define	ACTION_REPROCESS			2
#define	ACTION_SUMMARIZE			3
#define	ACTION_TEST					4
#define	ACTION_BACKUP				5
#define	ACTION_STIMULUS				6
#define	ACTION_IMPORTEXPORT			7

interface IProcSetting;
struct TrialRunInfo;

class AFX_EXT_CLASS UserObj : public CObject
{
public:
	// construction/destruction
	UserObj( CString& szID, CString& szDesc, CString& szPass,
			 CString& szPath, CString& szBackup, CString& szSig,
			 CString& szDelim, BOOL fPrivate, BOOL fProperLoc,
			 BOOL fWriteable, CString& szSiteID, CString& szSiteDesc,
			 BOOL fPrivacy = TRUE, CListCtrl* pMsgView = NULL );
	~UserObj();

	// reset the values (non-db)
	void Reset();
	// prep for the experiment by filling in the TrialRunInfo object, etc
	BOOL PrepareForExperiment( CStringList* pListConditions );

	// user id
	CString			m_szID;
	// user description
	CString			m_szDesc;
	// user password
	CString			m_szPass;
	// user root data path
	CString			m_szDataPath;
	// user backup path
	CString			m_szBackupPath;
	// user signature
	CString			m_szSig;
	// user tree delimiter
	CString			m_szDelim;
	// is this user a private user?
	BOOL			m_fPrivate;
	// is the user data in a proper/usable location (acc. to vista standards)
	BOOL			m_fProperLoc;
	// is the user data in a writeable location?
	BOOL			m_fWriteable;
	// user site id
	CString			m_szSiteID;
	// user site description
	CString			m_szSiteDesc;
	// privacy protection on/off
	BOOL			m_fPrivacy;
	// input type (mouse/tablet/gripper)
	short			m_nInputType;
	// tablet/display dimensions
	double			m_dTabletX;
	double			m_dTabletY;
	double			m_dDisplayX;
	double			m_dDisplayY;

	// non-db-related
	UINT			m_nState;
	// current subject
	CString			m_szSubjID;
	CString			m_szSubjCode;
	CString			m_szSubj;
	// current group
	CString			m_szGrpID;
	CString			m_szGrp;
	// current condition
	CString			m_szCondID;
	CString			m_szCond;
	// current experiment
	CString			m_szExpID;
	CString			m_szExp;
	// current trial
	CString			m_szTrial;
	// for recording only a single trial (redo)
	BOOL			m_fTrialOnly;

	// during recording... (from old mechanism)
	BOOL			m_fRunTrial;
	BOOL			m_fRedoTrial;
	BOOL			m_fContinue;

	// stimuli
	CString			m_szStim;
	CString			m_szWStim;
	CString			m_szPStim;
	BOOL			m_fStimOnly;

	// experiment settings
	IProcSetting*	m_pPS;

	// TrialRunInfo
	TrialRunInfo*	m_pTRI;

	// output window
	CListCtrl*		m_pMsgView;

	// gripper stuff
	double			m_dMagnetForce;
};

// condition event table for sounds and scripts
struct AFX_EXT_CLASS CondEvent {
public:
	CondEvent() : fUse(FALSE), nSound(0), fTone(FALSE), nFreq(0), nDur(0), fTrigger(FALSE), nScriptType(0) {}
	int nSound;
	BOOL fUse;
	BOOL fTone;
	int nFreq;
	int nDur;
	CString szSound;
	BOOL fTrigger;
	int nScriptType;
	CString szScript;
};

// all of condition's sounds/scripts
class AFX_EXT_CLASS CondEvents : public CObject {
public:
	CondEvents();
	~CondEvents();
	CString szCondID;
	CondEvent*	pEvents;
};

// houses condition events for each condition
class AFX_EXT_CLASS TrialRunEvents {
public:
	TrialRunEvents() {}
	~TrialRunEvents();
	CObList lstCondEvents;
	CondEvents* GetConditionEvents( CString szCondID );
};

// shared structure for use in tracking current trial information
// during running an experiment
struct AFX_EXT_CLASS TrialRunInfo
{
	TrialRunInfo() : pPS( NULL ), pConditions( NULL ), nEventCount( 0 ), pEvents( NULL ) {}
	// TODO: The following need to be removed over time
	CString			szExpID;
	CString			szExp;
	CString			szSubjID;
	CString			szSubj;
	CString			szGrpID;
	CString			szTrial;
	IProcSetting*	pPS;

	// events
	int				nEventCount;
	TrialRunEvents*	pEvents;

	// TODO: the rest can either be moved to UserObj or left as is
	CStringList*	pConditions;
	BOOL			fQuest;
	BOOL			fMaximize;
	BOOL			fNewTrial;
	CString			szCurCond;
	BOOL			fProcess;
	BOOL			fDisplay;
	BOOL			fHWR;
	BOOL			fTF;
	BOOL			fSumm;
	BOOL			fSelectSumm;
	BOOL			fOnlySumm;
	BOOL			fGrpOnlySumm;
	BOOL			fNormDBSumm;
	BOOL			fAnalysis;
	BOOL			fSeg;
	BOOL			fExt;
	BOOL			fExtExp;
	BOOL			fConsis;
	BOOL			fConsis2;
	double			dStart;
	double			dSecs;
	double			lTicks;
	double			dClockSecsMax;
	int				nTrial;
	int				nRep;
	HTREEITEM		hLast;
	double			dPDuration;
	double			dPLatency;
	BOOL			fImmediately;			// Begin recording immediately, else on pen down
	BOOL			fStopWrongTarget;		// Stop recording if wrong target is hit
	BOOL			fStartFirstTarget;		// Start recording if reach first correct target
	BOOL			fStopLastTarget;		// Stop recording if reach last correct target
	double			dWDuration;
	double			dWLatency;
	double			dMagnetForce;
	int				nTrialMode;
	BOOL			fSen2Word;

	BOOL			fTFA, fTFJ, fTFR, fTFU, fTFO;
	BOOL			fSegF, fSegL, fSegO, fSegS, fSegM, fSegA, fSegV, fSegMM, fSegJ;
	BOOL			fExtS, fExt3, fExtO, fExt2;
	BOOL			fConA, fConC, fConM, fConN, fConS, fConU, fConO, fConD;
	double			dFF, dBeta;
	short			nDec;
};

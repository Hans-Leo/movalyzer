#ifndef __SHARED_H__
#define	__SHARED_H__

bool LoadBMP( LPCTSTR sBMPFile, HGLOBAL *phDIB, CPalette *pPal );
void DrawDIB( CDC* pDC, HGLOBAL hDIB, CPalette *pPal, RECT& rect );
void DrawDIB( CDC* pDC, HGLOBAL hDIB, CPalette *pPal, long lX, long lY );
HANDLE DDBToDIB( CBitmap& bitmap, DWORD dwCompression, CPalette* pPal ) ;
HBITMAP DIBToDDB( HANDLE hDIB );
bool WriteDIB( LPTSTR szFile, HANDLE hDIB);
bool WriteWindowToDIB( LPTSTR szFile, CWnd *pWnd );
bool LoadBMPImage( LPCTSTR sBMPFile, CBitmap& bitmap, CPalette *pPal );

void SetAspectRatio( double arX, double arY );
void GetAspectRatio( double& arX, double& arY );
void SetDisplayDimension( double ddX, double ddY );
void GetDisplayDimension( double& ddX, double& ddY );
void SetTabletDimension( double dtX, double dtY );
void GetTabletDimension( double& dtX, double& dtY );
void SetBackgroundColor( COLORREF bg );
void SetIsHidden( bool fHide );

void DrawSettings( bool LineFixed, short LineSize, DWORD LineColor1,
							   DWORD LineColor2, DWORD LineColor3, DWORD MPPColor,
							   DWORD EllipseColor, short EllipseSize );

void DrawPoint( CDC* pDC, POINT ptNew, POINT ptOld, bool fDown,
							bool fBeganRecording, bool fFirstPoint,
							bool fWasOutOfRange, int nPenPressure = -1 );
void DrawPoint( HDC hDC, POINT ptNew, POINT ptOld, bool fDown,
							bool fBeganRecording, bool fFirstPoint,
							bool fWasOutOfRange, int nPenPressure = -1 );
void DrawGripper( CDC* pDC, int x, int y, int z, int xold, int yold, int zold,
							  unsigned long samples, HWND hwnd, bool fBeganRecording,
							  bool fFirstPoint, bool fWasOutofRange, UINT nRange );
void DrawGripper( HDC hDC, int x, int y, int z, int xold,
							  int yold, int zold, unsigned long samples,
							  HWND hwnd, bool fBeganRecording,
							  bool fFirstPoint, bool fWasOutofRange, UINT nRange );
void DoTextOut( CDC*, long x, long y, int press, int rate, bool fTC,
						    int stroke, bool fS, COLORREF bgColor );

// CMToDisplay - conver centimeter x & y position to display position
//
// cmx = x position in centimeters from lower,left
// cmy = y position in centimeters from lower,left
// hwndTarget = handle to recording window
// fDesktop = results calculated from lower,left of desktop (else recording window)
// fFromTop = calculate y position relative to top (default bottom)
// x & y = results in pixels from lower left
void CMToDisplay( double cmx, double cmy, HWND hwndTarget,
							  long& x, long& y, bool fDesktop = TRUE,
							  bool fFromTop = FALSE );

// Leftview size for maximization
int GetLeftViewMaximizeSize();
void SetLeftViewMaximizeSize( int nSize /*pixels*/ );

// get network resource drives
bool GetNetworkDrives( CStringList* );

#endif	// __SHARED_H__

// CPSink.cpp : implementation file
//

#include "stdafx.h"
#include "CPSink.h"
#include "Security.h"

#if defined _DEBUG
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CPSink

IMPLEMENT_DYNCREATE( CPSink, CCmdTarget )

BEGIN_MESSAGE_MAP( CPSink, CCmdTarget )
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP( CPSink, CCmdTarget )
	DISP_FUNCTION( CPSink, "OnMissingLicenseFile", OnMissingLicenseFile, VT_EMPTY, VTS_BSTR VTS_PBSTR )
	DISP_FUNCTION( CPSink, "OnAttemptReleaseCP", OnAttemptReleaseCP, VT_EMPTY, VTS_BSTR VTS_PBSTR )
	DISP_FUNCTION( CPSink, "OnAttemptDeactivate", OnAttemptDeactivate, VT_EMPTY, VTS_PI2 )
	DISP_FUNCTION( CPSink, "OnAttemptRecover", OnAttemptRecover, VT_EMPTY, VTS_BSTR VTS_PBSTR )
END_DISPATCH_MAP()

// {A2DBC1D3-5737-4F43-9742-076628D55598}
static const IID IID_ICPSink =
{ 0xa2dbc1d3, 0x5737, 0x4f43, { 0x97, 0x42, 0x7, 0x66, 0x28, 0xd5, 0x55, 0x98 } };

BEGIN_INTERFACE_MAP( CPSink, CCmdTarget )
	INTERFACE_PART( CPSink, __uuidof( ISSCProtectorEvents ), Dispatch )
END_INTERFACE_MAP()

CPSink::CPSink()
{
	EnableAutomation();
}

//---------------------------------------------------------------------------
// The ClientProtector will fire this event during the scope of the StartUp call if
// the Main License File is missing.  We will allow the user to browse for it.
void CPSink::OnMissingLicenseFile( LPCTSTR ExpectedFileNamePath, BSTR FAR* ActualFileNamePath ) 
{
    *ActualFileNamePath = CComBSTR("").m_str;
	if( ::MessageBox( NULL,
					  _T("Your license file is missing.  Would you like to browse for it?"),
					  _T("Security"),
					  MB_YESNO | MB_ICONQUESTION ) == IDYES  )
    {
		CFileDialog * pFileDlg = new CFileDialog( TRUE, NULL, ExpectedFileNamePath, 0, NULL, AfxGetMainWnd() );
		if( pFileDlg )
		{
			if( pFileDlg->DoModal() ) *ActualFileNamePath = CComBSTR( pFileDlg->GetPathName() ).m_str;
			delete pFileDlg;
		}
    }
}

//---------------------------------------------------------------------------
// The ClientProtector will fire this event if the Copy Protection needs to
// be released from the scope of a StartUp call.
void CPSink::OnAttemptReleaseCP( LPCTSTR ReleaseCPAuthRequestCode, BSTR FAR* InputActivationCode ) 
{
	*InputActivationCode = CComBSTR( _T("") ).m_str;
}

//---------------------------------------------------------------------------
// The ClientProtector will fire this event if a recovery attempt
// be released from the scope of a StartUp call.
void CPSink::OnAttemptRecover( LPCTSTR RecoverAuthRequestCode, BSTR FAR* InputActivationCode ) 
{
	*InputActivationCode = CComBSTR( _T("") ).m_str;
}

//---------------------------------------------------------------------------
// The ClientProtector will fire this event if a request to deactivate is made
// giving the user a last chance to abort deactivation.
void CPSink::OnAttemptDeactivate( short FAR* AllowDeactivate ) 
{
	const int result =
		::MessageBox( NULL,
					  _T("This action will permanently deactivate the license on this machine. Are you sure you want to do this?"),
					  _T("Security"),
					  MB_YESNO | MB_ICONQUESTION );
	*AllowDeactivate = (short)( result == IDYES );
}

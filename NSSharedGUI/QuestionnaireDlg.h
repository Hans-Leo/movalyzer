#pragma once

// QuestionnaireDlg.h : header file
//
#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireDlg dialog

class AFX_EXT_CLASS QuestionnaireDlg : public CBCGPDialog
{
// Construction
public:
	QuestionnaireDlg(CStringList* pListH, CStringList* pListQ,
					 CStringList* pListID, CStringList* pListS,
					 CStringList* pListI, CStringList* pListP,
					 CStringList* pListN, CStringList* pListCH,
					 BOOL fEdit = FALSE, int nIdx = 0,
					 BOOL fMaster = FALSE, CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_QUEST };
	CString			m_szQuestion;
	CString			m_szID;
	BOOL			m_fHdr;
	BOOL			m_fPrivate;
	BOOL			m_fNumeric;
	CString			m_szColHeader;
	NSToolTipCtrl	m_tooltip;
	CStringList*	m_pListH;		// is header list
	CStringList*	m_pListQ;		// question list
	CStringList*	m_pListID;		// id list
	CStringList*	m_pListS;		// state list
	CStringList*	m_pListI;		// item # list
	CStringList*	m_pListP;		// is private list
	CStringList*	m_pListN;		// numeric list
	CStringList*	m_pListCH;		// column header list
	BOOL			m_fEdit;
	int				m_nIdx;
	BOOL			m_fModified;
	BOOL			m_fAdd;
	BOOL			m_fNew;
	BOOL			m_fClose;
	BOOL			m_fMaster;
	CBCGPButton		m_bnPrev;
	CBCGPButton		m_bnNext;
	BOOL			m_fCanClose;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	virtual void OnCancel();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	afx_msg void OnBnPrevq();
	afx_msg void OnBnNextq();
	virtual BOOL OnInitDialog();
	afx_msg void OnChkHdr();
	afx_msg void OnChkPrivate();
	afx_msg void OnChkNumeric();
	afx_msg void OnChangeEditID();
	afx_msg void OnChangeEditQ();
	afx_msg void OnChangeEditCH();
	afx_msg void OnKillfocusEditId();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// ConditionPageWord.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ConditionPageWord dialog

interface INSCondition;

class AFX_EXT_CLASS ConditionPageWord : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ConditionPageWord)

// Construction
public:
	ConditionPageWord( INSCondition* = NULL, BOOL fNew = TRUE, CStringList* = NULL,
					   CStringList* = NULL );
	~ConditionPageWord();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_WORD };
	BOOL			m_fNew;
	BOOL			m_fRecord;
	BOOL			m_fProcess;
	CComboBox		m_cboParent;
	int				m_nWord;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;
	CString			m_szParent;
	CStringList*	m_plstConds;
	CStringList*	m_plstCondsWord;
	CString			m_szID;
	CColoredButton	m_chkRecord;
	CColoredButton	m_chkProcess;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* pHelpInfo );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnChkProcess();
	afx_msg void OnChkRecord();
	afx_msg void OnSelchangeCboParent();
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnKillfocusEditWord();
};

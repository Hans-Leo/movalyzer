// ConditionPageMain.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"
#include "UserMessages.h"

/////////////////////////////////////////////////////////////////////////////
// ConditionPageMain dialog

interface INSCondition;

class AFX_EXT_CLASS ConditionPageMain : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ConditionPageMain)

// Construction
public:
	ConditionPageMain( INSCondition* = NULL, CString szExpID = _T(""), BOOL fNew = TRUE,
					   BOOL fReps = FALSE, BOOL fDup = FALSE );
	~ConditionPageMain();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_MAIN };
	CString			m_szExpID;
	CString			m_szID;
	CString			m_szDesc;
	int				m_nReps;
	CString			m_szNotes;
	CString			m_szInstr;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fReps;
	CStringList		m_lstItems;
	BOOL			m_fNew;
	BOOL			m_fDup;
	BOOL			m_fInit;
	CBCGPURLLinkButton		m_linkInstr;
	CBCGPURLLinkButton		m_linkPattern;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp(HELPINFO* pHelpInfo);
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEditId();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickInstr();
	afx_msg void OnClickNotes();
	afx_msg void OnClickTest();
	afx_msg void OnClickHint();
	afx_msg void OnClickHintInstr();
	DECLARE_MESSAGE_MAP()
};

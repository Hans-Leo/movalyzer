#pragma once
#include "afxwin.h"
#include "afxdtctl.h"
#include "afxcmn.h"
#include "NSTooltipCtrl.h"

// ZResultsDlg dialog

class AFX_EXT_CLASS ZResultsDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(ZResultsDlg)

public:
	ZResultsDlg( CString szExp, CString szSubj = _T(""), CWnd* pParent = NULL );   // standard constructor
	virtual ~ZResultsDlg() {}

// Dialog Data
	enum { IDD = IDD_Z_HISTORY };
	CString			m_szExp;
	CString			m_szSubj;
	CComboBox		m_cboSubjects;
	int				m_nSubject;
	CDateTimeCtrl	m_dtcFrom;
	CDateTimeCtrl	m_dtcTo;
	CListCtrl		m_list;
	int				m_nNumTests;
	double			m_dTestTime;
	CString			m_szTestTime;
	int				m_nImpCount;
	int				m_nSubjImpCount;
	CStringList		m_lstSubjects;
	double			m_dZCritical;
	CImageList		m_ImageList;
	BOOL			m_fGlobal;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fWideDateRange;

public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

public:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

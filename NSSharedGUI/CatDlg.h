#pragma once

// CatDlg dialog

class AFX_EXT_CLASS CatDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(CatDlg)

public:
	CatDlg(CWnd* pParent = NULL, BOOL fNew = FALSE);   // standard constructor
	virtual ~CatDlg();

// Dialog Data
	enum { IDD = IDD_CAT };
	BOOL			m_fNew;
	CToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_szID;
	CString m_szDesc;
	int m_nType;
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual void OnOK();
public:
	afx_msg void OnEnKillfocusEditAbbr();
	CComboBox m_cboType;
};

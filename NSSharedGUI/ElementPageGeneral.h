#pragma once

// ElementPageGeneral.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ElementPageGeneral dialog

interface IElement;

class AFX_EXT_CLASS ElementPageGeneral : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ElementPageGeneral)

// Construction
public:
	ElementPageGeneral( IElement* = NULL, BOOL fDup = FALSE );
	~ElementPageGeneral();

// Dialog Data
	enum { IDD = IDD_PAGE_ELMT_GENERAL };
	CString			m_szID;
	CString			m_szDesc;
	BOOL			m_fTarget;
	CString			m_szBMP;
	CBCGPComboBox	m_cboBMP;
	double			m_dStart;
	double			m_dDuration;
	BOOL			m_fIndefinite;
	IElement*		m_pE;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	BOOL			m_fIsImage;
	CBCGPButton		m_bnBrowse;
	CString			m_szCatID;
	BOOL			m_fDup;
	BOOL			m_fInit;
	CBCGPComboBox	m_cboCat;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL IsTarget();
	BOOL IsPattern();
	BOOL IsImage();
	BOOL DoHelp( HELPINFO* );
protected:
	void FillImageList();
// message map functions
	afx_msg void OnKillfocusEditId();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnBrowse();
	afx_msg void OnRdoClick();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickIndefinite();
	DECLARE_MESSAGE_MAP()
};

// AddElementDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "AddElementDlg.h"
#include "ElementSheet.h"
#include "RelationshipDlg.h"
#include "Elements.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddElementDlg dialog

BEGIN_MESSAGE_MAP(AddElementDlg, CBCGPDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_WM_HELPINFO()
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST, OnLvnKeydownList)
END_MESSAGE_MAP()

AddElementDlg::AddElementDlg( CStringList* pExisting, CWnd* pParent,
							  BOOL fSingleSel, BOOL fNoChange )
	: AddDlg( pExisting, pParent, fSingleSel ), m_fNoChange( fNoChange )
{
}

AddElementDlg::~AddElementDlg()
{
	Cleanup();
}

void AddElementDlg::Cleanup()
{
}

void AddElementDlg::InsertColumns()
{
	// List size (width)
	RECT rect;
	m_list.GetClientRect( &rect );
	int nWidth = rect.right - rect.left;

	CString szCol;
	// Column Headers
	szCol.LoadString( IDS_COL_ID );
	m_list.InsertColumn( 0, szCol, LVCFMT_LEFT, 70, 0 );
	szCol.LoadString( IDS_COL_DESC );
	m_list.InsertColumn( 1, szCol, LVCFMT_LEFT, nWidth - 70, 1 );
}

void AddElementDlg::FillList()
{
	// Loop through all
	Elements elem;
	if( !elem.IsValid() ) return;

	CString szID, szDesc, szItem;
	int nItem = 0, nImage = GetImageNum();

	HRESULT hr = elem.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		elem.GetID( szID );
		if( m_pExisting && ( m_pExisting->Find( szID ) == NULL ) )
		{
			nItem = m_list.InsertItem( nItem, szID, nImage );
			if( nItem != -1 )
			{
				elem.GetDescription( szDesc );

				m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );

				nItem++;
			}
		}

		hr = elem.GetNext();
	}

	if( nItem <= 0 ) OnClickBnAdd();
}

/////////////////////////////////////////////////////////////////////////////
// AddElementDlg message handlers

BOOL AddElementDlg::OnInitDialog() 
{
	// Window Title
	CString szTitle = _T("Add Element(s)");
	SetWindowText( szTitle );

	m_tooltip.SetThisIcon( IDR_ELEM );

	if( m_fNoChange )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_DEL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ADD );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDIT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	return AddDlg::OnInitDialog();
}

void AddElementDlg::OK()
{
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	CString szID, szDesc, szItem;
	int nIdx = 0;
	while( pos )
	{
		nIdx = m_list.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szID = m_list.GetItemText( nIdx, 0 );
			szDesc = m_list.GetItemText( nIdx, 1 );

			CString szDelim;
			::GetDelimiter( szDelim );
			szItem.Format( _T("%s%s%s"), szID, szDelim, szDesc );
			m_lstNew.AddTail( szItem );
		}
	}
}

int AddElementDlg::Add()
{
	Elements elem;
	if( !elem.IsValid() ) return -1;

	if( elem.DoAdd( this ) )
	{
		CString szID;
		elem.GetID( szID );
		int nItem = m_list.GetItemCount(), nImage = GetImageNum();
		nItem = m_list.InsertItem( nItem, szID, nImage );
		if( nItem != -1 )
		{
			CString szDesc;
			elem.GetDescription( szDesc );
			m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );

			return nItem;
		}
	}

	return -1;
}

BOOL AddElementDlg::Edit( int nItem )
{
	Elements elem;
	if( !elem.IsValid() ) return FALSE;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	if( elem.DoEdit( szID, this ) )
	{
		CString szDesc;
		elem.GetDescription( szDesc );
		m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
	}

	return TRUE;
}

BOOL AddElementDlg::Delete( int nItem )
{
	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	Elements elem;
	if( !elem.IsValid() ) return FALSE;

	if( SUCCEEDED( elem.Find( szID ) ) )
	{
		if( FAILED( elem.Remove() ) )
		{
			BCGPMessageBox( IDS_DEL_ERR );
			return FALSE;
		}
	}

	return TRUE;
}

BOOL AddElementDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("stimuli.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void AddElementDlg::Relationships( int nItem )
{
	Elements elem;
	if( !elem.IsValid() ) return;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( SUCCEEDED( elem.Find( szID ) ) )
	{
		CString szItem;
		elem.GetDescriptionWithID( szItem );

		RelationshipDlg d( this, REL_ELEMENT, szItem );
		d.DoModal();
	}
	else BCGPMessageBox( _T("Could not find element.") );
}

#pragma once

#include "NSTooltipCtrl.h"

interface IElement;

// ElementPageResult dialog

class AFX_EXT_CLASS ElementPageResult : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(ElementPageResult)

public:
	ElementPageResult( IElement* = NULL );
	virtual ~ElementPageResult();

// Dialog Data
	enum { IDD = IDD_PAGE_ELMT_RESULT };
	NSToolTipCtrl	m_tooltip;
	IElement*		m_pE;
	CBCGPButton		m_bnAdd;
	CBCGPButton		m_bnAdd2;
	CStringList		lst;

public:
	BOOL DoHelp( HELPINFO* );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	// hide when correctly reached target
	BOOL m_fHideRightTarget;
	// hide when incorrectly reached target
	BOOL m_fHideWrongTarget;
	// hide when any target is correctly reached
	BOOL m_fHideIfAnyRightTarget;
	// hide when any target is incorrectly reached
	BOOL m_fHideIfAnyWrongTarget;
	// display a stimulus when target correctly reached
	BOOL m_fRightTarget;
	CString	m_szRightTarget;
	// display a stimulus when target incorrectly reached
	BOOL m_fWrongTarget;
	CString	m_szWrongTarget;

	afx_msg void OnBnClickedChkTargetright();
	afx_msg void OnBnClickedChkTargetwrong();
	CComboBox m_cboRightTarget;
	CComboBox m_cboWrongTarget;
	virtual BOOL OnInitDialog();
	virtual BOOL OnApply();
	virtual BOOL OnSetActive();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnAdd2();
};

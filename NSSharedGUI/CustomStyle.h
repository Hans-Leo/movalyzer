// CustomStyle.h: interface for the CCustomStyle class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

class AFX_EXT_CLASS CCustomStyleSettings
{
public:
	CCustomStyleSettings();
	void Reset();

// customizable attributes (colors)
	COLORREF	m_appbg1, m_appbg2;
	COLORREF	m_barbg1, m_barbg2;
	COLORREF	m_barcaptionbg1, m_barcaptionbg2;
	COLORREF	m_gripper1, m_gripper2;
	COLORREF	m_taberase1, m_taberase2;
	COLORREF	m_splitter1, m_splitter2;
	COLORREF	m_menuhl1, m_menuhl2;
	COLORREF	m_menubartext;
};

class AFX_EXT_CLASS CCustomStyle : public CBCGPVisualManager2007  
{
	DECLARE_DYNCREATE(CCustomStyle)

public:
	CCustomStyle();
	virtual ~CCustomStyle();

	virtual void OnDrawRibbonCaption( CDC* pDC, CBCGPRibbonBar* pBar, CRect rect, CRect rectText );

	virtual void DrawNcCaption( CDC* pDC, CRect rectCaption, DWORD dwStyle, DWORD dwStyleEx,
								const CString& strTitle, const CString& strDocument,
								HICON hIcon, BOOL bPrefix, BOOL bActive, BOOL bTextCenter,
								const CObList& lstSysButtons );

	virtual void OnDrawBarGripper (CDC* pDC, CRect rectGripper, BOOL bHorz, 
									CBCGPBaseControlBar* pBar);
	virtual COLORREF OnFillMiniFrameCaption (CDC* pDC, CRect rectCaption, 
											CBCGPMiniFrameWnd* pFrameWnd, 
											BOOL bActive);
	virtual void OnFillBarBackground (CDC* pDC, CBCGPBaseControlBar* pBar,
									CRect rectClient, CRect rectClip,
									BOOL bNCArea = FALSE);
	virtual void OnHighlightMenuItem (CDC* pDC, CBCGPToolbarMenuButton* pButton,
									CRect rect, COLORREF& clrText);
	virtual void OnDrawSeparator (CDC* pDC, CBCGPBaseControlBar* pBar, CRect rect, BOOL bHorz);
	virtual void OnEraseTabsArea (CDC* pDC, CRect rect, const CBCGPBaseTabWnd* pTabWnd);

	virtual COLORREF OnDrawControlBarCaption (CDC* pDC, CBCGPDockingControlBar* pBar, 
		BOOL bActive, CRect rectCaption, CRect rectButtons);

	virtual void OnFillOutlookPageButton (CDC* pDC, const CRect& rect,
										BOOL bIsHighlighted, BOOL bIsPressed,
										COLORREF& clrText);
	virtual void OnDrawOutlookPageButtonBorder (CDC* pDC, CRect& rectBtn, BOOL bIsHighlighted, BOOL bIsPressed);
	virtual COLORREF GetToolbarButtonTextColor (CBCGPToolbarButton* pButton,
												CBCGPVisualManager2007::BCGBUTTON_STATE state);
	virtual void OnDrawSlider (CDC* pDC, CBCGPSlider* pSlider, CRect rect, BOOL bAutoHideMode);
	virtual void OnDrawSplitterBorder(CDC* pDC, CBCGPSplitterWnd* pSplitterWnd, CRect rect);
	virtual void OnFillSplitterBackground (CDC* pDC, CBCGPSplitterWnd* pSplitterWnd, CRect rect);

	virtual void OnDrawStatusBarPaneBorder (CDC* pDC, CBCGPStatusBar* pBar,
					CRect rectPane, UINT uiID, UINT nStyle);
	CSize	GetSystemBorders () const;

public:
	CBCGPControlRenderer	m_ctrlStatusBarPaneBorder;
	CBCGPToolBarImages	m_StatusBarSizeBox;
	static void SetColors( CCustomStyleSettings& );
	static void ResetColors();
};

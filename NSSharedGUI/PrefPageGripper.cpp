// PrefPageGripper.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PrefSheet.h"
#include "PrefPageGripper.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	MAX_CHANNELS	16

/////////////////////////////////////////////////////////////////////////////
// PrefPageGripper property page

IMPLEMENT_DYNCREATE(PrefPageGripper, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(PrefPageGripper, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_RDO_SBMODE, OnBnClickedRdoSbmode)
	ON_BN_CLICKED(IDC_RDO_DBMODE, OnBnClickedRdoDbmode)
	ON_CBN_SELCHANGE(IDC_CBO_LOAD, OnCbnSelchangeCboLoad)
	ON_CBN_SELCHANGE(IDC_CBO_LOWER, OnCbnSelchangeCboLower)
	ON_CBN_SELCHANGE(IDC_CBO_UPPER, OnCbnSelchangeCboUpper)
	ON_EN_KILLFOCUS(IDC_EDIT_CHANNELS, OnEnKillfocusEditChannels)
END_MESSAGE_MAP()

PrefPageGripper::PrefPageGripper( GripperSettings* pGS )
	: CBCGPPropertyPage(PrefPageGripper::IDD), m_pGS( pGS )
{
	m_nDevice = 1;
	m_nChannels = 3;
	m_lSamples = 1000L;
	m_lSampleRate = 100000L;
	m_lScanRate = 100L;
	m_dCalibrationConstantL = 1000.;
	m_dCalibrationConstantLG = 1000.;
	m_dCalibrationConstantUG = 1000.;
	m_dExcitationVoltageL = 5.0;
	m_dExcitationVoltageLG = 5.0;
	m_dExcitationVoltageUG = 5.0;
	m_dFullScaleLoadL = 25.0;
	m_dFullScaleLoadLG = 25.0;
	m_dFullScaleLoadUG = 25.0;
	m_dGainL = 1.0;
	m_dGainLG = 1.0;
	m_dGainUG = 1.0;
	m_nBaseline = 50;
	m_nChanLower = 2;
	m_nChanUpper = 0;
	m_nChanLoad = 1;
	m_lDBSamples = 20;
	m_fVolts = FALSE;
	m_fNewtons = TRUE;
	m_fDBMode = TRUE;
}

PrefPageGripper::~PrefPageGripper()
{
}

void PrefPageGripper::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_UPPER, m_cboUpper);
	DDX_Control(pDX, IDC_CBO_LOWER, m_cboLower);
	DDX_Control(pDX, IDC_CBO_LOAD, m_cboLoad);
	DDX_Text(pDX, IDC_EDIT_DEVICE, m_nDevice);
	DDV_MinMaxInt(pDX, m_nDevice, 1, 20);
	DDX_Text(pDX, IDC_EDIT_CHANNELS, m_nChannels);
	DDV_MinMaxInt(pDX, m_nChannels, 2, 8);
	DDX_Text(pDX, IDC_EDIT_SAMPLES, m_lSamples);
	DDV_MinMaxInt(pDX, m_lSamples, 1, 10000);
	DDX_Text(pDX, IDC_EDIT_SAMPLERATE, m_lSampleRate);
	DDV_MinMaxLong(pDX, m_lSampleRate, 1, 500000);
	DDX_Text(pDX, IDC_EDIT_SCANRATE, m_lScanRate);
	DDV_MinMaxLong(pDX, m_lScanRate, 1, 500000);
	DDX_Text(pDX, IDC_EDIT_CC_L, m_dCalibrationConstantL);
	DDX_Text(pDX, IDC_EDIT_CC_LG, m_dCalibrationConstantLG);
	DDX_Text(pDX, IDC_EDIT_CC_UG, m_dCalibrationConstantUG);
	DDX_Text(pDX, IDC_EDIT_EV_L, m_dExcitationVoltageL);
	DDX_Text(pDX, IDC_EDIT_EV_LG, m_dExcitationVoltageLG);
	DDX_Text(pDX, IDC_EDIT_EV_UG, m_dExcitationVoltageUG);
	DDX_Text(pDX, IDC_EDIT_FSL_L, m_dFullScaleLoadL);
	DDX_Text(pDX, IDC_EDIT_FSL_LG, m_dFullScaleLoadLG);
	DDX_Text(pDX, IDC_EDIT_FSL_UG, m_dFullScaleLoadUG);
	DDX_Text(pDX, IDC_EDIT_GAIN_L, m_dGainL);
	DDX_Text(pDX, IDC_EDIT_GAIN_LG, m_dGainLG);
	DDX_Text(pDX, IDC_EDIT_GAIN_UG, m_dGainUG);
	DDX_Text(pDX, IDC_EDIT_BASELINE, m_nBaseline);
	DDV_MinMaxInt(pDX, m_nBaseline, 10, 50);
	DDX_CBIndex(pDX, IDC_CBO_LOAD, m_nChanLoad);
	DDX_CBIndex(pDX, IDC_CBO_LOWER, m_nChanLower);
	DDX_CBIndex(pDX, IDC_CBO_UPPER, m_nChanUpper);
	DDX_Text(pDX, IDC_EDIT_DBSAMPLES, m_lDBSamples);
	DDV_MinMaxLong(pDX, m_lDBSamples, 20, 500);
}

/////////////////////////////////////////////////////////////////////////////
// PrefPageGripper message handlers

BOOL PrefPageGripper::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	if( m_pGS )
	{
		m_nDevice = m_pGS->nDevice;
		m_nChannels = m_pGS->nChannels;
		m_lSamples = m_pGS->lSamples;
		m_lSampleRate = m_pGS->lSampleRate;
		m_lScanRate = m_pGS->lScanRate;
		m_nBaseline = m_pGS->nBaseline;
		m_dCalibrationConstantLG = m_pGS->dCalibrationConstantLG;
		m_dCalibrationConstantUG = m_pGS->dCalibrationConstantUG;
		m_dCalibrationConstantL = m_pGS->dCalibrationConstantL;
		m_dExcitationVoltageLG = m_pGS->dExcitationVoltageLG;
		m_dExcitationVoltageUG = m_pGS->dExcitationVoltageUG;
		m_dExcitationVoltageL = m_pGS->dExcitationVoltageL;
		m_dFullScaleLoadLG = m_pGS->dFullScaleLoadLG;
		m_dFullScaleLoadUG = m_pGS->dFullScaleLoadUG;
		m_dFullScaleLoadL = m_pGS->dFullScaleLoadL;
		m_dGainLG = m_pGS->dGainLG;
		m_dGainUG = m_pGS->dGainUG;
		m_dGainL = m_pGS->dGainL;
		m_fVolts = m_pGS->fVolts;
		m_fNewtons = m_pGS->fNewtons;
		m_fDBMode = m_pGS->fDoubleBuffer;
		m_lDBSamples = m_pGS->lDBSamples;
	}

	if( m_fVolts ) CheckRadioButton( IDC_RDO_NEWTON, IDC_RDO_RAW, IDC_RDO_VOLT );
	else if( m_fNewtons ) CheckRadioButton( IDC_RDO_NEWTON, IDC_RDO_RAW, IDC_RDO_NEWTON );
	else CheckRadioButton( IDC_RDO_NEWTON, IDC_RDO_RAW, IDC_RDO_RAW );
	if( m_fDBMode )
	{
		CheckRadioButton( IDC_RDO_SBMODE, IDC_RDO_DBMODE, IDC_RDO_DBMODE );
	}
	else
	{
		CheckRadioButton( IDC_RDO_SBMODE, IDC_RDO_DBMODE, IDC_RDO_SBMODE );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DBSAMPLES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	UpdateData( FALSE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_USER );
	m_tooltip.SetTitle( _T("GRIPPER SETTINGS") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DEVICE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Sequence number starting at 1 of National Instruments DAQpads connected.") );
	pWnd = GetDlgItem( IDC_EDIT_CHANNELS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Number of data channels to scan (max=16).") );
	pWnd = GetDlgItem( IDC_EDIT_SAMPLES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Number of data per channel per trial (trial duration = #Samples/Scan Rate") );
	pWnd = GetDlgItem( IDC_EDIT_SAMPLERATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Frequency of sampling the individual channels (pts/s).") );
	pWnd = GetDlgItem( IDC_EDIT_SCANRATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Frequency of sampling the set of channels (pts/s).") );
	pWnd = GetDlgItem( IDC_EDIT_BASELINE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Number of initial samples of the first trial to calibrate the zero-level at the time of testing.") );
	pWnd = GetDlgItem( IDC_CBO_LOWER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select DAQPad physical channel for lower grip force.") );
	pWnd = GetDlgItem( IDC_CBO_UPPER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select DAQPad physical channel for upper grip force.") );
	pWnd = GetDlgItem( IDC_CBO_LOAD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select DAQPad physical channel for lift force.") );
	pWnd = GetDlgItem( IDC_EDIT_CC_L );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Calibration constant (1/N) for Lift.") );
	pWnd = GetDlgItem( IDC_EDIT_CC_LG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Calibration constant (1/N) for Lower Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_CC_UG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Calibration constant (1/N) for Upper Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_EV_L );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Excitation voltage for Lift.") );
	pWnd = GetDlgItem( IDC_EDIT_EV_LG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Excitation voltage for Lower Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_EV_UG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Excitation voltage for Upper Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_FSL_L );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Full scale load (in Lbs.) for Lift.") );
	pWnd = GetDlgItem( IDC_EDIT_FSL_LG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Full scale load (in Lbs.) for Lower Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_FSL_UG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Full scale load (in Lbs.) for Upper Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_GAIN_L );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Channel gain for Lift.") );
	pWnd = GetDlgItem( IDC_EDIT_GAIN_LG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Channel gain for Lower Grip.") );
	pWnd = GetDlgItem( IDC_EDIT_GAIN_UG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Channel gain for Upper Grip.") );
	pWnd = GetDlgItem( IDC_RDO_NEWTON );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Report gripper results in Newtons (units/calibration constant).") );
	pWnd = GetDlgItem( IDC_RDO_VOLT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Report gripper results in volts.") );
	pWnd = GetDlgItem( IDC_RDO_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Report gripper results in raw units.") );
	pWnd = GetDlgItem( IDC_RDO_DBMODE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Acquire and display every n samples (\"real-time\").") );
	pWnd = GetDlgItem( IDC_RDO_SBMODE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Acquire and display after all samples acquired from DAQPad.") );
	pWnd = GetDlgItem( IDC_EDIT_DBSAMPLES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Number of samples for continuous feedback for double-buffering.") );

	// Fill channel combos
	CString szItem;
	for( int i = 0; i < MAX_CHANNELS; i++ )
	{
		szItem.Format( _T("%d"), i );
		m_cboLower.AddString( szItem );
		m_cboUpper.AddString( szItem );
		m_cboLoad.AddString( szItem );
	}
	if( m_pGS )
	{
		m_cboLower.SetCurSel( m_pGS->nChanLower );
		m_cboUpper.SetCurSel( m_pGS->nChanUpper );
		m_cboLoad.SetCurSel( m_pGS->nChanLoad );
	}
	else
	{
		m_cboLower.SetCurSel( 0 );
		m_cboUpper.SetCurSel( 1 );
		m_cboLoad.SetCurSel( 2 );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL PrefPageGripper::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL PrefPageGripper::OnApply() 
{
	UpdateData( TRUE );

	if( ( m_nChanLower >= m_nChannels ) ||
		( m_nChanUpper >= m_nChannels ) ||
		( m_nChanLoad >= m_nChannels ) )
	{
		BCGPMessageBox( _T("There are not enough channels for some of your channel selections below.") );
		m_cboLower.SetCurSel( 0 );
		m_cboUpper.SetCurSel( 0 );
		m_cboLoad.SetCurSel( 0 );
		return FALSE;
	}

	int nRes = GetCheckedRadioButton( IDC_RDO_NEWTON, IDC_RDO_VOLT );
	m_fVolts = ( nRes == IDC_RDO_VOLT );
	m_fNewtons = ( nRes == IDC_RDO_NEWTON );
	nRes = GetCheckedRadioButton( IDC_RDO_SBMODE, IDC_RDO_DBMODE );
	m_fDBMode = ( nRes == IDC_RDO_DBMODE );

	if( m_pGS )
	{
		m_pGS->nDevice = m_nDevice;
		m_pGS->nChannels = m_nChannels;
		m_pGS->lSamples = m_lSamples;
		m_pGS->lSampleRate = m_lSampleRate;
		m_pGS->lScanRate = m_lScanRate;
		m_pGS->nBaseline = m_nBaseline;
		m_pGS->nChanLower = m_nChanLower;
		m_pGS->nChanUpper = m_nChanUpper;
		m_pGS->nChanLoad = m_nChanLoad;
		m_pGS->dCalibrationConstantLG = m_dCalibrationConstantLG;
		m_pGS->dCalibrationConstantUG = m_dCalibrationConstantUG;
		m_pGS->dCalibrationConstantL = m_dCalibrationConstantL;
		m_pGS->dExcitationVoltageLG = m_dExcitationVoltageLG;
		m_pGS->dExcitationVoltageUG = m_dExcitationVoltageUG;
		m_pGS->dExcitationVoltageL = m_dExcitationVoltageL;
		m_pGS->dFullScaleLoadLG = m_dFullScaleLoadLG;
		m_pGS->dFullScaleLoadUG = m_dFullScaleLoadUG;
		m_pGS->dFullScaleLoadL = m_dFullScaleLoadL;
		m_pGS->dGainLG = m_dGainLG;
		m_pGS->dGainUG = m_dGainUG;
		m_pGS->dGainL = m_dGainL;
		m_pGS->fVolts = m_fVolts;
		m_pGS->fNewtons = m_fNewtons;
		m_pGS->fDoubleBuffer = m_fDBMode;
		m_pGS->lDBSamples = m_lDBSamples;
	}

	PrefSheet* pSheet = (PrefSheet*)GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL PrefPageGripper::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL PrefPageGripper::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("selectdevices.html"), this );
	return TRUE;
}

void PrefPageGripper::OnBnClickedRdoSbmode()
{
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DBSAMPLES );
	if( pWnd ) pWnd->EnableWindow( FALSE );
}

void PrefPageGripper::OnBnClickedRdoDbmode()
{
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DBSAMPLES );
	if( pWnd ) pWnd->EnableWindow( TRUE );
}

void PrefPageGripper::OnCbnSelchangeCboLoad()
{
	UpdateData( TRUE );
	if( m_nChanLoad >= m_nChannels )
	{
		BCGPMessageBox( _T("There are not enough channels for this selection.") );
		m_cboLoad.SetCurSel( 0 );
		m_cboLoad.SetFocus();
	}
}

void PrefPageGripper::OnCbnSelchangeCboLower()
{
	UpdateData( TRUE );
	if( m_nChanLower >= m_nChannels )
	{
		BCGPMessageBox( _T("There are not enough channels for this selection.") );
		m_cboLower.SetCurSel( 0 );
		m_cboLower.SetFocus();
	}
}

void PrefPageGripper::OnCbnSelchangeCboUpper()
{
	UpdateData( TRUE );
	if( m_nChanUpper >= m_nChannels )
	{
		BCGPMessageBox( _T("There are not enough channels for this selection.") );
		m_cboUpper.SetCurSel( 0 );
		m_cboUpper.SetFocus();
	}
}

void PrefPageGripper::OnEnKillfocusEditChannels()
{
//	UpdateData( TRUE );
//	if( ( m_nChanLower >= m_nChannels ) ||
//		( m_nChanUpper >= m_nChannels ) ||
//		( m_nChanLoad >= m_nChannels ) )
//	{
//		BCGPMessageBox( _T("There are not enough channels for some of your channel selections below.") );
//		m_cboLower.SetCurSel( 0 );
//		m_cboUpper.SetCurSel( 0 );
//		m_cboLoad.SetCurSel( 0 );
//	}
}

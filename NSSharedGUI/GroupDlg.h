#if !defined(AFX_GROUPDLG_H__10ADC4A3_A81D_11D3_8A59_000000000000__INCLUDED_)
#define AFX_GROUPDLG_H__10ADC4A3_A81D_11D3_8A59_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// GroupDlg dialog

class AFX_EXT_CLASS GroupDlg : public CBCGPDialog
{
// Construction
public:
	GroupDlg(CWnd* pParent = NULL, BOOL fNew = FALSE);   // standard constructor

// Dialog Data
	enum { IDD = IDD_GROUP };
	CString			m_szExpID;
	CString			m_szAbbr;
	CString			m_szDesc;
	CString			m_szNotes;
	BOOL			m_fInit;
	BOOL			m_fNew;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEditId();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickNotes();
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_GROUPDLG_H__10ADC4A3_A81D_11D3_8A59_000000000000__INCLUDED_)

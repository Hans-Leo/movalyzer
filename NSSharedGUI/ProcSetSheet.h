#pragma once

// ProcSetSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ProcSetSheet

interface IProcSetting;

class AFX_EXT_CLASS ProcSetSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(ProcSetSheet)

// Construction
public:
	ProcSetSheet( UINT nIDCaption, CWnd* pParentWnd = NULL,
				  BOOL fGripper = FALSE, UINT iSelectPage = 0 );
	ProcSetSheet( LPCTSTR pszCaption, CWnd* pParentWnd = NULL,
				  BOOL fGripper = FALSE, UINT iSelectPage = 0 );
	ProcSetSheet( IProcSetting* pPS, CWnd* pParentWnd = NULL,
				  BOOL fGripper = FALSE, BOOL fNew = FALSE,
				  UINT iSelectPage = 0, BOOL fDup = FALSE );

// Attributes
public:
	CString		m_szID;
	CString		m_szDesc;
	CString		m_szNotes;
	short		m_nType;
	double		m_dMDV;
	BOOL		m_fNew;
	BOOL		m_fModified;
	BOOL		m_fGripper;
	BOOL		m_fDup;

// Operations
public:
	void AddPages( IProcSetting* pPS = NULL );

// Overrides

// Implementation
public:
	virtual ~ProcSetSheet();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
};

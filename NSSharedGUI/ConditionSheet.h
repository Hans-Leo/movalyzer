#pragma once

// ConditionSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ConditionSheet

interface INSCondition;

class AFX_EXT_CLASS ConditionSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(ConditionSheet)

// Construction
public:
	ConditionSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, BOOL fGripper = FALSE, UINT iSelectPage = 0);
	ConditionSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, BOOL fGripper = FALSE, UINT iSelectPage = 0);
	ConditionSheet( INSCondition* pC, CString szExpID, BOOL fNew = TRUE, BOOL fReps = FALSE,
					BOOL fDup = FALSE, CStringList* = NULL, CStringList* = NULL,
					CWnd* pParentWnd = NULL, BOOL fGripper = FALSE,
					UINT iSelectPage = 0, int nEvent = -1 );
	virtual ~ConditionSheet();

// Attributes
public:
	CString			m_szExpID;
	BOOL			m_fModified;
	BOOL			m_fGripper;
	BOOL			m_fNew;
	BOOL			m_fReps;
	BOOL			m_fDup;
	short			m_nReps;
	int				m_nSelEvent;
	CStringList*	m_pList;
	CStringList*	m_pListWord;

// Operations
public:
	void AddPages( INSCondition* pC = NULL );

// Overrides

// Implementation
public:
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
protected:
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

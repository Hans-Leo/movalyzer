// Feedbacks.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "OverwriteDlg.h"
#include "FeedbackDlg.h"
#include "Feedbacks.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	Feedbacks::GetDescriptionWithID( m_pFeedback, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::GetNext()
{
	return Feedbacks::GetNext( m_pFeedback );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Find( CString szID )
{
	return Feedbacks::Find( m_pFeedback, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Add()
{
	return Feedbacks::Add( m_pFeedback );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Modify()
{
	return Feedbacks::Modify( m_pFeedback );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Remove()
{
	return Feedbacks::Remove( m_pFeedback );
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::SetDataPath( CString szPath )
{
	Feedbacks::SetDataPath( m_pFeedback, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Feedbacks::GetID( IFeedback* pFeedback, CString& szVal )
{
	if( pFeedback )
	{
		BSTR bstrItem;
		pFeedback->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::GetDescription( IFeedback* pFeedback, CString& szVal )
{
	if( pFeedback )
	{
		BSTR bstrItem;
		pFeedback->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::GetDescriptionWithID( IFeedback* pFeedback, CString& szVal, BOOL fReverse )
{
	if( pFeedback )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		Feedbacks::GetID( pFeedback, szID );
		pFeedback->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::GetFeature( IFeedback* pFeedback, CString& szVal )
{
	if( pFeedback )
	{
		BSTR bstrItem;
		pFeedback->get_Feature( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::GetMinColor( IFeedback* pFeedback, DWORD& dwVal )
{
	if( pFeedback ) pFeedback->get_MinColor( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::GetMaxColor( IFeedback* pFeedback, DWORD& dwVal )
{
	if( pFeedback ) pFeedback->get_MaxColor( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::ResetToStart()
{
	return Feedbacks::ResetToStart( m_pFeedback );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::ResetToStart( IFeedback* pFeedback )
{
	if( pFeedback ) return pFeedback->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::GetNext( IFeedback* pFeedback )
{
	if( pFeedback ) return pFeedback->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Find( IFeedback* pFeedback, CString szID )
{
	if( pFeedback ) return pFeedback->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Add( IFeedback* pFeedback )
{
	if( !pFeedback ) return E_FAIL;

	if( FAILED( pFeedback->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Feedbacks::Add") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Modify( IFeedback* pFeedback )
{
	if( !pFeedback ) return E_FAIL;

	if( FAILED( pFeedback->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Feedbacks::Remove( IFeedback* pFeedback )
{
	if( !pFeedback ) return E_FAIL;

	if( FAILED( pFeedback->Remove() ) )
	{
		BCGPMessageBox( _T("Unable to delete element.") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::SetDataPath( IFeedback* pFeedback, CString szPath )
{
	if( pFeedback ) pFeedback->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoAdd( CWnd* pOwner )
{
	return Feedbacks::DoAdd( m_pFeedback, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoAdd( IFeedback* pFeedback, CWnd* pOwner )
{
	if( !pFeedback ) return FALSE;
	FeedbackDlg d( pFeedback, TRUE, pOwner );
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = Feedbacks::Add( pFeedback );
		if( SUCCEEDED( hr ) ) return TRUE;
		else {
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Feedbacks::DoAdd") );
		}
	}
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoEdit( CString szID, CWnd* pOwner )
{
	return Feedbacks::DoEdit( m_pFeedback, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoEdit( IFeedback* pFeedback, CString szID, CWnd* pOwner )
{
	if( !pFeedback ) return FALSE;
	if( FAILED( Feedbacks::Find( pFeedback, szID ) ) ) return FALSE;
	FeedbackDlg d( pFeedback, FALSE, pOwner );
	if( FAILED( Feedbacks::Find( pFeedback, szID ) ) ) return FALSE;
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = Feedbacks::Modify( pFeedback );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_UPD_ERR );
			return FALSE;
		}

		return TRUE;
	}
	return FALSE;
}

BOOL Feedbacks::Export( CMemFile* pFile, BOOL fFirst )
{
	if( !m_pFeedback || !pFile ) return FALSE;

	CString szItem;

	WRITE_STRING( pFile, TAG_FEEDBACK );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	Feedbacks::GetID( m_pFeedback, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Desc
	Feedbacks::GetDescription( m_pFeedback, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Feature
	Feedbacks::GetFeature( m_pFeedback, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Min Color
	DWORD dwVal = 0;
	Feedbacks::GetMinColor( m_pFeedback, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Max Color
	Feedbacks::GetMaxColor( m_pFeedback, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan )
{
//	if( !m_pFeedback || !pFile || !pFile->m_pStream ) return FALSE;
	if( !m_pFeedback || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	Feedbacks comp;
	BOOL fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Feedback %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );
	// Feature
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutFeature( szLine );
	comp.PutFeature( szLine );
	// Min Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	DWORD dwVal = (DWORD)atol( szLine );
	PutMinColor( dwVal );
	comp.PutMinColor( dwVal );
	// Max Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutMaxColor( dwVal );
	comp.PutMaxColor( dwVal );

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_FEEDBACK, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		CFileFind ff;
		if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
		if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			comp.GetFeature( szLine );
			PutFeature( szLine );
			comp.GetMinColor( dwVal );
			PutMinColor( dwVal );
			comp.GetMaxColor( dwVal );
			PutMaxColor( dwVal );
			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::ForceLocal()
{
	if( !m_pFeedback ) return;

	HRESULT hr = m_pFeedback->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::ForceRemote()
{
	if( !m_pFeedback ) return;

	HRESULT hr = m_pFeedback->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::ForceDefault()
{
	if( !m_pFeedback ) return;

	HRESULT hr = m_pFeedback->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		Feedbacks dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );
		
		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::SetTemp( BOOL fVal )
{
	if( m_pFeedback ) m_pFeedback->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Feedbacks::DumpRecord( CString szID, CString szFile )
{
	if( !m_pFeedback ) return FALSE;
	return SUCCEEDED( m_pFeedback->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Feedbacks::RemoveTemp()
{
	if( m_pFeedback ) m_pFeedback->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Feedbacks::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pFeedback ) m_pFeedback->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

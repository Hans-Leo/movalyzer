// PrefPageInput.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PrefSheet.h"
#include "PrefPageUser.h"
#include "PrefPageInput.h"
#include "..\Common\EnumSerial.h"
#include "..\Common\MessageManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrefPageInput property page

IMPLEMENT_DYNCREATE(PrefPageInput, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(PrefPageInput, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_CHECK_ONLYAVAIL, OnBnClickedCheckOnlyavail)
END_MESSAGE_MAP()

PrefPageInput::PrefPageInput( Preferences* pPref )
	: CBCGPPropertyPage(PrefPageInput::IDD), m_pPref( pPref ), m_fOnlyAvail( TRUE )
{
}

PrefPageInput::~PrefPageInput()
{
}

void PrefPageInput::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_COM, m_cboCom);
	DDX_CBString(pDX, IDC_CBO_COM, m_szCom);
	DDX_Check(pDX, IDC_CHECK_ONLYAVAIL, m_fOnlyAvail);
}

/////////////////////////////////////////////////////////////////////////////
// PrefPageInput message handlers

BOOL PrefPageInput::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_USER );
	m_tooltip.SetTitle( _T("COMM PORT TRIGGER SETTINGS") );
	CWnd* pWnd= GetDlgItem( IDC_CBO_COM );
	m_tooltip.AddTool( pWnd, _T("Select a communications port to be used with trigger pulses (if desired)."), _T("Communications Port") );
	pWnd= GetDlgItem( IDC_CHECK_ONLYAVAIL );
	m_tooltip.AddTool( pWnd, _T("Show communication ports that are currently available only."), _T("Available Ports") );

	// comm ports
	FillCommPorts();
	CString szPort;
	::GetCommPort( szPort );
	if( szPort == _T("") ) m_cboCom.SetCurSel( 0 );
	else m_cboCom.SelectString( -1, szPort );

	// if not current user, we do not allow changes
	PrefSheet* pSheet = (PrefSheet*)GetParent();
	PrefPageUser* p = (PrefPageUser*)pSheet->GetPage( 0 );
	CString szUser;
	::GetCurrentUser( szUser );
	if( p && ( !p->m_fNew && ( szUser != p->m_szID ) ) || p->m_fNew )
	{
		pWnd = GetDlgItem( IDC_CBO_COM );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHECK_ONLYAVAIL );
		pWnd->EnableWindow( FALSE );
	}

	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL PrefPageInput::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

void PrefPageInput::FillCommPorts()
{
	UpdateData( TRUE );

	// scan for com ports
	CArray<SSerInfo,SSerInfo&> asi;
	SSerInfo* pSI = NULL;
	// Populate the list of serial ports.
	EnumSerialPorts( asi, m_fOnlyAvail );
	m_cboCom.ResetContent();
	for( int ii = 0; ii < asi.GetSize(); ii++ )
	{
		pSI = &asi[ ii ];
		m_cboCom.AddString( pSI->strFriendlyName );
	}
}

BOOL PrefPageInput::OnApply() 
{
	UpdateData( TRUE );

	strncpy_s( m_pPref->pszComPort, _countof( m_pPref->pszComPort ), m_szCom, _TRUNCATE );

	PrefSheet* pSheet = (PrefSheet*)GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}


BOOL PrefPageInput::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL PrefPageInput::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("mouseorpen.html"), this );
	return TRUE;
}

void PrefPageInput::OnBnClickedCheckOnlyavail()
{
	FillCommPorts();
}

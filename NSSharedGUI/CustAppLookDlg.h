#pragma once

#include "CustomStyle.h"

class AFX_EXT_CLASS PreviewColorBar : public CBCGPStatic
{
public:
	PreviewColorBar() : CBCGPStatic(), m_col1( RGB(0,0,0) ), m_col2( RGB(0,0,0 ) ) {}

	COLORREF	m_col1;
	COLORREF	m_col2;

protected:
	virtual void DrawItem( LPDRAWITEMSTRUCT );
};

// CCustAppLookDlg dialog
class AFX_EXT_CLASS CCustAppLookDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(CCustAppLookDlg)

public:
	CCustAppLookDlg( CWnd* pParent = NULL);   // standard constructor
	virtual ~CCustAppLookDlg();

// Dialog Data
	enum { IDD = IDD_APP_LOOK_CUST };
	CCustomStyleSettings	m_CS;
	CBCGPColorButton		m_color11;
	CBCGPColorButton		m_color12;
	PreviewColorBar			m_preview1;
	CBCGPColorButton		m_color21;
	CBCGPColorButton		m_color22;
	PreviewColorBar			m_preview2;
	CBCGPColorButton		m_color31;
	CBCGPColorButton		m_color32;
	PreviewColorBar			m_preview3;
	CBCGPColorButton		m_color41;
	CBCGPColorButton		m_color42;
	PreviewColorBar			m_preview4;
	CBCGPColorButton		m_color51;
	CBCGPColorButton		m_color52;
	PreviewColorBar			m_preview5;
	CBCGPColorButton		m_color61;
	CBCGPColorButton		m_color62;
	PreviewColorBar			m_preview6;
	CBCGPColorButton		m_color71;
	CBCGPColorButton		m_colorApp1;
	CBCGPColorButton		m_colorApp2;
	PreviewColorBar			m_preview7;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	void FillColors();

	afx_msg void OnChangeColor();
	afx_msg void OnReset();

	DECLARE_MESSAGE_MAP()
};

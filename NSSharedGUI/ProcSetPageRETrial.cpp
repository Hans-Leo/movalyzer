// ProcSetPageRETrial.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageRETrial.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"
#include "Experiments.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRETrial property page

IMPLEMENT_DYNCREATE(ProcSetPageRETrial, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageRETrial, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_RDO_REALSIZE, OnClickedSize)
	ON_BN_CLICKED(IDC_RDO_MAXIMIZE, OnClickedSize)
	ON_BN_CLICKED(IDC_RDO_NOTHING, OnClickedSize)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageRETrial::ProcSetPageRETrial( IProcSetting* pS )
	: CBCGPPropertyPage(ProcSetPageRETrial::IDD), m_pPS( pS )
{
// 06Aug08: GMB: Changed from 10 to 15 s to make program less "nervous"
//	m_dStart = 10.;
	m_dStart = 15.;
	m_dTimeout = 10.;
// 06Aug08: GMB: Changed from 3 to 2 s to make program less "nervous"
//	m_dTicks = 3.;
	m_dTicks = 2.;
	m_dBetween = 0.;
	m_fProc = TRUE;
	m_fMaximize = TRUE;
	m_fFullScreen = FALSE;
	m_fAsis = FALSE;

	m_editStart.SetDefaultValue( m_dStart );
	m_editTimeout.SetDefaultValue( m_dTimeout );
	m_editTicks.SetDefaultValue( m_dTicks );
	m_editBetween.SetDefaultValue( m_dBetween );
	m_chkProc.SetDefaultAsOff( !m_fProc );
	m_chkFullScreen.SetDefaultAsOff( !m_fFullScreen );
	m_rdoRealSize.SetDefaultRadio( IDC_RDO_MAXIMIZE );
	m_rdoMax.SetDefaultRadio( IDC_RDO_MAXIMIZE );
	m_rdoAsis.SetDefaultRadio( IDC_RDO_MAXIMIZE );

	if( m_pPS )
	{
		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			pPSRE->get_TimeoutStart( &m_dStart );
			pPSRE->get_TimeoutRecord( &m_dTimeout );
			pPSRE->get_TimeoutPenlift( &m_dTicks );
			pPSRE->get_TimeoutLatency( &m_dBetween );
			pPSRE->get_ProcImmediately( &m_fProc );

			// although intended as a boolean (0 or 1), we now have 3 options
			// if MaxRecArea = 2, leave window as is = true
			// if " = 1, then we maximize
			// else we real-size
			BOOL fVal = FALSE;
			pPSRE->get_MaxRecArea( &fVal );
			if( fVal == WINDOW_ASIS ) m_fAsis = TRUE;
			else m_fMaximize = fVal;
			pPSRE->get_FullScreen( &fVal );
			m_fFullScreen = fVal;

			pPSRE->Release();
		}
	}
}

ProcSetPageRETrial::~ProcSetPageRETrial()
{
}

void ProcSetPageRETrial::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SECS2, m_dStart);
	DDX_Text(pDX, IDC_EDIT_SECS, m_dTimeout);
	DDX_Text(pDX, IDC_EDIT_TICKS, m_dTicks);
	DDX_Text(pDX, IDC_EDIT_BETWEEN, m_dBetween);
	DDX_Check(pDX, IDC_CHK_PROC_NOW, m_fProc);
	DDX_Check(pDX, IDC_CHK_FULLSCREEN, m_fFullScreen);

	DDX_Control(pDX, IDC_EDIT_SECS2, m_editStart);
	DDX_Control(pDX, IDC_EDIT_SECS, m_editTimeout);
	DDX_Control(pDX, IDC_EDIT_TICKS, m_editTicks);
	DDX_Control(pDX, IDC_EDIT_BETWEEN, m_editBetween);
	DDX_Control(pDX, IDC_CHK_PROC_NOW, m_chkProc);
	DDX_Control(pDX, IDC_CHK_FULLSCREEN, m_chkFullScreen);
	DDX_Control(pDX, IDC_RDO_REALSIZE, m_rdoRealSize);
	DDX_Control(pDX, IDC_RDO_MAXIMIZE, m_rdoMax);
	DDX_Control(pDX, IDC_RDO_NOTHING, m_rdoAsis);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRETrial message handlers

BOOL ProcSetPageRETrial::OnApply() 
{
	if( !m_pPS ) return FALSE;

	UpdateData( TRUE );

	int nSel = GetCheckedRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING );
	if( nSel == IDC_RDO_MAXIMIZE ) m_fMaximize = TRUE;
	else if( nSel == IDC_RDO_REALSIZE ) m_fMaximize = FALSE;
	else m_fMaximize = WINDOW_ASIS;

	if( ( m_dStart <= 0. ) || ( m_dStart > 600. ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 2 );
		BCGPMessageBox( _T("Start timeout must be between 0 and 600 seconds.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SECS2 );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dTimeout <= 0. ) || ( m_dTimeout > 1800 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 2 );
		BCGPMessageBox( _T("Recording timeout must be between 0 and 1800 seconds.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SECS );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dTicks < 0.01 ) || ( m_dTicks > 600. ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 2 );
		BCGPMessageBox( _T("Penlift timeout must be between 0.01 and 600 seconds.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_TICKS );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dBetween < 0. ) || ( m_dBetween > 600. ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 2 );
		BCGPMessageBox( _T("Trial-to-Trial timeout must be between 0 and 600 seconds.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_BETWEEN );
		pWnd->SetFocus();
		return FALSE;
	}

	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->put_TimeoutStart( m_dStart );
		pPSRE->put_TimeoutRecord( m_dTimeout );
		pPSRE->put_TimeoutPenlift( m_dTicks );
		pPSRE->put_TimeoutLatency( m_dBetween );
		pPSRE->put_ProcImmediately( m_fProc );
		pPSRE->put_MaxRecArea( m_fMaximize );
		pPSRE->put_FullScreen( m_fFullScreen );

		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageRETrial::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageRETrial::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_SECS2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_SECS2, _T("Start") );
	pWnd = GetDlgItem( IDC_EDIT_SECS );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_SECS, _T("Recording") );
	pWnd = GetDlgItem( IDC_EDIT_TICKS );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_TICKS, _T("Pen-lift") );
	pWnd = GetDlgItem( IDC_EDIT_BETWEEN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Number of seconds waited between each trial (without graphs)."), _T("Trial-to-Trial") );
	pWnd = GetDlgItem( IDC_CHK_PROC_NOW );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_PROCNOW, _T("Process Immediately") );
	pWnd = GetDlgItem( IDC_RDO_NOTHING );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Do not alter window sizes, locations - leave as is."), _T("As Is") );
	pWnd = GetDlgItem( IDC_RDO_REALSIZE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Make recording window real-size (according to input device settings - tablet dimensions)."), _T("Real Size") );
	pWnd = GetDlgItem( IDC_RDO_MAXIMIZE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Reduce other window sizes to maximize recording view."), _T("Maximize") );
	pWnd = GetDlgItem( IDC_CHK_FULLSCREEN );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Eliminate all other windows to fully maximize recording view."), _T("Full Screen") );
		pWnd->EnableWindow( m_fMaximize );
	}

	Preferences prefs;
	::GetPreferences( &prefs, NULL );
	BSTR bstr;
	m_pPS->get_ExperimentID( &bstr );
	CString szID = bstr;
	Experiments exp;
	if( SUCCEEDED( exp.Find( szID ) ) )
	{
		short nVal = 0;
		exp.GetType( nVal );
		// if gripper - we only allow maximize window and disable the timeouts
		if( nVal == EXP_TYPE_GRIPPER )
		{
			pWnd = GetDlgItem( IDC_RDO_REALSIZE );
			pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_RDO_NOTHING );
			pWnd->EnableWindow( FALSE );
			CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_MAXIMIZE );

// 			pWnd = GetDlgItem( IDC_EDIT_SECS );
// 			pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_EDIT_SECS2 );
			pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_EDIT_TICKS );
			pWnd->EnableWindow( FALSE );
		}
		else	// handwriting
		{
			// if mouse, we only allow maximize & do nothing
			// if tablet and mapped to desktop we only allow maximize & do nothing
			// if tablet and mapped to recording window we allow all (unless tablet
			//    is larger than the screen)

			// screen/tablet sizes
			double dXt = 0., dYt = 0., dXd = 0., dYd = 0.;
			::GetTabletDimensions( dXt, dYt );
			::GetDisplayDimensions( dXd, dYd );
			BOOL fSizesOK = FALSE;
			if( ( dXt <= dXd ) && ( dYt <= dYd ) ) fSizesOK = TRUE;

			if( ( ( prefs.ip == Preferences::itTablet ) && ( prefs.tm == Preferences::tmDesktop ) ) ||
				( prefs.ip == Preferences::itMouse ) || !fSizesOK )
			{
				pWnd = GetDlgItem( IDC_RDO_REALSIZE );
				pWnd->EnableWindow( FALSE );

				// just a check for older versions containing an improper value
				if( !m_fMaximize && !m_fAsis )
				{
					m_fMaximize = TRUE;
					m_fAsis = FALSE;
				}
			}
		}
	}

	if( m_fAsis ) CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_NOTHING );
	else if( m_fMaximize ) CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_MAXIMIZE );
	else CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_REALSIZE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ProcSetPageRETrial::OnClickedSize()
{
	UpdateData( TRUE );
	BOOL fEnableFullScreen = FALSE;
	int nSel = GetCheckedRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING );
	if( nSel == IDC_RDO_MAXIMIZE ) fEnableFullScreen = TRUE;
	CWnd* pWnd = GetDlgItem( IDC_CHK_FULLSCREEN );
	pWnd->EnableWindow( fEnableFullScreen );
	if( !fEnableFullScreen ) m_fFullScreen = FALSE;
	UpdateData( FALSE );
}

BOOL ProcSetPageRETrial::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageRETrial::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_runexperiment_trial.html"), this );
	return TRUE;
}

void ProcSetPageRETrial::OnBnReset() 
{
	m_dStart = 15.;
	m_dTimeout = 10.;
	m_dTicks = 2.;
	m_dBetween = 0.;
	m_fProc = TRUE;
	m_fMaximize = TRUE;
	m_fFullScreen = FALSE;
	m_fAsis = FALSE;

	UpdateData( FALSE );

	if( m_fAsis ) CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_NOTHING );
	else if( m_fMaximize ) CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_MAXIMIZE );
	else CheckRadioButton( IDC_RDO_REALSIZE, IDC_RDO_NOTHING, IDC_RDO_REALSIZE );

	CWnd* pWnd = GetDlgItem( IDC_CHK_FULLSCREEN );
	pWnd->EnableWindow( m_fMaximize );
}

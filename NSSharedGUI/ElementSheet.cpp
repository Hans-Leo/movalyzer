// ElementSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ElementSheet.h"
#include "ElementPageGeneral.h"
#include "ElementPagePattern.h"
#include "ElementPageSize.h"
#include "ElementPageView.h"
#include "ElementPageResult.h"
#include "ElementPageAnimation.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ElementSheet

IMPLEMENT_DYNAMIC(ElementSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(ElementSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ElementSheet::ElementSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CBCGPPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 150 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	AddPages();
}

ElementSheet::ElementSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CBCGPPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 150 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	AddPages();
}

ElementSheet::ElementSheet( IElement* pE, BOOL fNew, BOOL fDup, CWnd* pParentWnd )
	: CBCGPPropertySheet( _T("Stimulus Element"), pParentWnd, 0 ), m_fNew( fNew ), m_fDup( fDup )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 150 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	AddPages( pE );
}

ElementSheet::~ElementSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void ElementSheet::AddPages( IElement* pE )
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	CBCGPPropSheetCategory* pCat1 = AddTreeCategory( _T("Element"), 0, 1 );
	AddPageToTree( pCat1, new ElementPageGeneral( pE, m_fDup ), -1, 2 );
	AddPageToTree( pCat1, new ElementPagePattern( pE ), -1, 2 );
	AddPageToTree( pCat1, new ElementPageSize( pE ), -1, 2 );
	AddPageToTree( pCat1, new ElementPageView( pE ), -1, 2 );
	AddPageToTree( pCat1, new ElementPageResult( pE ), -1, 2 );
	AddPageToTree( pCat1, new ElementPageAnimation( pE ), -1, 2 );

	m_fModified = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// ElementSheet message handlers

BOOL ElementSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( ElementPageGeneral ) ) )
			return ((ElementPageGeneral*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ElementPagePattern ) ) )
			return ((ElementPagePattern*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ElementPageSize ) ) )
			return ((ElementPageSize*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ElementPageView ) ) )
			return ((ElementPageView*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ElementPageResult ) ) )
			return ((ElementPageResult*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ElementPageAnimation ) ) )
			return ((ElementPageAnimation*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

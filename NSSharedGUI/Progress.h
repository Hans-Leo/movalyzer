// Progress.h -- Progress Meter management

#pragma once

namespace Progress
{

typedef void ( CALLBACK* SHOWPROGRESSCB )		( BOOL fShow );
typedef void ( CALLBACK* SETSTATUSTEXTCB )		( CString szTxt );
typedef void ( CALLBACK* ENABLEPROGRESSCB )		( long nTotal );
typedef void ( CALLBACK* SETPROGRESSCB )		( long nCur, CString szTxt );
typedef void ( CALLBACK* DISABLEPROGRESSCB )	();

	// callback functions
	void AFX_EXT_API UseCallbacks( BOOL fUse );
	void AFX_EXT_API SetShowProgressCB( SHOWPROGRESSCB );
	void AFX_EXT_API SetSetStatusTextCB( SETSTATUSTEXTCB );
	void AFX_EXT_API SetEnableProgressCB( ENABLEPROGRESSCB );
	void AFX_EXT_API SetSetProgressCB( SETPROGRESSCB );
	void AFX_EXT_API SetDisableProgressCB( DISABLEPROGRESSCB );

	// status bar text
	void AFX_EXT_API SetStatusText( CString szTxt );
	// progress meter/status bar stuff
	void AFX_EXT_API EnableProgress( long nTotal );
	void AFX_EXT_API SetProgress( long nCur, CString szTxt );
	void AFX_EXT_API DisableProgress();
	void AFX_EXT_API GetProgress( long& nCur, long& nTotal );
	void AFX_EXT_API ShowProgress( BOOL fShow = TRUE );
};

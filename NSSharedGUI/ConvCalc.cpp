// ConvCalc.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ConvCalc.h"

#define	IS_IN_TO_CM		GetCheckedRadioButton( IDC_RDO_INCM, IDC_RDO_DEGRAD ) == IDC_RDO_INCM
#define	IS_DEG_TO_RAD	GetCheckedRadioButton( IDC_RDO_INCM, IDC_RDO_DEGRAD ) == IDC_RDO_DEGRAD

// ConvCalc dialog

IMPLEMENT_DYNAMIC(ConvCalc, CBCGPDialog)

BEGIN_MESSAGE_MAP(ConvCalc, CBCGPDialog)
	ON_BN_CLICKED(IDC_RDO_INCM, OnBnClickedRdoIncm)
	ON_BN_CLICKED(IDC_RDO_DEGRAD, OnBnClickedRdoDegrad)
	ON_EN_KILLFOCUS(IDC_EDIT_INDEG, OnEnKillfocusEditIndeg)
	ON_EN_CHANGE(IDC_EDIT_INDEG, OnEnChangeEditIndeg)
	ON_EN_CHANGE(IDC_EDIT_CMRAD, OnEnChangeEditCmrad)
	ON_EN_KILLFOCUS(IDC_EDIT_CMRAD, OnEnKillfocusEditCmrad)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ConvCalc::ConvCalc(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ConvCalc::IDD, pParent)
	, m_dInDeg(0)
	, m_dCmRad(0)
	, m_szInDeg(_T("Inches"))
	, m_szCmRad(_T("Centimeters"))
	, _pFnClose( NULL )
{
	if( Create( ConvCalc::IDD, pParent ) ) ShowWindow( SW_SHOW );
}

ConvCalc::~ConvCalc()
{
}

void ConvCalc::SetCloseCalcFunc( CLOSETHECALC pFn )
{
	_pFnClose = pFn;
}

void ConvCalc::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INDEG, m_dInDeg);
	DDX_Text(pDX, IDC_EDIT_CMRAD, m_dCmRad);
	DDX_Text(pDX, IDC_TXT_INDEG, m_szInDeg);
	DDX_Text(pDX, IDC_TXT_CMRAD, m_szCmRad);
}

// ConvCalc message handlers

BOOL ConvCalc::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// Dialog icon
	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_CALC ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	EnableVisualManagerStyle( TRUE, TRUE );

	CheckRadioButton( IDC_RDO_INCM, IDC_RDO_DEGRAD, IDC_RDO_INCM );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void ConvCalc::OnOK()
{
	OnCancel();
}

void ConvCalc::OnCancel()
{
	DestroyWindow();
}

void ConvCalc::PostNcDestroy()
{
	if( _pFnClose ) _pFnClose();
	else ::CalcClosed();
}

void ConvCalc::OnBnClickedRdoIncm()
{
	m_fChanged = FALSE;
	m_dInDeg = 0.;
	m_dCmRad = 0.;
	m_szInDeg = _T("Inches");
	m_szCmRad = _T("Centimeters");

	UpdateData( FALSE );
}

void ConvCalc::OnBnClickedRdoDegrad()
{
	m_fChanged = FALSE;
	m_dInDeg = 0.;
	m_dCmRad = 0.;
	m_szInDeg = _T("Degrees");
	m_szCmRad = _T("Radians");

	UpdateData( FALSE );
}

void ConvCalc::OnEnChangeEditIndeg()
{
	m_fChanged = TRUE;
	CWnd* pWnd = GetDlgItem( IDC_EDIT_INDEG );
	CString szVal;
	pWnd->GetWindowText( szVal );
	if( szVal == _T("") ) return;
	if( szVal[ szVal.GetLength() - 1 ] == '.' ) return;
	OnEnKillfocusEditIndeg();
}

void ConvCalc::OnEnChangeEditCmrad()
{
	m_fChanged = TRUE;
	CWnd* pWnd = GetDlgItem( IDC_EDIT_CMRAD );
	CString szVal;
	pWnd->GetWindowText( szVal );
	if( szVal == _T("") ) return;
	if( szVal[ szVal.GetLength() - 1 ] == '.' ) return;
	OnEnKillfocusEditCmrad();
}

void ConvCalc::OnEnKillfocusEditIndeg()
{
	// do something only if changed
	if( !m_fChanged ) return;

	UpdateData( TRUE );

	if( IS_IN_TO_CM )
	{
		// 1 in = 2.54 cm
		m_dCmRad = m_dInDeg * 2.54;
	}
	else if( IS_DEG_TO_RAD )
	{
		// 1 deg = ( pi * rad ) / 180
		// 1 rad = 180 deg / pi
		m_dCmRad = m_dInDeg * PI / 180.;
	}

	UpdateData( FALSE );

	// reset change flag
	m_fChanged = FALSE;
}

void ConvCalc::OnEnKillfocusEditCmrad()
{
	// do something only if changed
	if( !m_fChanged ) return;

	UpdateData( TRUE );

	if( IS_IN_TO_CM )
	{
		m_dInDeg = m_dCmRad / 2.54;
	}
	else if( IS_DEG_TO_RAD )
	{
		// 1 deg = ( pi * rad ) / 180
		// 1 rad = 180 deg / pi
		m_dInDeg = m_dCmRad * 180. / PI;
	}

	UpdateData( FALSE );

	// reset change flag
	m_fChanged = FALSE;
}

BOOL ConvCalc::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("calculator.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

// ProcSetPageProp.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageProp.h"
#include "ProcSetSheet.h"
#include "InstructionDlg.h"
#include "NotesDlg.h"
#include "..\DataMod\DataMod.h"
#include "..\CTreeLink\DBCommon.h"
#include "Experiments.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageProp property page

IMPLEMENT_DYNCREATE(ProcSetPageProp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageProp, CBCGPPropertyPage)
	ON_EN_KILLFOCUS(IDC_EDIT_ABBR, OnKillfocusEditId)
	ON_CBN_SELCHANGE(IDC_CBO_TYPE, OnCbnSelchangeCboType)
	ON_BN_CLICKED(IDC_BN_INSTR, OnClickInstr)
	ON_BN_CLICKED(IDC_BN_NOTES, OnClickNotes)
	ON_BN_CLICKED(IDC_BN_ATTACHMENTS, OnClickAttachments)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ProcSetPageProp::ProcSetPageProp( BOOL fNew, BOOL fDup )
	: CBCGPPropertyPage(ProcSetPageProp::IDD), m_fNew( fNew ), m_fDup( fDup )
{
	m_szAbbr = _T("");
	m_szDesc = _T("");
	m_szNotes = _T("");
	m_nType = 0;
	m_dMDV = -1.e6;
}

ProcSetPageProp::~ProcSetPageProp()
{
}

void ProcSetPageProp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_TYPE, m_cboType);
	DDX_Text(pDX, IDC_EDIT_ABBR, m_szAbbr);
	DDV_MaxChars(pDX, m_szAbbr, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_Text(pDX, IDC_EDIT_NOTES, m_szNotes);
	DDV_MaxChars(pDX, m_szNotes, szNOTES);
	DDX_CBIndex(pDX, IDC_CBO_TYPE, m_nType);
	DDX_Text(pDX, IDC_EDIT_MDV, m_dMDV);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageProp message handlers

BOOL ProcSetPageProp::OnApply() 
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( !pSheet ) return FALSE;

	UpdateData( TRUE );

	if( m_szAbbr == _T("") )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 0 );
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}		
	if( m_szAbbr.GetLength() < szIDS )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 0 );
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szAbbr == _T("CON") )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 0 );
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szAbbr == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szDesc == _T("") )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 0 );
		BCGPMessageBox( _T("The experiment description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}		
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szAbbr ) ) return FALSE;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 0 );
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	pSheet->m_szID = m_szAbbr;
	pSheet->m_szDesc = m_szDesc;
	pSheet->m_szNotes = m_szNotes;
	pSheet->m_nType = m_nType;
	pSheet->m_dMDV = m_dMDV;
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageProp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageProp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_EXP_ID, _T("ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_EXP_DESC, _T("Description") );
	pWnd = GetDlgItem( IDC_EDIT_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Any additional information you would like.", _T("Notes") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_CBO_TYPE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select the type of this experiment.", _T("Type") );
	pWnd = GetDlgItem( IDC_BN_INSTR );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Set the instructions for running an experiment.", _T("Instructions") );
	pWnd = GetDlgItem( IDC_BN_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Add some extended notes to the experiment.", _T("Extended Notes") );
	pWnd = GetDlgItem( IDC_EDIT_MDV );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Specify the numeric value to be assigned to missing data values [Default=-1e6].", _T("Missing Data Value") );
	pWnd = GetDlgItem( IDC_BN_ATTACHMENTS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the attachments folder.\nFrom there you can add, delete and open files manually.\nThese files will be included in exports when specified."), _T("Attachments") );

	if( !m_fNew && !m_fDup )
	{
		pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	else
	{
		pWnd = GetDlgItem( IDC_BN_ATTACHMENTS );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// List of existing experiments
	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	Experiments exp;
	exp.SetDataPath( szPath );
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		exp.GetID( szItem );
		m_lstItems.AddTail( szItem );
		hr = exp.GetNext();
	}

	// data
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet )
	{
		m_szAbbr = pSheet->m_szID;
		m_szDesc = pSheet->m_szDesc;
		m_szNotes = pSheet->m_szNotes;
		m_nType = pSheet->m_nType;
		m_dMDV = pSheet->m_dMDV;
		UpdateData( FALSE );
	}

	// Fill experiment type list
	m_cboType.AddString( _T("Handwriting") );	// EXP_TYPE_HANDWRITING = 0
	m_cboType.AddString( _T("Grip-Force") );	// EXP_TYPE_GRIPPER = 1
	m_cboType.AddString( _T("Handwriting Image") );			// EXP_TYPE_IMAGE = 2
	m_cboType.SetCurSel( m_nType );

	// attachments indicator
	pWnd = GetDlgItem( IDC_BN_ATTACHMENTS );
	if( pWnd )
	{
		CString szRoot, szFiles, szTxt, szTitle;
		::GetDataPathRoot( szRoot );
		if( szRoot.GetAt( szRoot.GetLength() - 1 ) != '\\' ) szRoot += _T("\\");
		szRoot += m_szAbbr;
		szRoot += _T("\\attachments");
		szFiles.Format( _T("%s\\*.*"), szRoot );
		CFileFind ff;
		int nCount = 0;
		if( ff.FindFile( szRoot ) )
		{
			BOOL fCont = ff.FindFile( szFiles );
			while( fCont )
			{
				fCont = ff.FindNextFile();
				if( !ff.IsDirectory() ) nCount++;
			}
		}
		pWnd->GetWindowText( szTxt );
		szTitle.Format( _T("%s (%d)"), szTxt, nCount );
		pWnd->SetWindowText( szTitle );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageProp::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageProp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_properties.html"), this );
	return TRUE;
}

void ProcSetPageProp::OnKillfocusEditId() 
{
	UpdateData( TRUE );

	if( m_szAbbr == _T("") ) return;

	if( m_lstItems.Find( m_szAbbr ) )
	{
		BCGPMessageBox( _T("This ID already exists.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szAbbr ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
}

void ProcSetPageProp::OnCbnSelchangeCboType()
{
	UpdateData( TRUE );
	if( m_nType == EXP_TYPE_GRIPPER )
	{
		if( !::IsGA() )
		{
			NSSecurity* pSecurity = ::GetSecurity();
			if( pSecurity && !pSecurity->AG() )
			{
				m_nType = EXP_TYPE_HANDWRITING;
				UpdateData( FALSE );
			}
		}
	}
	else if( m_nType == EXP_TYPE_HANDWRITING )
	{
		if( !::IsSA() && !::IsOA() )
		{
			NSSecurity* pSecurity = ::GetSecurity();
			if( pSecurity && !pSecurity->AO() )
			{
				m_nType = EXP_TYPE_GRIPPER;
				UpdateData( FALSE );
			}
		}
	}
}

void ProcSetPageProp::OnClickInstr()
{
	// verify ID first
	UpdateData( TRUE );
	if( m_szAbbr == _T("") )
	{
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szAbbr.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CString szData = _T("SRC: Experiment - Instructions");
	LogToFile( szData, LOG_UI );

	// Check for directory structure
	if( !VerifyDirectory( m_szAbbr ) )
	{
		BCGPMessageBox( _T("Unable to create directory for files.") );
		return;
	}

	InstructionDlg d( this );
	d.m_szExpID = m_szAbbr;
	d.m_szFileName = m_szAbbr;
	CWaitCursor crs;
	d.DoModal();
}

void ProcSetPageProp::OnClickNotes()
{
	// verify ID first
	UpdateData( TRUE );
	if( m_szAbbr == _T("") )
	{
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szAbbr.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CString szData = _T("SRC: Experiment - Notes");
	LogToFile( szData, LOG_UI );

	// Check for directory structure
	if( !VerifyDirectory( m_szAbbr ) )
	{
		BCGPMessageBox( _T("Unable to create directory for files.") );
		return;
	}

	CString szPath, szFile;
	::GetDataPathRoot( szPath );
	if( szPath == _T("") ) szFile.Format( _T("%s\\%s.txt"), m_szAbbr, m_szAbbr );
	else szFile.Format( _T("%s\\%s\\%s.txt"), szPath, m_szAbbr, m_szAbbr );

	NotesDlg d( N_EXPERIMENT, m_szAbbr, szFile, this );
	d.DoModal();
}

void ProcSetPageProp::OnClickAttachments()
{
	CString szRoot, szTemp;
	::GetDataPathRoot( szRoot );
	if( szRoot.GetAt( szRoot.GetLength() - 1 ) != '\\' ) szRoot += _T("\\");
	szRoot += m_szAbbr;
	szRoot += _T("\\attachments");
	CFileFind ff;
	if( !ff.FindFile( szRoot ) ) _mkdir( szRoot );
	szTemp.Format( _T("explorer /ROOT,%s"), szRoot );
	WinExec( szTemp, SW_SHOW );
}

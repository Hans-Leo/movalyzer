// WizardExportSummary.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardExportSummary.h"
#include "WizardExportExp.h"
#include "WizardExportSettings.h"
#include "WizardExportSheet.h"

// WizardExportSummary dialog

IMPLEMENT_DYNAMIC(WizardExportSummary, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardExportSummary, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_CHK_FINISH, &WizardExportSummary::OnBnClickedChkFinish)
END_MESSAGE_MAP()

WizardExportSummary::WizardExportSummary()
	: CBCGPPropertyPage(WizardExportSummary::IDD), m_szSummary( _T("") )
{
}

WizardExportSummary::~WizardExportSummary()
{
}

void WizardExportSummary::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_RESULTS, m_summary);
	DDX_Text(pDX, IDC_EDIT_RESULTS, m_szSummary);
}

// WizardExportSummary message handlers

BOOL WizardExportSummary::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	CBCGPEdit* pEdit = (CBCGPEdit*)GetDlgItem( IDC_EDIT_RESULTS );
	if( pEdit ) pEdit->SetReadOnly( FALSE );
	OnSetActive();

	return TRUE;
}


LRESULT WizardExportSummary::OnWizardNext() 
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardExportSummary::OnSetActive() 
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	WizardExportSettings* pSettings = (WizardExportSettings*)pParent->GetPage( 1 );
	CString szTemp = pSettings->m_szSummary;

	int nPos = -1;
	nPos = szTemp.Find( _T("\n") );
	m_szSummary = _T("");
	while( nPos != -1 )
	{
		m_szSummary += szTemp.Left( nPos );
		m_szSummary += _T("\r\n");
		szTemp = szTemp.Mid( nPos + 1 );
		nPos = szTemp.Find( _T("\n") );
	}

	UpdateData( FALSE );

	CBCGPEdit* pEdit = (CBCGPEdit*)GetDlgItem( IDC_EDIT_RESULTS );
	if( pEdit ) pEdit->SetSel( -1, 0 );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardExportSummary::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("export_wizard.html"), this );
	return TRUE;
}

BOOL WizardExportSummary::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardExportSummary::OnBnClickedChkFinish()
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	WizardExportExp* pExp = (WizardExportExp*)pParent->GetPage( 0 );
	pExp->OnWizardFinish();
	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );
	CWnd* pWnd = GetDlgItem( IDC_CHK_FINISH );
	if( pWnd ) pWnd->ShowWindow( SW_HIDE );
}

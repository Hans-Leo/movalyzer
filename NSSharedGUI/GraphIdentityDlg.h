#pragma once

// GraphIdentityDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GraphIdentityDlg dialog

#include "NSTooltipCtrl.h"

class AFX_EXT_CLASS GraphIdentityDlg : public CBCGPDialog
{
// Construction
public:
	GraphIdentityDlg(CStringList* pGrps, CStringList* pSubjs,
					 CStringList* pConds, CObList* pQuests,
					 CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_GRAPHASSOC };
	CBCGPListCtrl	m_List;
	CBCGPComboBox	m_cbo;
	int				m_nCount;
	CStringList*	m_pGroups;
	CStringList*	m_pSubjects;
	CStringList*	m_pConditions;
	CObList			m_Quests;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillList();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelItem();
	DECLARE_MESSAGE_MAP()
};

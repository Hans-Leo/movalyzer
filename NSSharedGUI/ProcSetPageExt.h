#pragma once

// ProcSetPageExt.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageExt dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageExt : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageExt)

// Construction
public:
	ProcSetPageExt( IProcSetting* pPS = NULL );
	~ProcSetPageExt();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_EXT };
	BOOL			m_f3;
	BOOL			m_fO;
	CColoredButton	m_chkO;
	BOOL			m_f2;
	CColoredButton	m_chk2;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_linkAdv;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

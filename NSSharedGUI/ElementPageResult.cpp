// ElementPageResult.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ElementPageResult.h"
#include "ElementSheet.h"
#include "ElementPageGeneral.h"
#include "Elements.h"

#define	STR_NONE		_T("<NONE>")

// ElementPageResult dialog

IMPLEMENT_DYNAMIC(ElementPageResult, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ElementPageResult, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_TARGETRIGHT, OnBnClickedChkTargetright)
	ON_BN_CLICKED(IDC_CHK_TARGETWRONG, OnBnClickedChkTargetwrong)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_ADD2, OnClickBnAdd2)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ElementPageResult::ElementPageResult( IElement* pE )
	: CBCGPPropertyPage(ElementPageResult::IDD), m_fHideRightTarget(FALSE),
	  m_fHideWrongTarget(FALSE), m_fHideIfAnyRightTarget(FALSE),
	  m_fHideIfAnyWrongTarget(FALSE), m_fRightTarget(FALSE), m_fWrongTarget(FALSE), m_pE( pE )
{
	if( m_pE )
	{
		BSTR bstr = NULL;

		m_pE->get_HideRightTarget( &m_fHideRightTarget );
		m_pE->get_HideWrongTarget( &m_fHideWrongTarget );
		m_pE->get_HideIfAnyRightTarget( &m_fHideIfAnyRightTarget );
		m_pE->get_HideIfAnyWrongTarget( &m_fHideIfAnyWrongTarget );
		m_pE->get_RightTarget( &bstr );
		m_szRightTarget = bstr;
		m_pE->get_WrongTarget( &bstr );
		m_szWrongTarget = bstr;
	}

	m_fRightTarget = ( m_szRightTarget != _T("") );
	m_fWrongTarget = ( m_szWrongTarget != _T("") );

	// elements
	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	Elements elem;
	elem.SetDataPath( szPath );
	HRESULT hr = elem.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		elem.GetDescriptionWithID( szItem );
		lst.AddTail( szItem );
		hr = elem.GetNext();
	}

}

ElementPageResult::~ElementPageResult()
{
}

void ElementPageResult::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_HIDERIGHT, m_fHideRightTarget);
	DDX_Check(pDX, IDC_CHK_HIDEWRONG, m_fHideWrongTarget);
	DDX_Check(pDX, IDC_CHK_HIDERIGHTANY, m_fHideIfAnyRightTarget);
	DDX_Check(pDX, IDC_CHK_HIDEWRONGANY, m_fHideIfAnyWrongTarget);
	DDX_Check(pDX, IDC_CHK_TARGETRIGHT, m_fRightTarget);
	DDX_Check(pDX, IDC_CHK_TARGETWRONG, m_fWrongTarget);
	DDX_Control(pDX, IDC_CBO_TARGETRIGHT, m_cboRightTarget);
	DDX_Control(pDX, IDC_CBO_TARGEWRONG, m_cboWrongTarget);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_ADD2, m_bnAdd2);
}

// ElementPageResult message handlers

void ElementPageResult::OnBnClickedChkTargetright()
{
	UpdateData( TRUE );
	OnSetActive();
}

void ElementPageResult::OnBnClickedChkTargetwrong()
{
	UpdateData( TRUE );
	OnSetActive();
}

BOOL ElementPageResult::OnInitDialog()
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	// button images
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnAdd2.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_ELEM );
	m_tooltip.SetTitle( _T("ELEMENT") );
	CWnd* pWnd = GetDlgItem( IDC_CHK_HIDERIGHT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Hide this element if it is correctly reached as a target."), _T("Hide on Sucess") );
	pWnd = GetDlgItem( IDC_CHK_HIDEWRONG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Hide this element if it is incorrectly reached as a target."), _T("Hide on Failure") );
	pWnd = GetDlgItem( IDC_CHK_HIDERIGHTANY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Hide this element if any target is correctly reached."), _T("Hide On Any Success") );
	pWnd = GetDlgItem( IDC_CHK_HIDEWRONGANY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Hide this element if any target is incorrectly reached."), _T("Hide on Any Failure") );
	pWnd = GetDlgItem( IDC_CHK_TARGETRIGHT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display the selected element if this element is correctly reached as a target."), _T("Show Other on Success") );
	pWnd = GetDlgItem( IDC_CBO_TARGETRIGHT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an element to display if this element is correctly reached as a target."), _T("Shown on Sucess") );
	pWnd = GetDlgItem( IDC_CHK_TARGETWRONG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display the selected element if this element is incorrectly reached as a target."), _T("Show Other on Failure") );
	pWnd = GetDlgItem( IDC_CBO_TARGEWRONG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an element to display if this element is incorrectly reached as a target."), _T("Shown on Failure") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// ELEMENTS
	m_cboRightTarget.AddString( STR_NONE );
	m_cboWrongTarget.AddString( STR_NONE );
	m_cboRightTarget.SetCurSel( 0 );	// None
	m_cboWrongTarget.SetCurSel( 0 );	// None

	CString szID, szDelim, szItem;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos, nItem;
	POSITION pos = lst.GetHeadPosition();
	while( pos )
	{
		szItem = lst.GetNext( pos );

		nItem = m_cboRightTarget.AddString( szItem );
		nPos = szItem.Find( szDelim );
		szID = szItem.Mid( nPos + 2 );
		if( szID == m_szRightTarget ) m_cboRightTarget.SetCurSel( nItem );

		nItem = m_cboWrongTarget.AddString( szItem );
		nPos = szItem.Find( szDelim );
		szID = szItem.Mid( nPos + 2 );
		if( szID == m_szWrongTarget ) m_cboWrongTarget.SetCurSel( nItem );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ElementPageResult::OnApply()
{
	if( !m_pE ) return FALSE;

	UpdateData( TRUE );

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_pE->put_HideRightTarget( m_fHideRightTarget );
	m_pE->put_HideWrongTarget( m_fHideWrongTarget );
	m_pE->put_HideIfAnyRightTarget( m_fHideIfAnyRightTarget );
	m_pE->put_HideIfAnyWrongTarget( m_fHideIfAnyWrongTarget );

	if( m_fRightTarget )
	{
		// stimuli
		CString szStim;
		int nIdx = m_cboRightTarget.GetCurSel(), nPos;
		if( nIdx != CB_ERR )
		{
			m_cboRightTarget.GetLBText( nIdx, szStim );
			if( szStim != STR_NONE )
			{
				nPos = szStim.Find( szDelim );
				m_szRightTarget = szStim.Mid( nPos + 2 );
			}
			else m_szRightTarget = _T("");

			m_pE->put_RightTarget( m_szRightTarget.AllocSysString() );
		}
	}
	else
	{
		CString szEmpty;
		m_pE->put_RightTarget( szEmpty.AllocSysString() );
	}

	if( m_fWrongTarget )
	{
		// stimuli
		CString szStim;
		int nIdx = m_cboWrongTarget.GetCurSel(), nPos;
		if( nIdx != CB_ERR )
		{
			m_cboWrongTarget.GetLBText( nIdx, szStim );
			if( szStim != STR_NONE )
			{
				nPos = szStim.Find( szDelim );
				m_szWrongTarget = szStim.Mid( nPos + 2 );
			}
			else m_szWrongTarget = _T("");

			m_pE->put_WrongTarget( m_szWrongTarget.AllocSysString() );
		}
	}
	else
	{
		CString szEmpty;
		m_pE->put_WrongTarget( szEmpty.AllocSysString() );
	}

	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

void ElementPageResult::OnClickBnAdd()
{
	// Existing
	CStringList exist;
	CString szItem, szDelim, szID;
	int nPos = 0;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	int nCount = m_cboRightTarget.GetCount();
	for( int i = 0; i < nCount; i++ )
	{
		m_cboRightTarget.GetLBText( i, szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szItem = szItem.Left( nPos - 1 );
			exist.AddTail( szItem );
		}
	}

	// Add
	Elements elem;
	if( !elem.IsValid() ) return;

	if( elem.DoAdd( this ) )
	{
		CString szID, szDesc;
		elem.GetID( szID );
		elem.GetDescription( szDesc );
		szItem.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		nPos = m_cboRightTarget.AddString( szItem );
		m_cboRightTarget.SetCurSel( nPos );
		m_cboWrongTarget.AddString( szItem );
	}
}

void ElementPageResult::OnClickBnAdd2()
{
	// Existing
	CStringList exist;
	CString szItem, szDelim, szID;
	int nPos = 0;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	int nCount = m_cboRightTarget.GetCount();
	for( int i = 0; i < nCount; i++ )
	{
		m_cboRightTarget.GetLBText( i, szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szItem = szItem.Left( nPos - 1 );
			exist.AddTail( szItem );
		}
	}

	// Add
	Elements elem;
	if( !elem.IsValid() ) return;

	if( elem.DoAdd( this ) )
	{
		CString szID, szDesc;
		elem.GetID( szID );
		elem.GetDescription( szDesc );
		szItem.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		nPos = m_cboWrongTarget.AddString( szItem );
		m_cboWrongTarget.SetCurSel( nPos );
		m_cboRightTarget.AddString( szItem );
	}
}

BOOL ElementPageResult::OnSetActive()
{
	CWnd* pWnd = NULL;
	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = pSheet ? (ElementPageGeneral*)pSheet->GetPage( 0 ) : NULL;

	pWnd = GetDlgItem( IDC_CHK_HIDERIGHT );
	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() );
	pWnd = GetDlgItem( IDC_CHK_HIDEWRONG );
	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() );
//	pWnd = GetDlgItem( IDC_CHK_HIDERIGHTANY );
//	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() );
//	pWnd = GetDlgItem( IDC_CHK_HIDEWRONGANY );
//	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() );
//	pWnd = GetDlgItem( IDC_CHK_TARGETRIGHT );
//	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() );
//	pWnd = GetDlgItem( IDC_CHK_TARGETWRONG );
//	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() );

	pWnd = GetDlgItem( IDC_CBO_TARGETRIGHT );
	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() && m_fRightTarget );
	pWnd = GetDlgItem( IDC_CBO_TARGEWRONG );
	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() && m_fWrongTarget );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() && m_fRightTarget );
	pWnd = GetDlgItem( IDC_BN_ADD2 );
	if( pWnd ) pWnd->EnableWindow( pPage->IsTarget() && m_fWrongTarget );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL ElementPageResult::PreTranslateMessage(MSG* pMsg)
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPPropertyPage::PreTranslateMessage(pMsg);
}

BOOL ElementPageResult::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ElementPageResult::OnHelpInfo(HELPINFO* pHelpInfo)
{
	::GoToHelp( _T("element_results.html"), this );
	return TRUE;
}

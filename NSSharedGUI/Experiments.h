#pragma  once

#include "Objects.h"

#define	TAG_EXPERIMENT	_T("[EXPERIMENT]")

// Experiment type
#define	EXP_TYPE_HANDWRITING	0
#define	EXP_TYPE_GRIPPER		1
#define	EXP_TYPE_IMAGE			2

// Randomization Rules
#define RAND_RULE_NONE			0x0000		// NO RULES
#define RAND_RULE_1				0x0001		// Rule #1: Repeating
#define RAND_RULE_2				0x0002		// Rule #2: Blocks
#define RAND_RULE_3				0x0004		// Rule #3: Selection

interface IExperiment;
interface IProcess;
interface IProcSetting;

// This class is intended to encapsulate all view & functionality for an experiment
class AFX_EXT_CLASS Experiments : public Objects
{
	PUT_BASE( Experiment )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();

	// Interface methods
	PUT_GET( Experiment, ID, CString, val.AllocSysString() )

	PUT_GET( Experiment, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( IExperiment*, CString&, BOOL fReverse = FALSE );

	PUT_GET( Experiment, Notes, CString, val.AllocSysString() )

	PUT_GET( Experiment, Type, short, val );

	PUT_GET( Experiment, MissingDataValue, double, val );

	BOOL DoEdit( CString szID, CWnd* pOwner = NULL, UINT iSelectPage = 0 );
	static BOOL DoEdit( IExperiment*, CString szID, CWnd* pOwner = NULL, UINT iSelectPage = 0 );
	BOOL DoEditDup( CString szID, CWnd* pOwner = NULL );
	static BOOL DoEditDup( IExperiment*, CString szID, CWnd* pOwner = NULL );

	void SetLoggingOn( BOOL val )
	{
		CString szAppPath;
		::GetDataPathRoot( szAppPath );
		szAppPath += _T("\\Actions.log");
		m_pExperiment->SetLoggingOn( val, szAppPath.AllocSysString() );
	}
	void SetOutputWindow( VARIANT* pWnd ) { m_pExperiment->SetOutputWindow( (VARIANT*)pWnd ); }
	HRESULT TestLogin() { return m_pExperiment->TestLogin(); }
	void SetAppWindow( HWND AppWindow );
	static void SetAppWindow( IExperiment*, HWND AppWindow );

	// Reports
	BOOL ReportSubjects( CString& szFile );
	static BOOL ReportSubjects( IExperiment*, CString& szFile );
	BOOL ReportQuestionnaires( CString& szFile );
	static BOOL ReportQuestionnaires( IExperiment*, CString& szFile );

	// Importing/Exporting
	BOOL Export( CMemFile* pFile );
	BOOL Import( CStdioFile* pFile, CString& szExpID, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );
	static BOOL Import( CString& szExp, BOOL& fTrials, CStringList* pListImported,
						BOOL fAskBackup = TRUE, CString szImportFile = _T("") );
	static BOOL Export( CString& szWMsg, CString szExp, CString& szDefExpPath,
						CString szSubjOnly = _T(""), CString szSubjGroup = _T(""),
						BOOL fWizard = FALSE, BOOL fWFiles = FALSE,
						BOOL fWTrials = FALSE, BOOL fWMembers = FALSE, BOOL fWShow = FALSE,
						CString szWPath = _T("") );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, BOOL fRecurse, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );
	virtual BOOL DoCopyAll( CString szID, CString szPath, BOOL fOverwrite = FALSE, BOOL fRecurse = FALSE );
	virtual BOOL DoUploadAll( CString szID, BOOL fOverwrite = FALSE, BOOL fRecurse = FALSE );
	// Data validation
	virtual BOOL ValidateData( CStringList* pListErrors, BOOL fFirstOnly = FALSE, BOOL fNotify = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();

// Running/Processing/Etc-Experiment related
public:
	// global vars
	static HTREEITEM	_hItem;
	static IProcess*	_pProcess;
	static short		_Type;
	static BOOL			_Reproc;
	static BOOL			_ReprocAll;
	static BOOL			_TerminateThread;
	static CString		_ExpID;
	static LPVOID		_ItemView;
	static LPVOID		_MsgView;
	// last time exp run for subject (NOT STATIC) - if no grp spec., loop thru all
	CTime LastRun( CString szSubjID, CString& szGrpID );
	// writing trial sequence
	static void WriteTrialSequence( CStdioFile* pFile, CStringList* pList );
	// run experiment (used by subject->run experiment)
	static BOOL RunExperiment( CString szExpID, CString szExp = _T(""),
							   CString szGrpID = _T(""), CString szGrp = _T(""),
							   CString szSubjID = _T(""), CString szSubj = _T(""),
							   LPVOID param = NULL );
	// create list of conditions
	static BOOL GetListOfConditions( CString szExp, CStringList* );
	// randomize trials for running an experiment (used by subject->run experiment)
	static BOOL Randomize( CStringList* pLstConds, CStringList* pList, BOOL fTrials,
						   BOOL& fSen2Word, CString szExp, CString szGrp,
						   CString szSubj );
	// randomize trials for running an experiment (used by subject->run experiment)
	// ...szErrFile is empty if succeeds, otherwise points to file for view
	static BOOL RandomizeWithRules( CStringList* pLstConds, CStringList* pList,
									CString szRules, CString szBlocks, CString szSels,
									CString szSelTrials, BOOL& fSen2Word, CString szExp,
									CString szGrp, CString szSubj, BOOL fInform,
									CString& szErrFile );
	// set condition sequence based upon specified sequence from experiment properties
	static BOOL SequenceTrials( CStringList* pList, CString szSequence, BOOL& fSen2Word,
								CString szExp, CString szGrp, CString szSubj );
	// Rule functions
	static BOOL ConditionSequenceRuleDoAllOnce( CStringList* pListConds, int nPosIdx,
												CString szCond, int nChar,
												CStringList* pListChars );
	static BOOL ConditionSequenceRuleBlockOfSame( CStringList* pListConds, int nPosIdx,
												  CString szCond, int nChar, CStringList*
												  pListChars, int nBlocks );
	static BOOL ConditionSequenceRuleOnTrialNumber( int nTrial, CString szCond,
													int nChar, char c,
													CStringList* pListSelTrials );

	// processing object
	static IProcess* GetProcessObject( CWnd* pMsgList, IProcSetting* pPS = NULL,
									   CString szExp = _T(""), BOOL fSettings = FALSE );
	static void Cleanup();
	// processing steps
	static void CleanProcessFiles( CString szRootPath, CString szExpID, BOOL fHWR, BOOL fTF, BOOL fSeg,
								   BOOL fExt, BOOL fCon, BOOL fErr, BOOL fInc );
	static void ProcessSen2wrd( CString szIn, CString szOut, CWnd* pMsgList );
	static void ProcessUnrot( CString szIn, CString szOut, BOOL fVelocity,
							  BOOL fDiff, double dRotBeta, CWnd* pMsgList );
	static void ProcessTimfun( CString szIn, CString szOut, double dFreq,
							   int nDecimate, UINT tfSwitch, double dRotBeta, CWnd* pMsgList );
	static void ProcessSegmen( CString szIn, CString szOut, UINT segSwitch, CWnd* pMsgList, UINT gripSwitch = 0 );
	static void ProcessExtract( CString szTF, CString szSeg, CString szExt, UINT extSwitch, CWnd* pMsgList, UINT gripSwitch = 0 );
	static void ProcessConsis( CString szExtIn, CString szConOut, CString szLexIn,
						short nStrokeMin, short nStrokeMax, double dLength, double dRange,
						double dDir, double dRangeDir, short nSkip, BOOL fFlagBadTarget,
						CString szErrOut, CString szCondition, UINT conSwitch,
						BSTR trials, BOOL fLastOneOnly, BOOL& fPassed, CWnd* pMsgList,
						BOOL fDisDiscont, double dMagnetForce = 0., UINT gripSwitch = 0);
	static BOOL ProcessTrial( CString szExpID, CString szSubjID, CString szGrpID,
					   CString szCondID, CString szTrial, BOOL fAppend, BOOL fFlagBadTarget,
					   IProcSetting* pPS, LPVOID param /* default is CLeftView */, CWnd* pMsgList,
					   int& nSkipped, BOOL fDisplay = FALSE, HTREEITEM hItem = NULL,
					   BOOL fForceReproc = FALSE, BOOL fResetSkip = FALSE, UINT nGripSwitch = 0 );
	static void Reprocess( CString szExpID, LPVOID itemView = NULL, LPVOID msgView = NULL );
	static UINT ReprocessExperiment( LPVOID pParam );
	static void ReprocessSubject( CString szExpID, CString szSubjID, CString szGrpID, double dDevRes,
								  int nSampRate, HTREEITEM hItem, LPVOID param, CWnd* pMsgList,
								  int nSubjCount, int nCurSubj, BOOL fInformIfEmpty = TRUE,
								  BOOL fSubCond = TRUE, BOOL fSeparateThread = TRUE, BOOL fReprocAll = TRUE );
	static BOOL DidTrialPass( CString szTrial, CString szExpID, CString szGrpID,
							  CString szSubjID, CString szCondID, BOOL& fFound );
	static BOOL SummarizeExperiment( CString szExp, BOOL fDlg = TRUE, BOOL fOnly = FALSE,
									 BOOL fInc = FALSE, BOOL fErr = FALSE,
									 CString szSubjID = _T(""), CWnd* pMsgList = NULL,
									 BOOL fDoConsis = FALSE, BOOL fNormDB = FALSE,
									 CString szGrpID = _T("") );
	static int GetMostFrequentOccurrence( CStringList* pList, CString& szError );

	static BOOL GetInstructionFile( CString szExpID, CString& szFile, BOOL fNoExt = FALSE );

	// External Applications
	static BOOL IsMatlabInstalled();
	static BOOL ExecuteMatlabScript( CString szScript, CString szExp, CString szGrp,
									 CString szSubj, CString szCond, CString szTrial,
									 CString szExt );
	static BOOL ExecuteBatchScript( CString szScript, CString szExp, CString szGrp,
									CString szSubj, CString szCond, CString szTrial,
									CString szExt, BOOL fNoWait = FALSE );
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

AFX_EXT_API void SetReprocAll( BOOL fVal );
AFX_EXT_API void SetTermThread( BOOL fVal );

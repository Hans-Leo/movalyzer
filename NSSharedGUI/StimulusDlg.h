// StimulusDlg.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// StimulusDlg dialog

class AFX_EXT_CLASS StimulusDlg : public CBCGPDialog
{
// Construction
public:
	StimulusDlg( CWnd* pParent = NULL, BOOL fNew = FALSE, BOOL fDup = FALSE );

// Dialog Data
	enum { IDD = IDD_STIMULUS };
	CBCGPListBox	m_lstTargets;
	CBCGPListBox	m_lstTargetsAvail;
	CBCGPListBox	m_lstElements;
	CString			m_szID;
	CString			m_szDesc;
	BOOL			m_fNew;
	BOOL			m_fDup;
	BOOL			m_fInit;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	CStringList		m_lstOrigElem;
	CStringList		m_lstElem;
	CStringList		m_lstOrigTarg;
	CStringList		m_lstTarg;
	BOOL			m_fGenerated;
	CString			m_szFile;
	CBCGPButton		m_bnAdd;
	CBCGPButton		m_bnDel;
	CBCGPButton		m_bnEdit;
	CBCGPButton		m_bnRight;
	CBCGPButton		m_bnRightAll;
	CBCGPButton		m_bnLeft;
	CBCGPButton		m_bnLeftAll;
	CBCGPButton		m_bnUp;
	CBCGPButton		m_bnDown;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillLists();
	void FillTargets();
	BOOL Confirm();
	void EnableDisable();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEditId();
	afx_msg void OnClickBnEdit();
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnDel();
	afx_msg void OnClickBnLeft();
	afx_msg void OnClickBnRight();
	afx_msg void OnClickBnLeftAll();
	afx_msg void OnClickBnRightAll();
	afx_msg void OnClickBnUp();
	afx_msg void OnClickBnDown();
	afx_msg void OnBnGen();
	afx_msg void OnBnGraph();
	afx_msg void OnBnView();
	afx_msg void OnDblclkListElements();
	afx_msg void OnDblclkListTargets();
	afx_msg void OnDblclkListTargetsavail();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnLbnSelchangeListElements();
	afx_msg void OnLbnSelchangeListTargetsavail();
	afx_msg void OnLbnSelchangeListTargets();
	DECLARE_MESSAGE_MAP()
};

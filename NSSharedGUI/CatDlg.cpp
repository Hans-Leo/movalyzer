// CatDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "CatDlg.h"
#include "..\CTreeLink\DBCommon.h"
#include "Cats.h"

// CatDlg dialog

IMPLEMENT_DYNAMIC(CatDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(CatDlg, CBCGPDialog)
	ON_WM_HELPINFO()
	ON_EN_KILLFOCUS(IDC_EDIT_ABBR, OnEnKillfocusEditAbbr)
END_MESSAGE_MAP()

CatDlg::CatDlg(CWnd* pParent, BOOL fNew)
	: CBCGPDialog(CatDlg::IDD, pParent), m_fNew( fNew )
	, m_szID(_T(""))
	, m_szDesc(_T(""))
	, m_nType(0)
{
	// List of existing categories
	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	Cats cat;
	cat.SetDataPath( szPath );
	HRESULT hr = cat.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cat.GetID( szItem );
		m_lstItems.AddTail( szItem );
		hr = cat.GetNext();
	}
}

CatDlg::~CatDlg()
{
}

void CatDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ABBR, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_CBIndex(pDX, IDC_CBO_TYPE, m_nType);
	DDX_Control(pDX, IDC_CBO_TYPE, m_cboType);
}

// CatDlg message handlers

BOOL CatDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Category ID.") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Description of category.") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );
	pWnd = GetDlgItem( IDC_CBO_TYPE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select the type of this category." );

	if( !m_fNew )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// Dialog icon
	CWinApp* pApp = AfxGetApp();
	// TODO:
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_CAT ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// Fill category type list
	// TODO: Only currently supporting elements
	// Thus, combo box item # needs to be adjusted by 5
	m_cboType.AddString( _T("None") );			// EXP_TYPE_NONE = 0
//	m_cboType.AddString( _T("Experiment") );	// EXP_TYPE_EXPERIMENT = 1
//	m_cboType.AddString( _T("Group") );			// EXP_TYPE_GROUP = 2
//	m_cboType.AddString( _T("Subject") );		// EXP_TYPE_SUBJECT = 3
//	m_cboType.AddString( _T("Condition") );		// EXP_TYPE_CONDITION= 4
//	m_cboType.AddString( _T("Stimulus") );		// EXP_TYPE_STIMULUS = 5
	m_cboType.AddString( _T("Element") );		// EXP_TYPE_ELEMENT= 6
	m_cboType.SetCurSel( m_nType == 0 ? 0 : m_nType - 5 );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CatDlg::OnHelpInfo(HELPINFO* pHelpInfo)
{
	::GoToHelp( _T("categories.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

BOOL CatDlg::PreTranslateMessage(MSG* pMsg)
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void CatDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The category ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("The category description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CBCGPDialog::OnOK();

	// TODO: Only currently supporting elements
	// Thus, combo box item # needs to be adjusted by 5
	if( m_nType != 0 ) m_nType += 5;
}

void CatDlg::OnEnKillfocusEditAbbr()
{
	UpdateData( TRUE );

	if( m_szID == _T("") ) return;

	if( m_lstItems.Find( m_szID ) )
	{
		BCGPMessageBox( _T("This ID already exists.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
}

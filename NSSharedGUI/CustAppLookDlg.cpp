// CustAppLookDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "CustAppLookDlg.h"
#include "CustomStyle.h"

// CCustAppLookDlg dialog

IMPLEMENT_DYNAMIC(CCustAppLookDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(CCustAppLookDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_COLOR1, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR2, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR3, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR4, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR5, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR6, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR7, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR8, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR9, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR10, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR11, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR12, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR14, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_COLOR15, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_RESET, OnReset)
END_MESSAGE_MAP()

void PreviewColorBar::DrawItem( LPDRAWITEMSTRUCT lpDIS )
{
	CDC* pDC = GetDC();
	RECT rect;
	GetWindowRect( &rect );
	ScreenToClient( &rect );

	CBCGPDrawManager dm( *pDC );
	dm.FillGradient( rect, m_col1, m_col2, FALSE );
}

CCustAppLookDlg::CCustAppLookDlg( CWnd* pParent )
	: CBCGPDialog( CCustAppLookDlg::IDD, pParent )
{
}

CCustAppLookDlg::~CCustAppLookDlg()
{
}

void CCustAppLookDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BN_COLOR1, m_color11);
	DDX_Control(pDX, IDC_BN_COLOR2, m_color12);
	DDX_Control(pDX, IDC_PREVIEW1, m_preview1);
	DDX_Control(pDX, IDC_BN_COLOR3, m_color21);
	DDX_Control(pDX, IDC_BN_COLOR4, m_color22);
	DDX_Control(pDX, IDC_PREVIEW2, m_preview2);
	DDX_Control(pDX, IDC_BN_COLOR5, m_color31);
	DDX_Control(pDX, IDC_BN_COLOR6, m_color32);
	DDX_Control(pDX, IDC_PREVIEW3, m_preview3);
	DDX_Control(pDX, IDC_BN_COLOR7, m_color41);
	DDX_Control(pDX, IDC_BN_COLOR8, m_color42);
	DDX_Control(pDX, IDC_PREVIEW4, m_preview4);
	DDX_Control(pDX, IDC_BN_COLOR9, m_color51);
	DDX_Control(pDX, IDC_BN_COLOR10, m_color52);
	DDX_Control(pDX, IDC_PREVIEW5, m_preview5);
	DDX_Control(pDX, IDC_BN_COLOR11, m_color61);
	DDX_Control(pDX, IDC_BN_COLOR12, m_color62);
	DDX_Control(pDX, IDC_PREVIEW6, m_preview6);
	DDX_Control(pDX, IDC_BN_COLOR13, m_color71);
	DDX_Control(pDX, IDC_BN_COLOR14, m_colorApp1);
	DDX_Control(pDX, IDC_BN_COLOR15, m_colorApp2);
	DDX_Control(pDX, IDC_PREVIEW7, m_preview7);
}

BOOL CCustAppLookDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	EnableVisualManagerStyle( TRUE, TRUE );
	FillColors();

	return TRUE;
}

// CCustAppLookDlg message handlers

void CCustAppLookDlg::FillColors()
{
	m_colorApp1.SetColor( m_CS.m_appbg1 );
	m_colorApp1.EnableAutomaticButton (_T("Default"), m_CS.m_appbg1);
	m_colorApp1.EnableOtherButton( _T("Custom...") );
	m_colorApp2.SetColor( m_CS.m_appbg2 );
	m_colorApp2.EnableAutomaticButton (_T("Default"), m_CS.m_appbg2);
	m_colorApp2.EnableOtherButton( _T("Custom...") );
	m_preview7.m_col1 = m_CS.m_appbg1;
	m_preview7.m_col2 = m_CS.m_appbg2;

	m_color11.SetColor( m_CS.m_barbg1 );
	m_color11.EnableAutomaticButton (_T("Default"), m_CS.m_barbg1);
	m_color11.EnableOtherButton( _T("Custom...") );
	m_color12.SetColor( m_CS.m_barbg2 );
	m_color12.EnableAutomaticButton (_T("Default"), m_CS.m_barbg2);
	m_color12.EnableOtherButton( _T("Custom...") );
	m_preview1.m_col1 = m_CS.m_barbg1;
	m_preview1.m_col2 = m_CS.m_barbg2;

	m_color21.SetColor( m_CS.m_barcaptionbg1 );
	m_color21.EnableAutomaticButton (_T("Default"), m_CS.m_barcaptionbg1);
	m_color21.EnableOtherButton( _T("Custom...") );
	m_color22.SetColor( m_CS.m_barcaptionbg2 );
	m_color22.EnableAutomaticButton (_T("Default"), m_CS.m_barcaptionbg2);
	m_color22.EnableOtherButton( _T("Custom...") );
	m_preview2.m_col1 = m_CS.m_barcaptionbg1;
	m_preview2.m_col2 = m_CS.m_barcaptionbg2;

	m_color31.SetColor( m_CS.m_gripper1 );
	m_color31.EnableAutomaticButton (_T("Default"), m_CS.m_gripper1);
	m_color31.EnableOtherButton( _T("Custom...") );
	m_color32.SetColor( m_CS.m_gripper2 );
	m_color32.EnableAutomaticButton (_T("Default"), m_CS.m_gripper2);
	m_color32.EnableOtherButton( _T("Custom...") );
	m_preview3.m_col1 = m_CS.m_gripper1;
	m_preview3.m_col2 = m_CS.m_gripper2;

	m_color41.SetColor( m_CS.m_taberase1 );
	m_color41.EnableAutomaticButton (_T("Default"), m_CS.m_taberase1);
	m_color41.EnableOtherButton( _T("Custom...") );
	m_color42.SetColor( m_CS.m_taberase2 );
	m_color42.EnableAutomaticButton (_T("Default"), m_CS.m_taberase2);
	m_color42.EnableOtherButton( _T("Custom...") );
	m_preview4.m_col1 = m_CS.m_taberase1;
	m_preview4.m_col2 = m_CS.m_taberase2;

	m_color51.SetColor( m_CS.m_splitter1 );
	m_color51.EnableAutomaticButton (_T("Default"), m_CS.m_splitter1);
	m_color51.EnableOtherButton( _T("Custom...") );
	m_color52.SetColor( m_CS.m_splitter2 );
	m_color52.EnableAutomaticButton (_T("Default"), m_CS.m_splitter2);
	m_color52.EnableOtherButton( _T("Custom...") );
	m_preview5.m_col1 = m_CS.m_splitter1;
	m_preview5.m_col2 = m_CS.m_splitter2;

	m_color61.SetColor( m_CS.m_menuhl1 );
	m_color61.EnableAutomaticButton (_T("Default"), m_CS.m_menuhl1);
	m_color61.EnableOtherButton( _T("Custom...") );
	m_color62.SetColor( m_CS.m_menuhl2 );
	m_color62.EnableAutomaticButton (_T("Default"), m_CS.m_menuhl2);
	m_color62.EnableOtherButton( _T("Custom...") );
	m_preview6.m_col1 = m_CS.m_menuhl1;
	m_preview6.m_col2 = m_CS.m_menuhl2;

	m_color71.SetColor( m_CS.m_menubartext );
	m_color71.EnableOtherButton( _T("Custom...") );
	m_color71.EnableAutomaticButton (_T("Default"), m_CS.m_menubartext);

	UpdateData( FALSE );
}

void CCustAppLookDlg::OnChangeColor()
{
	m_CS.m_appbg1 = m_colorApp1.GetColor();
	m_CS.m_appbg2 = m_colorApp2.GetColor();
	m_preview7.m_col1 = m_CS.m_appbg1;
	m_preview7.m_col2 = m_CS.m_appbg2;
	m_preview7.Invalidate();

	m_CS.m_barbg1 = m_color11.GetColor();
	m_CS.m_barbg2 = m_color12.GetColor();
	m_preview1.m_col1 = m_CS.m_barbg1;
	m_preview1.m_col2 = m_CS.m_barbg2;
	m_preview1.Invalidate();

	m_CS.m_barcaptionbg1 = m_color21.GetColor();
	m_CS.m_barcaptionbg2 = m_color22.GetColor();
	m_preview2.m_col1 = m_CS.m_barcaptionbg1;
	m_preview2.m_col2 = m_CS.m_barcaptionbg2;
	m_preview2.Invalidate();

	m_CS.m_gripper1 = m_color31.GetColor();
	m_CS.m_gripper2 = m_color32.GetColor();
	m_preview3.m_col1 = m_CS.m_gripper1;
	m_preview3.m_col2 = m_CS.m_gripper2;
	m_preview3.Invalidate();

	m_CS.m_taberase1 = m_color41.GetColor();
	m_CS.m_taberase2 = m_color42.GetColor();
	m_preview4.m_col1 = m_CS.m_taberase1;
	m_preview4.m_col2 = m_CS.m_taberase2;
	m_preview4.Invalidate();

	m_CS.m_splitter1 = m_color51.GetColor();
	m_CS.m_splitter2 = m_color52.GetColor();
	m_preview5.m_col1 = m_CS.m_splitter1;
	m_preview5.m_col2 = m_CS.m_splitter2;
	m_preview5.Invalidate();

	m_CS.m_menuhl1 = m_color61.GetColor();
	m_CS.m_menuhl2 = m_color62.GetColor();
	m_preview6.m_col1 = m_CS.m_menuhl1;
	m_preview6.m_col2 = m_CS.m_menuhl2;
	m_preview6.Invalidate();
}

void CCustAppLookDlg::OnReset()
{
	m_CS.Reset();
	FillColors();
	OnChangeColor();
}

void CCustAppLookDlg::OnOK()
{
	m_CS.m_menubartext = m_color71.GetColor();

	CBCGPDialog::OnOK();
}

#pragma once

#include "NSTooltipCtrl.h"

// RelationshipDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// RelationshipDlg dialog

#define	REL_NONE		0
#define	REL_GROUP		1
#define	REL_SUBJECT		2
#define	REL_CONDITION	3
#define	REL_STIMULUS	4
#define	REL_ELEMENT		5
#define	REL_CAT			6
#define	REL_FEEDBACK	7

class AFX_EXT_CLASS RelationshipDlg : public CBCGPDialog
{
// Construction
public:
	RelationshipDlg(CWnd* pParent = NULL, UINT nType = REL_NONE,
					CString szItem = _T("") );

// Dialog Data
	enum { IDD = IDD_RELATIONS };
	CComboBox		m_cboConditions;
	CComboBox		m_cboSubjects;
	CComboBox		m_cboGroups;
	CComboBox		m_cboElem;
	CComboBox		m_cboStim;
	CComboBox		m_cboCat;
	CComboBox		m_cboFeedback;
	CTreeCtrl		m_tree;
	NSToolTipCtrl	m_tooltip;
	CImageList		m_ImageList;
	HTREEITEM		m_hGrps, m_hSubjs, m_hConds, m_hStim, m_hElem, m_hCat, m_hFb;
	UINT			m_nType;
	CString			m_szItem;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void SetCombos( CComboBox* );
	void ShowGroup( CString szItem, HTREEITEM hRoot = NULL );
	void ShowCondition( CString szItem, HTREEITEM hRoot = NULL );
	void ShowSubject( CString szItem, HTREEITEM hRoot = NULL );
	void ShowStimulus( CString szItem, HTREEITEM hRoot = NULL );
	void ShowElement( CString szItem, HTREEITEM hRoot = NULL );
	void ShowCategory( CString szItem, HTREEITEM hRoot = NULL );
	void ShowFeedback( CString szItem, HTREEITEM hRoot = NULL );

	afx_msg void OnSelchangeCboSubjects();
	afx_msg void OnSelchangeCboGroups();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboConditions();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnSelchangeCboStimuli();
	afx_msg void OnSelchangeCboElements();
	afx_msg void OnSelchangeCboCats();
	afx_msg void OnSelchangeCboFeedbacks();
	afx_msg void OnNMDblclkTree(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()
public:
};

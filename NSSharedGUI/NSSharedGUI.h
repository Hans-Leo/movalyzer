// NSSharedGUI.h : main header file for the NSSharedGUI DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "..\Common\UserObj.h"
#include "WindowManager.h"

/////////////////////////////////////////////////////////////////////////////
// CNSSharedGUIApp
// See NSSharedGUI.cpp for the implementation of this class
//

class CNSSharedGUIApp : public CWinApp
{
public:
	CNSSharedGUIApp();

// Overrides
	DECLARE_MESSAGE_MAP()
};

class AFX_EXT_CLASS NSSecurity;

void AFX_EXT_API SetWorkspaceApp( CBCGPWorkspace* );
AFX_EXT_API CBCGPWorkspace* GetWorkspaceApp();

void AFX_EXT_API SetIsAppRx( BOOL );
BOOL IsAppRx();

void AFX_EXT_API SetSecurity( NSSecurity* );
AFX_EXT_API NSSecurity* GetSecurity();

void AFX_EXT_API SetCurrentUserObj( UserObj* pObj );
AFX_EXT_API UserObj* GetCurrentUserObj();

void AFX_EXT_API SetWindowManager( WindowManager* pWM );
AFX_EXT_API WindowManager* GetWindowManager();

void GetHelpPath( CString& szPath );
void AFX_EXT_API GoToHelp( CString szLoc, CWnd* pOwner );

void AFX_EXT_API SetTerminateBackupThread( BOOL fTerm );

void AFX_EXT_API SetMissingDataValue( double dVal );
double AFX_EXT_API GetMissingDataValue();

void ViewCalc( CWnd* pWnd );
void CalcClosed();

// show modeless popup window (x=-1 & y=-1 & fcenter = false -> above system tray)
void AFX_EXT_API ShowPopupWindow( CWnd* pParent,
								  CString szMsg, int nXOrigin = -1, int nYOrigin = -1,
								  BOOL fCenter = FALSE, HMENU hMenu = NULL,
								  CString szURL = _T(""), UINT nURL = 0,
								  BYTE nTransparency = 220 /* 0-255[opaque] */,
								  int nTimeToClose = 3000 /* ms */ );
// kill modeless popup window
void AFX_EXT_API KillPopupWindow();

// module authorization
void AFX_EXT_API SetAuthorizationProcessMod( CString szAuthCode );
CString& GetAuthorizationProcessMod();
void AFX_EXT_API SetAuthorizationInputMod( CString szAuthCode );
CString& GetAuthorizationInputMod();

// load and decode (for true encryption stored in RES table)
CString AFX_EXT_API LoadAndDecode( UINT nID );

/////////////////////////////////////////////////////////////////////////////

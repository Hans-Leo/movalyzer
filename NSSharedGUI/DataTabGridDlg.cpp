// DataTabGridDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GraphDlg.h"
#include "DataTabGridDlg.h"
#include "DataTabDlg.h"
#include "WindowManager.h"
#include "MSOffice.h"

// DataTabGridDlg dialog
IMPLEMENT_DYNAMIC(DataTabGridDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(DataTabGridDlg, CBCGPDialog)
	ON_WM_SIZE()
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_EDIT, OnBnClickedBnEdit)
	ON_BN_CLICKED(IDC_BN_REFRESH, OnBnClickedBnRefresh)
	ON_BN_CLICKED(IDC_BN_EXCEL, OnClickExcel)
	ON_BN_CLICKED(IDC_BN_SORT, OnClickSort)
END_MESSAGE_MAP()

#define	GRID_ROW_ERROR	50

//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    GridCallback
// FullName:  GridCallback
// Access:    public static 
// Returns:   BOOL CALLBACK
// Qualifier:
// Parameter: BCGPGRID_DISPINFO * pdi
// Parameter: LPARAM lp
//************************************
static BOOL CALLBACK GridCallback (BCGPGRID_DISPINFO* pdi, LPARAM lp )
{
	DataTabGridDlg* pGD = (DataTabGridDlg*)lp;
	if( !pdi || !pGD || !pGD->_pGrid || !pGD->_pFile || !pGD->_pFile->m_pStream ) return TRUE;

	int nRow = pdi->item.nRow;	// Row of an item
	int nCol = pdi->item.nCol;	// Column of an item
	if( ( nRow < 0 ) || ( nCol < 0 ) ) return TRUE;

	CStringList* pList = NULL;
	// grid height
	CRect rect;
	pGD->_pGrid->GetWindowRect( &rect );
	// est. # rows for height
	int nRowHeight = pGD->_pGrid->GetRowHeight();
	int nRows = rect.Height() / nRowHeight + 1;
	// starting/ending rows
	int nStart = nRow - GRID_ROW_ERROR;
	if( nStart < 0 ) nStart = 0;
	int nLast = nRow + nRows + GRID_ROW_ERROR;
	if( nLast > pGD->_nRows ) nLast = pGD->_nRows;
	nRows = nLast - nStart;
	// if we are out of range of our last read, or have not yet read
	if( ( nRow < pGD->_nStartRow ) || ( nRow >= pGD->_nLastRow ) || ( pGD->_nStartRow == -1 ) )
	{
		// set global start/end vars
		pGD->_nStartRow = nStart;
		pGD->_nLastRow = nLast;
		// delete & recreate string list
		if( pGD->_pDataList ) delete [] pGD->_pDataList;
		pGD->_pDataList = new CStringList[ nRows ];
		pGD->_nListSize = nRows;
		// # of columns in grid
		int nCols = pGD->_pGrid->GetColumnCount();

		// DATA
		CString szLine, szItem;
		int nItem = 0, nPos = 0, nCurRow = 0, nCurCol = 0;
		// start at beginning of file
		pGD->_pFile->SeekToBegin();
		// skip header
		pGD->_pFile->ReadString( szLine );
		// get to 1st line based on _nStartRow;
		while( nItem < pGD->_nStartRow )
		{
			pGD->_pFile->ReadString( szLine );
			nItem++;
		}
		nCurRow = nItem;
		nItem = 0;
		while( pGD->_pFile->ReadString( szLine ) && ( nItem < nRows ) )
		{
			TRIM( szLine );
			if( szLine == _T("") ) continue;

			szItem.Format( _T("%d"), nCurRow + 1 );
			pGD->_pDataList[ nItem ].AddTail( szItem );

			nCurCol = 1;

			nPos = szLine.Find( _T(" ") );
			while( nPos != -1 )
			{
				szItem = szLine.Left( nPos );
				pGD->_pDataList[ nItem ].AddTail( szItem );

				nCurCol++;

				szLine = szLine.Mid( nPos + 1 );
				TRIM( szLine );
				nPos = szLine.Find( _T(" ") );
			}
			// last one
			pGD->_pDataList[ nItem ].AddTail( szLine );

			nItem++;
			nCurRow++;
		}
	}

	// get current string value from global list
	int nDataRow = nRow - pGD->_nStartRow;
	if( nDataRow < 0 ) nDataRow = 0;
	if( nDataRow >= pGD->_nListSize ) return TRUE;
	pList = &( pGD->_pDataList[ nDataRow ] );
	if( !pList ) return TRUE;
	POSITION pos = pList->FindIndex( nCol );
	if( !pos ) return FALSE;
	CString szItem = pList->GetNext( pos );

	// set value
	pdi->item.varValue = szItem.GetBuffer();

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    DataTabGridDlg
// FullName:  DataTabGridDlg::DataTabGridDlg
// Access:    public 
// Returns:   
// Qualifier: : CBCGPDialog(), m_szFile( szFile ), m_pWM( pWM ), m_nCurChart( nCurChart )
// Parameter: CString szFile
// Parameter: CWnd * pParent
// Parameter: WindowManager * pWM
// Parameter: int nCurChart
//************************************
DataTabGridDlg::DataTabGridDlg(CString szFile, CWnd* pParent /*=NULL*/, WindowManager* pWM /*=NULL*/, int nCurChart /*=0*/ )
	: CBCGPDialog(), m_szFile( szFile ), m_pWM( pWM ), m_nCurChart( nCurChart )
{
	m_fInit = FALSE;
	m_nRows = 0;
	_nStartRow = -1;
	_nLastRow = -1;
	_pFile = NULL;
	_pGrid = NULL;
	_nRows = 0;
	_pDataList = NULL;
	_nListSize = -1;

	if( Create( DataTabGridDlg::IDD, pParent ) ) ShowWindow( SW_SHOW );
}

//************************************
// Method:    ~DataTabGridDlg
// FullName:  DataTabGridDlg::~DataTabGridDlg
// Access:    public 
// Returns:   
// Qualifier:
//************************************
DataTabGridDlg::~DataTabGridDlg()
{
	if( _pDataList )
	{
		delete [] _pDataList;
		_pDataList = NULL;
	}
}

//************************************
// Method:    DoDataExchange
// FullName:  DataTabGridDlg::DoDataExchange
// Access:    virtual protected 
// Returns:   void
// Qualifier:
// Parameter: CDataExchange * pDX
//************************************
void DataTabGridDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_RECT, m_wndGridLocation);
}

// DataTabGridDlg message handlers

//************************************
// Method:    PreTranslateMessage
// FullName:  DataTabGridDlg::PreTranslateMessage
// Access:    virtual public 
// Returns:   BOOL
// Qualifier:
// Parameter: MSG * pMsg
//************************************
BOOL DataTabGridDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

//************************************
// Method:    OnInitDialog
// FullName:  DataTabGridDlg::OnInitDialog
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL DataTabGridDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// initialize global vars
	_nStartRow = -1;
	_nLastRow = -1;
	_pFile = NULL;
	_pGrid = NULL;
	_nRows = 0;
	_pDataList = NULL;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close this dialog."), _T("Close") );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/edit the raw data in Notepad."), _T("Edit") );
	pWnd = GetDlgItem( IDC_BN_REFRESH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Refresh the list to it's original state."), _T("Refresh") );
	pWnd = GetDlgItem( IDC_BN_EXCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open this file in an Excel spreadsheet.\nExcel must be installed."), _T("Microsoft Excel") );
	pWnd = GetDlgItem( IDC_BN_SORT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open this file in an alternate view where you can sort the data.\nNOTE: Load and sort times can be long for large files."), _T("Sorting") );

	// create grid control
	CRect rectGrid;
	m_wndGridLocation.GetClientRect( &rectGrid );
	m_wndGridLocation.MapWindowPoints( this, &rectGrid );
	m_wndGrid.Create( WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER, rectGrid, this, (UINT)-1 );
	m_wndGrid.EnableHeader( TRUE, BCGP_GRID_HEADER_MOVE_ITEMS );
	m_wndGrid.SetWholeRowSel( TRUE );
//	m_wndGrid.SetSingleSel( TRUE );
	m_wndGrid.EnableGroupByBox( FALSE );
	m_wndGrid.SetReadOnly();

	// Determine size, location differences for later use in resizing
	// ...the constant numbers below represent the pixel distance of
	// ...the edges of the graph window from the edge of the dialog window
	RECT rect, rect2;
	CWnd* pList = GetDlgItem( IDC_GRID_RECT );
	pList->GetWindowRect( &rect );
	GetWindowRect( &rect2 );
	int nToolbar = rect.top - rect2.top;
	m_nRightBuffer = ( ( rect2.right - rect2.left ) - ( rect.right - rect.left ) ) - 4;
	m_nBottomBuffer = ( ( rect2.bottom - rect2.top ) - ( rect.bottom - rect.top ) ) - 4;
	ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	nToolbar -= m_nTop;
	m_nBottomBuffer -= nToolbar;

	// fill list
	FillColumns( m_szFile );
	FillList( m_szFile );

	int nPos = m_szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		CString szTitle = m_szFile.Mid( nPos + 1 );
		SetWindowText( szTitle );
	}

	EnableVisualManagerStyle( TRUE, TRUE );
	m_fInit = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//************************************
// Method:    OnSize
// FullName:  DataTabGridDlg::OnSize
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nType
// Parameter: int cx
// Parameter: int cy
//************************************
void DataTabGridDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	if( m_fInit )
	{
		m_wndGrid.MoveWindow( m_nLeft, m_nTop, cx - m_nRightBuffer,
							  cy - m_nBottomBuffer, FALSE );
		m_wndGrid.AdjustLayout();
		RedrawWindow();
	}
}

//************************************
// Method:    OnCancel
// FullName:  DataTabGridDlg::OnCancel
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void DataTabGridDlg::OnCancel()
{
	DestroyWindow();
}

//************************************
// Method:    PostNcDestroy
// FullName:  DataTabGridDlg::PostNcDestroy
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void DataTabGridDlg::PostNcDestroy()
{
	if( m_pWM ) m_pWM->RemoveItem( this );
	delete this;
}

//************************************
// Method:    OnHelpInfo
// FullName:  DataTabGridDlg::OnHelpInfo
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: HELPINFO * pHelpInfo
//************************************
BOOL DataTabGridDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("viewingtrials.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

//************************************
// Method:    OnBnClickedBnEdit
// FullName:  DataTabGridDlg::OnBnClickedBnEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabGridDlg::OnBnClickedBnEdit()
{
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += m_szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnBnClickedBnRefresh
// FullName:  DataTabGridDlg::OnBnClickedBnRefresh
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabGridDlg::OnBnClickedBnRefresh()
{
	FillList( m_szFile );
}

//************************************
// Method:    OnClickExcel
// FullName:  DataTabGridDlg::OnClickExcel
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabGridDlg::OnClickExcel()
{
	CString szMsg = _T("Please ensure all data views using the grid are closed before opening MS Excel.\nContinue?" );
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return;

	_pFile = NULL;
	m_file.Close();
	EndDialog( IDOK );

	if( !Excel::OpenDelimitedText( m_szFile, FALSE, FALSE, FALSE, FALSE, TRUE ) )
		BCGPMessageBox( _T("Unable to open file in spreadsheet.") );
}

//************************************
// Method:    FillColumns
// FullName:  DataTabGridDlg::FillColumns
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szFile
//************************************
void DataTabGridDlg::FillColumns( CString szFile )
{
	// remove previous columns
	m_wndGrid.DeleteAllColumns();

	// get the column names and add to grid
	if( !m_file.m_pStream )
	{
		CFileFind ff;
		if( szFile == _T("") || !ff.FindFile( szFile ) ||
			!m_file.Open( szFile, CFile::modeRead | CFile::shareDenyNone ) )
		{
			BCGPMessageBox( _T("Unable to access file.") );
			return;
		}
	}
	else m_file.SeekToBegin();

	CString szLine, szItem;
	CStringList lstColumns;
	// we assume the 1st line is the headers
	// we also assume a space as a field delimiter (TODO: we can enhance this later)
	m_file.ReadString( szLine );
	TRIM( szLine );
	int nPos = szLine.Find( _T(" ") );
	while( nPos != -1 )
	{
		szItem = szLine.Left( nPos );
		lstColumns.AddTail( szItem );
		szLine = szLine.Mid( nPos + 1 );
		TRIM( szLine );
		nPos = szLine.Find( _T(" ") );
	}
	// add last one
	lstColumns.AddTail( szLine );
	// insert into grid
	m_wndGrid.InsertColumn( 0, _T("Row"), 40 );
	CString szTemp;
	POSITION pos = lstColumns.GetHeadPosition();
	int nCol = 1;
	while( pos )
	{
		szItem = lstColumns.GetNext( pos );
		szTemp.Format( _T(" (%d)"), nCol );
		szItem += szTemp;
		m_wndGrid.InsertColumn( nCol, szItem, 75 );
		nCol++;
	}

	// get # of rows
	m_nRows = 0;
	while( m_file.ReadString( szLine ) ) m_nRows++;
}

//************************************
// Method:    FillList
// FullName:  DataTabGridDlg::FillList
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szFile
//************************************
void DataTabGridDlg::FillList( CString szFile )
{
	// fill columns if necessary
	if( szFile != m_szFile ) FillColumns( szFile );

	_pFile = &m_file;
	_pGrid = &m_wndGrid;
	_nRows = m_nRows;

	// reset grid
	m_wndGrid.RemoveAll();
	m_wndGrid.EnableVirtualMode( GridCallback, (LPARAM)this );
	m_wndGrid.SetVirtualRows( m_nRows );
	m_wndGrid.AdjustLayout();
}

//************************************
// Method:    OnClickSort
// FullName:  DataTabGridDlg::OnClickSort
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabGridDlg::OnClickSort()
{
	DataTabDlg* d = new DataTabDlg( m_szFile, GetParent(), m_pWM );
}

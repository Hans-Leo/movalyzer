// ProcSetPageSeg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageSeg.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSeg property page

IMPLEMENT_DYNCREATE(ProcSetPageSeg, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageSeg, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_BN_CLICKED(IDC_CHK_SEGS, OnBnClickedChkSegs)
	ON_BN_CLICKED(IDC_RDO_SEGZ, OnBnClickedMethod)
	ON_BN_CLICKED(IDC_RDO_SEGM, OnBnClickedMethod)
	ON_BN_CLICKED(IDC_RDO_SEGJ, OnBnClickedMethod)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageSeg::ProcSetPageSeg( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageSeg::IDD), m_pPS( pPS )
{
	m_fF = FALSE;
	m_fL = FALSE;
	m_fO = FALSE;
	m_fS = FALSE;
	m_fM = FALSE;
	m_fA = FALSE;
	m_fV = FALSE;
	m_fMM = FALSE;

	m_chkF.SetDefaultAsOff( !m_fF );
	m_chkL.SetDefaultAsOff( !m_fL );
	m_chkO.SetDefaultAsOff( !m_fO );
	m_chkS.SetDefaultAsOff( !m_fS );
	m_chkV.SetDefaultAsOff( !m_fV );
	m_chkMM.SetDefaultAsOff( !m_fMM );
	m_rdoM.SetDefaultRadio( IDC_RDO_SEGZ );
	m_rdoZ.SetDefaultRadio( IDC_RDO_SEGZ );
	m_rdoJ.SetDefaultRadio( IDC_RDO_SEGZ );

	if( m_pPS )
	{
		IProcSetFlags* pPSF = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_SEG_F( &m_fF );
			pPSF->get_SEG_L( &m_fL );
			pPSF->get_SEG_O( &m_fO );
			pPSF->get_SEG_S( &m_fS );
			pPSF->get_SEG_M( &m_fM );
			pPSF->get_SEG_A( &m_fA );
			pPSF->get_SEG_V( &m_fV );
			pPSF->get_SEG_MM( &m_fMM );
			pPSF->get_SEG_J( &m_fJ );

			pPSF->Release();
		}
	}
}

ProcSetPageSeg::~ProcSetPageSeg()
{
}

void ProcSetPageSeg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_SEGF, m_fF);
	DDX_Check(pDX, IDC_CHK_SEGL, m_fL);
	DDX_Check(pDX, IDC_CHK_SEGO, m_fO);
	DDX_Check(pDX, IDC_CHK_SEGS, m_fS);
	DDX_Check(pDX, IDC_CHK_SEGV, m_fV);
	DDX_Check(pDX, IDC_CHK_SEGMM, m_fMM);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);

	DDX_Control(pDX, IDC_CHK_SEGF, m_chkF);
	DDX_Control(pDX, IDC_CHK_SEGL, m_chkL);
	DDX_Control(pDX, IDC_CHK_SEGO, m_chkO);
	DDX_Control(pDX, IDC_CHK_SEGS, m_chkS);
	DDX_Control(pDX, IDC_CHK_SEGV, m_chkV);
	DDX_Control(pDX, IDC_CHK_SEGMM, m_chkMM);
	DDX_Control(pDX, IDC_RDO_SEGM, m_rdoM);
	DDX_Control(pDX, IDC_RDO_SEGZ, m_rdoZ);
	DDX_Control(pDX, IDC_RDO_SEGJ, m_rdoJ);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSeg message handlers

BOOL ProcSetPageSeg::OnApply() 
{
	if( !m_pPS ) return FALSE;

	int nMethod = GetCheckedRadioButton( IDC_RDO_SEGZ, IDC_RDO_SEGJ );
	m_fM = ( nMethod == IDC_RDO_SEGM );
	m_fA = ( nMethod == IDC_RDO_SEGZ );
	m_fJ = ( nMethod == IDC_RDO_SEGJ );

	IProcSetFlags* pPSF = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( SUCCEEDED( hr ) )
	{
		pPSF->put_SEG_F( m_fF );
		pPSF->put_SEG_L( m_fL );
		pPSF->put_SEG_O( m_fO );
		pPSF->put_SEG_S( m_fS );
		pPSF->put_SEG_M( m_fM );
		pPSF->put_SEG_A( m_fA );
		pPSF->put_SEG_V( m_fV );
		pPSF->put_SEG_MM( m_fMM );
		pPSF->put_SEG_J( m_fJ );

		pPSF->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageSeg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageSeg::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	BOOL fF = FALSE, fL = FALSE, fO = FALSE, fS = FALSE, fM = FALSE, fA = FALSE, fV = FALSE, fMM = FALSE, fJ = FALSE;
	if( pPS )
	{
		IProcSetFlags* pPSF = NULL;
		HRESULT hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_SEG_F( &fF );
			pPSF->get_SEG_L( &fL );
			pPSF->get_SEG_O( &fO );
			pPSF->get_SEG_S( &fS );
			pPSF->get_SEG_M( &fM );
			pPSF->get_SEG_A( &fA );
			pPSF->get_SEG_V( &fV );
			pPSF->get_SEG_MM( &fMM );
			pPSF->get_SEG_J( &fJ );

			pPSF->Release();
		}
		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_CHK_SEGF );
	szData.LoadString( IDS_PSPS_FIRST );
	szTmp.Format( _T(" [DEFAULT=%s]"), fF ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("First Segment") );

	pWnd = GetDlgItem( IDC_CHK_SEGL );
	szData.LoadString( IDS_PSPS_LAST );
	szTmp.Format( _T(" [DEFAULT=%s]"), fL ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Last Segment") );

	pWnd = GetDlgItem( IDC_CHK_SEGS );
	szData.LoadString( IDS_PSPS_SPLIT );
	szTmp.Format( _T(" [DEFAULT=%s]"), fS ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Submovement") );

	pWnd = GetDlgItem( IDC_CHK_SEGO );
	szData.LoadString( IDS_PSPS_NOSPLIT );
	szTmp.Format( _T(" [DEFAULT=%s]"), fO ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("One Stroke") );

	pWnd = GetDlgItem( IDC_CHK_SEGV );
	szData = _T("Move segmentation point to nearest pendown if on a penlift");
	szTmp.Format( _T(" [DEFAULT=%s]"), fV ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Move") );

	pWnd = GetDlgItem( IDC_CHK_SEGMM );
	szData = _T("Move segmentation point to the nearest absolute velocity minima within stroke");
	szTmp.Format( _T(" [DEFAULT=%s]"), fMM ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Move") );

	pWnd = GetDlgItem( IDC_RDO_SEGZ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Segment at vertical velocity zero crossings [DEFAULT=At vertical velocity zero crossings]"), _T("Vertical Velocity") );

	pWnd = GetDlgItem( IDC_RDO_SEGM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Segment at absolute velocity minima [DEFAULT=At vertical velocity zero crossings]"), _T("Absolute Velocity") );

	pWnd = GetDlgItem( IDC_RDO_SEGJ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Segment at pendown trajectories [DEFAULT=At vertical velocity zero crossings]"), _T("Pendown") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	// 19Aug04 : Since we've removed the option (SEG_A) we need to ensure that
	// old data set to that gets reset to default
	if( m_fA )
	{
		m_fM = FALSE;
		m_fA = FALSE;
		m_fJ = FALSE;
	}

	// segmentation methods are not applicable in gripper exp
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet->m_fGripper )
	{
		pWnd = GetDlgItem( IDC_RDO_SEGZ );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_RDO_SEGM );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_RDO_SEGJ );
		pWnd->EnableWindow( FALSE );
	}

	CheckRadioButton( IDC_RDO_SEGZ, IDC_RDO_SEGJ,
					  ( m_fJ ? IDC_RDO_SEGJ :
							   ( m_fM ? IDC_RDO_SEGM : IDC_RDO_SEGZ ) ) );
	OnBnClickedMethod();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageSeg::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageSeg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_segmentation.html"), this );
	return TRUE;
}

void ProcSetPageSeg::OnBnReset() 
{
	m_fF = FALSE;
	m_fL = FALSE;
	m_fO = FALSE;
	m_fS = FALSE;
	m_fM = FALSE;
	m_fA = FALSE;
	m_fV = FALSE;
	m_fMM = FALSE;
	CheckRadioButton( IDC_RDO_SEGZ, IDC_RDO_SEGJ, IDC_RDO_SEGZ );

	UpdateData( FALSE );
}

void ProcSetPageSeg::OnBnClickedChkSegs()
{
	UpdateData( TRUE );
	if( m_fS && !::IsOA() )
	{
		NSSecurity* pSecurity = ::GetSecurity();
		if( pSecurity && !pSecurity->AO() )
		{
			m_fS = FALSE;
			UpdateData( FALSE );
		}
	}
	else
	{
		BCGPMessageBox( _T("This change requires reprocessing of all trials for this experiment followed by a Summarize.") );
	}
}

void ProcSetPageSeg::OnBnClickedMethod()
{
	int nMethod = GetCheckedRadioButton( IDC_RDO_SEGZ, IDC_RDO_SEGJ );
	BOOL fZ = ( nMethod == IDC_RDO_SEGZ );

	CWnd* pWnd = GetDlgItem( IDC_CHK_SEGMM );
	pWnd->EnableWindow( fZ );

	if( !fZ ) m_fMM = FALSE;
	UpdateData( FALSE );
}

void ProcSetPageSeg::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 16 );
}

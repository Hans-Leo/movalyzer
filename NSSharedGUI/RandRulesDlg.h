#pragma once

// RandRulesDlg dialog

#include "NSTooltipCtrl.h"

interface IProcSetting;

class RandRulesDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(RandRulesDlg)

public:
	RandRulesDlg( IProcSetting* pPS = NULL, CWnd* pParent = NULL );
	virtual ~RandRulesDlg();

// Dialog Data
	enum { IDD = IDD_RULES };
	BOOL			m_fUseRules[ 3 ][ 3 ];
	CString			m_szRules;
	int				m_nBlocks[ 3 ];
	CString			m_szBlocks;
	CString			m_szSelChar[ 3 ];
	CString			m_szSelChars;
	CBCGPComboBox	m_cboSelChar[ 3 ];
	CBCGPListBox	m_lbSelTrials[ 3 ];
	CStringList		m_listSelTrials[ 3 ];
	CString			m_szSelTrials;
	CBCGPListBox	m_lbResults;
	IProcSetting*	m_pPS;
	CStringList		m_listCond[ 2 ];
	int				m_nTrials;
	NSToolTipCtrl	m_tooltip;
	CMapStringToString	m_listChars[ 3 ];

// Overrides
protected:
	virtual void DoDataExchange( CDataExchange* pDX );
	virtual BOOL OnInitDialog();
	virtual void OnOK();

// Implementation:
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillData();
	void EnableFields();
	BOOL VerifyValues();
	BOOL ConstructParams();

	afx_msg void OnBnClickedChkRule();
	afx_msg void OnBnClickedBnTest();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

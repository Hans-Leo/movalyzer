#pragma once

#include "NSTooltipCtrl.h"

// WizardDevMapping dialog

class AFX_EXT_CLASS WizardDevMapping : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardDevMapping)

public:
	WizardDevMapping();
	~WizardDevMapping();

// Dialog Data
	enum { IDD = IDD_WIZ_DEV_MAPPING };
	BOOL	m_fRecWin;	// true for map to rec window, false for desktop
	UINT	m_nCurImg;
	NSToolTipCtrl	m_tooltip;

public:
	virtual BOOL OnWizardFinish();
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
protected:
	void ShowHideImages( UINT nID = -1 );
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedMapping();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnMouseMove(UINT, CPoint);

	DECLARE_MESSAGE_MAP()
};

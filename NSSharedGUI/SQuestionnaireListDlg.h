#pragma once

class SQuestGridCtrl;

// SQuestGridItem
class SQuestGridItem : public CBCGPGridItem
{
	DECLARE_DYNAMIC( SQuestGridItem )
	friend SQuestGridCtrl;
public:
	SQuestGridItem() {}
	virtual ~SQuestGridItem() {}
};

// SQuestGridROw
class SQuestGridRow : public CBCGPGridRow
{
	DECLARE_DYNAMIC( SQuestGridRow )
	friend SQuestGridCtrl;
public:
	SQuestGridRow() {}
	virtual ~SQuestGridRow() {}
};

// SQuestGridCtrl
class SQuestGridCtrl : public CBCGPGridCtrl
{
	DECLARE_DYNAMIC( SQuestGridCtrl )
	friend class SQuestGridItem;
	friend class SQuestGridRow;
public:
	SQuestGridCtrl() : fNotified( FALSE ) {}
	virtual ~SQuestGridCtrl() {}
protected:
	virtual void Sort( int nColumn, BOOL bAscending = TRUE, BOOL bAdd = FALSE ) {}
	virtual BOOL PreTranslateMessage( MSG* pMsg );
	virtual BOOL ValidateItemData( CBCGPGridRow* );
public:
	afx_msg void SQuestGridCtrl::OnLButtonDown(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
protected:
	BOOL fNotified;
};

// SQuestListDlg dialog

class AFX_EXT_CLASS SQuestListDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(SQuestListDlg)

public:
	SQuestListDlg( CStringList* pListH, CStringList* pListQ,
				   CStringList* pListA, CStringList* pListN,
				   CStringList* pListID, CWnd* pParent = NULL );
	virtual ~SQuestListDlg();

// Dialog Data
	enum { IDD = IDD_SQUEST_LIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual void OnOK();
	virtual void OnCancel();

// operations
public:
	void OK() { OnOK(); }
protected:
	void FillList();
public:
	BOOL			m_fInit;
	static BOOL		m_fTab;
	CStatic			m_wndGridLocation;
	SQuestGridCtrl	m_wndGrid;
	int				m_nRows;
	int				m_nTop;
	int				m_nLeft;
	int				m_nRightBuffer;
	int				m_nBottomBuffer;
	CStringList*	m_pListID;		// indices
	CStringList*	m_pListH;		// header/category
	CStringList*	m_pListQ;		// questions
	CStringList*	m_pListA;		// answers
	CStringList*	m_pListN;		// numeric

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage( MSG* );
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClickPaste();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

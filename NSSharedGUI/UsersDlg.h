#if !defined(AFX_USERSDLG_H__515F98F8_2603_4BF9_AD3B_02D175C6C499__INCLUDED_)
#define AFX_USERSDLG_H__515F98F8_2603_4BF9_AD3B_02D175C6C499__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UsersDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// UsersDlg dialog

interface INSMUser;

class UserGrid : public CBCGPReportCtrl
{
protected:
	afx_msg void OnLButtonDblClk( UINT nHitTest, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class AFX_EXT_CLASS UsersDlg : public CBCGPDialog
{
	friend class UserGrid;

// Construction
public:
	UsersDlg(CWnd* pParent = NULL);
	~UsersDlg();

// Dialog Data
	enum { IDD = IDD_USERS };
	UserGrid		m_grid;
	INSMUser*		m_pUser;
	NSToolTipCtrl	m_tooltip;
	CString			m_szCurUser;
	BOOL			m_fDelCurUser;
	CImageList		m_ImageList;
	BOOL			m_fVirgin;
	BOOL			m_fRefresh;
	BOOL			m_fUpdateUsers;
	CBCGPButton		m_bnAdd;
	CBCGPButton		m_bnDel;
	CBCGPButton		m_bnEdit;
	CBCGPButton		m_bnMove;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	virtual void OnCancel();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillList();
	BOOL CheckForOldList();
	virtual BOOL OnInitDialog();
	afx_msg void OnClickBnEdit();
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnDel();
	afx_msg void OnClickBnExit();
	afx_msg void OnClickBnMove();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_USERSDLG_H__515F98F8_2603_4BF9_AD3B_02D175C6C499__INCLUDED_)

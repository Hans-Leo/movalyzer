// SiteLoginDlg.h : header file

#pragma once

#include "..\NSShared\HyperLink.h"

class CInternetSession;

class AFX_EXT_CLASS SiteLoginDlg : public CBCGPDialog
{
// Construction
public:
	SiteLoginDlg( CWnd* pParent = NULL, CInternetSession* pIS = NULL );

// Dialog Data
	enum { IDD = IDD_LOGIN_SITE };
	CString		m_szUser;
	CString		m_szPass;
	CInternetSession*	m_pIS;
	CHyperLink	m_linkPass;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
protected:
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

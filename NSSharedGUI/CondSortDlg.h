// CondSortDlg.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

interface IProcSetting;

/////////////////////////////////////////////////////////////////////////////
// CondSortDlg dialog

class AFX_EXT_CLASS CondSortDlg : public CBCGPDialog
{
// Construction
public:
	CondSortDlg( IProcSetting* pPS = NULL, CWnd* pParent = NULL );
	~CondSortDlg();

// Dialog Data
	enum { IDD = IDD_SORT };
	CString			m_szSequence;
	BOOL			m_fMissing;
	CStringList		m_lstSortedID;
	CStringList		m_lstSortedDesc;
	CBCGPButton		m_bnUp;
	CBCGPButton		m_bnDown;
	IProcSetting*	m_pPS;
	CBCGPReportCtrl	m_grid;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void InsertColumns();
	void FillList();
	void OnMoveItem( int nDir );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnClickBnUp();
	afx_msg void OnClickBnDown();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// ProcSetPageCon.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageCon dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageCon : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageCon)

// Construction
public:
	ProcSetPageCon( IProcSetting* pPS = NULL );
	~ProcSetPageCon();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_CON };
	BOOL			m_fA;
	CColoredButton	m_chkA;
	BOOL			m_fC;
	CColoredButton	m_chkC;
	BOOL			m_fM;
	CColoredButton	m_chkM;
	BOOL			m_fN;
	CColoredButton	m_chkN;
	BOOL			m_fS;
	CColoredButton	m_chkS;
	BOOL			m_fU;
	CColoredButton	m_chkU;
	BOOL			m_fD;
	CColoredButton	m_chkD;
	BOOL			m_fDLTM;
	CColoredButton	m_chkDLTM;
	BOOL			m_fDGTM;
	CColoredButton	m_chkDGTM;
	double			m_dMINRT;
	CColoredEdit	m_editMINRT;
	double			m_dMAXRT;
	CColoredEdit	m_editMAXRT;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_linkAdv;
	CBCGPButton		m_linkDis;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnBnClickedChkDltm();
	afx_msg void OnBnClickedChkDgtm();
	afx_msg void OnClickAdvanced();
	afx_msg void OnClickDis();
	DECLARE_MESSAGE_MAP()
};

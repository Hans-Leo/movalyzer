// MoveUserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "MoveUserDlg.h"
#include "shlobj.h" // For SHFunctions
#include "direct.h"
#include "errno.h"
#include "..\DataMod\DataMod.h"
#include "..\Common\FileFinder.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// globals for data move thread
BOOL _TerminateThreadMove = FALSE;
BOOL _ThreadMoveRunning = FALSE;


// MoveUserDlg dialog
IMPLEMENT_DYNAMIC(MoveUserDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(MoveUserDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnBrowse)
	ON_BN_CLICKED(IDC_BN_DEFAULT, OnBnDefault)
	ON_BN_CLICKED(IDC_CHK_PRIVATE, OnClickPrivate)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

MoveUserDlg::MoveUserDlg( CWnd* pParent )
	: CBCGPDialog( MoveUserDlg::IDD, pParent )
{
	m_fBackup = FALSE;
	m_fPrivate = FALSE;
	m_fDoMove = FALSE;
}

MoveUserDlg::~MoveUserDlg()
{
}

void MoveUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szPath);
	DDV_MaxChars(pDX, m_szPath, _MAX_PATH - 1);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnBrowse);
	DDX_Check(pDX, IDC_CHK_PRIVATE, m_fPrivate);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
}

// MoveUserDlg message handlers

BOOL MoveUserDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	m_bnBrowse.SetImage( IDB_OPEN );
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the root location for the application-generated files.") );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Browse for the root location.") );
	pWnd = GetDlgItem( IDC_CHK_PRIVATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Private MovAlyzeR users are hidden from and not available to other Windows users.") );
	pWnd = GetDlgItem( IDC_BN_DEFAULT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the destination data path to the recommended location.") );

	// Default location to move
	OnClickPrivate();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL MoveUserDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void MoveUserDlg::OnBnBrowse()
{
	BROWSEINFO bi;
	memset( (LPVOID)&bi, 0, sizeof( bi ) );
	TCHAR szDisplayName[ _MAX_PATH ];
	szDisplayName[ 0 ] = '\0';
	bi.hwndOwner = GetSafeHwnd();
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szDisplayName;
	bi.lpszTitle = _T("Select a folder:");
	bi.ulFlags = BIF_RETURNONLYFSDIRS;

	LPITEMIDLIST pIIL = ::SHBrowseForFolder( &bi );

	TCHAR szInitialDir[ _MAX_PATH ];
	BOOL bRet = ::SHGetPathFromIDList( pIIL, (char*)&szInitialDir );
	if( bRet )
	{
		if( szInitialDir != _T("") ) m_szPath = szInitialDir;

		LPMALLOC pMalloc;
		HRESULT hr = SHGetMalloc( &pMalloc );
		pMalloc->Free( pIIL );
		pMalloc->Release();

		UpdateData( FALSE );
	}
}

void MoveUserDlg::OnBnDefault()
{
	CString szDefault;
	::GetDataPath( szDefault, m_fPrivate );
	if( szDefault[ szDefault.GetLength() - 1 ] != '\\' ) szDefault += _T("\\");
	szDefault += m_szID;

	m_szPath = szDefault;
	UpdateData( FALSE );
}

BOOL MoveUserDlg::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL MoveUserDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newuser.html"), this );
	return TRUE;
}

void MoveUserDlg::OnClickPrivate()
{
	UpdateData( TRUE );

	::GetDataPath( m_szPath, m_fPrivate );
	if( m_szPath[ m_szPath.GetLength() - 1 ] != '\\' ) m_szPath += _T("\\");
	m_szPath += m_szID;
	if( m_fBackup ) m_szPath += _T("\\Backup");

	UpdateData( FALSE );
}

void MoveUserDlg::OnCancel()
{
	if( _ThreadMoveRunning )
	{
		_TerminateThreadMove = TRUE;
		Reset();
	}
	else CBCGPDialog::OnCancel();
}

void MoveUserDlg::OnOK()
{
	UpdateData( TRUE );

	// Root data path
	if( m_szPath == _T("") )
	{
		BCGPMessageBox( _T("A data path must be specified.") );
		return;
	}
	if( m_szPath == m_szPathOld )
	{
		BCGPMessageBox( _T("The data path has not changed from its original setting.") );
		return;
	}
	CString szTest = m_szPath;
	szTest.MakeUpper();
	int nPos = szTest.Find( _T("\\CON\\") );
	if( nPos == -1 ) szTest.Find( _T("\\CON") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Data path cannot contain \'CON\'.") );
		return;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos == -1 ) szTest.Find( _T("\\NUL") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Data path cannot contain \'NUL\'.") );
		return;
	}
	nPos = m_szPath.ReverseFind( '\\' );
	while( nPos == ( m_szPath.GetLength() - 1 ) )
	{
		m_szPath = m_szPath.Left( m_szPath.GetLength() - 1 );
		nPos = m_szPath.ReverseFind( '\\' );
	}
	if( m_szPath[ m_szPath.GetLength() - 1 ] == '\\' )
		m_szPath = m_szPath.Left( m_szPath.GetLength() - 1 );
	CFileFind ff;

	// check against suggested
	CString szDefault;
	::GetDataPath( szDefault, m_fPrivate );
	if( szDefault[ szDefault.GetLength() - 1 ] != '\\' ) szDefault += _T("\\");
	szDefault += m_szID;
	if( m_fBackup ) szDefault += _T("\\Backup");
	CString szDir1 = m_szPath, szDir2 = szDefault;
	szDir1.MakeUpper();
	szDir2.MakeUpper();
	if( szDir1 != szDir2 )
	{
		CString szMsg = _T("The specified path does not match the recommended path.\nYou can still use this path, but you will be warned each time.\n\nContinue?");
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return;
	}
	// create directory if it doesnt exist (if does, see below else)
	if( !ff.FindFile( m_szPath ) )
	{
		int nRes, nPos = 0;
		CString szTmp;

		if( m_szPath[ m_szPath.GetLength() - 1 ] != '\\' ) m_szPath += _T("\\");
		nPos = m_szPath.Find( _T("\\") );
		nPos = m_szPath.Find( _T("\\"), nPos + 1 );
		while( nPos != -1 )
		{
			szTmp = m_szPath.Left( nPos );
			if( !ff.FindFile( szTmp ) )
			{
				nRes = _mkdir( szTmp );
				if( ( nRes != 0 ) && ( errno != EEXIST ) )
				{
					BCGPMessageBox( _T("Error creating data path directory.") );
					return;
				}
			}

			nPos = m_szPath.Find( _T("\\"), nPos + 1 );
		}
	}
	// if exists, we dont want to overwrite unless user says so
	else
	{
		if( BCGPMessageBox( _T("WARNING: This path already exists. If you continue, existing data will be overwritten.\n\nContinue?"), MB_YESNO ) == IDNO )
			return;
	}
	// test for writeability
	CStdioFile f;
	szTest = m_szPath;
	szTest += _T("\\test.txt");
	if( !f.Open( szTest, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("You do not have the permission to create files in the specified root path.") );
		return;
	}
	else
	{
		f.Close();
		CFile::Remove( szTest );
	}


	// get count for progress meter, reset count, and do again to show progress & get total bytes
	Start();
	m_fDoMove = FALSE;
	// file finder settings
	CFileFinder::CFindOpts opts;
	opts.sBaseFolder = m_szPathOld;
	opts.sFileMask = "*.*";
	opts.bSubfolders = TRUE;
	opts.FindNormalFiles();
	// count of files found
	CFileFinder finder;
	int nCount = finder.Find( opts );
	// set prog meter range
	m_progress.SetRange( 0, nCount );
	CWnd* pWnd = GetDlgItem( IDC_TXT_PROGRESS );
	pWnd->SetWindowText( _T("Scanning Files...") );
	// since we're researching, remove originally found files
	finder.RemoveAll();
	// initialze total # of bytes to 0
	m_bytes = 0;
	// callback function for file finder
	finder.SetCallback( FileFinderProc, this );
	// wait cursor
	CWaitCursor crs;
	// SEARCH for files
	finder.Find( opts );
	// cleanup & check
	Reset();
	if( _TerminateThreadMove ) return;
	// available disk space
	DWORD dwSec, dwByt, dwFree, dwClust;
	::GetDiskFreeSpace( m_szPath, &dwSec, &dwByt, &dwFree, &dwClust );
	__int64 lFree = dwFree * dwSec * dwByt;
	// inform user and confirm to continue
	CString szMsg;
	if( lFree < m_bytes )
	{
		szMsg.Format( _T("There are %d files to be moved with a total size of %I64d KB.\nYou do NOT have sufficient disk space available (%I64d KB).\n\nYou will need to choose another location."),
					  nCount, m_bytes / (__int64)1024, lFree / (__int64)1024 );
		BCGPMessageBox( szMsg );
		return;
	}
	szMsg.Format( _T("There are %d files to be moved with a total size of %I64d KB.\nYou have %I64d KB of available disk space in the new location.\n\nPrepared to move user data. No original files will be deleted.\n\nContinue?"),
				  nCount, m_bytes / (__int64)1024, lFree / (__int64)1024 );
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return;


// move data (we already have the # of files)
	Start();
	m_fDoMove = TRUE;
	// set prog meter range
	m_progress.SetRange( 0, nCount );
	// wait cursor
	crs.Restore();
	// Do the search & copy (m_fDoMove is the flag that says to copy)
	CFileFinder finder2;
	finder2.SetCallback( FileFinderProc, this );
	finder2.Find( opts );
	if( _TerminateThreadMove ) return;

	// reset
	Reset();

	// update database
	INSMUser* pUser = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
									 IID_INSMUser, (LPVOID*)&pUser );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create user object.") );
		return;
	}
	hr = pUser->Find( m_szID.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to locate user.") );
		pUser->Release();
		return;
	}
	if( !m_fBackup ) pUser->put_RootPath( m_szPath.AllocSysString() );
	else pUser->put_BackupPath( m_szPath.AllocSysString() );
	hr = pUser->Modify();
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to update user.") );
		pUser->Release();
		return;
	}
	pUser->Release();

	// complete, inform user, close
	if( m_fBackup ) szMsg = _T("Backup data successfully copied.");
	else szMsg = _T("User data sucessfully copied.");
	BCGPMessageBox( szMsg );
	EndDialog( IDOK );
}

void MoveUserDlg::FileFinderProc( CFileFinder *pFinder, DWORD dwCode, void *pCustomParam )
{
	CString szFile;
	MoveUserDlg	*pDlg = (MoveUserDlg*)pCustomParam;

	// check to see if we have canceled
	if( _TerminateThreadMove )
	{
		pFinder->StopSearch();
		return;
	}

	switch( dwCode )
	{
		case FF_FOUND:
		{
			// current item
			int nCount = pFinder->GetFileCount();
			szFile = pFinder->GetFilePath( nCount - 1 );

			// We're only getting file count, names & sizes -- not moving
			if( !pDlg->m_fDoMove )
			{
				// get file size and increment total
				CPath path( szFile );
				__int64 bytes = 0;
				path.GetFileSize( bytes );
				pDlg->m_bytes += bytes;
			}
			// moving
			else
			{
				// source file (path object)
				CPath pathSrc( szFile );
				// relational path from experiment root
				CString szRelPath = pathSrc.GetRelativePath( pDlg->m_szPathOld );
				// nix the . at the start if exists
				if( szRelPath.GetLength() > 0 )
				{
					if( szRelPath[ 0 ] == '.' ) szRelPath = szRelPath.Mid( 1 );
				}
				// destination path
				CString szDestPath = pDlg->m_szPath;
				szDestPath += szRelPath;
				// dest file (path object)
				CPath pathDest( szDestPath );
				// verify dest directory exists
				CString szVerify = pathDest.GetDrive();
				szVerify += pathDest.GetDir();
				if( ::CreateDirectory( szVerify ) )
				{
					// copy file
					BOOL fRet = CopyFile( szFile, szDestPath, FALSE );
				}
			}
			// update progress
			CProgressCtrl* pProgress = (CProgressCtrl*)pDlg->GetDlgItem( IDC_PROGRESS1 );
			pProgress->SetPos( nCount );
			if( pDlg->m_fDoMove )
			{
				CWnd* pTxt = pDlg->GetDlgItem( IDC_TXT_PROGRESS );
				pTxt->SetWindowText( szFile );
			}
		}
	}

	// Process all process messages
	MSG msg;
	while( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
	{
		TranslateMessage( &msg );
		DispatchMessage( &msg );
	}
}

void MoveUserDlg::Start()
{
	Reset();

	_ThreadMoveRunning = TRUE;
	_TerminateThreadMove = FALSE;
	CWnd* pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) pWnd->SetWindowText( _T("STOP") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->EnableWindow( FALSE );

	m_progress.SetRange( 0, 100 );
}

void MoveUserDlg::Reset()
{
	_ThreadMoveRunning = FALSE;
	CWnd* pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) pWnd->SetWindowText( _T("Cancel") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->EnableWindow( TRUE );
	m_progress.SetPos( 0 );
	pWnd = GetDlgItem( IDC_TXT_PROGRESS );
	pWnd->SetWindowText( _T("") );
}

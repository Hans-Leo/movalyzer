#pragma once

// PrefPageGeneral.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// PrefPageGeneral dialog

class Preferences;

class AFX_EXT_CLASS PrefPageGeneral : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(PrefPageGeneral)

// Construction
public:
	PrefPageGeneral( Preferences* pPref = NULL );
	~PrefPageGeneral();

// Dialog Data
	enum { IDD = IDD_PAGE_PREF_GENERAL };
	BOOL			m_fLog;
	BOOL			m_fAlpha;
	CString			m_szDelim;
	CString			m_szPath;
	CBCGPEdit		m_wndFolderPath;
	CString			m_szBackup;
	CBCGPEdit		m_wndFolderBackup;
	BOOL			m_fAutoUpdate;
	CBCGPButton		m_bnBrowse;
	CBCGPButton		m_bnBrowse2;
	BOOL			m_fPrivate;
	BOOL			m_fMovaGen;
	CString			m_szStart;
	CString			m_szEnd;
	CString			m_szCur;
	BOOL			m_fAutoShow;		// RX-only
	BOOL			m_fShowSubjects;	// Rx-only
	CBCGPButton		m_bnReset;
	Preferences*	m_pPref;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnBrowse();
	afx_msg void OnBnBrowse2();
	afx_msg void OnClickPrivate();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickReset();
	afx_msg void OnClickGenID();
	afx_msg void OnKillfocusIDStart();
	DECLARE_MESSAGE_MAP()

};

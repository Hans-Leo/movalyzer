#pragma once

// ProcSetPageNormDB.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRESumm dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageNormDB : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageNormDB)

	// Construction
public:
	ProcSetPageNormDB( IProcSetting* pS = NULL );
	~ProcSetPageNormDB();

	// Dialog Data
	enum { IDD = IDD_PAGE_PS_NORMDB };
	BOOL			m_fScore;
	CColoredButton	m_chkScore;
	BOOL			m_fScoreGlobal;
	CColoredButton	m_chkScoreGlobal;
	BOOL			m_fNoUpdateDB;
	CColoredButton	m_chkNoUpdateDB;
	double			m_dThreshold;
	CColoredEdit	m_editThreshold;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

	// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void SetFieldStates();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

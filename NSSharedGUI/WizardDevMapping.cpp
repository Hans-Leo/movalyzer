// WizardDevMapping.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardDevMapping.h"
#include "WizardDevSheet.h"
#include "WizardDevDevices.h"
#include "WizardDevConfDev.h"
#include "WizardDevMonConf.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// WizardDevMapping dialog

IMPLEMENT_DYNCREATE(WizardDevMapping, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardDevMapping, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_RDO_DESKTOP, OnBnClickedMapping)
	ON_BN_CLICKED(IDC_RDO_TARGET, OnBnClickedMapping)
	ON_WM_HELPINFO()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

WizardDevMapping::WizardDevMapping() : CBCGPPropertyPage(WizardDevMapping::IDD)
{
	m_fRecWin = FALSE;
	m_nCurImg = -1;
}

WizardDevMapping::~WizardDevMapping()
{
}

void WizardDevMapping::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

// WizardDevMapping message handlers

BOOL WizardDevMapping::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_DEV_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_RDO_DESKTOP );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PPI_DESKTOP, _T("Desktop") );
	pWnd= GetDlgItem( IDC_RDO_TARGET );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PPI_TARGET, _T("Recording Window") );

	CheckRadioButton( IDC_RDO_DESKTOP, IDC_RDO_TARGET, m_fRecWin ? IDC_RDO_TARGET : IDC_RDO_DESKTOP );

	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	WizardDevDevices* pDev = (WizardDevDevices*)pParent->GetPage( 2 );
	if( !::IsTabletInstalled() || ( pDev->m_nDevice != IDC_RDO_TABLET ) )
	{
		m_fRecWin = FALSE;
		CWnd* pWnd = GetDlgItem( IDC_RDO_TARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		CheckRadioButton( IDC_RDO_DESKTOP, IDC_RDO_TARGET, IDC_RDO_DESKTOP );
	}
	else
	{
		m_fRecWin = pParent->m_fRecWin;
		CheckRadioButton( IDC_RDO_DESKTOP, IDC_RDO_TARGET, m_fRecWin ? IDC_RDO_TARGET : IDC_RDO_DESKTOP );
	}

	// show/hide images
	ShowHideImages();

	// Highlight the main sentence
	CStatic* pDis = (CStatic*)GetDlgItem( IDC_TXT_HELP );
	CDC* pDC = GetDC();
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_BOLD;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	pDis->SetFont( &font, TRUE );
	pDC->SelectObject( pOldFont );
	ReleaseDC( pDC );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void WizardDevMapping::ShowHideImages( UINT nID )
{
	CWnd* pWnd = GetDlgItem( IDB_DESKTOP );
	if( pWnd ) pWnd->ShowWindow( ( nID != -1 ) ? ( nID == IDC_RDO_DESKTOP ) : !m_fRecWin );
	pWnd = GetDlgItem( IDB_WINDOW );
	if( pWnd ) pWnd->ShowWindow( ( nID != -1 ) ? ( nID == IDC_RDO_TARGET ) : m_fRecWin );

	if( nID != -1 ) m_nCurImg = nID;
	else m_nCurImg = m_fRecWin ? IDC_RDO_TARGET : IDC_RDO_DESKTOP;
}

void WizardDevMapping::OnMouseMove( UINT nFlags, CPoint pt )
{
	CRect rectDT, rectWnd;
	CWnd* pWnd = GetDlgItem( IDC_RDO_DESKTOP );
	pWnd->GetWindowRect( rectDT );
	ScreenToClient( rectDT );
	rectDT.InflateRect( 2, 2, 2, 2 );
	pWnd = GetDlgItem( IDC_RDO_TARGET );
	pWnd->GetWindowRect( rectWnd );
	ScreenToClient( rectWnd );
	rectWnd.InflateRect( 2, 2, 2, 2 );

	if( ( pt.x >= rectDT.left ) && ( pt.x <= rectDT.right ) &&
		( pt.y >= rectDT.top ) && ( pt.y <= rectDT.bottom ) )
	{
		if( m_nCurImg != IDC_RDO_DESKTOP ) ShowHideImages( IDC_RDO_DESKTOP );
	}
	else if( ( pt.x >= rectWnd.left ) && ( pt.x <= rectWnd.right ) &&
		( pt.y >= rectWnd.top ) && ( pt.y <= rectWnd.bottom ) )
	{
		if( m_nCurImg != IDC_RDO_TARGET ) ShowHideImages( IDC_RDO_TARGET );
	}
	else
	{
		UINT nID = m_fRecWin ? IDC_RDO_TARGET : IDC_RDO_DESKTOP;
		if( m_nCurImg != nID ) ShowHideImages();
	}
}

BOOL WizardDevMapping::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}

void WizardDevMapping::OnBnClickedMapping()
{
	if( GetCheckedRadioButton( IDC_RDO_DESKTOP, IDC_RDO_TARGET ) == IDC_RDO_TARGET ) m_fRecWin = TRUE;
	else m_fRecWin = FALSE;
	ShowHideImages();
}

BOOL WizardDevMapping::OnWizardFinish() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	WizardDevDevices* pDev = (WizardDevDevices*)pParent->GetPage( 2 );
	WizardDevConfDev* pDevConf = (WizardDevConfDev*)pParent->GetPage( 3 );
	WizardDevMonConf* pMonConf = (WizardDevMonConf*)pParent->GetPage( 1 );

	pParent->m_nDevice = pDev->m_nDevice;
	pParent->m_nRate = pDevConf->m_nRate;
	pParent->m_nPressure = pDevConf->m_nPressure;
	pParent->m_dResolution = pDevConf->m_dResolution;
	pParent->m_dHeightTablet = pDevConf->m_dHeightTablet;
	pParent->m_dWidthTablet = pDevConf->m_dWidthTablet;
	pParent->m_dHeightDisplay = pMonConf->m_dHeightDisplay;
	pParent->m_dWidthDisplay = pMonConf->m_dWidthDisplay;
	pParent->m_fRecWin = m_fRecWin;

	return CBCGPPropertyPage::OnWizardFinish();
}

BOOL WizardDevMapping::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("devicesetup.html"), this );
	return TRUE;
}

BOOL WizardDevMapping::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

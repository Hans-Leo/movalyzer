// ProcSetPageExt.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageExt.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageExt property page

IMPLEMENT_DYNCREATE(ProcSetPageExt, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageExt, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageExt::ProcSetPageExt( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageExt::IDD), m_pPS( pPS )
{
	m_f3 = FALSE;
	m_fO = FALSE;
	m_f2 = TRUE;

	m_chkO.SetDefaultAsOff( !m_fO );
	m_chk2.SetDefaultAsOff( !m_f2 );

	if( m_pPS )
	{
		IProcSetFlags* pPSF = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_EXT_3( &m_f3 );
			pPSF->get_EXT_O( &m_fO );
			pPSF->get_EXT_2( &m_f2 );

			pPSF->Release();
		}
	}
}

ProcSetPageExt::~ProcSetPageExt()
{
}

void ProcSetPageExt::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_EXT3, m_f3);
	DDX_Check(pDX, IDC_CHK_EXTO, m_fO);
	DDX_Check(pDX, IDC_CHK_EXT2, m_f2);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);

	DDX_Control(pDX, IDC_CHK_EXTO, m_chkO);
	DDX_Control(pDX, IDC_CHK_EXT2, m_chk2);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageExt message handlers

BOOL ProcSetPageExt::OnApply() 
{
	if( !m_pPS ) return FALSE;

	IProcSetFlags* pPSF = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( SUCCEEDED( hr ) )
	{
		pPSF->put_EXT_3( m_f3 );
		pPSF->put_EXT_O( m_fO );
		pPSF->put_EXT_2( m_f2 );

		pPSF->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageExt::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageExt::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	BOOL f3 = FALSE, fO = FALSE, f2 = FALSE;
	if( pPS )
	{
		IProcSetFlags* pPSF = NULL;
		HRESULT hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_EXT_3( &f3 );
			pPSF->get_EXT_O( &fO );
			pPSF->get_EXT_2( &f2 );

			pPSF->Release();
		}
		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_CHK_EXT3 );
	szData.LoadString( IDS_PSPE_3 );
	szTmp.Format( _T(" [DEFAULT=%s]"), f3 ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData );

	pWnd = GetDlgItem( IDC_CHK_EXTO );
	szData.LoadString( IDS_PSPE_ONE );
	szTmp.Format( _T(" [DEFAULT=%s]"), fO ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Onestroke Analysis") );

	pWnd = GetDlgItem( IDC_CHK_EXT2 );
	szData.LoadString( IDS_PSPE_2 );
	szTmp.Format( _T(" [DEFAULT=%s]"), f2 ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Every 2 Strokes") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageExt::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageExt::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_extraction.html"), this );
	return TRUE;
}

void ProcSetPageExt::OnBnReset() 
{
	m_f3 = FALSE;
	m_fO = FALSE;
	m_f2 = FALSE;

	UpdateData( FALSE );
}

void ProcSetPageExt::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 17 );
}

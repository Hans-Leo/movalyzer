#pragma once

#include "NSTooltipCtrl.h"

// WizardExportExp dialog

class AFX_EXT_CLASS WizardExportExp : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardExportExp)

public:
	WizardExportExp( CString szExpID = _T(""), CString szSubjID = _T(""), CString szGrpID = _T("") );
	virtual ~WizardExportExp();

// Dialog Data
	enum { IDD = IDD_WIZ_EXPORT_SUBJECTS };
	CBCGPComboBox	m_Cbo;
	CBCGPListCtrl	m_list;
	CImageList		m_ImageList;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fHideExported;
	CString			m_szExpID;
	CString			m_szSubjID;
	CString			m_szGrpID;
	CStringList		m_lstSubjects;
	int				m_nCurCol;
	int				m_nLastCol;
	BOOL			m_fAscending;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnWizardFinish();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );

protected:
	static int CALLBACK CompareFunc( LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort );
	void FillList();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnCbnSelchangeCboExps();
	afx_msg void OnBnClickedChkHide();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCancel();
	afx_msg void OnOK();
	afx_msg void OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult);
};

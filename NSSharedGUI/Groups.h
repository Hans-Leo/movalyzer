#ifndef _GROUPS_H_
#define	_GROUPS_H_

#include "Objects.h"

#define	TAG_GROUP	_T("[GROUP]")

interface IGroup;

// This class is intended to encapsulate all view & functionality for a group
class AFX_EXT_CLASS Groups : public Objects
{
	PUT_BASE( Group )

	// Attributes
public:
	CString	m_szExpID;

	// Operations
public:
	virtual ULONGLONG GetNumRecords();
	// Interface methods
	PUT_GET( Group, ID, CString, val.AllocSysString() )

	PUT_GET( Group, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( IGroup*, CString&, BOOL fReverse = FALSE );

	PUT_GET( Group, Notes, CString, val.AllocSysString() )

	// Importing/Exporting
	BOOL Export( CMemFile* pFile );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );

	// duplication (includes questionnaire)
	BOOL DoEditDup( CString szID, CString szExpID, CWnd* pOwner = NULL );
	BOOL Duplicate( CString szNewID, CString szExpID, CWnd* pOwner = NULL );

	// questionnaire
	static BOOL Questionnaire( CString szExp, CString szGrp );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _GROUPS_H_

// ProcSetPageRESumm.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageRESumm.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRESumm property page

IMPLEMENT_DYNCREATE(ProcSetPageRESumm, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageRESumm, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_SUMM, OnChkSumm)
	ON_BN_CLICKED(IDC_CHK_SELECT, OnChkSelect)
	ON_BN_CLICKED(IDC_CHK_ONLY, OnChkOnly)
	ON_BN_CLICKED(IDC_CHK_ONLY_GRP, OnChkOnlyGrp)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageRESumm::ProcSetPageRESumm( IProcSetting* pS )
	: CBCGPPropertyPage(ProcSetPageRESumm::IDD), m_pPS( pS )
{
	m_fSumm = TRUE;
	m_fOnly = TRUE;
	m_fOnlyGrp = TRUE;
	m_fNormDB = TRUE;
	m_fSelect = FALSE;
	m_fAnalysis = TRUE;

	m_chkSumm.SetDefaultAsOff( !m_fSumm );
	m_chkOnly.SetDefaultAsOff( !m_fOnly );
	m_chkOnlyGrp.SetDefaultAsOff( !m_fOnlyGrp );
	m_chkNormDB.SetDefaultAsOff( !m_fNormDB );
	m_chkSelect.SetDefaultAsOff( !m_fSelect );
	m_chkAnalysis.SetDefaultAsOff( !m_fAnalysis );

	if( m_pPS )
	{
		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			pPSRE->get_Summarize( &m_fSumm );
			pPSRE->get_SummarizeOnly( &m_fOnly );
			pPSRE->get_SummarizeOnlyGroup( &m_fOnlyGrp );
			pPSRE->get_SummarizeNormDB( &m_fNormDB );
			pPSRE->get_SummarizeSelect( &m_fSelect );
			pPSRE->get_Analyze( &m_fAnalysis );

			pPSRE->Release();
		}
	}
}

ProcSetPageRESumm::~ProcSetPageRESumm()
{
}

void ProcSetPageRESumm::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_SUMM, m_fSumm);
	DDX_Check(pDX, IDC_CHK_ONLY, m_fOnly);
	DDX_Check(pDX, IDC_CHK_ONLY_GRP, m_fOnlyGrp);
	DDX_Check(pDX, IDC_CHK_NORMDB, m_fNormDB);
	DDX_Check(pDX, IDC_CHK_SELECT, m_fSelect);
	DDX_Check(pDX, IDC_CHK_ANALYSIS, m_fAnalysis);

	DDX_Control(pDX, IDC_CHK_SUMM, m_chkSumm);
	DDX_Control(pDX, IDC_CHK_ONLY, m_chkOnly);
	DDX_Control(pDX, IDC_CHK_ONLY_GRP, m_chkOnlyGrp);
	DDX_Control(pDX, IDC_CHK_NORMDB, m_chkNormDB);
	DDX_Control(pDX, IDC_CHK_SELECT, m_chkSelect);
	DDX_Control(pDX, IDC_CHK_ANALYSIS, m_chkAnalysis);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRESumm message handlers

BOOL ProcSetPageRESumm::OnApply() 
{
	if( !m_pPS ) return FALSE;

	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->put_Summarize( m_fSumm );
		pPSRE->put_SummarizeOnly( m_fOnly );
		pPSRE->put_SummarizeOnlyGroup( m_fOnlyGrp );
		pPSRE->put_SummarizeNormDB( m_fNormDB );
		pPSRE->put_SummarizeSelect( m_fSelect );
		pPSRE->put_Analyze( m_fAnalysis );

		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageRESumm::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageRESumm::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_CHK_SUMM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Summarize experiment after administering experiment."), _T("Summarize") );
	pWnd = GetDlgItem( IDC_CHK_SELECT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Provide option to select which users to include/exclude in summarization."), _T("Inclusion/Exclusion") );
	pWnd = GetDlgItem( IDC_CHK_ONLY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Summarize only this subject for all groups."), _T("Subject Only") );
	pWnd = GetDlgItem( IDC_CHK_ONLY_GRP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Summarize only this subject and group."), _T("Subject and Group Only") );
	pWnd = GetDlgItem( IDC_CHK_NORMDB );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create and update the historical averages database for each subject and an overall subject."), _T("Update Historical Averages Database") );
	pWnd = GetDlgItem( IDC_CHK_ANALYSIS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View individual trial statistics with averages + std. devs after experiment."), _T("Analysis") );

	SetFieldStates();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageRESumm::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageRESumm::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_runexperiment_summanalysis.html"), this );
	return TRUE;
}

void ProcSetPageRESumm::OnChkSumm() 
{
	UpdateData( TRUE );

	if( !m_fSumm ) {
		m_fAnalysis = FALSE;
		UpdateData( FALSE );
	}

	SetFieldStates();
}

void ProcSetPageRESumm::SetFieldStates()
{
	UpdateData( TRUE );

	if( m_fSelect && m_fOnly ) {
		m_fOnly = FALSE;
		UpdateData( FALSE );
	}

	CWnd* pWnd = GetDlgItem( IDC_CHK_SELECT );
	if( pWnd ) pWnd->EnableWindow( m_fSumm );
	pWnd = GetDlgItem( IDC_CHK_ONLY );
	if( pWnd ) pWnd->EnableWindow( m_fSumm );
	pWnd = GetDlgItem( IDC_CHK_ONLY_GRP );
	if( pWnd ) pWnd->EnableWindow( m_fSumm && m_fOnly );
	pWnd = GetDlgItem( IDC_CHK_NORMDB );
	if( pWnd ) pWnd->EnableWindow( m_fSumm && m_fOnly && m_fOnlyGrp );
	pWnd = GetDlgItem( IDC_CHK_ANALYSIS );
	if( pWnd ) pWnd->EnableWindow( m_fSumm );
}

void ProcSetPageRESumm::OnChkSelect() 
{
	UpdateData( TRUE );

	if( m_fSelect && m_fOnly ) {
		m_fOnly = FALSE;
		UpdateData( FALSE );
	}

	SetFieldStates();
}

void ProcSetPageRESumm::OnChkOnly() 
{
	UpdateData( TRUE );

	if( m_fOnly && m_fSelect ) {
		m_fSelect = FALSE;
		UpdateData( FALSE );
	}

	SetFieldStates();
}

void ProcSetPageRESumm::OnChkOnlyGrp()
{
	SetFieldStates();
}

void ProcSetPageRESumm::OnBnReset() 
{
	m_fSumm = TRUE;
	m_fOnly = TRUE;
	m_fOnlyGrp = TRUE;
	m_fNormDB = TRUE;
	m_fSelect = FALSE;
	m_fAnalysis = TRUE;

	UpdateData( FALSE );

	SetFieldStates();
}

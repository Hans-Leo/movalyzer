// ProcSetPageREView.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageREView.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREView property page

IMPLEMENT_DYNCREATE(ProcSetPageREView, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageREView, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageREView::ProcSetPageREView( IProcSetting* pS )
	: CBCGPPropertyPage(ProcSetPageREView::IDD), m_pPS( pS )
{
	m_fExt = FALSE;
	m_fCon = FALSE;
	m_fCon2 = FALSE;
	m_fExt2 = FALSE;

	m_chkExt.SetDefaultAsOff( !m_fExt );
	m_chkExt2.SetDefaultAsOff( !m_fExt2 );
	m_chkCon.SetDefaultAsOff( !m_fCon );
	m_chkCon2.SetDefaultAsOff( !m_fCon2 );

	if( m_pPS )
	{
		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			pPSRE->get_ViewSubjExt( &m_fExt );
			pPSRE->get_ViewSubjCon( &m_fCon );
			pPSRE->get_ViewExpExt( &m_fExt2 );
			pPSRE->get_ViewExpCon( &m_fCon2 );

			pPSRE->Release();
		}
	}
}

ProcSetPageREView::~ProcSetPageREView()
{
}

void ProcSetPageREView::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_EXT, m_fExt);
	DDX_Check(pDX, IDC_CHK_CONSIS, m_fCon);
	DDX_Check(pDX, IDC_CHK_CONSIS2, m_fCon2);
	DDX_Check(pDX, IDC_CHK_EXT2, m_fExt2);

	DDX_Control(pDX, IDC_CHK_EXT, m_chkExt);
	DDX_Control(pDX, IDC_CHK_EXT2, m_chkExt2);
	DDX_Control(pDX, IDC_CHK_CONSIS, m_chkCon);
	DDX_Control(pDX, IDC_CHK_CONSIS2, m_chkCon2);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREView message handlers

BOOL ProcSetPageREView::OnApply() 
{
	if( !m_pPS ) return FALSE;

	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->put_ViewSubjExt( m_fExt );
		pPSRE->put_ViewSubjCon( m_fCon );
		pPSRE->put_ViewExpExt( m_fExt2 );
		pPSRE->put_ViewExpCon( m_fCon2 );

		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageREView::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageREView::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_CHK_EXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View subject's extracted data after experiment."), _T("Extracted Data") );
	pWnd = GetDlgItem( IDC_CHK_CONSIS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View subject's consistency errors after experiment."), _T("Consistency Errors") );
	pWnd = GetDlgItem( IDC_CHK_EXT2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View experiment's extracted data after summarization."), _T("Extracted Data") );
	pWnd = GetDlgItem( IDC_CHK_CONSIS2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View experiment's consistency errors after summarization."), _T("Consistency Errors") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageREView::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageREView::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_runexperiment_viewdata.html"), this );
	return TRUE;
}

void ProcSetPageREView::OnBnReset() 
{
	m_fExt = FALSE;
	m_fCon = FALSE;
	m_fCon2 = FALSE;
	m_fExt2 = FALSE;

	UpdateData( FALSE );
}

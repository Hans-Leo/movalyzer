// ProcSetPageGeneral.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageGeneral.h"
#include "ProcSetSheet.h"
#include "RandRulesDlg.h"
#include "CondSortDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageGeneral property page

IMPLEMENT_DYNCREATE(ProcSetPageGeneral, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageGeneral, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_QUEST, OnBnClickedChkQuest)
	ON_BN_CLICKED(IDC_CHK_OVERRIDE, OnChkSpecify)
	ON_BN_CLICKED(IDC_CHK_RANDOM, OnChkRandom)
	ON_BN_CLICKED(IDC_CHK_RANDOMR, OnChkRules)
	ON_BN_CLICKED(IDC_CHK_CONDINSTR, OnBnClickedChkCondinstr)
	ON_BN_CLICKED(IDC_RDO_TRIAL_ALL, OnBnClickedRdoTrialAll)
	ON_BN_CLICKED(IDC_CHK_ENTERKEY, OnBnClickedChkEnterKey)
	ON_BN_CLICKED(IDC_CHK_ACCEPTREDO, OnBnClickedChkAcceptredo)
	ON_BN_CLICKED(IDC_RDO_TRIAL_ONCE, OnBnClickedRdoTrialOnce)
	ON_BN_CLICKED(IDC_BN_SEL, OnBnClickedRules)
	ON_BN_CLICKED(IDC_BN_EDIT, OnBnClickedOrder)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageGeneral::ProcSetPageGeneral( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageGeneral::IDD), m_pPS( pPS )
{
	m_fQuest = FALSE;
	m_fEnd = FALSE;
	m_fSpecify = FALSE;
	m_fRandom = TRUE;
	m_fRandomT = TRUE;
	m_fRandomR = FALSE;
	m_fShowInstr = FALSE;
	m_fCondInstr = FALSE;
	m_fEnterKey = FALSE;
	m_fAcceptRedo = TRUE;
	m_nTrialMode = IDC_RDO_TRIAL_ONCE;
	m_nRedoMode = IDC_RDO_AD_ERR;

	m_chkQuest.SetDefaultAsOff( !m_fQuest );
	m_chkEnd.SetDefaultAsOff( !m_fEnd );
	m_chkSpecify.SetDefaultAsOff( !m_fSpecify );
	m_chkRandom.SetDefaultAsOff( !m_fRandom );
	m_chkRandomT.SetDefaultAsOff( !m_fRandomT );
	m_chkRandomR.SetDefaultAsOff( !m_fRandomR );
	m_chkShowInstr.SetDefaultAsOff( !m_fShowInstr );
	m_chkCondInstr.SetDefaultAsOff( !m_fCondInstr );
	m_chkEnterKey.SetDefaultAsOff( !m_fEnterKey );
	m_chkAcceptRedo.SetDefaultAsOff( !m_fAcceptRedo );
	m_rdoTrialFirst.SetDefaultRadio( IDC_RDO_TRIAL_ONCE );
	m_rdoTrialEach.SetDefaultRadio( IDC_RDO_TRIAL_ONCE );
	m_rdoRedoEach.SetDefaultRadio( IDC_RDO_AD_ERR );
	m_rdoRedoConsis.SetDefaultRadio( IDC_RDO_AD_ERR );
	m_rdoRedoAuto.SetDefaultRadio( IDC_RDO_AD_ERR );

	if( m_pPS )
	{
		m_pPS->get_SpecifySequence( &m_fSpecify );
		m_pPS->get_Randomize( &m_fRandom );
		m_pPS->get_RandomizeTrials( &m_fRandomT );
		m_pPS->get_RandWithRules( &m_fRandomR );
		m_pPS->get_InstructionAlwaysVisible( &m_fShowInstr );

		TrialsMode tm;
		m_pPS->get_TrialMode( &tm );
		switch( tm )
		{
			case eTM_None: 
				m_fAcceptRedo = FALSE;
				break;
			case eTM_Delay:
				m_fAcceptRedo = FALSE;
				m_fEnterKey = TRUE;
				break;
			case eTM_First:
				m_fAcceptRedo = FALSE;
				m_fCondInstr = TRUE;
				break;
			case eTM_FirstDelay:
				m_fAcceptRedo = FALSE;
				m_fCondInstr = TRUE;
				m_fEnterKey = TRUE;
				break;
			case eTM_All:
				m_fAcceptRedo = FALSE;
				m_fCondInstr = TRUE;
				m_nTrialMode = IDC_RDO_TRIAL_ALL;
				break;
			case eTM_NoneAD:
				m_fAcceptRedo = TRUE;
				break;
			case eTM_DelayAD:
				m_fEnterKey = TRUE;
				m_fAcceptRedo = TRUE;
				break;
			case eTM_FirstAD:
				m_fCondInstr = TRUE;
				m_fAcceptRedo = TRUE;
				break;
			case eTM_FirstDelayAD:
				m_fCondInstr = TRUE;
				m_fEnterKey = TRUE;
				m_fAcceptRedo = TRUE;
				break;
			case eTM_AllAD:
				m_fCondInstr = TRUE;
				m_nTrialMode = IDC_RDO_TRIAL_ALL;
				m_fAcceptRedo = TRUE;
				break;
			case eTM_NoneADErr:
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_DelayADErr:
				m_fEnterKey = TRUE;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_FirstADErr:
				m_fCondInstr = TRUE;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_FirstDelayADErr:
				m_fCondInstr = TRUE;
				m_fEnterKey = TRUE;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_AllADErr:
				m_fCondInstr = TRUE;
				m_nTrialMode = IDC_RDO_TRIAL_ALL;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_NoneErr:
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_DelayErr:
				m_fEnterKey = TRUE;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_FirstErr:
				m_fCondInstr = TRUE;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_FirstDelayErr:
				m_fCondInstr = TRUE;
				m_fEnterKey = TRUE;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_AllErr:
				m_fCondInstr = TRUE;
				m_nTrialMode = IDC_RDO_TRIAL_ALL;
				m_fAcceptRedo = TRUE;
				m_nRedoMode = IDC_RDO_AD_REDO;
				break;
		}

		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			pPSRE->get_DoQuestionnaire( &m_fQuest );
			pPSRE->get_QuestAtEnd( &m_fEnd );
			pPSRE->Release();
		}
	}
}

ProcSetPageGeneral::~ProcSetPageGeneral()
{
}

void ProcSetPageGeneral::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_QUEST, m_fQuest);
	DDX_Check(pDX, IDC_CHK_END, m_fEnd);
	DDX_Check(pDX, IDC_CHK_OVERRIDE, m_fSpecify);
	DDX_Check(pDX, IDC_CHK_RANDOM, m_fRandom);
	DDX_Check(pDX, IDC_CHK_RANDOMT, m_fRandomT);
	DDX_Check(pDX, IDC_CHK_RANDOMR, m_fRandomR);
	DDX_Check(pDX, IDC_CHK_INSTR, m_fShowInstr);
	DDX_Check(pDX, IDC_CHK_CONDINSTR, m_fCondInstr);
	DDX_Check(pDX, IDC_CHK_ENTERKEY, m_fEnterKey);
	DDX_Check(pDX, IDC_CHK_ACCEPTREDO, m_fAcceptRedo);

	DDX_Control(pDX, IDC_CHK_QUEST, m_chkQuest);
	DDX_Control(pDX, IDC_CHK_END, m_chkEnd);
	DDX_Control(pDX, IDC_CHK_OVERRIDE, m_chkSpecify);
	DDX_Control(pDX, IDC_CHK_RANDOM, m_chkRandom);
	DDX_Control(pDX, IDC_CHK_RANDOMT, m_chkRandomT);
	DDX_Control(pDX, IDC_CHK_RANDOMR, m_chkRandomR);
	DDX_Control(pDX, IDC_CHK_INSTR, m_chkShowInstr);
	DDX_Control(pDX, IDC_CHK_CONDINSTR, m_chkCondInstr);
	DDX_Control(pDX, IDC_CHK_ENTERKEY, m_chkEnterKey);
	DDX_Control(pDX, IDC_CHK_ACCEPTREDO, m_chkAcceptRedo);
	DDX_Control(pDX, IDC_RDO_TRIAL_ONCE, m_rdoTrialFirst);
	DDX_Control(pDX, IDC_RDO_TRIAL_ALL, m_rdoTrialEach);
	DDX_Control(pDX, IDC_RDO_AD_YES, m_rdoRedoEach);
	DDX_Control(pDX, IDC_RDO_AD_ERR, m_rdoRedoConsis);
	DDX_Control(pDX, IDC_RDO_AD_REDO, m_rdoRedoAuto);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageGeneral message handlers

BOOL ProcSetPageGeneral::OnApply() 
{
	if( !m_pPS ) return FALSE;

	m_pPS->put_SpecifySequence( m_fSpecify );
	m_pPS->put_Randomize( m_fRandom );
	m_pPS->put_RandomizeTrials( m_fRandomT );
	m_pPS->put_RandWithRules( m_fRandomR );
	m_pPS->put_InstructionAlwaysVisible( m_fShowInstr );

	short nTrialMode = 0, nRedoMode = 0;
	nTrialMode = GetCheckedRadioButton( IDC_RDO_TRIAL_ONCE, IDC_RDO_TRIAL_ALL );
	nRedoMode = GetCheckedRadioButton( IDC_RDO_AD_YES, IDC_RDO_AD_REDO );

	if( m_fCondInstr )
	{
		if( nTrialMode == IDC_RDO_TRIAL_ONCE )
		{
			if( m_fAcceptRedo )
			{
				if( nRedoMode == IDC_RDO_AD_YES )
				{
					if( !m_fEnterKey ) nTrialMode = (short)eTM_FirstAD;
					else nTrialMode = (short)eTM_FirstDelayAD;
				}
				else if( nRedoMode == IDC_RDO_AD_ERR )
				{
					if( !m_fEnterKey ) nTrialMode = (short)eTM_FirstADErr;
					else nTrialMode = (short)eTM_FirstDelayADErr;
				}
				else if( nRedoMode == IDC_RDO_AD_REDO )
				{
					if( !m_fEnterKey ) nTrialMode = (short)eTM_FirstErr;
					else nTrialMode = (short)eTM_FirstDelayErr;
				}
			}
			else
			{
				if( !m_fEnterKey ) nTrialMode = (short)eTM_First;
				else nTrialMode = (short)eTM_FirstDelay;
			}
		}
		else if( nTrialMode == IDC_RDO_TRIAL_ALL )
		{
			if( m_fAcceptRedo )
			{
				if( nRedoMode == IDC_RDO_AD_YES ) nTrialMode = (short)eTM_AllAD;
				else if( nRedoMode == IDC_RDO_AD_ERR ) nTrialMode = (short)eTM_AllADErr;
				else if( nRedoMode == IDC_RDO_AD_REDO ) nTrialMode = (short)eTM_AllErr;
			}
			else nTrialMode = (short)eTM_All;
		}
	}
	else
	{
		if( m_fAcceptRedo )
		{
			if( nRedoMode == IDC_RDO_AD_YES )
			{
				if( !m_fEnterKey ) nTrialMode = (short)eTM_NoneAD;
				else nTrialMode = (short)eTM_DelayAD;
			}
			else if( nRedoMode == IDC_RDO_AD_ERR )
			{
				if( !m_fEnterKey ) nTrialMode = (short)eTM_NoneADErr;
				else nTrialMode = (short)eTM_DelayADErr;
			}
			else if( nRedoMode == IDC_RDO_AD_REDO )
			{
				if( !m_fEnterKey ) nTrialMode = (short)eTM_NoneErr;
				else nTrialMode = (short)eTM_DelayErr;
			}
		}
		else
		{
			if( !m_fEnterKey ) nTrialMode = (short)eTM_None;
			else nTrialMode = (short)eTM_Delay;
		}
	}

	m_pPS->put_TrialMode( (TrialsMode)nTrialMode );

	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->put_DoQuestionnaire( m_fQuest );
		pPSRE->put_QuestAtEnd( m_fEnd );
		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

void ProcSetPageGeneral::OnBnClickedChkQuest()
{
	UpdateData( TRUE );
	if( !m_fQuest ) m_fEnd = FALSE;
	CWnd* pWnd = GetDlgItem( IDC_CHK_END );
	pWnd->EnableWindow( m_fQuest );
	UpdateData( FALSE );
}

void ProcSetPageGeneral::OnChkSpecify()
{
	UpdateData( TRUE );

	if( m_fSpecify && m_fRandom ) m_fRandom = FALSE;
	if( !m_fRandom )
	{
		m_fRandomR = FALSE;
		m_fRandomT = FALSE;
	}
	CWnd* pWnd = GetDlgItem( IDC_CHK_RANDOMT );
	if( pWnd ) pWnd->EnableWindow( m_fRandom );
	pWnd = GetDlgItem( IDC_CHK_RANDOMR );
	if( pWnd ) pWnd->EnableWindow( m_fRandom );
	pWnd = GetDlgItem( IDC_BN_SEL );
	if( pWnd ) pWnd->EnableWindow( m_fRandomR );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) pWnd->EnableWindow( m_fSpecify );

	UpdateData( FALSE );
}

void ProcSetPageGeneral::OnChkRandom() 
{
	UpdateData( TRUE );

	if( !m_fRandom )
	{
		m_fRandomR = FALSE;
		m_fRandomT = FALSE;
	}
	else if( m_fSpecify ) m_fSpecify = FALSE;
	CWnd* pWnd = GetDlgItem( IDC_CHK_RANDOMT );
	if( pWnd ) pWnd->EnableWindow( m_fRandom );
	pWnd = GetDlgItem( IDC_CHK_RANDOMR );
	if( pWnd ) pWnd->EnableWindow( m_fRandom );
	pWnd = GetDlgItem( IDC_BN_SEL );
	if( pWnd ) pWnd->EnableWindow( m_fRandomR );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) pWnd->EnableWindow( m_fSpecify );

	UpdateData( FALSE );
}

void ProcSetPageGeneral::OnChkRules()
{
	UpdateData( TRUE );

	if( m_fRandomR ) m_fRandomT = TRUE;
	CWnd* pWnd = GetDlgItem( IDC_CHK_RANDOMT );
	if( pWnd ) pWnd->EnableWindow( !m_fRandomR );
	pWnd = GetDlgItem( IDC_BN_SEL );
	if( pWnd ) pWnd->EnableWindow( m_fRandomR );

	UpdateData( FALSE );
}

BOOL ProcSetPageGeneral::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageGeneral::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_CHK_OVERRIDE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to specify the order in which conditions are used while running an experiment."), _T("Specify Condition Order") );
	pWnd = GetDlgItem( IDC_CHK_RANDOM );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_RANDC, _T("Randomize Conditions") );
	pWnd = GetDlgItem( IDC_CHK_QUEST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/edit this subject's questionnaire, [prior to] running the experiment."), _T("Questionnaire") );
	pWnd = GetDlgItem( IDC_CHK_END );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/edit AFTER the experiment has been run."), _T("After") );
	pWnd = GetDlgItem( IDC_CHK_RANDOMT );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, IDS_PSPG_RANDT, _T("Randomize Replications") );
		pWnd->EnableWindow( m_fRandom && !m_fRandomR );
	}
	pWnd = GetDlgItem( IDC_CHK_RANDOMR );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Use rules for the randomization for each character of the condition ID."), _T("Use Randomization Rules") );
		pWnd->EnableWindow( m_fRandom );
	}
	pWnd = GetDlgItem( IDC_BN_SEL );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Apply rules to the randomization for each character of the condition ID."), _T("Apply Randomization Rules") );
		pWnd->EnableWindow( m_fRandomR );
	}
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Specify the order in which conditions are used while running an experiment."), _T("Apply Condition Order") );
		pWnd->EnableWindow( m_fSpecify );
	}
	pWnd = GetDlgItem( IDC_CHK_INSTR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_SHOW_INSTR, _T("Experiment Instruction") );
	pWnd = GetDlgItem( IDC_CHK_CONDINSTR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Toggle condition instruction on/off and select an option below."), _T("Condition Instruction") );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ONCE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display the condition instructions for the first trial of that condition."), _T("First Trial") );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ALL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display the condition instructions for each trial."), _T("Each Trial") );
	pWnd = GetDlgItem( IDC_CHK_ENTERKEY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Delay the next trial until ENTER key is pressed."), _T("Enter to Continue") );
	pWnd = GetDlgItem( IDC_CHK_ACCEPTREDO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Toggle accept/redo on/off and select an option below."), _T("Accept/Redo") );
	pWnd = GetDlgItem( IDC_RDO_AD_YES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Ask to accept/redo the trial after every trial."), _T("Ask Every Trial") );
	pWnd = GetDlgItem( IDC_RDO_AD_ERR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Ask to accept/redo the trial if there is a consitency error."), _T("Ask When Error") );
	pWnd = GetDlgItem( IDC_RDO_AD_REDO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Automatically redo the trial if there is a consitency error."), _T("Automatically Redo") );

	CheckRadioButton( IDC_RDO_TRIAL_ONCE, IDC_RDO_TRIAL_ALL, m_nTrialMode );
	CheckRadioButton( IDC_RDO_AD_YES, IDC_RDO_AD_REDO, m_nRedoMode );

	pWnd = GetDlgItem( IDC_CHK_END );
	pWnd->EnableWindow( m_fQuest );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ONCE );
	pWnd->EnableWindow( m_fCondInstr );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ALL );
	pWnd->EnableWindow( m_fCondInstr );
	pWnd = GetDlgItem( IDC_RDO_AD_YES );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_RDO_AD_ERR );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_RDO_AD_REDO );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_CHK_ENTERKEY );
	pWnd->EnableWindow( !( m_fCondInstr && ( m_nTrialMode == IDC_RDO_TRIAL_ALL ) ) );


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageGeneral::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageGeneral::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_runexperiment_procedure.html"), this );
	return TRUE;
}

void ProcSetPageGeneral::OnBnClickedChkCondinstr()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_RDO_TRIAL_ONCE );
	pWnd->EnableWindow( m_fCondInstr );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ALL );
	pWnd->EnableWindow( m_fCondInstr );
	pWnd = GetDlgItem( IDC_CHK_ENTERKEY );
	if( m_fCondInstr &&
		GetCheckedRadioButton( IDC_RDO_TRIAL_ONCE, IDC_RDO_TRIAL_ALL ) == IDC_RDO_TRIAL_ALL )
	{
		m_fEnterKey = FALSE;
		pWnd->EnableWindow( FALSE );
		UpdateData( FALSE );
	}
	else pWnd->EnableWindow( TRUE );
}

void ProcSetPageGeneral::OnBnClickedRdoTrialAll()
{
	m_fEnterKey = FALSE;
	CWnd* pWnd = GetDlgItem( IDC_CHK_ENTERKEY );
	pWnd->EnableWindow( FALSE );
	UpdateData( FALSE );
}

void ProcSetPageGeneral::OnBnClickedChkAcceptredo()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_RDO_AD_YES );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_RDO_AD_ERR );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_RDO_AD_REDO );
	pWnd->EnableWindow( m_fAcceptRedo );

	// (currently) in order to see what was written before the question is asked
	// we default to click to enter to continue since pattern is erased (TODO)
	if( m_fAcceptRedo )
	{
		m_fEnterKey = TRUE;
		UpdateData( FALSE );
	}
}

void ProcSetPageGeneral::OnBnClickedChkEnterKey()
{
	// nothing now...unless we need it
}

void ProcSetPageGeneral::OnBnClickedRdoTrialOnce()
{
	CWnd* pWnd = GetDlgItem( IDC_CHK_ENTERKEY );
	pWnd->EnableWindow( TRUE );
}

void ProcSetPageGeneral::OnBnClickedRules()
{
	CString szMsg = _T("You must have already defined the conditions for this experiment\nand the number of trials for each. Continue?");
	if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	RandRulesDlg d( m_pPS, this );
	d.DoModal();
}

void ProcSetPageGeneral::OnBnClickedOrder()
{
	CString szMsg = _T("You must have already defined the conditions for this experiment\nand the number of trials for each. Continue?");
	if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	CondSortDlg d( m_pPS, this );
	d.DoModal();
}

void ProcSetPageGeneral::OnBnReset() 
{
	m_fQuest = FALSE;
	m_fEnd = FALSE;
	m_fSpecify = FALSE;
	m_fRandom = TRUE;
	m_fRandomT = TRUE;
	m_fRandomR = FALSE;
	m_fShowInstr = FALSE;
	m_fCondInstr = FALSE;
	m_fEnterKey = FALSE;
	m_fAcceptRedo = TRUE;
	m_nTrialMode = IDC_RDO_TRIAL_ONCE;
	m_nRedoMode = IDC_RDO_AD_ERR;

	UpdateData( FALSE );

	CheckRadioButton( IDC_RDO_TRIAL_ONCE, IDC_RDO_TRIAL_ALL, m_nTrialMode );
	CheckRadioButton( IDC_RDO_AD_YES, IDC_RDO_AD_REDO, m_nRedoMode );

	CWnd* pWnd = GetDlgItem( IDC_CHK_END );
	pWnd->EnableWindow( m_fQuest );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ONCE );
	pWnd->EnableWindow( m_fCondInstr );
	pWnd = GetDlgItem( IDC_RDO_TRIAL_ALL );
	pWnd->EnableWindow( m_fCondInstr );
	pWnd = GetDlgItem( IDC_RDO_AD_YES );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_RDO_AD_ERR );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_RDO_AD_REDO );
	pWnd->EnableWindow( m_fAcceptRedo );
	pWnd = GetDlgItem( IDC_CHK_ENTERKEY );
	pWnd->EnableWindow( !( m_fCondInstr && ( m_nTrialMode == IDC_RDO_TRIAL_ALL ) ) );
}

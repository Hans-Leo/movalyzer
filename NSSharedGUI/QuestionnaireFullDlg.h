#pragma once

// QuestionnaireFullDlg.h : header file
//

#include "NSTooltipCtrl.h"

#define	Q_STATE_SAME	_T("SAME")
#define	Q_STATE_NEW		_T("NEW")
#define	Q_STATE_DIFF	_T("DIFF")
#define	Q_STATE_DEL		_T("DEL")

#define	QB_STATE_SAME	0x0001
#define	QB_STATE_NEW	0x0002
#define	QB_STATE_DIFF	0x0004
#define	QB_STATE_DEL	0x0008

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireFullDlg dialog

interface IMQuestionnaire;

class AFX_EXT_CLASS QuestionnaireFullDlg : public CBCGPDialog
{
// Construction
public:
	QuestionnaireFullDlg(IMQuestionnaire* pQ, BOOL fMaster = FALSE, CWnd* pParent = NULL);
	~QuestionnaireFullDlg();

// Dialog Data
	enum { IDD = IDD_QUESTS };
	CBCGPListCtrl		m_List;
	IMQuestionnaire*	m_pQ;
	CStringList*		m_lstH;		// is header list
	CStringList*		m_lstQ;		// question list
	CStringList*		m_lstID;		// id list
	CStringList*		m_lstS;		// state list
	CStringList*		m_lstI;		// item # list
	CStringList*		m_lstP;		// is private list
	CStringList*		m_pListN;		// numeric list
	CStringList*		m_pListCH;		// column header list
	NSToolTipCtrl		m_tooltip;
	CImageList			m_ImageList;
	BOOL				m_fMaster;
	CBCGPButton			m_bnAdd;
	CBCGPButton			m_bnDel;
	CBCGPButton			m_bnEdit;
	CBCGPButton			m_bnUp;
	CBCGPButton			m_bnDown;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillList();
	void MoveItem( int nDir /* 1 for down, -1 for up */ );

// message map functions
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnDel();
	afx_msg void OnClickBnEdit();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickBnUp();
	afx_msg void OnClickBnDown();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

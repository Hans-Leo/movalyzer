#pragma once

//static int val3 = 501;		// SS SDK v2
//static int val3 = 963;		// SS SDK v3
static int val3 = 152;		// SS SDK v3.1

class ProductStatus
{
public:
	ProductStatus() : m_fChecked( FALSE ), m_fEnabled( TRUE ), m_fActivated( FALSE ) {}
	BOOL	m_fChecked;
	BOOL	m_fEnabled;
	BOOL	m_fActivated;
};

class ProductActivations
{
public:
	ProductActivations() : m_fWasOpti( FALSE ) {}
	ProductStatus	m_opti;
	ProductStatus	m_upgrade;
	ProductStatus	m_scripta;
	ProductStatus	m_rx;
	ProductStatus	m_gripa;
	ProductStatus	m_cs;
	BOOL			m_fWasOpti;
};

// ActivateDlg dialog
/*
class AFX_EXT_CLASS ActivateDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC( ActivateDlg )

public:
	ActivateDlg( CWnd* pParent = NULL );   // standard constructor
	virtual ~ActivateDlg();

// Dialog Data
	enum { IDD = IDD_ACTIVATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	BOOL		m_fScript;
	BOOL		m_fOpti;
	BOOL		m_fGrip;
	BOOL		m_fCS;
	BOOL		m_fRx;
	BOOL		m_fUpgrade;
	BOOL		m_fPPU;
	ProductActivations	m_acts;

	virtual BOOL OnInitDialog();
	afx_msg void OnItemClicked();
	afx_msg void OnBnClickedBnPurchase();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	afx_msg void OnClickDetails();
};
*/

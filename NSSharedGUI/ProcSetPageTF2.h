#pragma once

// ProcSetPageTF2.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageTF2 dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageTF2 : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageTF2)

// Construction
public:
	ProcSetPageTF2( IProcSetting* pPS = NULL );
	~ProcSetPageTF2();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_TF2 };
	short			m_nDisCorrection;
	double			m_dDisFactor;
	CColoredEdit	m_editDisFactor;
	double			m_dDisZInsertPoint;
	CColoredEdit	m_editDisZInsertPoint;
	double			m_dDisRelError;
	CColoredEdit	m_editDisRelError;
	double			m_dDisAbsError;
	CColoredEdit	m_editDisAbsError;
	double			m_dDisError;
	CColoredEdit	m_editDisError;
	BOOL			m_fNotify;
	CColoredButton	m_chkNotify;
	CColoredButton	m_rdoDiscard;
	CColoredButton	m_rdoInsert;
	CColoredButton	m_rdoMove;
	CColoredButton	m_rdoAccept;
	IProcSetting*	m_pPS;
	CBCGPButton		m_linkAdv;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

#pragma once

// DrawOptionsDlg dialog

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

interface IProcSetting;

class AFX_EXT_CLASS DrawOptionsDlg : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(DrawOptionsDlg)

public:
	DrawOptionsDlg( IProcSetting* pPS = NULL );
	~DrawOptionsDlg();

// Dialog Data
	enum { IDD = IDD_DRAWOPTIONS };
	IProcSetting*		m_pPS;
	BOOL				m_fLineFixed;
	short				m_nLineSize;
	BOOL				m_fLineByPressure;
	short				m_nMaxLine;
	CBCGPColorButton	m_bnBGColor;
	DWORD				m_dwBGColor;
	CBCGPColorButton	m_bnLineColor1;
	DWORD				m_dwLineColor1;
	CBCGPColorButton	m_bnLineColor2;
	DWORD				m_dwLineColor2;
	CBCGPColorButton	m_bnLineColor3;
	DWORD				m_dwLineColor3;
	CBCGPColorButton	m_bnEllipseColor;
	DWORD				m_dwEllipseColor;
	CBCGPColorButton	m_bnMPPColor;
	DWORD				m_dwMPPColor;
	short				m_nEllipseSize;
	CColoredEdit		m_editSize;
	CColoredButton		m_chkLBP;
	CColoredEdit		m_chkMax;
	NSToolTipCtrl		m_tooltip;

public:
	virtual BOOL OnApply();
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBnColorBG();
	afx_msg void OnBnClickedBnColor1();
	afx_msg void OnBnClickedBnColor2();
	afx_msg void OnBnClickedBnColor3();
	afx_msg void OnBnClickedBnColorEllipse();
	afx_msg void OnBnClickedBnColorMPP();
	afx_msg void OnCbnSelchangeCboLine();
	afx_msg void OnBnClickedBnLineByPressure();
	afx_msg void OnBnReset();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
};

#pragma once

// AcceptRedoDlg dialog

class AFX_EXT_CLASS AcceptRedoDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(AcceptRedoDlg)

public:
	AcceptRedoDlg(CWnd* pParent = NULL);
	virtual ~AcceptRedoDlg() {}

// Dialog Data
	enum { IDD = IDD_ACCEPTREDO };
	CString		m_szTF;
	CString		m_szSeg;
	double		m_dFF;
	BOOL		m_fNoMsgConsis;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();

public:
	afx_msg void OnBnClickedBnView();
	DECLARE_MESSAGE_MAP()
};

#pragma once

// ProcSetPageProp.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageProp dialog

class AFX_EXT_CLASS ProcSetPageProp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageProp)

// Construction
public:
	ProcSetPageProp( BOOL fNew = FALSE, BOOL fDup = FALSE );
	~ProcSetPageProp();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_PROP };
	CComboBox	m_cboType;
	CString		m_szAbbr;
	CString		m_szDesc;
	CString		m_szNotes;
	int			m_nType;
	double		m_dMDV;
	BOOL		m_fNew;
	BOOL		m_fDup;
	NSToolTipCtrl m_tooltip;
	CStringList	m_lstItems;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnKillfocusEditId();
	afx_msg void OnCbnSelchangeCboType();
	afx_msg void OnClickInstr();
	afx_msg void OnClickNotes();
	afx_msg void OnClickAttachments();
	DECLARE_MESSAGE_MAP()
};

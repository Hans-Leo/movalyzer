// DataTabDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GraphDlg.h"
#include "DataTabDlg.h"
#include "DataTabGridDlg.h"
#include "WindowManager.h"
#include "MSOffice.h"
#include "Experiments.h"

static int _ColClick = 0;
#define	UM_REFILLLIST		(WM_USER + 1001)


// LIST CONTROL SETTINGS
// Alternating cell text colors
#define	LIST_CELL_TEXT_COLOR_1		RGB( 0, 0, 0 )
#define	LIST_CELL_TEXT_COLOR_2		RGB( 0, 0, 0 )
// Alternating cell background colors
#define	LIST_CELL_BG_COLOR_1		RGB( 245, 245, 245 )
#define	LIST_CELL_BG_COLOR_2		::GetSysColor( COLOR_WINDOW )


// DataTabDlg dialog
IMPLEMENT_DYNAMIC(DataTabDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(DataTabDlg, CBCGPDialog)
	ON_WM_SIZE()
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_EDIT, OnBnClickedBnEdit)
	ON_MESSAGE(UM_REFILLLIST, OnFillList)
	ON_BN_CLICKED(IDC_BN_REFRESH, OnBnClickedBnRefresh)
	ON_MESSAGE( WM_WINDOWMANAGER, OnWMMessage )
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnNMClickList)
	ON_BN_CLICKED(IDC_BN_EXCEL, OnClickExcel)
	ON_BN_CLICKED(IDC_BN_SORT, OnClickSort)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    CompareFunc
// FullName:  CompareFunc
// Access:    public 
// Returns:   int CALLBACK
// Qualifier:
// Parameter: LPARAM lParam1
// Parameter: LPARAM lParam2
// Parameter: LPARAM lParamSort
//************************************
int CALLBACK CompareFunc( LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort )
{
	// lParamSort contains a pointer to the list view control.
	CListCtrl* pList = (CListCtrl*) lParamSort;
	CString szItem1, szItem2;
	szItem1 = pList->GetItemText( (int)lParam1, _ColClick );
	szItem2 = pList->GetItemText( (int)lParam2, _ColClick );

	BOOL fAlpha = FALSE;
	for( int i = 0; i < szItem1.GetLength(); i++ )
	{
		if( isalpha( szItem1.GetAt( i ) ) )
		{
			fAlpha = TRUE;
			break;
		}
	}
	for( int j = 0; j < szItem2.GetLength(); j++ )
	{
		if( isalpha( szItem2.GetAt( j ) ) )
		{
			fAlpha = TRUE;
			break;
		}
	}

	if( fAlpha ) return strcmp( szItem1, szItem2 );
	else if( atof( szItem1 ) > atof( szItem2 ) ) return 1;
	else if( atof( szItem2 ) > atof( szItem1 ) ) return -1;
	else return 0;
}

//************************************
// Method:    DataTabDlg
// FullName:  DataTabDlg::DataTabDlg
// Access:    public 
// Returns:   
// Qualifier: : CBCGPDialog(), m_szFile( szFile ), m_pWM( pWM ), m_nCurChart( nCurChart )
// Parameter: CString szFile
// Parameter: CWnd * pParent
// Parameter: WindowManager * pWM
// Parameter: int nCurChart
//************************************
DataTabDlg::DataTabDlg(CString szFile, CWnd* pParent /*=NULL*/, WindowManager* pWM /*=NULL*/, int nCurChart /*=0*/ )
	: CBCGPDialog(), m_szFile( szFile ), m_pWM( pWM ), m_nCurChart( nCurChart )
{
	m_fInit = FALSE;
	if( Create( DataTabDlg::IDD, pParent ) )
	{
		if( m_pWM ) m_pWM->AddItem( this );
		ShowWindow( SW_SHOW );
	}
}

//************************************
// Method:    ~DataTabDlg
// FullName:  DataTabDlg::~DataTabDlg
// Access:    public 
// Returns:   
// Qualifier:
//************************************
DataTabDlg::~DataTabDlg()
{
}

//************************************
// Method:    DoDataExchange
// FullName:  DataTabDlg::DoDataExchange
// Access:    virtual protected 
// Returns:   void
// Qualifier:
// Parameter: CDataExchange * pDX
//************************************
void DataTabDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
}

// DataTabDlg message handlers

//************************************
// Method:    PreTranslateMessage
// FullName:  DataTabDlg::PreTranslateMessage
// Access:    virtual public 
// Returns:   BOOL
// Qualifier:
// Parameter: MSG * pMsg
//************************************
BOOL DataTabDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

//************************************
// Method:    OnFillList
// FullName:  DataTabDlg::OnFillList
// Access:    public 
// Returns:   LRESULT
// Qualifier:
// Parameter: WPARAM
// Parameter: LPARAM
//************************************
LRESULT DataTabDlg::OnFillList( WPARAM, LPARAM )
{
	m_szFile = m_list.szFile;
	FillList( m_list.szFile );
	return 0;
}

//************************************
// Method:    FillList
// FullName:  DataTabDlg::FillList
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szFile
//************************************
void DataTabDlg::FillList( CString szFile )
{
	int nCount = m_list.GetHeaderCtrl().GetItemCount();
	for( int i = 0; i < nCount; m_list.DeleteColumn( 0 ), i++ );
	m_list.DeleteAllItems();

	// file
	CStdioFile f;
	CFileFind ff;
	if( szFile == _T("") || !ff.FindFile( szFile ) ||
		!f.Open( szFile, CFile::modeRead | CFile::shareDenyWrite ) )
	{
		BCGPMessageBox( _T("Unable to access file.") );
		return;
	}

	CString szLine, szItem, szExt;
	CStringList lstColumns;
	int nPos = szFile.ReverseFind( '.' );
	if( nPos == -1 ) return;
	szExt = szFile.Mid( nPos + 1 );
	szExt.MakeUpper();
	BOOL fOther = ( ( szExt == _T("TF") ) || ( szExt == _T("SEG") ) || ( szExt == _T("ERR") ) );
	// FOR EXT, CON, HWR
	if( !fOther )
	{
		// we assume the 1st line is the headers
		// we also assume a space as a field delimiter (TODO: we can enhance this later)
		f.ReadString( szLine );
		TRIM( szLine );
		int nPos = szLine.Find( _T(" ") );
		while( nPos != -1 )
		{
			szItem = szLine.Left( nPos );
			lstColumns.AddTail( szItem );
			szLine = szLine.Mid( nPos + 1 );
			TRIM( szLine );
			nPos = szLine.Find( _T(" ") );
		}
		// add last one
		lstColumns.AddTail( szLine );
	}
	// FOR TF, SEG, ERR
	else lstColumns.AddTail( _T("Data") );

	// columns
	m_list.InsertColumn( 0, _T("Row"), LVCFMT_LEFT, 40, 0 );
	CString szTemp;
	POSITION pos = lstColumns.GetHeadPosition();
	int nCol = 1;
	while( pos )
	{
		szItem = lstColumns.GetNext( pos );
		if( m_list.fExt || fOther )
		{
			szTemp.Format( _T(" (%d)"), nCol );
			szItem += szTemp;
		}
		else szItem.Format( _T("Col %d"), nCol );
		if( !fOther ) m_list.InsertColumn( nCol, szItem, LVCFMT_RIGHT, 75, nCol );
		else m_list.InsertColumn( nCol, szItem, LVCFMT_LEFT, 75, nCol );
		nCol++;
	}
	int nCols = nCol;		// total columns

	// data
	int nItem = 0;
	if( !m_list.fExt ) f.SeekToBegin();
	while( f.ReadString( szLine ) )
	{
		szItem.Format( _T("%d"), nItem );
		nItem = m_list.InsertItem( nItem, szItem );

		TRIM( szLine );
		nCol = 1;

		if( !fOther )
		{
			nPos = szLine.Find( _T(" ") );
			while( nPos != -1 )
			{
				szItem = szLine.Left( nPos );

				m_list.SetItem( nItem, nCol, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				nCol++;

				szLine = szLine.Mid( nPos + 1 );
				TRIM( szLine );
				nPos = szLine.Find( _T(" ") );
			}
		}
		// last one
		m_list.SetItem( nItem, nCol, LVIF_TEXT, szLine, 0, 0, 0, NULL );

		nItem++;
	}

	// if OTHER, disable cells button and increase width of column 2 (data)
	if( fOther )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_SORT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		m_list.SetColumnWidth( 1, 2000 );
	}
}

//************************************
// Method:    OnInitDialog
// FullName:  DataTabDlg::OnInitDialog
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL DataTabDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Tabular view of data. Click a column header to sort.") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close this dialog."), _T("Close") );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/edit the raw data in Notepad."), _T("Edit") );
	pWnd = GetDlgItem( IDC_BN_REFRESH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Refresh the list to it's original state."), _T("Refresh") );
	pWnd = GetDlgItem( IDC_BN_EXCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open this file in an Excel spreadsheet.\nExcel must be installed."), _T("Microsoft Excel") );
	pWnd = GetDlgItem( IDC_BN_SORT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open this file in an alternate view where you can sort the data.\nNOTE: Load and sort times can be long for large files."), _T("Sorting") );

	// Determine size, location differences for later use in resizing
	// ...the constant numbers below represent the pixel distance of
	// ...the edges of the graph window from the edge of the dialog window
	RECT rect, rect2;
	CWnd* pList = GetDlgItem( IDC_LIST );
	pList->GetWindowRect( &rect );
	GetWindowRect( &rect2 );
	int nToolbar = rect.top - rect2.top;
	m_nRightBuffer = ( ( rect2.right - rect2.left ) - ( rect.right - rect.left ) ) - 4;
	m_nBottomBuffer = ( ( rect2.bottom - rect2.top ) - ( rect.bottom - rect.top ) ) - 4;
	ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	nToolbar -= m_nTop;
	m_nBottomBuffer -= nToolbar;

	// sub-movement analysis?
	int nPos = m_szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		CString szExp = m_szFile.Mid( nPos + 1 );
		szExp = szExp.Left( 3 );

		// sub-movement analysis?
		IProcSetting* pPS = NULL;
		HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
										 IID_IProcSetting, (LPVOID*)&pPS );
		if( SUCCEEDED( hr ) )
		{
			pPS->Find( szExp.AllocSysString() );
			IProcSetFlags* pPSF = NULL;
			hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
			if( SUCCEEDED( hr ) )
			{
				pPSF->get_SEG_S( &(m_list.fSMA) );
				pPSF->Release();
			}
			pPS->Release();
		}

	}

	// extract file?
	// get extension of this file
	CString szExtFile = m_szFile.Right( 3 );
	szExtFile.MakeUpper();
	m_list.fExt = ( ( szExtFile == _T("EXT") ) || ( szExtFile == _T("CON") ) );

	// list settings
	m_list.hwndParent = this->m_hWnd;
	m_list.szFile = this->m_szFile;
	m_list.ModifyStyle( 0, LVS_SINGLESEL | LVS_SHOWSELALWAYS, 0 );
	m_list.SendMessage( LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT );
	FillList( m_szFile );

	// title
	nPos = m_szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		CString szTitle = m_szFile.Mid( nPos + 1 );
		SetWindowText( szTitle );
	}

	EnableVisualManagerStyle( TRUE, TRUE );
	m_fInit = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//************************************
// Method:    OnSize
// FullName:  DataTabDlg::OnSize
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nType
// Parameter: int cx
// Parameter: int cy
//************************************
void DataTabDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	CWnd* pList = GetDlgItem( IDC_LIST );
	if( pList )
	{
		pList->MoveWindow( m_nLeft, m_nTop, cx - m_nRightBuffer,
							  cy - m_nBottomBuffer, FALSE );
	}
	if( m_fInit )
	{
		m_list.RedrawWindow();
		RedrawWindow();
	}
}

//************************************
// Method:    OnCancel
// FullName:  DataTabDlg::OnCancel
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void DataTabDlg::OnCancel()
{
	DestroyWindow();
}

//************************************
// Method:    PostNcDestroy
// FullName:  DataTabDlg::PostNcDestroy
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void DataTabDlg::PostNcDestroy()
{
	if( m_pWM ) m_pWM->RemoveItem( this );
	delete this;
}

//************************************
// Method:    OnHelpInfo
// FullName:  DataTabDlg::OnHelpInfo
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: HELPINFO * pHelpInfo
//************************************
BOOL DataTabDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("viewingtrials.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

//************************************
// Method:    OnBnClickedBnEdit
// FullName:  DataTabDlg::OnBnClickedBnEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabDlg::OnBnClickedBnEdit()
{
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += m_szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnBnClickedBnRefresh
// FullName:  DataTabDlg::OnBnClickedBnRefresh
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabDlg::OnBnClickedBnRefresh()
{
	FillList( m_szFile );
}

//************************************
// Method:    OnWMMessage
// FullName:  DataTabDlg::OnWMMessage
// Access:    public 
// Returns:   LRESULT
// Qualifier:
// Parameter: WPARAM wParam
// Parameter: LPARAM lParam
//************************************
LRESULT DataTabDlg::OnWMMessage( WPARAM wParam, LPARAM lParam )
{
	// do nothing if aspec chart
	if( m_nCurChart == 16 ) return 0;

	// get current chart
	CString szCurChart;
	::GetCurrentChart( szCurChart );

	// get extension of current chart
	CString szExtChart;
	if( szCurChart.GetLength() >= 3 ) szExtChart = szCurChart.Right( 3 );
	szExtChart.MakeUpper();

	// get extension of this file
	CString szExtFile = m_szFile.Right( 3 );
	szExtFile.MakeUpper();

	// if this file is extract & cur graph is hwr, frd or tf...continue
	BOOL fExt = FALSE;
	int nTrial = -1;
	if( szExtFile == _T("EXT") )
	{
		CString szChart, szFile;
		// we need to ensure we're dealing with the same set of data
		// ... ext file
		int nPos1 = m_szFile.ReverseFind( '\\' );
		if( nPos1 == -1 ) return 0;
		int nPos2 = m_szFile.ReverseFind( '.' );
		if( nPos2 == -1 ) return 0;
		szFile = m_szFile.Mid( nPos1 + 1, nPos2 );
		// ... chart
		nPos1 = szCurChart.ReverseFind( '\\' );
		if( nPos1 == -1 ) return 0;
		nPos2 = szCurChart.ReverseFind( '.' );
		if( nPos2 == -1 ) return 0;
		szChart = szCurChart.Mid( nPos1 + 1, nPos2 );
		// trial #
		nPos2 = szChart.ReverseFind( '.' );
		CString szTrial = szChart.Left( nPos2 );
		szTrial = szTrial.Right( 2 );
		nTrial = atoi( szTrial );

		// 1st 12 chars should match
		szChart.MakeUpper();
		szFile.MakeUpper();
		if( szChart.Left( 12 ) != ( szFile.Left( 12 ) ) ) return 0;

		fExt = TRUE;
	}
	// otherwise only if file is same as chart
//	else if( szCurChart != m_szFile ) return 0;

	if( ( szExtChart != _T("EXT") ) && ( LOWORD( wParam ) == MSG_WND_GRAPH ) )
	{
		LVITEM lvi;
		lvi.mask = LVIF_STATE;
		lvi.state = LVIS_SELECTED | LVIS_FOCUSED;
		lvi.stateMask = LVIS_SELECTED | LVIS_FOCUSED;
		lvi.iSubItem = 0;

		if( ( HIWORD( wParam ) == MSG_WND_SUB_CURSOR ) && !fExt )
		{
			m_list.SetItemState( (int)LOWORD( lParam ), &lvi );
			m_list.EnsureVisible( (int)LOWORD( lParam ), FALSE );
		}
		if( ( HIWORD( wParam ) == MSG_WND_SUB_CURSOR_EXT ) && fExt )
		{
			int nStroke = (int)LOWORD( lParam );
			BOOL fSub = (BOOL)HIWORD( lParam );
			// loop through and find appropriate stroke
			CString szStroke, szTrial;
			BOOL fFound = FALSE;
			int nCount = m_list.GetItemCount();
			for( int i = 0; i < nCount; i++ )
			{
				szTrial = m_list.GetItemText( i, 1 );
				szStroke = m_list.GetItemText( i, 2 );
				if( ( atoi( szTrial ) == nTrial ) && ( atoi( szStroke ) == ( nStroke * ( fSub ? 3 : 1 ) ) ) )
				{
					m_list.SetItemState( i, &lvi );
					m_list.EnsureVisible( i, FALSE );
					fFound = TRUE;
					break;
				}
			}
			// if an item not found, and item is selected, deselect it
			if( !fFound )
			{
				int nSelIdx = -1;
				if( m_list.GetSelectedCount() > 0 )
					nSelIdx = m_list.GetNextItem( -1, LVNI_SELECTED );
				if( nSelIdx != -1 )
				{
					lvi.stateMask = 0;
					m_list.SetItemState( nSelIdx, 0, LVIS_SELECTED );
				}
			}
		}
	}

	return 0;
}

//************************************
// Method:    OnNMClickList
// FullName:  DataTabDlg::OnNMClickList
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NMHDR * pNMHDR
// Parameter: LRESULT * pResult
//************************************
void DataTabDlg::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nCount = m_list.GetSelectedCount();
	if( nCount == 0 ) return;
	int nIdx = m_list.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	::SetCurrentChart( m_szFile );
	if( m_pWM && !m_list.fExt )
		m_pWM->PostMessage( MAKEWPARAM( MSG_WND_GRAPH, MSG_WND_SUB_CURSOR ), nIdx, this );
	*pResult = 0;
}

//************************************
// Method:    OnClickExcel
// FullName:  DataTabDlg::OnClickExcel
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabDlg::OnClickExcel()
{
	CString szMsg = _T("Please ensure all data views using the grid are closed before opening MS Excel.\nContinue?" );
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return;

	if( !Excel::OpenDelimitedText( m_szFile, FALSE, FALSE, FALSE, FALSE, TRUE ) )
		BCGPMessageBox( _T("Unable to open file in spreadsheet.") );
}

//************************************
// Method:    OnClickSort
// FullName:  DataTabDlg::OnClickSort
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataTabDlg::OnClickSort()
{
	DataTabGridDlg* d = new DataTabGridDlg( m_szFile, GetParent() );
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CSortListCtrl, CBCGPListCtrl)
	ON_NOTIFY(HDN_ITEMCLICKA, 0, OnHeaderClicked) 
	ON_NOTIFY(HDN_ITEMCLICKW, 0, OnHeaderClicked)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()

//************************************
// Method:    OnHeaderClicked
// FullName:  CSortListCtrl::OnHeaderClicked
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NMHDR * pNMHDR
// Parameter: LRESULT * pResult
//************************************
void CSortListCtrl::OnHeaderClicked(NMHDR* pNMHDR, LRESULT* pResult) 
{
        HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;

        if( phdn->iButton == 0 )
        {
            // User clicked on header using left mouse button
            if( phdn->iItem == nSortedCol )
                    bSortAscending = !bSortAscending;
            else
                    bSortAscending = TRUE;

            nSortedCol = phdn->iItem;
			CWaitCursor crs;
			SortItems( nSortedCol, bSortAscending );
        }
        *pResult = 0;
}

// SortItems	- Sort the list based on column text/nums
// Returns		- Returns true for success
// nCol			- column that contains the text to be sorted
// bAscending		- indicate sort order
// low			- row to start scanning from - default row is 0
// high			- row to end scan. -1 indicates last row
BOOL CSortListCtrl::SortItems( int nCol, BOOL bAscending, int low, int high )
{
	if( nCol >= ((CHeaderCtrl*)GetDlgItem(0))->GetItemCount() )
		return FALSE;

	if( high == -1 ) high = GetItemCount() - 1;

	int lo = low;
	int hi = high;
	CString midItem, szItem;

	if( hi <= lo ) return FALSE;

	midItem = GetItemText( ( lo + hi ) / 2, nCol );
	double dmid = atof( midItem ), dlo, dhi;

	// loop through the list until indices cross
	while( lo <= hi )
	{
		// rowText will hold all column text for one row
		CStringArray rowText;

		// are we sorting text or numbers
		BOOL fAlpha = FALSE;
		szItem = GetItemText( lo, nCol );
		for( int i = 0; i < szItem.GetLength(); i++ )
		{
			if( isalpha( GetItemText(lo, nCol).GetAt( i ) ) )
			{
				fAlpha = TRUE;
				break;
			}
		}

		// find the first element that is greater than or equal to 
		// the partition element starting from the left Index.
		if( bAscending )
		{
			if( fAlpha )
			{
				while( ( lo < high ) && ( GetItemText( lo, nCol ) < midItem ) )
					++lo;
			}
			else
			{
				dlo = atof( GetItemText( lo, nCol ) );
				while( ( lo < high ) && ( dlo < dmid ) )
					dlo = atof( GetItemText( ++lo, nCol ) );

			}
		}
		else
		{
			if( fAlpha )
			{
				while( ( lo < high ) && ( GetItemText(lo, nCol) > midItem ) )
					++lo;
			}
			else
			{
				dlo = atof( GetItemText( lo, nCol ) );
				while( ( lo < high ) && ( dlo > dmid ) )
					dlo = atof( GetItemText( ++lo, nCol ) );
			}
		}

		// find an element that is smaller than or equal to 
		// the partition element starting from the right Index.
		if( bAscending )
		{
			if( fAlpha )
			{
				while( ( hi > low ) && ( GetItemText(hi, nCol) > midItem ) )
					--hi;
			}
			else
			{
				dhi = atof( GetItemText( hi, nCol ) );
				while( ( hi > low ) && ( dhi > dmid ) )
					dhi = atof( GetItemText( --hi, nCol ) );
			}
		}
		else
		{
			if( fAlpha )
			{
				while( ( hi > low ) && ( GetItemText(hi, nCol) < midItem ) )
					--hi;
			}
			else
			{
				dhi = atof( GetItemText( hi, nCol ) );
				while( ( hi > low ) && ( dhi < dmid ) )
					dhi = atof( GetItemText( --hi, nCol ) );
			}
		}

		// if the indexes have not crossed, swap
		// and if the items are not equal
		if( lo <= hi )
		{
			// swap only if the items are not equal
			BOOL fSwap = FALSE;
			if( !fAlpha )
			{
				dlo = atof( GetItemText( lo, nCol ) );
				dhi = atof( GetItemText( hi, nCol ) );
				fSwap = ( dlo != dhi );
			}
			else fSwap = ( GetItemText( lo, nCol ) != GetItemText( hi, nCol ) );
			if( fSwap )
			{
				// swap the rows
				LV_ITEM lvitemlo, lvitemhi;
				int nColCount = 
					((CHeaderCtrl*)GetDlgItem(0))->GetItemCount();
				rowText.SetSize( nColCount );
				int i;
				for( i=0; i<nColCount; i++)
					rowText[i] = GetItemText(lo, i);
				lvitemlo.mask = LVIF_IMAGE | LVIF_PARAM | LVIF_STATE;
				lvitemlo.iItem = lo;
				lvitemlo.iSubItem = 0;
				lvitemlo.stateMask = LVIS_CUT | LVIS_DROPHILITED | 
						LVIS_FOCUSED |  LVIS_SELECTED | 
						LVIS_OVERLAYMASK | LVIS_STATEIMAGEMASK;

				lvitemhi = lvitemlo;
				lvitemhi.iItem = hi;

				GetItem( &lvitemlo );
				GetItem( &lvitemhi );

				for( i=0; i<nColCount; i++)
					SetItemText(lo, i, GetItemText(hi, i));

				lvitemhi.iItem = lo;
				SetItem( &lvitemhi );

				for( i=0; i<nColCount; i++)
					SetItemText(hi, i, rowText[i]);

				lvitemlo.iItem = hi;
				SetItem( &lvitemlo );
			}

			++lo;
			--hi;
		}
	}

	// If the right index has not reached the left side of array
	// must now sort the left partition.
	if( low < hi )
		SortItems( nCol, bAscending , low, hi);

	// If the left index has not reached the right side of array
	// must now sort the right partition.
	if( lo < high )
		SortItems( nCol, bAscending , lo, high );

	return TRUE;
}

//************************************
// Method:    OnDropFiles
// FullName:  CSortListCtrl::OnDropFiles
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: HDROP dropInfo
//************************************
void CSortListCtrl::OnDropFiles(HDROP dropInfo)
{
	UINT nNumFilesDropped = DragQueryFile( dropInfo, 0xFFFFFFFF, NULL, 0 );
	TCHAR szFileName[MAX_PATH + 1];
	DragQueryFile( dropInfo, 0, szFileName, MAX_PATH + 1 );
	szFile = szFileName;
	::PostMessage( hwndParent, UM_REFILLLIST, 0, 0 );
}
//************************************
// Method:    OnGetCellTextColor
// FullName:  CSortListCtrl::OnGetCellTextColor
// Access:    public 
// Returns:   COLORREF
// Qualifier:
// Parameter: int nRow
// Parameter: int nColum
//************************************
COLORREF CSortListCtrl::OnGetCellTextColor( int nRow, int nColum )
{
	return ( ( nRow % 2 ) == 0 ) ? LIST_CELL_TEXT_COLOR_1 : LIST_CELL_TEXT_COLOR_2;
}

//************************************
// Method:    OnGetCellBkColor
// FullName:  CSortListCtrl::OnGetCellBkColor
// Access:    virtual protected 
// Returns:   COLORREF
// Qualifier:
// Parameter: int nRow
// Parameter: int nColum
//************************************
COLORREF CSortListCtrl::OnGetCellBkColor( int nRow, int nColum )
{
	if( fExt )
	{
		if( fSMA )
		{
			return ( (int)( nRow / 3 ) % 2 ) == 0 ? LIST_CELL_BG_COLOR_1 : LIST_CELL_BG_COLOR_2;
		}
	}

	return ( ( nRow % 2 ) == 0 ) ? LIST_CELL_BG_COLOR_1 : LIST_CELL_BG_COLOR_2;
}

//************************************
// Method:    OnGetCellFont
// FullName:  CSortListCtrl::OnGetCellFont
// Access:    virtual protected 
// Returns:   HFONT
// Qualifier:
// Parameter: int nRow
// Parameter: int nColum
// Parameter: DWORD dwData
//************************************
HFONT CSortListCtrl::OnGetCellFont( int nRow, int nColum, DWORD dwData )
{
	POSITION pos = GetFirstSelectedItemPosition();
	if( GetNextSelectedItem( pos ) == nRow ) return globalData.fontDefaultGUIBold;
	else return globalData.fontRegular;
}

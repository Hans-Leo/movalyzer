// ProcSetPageInput.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageInput.h"
#include "ProcSetSheet.h"
#include "UserMessages.h"
#include "..\Common\MessageManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageInput property page

IMPLEMENT_DYNCREATE(ProcSetPageInput, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageInput, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_CONTROL(STN_CLICKED, IDC_TXT_DSW, OnClickDSW)
END_MESSAGE_MAP()

ProcSetPageInput::ProcSetPageInput( BOOL fNew, IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageInput::IDD), m_pPS( pPS )
{
	m_fNew = fNew;
	m_nRate = MOUSE_RATE;
	m_nPressure = MOUSE_PRESS;
	m_dResolution = MOUSE_RES;
	m_fTilt = FALSE;
	m_nMaxPP = 0;
	m_nMaxPPDevice = 0;

	if( m_pPS )
	{
		short nVal = 0;
		m_pPS->get_SamplingRate( &nVal );
		m_nRate = nVal;
		m_pPS->get_MinPenPressure( &nVal );
		m_nPressure = nVal;
		m_pPS->get_DeviceResolution( &m_dResolution );
		m_pPS->get_UseTilt( &m_fTilt );
		m_pPS->get_MaxPenPressure( &nVal );
		m_nMaxPP = nVal;
	}

	if( m_fNew )
	{
		BOOL fAcquire = !::WasDeviceSetupRun();
		CBCGPWorkspace* app = ::GetWorkspaceApp();
		if( !fAcquire && app )
		{
			CString szPrefix, szKey, szVal, szRB, szDef, szUser;
			::GetCurrentUser( szUser );

			if( ::IsGripperModeOn() ) szPrefix = _T("Gripper_");
			else if( ::IsTabletModeOn() ) szPrefix = _T("Tablet_");
			else szPrefix = _T("Mouse_");

			szVal.Format( _T("Settings\\%s"), szUser );
			szRB = app->GetRegistryBase();
			app->SetRegistryBase( szVal );
			// sampling rate
			szKey = szPrefix;
			szKey += _T("SamplingRate");
			szVal = app->GetString( szKey, szDef );
			if( szVal == _T("") ) {
				short nRate = 0, nPress = 0;
				BOOL fTilt = FALSE;
				::GetDeviceInfo( nRate, nPress, m_dResolution, m_szDev, m_szDim, fTilt, m_szMaxPP );
				m_nRate = nRate;
				m_nPressure = nPress;
			}
			else {
				m_nRate = atoi( szVal );
				// min pen pressure
				szKey = szPrefix;
				szKey += _T("MPP");
				szDef.Format( _T("%d"), ::GetMinPenPressure() );
				szVal = app->GetString( szKey, szDef );
				m_nPressure = atoi( szVal );
				// dev resolution
				szKey = szPrefix;
				szKey += _T("Resolution");
				szDef.Format( _T("%g"), ::GetDeviceResolution() );
				szVal = app->GetString( szKey, szDef );
				if( szVal == _T("") ) szVal = szDef;
				m_dResolution = atof( szVal );
			}
			app->SetRegistryBase( szRB );
		}
		else {
			m_nRate = ::GetSamplingRate();
			m_dResolution = ::GetDeviceResolution();
			m_nPressure = ::GetMinPenPressure();
		}
		m_nMaxPP = ::GetMaxPenPressure();
	}
}

ProcSetPageInput::~ProcSetPageInput()
{
}

void ProcSetPageInput::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_RATE, m_nRate);
	DDX_Text(pDX, IDC_EDIT_PRESSURE, m_nPressure);
	DDX_Text(pDX, IDC_EDIT_RESOLUTION, m_dResolution);
	DDX_Text(pDX, IDC_TXT_DEVICE, m_szDev);
	DDX_Text(pDX, IDC_TXT_DIM, m_szDim);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Check(pDX, IDC_CHK_TILT, m_fTilt);
	DDX_Text(pDX, IDC_TXT_MPP, m_szMaxPP);
	DDX_Text(pDX, IDC_EDIT_MAXPP, m_nMaxPP);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageInput message handlers

BOOL ProcSetPageInput::OnApply() 
{
	if( !m_pPS ) return FALSE;

	if( m_nMaxPP > ::GetMaxPenPressure() )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 1 );
		CString szMsg;
		szMsg.Format( _T("The max pen pressure for notifications (%d) is more than the max device pressure (%d).\nIf you keep this value, you will receive no notifications for pen pressure.\n\nDo you wish to keep this max pen pressure setting?"), m_nMaxPP, ::GetMaxPenPressure() );
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return FALSE;
	}

	m_pPS->put_SamplingRate( m_nRate );
	m_pPS->put_MinPenPressure( m_nPressure );
	m_pPS->put_DeviceResolution( m_dResolution );
	m_pPS->put_UseTilt( m_fTilt );
	m_pPS->put_MaxPenPressure( m_nMaxPP );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageInput::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageInput::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_RATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_RATE, _T("Sampling Rate") );
	pWnd = GetDlgItem( IDC_EDIT_PRESSURE );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_PPMIN, _T("Min. Pen Pressure") );
	pWnd = GetDlgItem( IDC_EDIT_RESOLUTION );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_RES, _T("Resolution") );
	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Reset default input device settings."), _T("Reset") );
	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );
	pWnd = GetDlgItem( IDC_CHK_TILT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Recordings will include X and Y tilt (azimuth and altitude) if supported by the device."), _T("Pen Orientation") );
	pWnd = GetDlgItem( IDC_EDIT_MAXPP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Pressure at which the user will be alerted by a beep when the pen pressure during recording exceeds this value.\nDefault value is the max pen pressure of the device - 1."), _T("Max. Pen Pressure") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageInput::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageInput::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_inputdevice.html"), this );
	return TRUE;
}

void ProcSetPageInput::OnBnReset() 
{
	short nRate = 0, nPressure = 0;
	BOOL fTilt = FALSE;
	::GetDeviceInfo( nRate, nPressure, m_dResolution, m_szDev, m_szDim, fTilt, m_szMaxPP );
	m_nRate = nRate;
	m_nPressure = nPressure;
	m_fTilt = FALSE;
	if( ::IsGripperModeOn() ) m_nMaxPP = 20;
	else m_nMaxPP = ::GetMaxPenPressure() - 1;
	UpdateData( FALSE );
}

void ProcSetPageInput::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void ProcSetPageInput::OnClickDSW()
{
	CWnd* pWnd = AfxGetMainWnd();
	if( pWnd )
	{
		MessageManager* pMM = MessageManager::GetMessageManager();
		if( pMM )
		{
			pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_DEVICESETUP ), 0 );
			m_nRate = ::GetSamplingRate();
			m_dResolution = ::GetDeviceResolution();
			m_nPressure = ::GetMinPenPressure();
			UpdateData( FALSE );
		}
	}
}

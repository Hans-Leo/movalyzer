// GrpQuestionnaireDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GrpQuestionnaireDlg.h"
#include "..\DataMod\DataMod.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GrpQuestionnaireDlg dialog

BEGIN_MESSAGE_MAP(GrpQuestionnaireDlg, CBCGPDialog)
	ON_BN_CLICKED(IDOK, OnOK2)
	ON_BN_CLICKED(IDC_BN_SELECTALL, OnBnSelectall)
	ON_BN_CLICKED(IDC_BN_SELECTNONE, OnBnSelectnone)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

GrpQuestionnaireDlg::GrpQuestionnaireDlg(CString szExpID, CString szGrpID, CWnd* pParent)
	: CBCGPDialog(GrpQuestionnaireDlg::IDD, pParent), m_szExpID( szExpID ), m_szGrpID( szGrpID )
{
}

void GrpQuestionnaireDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
}

/////////////////////////////////////////////////////////////////////////////
// GrpQuestionnaireDlg message handlers

BOOL GrpQuestionnaireDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_GRP );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_QUESTLIST, _T("Question List") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CANCEL, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_SELECTALL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select all items in the list."), _T("Select All") );
	pWnd = GetDlgItem( IDC_BN_SELECTNONE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Deselect all items in the list."), _T("Select None") );

	IMQuestionnaire* pQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
								   IID_IMQuestionnaire, (LPVOID*)&pQ );
	if( SUCCEEDED( hr ) )
	{
		BSTR bstrID = NULL, bstrQ = NULL;
		CString szID, szQ, szH, szI;
		BOOL fHdr = FALSE;
		short nPos = 0;
		CStringList ID, Q, H, I;

		// Get list of questions
		hr = pQ->ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			pQ->get_ID( &bstrID );
			pQ->get_Question( &bstrQ );
			pQ->get_IsHeader( &fHdr );
			pQ->get_ItemNum( &nPos );

			szID = bstrID;
			szQ = bstrQ;
			szH = fHdr ? _T("TRUE") : _T("");
			szI.Format( _T("%d"), nPos );

			ID.AddTail( szID );
			Q.AddTail( szQ );
			H.AddTail( szH );
			I.AddTail( szI );

			hr = pQ->GetNext();
		}

		pQ->Release();

		// Sort
		int nCnt = (int)ID.GetCount();
		CString szTemp;
		for( int i = 0; i < nCnt; i++ )
		{
			m_lstID.AddTail( szTemp );
			m_lstQ.AddTail( szTemp );
			m_lstH.AddTail( szTemp );
			m_lstI.AddTail( szTemp );
		}
		POSITION posID = ID.GetHeadPosition();
		POSITION posQ = Q.GetHeadPosition();
		POSITION posH = H.GetHeadPosition();
		POSITION posI = I.GetHeadPosition();
		POSITION pos = NULL;
		while( posID )
		{
			szID = ID.GetNext( posID );
			szQ = Q.GetNext( posQ );
			szH = H.GetNext( posH );
			szI = I.GetNext( posI );

			nPos = atoi( szI );

			pos = m_lstID.FindIndex( nPos - 1 );
			if( pos ) m_lstID.SetAt( pos, szID );
			pos = m_lstQ.FindIndex( nPos - 1 );
			if( pos ) m_lstQ.SetAt( pos, szQ );
			pos = m_lstH.FindIndex( nPos - 1 );
			if( pos ) m_lstH.SetAt( pos, szH );
			pos = m_lstI.FindIndex( nPos - 1 );
			if( pos ) m_lstI.SetAt( pos, szI );
		}
	}

	// Fill visible list
	POSITION posID = m_lstID.GetHeadPosition();
	POSITION posQ = m_lstQ.GetHeadPosition();
	POSITION posH = m_lstH.GetHeadPosition();
	CString szH, szQ, szID, szTemp;
	int nItem = 0;
	CString szDelim;
	::GetDelimiter( szDelim );
	while( posH )
	{
		szID = m_lstID.GetNext( posID );
		szQ = m_lstQ.GetNext( posQ );
		szH = m_lstH.GetNext( posH );

		if( szH == _T("") )
		{
			szTemp = _T("     ");
			szTemp += szID;
		}
		else szTemp = szID;
		szTemp += szDelim;
		szTemp += szQ;

		nItem = m_List.AddString( szTemp );
	}

	// Identify group selections (if any)
	IGQuestionnaire* pGQ = NULL;
	hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
						   IID_IGQuestionnaire, (LPVOID*)&pGQ );
	if( SUCCEEDED( hr ) )
	{
		BSTR bstrID = NULL;

		hr = pGQ->FindForGroup( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			pGQ->get_ID( &bstrID );
			szID = bstrID;

			nItem = FindItem( szID );
			if( nItem != -1 ) m_List.SetSel( nItem );

			hr = pGQ->GetNext();
		}

		pGQ->Release();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL GrpQuestionnaireDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void GrpQuestionnaireDlg::OnOK()
{
	m_lstH.RemoveAll();

	// Any items selected ?
	int nSelected = m_List.GetSelCount();
	if( nSelected <= 0 )
	{
//		BCGPMessageBox( IDS_NOSEL );
//		return;
		CBCGPDialog::OnOK();
	}

	// Get list of selected items
	int pIdx[ 500 ];
	nSelected = m_List.GetSelItems( nSelected, pIdx );

	// Get data for the selected items, via list index
	// We'll use one of the existing CStringLists so not to add more data
	CString szID;
	POSITION pos = NULL;
	for( int i = 0; i < nSelected; i++ )
	{
		pos = m_lstID.FindIndex( pIdx[ i ] );
		szID = m_lstID.GetAt( pos );
		m_lstH.AddTail( szID );
	}

	// Update DB
	IGQuestionnaire* pQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
								   IID_IGQuestionnaire, (LPVOID*)&pQ );
	if( SUCCEEDED( hr ) )
	{
		hr = pQ->FindForGroup( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString() );
		if( SUCCEEDED( hr ) )
			pQ->RemoveForGroup( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString() );

		pos = m_lstH.GetHeadPosition();
		int nPos = 1;
		while( pos )
		{
			szID = m_lstH.GetNext( pos );
			pQ->put_ExperimentID( m_szExpID.AllocSysString() );
			pQ->put_GroupID( m_szGrpID.AllocSysString() );
			pQ->put_ID( szID.AllocSysString() );
			pQ->put_ItemNum( (short)nPos );
			hr = pQ->Add();

			nPos++;
		}

		pQ->Release();
	}

	CBCGPDialog::OnOK();
}

int GrpQuestionnaireDlg::FindItem( CString szID )
{
	int nItem = -1, nPos, nCount = m_List.GetCount();
	CString szItem, szDelim;

	::GetDelimiter( szDelim );

	for( int i = 0; i < nCount; i++ )
	{
		m_List.GetText( i, szItem );
		nPos = szItem.Find( szDelim );
		szItem = szItem.Left( nPos );
		TRIM( szItem );

		if( szItem == szID )
		{
			nItem = i;
			break;
		}
	}

	return nItem;
}

void GrpQuestionnaireDlg::OnOK2() 
{
	OnOK();
}

void GrpQuestionnaireDlg::OnBnSelectall() 
{
	int nCnt = m_List.GetCount();
	if( nCnt <= 0 ) return;

	m_List.SelItemRange( TRUE, 0, nCnt );
}

void GrpQuestionnaireDlg::OnBnSelectnone() 
{
	int nCnt = m_List.GetCount();
	if( nCnt <= 0 ) return;

	m_List.SelItemRange( FALSE, 0, nCnt );
}

BOOL GrpQuestionnaireDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("questionnaire.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

BEGIN_MESSAGE_MAP(GroupQuestListBox, CBCGPListBox)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void GroupQuestListBox::OnLButtonUp( UINT nFlags, CPoint point )
{
	m_fMouseDown = FALSE;
	CListBox::OnLButtonUp( nFlags, point );
}

void GroupQuestListBox::OnLButtonDown( UINT nFlags, CPoint point )
{
	m_fMouseDown = TRUE;
	BOOL fOutside = TRUE;
	UINT nItem = ItemFromPoint( point, fOutside );
	if( !fOutside ) m_nLastItem = nItem;
	CListBox::OnLButtonDown( nFlags, point );
}

void GroupQuestListBox::OnMouseMove( UINT nFlags, CPoint point )
{
	CListBox::OnMouseMove( nFlags, point );
	if( !m_fMouseDown ) return;

	BOOL fOutside = TRUE, fSelected = FALSE;
	UINT nItem = ItemFromPoint( point, fOutside );
	if( !fOutside && ( nItem >= 0 ) && ( m_nLastItem != nItem ) )
	{
		// direction
		UINT nDir = LIST_DIRECTION_NONE;
		if( nItem > m_nLastItem ) nDir = LIST_DIRECTION_DOWN;
		else if( nItem < m_nLastItem ) nDir = LIST_DIRECTION_UP;

		// change of direction? if so, then the last item needs to be addressed
		if( m_nDir != LIST_DIRECTION_NONE )
		{
			if( nDir != m_nDir )
			{
				UINT nPrevItem = nItem + ( ( nDir == LIST_DIRECTION_UP ) ? 1 : -1 );
				if( nPrevItem >= 0 )
				{
					fSelected = ( GetSel( nPrevItem ) > 0 );
					SetSel( nPrevItem, !fSelected );
				}
			}
		}

		// get previous state
		fSelected = ( GetSel( nItem ) > 0 );
		// set new state
		SetSel( nItem, !fSelected );

		m_nDir = nDir;
		m_nLastItem = nItem;
	}
}

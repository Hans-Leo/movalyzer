// WizardImportLocation.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardImportLocation.h"
#include "WizardImportRemoteSheet.h"

// WizardImportLocation dialog

IMPLEMENT_DYNAMIC(WizardImportLocation, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportLocation, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_TEST, &WizardImportLocation::OnBnClickedBnTest)
END_MESSAGE_MAP()

WizardImportLocation::WizardImportLocation() : CBCGPPropertyPage(WizardImportLocation::IDD)
{
	m_szRemote = _T("ftp://www.domain.com");
#ifdef	_DEBUG
	m_szLogin = _T("eps");
	m_szPassword = _T("epsdata");
#endif
}

WizardImportLocation::~WizardImportLocation()
{
}

void WizardImportLocation::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_LOC_REMOTE, m_szRemote);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szPath);
	DDX_Text(pDX, IDC_EDIT_LOGIN, m_szLogin);
	DDX_Text(pDX, IDC_EDIT_PASSWORD, m_szPassword);
	DDX_Text(pDX, IDC_TXT_STATUS, m_szStatus);
}

// WizardImportLocation message handlers


BOOL WizardImportLocation::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.SetTitle( _T("Export Wizard") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_LOC_REMOTE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Input the remote FTP location for export files to download and import."), _T("FTP Address") );
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the relative path from the root directory of the FTP (can be empty)."), _T("FTP Relative Path") );
	pWnd = GetDlgItem( IDC_EDIT_LOGIN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Login (username) for remote location."), _T("Username") );
	pWnd = GetDlgItem( IDC_EDIT_PASSWORD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Password for remote location."), _T("Password") );

	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("UploadURL");
		m_szRemote = app->GetString( szKey, m_szRemote );
		if( m_szRemote == _T("") ) m_szRemote = _T("ftp://www.domain.com");
		szKey = _T("UploadPath");
		m_szPath = app->GetString( szKey, m_szPath );
		app->SetRegistryBase( szRB );
	}
	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportLocation::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardImportLocation::OnWizardNext()
{
	// verify connection first
	if( !Test( FALSE ) ) return 0;

	// set upload locations to registry
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("UploadURL");
		szVal = m_szRemote;
		app->WriteString( szKey, szVal );
		szKey = _T("UploadPath");
		szVal = m_szPath;
		app->WriteString( szKey, szVal );
		app->SetRegistryBase( szRB );
	}

	WizardImportRemoteSheet* pParent = (WizardImportRemoteSheet*)GetParent();
	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportLocation::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

BOOL WizardImportLocation::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("remote_import_wizard.html"), this );
	return TRUE;
}

void WizardImportLocation::OnBnClickedBnTest()
{
	Test( TRUE );
}

BOOL WizardImportLocation::Test( BOOL fInformOnSuccess )
{
	UpdateData( TRUE );

	WizardImportRemoteSheet* pParent = (WizardImportRemoteSheet*)GetParent();
	BOOL fFail = FALSE;

	::ShowPopupWindow( this, _T("Attempting connection. Please wait..."), 0, 0, TRUE );

	// verify location, login, password (warn for last 2)
	if( m_szRemote == _T("") )
	{
		BCGPMessageBox( _T("No remote location has been specified.") );
		::KillPopupWindow();
		return FALSE;
	}
	if( ( m_szLogin == _T("") ) || ( m_szPassword == _T("") ) )
	{
		if( BCGPMessageBox( _T("Login information has not been provided. Continue?"), MB_YESNO ) == IDNO )
		{
			::KillPopupWindow();
			return FALSE;
		}
	}

	// TEST CONNECTION
	CWaitCursor crs;
	if( !pParent->m_pInetSession )
	{
		CString szObject;
		INTERNET_PORT nPort = 0;
		DWORD dwServiceType = 0;
		// create internet session
		pParent->m_pInetSession = new CInternetSession( _T("MovAlyzeR"), 1, PRE_CONFIG_INTERNET_ACCESS );
		if( !pParent->m_pInetSession )
		{
			BCGPMessageBox( _T("Cannot open an internet session.  Please check your internet configuration.") );
			fFail = TRUE;
			::KillPopupWindow();
			return FALSE;
		}
		// parse URL
		if( !AfxParseURL( m_szRemote, dwServiceType, m_szServerName, szObject, nPort ) )
		{
			BCGPMessageBox( _T("The remote location FTP URL is invalid.") );
			fFail = TRUE;
			::KillPopupWindow();
			return FALSE;
		}
		// connect
		if( ( dwServiceType != INTERNET_SERVICE_FTP ) || m_szServerName.IsEmpty() )
		{
			BCGPMessageBox( _T("The remote location FTP URL is invalid.") );
			fFail = TRUE;
			::KillPopupWindow();
			return FALSE;
		}
	}
	try
	{
		crs.Restore();
		pParent->m_pFtpConnection = pParent->m_pInetSession->GetFtpConnection( m_szServerName, m_szLogin, m_szPassword );
		pParent->m_pFtpConnection->SetCurrentDirectory( m_szPath );
	}
	catch( CInternetException* ie )
	{
		TCHAR szErr[ 1024 ];
		if( ie->GetErrorMessage( szErr, 1024 ) )
			BCGPMessageBox( szErr );
		else
			BCGPMessageBox( _T("An exception occurred when attempting to create an FTP connection.\r\n  Please check your internet configuration and make sure your environment\r\n is set up with WININET.DLL in the path.") );
		ie->Delete();
		fFail = TRUE;
		::KillPopupWindow();
		return FALSE;
	}

	::KillPopupWindow();

	// success
	if( fInformOnSuccess ) BCGPMessageBox( _T("Username and password accepted.") );

	// since the test passed, set the wizard buttons to next
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return !fFail;
}

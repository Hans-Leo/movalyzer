#if !defined(AFX_ELEMENTPAGEPATTERN_H__A09D1981_BC7D_490B_84D3_632A61FD8A08__INCLUDED_)
#define AFX_ELEMENTPAGEPATTERN_H__A09D1981_BC7D_490B_84D3_632A61FD8A08__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ElementPagePattern.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ElementPagePattern dialog

interface IElement;

class AFX_EXT_CLASS ElementPagePattern : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ElementPagePattern)

// Construction
public:
	ElementPagePattern( IElement* = NULL );
	~ElementPagePattern();

// Dialog Data
	enum { IDD = IDD_PAGE_ELMT_HWR };
	CComboBox		m_cboTrial;
	CComboBox		m_cboSubj;
	CComboBox		m_cboGrp;
	CComboBox		m_cboExp;
	IElement*		m_pE;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	CString			szExp, szGrp, szSubj, szTrial;

// Overrides
public:
	virtual BOOL OnApply();
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void GetIDs( CString& szExpID, CString& szGrpID, CString& szSubjID, CString& szTrial );
// message map functions
	afx_msg void OnSelchangeCboExp();
	afx_msg void OnSelchangeCboGrp();
	afx_msg void OnSelchangeCboSubj();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()

};

#endif // !defined(AFX_ELEMENTPAGEPATTERN_H__A09D1981_BC7D_490B_84D3_632A61FD8A08__INCLUDED_)

#pragma once

// ActivateHTMLDlg dialog

#include <Mshtml.h>
#include <afxdhtml.h>        // HTML Dialogs
#include "ActivateDlg.h"

#define ACT_MODE_COPYPROTECT	0
#define ACT_MODE_PURCHASE		1
#define ACT_MODE_EXPIRED		2
#define ACT_MODE_RECOVERY		3


class AFX_EXT_CLASS ActivateHTMLDlg : public CDHtmlDialog
{
	DECLARE_DYNAMIC(ActivateHTMLDlg)

public:
	ActivateHTMLDlg( CWnd* pParent = NULL, BOOL fModeless = FALSE );
	virtual ~ActivateHTMLDlg() {}

	// Dialog Data
	enum { IDD = IDD_HTML, IDH = IDR_ACTIVATION };
	BOOL	m_fModeless;
	BOOL	m_nMode;
	ProductActivations	m_acts;
	BOOL	m_fScript;
	BOOL	m_fOpti;
	BOOL	m_fGrip;
	BOOL	m_fCS;
	BOOL	m_fRx;
	BOOL	m_fUpgrade;
	BOOL	m_fPurchase;
	BOOL	m_fActivate;
	int		m_nItem;
	int		m_nParam;
	CString	m_szProds;
	CString	m_szRequestCode;
	CString	m_szActivationCode;
	BOOL	m_fUnlock;
	BOOL	m_fSuccess;

	// operations
public:
	static ActivateHTMLDlg* GetActivateHTMLDlg();
	void UpdateCode();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnDocumentComplete(LPDISPATCH pDisp, LPCTSTR szUrl);
	virtual void OnCancel();
	virtual void PostNcDestroy();
protected:
	void SetAttr( CString szItem, CString szAttr, CString szVal, BOOL fRemove = FALSE );
	void EnableDisable();
	BOOL DoActivate( BOOL fActivate = FALSE );
public:
	HRESULT OnItemChecked( IHTMLElement* );
	HRESULT OnClickPurchase( IHTMLElement* );
	HRESULT OnClickActivate( IHTMLElement* );
	HRESULT OnClickCancel( IHTMLElement* );
	DECLARE_MESSAGE_MAP()
	DECLARE_DHTML_EVENT_MAP()
};

// WizardExportExp.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardExportExp.h"
#include "WizardExportSheet.h"
#include "Experiments.h"
#include "Subjects.h"
#include "Groups.h"
#include "..\DataMod\DataMod.h"

struct ExportStruct
{
	CString	szGrpID;
	CString szSubjID;
	CString	szSubj;
	CString	szCode;
	CString	szExp;
	CString szExpWhen;
};

typedef struct
{
	int nColumn;
	BOOL bAscending;
} ESORTINFO;

// WizardExportExp dialog

IMPLEMENT_DYNAMIC(WizardExportExp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardExportExp, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_EXPS, &WizardExportExp::OnCbnSelchangeCboExps)
	ON_BN_CLICKED(IDC_CHK_HIDE, &WizardExportExp::OnBnClickedChkHide)
	ON_WM_HELPINFO()
	ON_NOTIFY(NM_CLICK, IDC_LIST, &WizardExportExp::OnNMClickList)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST, &WizardExportExp::OnLvnColumnclickList)
END_MESSAGE_MAP()

WizardExportExp::WizardExportExp( CString szExpID, CString szSubjID, CString szGrpID )
	: CBCGPPropertyPage( WizardExportExp::IDD )
{
	m_szExpID = szExpID;
	m_szSubjID = szSubjID;
	m_szGrpID = szGrpID;
	m_fHideExported = TRUE;
	m_nCurCol = -1;
	m_nLastCol = -1;
	m_fAscending = TRUE;
}

WizardExportExp::~WizardExportExp()
{
}

void WizardExportExp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_EXPS, m_Cbo);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Check(pDX, IDC_CHK_HIDE, m_fHideExported);
}

// WizardExportExp message handlers

BOOL WizardExportExp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.SetTitle( _T("Export Wizard") );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	m_tooltip.AddTool( pWnd, _T("Select an existing experiment."), _T("Experiment") );
	pWnd = GetDlgItem( IDC_LIST );
	m_tooltip.AddTool( pWnd, _T("Select the subjects that you would like to export. Select NONE to go directly to the upload section."), _T("Select Subjects") );
	pWnd = GetDlgItem( IDC_CHK_HIDE );
	m_tooltip.AddTool( pWnd, _T("Check to hide subjects that have already been exported."), _T("Hide/Show Subjects") );

	// last experiment exported (if not set in constructor)
	CString szExp;
	if( m_szExpID == _T("") )
	{
		CBCGPWorkspace* app = ::GetWorkspaceApp();
		if( app )
		{
			CString szKey, szVal, szUser, szRB;
			::GetCurrentUser( szUser );
			szVal.Format( _T("Settings\\%s"), szUser );
			szRB = app->GetRegistryBase();
			app->SetRegistryBase( szVal );
			szKey = _T("ExportLastExp");
			szExp = app->GetString( szKey, szExp );
			app->SetRegistryBase( szRB );
		}
	}
	else szExp = m_szExpID;

	// Fill experiment list
	Experiments exp;
	if( !exp.IsValid() ) return TRUE;
	CString szID, szItem;
	int nSel = -1, nItem = -1;
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		exp.GetID( szID );
		exp.GetDescriptionWithID( szItem );

		nItem = m_Cbo.AddString( szItem );
		if( szExp == szID ) nSel = nItem;

		hr = exp.GetNext();
	}
	if( ( nSel == -1 ) && ( nItem != -1 ) ) nSel = 0;

	// image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );

	// Column Headers
	m_list.InsertColumn( 0, _T("ID"), LVCFMT_LEFT, 50, 0 );
	m_list.InsertColumn( 1, _T("Code"), LVCFMT_LEFT, 80, 1 );
	m_list.InsertColumn( 2, _T("Group"), LVCFMT_LEFT, 50, 2 );
	m_list.InsertColumn( 3, _T("Name"), LVCFMT_LEFT, 100, 3 );
	m_list.InsertColumn( 4, _T("When"), LVCFMT_LEFT, 100, 4 );

	if( nSel != -1 )
	{
		m_Cbo.SetCurSel( nSel );
		OnCbnSelchangeCboExps();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardExportExp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardExportExp::OnWizardNext() 
{
	// if none selected, inform user and ask to continue to upload
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if( !pos )
	{
		if( BCGPMessageBox( _T("No subjects selected. Continue to upload?"), MB_YESNO ) == IDYES )
			return IDD_WIZ_EXPORT_SUBJECTSTOUP;
		else return -1;
	}

	// clear out list
	m_lstSubjects.RemoveAll();
	// snag list of subjects
	CString szID, szDesc, szGrpID, szItem;
	short nIdx = 0;
	while( pos )
	{
		nIdx = m_list.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szID = m_list.GetItemText( nIdx, 0 );
			szGrpID = m_list.GetItemText( nIdx, 2 );
			szDesc = m_list.GetItemText( nIdx, 3 );

			CString szDelim;
			::GetDelimiter( szDelim );
			szItem.Format( _T("%s%s%s"), m_szExpID, szGrpID, szID );
			m_lstSubjects.AddTail( szItem );
		}
	}

	// last experiment exported
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("ExportLastExp");
		app->WriteString( szKey, m_szExpID );
		app->SetRegistryBase( szRB );
	}

	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardExportExp::OnWizardFinish()
{
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		ExportStruct* pItem = (ExportStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
	m_list.DeleteAllItems();

	return CBCGPPropertyPage::OnWizardFinish();
}

BOOL WizardExportExp::OnSetActive() 
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
//	POSITION pos = m_list.GetFirstSelectedItemPosition();
//	if( pos ) pParent->SetWizardButtons( PSWIZB_NEXT );
//	else pParent->SetWizardButtons( 0 );
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnSetActive();
}

void WizardExportExp::OnCbnSelchangeCboExps()
{
	if( m_Cbo.GetCurSel() < 0 ) return;
	FillList();
}

void WizardExportExp::FillList()
{
	CString szItem, szDelim, szID, szDesc;
	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED;
	lvi.stateMask = (UINT)-1;

	// empty first
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		ExportStruct* pItem = (ExportStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
	m_list.DeleteAllItems();

	// delim
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// get exp info from selection combo
	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	szID = szItem.Mid( nPos + 2 );
	m_szExpID = szID;
	szDesc = szItem.Left( nPos - 1 );

	// exp member list object
	HRESULT hr = E_FAIL;
	IExperimentMember*	pEML = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEML );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return;
	}

	// Now get all groups/subjects for this experiment (loop through)
	BSTR bstr = NULL;
	CString szGrpID, szGrp, szSubjID, szNameLast, szNameFirst, szCode;
	DATE dt;
	COleDateTime dtWhen;
	int nItem = 0;
	BOOL fExp = FALSE;
	Subjects subj;
	hr = pEML->FindForExperiment( szID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		hr = pEML->get_GroupID( &bstr );
		szGrpID = bstr;
		if( SUCCEEDED( hr ) && ( bstr != NULL ) && szGrpID != _T("") )
		{
			// Get data
			pEML->get_SubjectID( &bstr );
			szSubjID = bstr;
			hr = subj.Find( szSubjID );
			if( SUCCEEDED( hr ) &&
				( ( m_szSubjID == _T("") ) ||
				  ( ( szSubjID == m_szSubjID ) && ( ( m_szGrpID == _T("") ) || ( szGrpID == m_szGrpID ) ) ) ) )
			{
				subj.GetCode( szCode );
				subj.GetNameLast( szNameLast, ::IsPrivacyOn() );
				subj.GetNameFirst( szNameFirst, ::IsPrivacyOn() );
				pEML->get_Exported( &fExp );
				pEML->get_ExpWhen( &dt );
				dtWhen = dt;
				szItem.Format( _T("%s, %s"), szNameLast, szNameFirst );

				if( !m_fHideExported || ( m_fHideExported && !fExp ) )
				{
					ExportStruct* pES = new ExportStruct;
					pES->szGrpID = szGrpID;
					pES->szSubjID = szSubjID;
					pES->szSubj = szItem;
					pES->szCode = szCode;
					pES->szExp = fExp ? _T("YES") : _T("NO");
					pES->szExpWhen = dtWhen.Format( _T("%Y-%m-%d") );

					nItem = m_list.InsertItem( nItem, szSubjID, 4 );
					m_list.SetItemData( nItem, (LPARAM)pES );
					if( nItem != -1 )
					{
						m_list.SetItem( nItem, 1, LVIF_TEXT, pES->szCode, 0, 0, 0, NULL );
						m_list.SetItem( nItem, 2, LVIF_TEXT, pES->szGrpID, 0, 0, 0, NULL );
						m_list.SetItem( nItem, 3, LVIF_TEXT, pES->szSubj, 0, 0, 0, NULL );
						m_list.SetItem( nItem, 4, LVIF_TEXT, fExp ? pES->szExpWhen : _T("N/A"), 0, 0, 0, NULL );
						if( !fExp ) m_list.SetItemState( nItem, &lvi );
					}
				}
			}
		}
		hr = pEML->GetNext();
	}

	pEML->Release();

	OnSetActive();
}

void WizardExportExp::OnBnClickedChkHide()
{
	UpdateData( TRUE );
	FillList();
}

BOOL WizardExportExp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("export_wizard.html"), this );
	return TRUE;
}

BOOL WizardExportExp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardExportExp::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
//	POSITION pos = m_list.GetFirstSelectedItemPosition();
//	if( pos ) pParent->SetWizardButtons( PSWIZB_NEXT );
//	else pParent->SetWizardButtons( 0 );
	pParent->SetWizardButtons( PSWIZB_NEXT );

	*pResult = 0;
}

void WizardExportExp::OnCancel()
{
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		ExportStruct* pItem = (ExportStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
	m_list.DeleteAllItems();
}

void WizardExportExp::OnOK()
{
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		ExportStruct* pItem = (ExportStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
}

int CALLBACK WizardExportExp::CompareFunc( LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort )
{
	ExportStruct* pItem1 = (ExportStruct*)lParam1;
	ExportStruct* pItem2 = (ExportStruct*)lParam2;
	ESORTINFO* pSortInfo = (ESORTINFO*)lParamSort;
	ASSERT( pItem1 && pItem2 && pSortInfo );

	int result = 0;
	switch( pSortInfo->nColumn )
	{
		case 0:
			result = pItem1->szSubjID.Compare( pItem2->szSubjID );
			break;
		case 1:
			result = pItem1->szCode.Compare( pItem2->szCode );
			break;
		case 2:
			result = pItem1->szGrpID.Compare( pItem2->szGrpID );
			break;
		case 3:
			result = pItem1->szSubj.Compare( pItem2->szSubj );
			break;
		case 4:
			result = pItem1->szExp.Compare( pItem2->szExp );
			break;
		case 5:
			result = pItem1->szExpWhen.Compare( pItem2->szExpWhen );
			break;
		default:
			ASSERT( FALSE );
	}

	if( !pSortInfo->bAscending )
	result = -result;

	return result;
}

void WizardExportExp::OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	CWaitCursor waiter;

	ESORTINFO si;
	si.nColumn = pNMLV->iSubItem;

	m_nLastCol = m_nCurCol;
	m_nCurCol = si.nColumn;

	if( m_nCurCol == m_nLastCol ) m_fAscending = !m_fAscending;
	else m_fAscending = TRUE;
	si.bAscending = m_fAscending;

	m_list.SortItems( CompareFunc, (LPARAM)&si );

	*pResult = 0;
}

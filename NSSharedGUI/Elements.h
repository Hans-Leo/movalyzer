#ifndef _ELEMENTS_H_
#define	_ELEMENTS_H_

#include "Objects.h"

#define	TAG_ELEMENT	_T("[ELEMENT]")

interface IElement;

// This class is intended to encapsulate all view & functionality for an element
class AFX_EXT_CLASS Elements : public Objects
{
	PUT_BASE( Element )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();

	// Interface methods
	PUT_GET( Element, ID, CString, val.AllocSysString() )

	PUT_GET( Element, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( IElement*, CString&, BOOL fReverse = FALSE );

	PUT_GET( Element, Pattern, CString, val.AllocSysString() )
	PUT_GET( Element, Shape, short, val )
	PUT_GET( Element, CenterX, double, val )
	PUT_GET( Element, CenterY, double, val )
	PUT_GET( Element, WidthX, double, val )
	PUT_GET( Element, WidthY, double, val )
	PUT_GET( Element, IsTarget, BOOL, val )
	PUT_GET( Element, ErrorX, double, val )
	PUT_GET( Element, ErrorY, double, val )
	PUT_GET( Element, Line1Size, short, val )
	PUT_GET( Element, Line2Size, short, val )
	PUT_GET( Element, Line3Size, short, val )
	PUT_GET( Element, Line1Color, DWORD, val )
	PUT_GET( Element, Line2Color, DWORD, val )
	PUT_GET( Element, Line3Color, DWORD, val )
	PUT_GET( Element, Background1Color, DWORD, val )
	PUT_GET( Element, Background2Color, DWORD, val )
	PUT_GET( Element, Background3Color, DWORD, val )
	PUT_GET( Element, Text1, CString, val.AllocSysString() )
	PUT_GET( Element, Text2, CString, val.AllocSysString() )
	PUT_GET( Element, Text3, CString, val.AllocSysString() )
	PUT_GET( Element, Text1Color, DWORD, val )
	PUT_GET( Element, Text2Color, DWORD, val )
	PUT_GET( Element, Text3Color, DWORD, val )
	PUT_GET( Element, Text1Size, short, val )
	PUT_GET( Element, Text2Size, short, val )
	PUT_GET( Element, Text3Size, short, val )
// TODO: these will need to be reworked due to double variant pointers
//	PUT_GET( Element, Font1, VARIANT*, val )
//	PUT_GET( Element, Font2, VARIANT*, val )
//	PUT_GET( Element, Font3, VARIANT*, val )
	PUT_GET( Element, IsImage, BOOL, val )
	PUT_GET( Element, Start, double, val )
	PUT_GET( Element, Duration, double, val )
	PUT_GET( Element, HideRightTarget, BOOL, val )
	PUT_GET( Element, HideWrongTarget, BOOL, val )
	PUT_GET( Element, HideIfAnyRightTarget, BOOL, val )
	PUT_GET( Element, HideIfAnyWrongTarget, BOOL, val )
	PUT_GET( Element, RightTarget, CString, val.AllocSysString() );
	PUT_GET( Element, WrongTarget, CString, val.AllocSysString() );
	PUT_GET( Element, CatID, CString, val.AllocSysString() );
	PUT_GET( Element, Animate, BOOL, val );
	PUT_GET( Element, AnimationRate, double, val );
	PUT_GET( Element, AnimationFile, CString, val.AllocSysString() );
	PUT_GET( Element, AnimationRandom, BOOL, val )
	PUT_GET( Element, AnimationGenerate, BOOL, val )
	PUT_GET( Element, AnimationSubjID, CString, val.AllocSysString() );

	// Generate the X,Y,Z matrices - return value = # of points
	// responsibility of caller to delete x,y,z
	int GenerateXYZ( /*[out, retval]*/ double** x,
					 /*[out, retval]*/ double** y,
					 /*[out, retval]*/ double** z,
					 /*[in]*/ BOOL fDelete = FALSE );
	static int GenerateXYZ( /*[in]*/ IElement*,
							/*[out, retval]*/ double** x,
							/*[out, retval]*/ double** y,
							/*[out, retval]*/ double** z,
							/*[in]*/ BOOL fDelete = FALSE );
	// Generate the X,Y,Z matrices with error margin- return value = # of points
	// responsibility of caller to delete x,y,z
	int GenerateXYZWithError( /*[out, retval]*/ double** x,
							  /*[out, retval]*/ double** y,
							  /*[out, retval]*/ double** z,
							  /*[in]*/ BOOL fDelete = FALSE );
	static int GenerateXYZWithError( /*[in]*/ IElement*,
									 /*[out, retval]*/ double** x,
									 /*[out, retval]*/ double** y,
									 /*[out, retval]*/ double** z,
									 /*[in]*/ BOOL fDelete = FALSE );

	// View/Chart the results
	void ViewData();
	static void ViewData( IElement* );
	void ChartData();
	static void ChartData( IElement* );

	// Importing/Exporting
	BOOL Export( CMemFile* pFile, BOOL fFirst = FALSE );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );
	// Data validation
	virtual BOOL ValidateData( CStringList* pListErrors, BOOL fFirstOnly = FALSE, BOOL fNotify = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();

	// file generation
	void GenerateAnimation( CStdioFile* );
	static void GenerateAnimation( IElement*, CStdioFile* );
	void GenerateTargets( CStdioFile* );
	static void GenerateTargets( IElement*, CStdioFile* );
	void GenerateBehavior( CStdioFile* );
	static void GenerateBehavior( IElement*, CStdioFile* );
	void GenerateDecorations( CStdioFile*, int nLines = -1 );
	static void GenerateDecorations( IElement*, CStdioFile*, int nLines = -1 );
	BOOL GenerateFile( CString& szFile, int& nLines );
	static BOOL GenerateFile( IElement*, CString& szFile, int& nLines );
	BOOL GenerateFiles();
	static BOOL GenerateFiles( IElement* );
	void GetDataFile( CString& szFile );
	static void GetDataFile( IElement*, CString& szFile );
	static void GetDataFileFromID( CString szID, CString& szFile );
	void GetDecFile( CString& szFile );
	static void GetDecFile( IElement*, CString& szFile );
	static void GetDecFileFromID( CString szID, CString& szFile );
	void GetBehaviorFile( CString& szFile );
	static void GetBehaviorFile( IElement*, CString& szFile );
	static void GetBehaviorFileFromID( CString szID, CString& szFile );
	void GetTargetFile( CString& szFile );
	static void GetTargetFile( IElement*, CString& szFile );
	static void GetTargetFileFromID( CString szID, CString& szFile );
	void GetAnimationsFile( CString& szFile );
	static void GetAnimationsFile( IElement*, CString& szFile );
	static void GetAnimationsFileFromID( CString szID, CString& szFile );
protected:
	static void GetBaseFile( CString& szBase );
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _ELEMENTS_H_

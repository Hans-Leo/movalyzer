// QuestionnaireFullDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "QuestionnaireFullDlg.h"
#include "QuestionnaireDlg.h"
#include "..\DataMod\DataMod.h"
#include "MQuests.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireFullDlg dialog

BEGIN_MESSAGE_MAP(QuestionnaireFullDlg, CBCGPDialog)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_UP, OnClickBnUp)
	ON_BN_CLICKED(IDC_BN_DOWN, OnClickBnDown)
END_MESSAGE_MAP()

QuestionnaireFullDlg::QuestionnaireFullDlg(IMQuestionnaire* pQ, BOOL fMaster, CWnd* pParent)
	: CBCGPDialog(QuestionnaireFullDlg::IDD, pParent), m_pQ( pQ ), m_fMaster( fMaster )
{
}

QuestionnaireFullDlg::~QuestionnaireFullDlg()
{
	if( m_lstH ) delete m_lstH;
	if( m_lstID ) delete m_lstID;
	if( m_lstQ ) delete m_lstQ;
	if( m_lstS ) delete m_lstS;
	if( m_lstI ) delete m_lstI;
	if( m_lstP ) delete m_lstP;
	if( m_pListN ) delete m_pListN;
	if( m_pListCH ) delete m_pListCH;
}

void QuestionnaireFullDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_UP, m_bnUp);
	DDX_Control(pDX, IDC_BN_DOWN, m_bnDown);
}

void QuestionnaireFullDlg::OnCancel()
{
	if( m_lstH->GetCount() > 0 )
	{
		if( BCGPMessageBox( _T("Cancel and lose all changes?"), MB_YESNO ) == IDNO )
			return;
	}
	CBCGPDialog::OnCancel();
}

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireFullDlg message handlers

void QuestionnaireFullDlg::OnClickBnAdd() 
{
	// selected item
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) nIdx = 0;

	// add/edit dialog
	QuestionnaireDlg d( m_lstH, m_lstQ, m_lstID, m_lstS, m_lstI, m_lstP,
						m_pListN, m_pListCH, FALSE, nIdx, m_fMaster, this );
	d.DoModal();

	// fill list
	FillList();

	// select item & ensure visible
	if( nIdx >= 0 )
	{
		LVITEM lvi;
		lvi.mask = LVIF_STATE;
		lvi.state = LVIS_SELECTED;
		lvi.stateMask = (UINT)-1;
		m_List.SetItemState( nIdx, &lvi );
		m_List.EnsureVisible( nIdx, FALSE );
	}
}

void QuestionnaireFullDlg::OnClickBnDel() 
{
	// selected item
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	// if already deleted, return
	POSITION pos = m_lstS->FindIndex( nIdx );
	CString szS = pos ? m_lstS->GetAt( pos ) : _T("");
	if( ( szS == _T("") ) || ( szS == Q_STATE_DEL ) ) return;

	// ask user
	if( BCGPMessageBox( IDS_DEL_SURE, MB_YESNO ) == IDNO ) return;

	// change to delete flag
	m_lstS->SetAt( pos, Q_STATE_DEL );

	// fill list
	FillList();

	m_List.SetFocus();

	// select item & ensure visible
	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED | LVIS_FOCUSED;
	lvi.stateMask = (UINT)-1;
	if( nIdx < ( m_List.GetItemCount() - 1 ) ) nIdx++;
	m_List.SetItemState( nIdx, &lvi );
	m_List.EnsureVisible( nIdx, FALSE );
}

void QuestionnaireFullDlg::OnClickBnEdit() 
{
	// selected item
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	// current state
	POSITION pos = m_lstS->FindIndex( nIdx );
	CString szS = pos ? m_lstS->GetAt( pos ) : _T("");
	if( szS == _T("") ) return;

	// add/edit dialog
	QuestionnaireDlg d( m_lstH, m_lstQ, m_lstID, m_lstS, m_lstI, m_lstP,
						m_pListN, m_pListCH, TRUE, nIdx, m_fMaster, this );
	if( d.DoModal() == IDOK )
	{
		// if already set to new, leave as new, otherwise set to edit
		// if was deleted, change state to edit (caller will have to determine if it was originally new or not)
		if( szS != Q_STATE_NEW ) m_lstS->SetAt( pos, Q_STATE_DIFF );

		// fill list
		FillList();

		// select item & ensure visible
		LVITEM lvi;
		lvi.mask = LVIF_STATE;
		lvi.state = LVIS_SELECTED;
		lvi.stateMask = (UINT)-1;
		m_List.SetItemState( nIdx, &lvi );
		m_List.EnsureVisible( nIdx, FALSE );
	}
}

BOOL QuestionnaireFullDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// button images
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnDel.SetImage( IDB_DEL, IDB_DEL, IDB_DEL_DIS );
	m_bnEdit.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnUp.SetImage( IDB_UP, IDB_UP, IDB_UP );
	m_bnDown.SetImage( IDB_DOWN, IDB_DOWN, IDB_DOWN );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PROP, _T("Modify Question") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_NEW, _T("Insert Question") );
	pWnd = GetDlgItem( IDC_BN_DEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL, _T("Delete Question") );
	pWnd = GetDlgItem( IDC_BN_UP );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEUP, _T("Up") );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEDOWN, _T("Down") );
	pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_QUESTLIST, _T("Question List") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CANCEL, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_EXCEL );
	if( pWnd ) pWnd->ShowWindow( SW_HIDE );

	// Image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_List.SetImageList( &m_ImageList, LVSIL_SMALL );

	// List size (width)
	RECT rect;
	m_List.GetClientRect( &rect );
	int nWidth = rect.right - rect.left;

	// Column Headers
	m_List.InsertColumn( 0, _T("ID"), LVCFMT_LEFT, 60, 0 );
	m_List.InsertColumn( 1, _T("Private"), LVCFMT_LEFT, 50, 1 );
	m_List.InsertColumn( 2, _T("Numeric"), LVCFMT_LEFT, 60, 2 );
	m_List.InsertColumn( 3, _T("Header"), LVCFMT_LEFT, 60, 3 );
	m_List.InsertColumn( 4, _T("Topic/Question"), LVCFMT_LEFT, nWidth - 250, 4 );

	// Do a repair on questionnaire for item nums (just in case)
	{
	MQuestionnaires q;
	q.Repair();
	}

	// Fill Lists
	m_lstH = new CStringList;
	m_lstQ = new CStringList;
	m_lstID = new CStringList;
	m_lstS = new CStringList;
	m_lstI = new CStringList;
	m_lstP = new CStringList;
	m_pListN = new CStringList;
	m_pListCH = new CStringList;

	if( m_pQ )
	{
		HRESULT hr = S_OK;
		BSTR bstrQ = NULL, bstrID = NULL, bstrCH = NULL;
		CString szQ, szID, szH, szI, szS = Q_STATE_SAME, szP, szN, szCH;
		CStringList Q, ID, H, I, P, N, CH;
		BOOL fHeader = FALSE, fPrivate = FALSE, fNumeric = FALSE;
		short nPos = 0;

		hr = m_pQ->ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			m_pQ->get_IsHeader( &fHeader );
			m_pQ->get_IsPrivate( &fPrivate );
			m_pQ->get_ItemNum( &nPos );
			m_pQ->get_Question( &bstrQ );
			m_pQ->get_ID( &bstrID );
			m_pQ->get_IsNumeric( &fNumeric );
			m_pQ->get_ColumnHeader( &bstrCH );

			szQ = bstrQ;
			szID = bstrID;
			szCH = bstrCH;
			szI.Format( _T("%d"), nPos );
			szH = fHeader ? _T("TRUE") : _T("");
			szP = fPrivate ? _T("TRUE") : _T("");
			szN = fNumeric ? _T("TRUE") : _T("");

			H.AddTail( szH );
			P.AddTail( szP );
			Q.AddTail( szQ );
			ID.AddTail( szID );
			I.AddTail( szI );
			N.AddTail( szN );
			CH.AddTail( szCH );
			m_lstS->AddTail( szS );

			hr = m_pQ->GetNext();
		}

		// Sort
		int nCnt = (int)H.GetCount();
		CString szTemp = _T("");
		for( int i = 0; i < nCnt; i++ )
		{
			m_lstH->AddTail( szTemp );
			m_lstP->AddTail( szTemp );
			m_lstQ->AddTail( szTemp );
			m_lstID->AddTail( szTemp );
			m_lstI->AddTail( szTemp );
			m_pListN->AddTail( szTemp );
			m_pListCH->AddTail( szTemp );
		}
		POSITION posH = H.GetHeadPosition();
		POSITION posP = P.GetHeadPosition();
		POSITION posQ = Q.GetHeadPosition();
		POSITION posID = ID.GetHeadPosition();
		POSITION posI = I.GetHeadPosition();
		POSITION posN = N.GetHeadPosition();
		POSITION posCH = CH.GetHeadPosition();
		POSITION pos = NULL;
		while( posI )
		{
			szI = I.GetNext( posI );
			szH = H.GetNext( posH );
			szP = P.GetNext( posP );
			szQ = Q.GetNext( posQ );
			szID = ID.GetNext( posID );
			szN = N.GetNext( posN );
			szCH = CH.GetNext( posCH );

			nPos = atoi( szI ) - 1;

			pos = m_lstI->FindIndex( nPos );
			if( pos ) m_lstI->SetAt( pos, szI );
			pos = m_lstH->FindIndex( nPos );
			if( pos ) m_lstH->SetAt( pos, szH );
			pos = m_lstP->FindIndex( nPos );
			if( pos ) m_lstP->SetAt( pos, szP );
			pos = m_lstQ->FindIndex( nPos );
			if( pos ) m_lstQ->SetAt( pos, szQ );
			pos = m_lstID->FindIndex( nPos );
			if( pos ) m_lstID->SetAt( pos, szID );
			pos = m_pListN->FindIndex( nPos );
			if( pos ) m_pListN->SetAt( pos, szN );
			pos = m_pListCH->FindIndex( nPos );
			if( pos ) m_pListCH->SetAt( pos, szCH );
		}

		FillList();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL QuestionnaireFullDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void QuestionnaireFullDlg::FillList()
{
	m_List.DeleteAllItems();

	POSITION posH = m_lstH->GetHeadPosition();
	POSITION posP = m_lstP->GetHeadPosition();
	POSITION posQ = m_lstQ->GetHeadPosition();
	POSITION posID = m_lstID->GetHeadPosition();
	POSITION posS = m_lstS->GetHeadPosition();
	POSITION posN = m_pListN->GetHeadPosition();
	POSITION posCH = m_pListCH->GetHeadPosition();
	CString szH, szQ, szID, szS, szP, szTemp, szN, szCH;
	int nItem = 0, nImage = 5;

	while( posH )
	{
		szH = m_lstH->GetNext( posH );
		szP = m_lstP->GetNext( posP );
		szQ = m_lstQ->GetNext( posQ );
		szID = m_lstID->GetNext( posID );
		szS = m_lstS->GetNext( posS );
		szN = m_pListN->GetNext( posN );
		szCH = m_pListCH->GetNext( posCH );

		if( szS == Q_STATE_SAME ) nImage = 5;
		else if( szS == Q_STATE_NEW ) nImage = 12;
		else if( szS == Q_STATE_DEL ) nImage = 6;
		else if( szS == Q_STATE_DIFF ) nImage = 13;

		nItem = m_List.InsertItem( nItem, szID, nImage );
		if( szH == _T("") )
		{
			szTemp = _T("     ");
			szTemp += szQ;
			m_List.SetItem( nItem, 1, LVIF_TEXT, szP, 0, 0, 0, NULL );
			m_List.SetItem( nItem, 2, LVIF_TEXT, szN, 0, 0, 0, NULL );
			m_List.SetItem( nItem, 3, LVIF_TEXT, szCH, 0, 0, 0, NULL );
			m_List.SetItem( nItem, 4, LVIF_TEXT, szTemp, 0, 0, 0, NULL );
		}
		else
		{
			m_List.SetItem( nItem, 1, LVIF_TEXT, _T(""), 0, 0, 0, NULL );
			m_List.SetItem( nItem, 2, LVIF_TEXT, _T(""), 0, 0, 0, NULL );
			m_List.SetItem( nItem, 3, LVIF_TEXT, _T(""), 0, 0, 0, NULL );
			m_List.SetItem( nItem, 4, LVIF_TEXT, szQ, 0, 0, 0, NULL );
		}

		nItem++;
	}
}

void QuestionnaireFullDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnClickBnEdit();

	*pResult = 0;
}

void QuestionnaireFullDlg::MoveItem( int nDir )
{
	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL;

	// selected item
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( ( nIdx < 0 ) || ( nIdx > m_List.GetItemCount() ) ) { Beep( 800, 100 ); return; }
	// if move up & 1st item, return
	if( ( nDir == -1 ) && ( nIdx == 0 ) ) { Beep( 800, 100 ); return; }
	// if move down & last item, return
	else if( ( nDir == 1 ) && ( nIdx == m_lstQ->GetCount() ) ) { Beep( 800, 100 ); return; }

	// STATE
	// position of selected item
	pos1 = m_lstS->FindIndex( nIdx );
	// position of previous item
	pos2 = m_lstS->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_lstS->GetAt( pos1 );
	// value of previous item
	szItem2 = m_lstS->GetAt( pos2 );
	// update item state values
	// ...if was new, leave new.
	// ...if was del, leave del.
	// ...else diff
	if( ( szItem1 != Q_STATE_NEW ) && ( szItem1 != Q_STATE_DEL ) ) szItem1 = Q_STATE_DIFF;
	if( ( szItem2 != Q_STATE_NEW ) && ( szItem2 != Q_STATE_DEL ) ) szItem2 = Q_STATE_DIFF;
	// alter values
	m_lstS->SetAt( pos2, szItem1 );
	m_lstS->SetAt( pos1, szItem2 );

	// HEADER
	// position of selected item
	pos1 = m_lstH->FindIndex( nIdx );
	// position of previous item
	pos2 = m_lstH->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_lstH->GetAt( pos1 );
	// value of previous item
	szItem2 = m_lstH->GetAt( pos2 );
	// swap values
	m_lstH->SetAt( pos2, szItem1 );
	m_lstH->SetAt( pos1, szItem2 );

	// PRIVATE
	// position of selected item
	pos1 = m_lstP->FindIndex( nIdx );
	// position of previous item
	pos2 = m_lstP->FindIndex( nIdx + nDir );
	// value of selected item
	szItem1 = m_lstP->GetAt( pos1 );
	// value of previous item
	szItem2 = m_lstP->GetAt( pos2 );
	// swap values
	m_lstP->SetAt( pos2, szItem1 );
	m_lstP->SetAt( pos1, szItem2 );

	// NUMERIC
	// position of selected item
	pos1 = m_pListN->FindIndex( nIdx );
	// position of previous item
	pos2 = m_pListN->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_pListN->GetAt( pos1 );
	// value of previous item
	szItem2 = m_pListN->GetAt( pos2 );
	// swap values
	m_pListN->SetAt( pos2, szItem1 );
	m_pListN->SetAt( pos1, szItem2 );

	// QUESTION
	// position of selected item
	pos1 = m_lstQ->FindIndex( nIdx );
	// position of previous item
	pos2 = m_lstQ->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_lstQ->GetAt( pos1 );
	// value of previous item
	szItem2 = m_lstQ->GetAt( pos2 );
	m_lstQ->SetAt( pos2, szItem1 );
	// swap values
	m_lstQ->SetAt( pos1, szItem2 );

	// COL HEADER
	// position of selected item
	pos1 = m_pListCH->FindIndex( nIdx );
	// position of previous item
	pos2 = m_pListCH->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_pListCH->GetAt( pos1 );
	// value of previous item
	szItem2 = m_pListCH->GetAt( pos2 );
	m_pListCH->SetAt( pos2, szItem1 );
	// swap values
	m_pListCH->SetAt( pos1, szItem2 );

	// ID
	// position of selected item
	pos1 = m_lstID->FindIndex( nIdx );
	// position of previous item
	pos2 = m_lstID->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_lstID->GetAt( pos1 );
	// value of previous item
	szItem2 = m_lstID->GetAt( pos2 );
	// swap values
	m_lstID->SetAt( pos2, szItem1 );
	m_lstID->SetAt( pos1, szItem2 );

	// ITEM NUM (SEQUENCE)
	// position of selected item
	pos1 = m_lstI->FindIndex( nIdx );
	// position of previous item
	pos2 = m_lstI->FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) { Beep( 800, 100 ); return; }
	// value of selected item
	szItem1 = m_lstI->GetAt( pos1 );
	// value of previous item
	szItem2 = m_lstI->GetAt( pos2 );
	// swap values
	m_lstI->SetAt( pos2, szItem1 );
	m_lstI->SetAt( pos1, szItem2 );

	// refill list
	FillList();

	// select & ensure visible selected item
	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED;
	lvi.stateMask = (UINT)-1;
	m_List.SetItemState( nIdx + nDir, &lvi );
	m_List.EnsureVisible( nIdx + nDir, FALSE );
}

void QuestionnaireFullDlg::OnClickBnUp() 
{
	MoveItem( -1 );
}

void QuestionnaireFullDlg::OnClickBnDown() 
{
	MoveItem( 1 );
}

BOOL QuestionnaireFullDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("questionnaire.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

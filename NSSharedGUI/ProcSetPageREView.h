#pragma once

// ProcSetPageREView.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREView dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageREView : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageREView)

// Construction
public:
	ProcSetPageREView( IProcSetting* pS = NULL );
	~ProcSetPageREView();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_RE_VIEW };
	BOOL			m_fExt;
	BOOL			m_fCon;
	BOOL			m_fCon2;
	BOOL			m_fExt2;
	CColoredButton	m_chkExt;
	CColoredButton	m_chkExt2;
	CColoredButton	m_chkCon;
	CColoredButton	m_chkCon2;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

// ImportDialog.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ImportDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString ImportDialog::_szFile = _T("");

/////////////////////////////////////////////////////////////////////////////
// ImportDialog dialog

BEGIN_MESSAGE_MAP(ImportDialog, CBCGPDialog)
	ON_WM_HELPINFO()
	ON_EN_UPDATE( IDC_EDIT_FILE, OnUpdateFile )
END_MESSAGE_MAP()

ImportDialog::ImportDialog(CWnd* pParent)
	: CBCGPDialog(ImportDialog::IDD, pParent) , m_fFiles( FALSE ) ,
	  m_fTrials( FALSE ), m_fSummary( FALSE ), m_fZip( FALSE )
{
	m_szFile = _T("");
}

void ImportDialog::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_FILE, m_szFile);
	DDX_Control(pDX, IDC_EDIT_FILE, m_wndFolderEdit);
	DDX_Check(pDX, IDC_CHK_FILES, m_fFiles);
	DDX_Check(pDX, IDC_CHK_TRIALS, m_fTrials);
	DDX_Check(pDX, IDC_CHK_INFO, m_fSummary );
}

/////////////////////////////////////////////////////////////////////////////
// ImportDialog message handlers

BOOL ImportDialog::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify import file location here."), _T("Import File") );
	pWnd = GetDlgItem( IDC_CHK_FILES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Import the test/experiment support files."), _T("Support Files") );
	pWnd = GetDlgItem( IDC_CHK_TRIALS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Import all of the patient/subject trials."), _T("Trials") );
	pWnd = GetDlgItem( IDC_CHK_INFO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Show summary information for this import (eg., user, date, device info)."), _T("Summary") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Import specified test/experiment."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Cancel import."), _T("Cancel") );

	// trials & files are disabled until a file is selected to check for their existence
	pWnd = GetDlgItem( IDC_CHK_FILES );
	pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_CHK_TRIALS );
	pWnd->EnableWindow( FALSE );

	// initializer location to default
	m_wndFolderEdit.EnableFileBrowseButton( _T("mef"), _T("MEF Files (*.mef)|*.mef|ZIP Files (*.zip)|*.zip|EXP Files (*.exp)|*.exp||") );
	::GetAllUserPath( m_szFile );
	m_szFile += _T("Export\\*.mef");
	if( _szFile == _T("") ) _szFile = m_szFile;
	else m_szFile = _szFile;
	UpdateData( FALSE );

	// OK button is disabled initially
	pWnd = GetDlgItem( IDOK );
	pWnd->EnableWindow( FALSE );

	// set focus to edit control enter key
	m_wndFolderEdit.OnBrowse();

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ImportDialog::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void ImportDialog::Browse()
{
	CString szExt = m_szFile.Right( 3 );
	szExt.MakeUpper();

	// ensure it's not a wildcard
	const CString strInvalidChars = _T("*?<>|");
	if( m_szFile.FindOneOf( strInvalidChars ) >= 0 ) return;

	// verify file exists
	CFileFind ff;
	if( !ff.FindFile( m_szFile ) ) return;
	if( ff.FindNextFile() ) return;

	CWaitCursor crs;

	// if ZIP file (version >= 4.3), MEF file (>=5.0)...
	m_fZip = FALSE;
	if( szExt == _T("MEF") || ( szExt == _T("ZIP") ) )
	{
		m_fZip = TRUE;
		// get files in zip
		CStringList lstFiles;
		::ZIPFileList( m_szFile, lstFiles );
		crs.Restore();
		// check to be sure it at least has the EXP file
		CString szFile;
		BOOL fExp = FALSE;
		POSITION pos = lstFiles.GetHeadPosition();
		while( pos )
		{
			szFile = lstFiles.GetNext( pos );
			szFile.MakeUpper();
			if( szFile.Right( 3 ) == _T("EXP") )
				fExp = TRUE;
			else if( szFile.Find( _T("TRIALS.ZIP") ) != -1 )
				m_fTrials = TRUE;
			else if( szFile.Find( _T("SETTINGS.ZIP") ) != -1 )
				m_fFiles = TRUE;
		}
		// if no EXP file, cannot do anything
		if( !fExp )
		{
			BCGPMessageBox( _T("This does not appear to be a valid export file.\nCannot continue.") );
			return;
		}
	}

	m_szFileName = m_szFile;
	int nPos = m_szFileName.ReverseFind( '.' );
	if( nPos != -1 )
	{
		m_szFileName = m_szFileName.Left( nPos );
		nPos = m_szFileName.ReverseFind( '\\' );
		if( nPos != -1 ) m_szFileName = m_szFileName.Mid( nPos + 1 );
	}

	if( !m_fZip )
	{
		// check if trials & files zips are present
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szPath = m_szFile.Left( nPos );
		nPos = m_szFileName.ReverseFind( '.' );
		if( nPos != -1 ) m_szFileName = m_szFileName.Left( nPos );
		CString szTemp;
		CString szFiles = szPath;
		szTemp.Format( _T("\\%s-SETTINGS.ZIP"), m_szFileName );
		szFiles += szTemp;
		CFileFind ff;
		if( ff.FindFile( szFiles ) ) m_fFiles = TRUE;
		CString szTrials = szPath;
		szTemp.Format( _T("\\%s-TRIALS.ZIP"), m_szFileName );
		szTrials += szTemp;
		if( ff.FindFile( szTrials ) ) m_fTrials = TRUE;
	}

	// enable accordingly
	CWnd* pWnd = GetDlgItem( IDC_CHK_FILES );
	pWnd->EnableWindow( m_fFiles );
	pWnd = GetDlgItem( IDC_CHK_TRIALS );
	pWnd->EnableWindow( m_fTrials );
	pWnd = GetDlgItem( IDOK );
	pWnd->EnableWindow( TRUE );

	UpdateData( FALSE );
}

void ImportDialog::OnUpdateFile()
{
	UpdateData( TRUE );
	Browse();
}

void ImportDialog::OnOK()
{
	UpdateData( TRUE );

	if( m_szFile == _T("") )
	{
		BCGPMessageBox( _T("No file has been specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szFile[ m_szFile.GetLength() - 1 ] == '\\' )
		m_szFile = m_szFile.Left( m_szFile.GetLength() - 1 );
	CString szTest = m_szFile;
	szTest.MakeUpper();
	int nPos = szTest.Find( _T("\\CON\\") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Path/Import cannot contain \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Path/Import cannot contain \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CFileFind ff;
	if( ( szTest != _T("C:") ) && ( szTest != _T("C:\\") ) &&
		( !ff.FindFile( szTest ) ) )
	{
		BCGPMessageBox( _T("File does not exist.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( ff.FindNextFile() ) return;

	nPos = m_szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		_szFile = m_szFile.Left( nPos );
		_szFile += _T("\\*.mef");
	}

	CBCGPDialog::OnOK();
}

BOOL ImportDialog::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

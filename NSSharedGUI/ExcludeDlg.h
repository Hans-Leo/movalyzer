// ExcludeDlg.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ExcludeDlg dialog
#define	LIST_DIRECTION_NONE		0
#define LIST_DIRECTION_DOWN		1
#define LIST_DIRECTION_UP		2

class ExcludeListBox : public CBCGPListBox
{
public:
	ExcludeListBox() : CBCGPListBox(), 	m_fMouseDown( FALSE ), m_nLastItem( -1 ),
					   m_nDir( LIST_DIRECTION_NONE )
	{}
	virtual ~ExcludeListBox() {}

	BOOL	m_fMouseDown;
	UINT	m_nLastItem;
	UINT	m_nDir;

	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class AFX_EXT_CLASS ExcludeDlg : public CBCGPDialog
{
// Construction
public:
	ExcludeDlg( CString szExpID, CWnd* pParent = NULL,
				BOOL fHeaders = FALSE, BOOL fQuests = FALSE,
				BOOL fTrials = FALSE, CString szGrpID = _T(""),
				CString szSubjID = _T("") );

// Dialog Data
	enum { IDD = IDD_EXCLUDE };
	ExcludeListBox	m_List;
	CString			m_szExpID;
	short			m_nType;
	CString			m_szGrpID;
	CString			m_szSubjID;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstExclude;
	BOOL			m_fHeaders;
	BOOL			m_fQuests;
	BOOL			m_fTrials;
	CString			m_szDesc;
	CString			m_szExtFile;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnSelectall();
	afx_msg void OnBnSelectnone();
	afx_msg void OnOK2();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

#pragma once

// ProcSetPageRETrial.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRETrial dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageRETrial : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageRETrial)

// Construction
public:
	ProcSetPageRETrial( IProcSetting* pS = NULL );
	~ProcSetPageRETrial();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_RE_TRIALS };
	double			m_dStart;
	double			m_dTimeout;
	double			m_dTicks;
	double			m_dBetween;
	BOOL			m_fProc;
	// HACK ALERT (see globals.h)
	// we have 3 options: real size, maximize, and do nothing, but are only using 2 variables (now)
	BOOL			m_fMaximize;
	BOOL			m_fFullScreen;
	BOOL			m_fAsis;
	CColoredEdit	m_editStart;
	CColoredEdit	m_editTimeout;
	CColoredEdit	m_editTicks;
	CColoredEdit	m_editBetween;
	CColoredButton	m_chkProc;
	CColoredButton	m_chkFullScreen;
	CColoredButton	m_rdoRealSize;
	CColoredButton	m_rdoMax;
	CColoredButton	m_rdoAsis;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnClickedSize();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

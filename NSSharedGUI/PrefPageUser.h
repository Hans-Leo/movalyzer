#if !defined(AFX_PREFPAGEUSER_H__4DAF3A05_EB71_45DC_8E80_C88A455D3895__INCLUDED_)
#define AFX_PREFPAGEUSER_H__4DAF3A05_EB71_45DC_8E80_C88A455D3895__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrefPageUser.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// PrefPageUser dialog

class Preferences;

class AFX_EXT_CLASS PrefPageUser : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(PrefPageUser)

// Construction
public:
	PrefPageUser( Preferences* pPref = NULL );
	~PrefPageUser() {}

// Dialog Data
	enum { IDD = IDD_PAGE_PREF_USER };
	CString			m_szID;
	CString			m_szDesc;
	Preferences*	m_pPref;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	BOOL			m_fCancel;
	BOOL			m_fNew;
	CString			m_szSiteID;
	CString			m_szSiteDesc;
	BOOL			m_fWarned;

// Overrides
public:
	virtual BOOL OnApply();
	virtual void OnCancel();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnKillfocusEditId();
	afx_msg void OnChangeEditId();
	afx_msg void OnBnPass();
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_PREFPAGEUSER_H__4DAF3A05_EB71_45DC_8E80_C88A455D3895__INCLUDED_)

// ConditionPageConsis.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ConditionPageConsis.h"
#include "ConditionSheet.h"
#include "Conditions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	LEX_ALLOW		_T("acdehijlmnotuvy-/|\\_?}].@")

/////////////////////////////////////////////////////////////////////////////
// ConditionPageConsis property page

IMPLEMENT_DYNCREATE(ConditionPageConsis, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageConsis, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ConditionPageConsis::ConditionPageConsis( INSCondition* pC, BOOL fNew )
	: CBCGPPropertyPage(ConditionPageConsis::IDD), m_pC( pC )
{
	m_szLex = _T("");
	m_nMin = 30;
	m_nMax = 33;
	m_dStrokeLength = 0.0;
	m_dRangeLength = 0.0;
	m_dStrokeDirection = 0.0;
	m_dRangeDirection = 0.0;
	m_nSkip = 0;
	m_fFlagBadTarget = FALSE;

	m_editMin.SetDefaultValue( m_nMin );
	m_editMax.SetDefaultValue( m_nMax );
	m_editStrokeLength.SetDefaultValue( m_dStrokeLength );
	m_editRangeLength.SetDefaultValue( m_dRangeLength );
	m_editStrokeDir.SetDefaultValue( m_dStrokeDirection );
	m_editRangeDir.SetDefaultValue( m_dRangeDirection );
	m_editSkip.SetDefaultValue( m_nSkip );
	m_chkFlag.SetDefaultAsOff( !m_fFlagBadTarget );

	if( !fNew && pC )
	{
		short nVal;
		NSConditions::GetLex( pC, m_szLex );
		NSConditions::GetStrokeMin( pC, nVal );
		m_nMin = nVal;
		NSConditions::GetStrokeMax( pC, nVal );
		m_nMax = nVal;
		NSConditions::GetStrokeLength( pC, m_dStrokeLength );
		NSConditions::GetRangeLength( pC, m_dRangeLength );
		NSConditions::GetStrokeDirection( pC, m_dStrokeDirection );
		NSConditions::GetRangeDirection( pC, m_dRangeDirection );
		NSConditions::GetStrokeSkip( pC, nVal );
		m_nSkip = nVal;
		NSConditions::GetFlagBadTarget( pC, m_fFlagBadTarget );
	}
}

ConditionPageConsis::~ConditionPageConsis()
{
}

void ConditionPageConsis::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_LEX, m_szLex);
	DDX_Text(pDX, IDC_EDIT_MIN, m_nMin);
	DDX_Text(pDX, IDC_EDIT_MAX, m_nMax);
	DDX_Text(pDX, IDC_EDIT_LEN, m_dStrokeLength );
	DDX_Text(pDX, IDC_EDIT_RANGE, m_dRangeLength );
	DDX_Text(pDX, IDC_EDIT_DIR, m_dStrokeDirection );
	DDX_Text(pDX, IDC_EDIT_RANGE2, m_dRangeDirection );
	DDX_Text(pDX, IDC_EDIT_SKIP, m_nSkip);
	DDX_Check(pDX, IDC_CHK_ORDER, m_fFlagBadTarget);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);

	DDX_Control(pDX, IDC_EDIT_MIN, m_editMin);
	DDX_Control(pDX, IDC_EDIT_MAX, m_editMax);
	DDX_Control(pDX, IDC_EDIT_LEN, m_editStrokeLength);
	DDX_Control(pDX, IDC_EDIT_RANGE, m_editRangeLength);
	DDX_Control(pDX, IDC_EDIT_DIR, m_editStrokeDir);
	DDX_Control(pDX, IDC_EDIT_RANGE2, m_editRangeDir);
	DDX_Control(pDX, IDC_EDIT_SKIP, m_editSkip);
	DDX_Control(pDX, IDC_CHK_ORDER, m_chkFlag);
}

/////////////////////////////////////////////////////////////////////////////
// ConditionPageConsis message handlers

BOOL ConditionPageConsis::OnApply() 
{
	if( !m_pC ) return FALSE;

	UpdateData( TRUE );

	// stroke description (lex)
	CString szLexAllow = LEX_ALLOW;
	CString szC;
	for( int i = 0; i < m_szLex.GetLength(); i++ )
	{
		szC = m_szLex.GetAt( i );
		if( szLexAllow.Find( szC ) == -1 )
		{
			((ConditionSheet*)GetParent())->SetActivePage( 2 );
			CString szMsg;
			szMsg.Format( _T("Only the following characters are allowed in the stroke description: %s"), szLexAllow );
			BCGPMessageBox( szMsg );
			return FALSE;
		}
	}

	// Ranges
	if( ( m_nMin < 1 ) || ( m_nMin > 1000 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Minimum strokes must be between 1 and 1000.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MIN );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nMax < 1 ) || ( m_nMax > 1000 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Maximum strokes must be between 1 and 1000.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MAX );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nSkip < 0 ) || ( m_nSkip > 1000 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Strokes to skip must be between 0 and 1000.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SKIP );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dStrokeDirection < -6.2832 ) || ( m_dStrokeDirection > 6.2832 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Stroke direction must be between -6.2832 and 6.2832.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DIR );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dRangeDirection < -6.2832 ) || ( m_dRangeDirection > 6.2832 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Range direction must be between -6.2832 and 6.2832.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_RANGE2 );
		pWnd->SetFocus();
		return FALSE;
	}

	// strokes
	if( m_nMin > m_nMax )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Maximum strokes summarized cannot be greater than maximum strokes acceptable.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MIN );
		pWnd->SetFocus();
		return FALSE;
	}
	// skip < min
	if( m_nSkip >= m_nMin )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Strokes to skip must be less than Minimum strokes.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SKIP );
		pWnd->SetFocus();
		return FALSE;
	}

	// Lengths
	if( m_dStrokeLength < 0. )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Stroke length cannot be negative.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LEN );
		pWnd->SetFocus();
		return FALSE;
	}
	if( m_dRangeLength < 0. )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 2 );
		BCGPMessageBox( _T("Range length cannot be negative.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_RANGE );
		pWnd->SetFocus();
		return FALSE;
	}

	m_pC->put_Lex( m_szLex.AllocSysString() );
	m_pC->put_StrokeMin( m_nMin );
	m_pC->put_StrokeMax( m_nMax );
	m_pC->put_StrokeSkip( m_nSkip );
	m_pC->put_StrokeLength( m_dStrokeLength );
	m_pC->put_StrokeDirection( m_dStrokeDirection );
	m_pC->put_RangeLength( m_dRangeLength );
	m_pC->put_RangeDirection( m_dRangeDirection );
	m_pC->put_FlagBadTarget( m_fFlagBadTarget );

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageConsis::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageConsis::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button image
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_LEX );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_STROKE, _T("Stroke Pattern") );
	pWnd = GetDlgItem( IDC_EDIT_MIN );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_MIN, _T("Maximum Strokes Summarized") );
	pWnd = GetDlgItem( IDC_EDIT_MAX );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_MAX, _T("Maximum Strokes Acceptable") );
	pWnd = GetDlgItem( IDC_EDIT_LEN );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_LEN, _T("Stroke Length") );
	pWnd = GetDlgItem( IDC_EDIT_RANGE );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_RANGE, _T("Length Error") );
	pWnd = GetDlgItem( IDC_EDIT_DIR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_DIR, _T("Stroke Direction") );
	pWnd = GetDlgItem( IDC_EDIT_RANGE2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_RANGEDIR, _T("Direction Error") );
	pWnd = GetDlgItem( IDC_EDIT_SKIP );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_SKIP, _T("Strokes to Discard") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_CHK_ORDER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to discard the trial if the order of targets in the imperative stimulus is not met."), _T("Target Sequence") );
	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );

// 	pWnd = GetDlgItem( IDC_CHK_ORDER );
// 	pWnd->EnableWindow( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ConditionPageConsis::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageConsis::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_consistencychecking.html"), this );
	return TRUE;
}

void ConditionPageConsis::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void ConditionPageConsis::OnBnReset() 
{
	m_nMin = 30;
	m_nMax = 33;
	m_dStrokeLength = 0.0;
	m_dRangeLength = 0.0;
	m_dStrokeDirection = 0.0;
	m_dRangeDirection = 0.0;
	m_nSkip = 0;
	m_fFlagBadTarget = FALSE;

	UpdateData( FALSE );
}

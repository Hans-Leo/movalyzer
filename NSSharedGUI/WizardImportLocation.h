#pragma once

#include "NSTooltipCtrl.h"

// WizardImportLocation dialog

class AFX_EXT_CLASS WizardImportLocation : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardImportLocation)

public:
	WizardImportLocation();
	virtual ~WizardImportLocation();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_LOC };
	CString			m_szRemote;
	CString			m_szPath;
	CString			m_szLogin;
	CString			m_szPassword;
	CString			m_szStatus;
	CString			m_szServerName;
	NSToolTipCtrl	m_tooltip;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Overrides
public:
	virtual LRESULT OnWizardNext();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
	BOOL Test( BOOL fInformOnSuccess );

protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedBnTest();

	DECLARE_MESSAGE_MAP()
};

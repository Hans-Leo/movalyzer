#pragma once

// ElementPageSize.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ElementPageSize dialog

interface IElement;

class AFX_EXT_CLASS ElementPageSize : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ElementPageSize)

// Construction
public:
	ElementPageSize( IElement* = NULL );
	~ElementPageSize();

// Dialog Data
	enum { IDD = IDD_PAGE_ELMT_SIZE };
	CComboBox		m_cboShape;
	double			m_dX;
	double			m_dY;
	double			m_dXWidth;
	double			m_dYWidth;
	double			m_dXErr;
	double			m_dYErr;
	int				m_nShape;
	IElement*		m_pE;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	CBCGPButton		m_bnCalc;

// Overrides
public:
	virtual BOOL OnApply();
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
// message map functions
	afx_msg void OnSelchangeCboShape();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedBnCalc();
};

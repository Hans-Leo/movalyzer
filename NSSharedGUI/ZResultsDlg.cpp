// ZResultsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ZResultsDlg.h"
#include "Experiments.h"
#include "Subjects.h"
#include "..\DataMod\DataMod.h"
#include "AGraphDlg.h"
#include <math.h>

#define ALL_SUBJ		_T("***")

// ZResultsDlg dialog

IMPLEMENT_DYNAMIC(ZResultsDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(ZResultsDlg, CBCGPDialog)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

//************************************
// Method:    ZResultsDlg
// FullName:  ZResultsDlg::ZResultsDlg
// Access:    public 
// Returns:   
// Qualifier: : CBCGPDialog(IDD, pParent), m_szExp( szExp ), m_szSubj( szSubj ), m_nSubject(0), m_nNumTests(0), m_dTestTime(0), m_nImpCount(0), m_nSubjImpCount(0)
// Parameter: CString szExp
// Parameter: CString szSubj
// Parameter: CWnd * pParent
//************************************
ZResultsDlg::ZResultsDlg( CString szExp, CString szSubj, CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ZResultsDlg::IDD, pParent), m_szExp( szExp ), m_szSubj( szSubj ),
	  m_nSubject(0), m_nNumTests(0), m_dTestTime(0), m_nImpCount(0), m_nSubjImpCount(0)
{
	m_szTestTime = _T("0 m");
	m_dZCritical = 0.;
	m_fGlobal = FALSE;
	m_fWideDateRange = FALSE;
}

//************************************
// Method:    DoDataExchange
// FullName:  ZResultsDlg::DoDataExchange
// Access:    virtual protected 
// Returns:   void
// Qualifier:
// Parameter: CDataExchange * pDX
//************************************
void ZResultsDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_SUBJECTS, m_cboSubjects);
	DDX_Control(pDX, IDC_DTP_FROM, m_dtcFrom);
	DDX_Control(pDX, IDC_DTP_TO, m_dtcTo);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Text(pDX, IDC_TXT_REPS, m_nNumTests);
	DDX_Text(pDX, IDC_TXT_DESC, m_szTestTime);
	DDX_Text(pDX, IDC_TXT_COUNT, m_nImpCount);
	DDX_Text(pDX, IDC_TXT_SIZE, m_nSubjImpCount);
	DDX_Check(pDX, IDC_CHK_AVG, m_fGlobal);
}

// ZResultsDlg message handlers

//************************************
// Method:    PreTranslateMessage
// FullName:  ZResultsDlg::PreTranslateMessage
// Access:    virtual public 
// Returns:   BOOL
// Qualifier:
// Parameter: MSG * pMsg
//************************************
BOOL ZResultsDlg::PreTranslateMessage( MSG* pMsg )
{
	m_tooltip.RelayEvent(pMsg);
	return CBCGPDialog::PreTranslateMessage(pMsg);
}

//************************************
// Method:    OnInitDialog
// FullName:  ZResultsDlg::OnInitDialog
// Access:    virtual protected 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL ZResultsDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// tooltips
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_CBO_SUBJECTS );
	m_tooltip.AddTool( pWnd, _T("Select the desired subject to view their history, or select ALL subjects to view all simultaneously."), _T("Select Subject") );
	pWnd = GetDlgItem( IDC_DTP_FROM );
	m_tooltip.AddTool( pWnd, _T("Enter the starting date for the history review."), _T("From Date") );
	pWnd = GetDlgItem( IDC_DTP_FROM );
	m_tooltip.AddTool( pWnd, _T("Enter the ending date for the history review."), _T("To Date") );
	pWnd = GetDlgItem( IDC_CHK_AVG );
	m_tooltip.AddTool( pWnd, _T("Check ON to view the results as compared to the global average subject.\nCheck OFF to compare against each subject's own history."), _T("Compare Against Global?") );
	pWnd = GetDlgItem( IDC_LIST );
	m_tooltip.AddTool( pWnd, _T("The history will be presented here."), _T("History") );

	// verify experiment
	if( m_szExp == _T("") ) {
		BCGPMessageBox( _T("No experiment specified in creation of dialog. Cannot continue."), MB_OK | MB_ICONERROR );
		return TRUE;
	}

	// subjects
	CString szDataPath;
	::GetDataPathRoot( szDataPath );
	IExperimentMember* pEML = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEML );
	if( !pEML ) {
		BCGPMessageBox( IDS_EXPMEML_ERR, MB_OK | MB_ICONERROR );
		return TRUE;
	}
	pEML->SetDataPath( szDataPath.AllocSysString() );
	hr = pEML->FindForExperiment( m_szExp.AllocSysString() );
	if( FAILED( hr ) ) {
		BCGPMessageBox( IDS_NOSUBJS, MB_OK | MB_ICONERROR );
		return TRUE;
	}
	// add ALL item
	m_lstSubjects.AddTail( ALL_SUBJ );
	int nPos = m_cboSubjects.AddString( "All" );
	m_cboSubjects.SetItemData( nPos, 0 );
	CString szItem, szCode, szData;
	BSTR bstrID = NULL;
	while( SUCCEEDED( hr ) ) {
		pEML->get_SubjectID( &bstrID );
		szItem = bstrID;
		if( ( szItem != _T("") ) && !m_lstSubjects.Find( szItem ) ) m_lstSubjects.AddTail( szItem );
		hr = pEML->GetNext();
	}
	pEML->Release();
	// add to list (get info of subject)
	{
	CString szSrch;
	Subjects subj;
	subj.SetDataPath( szDataPath );
	int nItem = -1;
	POSITION pos = m_lstSubjects.GetHeadPosition();
	while( pos ) {
		szItem = m_lstSubjects.GetNext( pos );
		nItem++;
		if( ( szItem != ALL_SUBJ ) && SUCCEEDED( subj.Find( szItem ) ) ) {
			subj.GetCode( szCode );
			szData.Format( _T("%s (%s)"), szItem, szCode );
			if( m_szSubj == szItem ) szSrch = szData;
			nPos = m_cboSubjects.AddString( szData );
			m_cboSubjects.SetItemData( nPos, nItem );
		}
	}
	// select ALL or specified subject
	if( szSrch != _T("") ) m_cboSubjects.SelectString( -1, szSrch );
	else m_cboSubjects.SetCurSel( 0 );
	}

	// dates default to today unless specified
	if( m_fWideDateRange ) {
		COleDateTime dt = COleDateTime::GetCurrentTime();
		COleDateTimeSpan dts;
		dts.SetDateTimeSpan( -365*4, 0, 0, 0 );
		COleDateTime dtNew = dt + dts;
		m_dtcFrom.SetTime( dtNew );
	}

	// get experiment's critical Z
	BOOL fShowGlobal = FALSE;
	IProcSetting* pPS = NULL;
	if( SUCCEEDED( ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS ) ) ) {
		pPS->Find( m_szExp.AllocSysString() );
		IProcSetFlags* pPSF = NULL;
		if( SUCCEEDED( pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF ) ) ) {
			pPSF->get_NormDBThreshold( &m_dZCritical );
			pPSF->get_NormDBCalcScoreGlobal( &fShowGlobal );
			pPSF->Release();
		}
		pPS->Release();
	}

	// if not calculating global (exp. setting above), hide check box
	pWnd = GetDlgItem( IDC_CHK_AVG );
	if( pWnd ) pWnd->ShowWindow( fShowGlobal );

	// list settings
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) ) ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );
	m_list.SendMessage( LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT );

	// add columns to list
	m_list.InsertColumn( 0, _T("PERSON"), LVCFMT_LEFT, 100, 0 );
	m_list.InsertColumn( 1, _T("SESSION"), LVCFMT_LEFT, 55, 1 );
	m_list.InsertColumn( 2, _T("DATE"), LVCFMT_LEFT, 70, 2 );
	m_list.InsertColumn( 3, _T("TIME"), LVCFMT_LEFT, 60, 3 );
	m_list.InsertColumn( 4, _T("DUR"), LVCFMT_RIGHT, 40, 4 );
	m_list.InsertColumn( 5, _T("SCORE"), LVCFMT_RIGHT, 50, 5 );
	m_list.InsertColumn( 6, _T("STATUS"), LVCFMT_LEFT, 70, 6 );
	m_list.InsertColumn( 7, _T("CONF."), LVCFMT_CENTER, 50, 7 );

	// startup with default values
	OnOK();

	return TRUE;
}

//************************************
// Method:    OnOK
// FullName:  ZResultsDlg::OnOK
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void ZResultsDlg::OnOK()
{
	UpdateData( TRUE );

	// initialize vars
	m_list.DeleteAllItems();
	m_nNumTests = 0;
	m_dTestTime = 0;
	m_szTestTime = _T("0 m");
	m_nImpCount = 0;
	m_nSubjImpCount = 0;

	// get selected subject (or ALL)
	int nSel = m_cboSubjects.GetCurSel();
	int nIdx = ( nSel >= 0 ) ? (int)m_cboSubjects.GetItemData( nSel ) : -1;
	POSITION pos = ( nIdx >= 0 ) ? m_lstSubjects.FindIndex( nIdx ) : NULL;
	CString szSubj = pos ? m_lstSubjects.GetAt( pos ) : _T("");

	// open history file
	CString szTemp, szXMH;
	::GetExpINC( m_szExp, szTemp );
	nSel = szTemp.ReverseFind( '.' );
	if( nSel != -1 ) szXMH = szTemp.Left( nSel );
	else szXMH = szTemp;
	szXMH += _T(".XMH");
	CStdioFile f;
	if( !f.Open( szXMH, CFile::modeRead | CFile::shareDenyNone ) ) {
		BCGPMessageBox( _T("Unable to open z-score history file. Is norm DB creation turned on? (see experiment->running exp->summ/analysis)"), MB_OK | MB_ICONERROR );
		return;
	}

	CString szLine, szHeaders, szNorm, szData, szStatus, szSubjDisp, szScore, szDur;
	CStringList lstImpSubj;
	COleDateTime dtThen, dtFrom, dtTo;
	COleDateTimeSpan dtsFrom, dtsTo;
	HALine* line = NULL;
	int nHeaders = 0, nCurPos = 0, nConfidence = 0, nImage = 0, nN = 0, nListItem = 0;
	char* pszData = new char[ 1000 ];
	char pszDate[ 20 ], pszGrp[5], pszSubj[5], pszCond[5], pszSeg[5], pszNorm[5];
	double dAll = 0.;

	// create data object
	HAData* haData = new HAData();
	if( !haData ) {
		BCGPMessageBox( _T("Not enough memory to run operation."), MB_OK | MB_ICONERROR );
		goto Cleanup;
	}
	// add line
	line = new HALine();
	haData->m_lstLines.AddTail( line );
	haData->m_nLines = 1;
	// from/to dates
	m_dtcFrom.GetTime( dtFrom );
	m_dtcTo.GetTime( dtTo );

	// loop through file, find entries for specified subject (or ALL), construct string for each and add to results string
	// we will be performing cleanup in order to reduce overuse of memory
	while( f.ReadString( szLine ) ) {
		// 1st line is headers
		if( szHeaders == _T("") ) {
			szHeaders = szLine;
		}
		// rest is data
		else {
			sscanf( szLine, _T("%s %lf %s %s %s %s %s %d %lf %[^\n]"), pszDate, &(line->m_dDur), pszGrp, pszSubj, pszCond, pszSeg, pszNorm, &nN, &dAll, pszData );
			// is this the correct subject? We must also ensure subj = norm (compared to)
			szNorm = pszNorm;
			if( ( ( szSubj == ALL_SUBJ ) || ( szSubj == pszSubj ) ) &&
				( ( !m_fGlobal && szNorm == pszSubj ) || ( m_fGlobal && szNorm == SUBJ_GLOBAL ) ) ) {
				// verify the date
				line->m_szDate = pszDate;
				if( line->m_szDate.GetLength() != 14 ) {
					BCGPMessageBox( _T("ERROR: Z-Score history file date/time stamp is in unreadable format."), MB_OK | MB_ICONERROR );
					goto Cleanup;
				}
				dtThen.SetDateTime( atoi( line->m_szDate.Left( 4 ) ), atoi( line->m_szDate.Mid( 4, 2 ) ), atoi( line->m_szDate.Mid( 6, 2 ) ),
									atoi( line->m_szDate.Mid( 8, 2 ) ), atoi( line->m_szDate.Mid( 10, 2 ) ), atoi( line->m_szDate.Right( 2 ) ) );
				// are we within range for date?
				dtsFrom = dtThen - dtFrom;
				dtsTo = dtTo - dtThen;
				if( ( dtsFrom.GetDays() >= 0 ) && ( dtsTo.GetDays() >= 0 ) ) {
					// set other data for HALine object (necessary?)
					line->m_szGrp = pszGrp;
					line->m_szSubj = pszSubj;
					line->m_szCond = pszCond;
					m_nNumTests++;
					m_dTestTime += line->m_dDur;
					// score/confidence
					szScore.Format( _T("%.3f"), dAll );
					szDur.Format( _T("%.1f m"), line->m_dDur );
					nImage = 6;
					szStatus = _T("Abnormal");
					szTemp = _T("-");
					if( dAll > 3.090 ) {
						szStatus = _T("Extreme");
						szTemp = _T("99.9%");
					}
					else if( dAll > 2.576 ) szTemp = _T("99%");
					else if( dAll > 1.960 ) szTemp = _T("95%");
					else if( dAll > 1.645 ) szTemp = _T("90%");
					else if( dAll > 1.440 ) szTemp = _T("85%");
					else if( dAll > 1.282 ) szTemp = _T("80%");
					else if( dAll > 1.150 ) szTemp = _T("75%");
					else if( dAll > 1.036 ) szTemp = _T("70%");
					else if( dAll > 0.842 ) szTemp = _T("60%");
					else if( dAll > 0.674 ) szTemp = _T("50%");
					if( dAll < m_dZCritical ) {
						nImage = -1;
						szStatus = _T("-");
						szTemp = _T("-");
					}

					// how subject is displayed
					nIdx = m_cboSubjects.FindString( 0, line->m_szSubj );
					if( nIdx > 0 ) m_cboSubjects.GetLBText( nIdx, szSubjDisp );
					else szSubjDisp = line->m_szSubj;

					// add to list
					nListItem = m_list.InsertItem( 0, szSubjDisp, nImage );
					m_list.SetItem( nListItem, 1, LVIF_TEXT, line->m_szGrp, 0, 0, 0, NULL );
					m_list.SetItem( nListItem, 2, LVIF_TEXT, dtThen.Format( _T("%m-%d-%Y") ), 0, 0, 0, NULL );
					m_list.SetItem( nListItem, 3, LVIF_TEXT, dtThen.Format( _T("%H:%M:%S") ), 0, 0, 0, NULL );
					m_list.SetItem( nListItem, 4, LVIF_TEXT, szDur, 0, 0, 0, NULL );
					m_list.SetItem( nListItem, 5, LVIF_TEXT, szScore, 0, 0, 0, NULL );
					m_list.SetItem( nListItem, 6, LVIF_TEXT, szStatus, 0, 0, 0, NULL );
					m_list.SetItem( nListItem, 7, LVIF_TEXT, szTemp, 0, 0, 0, NULL );
				}
			}
		}
	}

	if( m_nNumTests > 0 ) m_szTestTime.Format( _T("%.1f m"), m_dTestTime / m_nNumTests );
	UpdateData( FALSE );

	// cleanup
Cleanup:
	if( pszData ) delete [] pszData;
	if( haData ) delete haData;
}

//************************************
// Method:    OnHelpInfo
// FullName:  ExcludeDlg::OnHelpInfo
// Access:    protected 
// Returns:   BOOL
// Qualifier:
// Parameter: HELPINFO * pHelpInfo
//************************************
BOOL ZResultsDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("experimentsettings_processing_normdb.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

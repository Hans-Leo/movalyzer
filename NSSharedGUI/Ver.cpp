// Globals.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "Globals.h"
#include "Ver.h"

/////////////////////////////////////////////////////////////////////////////

#define	APP_VERSION					6200

int AppVersion()
{
	BOOL fNeedVerChange = FALSE;

	CString szFile;
	::GetDataPath( szFile, FALSE, TRUE );
	if( ::IsAppRx() ) szFile += _T("verx.txt");
	else szFile += _T("ver.txt");

TryAgain:;
	// look for file...if not exist, create with base APP_VERSION
	CFileFind ff;
	if( !ff.FindFile( szFile ) || fNeedVerChange )
	{
		CStdioFile f;
		if( f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
		{
			CString szLine;
			szLine.Format( _T("%d"), APP_VERSION );
			f.WriteString( szLine );
		}
		else
		{
			CString szMsg;
			szMsg.Format( _T("Unable to create version file:\n%s"), szFile );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONWARNING );
		}

		return APP_VERSION;
	}
	else
	{
		CStdioFile f;
		if( f.Open( szFile, CFile::modeRead ) )
		{
			CString szLine;
			f.ReadString( szLine );
			int nVer = atoi( szLine );
			if( nVer < APP_VERSION )
			{
				fNeedVerChange = TRUE;
				goto TryAgain;
			}
			else return nVer;
		}
		else return APP_VERSION;
	}
}

void UpdateVersion( double dVer )
{
	CString szFile;
	::GetDataPath( szFile );
	szFile += _T("ver.txt");

	CStdioFile f;
	if( f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		CString szLine;
		szLine.Format( _T("%d\n"), (int)( dVer * 1000. ) );
		f.WriteString( szLine );
	}
	else BCGPMessageBox( _T("Unable to update version file.") );
}

/////////////////////////////////////////////////////////////////////////////

#pragma once

#include <afxinet.h>

/////////////////////////////////////////////////////////////////////////////
// WizardImportRemoteSheet

class AFX_EXT_CLASS WizardImportRemoteSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardImportRemoteSheet)

// Construction
public:
	WizardImportRemoteSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	virtual ~WizardImportRemoteSheet();

// Attributes
public:
	CInternetSession*	m_pInetSession;
	CFtpConnection*		m_pFtpConnection;
	CStringList			m_lstFiles;

// Operations
public:
	virtual void AddPages();
	void CloseInet();

// Overrides

protected:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

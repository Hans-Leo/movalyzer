// AGraphDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\Common\UserObj.h"
#include "AGraphDlg.h"
#include "GraphIdentityDlg.h"
#include "AxisCust.h"
#include "Experiments.h"
#include "Subjects.h"
#include "..\DataMod\DataMod.h"
#include "..\ProcessMod\ProcessMod.h"
#include "..\ShowMod\ShowMod.h"
#include "..\NSShared\iolib.h"
#include <math.h>
#include "DataTabDlg.h"
#include "DataTabGridDlg.h"
#include "SortDlg.h"
#include "Progress.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int _AGDRefCnt = 0;	// Counter for # of windows open...
							// If more than one graph open, then we must always
							// recalc using XMean because one graph might change
							// a factor w/o the others knowing

CWnd* _hwndAxis = NULL;

//#define	NDATAMAX	30
#define	NDATAMAX		126

// Indice combobox data columns
#define	COL_GROUP		1
#define	COL_SUBJ		2
#define	COL_COND		3
#define	COL_TRIAL		4
#define	COL_STROKE		5
#define	COL_STROKES		6


#define GROUP			1
#define SUBJ			2
#define COND			3
#define TRIAL			4
#define SEGM			5
#define SUBM			6
#define NTRIAL			100
#define NSEGM			33
#define NSUBM			3

static int _stdev_offset = 0;
static int _colN = 0;

// Available charts
#define	CHART_NONE		0
#define	CHART_T			1
#define	CHART_AVG		2
#define	CHART_TAVG		3
#define	CHART_AVGSD		4
#define	CHART_AVGSDN	5
#define	CHART_TAVGSD	6
#define	CHART_TAVGSDN	7

// X/Y column offsets
#define	OS_AVG			0	// Also avgs of avgs
#define	OS_SD			1	// Also avgs of std devs
#define	OS_SDAVG		2	// Only for equal-weight
#define	OS_SDSD			3	// Only for equal-weight

#define	CSIZE(x, r)			( ( x >= -r ) && ( x <= r ) )
#define	CRANGE(x, r1, r2)	( ( x >= r1 ) && ( x <= r2 ) )
#define	CBOOL(x)			( ( x == 1 ) || ( x == 0 ) )
#define	TOOSMALL(x)			if( ( x * 1000000 ) < 1 ) x = 0;

// Temporary struct for maintaining graph settings in persistence
struct GraphMem
{
	// Main
	int		nX;
	int		nColX;
	int		nY;
	int		nColY;
	int		nGrp;
	BOOL	fStroke;
	int		nXStroke;
	int		nYStroke;
	// Point inclusion/dispersion/exclusion
	BOOL	fDispX;
	int		nDispX;
	double	dDispX;
	BOOL	fDispY;
	int		nDispY;
	double	dDispY;
	BOOL	fExcludeX;
	double	dExcludeX;
	BOOL	fExcludeY;
	double	dExcludeY;
	// Flagging
	BOOL	fFlag;
	int		nFlag;
	double	dFlagMin;
	double	dFlagMax;
	int		nPoint;
	int		nSize;
	long	nColor;
	// Point inclusion/dispersion/exclusion (forgot these)
	int		nGroup;
	int		nSubj;
	int		nCond;
	int		nTrial;
	int		nStroke;
	int		nSubMvmt;
};


/////////////////////////////////////////////////////////////////////////////
// AGraphDlg dialog

BEGIN_MESSAGE_MAP(AGraphDlg, CBCGPDialog)
	ON_WM_CONTEXTMENU()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_CBO_X, OnSelchangeCboX)
	ON_CBN_SELCHANGE(IDC_CBO_FACTOR1, OnSelchangeCboFactor1)
	ON_BN_CLICKED(IDC_CHK_TRIALS, OnChkTrials)
	ON_BN_CLICKED(IDC_CHK_AVG, OnChkAvg)
	ON_BN_CLICKED(IDC_CHK_STROKES, OnChkStrokes)
	ON_CBN_SELCHANGE(IDC_CBO_Y, OnSelchangeCboY)
	ON_BN_CLICKED(IDC_CHK_EQUAL, OnChkEqual)
	ON_BN_CLICKED(IDC_CHK_EQUAL2, OnChkEqual2)
	ON_BN_CLICKED(IDC_CHK_STDDEV, OnChkStddev)
	ON_EN_CHANGE(IDC_EDIT_XSTROKE, OnChangeEditXstroke)
	ON_EN_CHANGE(IDC_EDIT_YSTROKE, OnChangeEditYstroke)
	ON_BN_CLICKED(IDC_BN_GO, OnBnGo)
	ON_BN_CLICKED(IDC_CHK_SCALE, OnChkScale)
	ON_CBN_SELCHANGE(IDC_CBO_WHICH, OnSelchangeCboChart)
	ON_CBN_SELCHANGE(IDC_CBO_COLX, OnSelchangeCboColX)
	ON_CBN_SELCHANGE(IDC_CBO_COLY, OnSelchangeCboColY)
	ON_WM_HELPINFO()
	ON_COMMAND(IDM_AXIS_CUSTOMIZE, OnAxisCustomize)
	ON_BN_CLICKED(IDC_BN_REFRESH, OnClickBnRefresh)
END_MESSAGE_MAP()

#pragma warning(disable: 4355)
AGraphDlg::AGraphDlg(CString szExp, CString szINC, BOOL fEqual, UINT nMode, CWnd* pParent)
	: CBCGPDialog( AGraphDlg::IDD, pParent ), m_szExp( szExp ), m_szINC( szINC ), m_fEqual( fEqual ), m_nMode( nMode ),
	  m_pGraph( NULL ), m_pProcess( NULL ), m_pShowStats( NULL ),
	  m_dlgOptions( szINC, ( nMode == MODE_SCATTER ) ? TRUE : FALSE, this ),
	  m_dlgFlagging( this ), m_dlgGrouping( this )
#pragma warning(default: 4355)
{
	m_nX = 4;
	m_nColX = 0;
	m_nY = 7;
	m_nColY = 0;
	m_nFactor1 = 5;
	m_fUseStrokes = FALSE;
	m_nXStroke = 1;
	m_nYStroke = 1;
	m_fScale = FALSE;
	m_fInit = FALSE;
	m_fInitG = FALSE;
	m_nXOld = 2;
	m_nFactor1Old = 5;
	m_fAvg = ( ( nMode == MODE_AVERAGE ) || ( nMode == MODE_COMBO ) || ( nMode == MODE_STDDEVN ) );
	m_fStdDev = ( ( nMode == MODE_STDDEV ) || ( nMode == MODE_COMBO ) || ( nMode == MODE_STDDEVN ) );
	m_fTrial = ( ( nMode == MODE_SCATTER ) || ( nMode == MODE_COMBO ) );
	m_fTMPGenerated = FALSE;
	m_fEWChg = FALSE;
	m_nLGoBn = 0;
	m_nLTxtX = 0;
	m_nLX = 0;
	m_nLTxtY = 0;
	m_nLY = 0;
	m_nLStrokes = 0;
	m_nLScale = 0;
	m_fAssociationsDone = FALSE;
	m_pWndFocus = NULL;
//	m_fsma = FALSE;
	if( !m_fEqual ) averagebytrial = FALSE;

	_AGDRefCnt++;
}

//************************************
// Method:    ~AGraphDlg
// FullName:  AGraphDlg::~AGraphDlg
// Access:    public 
// Returns:   
// Qualifier:
//************************************
AGraphDlg::~AGraphDlg()
{
	Experiments::Cleanup();
	if( m_pShowStats ) m_pShowStats->Release();

	_AGDRefCnt--;

	CleanupQuests();

	// write settings to persistence if last instance
	if( _AGDRefCnt == 0 )
	{
		GraphMem gm;
		CFile f;

		memset( &gm, 0, sizeof( GraphMem ) );

		CString szMem;
		::GetDataPathRoot( szMem );
		szMem += _T("\\");
		szMem += m_szExp;
		szMem += _T("\\graph.mem");
		if( f.Open( szMem, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary ) )
		{
			// Main window
			gm.nX = m_nX;
			gm.nColX = m_nColX;
			gm.nY = m_nY;
			gm.nColY = m_nColY;
			gm.nGrp = m_nFactor1;
			gm.fStroke = m_fUseStrokes;
			gm.nXStroke = m_nXStroke;
			gm.nYStroke = m_nYStroke;
			// Point inclusion/dispersion/exclusion
			gm.nGroup = m_dlgOptions.m_nGrp2;
			gm.nSubj = m_dlgOptions.m_nSubj2;
			gm.nCond = m_dlgOptions.m_nCond2;
			gm.nTrial = m_dlgOptions.m_nTrial2;
			gm.nStroke = m_dlgOptions.m_nStroke2;
//			gm.nSubMvmt = m_dlgOptions.m_nSubMvmt2;
			gm.fDispX = m_dlgOptions.m_fDispX;
			gm.nDispX = m_dlgOptions.m_nDisp;
			gm.dDispX = m_dlgOptions.m_dStrength;
			gm.fDispY = m_dlgOptions.m_fDispY;
			gm.nDispY = m_dlgOptions.m_nDisp2;
			gm.dDispY = m_dlgOptions.m_dStrength2;
			gm.fExcludeX = m_dlgOptions.m_fExcludeX;
			gm.dExcludeX = m_dlgOptions.m_dExcludeValX;
			gm.fExcludeY = m_dlgOptions.m_fExcludeY;
			gm.dExcludeY = m_dlgOptions.m_dExcludeValY;
			// Flagging
			gm.fFlag = m_dlgFlagging.m_fFlag;
			gm.nFlag = m_dlgFlagging.m_nY;
			gm.dFlagMin = m_dlgFlagging.m_dMin;
			gm.dFlagMax = m_dlgFlagging.m_dMax;
			gm.nPoint = m_dlgFlagging.m_nYPoint;
			gm.nSize = m_dlgFlagging.m_nYSize;
			gm.nColor = m_dlgFlagging.m_nYColor;

			f.Write( &gm, sizeof( GraphMem ) );
			f.Close();
		}
	}
}

//************************************
// Method:    CleanupQuests
// FullName:  AGraphDlg::CleanupQuests
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::CleanupQuests()
{
	POSITION posQ = m_lstQuests.GetHeadPosition();
	while( posQ )
	{
		Question* pQuest = (Question*)m_lstQuests.GetNext( posQ );
		if( pQuest ) delete pQuest;
	}
	m_lstQuests.RemoveAll();
}

//************************************
// Method:    DoDataExchange
// FullName:  AGraphDlg::DoDataExchange
// Access:    virtual protected 
// Returns:   void
// Qualifier:
// Parameter: CDataExchange * pDX
//************************************
void AGraphDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BN_GO, m_bnGo);
	DDX_Control(pDX, IDC_CBO_X, m_cboX);
	DDX_Control(pDX, IDC_CBO_Y, m_cboY);
	DDX_CBIndex(pDX, IDC_CBO_X, m_nX);
	DDX_CBIndex(pDX, IDC_CBO_Y, m_nY);
	DDX_Control(pDX, IDC_CBO_FACTOR1, m_cboF1);
	DDX_CBIndex(pDX, IDC_CBO_FACTOR1, m_nFactor1);
	DDX_Check(pDX, IDC_CHK_AVG, m_fAvg );
	DDX_Check(pDX, IDC_CHK_STDDEV, m_fStdDev);
	DDX_Check(pDX, IDC_CHK_TRIALS, m_fTrial);
	DDX_Check(pDX, IDC_CHK_EQUAL, m_fEqual);
	DDX_Check(pDX, IDC_CHK_EQUAL2, averagebytrial);
	DDX_Check(pDX, IDC_CHK_STROKES, m_fUseStrokes);
	DDX_Text(pDX, IDC_EDIT_XSTROKE, m_nXStroke);
	DDX_Text(pDX, IDC_EDIT_YSTROKE, m_nYStroke);
	DDX_Check(pDX, IDC_CHK_SCALE, m_fScale);
	DDX_Control(pDX, IDC_CBO_WHICH, m_cboChart);
	DDX_CBIndex(pDX, IDC_CBO_WHICH, m_nChart);
	DDX_Control(pDX, IDC_CBO_COLX, m_cboColX);
	DDX_CBIndex(pDX, IDC_CBO_COLX, m_nColX);
	DDX_Control(pDX, IDC_CBO_COLY, m_cboColY);
	DDX_CBIndex(pDX, IDC_CBO_COLY, m_nColY);
}

//************************************
// Method:    OnContextMenu
// FullName:  AGraphDlg::OnContextMenu
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pWnd
// Parameter: CPoint point
//************************************
void AGraphDlg::OnContextMenu( CWnd* pWnd, CPoint point )
{
	// Determine type of object and get approp menu
	int nSubMenu = -1;	// Item

	CWnd* pWndX = GetDlgItem( IDC_CBO_X );
	CWnd* pWndY = GetDlgItem( IDC_CBO_Y );
	CWnd* pWndG = GetDlgItem( IDC_CBO_FACTOR1 );

	if( ( pWnd->m_hWnd == pWndX->m_hWnd ) || ( pWnd->m_hWnd == pWndY->m_hWnd ) ||
		( pWnd->m_hWnd == pWndG->m_hWnd ) )
		nSubMenu = 0;

	if( nSubMenu == -1 ) return;

	_hwndAxis = pWnd;

	CMenu menu;
	VERIFY( menu.LoadMenu( IDR_POPUP_GRAPH ) );
	CMenu* pPopup = menu.GetSubMenu( nSubMenu );
	if( pPopup )
	{
		CBCGPPopupMenu* pPopupMenu = new CBCGPPopupMenu;
		if( !pPopupMenu->Create( this, point.x, point.y, (HMENU)pPopup->m_hMenu, FALSE, TRUE ) )
			return;
		UpdateDialogControls( this, FALSE );
	}
}

//************************************
// Method:    OnAxisCustomize
// FullName:  AGraphDlg::OnAxisCustomize
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnAxisCustomize()
{
	int nSelX = m_cboX.GetCurSel();
	int nSelY = m_cboY.GetCurSel();
	int nSelG = m_cboF1.GetCurSel();
	int nSel = -1;

	CWnd* pWndX = GetDlgItem( IDC_CBO_X );
	CWnd* pWndY = GetDlgItem( IDC_CBO_Y );
	CWnd* pWndG = GetDlgItem( IDC_CBO_FACTOR1 );
	if( pWndX == _hwndAxis ) nSel = nSelX;
	else if( pWndY == _hwndAxis ) nSel = nSelY;
	else if( pWndG == _hwndAxis ) nSel = nSelY;
	else return;

	AxisCust d( &m_lstAxisD, &m_lstAxis, nSel, this );
	if( d.DoModal() == IDOK )
	{
		m_lstAxis.RemoveAll();
		POSITION pos = d.m_lstT.GetHeadPosition();
		CString szMem;
		while( pos )
		{
			szMem = d.m_lstT.GetNext( pos );
			m_lstAxis.AddTail( szMem );
		}

		// Write to file
		::GetDataPathRoot( szMem );
		szMem += _T("\\");
		szMem += m_szExp;
		szMem += _T("\\graph.axs");
		CStdioFile f;
		if( f.Open( szMem, CFile::modeWrite | CFile::modeCreate ) )
		{
			CString szItem;
			pos = m_lstAxis.GetHeadPosition();
			while( pos )
			{
				szItem = m_lstAxis.GetNext( pos );
				f.WriteString( szItem );
				f.WriteString( _T("\n") );
			}
		}
		else BCGPMessageBox( _T("Unable to update axes customization data file.") );

		// Update view
		m_lstAxis.RemoveAll();
		pos = d.m_lstT.GetHeadPosition();
		CString szItem;
		while( pos )
		{
			szItem = d.m_lstT.GetNext( pos );
			m_lstAxis.AddTail( szItem );
		}

		FillAxes();

		m_cboX.SetCurSel( nSelX );
		m_cboY.SetCurSel( nSelY );

		SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
	}
}

//************************************
// Method:    FillAxes
// FullName:  AGraphDlg::FillAxes
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::FillAxes()
{
	// Fill axes combos
	m_cboX.ResetContent();
	m_cboY.ResetContent();
	POSITION posAxis = m_lstAxis.GetHeadPosition();
	CString szItem;
	while( posAxis )
	{
		szItem = m_lstAxis.GetNext( posAxis );
		m_cboX.AddString( szItem );
		m_cboY.AddString( szItem );
	}
}

//************************************
// Method:    GetProcessObject
// FullName:  AGraphDlg::GetProcessObject
// Access:    protected 
// Returns:   IProcess*
// Qualifier:
//************************************
IProcess* AGraphDlg::GetProcessObject()
{
	if( m_pProcess ) return m_pProcess;
	m_pProcess = Experiments::GetProcessObject( ::GetOutputWindow(), NULL, m_szExp );
	double dMDV = -1.e6;
	{
		Experiments exp;
		if( SUCCEEDED( exp.Find( m_szExp ) ) )
		{
			exp.GetMissingDataValue( dMDV );
			::SetMissingDataValue( dMDV );
		}
	}
	m_pProcess->SetMissingDataValue( dMDV );
	return m_pProcess;
}

//************************************
// Method:    GetShowStatsObject
// FullName:  AGraphDlg::GetShowStatsObject
// Access:    protected 
// Returns:   IShowStats*
// Qualifier:
//************************************
IShowStats* AGraphDlg::GetShowStatsObject()
{
	if( !m_pShowStats )
	{
		HRESULT hr = E_FAIL;
		hr = CoCreateInstance( CLSID_ShowStats, NULL, CLSCTX_ALL,
							   IID_IShowStats, (LPVOID*)&m_pShowStats );
		if( FAILED( hr ) )
		{
			CString szMsg;
			szMsg.Format( IDS_STATS_ERR, hr );
			BCGPMessageBox( szMsg );
			return NULL;
		}

		CString szAppPath, szUser;
		::GetDataPath( szAppPath, ::IsPrivateUserOn() );
		::GetCurrentUser( szUser );
		szAppPath += szUser;
		szAppPath += _T("\\Actions.log");
		m_pShowStats->SetLoggingOn( ::GetLogGraph(), szAppPath.AllocSysString() );
		m_pShowStats->SetMissingDataValue( ::GetMissingDataValue() );
	}

	return m_pShowStats;
}

//************************************
// Method:    GenerateTmpFile
// FullName:  AGraphDlg::GenerateTmpFile
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: BOOL fForce
// Parameter: BOOL fNoIdentities
// Parameter: BOOL fBypassSettings
//************************************
void AGraphDlg::GenerateTmpFile( BOOL fForce, BOOL fNoIdentities, BOOL fBypassSettings )
{
	if( m_szINC == _T("") || m_szExp == _T("") ) return;

	CWaitCursor crs;

	// Check for existence
	if( !fNoIdentities && !fForce )
	{
		CFileFind ff;
		if( ff.FindFile( m_szTMP ) )
		{
			m_fTMPGenerated = TRUE;
			return;
		}
	}

	// Files
	CStdioFile fileINC, fileTMP;
	CString szTMP = m_szTMP;
	if( fNoIdentities )
	{
		szTMP = m_szTMP.Left( m_szTMP.GetLength() - 3 );	// remove TMP extension
		szTMP += _T("ANA");
	}
	if( !fileINC.Open( m_szINC, CFile::modeRead ) ) return;
	if( !fileTMP.Open( szTMP, CFile::modeCreate | CFile::modeWrite ) ) return;

#if 0
	// get frequency of high-resolution performance counter
	LARGE_INTEGER freq, startAll, stopAll, start, stop;
	double dTime = 0., dt1 = 0., dt2 = 0., dt3 = 0.;
	UINT nLines = 0, nWritten = 0;
	CString szTimeMsg;
	::QueryPerformanceFrequency( &freq );
	StopWatch( 0, startAll, stopAll, freq );
	dTime = StopWatch( 1, startAll, stopAll, freq );
	szTimeMsg.Format( _T("TIME: TMP: Total = %.8f seconds (%u lines / %u written)"), dTime, nLines, nWritten );
	OutputAMessage( szTimeMsg );
#endif

	// subject questionnaire object
	ISQuestionnaire* pSQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
								   IID_ISQuestionnaire, (LPVOID*)&pSQ );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Error creating SQuestionnaire object.") );
		return;
	}

	// progress
	// steps for: obtaining subjects, groups, conditions; sorting
	int nPos = 0;
	long nSteps = 0, nCur = 0, nStep = 0;
	double dVal = 0.;
	CString szData, szLine, szStrokeLabel;
	// 1 step per line in INC
	Progress::EnableProgress( 10 );
	Progress::SetProgress( 3, _T("Calculating the number of steps for XMEAN...") );
	while( fileINC.ReadString( szLine ) ) nSteps++;
	if( nSteps == 0 )
	{
		pSQ->Release();
		Progress::DisableProgress();
		return;
	}
	Progress::SetProgress( 7, _T("Resetting file pointer...") );
	nStep = MAX( 1, nSteps / 100 );
	fileINC.SeekToBegin();
	Progress::SetProgress( 10, _T("Complete.") );
	Progress::DisableProgress();

	// now enable full progress meter with calculated # of steps
	Progress::EnableProgress( nSteps );

	// sub-movement analysis?
// 	int nps = 0, nss = 0;	// primary & secondary strokes
 	IProcSetting* pPS = NULL;
 	BOOL fUserObj = FALSE;
 	UserObj* pObj = ::GetCurrentUserObj();
 	if( pObj && pObj->m_pPS )
 	{
 		pPS = pObj->m_pPS;
 		hr = S_OK;
 		fUserObj = TRUE;
 	}
 	else
 	{
 		hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
 								 IID_IProcSetting, (LPVOID*)&pPS );
 	}
 	if( FAILED( hr ) )
 	{
 		BCGPMessageBox( IDS_PS_ERR );
 		pSQ->Release();
 		return;
 	}
 
 	if( !fUserObj ) pPS->Find( m_szExp.AllocSysString() );
 	IProcSetFlags* pPSF = NULL;
 	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
 	if( SUCCEEDED( hr ) )
 	{
 		pPSF->get_SEG_S( &m_fsma );
 		pPSF->Release();
 	}
 	if( !fUserObj ) pPS->Release();

	// GENERATE LISTS
	BSTR bstrID = NULL, bstr = NULL;
	CString szID;
	// Generate lists of groups/subjects
	IExperimentMember* pEML = NULL;
	hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							 IID_IExperimentMember, (LPVOID*)&pEML );
	if( !pEML )
	{
		BCGPMessageBox( IDS_EXPMEML_ERR );
		pSQ->Release();
		return;
	}
	hr = pEML->FindForExperiment( m_szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOSUBJS );
		pSQ->Release();
		return;
	}
	// Get subjects/groups
	CStringList lstSubj, lstGrp;
	while( SUCCEEDED( hr ) )
	{
		pEML->get_SubjectID( &bstrID );
		szID = bstrID;
		if( ( szID != HOLDER ) && ( lstSubj.Find( szID ) == NULL ) ) lstSubj.AddTail( szID );

		pEML->get_GroupID( &bstrID );
		szID = bstrID;
		if( ( szID != HOLDER ) && ( lstGrp.Find( szID ) == NULL ) ) lstGrp.AddTail( szID );

		hr = pEML->GetNext();
	}
	pEML->Release();
	// Generate list of conditions
	IExperimentCondition* pECL = NULL;
	hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
							 IID_IExperimentCondition, (LPVOID*)&pECL );
	if( !pECL )
	{
		BCGPMessageBox( IDS_EXPCONDL_ERR );
		pSQ->Release();
		return;
	}
	hr= pECL->FindForExperiment( m_szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOCONDS );
		pSQ->Release();
		return;
	}
	// Get condition
	CStringList lstCond;
	while( SUCCEEDED( hr ) )
	{
		pECL->get_ConditionID( &bstrID );
		szID = bstrID;
		if( lstCond.Find( szID ) == NULL ) lstCond.AddTail( szID );

		hr = pECL->GetNext();
	}
	pECL->Release();

	// NOW CHECK FOR SORT FILES
	CStringList lstExcGrp, lstExcCond, lstExcSubj;
	CStringList *pLstSortGrp = NULL, *pLstSortCond = NULL, *pLstSortSubj = NULL;
	BOOL fSortGrp = FALSE, fSortCond = FALSE, fSortSubj = FALSE, fInc = FALSE;
	CString szFile, szItem;
	CStdioFile f;
	if( !fBypassSettings ) {
		// Groups
		::GetGroupSort( m_szExp, szFile );
		if( f.Open( szFile, CFile::modeRead | CFile::typeText ) )
		{
			fSortGrp = TRUE;
			pLstSortGrp = new CStringList;
			while( f.ReadString( szItem ) )
			{
				// read sort mode from file (older versions wont have this so need to check)
				if( szItem.Find( _T(":") ) == -1 ) f.ReadString( szItem );

				if( szItem != _T("") &&
					( szItem[ szItem.GetLength() - 1 ] == '1' ) )
					fInc = TRUE;
				else fInc = FALSE;
				nPos = szItem.Find( _T(":") );
				if( nPos != -1 ) szItem = szItem.Left( nPos );

				if( fInc && lstGrp.Find( szItem ) )
					pLstSortGrp->AddTail( szItem );
				else 
					lstExcGrp.AddTail( szItem );
			}
			f.Close();
		}
		// Conditions
		::GetCondSort( m_szExp, szFile );
		if( f.Open( szFile, CFile::modeRead | CFile::typeText ) )
		{
			fSortCond = TRUE;
			pLstSortCond = new CStringList;
			while( f.ReadString( szItem ) )
			{
				// read sort mode from file (older versions wont have this so need to check)
				if( szItem.Find( _T(":") ) == -1 ) f.ReadString( szItem );

				if( szItem != _T("") &&
					( szItem[ szItem.GetLength() - 1 ] == '1' ) )
					fInc = TRUE;
				else fInc = FALSE;
				nPos = szItem.Find( _T(":") );
				if( nPos != -1 ) szItem = szItem.Left( nPos );

				if( fInc && lstCond.Find( szItem ) )
					pLstSortCond->AddTail( szItem );
				else
					lstExcCond.AddTail( szItem );
			}
			f.Close();
		}
		// Subjects
		::GetSubjSort( m_szExp, szFile );
		if( f.Open( szFile, CFile::modeRead | CFile::typeText ) )
		{
			fSortSubj = TRUE;
			pLstSortSubj = new CStringList;
			while( f.ReadString( szItem ) )
			{
				// read sort mode from file (older versions wont have this so need to check)
				if( szItem.Find( _T(":") ) == -1 ) f.ReadString( szItem );

				if( szItem != _T("") &&
					( szItem[ szItem.GetLength() - 1 ] == '1' ) )
					fInc = TRUE;
				else fInc = FALSE;
				nPos = szItem.Find( _T(":") );
				if( nPos != -1 ) szItem = szItem.Left( nPos );

				if( fInc && lstSubj.Find( szItem ) )
					pLstSortSubj->AddTail( szItem );
				else 
					lstExcSubj.AddTail( szItem );
			}
			f.Close();
		}

		// questionnaire info
		GetQuestions();
	}

	// Now loop through each line of INC and replace codes with indices for grp, subj, cond
	// ... also update options dlg lists for these numbers
	m_dlgOptions.m_lstGrp.RemoveAll();
	m_dlgOptions.m_lstGrp.AddTail( _T("All") );
	m_dlgOptions.m_lstSubj.RemoveAll();
	m_dlgOptions.m_lstSubj.AddTail( _T("All") );
	m_dlgOptions.m_lstCond.RemoveAll();
	m_dlgOptions.m_lstCond.AddTail( _T("All") );
	m_dlgOptions.m_lstTrial.RemoveAll();
	m_dlgOptions.m_lstTrial.AddTail( _T("All") );
	m_dlgOptions.m_lstStroke.RemoveAll();
	m_dlgOptions.m_lstStroke.AddTail( _T("All") );
// 	m_dlgOptions.m_lstSubMvmt.RemoveAll();
// 	m_dlgOptions.m_lstSubMvmt.AddTail( _T("All") );

	int nIdxGrp = 0, nIdxSubj = 0, nIdxCond = 0, nIdxTrial = 0, nIdxStroke = 0/*, nIdxSubMvmt = 0*/;
	CString szNewLine, szTemp, szGrpID, szSubjID, szAnswer, szLastGrpID,
			szLastSubjID, szLastLine, szCondID;
	POSITION pos = NULL, posGrp = NULL, posSubj = NULL;
	BOOL fFound = FALSE;

	m_lstGroups.RemoveAll();
	m_lstSubjs.RemoveAll();
	m_lstConds.RemoveAll();

	// 1st line is headers
	if( !fileINC.ReadString( szLine ) ) goto Cleanup;

	// if submovement analysis, determine column name for stroke
// 	if( m_fsma )
// 	{
// 		szNewLine = szLine;
// 		int nItem = 0;
// 		nPos = szNewLine.Find( _T(" ") );
// 		while( nPos != -1 )
// 		{
// 			szItem = szNewLine.Left( nPos );
// 			TRIM( szItem );
// 			szNewLine = szNewLine.Mid( nPos + 1 );
// 			TRIM( szNewLine );
// 
// 			if( nItem == COL_TRIAL )
// 			{
// 				szStrokeLabel = szItem;
// 				break;
// 			}
// 
// 			nItem++;
// 			nPos = szNewLine.Find( _T(" ") );
// 		}
// 
// 		// update headers for TMP file
// 		nPos = szLine.Find( szStrokeLabel );
// 		if( nPos != -1 ) szLine.Insert( nPos + szStrokeLabel.GetLength(), _T(" Submovement") );
// 	}
	szLine.Trim();
	fileTMP.WriteString( szLine );

	// question headers if applicable
	POSITION posQ = NULL;
	if( m_lstQuests.GetCount() > 0 )
	{
		posQ = m_lstQuests.GetHeadPosition();
		while( posQ )
		{
			Question* pQuest = (Question*)m_lstQuests.GetNext( posQ );
			if( pQuest )
			{
				szLine.Format( _T(" %s"), pQuest->szHdr );
				fileTMP.WriteString( szLine );
			}
		}
	}

	// end of headers
	fileTMP.WriteString( _T("\n") );

	// loop through each line of file
	while( fileINC.ReadString( szLine ) )
	{
		szLine.MakeUpper();
		// Get group ID
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szID = szLine.Left( nPos );
		szGrpID = szID;
		szLine = szLine.Mid( nPos + 1 );
		// Now find index for group
		nIdxGrp = 0;
		fFound = FALSE;
		if( !fSortGrp )
		{
			// Chronological sorting by default
			pos = lstGrp.GetHeadPosition();
			while( pos )
			{
				nIdxGrp++;
				szNewLine = lstGrp.GetNext( pos );
				if( szNewLine == szID )
				{
					fFound = TRUE;
					break;
				}
			}
		}
		else if( pLstSortGrp )
		{
			// Look in sort list first
			pos = pLstSortGrp->GetHeadPosition();
			while( pos )
			{
				nIdxGrp++;
				szNewLine = pLstSortGrp->GetNext( pos );
				if( szNewLine == szID )
				{
					fFound = TRUE;
					break;
				}
			}
			// If not found in sort list, we adjust for total in sort list
			if( !fFound )
			{
				pos = lstGrp.GetHeadPosition();
				while( pos )
				{
					nIdxGrp++;
					szNewLine = lstGrp.GetNext( pos );
					if( szNewLine == szID )
					{
						fFound = TRUE;
						break;
					}
				}
				nIdxGrp += (int)pLstSortGrp->GetCount();
			}
		}

		if( fFound && !lstExcGrp.Find( szID ) )
		{
			// Add group to identity list
			szTemp.Format( _T("%d:%s"), nIdxGrp, szID );
			if( !m_lstGroups.Find( szTemp ) )
			{
				m_lstGroups.AddTail( szTemp );
				posGrp = m_lstGroups.GetTailPosition();
			}
			else posGrp = NULL;

			// Get subject ID
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 ) break;
			szID = szLine.Left( nPos );
			szSubjID = szID;
			szLine = szLine.Mid( nPos + 1 );
			// Now find index for subject
			nIdxSubj = 0;
			fFound = FALSE;
			if( !fSortSubj )
			{
				// Chronological sorting by default
				pos = lstSubj.GetHeadPosition();
				while( pos )
				{
					nIdxSubj++;
					szNewLine = lstSubj.GetNext( pos );
					if( szNewLine == szID )
					{
						fFound = TRUE;
						break;
					}
				}
			}
			else if( pLstSortSubj )
			{
				// Look in sort list first
				pos = pLstSortSubj->GetHeadPosition();
				while( pos )
				{
					nIdxSubj++;
					szNewLine = pLstSortSubj->GetNext( pos );
					if( szNewLine == szID )
					{
						fFound = TRUE;
						break;
					}
				}
				// If not found in sort list, we adjust for total in sort list
				if( !fFound )
				{
					pos = lstSubj.GetHeadPosition();
					while( pos )
					{
						nIdxSubj++;
						szNewLine = lstSubj.GetNext( pos );
						if( szNewLine == szID )
						{
							fFound = TRUE;
							break;
						}
					}
					nIdxSubj += (int)pLstSortSubj->GetCount();
				}
			}

			if( fFound && !lstExcSubj.Find( szID ) )
			{
				// Add subject to identity list
				szTemp.Format( _T("%d:%s"), nIdxSubj, szID );
				if( !m_lstSubjs.Find( szTemp ) )
				{
					m_lstSubjs.AddTail( szTemp );
					posSubj = m_lstSubjs.GetTailPosition();
				}
				else posSubj = NULL;

				// Get condition ID
				nPos = szLine.Find( _T(" ") );
				if( nPos == -1 ) break;
				szID = szLine.Left( nPos );
				szCondID = szID;
				szLine = szLine.Mid( nPos + 1 );
				// Now find index for condition
				nIdxCond = 0;
				fFound = FALSE;
				if( !fSortCond )
				{
					// Chronological sorting by default
					pos = lstCond.GetHeadPosition();
					while( pos )
					{
						nIdxCond++;
						szNewLine = lstCond.GetNext( pos );
						if( szNewLine == szID )
						{
							fFound = TRUE;
							break;
						}
					}
				}
				else if( pLstSortCond )
				{
					// Look in sort list first
					pos = pLstSortCond->GetHeadPosition();
					while( pos )
					{
						nIdxCond++;
						szNewLine = pLstSortCond->GetNext( pos );
						if( szNewLine == szID )
						{
							fFound = TRUE;
							break;
						}
					}
					// If not found in sort list, we adjust for total in sort list
					if( !fFound )
					{
						pos = lstCond.GetHeadPosition();
						while( pos )
						{
							nIdxCond++;
							szNewLine = lstCond.GetNext( pos );
							if( szNewLine == szID )
							{
								fFound = TRUE;
								break;
							}
						}
						nIdxCond += (int)pLstSortCond->GetCount();
					}
				}
			}
			else fFound = FALSE;
		}
		else fFound = FALSE;

		if( fFound && !lstExcGrp.Find( szGrpID ) && !lstExcCond.Find( szID ) )
		{
			// Add condition to identity list
			szTemp.Format( _T("%d:%s"), nIdxCond, szID );
			if( !m_lstConds.Find( szTemp ) ) m_lstConds.AddTail( szTemp );

			// Get trial
			szNewLine = szLine;
			nPos = szNewLine.Find( _T(" ") );
			if( nPos == -1 ) break;
			szID = szNewLine.Left( nPos );
			szNewLine = szNewLine.Mid( nPos + 1 );
			nIdxTrial = atoi( szID );

			// Get stroke
			nPos = szNewLine.Find( _T(" ") );
			if( nPos == -1 ) break;
			szID = szNewLine.Left( nPos );
			nIdxStroke = atoi( szID );
			szNewLine = szNewLine.Mid( nPos + 1 );

			// sub-movement index
 			if( m_fsma )
 			{
// 				nps = (int)( ( nIdxStroke - 1 ) / 3 ) + 1;
// 				nIdxSubMvmt = nIdxStroke - ( (int)( ( nIdxStroke - 1 ) / 3 ) * 3 );
				nIdxStroke /= 3;
 			}

			// Replacement line for TMP file
			BOOL fInclude = TRUE;
			if( !fBypassSettings ) {
				if( ( m_dlgOptions.m_nGrp2 > 0 ) || ( m_dlgOptions.m_nSubj2 > 0 ) ||
					( m_dlgOptions.m_nCond2 > 0 ) || ( m_dlgOptions.m_nTrial2 > 0 ) ||
					( m_dlgOptions.m_nStroke2 > 0 ) /*|| ( m_dlgOptions.m_nSubMvmt2 > 0 )*/ )
				{
					if( fInclude && ( m_dlgOptions.m_nGrp2 > 0 ) )
					{
						if( nIdxGrp == m_dlgOptions.m_nGrp2 ) fInclude = TRUE;
						else fInclude = FALSE;
					}
					if( fInclude && ( m_dlgOptions.m_nSubj2 > 0 ) )
					{
						if( nIdxSubj == m_dlgOptions.m_nSubj2 ) fInclude = TRUE;
						else fInclude = FALSE;
					}
					if( fInclude && ( m_dlgOptions.m_nCond2 > 0 ) )
					{
						if( nIdxCond == m_dlgOptions.m_nCond2 ) fInclude = TRUE;
						else fInclude = FALSE;
					}
					if( fInclude && ( m_dlgOptions.m_nTrial2 > 0 ) )
					{
						if( nIdxTrial == m_dlgOptions.m_nTrial2 ) fInclude = TRUE;
						else fInclude = FALSE;
					}
					if( fInclude && ( m_dlgOptions.m_nStroke2 > 0 ) )
					{
						if( nIdxStroke == m_dlgOptions.m_nStroke2 ) fInclude = TRUE;
						else fInclude = FALSE;
					}
//	 				if( fInclude && ( m_dlgOptions.m_nSubMvmt2 > 0 ) )
// 					{
// 						if( nIdxSubMvmt == m_dlgOptions.m_nSubMvmt2 ) fInclude = TRUE;
// 						else fInclude = FALSE;
// 					}
				}
			}
			else fInclude = TRUE;
			if( fInclude )
			{
// 				if( m_fsma )
// 				{
// 					// submovement analysis
// 					szLine = szNewLine;
// 					szNewLine.Format( _T("%d %d %d %d %d %d %s"),
// 									  nIdxGrp, nIdxSubj, nIdxCond, nIdxTrial, nps, nIdxSubMvmt, szLine );
// 				}
// 				else szNewLine.Format( _T("%d %d %d %s"), nIdxGrp, nIdxSubj, nIdxCond, szLine );
				TRIM( szLine );
				if( fNoIdentities )
				{
					if( m_fsma )
					{
						szLine = szNewLine;
						szNewLine.Format( _T("%s %s %s %d %d %s"), szGrpID, szSubjID, szCondID, nIdxTrial, nIdxStroke, szLine );
					}
					else szNewLine.Format( _T("%s %s %s %s"), szGrpID, szSubjID, szCondID, szLine );
				}
				else
				{
					if( m_fsma )
					{
						szLine = szNewLine;
						szNewLine.Format( _T("%d %d %d %d %d %s"), nIdxGrp, nIdxSubj, nIdxCond, nIdxTrial, nIdxStroke, szLine );
					}
					else szNewLine.Format( _T("%d %d %d %s"), nIdxGrp, nIdxSubj, nIdxCond, szLine );
				}
				fileTMP.WriteString( szNewLine );

				// questions
				// ... if we get the answer for a question and the next item has
				// ... the same subject & group, we do not need to look up the answer again
				if( ( szSubjID != szLastSubjID ) || ( szGrpID != szLastGrpID ) )
				{
					szNewLine = _T("");
					posQ = m_lstQuests.GetHeadPosition();
					while( posQ )
					{
						// current question
						Question* pQuest = (Question*)m_lstQuests.GetNext( posQ );
						if( pQuest )
						{
							// look to see if subject has an answer
							hr = pSQ->Find( m_szExp.AllocSysString(), szGrpID.AllocSysString(),
											szSubjID.AllocSysString(), pQuest->szID.AllocSysString() );
							if( SUCCEEDED( hr ) )
							{
								// answer
								pSQ->get_Answer( &bstr );
								szAnswer = bstr;
								szAnswer.Trim();
								// if numeric, just output the "string->double" conversion
								// ...if empty, output missing
								if( pQuest->fNum )
								{
									if( szAnswer != _T("") )
									{
										dVal = _tcstod( szAnswer, NULL );
										szLine.Format( _T(" %g"), dVal );
									}
									else szLine.Format( _T(" %g"), ::GetMissingDataValue() );
								}
								// if it's alphanumeric
								else
								{
									// does the answer already exist in the list?
									POSITION posA = pQuest->FindAnswer( szAnswer );
									TRIM( szAnswer );
									if( !posA )
									{
										// no...so add it and update the index to the next val
										Answer* pA = new Answer;
										pA->nIdx = pQuest->lstA.GetCount() + 1;
										pA->szA = szAnswer;
										pQuest->lstA.AddTail( pA );
										if( szAnswer != _T("") )
										{
											if( fNoIdentities )
												szLine.Format( _T(" %s"), szAnswer );
											else
												szLine.Format( _T(" %d"), pA->nIdx );
										}
										else szLine.Format( _T(" %g"), ::GetMissingDataValue() );
									}
									else
									{
										if( szAnswer != _T("") )
										{
											if( !fNoIdentities )
											{
												// yes, so get the answer and get the associated index
												Answer* pA = (Answer*)pQuest->lstA.GetAt( posA );
												if( pA ) szLine.Format( _T(" %d"), pA->nIdx );
												else szLine.Format( _T(" %g"), ::GetMissingDataValue() );
											}
											else szLine.Format( _T(" %s"), szAnswer );
										}
										else szLine.Format( _T(" %g"), ::GetMissingDataValue() );
									}
								}
							}
							else szLine.Format( _T(" %g"), ::GetMissingDataValue() );

//							fileTMP.WriteString( szLine );
							szNewLine += szLine;
						}
					}
					szLastLine = szNewLine;
					szLastSubjID = szSubjID;
					szLastGrpID = szGrpID;
				}
				else szNewLine = szLastLine;

				fileTMP.WriteString( szNewLine );
				fileTMP.WriteString( _T("\n") );
			}

			// Options dlg lists
			// Groups
			szLine.Format( _T("%d"), nIdxGrp );
			if( !m_dlgOptions.m_lstGrp.Find( szLine ) ) m_dlgOptions.m_lstGrp.AddTail( szLine );
			// Subjects
			szLine.Format( _T("%d"), nIdxSubj );
			if( !m_dlgOptions.m_lstSubj.Find( szLine ) ) m_dlgOptions.m_lstSubj.AddTail( szLine );
			// Conditions
			szLine.Format( _T("%d"), nIdxCond );
			if( !m_dlgOptions.m_lstCond.Find( szLine ) ) m_dlgOptions.m_lstCond.AddTail( szLine );
			// Trials
			szLine.Format( _T("%d"), nIdxTrial );
			if( !m_dlgOptions.m_lstTrial.Find( szLine ) ) m_dlgOptions.m_lstTrial.AddTail( szLine );
			// Strokes
			szLine.Format( _T("%d"), nIdxStroke );
			if( !m_dlgOptions.m_lstStroke.Find( szLine ) ) m_dlgOptions.m_lstStroke.AddTail( szLine );
			// submovements
// 			if( m_fsma )
// 			{
// 				szLine.Format( _T("%d"), nIdxSubMvmt );
// 				if( !m_dlgOptions.m_lstSubMvmt.Find( szLine ) ) m_dlgOptions.m_lstSubMvmt.AddTail( szLine );
// 			}
		}
		else
		{
			if( posGrp ) m_lstGroups.RemoveAt( posGrp );
			if( posSubj ) m_lstSubjs.RemoveAt( posSubj );
		}

		nCur++;
		if( ( nCur % nStep ) == 0 )
		{
			dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
			szData.Format( _T("Progress %.1f %% - TMP: Line %u of INC file complete"), dVal, nCur );
			Progress::SetProgress( nCur, szData );
		}
	}

Cleanup:;

	if( pLstSortGrp ) delete pLstSortGrp;
	if( pLstSortCond ) delete pLstSortCond;
	if( pLstSortSubj ) delete pLstSortSubj;

	pSQ->Release();

	m_fTMPGenerated = TRUE;
	m_fAssociationsDone = TRUE;

	Progress::DisableProgress();
}

//************************************
// Method:    DoStatisticsEVA
// FullName:  AGraphDlg::DoStatisticsEVA
// Access:    protected 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szIn
// Parameter: CString szOut
// Parameter: int nX
// Parameter: int nY
// Parameter: int nGrp
//************************************
BOOL AGraphDlg::DoStatisticsEVA( CString szIn, CString szOut, int nX, int nY, int nGrp )
{
	bool GrpTest = 0;
    /* Test if group = NONE and set group = group in this case*/
	if(nGrp == 0)
	{
		/* Set a switch to indicate the difference between nGrp = 0 and nGrp = 1 for the labels*/
        GrpTest = 1;
		nGrp = 1;
	}
	if( ( nX <= 0 ) || ( nY <= 0 ) || ( nGrp < 0 ) ) return FALSE;

	// Convert (XM1 -> EVA)
	// TODO (we are not using these values yet)
	int nRepeatedMeasure = 2;
	double dExcludeY = m_dlgOptions.m_dExcludeValY;
	BOOL fExcludeY = m_dlgOptions.m_fExcludeY;

	// input file
	CStdioFile f;
	if( !f.Open( szIn, CFile::modeRead ) )
	{
		BCGPMessageBox( _T("Error reading xm1 input file.") );
		return FALSE;
	}

	// progress
	// steps for: obtaining min/max of subjects, groups, conditions; getting data values
	long nSteps = 0, nCur = 0;
	double dVal = 0.;
	CString szData, szLine;
	// 1 step per line * each section

	/* 14Oct03: hlt: do not process header line, so one less step to do */
	nSteps--;

	while( f.ReadString( szLine ) ) nSteps++;
	f.SeekToBegin();
	nSteps *= 2;	// min/max & data values
	nSteps += 1;	// outputting (TODO: perhaps we can expand this one too)
	Progress::EnableProgress( nSteps );

	// Get min/max of groups, subjects, conditions
	int nGroupMin = 0, nGroupMax = 0, nSubjMin = 0, nSubjMax = 0, nCondMin = 0, nCondMax = 0;
	int nLine = 0, nData = 0;
	double dataline[ NDATAMAX ];

	/********/
	/*Header*/
	/********/
    /* 14Oct03:hlt: Read column headers from .xme or .xm1 file */
	CString szHeaders;
	f.ReadString( szHeaders );
	/*19Nov03: New code to parse the header line and find the labels*/
	/*Corresponding to the values written in EVA file*/
		CString szValue;
		CString szColumnLabel1, szColumnLabel2,szColumnLabel3, szColumnLabel4,szHeaderLine;
	    CString szLineNew = szHeaders;
		int nItem = 0;
		int nPos = szLineNew.Find( _T(" ") );
		/* Split the header in to individual columns seperated by a space*/
		while( nPos != -1 )
		{
			szValue = szLineNew.Left( nPos );
			TRIM( szValue );
			szLineNew = szLineNew.Mid( nPos + 1 );
			TRIM( szLineNew );
			nItem++;
			/* If the column number matches with that of the value written in the EVA file, store the label*/
			/* nGRP, nRepeatedMeasure(subject), nY and nX can have same values and logic still works correctly*/
			if (nItem == nGrp)
			{
				szColumnLabel1 = szValue;
				/* If there is only no group (in which case we made nGrp = 1 and set a switch)then make the group label as '*' */
				if (nGrp == 1 && GrpTest == 1)
					szColumnLabel1 = "*";
			}
			if(nItem == nRepeatedMeasure)
			{
				szColumnLabel2 = szValue;
			}
			if(nItem == nY)
			{
				szColumnLabel3 = szValue;
			}
			if(nItem == nX)
			{
				szColumnLabel4 = szValue;
			}
			nPos = szLineNew.Find( _T(" ") );
		}
		/* Concatenate the labels to form the  header for the output file*/
    	szHeaderLine.Format( _T("%s %s %s PER %s"), szColumnLabel1,szColumnLabel2,szColumnLabel3, szColumnLabel4);
		

	while( TRUE )
	{
		nData = getlinefloat( f, dataline, NDATAMAX );
		if( nData == 0 ) break;

		nLine++;
		if( nLine == 1 )
		{
			nGroupMin = nGroupMax = (int)dataline[ nGrp - 1 ];
			nSubjMin = nSubjMax = (int)dataline[ nRepeatedMeasure - 1 ];
			nCondMin = nCondMax = (int)dataline[ nX - 1 ];
		}
		else
		{
			nGroupMin = MIN( nGroupMin, (int)dataline[ nGrp - 1 ] );
			nGroupMax = MAX( nGroupMax, (int)dataline[ nGrp - 1 ] );
			nSubjMin = MIN( nSubjMin, (int)dataline[ nRepeatedMeasure - 1 ] );
			nSubjMax = MAX( nSubjMax, (int)dataline[ nRepeatedMeasure - 1 ] );
			nCondMin = MIN( nCondMin, (int)dataline[ nX - 1 ] );
			nCondMax = MAX( nCondMax, (int)dataline[ nX - 1 ] );
		}

		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szData.Format( _T("Progress %.1f %% - STAT: Min/Max: Line %u complete"), dVal, nCur );
		Progress::SetProgress( nCur, szData );
	}
	f.SeekToBegin();

	// Data values
	COleSafeArray datapersubj;
	DWORD els[] = { nGroupMax, nSubjMax, nCondMax };
	TRY
	{
		datapersubj.Create( VT_R8, 3, els );
	}
	CATCH_ALL(e)
	{
		BCGPMessageBox( _T("Error") );
		Progress::DisableProgress();
		return FALSE;
	}
	END_CATCH_ALL
	int nGroup = 0, nSubj = 0, nCond = 0;
    /* 19Nov03: Raji: Need to read the header from XM1 file again */
	/* As the dataline is point to the very beginning of the file due to the fseekbegin() function*/
	f.ReadString( szHeaders );
	while( TRUE )
	{
		nData = getlinefloat( f, dataline, NDATAMAX );
		if( nData == 0 ) break;

		nGroup = (int)dataline[ nGrp - 1 ];
		nSubj = (int)dataline[ nRepeatedMeasure - 1 ];
		nCond = (int)dataline[ nX - 1 ];

		if( ( nGroup > 0 ) && ( nSubj > 0 ) && ( nCond > 0 ) )
		{
			long ind[] = { nGroup - 1, nSubj - 1, nCond - 1 };
			double dVal = dataline[ nY - 1 ];
			datapersubj.PutElement( ind, (LPVOID)&dVal );
		}

		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szData.Format( _T("Progress %.1f %% - STAT: Data: Line %u complete"), dVal, nCur );
		Progress::SetProgress( nCur, szData );
	}
	f.Close();

	// Output
	if( !f.Open( szOut, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Error opening eva file for output.") );
		Progress::DisableProgress();
		return FALSE;
	}
	nCur++;
	dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
	szData.Format( _T("Progress %.1f %% - STAT: Outputting data"), dVal );
	Progress::SetProgress( nCur, szData );
	/* 19Nov03: Raji: write the header szHeaderLine to output*/
	f.WriteString( szHeaderLine);
	f.WriteString( _T("\n") );
	for( nGroup = nGroupMin; nGroup <= nGroupMax; nGroup++ )
	{
		for( nSubj = nSubjMin; nSubj <= nSubjMax; nSubj++ )
		{
			szLine.Format( _T("%d %d"), nGroup, nSubj );
			f.WriteString( szLine );
			for( nCond = nCondMin; nCond <= nCondMax; nCond++ )
			{
				if( ( nGroup > 0 ) && ( nSubj > 0 ) && ( nCond > 0 ) )
				{
					long ind[] = { nGroup - 1, nSubj - 1, nCond - 1 };
					double dVal = 0;
					datapersubj.GetElement( ind, (LPVOID)&dVal );
					szLine.Format( _T(" %g"), dVal );
					f.WriteString( szLine );
				}
			}
			f.WriteString( _T("\n") );
		}
	}
	f.Close();

	// cleanup
	Progress::DisableProgress();

	return TRUE;
}
/* 05Jan04: New module to process averaging per trial*/
//************************************
// Method:    DoStatisticsGR1
// FullName:  AGraphDlg::DoStatisticsGR1
// Access:    protected 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szIn
// Parameter: CString szOut
// Parameter: int nX
// Parameter: int nY
// Parameter: int nGrp
//************************************
BOOL AGraphDlg::DoStatisticsGR1( CString szIn, CString szOut, int nX, int nY, int nGrp )
{	
	int ntrial = 0,nsegm = 0,nsubm = 0, itrial1 = 0,isegm1 = 0,isubm1 = 0, nData = 0, isegm = 0, isubm = 0, itrial = 0, idata = 0/*, submovement = 0*/;
	/* create a data array to store input data, array boundary incremented by 1, as the initial value is 1 not 0*/
	double data [NTRIAL + 1][NSEGM + 1][NSUBM + 1];
	int i,j,k;
	int averstroke;
	CString szHeaders, szHeaderLine;
	CString szValue, szoutLine;
	CStringArray szColumnLabel;
	CString szLineNew, szTemp;
	int nItem = 0;
	int nPos = szLineNew.Find( _T(" ") );
	double dataline[ NDATAMAX ];
	int nLine = 0;
	// input file
	CStdioFile f, fOut;
	int groupprev = 0, subjprev = 0, condprev = 0, trialprev = 0, submprev = 0;
	int group, subj, cond, trial, segm, subm;
	double averdata [NTRIAL][NSUBM];
	
	if( ( nX <= 0 ) || ( nY <= 0 ) || ( nGrp < 0 ) ) return FALSE;

	/*Zero array as there is no guarantee that all cells will be actually filled*/
    /*The data array needs to be filled with the missing data*/
	for (i = 0; i < NTRIAL; i++)
	{
		for (j = 0; j < NSEGM; j++)
		{
			for (k = 0; k < NSUBM; k++)
			{
				data [i][j][k] = 0;
			 }
		}
	}
	if( !f.Open( szIn, CFile::modeRead ) )
	{
		BCGPMessageBox( _T("Error reading tmp input file.") );
		return FALSE;
	}
	
	/* output file*/
	if( !fOut.Open( szOut, CFile::modeWrite | CFile::modeCreate) )
	{
		BCGPMessageBox( _T("Error reading gr1 output file.") );
		return FALSE;
	}

	/* If the column number of X-axis or the grouping variable equals column number segment, dont average across strokes*/
	if ((nX == SEGM) || (nY == SEGM) || (nGrp == SEGM))
		averstroke = 0;
	else 
		averstroke = 1;

	/*if submovement switch is ON, set submovement = 1*/
//	if (m_fsma) submovement = 1;
	
	/* Read the header line from the input file*/
	f.ReadString( szLineNew );
	/* Split the header in to individual columns seperated by a space*/
	nPos = szLineNew.Find( _T(" ") );
	while( nPos != -1 )
	{
		szValue = szLineNew.Left( nPos );
		TRIM( szValue );
		szLineNew = szLineNew.Mid( nPos + 1 );
		TRIM( szLineNew );
		nItem++;
		szColumnLabel.Add(szValue);
		nPos = szLineNew.Find( _T(" ") );
		
	}
	/* write header to output*/
	if (nGrp != 0)
		szTemp.Format (_T("%s Vs %s Per %s for each trial\n"), szColumnLabel.GetAt(nY - 1), szColumnLabel.GetAt(nX - 1), szColumnLabel.GetAt(nGrp - 1));
	else
        szTemp.Format (_T("%s Vs %s for each trial (No Grouping)\n"), szColumnLabel.GetAt(nY - 1), szColumnLabel.GetAt(nX - 1));
	fOut.WriteString( szTemp );
	/* Read and process the input tmp file line by line*/					
	while( TRUE )
	{	
		
		/* Get line*/
		nData = getlinefloat( f, dataline, NDATAMAX );
		if( nData > 0 )
		{
			nLine++;
			/* For each line extract the following*/ 
			group = (int)dataline[ GROUP - 1];
			subj = (int)dataline[ SUBJ - 1];
			cond = (int)dataline[ COND - 1];
			trial = (int)dataline[ TRIAL - 1];
			segm = (int)dataline[ SEGM - 1 ];	

			/* Assign the submovement m_fsma else If there are no submovements, just set it to submovement 1*/
// 			if(m_fsma) 
// 				subm = (int)dataline[ SUBM - 1];
// 			else
			{
				subm = 1;
				isubm1 = 1;
				nsubm = 1;
			}
				
			/* Initialize*/
			if (groupprev == 0)
				groupprev = group;
			if (subjprev == 0) 
				subjprev = subj;
			if (condprev == 0) 
				condprev = cond;
			if (trialprev == 0) 
				trialprev = trial;
// 			if (m_fsma) 
// 				if (submprev == 0) 
// 					submprev = subm;

			/* Find the first and last trial, stroke, submovement*/
			/* Assign to first value if not yet defined */
			if (itrial1 == 0) 
				itrial1 = trial;
			if (ntrial == 0) 
				ntrial = trial;
			/* Find minimum */
			if (trial < itrial1) 
				itrial1 = trial;
			/* Find maximum */
			if (trial > ntrial) 
				ntrial = trial;

			/* Assign to first value if not yet defined*/
			if (isegm1 == 0) 
				isegm1 = segm;
			if (nsegm == 0) 
				nsegm = segm;
			/* Find minimum */
			if (segm < isegm1) 
				isegm1 = segm;
			/* Find maximum */
			if (segm > nsegm) 
				nsegm = segm;
// 			if (m_fsma) 
// 			{
// 				/* Assign to first value if not yet defined*/
// 				if (isubm1 == 0) 
// 					isubm1 = subm;
// 				if (nsubm == 0) 
// 					nsubm = subm;
// 				/* Find minimum */
// 				if (subm < isubm1) 
// 					isubm1 = subm;
// 				/* Find maximum */
// 				if (subm > nsubm) 
// 					nsubm = subm;
// 			}
		}
		else
		{
			cond = -1;
		}
		if ((groupprev != group || subjprev != subj || condprev != cond)) 
		{
			/* If new condition write current data*/
			if (averstroke == 0) 
			{
				/* Output for all strokes */
				for (isegm = isegm1; isegm <= nsegm; isegm++) 
				{
					/* All submovements if any */
					for (isubm = isubm1; isubm <= nsubm; isubm++) 
					{ 
						/* Produce a package header*/
						szoutLine.Format (_T("%d %s%d_%s%d_%s%d_%s%d"), ntrial - itrial1 + 1, szColumnLabel.GetAt(GROUP - 1), groupprev, szColumnLabel.GetAt(SUBJ - 1), subjprev, szColumnLabel.GetAt(COND - 1), condprev, szColumnLabel.GetAt(SEGM - 1), isegm);
// 						if( m_fsma )
// 						{
// 							szTemp.Format (_T("_%s%d"), szColumnLabel.GetAt(SUBM - 1), isubm);
// 							szoutLine +=szTemp;
//                         }
						szTemp.Format (_T("\n"));
						szoutLine +=szTemp;
						fOut.WriteString( szoutLine );
						/* All repeated measures are already in one package */
						for (itrial = itrial1; itrial <= ntrial; itrial++) 
						{
							szoutLine.Format( _T(" %g"), data [itrial] [isegm] [isubm]);
							fOut.WriteString( szoutLine );
						}
						szTemp.Format (_T("\n"));
						fOut.WriteString( szTemp );
					}
				}
			}
			else 
			{
			/* All submovements if any */
				for (isubm = isubm1; isubm <= nsubm; isubm++) 
				{ 
					/* Produce a package header */
					szoutLine.Format (_T("%d %s%d_%s%d_%s%d"), ntrial - itrial1 + 1, szColumnLabel.GetAt(GROUP - 1), groupprev, szColumnLabel.GetAt(SUBJ - 1), subjprev, szColumnLabel.GetAt(COND - 1), condprev);
// 					if (m_fsma) 
// 					{
// 						szTemp.Format (_T("_%s%d"), szColumnLabel.GetAt(SUBM - 1), isubm);
// 						szoutLine +=szTemp;
// 					}
					szTemp.Format (_T("\n"));
					szoutLine +=szTemp;
					fOut.WriteString( szoutLine );
					/* All repeated measures are already in one package */
					for (itrial = itrial1; itrial <= ntrial; itrial++)
					{
						/* Average across all strokes */
						int nsegmtrial=0;
						averdata [itrial] [isubm] = 0.0;
						for (isegm = isegm1; isegm <= nsegm; isegm++) 
						{
							nsegmtrial++;
							averdata [itrial] [isubm] += data [itrial] [isegm] [isubm];
						}
						if (nsegmtrial > 0) 
							averdata [itrial] [isubm] /= nsegmtrial;
						szoutLine.Format( _T(" %g"), averdata [itrial][isubm]);
						fOut.WriteString( szoutLine );
					}
					szTemp.Format (_T("\n"));
					fOut.WriteString( szTemp );
				}
			}
			/* After writing data for the last condition, go to file close*/
			if (cond == -1)
				goto fileclose;
			/* Start counting and filling from scratch */
			ntrial=0;
			nsegm=0;
			nsubm=0;
			itrial1=0;
			isegm1=0;
			isubm1=0;
			/*Zero array as there is no guarantee that all cells will be actually filled*/
			/*The data array needs to be filled with the missing data*/
			for (i = 0; i < NTRIAL; i++)
			{
				for (j = 0; j < NSEGM; j++)
				{
					for (k = 0; k < NSUBM; k++)
					{
						data [i][j][k] = 0;
				    }
				}
			}

		}
		/* Store each trial for each segment*/
		data [trial] [segm] [subm] = dataline [nY - 1];

		/*To find that a new condition starts*/
		groupprev = group;
		subjprev = subj;
		condprev = cond;
		trialprev = trial;
//		if (m_fsma) submprev = subm;
	}

    fileclose:
	f.Close();
	fOut.Close();
	/* If starting with the next trial, print for each stroke, submovement ALL trials */

	return TRUE;
}

//************************************
// Method:    DoGraphA
// FullName:  AGraphDlg::DoGraphA
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::DoGraphA()
{
	UpdateData( TRUE );
	if( !m_fAvg && !m_fStdDev && !m_fTrial ) return;

	// get which item has focus before refreshing
	m_pWndFocus = GetFocus();

	// Do we xmean?
	BOOL fXmean = ( !m_fInit || !m_fInitG || ( m_nX != m_nXOld ) ||
					( m_nFactor1 != m_nFactor1Old ) || ( _AGDRefCnt > 1 ) || m_fEWChg );

	// temp file generation
	GenerateTmpFile();
	if( m_szTMP == _T("") ) return;

	CWaitCursor crs;

	// Obtain which graph, factors, and equal-weighting
	CString szFileIn = m_szTMP, szFileOut;
	HRESULT hr = E_FAIL;
	UINT nSwitch = eX_None;
	int nPos = -1;

	// Which columns
	int nX = 0, nY = 0;
	if( m_nColX <= 1 ) nX = ( m_nColX * _stdev_offset ) + m_nX;
	else nX = ( m_nColX * _stdev_offset ) + 1 + m_nX;
	if( m_nColY <= 1 ) nY = ( m_nColY * _stdev_offset ) + m_nY;
	else nY = ( m_nColY * _stdev_offset ) + 1 + m_nY;

	CString szXAxisMod, szYAxisMod;
	int nXColCnt = m_cboColX.GetCount();
	int nYColCnt = m_cboColY.GetCount();
	if( nXColCnt > 2 )
	{
		if( m_nColX == 1 ) szXAxisMod = _T("Avg of SD of");
		else if( m_nColX == 2 ) szXAxisMod = _T("SD of Avg of");
		else if( m_nColX == 3 ) szXAxisMod = _T("SD of SD of");
	}
	else if( m_nColX == 1 ) szXAxisMod = _T("SD of");
	if( nYColCnt > 2 )
	{
		if( m_nColY == 1 ) szYAxisMod = _T("Avg of SD of");
		else if( m_nColY == 2 ) szYAxisMod = _T("SD of Avg of");
		else if( m_nColY == 3 ) szYAxisMod = _T("SD of SD of");
	}
	else if( m_nColY == 1 ) szYAxisMod = _T("SD of");

	// WE ONLY REDO XMEAN IF INITIALIZING OR FACTORS HAVE CHANGED
	// If equal-weighting is selected, 2 step process
	if( fXmean )
	{
		if( m_fEqual )
		{
			// Output file
			nPos = m_szTMP.ReverseFind( '.' );
			szFileOut = m_szTMP.Left( nPos );
			szFileOut += _T(".XM1");
			
			// Switches
			nSwitch = eX_A;
			nSwitch |= eX_S;
			nSwitch |= eX_N;

			// Call XMean
			double dMax1 = -1., dMax2 = -1., dInMin1 = -1., dInMax1 = -1., dInMin2 = -1., dInMax2 = -1.;
			hr =		 ::XMean( szFileIn.AllocSysString(), szFileOut.AllocSysString(),
								  nX + 1, m_nFactor1,
								  m_dlgOffset.m_dOffset1, m_dlgOffset.m_dScale1,
								  m_dlgOffset.m_dOffset2, m_dlgOffset.m_dScale2,
								  nSwitch, dInMin1, dInMax1, dInMin2, dInMax2,
								  dMax1, dMax2, FALSE );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Either no correct trials or an error occurred. Check the results.") );
				return;
			}

			// Next in file was last outfile
			szFileIn = szFileOut;
		}
	}

	// Output file
	nPos = m_szTMP.ReverseFind( '.' );
	szFileOut = m_szTMP.Left( nPos );
	szFileOut += _T(".XME");

	// Switches
	nSwitch = eX_A;

	// WE ONLY REDO XMEAN IF INITIALIZING OR FACTORS HAVE CHANGED
	// Call XMean
	if( fXmean )
	{
		double dMax1 = -1., dMax2 = -1., dInMin1 = -1., dInMax1 = -1., dInMin2 = -1., dInMax2 = -1.;
		hr =		 ::XMean( szFileIn.AllocSysString(), szFileOut.AllocSysString(),
							  nX + 1, m_nFactor1,
							  m_dlgOffset.m_dOffset1, m_dlgOffset.m_dScale1,
							  m_dlgOffset.m_dOffset2, m_dlgOffset.m_dScale2,
							  nSwitch, dInMin1, dInMax1, dInMin2, dInMax2,
							  dMax1, dMax2, FALSE );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("XMEAN failure.") );
			return;
		}

		// determine if out of range/scale
		// the offsets/scales are used in subsequent calls to xmean
		if( ( dMax1 != -1. ) || ( dMax2 != -1. ) )
		{
			CString szMsg, szTemp;
			szMsg.Format( _T("X-axis or Grouping bins have data outside the range of 1-%.0f. Data can be optimally offset and scaled to fit inside the range of bins. View or Update offset or scale?"), dMax1 );
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				if( dMax1 != -1. )
				{
					m_dlgOffset.m_dOffset1Rec = 1 - dInMin1;
					if( ( dInMax1 - dInMin1 ) != 0. )
						m_dlgOffset.m_dScale1Rec = ( dMax1 - 1 ) / ( dInMax1 - dInMin1 );
					else
						m_dlgOffset.m_dScale1Rec = 1.;
					szMsg.Format( _T("Recommending x-bin offset of %g (default 0) and scaling of %g (default 1)."),
								  m_dlgOffset.m_dOffset1Rec, m_dlgOffset.m_dScale1Rec );
				}
				if( dMax2 != -1. )
				{
					m_dlgOffset.m_dOffset2Rec = 1 - dInMin2;
					if( ( dInMax2 - dInMin2 ) != 0. )
						m_dlgOffset.m_dScale2Rec = ( dMax2 - 1 ) / ( dInMax2 - dInMin2 );
					else
						m_dlgOffset.m_dScale2Rec = 1.;
					szTemp.Format( _T("Recommending group-bin offset of %g (default 0) and scaling of %g (default 1)."),
								   m_dlgOffset.m_dOffset1Rec, m_dlgOffset.m_dScale1Rec );
					if( dMax1 != -1. ) szMsg += _T("\n");
					szMsg += szTemp;
				}
				if( szMsg != _T("") ) OutputAMessage( szMsg, TRUE, LOG_PROC );

				if( m_dlgOffset.DoModal() == IDOK )
				{
					BCGPMessageBox( _T("Offset/scaling adjusted. Refreshing chart.") );
					m_dlgOffset.m_dOffset1Rec = -1.;
					m_dlgOffset.m_dScale1Rec = 0.;
					m_dlgOffset.m_dOffset2Rec = -1.;
					m_dlgOffset.m_dScale2Rec = 0.;
					OnClickBnRefresh();
					return;
				}
				m_dlgOffset.m_dOffset1Rec = -1.;
				m_dlgOffset.m_dScale1Rec = 0.;
				m_dlgOffset.m_dOffset2Rec = -1.;
				m_dlgOffset.m_dScale2Rec = 0.;
				// else, we're continuing, so start the wait cursor again
				crs.Restore();
			}
			// else, we're continuing, so start the wait cursor again
			crs.Restore();
		}
	}

	if( !m_fInitG ) m_fInitG = TRUE;

	// Now we graph
	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	int nGraphX = m_nX + 1;
	int nGraphY = m_nY + 1;
	int nStdDev = -1;
	CString szGraph, szSub, szX, szY;
	m_cboX.GetLBText( m_cboX.GetCurSel(), szX );
	m_cboY.GetLBText( m_cboY.GetCurSel(), szY );
	szGraph.Format( _T("%s vs %s"), szX, szY );
	CString szTemp;
	if( szXAxisMod != _T("") )
	{
		szTemp.Format( _T("%s %s"), szXAxisMod, szX );
		szX = szTemp;
	}
	if( szYAxisMod != _T("") )
	{
		szTemp.Format( _T("%s %s"), szYAxisMod, szY );
		szY = szTemp;
	}

	if( m_fStdDev && m_fAvg )
	{
		szSub = _T("Averages with Std. Devs.");
		if( ( m_nChart == CHART_AVGSDN ) || ( m_nChart == CHART_TAVGSDN ) )
			szSub += _T("/Sqrt(N)");
		nStdDev = nGraphY + _stdev_offset;
	}
	else if( m_fAvg )
	{
		szSub = _T("Averages");
	}
	if( m_fEqual ) szSub += _T(" (Averaging first per subject)");
	szTemp.Format( _T(" (%s)"), m_szExp );
	szSub += szTemp;
	// 17Jul09: GMB: Adding notice that custom offset/scale being used
	if( m_dlgOffset.IsCustom() ) szSub += _T("  *Offset/scale lumping.");

	pShow->put_UseSpecificStrokes( m_fUseStrokes );
	if( m_fUseStrokes )
	{
		pShow->put_XStroke( (short)m_nXStroke );
		pShow->put_YStroke( (short)m_nYStroke );
	}

	pShow->put_SubsetColumn( (short)m_nFactor1 );
	m_cboF1.GetLBText( m_nFactor1, szTemp );
	pShow->put_SubsetName( szTemp.AllocSysString() );

	m_pGraph = GetDlgItem( IDC_GRAPH );

	Progress::EnableProgress( 2 );
	Progress::SetProgress( 1, _T("Graph is now loading.Please wait...") );
	hr = pShow->ShowStatGraph( (VARIANT*)m_pGraph, m_fScale, szFileOut.AllocSysString(),
							   m_szTMP.AllocSysString(), m_szExp.AllocSysString(),
							   szTemp.AllocSysString(), szGraph.AllocSysString(),
							   szSub.AllocSysString(), szX.AllocSysString(),
							   szY.AllocSysString(), nX + 1, nY + 1,
							   (short)nGraphX, (short)nGraphY, (short)nStdDev,
							   (VARIANT*)&m_hPE );
	Progress::SetProgress( 2, _T("Complete.") );
	Progress::DisableProgress();

	// reset focus to control that had focus before refresh
	if( m_pWndFocus ) m_pWndFocus->SetFocus();

	m_fEWChg = FALSE;
}

//************************************
// Method:    DoGraphS
// FullName:  AGraphDlg::DoGraphS
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::DoGraphS()
{
	GenerateTmpFile();
	if( m_szTMP == _T("") ) return;

	CWaitCursor crs;

	UpdateData( TRUE );				// Obtain axes
	HRESULT hr = E_FAIL;

	// get which item has focus before refreshing
	m_pWndFocus = GetFocus();

	// Graph
	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	int nX = m_nX;
	int nY = m_nY;

	CString szX, szY;
	m_cboX.GetLBText( m_cboX.GetCurSel(), szX );
	m_cboY.GetLBText( m_cboY.GetCurSel(), szY );
	m_pGraph = GetDlgItem( IDC_GRAPH );

	pShow->put_UseSpecificStrokes( m_fUseStrokes );
	if( m_fUseStrokes )
	{
		pShow->put_XStroke( (short)m_nXStroke );
		pShow->put_YStroke( (short)m_nYStroke );
	}

	pShow->put_SubsetColumn( (short)m_nFactor1 );
	CString szTemp;
	m_cboF1.GetLBText( m_nFactor1, szTemp );
	pShow->put_SubsetName( szTemp.AllocSysString() );

	Progress::EnableProgress( 2 );
	Progress::SetProgress( 1, _T("Graph is now loading.Please wait...") );
	hr = pShow->ShowScatter( (VARIANT*)m_pGraph, m_szTMP.AllocSysString(),
							  m_szExp.AllocSysString(), szTemp.AllocSysString(),
							  szX.AllocSysString(), szY.AllocSysString(),
							  nX + 1, nY + 1, (VARIANT*)&m_hPE );
	Progress::SetProgress( 2, _T("Complete.") );
	Progress::DisableProgress();

	// reset focus to control that had focus before refresh
	if( m_pWndFocus ) m_pWndFocus->SetFocus();
}

//************************************
// Method:    DoGraphCombo
// FullName:  AGraphDlg::DoGraphCombo
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::DoGraphCombo()
{
	UpdateData( TRUE );
	if( !m_fAvg && !m_fStdDev && !m_fTrial ) return;

	// get which item has focus before refreshing
	m_pWndFocus = GetFocus();

	// Do we xmean?
	BOOL fXmean = ( !m_fInit || !m_fInitG || ( m_nX != m_nXOld ) ||
					( m_nFactor1 != m_nFactor1Old ) || ( _AGDRefCnt > 1 ) || m_fEWChg );

	// tmp file generation
	GenerateTmpFile();
	if( m_szTMP == _T("") ) return;

	CWaitCursor crs;

	// Obtain which graph, factors, and equal-weighting
	CString szFileIn = m_szTMP, szFileOut;
	HRESULT hr = E_FAIL;
	UINT nSwitch = eX_None;
	int nPos = -1;

	int nX = 0, nY = 0;
	if( m_nColX <= 1 ) nX = ( m_nColX * _stdev_offset ) + m_nX;
	else nX = ( m_nColX * _stdev_offset ) + 1 + m_nX;
	if( m_nColY <= 1 ) nY = ( m_nColY * _stdev_offset ) + m_nY;
	else nY = ( m_nColY * _stdev_offset ) + 1 + m_nY;

	CString szXAxisMod, szYAxisMod;
	int nXColCnt = m_cboColX.GetCount();
	int nYColCnt = m_cboColY.GetCount();
	if( nXColCnt > 2 )
	{
		if( m_nColX == 1 ) szXAxisMod = _T("Avg of SD of");
		else if( m_nColX == 2 ) szXAxisMod = _T("SD of Avg of");
		else if( m_nColX == 3 ) szXAxisMod = _T("SD of SD of");
	}
	else if( m_nColX == 1 ) szXAxisMod = _T("SD of");
	if( nYColCnt > 2 )
	{
		if( m_nColY == 1 ) szYAxisMod = _T("Avg of SD of");
		else if( m_nColY == 2 ) szYAxisMod = _T("SD of Avg of");
		else if( m_nColY == 3 ) szYAxisMod = _T("SD of SD of");
	}
	else if( m_nColY == 1 ) szYAxisMod = _T("SD of");

	// WE ONLY REDO XMEAN IF INITIALIZING OR FACTORS HAVE CHANGED
	// If equal-weighting is selected, 2 step process
	if( fXmean )
	{
		if( m_fEqual )
		{
			// Output file
			nPos = m_szTMP.ReverseFind( '.' );
			szFileOut = m_szTMP.Left( nPos );
			szFileOut += _T(".XM1");

			// Switches
			nSwitch = eX_A;
			nSwitch |= eX_S;
			nSwitch |= eX_N;

			// Call XMean
			double dMax1 = -1., dMax2 = -1., dInMin1 = -1., dInMax1 = -1., dInMin2 = -1., dInMax2 = -1.;
			hr =		 ::XMean( szFileIn.AllocSysString(), szFileOut.AllocSysString(),
								  nX + 1, m_nFactor1,
								  m_dlgOffset.m_dOffset1, m_dlgOffset.m_dScale1,
								  m_dlgOffset.m_dOffset2, m_dlgOffset.m_dScale2,
								  nSwitch, dInMin1, dInMax1, dInMin2, dInMax2,
								  dMax1, dMax2, FALSE );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("XMEAN failure.") );
				return;
			}

			// Next in file was last outfile
			szFileIn = szFileOut;
		}
	}

	// Output file
	nPos = m_szTMP.ReverseFind( '.' );
	szFileOut = m_szTMP.Left( nPos );
	szFileOut += _T(".XME");

	// Switches
	nSwitch = eX_A;

	// WE ONLY REDO XMEAN IF INITIALIZING OR FACTORS HAVE CHANGED
	// Call XMean
	if( !m_fInit || !m_fInitG || ( m_nX != m_nXOld ) ||
		( m_nFactor1 != m_nFactor1Old ) || ( _AGDRefCnt > 1 ) || m_fEWChg )
	{
		double dMax1 = -1., dMax2 = -1., dInMin1 = -1., dInMax1 = -1., dInMin2 = -1., dInMax2 = -1.;
		hr =		 ::XMean( szFileIn.AllocSysString(), szFileOut.AllocSysString(),
							  nX + 1, m_nFactor1,
							  m_dlgOffset.m_dOffset1, m_dlgOffset.m_dScale1,
							  m_dlgOffset.m_dOffset2, m_dlgOffset.m_dScale2,
							  nSwitch, dInMin1, dInMax1, dInMin2, dInMax2,
							  dMax1, dMax2, FALSE );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("XMEAN failure.") );
			return;
		}

		// determine if out of range/scale
		// the offsets/scales are used in subsequent calls to xmean
		if( ( dMax1 != -1. ) || ( dMax2 != -1. ) )
		{
			CString szMsg, szTemp;
			szMsg.Format( _T("X-axis or Grouping bins have data outside the range of 1-%.0f. Data can be optimally offset and scaled to fit inside the range of bins. View or Update offset or scale?"), dMax1 );
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				if( dMax1 != -1. )
				{
					m_dlgOffset.m_dOffset1Rec = 1 - dInMin1;
					if( ( dInMax1 - dInMin1 ) != 0. )
						m_dlgOffset.m_dScale1Rec = ( dMax1 - 1 ) / ( dInMax1 - dInMin1 );
					else
						m_dlgOffset.m_dScale1Rec = 1.;
					szMsg.Format( _T("Recommending x-bin offset of %g (default 0) and scaling of %g (default 1)."),
								  m_dlgOffset.m_dOffset1Rec, m_dlgOffset.m_dScale1Rec );
				}
				if( dMax2 != -1. )
				{
					m_dlgOffset.m_dOffset2Rec = 1 - dInMin2;
					if( ( dInMax2 - dInMin2 ) != 0. )
						m_dlgOffset.m_dScale2Rec = ( dMax2 - 1 ) / ( dInMax2 - dInMin2 );
					else
						m_dlgOffset.m_dScale1Rec = 1.;
					szTemp.Format( _T("Recommending group-bin offset of %g (default 0) and scaling of %g (default 1)."),
								   m_dlgOffset.m_dOffset1Rec, m_dlgOffset.m_dScale1Rec );
					if( dMax1 != -1. ) szMsg += _T("\n");
					szMsg += szTemp;
				}
				if( szMsg != _T("") ) OutputAMessage( szMsg, TRUE, LOG_PROC );

				if( m_dlgOffset.DoModal() == IDOK )
				{
					BCGPMessageBox( _T("Offset/scaling adjusted. Refreshing chart.") );
					m_dlgOffset.m_dOffset1Rec = -1.;
					m_dlgOffset.m_dScale1Rec = 0.;
					m_dlgOffset.m_dOffset2Rec = -1.;
					m_dlgOffset.m_dScale2Rec = 0.;
					OnClickBnRefresh();
					return;
				}
				m_dlgOffset.m_dOffset1Rec = -1.;
				m_dlgOffset.m_dScale1Rec = 0.;
				m_dlgOffset.m_dOffset2Rec = -1.;
				m_dlgOffset.m_dScale2Rec = 0.;
				// else, we're continuing, so start the wait cursor again
				crs.Restore();
			}
			// else, we're continuing, so start the wait cursor again
			crs.Restore();
		}
	}

	if( !m_fInitG ) m_fInitG = TRUE;

	// Now we graph
	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	int nGraphX = m_nX + 1;
	int nGraphY = m_nY + 1;
	int nStdDev = -1;

	CString szGraph, szSub, szX, szY;

	if( m_fStdDev && m_fAvg )
	{
		szSub = _T("Averages with Std. Devs.");
		if( ( m_nChart == CHART_AVGSDN ) || ( m_nChart == CHART_TAVGSDN ) )
			szSub += _T("/Sqrt(N)");
		nStdDev = nGraphY + _stdev_offset;
	}
	else
	{
		szSub = _T("Averages");
	}
	m_cboX.GetLBText( m_cboX.GetCurSel(), szX );
	m_cboY.GetLBText( m_cboY.GetCurSel(), szY );
	szGraph.Format( _T("%s vs %s"), szX, szY );
	CString szTemp;
	if( szXAxisMod != _T("") )
	{
		szTemp.Format( _T("%s %s"), szXAxisMod, szX );
		szX = szTemp;
	}
	if( szYAxisMod != _T("") )
	{
		szTemp.Format( _T("%s %s"), szYAxisMod, szY );
		szY = szTemp;
	}

	if( m_fEqual ) szSub += _T(" (Averaging first per subject)");
	szTemp.Format( _T(" (%s)"), m_szExp );
	szSub += szTemp;
	// 17Jul09: GMB: Adding notice that custom offset/scale being used
	if( m_dlgOffset.IsCustom() ) szSub += _T("  *Offset/scale lumping.");

	pShow->put_UseSpecificStrokes( m_fUseStrokes );
	if( m_fUseStrokes )
	{
		pShow->put_XStroke( (short)m_nXStroke );
		pShow->put_YStroke( (short)m_nYStroke );
	}

	pShow->put_SubsetColumn( (short)m_nFactor1 );
	m_cboF1.GetLBText( m_nFactor1, szTemp );
	pShow->put_SubsetName( szTemp.AllocSysString() );

	Progress::EnableProgress( 2 );
	Progress::SetProgress( 1, _T("Graph is now loading.Please wait...") );
	m_pGraph = GetDlgItem( IDC_GRAPH );
	hr = pShow->ShowCombo( (VARIANT*)m_pGraph, TRUE, szFileOut.AllocSysString(),
						   m_szTMP.AllocSysString(), m_szExp.AllocSysString(),
						   szTemp.AllocSysString(), szGraph.AllocSysString(),
						   szSub.AllocSysString(), szX.AllocSysString(),
						   szY.AllocSysString(), nX + 1, nY + 1,
						   (short)nGraphX, (short)nGraphY, (short)nStdDev,
						   (VARIANT*)&m_hPE );
	m_fEWChg = FALSE;
	Progress::SetProgress( 2, _T("Complete.") );
	Progress::DisableProgress();

	// reset focus to control that had focus before refresh
	if( m_pWndFocus ) m_pWndFocus->SetFocus();
}

/////////////////////////////////////////////////////////////////////////////
// AGraphDlg message handlers

//************************************
// Method:    OnInitDialog
// FullName:  AGraphDlg::OnInitDialog
// Access:    virtual protected 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL AGraphDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// Dialog icon
	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDI_CHART ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// nothing to refresh initially
	SetRefresh( FALSE );
	_colN = 0;
	_stdev_offset = 0;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_CHART );
	m_tooltip.SetTitle( _T("ANALYSIS GRAPH") );
	CWnd* pWnd = GetDlgItem( IDC_GRAPH );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_GRAPH, _T("Graph") );
	pWnd = GetDlgItem( IDC_CBO_FACTOR1 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an averaging factor."), _T("Grouping") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_CLOSE, _T("Close") );
	pWnd = GetDlgItem( IDC_CHK_STROKES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check here to specify certain strokes."), _T("Strokes") );
	pWnd = GetDlgItem( IDC_EDIT_XSTROKE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify X-Stroke here."), _T("X-Stroke") );
	pWnd = GetDlgItem( IDC_EDIT_YSTROKE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify Y-Stroke here."), _T("Y-Stroke") );
	pWnd = GetDlgItem( IDC_CBO_X );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Choose the statistic for the x (horizontal) axis."), _T("X-Axis") );
	pWnd = GetDlgItem( IDC_CBO_Y );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Choose the statistic for the y (vertical) axis."), _T("Y-Axis") );
	pWnd = GetDlgItem( IDC_CHK_SCALE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Scale avg/std. dev. graphs the same as trials chart.\nBoth the X and Y axes are scaled to match the chart of trials."), _T("Scale") );
	pWnd = GetDlgItem( IDC_CHK_EQUAL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to average 1st per subject, then across all subjects."), _T("Equal Weight") );
	pWnd = GetDlgItem( IDC_CHK_EQUAL2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to average 1st per subject per trial, then across all subjects."), _T("Trial Equal Weight") );
	pWnd = GetDlgItem( IDC_CBO_WHICH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select your desired chart here."), _T("Select Chart") );
	pWnd = GetDlgItem( IDC_CBO_COLX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select which statistic of the X-axis you desire."), _T("X-Axis Statistic") );
	pWnd = GetDlgItem( IDC_CBO_COLY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select which statistic of the Y-axis you desire."), _T("Y-Axis Statistic") );
	pWnd = GetDlgItem( IDC_BN_REFRESH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Refresh chart."), _T("Refresh") );
	pWnd = GetDlgItem( IDC_BN_GO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to view a menu of available settings and actions."), _T("Actions/Settings") );

	// action menu
	m_mnuGo.LoadMenu( IDR_AGRAPH_ACTION );
	m_bnGo.m_hMenu = m_mnuGo.GetSubMenu( 0 )->GetSafeHmenu();
	m_bnGo.m_bOSMenu = FALSE;

	// Graph window
	m_pGraph = GetDlgItem( IDC_GRAPH );

	pWnd = GetDlgItem( IDC_CHK_EQUAL2 );
	pWnd->EnableWindow( m_fEqual );

	// TMP file
	int nPos = m_szINC.ReverseFind( '.' );
	m_szTMP = m_szINC.Left( nPos );
	m_szTMP += _T(".TMP");

	// Get "dynamic" list of categories
	CStringList lst;
	CString szItem, szHFile;
	CFileFind ff;
	if( ff.FindFile( m_szTMP ) )
	{
		szHFile = m_szTMP;
		m_fTMPGenerated = TRUE;
	}
	else szHFile = m_szINC;
	CStdioFile fileINC;
	CString szStrokeLabel;
	if( fileINC.Open( szHFile, CFile::modeRead ) )
	{
		CString szLine;
		// 1st line "should" be the headers
		fileINC.ReadString( szLine );

		// parse it (separated by spaces
		nPos = -1;
		while( szLine != _T("") )
		{
			nPos = szLine.Find( _T(" ") );
			if( nPos != -1 )
			{
				szItem = szLine.Left( nPos );
				szLine = szLine.Mid( nPos + 1 );
				_colN++;
			}
			else
			{
				szItem = szLine;
				szLine = _T("");
				_colN++;
			}

			TRIM( szItem );
			lst.AddTail( szItem );
			m_lstAxisD.AddTail( szItem );
			TRIM( szLine );
		}

		fileINC.Close();
	}

	// if TMP not generated (axes pulled from INC), we need to add questionnaire col headers
	if( !m_fTMPGenerated && GetQuestions() )
	{
		POSITION posQ = m_lstQuests.GetHeadPosition();
		while( posQ )
		{
			Question* pQuest = (Question*)m_lstQuests.GetNext( posQ );
			if( pQuest )
			{
				lst.AddTail( pQuest->szHdr );
				m_lstAxisD.AddTail( pQuest->szHdr );
				_colN++;
			}
		}
	}

	// axis customization
	CString szMem;
	::GetDataPathRoot( szMem );
	szMem += _T("\\");
	szMem += m_szExp;
	szMem += _T("\\graph.axs");
	BOOL fDefault = TRUE;
	if( ff.FindFile( szMem ) )
	{
		int nCnt = (int)m_lstAxisD.GetCount(), nCur = 0;
		CStdioFile f;
		if( f.Open( szMem, CFile::modeRead ) )
		{
			while( nCur < nCnt )
			{
				if( f.ReadString( szMem ) )
					m_lstAxis.AddTail( szMem );
				else
					m_lstAxis.AddTail( _T("UNKNOWN") );

				nCur++;
			}
			fDefault = FALSE;
		}
		else BCGPMessageBox( _T("Unable to open axes customization file.") );
	}
	else fDefault = TRUE;

	// FILL COMBOS
	// factor combo
	m_cboF1.AddString( _T("NONE") );
	POSITION pos = lst.GetHeadPosition();
	while( pos )
	{
		szItem = lst.GetNext( pos );
		if( fDefault ) m_lstAxis.AddTail( szItem );
		m_cboF1.AddString( szItem );
	}
	// N we add since not in list
	m_lstAxisD.AddTail( _T("N") );
	m_lstAxis.AddTail( _T("N") );
	_colN++;

	// Fill axes combos
	FillAxes();
	// Fill chart combo
	m_cboChart.AddString( _T("NO CHART SELECTED") );
	m_cboChart.AddString( _T("Trial Scatter Plot") );
	m_cboChart.AddString( _T("Averages") );
	m_cboChart.AddString( _T("Trials + Avgs") );
	m_cboChart.AddString( _T("Avgs +/- Std Devs") );
	m_cboChart.AddString( _T("Avgs +/- Std Devs/sqrt(N)") );
	m_cboChart.AddString( _T("Trials + Avgs +/- Std Devs") );
	m_cboChart.AddString( _T("Trials + Avgs +/- Std Devs/sqrt(N)") );
	// Fill X/Y column offset combos
	if( m_fEqual )
	{
		m_cboColX.AddString( _T("Avgs of Avgs") );
		m_cboColX.AddString( _T("Avgs of SDs") );
		m_cboColX.AddString( _T("SDs of Avgs") );
		m_cboColX.AddString( _T("SDs of SDs") );
		m_cboColY.AddString( _T("Avgs of Avgs") );
		m_cboColY.AddString( _T("Avgs of SDs") );
		m_cboColY.AddString( _T("SDs of Avgs") );
		m_cboColY.AddString( _T("SDs of SDs") );
	}
	else
	{
		m_cboColX.AddString( _T("Avgs") );
		m_cboColX.AddString( _T("SDs") );
		m_cboColY.AddString( _T("Avgs") );
		m_cboColY.AddString( _T("SDs") );
	}
	m_cboColX.SetCurSel( m_nColX );
	m_cboColY.SetCurSel( m_nColY );

	// offset to standard deviations in xmean
	_stdev_offset = _colN;

	// Check for last state
	CFile f;
	::GetDataPathRoot( szMem );
	szMem += _T("\\");
	szMem += m_szExp;
	szMem += _T("\\graph.mem");
	if( f.Open( szMem, CFile::modeRead | CFile::typeBinary ) )
	{
		GraphMem gm;
		memset( &gm, 0, sizeof( GraphMem ) );
		f.Read( &gm, sizeof( GraphMem ) );
		f.Close();

		// Main window
		m_nX = gm.nX;
		if( !CRANGE( m_nX, 0, _colN ) ) m_nX = 0;
		m_nColX = gm.nColX;
		if( !CRANGE( m_nColX, 0, 3 ) ) m_nColX = 0;
		if( ( m_nColX > 1 ) && !m_fEqual ) m_nColX -= 2;
		m_nY = gm.nY;
		if( !CRANGE( m_nY, 0, _colN ) ) m_nY = 0;
		m_nColY = gm.nColY;
		if( !CRANGE( m_nColY, 0, 3 ) ) m_nColY = 0;
		if( ( m_nColY > 1 ) && !m_fEqual ) m_nColY -= 2;
		m_nFactor1 = gm.nGrp;
		if( !CRANGE( m_nFactor1, 0, _colN - 1 ) ) m_nFactor1 = 0;
		m_fUseStrokes = gm.fStroke;
		if( !CBOOL( m_fUseStrokes ) ) m_fUseStrokes = FALSE;
		m_nXStroke = gm.nXStroke;
		if( !CRANGE( m_nXStroke, 0, 100 ) ) m_nXStroke = 0;
		m_nYStroke = gm.nYStroke;
		if( !CRANGE( m_nYStroke, 0, 100 ) ) m_nYStroke = 0;
		// Point inclusion/dispersion/exclusion
		//Yi Jun.12.2007 fix the problem of no data in TMP file
// GMB: 2008-07-02: Why? Blindly setting this to 0 breaks the point inclusion logic (memory).
//		gm.nColor = 0;
//		gm.nGroup = 0;
		
		m_dlgOptions.m_nGrp2 = gm.nGroup;
		m_dlgOptions.m_nSubj2 = gm.nSubj;
		m_dlgOptions.m_nCond2 = gm.nCond;
		m_dlgOptions.m_nTrial2 = gm.nTrial;
		m_dlgOptions.m_nStroke2 = gm.nStroke;
//		m_dlgOptions.m_nSubMvmt2 = gm.nSubMvmt;
		m_dlgOptions.m_fDispX = gm.fDispX;
		if( !CBOOL( m_dlgOptions.m_fDispX ) ) m_dlgOptions.m_fDispX = FALSE;
		m_dlgOptions.m_nDisp = gm.nDispX;
		if( !CRANGE( m_dlgOptions.m_nDisp, 0, 20 ) ) m_dlgOptions.m_nDisp = 0;
		m_dlgOptions.m_dStrength = gm.dDispX;
		if( !CRANGE( m_dlgOptions.m_dStrength, -1000.0, 1000.0 ) ) m_dlgOptions.m_dStrength = 0.0;
		m_dlgOptions.m_fDispY = gm.fDispY;
		if( !CBOOL( m_dlgOptions.m_fDispY ) ) m_dlgOptions.m_fDispY = FALSE;
		m_dlgOptions.m_nDisp2 = gm.nDispY;
		if( !CRANGE( m_dlgOptions.m_nDisp2, 0, 20 ) ) m_dlgOptions.m_nDisp2 = 0;
		m_dlgOptions.m_dStrength2 = gm.dDispY;
		if( !CRANGE( m_dlgOptions.m_dStrength2, -1000.0, 1000.0 ) ) m_dlgOptions.m_dStrength2 = 0.0;
		m_dlgOptions.m_fExcludeX = gm.fExcludeX;
		if( !CBOOL( m_dlgOptions.m_fExcludeX ) ) m_dlgOptions.m_fExcludeX = FALSE;
		m_dlgOptions.m_dExcludeValX = gm.dExcludeX;
		if( !CRANGE( m_dlgOptions.m_dExcludeValX, -10000000.0, 10000000.0 ) ) m_dlgOptions.m_dExcludeValX = 0.0;
		m_dlgOptions.m_fExcludeY = gm.fExcludeY;
		if( !CBOOL( m_dlgOptions.m_fExcludeY ) ) m_dlgOptions.m_fExcludeY = FALSE;
		m_dlgOptions.m_dExcludeValY = gm.dExcludeY;
		if( !CRANGE( m_dlgOptions.m_dExcludeValY, 10000000.0, 10000000.0 ) ) m_dlgOptions.m_dExcludeValY = 0.0;
		// Flagging
		m_dlgFlagging.m_fFlag = gm.fFlag;
		if( !CBOOL( m_dlgFlagging.m_fFlag ) ) m_dlgFlagging.m_fFlag = FALSE;
		m_dlgFlagging.m_nY = gm.nFlag;
		if( !CRANGE( m_dlgFlagging.m_nY, 0, 100 ) ) m_dlgFlagging.m_nY = 0;
		m_dlgFlagging.m_dMin = gm.dFlagMin;
		if( !CRANGE( m_dlgFlagging.m_dMin, 10000000.0, 10000000.0 ) ) m_dlgFlagging.m_dMin = 0.0;
		m_dlgFlagging.m_dMax = gm.dFlagMax;
		if( !CRANGE( m_dlgFlagging.m_dMax, 10000000.0, 10000000.0 ) ) m_dlgFlagging.m_dMax = 0.0;
		m_dlgFlagging.m_nYPoint = gm.nPoint;
		if( !CRANGE( m_dlgFlagging.m_nYPoint, 0, 100 ) ) m_dlgFlagging.m_nYPoint = 0;
		m_dlgFlagging.m_nYSize = gm.nSize;
		if( !CRANGE( m_dlgFlagging.m_nYSize, 0, 100 ) ) m_dlgFlagging.m_nYSize = 0;
		m_dlgFlagging.m_nYColor = gm.nColor;
		if( !CRANGE( m_dlgFlagging.m_nYColor, 0, RGB(255,255,255) ) ) m_dlgFlagging.m_nYColor = 0;

		// Check for data that is basically 0 because it's so small...screws up dialogs
		TOOSMALL( m_nX );
		TOOSMALL( m_nColX );
		TOOSMALL( m_nY );
		TOOSMALL( m_nColY );
		TOOSMALL( m_nFactor1 );
		TOOSMALL( m_nXStroke );
		TOOSMALL( m_nYStroke );
		TOOSMALL( m_dlgOptions.m_nDisp );
		TOOSMALL( m_dlgOptions.m_dStrength );
		TOOSMALL( m_dlgOptions.m_nDisp2 );
		TOOSMALL( m_dlgOptions.m_dStrength2 );
		TOOSMALL( m_dlgOptions.m_dExcludeValX );
		TOOSMALL( m_dlgOptions.m_dExcludeValY );
		TOOSMALL( m_dlgFlagging.m_nY );
		TOOSMALL( m_dlgFlagging.m_dMin );
		TOOSMALL( m_dlgFlagging.m_dMax );
		TOOSMALL( m_dlgFlagging.m_nYPoint );
		TOOSMALL( m_dlgFlagging.m_nYSize );
		TOOSMALL( m_dlgFlagging.m_nYColor );

		IShowStats* pShow = GetShowStatsObject();
		if( pShow )
		{
			pShow->put_GroupFilter( m_dlgOptions.m_nGrp2 );
			pShow->put_SubjectFilter( m_dlgOptions.m_nSubj2 );
			pShow->put_ConditionFilter( m_dlgOptions.m_nCond2 );
			pShow->put_TrialFilter( m_dlgOptions.m_nTrial2 );
			pShow->put_StrokeFilter( m_dlgOptions.m_nStroke2 );
//			pShow->put_SubMovementFilter( m_dlgOptions.m_nSubMvmt2 );
			pShow->put_UseXDispersion( m_dlgOptions.m_fDispX );
			pShow->put_XDispersionIndex( m_dlgOptions.m_nDisp + 1 );
			pShow->put_XDispersionStrength( m_dlgOptions.m_dStrength );
			pShow->put_UseYDispersion( m_dlgOptions.m_fDispY );
			pShow->put_YDispersionIndex( m_dlgOptions.m_nDisp2 + 1 );
			pShow->put_YDispersionStrength( m_dlgOptions.m_dStrength2 );
			pShow->put_FlagIndex( m_dlgFlagging.m_nY + 1 );
			pShow->put_FlagMin( m_dlgFlagging.m_dMin );
			pShow->put_FlagMax( m_dlgFlagging.m_dMax );
			pShow->put_FlagType( m_dlgFlagging.m_nYPoint );
			pShow->put_FlagSize( m_dlgFlagging.m_nYSize );
			pShow->put_FlagColor( m_dlgFlagging.m_nYColor );
		}
	}

	// Determine size, location differences for later use in resizing
	// ...the constant numbers below represent the pixel distance of
	// ...the edges of the graph window from the edge of the dialog window
	RECT rect, rect2;
	m_pGraph = GetDlgItem( IDC_GRAPH );
	m_pGraph->GetWindowRect( &rect );
	this->GetWindowRect( &rect2 );
	m_nToolbar = rect.top - rect2.top;
	m_nRightBuffer = ( ( rect2.right - rect2.left ) - ( rect.right - rect.left ) ) - 4;
	m_nBottomBuffer = ( ( rect2.bottom - rect2.top ) - ( rect.bottom - rect.top ) ) - 4;
	this->ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	m_nToolbar -= m_nTop;
	m_nBottomBuffer -= m_nToolbar;

	pWnd=GetDlgItem( IDC_BN_GO );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLGoBn = rect.left;
	}
	pWnd=GetDlgItem( IDC_TXT_STROKE_X );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLTxtX = rect.left;
	}
	pWnd=GetDlgItem( IDC_TXT_STROKE_Y );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLTxtY = rect.left;
	}
	pWnd=GetDlgItem( IDC_EDIT_XSTROKE );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLX = rect.left;
	}
	pWnd=GetDlgItem( IDC_EDIT_YSTROKE );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLY = rect.left;
	}
	pWnd = GetDlgItem( IDC_CHK_STROKES );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLStrokes = rect.left;
	}
	pWnd = GetDlgItem( IDC_CHK_SCALE );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		this->ScreenToClient( &rect );
		m_nLScale = rect.left;
	}

	UpdateData( FALSE );
	OnSelchangeCboFactor1();

	pWnd = GetDlgItem( IDC_CHK_SCALE );
	if( pWnd ) pWnd->EnableWindow( !m_fTrial );

	if( m_nMode == MODE_STDDEVN )
	{
		m_cboChart.SetCurSel( CHART_AVGSDN );
		IShowStats* pShow = GetShowStatsObject();
		if( pShow ) pShow->put_StdDevN( TRUE );
	}
	else if( m_fTrial && ( ( m_nX == ( _colN - 1 ) ) || ( m_nY == ( _colN - 1 ) ) ) )
	{
		BCGPMessageBox( _T("N is not available with a chart containing trials.") );
		m_cboChart.SetCurSel( CHART_AVG );
		m_fTrial = FALSE;
		m_fStdDev = FALSE;
	}
	else if( m_fTrial && m_fAvg && m_fStdDev && ( m_nX != ( _colN - 1 ) ) && ( m_nY != ( _colN - 1 ) ) )
		m_cboChart.SetCurSel( CHART_TAVGSD );
	else if( m_fTrial && m_fAvg && ( m_nX != ( _colN - 1 ) ) && ( m_nY != ( _colN - 1 ) ) )
		m_cboChart.SetCurSel( CHART_TAVG );
	else if( m_fTrial && ( m_nX != ( _colN - 1 ) ) && ( m_nY != ( _colN - 1 ) ) )
		m_cboChart.SetCurSel( CHART_T );
	else if( m_fAvg && m_fStdDev )
		m_cboChart.SetCurSel( CHART_AVGSD );
	else if( m_fAvg )
		m_cboChart.SetCurSel( CHART_AVG );
	else
		m_cboChart.SetCurSel( CHART_NONE );

	if( m_fTrial )
	{
		if( m_fAvg ) DoGraphCombo();
		else DoGraphS();
		m_fInitG = TRUE;
	}
	else if( m_fAvg || m_fStdDev )
	{
		DoGraphA();
		m_fInitG = TRUE;
	}

	m_fInit = TRUE;
	EnableVisualManagerStyle( TRUE, TRUE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//************************************
// Method:    PreTranslateMessage
// FullName:  AGraphDlg::PreTranslateMessage
// Access:    virtual public 
// Returns:   BOOL
// Qualifier:
// Parameter: MSG * pMsg
//************************************
BOOL AGraphDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	// Shift+F10: show pop-up menu.
	if( ( ( ( pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN ) &&	// If we hit a key and
		( pMsg->wParam == VK_F10 ) && ( GetKeyState( VK_SHIFT ) & ~1 ) ) != 0 ) ||	// it's Shift+F10 OR
		( pMsg->message == WM_CONTEXTMENU ) )										// Natural keyboard key
	{
		CRect rect;
		GetClientRect( rect );
		ClientToScreen( rect );

		CPoint point;
		GetCursorPos( &point );
		point.Offset( 5, 5 );
		OnContextMenu( NULL, point );

		return TRUE;
	}

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

//************************************
// Method:    OnSize
// FullName:  AGraphDlg::OnSize
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: UINT nType
// Parameter: int cx
// Parameter: int cy
//************************************
void AGraphDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	CWnd* m_pGraph = GetDlgItem( IDC_GRAPH );
	if( m_pGraph )
	{
		m_pGraph->MoveWindow( m_nLeft, m_nTop, cx - m_nRightBuffer,
							  cy - m_nBottomBuffer, FALSE );
		if( m_hPE ) ::MoveWindow( m_hPE, 0, 0, cx - m_nRightBuffer,
								  cy - m_nBottomBuffer, FALSE);
	}
	CWnd* pBn = GetDlgItem( IDC_BN_GO );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLGoBn, cy - m_nToolbar, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_TXT_STROKE_X );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLTxtX, cy - m_nToolbar + 4, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_TXT_STROKE_Y );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLTxtY, cy - m_nToolbar + 4, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_EDIT_XSTROKE );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLX, cy - m_nToolbar, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_EDIT_YSTROKE );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLY, cy - m_nToolbar, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_CHK_STROKES );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLStrokes, cy - m_nToolbar + 2, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_CHK_SCALE );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLScale, cy - m_nToolbar + 2, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
	pBn = GetDlgItem( IDC_TXT_GO );
	if( pBn )
	{
		RECT rect;
		pBn->GetWindowRect( &rect );
		pBn->MoveWindow( m_nLeft, cy - m_nToolbar + 4, ( rect.right - rect.left ),
						 ( rect.bottom - rect.top ), TRUE );
	}
}

//************************************
// Method:    OnSelchangeCboFactor1
// FullName:  AGraphDlg::OnSelchangeCboFactor1
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnSelchangeCboFactor1() 
{
	int nSel = m_cboF1.GetCurSel();
	if( nSel < 0 ) return;

	m_nFactor1Old = m_nFactor1;
	UpdateData( TRUE );
	m_nFactor1 = nSel;

	CWnd* pWnd = GetDlgItem( IDC_CHK_STROKES );
//	if( pWnd ) pWnd->EnableWindow( nSel == 5 /* stroke */ );
	pWnd = GetDlgItem( IDC_EDIT_XSTROKE );
	if( pWnd ) pWnd->EnableWindow( m_fUseStrokes );
	pWnd = GetDlgItem( IDC_EDIT_YSTROKE );
	if( pWnd ) pWnd->EnableWindow( m_fUseStrokes );
	UpdateData( FALSE );
	OnChkStrokes();

	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	if( m_nFactor1 == 0 ) // None
		pShow->put_UseGrouping( FALSE );
	else
		pShow->put_UseGrouping( m_dlgGrouping.m_fGrouping );

	if( !m_fInit ) return;

	m_dlgOptions.m_fHasChanged = TRUE;

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnSelchangeCboX
// FullName:  AGraphDlg::OnSelchangeCboX
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnSelchangeCboX() 
{
	int nSel = m_cboX.GetCurSel();
	if( nSel < 0 ) return;

	if( m_fTrial && ( m_cboX.GetCurSel() == ( _colN - 1 ) ) )
	{
		BCGPMessageBox( _T("N is not available with a chart containing trials.") );
		UpdateData( FALSE );
		m_cboX.SetFocus();
		return;
	}

	m_nXOld = m_nX;

	if( !m_fInit ) return;

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnOK
// FullName:  AGraphDlg::OnOK
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnOK()
{
//	OnCancel();
	CBCGPDialog::OnOK();
}

//************************************
// Method:    OnCancel
// FullName:  AGraphDlg::OnCancel
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnCancel()
{
//	DestroyWindow();
//	delete this;
	CBCGPDialog::OnOK();
}

//************************************
// Method:    PostNcDestroy
// FullName:  AGraphDlg::PostNcDestroy
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::PostNcDestroy()
{
}

//************************************
// Method:    OnChkStrokes
// FullName:  AGraphDlg::OnChkStrokes
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkStrokes()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_XSTROKE );
	if( pWnd ) pWnd->EnableWindow( m_fUseStrokes );
	pWnd = GetDlgItem( IDC_EDIT_YSTROKE );
	if( pWnd ) pWnd->EnableWindow( m_fUseStrokes );

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnClickBnRefresh
// FullName:  AGraphDlg::OnClickBnRefresh
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnClickBnRefresh() 
{
	UpdateData( TRUE );

	// verify that INC, XME, XM1 are not open
	int nPos = m_szINC.ReverseFind( '.' );
	CString szMask = m_szINC.Left( nPos );
	CString szTmp = szMask;
	szTmp += _T(".TMP");
	CString szXM1 = szMask;
	szXM1 += _T(".XM1");
	CString szXME = szMask;
	szXME += _T(".XME");
	CFileFind ff;
	CStdioFile fTest;
	if( ff.FindFile( szTmp ) )
	{
		if( !fTest.Open( szTmp, CFile::modeWrite ) )
		{
			BCGPMessageBox( _T("TMP file is currently open.\nPlease close before updating the graph.") );
			return;
		}
		fTest.Close();
	}
	if( ff.FindFile( szXME ) )
	{
		if( !fTest.Open( szXME, CFile::modeWrite ) )
		{
			BCGPMessageBox( _T("XME file is currently open.\nPlease close before updating the graph.") );
			return;
		}
		fTest.Close();
	}
	if( ff.FindFile( szXM1 ) )
	{
		if( !fTest.Open( szXM1, CFile::modeWrite ) )
		{
			BCGPMessageBox( _T("XM1 file is currently open.\nPlease close before updating the graph.") );
			return;
		}
		fTest.Close();
	}

	if( m_fTrial )
	{
		if( m_fAvg ) DoGraphCombo();
		else DoGraphS();
	}
	else if( m_fAvg || m_fStdDev )
	{
		DoGraphA();
	}
	else
	{
		IShowStats* pShow = GetShowStatsObject();
		if( pShow )
		{
			m_pGraph = GetDlgItem( IDC_GRAPH );
			pShow->ShowClear( (VARIANT*)m_pGraph, (VARIANT*)&m_hPE );
			m_hPE = NULL;
			m_pGraph->ShowWindow( FALSE );
			m_pGraph->ShowWindow( TRUE );
		}
	}

	m_nXOld = m_nX;
	m_nFactor1Old = m_nFactor1;

	SetRefresh( FALSE );
}

//************************************
// Method:    OnBnOptions
// FullName:  AGraphDlg::OnBnOptions
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnOptions()
{
	OutputAMessage( _T("Viewing chart options."), TRUE );
	UpdateData( TRUE );

	GenerateTmpFile( !m_fAssociationsDone );

	m_cboX.GetLBText( m_nX, m_dlgOptions.m_szY );
	m_cboY.GetLBText( m_nY, m_dlgOptions.m_szY );

	if( m_dlgOptions.DoModal() == IDOK )
	{
		OutputAMessage( _T("Applying chart options changes."), TRUE );

		IShowStats* pShow = GetShowStatsObject();
		if( !pShow ) return;

		pShow->put_GroupFilter( m_dlgOptions.m_nGrp2 );
		pShow->put_SubjectFilter( m_dlgOptions.m_nSubj2 );
		pShow->put_ConditionFilter( m_dlgOptions.m_nCond2 );
		pShow->put_TrialFilter( m_dlgOptions.m_nTrial2 );
		pShow->put_StrokeFilter( m_dlgOptions.m_nStroke2 );
//		pShow->put_SubMovementFilter( m_dlgOptions.m_nSubMvmt2 );
		pShow->put_UseXDispersion( m_dlgOptions.m_fDispX );
		pShow->put_XDispersionIndex( m_dlgOptions.m_nDisp + 1 );
		pShow->put_XDispersionStrength( m_dlgOptions.m_dStrength );
		pShow->put_UseYDispersion( m_dlgOptions.m_fDispY );
		pShow->put_YDispersionIndex( m_dlgOptions.m_nDisp2 + 1 );
		pShow->put_YDispersionStrength( m_dlgOptions.m_dStrength2 );

		SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );

		m_fEWChg = TRUE;
		GenerateTmpFile( TRUE );
	}
}

//************************************
// Method:    OnBnGrouping
// FullName:  AGraphDlg::OnBnGrouping
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnGrouping()
{
	OutputAMessage( _T("Viewing data grouping options."), TRUE );
	UpdateData( TRUE );

	GenerateTmpFile( !m_fAssociationsDone );

	if( m_dlgGrouping.DoModal() == IDCANCEL ) return;

	OutputAMessage( _T("Applying data grouping options."), TRUE );

	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	pShow->put_UseGrouping( m_dlgGrouping.m_fGrouping );
	if( m_dlgGrouping.m_fGrouping )
	{
		pShow->put_Grouping( m_dlgGrouping.m_nGrouping + 1 );

		int nPoints = 0;
		CStringList* pList = NULL;
		if( m_dlgGrouping.m_nGrouping == 0 )		// Group
		{
			nPoints = (int)m_dlgOptions.m_lstGrp.GetCount() - 1;
			pList = &m_dlgOptions.m_lstGrp;
		}
		else if( m_dlgGrouping.m_nGrouping == 1 )	// Subject
		{
			nPoints = (int)m_dlgOptions.m_lstSubj.GetCount() - 1;
			pList = &m_dlgOptions.m_lstSubj;
		}
		else if( m_dlgGrouping.m_nGrouping == 2 )	// Condition
		{
			nPoints = (int)m_dlgOptions.m_lstCond.GetCount() - 1;
			pList = &m_dlgOptions.m_lstCond;
		}
		else if( m_dlgGrouping.m_nGrouping == 3 )	// Trial
		{
			nPoints = (int)m_dlgOptions.m_lstTrial.GetCount() - 1;
			pList = &m_dlgOptions.m_lstTrial;
		}
		else if( m_dlgGrouping.m_nGrouping == 4 )	// Stroke
		{
			nPoints = (int)m_dlgOptions.m_lstStroke.GetCount() - 1;
			pList = &m_dlgOptions.m_lstStroke;
		}

		pShow->put_GroupingCount( (short)nPoints );

		CString szItem;
		int nItem = 0, i = 0;
		POSITION pos = pList ? pList->GetHeadPosition() : NULL;
		if( pos ) szItem = pList->GetNext( pos );	// Eliminate "all"
		while( pos )
		{
			szItem = pList->GetNext( pos );
			nItem = atoi( szItem );

			pShow->put_GroupingItemIndex( i, nItem );
			pShow->put_GroupingItemType( i, (short)m_dlgGrouping.m_pPoints[ i ] );
			pShow->put_GroupingItemSize( i, (short)m_dlgGrouping.m_pSizes[ i ] );
			pShow->put_GroupingItemColor( i, m_dlgGrouping.m_pColors[ i ] );

			i++;
		}
	}

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnBnFlagging
// FullName:  AGraphDlg::OnBnFlagging
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnFlagging()
{
	OutputAMessage( _T("Viewing point flagging."), TRUE );
	UpdateData( TRUE );

	if( m_dlgFlagging.DoModal() == IDCANCEL ) return;

	OutputAMessage( _T("Applying point flagging changes."), TRUE );

	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	pShow->put_UseFlagging( m_dlgFlagging.m_fFlag );
	if( m_dlgFlagging.m_fFlag )
	{
		pShow->put_FlagIndex( m_dlgFlagging.m_nY + 1 );
		pShow->put_FlagMin( m_dlgFlagging.m_dMin );
		pShow->put_FlagMax( m_dlgFlagging.m_dMax );
		pShow->put_FlagType( m_dlgFlagging.m_nYPoint );
		pShow->put_FlagSize( m_dlgFlagging.m_nYSize );
		pShow->put_FlagColor( m_dlgFlagging.m_nYColor );
	}

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnBnOffset
// FullName:  AGraphDlg::OnBnOffset
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnOffset()
{
	OutputAMessage( _T("Viewing offset/scaling."), TRUE );
	UpdateData( TRUE );

	if( m_dlgOffset.DoModal() == IDCANCEL ) return;

	m_fEWChg = TRUE;
	GenerateTmpFile();

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnChkTrials
// FullName:  AGraphDlg::OnChkTrials
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkTrials()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_STDDEV );
	if( !pWnd ) return;
	pWnd->EnableWindow( ( m_fTrial && m_fAvg ) || !m_fTrial );

	pWnd = GetDlgItem( IDC_CHK_SCALE );
	if( !pWnd ) return;
	pWnd->EnableWindow( !m_fTrial );

	if( !m_fInit ) return;

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnChkAvg
// FullName:  AGraphDlg::OnChkAvg
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkAvg()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_STDDEV );
	if( !pWnd ) return;
	pWnd->EnableWindow( ( m_fTrial && m_fAvg ) || !m_fTrial );

	if( !m_fInit ) return;

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnBnSortG
// FullName:  AGraphDlg::OnBnSortG
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnSortG() 
{
	IExperimentMember* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}

	hr = pEM->FindForExperiment( m_szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->Release();
		return;
	}

	CStringList lst;
	BSTR bstrID = NULL;
	CString szItem;
	while( SUCCEEDED( hr ) )
	{
		pEM->get_GroupID( &bstrID );
		szItem = bstrID;
		if( !lst.Find( szItem ) ) lst.AddTail( szItem );

		hr = pEM->GetNext();
	}
	pEM->Release();

	if( lst.GetCount() == 0 )
	{
		BCGPMessageBox( _T("There are no groups/visits to sort.") );
		return;
	}

	OutputAMessage( _T("Viewing/changing analysis graphing sort order of groups/visits."), TRUE );

	SortDlg d( SORT_GROUPS, m_szExp, &lst, this );
	if( d.DoModal() == IDOK )
	{
		GenerateTmpFile( TRUE );
		m_fInit = FALSE;
		OnClickBnRefresh();
		m_fInit = TRUE;
	}
}

//************************************
// Method:    OnBnSortC
// FullName:  AGraphDlg::OnBnSortC
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnSortC() 
{
	IExperimentCondition* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
		IID_IExperimentCondition, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCOND_ERR );
		return;
	}

	hr = pEM->FindForExperiment( m_szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->Release();
		return;
	}

	CStringList lst;
	BSTR bstrID = NULL;
	CString szItem;
	while( SUCCEEDED( hr ) )
	{
		pEM->get_ConditionID( &bstrID );
		szItem = bstrID;
		if( !lst.Find( szItem ) ) lst.AddTail( szItem );

		hr = pEM->GetNext();
	}
	pEM->Release();

	if( lst.GetCount() == 0 )
	{
		BCGPMessageBox( _T("There are no conditions/tasks to sort.") );
		return;
	}

	OutputAMessage( _T("Viewing/changing analysis graphing sort order of conditions/tasks."), TRUE );

	SortDlg d( SORT_CONDITIONS, m_szExp, &lst, this );
	if( d.DoModal() == IDOK )
	{
		GenerateTmpFile( TRUE );
		m_fInit = FALSE;
		OnClickBnRefresh();
		m_fInit = TRUE;
	}
}

//************************************
// Method:    OnBnSortS
// FullName:  AGraphDlg::OnBnSortS
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnSortS() 
{
	IExperimentMember* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
		IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}

	hr = pEM->FindForExperiment( m_szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->Release();
		return;
	}

	CStringList lst;
	BSTR bstrID = NULL;
	CString szItem;
	while( SUCCEEDED( hr ) )
	{
		pEM->get_SubjectID( &bstrID );
		szItem = bstrID;
		if( !lst.Find( szItem ) ) lst.AddTail( szItem );

		hr = pEM->GetNext();
	}
	pEM->Release();

	if( lst.GetCount() == 0 )
	{
		BCGPMessageBox( _T("There are no subjects/patients to sort.") );
		return;
	}

	OutputAMessage( _T("Viewing/changing analysis graphing sort order of subjects/patients."), TRUE );

	SortDlg d( SORT_SUBJECTS, m_szExp, &lst, this );
	if( d.DoModal() == IDOK )
	{
		GenerateTmpFile( TRUE );
		m_fInit = FALSE;
		OnClickBnRefresh();
		m_fInit = TRUE;
	}
}

//************************************
// Method:    OnSelchangeCboY
// FullName:  AGraphDlg::OnSelchangeCboY
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnSelchangeCboY() 
{
	if( m_fTrial && ( m_cboY.GetCurSel() == ( _colN - 1 ) ) )
	{
		BCGPMessageBox( _T("N is not available with a graph containing trials.") );
		UpdateData( FALSE );
		m_cboY.SetFocus();
		return;
	}

	if( !m_fInit ) return;

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnChkEqual2
// FullName:  AGraphDlg::OnChkEqual2
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkEqual2() 
{
	if( !m_fInit ) return;
	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnChkEqual
// FullName:  AGraphDlg::OnChkEqual
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkEqual() 
{
	if( !m_fInit ) return;
	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );

	UpdateData( TRUE );
	m_cboColX.ResetContent();
	m_cboColY.ResetContent();
	m_fEWChg = TRUE;

	if( m_fEqual )
	{
		m_cboColX.AddString( _T("Avgs of Avgs") );
		m_cboColX.AddString( _T("Avgs of SDs") );
		m_cboColX.AddString( _T("SDs of Avgs") );
		m_cboColX.AddString( _T("SDs of SDs") );
		m_cboColY.AddString( _T("Avgs of Avgs") );
		m_cboColY.AddString( _T("Avgs of SDs") );
		m_cboColY.AddString( _T("SDs of Avgs") );
		m_cboColY.AddString( _T("SDs of SDs") );
	}
	else
	{
		m_cboColX.AddString( _T("Avgs") );
		m_cboColX.AddString( _T("SDs") );
		m_cboColY.AddString( _T("Avgs") );
		m_cboColY.AddString( _T("SDs") );
	}
	m_cboColX.SetCurSel( 0 );
	m_cboColY.SetCurSel( 0 );

	CWnd* pWnd = GetDlgItem( IDC_CHK_EQUAL2 );
	pWnd->EnableWindow( m_fEqual );
	if( !m_fEqual ) averagebytrial = FALSE;
}

//************************************
// Method:    OnChkStddev
// FullName:  AGraphDlg::OnChkStddev
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkStddev() 
{
	if( !m_fInit ) return;
	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnChangeEditXstroke
// FullName:  AGraphDlg::OnChangeEditXstroke
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChangeEditXstroke() 
{
	if( !m_fInit ) return;
	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnChangeEditYstroke
// FullName:  AGraphDlg::OnChangeEditYstroke
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChangeEditYstroke() 
{
	if( !m_fInit ) return;
	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnBnGo
// FullName:  AGraphDlg::OnBnGo
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnBnGo() 
{
	if( !m_fTMPGenerated && ( m_bnGo.m_nMenuResult != IDM_AGRAPH_VIEW_STAT ) )
	{
		if( BCGPMessageBox( _T("Temporary data file must be generated first. This could take awhile. Continue?"), MB_YESNO ) == IDNO )
			return;
		if( m_cboChart.GetCurSel() == -1 )
		{
			BCGPMessageBox( _T("Please select a chart first.") );
			m_cboChart.SetFocus();
			return;
		}
		GenerateTmpFile( TRUE );
	}

	switch( m_bnGo.m_nMenuResult )
	{
		case IDM_AGRAPH_PRINT:
		{
			IShowStats* pShow = GetShowStatsObject();
			if( !pShow ) break;

			HRESULT hr = pShow->PrintGraph( (VARIANT*)&m_hPE );
			if( FAILED( hr ) ) BCGPMessageBox( _T("Error in printing.") );
			break;
		}
		case IDM_AGRAPH_POINTS:
			OnBnOptions();
			break;
		case IDM_AGRAPH_GROUPING:
			OnBnGrouping();
			break;
		case IDM_AGRAPH_FLAGGING:
			OnBnFlagging();
			break;
		case IDM_AGRAPH_OFFSET:
			OnBnOffset();
			break;
		case IDM_AGRAPH_SORT_G:
			OnBnSortG();
			break;
		case IDM_AGRAPH_SORT_C:
			OnBnSortC();
			break;
		case IDM_AGRAPH_SORT_S:
			OnBnSortS();
			break;
		case IDM_AGRAPH_IDENT:
		{
			GenerateTmpFile( !m_fAssociationsDone );
			GraphIdentityDlg d( &m_lstGroups, &m_lstSubjs, &m_lstConds, &m_lstQuests, this );
			d.DoModal();
			break;
		}
		case IDM_AGRAPH_VIEW_ALL:
		{
			CFileFind ff;
			CString szCmd;

			if( ff.FindFile( m_szTMP ) )
			{
				CWaitCursor crs;
				DataTabGridDlg* d = new DataTabGridDlg( m_szTMP, this );
			}
			else BCGPMessageBox( _T("No data file available.") );

			break;
		}
		case IDM_AGRAPH_EXPORT_ALL:
		{
			GenerateTmpFile( FALSE, TRUE );
			CString szTMP = m_szTMP.Left( m_szTMP.GetLength() - 3 );	// remove TMP extension
			szTMP += _T("ANA");
			CFileFind ff;
			if( ff.FindFile( szTMP ) )
			{
				CWaitCursor crs;
				CString szCmd;
				szCmd.LoadString( IDS_EDITOR );
				szCmd += szTMP;
				::WinExec( szCmd, SW_SHOW );
			}
			else BCGPMessageBox( _T("There was an error exporting the raw data.") );
			break;
		}
		case IDM_AGRAPH_VIEW_STAT:
		{
			if( !m_fEqual )
			{
				BCGPMessageBox( _T("Statistics are only available when averaging first per subject.") );
				return;
			}

			// Which columns
			int nX = 0, nY = 0;
			if( m_nColX <= 1 ) nX = ( m_nColX * _stdev_offset ) + m_nX;
			else nX = ( m_nColX * _stdev_offset ) + 1 + m_nX;
			if( m_nColY <= 1 ) nY = ( m_nColY * _stdev_offset ) + m_nY;
			else nY = ( m_nColY * _stdev_offset ) + 1 + m_nY;

			// Output file
			int nPos = m_szTMP.ReverseFind( '.' );
			CString szFileOut = m_szTMP.Left( nPos );
			szFileOut += _T(".XM1");
			CFileFind ff;
			if( !ff.FindFile( szFileOut ) )
			{
				BCGPMessageBox( _T("The chart must first be refreshed.") );
				return;
			}

			// Process object
			IProcess* pProcess = GetProcessObject();
			if( !pProcess ) return;

			// Statistics
			// Average by subject
			if (!averagebytrial)
			{
				CString szFileEVA = szFileOut.Left( szFileOut.GetLength() - 3 );
				CString szFileGRP = szFileEVA;
				CString szFileLST = szFileEVA;
				szFileEVA += _T("EVA");
				szFileGRP += _T("GRP");
				szFileLST += _T("LST");
				if( DoStatisticsEVA( szFileOut, szFileEVA, nX + 1, nY + 1, m_nFactor1 ) )
				{
					// TODO: WHere do we get these?
					BOOL fBetween = TRUE;
					BOOL fTestAdded = TRUE;
					BOOL fLogTransform = FALSE;

					HRESULT hr = pProcess->Group( szFileEVA.AllocSysString(), szFileGRP.AllocSysString(), fBetween, fTestAdded );
					if( SUCCEEDED( hr ) )
					{
						hr = pProcess->Wilcx( szFileGRP.AllocSysString(), szFileLST.AllocSysString(), fLogTransform );
						if( FAILED( hr ) )
						{
							OutputAMessage( _T("Error running statistics wilcx.") );
							return;
						}
					}
					else
					{
						OutputAMessage( _T("Error running statistics grouping.") );
						return;
					}

					CString szCmd;
					CString szFile = m_szTMP.Left( m_szTMP.GetLength() - 3 );
					szFile += _T("LST");

					if( ff.FindFile( szFile ) )
					{
						szCmd.LoadString( IDS_EDITOR );
						szCmd += szFile;
						WinExec( szCmd, SW_SHOW );
					}
					else BCGPMessageBox( _T("No statistics file available.") );
				}

				break;
			}
			// Or average by trial
			else
			{
				// Statistics
				CString szFileGR1 = szFileOut.Left( szFileOut.GetLength() - 3 );
				CString szFileGRP = szFileGR1;
				CString szFileLST = szFileGR1;
				szFileGR1 += _T("GR1");
				szFileGRP += _T("GRP");
				szFileLST += _T("LST");
				if( DoStatisticsGR1( m_szTMP, szFileGR1, nX + 1, nY + 1, m_nFactor1 ) )
				{
					// TODO: WHere do we get these?
					BOOL fBetween = TRUE;
					BOOL fTestAdded = TRUE;
					BOOL fLogTransform = FALSE;
					// Perform Wilcx directly after DostatisticsGR1
					//HRESULT hr = pProcess->Group( szFileGR1.AllocSysString(), szFileGRP.AllocSysString(), fBetween, fTestAdded );
					HRESULT hr = pProcess->Wilcx( szFileGR1.AllocSysString(), szFileLST.AllocSysString(), fLogTransform );
					if( FAILED( hr ) )
					{
						OutputAMessage( _T("Error running statistics wilcx.") );
						return;
					}
					CString szCmd;
					CString szFile = m_szTMP.Left( m_szTMP.GetLength() - 3 );
					szFile += _T("LST");

					if( ff.FindFile( szFile ) )
					{
						szCmd.LoadString( IDS_EDITOR );
						szCmd += szFile;
						WinExec( szCmd, SW_SHOW );
					}
					else BCGPMessageBox( _T("No statistics file available.") );
				}
				break;
			}

		}
		case IDM_AGRAPH_VIEW_XMEAN:
		{
			CFileFind ff;
			int nPos = -1;
			CString szFile1, szFile2, szCmd;
			BOOL fFound = FALSE;

			nPos = m_szTMP.ReverseFind( '.' );
			if( nPos < 0 ) break;

			szFile1 = m_szTMP.Left( nPos );
			szFile2 = szFile1;
			szFile1 += _T(".XM1");
			szFile2 += _T(".XME");

			if( ff.FindFile( szFile1 ) )
			{
				CWaitCursor crs;
				DataTabDlg* d = new DataTabDlg( szFile1, this );
				fFound = TRUE;
			}

			if( ff.FindFile( szFile2 ) )
			{
				CWaitCursor crs;
				DataTabDlg* d = new DataTabDlg( szFile2, this );
				fFound = TRUE;
			}

			if( !fFound ) BCGPMessageBox( _T("No data file available.") );

			break;
		}
		case IDM_AGRAPH_VIEW_TABULAR:
		{
			CWaitCursor crs;

			CStdioFile fIn;
			CString szFile;

			if( m_fAvg || m_fStdDev )
			{

				szFile = m_szTMP.Left( m_szTMP.GetLength() - 3 );
				szFile += _T("XME");
			}
			else szFile = m_szTMP;

			if( fIn.Open( szFile, CFile::modeRead ) )
			{
				CStdioFile fOut;
				CString szItem, szX, szY;

				CString szF;
				::GetDataPathRoot( szF );
				szF += _T("\\");
				szF += m_szExp;
				szF += _T("\\data.txt");

				if( !fOut.Open( szF, CFile::modeWrite | CFile::modeCreate ) ) return;

				m_cboX.GetLBText( m_cboX.GetCurSel(), szX );
				m_cboY.GetLBText( m_cboY.GetCurSel(), szY );

				szItem.Format( _T("Group Subject Condition Trial Stroke #Strokes %s %s\n"), szX, szY );
				fOut.WriteString( szItem );

				double dTemp[ 100 ];
				long lLines = 0;
				BOOL fInclude = TRUE;
				// progress
				// 1 step per line
				CString szData;
				int nSteps = 0;
				while( fIn.ReadString( szData ) ) nSteps++;
				nSteps -= 1;
				if( nSteps <= 0 ) return;
				int nStep = MAX( 1, nSteps / 100 ), nCur = 0;
				double dVal = 0.;
				fIn.SeekToBegin();
				Progress::EnableProgress( nSteps );
				// Skip 1st line
				getlinefloat( fIn, dTemp, 100 );
				while( getlinefloat( fIn, dTemp, 100 ) != 0 )
				{
					if( ( m_dlgOptions.m_nGrp > 0 ) || ( m_dlgOptions.m_nSubj > 0 ) ||
						( m_dlgOptions.m_nCond > 0 ) || ( m_dlgOptions.m_nTrial > 0 ) ||
						( m_dlgOptions.m_nStroke > 0 ) || m_dlgOptions.m_fExcludeX ||
						m_dlgOptions.m_fExcludeY )
					{
						if( ( m_dlgOptions.m_nGrp2 > 0 ) && ( dTemp[ 0 ] == m_dlgOptions.m_nGrp2 ) ) fInclude = TRUE;
						else if( ( m_dlgOptions.m_nSubj2 > 0 ) && ( dTemp[ 1 ] == m_dlgOptions.m_nSubj2 ) ) fInclude = TRUE;
						else if( ( m_dlgOptions.m_nCond2 > 0 ) && ( dTemp[ 2 ] == m_dlgOptions.m_nCond2 ) ) fInclude = TRUE;
						else if( ( m_dlgOptions.m_nTrial2 > 0 ) && ( dTemp[ 3 ] == m_dlgOptions.m_nTrial2 ) ) fInclude = TRUE;
						else if( ( m_dlgOptions.m_nStroke2 > 0 ) && ( dTemp[ 4 ] == m_dlgOptions.m_nStroke2 ) ) fInclude = TRUE;
						else if( m_dlgOptions.m_fExcludeX && ( dTemp[ m_cboX.GetCurSel() ] != m_dlgOptions.m_dExcludeValX ) ) fInclude = TRUE;
						else if( m_dlgOptions.m_fExcludeY && ( dTemp[ m_cboY.GetCurSel() ] != m_dlgOptions.m_dExcludeValY ) ) fInclude = TRUE;
						else fInclude = FALSE;
					}

					if( fInclude )
					{
						if( szFile == m_szTMP )
						{
							szItem.Format( _T("%5.0f %7.0f %9.0f %5.0f %6.0f %8.0f %7.3f %7.3f\n"),
										   dTemp[ 0 ], dTemp[ 1 ], dTemp[ 2 ],
										   dTemp[ 3 ], dTemp[ 4 ], dTemp[ 5 ],
										   dTemp[ m_cboX.GetCurSel() ],
										   dTemp[ m_cboY.GetCurSel() ] );
						}
						else
						{
							szItem.Format( _T("%5.3f %7.3f %9.3f %5.3f %6.3f %8.3f %7.3f %7.3f\n"),
										   dTemp[ 0 ], dTemp[ 1 ], dTemp[ 2 ],
										   dTemp[ 3 ], dTemp[ 4 ], dTemp[ 5 ],
										   dTemp[ m_cboX.GetCurSel() ],
										   dTemp[ m_cboY.GetCurSel() ] );
						}
						fOut.WriteString( szItem );
						lLines++;
					}

					nCur++;
					if( ( nCur % nStep ) == 0 ) 
					{
						dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
						szData.Format( _T("Progress %.1f %% - OUTPUT: Line %u of file complete"), dVal, nCur );
						Progress::SetProgress( nCur, szData );
					}
				}

				fOut.Close();
				Progress::DisableProgress();

				CWaitCursor crs;
				if( lLines > 1000 ) DataTabGridDlg* d = new DataTabGridDlg( szF, this );
				else DataTabDlg* d = new DataTabDlg( szF, this );
			}
			else BCGPMessageBox( _T("No data file available.") );

			break;
		}
		case IDM_AGRAPH_VIEW_EXPORT:
		{
			CWaitCursor crs;

			CStdioFile fIn;
			CString szFile;

			if( m_fAvg || m_fStdDev )
			{

				szFile = m_szTMP.Left( m_szTMP.GetLength() - 3 );
				szFile += _T("XME");
			}
			else szFile = m_szTMP;

			if( fIn.Open( szFile, CFile::modeRead ) )
			{
				CStdioFile fOut;
				CString szItem, szX, szY;
				double dTemp[ 100 ];
				BOOL fInclude = TRUE;
				int nX = 0, nY = 0, nPts = 0, nIdx = 0;

				CString szF;
				::GetDataPathRoot( szF );
				szF += _T("\\");
				szF += m_szExp;
				szF += _T("\\data.txt");

				if( !fOut.Open( szF, CFile::modeWrite | CFile::modeCreate ) ) return;

				m_cboX.GetLBText( m_cboX.GetCurSel(), szX );
				m_cboY.GetLBText( m_cboY.GetCurSel(), szY );

				// XME Files (averages/stddev)
				if( szFile != m_szTMP )
				{
					if( m_nColX <= 1 ) nX = ( m_nColX * _stdev_offset ) + m_nX;
					else nX = ( m_nColX * _stdev_offset ) + 1 + m_nX;
					if( m_nColY <= 1 ) nY = ( m_nColY * _stdev_offset ) + m_nY;
					else nY = ( m_nColY * _stdev_offset ) + 1 + m_nY;

					// 1st GET NUMBER OF POINTS
					// Skip 1st line
					getlinefloat( fIn, dTemp, 100 );
					// Loop through
					while( getlinefloat( fIn, dTemp, 100 ) != 0 )
					{
						if( ( m_dlgOptions.m_nGrp > 0 ) || ( m_dlgOptions.m_nSubj > 0 ) ||
							( m_dlgOptions.m_nCond > 0 ) || ( m_dlgOptions.m_nTrial > 0 ) ||
							( m_dlgOptions.m_nStroke > 0 ) || m_dlgOptions.m_fExcludeX ||
							m_dlgOptions.m_fExcludeY )
						{
							if( ( m_dlgOptions.m_nGrp2 > 0 ) && ( dTemp[ 0 ] == m_dlgOptions.m_nGrp2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nSubj2 > 0 ) && ( dTemp[ 1 ] == m_dlgOptions.m_nSubj2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nCond2 > 0 ) && ( dTemp[ 2 ] == m_dlgOptions.m_nCond2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nTrial2 > 0 ) && ( dTemp[ 3 ] == m_dlgOptions.m_nTrial2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nStroke2 > 0 ) && ( dTemp[ 4 ] == m_dlgOptions.m_nStroke2 ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeX && ( dTemp[ m_cboX.GetCurSel() ] != m_dlgOptions.m_dExcludeValX ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeY && ( dTemp[ m_cboY.GetCurSel() ] != m_dlgOptions.m_dExcludeValY ) ) fInclude = TRUE;
							else fInclude = FALSE;
						}

						if( fInclude ) nPts++;
					}
					if( nPts == 0 ) break;

					// NOW GET THE X,Y POINTS
					double* dX = new double[ nPts ];
					double* dY = new double[ nPts ];
					// Skip 1st line
					fIn.SeekToBegin();
					getlinefloat( fIn, dTemp, 100 );
					// Loop through
					while( getlinefloat( fIn, dTemp, 100 ) != 0 )
					{
						if( ( m_dlgOptions.m_nGrp > 0 ) || ( m_dlgOptions.m_nSubj > 0 ) ||
							( m_dlgOptions.m_nCond > 0 ) || ( m_dlgOptions.m_nTrial > 0 ) ||
							( m_dlgOptions.m_nStroke > 0 ) || m_dlgOptions.m_fExcludeX ||
							m_dlgOptions.m_fExcludeY )
						{
							if( ( m_dlgOptions.m_nGrp2 > 0 ) && ( dTemp[ 0 ] == m_dlgOptions.m_nGrp2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nSubj2 > 0 ) && ( dTemp[ 1 ] == m_dlgOptions.m_nSubj2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nCond2 > 0 ) && ( dTemp[ 2 ] == m_dlgOptions.m_nCond2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nTrial2 > 0 ) && ( dTemp[ 3 ] == m_dlgOptions.m_nTrial2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nStroke2 > 0 ) && ( dTemp[ 4 ] == m_dlgOptions.m_nStroke2 ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeX && ( dTemp[ m_cboX.GetCurSel() ] != m_dlgOptions.m_dExcludeValX ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeY && ( dTemp[ m_cboY.GetCurSel() ] != m_dlgOptions.m_dExcludeValY ) ) fInclude = TRUE;
							else fInclude = FALSE;
						}

						if( fInclude )
						{
							dX[ nIdx ] = dTemp[ nX ];
							dY[ nIdx ] = dTemp[ nY ];
							nIdx++;
						}
					}

					// NOW WE MUST DETERMINE THE MAX # OF POINTS PER X
					// How Many Diff. X's
					CStringList lstX, lstXTemp;
					CString szItem, szCur, szTemp;
					int i = 0;
					for( i = 0; i < nPts; i++ )
					{
						szItem.Format( _T("%7.3f"), dX[ i ] );
						if( !lstX.Find( szItem ) )
						{
							lstX.AddTail( szItem );
							lstXTemp.AddTail( szItem );
						}
					}
					int nNumX = (int)lstX.GetCount();
					// Now go through each X, loop through the vals, and count per (delete from list after done per)
					int nMax = 0, nCur = 0;
					while( lstXTemp.GetCount() > 0 )
					{
						szCur = lstXTemp.GetHead();
						for( i = 0; i < nPts; i++ )
						{
							szItem.Format( _T("%7.3f"), dX[ i ] );
							if( szItem == szCur ) nCur++;
						}

						if( nCur > nMax ) nMax = nCur;
						nCur = 0;
						lstXTemp.RemoveHead();
					}

					// Our header to out file is determined by # of max Y's (per X)
					CString szXAxisMod, szYAxisMod;
					int nXColCnt = m_cboColX.GetCount();
					int nYColCnt = m_cboColY.GetCount();
					if( nXColCnt > 2 )
					{
						if( m_nColX == 1 ) szXAxisMod = _T("Avg");
						else if( m_nColX == 2 ) szXAxisMod = _T("SDAvg");
						else if( m_nColX == 3 ) szXAxisMod = _T("SDSD");
					}
					else if( m_nColX == 1 ) szXAxisMod = _T("SD");
					if( nYColCnt > 2 )
					{
						if( m_nColY == 1 ) szYAxisMod = _T("AvgSD");
						else if( m_nColY == 2 ) szYAxisMod = _T("SDAvg");
						else if( m_nColY == 3 ) szYAxisMod = _T("SDSD");
					}
					else if( m_nColY == 1 ) szYAxisMod = _T("SD");
					if( szXAxisMod != _T("") )
						szItem.Format( _T("%s%s"), szXAxisMod, szX );
					else
						szItem.Format( _T("%s"), szX );
					for( i = 0; i < nMax; i++ )
					{
						if( szYAxisMod != _T("") )
							szTemp.Format( _T(" %s%s%d"), szYAxisMod, szY, i + 1 );
						else
							szTemp.Format( _T(" %s%d"), szY, i + 1 );
						szItem += szTemp;
					}
					szItem += _T("\n");
					fOut.WriteString( szItem );

					// Now reloops through data and add to file per X
					::SortStringListAlpha( &lstX );
					while( lstX.GetCount() > 0 )
					{
						szCur = lstX.GetHead();
						szItem.Format( _T("%s"), szCur );
						for( i = 0; i < nPts; i++ )
						{
							szTemp.Format( _T("%7.3f"), dX[ i ] );
							if( szTemp == szCur )
							{
								szTemp.Format( _T(" %7.3f"), dY[ i ] );
								szItem += szTemp;
							}
						}
						szItem += _T("\n");
						fOut.WriteString( szItem );

						lstX.RemoveHead();
					}

					delete [] dX;
					delete [] dY;
					fOut.Close();

					CWaitCursor crs;
					if( lstX.GetCount() > 1000 ) DataTabGridDlg* d = new DataTabGridDlg( szF, this );
					else DataTabDlg* d = new DataTabDlg( szF, this );
				}
				else
				{
					if( m_nColX == 0 ) nX = m_nX;
					else nX = _stdev_offset + m_nX;
					if( m_nColY == 0 ) nY = m_nY;
					else nY = _stdev_offset + m_nY;

					// progress
					// 1 step per line in INC
					CString szData;
					int nSteps = 0;
					while( fIn.ReadString( szData ) ) nSteps++;
					nSteps -= 1;
					nSteps *= 2;
					if( nSteps <= 0 ) return;
					int nStep = MAX( 1, nSteps / 100 ), nCur = 0;
					double dVal = 0.;
					fIn.SeekToBegin();
					Progress::EnableProgress( nSteps );

					// 1st GET NUMBER OF POINTS
					// Skip 1st line
					getlinefloat( fIn, dTemp, 100 );
					// Loop through
					while( getlinefloat( fIn, dTemp, 100 ) != 0 )
					{
						if( ( m_dlgOptions.m_nGrp > 0 ) || ( m_dlgOptions.m_nSubj > 0 ) ||
							( m_dlgOptions.m_nCond > 0 ) || ( m_dlgOptions.m_nTrial > 0 ) ||
							( m_dlgOptions.m_nStroke > 0 ) || m_dlgOptions.m_fExcludeX ||
							m_dlgOptions.m_fExcludeY )
						{
							if( ( m_dlgOptions.m_nGrp2 > 0 ) && ( dTemp[ 0 ] == m_dlgOptions.m_nGrp2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nSubj2 > 0 ) && ( dTemp[ 1 ] == m_dlgOptions.m_nSubj2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nCond2 > 0 ) && ( dTemp[ 2 ] == m_dlgOptions.m_nCond2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nTrial2 > 0 ) && ( dTemp[ 3 ] == m_dlgOptions.m_nTrial2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nStroke2 > 0 ) && ( dTemp[ 4 ] == m_dlgOptions.m_nStroke2 ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeX && ( dTemp[ m_cboX.GetCurSel() ] != m_dlgOptions.m_dExcludeValX ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeY && ( dTemp[ m_cboY.GetCurSel() ] != m_dlgOptions.m_dExcludeValY ) ) fInclude = TRUE;
							else fInclude = FALSE;
						}

						if( fInclude ) nPts++;

						nCur++;
						if( ( nCur % nStep ) == 0 ) 
						{
							dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
							szData.Format( _T("Progress %.1f %% - INPUT: Step %u of %u complete"), dVal, nCur, nSteps );
							Progress::SetProgress( nCur, szData );
						}
					}
					if( nPts == 0 ) break;

					// NOW GET THE X,Y POINTS
					double* dX = new double[ nPts ];
					double* dY = new double[ nPts ];
					// Skip 1st line
					fIn.SeekToBegin();
					getlinefloat( fIn, dTemp, 100 );
					// Loop through
					while( getlinefloat( fIn, dTemp, 100 ) != 0 )
					{
						if( ( m_dlgOptions.m_nGrp > 0 ) || ( m_dlgOptions.m_nSubj > 0 ) ||
							( m_dlgOptions.m_nCond > 0 ) || ( m_dlgOptions.m_nTrial > 0 ) ||
							( m_dlgOptions.m_nStroke > 0 ) || m_dlgOptions.m_fExcludeX ||
							m_dlgOptions.m_fExcludeY )
						{
							if( ( m_dlgOptions.m_nGrp2 > 0 ) && ( dTemp[ 0 ] == m_dlgOptions.m_nGrp2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nSubj2 > 0 ) && ( dTemp[ 1 ] == m_dlgOptions.m_nSubj2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nCond2 > 0 ) && ( dTemp[ 2 ] == m_dlgOptions.m_nCond2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nTrial2 > 0 ) && ( dTemp[ 3 ] == m_dlgOptions.m_nTrial2 ) ) fInclude = TRUE;
							else if( ( m_dlgOptions.m_nStroke2 > 0 ) && ( dTemp[ 4 ] == m_dlgOptions.m_nStroke2 ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeX && ( dTemp[ m_cboX.GetCurSel() ] != m_dlgOptions.m_dExcludeValX ) ) fInclude = TRUE;
							else if( m_dlgOptions.m_fExcludeY && ( dTemp[ m_cboY.GetCurSel() ] != m_dlgOptions.m_dExcludeValY ) ) fInclude = TRUE;
							else fInclude = FALSE;
						}

						if( fInclude )
						{
							dX[ nIdx ] = dTemp[ nX ];
							dY[ nIdx ] = dTemp[ nY ];
							nIdx++;
						}

						nCur++;
						if( ( nCur % nStep ) == 0 ) 
						{
							dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
							szData.Format( _T("Progress %.1f %% - INPUT: Step %u of %u complete"), dVal, nCur, nSteps );
							Progress::SetProgress( nCur, szData );
						}
					}
					Progress::DisableProgress();
					fIn.Close();

					// How Many Diff. X's
					CStringList lstX;
					CString szItem, szCur, szTemp;
					int i = 0;
					for( i = 0; i < nPts; i++ )
					{
						szItem.Format( _T("%7.3f"), dX[ i ] );
						if( !lstX.Find( szItem ) ) lstX.AddTail( szItem );
					}
					int nNumX = (int)lstX.GetCount();

					// File header
					szItem.Format( _T("%s %s\n"), szX, szY );
					fOut.WriteString( szItem );

					// progress
					// 1 step per line in INC
					nSteps = nPts;
					nStep = MAX( 1, nSteps / 100 ), nCur = 0;
					Progress::EnableProgress( nSteps );
					nCur = 0;

					// Now reloops through data and add to file per X
					::SortStringListAlpha( &lstX );
					while( lstX.GetCount() > 0 )
					{
						szCur = lstX.GetHead();
						for( i = 0; i < nPts; i++ )
						{
							szTemp.Format( _T("%7.3f"), dX[ i ] );
							if( szTemp == szCur )
							{
								szItem.Format( _T("%s %7.3f\n"), szCur, dY[ i ] );
								fOut.WriteString( szItem );
							}

							nCur++;
							if( ( nCur % nStep ) == 0 ) 
							{
								dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
								szData.Format( _T("Progress %.1f %% - OUTPUT: Line %u of file complete"), dVal, nCur );
								Progress::SetProgress( nCur, szData );
							}
						}

						lstX.RemoveHead();
					}

					delete [] dX;
					delete [] dY;
					fOut.Close();
					Progress::DisableProgress();

					CWaitCursor crs;
					if( nSteps > 1000 ) DataTabGridDlg* d = new DataTabGridDlg( szF, this );
					else DataTabDlg* d = new DataTabDlg( szF, this );
				}
			}
			else BCGPMessageBox( _T("No data file available.") );
			break;
		}
	}
}

//************************************
// Method:    OnChkScale
// FullName:  AGraphDlg::OnChkScale
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnChkScale() 
{
	UpdateData( TRUE );
	if( !m_fInit ) return;

	if( m_fScale &&
		( ( m_cboX.GetCurSel() == ( _colN - 1 ) ) ||
		  ( m_cboY.GetCurSel() == ( _colN - 1 ) ) ) )
	{
		BCGPMessageBox( _T("N is not available when scaling the same as trials.") );
		m_fScale = FALSE;
		UpdateData( FALSE );
		return;
	}

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnSelchangeCboChart
// FullName:  AGraphDlg::OnSelchangeCboChart
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnSelchangeCboChart()
{
	if( !m_fInit ) return;

	IShowStats* pShow = GetShowStatsObject();
	if( !pShow ) return;

	UpdateData( TRUE );

	BOOL fBadSelection = FALSE;

Reset:
	switch( m_nChart )
	{
		case CHART_NONE:
			m_fAvg = FALSE;
			m_fStdDev = FALSE;
			m_fTrial = FALSE;
			break;
		case CHART_T:
			m_fAvg = FALSE;
			m_fStdDev = FALSE;
			m_fTrial = TRUE;
			break;
		case CHART_AVG:
			m_fAvg = TRUE;
			m_fStdDev = FALSE;
			m_fTrial = FALSE;
			break;
		case CHART_TAVG:
			m_fAvg = TRUE;
			m_fStdDev = FALSE;
			m_fTrial = TRUE;
			break;
		case CHART_AVGSD:
			pShow->put_StdDevN( FALSE );
			m_fAvg = TRUE;
			m_fStdDev = TRUE;
			m_fTrial = FALSE;
			break;
		case CHART_AVGSDN:
			pShow->put_StdDevN( TRUE );
			m_fAvg = TRUE;
			m_fStdDev = TRUE;
			m_fTrial = FALSE;
			break;
		case CHART_TAVGSD:
			pShow->put_StdDevN( FALSE );
			m_fAvg = TRUE;
			m_fStdDev = TRUE;
			m_fTrial = TRUE;
			break;
		case CHART_TAVGSDN:
			pShow->put_StdDevN( TRUE );
			m_fAvg = TRUE;
			m_fStdDev = TRUE;
			m_fTrial = TRUE;
			break;
	};
	UpdateData( FALSE );

	if( ( ( m_nColX != 0 ) || ( m_nColY != 0 ) ) &&
		( m_nChart != CHART_AVG ) )
	{
		BCGPMessageBox( _T("Only averages are available when an axis statistic has standard deviations.") );
		m_nChart = CHART_AVG;
		m_fStdDev = FALSE;
		m_cboChart.SetFocus();
		UpdateData( FALSE );
		fBadSelection = TRUE;
		goto Reset;
	}

	if( m_fTrial && ( ( m_cboY.GetCurSel() == ( _colN - 1 ) ) ) )
	{
		BCGPMessageBox( _T("N is not available with a graph containing trials.") );
		m_nChart = CHART_AVG;
		m_cboChart.SetFocus();
		UpdateData( FALSE );
		fBadSelection = TRUE;
		goto Reset;
	}

	if( m_fTrial && ( ( m_cboX.GetCurSel() == ( _colN - 1 ) ) ) )
	{
		BCGPMessageBox( _T("N is not available with a graph containing trials.") );
		m_nChart = CHART_AVG;
		m_cboChart.SetFocus();
		UpdateData( FALSE );
		fBadSelection = TRUE;
		goto Reset;
	}

	if( !fBadSelection )
	{
		SetRefresh( TRUE );

		CWnd* pWnd = GetDlgItem( IDC_CHK_SCALE );
		if( pWnd ) pWnd->EnableWindow( !m_fTrial );
	}
}

//************************************
// Method:    OnSelchangeCboColX
// FullName:  AGraphDlg::OnSelchangeCboColX
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnSelchangeCboColX()
{
	UpdateData( TRUE );
	if( !m_fInit ) return;

	if( m_nColX != 0 )
	{
		m_nChart = CHART_AVG;
		m_fStdDev = FALSE;
		UpdateData( FALSE );
	}

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnSelchangeCboColY
// FullName:  AGraphDlg::OnSelchangeCboColY
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void AGraphDlg::OnSelchangeCboColY()
{
	UpdateData( TRUE );
	if( !m_fInit ) return;

	if( m_nColY != 0 )
	{
		m_nChart = CHART_AVG;
		m_fStdDev = FALSE;
		UpdateData( FALSE );
	}

	SetRefresh( !( !m_fAvg && !m_fTrial && !m_fStdDev ) );
}

//************************************
// Method:    OnHelpInfo
// FullName:  AGraphDlg::OnHelpInfo
// Access:    protected 
// Returns:   BOOL
// Qualifier:
// Parameter: HELPINFO * pHelpInfo
//************************************
BOOL AGraphDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("analysischarts.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

//************************************
// Method:    SetRefresh
// FullName:  AGraphDlg::SetRefresh
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: BOOL fEnable
//************************************
void AGraphDlg::SetRefresh( BOOL fEnable )
{
	// get button object
	CButton* pBn = (CButton*)GetDlgItem( IDC_BN_REFRESH );
	// enable/disable window
	pBn->EnableWindow( fEnable );
}

//************************************
// Method:    ExportSummarization
// FullName:  AGraphDlg::ExportSummarization
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: int nGroupMode
//************************************
BOOL AGraphDlg::ExportSummarization( CString szExp, int nGroupMode )
{
/*
	// TODO: we need to add progress meter
	if( ( nGroupMode != EXP_SUMM_PARAM ) && ( nGroupMode != EXP_SUMM_SUBM ) )
		return FALSE;

	// sub-movement analysis?
	BOOL fsma = FALSE;
	IProcSetting* pPS = NULL;
	UserObj* pObj = ::GetCurrentUserObj();
	BOOL fUserObj = FALSE;
	HRESULT hr = E_FAIL;
	if( pObj && pObj->m_pPS )
	{
		pPS = pObj->m_pPS;
		hr = S_OK;
		fUserObj = TRUE;
	}
	else
	{
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
							   IID_IProcSetting, (LPVOID*)&pPS );
//		if( pObj ) pObj->m_pPS = pPS;
	}
	if( SUCCEEDED( hr ) )
	{
		if( !fUserObj ) pPS->Find( szExp.AllocSysString() );
		IProcSetFlags* pPSF = NULL;
		hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_SEG_S( &fsma );
			pPSF->Release();
		}
		if( !fUserObj ) pPS->Release();
	}
	if( !fsma )
	{
		BCGPMessageBox( _T("This experiment does not use sub-movement analysis. Halting.") );
		return FALSE;
	}

	// TMP file
	CString szFile;
	GetExpINC( szExp, szFile );
	int nPos = szFile.ReverseFind( '.' );
	if( nPos == -1 ) return FALSE;
	szFile = szFile.Left( nPos + 1 );
	szFile += _T("tmp");

	// verify existence
	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("TMP file does not exist. You must first run analysis.") );
		return FALSE;
	}

	// output file name
	CString szOutFile = szFile;
	nPos = szOutFile.ReverseFind( '.' );
	if( nPos == -1 ) return FALSE;
	szOutFile = szOutFile.Left( nPos + 1 );
	szOutFile += _T("ana");

	// open files
	CStdioFile fi, fo;
	if( !fi.Open( szFile, CFile::modeRead ) )
	{
		BCGPMessageBox( _T("Unable to open summarization file for reading.") );
		return FALSE;
	}
	if( !fo.Open( szOutFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Unable to open export file for writing.") );
		return FALSE;
	}

	// 1st line is the column headers
	CString szLine;
	if( !fi.ReadString( szLine ) ) 
	{
		BCGPMessageBox( _T("Summarization file is empty.") );
		return FALSE;
	}

	CWaitCursor crs;

	// write headers to output file
	CString szItem, szTemp;
	CStringList m_lstHeaders;
	int nItem = 0;
	nPos = szLine.Find( _T(" ") );
	if( nPos == -1 ) return FALSE;
	BOOL fCont = TRUE;
	while( fCont )
	{
		// get next header
		if( nPos != -1 )
		{
			szItem = szLine.Left( nPos );
			szLine = szLine.Mid( nPos + 1 );
			TRIM( szLine );
		}
		else
		{
			szItem = szLine;
			fCont = FALSE;
		}

		// write to file (fixed columns are not changed)
		if( nItem > ( COL_STROKE ) )
		{
			if( nGroupMode == EXP_SUMM_PARAM )
			{
				szTemp.Format( _T(" %s1 %s2 %s3"), szItem, szItem, szItem );
				fo.WriteString( szTemp );
			}
			else if( nGroupMode == EXP_SUMM_SUBM )
			{
				// we'll just add to string list and then add them later
				m_lstHeaders.AddTail( szItem );
			}
		}
		// ignore submovement
		else if( nItem != COL_STROKE )
		{
			if( nItem != 0 ) fo.WriteString( _T(" ") );
			fo.WriteString( szItem );
		}

		nItem++;
		nPos = szLine.Find( _T(" ") );
	}

	// if was submovement move, we now need to add the non-fixed headers
	POSITION pos;
	if( nGroupMode == EXP_SUMM_SUBM )
	{
		for( int sub = 0; sub < 3; sub++ )
		{
			pos = m_lstHeaders.GetHeadPosition();
			while( pos )
			{
				szItem = m_lstHeaders.GetNext( pos );
				szTemp.Format( _T(" %s%d"), szItem, sub + 1 );
				fo.WriteString( szTemp );
			}
		}
	}
	// conclude line
	fo.WriteString( _T("\n") );

	int nGroup = 0, nSubj = 0, nCond = 0, nIdxTrial = 0, nIdxStroke = 0, nIdxSubMvmt = 0;
	double thedata[ NDATAMAX ][ 3 ];
	memset( thedata, 0, sizeof( double ) * NDATAMAX * 3 );
	int nCol, nTotal = 0;
	// loop through each line of file
	while( fi.ReadString( szLine ) )
	{
		szLine.MakeUpper();
		nCol = 0;

		// Get group
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szItem = szLine.Left( nPos );
		nGroup = atoi( szItem );
		szLine = szLine.Mid( nPos + 1 );
		nCol++;
			
		// Get subject
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szItem = szLine.Left( nPos );
		nSubj = atoi( szItem );
		szLine = szLine.Mid( nPos + 1 );
		nCol++;

		// Get condition ID
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szItem = szLine.Left( nPos );
		nCond = atoi( szItem );
		szLine = szLine.Mid( nPos + 1 );
		nCol++;

		// Get trial
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szItem = szLine.Left( nPos );
		nIdxTrial = atoi( szItem );
		szLine = szLine.Mid( nPos + 1 );
		nCol++;

		// Get stroke
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szItem = szLine.Left( nPos );
		nIdxStroke = atoi( szItem );
		szLine = szLine.Mid( nPos + 1 );
		nCol++;

		// Get submovement
		nPos = szLine.Find( _T(" ") );
		if( nPos == -1 ) break;
		szItem = szLine.Left( nPos );
		nIdxSubMvmt = atoi( szItem );
		if( nIdxSubMvmt < 1 )
		{
			BCGPMessageBox( _T("Submovement index is out of range. Cannot continue.") );
			return FALSE;
		}
		szLine = szLine.Mid( nPos + 1 );
		nCol++;

	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
		// This is where we need to grab the actual data and store in arrays for
		// later writing based upon grouping method
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////

		// let's nix all whitespace to be sure we don't have junk and data isn't shifted
		TRIM( szLine );
		while( nCol < NDATAMAX )
		{
			// get next data value
			nPos = szLine.Find( _T(" ") );
			if( nPos == -1 )
			{
				// our last value
				thedata[ nCol - COL_STROKES ] [ nIdxSubMvmt - 1 ] = atof( szLine );
				nCol++;
				break;
			}
			szItem = szLine.Left( nPos );
			TRIM( szItem );
			szLine = szLine.Mid( nPos + 1 );
			TRIM( szLine );
			// assign value to array
			thedata[ nCol - COL_STROKES ][ nIdxSubMvmt - 1 ] = atof( szItem );
			// next column
			nCol++;
		}

		// update total columns if necessary
		if( ( nCol - 1 ) > nTotal ) nTotal = nCol;

		// if 3rd submovement, time to write & reset
		if( nIdxSubMvmt == 3 )
		{
			// write the fixed columns
			szItem.Format( _T("%d %d %d %d %d"), nGroup, nSubj, nCond, nIdxTrial, nIdxStroke );
			fo.WriteString( szItem );

			// WRITE THE REMAINDER
			// which mode are we using?
			if( nGroupMode == EXP_SUMM_PARAM )
			{
				// Feat-SubM 1, Feat-SubM 2, Feat-SubM 3
				for( int i = 0; i < ( nTotal - COL_STROKES ); i++ )
				{
					szItem.Format( _T(" %g %g %g"), thedata[ i ][ 0 ],
													thedata[ i ][ 1 ],
													thedata[ i ][ 2 ] );
					fo.WriteString( szItem );
				}
				fo.WriteString( _T("\n") );
			}
			else if( nGroupMode == EXP_SUMM_SUBM )
			{
				// All features of SubM 1, All features of SubM 2, All features of SubM 3
				for( int sub = 0; sub < 3; sub++ )
				{
					for( int col = 0; col < ( nTotal - COL_STROKES ); col++ )
					{
						szItem.Format( _T(" %g"), thedata[ col ][ sub ] );
						fo.WriteString( szItem );
					}
				}
				fo.WriteString( _T("\n") );
			}
		}
	}

	szLine.Format( _T("Export succeeded successfully. The resultant file is %s"), szOutFile );
	BCGPMessageBox( szLine );
*/

	return TRUE;
}

//************************************
// Method:    GetQuestions
// FullName:  AGraphDlg::GetQuestions
// Access:    protected 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL AGraphDlg::GetQuestions()
{
	// if already filled, do nothing
	if( m_lstQuests.GetCount() > 0 ) return TRUE;

	// questionnaire exclusions
	CString szQuestFile;
	::GetDataPathRoot( szQuestFile );
	szQuestFile += _T("\\");
	szQuestFile += m_szExp;
	szQuestFile += _T("\\");
	szQuestFile += m_szExp;
	szQuestFile += _T("-quest.SUM");
	CStringList lstExclQ;
	CStdioFile fileQ;
	if( fileQ.Open( szQuestFile, CFile::modeRead ) )
	{
		CString szQLine, szQID;
		while( fileQ.ReadString( szQLine ) )
		{
			szQID = szQLine.Left( 3 );
			if( !lstExclQ.Find( szQID ) ) lstExclQ.AddTail( szQID );
		}
		fileQ.Close();
	}

	// questionnaire objects
	IMQuestionnaire* pQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
								   IID_IMQuestionnaire, (LPVOID*)&pQ );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Error creating MQuestionnaire object.") );
		return FALSE;
	}
	IGQuestionnaire* pGQ = NULL;
	hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
						   IID_IGQuestionnaire, (LPVOID*)&pGQ );
	if( FAILED( hr ) )
	{
		pQ->Release();
		return FALSE;
	}

	// cleanup lists
	CleanupQuests();

	// get groups
	CStringList lstGrps;
	IExperimentMember* pEML = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEML );
	if( FAILED( hr ) )
	{
		pQ->Release();
		pGQ->Release();
		return FALSE;
	}
	BSTR bstrGrpID = NULL;
	CString szGrpID;
	hr = pEML->FindForExperiment( m_szExp.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEML->get_GroupID( &bstrGrpID );
		szGrpID = bstrGrpID;
		if( !lstGrps.Find( szGrpID ) ) lstGrps.AddTail( szGrpID );
		hr = pEML->GetNext();
	}
	pEML->Release();

	// questionnaire info
	POSITION posQ = NULL, posI = NULL, pos = NULL;
	CStringList lstIdx, lstID, lstIDSorted;
	BSTR bstr = NULL;
	BOOL fHdr = FALSE;
	short nPos = 0;
	CString szQID, szIdx;
	hr = pQ->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// question ID
		pQ->get_ID( &bstr );
		szQID = bstr;
		// loop through groups to see if it's a group question
		HRESULT hr2 = S_OK;
		BOOL fFound = FALSE;
		POSITION pos = lstGrps.GetHeadPosition();
		while( pos && !fFound )
		{
			szGrpID = lstGrps.GetNext( pos );
			hr2 = pGQ->FindForGroup( m_szExp.AllocSysString(), szGrpID.AllocSysString() );
			if( SUCCEEDED( hr2 ) )
			{
				fFound = TRUE;
				lstID.AddTail( szQID );

				pQ->get_ItemNum( &nPos );
				szIdx.Format( _T("%d"), nPos );
				lstIdx.AddTail( szIdx );
			}
		}
		// next question
		hr = pQ->GetNext();
	}
	// sort
	int nCount = lstID.GetCount();
	szQID = _T("");
	for( int i = 0; i < nCount; i++ )
	{
		lstIDSorted.AddTail( szQID );
	}
	posQ = lstID.GetHeadPosition();
	posI = lstIdx.GetHeadPosition();
	while( posI )
	{
		szQID = lstID.GetNext( posQ );
		szIdx = lstIdx.GetNext( posI );
		nPos = atoi( szIdx ) - 1;
		pos = lstIDSorted.FindIndex( nPos );
		if( pos ) lstIDSorted.SetAt( pos, szQID );
	}

	// create questions
	posQ = lstIDSorted.GetHeadPosition();
	while( posQ )
	{
		szQID = lstIDSorted.GetNext( posQ );
		if( SUCCEEDED( pQ->Find( szQID.AllocSysString() ) ) )
		{
			pQ->get_IsHeader( &fHdr );
			if( !fHdr && !lstExclQ.Find( szQID ) )
			{
				Question* pQuest = new Question;
				pQuest->szID = szQID;
				pQ->get_Question( &bstr );
				pQuest->szQ = bstr;
				pQ->get_ColumnHeader( &bstr );
				pQuest->szHdr = bstr;
				pQ->get_IsNumeric( &(pQuest->fNum) );
				m_lstQuests.AddTail( pQuest );
			}
		}
	}

	pQ->Release();
	pGQ->Release();

	return TRUE;
}

//************************************
// Method:    GetIdentityIDs
// FullName:  GetIdentityIDs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CStringList * pListGrp
// Parameter: CStringList * pListSubj
// Parameter: CStringList * pListCond
// Parameter: int nGrp
// Parameter: int nSubj
// Parameter: int nCond
// Parameter: int & nLastGrp
// Parameter: int & nLastSubj
// Parameter: int & nLastCond
// Parameter: CString & szGrp
// Parameter: CString & szSubj
// Parameter: CString & szCond
//************************************
void GetIdentityIDs( CStringList* pListGrp, CStringList* pListSubj, CStringList* pListCond,
					 int nGrp, int nSubj, int nCond, int& nLastGrp, int& nLastSubj, int& nLastCond,
					 CString& szGrp, CString& szSubj, CString& szCond )
{
	if( !pListGrp || !pListSubj || !pListCond ) return;

	POSITION pos = NULL;
	CString szTmp, szIdx, szID;
	int nPos = -1;

	// get group if not same
	if( nGrp != nLastGrp ) {
		szGrp = _T("?");
		pos = pListGrp->GetHeadPosition();
		while( pos ) {
			szTmp = pListGrp->GetNext( pos );
			nPos = szTmp.Find( _T(":") );
			if( nPos == -1 ) break;
			szIdx = szTmp.Left( nPos );
			szID = szTmp.Mid( nPos + 1 );
			if( atoi( szIdx ) == nGrp ) {
				szGrp = szID;
				break;
			}
		}
	}
	nLastGrp = nGrp;
	// get subject if not same
	if( nSubj != nLastSubj ) {
		szSubj = _T("?");
		pos = pListSubj->GetHeadPosition();
		while( pos ) {
			szTmp = pListSubj->GetNext( pos );
			nPos = szTmp.Find( _T(":") );
			if( nPos == -1 ) break;
			szIdx = szTmp.Left( nPos );
			szID = szTmp.Mid( nPos + 1 );
			if( atoi( szIdx ) == nSubj ) {
				szSubj = szID;
				break;
			}
		}
	}
	nLastSubj = nSubj;
	// get condition if not same
	if( nCond != nLastCond ) {
		szCond = _T("?");
		pos = pListCond->GetHeadPosition();
		while( pos ) {
			szTmp = pListCond->GetNext( pos );
			nPos = szTmp.Find( _T(":") );
			if( nPos == -1 ) break;
			szIdx = szTmp.Left( nPos );
			szID = szTmp.Mid( nPos + 1 );
			if( atoi( szIdx ) == nCond ) {
				szCond = szID;
				break;
			}
		}
	}
	nLastCond = nCond;
}

//************************************
// Method:    BuildNormDB
// FullName:  AGraphDlg::BuildNormDB
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szINC
// Parameter: BOOL fBypassSettings
//************************************
BOOL AGraphDlg::BuildNormDB( CString szExp, CString szINC, BOOL fBypassSettings )
{
	// remove files
	int nPos = szINC.ReverseFind( '.' );
	CString szXM1 = szINC.Left( nPos ), szMsg;
	CString szXMD = szXM1, szXMZ = szXM1, szINCTemp = szXM1, szXMH = szXM1;
	szXM1 += _T(".XM1");
	szXMD += _T(".XMD");
	szXMZ += _T(".XMZ");
	szXMH += _T(".XMH");
	szINCTemp += _T("t.INC");
	CFileFind ff;
	if( ff.FindFile( szXM1 ) ) REMOVE_FILE( szXM1 );
	if( ff.FindFile( szXMD ) ) REMOVE_FILE( szXMD );
	if( ff.FindFile( szXMZ ) ) REMOVE_FILE( szXMZ );
	if( ff.FindFile( szXMH ) ) REMOVE_FILE( szXMH );
	szMsg.Format( _T("NORM DB: Rebuilding. Deleting existing norm DB (%s.XMD) and history (%s.XMH) files."), szExp, szExp );
	OutputAMessage( szMsg, TRUE, LOG_PROC );

	// experiment settings
	BOOL fScore = FALSE, fScoreGlobal = FALSE, fNoUpdate = FALSE;
	double dTH = 0.;
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) ) {
		szMsg = _T("Unable to create ProcSettings object.");
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
		return FALSE;
	}
	pPS->Find( szExp.AllocSysString() );
	IProcSetFlags* pPSF = NULL;
	if( SUCCEEDED( pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF ) ) && pPSF ) {
		pPSF->get_NormDBCalcScore( &fScore );
		pPSF->get_NormDBCalcScoreGlobal( &fScoreGlobal );
		pPSF->get_NormDBNoUpdate( &fNoUpdate );
		pPSF->get_NormDBThreshold( &dTH );
		pPSF->Release();
	}
	pPS->Release();
	// open INC and temp INC files
	CStdioFile fInc, fIncTemp;
	if( !fInc.Open( szINC, CFile::modeRead ) ) {
		szMsg =  _T("Unable to open INC file for reading.");
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
		return FALSE;
	}
	// Loop through INC and write subject blocks to temp file, call "updateNormDB", and continue
	CString szLine, szGrp, szSubj, szData, szLastGrp, szLastSubj, szLastLine;
	char* pszData = new char[ 2000 ];
	char pszGrp[ 5 ], pszSubj[ 5 ];
	// Get header
	CString szHeaders;
	fInc.ReadString( szHeaders );
	// Data
	int nRow = 0;
	while( ( szLastLine != _T("") ) || fInc.ReadString( szLine ) ) {
		if( nRow == 0 ) {
			// open file
			if( !fIncTemp.Open( szINCTemp, CFile::modeWrite | CFile::modeCreate ) ) {
				szMsg =  _T("Unable to open INC file for reading.");
				OutputAMessage( szMsg, TRUE, LOG_PROC );
				BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
				delete [] pszData;
				return FALSE;
			}
			// write header
			fIncTemp.WriteString( szHeaders );
			fIncTemp.WriteString( _T("\n") );
		}
		if( szLastLine != _T("") ) {
			szLine = szLastLine;
			szLastLine = _T("");
		}
		nRow++;
		// read from INC - grp, subj, & rest
		sscanf( szLine, _T("%s %s %[^\n]"), pszGrp, pszSubj, pszData );
		szGrp = pszGrp;
		szSubj = pszSubj;
		szData = pszData;
		if( szLastGrp == _T("") ) szLastGrp = szGrp;
		if( szLastSubj == _T("") ) szLastSubj = szSubj;
		// write until a new group/subject
		if( ( szGrp == szLastGrp ) && ( szSubj == szLastSubj ) ) {
			szData.Format( _T("%s\n"), szLine );
			fIncTemp.WriteString( szData );
		}
		else {
			if( fIncTemp.m_pStream ) fIncTemp.Close();
			if( !UpdateNormDB( fScore, fScoreGlobal, fNoUpdate, dTH, szExp, szINCTemp, TRUE, FALSE, NULL, fBypassSettings ) ) {
				REMOVE_FILE( szINCTemp );
				return FALSE;
			}
			REMOVE_FILE( szINCTemp );
			szLastGrp = szLastSubj = _T("");
			szLastLine = szLine;
			nRow = 0;
		}
		szLastGrp = szGrp;
		szLastSubj = szSubj;
	}
	// last block would not have finished
	if( nRow > 0 ) {
		if( fIncTemp.m_pStream ) fIncTemp.Close();
		if( !UpdateNormDB( fScore, fScoreGlobal, fNoUpdate, dTH, szExp, szINCTemp, TRUE, FALSE, NULL, fBypassSettings  ) ) {
			REMOVE_FILE( szINCTemp );
			return FALSE;
		}
		REMOVE_FILE( szINCTemp );
	}

	delete [] pszData;
	return TRUE;
}

//************************************
// Method:    UpdateNormDB
// FullName:  AGraphDlg::UpdateNormDB
// Access:    public static 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fComputeZ
// Parameter: BOOL fComputeZGlobal
// Parameter: BOOL fCheckToUpdateDB
// Parameter: double dThreshold
// Parameter: CString szExp
// Parameter: CString szINC
// Parameter: BOOL fCompare
// Parameter: BOOL fReturnZData
// Parameter: HAData ** retData
// Parameter: BOOL fBypassSettings
//************************************
BOOL AGraphDlg::UpdateNormDB( BOOL fComputeZ, BOOL fComputeZGlobal, BOOL fCheckToUpdateDB, double dThreshold,
							  CString szExp, CString szINC, BOOL fCompare, BOOL fReturnZData, HAData** retData,
							  BOOL fBypassSettings )
{
	BOOL fOutputImpairment = FALSE;	// TODO: Option from somewhere
	BOOL fUpdAvg = FALSE;
	BOOL fRet = TRUE, fExists = FALSE, fWroteToXMZ = FALSE, fUpdateDB = TRUE, fHaveData = FALSE;
	CString szHeaders, szData, szGrp, szSubj, szCond, szOldHeaders, szOldTemp, szToWrite;
	CString szLine, szTemp, szLineTemp, szMsg;
	int nHeaders = 0, nCurPos = 0, nN = -1, nGrp = 0, nSubj = 0, nCond = 0;
	int nLastGrp = 0, nLastSubj = 0, nLastCond = 0, nNum = 0;
	int nIdx = 0, nStroke = 0, nCurPosTemp = 0, nToAvg = 0, nNewN = 0;
	char* pszData = new char[ 3000 ];
	char pszGrp[ 5 ], pszSubj[ 5 ], pszCond[ 5 ];
	double* dDataAvg = new double[ SIZE_DATA ];
	double* dDataNew = new double[ SIZE_DATA ];
	double dGrp = 0., dSubj = 0., dCond = 0., dVal = 0.;
	double dTrial = 0., dAvg = 0., dSD = 0.;
	CStringList lst;
	POSITION posL = NULL;
	CFileFind ff;
	HAData* haNew = new HAData();
	HALine *line = NULL, *lineNew = NULL;
	CStdioFile fXM1, fXMD, fXMDTemp, fXMZ, fXMH;

	haNew->m_szExp = szExp;
	haNew->m_dZMaxSize = dThreshold;
	haNew->m_dZMaxDuration = dThreshold;
	haNew->m_dZMaxPressure = dThreshold;
	haNew->m_dZMaxJerk = dThreshold;

	// create agraphdlg object (but don't open)
	AGraphDlg* pAnalysis = new AGraphDlg( szExp, szINC, TRUE, MODE_AVERAGE );
	// generate TMP file from INC
	int nPos = pAnalysis->m_szINC.ReverseFind( '.' );
	pAnalysis->m_szTMP = pAnalysis->m_szINC.Left( nPos );
	pAnalysis->m_szTMP += _T(".TMP");
	pAnalysis->GenerateTmpFile( TRUE, FALSE, fBypassSettings );
	if( pAnalysis->m_szTMP == _T("") ) {
		delete pAnalysis;
		return FALSE;
	}
	// call XMean
	nPos = pAnalysis->m_szTMP.ReverseFind( '.' );
	CString szXM1 = pAnalysis->m_szTMP.Left( nPos - 1 );
	CString szTMP = pAnalysis->m_szTMP.Left( nPos );
	CString szXMD = szXM1, szXMN = szXM1, szXMZ, szXMH = szXM1;
	CString szXMDTemp = szXM1;
	CString szXMNTemp = szXM1;
	szXM1 += _T(".XM1");
	szTMP += _T(".TMP");
	szXMD += _T(".XMD");
	szXMN += _T(".XMN");
	szXMH += _T(".XMH");
	szXMDTemp += _T("t.XMD");
	szXMNTemp += _T("t.XMN");
	UINT nSwitch = eX_A | eX_S | eX_N;
	double dMax1 = -1., dMax2 = -1., dInMin1 = -1., dInMax1 = -1., dInMin2 = -1., dInMax2 = -1.;
	int nX = COL_STROKE, nFactor = COL_COND;
	HRESULT hr = E_FAIL;
	hr = ::XMean( pAnalysis->m_szTMP, szXM1, nX, nFactor,
				  pAnalysis->m_dlgOffset.m_dOffset1, pAnalysis->m_dlgOffset.m_dScale1,
				  pAnalysis->m_dlgOffset.m_dOffset2, pAnalysis->m_dlgOffset.m_dScale2,
				  nSwitch, dInMin1, dInMax1, dInMin2, dInMax2, dMax1, dMax2, FALSE );
	if( FAILED( hr ) ) {
		szMsg =  _T("There was a problem computing averages. Either there were no consistent trials, or an error occurred.");
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
		delete pAnalysis;
		return FALSE;
	}

	// Open XM1
	if( !fXM1.Open( szXM1, CFile::modeRead ) ) {
		szMsg =  _T("Unable to open XM1 file for processing.");
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
		goto error;
	}
	// open up Z history file
	if( !fXMH.Open( szXMH, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate ) ) {
		szMsg =  _T("Unable to open XMH file for reading.");
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
		goto error;
	}

	// Does XMD exist?
	nPos = szINC.ReverseFind( '.' );
	if( nPos < 0 ) return FALSE;
	fExists = ff.FindFile( szXMD );

	// If not exists, we create from new
	if( !fExists ) {
		// open subject averages file
		if( !fXMD.Open( szXMD, CFile::modeWrite | CFile::modeCreate ) ) {
			szMsg =  _T("Unable to open XMD file for writing.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		// Read headers, get header count, and get index of N
		if( !fXM1.ReadString( szLine ) ) {
			szMsg =  _T("No data in XM1 file.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		szHeaders = szLine;
		szTemp = szHeaders.Tokenize( _T(" "), nCurPos );
		while( szTemp != _T("") ) {
			if( szTemp == _T("N") ) nN = nHeaders;
			nHeaders++;
			if( nHeaders > HA_COL_OFFSET ) haNew->m_lstHeaders.AddTail( szTemp );
			szTemp = szHeaders.Tokenize( _T(" "), nCurPos );
		}
		if( ( nHeaders == 0 ) || ( nN == -1 ) ) {
			szMsg =  _T("XM1 column information invalid.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		if( nHeaders >= SIZE_DATA ) {
			szMsg.Format( _T("The XM1 has more columns (%d) than allowed (%d). Extra columns are ignored."), nHeaders, SIZE_DATA );
			OutputAMessage( szMsg, TRUE, LOG_PROC );
		}
		// write headers to output
		szTemp.Format( _T("%s N\n"), szHeaders );
		fXMD.WriteString( szTemp );

		// loop through each line of the XM1, write to XMD + 1 extra field for # of averaged = N
		// also build string to write at very end for the global subject (szLineTemp)
		szLineTemp = _T("");
		while( fXM1.ReadString( szLine ) ) {
			fHaveData = TRUE;
			// read from xm1 - grp, subj, cond & rest
			sscanf( szLine, _T("%lf %lf %lf %lf %d %[^\n]"), &dGrp, &dSubj, &dCond, &dTrial, &nStroke, pszData );
			nGrp = (int)dGrp;
			nSubj = (int)dSubj;
			nCond = (int)dCond;
			szData = pszData;
			// get N for this line: fill string list and find index
			nCurPos = 0;
			nNum = 0;
			posL = NULL;
			lst.RemoveAll();
			szTemp = szData.Tokenize( _T(" "), nCurPos );
			szTemp.Trim();
			while( szTemp != _T("") ) {
				lst.AddTail( szTemp );
				szTemp = szData.Tokenize( _T(" "), nCurPos );
				szTemp.Trim();
			}
			posL = lst.FindIndex( nN - HA_COL_OFFSET );	// reducing column # by grp/subj/cond
			if( !posL ) {
				szMsg =  _T("Unable to get number of averaged data (N) from XM1 file.");
				OutputAMessage( szMsg, TRUE, LOG_PROC );
				BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
				goto error;
			}
			szTemp = lst.GetAt( posL );
			nNum = atoi( szTemp );
			// get grp,subj,cond IDs from identity table (if we got it last time, we can skip lookup)
			GetIdentityIDs( &(pAnalysis->m_lstGroups), &(pAnalysis->m_lstSubjs), &(pAnalysis->m_lstConds),
							nGrp, nSubj, nCond, nLastGrp, nLastSubj, nLastCond, szGrp, szSubj, szCond );
			// write to XMD
			szLine.Format( _T("1 %s %s 1 %d %s %d\n"), szSubj, szCond, nStroke, szData, nNum );
			fXMD.WriteString( szLine );

			// for global subject
			if( fComputeZGlobal ) {
				szTemp.Format( _T("1 %s %s 1 %d %s %d\n"), SUBJ_GLOBAL, szCond, nStroke, szData, nNum );
				szLineTemp += szTemp;
			}

			// HAData object
			line = new HALine();
			line->m_szGrp = szGrp;
			line->m_szSubj = szSubj;
			line->m_szCond = szCond;
			line->m_nStroke = nStroke;
			line->m_nN = nNum;
			// file for XMZ
			if( szXMZ == _T("") ) {
				::GetHWR( szExp, szGrp, szSubj, _T(""), _T(""), szTemp );
				nPos = szTemp.ReverseFind( '.' );
				szXMZ = szTemp.Left( nPos );
				szXMZ += _T(".XMZ");
			}
			// add to averages data object
			haNew->m_lstLines.AddTail( line );
			haNew->m_nLines++;
		}
		// verify that we read data
		if( !fHaveData ) {
			szMsg =  _T("There is no data in the XM1 file. Was this subject excluded for summarize?\nGo to experiment->summarize->summarize to view the exclusions list.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		// we're done writing this subject...now write global subject (which = new subject in this case)
		if( fComputeZGlobal ) fXMD.WriteString( szLineTemp );
		// now write to history
		if( fComputeZ ) {
			if( fXMZ.Open( szXMZ, CFile::modeWrite | CFile::modeCreate ) ) {
				haNew->OutputOverall( &fXMZ, &fXMH );
				haNew->OutputByCondition( &fXMZ );
				haNew->OutputByStroke( &fXMZ );
				if( fComputeZGlobal ) {
					haNew->OutputOverall( &fXMZ, &fXMH, TRUE );
					haNew->OutputByCondition( &fXMZ, TRUE );
					haNew->OutputByStroke( &fXMZ, TRUE );
				}
				fXMZ.Close();

				szTemp.Format( _T("RESULTS OF TEST: Subject %s of group %s of experiment %s: "), szSubj, szGrp, szExp );
				if( fComputeZGlobal ) {
					szMsg.Format( _T("%s Personal Z-score (%.3f) and overall Z-score (%.3f) - Insufficient samples to compute Z."),
								  szTemp, haNew->m_dZ, haNew->m_dZGlobal );
				}
				else {
					szMsg.Format( _T("%s Personal Z-score (%.3f) - Insufficient samples to compute Z."), szTemp, haNew->m_dZ );
				}
				OutputAMessage( szMsg, TRUE, LOG_PROC );
			}
			else {
				szMsg =  _T("Unable to open XMZ file for writing.");
				OutputAMessage( szMsg, TRUE, LOG_PROC );
				BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
				goto error;
			}
		}
		fUpdAvg = TRUE;
		szMsg.Format( _T("NORM DB: Added subject %s of group %s to norm of subject %s."), szSubj, szGrp, szSubj );
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		if( fComputeZGlobal ) {
			szMsg.Format( _T("NORM DB: Added subject %s of group %s to overall norm (%s)."), szSubj, szGrp, SUBJ_GLOBAL );
			OutputAMessage( szMsg, TRUE, LOG_PROC );
		}
	}
	// else, we process to update with new data (TODO: Add checks for whether to include (impairment only?)
	else {
		// open existing XMD file
		if( !fXMDTemp.Open( szXMDTemp, CFile::modeWrite | CFile::modeCreate ) ) {
			szMsg =  _T("Unable to open temp XMD file for writing.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		// open subject averages file
		if( !fXMD.Open( szXMD, CFile::modeRead ) ) {
			szMsg =  _T("Unable to open XMD file for reading.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}

		// VERIFY HEADERS
		// read headers of XM1 and XMD to ensure they're the same
		if( !fXM1.ReadString( szLine ) ) {
			szMsg =  _T("No data in XM1 file.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		szHeaders = szLine;
		if( !fXMD.ReadString( szLineTemp ) ) {
			szMsg =  _T("No data in XMD file.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		// loop through each header and compare (also get header count + column # of N)
		szTemp = szLine.Tokenize( _T(" "), nCurPos );
		szOldTemp = szLineTemp.Tokenize( _T(" "), nCurPosTemp );
		while( ( szTemp != _T("") ) && ( szOldTemp != _T("") ) ) {
			if( szTemp == _T("N") ) nN = nHeaders;
			nHeaders++;
			if( nHeaders > HA_COL_OFFSET ) haNew->m_lstHeaders.AddTail( szTemp );
			if( szTemp != szOldTemp ) {
				szMsg =  _T("The features (columns) of the norm DB do not match the new data.\nYou will need to rebuild the norm DB or change your summarization data columns to match.");
				OutputAMessage( szMsg, TRUE, LOG_PROC );
				BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
				goto error;
			}
			szTemp = szLine.Tokenize( _T(" "), nCurPos );
			szOldTemp = szLineTemp.Tokenize( _T(" "), nCurPosTemp );
		}
		// verify we've gone through all headers and are only left with "N" in the old processed file
		if( ( szTemp != _T("") ) || ( szOldTemp != _T("N") ) ) {
			szMsg =  _T("The features (columns) of the norm DB do not match the new data.\nYou will need to rebuild the norm DB or change your summarization data columns to match.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		// verify we've got a legitimate N column
		if( nN <= 0 ) {
			szMsg =  _T("There was an error computing the norm DB.\nThe N column could not be determined.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}
		if( nHeaders >= SIZE_DATA ) {
			szMsg.Format( _T("The XM1 has more columns (%d) than allowed (%d). Extra columns are ignored."), nHeaders, SIZE_DATA );
			OutputAMessage( szMsg, TRUE, LOG_PROC );
		}

		// Write headers to XMD
		szTemp.Format( _T("%s N\n"), szHeaders );
		fXMDTemp.WriteString( szTemp );

		// Read XM1 and put into HAData object (should be fairly small amount of data)
		while( fXM1.ReadString( szLine ) ) {
			fHaveData = TRUE;
			// grp, subj, cond, trial, stroke & rest
			sscanf( szLine, _T("%lf %lf %lf %lf %d %[^\n]"), &dGrp, &dSubj, &dCond, &dTrial, &nStroke, pszData );
			nGrp = (int)dGrp;
			nSubj = (int)dSubj;
			nCond = (int)dCond;
			szData = pszData;
			GetIdentityIDs( &(pAnalysis->m_lstGroups), &(pAnalysis->m_lstSubjs), &(pAnalysis->m_lstConds),
							nGrp, nSubj, nCond, nLastGrp, nLastSubj, nLastCond, szGrp, szSubj, szCond );
			line = new HALine();
			line->m_szGrp = szGrp;
			line->m_szSubj = szSubj;
			line->m_szCond = szCond;
			line->m_nStroke = nStroke;
			// file for XMZ
			if( szXMZ == _T("") ) {
				::GetHWR( szExp, szGrp, szSubj, _T(""), _T(""), szTemp );
				nPos = szTemp.ReverseFind( '.' );
				szXMZ = szTemp.Left( nPos );
				szXMZ += _T(".XMZ");
			}
			// get N for this line: fill string list and find index
			nCurPos = 0;
			nIdx = 0;
			posL = NULL;
			lst.RemoveAll();
			szTemp = szData.Tokenize( _T(" "), nCurPos );
			szTemp.Trim();
			while( szTemp != _T("") ) {
				lst.AddTail( szTemp );
				if( nIdx < SIZE_DATA ) line->m_pData[ nIdx++ ] = atof( szTemp );
				szTemp = szData.Tokenize( _T(" "), nCurPos );
				szTemp.Trim();
			}
			posL = lst.FindIndex( nN - HA_COL_OFFSET );	// reducing column # by grp/subj/cond/trial/stroke
			if( !posL ) {
				szMsg =  _T("Unable to get number of averaged data (N) from XM1 file.");
				OutputAMessage( szMsg, TRUE, LOG_PROC );
				BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
				goto error;
			}
			szTemp = lst.GetAt( posL );
			line->m_nN = atoi( szTemp );
			// add to averages data object
			haNew->m_lstLines.AddTail( line );
			haNew->m_nLines++;
		}
		fXM1.Close();

		// verify that we read data
		if( !fHaveData ) {
			szMsg =  _T("There is no data in the XM1 file. Was this subject excluded for summarize?\nGo to experiment->summarize->summarize to view the exclusions list.");
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			goto error;
		}

		// Loop through XMD to test compute Z
		if( fComputeZ ) {
			while( fXMD.ReadString( szLine ) ) {
				// grp, subj, cond, trial, stroke & rest
				sscanf( szLine, _T("%s %s %s %lf %d %[^\n]"), pszGrp, pszSubj, pszCond, &dTrial, &nStroke, pszData );
				// if this subject is the subject/cond/stroke from the xm1, process
				HAData::haModeFind nMode = fComputeZGlobal ? HAData::haFindBoth : HAData::haFindSubject;
				line = haNew->Find( pszSubj, pszCond, nStroke, nMode );
				if( line ) {
					fWroteToXMZ = TRUE;
					// get data
					szData = pszData;
					nCurPos = 0;
					nNum = 0;
					nIdx = 0;
					ZeroMemory( dDataAvg, SIZE_DATA * sizeof( double ) );
					szTemp = szData.Tokenize( _T(" "), nCurPos );
					szTemp.Trim();
					while( szTemp != _T("") ) {
						dVal = atof( szTemp );
						if( nIdx < SIZE_DATA ) dDataAvg[ nIdx++ ] = dVal;
						szTemp = szData.Tokenize( _T(" "), nCurPos );
						szTemp.Trim();
						// if last item, it's the # of data
						if( szTemp == _T("") ) nNum = (int)dVal;
					}
					szTemp = pszSubj;
					// get Zs
					if( szTemp == SUBJ_GLOBAL ) {
						haNew->GetZs( line->m_pData, line->m_pData+nN+1, line->m_nN, dDataAvg, dDataAvg+nN+1, nNum,
									  &(line->m_pZGlobal), nN - HA_COL_OFFSET, TRUE, line );
					}
					else if( szTemp != SUBJ_GLOBAL ) {
						haNew->GetZs( line->m_pData, line->m_pData+nN+1, line->m_nN, dDataAvg, dDataAvg+nN+1, nNum,
									  &(line->m_pZSubject), nN - HA_COL_OFFSET, FALSE, line );
					}
				}
			}
			// Compute global Z and determine whether to update norm DB (if that setting is on)
			// write Zs to XMZ file
			if( fXMZ.Open( szXMZ, CFile::modeWrite | CFile::modeCreate ) ) {
				haNew->OutputOverall( &fXMZ, &fXMH );
				haNew->OutputByCondition( &fXMZ );
				haNew->OutputByStroke( &fXMZ );
				if( fComputeZGlobal ) {
					haNew->OutputOverall( &fXMZ, &fXMH, TRUE );
					haNew->OutputByCondition( &fXMZ, TRUE );
					haNew->OutputByStroke( &fXMZ, TRUE );
				}
				fXMZ.Close();
			}
			else {
				szMsg =  _T("Unable to open XMZ file for writing.");
				OutputAMessage( szMsg, TRUE, LOG_PROC );
				BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
				goto error;
			}
			// impairment + output of Z
			if( fOutputImpairment ) haNew->CalculateImpairment();
			if( fCompare ) {
				szTemp.Format( _T("RESULTS OF TEST: Subject %s of group %s of experiment %s: "), szSubj, szGrp, szExp );
				if( fComputeZGlobal ) {
					szMsg.Format( _T("%s Personal Z-score (%.3f) and overall Z-score (%.3f) - Averages of the absolute Z-scores."),
								  szTemp, haNew->m_dZ, haNew->m_dZGlobal );
				}
				else {
					szMsg.Format( _T("%s Personal Z-score (%.3f) - Average of the absolute Z-scores."), szTemp, haNew->m_dZ );
				}
				OutputAMessage( szMsg, TRUE, LOG_PROC );

				// get Z's/p's of individual features (duration, size, pressure, transmission?) - jerk = use average? (trying across all conditions first)
				if( fOutputImpairment ) haNew->OutputImpairmentSummary();
			}
			// if the option to determine whether Z is in line in order to update the norm DB...
			if( fCheckToUpdateDB ) {
				// check score
				// if not within range, do not update DB
				if( haNew->m_dZ > dThreshold ) fUpdateDB = FALSE;
				// otherwise update DB
				else fUpdateDB = TRUE;
			}
			// otherwise update DB
			else fUpdateDB = TRUE;
			// rewind the XMD file & skip header
			fXMD.SeekToBegin();
			fXMD.ReadString( szLine );
		}

		// Loop through XMD to process changes (if Z not out of range and set to not update)
		// If subject from XM1 is found in XMD, process, then write. Otherwise, just write (if not same subject).
		// If global subject found, update unless specified not to.
		while( fXMD.ReadString( szLine ) ) {
			// grp, subj, cond, trial, stroke & rest
			sscanf( szLine, _T("%s %s %s %lf %d %[^\n]"), pszGrp, pszSubj, pszCond, &dTrial, &nStroke, pszData );
			// if this subject is the subject/cond/stroke from the xm1, process
			line = haNew->Find( pszSubj, pszCond, nStroke, HAData::haFindBoth );
			szTemp = pszSubj;
			if( fUpdateDB && line ) {
				fUpdAvg = TRUE;
				if( szTemp != SUBJ_GLOBAL ) line->m_fUsed = TRUE;
				szData = pszData;
				// get N for this line (last col) + fill data array
				nCurPos = 0;
				nNum = 0;
				nIdx = 0;
				ZeroMemory( dDataAvg, SIZE_DATA * sizeof( double ) );
				ZeroMemory( dDataNew, SIZE_DATA * sizeof( double ) );
				szTemp = szData.Tokenize( _T(" "), nCurPos );
				szTemp.Trim();
				while( szTemp != _T("") ) {
					dVal = atof( szTemp );
					if( nIdx < SIZE_DATA ) dDataAvg[ nIdx++ ] = dVal;
					szTemp = szData.Tokenize( _T(" "), nCurPos );
					szTemp.Trim();
					// if last item, it's the # of data
					if( szTemp == _T("") ) nNum = (int)dVal;
				}

				// beginning of data to write
				szToWrite.Format( _T("1 %s %s 1 %d"), pszSubj, pszCond, nStroke );

				// CALCULATIONS
				// offset from SD to AVG
				nToAvg = nN + 1;
				// Loop from 1st data point after N and after SDs of grp/subj/cond/trial/segment til end, but excluding SD_N (so, HA_COL_OFFSET+1)
				for( int i = ( nN + 1 ); i < ( nHeaders - 6 ); i++ ) {
					haNew->NewSD( dDataAvg[i-nToAvg], dDataAvg[i], nNum, line->m_pData[i-nToAvg], line->m_pData[i], line->m_nN, dAvg, dSD, nNewN );
					if( i < SIZE_DATA ) {
						dDataNew[i-nToAvg] = dAvg;
						dDataNew[i] = dSD;
					}
				}
				// write data
				for( int i = 0; i < ( nHeaders - HA_COL_OFFSET ); i++ ) {
					// if column N, write nNewN
					if( i < SIZE_DATA ) {
						if( i == ( nN - HA_COL_OFFSET) ) szTemp.Format( _T(" %d"), nNewN );
						else szTemp.Format( _T(" %g"), dDataNew[ i ] );
						szToWrite += szTemp;
					}
				}
				// add num data
				szTemp.Format( _T(" %d"), nNewN );
				szToWrite += szTemp;
				// write
				szToWrite += _T("\n");
				fXMDTemp.WriteString( szToWrite );
			}
			// otherwise, just write it to the output file
			else {
				if( line && szTemp != SUBJ_GLOBAL ) line->m_fUsed = TRUE;
				// now write the line that we just read from XMD
				szTemp.Format( _T("%s\n"), szLine );
				fXMDTemp.WriteString( szTemp );
			}
		}
		// Write un-processed XM1 items
		posL = haNew->m_lstLines.GetHeadPosition();
		while( posL ) {
			line = (HALine*)haNew->m_lstLines.GetNext( posL );
			if( line && !(line->m_fUsed) ) {
				szTemp.Format( _T("1 %s %s 1 %d"), line->m_szSubj, line->m_szCond, line->m_nStroke );
				szToWrite = szTemp;
				for( int i = 0; i < nHeaders - HA_COL_OFFSET; i++ ) {
					szTemp.Format( _T(" %g"), line->m_pData[ i ] );
					szToWrite += szTemp;
				}
				szTemp.Format( _T(" %d\n"), line->m_nN );
				szToWrite += szTemp;
				fXMDTemp.WriteString( szToWrite );
			}
		}
		if( fUpdAvg ) {
			szMsg.Format( _T("NORM DB: Added subject %s of group %s to norm of subject %s."), szSubj, szGrp, szSubj );
			OutputAMessage( szMsg, TRUE, LOG_PROC );
			if( fComputeZGlobal ) {
				szMsg.Format( _T("NORM DB: Added subject %s of group %s to overall norm (%s)."), szSubj, szGrp, SUBJ_GLOBAL );
				OutputAMessage( szMsg, TRUE, LOG_PROC );
			}
		}

		// if returning Z data...
		if( fReturnZData ) *retData = haNew;
	}

	// NO ERRORS
	// if XMD existed, delete XMD and rename XMDt to XMD
	if( fExists ) {
		fXMD.Close();
		fXMDTemp.Close();
		REMOVE_FILE( szXMD );
		::CopyFile( szXMDTemp, szXMD, FALSE );
		REMOVE_FILE( szXMDTemp );

	}
	if( !fUpdAvg ) {
		szMsg.Format( _T("NORM DB: Subject %s of group %s was not added to the norm DB."), szSubj, szGrp );
		OutputAMessage( szMsg, TRUE, LOG_PROC );
	}
	// go to cleanup and return
	goto cleanup;

// There was an error
error:
	fRet = FALSE;

// cleanup
cleanup:
	if( haNew && !fReturnZData ) delete haNew;
	if( pszData ) delete [] pszData;
	if( dDataAvg ) delete [] dDataAvg;
	if( dDataNew ) delete [] dDataNew;
	if( fXM1.m_pStream ) fXM1.Close();
	if( fXMDTemp.m_pStream ) fXMDTemp.Close();
	if( fXMD.m_pStream ) fXMD.Close();
	if( fXMH.m_pStream ) fXMH.Close();
	if( ff.FindFile( szTMP ) ) REMOVE_FILE( szTMP );
	if( ff.FindFile( szXMDTemp ) ) REMOVE_FILE( szXMDTemp );
	if( ff.FindFile( szXMNTemp ) ) REMOVE_FILE( szXMNTemp );
	delete pAnalysis;

	return fRet;
}

//************************************
// Method:    NewAvg
// FullName:  HAData::NewAvg
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double dAvg1
// Parameter: int nN1
// Parameter: double dAvg2
// Parameter: int nN2
// Parameter: double & dAvgNew
// Parameter: int & nNNew
//************************************
void HAData::NewAvg( double dAvg1, int nN1, double dAvg2, int nN2, double& dAvgNew, int& nNNew )
{
	// new N
	nNNew = ( nN1 + nN2 );
	// new Avg
	if( nNNew > 0 ) dAvgNew = ( ( dAvg1 * nN1 ) + ( dAvg2 * nN2 ) ) / nNNew;
	else dAvgNew = 0.;
}

//************************************
// Method:    DoSums
// FullName:  HAData::DoSums
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double dAvg
// Parameter: double dSD
// Parameter: int nN
// Parameter: double & dSum
// Parameter: double & dSumSquares
//************************************
void HAData::DoSums( double dAvg, double dSD, int nN, double& dSum, double& dSumSquares )
{
	// calculate sum
	dSum = nN * dAvg;
	// calculate sum of squares
	dSumSquares = ((nN-1) * SQR(dSD)) + (nN * SQR(dAvg));
}

//************************************
// Method:    NewSD
// FullName:  HAData::NewSD
// Access:    public
// Returns:   void
// Qualifier:
// Parameter: double dAvg1
// Parameter: double dSD1
// Parameter: int nN1
// Parameter: double dAvg2
// Parameter: double dSD2
// Parameter: int nN2
// Parameter: double & dAvgNew
// Parameter: double & dSDNew
// Parameter: int & nNNew
//************************************
void HAData::NewSD( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2,
					double& dAvgNew, double& dSDNew, int &nNNew )
{
	// new average
	NewAvg( dAvg1, nN1, dAvg2, nN2, dAvgNew, nNNew );

	// NEW SD
	double dSumSq1 = 0., dSumSq2 = 0., dSum1 = 0., dSum2 = 0.;
	// sum and sum of squares of 1 & 2
	DoSums( dAvg1, dSD1, nN1, dSum1, dSumSq1 );
	DoSums( dAvg2, dSD2, nN2, dSum2, dSumSq2 );
	// calculate new SD
	if( nNNew > 1 ) {
		double dCalc = ( (dSumSq1 + dSumSq2) - (SQR(dSum1+dSum2) / nNNew) ) / (nNNew-1);
		if( dCalc > 0. ) dSDNew = sqrt( ( (dSumSq1 + dSumSq2) - (SQR(dSum1+dSum2) / nNNew) ) / (nNNew-1) );
		else dSDNew = 0.;
	}
	else dSDNew = 0.;
}

//************************************
// Method:    ZMethod1
// FullName:  HAData::ZMethod1
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dAvgThis
// Parameter: double dAvgAll
// Parameter: double dSD
//************************************
double HAData::ZMethod1( double dAvgThis, double dAvgAll, double dSD )
{
	if( dSD != 0. ) return ( ( dAvgThis - dAvgAll ) / dSD );
	else return 0.;
}

//************************************
// Method:    ZMethod2
// FullName:  HAData::ZMethod2
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dAvg1
// Parameter: double dSD1
// Parameter: int nN1
// Parameter: double dAvg2
// Parameter: double dSD2
// Parameter: int nN2
//
// Method 2 Exploiting we have 2 averages with 2 SDs
// We then can use Student's t 2-sample test for unpaired data 
// (with very unequal nr of data, but assuming equal variances)
// From: http://en.wikipedia.org/wiki/Student's_t-test
// While the SD of the normal distribution = 1 it is sqrt(df/df-2) for t(df).
// Si dividing t by this number will bring t to SD=1 
//************************************
double HAData::ZMethod2( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2 )
{
	double dAvgDiff = 0., dSDEst = 0., dInv = 0., dT = 0., dDoF = 0., dCalc = 0., dZ = 0.;
	if( ( nN1 > 0 ) && ( nN2 > 0 ) ) {
		// degrees of freedom
		dDoF = nN1 + nN2 - 2;
		if( dDoF <= 0. ) return 0.;
		// difference of averages
		dAvgDiff = dAvg1 - dAvg2;
		// estimator of the SD of 2 samples
		dCalc = ((nN1-1)*SQR(dSD1) + (nN2-1)*SQR(dSD2)) / dDoF;
		if( dCalc > 0. ) dSDEst = sqrt( dCalc );
		// Ninverse
		dInv = sqrt( 1.0/nN1 + 1.0/nN2 );
		// student's t-statistic
		if( ( dSDEst != 0. ) && ( dInv != 0. ) ) dT = dAvgDiff / ( dSDEst * dInv );
		else dT = 0.;
		// Z
		if( dDoF > 2 ) dZ = dT * sqrt( ( dDoF - 2 ) / dDoF );
		else dZ = 0.;
	}

	return dZ;
}

//************************************
// Method:    ZMethod3
// FullName:  HAData::ZMethod3
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dAvg1
// Parameter: double dSD1
// Parameter: int nN1
// Parameter: double dAvg2
// Parameter: double dSD2
// Parameter: int nN2
//
// Method 3. Same as Method 2 but not assuming equal variances
// To compare 2 averages, each with their N and SD we can use Student's t-test
// for 2 independent samples, with unequal sizes and unequal variances 
// From: http://en.wikipedia.org/wiki/Student's_t-test
//************************************
double HAData::ZMethod3( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2 )
{
	double dAvgDiff = 0., dSDEst = 0., dT = 0., dDoF = 0., dCalc = 0., dZ = 0.;
	if( ( nN1 > 0 ) && ( nN2 > 0 ) ) {
		// difference of averages
		dAvgDiff = dAvg1 - dAvg2;
		// unbiased estimator of the SD of 2 samples
		dCalc = SQR(dSD1)/nN1 + SQR(dSD2)/nN2;
		if( dCalc > 0. ) dSDEst = sqrt( dCalc );
		// student's t-statistic
		if( dSDEst > 0. ) dT = dAvgDiff / dSDEst;
		// Welch-Satterthwaite equation for effective degrees of freedom
		if( ( nN1 > 1 ) && ( nN2 > 1 ) ) {
			dDoF = SQR(SQR(dSD1)/nN1 + SQR(dSD2)/nN2) / ( SQR(SQR(dSD1)/nN1) / (nN1-1) + SQR(SQR(dSD2)/nN2) / (nN2-1) );
		}
		// Normal approximation as 1/sqrt((DoF-2)/DoF) is the SD of t(DoF).
		if( dDoF > 0. ) {
			dCalc = ( dDoF - 2 ) / dDoF;
			if( dCalc > 0. ) dZ = dT * sqrt( dCalc );
		}
	}

	return dZ;
}

//************************************
// Method:    CalcZ
// FullName:  HAData::CalcZ
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dAvg1
// Parameter: double dSD1
// Parameter: int nN1
// Parameter: double dAvg2
// Parameter: double dSD2
// Parameter: int nN2
//************************************
double HAData::CalcZ( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2 )
{
	return ZMethod2( dAvg1, dSD1, nN1, dAvg2, dSD2, nN2 );
}

//************************************
// Method:    GetZs
// FullName:  HAData::GetZs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double * pAvg1
// Parameter: double * pSD1
// Parameter: int nN1
// Parameter: double * pAvg2
// Parameter: double * pSD2
// Parameter: int nN2
// Parameter: double * * pZs
// Parameter: int nNumData
// Parameter: BOOL fGlobal
// Parameter: HALine pLine
//************************************
void HAData::GetZs( double* pAvg1, double* pSD1, int nN1, double* pAvg2, double* pSD2, int nN2,
				    double** pZs, int nNumData, BOOL fGlobal, HALine* pLine )
{
	// verify pointers & num data
	if( !pAvg1 || !pSD1 || !pAvg2 || !pSD2 || !pZs || !(*pZs) || ( nNumData <= 0 ) ) return;
	// Get Z of each column
	double dVal = 0.;
	for( int i = 0; i < nNumData; i++ ) {
		(*pZs)[i] = CalcZ( pAvg1[i], pSD1[i], nN1, pAvg2[i], pSD2[i], nN2 );
		if( pLine ) pLine->m_pDataAvg[i] = pAvg2[i];
	}
	// Update internal count of comparison items
	if( fGlobal && ( nN2 > m_nNumCompGlobal ) ) m_nNumCompGlobal = nN2;
	else if ( !fGlobal && ( nN2 > m_nNumComp ) ) m_nNumComp = nN2;
}

//************************************
// Method:    CalculateLineZ
// FullName:  HAData::CalculateLineZ
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double * pZs
// Parameter: int nNumData
//************************************
double HAData::CalculateLineZ( double* pZs, int nNumData )
{
	// verify pointer & num data
	if( !pZs || ( nNumData <= 0 ) ) return 0.;
	// actually, we're trying the average now
	double dCalc = 0.;
	int nCount = 0;
	for( int i = 0; i < nNumData; i++ ) {
//		if( pZs[i] != 0. ) {
			dCalc += abs( pZs[i] );
//			dCalc += pZs[i];
			nCount++;
//		}
	}
	// return the average
	return ( nCount > 0. ) ? ( dCalc / nCount ) : 0.;
}

//************************************
// Method:    GetFeatureIndex
// FullName:  HAData::GetFeatureIndex
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: CString szFeature
// Parameter: BOOL fNotAvg
//************************************
int HAData::GetFeatureIndex( CString szFeature, BOOL fNotAvg )
{
	CString szHeader, szCompare = fNotAvg ? _T("") : _T("Av_");
	szCompare += szFeature;
	szCompare.MakeLower();
	int nInc = -1, nIdx = -1;
	POSITION pos = m_lstHeaders.GetHeadPosition();
	while( pos ) {
		nInc++;
		szHeader = m_lstHeaders.GetNext( pos );
		szHeader.MakeLower();
		if( szCompare == szHeader ) {
			nIdx = nInc;
			break;
		}
	}
	return nIdx;
}

//************************************
// Method:    CalculateFeatureZandAvg
// FullName:  HAData::CalculateFeatureZandAvg
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: int nIdx
// Parameter: double & dAvg
// Parameter: CString szCond
// Parameter: BOOL fGlobal
//************************************
double HAData::CalculateFeatureZandAvg( int nIdx, double& dAvg, CString szCond, BOOL fGlobal )
{
	// verify index & num data
	if( ( nIdx < 0 ) || ( nIdx >= m_lstHeaders.GetCount() ) ) {
		OutputAMessage( _T("ERROR: CalculateFeatureZandAvg: Index out of range."), TRUE, LOG_PROC );
		return 0.;
	}

	// loop through each line of data
	double dCalc = 0., dSum = 0.;
	int nCount = 0, nAvgCount = 0;
	POSITION pos = m_lstLines.GetHeadPosition();
	while( pos ) {
		HALine* line = (HALine*)m_lstLines.GetNext( pos );
		if( line ) {
			// if we're checking for condition...
			if( ( szCond != _T("") ) && ( szCond != line->m_szCond ) ) continue;
			// update running average
			if( fGlobal ) {
//				if( line->m_pZGlobal[nIdx] != 0. ) {
//					dCalc += abs( line->m_pZGlobal[nIdx] );
					dCalc += line->m_pZGlobal[nIdx];
					nAvgCount++;
//				}
			}
			else {
//				if( line->m_pZSubject[nIdx] != 0. ) {
//					dCalc += abs( line->m_pZSubject[nIdx] );
					dCalc += line->m_pZSubject[nIdx];
					nAvgCount++;
//				}
			}
			// update running sum of value + count
//			dSum += ( line->m_nN * abs( line->m_pData[ nIdx ] ) );
			dSum += ( line->m_nN * line->m_pData[ nIdx ] );
			nCount += line->m_nN;
		}
	}
	// update average
	if( nCount > 0 ) dAvg = dSum / nCount;
	else dAvg = 0.;
	// returning average of Zs
	return ( nAvgCount > 0 ) ? ( dCalc / nAvgCount ) : 0.;
}

//************************************
// Method:    CalculateFeatureZandAvg
// FullName:  HAData::CalculateFeatureZandAvg
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: CString szFeature
// Parameter: double & dAvg
// Parameter: CString szCond
// Parameter: BOOL fGlobal
//************************************
double HAData::CalculateFeatureZandAvg( CString szFeature, double& dAvg, CString szCond, BOOL fGlobal )
{
	return CalculateFeatureZandAvg( GetFeatureIndex( szFeature ), dAvg, szCond, fGlobal );
}

//************************************
// Method:    CalculateImpairment
// FullName:  HAData::CalculateImpairment
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void HAData::CalculateImpairment()
{
	// maximums - TODO: Configurable?
	m_dMaxSize = 99.;
	m_dMaxDuration = 99.;
	m_dMaxPressure = 1000.;
	m_dMaxJerk = 50.;

	m_dAvgSize = 0., m_dAvgDur = 0., m_dAvgPress = 0., m_dAvgJerk = 0.;
	m_dZSize = CalculateFeatureZandAvg( _T("VerticalSize"), m_dAvgSize );
	m_dZDur = CalculateFeatureZandAvg( _T("Duration"), m_dAvgDur );
	m_dZPress = CalculateFeatureZandAvg( _T("AveragePenPressure"), m_dAvgPress );
	m_dZJerk = CalculateFeatureZandAvg( _T("NormalizedJerk"), m_dAvgJerk );
}

//************************************
// Method:    Z2P
// FullName:  HAData::Z2P
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dZscore
//************************************
double HAData::Z2P( double dZscore )
{
	double dPscore = 0.1, dZ = abs( dZscore );
	if( dZ > 5.7 ) dPscore = 0.00000001;
	else if( dZ > 5.2 ) dPscore = 0.0000001;
	else if( dZ > 4.7 ) dPscore = 0.000001;
	else if( dZ > 4.2 ) dPscore = 0.00001;
	else if( dZ > 3.71 ) dPscore = 0.0001;
	else if( dZ > 3.090 ) dPscore = 0.001;
	else if( dZ > 3.090 ) dPscore = 0.01;
	else if( dZ > 1.645 ) dPscore = 0.05;
	if( dZscore < 0. ) dPscore *= -1.0;
	return dPscore;
}

//************************************
// Method:    GenHeaders
// FullName:  HAData::GenHeaders
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szHeaders
// Parameter: BOOL fIncludeSD
// Parameter: BOOL fSkipInitial
// Parameter: BOOL fRemovePrefix
// Parameter: BOOL fAddZPrefix
//************************************
void HAData::GenHeaders( CString& szHeaders, BOOL fIncludeSD, BOOL fSkipInitial,
						 BOOL fRemovePrefix, BOOL fAddZPrefix )
{
	CString szTemp, szHdr;
	if( !fSkipInitial ) szHeaders = _T("Av_Group Av_Subject Av_Condition Av_Trial Av_Segment");
	if( fAddZPrefix ) fRemovePrefix = TRUE;
	POSITION pos = m_lstHeaders.GetHeadPosition();
	int nDo = ( m_lstHeaders.GetCount() - HA_COL_OFFSET - 2 /* each N */ ) / 2;
	int nItem = 0, nPos = 0;
	while( pos ) {
		szHdr = m_lstHeaders.GetNext( pos );
		szHdr.Trim();
		if( szHdr != _T("") ) {
			nItem++;
			if( !fIncludeSD && ( nItem > nDo ) ) break;
			if( fRemovePrefix ) {
				nPos = szHdr.Find( _T("_") );
				if( nPos != -1 ) {
					szTemp = szHdr.Mid( nPos + 1 );
					szHdr = szTemp;
				}
			}
			if( fAddZPrefix ) {
				szTemp.Format( _T("z_%s"), szHdr );
				szHdr = szTemp;
			}
			szHeaders += _T(" ");
			szHeaders += szHdr;
		}
	}
}

//************************************
// Method:    OutputOverall
// FullName:  HAData::OutputOverall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CStdioFile * f
// Parameter: CStdioFile * fH
// Parameter: BOOL fGlobal
//************************************
void HAData::OutputOverall( CStdioFile* f, CStdioFile* fH, BOOL fGlobal )
{
	HALine* line = (HALine*)m_lstLines.GetHead();
	if( !line ) return;
	if( !f->m_pStream ) f = NULL;
	if( !fH->m_pStream ) fH = NULL;

	CString szToWrite, szHeaders, szTemp, szData;
	short nTrials = 0, nGood = 0, nBad = 0, nExpTrials = 0;
	BOOL fInSumm = FALSE;
	COleDateTime dtStart, dtEnd, dtProc;
	COleDateTimeSpan dtsDur;
	double dDur = 0.;

	// get list of conditions
	CStringList lstCond;
	POSITION pos = m_lstLines.GetHeadPosition();
	while( pos ) {
		line = (HALine*)m_lstLines.GetNext( pos );
		if( line && !lstCond.Find( line->m_szCond ) ) lstCond.AddTail( line->m_szCond );
	}

	// output headers & description
	int nHeaders = ( m_lstHeaders.GetCount() - HA_COL_OFFSET - 2 ) / 2;
	GenHeaders( szTemp, FALSE, TRUE, TRUE, TRUE );
	szHeaders = _T("Group Subject Condition Segment NORM N z_All");
	szHeaders += szTemp;
	szToWrite.Format( _T("%s\n"), szHeaders );
	if( f ) f->WriteString( szToWrite );
	// for history file
	if( fH ) {
		fH->SeekToBegin();
		if( fH->SeekToEnd() == 0 ) {
			szTemp.Format( _T("TestDate TestDuration %s\n"), szHeaders );
			fH->WriteString( szTemp );
		}
	}

	// loop through data and calculate compressed Z
	double dAvg = 0., dZ = 0., dCalc = 0., dOverall = 0.;
	// first part of each line
	if( fGlobal ) szToWrite.Format( _T("%s %s . . --- %d"), line->m_szGrp, line->m_szSubj, m_nNumCompGlobal );
	else szToWrite.Format( _T("%s %s . . %s %d"), line->m_szGrp, line->m_szSubj, line->m_szSubj, m_nNumComp );
	// data
	for( int i = 0; i < nHeaders; i++ )	{
		dZ = CalculateFeatureZandAvg( i, dAvg, _T(""), fGlobal );
		dCalc += abs( dZ );
		szTemp.Format( _T(" %.3f"), dZ );
		szData += szTemp;
	}
	// overall
	dOverall = dCalc / nHeaders;
	if( fGlobal ) m_dZGlobal = dOverall;
	else m_dZ = dOverall;
	szTemp.Format( _T(" %.3f"), dOverall );
	szToWrite += szTemp;
	szToWrite += szData;
	// end of line
	szToWrite += _T("\n");
	// write to file
	if( f ) f->WriteString( szToWrite );

	// to history file
	Subjects::GetTrialInfo( m_szExp, line->m_szGrp, line->m_szSubj, &lstCond, nTrials, nGood, nBad,
							dtStart, dtEnd, dtProc, fInSumm, nExpTrials );
	dtsDur = dtEnd - dtStart;
	dDur = dtsDur.GetTotalMinutes();
	dDur += ( 1. * dtsDur.GetSeconds() / 60. );
	szTemp.Format( _T("%s %.3f %s"), dtEnd.Format( _T("%Y%m%d%H%M%S") ), dDur, szToWrite );
	if( fH ) fH->WriteString( szTemp );
}

//************************************
// Method:    OutputByCondition
// FullName:  HAData::OutputByCondition
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CStdioFile * f
// Parameter: BOOL fGlobal
//************************************
void HAData::OutputByCondition( CStdioFile* f, BOOL fGlobal )
{
	if( !f ) return;
	HALine* line = (HALine*)m_lstLines.GetHead();
	if( !line ) return;

	CString szToWrite, szCond, szHeaders, szTemp, szData;

	// output headers
	GenHeaders( szTemp, FALSE, TRUE, TRUE );
	szHeaders = _T("Group Subject Condition Segment NORM N z_All");
	szHeaders += szTemp;
	szToWrite.Format( _T("%s\n"), szHeaders );
	f->WriteString( szToWrite );

	// get list of conditions
	CStringList lstCond;
	POSITION pos = m_lstLines.GetHeadPosition();
	while( pos ) {
		line = (HALine*)m_lstLines.GetNext( pos );
		if( line && !lstCond.Find( line->m_szCond ) ) lstCond.AddTail( line->m_szCond );
	}

	// compute avg + Z for each condition
	double dAvg = 0., dZ = 0., dCalc = 0., dOverall = 0.;
	int nHeaders = ( m_lstHeaders.GetCount() - HA_COL_OFFSET - 2 ) / 2;
	pos = lstCond.GetHeadPosition();
	while( pos ) {
		// this condition
		szCond = lstCond.GetNext( pos );
		// first part of each line
		if( fGlobal ) szToWrite.Format( _T("%s %s %s . --- %d"), line->m_szGrp, line->m_szSubj, szCond, m_nNumCompGlobal );
		else szToWrite.Format( _T("%s %s %s . %s %d"), line->m_szGrp, line->m_szSubj, szCond, line->m_szSubj, m_nNumComp );
		// data
		szData = _T("");
		dCalc = 0.; dOverall = 0.;
		for( int i = 0; i < nHeaders; i++ )	{
			dZ = CalculateFeatureZandAvg( i, dAvg, szCond, fGlobal );
			dCalc += abs( dZ );
			szTemp.Format( _T(" %.3f"), dZ );
			szData += szTemp;
		}
		// overall
		dOverall = dCalc / nHeaders;
		szTemp.Format( _T(" %.3f"), dOverall );
		szToWrite += szTemp;
		szToWrite += szData;
		// end of line
		szToWrite += _T("\n");
		// write to file
		f->WriteString( szToWrite );
	}
}

//************************************
// Method:    OutputByStroke
// FullName:  HAData::OutputByStroke
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CStdioFile * f
// Parameter: BOOL fGlobal
//************************************
void HAData::OutputByStroke( CStdioFile* f, BOOL fGlobal )
{
	if( !f ) return;
	HALine* line = (HALine*)m_lstLines.GetHead();
	if( !line ) return;

	CString szToWrite, szCond, szHeaders, szTemp, szData;

	// output headers
	GenHeaders( szTemp, FALSE, TRUE, TRUE );
	szHeaders = _T("Group Subject Condition Segment NORM N z_All");
	szHeaders += szTemp;
	szToWrite.Format( _T("%s\n"), szHeaders );
	f->WriteString( szToWrite );

	// loop through z table and add to file
	int nHeaders = ( m_lstHeaders.GetCount() - HA_COL_OFFSET - 2 ) / 2;
	POSITION pos = m_lstLines.GetHeadPosition();
	double dOverall = 0.;
	while( pos ) {
		line = (HALine*)m_lstLines.GetNext( pos );
		if( line ) {
			// overall per stroke
			dOverall = CalculateLineZ( fGlobal ? line->m_pZGlobal : line->m_pZSubject, nHeaders );
			// first part of each line
			if( fGlobal ) szToWrite.Format( _T("%s %s %s %d --- %d %.3f"), line->m_szGrp, line->m_szSubj, line->m_szCond,
											line->m_nStroke, m_nNumCompGlobal, dOverall );
			else szToWrite.Format( _T("%s %s %s %d %s %d %.3f"), line->m_szGrp, line->m_szSubj, line->m_szCond, line->m_nStroke,
											line->m_szSubj, m_nNumComp, dOverall );
			// data
			szData = _T("");
			if( fGlobal ) {
				for( int i = 0; i < nHeaders; i++ )	{
					szTemp.Format( _T(" %.3f"), line->m_pZGlobal[ i ] );
					szData += szTemp;
				}
			}
			else {
				for( int i = 0; i < nHeaders; i++ )	{
					szTemp.Format( _T(" %.3f"), line->m_pZSubject[ i ] );
					szData += szTemp;
				}
			}
			szToWrite += szData;
			// end of line
			szToWrite += _T("\n");
			// write to file
			f->WriteString( szToWrite );
		}
	}
}

//************************************
// Method:    OutputImpairmentSummary
// FullName:  HAData::OutputImpairmentSummary
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void HAData::OutputImpairmentSummary()
{
	// indices for the impairment features
	int nSize = GetFeatureIndex( _T("VerticalSize") );
	int nDur = GetFeatureIndex( _T("Duration") );
	int nPress = GetFeatureIndex( _T("AveragePenPressure") );
	int nJerk = GetFeatureIndex( _T("NormalizedJerk") );

	// calculate averages of the above features
	HALine* pLine = NULL;
	int nItems = 0;
	double dAvgSize = 0., dAvgDur = 0., dAvgPress = 0., dAvgJerk = 0.;
	POSITION pos = m_lstLines.GetHeadPosition();
	while ( pos ) {
		pLine = (HALine*)m_lstLines.GetNext( pos );
		if( pLine ) {
			nItems++;
			dAvgSize += pLine->m_pDataAvg[nSize];
			dAvgDur += pLine->m_pDataAvg[nDur];
			dAvgPress += pLine->m_pDataAvg[nPress];
			dAvgJerk += pLine->m_pDataAvg[nJerk];
		}
	}
	if( nItems > 0 ) {
		dAvgSize = ( dAvgSize / nItems );
		dAvgDur = ( dAvgDur / nItems );
		dAvgPress = ( dAvgPress / nItems );
		dAvgJerk = ( dAvgJerk / nItems );
	}

	// output
	CString szMsg;
	OutputAMessage( _T("---------OUTPUT OF IMPAIRMENT TEST---------"), TRUE, LOG_PROC );
	szMsg.Format( _T("Vertical Size: Avg=%.3f, HistAvg=%.3f, Z-Score=%.3f"), m_dAvgSize, dAvgSize, m_dZSize );
	OutputAMessage( szMsg, TRUE, LOG_PROC );
	szMsg.Format( _T("Duration: Avg=%.3f, HistAvg=%.3f, Z-Score=%.3f"), m_dAvgDur, dAvgDur, m_dZDur );
	OutputAMessage( szMsg, TRUE, LOG_PROC );
	szMsg.Format( _T("Pen Pressure: Avg=%.3f, HistAvg=%.3f, Z-Score=%.3f"), m_dAvgPress, dAvgPress, m_dZPress );
	OutputAMessage( szMsg, TRUE, LOG_PROC );
	szMsg.Format( _T("Normalized Jerk: Avg=%.3f (MAX=%.3f), HistAvg=%.3f, Z-Score=%.3f"), m_dAvgJerk, m_dMaxJerk, dAvgJerk, m_dZJerk );
	OutputAMessage( szMsg, TRUE, LOG_PROC );
	OutputAMessage( _T("----------END OF IMPAIRMENT TEST-----------"), TRUE, LOG_PROC );
}

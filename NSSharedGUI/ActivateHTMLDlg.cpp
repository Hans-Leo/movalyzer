// HtmlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ActivateHTMLDlg.h"
#include "..\NSShared\HyperLink.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// ActivateHTMLDlg dialog

ActivateHTMLDlg* _pActivateHTMLDlg = NULL;

IMPLEMENT_DYNAMIC(ActivateHTMLDlg, CDHtmlDialog)

BEGIN_MESSAGE_MAP(ActivateHTMLDlg, CDHtmlDialog)
END_MESSAGE_MAP()

BEGIN_DHTML_EVENT_MAP(ActivateHTMLDlg)
	DHTML_EVENT_ONCLICK(_T("prodScripta"), OnItemChecked)
	DHTML_EVENT_ONCLICK(_T("prodMova"), OnItemChecked)
	DHTML_EVENT_ONCLICK(_T("prodUpgrade"), OnItemChecked)
	DHTML_EVENT_ONCLICK(_T("prodRx"), OnItemChecked)
	DHTML_EVENT_ONCLICK(_T("prodGripa"), OnItemChecked)
	DHTML_EVENT_ONCLICK(_T("prodCS"), OnItemChecked)
	DHTML_EVENT_ONCLICK(_T("bnPurchase"), OnClickPurchase)
	DHTML_EVENT_ONCLICK(_T("bnActivate"), OnClickActivate)
	DHTML_EVENT_ONCLICK(_T("bnCancel"), OnClickCancel)
END_DHTML_EVENT_MAP()

ActivateHTMLDlg::ActivateHTMLDlg( CWnd* pParent, BOOL fModeless )
	: CDHtmlDialog( ActivateHTMLDlg::IDD, ActivateHTMLDlg::IDH, pParent ),
	  m_fModeless( fModeless ), m_nMode( ACT_MODE_PURCHASE ), m_fScript( FALSE ),
	  m_fOpti( FALSE ), m_fGrip( FALSE ), m_fCS( FALSE ), m_fPurchase( FALSE ),
	  m_fRx( FALSE ), m_fUpgrade( FALSE ), m_fActivate( FALSE ), m_nItem( 0 ),
	  m_nParam( 0 ), m_fUnlock( FALSE ), m_fSuccess( FALSE )
{
	if( m_fModeless && Create( ActivateHTMLDlg::IDD, pParent ) ) {
		ShowWindow( SW_SHOW );
		_pActivateHTMLDlg = this;
	}
}

void ActivateHTMLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDHtmlDialog::DoDataExchange(pDX);
}

ActivateHTMLDlg* ActivateHTMLDlg::GetActivateHTMLDlg()
{
	return _pActivateHTMLDlg;
}

// ActivateHTMLDlg message handlers

BOOL ActivateHTMLDlg::OnInitDialog()
{
	CDHtmlDialog::OnInitDialog();

	// update product check boxes
	if( ( m_nMode != ACT_MODE_COPYPROTECT ) && ( m_nMode != ACT_MODE_RECOVERY ) ) {
		NSSecurity* pSecurity = ::GetSecurity();
		if( pSecurity ) m_acts.m_fWasOpti = pSecurity->WasOldMova();

		// by default, upgrade is disabled
		m_acts.m_upgrade.m_fEnabled = FALSE;

		if( ::IsOA( FALSE ) ) {
			m_acts.m_opti.m_fActivated = TRUE;
			m_acts.m_opti.m_fChecked = TRUE;
			m_acts.m_opti.m_fEnabled = FALSE;
			m_acts.m_upgrade.m_fActivated = TRUE;
			m_acts.m_upgrade.m_fChecked = m_acts.m_fWasOpti;
			m_acts.m_upgrade.m_fEnabled = FALSE;
			m_acts.m_scripta.m_fActivated = TRUE;
			m_acts.m_scripta.m_fChecked = TRUE;
			m_acts.m_scripta.m_fEnabled = FALSE;
			m_acts.m_rx.m_fActivated = TRUE;
			m_acts.m_rx.m_fChecked = TRUE;
			m_acts.m_rx.m_fEnabled = FALSE;
		}
		if( ::IsSA( FALSE ) ) {
			m_acts.m_scripta.m_fActivated = TRUE;
			m_acts.m_scripta.m_fChecked = TRUE;
			m_acts.m_scripta.m_fEnabled = FALSE;
		}
		if( ::IsRxA( FALSE ) ) {
			m_acts.m_rx.m_fActivated = TRUE;
			m_acts.m_rx.m_fChecked = TRUE;
			m_acts.m_rx.m_fEnabled = FALSE;
		}
		if( ::IsGA( FALSE ) ) {
			m_acts.m_gripa.m_fActivated = TRUE;
			m_acts.m_gripa.m_fChecked = TRUE;
			m_acts.m_gripa.m_fEnabled = FALSE;
		}
		if( ::IsCS( FALSE ) && ( ::IsSA( FALSE ) || ::IsOA( FALSE ) || ::IsGA( FALSE ) ) ) {
			m_acts.m_cs.m_fActivated = TRUE;
			m_acts.m_cs.m_fChecked = TRUE;
			m_acts.m_cs.m_fEnabled = FALSE;
		}

		m_fOpti = m_acts.m_opti.m_fChecked;
		m_fUpgrade = m_acts.m_upgrade.m_fChecked;
		m_fScript = m_acts.m_scripta.m_fChecked;
		m_fRx = m_acts.m_rx.m_fChecked;
		m_fGrip = m_acts.m_gripa.m_fChecked;
		m_fCS = m_acts.m_cs.m_fChecked;
	}

	int nX = 630, nY = 600;
	CString szTitle = _T("Activate a Product");
	switch( m_nMode ) {
		case ACT_MODE_PURCHASE: nY = 530; break;
		case ACT_MODE_RECOVERY: nY = 440; szTitle = _T("Recover From a License Error"); break;
		case ACT_MODE_COPYPROTECT: nY = 450; m_fUnlock = TRUE; szTitle = _T("Activate the Trial"); break;
		case ACT_MODE_EXPIRED: szTitle = _T("Trial Expiration"); break;
	}
	SetWindowText( szTitle );

	SetWindowPos( NULL, 0, 0, nX, nY, SWP_NOREPOSITION );
	CenterWindow();

	return TRUE;  // return TRUE  unless you set the focus to a control}
}

void ActivateHTMLDlg::SetAttr( CString szItem, CString szAttr, CString szVal, BOOL fRemove )
{
	IHTMLElement* pCB = NULL;
	BSTR bstrAttr = szAttr.AllocSysString();

	if( GetElement( szItem, &pCB ) == S_OK && pCB ) {
		if( !fRemove ) {
			VARIANT v;
			V_VT( &v ) = VT_BSTR;
			BSTR bstrVal = szVal.AllocSysString();
			V_BSTR( &v ) = bstrVal;
			pCB->setAttribute( bstrAttr, v );
		}
		else {
			if( szAttr != _T("checked") ) {
				VARIANT_BOOL b;
				pCB->removeAttribute( bstrAttr, 0, &b );
			}
			else {
				VARIANT v;
				V_VT( &v ) = VT_BOOL;
				V_BOOL( &v ) = VARIANT_FALSE;
				pCB->setAttribute( bstrAttr, v );
			}
		}
		pCB->Release();
	}
}

void ActivateHTMLDlg::EnableDisable()
{
	SetAttr( _T("prodScripta"), _T("checked"), _T("checked"), !m_fScript );
	SetAttr( _T("prodScripta"), _T("disabled"), _T("disabled"), m_acts.m_scripta.m_fEnabled );

	SetAttr( _T("prodMova"), _T("checked"), _T("checked"), !m_fOpti );
	SetAttr( _T("prodMova"), _T("disabled"), _T("disabled"), m_acts.m_opti.m_fEnabled );

	SetAttr( _T("prodRx"), _T("checked"), _T("checked"), !m_fRx );
	SetAttr( _T("prodRx"), _T("disabled"), _T("disabled"), m_acts.m_rx.m_fEnabled );

	SetAttr( _T("prodGripa"), _T("checked"), _T("checked"), !m_fGrip );
	SetAttr( _T("prodGripa"), _T("disabled"), _T("disabled"), m_acts.m_gripa.m_fEnabled );

	SetAttr( _T("prodCS"), _T("checked"), _T("checked"), !m_fCS );
	SetAttr( _T("prodCS"), _T("disabled"), _T("disabled"), m_acts.m_cs.m_fEnabled );

	SetAttr( _T("prodUpgrade"), _T("disabled"), _T("disabled"), m_acts.m_upgrade.m_fEnabled );

	SetAttr( _T("bnPurchase"), _T("disabled"), _T("disabled"), m_fPurchase );
	SetAttr( _T("bnActivate"), _T("disabled"), _T("disabled"), m_fActivate );
}

void ActivateHTMLDlg::OnDocumentComplete( LPDISPATCH pDisp, LPCTSTR szUrl )
{
	// update messages
	if( m_nMode == ACT_MODE_PURCHASE ) {
		IHTMLElement* pCB = NULL;
		CString szClass = _T("hidden");
		BSTR bstr = szClass.AllocSysString();
		if( GetElement( _T("sm"), &pCB ) == S_OK && pCB ) {
			pCB->put_className( bstr );
			pCB->Release();
		}
	}
	else if( m_nMode == ACT_MODE_COPYPROTECT ) {
		IHTMLElement* pCB = NULL;
		CString szClass = _T("hidden");
		BSTR bstr = szClass.AllocSysString();
		if( GetElement( _T("prods"), &pCB ) == S_OK && pCB ) {
			pCB->put_className( bstr );
			pCB->Release();
		}
		if( GetElement( _T("sm_title"), &pCB ) == S_OK && pCB ) {
			szClass = _T("You Must Activate This Trial");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("sm_msg"), &pCB ) == S_OK && pCB ) {
			szClass = _T("In order to activate the trial version of MovAlyzeR&reg;, you need a trial activation code.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("arcTitle"), &pCB ) == S_OK && pCB ) {
			szClass = _T("Step 1: Submit Trial Activation Request Code.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("act_arc_msg"), &pCB ) == S_OK && pCB ) {
			szClass.Format( _T("The <strong>Request Code</strong> can be <a href='mailto:sales@neuroscriptsoftware.com?subject=Activate Trial: %s.'>emailed to us</a> or entered online by clicking the 'Request' button."), m_szRequestCode );
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		SetAttr( _T("bnPurchase"), _T("value"), _T("Request") );
		UpdateCode();
		if( GetElement( _T("acTitle"), &pCB ) == S_OK && pCB ) {
			szClass = _T("Step 2: Activate Trial.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("act_ac_msg"), &pCB ) == S_OK && pCB ) {
			szClass = _T("Upon reception of the trial <strong>Activation Code</strong>, enter it below to complete the activation.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		m_fActivate = TRUE;
		SetAttr( _T("bnActivate"), _T("disabled"), _T("disabled"), m_fActivate );
	}
	else if( m_nMode == ACT_MODE_RECOVERY ) {
		IHTMLElement* pCB = NULL;
		CString szClass = _T("hidden");
		BSTR bstr = szClass.AllocSysString();
		if( GetElement( _T("prods"), &pCB ) == S_OK && pCB ) {
			pCB->put_className( bstr );
			pCB->Release();
		}
		if( GetElement( _T("sm_title"), &pCB ) == S_OK && pCB ) {
			szClass = _T("License Error");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("sm_msg"), &pCB ) == S_OK && pCB ) {
			szClass = ::LoadAndDecode( IDS_LICENSE_ERROR );
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("arcTitle"), &pCB ) == S_OK && pCB ) {
			szClass = _T("Step 1: Submit License Recovery Request Code.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("act_arc_msg"), &pCB ) == S_OK && pCB ) {
			szClass.Format( _T("The <strong>Request Code</strong> can be <a href='mailto:sales@neuroscriptsoftware.com?subject=Recovery Request: %s.'>emailed to us</a> or faxed to +1-480-350-9199."), m_szRequestCode );
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		SetAttr( _T("bnPurchase"), _T("value"), _T("Request") );
		UpdateCode();
		if( GetElement( _T("acTitle"), &pCB ) == S_OK && pCB ) {
			szClass = _T("Step 2: Recover License.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		if( GetElement( _T("act_ac_msg"), &pCB ) == S_OK && pCB ) {
			szClass = _T("Upon reception of the <strong>Activation Code</strong>, enter it below to complete the license recovery.");
			bstr = szClass.AllocSysString();
			pCB->put_innerHTML( bstr );
			pCB->Release();
		}
		m_fActivate = TRUE;
		if( GetElement( _T("bnPurchase"), &pCB ) == S_OK && pCB ) {
			szClass = _T("hidden");
			bstr = szClass.AllocSysString();
			pCB->put_className( bstr );
			pCB->Release();
		}
		SetAttr( _T("bnActivate"), _T("disabled"), _T("disabled"), m_fActivate );
	}

	// update product check boxes
	if( ( m_nMode != ACT_MODE_COPYPROTECT ) && ( m_nMode != ACT_MODE_RECOVERY ) ) EnableDisable();
}

void ActivateHTMLDlg::OnCancel()
{
	if( m_fModeless ) DestroyWindow();
	else CDHtmlDialog::OnCancel();
}

void ActivateHTMLDlg::PostNcDestroy()
{
	// update registry for check box of show or not at startup
	if( !m_fModeless ) CDHtmlDialog::PostNcDestroy();
	else delete this;
	_pActivateHTMLDlg = NULL;
}

BOOL ActivateHTMLDlg::DoActivate( BOOL fActivate )
{
	BOOL fRet = FALSE;
	NSSecurity* pSecurity = ::GetSecurity();
	if( !pSecurity ) {
		BCGPMessageBox( _T("Security object not available.") );
		return FALSE;
	}
	if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
		m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated &&
		m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: MovAlyzeR, GripAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->AOGCS( this, fActivate );
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: MovAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->AOCS( this, fActivate );
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
			 m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: MovAlyzeR, GripAlyzeR" ), TRUE );
		fRet = pSecurity->AGO( this, fActivate );
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
			 m_acts.m_upgrade.m_fChecked && !m_acts.m_upgrade.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: MovAlyzeR Upgrade" ), TRUE );
		fRet = pSecurity->AOU( this, fActivate );
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: MovAlyzeR" ), TRUE );
		fRet = pSecurity->AO( FALSE, this, fActivate );
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated && 
			 m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated &&
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: ScriptAlyzeR, GripAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->ASGCS( this, fActivate );
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated && 
			 m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: ScriptAlyzeR, GripAlyzeR" ), TRUE );
		fRet = pSecurity->ASG( this, fActivate );
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated && 
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: ScriptAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->ASCS( this, fActivate );
	}
	else if( m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated && 
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: GripAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->AGCS( this, fActivate );
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: ScriptAlyzeR" ), TRUE );
		fRet = pSecurity->AS( FALSE, this, fActivate );
	}
	else if( m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: GripAlyzeR" ), TRUE );
		fRet = pSecurity->AG( FALSE, this, fActivate );
	}
	else if( m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: Client-Server" ), TRUE );
		fRet = pSecurity->ACS( this, fActivate );
	}
	else if( m_acts.m_rx.m_fChecked && !m_acts.m_rx.m_fActivated ) {
		if( !fActivate ) OutputAMessage( _T("Attempting to activate: MovAlyzeRx" ), TRUE );
		fRet = pSecurity->ARX( FALSE, this, fActivate );
	}
	else {
		m_szRequestCode = _T("");
		fRet = TRUE;
	}

	IHTMLElement* pCB = NULL;
	if( GetElement( _T("linkPurchase"), &pCB ) == S_OK && pCB ) {
		VARIANT v;
		CString szAttr = _T("href");
		BSTR bstrAttr = szAttr.AllocSysString();
		CString szURL;
		szURL.Format( _T("https://sales.neuroscript.net/products.php?prods=%s&arc=%s"), m_szProds, m_szRequestCode );
		BSTR bstrURL = szURL.AllocSysString();
		V_VT( &v ) = VT_BSTR;
		V_BSTR( &v ) = bstrURL;
		pCB->setAttribute( bstrAttr, v );
		pCB->Release();
	}

	return fRet;
}

HRESULT ActivateHTMLDlg::OnItemChecked( IHTMLElement* )
{
	m_fPurchase = FALSE;

	// get check box states
	VARIANT v;
	IHTMLElement* pCB = NULL;
	CString szAttr = _T("checked");
	BSTR bstrAttr = szAttr.AllocSysString();
	if( GetElement( _T("prodScripta"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_fScript = ( v.boolVal == VARIANT_TRUE );
		pCB->Release();
	}
	if( GetElement( _T("prodMova"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_fOpti = ( v.boolVal == VARIANT_TRUE );
		pCB->Release();
	}
	if( GetElement( _T("prodUpgrade"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_fUpgrade = ( v.boolVal == VARIANT_TRUE );
		pCB->Release();
	}
	if( GetElement( _T("prodRx"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_fRx = ( v.boolVal == VARIANT_TRUE );
		pCB->Release();
	}
	if( GetElement( _T("prodGripa"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_fGrip = ( v.boolVal == VARIANT_TRUE );
		pCB->Release();
	}
	if( GetElement( _T("prodCS"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_fCS = ( v.boolVal == VARIANT_TRUE );
		pCB->Release();
	}

	// ONLY CHANGE STATUS OF THOSE ITEMS THAT ARE NOT ALREADY ACTIVATED
	// movalyzer
	if( !m_acts.m_opti.m_fActivated )
	{
		// should never really get unchecked by force
		m_acts.m_opti.m_fChecked = m_fOpti;
		m_acts.m_opti.m_fEnabled = TRUE;
		if( m_acts.m_fWasOpti )
		{
			m_acts.m_upgrade.m_fChecked = m_fOpti;
			m_acts.m_upgrade.m_fEnabled = m_fOpti;
			m_fUpgrade = m_fOpti;
		}
		// purchase button
		m_fPurchase |= m_acts.m_opti.m_fChecked;
	}
	// scriptalyzer
	if( !m_acts.m_scripta.m_fActivated )
	{
		// checked if mova or scripta & not upgrade
		m_acts.m_scripta.m_fChecked = ( m_fScript || m_fOpti ) && !m_fUpgrade;
		// disabled if is mova or an upgrade
		m_acts.m_scripta.m_fEnabled = !m_fOpti && !m_fUpgrade;
		// purchase button
		m_fPurchase |= m_acts.m_scripta.m_fChecked;
	}
	// rx
	if( !m_acts.m_rx.m_fActivated )
	{
		m_fRx = ( ( m_fRx && !m_fGrip && !m_fScript ) || ( m_fRx && m_fOpti ) );
		// checked if mova, scripta or rx (& not gripa alone) & not upgrade
		m_acts.m_rx.m_fChecked = ( m_fRx || m_fOpti ) && !m_fUpgrade;
		// disabled if scripta/mova/gripa or an upgrade
		m_acts.m_rx.m_fEnabled = !m_fScript && !m_fOpti && !m_fGrip && !m_fUpgrade;
		// purchase button
		m_fPurchase |= m_acts.m_rx.m_fChecked;
	}
	// gripalyzer
	if( !m_acts.m_gripa.m_fActivated )
	{
		// checked if gripa & not upgrade
		m_acts.m_gripa.m_fChecked = m_fGrip && ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// disabled if an upgrade
		m_acts.m_gripa.m_fEnabled = ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// purchase button
		m_fPurchase |= m_acts.m_gripa.m_fChecked;
	}
	// client-server
	if( !m_acts.m_cs.m_fActivated )
	{
		m_fCS = m_fCS && ( m_fOpti || m_fScript || m_fGrip ) && ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// checked if cs
		m_acts.m_cs.m_fChecked = m_fCS;
		// disabled if no product selected (except rx) or upgrade
		m_acts.m_cs.m_fEnabled = ( m_fOpti || m_fScript || m_fGrip ) && ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// purchase button
		m_fPurchase |= m_acts.m_cs.m_fChecked;
	}

	m_fOpti = m_acts.m_opti.m_fChecked;
	m_fUpgrade = m_acts.m_upgrade.m_fChecked;
	m_fScript = m_acts.m_scripta.m_fChecked;
	m_fRx = m_acts.m_rx.m_fChecked;
	m_fGrip = m_acts.m_gripa.m_fChecked;
	m_fCS = m_acts.m_cs.m_fChecked;

	// enable/disable checkboxes where appropriate
	EnableDisable();

	// update activation request code
	BOOL fRet = DoActivate();
	m_fActivate = ( m_szRequestCode != _T("") );
	if( fRet ) SetAttr( _T("codeARC"), _T("value"), m_szRequestCode );
	SetAttr( _T("bnActivate"), _T("disabled"), _T("disabled"), m_fActivate );

	return S_OK;
}

void ActivateHTMLDlg::UpdateCode()
{
	// update activation request code
	m_fPurchase = ( m_szRequestCode != _T("") );
	SetAttr( _T("codeARC"), _T("value"), m_szRequestCode );
	SetAttr( _T("bnActivate"), _T("disabled"), _T("disabled"), m_fActivate );
}

HRESULT ActivateHTMLDlg::OnClickPurchase( IHTMLElement* pElement )
{
	if( !m_fPurchase ) return S_OK;

	CString szURL;
	if( !m_fUnlock ) szURL.Format( _T("https://sales.neuroscript.net/products.php?prods=%s&arc=%s"), m_szProds, m_szRequestCode );
	else szURL.Format( _T("http://www.neuroscript.net/copyprotection.php?arc=%s"), m_szRequestCode );
	CHyperLink::GotoURL( szURL );

	return S_OK;
}

HRESULT ActivateHTMLDlg::OnClickActivate( IHTMLElement* pElement )
{
	if( !m_fActivate ) return S_OK;

	// get activation code from input field
	VARIANT v;
	IHTMLElement* pCB = NULL;
	CString szAttr = _T("value");
	BSTR bstrAttr = szAttr.AllocSysString();
	if( GetElement( _T("codeAC"), &pCB ) == S_OK && pCB ) {
		pCB->getAttribute( bstrAttr, 0, &v );
		m_szActivationCode = v.bstrVal;
		pCB->Release();
	}

	// cleanup any whitespaces
	TRIM( m_szActivationCode );

	// verify the legitimacy of the activation code
	if( ( m_szActivationCode.GetLength() != 19 ) ||
		( m_szActivationCode.GetAt( 4 ) != '-' ) ||
		( m_szActivationCode.GetAt( 9 ) != '-' ) ||
		( m_szActivationCode.GetAt( 14 ) != '-' ) )
	{
		BCGPMessageBox( _T("The Activation Code is not in the appropriate format of ####-####-####-####.") );
		return S_OK;
	}
	for( int i = 0; i < 19; i++ )
	{
		if( ( i != 4 ) && ( i != 9 ) && ( i != 14 ) )
		{
			if( !isdigit( m_szActivationCode.GetAt( i ) ) &&
				!isalpha( m_szActivationCode.GetAt( i ) ) )
			{
				BCGPMessageBox( _T("The Activation code provided is not in the appropriate format of ####-####-####-####.") );
				return S_OK;
			}
		}
	}

	// now handle the code
	NSSecurity* pSecurity = ::GetSecurity();
	if( !pSecurity ) {
		BCGPMessageBox( _T("Security object not available.") );
		return S_OK;
	}
	if( pSecurity->Activate( m_nItem, m_nParam, this, TRUE ) ) {
		m_fSuccess = TRUE;

		if( !m_fUnlock ) {
			DoActivate( TRUE );
			m_fPurchase = false;
			m_fActivate = false;

			m_acts.m_scripta.m_fEnabled = FALSE;
			m_acts.m_opti.m_fEnabled = FALSE;
			m_acts.m_upgrade.m_fEnabled = FALSE;
			m_acts.m_rx.m_fEnabled = FALSE;
			m_acts.m_gripa.m_fEnabled = FALSE;
			m_acts.m_cs.m_fEnabled = FALSE;
			EnableDisable();
		}

		SetAttr( _T("bnCancel"), _T("value"), _T("Close This Window") );

		if( m_nMode == ACT_MODE_RECOVERY ) BCGPMessageBox( _T("Recovery successful. Thank you.") );
		else BCGPMessageBox( _T("Activation successful. Thank you.") );
		if( ( m_nMode == ACT_MODE_COPYPROTECT ) || ( m_nMode == ACT_MODE_RECOVERY ) ) {
			// if we recovered, still need to do copy protection
			if( m_nMode == ACT_MODE_RECOVERY ) {
				m_nMode = ACT_MODE_COPYPROTECT;
				if( pSecurity->Activate( 1, 0 ) ) EndDialog( IDOK );
				else EndDialog( IDCANCEL );
			}
			else EndDialog( IDOK );
		}
	}
	else {
		BCGPMessageBox( _T("Nothing has been activated.") );
//		EndDialog( IDCANCEL );
	}

	return S_OK;
}

HRESULT ActivateHTMLDlg::OnClickCancel( IHTMLElement* pElement )
{
	if( m_fSuccess ) OnOK();
	else OnCancel();
	return S_OK;
}

#ifndef _USERS_H_
#define	_USERS_H_

#include "Objects.h"

#define	TAG_USER	_T("[USER]")

interface INSMUser;

// This class is intended to encapsulate all view & functionality for a group
class AFX_EXT_CLASS NSMUsers : public Objects
{
	PUT_BASE( NSMUser )

protected:
	virtual void Init() { m_nPage = 0; }

	// Operations
public:
	virtual ULONGLONG GetNumRecords();
	// Interface methods
	PUT_GET( NSMUser, UserID, CString, val.AllocSysString() );
	PUT_GET( NSMUser, Description, CString, val.AllocSysString() );
	PUT_GET( NSMUser, Private, BOOL, val );
	PUT_GET( NSMUser, WinUser, CString, val.AllocSysString() );
	PUT_GET( NSMUser, RootPath, CString, val.AllocSysString() );
	PUT_GET( NSMUser, BackupPath, CString, val.AllocSysString() );
	PUT_GET( NSMUser, Delimiter, CString, val.AllocSysString() );
	PUT_GET( NSMUser, InputType, short, val );
	PUT_GET( NSMUser, TabletMode, short, val );
	PUT_GET( NSMUser, EntireTablet, BOOL, val );
	PUT_GET( NSMUser, Log, BOOL, val );
	PUT_GET( NSMUser, Alpha, BOOL, val );
	PUT_GET( NSMUser, TabletWidth, double, val );
	PUT_GET( NSMUser, TabletHeight, double, val );
	PUT_GET( NSMUser, DisplayWidth, double, val );
	PUT_GET( NSMUser, DisplayHeight, double, val );
	PUT_GET( NSMUser, AspectRatioWidth, double, val );
	PUT_GET( NSMUser, AspectRatioHeight, double, val );
	PUT_GET( NSMUser, CommPort, CString, val.AllocSysString() );
	PUT_GET( NSMUser, AutoUpdate, BOOL, val );
	GET_ONLY( NSMUser, Virgin, BOOL, val );
	PUT_GET( NSMUser, GenerateSubjectIDs, BOOL, val );
	PUT_GET( NSMUser, SubjectStartID, CString, val.AllocSysString() );
	PUT_GET( NSMUser, SubjectEndID, CString, val.AllocSysString() );
	PUT_GET( NSMUser, SubjectCurID, CString, val.AllocSysString() );

	void PutSysPass( CString, BOOL fSkipEncode = FALSE );
	void GetSysPass( CString&, BOOL fSkipDecode = FALSE );
	static void GetSysPass( INSMUser*, CString&, BOOL fSkipDecode = FALSE );
	PUT_GET( NSMUser, Encrypted, BOOL, val );
	PUT_GET( NSMUser, EncryptionMethod, short, val );
	PUT_GET( NSMUser, SiteID, CString, val.AllocSysString() );
	PUT_GET( NSMUser, SiteDesc, CString, val.AllocSysString() );
	void PutSignature( CString, BOOL fSkipEncode = FALSE );
	void GetSignature( CString&, BOOL fSkipDecode = FALSE );
	static void GetSignature( INSMUser*, CString&, BOOL fSkipDecode = FALSE );

	// subject ID generation and range
	void ResetSubjectIDStart();
	static void ResetSubjectIDStart( INSMUser* );

	// Importing/Exporting
	BOOL Export( CStdioFile* pFile );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo );

	// Backup/restore
	static BOOL BackupDB( BOOL fAsk = TRUE );
	static void RestoreDB();

	// Verify paths
	static void VerifyPaths( CString szID, CString szRootPath, CString szBackupPath,
							 BOOL fPrivate, BOOL& fRecommendedLocation, BOOL& fWriteable );
	static BOOL VerifyBackupPath();
	static BOOL BackupRaw( CString szExp, CString szGrp, CString szSubj, CString szRaw );

	// Encrypt Users that haven't been yet
	static void EncryptAll();

	// Cleanup any elements that were using paths to images and copy and move them
	static BOOL VerifyElementImages();

	// Cleanup any conditions that were using paths to sounds and copy and move them
	static BOOL VerifyConditionSounds();

	// Editing specific page (used by doedit-defined in objects)
	int m_nPage;
};

#endif	// _USERS_H_

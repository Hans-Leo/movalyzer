#ifndef _MQUESTS_H_
#define	_MQUESTS_H_

#include "Objects.h"

#define	TAG_MQUEST	_T("[MQuestionnaire]")

interface IMQuestionnaire;

// This class is intended to encapsulate all view & functionality for a MQuestionnaire
class AFX_EXT_CLASS MQuestionnaires : public Objects
{
	PUT_BASE( MQuestionnaire )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();
	// Interface methods
	PUT_GET( MQuestionnaire, ID, CString, val.AllocSysString() )
	PUT_GET( MQuestionnaire, IsHeader, BOOL, val );
	PUT_GET( MQuestionnaire, IsPrivate, BOOL, val );
	PUT_GET( MQuestionnaire, ItemNum, short, val );
	PUT_GET( MQuestionnaire, Question, CString, val.AllocSysString() );
	PUT_GET( MQuestionnaire, IsNumeric, BOOL, val );
	PUT_GET( MQuestionnaire, ColumnHeader, CString, val.AllocSysString() );

	// Importing/Exporting
	BOOL Export( CStdioFile* pFile );
	BOOL Import( CStdioFile* pFile, BOOL fOverWrite = FALSE );

	// Repair (in case soring order gets out of whack)
	BOOL Repair();

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();
};

#endif	// _MQUESTS_H_

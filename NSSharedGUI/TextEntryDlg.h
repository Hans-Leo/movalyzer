#pragma once

class AFX_EXT_CLASS CTextEntryDlg : protected CWnd
{
	// Construction
public:
	CTextEntryDlg() {}
	virtual ~CTextEntryDlg() {}

	// Operations
public:

	// Overrides
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	// Implementation
public:
	LPCTSTR GetText();
	int Show(CWnd *pParent, LPCTSTR pszTitle, LPCTSTR pszPrompt, LPCTSTR pszDefault = _T(""), bool bPassword = false);

protected:
	int DoModal(CWnd *pParent);

	CString m_strText;

	// controls
	CEdit m_ctlEdit;
	CFont m_Font;
	CStatic m_ctlLabel;
	CButton m_ctlCancel;
	CButton m_ctlOK;

	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnClose();
	afx_msg void OnCancel();
	afx_msg void OnOK();
	afx_msg void OnEditboxChanged();

	DECLARE_MESSAGE_MAP()
};

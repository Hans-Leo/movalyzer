// ScatterOffsetDlg.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ScatterOffsetDlg dialog

class AFX_EXT_CLASS ScatterOffsetDlg : public CBCGPDialog
{
// Construction
public:
	ScatterOffsetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_SCATSCALE };
	double			m_dOffset1;
	double			m_dOffset2;
	double			m_dScale1;
	double			m_dScale2;
	NSToolTipCtrl	m_tooltip;
	double			m_dOffset1Rec;
	double			m_dOffset2Rec;
	double			m_dScale1Rec;
	double			m_dScale2Rec;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL IsCustom();
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

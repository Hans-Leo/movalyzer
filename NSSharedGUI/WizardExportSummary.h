#pragma once

#include "NSTooltipCtrl.h"

// WizardExportSummary dialog

class AFX_EXT_CLASS WizardExportSummary : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardExportSummary)

public:
	WizardExportSummary();
	virtual ~WizardExportSummary();

// Dialog Data
	enum { IDD = IDD_WIZ_EXPORT_SUMMARY };
	CBCGPEdit	m_summary;
	CString		m_szSummary;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();

	// Implementation
public:
	BOOL DoHelp( HELPINFO* );

protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedChkFinish();
	DECLARE_MESSAGE_MAP()
};

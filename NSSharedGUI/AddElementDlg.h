#pragma once

// AddElementDlg.h : header file
//

#include "AddDlg.h"

/////////////////////////////////////////////////////////////////////////////
// AddElementDlg dialog

class AFX_EXT_CLASS AddElementDlg : public AddDlg
{
// Construction
public:
	AddElementDlg( CStringList* pExisting, CWnd* pParent = NULL,
				   BOOL fSingleSel = FALSE, BOOL fNoChange = FALSE );
	~AddElementDlg();

// Dialog Data
	BOOL	m_fNoChange;

// Overrides

// Implementation
protected:
	virtual int GetImageNum() { return 17; }
	virtual void InsertColumns();
	virtual void FillList();
	virtual int Add();
	virtual BOOL Edit( int nItem );
	virtual BOOL Delete( int nItem );
	virtual void Relationships( int nItem );
	virtual void OK();
	virtual void Cleanup();
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

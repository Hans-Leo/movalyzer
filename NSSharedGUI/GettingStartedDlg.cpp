// HtmlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GettingStartedDlg.h"
#include "..\NSShared\HyperLink.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// GettingStartedDlg dialog

GettingStartedDlg* _pGettingStartedDlg = NULL;

IMPLEMENT_DYNAMIC(GettingStartedDlg, CDHtmlDialog)

BEGIN_MESSAGE_MAP(GettingStartedDlg, CDHtmlDialog)
END_MESSAGE_MAP()

BEGIN_DHTML_EVENT_MAP(GettingStartedDlg)
	DHTML_EVENT_ONCLICK(_T("ShowMe"), OnClickShow)
	DHTML_EVENT_ONCLICK(_T("navClick"), OnClickVidLink)
	DHTML_EVENT_ONCLICK(_T("tutClick"), OnClickTutLink)
END_DHTML_EVENT_MAP()

GettingStartedDlg::GettingStartedDlg( CWnd* pParent, BOOL fModeless )
	: CDHtmlDialog( GettingStartedDlg::IDD, GettingStartedDlg::IDH, pParent ),
	  m_fModeless( fModeless ), m_fShow( TRUE )
{
	if( m_fModeless && Create( GettingStartedDlg::IDD, pParent ) )
	{
		ShowWindow( SW_SHOW );
		_pGettingStartedDlg = this;
	}
}

void GettingStartedDlg::DoDataExchange(CDataExchange* pDX)
{
	CDHtmlDialog::DoDataExchange(pDX);
}

GettingStartedDlg* GettingStartedDlg::GetGettingStartedDlg()
{
	return _pGettingStartedDlg;
}

// GettingStartedDlg message handlers

BOOL GettingStartedDlg::OnInitDialog()
{
	CDHtmlDialog::OnInitDialog();

	CenterWindow();

	return TRUE;  // return TRUE  unless you set the focus to a control}
}

void GettingStartedDlg::OnDocumentComplete( LPDISPATCH pDisp, LPCTSTR szUrl )
{
	// update check box for show/not show at startup
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	CString szKey, szRB;
	szKey = _T("Settings");
	szRB = app->GetRegistryBase();
	app->SetRegistryBase( szKey );
	szKey = _T("StartPage");
	m_fShow = app->GetInt( szKey, TRUE );
	app->SetRegistryBase( szRB );
	IHTMLElement* pCB = NULL;
	if( GetElement( _T("cbArea"), &pCB ) == S_OK && pCB )
	{
		CString szCB = _T("<label><input type=\"checkbox\" id=\"ShowMe\" value=\"active\"");
		if( m_fShow ) szCB += _T(" checked=\"checked\"");
		szCB += _T(" />Show this at startup (MENU: View -&gt; Getting Started)</label>");
		pCB->put_innerHTML( szCB.AllocSysString() );
	}
	pCB->Release();
}

void GettingStartedDlg::OnCancel()
{
	if( m_fModeless ) DestroyWindow();
	else CDHtmlDialog::OnCancel();
}

void GettingStartedDlg::PostNcDestroy()
{
	// update registry for check box of show or not at startup
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	CString szKey, szRB;
	szKey = _T("Settings");
	szRB = app->GetRegistryBase();
	app->SetRegistryBase( szKey );
	szKey = _T("StartPage");
	app->WriteInt( szKey, m_fShow );
	app->SetRegistryBase( szRB );

	if( !m_fModeless ) CDHtmlDialog::PostNcDestroy();
	else delete this;
	_pGettingStartedDlg = NULL;
}

HRESULT GettingStartedDlg::OnClickShow( IHTMLElement* pElement )
{
	m_fShow = !m_fShow;
	return S_OK;
}

HRESULT GettingStartedDlg::OnClickVidLink( IHTMLElement* pElement )
{
	if( pElement ) {
		BSTR bstr = NULL;
		pElement->get_title( &bstr );
		CString szTitle = bstr;
		pElement->get_lang( &bstr );
		CString szAlt = bstr;

		CString szURL;
		szURL.Format( _T("http://www.neuroscript.net/videos.php?cat=%s&vid=%s"), szTitle, szAlt );
		CHyperLink::GotoURL( szURL );
	}
	return S_OK;
}

HRESULT GettingStartedDlg::OnClickTutLink( IHTMLElement * pElement )
{
	if( pElement ) {
		BSTR bstr = NULL;
		pElement->get_lang( &bstr );
		CString szTopic = bstr, szLink;

		if( szTopic == _T("overview") ) szLink = _T("overview.html");
		else if( szTopic == _T("driver") ) szLink = _T("setup.html");
		else if( szTopic == _T("user") ) szLink = _T("createnewuser.html");
		else if( szTopic == _T("device") ) szLink = _T("devicesetuptut.html");
		else if( szTopic == _T("setupexp") ) szLink = _T("expsetup.html");
		else if( szTopic == _T("runexp") ) szLink = _T("runexp.html");
		else if( szTopic == _T("charting") ) szLink = _T("chart.html");
		else if( szTopic == _T("analysis") ) szLink = _T("analysis.html");

		if( szLink != _T("") ) ::GoToHelp( szLink, GetParent() );
	}
	return S_OK;
}

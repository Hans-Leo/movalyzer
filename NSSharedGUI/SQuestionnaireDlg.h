#pragma once

// SQuestionnaireDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireDlg dialog

class AFX_EXT_CLASS SQuestionnaireDlg : public CBCGPDialog
{
// Construction
public:
	SQuestionnaireDlg(CStringList* pListH, CStringList* pListQ,
					  CStringList* pListA, CStringList* pListN,
					  CStringList* pListI, CStringList* pListCH,
					  BOOL fEdit = FALSE, int nIdx = 0, CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_SQUEST };
	CString			m_szID;
	CString			m_szColHeader;
	CString			m_szQuestion;
	CString			m_szAnswer;
	CStringList*	m_pListID;		// indices
	CStringList*	m_pListH;		// header/category
	CStringList*	m_pListQ;		// questions
	CStringList*	m_pListA;		// answers
	CStringList*	m_pListN;		// numeric
	CStringList*	m_pListCH;		// column headers
	BOOL			m_fEdit;
	int				m_nIdx;
	BOOL			m_fClose;
	BOOL			m_fCanClose;
	BOOL			m_fHdr;
	BOOL			m_fNum;
	CBCGPButton		m_bnPrev;
	CBCGPButton		m_bnNext;
	CBCGPToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	virtual void OnCancel();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	BOOL IsValidAnswer();
	afx_msg void OnBnPrevq();
	afx_msg void OnBnNextq();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckNumeric();
	DECLARE_MESSAGE_MAP()
};

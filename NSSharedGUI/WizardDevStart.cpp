// WizardDevStart.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardDevStart.h"
#include "WizardDevSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// WizardDevStart dialog

IMPLEMENT_DYNAMIC(WizardDevStart, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardDevStart, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardDevStart::WizardDevStart()
	: CBCGPPropertyPage(WizardDevStart::IDD)
{

}

WizardDevStart::~WizardDevStart()
{
}

void WizardDevStart::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

// WizardDevStart message handlers

LRESULT WizardDevStart::OnWizardNext() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	pParent->PostMessage( UM_ADJUSTBUTTONS );
	AdjustControlsLayout();
	RedrawWindow();

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardDevStart::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );
	pParent->PostMessage( UM_ADJUSTBUTTONS );
	AdjustControlsLayout();
	RedrawWindow();

	// Highlight the main sentence
	CStatic* pDis = (CStatic*)GetDlgItem( IDC_TXT_HELP );
	CDC* pDC = GetDC();
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_BOLD;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	pDis->SetFont( &font, TRUE );
	pDC->SelectObject( pOldFont );
	ReleaseDC( pDC );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardDevStart::OnSetActive() 
{
	BOOL fRet = CBCGPPropertyPage::OnSetActive();
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->PostMessage( UM_ADJUSTBUTTONS );
	AdjustControlsLayout();
	RedrawWindow();
	return fRet;
}

BOOL WizardDevStart::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("devicesetup.html"), this );
	return TRUE;
}

BOOL WizardDevStart::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

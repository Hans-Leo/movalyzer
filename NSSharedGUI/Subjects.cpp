// Subjects.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "OverwriteDlg.h"
#include "PasswordChgDlg.h"
#include "PasswordDlg.h"
#include "SQuestionnaireFullDlg.h"
#include "Subjects.h"
#include "Experiments.h"
#include "Groups.h"
#include "SubjectDlg.h"
#include "..\CTreeLink\DBCommon.h"
#include "Users.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString		_szSubjCode;
CStringList	_lstExportedSubjects;
CString		_szSubjExpID;
CString		_szSubjGrpID;
double		_dDevRes = 0.;
int			_nSampRate = 0;


///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

void Subjects::PutNameLast( CString szVal, BOOL fSkipEncode )
{
	if( m_pSubject )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pSubject->put_LastName( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameLast( CString& szVal, BOOL fPrv, BOOL fSkipDecode, BOOL fIgnorePass )
{
	Subjects::GetNameLast( m_pSubject, szVal, fPrv, fSkipDecode, fIgnorePass );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::PutNameFirst( CString szVal, BOOL fSkipEncode )
{
	if( m_pSubject )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pSubject->put_FirstName( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameFirst( CString& szVal, BOOL fPrv, BOOL fSkipDecode, BOOL fIgnorePass )
{
	Subjects::GetNameFirst( m_pSubject, szVal, fPrv, fSkipDecode, fIgnorePass );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetName( CString& szVal, BOOL fPrv, BOOL fSkipDecode )
{
	Subjects::GetName( m_pSubject, szVal, fPrv, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameWithID( CString& szVal, BOOL fPrv, BOOL fReverse, BOOL fSkipDecode )
{
	Subjects::GetNameWithID( m_pSubject, szVal, fPrv, fReverse, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameWithCodeID( CString& szVal, BOOL fPrv, BOOL fReverse, BOOL fSkipDecode )
{
	Subjects::GetNameWithCodeID( m_pSubject, szVal, fPrv, fReverse, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetDescriptionWithID( CString& szVal, BOOL fPrv, BOOL fReverse, BOOL fSkipDecode )
{
	Subjects::GetDescriptionWithID( m_pSubject, szVal, fPrv, fReverse, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::PutPrvNotes( CString szVal, BOOL fSkipEncode )
{
	if( m_pSubject )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pSubject->put_PrvNotes( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetPrvNotes( CString& szVal, BOOL fPrv, BOOL fSkipDecode, BOOL fIgnorePass )
{
	Subjects::GetPrvNotes( m_pSubject, szVal, fPrv, fSkipDecode, fIgnorePass );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::GetNext()
{
	return Subjects::GetNext( m_pSubject );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Find( CString szID )
{
	return Subjects::Find( m_pSubject, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Add()
{
	return Subjects::Add( m_pSubject );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Modify()
{
	return Subjects::Modify( m_pSubject );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Remove()
{
	return Subjects::Remove( m_pSubject );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::SetDataPath( CString szPath )
{
	Subjects::SetDataPath( m_pSubject, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Subjects::GetID( ISubject* pSubject, CString& szVal )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameLast( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fSkipDecode, BOOL fIgnorePass )
{
	if( pSubject )
	{
		if( !fIgnorePass )
		{
			CString szPass;
			Subjects::GetPassword( pSubject, szPass );
			CString szUserPass = ::GetUserPassword();
			if( szPass != szUserPass ) fPrv = TRUE;
		}
		if( !fPrv )
		{
			BSTR bstrItem;
			pSubject->get_LastName( &bstrItem );
			short nMethod = -1;
			pSubject->get_EncryptionMethod( &nMethod );
			BOOL fEnc = FALSE;
			pSubject->get_Encrypted( &fEnc );
			szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
		}
		else szVal = PVT_NAME_LAST;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameFirst( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fSkipDecode, BOOL fIgnorePass )
{
	if( pSubject )
	{
		if( !fIgnorePass )
		{
			CString szPass;
			Subjects::GetPassword( pSubject, szPass );
			CString szUserPass = ::GetUserPassword();
			if( szPass != szUserPass ) fPrv = TRUE;
		}
		if( !fPrv )
		{
			BSTR bstrItem;
			pSubject->get_FirstName( &bstrItem );
			short nMethod = -1;
			pSubject->get_EncryptionMethod( &nMethod );
			BOOL fEnc = FALSE;
			pSubject->get_Encrypted( &fEnc );
			szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
		}
		else szVal = PVT_NAME_FIRST;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetName( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fSkipDecode )
{
	if( pSubject )
	{
		CString szLast, szFirst;

		Subjects::GetNameLast( pSubject, szLast, fPrv, fSkipDecode );
		Subjects::GetNameFirst( pSubject, szFirst, fPrv, fSkipDecode );

		szVal.Format( _T("%s, %s"), szLast, szFirst );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameWithID( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fReverse, BOOL fSkipDecode )
{
	if( pSubject )
	{
		CString szDelim;
		::GetDelimiter( szDelim );
		CString szID, szLast, szFirst;

		Subjects::GetID( pSubject, szID );
		Subjects::GetNameLast( pSubject, szLast, fPrv, fSkipDecode );
		Subjects::GetNameFirst( pSubject, szFirst, fPrv, fSkipDecode );

		if( fReverse )
			szVal.Format( _T("%s%s%s, %s"), szID, szDelim, szLast, szFirst );
		else
			szVal.Format( _T("%s, %s%s%s"), szLast, szFirst, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNameWithCodeID( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fReverse, BOOL fSkipDecode )
{
	if( pSubject )
	{
		CString szDelim;
		::GetDelimiter( szDelim );
		CString szID, szLast, szFirst, szCode;

		Subjects::GetID( pSubject, szID );
		Subjects::GetNameLast( pSubject, szLast, fPrv , fSkipDecode);
		Subjects::GetNameFirst( pSubject, szFirst, fPrv, fSkipDecode );
		Subjects::GetCode( pSubject, szCode );

		if( fReverse )
			szVal.Format( _T("%s%s(%s) %s, %s"), szID, szDelim, szCode, szLast, szFirst );
		else
			szVal.Format( _T("(%s) %s, %s%s%s"), szCode, szLast, szFirst, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetDescriptionWithID( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fReverse, BOOL fSkipDecode )
{
	Subjects::GetNameWithID( pSubject, szVal, fPrv, fReverse, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetNotes( ISubject* pSubject, CString& szVal )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_Notes( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetPrvNotes( ISubject* pSubject, CString& szVal, BOOL fPrv, BOOL fSkipDecode, BOOL fIgnorePass )
{
	if( pSubject )
	{
		if( !fIgnorePass )
		{
			CString szPass;
			Subjects::GetPassword( pSubject, szPass );
			CString szUserPass = ::GetUserPassword();
			if( szPass != szUserPass ) fPrv = TRUE;
		}
		if( !fPrv )
		{
			BSTR bstrItem;
			pSubject->get_PrvNotes( &bstrItem );
			short nMethod = -1;
			pSubject->get_EncryptionMethod( &nMethod );
			BOOL fEnc = FALSE;
			pSubject->get_Encrypted( &fEnc );
			szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
		}
		else szVal = PVT_NAME_LAST;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetDateAdded( ISubject* pSubject, DATE& dtVal )
{
	if( pSubject )
	{
		pSubject->get_DateAdded( &dtVal );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetDateAdded( ISubject* pSubject, COleDateTime& dtVal )
{
	if( pSubject )
	{
		DATE dt;
		Subjects::GetDateAdded( pSubject, dt );
		dtVal = dt;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetDefaultExperiment( ISubject* pSubject, CString& szVal )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_DefaultExperiment( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetActive( ISubject* pSubject, BOOL& fVal )
{
	if( pSubject ) pSubject->get_Active( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetExperimentCount( ISubject* pSubject, short& nVal )
{
	if( pSubject ) pSubject->get_ExperimentCount( &nVal );
}


///////////////////////////////////////////////////////////////////////////////

void Subjects::GetEncrypted( ISubject* pSubject, BOOL& fVal )
{
	if( pSubject ) pSubject->get_Encrypted( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetCode( ISubject* pSubject, CString& szVal )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_Code( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetEncryptionMethod( ISubject* pSubject, short& nVal )
{
	if( pSubject ) pSubject->get_EncryptionMethod( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetSiteID( ISubject* pSubject, CString& szVal )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_SiteID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetSiteDesc( ISubject* pSubject, CString& szVal )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_SiteDesc( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::PutPassword( CString szVal, BOOL fSkipEncode )
{
	if( m_pSubject )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pSubject->put_Password( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetPassword( CString& szVal, BOOL fSkipDecode )
{
	Subjects::GetPassword( m_pSubject, szVal, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetPassword( ISubject* pSubject, CString& szVal, BOOL fSkipDecode )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_Password( &bstrItem );
		short nMethod = -1;
		pSubject->get_EncryptionMethod( &nMethod );
		BOOL fEnc = FALSE;
		pSubject->get_Encrypted( &fEnc );
		szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::PutSignature( CString szVal, BOOL fSkipEncode )
{
	if( m_pSubject )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pSubject->put_Signature( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetSignature( CString& szVal, BOOL fSkipDecode )
{
	Subjects::GetSignature( m_pSubject, szVal, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetSignature( ISubject* pSubject, CString& szVal, BOOL fSkipDecode )
{
	if( pSubject )
	{
		BSTR bstrItem;
		pSubject->get_Signature( &bstrItem );
		short nMethod = -1;
		pSubject->get_EncryptionMethod( &nMethod );
		BOOL fEnc = FALSE;
		pSubject->get_Encrypted( &fEnc );
		szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
	}
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::ResetToStart()
{
	return Subjects::ResetToStart( m_pSubject );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::ResetToStart( ISubject* pSubject )
{
	if( pSubject ) return pSubject->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::GetNext( ISubject* pSubject )
{
	if( pSubject ) return pSubject->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Find( ISubject* pSubject, CString szID )
{
	if( pSubject ) return pSubject->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Add( ISubject* pSubject )
{
	if( !pSubject ) return E_FAIL;

	if( FAILED( pSubject->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Subjects::Add") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Modify( ISubject* pSubject )
{
	if( !pSubject ) return E_FAIL;

	if( FAILED( pSubject->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Subjects::Remove( ISubject* pSubject )
{
	if( !pSubject ) return E_FAIL;

	if( FAILED( pSubject->Remove() ) )
	{
		BCGPMessageBox( IDS_SUBJDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::SetDataPath( ISubject* pSubject, CString szPath )
{
	if( pSubject ) pSubject->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoAdd( CWnd* pOwner )
{
	_szSubjCode = _T("");
	return Subjects::DoAdd( m_pSubject, pOwner );
}

BOOL Subjects::DoAdd( CString szCode, CWnd* pOwner )
{
	_szSubjCode = szCode;
	return Subjects::DoAdd( m_pSubject, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoAdd( CString szCode, ISubject* pSubject, CWnd* pOwner )
{
	_szSubjCode = szCode;
	return DoAdd( pSubject, pOwner );
}

BOOL Subjects::DoAdd( ISubject* pSubject, CWnd* pOwner )
{
	if( !pSubject ) return FALSE;

	CString szExpID, szGrpID, szSubjID;
	::GetLastExperimentInfo( szExpID, szGrpID, szSubjID );
	SubjectDlg d( pOwner, TRUE );
	d.m_szCode = _szSubjCode;
	d.m_szDefExp = szExpID;
	if( d.DoModal() == IDOK )
	{
		if( d.m_szID.GetLength() == 0 )
		{
			BCGPMessageBox( IDS_SUBJID_ERR );
			return FALSE;
		}

		pSubject->put_ID( d.m_szID.AllocSysString() );
		pSubject->put_LastName( ( Objects::Encode( d.m_szLast ) ).AllocSysString() );
		pSubject->put_FirstName( ( Objects::Encode( d.m_szFirst ) ).AllocSysString() );
		pSubject->put_Active( d.m_fActive );
		pSubject->put_Notes( d.m_szNotes.AllocSysString() );
		pSubject->put_DateAdded( d.m_DateAdded );
		pSubject->put_Encrypted( TRUE );
		pSubject->put_Code( d.m_szCode.AllocSysString() );
		pSubject->put_PrvNotes( ( Objects::Encode( d.m_szPrvNotes ) ).AllocSysString() );
		pSubject->put_DefaultExperiment( d.m_szDefExp.AllocSysString() );
		pSubject->put_EncryptionMethod( (short)::GetEncryptionMethod() );
		pSubject->put_SiteID( ( ::GetCurrentUserSiteID() ).AllocSysString() );
		pSubject->put_SiteDesc( ( ::GetCurrentUserSiteDesc() ).AllocSysString() );
		pSubject->put_Signature( ( Objects::Encode( ::GetSignature() ) ).AllocSysString() );
		pSubject->put_Password( ( Objects::Encode( ::GetUserPassword() ) ).AllocSysString() );
#ifdef	_DEBUG
		pSubject->put_ExperimentCount( d.m_nExpCount );
#endif

		HRESULT hr = Subjects::Add( pSubject );
		if( SUCCEEDED( hr ) )
		{
			// update user with current subj ID if autogenerate is on
			CString szUser;
			::GetCurrentUser( szUser );
			NSMUsers user;
			if( SUCCEEDED( user.Find( szUser ) ) )
			{
				BOOL fGen = FALSE;
				user.GetGenerateSubjectIDs( fGen );
				if( fGen )
				{
					user.PutSubjectCurID( d.m_szID );
					hr = user.Modify();
					if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to update current subject ID for auto-generate.") );
				}
			}

			return TRUE;
		}
		else {
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Subjects::DoAdd") );
		}
	}
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoEdit( CString szID, CWnd* pOwner )
{
	_szSubjExpID = m_szExpID;
	_szSubjGrpID = m_szGrpID;
	_dDevRes = m_dDevRes;
	_nSampRate = m_nSampRate;
	return Subjects::DoEdit( m_pSubject, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoEdit( ISubject* pSubject, CString szID, CWnd* pOwner )
{
	if( !pSubject ) return FALSE;
	if( FAILED( Subjects::Find( pSubject, szID ) ) ) return FALSE;

	CString szLast, szFirst, szNotes, szPrvNotes, szDefExp, szCode;
	CString szSiteID, szSiteDesc, szSig, szPass;
	COleDateTime dt;
	BOOL fActive;
	short nExpCount = 0, nEncMethod = 0;

	// if user pass does not match subject pass, inform user and offer password dialog
	CString szUserPass = ::GetUserPassword();
	Subjects::GetPassword( pSubject, szPass, FALSE );
	if( szPass != szUserPass )
	{
		if( !CheckPass( TRUE, szPass ) ) return FALSE;
	}
	else if( !CheckPass() ) return FALSE;

	Subjects::GetCode( pSubject, szCode );
	Subjects::GetNameLast( pSubject, szLast, FALSE, FALSE, TRUE );
	Subjects::GetNameFirst( pSubject, szFirst, FALSE, FALSE, TRUE );
	Subjects::GetNotes( pSubject, szNotes );
	Subjects::GetPrvNotes( pSubject, szPrvNotes, FALSE, FALSE, TRUE );
	Subjects::GetDateAdded( pSubject, dt );
	Subjects::GetDefaultExperiment( pSubject, szDefExp );
	Subjects::GetActive( pSubject, fActive );
	Subjects::GetExperimentCount( pSubject, nExpCount );
	Subjects::GetEncryptionMethod( pSubject, nEncMethod );
	Subjects::GetSiteID( pSubject, szSiteID );
//	if( szSiteID == _T("") ) szSiteID = ::GetCurrentUserSiteID();
	Subjects::GetSiteDesc( pSubject, szSiteDesc );
//	if( szSiteDesc == _T("") ) szSiteDesc = ::GetCurrentUserSiteDesc();
	Subjects::GetSignature( pSubject, szSig, FALSE );
	if( szSig == _T("") ) szSig = ::GetSignature();

	SubjectDlg d( pOwner );
	d.m_szExpID = _szSubjExpID;
	d.m_szGrpID = _szSubjGrpID;
	d.m_szID = szID;
	d.m_szCode = szCode;
	d.m_szLast = szLast;
	d.m_szFirst = szFirst;
	d.m_szNotes = szNotes;
	d.m_szPrvNotes = szPrvNotes;
	d.m_DateAdded = dt;
	d.m_szDefExp = szDefExp;
	d.m_fActive = fActive;
	d.m_nExpCount = nExpCount;
	d.m_dDevRes = _dDevRes;
	d.m_nSampRate = _nSampRate;
	d.m_nEncryptionMethod = nEncMethod;
	d.m_szSiteID = szSiteID;
	d.m_szSiteDesc = szSiteDesc;
	d.m_szSignature = szSig;
	d.m_szPassword = szPass;
	if( d.DoModal() == IDOK )
	{
		pSubject->Find( szID.AllocSysString() );
		pSubject->put_LastName( ( Objects::Encode( d.m_szLast ) ).AllocSysString() );
		pSubject->put_FirstName( ( Objects::Encode( d.m_szFirst ) ).AllocSysString() );
		pSubject->put_Notes( d.m_szNotes.AllocSysString() );
		pSubject->put_PrvNotes( ( Objects::Encode( d.m_szPrvNotes ) ).AllocSysString() );
		pSubject->put_DateAdded( d.m_DateAdded );
		pSubject->put_DefaultExperiment( d.m_szDefExp.AllocSysString() );
		pSubject->put_Active( d.m_fActive );
		pSubject->put_Encrypted( TRUE );
		pSubject->put_Code( d.m_szCode.AllocSysString() );
		pSubject->put_EncryptionMethod( (short)::GetEncryptionMethod() );
		pSubject->put_SiteID( d.m_szSiteID.AllocSysString() );
		pSubject->put_SiteDesc( d.m_szSiteDesc.AllocSysString() );
		pSubject->put_Signature( ( Objects::Encode( d.m_szSignature ) ).AllocSysString() );
// use below to clean sig out when needed
//		pSubject->put_Signature( ( Objects::Encode( "" ) ).AllocSysString() );
		pSubject->put_Password( ( Objects::Encode( d.m_szPassword ) ).AllocSysString() );
#ifdef	_DEBUG
		pSubject->put_ExperimentCount( d.m_nExpCount );
#endif
		HRESULT hr = pSubject->Modify();
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_UPD_ERR );
			return FALSE;
		}

		// subject samp rate & dev res if edited
		if( d.m_fEditDRSR )
		{
			IExperimentMember* pEM = NULL;
			HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
										   IID_IExperimentMember, (LPVOID*)&pEM );
			if( SUCCEEDED( hr ) )
			{
				hr = pEM->Find( _szSubjExpID.AllocSysString(), _szSubjGrpID.AllocSysString(),
								szID.AllocSysString() );
				if( SUCCEEDED( hr ) )
				{
					pEM->put_DeviceRes( d.m_dDevRes );
					pEM->put_SamplingRate( d.m_nSampRate );
					pEM->Modify();
				}
				pEM->Release();
			}
			else BCGPMessageBox( _T("Error in creating Experiment Member object") );
		}

		return TRUE;
	}

	return FALSE;
}

void Subjects::IncrementExperimentCount( CString szID )
{
	if( !m_pSubject ) return;
	if( FAILED( Find( szID ) ) ) return;

	short nCount = 0;
	m_pSubject->get_ExperimentCount( &nCount );
	nCount++;
	m_pSubject->put_ExperimentCount( nCount );
	m_pSubject->Modify();
}

BOOL Subjects::Export( CMemFile* pFile, BOOL fFirst )
{
	if( !m_pSubject || !pFile ) return FALSE;

	CString szItem;
	BOOL fPrv = ::IsPrivacyOn();

	Subjects::GetID( m_pSubject, szItem );
	if( fFirst ) _lstExportedSubjects.RemoveAll();
	// else see if on list...if so, skip
	else if( _lstExportedSubjects.Find( szItem ) ) return TRUE;
	_lstExportedSubjects.AddTail( szItem );

	WRITE_STRING( pFile, TAG_SUBJECT );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
		// Last Name
	Subjects::GetNameLast( m_pSubject, szItem, fPrv, TRUE, TRUE );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
		// First Name
	Subjects::GetNameFirst( m_pSubject, szItem, fPrv, TRUE, TRUE );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Notes
	Subjects::GetNotes( m_pSubject, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Private Notes
	Subjects::GetPrvNotes( m_pSubject, szItem, fPrv, TRUE, TRUE );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Date Added
	COleDateTime dt;
	Subjects::GetDateAdded( dt );
	szItem = dt.Format( "%m/%d/%Y %H:%M:%S" ); \
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Default Experiment
	Subjects::GetDefaultExperiment( m_pSubject, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Active?
	BOOL fVal;
	Subjects::GetActive( m_pSubject, fVal );
	szItem.Format( fVal ? _T("1") : _T("0") );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Experiment Count
	short nVal;
	Subjects::GetExperimentCount( m_pSubject, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Encrypted
	Subjects::GetEncrypted( m_pSubject, fVal );
	szItem.Format( fVal ? _T("1") : _T("0") );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Code
	Subjects::GetCode( m_pSubject, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Encryption Method
	Subjects::GetEncryptionMethod( m_pSubject, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Site ID
	Subjects::GetSiteID( m_pSubject, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Site Desc
	Subjects::GetSiteDesc( m_pSubject, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Signature
	Subjects::GetSignature( m_pSubject, szItem, TRUE );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Password
	Subjects::GetPassword( m_pSubject, szItem, TRUE );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );

	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo,
					   int nVer, BOOL fScan )
{
	if( !m_pSubject || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp, szTemp2, szPass;
	Subjects comp;
	COleDateTime dt;
	BOOL fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE, fWasEnc = FALSE;
	BOOL fPassDiff = FALSE, fNameLastDiff = FALSE, fNameFirstDiff = FALSE,
		 fNotesPrvDiff = FALSE, fSigDiff = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( szLine == HOLDER ) return TRUE;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Subject %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Last Name
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	GetNameLast( szTemp, FALSE, FALSE, TRUE );
	PutNameLast( szLine, TRUE );
	GetNameLast( szTemp2, FALSE, FALSE, TRUE );
	comp.PutNameLast( szLine, TRUE );
	if( szTemp != szTemp2 ) fNameLastDiff = TRUE;
	// First Name
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	GetNameFirst( szTemp, FALSE, FALSE, TRUE );
	PutNameFirst( szLine, TRUE );
	GetNameFirst( szTemp2, FALSE, FALSE, TRUE );
	comp.PutNameFirst( szLine, TRUE );
	if( szTemp != szTemp2 ) fNameFirstDiff = TRUE;
	// Notes
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	char charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
	if( charEnd == '\r' )
	{
		szLine = szLine.Left( szLine.GetLength() - 1 );
		szLine += _T(" ");
	}
	szTemp = szLine;
	while( charEnd == '\r' )
	{
		fRet = READ_STRING( pFile, szLine );
		if( !fRet ) return FALSE;
		charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
		if( charEnd == '\r' )
		{
			szLine = szLine.Left( szLine.GetLength() - 1 );
			szLine += _T(" ");;
		}
		szTemp += szLine;
	}
	PutNotes( szTemp );
	comp.PutNotes( szTemp );
	// Private Notes
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
	if( charEnd == '\r' )
	{
		szLine = szLine.Left( szLine.GetLength() - 1 );
		szLine += _T(" ");
	}
	szTemp = szLine;
	while( charEnd == '\r' )
	{
		fRet = READ_STRING( pFile, szLine );
		if( !fRet ) return FALSE;
		charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
		if( charEnd == '\r' )
		{
			szLine = szLine.Left( szLine.GetLength() - 1 );
			szLine += _T(" ");;
		}
		szTemp += szLine;
	}
	szLine = szTemp;
	GetPrvNotes( szTemp, FALSE, FALSE, TRUE );
	PutPrvNotes( szLine, TRUE );
	GetPrvNotes( szTemp2, FALSE, FALSE, TRUE );
	comp.PutPrvNotes( szLine, TRUE );
	if( szTemp != szTemp2 ) fNotesPrvDiff = TRUE;
	// Date Added
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	BOOL fVal = dt.ParseDateTime( szLine );
	if( fVal )
	{
		PutDateAdded( dt );
		comp.PutDateAdded( dt );
	}
	// Default Experiment
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDefaultExperiment( szLine );
	comp.PutDefaultExperiment( szLine );
	// Active?
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutActive( (BOOL)atoi( szLine ) );
	comp.PutActive( (BOOL)atoi( szLine ) );
	// Experiment Count
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutExperimentCount( (short)atoi( szLine ) );
	comp.PutExperimentCount( (short)atoi( szLine ) );
	// Encrypted
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutEncrypted( (short)atoi( szLine ) );
	comp.PutEncrypted( (short)atoi( szLine ) );
	fWasEnc = (BOOL)atoi( szLine );
	// Code
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutCode( szLine );
	comp.PutCode( szLine );
	// Encryption Method
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutEncryptionMethod( (short)atoi( szLine ) );
	comp.PutEncryptionMethod( (short)atoi( szLine ) );
	// Site ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSiteID( szLine );
	comp.PutSiteID( szLine );
	// Site Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSiteDesc( szLine );
	comp.PutSiteDesc( szLine );
	// Signature
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSignature( szLine, TRUE );
	comp.PutSignature( szLine, TRUE );
	GetSignature( szTemp );
	PutSignature( szLine, TRUE );
	GetSignature( szTemp2 );
	comp.PutSignature( szLine, TRUE );
	if( szTemp != szTemp2 ) fSigDiff = TRUE;
	// Password
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	szPass = szLine;
	GetPassword( szTemp );
	PutPassword( szLine, TRUE );
	GetPassword( szTemp2 );
	comp.PutPassword( szLine, TRUE );
	if( szTemp != szTemp2 ) fPassDiff = TRUE;

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_SUBJECT, szID, szFile1, szFile2 );
		od.m_fDiffNameLast = fNameLastDiff;
		od.m_fDiffNameFirst = fNameFirstDiff;
		od.m_fDiffNotesPrv = fNotesPrvDiff;
		od.m_fDiffSig = fSigDiff;
		od.m_fDiffPass = fPassDiff;
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		try
		{
			CFileFind ff;
			if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
			if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
		}
		catch( ... ) {}
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetNameLast( szLine, FALSE, !fWasEnc, TRUE );
			PutNameLast( szLine, !fWasEnc );
			comp.GetNameFirst( szLine, FALSE, !fWasEnc, TRUE );
			PutNameFirst( szLine, !fWasEnc );
			comp.GetNotes( szLine );
			PutNotes( szLine );
			comp.GetPrvNotes( szLine, FALSE, !fWasEnc, TRUE );
			PutPrvNotes( szLine, !fWasEnc );
			comp.GetDateAdded( dt );
			PutDateAdded( dt );
			comp.GetDefaultExperiment( szLine );
			PutDefaultExperiment( szLine );
			BOOL fVal = FALSE;
			comp.GetActive( fVal );
			PutActive( fVal );
			short nVal = 0;
			comp.GetExperimentCount( nVal );
			PutExperimentCount( nVal );
			comp.GetEncrypted( fVal );
			PutEncrypted( fVal );
			comp.GetCode( szLine );
			PutCode( szLine );
			comp.GetEncryptionMethod( nVal );
			PutEncryptionMethod( nVal );
			comp.GetSiteID( szLine );
			PutSiteID( szLine );
			comp.GetSiteDesc( szLine );
			PutSiteDesc( szLine );
			comp.GetSignature( szLine );
			if( !fWasEnc && ( szLine == _T("") ) ) szLine = ::GetSignature();
			PutSignature( szLine, !fWasEnc );
			comp.GetPassword( szLine );
			if( !fWasEnc && ( szLine == _T("") ) ) szLine = ::GetUserPassword();
			PutPassword( szLine, !fWasEnc );

			hr = Modify();
			// resolve any old, non-encrypted data in subjects
			if( !fWasEnc ) Subjects::UpdateMissingPassword( ::GetUserPassword() );
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) )
	{
		if( !fWasEnc && ( szPass == _T("") ) )
		{
			PutPassword( ::GetUserPassword(), fWasEnc );
			PutSignature( ::GetSignature(), !fWasEnc );
		}

		hr = Add();
		if( !fWasEnc ) Subjects::UpdateMissingPassword( ::GetUserPassword() );
	}
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::ForceLocal()
{
	if( !m_pSubject ) return;

	HRESULT hr = m_pSubject->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::ForceRemote()
{
	if( !m_pSubject ) return;

	HRESULT hr = m_pSubject->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::ForceDefault()
{
	if( !m_pSubject ) return;

	HRESULT hr = m_pSubject->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		Subjects dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );

		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::CleanProcessFiles( CString szExpID, CString szGrpID,
								  CString szSubjID, short nExpType, BOOL fHWR, BOOL fTF,
								  BOOL fSeg, BOOL fExt, BOOL fCon, BOOL fErr )
{
#define	NUM_FILE_EXT	10

	CFileFind ff;
	CString szItem, szMsg;
	BOOL fItem = FALSE;

	szMsg.Format( _T("Deleting processing files of experiment %s, group %s, subject %s."),
				  szExpID, szGrpID, szSubjID );
	OutputAMessage( szMsg, FALSE );

	// for PCX (image experiments)...if TF selected, we want to eliminate the HWR as well
	if( fTF && ( nExpType == EXP_TYPE_IMAGE ) ) fHWR = TRUE;

	for( int i = 0; i < NUM_FILE_EXT; i++ )
	{
		if( i == 0 )
		{
			fItem = fHWR;
			if( nExpType == EXP_TYPE_HANDWRITING )
				GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
			else if( nExpType == EXP_TYPE_IMAGE )
				GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
			else if( nExpType == EXP_TYPE_GRIPPER )
				GetFRDMask( szExpID, szGrpID, szSubjID, szItem );
		}
		else if( i == 1 )
		{
			if( nExpType == EXP_TYPE_IMAGE )
			{
				fItem = fTF;
				GetPCXMask( szExpID, szGrpID, szSubjID, szItem );
				int nPos = szItem.ReverseFind( '.' );
				szItem = szItem.Left( nPos );
				szItem += _T("-F.PCX");
			}
			else
			{
				fItem = fHWR;
				if( nExpType == EXP_TYPE_HANDWRITING )
					GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
				else if( nExpType == EXP_TYPE_GRIPPER )
					GetFRDMask( szExpID, szGrpID, szSubjID, szItem );
				int nPos = szItem.ReverseFind( '.' );
				szItem = szItem.Left( nPos + 1 );
				szItem += _T("DIS");
			}
		}
		else if( i == 2 )
		{
			fItem = fTF;
			GetTFMask( szExpID, szGrpID, szSubjID, szItem );
		}
		else if( i == 3 )
		{
			fItem = fSeg;
			GetSEGMask( szExpID, szGrpID, szSubjID, szItem );
		}
		else if( i == 4 )
		{
			fItem = fExt;
			GetSubjEXTMask( szExpID, szGrpID, szSubjID, szItem );
		}
		else if( i == 5 )
		{
			fItem = fCon;
			GetSubjCONMask( szExpID, szGrpID, szSubjID, szItem );
		}
		else if( i == 6 )
		{
			fItem = fErr;
			GetSubjERRMask( szExpID, szGrpID, szSubjID, szItem );
		}
		else if( i == 7 )
		{
			fItem = fHWR;
			GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
			int nPos = szItem.ReverseFind( '.' );
			szItem = szItem.Left( nPos + 1 );
			szItem += _T("TGT");
		}
		else if( i == 8 )
		{
			fItem = fHWR;
			GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
			int nPos = szItem.ReverseFind( '.' );
			szItem = szItem.Left( nPos + 1 );
			szItem += _T("SEQ");
		}
		else if( i == 9 )
		{
			fItem = fHWR;
			GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
			int nPos = szItem.ReverseFind( '.' );
			szItem = szItem.Left( nPos + 1 );
			szItem += _T("WRD");
		}

		if( fItem && ff.FindFile( szItem ) )
		{
			while( ff.FindNextFile() )
			{
				szItem = ff.GetFilePath();
				try
				{
					if( fHWR ) ::SendToRecycleBin( szItem );
					else CFile::Remove( szItem );
				}
				catch( ... ) {}
			}
			// Last one
			szItem = ff.GetFilePath();
			try
			{
				if( fHWR ) ::SendToRecycleBin( szItem );
				else CFile::Remove( szItem );
			}
			catch( ... ) {}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::SetTemp( BOOL fVal )
{
	if( m_pSubject ) m_pSubject->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::DumpRecord( CString szID, CString szFile )
{
	if( !m_pSubject ) return FALSE;
	return SUCCEEDED( m_pSubject->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::RemoveTemp()
{
	if( m_pSubject ) m_pSubject->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::RecoverPassword( CString szUserID, CString szUserSiteID, CString szUserSiteDesc,
							    CString szUserSig, CString szFilePath, CString& szFile, BOOL fUser )
{
	// inform user of what can happen and what is expected
	CString szMsg = _T("RECOVER PASSWORD.\n\n");
	szMsg += _T("An encrypted file will be created that you need to email to NeuroScript.\n");
	szMsg += _T("This file will NOT contain any private information.\n\n");
	szMsg += _T("NeuroScript will ensure that the request originates from the legitimate\n");
	szMsg += _T("owner of the data. Upon confirmation, NeuroScript will send (at a fee)\n");
	szMsg += _T("an unlock file which will enable you to enter a temporary password\n");
	szMsg += _T("of your choosing in order to set the new password(s).\n\n");
	szMsg += _T("Information will be encrypted in both files, and no passwords will be\n");
	szMsg += _T("stored or displayed to any user. Once used, the unlock will no longer\n");
	szMsg += _T("be usable.\n\n");
	szMsg += _T("Do you wish to continue?");
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return FALSE;

	// subject info (not used for user pass request)
	CString szSubjID, szSubjSig, szSubjPass, szSubjSiteID, szSubjSiteDesc;
	GetID( szSubjID );
	GetSignature( szSubjSig );
	GetPassword( szSubjPass );
	GetSiteID( szSubjSiteID );
	GetSiteDesc( szSubjSiteDesc );

	// user pass
	CString szUserPass = ::GetUserPassword();

	// do signatures match? (subjects only)
	if( !fUser && ( szSubjSig != szUserSig ) )
	{
		szMsg = _T("This subject was not created on this computer.\n\n");
		szMsg += _T("You will need to include evidence that this subject belongs to you and your company.\n");
		szMsg += _T("Please include this information in your email with the attached file.\n\n");
		szMsg += _T("Do you wish to continue?");
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return FALSE;
	}

	// output file (UserSubjDate.exp in UserSubjDate.zip - using pass & encryption OR userDate.exp/zip)
	COleDateTime dt = COleDateTime::GetCurrentTime();
	CString szExpFile, szZipFile;
	if( !fUser )
	{
		szExpFile.Format( _T("%s%s%s.exp"), szUserID, szSubjID, dt.Format( _T("%Y%m%d") ) );
		szZipFile.Format( _T("%s\\%s%s%s.mef"), szFilePath, szUserID, szSubjID, dt.Format( _T("%Y%m%d") ) );
	}
	else
	{
		szExpFile.Format( _T("%s%s.exp"), szUserID, dt.Format( _T("%Y%m%d") ) );
		szZipFile.Format( _T("%s\\%s%s.mef"), szFilePath, szUserID, dt.Format( _T("%Y%m%d") ) );
	}

	// temporary password
	BCGPMessageBox( _T("Enter a temporary password to use when restoring the password(s).") );
	CString szTempPass;
	PasswordChgDlg d( AfxGetMainWnd() );
	d.m_fForTempPass = TRUE;
	if( d.DoModal() == IDCANCEL ) return FALSE;
	szTempPass = d.m_szPassNew;

	// write to memory file
	CString szItem;
	CMemFile* pMemFile = new CMemFile();
	WRITE_STRING( pMemFile, _T("1\n") );	// 1 = out, 2 = in
	if( !fUser )	// 1 = subject, 2 = user
		WRITE_STRING( pMemFile, _T("1\n") );
	else
		WRITE_STRING( pMemFile, _T("2\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szTempPass, _T("rijndael"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szUserID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szUserSig, _T("rijndael"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szUserPass, _T("rijndael"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szUserSiteID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szUserSiteDesc );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szSubjID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szSubjSig, _T("rijndael"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szSubjPass, _T("rijndael"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szSubjSiteID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szSubjSiteDesc );
	WRITE_STRING( pMemFile, _T("\n") );

	// write/compress to zip file
	BOOL fRet = ::CompressAll( szFilePath, szZipFile, _T(""), szExpFile, pMemFile );
	if( fRet ) szFile += szZipFile;
	else BCGPMessageBox( _T("Error writing password recovery file.") );

	// cleanup
	BYTE* data = pMemFile->Detach();
	if( data ) delete data;
	pMemFile->Close();
	delete pMemFile;

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::RestorePassword( CString szUserID, CString szUserSiteID, CString szUserSiteDesc,
							    CString szUserSig, BOOL fUser )
{
	// vars
	BOOL fAll = FALSE, fRet = FALSE, fMatched = FALSE, fReadUser = FALSE, fCont = TRUE;
	CString szNewPass, szExpFile, szFile, szTempPass, szID, szCurID, szCurSig, szUserPass,
			szOrigSig, szInOut, szSubjID, szSubjPass, szSubjSig, szItem, szCurPass, szWho,
			szCurUserID;
	CStringList lstFiles;
	PasswordChgDlg pcd( AfxGetMainWnd() );
	PasswordDlg pd( AfxGetMainWnd() );
	CMemFile* pMemFile = NULL;
	CFileDialog fd( TRUE, _T("MEF"), _T(""), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					_T("MEF files (*.MEF)|*.MEF|"), AfxGetMainWnd() );
	HRESULT hr = E_FAIL;
	int nCount = 0;

	// inform user of what can happen and what is expected
	CString szMsg = _T("RESTORE PASSWORD.\n\n");
	szMsg += _T("You will need to select the encrypted MEF file returned to you\n");
	szMsg += _T("by NeuroScript for a password recovery. You will then be prompted\n");
	szMsg += _T("for the temporary password that you chose during the request.\n");
	szMsg += _T("Next, you will be asked for a new password and whether to apply it to\n");
	szMsg += _T("the selected subject, user, or to the user and/or all subjects\n");
	szMsg += _T("with the same unknown password.\n\n");
	szMsg += _T("Do you wish to continue?");
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) goto Cleanup;

	// get file
	BCGPMessageBox( _T("Locate and select the MEF file sent to you by NeuroScript.") );
	if( fd.DoModal() == IDCANCEL ) return FALSE;
	szFile = fd.GetPathName();

	// verify there is an EXP file in this ZIP (there should be only ONE file in the zip)
	::ZIPFileList( szFile, lstFiles );
	if( lstFiles.GetCount() > 1 ) fCont = FALSE;
	if( fCont )
	{
		szExpFile = lstFiles.GetHead();
		szItem = szExpFile;
		szItem.MakeUpper();
		if( szItem.Right( 3 ) != _T("EXP") ) fCont = FALSE;
	}
	if( !fCont )
	{
		BCGPMessageBox( _T("This does not appear to be a valid password restore file.\nCannot continue.") );
		goto Cleanup;
	}

	// extract exp into memory file
	pMemFile = ::ExtractToMemoryFile( szFile, szExpFile );
	if( !pMemFile )
	{
		BCGPMessageBox( _T("Error reading password restore file (extraction).") );
		goto Cleanup;
	}

	// read data
	READ_STRING( pMemFile, szInOut );	// 1 = in, 2 = out
	if( szInOut != _T("2") )
	{
		BCGPMessageBox( _T("Error reading password restore file (params).") );
		goto Cleanup;
	}
	READ_STRING( pMemFile, szWho );		// 1 = subject, 2 = user
	fReadUser = ( szWho == _T("2") );
	if( fReadUser != fUser )
	{
		BCGPMessageBox( _T("This does not appear to be a valid password restore file.\nCannot continue.") );
		goto Cleanup;
	}
	READ_STRING( pMemFile, szItem );	// temp pass
	szTempPass = Objects::EncDecrypt( szItem, _T("blowfish"), FALSE );
	READ_STRING( pMemFile, szCurUserID );	// user id
	if( fUser && ( szCurUserID != szUserID ) )
	{
		szMsg.Format( _T("The current user (%s) is not the same user (%s) for the password recovery."), szUserID, szCurUserID );
		BCGPMessageBox( szMsg );
		goto Cleanup;
	}
	READ_STRING( pMemFile, szItem );	// user sig
	szOrigSig = Objects::EncDecrypt( szItem, _T("blowfish"), FALSE );
//	if( fUser && ( szOrigSig != szUserSig ) )
	if( szOrigSig != szUserSig )
	{
		BCGPMessageBox( _T("The current user is not the same as the user for the password recovery.") );
		goto Cleanup;
	}
	READ_STRING( pMemFile, szItem );	// user pass
	szUserPass = Objects::EncDecrypt( szItem, _T("blowfish"), FALSE );
	READ_STRING( pMemFile, szItem );	// user site id
	READ_STRING( pMemFile, szItem );	// user site desc
	READ_STRING( pMemFile, szSubjID );	// subject id
	READ_STRING( pMemFile, szItem );	// subject sig
	szSubjSig = Objects::EncDecrypt( szItem, _T("blowfish"), FALSE );
	READ_STRING( pMemFile, szItem );	// subject pass
	szSubjPass = Objects::EncDecrypt( szItem, _T("blowfish"), FALSE );
	READ_STRING( pMemFile, szItem );	// subj site id
	READ_STRING( pMemFile, szItem );	// subj site desc
	READ_STRING( pMemFile, szItem );	// matched
	fMatched = atoi( szItem );
	if( !fMatched )
	{
		BCGPMessageBox( _T("Error reading password restore file (params).") );
		goto Cleanup;
	}

	// Temporary password (outbound is AES, inbound is blowfish)
	BCGPMessageBox( _T("Enter the temporary password you provided to NeuroScript.") );
	pd.m_szConfirm = szTempPass;
	pd.m_fSubject = TRUE;
	if( pd.DoModal() == IDCANCEL ) goto Cleanup;

	// New password
	BCGPMessageBox( _T("Enter the new password you would like to apply to the unknown password.") );
	pcd.m_fForTempPass = TRUE;
	if( pcd.DoModal() == IDCANCEL ) goto Cleanup;
	szNewPass = pcd.m_szPassNew;

	// all subjects or just this one? (or apply to all subjects who match for user)
	if( fUser )
	{
		if( BCGPMessageBox( _T("Would you like to apply this new password to all subjects with the same unknown password?"), MB_YESNO ) == IDYES )
			fAll = TRUE;
	}
	else
	{
		if( BCGPMessageBox( _T("Would you like to apply this new password to all subjects created on this computer with the same unknown password?"), MB_YESNO ) == IDYES )
			fAll = TRUE;
	}

	// if just one subject, we don't compare signatures (just in case of override)
	// outbound pass is in AES / inbound in blowfish
	if( fAll )
	{
		// update user if applicable
		if( fUser )
		{
			NSMUsers user;
			if( SUCCEEDED( user.Find( szUserID ) ) )
			{
				user.PutSysPass( szNewPass );
				hr = user.Modify();
				if( FAILED( hr ) )
				{
					BCGPMessageBox( _T("Error modifying user password.") );
					goto Cleanup;
				}
				::SetUserPassword( szNewPass );
				fRet = TRUE;
			}
			else
			{
				BCGPMessageBox( _T("Unable to locate user to update.") );
				fRet = FALSE;
			}
		}

		// loop through subjects, compare passwords (& sigs), and adjust those that match
		hr = ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			GetID( szCurID );
			GetPassword( szCurPass );
			GetSignature( szCurSig );
//			if( ( ( ( szCurID == szSubjID ) && ( szSubjSig == szCurSig ) ) || ( ( szCurSig == szUserSig ) && ( szUserSig == szOrigSig ) ) ) &&
			if( ( fUser && ( szCurPass == szUserPass ) ) ||						// user update
				( ( szSubjSig == szCurSig ) && ( szCurPass == szSubjPass ) ) )	// subject update
			{
				PutPassword( szNewPass );
				hr = Modify();
				if( FAILED( hr ) )
				{
					BCGPMessageBox( _T("Error modifying subject password.") );
					goto Cleanup;
				}
				nCount++;
			}
			hr = GetNext();
		}
		if( !fUser && ( nCount == 0 ) ) BCGPMessageBox( _T("No subjects matched the recovery criteria.\nNo subject passwords updated.") );
		else if( !fUser ) fRet = TRUE;
	}
	else
	{
		if( fUser )
		{
			NSMUsers user;
			if( SUCCEEDED( user.Find( szUserID ) ) )
			{
				user.PutSysPass( szNewPass );
				hr = user.Modify();
				if( FAILED( hr ) )
				{
					BCGPMessageBox( _T("Error modifying user password.") );
					goto Cleanup;
				}
				::SetUserPassword( szNewPass );
				fRet = TRUE;
			}
			else
			{
				BCGPMessageBox( _T("Unable to locate user to update.") );
				fRet = FALSE;
			}
		}
		else
		{
			GetID( szID );
			GetSignature( szCurSig );
			if( ( szID != szSubjID ) || ( szCurSig != szSubjSig ) )
			{
				szMsg.Format( _T("This subject does not match the subject (%s) of the password request."), szSubjID );
				BCGPMessageBox( szMsg );
				goto Cleanup;
			}
			else
			{
				PutPassword( szNewPass );
				hr = Modify();
				if( FAILED( hr ) )
				{
					BCGPMessageBox( _T("Error modifying subject password.") );
					goto Cleanup;
				}
				fRet = TRUE;
			}
		}
	}

	// cleanup
Cleanup:
	if( pMemFile )
	{
		BYTE* data = pMemFile->Detach();
		if( data ) delete data;
		pMemFile->Close();
		delete pMemFile;
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::ReadRecoverFile()
{
	// vars
	BOOL fRet = FALSE, fUser = FALSE;
	CFileDialog fd( TRUE, _T("MEF"), _T(""), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					_T("MEF files (*.MEF)|*.MEF|"), AfxGetMainWnd() );
	CString szFile, szMsg, szTmp, szExpFile, szFilePath;
	CStringList lstFiles;
	CMemFile * pMemFile = NULL;
	CString szInOut, szUserSig, szUserSiteID, szUserSiteDesc, szTempPass;
	CString szSubjID, szSubjSig, szSubjPass, szSubjSiteID, szSubjSiteDesc;
	CString szItem, szWho, szUserPass, szUserID;
	BYTE* data = NULL;
	int nPos = -1;

	// select/open file
	BCGPMessageBox( _T("Locate and select the MEF file sent to you by client.") );
	if( fd.DoModal() == IDCANCEL ) return FALSE;
	szFile = fd.GetPathName();

	// get data
	::ZIPFileList( szFile, lstFiles );
	if( lstFiles.GetCount() <= 0 ) goto Cleanup;
	szExpFile = lstFiles.GetHead();
	szTmp = szExpFile;
	szTmp.MakeUpper();
	if( szTmp.Right( 3 ) != _T("EXP") ) goto Cleanup;
	pMemFile = ::ExtractToMemoryFile( szFile, szExpFile );
	if( !pMemFile ) goto Cleanup;

	READ_STRING( pMemFile, szInOut );
	READ_STRING( pMemFile, szWho );	// 1 = subject, 2 = user
	fUser = ( szWho == _T("2") );
	READ_STRING( pMemFile, szItem );
	szTempPass = Objects::EncDecrypt( szItem, _T("rijndael"), FALSE );
	READ_STRING( pMemFile, szUserID );
	READ_STRING( pMemFile, szItem );
	szUserSig = Objects::EncDecrypt( szItem, _T("rijndael"), FALSE );
	READ_STRING( pMemFile, szItem );
	szUserPass = Objects::EncDecrypt( szItem, _T("rijndael"), FALSE );
	READ_STRING( pMemFile, szUserSiteID );
	READ_STRING( pMemFile, szUserSiteDesc );
	READ_STRING( pMemFile, szSubjID );
	READ_STRING( pMemFile, szItem );
	szSubjSig = Objects::EncDecrypt( szItem, _T("rijndael"), FALSE );
	READ_STRING( pMemFile, szItem );
	szSubjPass = Objects::EncDecrypt( szItem, _T("rijndael"), FALSE );
	READ_STRING( pMemFile, szSubjSiteID );
	READ_STRING( pMemFile, szSubjSiteDesc );
	BOOL fMatched = ( szUserSig == szSubjSig );

	// show results
	if( fUser )
	{
		szMsg = _T("USER PASSWORD RECOVERY\n");
		szMsg += _T("--------------------------------------------\n\n");
		szTmp.Format( _T("USER ID: %s\nUSER SIG: %s\nUSER PASS: %s\n"), szUserID, szUserSig, szUserPass );
		szMsg += szTmp;
		szTmp.Format( _T("USER SITE ID: %s\nUSER SITE DESC: %s\n"), szUserSiteID, szUserSiteDesc );
		szMsg += szTmp;
		szTmp.Format( _T("TEMP PASS: %s\n\n"), szTempPass );
		szMsg += szTmp;
	}
	else
	{
		szMsg = _T("SUBJECT PASSWORD RECOVERY\n");
		szMsg += _T("--------------------------------------------\n\n");
		szTmp.Format( _T("USER ID: %s\n\nUSER SIG: %s\nSUBJ SIG: %s\n"), szUserID, szUserSig, szSubjSig );
		szMsg += szTmp;
		szMsg += fMatched ? _T("******MATCH******\n\n") : _T("******DO NOT MATCH******\n\n");
		szTmp.Format( _T("USER SITE ID: %s\nSUBJ SITE ID: %s\nUSER SITE DESC: %s\nSUBJ SITE DESC: %s\n\n"),
					  szUserSiteID, szSubjSiteID, szUserSiteDesc, szSubjSiteDesc );
		szMsg += szTmp;
		szTmp.Format( _T("SUBJ ID: %s\nSUBJ PASS: %s\n\n"), szSubjID, szSubjPass );
		szMsg += szTmp;
		szTmp.Format( _T("TEMP PASS: %s\n\n"), szTempPass );
		szMsg += szTmp;
	}
	BCGPMessageBox( szMsg );
	fRet = TRUE;

	// now ask to generate response file
	if( BCGPMessageBox( _T("Generate response file?"), MB_YESNO ) == IDNO ) goto Cleanup;

	// cleanup of old memfile
	data = pMemFile->Detach();
	if( data ) delete data;
	pMemFile->Close();
	delete pMemFile;

	// generate response file
	pMemFile = new CMemFile();
	WRITE_STRING( pMemFile, _T("2\n") );
	WRITE_STRING( pMemFile, fUser ? _T("2\n") : _T("1\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szTempPass, _T("blowfish"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szUserID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szUserSig, _T("blowfish"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szUserPass, _T("blowfish"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szUserSiteID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szUserSiteDesc );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szSubjID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szSubjSig, _T("blowfish"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, Objects::EncDecrypt( szSubjPass, _T("blowfish"), TRUE ) );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szSubjSiteID );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, szSubjSiteDesc );
	WRITE_STRING( pMemFile, _T("\n") );
	WRITE_STRING( pMemFile, _T("1\n") );

	// write/compress to zip file
	nPos = szFile.ReverseFind( '\\' );
	szFilePath = szFile.Left( nPos );
	szFile = szFile.Left( szFile.GetLength() - 4 );
	szFile += _T("Response.mef");
	fRet = ::CompressAll( szFilePath, szFile, _T(""), szExpFile, pMemFile );
	if( fRet )
	{
		szMsg.Format( _T("Response file successfully generated:\n\n%s"), szFile );
		BCGPMessageBox( szMsg );
	}

	// cleanup
Cleanup:
	if( pMemFile)
	{
		data = pMemFile->Detach();
		if( data ) delete data;
		pMemFile->Close();
		delete pMemFile;
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

int Subjects::GetNextGroupNum( CString szSubjID, CString szExpID, CString szStartLetter )
{
	// verify starting letter is valid (1 char)
	if( ( szStartLetter == _T("") ) || ( szStartLetter.GetLength() > 1 ) ) return -1;

	// search from lowest num on up and see if it exists and is used
	int nNextNum = 1;

	// root path
	CString szRootPath;
	::GetDataPathRoot( szRootPath );
	// path to search for groups (in experiment folder)
	szRootPath += _T("\\");
	szRootPath += szExpID;
	// loop from min to max
	CString szGrp, szSearch;
	CFileFind ff;
	for( ; nNextNum < 99; nNextNum++ )
	{
		// search string
		if( nNextNum < 10 ) szGrp.Format( _T("%s0%d"), szStartLetter, nNextNum );
		else szGrp.Format( _T("%s%d"), szStartLetter, nNextNum );
		szSearch.Format( _T("%s\\%s"), szRootPath, szGrp );
		// search: if not found, we're done
		if( ff.FindFile( szSearch ) )
		{
			// if found, check directory for subject folder
			szSearch.Format( _T("%s\\%s\\%s"), szRootPath, szGrp, szSubjID );
			if( ff.FindFile( szSearch ) )
			{
				// if subj folder found, check for any hwr files
				// if none, we're done
				szSearch.Format( _T("%s\\%s\\%s\\*.hwr"), szRootPath, szGrp, szSubjID );
				if( !ff.FindFile( szSearch ) ) break;
			}
			// if not found, we're done
			else break;
		}
		else break;
	}

	if( nNextNum <= 99 ) return nNextNum;
	else return -1;
}

///////////////////////////////////////////////////////////////////////////////

int Subjects::GetTrialCount( CString szExpID, CString szGrpID, CString szSubjID, short nExpType )
{
	CString szFile, szBase;

	::GetSubjectMask( szExpID, szGrpID, szSubjID, szBase );
	szBase = szBase.Left( szBase.GetLength() - 1 );
	szFile = szBase;

	if( nExpType == EXP_TYPE_GRIPPER ) szFile += _T("FRD");
	else if( nExpType == EXP_TYPE_IMAGE ) szFile += _T("PCX");
	else szFile += _T("HWR");
	CFileFind ff;
	BOOL fCont = ff.FindFile( szFile );
	int nFileCount = 0;
	while( fCont )
	{
		nFileCount++;
		fCont = ff.FindNextFile();
	}

	return nFileCount;
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::GetTrialInfo( CString szExpID, CString szGrpID, CString szSubjID,
							 CStringList* pLstCond, short& nTrials, short& nGoodTrials,
							 short& nBadTrials, COleDateTime& dtExpStart, COleDateTime& dtExpEnd,
							 COleDateTime& dtProcessed, BOOL& fInSummarize, short& nExpectedTrials )
{
	CString szTrial, szPath, szCond, szTemp, szTF;
	CStringList lstConsConds, lstAllConds;
	BSTR bstr = NULL;
	COleDateTime dtTemp;
	CTime ctTime;
	CFileFind ff, ff2;
	int nImage = 0, nSelImage = 0, nPos = 0;
	BOOL fFound = FALSE;
	short nReps = 0;

	if( !pLstCond ) return;

	// initialize
	pLstCond->RemoveAll();
	nTrials = nGoodTrials = nBadTrials = nExpectedTrials = 0;
	dtExpStart = dtExpEnd = dtProcessed = DATE( 0 );
	fInSummarize = TRUE;

	// fill 'all conditions' list
	IExperimentCondition* pEC = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
									 IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCONDL_ERR );
		return;
	}
	hr = pEC->FindForExperiment( szExpID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEC->get_ConditionID( &bstr );
		szCond = bstr;
		if( !lstAllConds.Find( szCond ) ) lstAllConds.AddTail( szCond );
		pEC->get_Replications( &nReps );
		nExpectedTrials += nReps;

		hr = pEC->GetNext();
	}
	pEC->Release();

	// gripper/image exp?
	BOOL fGripper = FALSE, fImage = FALSE;
	{
		short nType = EXP_TYPE_HANDWRITING;
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExpID ) ) ) exp.GetType( nType );
		fGripper = ( nType == EXP_TYPE_GRIPPER );
		fImage = ( nType == EXP_TYPE_IMAGE );
	}

	// loop through each trial
	if( fGripper )
		::GetFRDMask( szExpID, szGrpID, szSubjID, szPath );
	else if( fImage )
		::GetPCXMask( szExpID, szGrpID, szSubjID, szPath );
	else
		::GetHWRMask( szExpID, szGrpID, szSubjID, szPath );
	BOOL fCont = ff.FindFile( szPath );
	CString szTrialTemp;
	while( fCont )
	{
		fCont = ff.FindNextFile();
		// trial name
		szTrial = ff.GetFileName();
		// if image exp. and -f in file name (processed pcx), skip
		szTrialTemp = szTrial;
		szTrialTemp.MakeUpper();
		if( szTrialTemp.Find( _T("-F") ) >= 0 ) continue;
		// trial count
		nTrials++;
		// TF file (& path)
		szTF = szPath.Left( szPath.GetLength() - 4 );		// removes ext + .
		szTF += _T(".TF");
		// trial condition
		szTemp = szTrial.Left( szTrial.GetLength() - 4 );	// removes ext + .
		szTemp = szTemp.Left( szTemp.GetLength() - 2 );		// remove trial # (2 chars)
		szCond = szTemp.Right( 3 );	// TODO - remove ID length fixed #
		// trial consistency
		if( Experiments::DidTrialPass( szTrial, szExpID, szGrpID, szSubjID, szCond, fFound ) )
		{
			if( !lstConsConds.Find( szCond ) ) lstConsConds.AddTail( szCond );
			nGoodTrials++;
		}
		else
		{
			if( nImage == IMG_TRIAL_BAD ) nBadTrials++;
		}
		// get raw data record date
		ff.GetLastWriteTime( ctTime );
		dtTemp = ctTime.GetTime();
		if( dtExpStart == 0 ) dtExpStart = dtTemp;
		if( dtTemp < dtExpStart ) dtExpStart = dtTemp;
		if( dtTemp > dtExpEnd ) dtExpEnd = dtTemp;
		// get TF date
		if( ff2.FindFile( szTF ) )
		{
			ff2.FindNextFile();
			ff2.GetLastWriteTime( ctTime );
			dtTemp = ctTime.GetTime();
			if( dtTemp > dtProcessed ) dtProcessed = dtTemp;
		}
	}

	// now find the conditions with no consistent trials
	POSITION pos = lstAllConds.GetHeadPosition();
	while( pos )
	{
		szCond = lstAllConds.GetNext( pos );
		if( !lstConsConds.Find( szCond ) )
		{
			if( !pLstCond->Find( szCond ) ) pLstCond->AddTail( szCond );
		}
	}

	// now parse the summarize exclude file for this subject (of this group)
	CString szFile;
	::GetDataPathRoot( szPath );
	szFile.Format( _T("%s\\%s\\%s-who.SUM"), szPath, szExpID, szExpID );
	if( ff.FindFile( szFile ) )
	{
		CStdioFile f;
		if( f.Open( szFile, CFile::modeRead ) )
		{
			int nPos1 = -1, nPos2 = -1;
			CString szLine, szGrp, szSubj, szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );

			while( f.ReadString( szLine ) )
			{
				nPos1 = szLine.Find( _T(" ") );
				nPos2 = szLine.Find( _T("-") );
				if( ( nPos1 != -1 ) && ( nPos2 != -1 ) )
				{
					szGrp = szLine.Mid( nPos1 + 1, nPos2 - nPos1 - 1 );
					TRIM( szGrp );
					nPos1 = szLine.Find( szDelim );
					if( nPos1 != -1 )
					{
						szSubj = szLine.Mid( nPos1 + 1 );
						TRIM( szSubj );

						if( ( szGrp == szGrpID ) && ( szSubj == szSubjID ) )
						{
							fInSummarize = FALSE;
							break;
						}
					}
				}
			}
			f.Close();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

static CString _szEmpty;
static CString _szExpID;
CString& Subjects::GetLastRunExperiment( CString szExpID, CString szSubjID, CString& szLastGroup )
{
	// data path
	CString szPath;
	::GetDataPathRoot( szPath );

	// subject
	Subjects subj;
	subj.SetDataPath( szPath );
	if( SUCCEEDED( subj.Find( szSubjID ) ) ) return subj.GetLastRunExperiment( szExpID, szLastGroup );
	else return _szEmpty;
}

CString& Subjects::GetLastRunExperiment( CString szExpID, CString& szLastGroup )
{
	_szExpID = _T("");

	// subject id
	CString szID;
	GetID( szID );

	// loop thru experiments to find latest date, and thus, last exp run
	CString szExp, szGrp;
	CTime tmLast = 0, tmFind = 0;
	Experiments exp;
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// exp id
		exp.GetID( szExp );
		// if expid specified, only check that experiment
		if( ( szExpID == _T("") ) || ( szExpID == szExp ) )
		{
			// last run of this exp time
			szGrp = _T("");
			tmFind = exp.LastRun( szID, szGrp );
			// if last time THIS exp run > last found exp run time, update
			if( tmFind > tmLast )
			{
				_szExpID = szExp;
				szLastGroup = szGrp;
				tmLast = tmFind;
			}
		}
		// get next exp
		hr = exp.GetNext();
	}

//	szLastGroup = szGrpID;
	return _szExpID;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::UpdateMissingPassword( CString szPass )
{
	if( !m_pSubject ) return FALSE;

	CString szItem;

	GetNameLast( szItem, FALSE, TRUE, TRUE );
	PutNameLast( szItem );
	GetNameFirst( szItem, FALSE, TRUE, TRUE );
	PutNameFirst( szItem );
	GetSignature( szItem, TRUE );
	PutSignature( szItem );
	PutEncrypted( TRUE );
	PutEncryptionMethod( ::GetEncryptionMethod() );
	PutPassword( szPass );
	HRESULT hr = Modify();

	return( SUCCEEDED( hr ) );
}

BOOL Subjects::UpdateMissingPasswords( CString szRootPath, CString szPass )
{
	BOOL fEnc = FALSE;
	Subjects subj;
	subj.SetDataPath( szRootPath );
	HRESULT hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetEncrypted( fEnc );
		if( !fEnc && !subj.UpdateMissingPassword( szPass ) ) return FALSE;
		hr = subj.GetNext();
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Subjects::Questionnaire( CString szExp, CString szGrp, CString szSubj )
{
	ISQuestionnaire* pQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
								   IID_ISQuestionnaire, (LPVOID*)&pQ );
	if( SUCCEEDED( hr ) )
	{
		CString szData;
		szData.Format( IDS_AL_QUEST, szSubj );
		LogToFile( szData, LOG_UI );

		IGQuestionnaire* pGQ = NULL;
		hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
							   IID_IGQuestionnaire, (LPVOID*)&pGQ );
		if( FAILED( hr ) ) return;
SearchForGroup:
		hr = pGQ->FindForGroup( szExp.AllocSysString(), szGrp.AllocSysString() );
		if( FAILED( hr ) )
		{
			if( BCGPMessageBox( IDS_NOGRP_QUEST, MB_YESNO ) == IDYES )
			{
				if( Groups::Questionnaire( szExp, szGrp ) ) goto SearchForGroup;
			}
			pQ->Release();
			pGQ->Release();
			return;
		}

		BSTR bstrID = NULL;
		CString szID2;
		CStringList ID;

		while( SUCCEEDED( hr ) )
		{
			pGQ->get_ID( &bstrID );
			szID2 = bstrID;
			ID.AddTail( szID2 );

			hr = pGQ->GetNext();
		}

		pGQ->Release();

		POSITION pos = ID.GetHeadPosition();
		while( pos )
		{
			szID2 = ID.GetNext( pos );
			if( FAILED( pQ->Find( szExp.AllocSysString(), szGrp.AllocSysString(),
								  szSubj.AllocSysString(), szID2.AllocSysString() ) ) )
			{
				pQ->put_ExperimentID( szExp.AllocSysString() );
				pQ->put_GroupID( szGrp.AllocSysString() );
				pQ->put_SubjectID( szSubj.AllocSysString() );
				pQ->put_ID( szID2.AllocSysString() );
				CString szA;
				pQ->put_Answer( szA.AllocSysString() );
				hr = pQ->Add();
				::DataHasChanged();
			}
		}

		hr = pQ->FindForSubject( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );

		SQuestionnaireFullDlg d( pQ, szExp, szGrp, AfxGetApp()->m_pMainWnd );
		if( d.DoModal() == IDOK )
		{
			// TODO: To avoid early complications, I'm simply deleting all for subject & re-adding
			if( SUCCEEDED( hr ) )
			{
				hr = pQ->ClearQuestionnaire( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );
				if( FAILED( hr ) )
				{
					BCGPMessageBox( IDS_Q_DELALLERR );
					pQ->Release();
					return;
				}
			}

			POSITION posID = d.m_lstID->GetHeadPosition();
			POSITION posA = d.m_lstA->GetHeadPosition();
			CString szIDs, szA;

			while( posID )
			{
				szIDs = d.m_lstID->GetNext( posID );
				szA = d.m_lstA->GetNext( posA );

				pQ->put_ExperimentID( szExp.AllocSysString() );
				pQ->put_GroupID( szGrp.AllocSysString() );
				pQ->put_SubjectID( szSubj.AllocSysString() );
				pQ->put_ID( szIDs.AllocSysString() );
				pQ->put_Answer( szA.AllocSysString() );
				hr = pQ->Add();
				if( FAILED( hr ) )
				{
					CString szTempMsg;
					szTempMsg.LoadString(IDS_ADD_ERR);
					BCGPMessageBox( szTempMsg+_T(" Subjects::Questionnaire") );
					break;
				}
				::DataHasChanged();
			}
		}

		pQ->Release();
	}
	else
	{
		CString szItem;
		szItem.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szItem );
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL Subjects::GenerateTrialSequence( CString szExp, CString szGrp,
									  CString szSubj, short nExpType )
{
	// if experiment type is image, exit
	if( nExpType == EXP_TYPE_IMAGE ) return FALSE;
	BOOL fGripper = ( nExpType == EXP_TYPE_GRIPPER );

	// Sequence file name
	CString szFile;
	// we'll use the ERR file to avoid writing more code
	::GetERR( szExp, szGrp, szSubj, _T(""), szFile );
	// remove the extension and add "SEQ"
	szFile = szFile.Left( szFile.GetLength() - 3 );
	szFile += _T("SEQ");

	// Get trial chronological sequence
	// MASK
	CString szMask;
	if( fGripper ) ::GetFRDMask( szExp, szGrp, szSubj, szMask );
	else ::GetHWRMask( szExp, szGrp, szSubj, szMask );
	// CHRONO TRIALS
	CStringList lstTrials;
	::SortFilesChrono( &lstTrials, szMask );
	// VERIFY COUNT
	if( lstTrials.GetCount() == 0 )
	{
//		OutputAMessage( _T("WARNING: Trial sequence generation - No trials could be found for this subject."), TRUE );
		OutputAMessage( _T("WARNING: No trials could be found for this subject."), TRUE );
		return FALSE;
	}

	// OPEN FILE
	CStdioFile f;
	if( !f.Open( szFile, CFile::modeCreate | CFile::modeWrite ) )
	{
		OutputAMessage( _T("ERROR: Trial sequence generation - Unable to open file for writing."), TRUE );
		return FALSE;
	}

	// WRITE TO FILE
	CString szLine, szTrial, szCond;
	int nTrial = 0;
	// Write Header
	szLine = _T("// TRIAL SEQUENCE - GENERATED:\n//\n");
	f.WriteString( szLine );
	// Get condition sequence
	CStringList lstCond;
	POSITION pos = lstTrials.GetHeadPosition();
	while( pos )
	{
		// get next trial
		szTrial = lstTrials.GetNext( pos );
		if( szTrial.GetLength() < 18 ) continue;
		nTrial++;
		// extract condition
		szCond = szTrial.Mid( 9, 3 );
		// add info
		lstCond.AddTail( szCond );
	}
	// write sequence
	Experiments::WriteTrialSequence( &f, &lstCond );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Subjects::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pSubject ) m_pSubject->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

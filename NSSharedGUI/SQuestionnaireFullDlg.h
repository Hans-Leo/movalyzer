#pragma once

// SQuestionnaireFullDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireFullDlg dialog

interface ISQuestionnaire;

class AFX_EXT_CLASS SQuestionnaireFullDlg : public CBCGPDialog
{
// Construction
public:
	SQuestionnaireFullDlg(ISQuestionnaire* pQ, CString szExpID, CString szGrpID, CWnd* pParent = NULL);
	~SQuestionnaireFullDlg();

// Dialog Data
	enum { IDD = IDD_QUESTS };
	CBCGPListCtrl		m_List;
	ISQuestionnaire*	m_pQ;
	CStringList*		m_lstID;
	CStringList*		m_lstQ;
	CStringList*		m_lstA;
	CStringList*		m_lstH;
	CStringList*		m_lstN;
	CStringList*		m_lstI;
	CStringList*		m_lstCH;
	CString				m_szExpID;
	CString				m_szGrpID;
	CBCGPButton			m_bnAdd;
	CBCGPButton			m_bnEdit;
	CBCGPButton			m_bnGrid;
	CBCGPToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillList();
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnEdit();
	afx_msg void OnClickBnList();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

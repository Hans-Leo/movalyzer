// WizardDevConfDev.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardDevConfDev.h"
#include "WizardDevSheet.h"
#include "WizardDevDevices.h"
#include "WizardDevMonConf.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// WizardDevConfDev dialog

IMPLEMENT_DYNAMIC(WizardDevConfDev, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardDevConfDev, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_ACQUIRE, OnBnClickedBnAcquire)
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardDevConfDev::WizardDevConfDev() : CBCGPPropertyPage( WizardDevConfDev::IDD )
{
	m_szMPP = _T("Minimum Pen &Pressure");
	m_szDev = _T("");
	m_szDim = _T("");
	m_szMaxPP = _T("");
	m_nRate = MOUSE_RATE;
	m_nPressure = MOUSE_PRESS;
	m_dResolution = MOUSE_RES;
	m_dHeightTablet = 6. * 2.54;
	m_dWidthTablet = 8. * 2.54;
}

WizardDevConfDev::~WizardDevConfDev()
{
}

void WizardDevConfDev::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_RATE, m_nRate);
	DDX_Text(pDX, IDC_EDIT_PRESSURE, m_nPressure);
	DDX_Text(pDX, IDC_EDIT_RESOLUTION, m_dResolution);
	DDX_Text(pDX, IDC_EDIT_HEIGHT_TABLET, m_dHeightTablet);
	DDV_MinMaxDouble(pDX, m_dHeightTablet, 0., 100.);
	DDX_Text(pDX, IDC_EDIT_WIDTH_TABLET, m_dWidthTablet);
	DDV_MinMaxDouble(pDX, m_dWidthTablet, 0., 100.);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Text(pDX, IDC_TXT_DEVICE, m_szDev);
	DDX_Text(pDX, IDC_TXT_DIM, m_szDim);
	DDX_Text(pDX, IDC_TXT_MPP, m_szMaxPP);
}

// WizardDevConfDev message handlers

BOOL WizardDevConfDev::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_bnCalc.SetImage( IDB_CALC );
	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_DEV_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_RATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_RATE, _T("Sampling Rate") );
	pWnd = GetDlgItem( IDC_EDIT_PRESSURE );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_PPMIN, _T("Min Pen Pressure") );
	pWnd = GetDlgItem( IDC_EDIT_RESOLUTION );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPG_RES, _T("Resolution") );
	pWnd = GetDlgItem( IDC_BN_ACQUIRE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Try to acquire device settings directly from the device."), _T("Acquire") );
	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );
	pWnd = GetDlgItem( IDC_EDIT_HEIGHT_TABLET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the height of the tablet in cm."), _T("Tablet Height") );
	pWnd = GetDlgItem( IDC_EDIT_WIDTH_TABLET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the width of the tablet in cm."), _T("Tablet Width") );

	// device
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	WizardDevDevices* pDev = (WizardDevDevices*)pParent->GetPage( 2 );
	if( pDev->m_nDevice == IDC_RDO_MOUSE )
	{
		m_nRate = MOUSE_RATE;
		m_nPressure = MOUSE_PRESS;
		m_dResolution = MOUSE_RES;
		pWnd = GetDlgItem( IDC_EDIT_HEIGHT_TABLET );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_TXT_HEIGHT );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_EDIT_WIDTH_TABLET );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_TXT_WIDTH );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_BN_CALC );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	}
	else if( pDev->m_nDevice == IDC_RDO_TABLET )
	{
		m_nRate = TABLET_RATE;
		m_nPressure = TABLET_PRESS;
		m_dResolution = TABLET_RES;
	}
	else if( pDev->m_nDevice == IDC_RDO_GRIPPER )
	{
		m_szMPP = _T("&Load (N)/Z position (cm)");
		m_nRate = GRIPPER_RATE;
		m_nPressure = GRIPPER_PRESS;
		m_dResolution = GRIPPER_RES;
		pWnd = GetDlgItem( IDC_EDIT_HEIGHT_TABLET );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_TXT_HEIGHT );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_EDIT_WIDTH_TABLET );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_TXT_WIDTH );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_BN_CALC );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	}
	
	m_dHeightTablet = pParent->m_dHeightTablet;
	m_dWidthTablet = pParent->m_dWidthTablet;

	// if we're doing this from a user that is not loaded, parent m_szUser will be defined
	// if so, then we need to pull data from the registry for that user, otherwise from 
	// the current user
	BOOL fAcquire = !::WasDeviceSetupRun();
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( !fAcquire && app )
	{
		CString szPrefix, szKey, szVal, szRB, szDef, szUser;
		::GetCurrentUser( szUser );

		if( pDev->m_nDevice == IDC_RDO_TABLET ) szPrefix = _T("Tablet_");
		else if( pDev->m_nDevice == IDC_RDO_MOUSE ) szPrefix = _T("Mouse_");
		else if( pDev->m_nDevice == IDC_RDO_GRIPPER ) szPrefix = _T("Gripper_");

		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		// sampling rate
		szKey = szPrefix;
		szKey += _T("SamplingRate");
		szVal = app->GetString( szKey, szDef );
		if( szVal == _T("") ) {
			short nRate = 0, nPress = 0;
			BOOL fTilt = FALSE;
			::GetDeviceInfo( nRate, nPress, m_dResolution, m_szDev, m_szDim, fTilt, m_szMaxPP );
			m_nRate = nRate;
			m_nPressure = nPress;
		}
		else {
			m_nRate = atoi( szVal );
			// min pen pressure
			szKey = szPrefix;
			szKey += _T("MPP");
			szDef.Format( _T("%d"), ::GetMinPenPressure() );
			szVal = app->GetString( szKey, szDef );
			m_nPressure = atoi( szVal );
			// dev resolution
			szKey = szPrefix;
			szKey += _T("Resolution");
			szDef.Format( _T("%g"), ::GetDeviceResolution() );
			szVal = app->GetString( szKey, szDef );
			if( szVal == _T("") ) szVal = szDef;
			m_dResolution = atof( szVal );
		}
		app->SetRegistryBase( szRB );
	}
	else if( !app ) fAcquire = TRUE;

	// force an acquire if not already run
	if( fAcquire ) OnBnClickedBnAcquire();

	UpdateData( FALSE );

	// show/hide images
	ShowHideImages();

	// Highlight the main sentence
	CStatic* pDis = (CStatic*)GetDlgItem( IDC_TXT_HELP );
	CDC* pDC = GetDC();
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_BOLD;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	pDis->SetFont( &font, TRUE );
	pDC->SelectObject( pOldFont );
	ReleaseDC( pDC );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void WizardDevConfDev::ShowHideImages()
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	WizardDevDevices* pDev = (WizardDevDevices*)pParent->GetPage( 2 );

	CWnd* pWnd = GetDlgItem( IDB_MOUSE );
	if( pWnd ) pWnd->ShowWindow( pDev->m_nDevice == IDC_RDO_MOUSE );
	pWnd = GetDlgItem( IDB_TABLET_CFG );
	if( pWnd ) pWnd->ShowWindow( pDev->m_nDevice == IDC_RDO_TABLET );
	pWnd = GetDlgItem( IDB_DAQPAD );
	if( pWnd ) pWnd->ShowWindow( pDev->m_nDevice == IDC_RDO_GRIPPER );
}

BOOL WizardDevConfDev::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardDevConfDev::OnWizardBack() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardDevConfDev::OnSetActive() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	WizardDevDevices* pDev = (WizardDevDevices*)pParent->GetPage( 2 );
	BOOL fShow = ( pDev->m_nDevice == IDC_RDO_TABLET );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_HEIGHT_TABLET );
	if( pWnd ) pWnd->ShowWindow( fShow ? SW_SHOW : SW_HIDE );
	pWnd = GetDlgItem( IDC_TXT_HEIGHT );
	if( pWnd ) pWnd->ShowWindow( fShow ? SW_SHOW : SW_HIDE );
	pWnd = GetDlgItem( IDC_EDIT_WIDTH_TABLET );
	if( pWnd ) pWnd->ShowWindow( fShow ? SW_SHOW : SW_HIDE );
	pWnd = GetDlgItem( IDC_TXT_WIDTH );
	if( pWnd ) pWnd->ShowWindow( fShow ? SW_SHOW : SW_HIDE );

	pWnd = GetDlgItem( IDC_BN_ACQUIRE );
	if( fShow )	// tablet
	{
		pWnd->EnableWindow( TRUE );
		m_szMPP = _T("Minimum Pen &Pressure");
		pWnd = GetDlgItem( IDC_BN_CALC );
		if( pWnd ) pWnd->ShowWindow( SW_SHOW );
	}
	else if( pDev->m_nDevice == IDC_RDO_GRIPPER )
	{
		m_szMPP = _T("&Load (N)/Z position (cm)");
		pWnd = GetDlgItem( IDC_BN_CALC );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	}
	else
	{
		m_szMPP = _T("Minimum Pen &Pressure");
		pWnd = GetDlgItem( IDC_BN_CALC );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	}

	ShowHideImages();

	BOOL fRet = CBCGPPropertyPage::OnSetActive();
	pParent->PostMessage( UM_ADJUSTBUTTONS );
	return fRet;
}

LRESULT WizardDevConfDev::OnWizardNext() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetFinishText( _T("Finish") );
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	return CBCGPPropertyPage::OnWizardNext();
}

void WizardDevConfDev::OnBnClickedBnAcquire()
{
	// which device was selected previous step
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	WizardDevDevices* pDev = (WizardDevDevices*)pParent->GetPage( 2 );
	BOOL fTablet = ( pDev->m_nDevice == IDC_RDO_TABLET );
	BOOL fMouse = ( pDev->m_nDevice == IDC_RDO_MOUSE );
	BOOL fGripper = ( pDev->m_nDevice == IDC_RDO_GRIPPER );
	m_szDev = _T("");
	m_szDim = _T("");
	m_szMaxPP = _T("");

	if( fTablet )
	{
		short nRate = 0, nPressure = 0;
		BOOL fTilt = FALSE;
		BOOL fWasTablet = ::IsTabletModeOn();
		BOOL fWasGripper = ::IsGripperModeOn();
		// need to force tablet mode on
		::SetTabletModeOn( TRUE );
		// acquire
		::GetDeviceDimensions( m_dWidthTablet, m_dHeightTablet );
		::GetDeviceInfo( nRate, nPressure, m_dResolution, m_szDev, m_szDim, fTilt, m_szMaxPP );
		m_nRate = nRate;
		m_nPressure = nPressure;
		// reset to previous device mode
		::SetTabletModeOn( fWasTablet );
		::SetGripperModeOn( fWasGripper );
	}
	else if( fMouse )
	{
		m_nRate = MOUSE_RATE;
		m_nPressure = MOUSE_PRESS;
		m_dResolution = MOUSE_RES;
		// calculate anticipated resolution: screen width / windows resolution (horiz)
		WizardDevMonConf* pMonConf = (WizardDevMonConf*)pParent->GetPage( 1 );
		double dWidth = pMonConf->m_dWidthDisplay;
		m_dResolution = GetMouseResolution( dWidth );
		m_szDev = _T("Mouse");
		double dX = 0., dY = 0.;
		::GetDisplayDimensions( dX, dY );
		m_szDim.Format( _T("Dimensions: X = %.2f cm - Y = %.2f cm"), dX, dY );
		m_szMaxPP = _T("Max Pen Pressure: N/A");
	}
	else if( fGripper )
	{
		GripperSettings gs;
		::GetGripperSettings( &gs );
		m_nRate = gs.lScanRate;
		m_nPressure = GRIPPER_PRESS;
		m_dResolution = GRIPPER_RES;
		m_szDev = _T("DAQPad");
		m_szDim = _T("Dimensions: N/A");
		m_szMaxPP = _T("Max Pen Pressure: 20 N default");
	}

	UpdateData( FALSE );

	if( this->IsWindowVisible() ) {
		CString szMsg = _T("Device settings have been acquired. The initial settings might be identical.\n\n");
		szMsg += _T("WARNING: Some recent Wacom drivers do not report the observed sampling rate, e.g., 100Hz.\n");
		szMsg += _T("Replace the sampling rate with the one observed in the recording window if necessary.");
		BCGPMessageBox( szMsg, MB_OK | MB_ICONWARNING );
	}
}

void WizardDevConfDev::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

BOOL WizardDevConfDev::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("devicesetup.html"), this );
	return TRUE;
}

BOOL WizardDevConfDev::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

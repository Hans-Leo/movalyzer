// AGraphDlg.h : header file
//
#pragma once

#include "ScatterOptionsDlg.h"
#include "FlaggingDlg.h"
#include "ScatterGroupingDlg.h"
#include "ScatterOffsetDlg.h"

#include "NSTooltipCtrl.h"

// Mode
#define	MODE_ALL			0
#define	MODE_AVERAGE		1
#define	MODE_STDDEV			2
#define	MODE_SCATTER		3
#define	MODE_COMBO			4
#define MODE_STDDEVN		5

// Summarization Export Modes
#define	EXP_SUMM_PARAM			1		// group columns by parameters
#define	EXP_SUMM_SUBM			2		// group columns by submovements

// Offset of columns for historical averages database (grp/subj/cond/trial/segment = 5)
#define HA_COL_OFFSET		5

/////////////////////////////////////////////////////////////////////////////
// AGraphDlg dialog

interface IProcess;
interface IShowStats;
class HAData;

class AFX_EXT_CLASS AGraphDlg : public CBCGPDialog
{
// Construction
public:
	AGraphDlg(CString szExp, CString szINC, BOOL fEqual, UINT nMode,
			  CWnd* pParent = NULL);
	~AGraphDlg();
	void CleanupQuests();

// Dialog Data
public:
	enum { IDD = IDD_AGRAPH };
	CBCGPComboBox	m_cboX;
	CBCGPComboBox	m_cboY;
	int				m_nX;
	int				m_nY;
	CBCGPComboBox	m_cboF1;
	int				m_nFactor1;
	BOOL			m_fAvg;
	BOOL			averagebytrial;
	BOOL			m_fStdDev;
	BOOL			m_fTrial;
	BOOL			m_fEqual;
	BOOL			m_fUseStrokes;
	int				m_nXStroke;
	int				m_nYStroke;
	BOOL			m_fScale;
	CBCGPComboBox	m_cboChart;
	int				m_nChart;
	CBCGPComboBox	m_cboColX;
	int				m_nColX;
	CBCGPComboBox	m_cboColY;
	int				m_nColY;
	BOOL			m_fInit;
	BOOL			m_fInitG;
	int				m_nXOld;
	int				m_nFactor1Old;
	CWnd*			m_pGraph;
	HWND			m_hPE;
	CString			m_szExp;
	CString			m_szINC;
	CString			m_szTMP;
	UINT			m_nMode;
	NSToolTipCtrl	m_tooltip;
	IProcess*		m_pProcess;
	IShowStats*		m_pShowStats;
	int				m_nTop;
	int				m_nLeft;
	int				m_nRightBuffer;
	int				m_nBottomBuffer;
	int				m_nToolbar;
	ScatterOptionsDlg	m_dlgOptions;
	ScatterGroupingDlg	m_dlgGrouping;
	ScatterOffsetDlg	m_dlgOffset;
	FlaggingDlg			m_dlgFlagging;
	BOOL			m_fTMPGenerated;
	CStringList		m_lstGroups;
	CStringList		m_lstSubjs;
	CStringList		m_lstConds;
	BOOL			m_fEWChg;
	int				m_nLGoBn;
	int				m_nLTxtX;
	int				m_nLX;
	int				m_nLTxtY;
	int				m_nLY;
	int				m_nLStrokes;
	int				m_nLScale;
	CStringList		m_lstAxis, m_lstAxisD;
	BOOL			m_fAssociationsDone;
	BOOL			m_fsma;
	CWnd*			m_pWndFocus;
	CBCGPMenuButton	m_bnGo;
	CMenu			m_mnuGo;
	CObList			m_lstQuests;

// Overrides
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
	virtual void DoDataExchange(CDataExchange* pDX);

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	static BOOL	ExportSummarization( CString szExpID, int nGroupMode );

	static BOOL BuildNormDB( CString szExp, CString szINC, BOOL fBypassSettings = TRUE );
	// if "fReturnZData" is specified, the caller must delete the HAData object
	static BOOL UpdateNormDB( BOOL fComputeZ, BOOL fComputeZGlobal, BOOL fCheckToUpdateDB,
							  double dPThreshold, CString szExp, CString szINC, BOOL fCompare = TRUE,
							  BOOL fReturnZData = FALSE, HAData** retData = NULL, BOOL fBypassSettings = TRUE );
protected:
	void		GenerateTmpFile( BOOL fForce = FALSE, BOOL fNoIdentities = FALSE, BOOL fBypassSettings = FALSE );
	BOOL		GetQuestions();
	IProcess*	GetProcessObject();
	IShowStats*	GetShowStatsObject();
	void		DoGraphA();
	void		DoGraphS();
	void		DoGraphCombo();
	void		OnBnOptions();
	void		OnBnGrouping();
	void		OnBnFlagging();
	void		OnBnOffset();
	void		OnBnSortG();
	void		OnBnSortC();
	void		OnBnSortS();
	void		FillAxes();
	BOOL		DoStatisticsEVA( CString szIn, CString szOut, int nX, int nY, int nGrp );
	BOOL		DoStatisticsGR1( CString szIn, CString szOut, int nX, int nY, int nGrp );
	void		SetRefresh( BOOL fEnable );
	virtual BOOL OnInitDialog();

	afx_msg void OnContextMenu( CWnd*, CPoint );
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeCboX();
	afx_msg void OnSelchangeCboFactor1();
	afx_msg void OnChkTrials();
	afx_msg void OnChkAvg();
	afx_msg void OnChkStrokes();
	afx_msg void OnClickBnRefresh();
	afx_msg void OnSelchangeCboY();
	afx_msg void OnChkEqual();
	afx_msg void OnChkEqual2();
	afx_msg void OnChkStddev();
	afx_msg void OnChangeEditXstroke();
	afx_msg void OnChangeEditYstroke();
	afx_msg void OnBnGo();
	afx_msg void OnChkScale();
	afx_msg void OnSelchangeCboChart();
	afx_msg void OnSelchangeCboColX();
	afx_msg void OnSelchangeCboColY();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnAxisCustomize();
	DECLARE_MESSAGE_MAP()
};

class Answer : public CObject
{
public:
	Answer() : CObject() { nIdx = 0; }
	int			nIdx;
	CString		szA;
};

class Question : public CObject
{
public:
	Question() : CObject() { fNum = FALSE; }
	~Question()
	{
		POSITION pos = lstA.GetHeadPosition();
		while( pos )
		{
			Answer* pA = (Answer*)lstA.GetNext( pos );
			if( pA ) delete pA;
		}
		lstA.RemoveAll();
	}
	POSITION FindAnswer( CString szAnswer )
	{
		POSITION ret = NULL;
		POSITION pos = lstA.GetHeadPosition();
		while( pos )
		{
			ret = pos;
			Answer* pA = (Answer*)lstA.GetNext( pos );
			if( pA && ( pA->szA == szAnswer ) ) return ret;
		}
		return NULL;
	}
	CString		szID;
	CString		szQ;
	CString		szHdr;
	BOOL		fNum;
	CObList		lstA;		// list of answers
};


// Historical Averaging defines
#define SIZE_DATA		300
#define SUBJ_GLOBAL		_T("---")

//************************************
// Class structures for managing data
// ...for historical averaging
//************************************
// HALine corresponds with each line of the XM1 file with z-tables for this subject & global subject
class HALine : public CObject {
public:
	// constructor
	HALine() : CObject(), m_nStroke( 0 ), m_nN( 0 ), m_fUsed( FALSE ) {
		m_pData = new double[ SIZE_DATA ];
		m_pDataAvg = new double[ SIZE_DATA ];
		m_pZSubject = new double[ SIZE_DATA ];
		m_pZGlobal = new double[ SIZE_DATA ];
		ZeroMemory( m_pData, SIZE_DATA * sizeof( double ) );
		ZeroMemory( m_pDataAvg, SIZE_DATA * sizeof( double ) );
		ZeroMemory( m_pZSubject, SIZE_DATA * sizeof( double ) );
		ZeroMemory( m_pZGlobal, SIZE_DATA * sizeof( double ) );
	}
	// destructor
	~HALine() { 
		if( m_pData ) delete [] m_pData;
		if( m_pDataAvg ) delete [] m_pDataAvg;
		if( m_pZSubject ) delete [] m_pZSubject;
		if( m_pZGlobal ) delete [] m_pZGlobal;
	}
	// member vars
	CString		m_szDate;			// date/time of this test
	double		m_dDur;				// duration of this test
	CString		m_szGrp;			// this group
	CString		m_szSubj;			// this subject
	CString		m_szCond;			// this condition
	int			m_nStroke;			// this stroke
	int			m_nN;				// "N" # of data
	double*		m_pData;			// array of XM1 data
	double*		m_pDataAvg;			// array of corresponding XMD averaves
	double*		m_pZSubject;		// Z-score table for subject
	double*		m_pZGlobal;			// Z-score table for overall
	BOOL		m_fUsed;			// flag to indicate this was processed during averaging
};

// HAData is the object containing all XM1 (HALine) data/lines
class HAData {
public:
	// constructor
	HAData() : m_nLines( 0 ), m_nNumComp( 0 ), m_nNumCompGlobal( 0 ) {
		m_dAvgSize = m_dZSize = m_dAvgDur = m_dZDur = 0.;
		m_dAvgPress = m_dZPress = m_dAvgJerk = m_dZJerk = 0.;
		m_dMaxSize = m_dMaxDuration = m_dMaxPressure = 1000;
		m_dMaxJerk = 50;
		m_dZMaxSize = m_dZMaxDuration = m_dZMaxPressure = m_dZMaxJerk = 3.09;
		m_dZ = m_dZGlobal = 0.;
	}
	// destructor
	~HAData() {
		POSITION pos = m_lstLines.GetHeadPosition();
		while( pos ) {
			HALine* line = (HALine*)m_lstLines.GetNext( pos );
			if( line ) delete line;
		}
		m_lstLines.RemoveAll();
		m_lstHeaders.RemoveAll();
	}
	// search modes
	enum haModeFind { haFindSubject = 1, haFindGlobal = 2, haFindBoth = 3 };
	// search based upon subj/cond/stroke
	HALine* Find( CString szSubj, CString szCond, int nStroke, haModeFind mode = haFindSubject ) {
		HALine* line = NULL;
		POSITION pos = m_lstLines.GetHeadPosition();
		while( pos ) {
			HALine* l = (HALine*)m_lstLines.GetNext( pos );
			if( l ) {
				BOOL fSubj = FALSE;
				switch( mode ) {
					case haFindSubject: fSubj = ( szSubj == l->m_szSubj ); break;
					case haFindGlobal: fSubj = ( szSubj == SUBJ_GLOBAL ); break;
					case haFindBoth: fSubj = ( ( szSubj == l->m_szSubj ) || ( szSubj == SUBJ_GLOBAL ) ); break;
					default: break;
				}
				if( fSubj && ( szCond == l->m_szCond ) && ( nStroke == l->m_nStroke ) ) {
					line = l;
					break;
				}
			}
		}
		return line;
	}
	// calculate new average
	void NewAvg( double dAvg1, int nN1, double dAvg2, int nN2, double& dAvgNew, int& nNNew );
	// calculate new standard deviation
	void NewSD( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2,
				double& dAvgNew, double& dSDNew, int &nNNew );
	// calculate the sums and sum of squares
	void DoSums( double dAvg, double dSD, int nN, double& dSum, double& dSumSquares );
	// Calculate Z score
	double ZMethod1( double dAvgThis, double dAvgAll, double dSD );
	double ZMethod2( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2 );
	double ZMethod3( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2 );
	double CalcZ( double dAvg1, double dSD1, int nN1, double dAvg2, double dSD2, int nN2 );
	// Get Z scores
	void GetZs( double* pAvg1, double* pSD1, int nN1, double* pAvg2, double* pSD2, int nN2, 
				double** pZs, int nNumData, BOOL fGlobal = FALSE, HALine* pLine = NULL );
	// Calculate Z from a single array
	double CalculateLineZ( double* pZs, int nNumData );
	// Get index of feature
	int GetFeatureIndex( CString szFeature, BOOL fNotAvg = FALSE );
	// Calculate columnar (feature) Z/Avg with option to distinguish condition
	double CalculateFeatureZandAvg( int nIdx, double& dAvg, CString szCond = _T(""), BOOL fGlobal = FALSE );
	double CalculateFeatureZandAvg( CString szFeature, double& dAvg, CString szCond = _T(""), BOOL fGlobal = FALSE );
	// Calculate impairment
	void CalculateImpairment();
	// calculate p-value
	double Z2P( double dZscore );
	// Output Z-scores/etc.
	void GenHeaders( CString& szHeaders, BOOL fIncludeSD = FALSE, BOOL fSkipInitial = FALSE,
					 BOOL fRemovePrefix = FALSE, BOOL fAddZPrefix = FALSE );
	void OutputImpairmentSummary();
	void OutputOverall( CStdioFile* f, CStdioFile* fH, BOOL fGlobal = FALSE );
	void OutputByCondition( CStdioFile* f, BOOL fGlobal = FALSE );
	void OutputByStroke( CStdioFile* f, BOOL fGlobal = FALSE );

	// member vars
	CString		m_szExp;
	int			m_nLines;			// # of lines of data
	CStringList	m_lstHeaders;		// list of headers that maps to columns of data
	CObList		m_lstLines;			// lines of data
	int			m_nNumComp;
	int			m_nNumCompGlobal;
	// impairment data
	double		m_dZ;
	double		m_dZGlobal;
	double		m_dMaxSize;
	double		m_dMaxDuration;
	double		m_dMaxPressure;
	double		m_dMaxJerk;
	double		m_dZMaxSize;
	double		m_dZMaxDuration;
	double		m_dZMaxPressure;
	double		m_dZMaxJerk;
	double		m_dAvgSize;
	double		m_dZSize;
	double		m_dAvgDur;
	double		m_dZDur;
	double		m_dAvgPress;
	double		m_dZPress;
	double		m_dAvgJerk;
	double		m_dZJerk;
};

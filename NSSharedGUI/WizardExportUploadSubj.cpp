// WizardExportUploadSubj.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardExportUploadSubj.h"
#include "WizardExportExp.h"
#include "WizardExportSheet.h"
#include "Experiments.h"
#include "Subjects.h"
#include "Groups.h"

struct UploadStruct
{
	CString	szGrpID;
	CString szSubjID;
	CString	szSubj;
	CString szCode;
	CString	szUp;
	CString szUpWhen;
};

typedef struct
{
	int nColumn;
	BOOL bAscending;
} USORTINFO;

// WizardExportUploadSubj dialog

IMPLEMENT_DYNAMIC(WizardExportUploadSubj, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardExportUploadSubj, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_EXPS, &WizardExportUploadSubj::OnCbnSelchangeCboExps)
	ON_BN_CLICKED(IDC_CHK_HIDE, &WizardExportUploadSubj::OnBnClickedChkHide)
	ON_WM_HELPINFO()
	ON_NOTIFY(NM_CLICK, IDC_LIST, &WizardExportUploadSubj::OnNMClickList)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST, &WizardExportUploadSubj::OnLvnColumnclickList)
END_MESSAGE_MAP()

WizardExportUploadSubj::WizardExportUploadSubj() : CBCGPPropertyPage( WizardExportUploadSubj::IDD )
{
	m_fHideUploaded = TRUE;
	m_nCurCol = -1;
	m_nLastCol = -1;
	m_fAscending = TRUE;
}

WizardExportUploadSubj::~WizardExportUploadSubj()
{
}

void WizardExportUploadSubj::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_EXPS, m_Cbo);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Check(pDX, IDC_CHK_HIDE, m_fHideUploaded);
}

// WizardExportUploadSubj message handlers

BOOL WizardExportUploadSubj::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.SetTitle( _T("Export Wizard") );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	m_tooltip.AddTool( pWnd, _T("Select an existing experiment."), _T("Experiment") );
	pWnd = GetDlgItem( IDC_LIST );
	m_tooltip.AddTool( pWnd, _T("Select the subjects that you would like to upload."), _T("Select Subjects") );
	pWnd = GetDlgItem( IDC_CHK_HIDE );
	m_tooltip.AddTool( pWnd, _T("Check to hide subjects that have already been uploaded."), _T("Hide/Show Subjects") );

	// currently selected experiment (if set)
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	WizardExportExp* pExp = (WizardExportExp*)pParent->GetPage( 0 );
	// Fill experiment list
	Experiments exp;
	if( !exp.IsValid() ) return TRUE;
	CString szID, szItem, szExp = pExp->m_szExpID, szGrp, szSubj;
	int nSel = -1, nItem = -1;
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		exp.GetID( szID );
		exp.GetDescriptionWithID( szItem );

		nItem = m_Cbo.AddString( szItem );
		if( szExp == szID ) nSel = nItem;

		hr = exp.GetNext();
	}
	if( ( nSel == -1 ) && ( nItem != -1 ) ) nSel = 0;

	// image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );

	// Column Headers
	m_list.InsertColumn( 0, _T("ID"), LVCFMT_LEFT, 50, 0 );
	m_list.InsertColumn( 1, _T("Code"), LVCFMT_LEFT, 80, 1 );
	m_list.InsertColumn( 2, _T("Group"), LVCFMT_LEFT, 50, 2 );
	m_list.InsertColumn( 3, _T("Name"), LVCFMT_LEFT, 100, 3 );
	m_list.InsertColumn( 4, _T("When"), LVCFMT_LEFT, 100, 4 );

	if( nSel != -1 )
	{
		// select experiment
		m_Cbo.SetCurSel( nSel );
		// fill list (as if someone selected an experiment from combo)
		OnCbnSelchangeCboExps();
		// select subjects that were exported
		CString szItem, szExp, szGrp, szSubj;
		LVFINDINFO fi;
		fi.flags = LVFI_STRING;
		int nItem = -1;
		LVITEM lvi;
		lvi.mask = LVIF_STATE;
		lvi.state = LVIS_SELECTED;
		lvi.stateMask = (UINT)-1;
		POSITION pos = pExp->m_lstSubjects.GetHeadPosition();
		while( pos )
		{
			szItem = pExp->m_lstSubjects.GetNext( pos );
			szExp = szItem.Left( 3 );
			szGrp = szItem.Mid( 3, 3 );
			szSubj = szItem.Right( 3 );
			fi.psz = szSubj.GetBuffer();
			nItem = m_list.FindItem( &fi );
			if( nItem != -1 ) m_list.SetItemState( nItem, &lvi );
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardExportUploadSubj::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardExportUploadSubj::OnWizardNext() 
{
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if( !pos ) return FALSE;
	CString szID, szDesc, szGrpID, szItem;
	short nIdx = 0;

	while( pos )
	{
		nIdx = m_list.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szID = m_list.GetItemText( nIdx, 0 );
			szGrpID = m_list.GetItemText( nIdx, 2 );
			szDesc = m_list.GetItemText( nIdx, 3 );

			CString szDelim;
			::GetDelimiter( szDelim );
			szItem.Format( _T("%s%s%s"), m_szExpID, szGrpID, szID );
			m_lstSubjects.AddTail( szItem );
		}
	}

	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardExportUploadSubj::OnWizardFinish()
{
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		UploadStruct* pItem = (UploadStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}

	return CBCGPPropertyPage::OnWizardFinish();
}

BOOL WizardExportUploadSubj::OnSetActive() 
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if( pos ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

void WizardExportUploadSubj::OnCbnSelchangeCboExps()
{
	if( m_Cbo.GetCurSel() < 0 ) return;
	FillList();
}

void WizardExportUploadSubj::FillList()
{
	CString szItem, szDelim, szID, szDesc;

	// empty first
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		UploadStruct* pItem = (UploadStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
	m_list.DeleteAllItems();

	// delim
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// get exp info from selection combo
	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	szID = szItem.Mid( nPos + 2 );
	m_szExpID = szID;
	szDesc = szItem.Left( nPos - 1 );

	// exp member list object
	HRESULT hr = E_FAIL;
	IExperimentMember*	pEML = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEML );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return;
	}

	// Now get all groups/subjects for this experiment (loop through)
	BSTR bstr = NULL;
	CString szGrpID, szGrp, szSubjID, szNameLast, szNameFirst, szCode;
	DATE dt;
	COleDateTime dtWhen;
	int nItem = 0;
	BOOL fUpl = FALSE, fExp = FALSE;
	Subjects subj;
	hr = pEML->FindForExperiment( szID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		hr = pEML->get_GroupID( &bstr );
		szGrpID = bstr;
		if( SUCCEEDED( hr ) && ( bstr != NULL ) && szGrpID != _T("") )
		{
			// Get data
			pEML->get_SubjectID( &bstr );
			szSubjID = bstr;
			hr = subj.Find( szSubjID );
			if( SUCCEEDED( hr ) )
			{
				subj.GetCode( szCode );
				subj.GetNameLast( szNameLast, ::IsPrivacyOn() );
				subj.GetNameFirst( szNameFirst, ::IsPrivacyOn() );
				pEML->get_Uploaded( &fUpl );
				pEML->get_UpWhen( &dt );
				pEML->get_Exported( &fExp );
				dtWhen = dt;
				szItem.Format( _T("%s, %s"), szNameLast, szNameFirst );

				if( ( !m_fHideUploaded || ( m_fHideUploaded && !fUpl ) ) && fExp )
				{
					UploadStruct* pUS = new UploadStruct;
					pUS->szCode = szCode;
					pUS->szGrpID = szGrpID;
					pUS->szSubjID = szSubjID;
					pUS->szSubj = szItem;
					pUS->szUp = fUpl ? _T("YES") : _T("NO");
					pUS->szUpWhen = dtWhen.Format( _T("%Y-%m-%d") );

					nItem = m_list.InsertItem( nItem, pUS->szSubjID, 4 );
					m_list.SetItemData( nItem, (LPARAM)pUS );
					if( nItem != -1 )
					{
						m_list.SetItem( nItem, 1, LVIF_TEXT, pUS->szCode, 0, 0, 0, NULL );
						m_list.SetItem( nItem, 2, LVIF_TEXT, pUS->szGrpID, 0, 0, 0, NULL );
						m_list.SetItem( nItem, 3, LVIF_TEXT, pUS->szSubj, 0, 0, 0, NULL );
						m_list.SetItem( nItem, 4, LVIF_TEXT, fUpl ? pUS->szUpWhen : _T("N/A"), 0, 0, 0, NULL );
					}
				}
			}
		}
		hr = pEML->GetNext();
	}

	pEML->Release();

	OnSetActive();
}

void WizardExportUploadSubj::OnBnClickedChkHide()
{
	UpdateData( TRUE );
	FillList();
}

BOOL WizardExportUploadSubj::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("export_wizard.html"), this );
	return TRUE;
}

BOOL WizardExportUploadSubj::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardExportUploadSubj::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if( pos ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );
	*pResult = 0;
}

void WizardExportUploadSubj::OnCancel()
{
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		UploadStruct* pItem = (UploadStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
}

void WizardExportUploadSubj::OnOK()
{
	int nCount = m_list.GetItemCount();
	for( int i = 0; i < nCount; i++ )
	{
		UploadStruct* pItem = (UploadStruct*)m_list.GetItemData( i );
		if( pItem ) delete pItem;
	}
}

int CALLBACK WizardExportUploadSubj::CompareFunc( LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort )
{
	UploadStruct* pItem1 = (UploadStruct*)lParam1;
	UploadStruct* pItem2 = (UploadStruct*)lParam2;
	USORTINFO* pSortInfo = (USORTINFO*)lParamSort;
	ASSERT( pItem1 && pItem2 && pSortInfo );

	int result = 0;
	switch( pSortInfo->nColumn )
	{
		case 0:
			result = pItem1->szSubjID.Compare( pItem2->szSubjID );
			break;
		case 1:
			result = pItem1->szCode.Compare( pItem2->szCode );
			break;
		case 2:
			result = pItem1->szGrpID.Compare( pItem2->szGrpID );
			break;
		case 3:
			result = pItem1->szSubj.Compare( pItem2->szSubj );
			break;
		case 4:
			result = pItem1->szUp.Compare( pItem2->szUp );
			break;
		case 5:
			result = pItem1->szUpWhen.Compare( pItem2->szUpWhen );
			break;
		default:
			ASSERT( FALSE );
	}

	if( !pSortInfo->bAscending )
	result = -result;

	return result;
}

void WizardExportUploadSubj::OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	CWaitCursor waiter;

	USORTINFO si;
	si.nColumn = pNMLV->iSubItem;

	m_nLastCol = m_nCurCol;
	m_nCurCol = si.nColumn;

	if( m_nCurCol == m_nLastCol ) m_fAscending = !m_fAscending;
	else m_fAscending = TRUE;
	si.bAscending = m_fAscending;

	m_list.SortItems( CompareFunc, (LPARAM)&si );

	*pResult = 0;
}

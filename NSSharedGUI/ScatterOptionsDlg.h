#pragma once

#include "NSTooltipCtrl.h"

// ScatterOptionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ScatterOptionsDlg dialog

class AFX_EXT_CLASS ScatterOptionsDlg : public CBCGPDialog
{
// Construction
public:
	ScatterOptionsDlg(CString szINC, BOOL fScatter = TRUE, CWnd* pParent = NULL);
	~ScatterOptionsDlg();

// Dialog Data
	enum { IDD = IDD_SCATOPTIONS };
	int				m_nGrp;
	int				m_nSubj;
	int				m_nCond;
	int				m_nTrial;
	int				m_nStroke;
	int				m_nSubMvmt;
	int				m_nDisp;
	int				m_nDisp2;
	double			m_dStrength;
	double			m_dStrength2;
	BOOL			m_fDispX;
	BOOL			m_fDispY;
	BOOL			m_fExcludeX;
	double			m_dExcludeValX;
	BOOL			m_fExcludeY;
	double			m_dExcludeValY;
	BOOL			m_fScatter;
	CString			m_szFile;
	int				m_nGrp2;
	int				m_nSubj2;
	int				m_nCond2;
	int				m_nTrial2;
	int				m_nStroke2;
//	int				m_nSubMvmt2;
	double*			m_pDisps;
	double*			m_pDisps2;
	CString			m_szX;
	CString			m_szY;
	CStringList		m_lstGrp;
	CStringList		m_lstSubj;
	CStringList		m_lstCond;
	CStringList		m_lstTrial;
	CStringList		m_lstStroke;
//	CStringList		m_lstSubMvmt;
	BOOL			m_fHasChanged;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void OnOK();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnChkExcludeX();
	afx_msg void OnChkExcludeY();
	afx_msg void OnChange();
	afx_msg void OnSelchangeCboDisp();
	afx_msg void OnKillfocusEditStrength();
	afx_msg void OnChkDispx();
	afx_msg void OnChkDispy();
	afx_msg void OnKillfocusEditStrength2();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

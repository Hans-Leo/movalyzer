#pragma once

#include "AutoRichEditCtrl.h"

// InstructionDlg dialog
class AFX_EXT_CLASS InstructionDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(InstructionDlg)

public:
	InstructionDlg(CWnd* pParent = NULL);
	virtual ~InstructionDlg();

// Dialog Data
	enum { IDD = IDD_INSTRUCTION };
	BOOL				m_fCond;
	CString				m_szExpID;
	CString				m_szFile;
	CString				m_szFileRTF;
	CString				m_szFileName;	// no extension (unless just displaying)
	BOOL				m_fReadOnly;
	BOOL				m_fOld;
	CBCGPFontComboBox	m_cboFont;
	CComboBox			m_cboFontSize;
	CBCGPButton			m_bnBold;
	CBCGPButton			m_bnItalic;
	CBCGPButton			m_bnUnderline;
	CBCGPButton			m_bnLeft;
	CBCGPButton			m_bnCenter;
	CBCGPButton			m_bnRight;
	CBCGPColorButton	m_bnColor;
	CBCGPButton			m_bnPic;
	CBCGPButton			m_bnPrint;
	BOOL				m_fJustDisplay;
	int					m_nTop;
	int					m_nLeft;
	int					m_nRightBuffer;
	int					m_nBottomBuffer;
	int					m_nTopBuffer;
	int					m_nBnBuffer;
protected:
	CAutoRichEditCtrl	m_re;
	CToolTipCtrl		m_tooltip;

public:
	CString GetFile();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	virtual void OnCancel();
	void Convert();
	BOOL PrintRich( const CString& Title );

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnEnSelchangeRe(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeCboFont();
	afx_msg void OnCbnSelchangeCboFontSize();
	afx_msg void OnClickBold()			{ m_re.SetSelectionBold(); }
	afx_msg void OnClickItalic()		{ m_re.SetSelectionItalic(); }
	afx_msg void OnClickUnderline()		{ m_re.SetSelectionUnderlined(); }
	afx_msg void OnClickAlign();
	afx_msg void OnChangeColor();
	afx_msg void OnClickPic();
	afx_msg void OnClickPrint();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
};

// rich edit callback class for enabling images within the rich edit control
#include <richole.h>
#include "afxwin.h"
class REOLECallback : IRichEditOleCallback
{
public:
	// Constructor / Destructor
	REOLECallback () { mRefCounter = 0; }
	~REOLECallback () {}

	// Methods of the IUnknown interface
	STDMETHOD_(ULONG, AddRef) (void)
	{
		mRefCounter++;
		return mRefCounter;
	}
	STDMETHOD_(ULONG, Release) (void)
	{
		if ( --mRefCounter == 0 && this )
		{
			delete this;
			return 0;
		}
		return mRefCounter;
	}
	STDMETHOD(QueryInterface) (REFIID iid, void** ppvObject)
	{
		if (iid == IID_IUnknown || iid == IID_IRichEditOleCallback) { *ppvObject = this; AddRef(); return S_OK; }
		else return E_NOINTERFACE;
	}

	// Methods of the IRichEditOleCallback interface
	STDMETHOD(ContextSensitiveHelp) (BOOL fEnterMode) { return E_NOTIMPL; }
	STDMETHOD(DeleteObject) (LPOLEOBJECT lpoleobj) { return E_NOTIMPL; }
	STDMETHOD(GetClipboardData) (CHARRANGE FAR *lpchrg, DWORD reco, LPDATAOBJECT FAR *lplpdataobj) { return E_NOTIMPL; }
	STDMETHOD(GetContextMenu) (WORD seltype, LPOLEOBJECT lpoleobj, CHARRANGE FAR *lpchrg, HMENU FAR *lphmenu) { return E_NOTIMPL; }
	STDMETHOD(GetDragDropEffect) (BOOL fDrag, DWORD grfKeyState, LPDWORD pdwEffect) { return E_NOTIMPL; }
	STDMETHOD(GetInPlaceContext) (LPOLEINPLACEFRAME FAR *lplpFrame, LPOLEINPLACEUIWINDOW FAR *lplpDoc, LPOLEINPLACEFRAMEINFO lpFrameInfo) { return E_NOTIMPL; }
	STDMETHOD(GetNewStorage) (LPSTORAGE FAR *lplpstg);
	STDMETHOD(QueryAcceptData) (LPDATAOBJECT lpdataobj, CLIPFORMAT FAR *lpcfFormat, DWORD reco, BOOL fReally, HGLOBAL hMetaPict) { return E_NOTIMPL; }
	STDMETHOD(QueryInsertObject) (LPCLSID lpclsid, LPSTORAGE lpstg, LONG cp) { return S_OK; }
	STDMETHOD(ShowContainerUI) (BOOL fShow) { return E_NOTIMPL; }

	// Data
private:
	ULONG mRefCounter;
};

#pragma warning( disable: 4312 )
static DWORD CALLBACK StreamInCallback( DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb )
{
   CFile *pFile = (CFile*)dwCookie;
	if( !pFile ) return 0;
   *pcb = pFile->Read( pbBuff, cb );
   return 0;
}

static DWORD CALLBACK StreamOutCallback( DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb )
{
   CFile *pFile = (CFile*)dwCookie;
	if( !pFile ) return 0;
   pFile->Write( pbBuff, cb );
	*pcb = cb;
   return 0;
}
#pragma warning( default: 4312 )

// NotesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "NotesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NotesDlg dialog

BEGIN_MESSAGE_MAP(NotesDlg, CBCGPDialog)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

NotesDlg::NotesDlg(UINT nType, CString szItem, CString szFile, CWnd* pParent)
	: CBCGPDialog(NotesDlg::IDD, pParent), m_nType( nType ),
	  m_szItem( szItem ), m_szFile( szFile )
{
	m_szNotes = _T("");
}

void NotesDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_NOTES, m_szNotes);
	DDV_MaxChars(pDX, m_szNotes, 500);
}

/////////////////////////////////////////////////////////////////////////////
// NotesDlg message handlers

BOOL NotesDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter any descriptive text here.") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// TITLE: Based on type
	CString szItem;
	if( m_nType == N_EXPERIMENT ) szItem = _T("Experiment");
	else if( m_nType == N_GROUP ) szItem = _T("Group");
	else if( m_nType == N_SUBJECT ) szItem = _T("Subject");
	else if( m_nType == N_CONDITION ) szItem = _T("Condition");
	else if( m_nType == N_BACKUP ) szItem = _T("Backup");
	else if( m_nType == N_TRIAL ) szItem = _T("Trial");

	CString szTitle;
	if( m_nType == N_INSTRUCTIONS )
		szTitle.Format( _T("Instructions For Experiment %s"), szItem, m_szItem );
	else
		szTitle.Format( _T("Notes For %s %s"), szItem, m_szItem );
	SetWindowText( szTitle );

	// Open file (if exists) and read contents
	CStdioFile f;
	if( f.Open( m_szFile, CFile::modeRead ) )
	{
		while( f.ReadString( szItem ) )
		{
			m_szNotes += szItem;
			m_szNotes += _T("\n");
		}
		m_szNotes += _T("\r\n");
		UpdateData( FALSE );
		m_fExists = TRUE;
	}
	else m_fExists = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL NotesDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void NotesDlg::OnOK()
{
	if( m_nType != N_BACKUP )
	{
		if( m_fExists ) CFile::Remove( m_szFile );

		UpdateData( TRUE );
		if( m_szNotes != _T("") )
		{
			CStdioFile f;
			if( f.Open( m_szFile, CFile::modeWrite | CFile::modeCreate ) )
			{
				f.WriteString( m_szNotes );
				f.WriteString( _T("\r\n") );
			}
			else BCGPMessageBox( _T("Unable to write notes file.") );
		}
	}

	CBCGPDialog::OnOK();
}

BOOL NotesDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("extendednotes.html"), this );
	return CBCGPDialog::OnHelpInfo( pHelpInfo );
}

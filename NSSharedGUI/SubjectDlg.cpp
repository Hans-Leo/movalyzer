// SubjectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "RelationshipDlg.h"
#include "NotesDlg.h"
#include "SubjectDlg.h"
#include "PasswordChgDlg.h"
#include "Experiments.h"
#include "Users.h"
#include "..\CTreeLink\DBCommon.h"
#include "Subjects.h"
#include "UserMessages.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SubjectDlg dialog

BEGIN_MESSAGE_MAP(SubjectDlg, CBCGPDialog)
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnKillfocusEditId)
	ON_EN_KILLFOCUS(IDC_EDIT_SITEID, OnKillfocusEditSiteId)
	ON_WM_HELPINFO()
	ON_EN_KILLFOCUS(IDC_EDIT_CODE, OnKillfocusEditCode)
	ON_EN_CHANGE( IDC_EDIT_CODE, OnChangeCode )
	ON_BN_CLICKED(IDC_BN_NOTES, OnClickNotes)
	ON_CONTROL(STN_CLICKED, IDC_TXT_EDIT, OnClickEdit)
	ON_CONTROL(STN_CLICKED, IDC_TXT_EDIT2, OnClickEdit2)
	ON_BN_CLICKED(IDC_BN_PASS, OnClickPassword)
	ON_BN_CLICKED(IDC_BN_RECOVER, OnClickRecover)
END_MESSAGE_MAP()

SubjectDlg::SubjectDlg(CWnd* pParent, BOOL fNew)
	: CBCGPDialog(SubjectDlg::IDD, pParent), m_fNew( fNew ), m_fActive( FALSE ), m_nExpCount( 0 )
{
	m_fInit = FALSE;
	m_szID = _T("");
	m_szCode = _T("");
	m_szFirst = _T("-");
	m_szLast = _T("-");
	m_szNotes = _T("");
	m_szPrvNotes = _T("");
	m_DateAdded = COleDateTime::GetCurrentTime();
	m_dDevRes = 0.;
	m_nSampRate = 0;
	m_fEditDRSR = FALSE;
	m_nEncryptionMethod = ENC_NONE;
	m_szSiteID = ::GetCurrentUserSiteID();
	m_szSiteDesc = ::GetCurrentUserSiteDesc();
	m_fEditCode = FALSE;
}

void SubjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Control(pDX, IDC_EDIT_FIRST, m_editFirst);
	DDX_Text(pDX, IDC_EDIT_FIRST, m_szFirst);
	DDV_MaxChars(pDX, m_szFirst, szSUBJECT_FIRST);
	DDX_Control(pDX, IDC_EDIT_LAST, m_editLast);
	DDX_Text(pDX, IDC_EDIT_LAST, m_szLast);
	DDV_MaxChars(pDX, m_szLast, szSUBJECT_LAST);
	DDX_Text(pDX, IDC_EDIT_NOTES, m_szNotes);
	DDV_MaxChars(pDX, m_szNotes, szNOTES);
	DDX_Text(pDX, IDC_EDIT_CODE, m_szCode);
	DDV_MaxChars(pDX, m_szCode, szCODE);
	DDX_Control(pDX, IDC_EDIT_PRVNOTES, m_editPrvNotes);
	DDX_Text(pDX, IDC_EDIT_PRVNOTES, m_szPrvNotes);
	DDV_MaxChars(pDX, m_szPrvNotes, szNOTES);
	DDX_Control(pDX, IDC_CBO_DEFEXP, m_cboDefExp);
	DDX_Text(pDX, IDC_EDIT_COUNT, m_nExpCount);
	DDX_Text(pDX, IDC_EDIT_DEVRES, m_dDevRes);
	DDX_Text(pDX, IDC_EDIT_SAMPRATE, m_nSampRate);
	DDX_Control(pDX, IDC_TXT_EDIT, m_linkEdit);
	DDX_Control(pDX, IDC_TXT_EDIT2, m_linkEdit2);
	DDX_Text(pDX, IDC_EDIT_SITEID, m_szSiteID );
	DDX_Text(pDX, IDC_EDIT_SITEDESC, m_szSiteDesc );
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_ctrlDateTime);
	DDX_Check(pDX, IDC_CHK_ACTIVE, m_fActive);
}

/////////////////////////////////////////////////////////////////////////////
// SubjectDlg message handlers

BOOL SubjectDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	if( ::IsAppRx() )
	{
		// change subject to patient for Rx
		SetWindowText( _T("Patient") );

		CWnd* pTxt = GetDlgItem( IDC_TXT_ID );
		if( pTxt ) pTxt->SetWindowText( _T("Patient ID:") );
		pTxt = GetDlgItem( IDC_TXT_DESC );
		if( pTxt ) pTxt->SetWindowText( _T("Patient Code:") );
	}

	m_linkEdit.SetImage( IDB_EDIT );
	m_linkEdit.SizeToContent();
	m_linkEdit2.SetImage( IDB_EDIT );
	m_linkEdit2.SizeToContent();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_SUBJ );
	m_tooltip.SetTitle( _T("SUBJECT PROPERTIES") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( ::IsAppRx() && pWnd ) {
		m_tooltip.AddTool( pWnd, IDS_SUBJ_ID, _T("Patient ID") );
		CWnd* pNext = pWnd->GetNextWindow( GW_HWNDNEXT );
		CString szTxt;
		while( pNext ) {
			pNext->GetWindowText( szTxt );
			szTxt.MakeUpper();
			if( szTxt == _T("DE&FAULT EXPERIMENT:") )
			{
				pNext->SetWindowText( _T("De&fault Test:" ) );
				break;
			}
			pNext = pNext->GetNextWindow( GW_HWNDNEXT );
		}
	}
	else if( pWnd ) {
		m_tooltip.AddTool( pWnd, IDS_SUBJ_ID, _T("Subject ID") );
	}
	pWnd = GetDlgItem( IDC_EDIT_FIRST );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_SUBJ_FIRST, _T("First Name / Alias") );
	pWnd = GetDlgItem( IDC_EDIT_LAST );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_SUBJ_LAST, _T("Last Name / Alias") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Save/commit your changes"), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_EDIT_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Any additional public information you would like."), _T("Notes") );
	if( ::IsAppRx() )
	{
		pWnd = GetDlgItem( IDC_EDIT_CODE );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unique patient identification code. Defaults to patient ID."), _T("Patient Code") );
		pWnd = GetDlgItem( IDC_CBO_DEFEXP );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a default test for this patient."), _T("Default Test") );
// 		pWnd = GetDlgItem( IDC_EDIT_COUNT );
// 		if( pWnd ) m_tooltip.AddTool( pWnd, _T("This value is updated each time a patient participates in an experiment. It cannot be modified."), _T("Experiment Count") );
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add some extended notes to the patient."), _T("Extended Notes") );
 		pWnd = GetDlgItem( IDC_TXT_EDIT );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the patient-level device resolution and sampling rate.\nCAUTION: Changing these values can result in improper processing!"), _T("Edit Device Settings") );
 		pWnd = GetDlgItem( IDC_TXT_EDIT2 );
 		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the patient-level site ID and description."), _T("Edit Site Information") );
	}
	else
	{
		pWnd = GetDlgItem( IDC_EDIT_CODE );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unique subject identification code. Defaults to subject ID."), _T("Subject Code") );
		pWnd = GetDlgItem( IDC_CBO_DEFEXP );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a default experiment for this subject."), _T("Default Experiment") );
// 		pWnd = GetDlgItem( IDC_EDIT_COUNT );
// 		if( pWnd ) m_tooltip.AddTool( pWnd, _T("This value is updated each time a subject participates in an experiment. It cannot be modified."), _T("Experiment Count") );
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add some extended notes to the subject."), _T("Extended Notes") );
 		pWnd = GetDlgItem( IDC_TXT_EDIT );
 		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the subject-level device resolution and sampling rate.\nCAUTION: Changing these values can result in improper processing!"), _T("Edit Device Settings") );
 		pWnd = GetDlgItem( IDC_TXT_EDIT2 );
 		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the subject-level site ID and description."), _T("Edit Site Information") );
	}
	pWnd = GetDlgItem( IDC_BN_PASS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to change the password for this subject/patient"), _T("Change Password") );
	pWnd = GetDlgItem( IDC_EDIT_PRVNOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Any additional private information you would like."), _T("Private Notes") );
	pWnd = GetDlgItem( IDC_EDIT_DEVRES );
	if( pWnd )
	{
		if( m_fNew || ( m_szExpID == _T("") ) )
		{
			pWnd->ShowWindow( SW_HIDE );
			pWnd = GetDlgItem( IDC_TXT_DEVRES );
			if( pWnd ) pWnd->ShowWindow( SW_HIDE );
			pWnd = GetDlgItem( IDC_TXT_EDIT );
			if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		}
		else if( ::IsAppRx() ) m_tooltip.AddTool( pWnd, _T("Device resolution used by patient in experiment."), _T("Device Resolution") );
		else m_tooltip.AddTool( pWnd, _T("Device resolution used by subject in experiment."), _T("Device Resolution") );
	}
	pWnd = GetDlgItem( IDC_EDIT_SAMPRATE );
	if( pWnd )
	{
		if( m_fNew || ( m_szExpID == _T("") ) )
		{
			pWnd->ShowWindow( SW_HIDE );
			pWnd = GetDlgItem( IDC_TXT_SAMPRATE );
			if( pWnd ) pWnd->ShowWindow( SW_HIDE );
			pWnd = GetDlgItem( IDC_TXT_EDIT );
			if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		}
		else if( ::IsAppRx() ) m_tooltip.AddTool( pWnd, _T("Sampling rate used by patient in test."), _T("Sampling Rate") );
		else m_tooltip.AddTool( pWnd, _T("Sampling rate used by subject in experiment."), _T("Sampling Rate") );
	}
#ifdef _DEBUG
//	CEdit* pEdit = (CEdit*)GetDlgItem( IDC_EDIT_COUNT );
//	if( pEdit ) pEdit->SetReadOnly( FALSE );
	pWnd = GetDlgItem( IDC_BN_RECOVER );
	if( pWnd ) pWnd->ShowWindow( SW_SHOW );
#endif	// _DEBUG
	pWnd = GetDlgItem( IDC_EDIT_SITEID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("An identifier for this site/location [DEFAULT=From User]."), _T("Site ID") );
	pWnd = GetDlgItem( IDC_EDIT_SITEDESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("A description for this site/location [DEFAULT=From User]."), _T("Site Description") );
	if( ::IsAppRx() )
	{
		pWnd = GetDlgItem( IDC_DATETIMEPICKER1 );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Date the patient was added to the test (MM/DD/YYYY)."), _T("Added To Test") );
		pWnd = GetDlgItem( IDC_CHK_ACTIVE );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this if this patient is currently inactive."), _T("(In)Active Patient") );
	}
	else
	{
		pWnd = GetDlgItem( IDC_DATETIMEPICKER1 );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Date the subject was added to the experiment (MM/DD/YYYY)."), _T("Added To Experiment") );
		pWnd = GetDlgItem( IDC_CHK_ACTIVE );
		if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this if this subject is currently inactive."), _T("(In)Active Subject") );
	}

	if( !m_fNew )
	{
		pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( ( m_szExpID == _T("") ) || ( m_szGrpID == _T("") ) )
	{
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_SUBJ ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	{
	Subjects subj;
	CString szID, szCode;
	HRESULT hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetID( szID );
		subj.GetCode( szCode );
		szCode.MakeUpper();
		if( szID != m_szID )
		{
			m_lstIDs.AddTail( szID );
			m_lstCodes.AddTail( szCode );
		}

		hr = subj.GetNext();
	}
	}

	m_ctrlDateTime.SetTime( m_DateAdded );

	// fill experiment list
	Experiments exp;
	CString szDataPathRoot;
	::GetDataPathRoot( szDataPathRoot );
	exp.SetDataPath( szDataPathRoot );
	CString szItem;
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		exp.GetDescriptionWithID( szItem );
		m_cboDefExp.AddString( szItem );
		hr = exp.GetNext();
	}
	// now search for experiment to select in list
	if( ( m_szDefExp != _T("") ) )
	{
		hr = exp.Find( m_szDefExp );
		if( SUCCEEDED( hr ) )
		{
			exp.GetDescriptionWithID( szItem );
			int nSel = m_cboDefExp.FindStringExact( -1, szItem );
			if( nSel >= 0 ) m_cboDefExp.SetCurSel( nSel );
		}
	}

	// movalyzer-generated subject ID (if applicable)
	if( m_fNew )
	{
		// hide password button if new subj
		pWnd = GetDlgItem( IDC_BN_PASS );
		if( pWnd ) pWnd->EnableWindow( FALSE );

		CString szUser;
		::GetCurrentUser( szUser );
		NSMUsers user;
		if( SUCCEEDED( user.Find( szUser ) ) )
		{
			BOOL fGen = FALSE;
			user.GetGenerateSubjectIDs( fGen );
			if( fGen )
			{
				CString szStart, szEnd, szCur, szNext;
				// get user set start, end & current IDs for subject IDs
				user.GetSubjectStartID( szStart );
				user.GetSubjectEndID( szEnd );
				user.GetSubjectCurID( szCur );
				// if current is set, increment until not found in db (or beyond max)
				// we do this in case the current was reset to the start
				if( szCur != _T("") )
				{
					while( m_lstIDs.Find( szCur ) )
					{
						if( !NextID( szCur, szStart, szEnd, FALSE ) ) break;
					}
					if( !NextID( szCur, szStart, szEnd, TRUE ) )
					{
						BCGPMessageBox( _T("The next subject ID is out of range.\n\nOpening user settings: Change the ID range or reset to the beginning.") );
						EndDialog( IDCANCEL );
						CWnd* pMainWnd = AfxGetMainWnd();
						if( pMainWnd ) ::PostMessage( pMainWnd->m_hWnd, WM_COMMAND, UM_SUBJECT_OUTOFRANGE, 0 );
						return FALSE;
					}
					szNext = szCur;
				}
				// if current is not set (no subject created), set to start
				else szNext = szStart;

				m_szID = szNext;
				pWnd = GetDlgItem( IDC_EDIT_ID );
				pWnd->EnableWindow( FALSE );
				pWnd->GetDlgItem( IDC_EDIT_CODE );
				pWnd->SetFocus();
				UpdateData( FALSE );
				OnKillfocusEditId();
				szCur.Format( _T("Generated next subject ID: %s"), m_szID );
				::OutputAMessage( szCur );
			}
		}
	}

	// if code is not empty, ensure it is not already defined
	CString szTemp = m_szCode;
	szTemp.MakeUpper();
	if( ( szTemp != _T("") ) && ( m_lstCodes.Find( szTemp ) != NULL ) )
	{
		m_szCode = _T("");
		UpdateData( FALSE );
	}

	m_fInit = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL SubjectDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void SubjectDlg::OnOK()
{
	UpdateData( TRUE );

	if( !CheckID() ) return;
	if( !CheckCode() ) return;

	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The subject ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szCode == _T("") )
	{
		BCGPMessageBox( _T("The subject CODE must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_CODE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szFirst == _T("") )
	{
		BCGPMessageBox( _T("The subject first name must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FIRST );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szLast == _T("") )
	{
		BCGPMessageBox( _T("The subject last name must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LAST );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	m_ctrlDateTime.GetTime( m_DateAdded );

	if( m_szID == HOLDER )
	{
		BCGPMessageBox( _T("This ID is not allowed.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szCode == HOLDER )
	{
		BCGPMessageBox( _T("This CODE is not allowed.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_CODE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return;
	// no reserved nor delim in CODE (msg handled in killfocus);
	if( !::VerifyStringForReserved( m_szCode ) ) return;
	// no reserved nor delim in last name
	if( !::VerifyStringForReserved( m_szLast ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Last name cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LAST );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	// no reserved nor delim in first name
	if( !::VerifyStringForReserved( m_szFirst ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("First name cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FIRST );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	// extract exp id
	if( m_cboDefExp.GetCurSel() >= 0 )
	{
		CString szDelim, szItem;
		::GetDelimiter( szDelim );
		TRIM( szDelim );
		m_cboDefExp.GetLBText( m_cboDefExp.GetCurSel(), szItem );
		int nPos = szItem.Find( szDelim );
		if( nPos != -1 ) m_szDefExp = szItem.Mid( nPos + 2 );
		else m_szDefExp = _T("");
	}
	else m_szDefExp = _T("");

	// update movalyzer-generated subject ID (if applicable)
	if( m_fNew )
	{
		CString szUser;
		::GetCurrentUser( szUser );
		NSMUsers user;
		if( SUCCEEDED( user.Find( szUser ) ) )
		{
			BOOL fGen = FALSE;
			user.GetGenerateSubjectIDs( fGen );
			if( fGen )
			{
				user.PutSubjectCurID( m_szID );
				if( SUCCEEDED( user.Modify() ) )
				{
					szUser.Format( _T("Current generated subject ID set to %s"), m_szID );
					::OutputAMessage( szUser );
				}
			}
		}
	}

	CBCGPDialog::OnOK();
}

void SubjectDlg::OnKillfocusEditId()
{
	CheckID();
}

BOOL SubjectDlg::CheckID( BOOL fMsg )
{
	if( !m_fInit ) return TRUE;

	UpdateData( TRUE );

	if( m_szID == _T("") ) return TRUE;

	if( m_lstIDs.Find( m_szID ) )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->EnableWindow( TRUE );
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
		if( fMsg )
		{
			int nResp = BCGPMessageBox( _T("This ID already exists. Would you like to see how it is currently being used?"), MB_YESNO );
			if( nResp == IDYES )
			{
				m_fInit = FALSE;
				Subjects subj;
				HRESULT hr;
				if( subj.IsValid() )
				{
					hr = subj.Find( m_szID );
					if( SUCCEEDED( hr ) )
					{
						CString szItem;
						subj.GetNameWithID( szItem, ::IsPrivacyOn() );
						RelationshipDlg d( this, REL_SUBJECT, szItem );
						d.DoModal();
					}
					else BCGPMessageBox( _T("Could not find subject.") );
				}
				m_fInit = TRUE;
			}
		}
		return FALSE;
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		if( fMsg ) BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
		return FALSE;
	}

	// set code if not already entered (default to ID) and does not exist
	CString szNewID = m_szSiteID;
	szNewID += m_szID;
	if( !m_fEditCode && ( m_lstCodes.Find( szNewID ) == NULL ) )
	{
		m_szCode = szNewID;
		UpdateData( FALSE );
		return CheckCode( fMsg );
	}

	return TRUE;
}

void SubjectDlg::OnKillfocusEditSiteId() 
{
	UpdateData( TRUE );

	if( m_szSiteID == _T("") ) return;
	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szSiteID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Site ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SITEID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// set code if not already entered (default to SITEID+ID) and does not exist
	CString szNewID = m_szSiteID;
	szNewID += m_szID;
	if( !m_fEditCode && ( m_lstCodes.Find( szNewID ) == NULL ) )
	{
		m_szCode = szNewID;
		UpdateData( FALSE );
		CheckCode();
	}
}

void SubjectDlg::OnKillfocusEditCode()
{
	CheckCode();
}

BOOL SubjectDlg::CheckCode( BOOL fMsg )
{
	UpdateData( TRUE );

	if( m_szCode == _T("") ) return TRUE;

	CString szTemp = m_szCode;
	szTemp.MakeUpper();
	if( m_lstCodes.Find( szTemp ) )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_CODE );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
		if( fMsg ) BCGPMessageBox( _T("This CODE already exists."), MB_OK );
		return FALSE;
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szCode ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("CODE cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		if( fMsg ) BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_CODE );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
		return FALSE;
	}

	return TRUE;
}

void SubjectDlg::OnChangeCode()
{
	m_fEditCode = TRUE;
}

BOOL SubjectDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newsubject.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void SubjectDlg::OnClickNotes()
{
	// verify ID first
	UpdateData( TRUE );
	if( m_szExpID == _T("") ) return;
	if( m_szGrpID == _T("") ) return;
	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CString szData = _T("SRC: Experiment - Notes");
	LogToFile( szData, LOG_UI );

	// Check for directory structure
	if( !VerifyDirectory( m_szExpID, m_szGrpID, m_szID ) )
	{
		BCGPMessageBox( _T("Unable to create directory for files.") );
		return;
	}

	CString szPath, szFile;
	::GetDataPathRoot( szPath );
	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s\\%s\\%s%s%s.txt"),
					   m_szExpID, m_szGrpID, m_szID, m_szExpID, m_szGrpID, m_szID );
	else
		szFile.Format( _T("%s\\%s\\%s\\%s\\%s%s%s.txt"),
					   szPath, m_szExpID, m_szGrpID, m_szID, m_szExpID, m_szGrpID, m_szID );

	NotesDlg d( N_SUBJECT, m_szID, szFile, this );
	d.DoModal();
}

void SubjectDlg::OnClickEdit()
{
	CEdit* pWndDR = (CEdit*)GetDlgItem( IDC_EDIT_DEVRES );
	if( pWndDR && ( pWndDR->GetStyle() & ES_READONLY ) )
	{
		CEdit* pWndSR = (CEdit*)GetDlgItem( IDC_EDIT_SAMPRATE );
		pWndDR->SetReadOnly( FALSE );
		pWndSR->SetReadOnly( FALSE );
		CWnd* pWnd = GetDlgItem( IDC_TXT_EDIT );
		pWnd->ShowWindow( SW_HIDE );
		pWndDR->SetFocus();
		m_fEditDRSR = TRUE;
	}

// #ifdef	_DEBUG
// 	// to test password and signature
// 	CString szMsg;
// 	szMsg.Format( _T("EM: %d - SIG: %s - PASS: %s"), m_nEncryptionMethod, m_szSignature, m_szPassword );
// 	BCGPMessageBox( szMsg );
// #endif
}

void SubjectDlg::OnClickEdit2()
{
	CEdit* pWndID = (CEdit*)GetDlgItem( IDC_EDIT_SITEID );
	if( pWndID && ( pWndID->GetStyle() & ES_READONLY ) )
	{
		CEdit* pWndDesc = (CEdit*)GetDlgItem( IDC_EDIT_SITEDESC );
		pWndID->SetReadOnly( FALSE );
		pWndDesc->SetReadOnly( FALSE );
		CWnd* pWnd = GetDlgItem( IDC_TXT_EDIT2 );
		pWnd->ShowWindow( SW_HIDE );
		pWndID->SetFocus();
	}
}

void SubjectDlg::OnClickPassword()
{
	PasswordChgDlg d( this );
	d.m_szPass = m_szPassword;
	if( d.DoModal() != IDOK ) return;
	m_szPassword = d.m_szPassNew;
}

void SubjectDlg::OnClickRecover()
{
	Subjects subj;
	subj.ReadRecoverFile();
}

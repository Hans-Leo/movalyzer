#pragma once
#include "afxcmn.h"


// ListDialog dialog

class AFX_EXT_CLASS ListDialog : public CBCGPDialog
{
	DECLARE_DYNAMIC(ListDialog)

public:
	ListDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~ListDialog();

// Dialog Data
	enum { IDD = IDD_LIST };

	CBCGPListCtrl	m_list;
	CImageList		m_ImageList;
	CString			m_szFileOrig;
	CString			m_szFileNew;
	CString			m_szItem;
	BOOL			m_fShowOnly;
	BOOL			m_fDiffNameFirst;
	BOOL			m_fDiffNameLast;
	BOOL			m_fDiffNotesPrv;
	BOOL			m_fDiffSig;
	BOOL			m_fDiffPass;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	void FillFileCompareList();

	afx_msg void OnBnClickedChkHide();
	afx_msg void OnBnClickedRel();
	DECLARE_MESSAGE_MAP()
};

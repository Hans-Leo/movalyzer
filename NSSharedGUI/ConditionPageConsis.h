#pragma once

// ConditionPageConsis.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ConditionPageConsis dialog

interface INSCondition;

class AFX_EXT_CLASS ConditionPageConsis : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ConditionPageConsis)

// Construction
public:
	ConditionPageConsis( INSCondition* = NULL, BOOL fNew = TRUE );
	~ConditionPageConsis();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_CONSIS };
	CString			m_szLex;
	int				m_nMin;
	int				m_nMax;
	double			m_dStrokeLength;
	double			m_dRangeLength;
	double			m_dStrokeDirection;
	double			m_dRangeDirection;
	int				m_nSkip;
	BOOL			m_fFlagBadTarget;
	CColoredEdit	m_editMin;
	CColoredEdit	m_editMax;
	CColoredEdit	m_editStrokeLength;
	CColoredEdit	m_editRangeLength;
	CColoredEdit	m_editStrokeDir;
	CColoredEdit	m_editRangeDir;
	CColoredEdit	m_editSkip;
	CColoredButton	m_chkFlag;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBnCalc();
};

#pragma once

// ProcSetPageREExt.h : header file
//

#include "NSTooltipCtrl.h"

BOOL CheckedMatlab();
void SetCheckedMatlab();
BOOL MatlabInstalled();
void SetMatlabInstalled();

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREExt dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageREExt : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageREExt)

// Construction
public:
	ProcSetPageREExt( IProcSetting* pS = NULL );
	~ProcSetPageREExt();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_EXTERNAL };
	int				m_nExtTypeRaw;
	CBCGPComboBox	m_cboETRaw;
	CString			m_szExtScriptRaw;
	CBCGPComboBox	m_cboESRaw;
	CBCGPButton		m_bnEditRaw;

	int				m_nExtTypeTF;
	CBCGPComboBox	m_cboETTF;
	CString			m_szExtScriptTF;
	CBCGPComboBox	m_cboESTF;
	CBCGPButton		m_bnEditTF;

	int				m_nExtTypeSeg;
	CBCGPComboBox	m_cboETSeg;
	CString			m_szExtScriptSeg;
	CBCGPComboBox	m_cboESSeg;
	CBCGPButton		m_bnEditSeg;

	int				m_nExtTypeExt;
	CBCGPComboBox	m_cboETExt;
	CString			m_szExtScriptExt;
	CBCGPComboBox	m_cboESExt;
	CBCGPButton		m_bnEditExt;

	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void FillScriptLists();
	void EnableDisable();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnChangeType();
	afx_msg void OnClickEditRaw();
	afx_msg void OnClickEditTF();
	afx_msg void OnClickEditSeg();
	afx_msg void OnClickEditExt();
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

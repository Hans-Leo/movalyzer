// ConditionPageStimuli.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "RelationshipDlg.h"
#include "Stimuli.h"
#include "..\DataMod\DataMod.h"
#include "..\CTreeLink\DBCommon.h"
#include "ConditionPageStimuli.h"
#include "ConditionSheet.h"
#include "Conditions.h"
#include "Progress.h"
#include "..\Common\MessageManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	STR_NONE		_T("<NONE>")
#define	CBO_STIM		0
#define	CBO_STIMW		1
#define	CBO_STIMP		2

/////////////////////////////////////////////////////////////////////////////
// ConditionPageStimuli property page

IMPLEMENT_DYNCREATE(ConditionPageStimuli, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageStimuli, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_CBN_SELCHANGE(IDC_CBO_STIMULUSW, OnSelchangeCboWarning)
	ON_CBN_SELCHANGE(IDC_CBO_STIMULUSP, OnSelchangeCboPrecue)
	ON_CBN_SELCHANGE(IDC_CBO_STIMULUS, OnSelchangeCboStimulus)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_BN_CLICKED(IDC_BN_CHART, OnClickBnChart)
	ON_BN_CLICKED(IDC_BN_ADDW, OnClickBnAddW)
	ON_BN_CLICKED(IDC_BN_EDITW, OnClickBnEditW)
	ON_BN_CLICKED(IDC_BN_RELW, OnClickBnRelW)
	ON_BN_CLICKED(IDC_BN_CHARTW, OnClickBnChartW)
	ON_BN_CLICKED(IDC_BN_ADDP, OnClickBnAddP)
	ON_BN_CLICKED(IDC_BN_EDITP, OnClickBnEditP)
	ON_BN_CLICKED(IDC_BN_RELP, OnClickBnRelP)
	ON_BN_CLICKED(IDC_BN_CHARTP, OnClickBnChartP)
	ON_BN_CLICKED(IDC_CHK_FIRSTTARGET, OnBnClickedChkFirsttarget)
	ON_BN_CLICKED(IDC_CHK_ONPENDOWN, OnBnClickedChkOnpendown)
	ON_BN_CLICKED(IDC_BN_SE, OnClickBnSE)
	ON_BN_CLICKED(IDC_BN_SEW, OnClickBnSEW)
	ON_BN_CLICKED(IDC_BN_SEP, OnClickBnSEP)
END_MESSAGE_MAP()

ConditionPageStimuli::ConditionPageStimuli( INSCondition* pC, BOOL fNew )
	: CBCGPPropertyPage(ConditionPageStimuli::IDD), m_pC( pC )
{
	m_dPDuration = 2.5;
	m_dPLatency = 2.5;
	m_fOnPenDown = TRUE;
	m_fStopWrongTarget = FALSE;
	m_fStartFirstTarget = FALSE;
	m_fStopLastTarget = FALSE;
	m_dWDuration = 2.5;
	m_dWLatency = 2.5;
	m_fStimulus = FALSE;

	if( !fNew && pC )
	{
		NSConditions::GetUseStimulus( pC, m_fStimulus );
		NSConditions::GetStimulus( pC, m_szStimulus );
		NSConditions::GetStimulusWarning( pC, m_szStimulusW );
		NSConditions::GetStimulusPrecue( pC, m_szStimulusP );
		NSConditions::GetPrecueDuration( pC, m_dPDuration );
		NSConditions::GetPrecueLatency( pC, m_dPLatency );
		NSConditions::GetRecordImmediately( pC, m_fOnPenDown );
		m_fOnPenDown = !m_fOnPenDown; // what a goof i am
		NSConditions::GetStopWrongTarget( pC, m_fStopWrongTarget );
		NSConditions::GetStartFirstTarget( pC, m_fStartFirstTarget );
		NSConditions::GetStopLastTarget( pC, m_fStopLastTarget );
		NSConditions::GetWarningDuration( pC, m_dWDuration );
		NSConditions::GetWarningLatency( pC, m_dWLatency );
	}

	// hack: just in case a conversion put start with pen down AND on first target
	if( m_fStartFirstTarget && m_fOnPenDown )
	{
		m_fStartFirstTarget = FALSE;
		m_fStopLastTarget = FALSE;
	}
}

ConditionPageStimuli::~ConditionPageStimuli()
{
}

void ConditionPageStimuli::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_STIMULUS, m_cboStimulus);
	DDX_Control(pDX, IDC_CBO_STIMULUSW, m_cboStimulusW);
	DDX_Control(pDX, IDC_CBO_STIMULUSP, m_cboStimulusP);
	DDX_Text(pDX, IDC_EDIT_DUR, m_dPDuration );
	DDX_Text(pDX, IDC_EDIT_LATENCY, m_dPLatency );
	DDX_Check(pDX, IDC_CHK_WRONGTARGET, m_fStopWrongTarget);
	DDX_Check(pDX, IDC_CHK_FIRSTTARGET, m_fStartFirstTarget);
	DDX_Check(pDX, IDC_CHK_LASTTARGET, m_fStopLastTarget);
	DDX_Check(pDX, IDC_CHK_ONPENDOWN, m_fOnPenDown);
	DDX_Text(pDX, IDC_EDIT_DUR2, m_dWDuration );
	DDX_Text(pDX, IDC_EDIT_LATENCY2, m_dWLatency );
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_ADDW, m_bnAddW);
	DDX_Control(pDX, IDC_BN_ADDP, m_bnAddP);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_EDITW, m_bnEditW);
	DDX_Control(pDX, IDC_BN_EDITP, m_bnEditP);
	DDX_Control(pDX, IDC_BN_REL, m_bnRel);
	DDX_Control(pDX, IDC_BN_RELW, m_bnRelW);
	DDX_Control(pDX, IDC_BN_RELP, m_bnRelP);
	DDX_Control(pDX, IDC_BN_CHART, m_bnChart);
	DDX_Control(pDX, IDC_BN_CHARTW, m_bnChartW);
	DDX_Control(pDX, IDC_BN_CHARTP, m_bnChartP);
	DDX_Control(pDX, IDC_BN_SE, m_bnSE);
	DDX_Control(pDX, IDC_BN_SEW, m_bnSEW);
	DDX_Control(pDX, IDC_BN_SEP, m_bnSEP);
}

/////////////////////////////////////////////////////////////////////////////
// ConditionPageStimuli message handlers

BOOL ConditionPageStimuli::OnApply() 
{
	UpdateData( TRUE );

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos;

	// stimuli
	CString szStim;
	int nIdx = m_cboStimulus.GetCurSel();
	if( nIdx != CB_ERR )
	{
		m_cboStimulus.GetLBText( nIdx, szStim );
		if( szStim != STR_NONE )
		{
			nPos = szStim.Find( szDelim );
			m_szStimulus = szStim.Mid( nPos + 2 );
		}
		else m_szStimulus = _T("");
	}
	nIdx = m_cboStimulusW.GetCurSel();
	if( nIdx != CB_ERR )
	{
		m_cboStimulusW.GetLBText( nIdx, szStim );
		if( szStim != STR_NONE )
		{
			nPos = szStim.Find( szDelim );
			m_szStimulusW = szStim.Mid( nPos + 2 );
		}
		else m_szStimulusW = _T("");
	}
	nIdx = m_cboStimulusP.GetCurSel();
	if( nIdx != CB_ERR )
	{
		m_cboStimulusP.GetLBText( nIdx, szStim );
		if( szStim != STR_NONE )
		{
			nPos = szStim.Find( szDelim );
			m_szStimulusP = szStim.Mid( nPos + 2 );
		}
		else m_szStimulusP = _T("");
	}
	if( ( m_szStimulus == _T("") ) && ( m_szStimulusW == _T("") ) && ( m_szStimulusP == _T("") ) )
		m_fStimulus = FALSE;
	else
		m_fStimulus = TRUE;

	if( ( m_dPDuration < 0 ) || ( m_dPDuration > 100 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 3 );
		BCGPMessageBox( _T("Precue stimulus duration must be between 0 and 100."), MB_OK | MB_ICONERROR );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DUR );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dPLatency < 0 ) || ( m_dPLatency > 100 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 3 );
		BCGPMessageBox( _T("Precue stimulus latency must be between 0 and 100."), MB_OK | MB_ICONERROR );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LATENCY );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dWDuration < 0 ) || ( m_dWDuration > 100 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 3 );
		BCGPMessageBox( _T("Warning stimulus duration must be between 0 and 100."), MB_OK | MB_ICONERROR );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DUR2 );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dWLatency < 0 ) || ( m_dWLatency > 100 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 3 );
		BCGPMessageBox( _T("Warning stimulus latency must be between 0 and 100."), MB_OK | MB_ICONERROR );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LATENCY2 );
		pWnd->SetFocus();
		return FALSE;
	}

	m_pC->put_UseStimulus( m_fStimulus );
	m_pC->put_Stimulus( m_szStimulus.AllocSysString() );
	m_pC->put_StimulusWarning( m_szStimulusW.AllocSysString() );
	m_pC->put_StimulusPrecue( m_szStimulusP.AllocSysString() );
	m_pC->put_WarningDuration( m_dWDuration );
	m_pC->put_WarningLatency( m_dWLatency );
	m_pC->put_PrecueDuration( m_dPDuration );
	m_pC->put_PrecueLatency( m_dPLatency );
	m_pC->put_RecordImmediately( !m_fOnPenDown );	// haxorz (doh)
	m_pC->put_StopWrongTarget( m_fStopWrongTarget );
	m_pC->put_StartFirstTarget( m_fStartFirstTarget );
	m_pC->put_StopLastTarget( m_fStopLastTarget );

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageStimuli::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageStimuli::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnAddW.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnAddP.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnEdit.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnEditW.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnEditP.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnRel.SetImage( IDB_REL, IDB_REL, IDB_REL );
	m_bnRelW.SetImage( IDB_REL, IDB_REL, IDB_REL );
	m_bnRelP.SetImage( IDB_REL, IDB_REL, IDB_REL );
	m_bnChart.SetImage( IDB_CHART, IDB_CHART, IDB_CHART );
	m_bnChartW.SetImage( IDB_CHART, IDB_CHART, IDB_CHART );
	m_bnChartP.SetImage( IDB_CHART, IDB_CHART, IDB_CHART );
	m_bnSE.SetImage( IDB_BN_SE, IDB_BN_SE, IDB_BN_SE );
	m_bnSEW.SetImage( IDB_BN_SE, IDB_BN_SE, IDB_BN_SE );
	m_bnSEP.SetImage( IDB_BN_SE, IDB_BN_SE, IDB_BN_SE );

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_CBO_STIMULUS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an imperative stimulus."), _T("Stimulus") );
	pWnd = GetDlgItem( IDC_CBO_STIMULUSW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a warning stimulus."), _T("Stimulus") );
	pWnd = GetDlgItem( IDC_CBO_STIMULUSP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a precue stimulus."), _T("Stimulus") );
	pWnd = GetDlgItem( IDC_EDIT_DUR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Precue stimulus duration (s)."), _T("Duration") );
	pWnd = GetDlgItem( IDC_EDIT_LATENCY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Duration after precue and before imperative stimuli (s)."), _T("Latency") );
	pWnd = GetDlgItem( IDC_EDIT_DUR2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Warning stimulus duration (s)."), _T("Duration") );
	pWnd = GetDlgItem( IDC_EDIT_LATENCY2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Duration after warning and before precue stimuli (s)."), _T("Latency") );
	pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Stop recording current trial if wrong target is reached."), _T("Stop at Wrong Target") );
	pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Start recording current trial if first correct target is reached."), _T("Start at First Correct Target") );
	pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Stop recording current trial if last correct target is reached."), _T("Stop at Last Correct Target") );
	pWnd = GetDlgItem( IDC_CHK_ONPENDOWN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Begin recording when pen touches tablet, otherwise begin immediately."), _T("Begin Recording") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify stimulus properties."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new stimulus."), _T("Create New") );
	pWnd = GetDlgItem( IDC_BN_REL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the stimulus' relationships."), _T("Relationships") );
	pWnd = GetDlgItem( IDC_BN_EDITW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify stimulus properties."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADDW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new stimulus."), _T("Create New") );
	pWnd = GetDlgItem( IDC_BN_RELW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the stimulus' relationships."), _T("Relationships") );
	pWnd = GetDlgItem( IDC_BN_EDITP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify stimulus properties."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADDP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new stimulus."), _T("Create New") );
	pWnd = GetDlgItem( IDC_BN_RELP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the stimulus' relationships."), _T("Relationships") );
	pWnd = GetDlgItem( IDC_BN_CHART );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Chart the stimulus."), _T("Chart") );
	pWnd = GetDlgItem( IDC_BN_CHARTW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Chart the stimulus."), _T("Chart") );
	pWnd = GetDlgItem( IDC_BN_CHARTP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Chart the stimulus."), _T("Chart") );
	pWnd = GetDlgItem( IDC_BN_SE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open stimulus in Stimulus Editor."), _T("Stimulus Editor") );
	pWnd = GetDlgItem( IDC_BN_SEW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open stimulus in Stimulus Editor."), _T("Stimulus Editor") );
	pWnd = GetDlgItem( IDC_BN_SEP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open stimulus in Stimulus Editor."), _T("Stimulus Editor") );

	// STIMULI
	FillStimuli();	// use default for all & default selections

	// Disable stimuli if movalyzer not activated
	if( !::IsOA() )
	{
		m_szStimulusW = _T("");
		m_szStimulusP = _T("");
		m_szStimulus = _T("");

		pWnd = GetDlgItem( IDC_CBO_STIMULUS );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_STIMULUSW );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_STIMULUSP );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// pen touches tablet is not an option for gripper exp
	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	if( pSheet->m_fGripper )
	{
		pWnd = GetDlgItem( IDC_CHK_ONPENDOWN );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
		pWnd->EnableWindow( FALSE );
	}

	// disable duration/latency/wrong target if need be
	if( m_szStimulusW == "" )
	{
		pWnd = GetDlgItem( IDC_EDIT_DUR2 );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_LATENCY2 );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDITW );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_RELW );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_CHARTW );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_SEW );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( m_szStimulusP == "" )
	{
		pWnd = GetDlgItem( IDC_EDIT_DUR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_LATENCY );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDITP );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_RELP );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_CHARTP );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_SEP );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( m_szStimulus == "" )
	{
		pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDIT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_REL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_CHART );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_SE );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	pWnd = GetDlgItem( IDC_CHK_ONPENDOWN );
	pWnd->EnableWindow( !m_fStartFirstTarget );
	pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
	pWnd->EnableWindow( !m_fOnPenDown && ( m_szStimulus != _T("") ) );

	// disable these if writalyzer
	NSSecurity* pSecurity = ::GetSecurity();
	if( pSecurity && ( pSecurity->GetApplication() == APP_SECURITY_WRITALYZER ) )
	{
		pWnd = GetDlgItem( IDC_CBO_STIMULUS );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_STIMULUSW );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_STIMULUSP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_DUR );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_LATENCY );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_DUR2 );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_LATENCY2 );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_ONPENDOWN );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDIT );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ADD );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_REL );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_CHART );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDITW );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ADDW );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_RELW );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_CHARTW );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_EDITP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ADDP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_RELP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_CHARTP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_SE );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_SEW );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_SEP );
		pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ConditionPageStimuli::FillStimuli( int nWhich, int nSel )
{
	// clear
	if( ( nWhich == -1 ) || ( nWhich == CBO_STIM ) )
	{
		m_cboStimulus.ResetContent();
		m_cboStimulus.AddString( STR_NONE );
	}
	if( ( nWhich == -1 ) || ( nWhich == CBO_STIMW ) )
	{
		m_cboStimulusW.ResetContent();
		m_cboStimulusW.AddString( STR_NONE );
	}
	if( ( nWhich == -1 ) || ( nWhich == CBO_STIMP ) )
	{
		m_cboStimulusP.ResetContent();
		m_cboStimulusP.AddString( STR_NONE );
	}

	Stimuluss stim;
	int nSelCur = -1, nPos, nItem;
	CString szItem, szID, szDelim, szMsg;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	HRESULT hr = stim.ResetToStart();
	long lRecords = (long)stim.GetNumRecords();
	Progress::EnableProgress( lRecords );
	long nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		stim.GetDescriptionWithID( szItem );

		nStep++;
		szMsg.Format( _T("Loading stimulus: %s"), szItem );
		Progress::SetProgress( nStep, szMsg );

		// if all or just this stimulus, fill
		if( ( nWhich == -1 ) || ( nWhich == CBO_STIMW ) )
		{
			// if all & matches, select the initial stored stimulus
			nItem = m_cboStimulusW.AddString( szItem );
			if( nWhich == -1 )
			{
				nPos = szItem.Find( szDelim );
				szID = szItem.Mid( nPos + 2 );
				if( szID == m_szStimulusW ) m_cboStimulusW.SetCurSel( nItem );
			}
		}

		// if all or just this stimulus, fill
		if( ( nWhich == -1 ) || ( nWhich == CBO_STIMP ) )
		{
			// if all & matches, select the initial stored stimulus
			nItem = m_cboStimulusP.AddString( szItem );
			if( nWhich == -1 )
			{
				nPos = szItem.Find( szDelim );
				szID = szItem.Mid( nPos + 2 );
				if( szID == m_szStimulusP ) m_cboStimulusP.SetCurSel( nItem );
			}
		}

		// if all or just this stimulus, fill
		if( ( nWhich == -1 ) || ( nWhich == CBO_STIM ) )
		{
			// if all & matches, select the initial stored stimulus
			nItem = m_cboStimulus.AddString( szItem );
			if( nWhich == -1 )
			{
				nPos = szItem.Find( szDelim );
				szID = szItem.Mid( nPos + 2 );
				if( szID == m_szStimulus ) m_cboStimulus.SetCurSel( nItem );
			}
		}

		hr = stim.GetNext();
	}

	Progress::DisableProgress();

	// if a specific stimulus, select the specified item
	if( nSel >= 0 )
	{
		if( nWhich == CBO_STIMW ) m_cboStimulusW.SetCurSel( nSel );
		else if( nWhich == CBO_STIMP ) m_cboStimulusP.SetCurSel( nSel );
		else if( nWhich == CBO_STIM ) m_cboStimulus.SetCurSel( nSel );
	}

	// in case none have been selected, select "<NONE>"
	if( m_cboStimulus.GetCurSel() < 0 ) m_cboStimulus.SetCurSel( 0 );
	if( m_cboStimulusW.GetCurSel() < 0 ) m_cboStimulusW.SetCurSel( 0 );
	if( m_cboStimulusP.GetCurSel() < 0 ) m_cboStimulusP.SetCurSel( 0 );
}

void ConditionPageStimuli::OnSelchangeCboWarning()
{
	CComboBox* pCbo = (CComboBox*)GetDlgItem( IDC_CBO_STIMULUSW );
	if( pCbo )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DUR2 );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_EDIT_LATENCY2 );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_EDITW );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_RELW );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_CHARTW );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_SEW );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	}
}

void ConditionPageStimuli::OnSelchangeCboPrecue()
{
	CComboBox* pCbo = (CComboBox*)GetDlgItem( IDC_CBO_STIMULUSP );
	if( pCbo )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DUR );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_EDIT_LATENCY );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_EDITP );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_RELP );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_CHARTP );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_SEP );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	}
}

void ConditionPageStimuli::OnSelchangeCboStimulus()
{
	CComboBox* pCbo = (CComboBox*)GetDlgItem( IDC_CBO_STIMULUS );
	if( pCbo )
	{
		CWnd* pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		if( pCbo->GetCurSel() == 0 ) m_fStopWrongTarget = FALSE;
		pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
		pWnd->EnableWindow( !m_fOnPenDown && ( pCbo->GetCurSel() > 0 ) );
		if( pCbo->GetCurSel() == 0 ) m_fStartFirstTarget = FALSE;
		pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		if( pCbo->GetCurSel() == 0 ) m_fStopLastTarget = FALSE;
		pWnd = GetDlgItem( IDC_BN_EDIT );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_REL );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_CHART );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
		pWnd = GetDlgItem( IDC_BN_SE );
		pWnd->EnableWindow( pCbo->GetCurSel() > 0 );

		UpdateData( FALSE );
	}
}

void ConditionPageStimuli::DoAdd( int nWhich )
{
	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CComboBox* pStim = NULL;
	if( nWhich == CBO_STIMW ) pStim = &m_cboStimulusW;
	else if( nWhich == CBO_STIMP ) pStim = &m_cboStimulusP;
	else if( nWhich == CBO_STIM ) pStim = &m_cboStimulus;
	if( !pStim ) return;

	// do add
	CWaitCursor crs;
	if( stim.DoAdd( this ) )
	{
		// get info
		CString szItem;
		stim.GetDescriptionWithID( szItem );

		// add item, get index, and select
		int nItem = pStim->AddString( szItem );
		if( nItem >= 0 ) pStim->SetCurSel( nItem );
	}
}

void ConditionPageStimuli::DoEdit( int nWhich )
{
	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CComboBox* pStim = NULL;
	if( nWhich == CBO_STIMW ) pStim = &m_cboStimulusW;
	else if( nWhich == CBO_STIMP ) pStim = &m_cboStimulusP;
	else if( nWhich == CBO_STIM ) pStim = &m_cboStimulus;
	if( !pStim ) return;

	int nIdx = pStim->GetCurSel();
	if( nIdx < 0 ) return;

	CString szID, szDelim;
	pStim->GetLBText( nIdx, szID );
	if( szID == _T("<NONE>") ) return;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos = szID.Find( szDelim );
	szID = szID.Mid( nPos + 2 );

	CWaitCursor crs;
	if( stim.DoEdit( szID, this ) ) FillStimuli( nWhich, nIdx );
}

void ConditionPageStimuli::DoRel( int nWhich )
{
	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CComboBox* pStim = NULL;
	if( nWhich == CBO_STIMW ) pStim = &m_cboStimulusW;
	else if( nWhich == CBO_STIMP ) pStim = &m_cboStimulusP;
	else if( nWhich == CBO_STIM ) pStim = &m_cboStimulus;
	if( !pStim ) return;

	int nIdx = pStim->GetCurSel();
	if( nIdx < 0 ) return;

	CString szID, szDelim;
	pStim->GetLBText( nIdx, szID );
	if( szID == _T("<NONE>") ) return;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos = szID.Find( szDelim );
	szID = szID.Mid( nPos + 2 );

	CWaitCursor crs;
	if( SUCCEEDED( stim.Find( szID ) ) )
	{
		CString szItem;
		stim.GetDescriptionWithID( szItem );

		RelationshipDlg d( this, REL_STIMULUS, szItem );
		d.DoModal();
	}
	else BCGPMessageBox( _T("Could not find stimulus.") );
}

void ConditionPageStimuli::OnClickBnAdd() 
{
	DoAdd( CBO_STIM );

	CComboBox* pCbo = &m_cboStimulus;
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_REL );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_CHART );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_SE );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );

	pWnd = GetDlgItem( IDC_CHK_WRONGTARGET );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	if( pCbo->GetCurSel() == 0 ) m_fStopWrongTarget = FALSE;
	pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
	pWnd->EnableWindow( !m_fOnPenDown && ( pCbo->GetCurSel() > 0 ) );
	if( pCbo->GetCurSel() == 0 ) m_fStartFirstTarget = FALSE;
	pWnd = GetDlgItem( IDC_CHK_LASTTARGET );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	if( pCbo->GetCurSel() == 0 ) m_fStopLastTarget = FALSE;

	UpdateData( FALSE );
}

void ConditionPageStimuli::OnClickBnEdit()
{
	DoEdit( CBO_STIM );
}

void ConditionPageStimuli::OnClickBnRel()
{
	DoRel( CBO_STIM );
}

void ConditionPageStimuli::OnClickBnAddW() 
{
	DoAdd( CBO_STIMW );

	CComboBox* pCbo = &m_cboStimulusW;
	CWnd* pWnd = GetDlgItem( IDC_BN_EDITW );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_RELW );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_CHARTW );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_SEW );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_EDIT_DUR2 );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_EDIT_LATENCY2 );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
}

void ConditionPageStimuli::OnClickBnEditW()
{
	DoEdit( CBO_STIMW );
}

void ConditionPageStimuli::OnClickBnRelW()
{
	DoRel( CBO_STIMW );
}

void ConditionPageStimuli::OnClickBnAddP() 
{
	DoAdd( CBO_STIMP );

	CComboBox* pCbo = &m_cboStimulusP;
	CWnd* pWnd = GetDlgItem( IDC_BN_EDITP );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_RELP );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_CHARTP );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_BN_SEP );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_EDIT_DUR );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
	pWnd = GetDlgItem( IDC_EDIT_LATENCY );
	pWnd->EnableWindow( pCbo->GetCurSel() > 0 );
}

void ConditionPageStimuli::OnClickBnEditP()
{
	DoEdit( CBO_STIMP );
}

void ConditionPageStimuli::OnClickBnRelP()
{
	DoRel( CBO_STIMP );
}

BOOL ConditionPageStimuli::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageStimuli::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_stimuli.html"), this );
	return TRUE;
}

void ConditionPageStimuli::OnBnClickedChkFirsttarget()
{
	UpdateData( TRUE );
	if( m_fStartFirstTarget )
	{
		m_fOnPenDown = FALSE;
		UpdateData( FALSE );
	}
	CWnd* pWnd = GetDlgItem( IDC_CHK_ONPENDOWN );
	pWnd->EnableWindow( !m_fStartFirstTarget );
}

void ConditionPageStimuli::OnBnClickedChkOnpendown()
{
	UpdateData( TRUE );
	if( m_fOnPenDown )
	{
		m_fStartFirstTarget = FALSE;
		UpdateData( FALSE );
	}
	CWnd* pWnd = GetDlgItem( IDC_CHK_FIRSTTARGET );
	pWnd->EnableWindow( !m_fOnPenDown && ( m_cboStimulus.GetCurSel() > 0 ) );
}

void ConditionPageStimuli::DoChart( int nWhich )
{
	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CComboBox* pStim = NULL;
	if( nWhich == CBO_STIMW ) pStim = &m_cboStimulusW;
	else if( nWhich == CBO_STIMP ) pStim = &m_cboStimulusP;
	else if( nWhich == CBO_STIM ) pStim = &m_cboStimulus;
	if( !pStim ) return;

	int nIdx = pStim->GetCurSel();
	if( nIdx < 0 ) return;

	CString szID, szDelim;
	pStim->GetLBText( nIdx, szID );
	if( szID == _T("<NONE>") ) return;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos = szID.Find( szDelim );
	szID = szID.Mid( nPos + 2 );

	CWaitCursor crs;
	if( SUCCEEDED( stim.Find( szID ) ) ) stim.ChartData( FALSE );
	else BCGPMessageBox( _T("Could not find stimulus.") );
}

void ConditionPageStimuli::OnClickBnChart() 
{
	DoChart( CBO_STIM );
}

void ConditionPageStimuli::OnClickBnChartW()
{
	DoChart( CBO_STIMW );
}

void ConditionPageStimuli::OnClickBnChartP()
{
	DoChart( CBO_STIMP );
}

void ConditionPageStimuli::DoSE( int nWhich )
{
	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CComboBox* pStim = NULL;
	if( nWhich == CBO_STIMW ) pStim = &m_cboStimulusW;
	else if( nWhich == CBO_STIMP ) pStim = &m_cboStimulusP;
	else if( nWhich == CBO_STIM ) pStim = &m_cboStimulus;
	if( !pStim ) return;

	int nIdx = pStim->GetCurSel();
	if( nIdx < 0 ) return;

	CString szID, szDelim;
	pStim->GetLBText( nIdx, szID );
	if( szID == _T("<NONE>") ) return;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos = szID.Find( szDelim );
	szID = szID.Mid( nPos + 2 );

	if( SUCCEEDED( stim.Find( szID ) ) )
	{
		MessageManager* pMM = MessageManager::GetMessageManager();
		if( pMM ) pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_STIMEDIT ), (WPARAM)szID.GetBuffer() );
	}
	else BCGPMessageBox( _T("Could not find stimulus.") );
}

void ConditionPageStimuli::OnClickBnSE() 
{
	DoSE( CBO_STIM );
}

void ConditionPageStimuli::OnClickBnSEW()
{
	DoSE( CBO_STIMW );
}

void ConditionPageStimuli::OnClickBnSEP()
{
	DoSE( CBO_STIMP );
}

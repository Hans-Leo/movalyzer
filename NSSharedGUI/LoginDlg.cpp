// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "LoginDlg.h"

// LoginDlg dialog

IMPLEMENT_DYNAMIC(LoginDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(LoginDlg, CBCGPDialog)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

LoginDlg::LoginDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(LoginDlg::IDD, pParent)
#ifdef	_DEBUG
	, m_szServer(_T("")) , m_szUser(_T("")) , m_szPassword(_T(""))
#endif
{
}

LoginDlg::~LoginDlg()
{
}

void LoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SERVER, m_szServer);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_PASS, m_szPassword);
}


// LoginDlg message handlers
BOOL LoginDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_SERVER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the Faircom ctree-server hostname, URL, or IP. The server must be running and must be accessible from your machine over a network."), _T("Server") );
	pWnd = GetDlgItem( IDC_EDIT_USER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter your server user name."), _T("User Name") );
	pWnd = GetDlgItem( IDC_EDIT_PASS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the password for this user."), _T("Password") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Login"), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Cancel login."), _T("Cancel") );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL LoginDlg::OnHelpInfo(HELPINFO* pHelpInfo)
{
	::GoToHelp( _T("clientserver.html#connecting"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

BOOL LoginDlg::PreTranslateMessage(MSG* pMsg)
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void LoginDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szServer == _T("") )
	{
		BCGPMessageBox( _T("Please specify a server.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SERVER );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	if( m_szUser == _T("") )
	{
		BCGPMessageBox( _T("Please specify a user name.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_USER );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	if( m_szPassword == _T("") )
	{
		BCGPMessageBox( _T("Please specify a password.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASS );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CBCGPDialog::OnOK();
}

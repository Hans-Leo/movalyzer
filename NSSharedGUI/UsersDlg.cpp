// UsersDlg.cpp : implementation file
//

#include "stdafx.h"
#include "..\Common\UserObj.h"
#include "NSSharedGUI.h"
#include "LoginDlg.h"
#include "UsersDlg.h"
#include "..\DataMod\DataMod.h"
#include "Users.h"
#include <direct.h>
#include "Experiments.h"
#include "MoveUserDlg.h"
#include "..\NSSharedGUI\Progress.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	GRID_ID						99

#define	ID_USER_OPEN_NS				0		// stamp-looking thing
#define	ID_USER_OPEN_ALLOW			12		// green check mark
#define	ID_USER_OPEN_DENY			6		// red check mark
#define	ID_USER_OPEN_INVALID		7		// red folder
#define	ID_USER_OPEN_IMPROPER		1		// green folder

/////////////////////////////////////////////////////////////////////////////
// UsersDlg dialog

BEGIN_MESSAGE_MAP(UsersDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_EXIT, OnClickBnExit)
	ON_BN_CLICKED(IDC_BN_MOVE, OnClickBnMove)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

UsersDlg::UsersDlg(CWnd* pParent)
: CBCGPDialog(UsersDlg::IDD, pParent), m_pUser( NULL )
{
	m_szCurUser = _T("");
	m_fDelCurUser = FALSE;
	m_fVirgin = FALSE;
	m_fRefresh = FALSE;
	m_fUpdateUsers = FALSE;
}

UsersDlg::~UsersDlg()
{
	if( m_pUser ) m_pUser->Release();
}

void UsersDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_MOVE, m_bnMove);
}

/////////////////////////////////////////////////////////////////////////////
// UsersDlg message handlers

BOOL UsersDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// icon
	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDI_USER ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// button images
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnDel.SetImage( IDB_DEL, IDB_DEL, IDB_DEL_DIS );
	m_bnEdit.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnMove.SetImage( IDB_RIGHTALL, IDB_RIGHTALL, IDB_RIGHTALL );

	// Image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) ) ASSERT( FALSE );

	// Create grid
	CRect rectGrid( 12, 50, 340, 240 );
	DWORD dwViewStyle =	WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | LVS_REPORT;
	if( !m_grid.Create( dwViewStyle, rectGrid, this, GRID_ID ) )
	{
		TRACE0("Failed to create users grid\n");
		return TRUE;      // fail to create
	}
	// grid settings
	m_grid.EnableHeader();
	m_grid.EnableColumnAutoSize();
	m_grid.SetWholeRowSel( TRUE );
	m_grid.SetSingleSel( TRUE );
	m_grid.EnableMarkSortedColumn( TRUE );
	m_grid.EnableGroupByBox( FALSE );
	m_grid.SetImageList( &m_ImageList );
	// columns
	m_grid.InsertColumn( 0, _T(""), 20 );
	m_grid.SetColumnLocked( 0 );
	m_grid.InsertColumn( 1, _T("ID"), 60 );
	m_grid.InsertColumn( 2, _T("Description"), 150 );
	m_grid.InsertColumn( 3, _T("Status"), 100 );
	m_grid.SetSortColumn( 1 );
	m_grid.AdjustLayout();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_USER );
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify the selected user."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new user."), _T("Create New") );
	pWnd = GetDlgItem( IDC_BN_DEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Delete the selected user."), _T("Delete") );
	pWnd = GetDlgItem( GRID_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing user from the list."), _T("Select") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPTSEL, _T("Select") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close without selecting."), _T("Close") );
	pWnd = GetDlgItem( IDC_BN_MOVE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Move this user to a new location."), _T("Move User") );

	// Check for old user list if first time run
	static BOOL fChecked = FALSE;
	if( !fChecked && !CheckForOldList() ) return TRUE;
	fChecked = TRUE;

	// Fill list
	FillList();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL UsersDlg::CheckForOldList()
{
	// Default NS User (for examples)
	CString szNSUser = _T("UU1");
	CString szCSUser = _T("UUU");

	CString szAppPath, szPath, szImpExp, szOldFile;
	CFileFind ff;
	::GetAppPath( szAppPath );
	::GetDataPath( szPath, FALSE, TRUE );
	szImpExp = szPath;
	szImpExp += _T("TEMP.EXP");
	szOldFile.Format( _T("%suser.dat"), szAppPath );

	// Search for previous version user file (stored in program files directory)
	BOOL fOldExists = FALSE;
	if( ff.FindFile( szOldFile ) ) fOldExists = TRUE;
	// if virgin user db and old exists, we must export old and import into new
	if( fOldExists && m_fVirgin )
	{
		// export
		{
		NSMUsers u;
		u.SetDataPath( szAppPath );
		HRESULT hr = u.ResetToStart();
		if( SUCCEEDED( hr ) )
		{
			CString szID;
			CStdioFile ief;
			if( !ief.Open( szImpExp, CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate ) )
			{
				CString szMsg;
				szMsg.Format( _T("Unable to open temporary export file while porting users.\n\n%s\n\n"), szImpExp );
				szMsg += _T("Please check that you have write permissions for the above location.");
				BCGPMessageBox( szMsg );
				return FALSE;
			}
			while( SUCCEEDED( hr ) )
			{
				u.GetUserID( szID );
				if( ( szID != szCSUser ) && ( szID != szNSUser ) ) u.Export( &ief );
				hr = u.GetNext();
			}
		}
		}
		// import
		{
		NSMUsers u;
		u.SetDataPath( szPath );
		CString szLine;
		CStdioFile ief;
		if( ief.Open( szImpExp, CFile::modeRead ) )
		{
			BOOL f1 = FALSE, f2 = FALSE;
			while( ief.ReadString( szLine ) )
			{
				if( szLine == TAG_USER ) u.Import( &ief, f1, f2 );
			}
		}
		}

		// remove temporary export/import file
		CFile::Remove( szImpExp );
	}

	return TRUE;
}

void UsersDlg::FillList()
{
	// Clear list
	m_grid.RemoveAll();

	// Current user
	::GetCurrentUser( m_szCurUser );
	int nSelCol = 0;

	// Default NS User (for examples)
	CString szNSUser = _T("UU1");
	BOOL fFoundNS = FALSE;

	// Client/Server user (for those with complete package)
	CString szCSUser = _T("UUU");
	BOOL fFoundCS = FALSE;

	// Paths, files
	CString szPath;
	BOOL fIPUO = ::IsPrivateUserOn();
	::SetPrivateUserOn( FALSE );
	::GetDataPath( szPath, FALSE, TRUE );
	::SetPrivateUserOn( fIPUO );

	// Does user database exist?
	CString szUserDB = szPath;
	szUserDB += _T("user.dat");
	CFileFind ff;
	BOOL fDBExists = ff.FindFile( szUserDB );

	// Fill List
	CString szID, szDesc, szWinUser, szCurWinUser, szStatus,
			szRootPath, szBackupPath, szTooltip;
	BOOL fPrivate = FALSE;
	int nImage = -1;
	CBCGPGridRow *pRow = NULL, *pRowSel = NULL;
	CWaitCursor crs;
	::GetWinUser( szCurWinUser );
	if( fDBExists )
	{
		NSMUsers u;
		u.SetDataPath( szPath );

		// first count the # of users
		HRESULT hr = u.ResetToStart();
		long nUsers = (long)u.GetNumRecords();

		// set progress meter
		Progress::EnableProgress( nUsers );

		// now loop through and fill list
		if( SUCCEEDED( hr ) )
		{
			CString szMsg;
			int nUser = 0;
			BOOL fCanWrite = FALSE, fProper = FALSE;
			while( SUCCEEDED( hr ) )
			{
				u.GetUserID( szID );
				u.GetDescription( szDesc );
				u.GetPrivate( fPrivate );
				u.GetWinUser( szWinUser );
				u.GetRootPath( szRootPath );
				u.GetBackupPath( szBackupPath );

				// update progress meter
				nUser++;
				szMsg.Format( _T("USERS: Loading user %s - %s and verifying data locations (%.0f)."),
							  szID, szDesc, ( ( nUser * 1. ) / ( nUsers * 1. ) ) );
				Progress::SetProgress( nUser, szMsg );

				// verify path writeability and proper location
				NSMUsers::VerifyPaths( szID, szRootPath, szBackupPath, fPrivate, fProper, fCanWrite );

				// NS user?
				if( szID == szNSUser ) fFoundNS = TRUE;
				if( szID == szCSUser ) fFoundCS = TRUE;

				// adjust list image based upon path info
				if( ( szID != szCSUser ) || ( ( szID == szCSUser ) && ::IsCS( TRUE ) ) )
				{
					if( fPrivate )
					{
						if( szWinUser == szCurWinUser )
						{
							szStatus = _T("Private (A)");
							nImage = ID_USER_OPEN_ALLOW;
							szTooltip = _T("This user is in a valid and/or recommended location.");
							if( !fProper )
							{
								nImage = ID_USER_OPEN_IMPROPER;
								szTooltip = _T("This user is accessible, but is not in a recommended location.");
							}
							if( !fCanWrite )
							{
								nImage = ID_USER_OPEN_INVALID;
								szTooltip = _T("This user (or backup) is located in a non-accessible or non-existing location.\n You need to correct or move this user.");
							}
						}
						else
						{
							szStatus = _T("Private (NA)");
							nImage = ID_USER_OPEN_DENY;
							szTooltip = _T("This is a private user of another windows user and is not accessible.");
						}
					}
					else
					{
						szStatus = _T("Public");
						nImage = ID_USER_OPEN_ALLOW;
						szTooltip = _T("This user is in a valid and/or recommended location.");
						if( ( szID == szNSUser ) || ( szID == szCSUser ) )
						{
							nImage = ID_USER_OPEN_NS;
							szTooltip = _T("NeuroScript example user.");
						}
						else
						{
							if( !fProper )
							{
								nImage = ID_USER_OPEN_IMPROPER;
								szTooltip = _T("This user is accessible, but is not in a recommended location.");
							}
							if( !fCanWrite )
							{
								nImage = ID_USER_OPEN_INVALID;
								szTooltip = _T("This user (or backup) is located in a non-accessible or non-existing location.\n You need to correct or move this user.");
							}
						}
					}

					CBCGPGridRow* pRow = m_grid.CreateRow( m_grid.GetColumnCount() );
					pRow->GetItem( 0 )->SetImage( nImage );
					pRow->GetItem( 0 )->SetValue( szTooltip.GetBuffer() );
					pRow->GetItem( 1 )->SetValue( szID.GetBuffer(), FALSE );
					pRow->GetItem( 2 )->SetValue( szDesc.GetBuffer(), FALSE );
					pRow->GetItem( 3 )->SetValue( szStatus.GetBuffer(), FALSE );
					m_grid.AddRow( pRow, FALSE );
					if( szID == m_szCurUser ) pRowSel = pRow;
				}

				hr = u.GetNext();
			}
		}

		// end progress meter
		Progress::DisableProgress();
	}

	m_grid.AdjustLayout();

	// Ensure default NS user exists
	if( !fFoundNS )
	{
		NSMUsers u;
		szID = szNSUser;
		u.PutUserID( szID );
		szDesc = _T("NeuroScript Examples 1");
		u.PutDescription( szDesc );
		CString szAppPath;
		::GetDataPath( szAppPath, FALSE, TRUE );
		CString szPath = szAppPath;
		szPath += _T("Examples");
		CFileFind ff;
		if( !ff.FindFile( szPath ) ) _mkdir( szPath );
		szPath += _T("\\UU1");
		if( !ff.FindFile( szPath ) ) _mkdir( szPath );
		u.PutRootPath( szPath );
		szPath += _T("\\Backup");
		u.PutBackupPath( szPath );
		if( !ff.FindFile( szPath ) ) _mkdir( szPath );
		u.PutInputType( Preferences::itMouse );
		u.PutSysPass( _T("") );	// no pass
		u.PutEncrypted( TRUE );
		u.PutEncryptionMethod( (short)::GetEncryptionMethod() );
		// all else is default
		if( SUCCEEDED( u.Add() ) )
		{
			CBCGPGridRow* pRow = m_grid.CreateRow( m_grid.GetColumnCount() );
			pRow->GetItem( 0 )->SetImage( ID_USER_OPEN_NS );
			pRow->GetItem( 0 )->SetValue( _T("NeuroScript example user.") );
			pRow->GetItem( 1 )->SetValue( szID.GetBuffer(), FALSE );
			pRow->GetItem( 2 )->SetValue( szDesc.GetBuffer(), FALSE );
			pRow->GetItem( 3 )->SetValue( _T("Public"), FALSE );
			m_grid.AddRow( pRow, FALSE );
			m_grid.AdjustLayout();
		}
	}

	// Ensure default client/server user exists (if entitled)
	if( !fFoundCS && ::IsCS( TRUE ) )
	{
		NSMUsers u;
		szID = szCSUser;
		u.PutUserID( szID );
		szDesc = _T("Client-Server User");
		u.PutDescription( szDesc );
		CString szAppPath;
		::GetDataPath( szAppPath, FALSE, TRUE );
		u.PutRootPath( szAppPath );
		u.PutBackupPath( szAppPath );
		u.PutInputType( Preferences::itMouse );
		u.PutSysPass( _T("") );	// no pass (TODO: How do we manage this?)
		u.PutEncrypted( TRUE );
		u.PutEncryptionMethod( (short)::GetEncryptionMethod() );
		// all else is default
		if( SUCCEEDED( u.Add() ) )
		{
			CBCGPGridRow* pRow = m_grid.CreateRow( m_grid.GetColumnCount() );
			pRow->GetItem( 0 )->SetImage( ID_USER_OPEN_NS );
			pRow->GetItem( 0 )->SetValue( _T("NeuroScript example user.") );
			pRow->GetItem( 1 )->SetValue( szID.GetBuffer(), FALSE );
			pRow->GetItem( 2 )->SetValue( szDesc.GetBuffer(), FALSE );
			pRow->GetItem( 3 )->SetValue( _T("Public"), FALSE );
			m_grid.AddRow( pRow, FALSE );
			m_grid.AdjustLayout();
		}
	}

	// Select a user
	if( pRowSel )
	{
		m_grid.SetCurSel( pRowSel );
		m_grid.EnsureVisible( pRowSel );
	}
}

BOOL UsersDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);
	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void UsersDlg::OnOK()
{
	HRESULT hr;
	Experiments exp;
	BSTR bstr = NULL;
	CString szPath, szLck;
	CFileFind ff;
	int nRes;

	CBCGPGridRow* pRow = m_grid.GetCurSel();
	if( !pRow )
	{
		BCGPMessageBox( _T("No user has been selected in the list.") );
		return;
	}

	// which list item - get ID
	CBCGPGridItem* pItem = pRow->GetItem( 1 );
	CString szID = pItem->GetValue();

	// if this user is already open, end
	CString szCurUser;
	::GetCurrentUser( szCurUser );
	if( szID == szCurUser )
	{
		BCGPMessageBox( _T("This user is already open.") );
		return;
	}

	// verify this user can be accessed
	pItem = pRow->GetItem( 0 );
	DWORD dwData = (DWORD)pItem->GetImage();
	if( dwData == ID_USER_OPEN_DENY )
	{
		BCGPMessageBox( _T("You do not have permission to access this user.") );
		return;
	}
	else if( dwData == ID_USER_OPEN_INVALID )
	{
		BCGPMessageBox( _T("The location of this user's data is in a non-writeable location.\nPlease click the Move button in the User Management window to correct this problem.") );
		return;
	}
	else if( dwData == ID_USER_OPEN_IMPROPER )
	{
		nRes = BCGPMessageBox( _T("The location of this user's data is accessible, but is not in the recommended location.\nYou can click the Move button in the User Management window to correct this problem.\n\nContinue?"), MB_YESNO );
		if( nRes == IDNO ) return;
	}

	// create user
	if( !m_pUser )
		hr = ::CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
								 IID_INSMUser, (LPVOID*)&m_pUser );
	// locate user
	hr = m_pUser ? m_pUser->Find( szID.AllocSysString() ) : E_FAIL;
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to locate user.") );
		return;
	}

	// if this is the client-server user special handling required
	CString szServer, szUser, szPassword;
	BOOL fCS = FALSE;
	if( szID == _T("UUU") )
	{
		// test login
		if( !::VerifyLogin() ) return;

		fCS = TRUE;
	}

	if( fCS )
	{
		// If successful login, set the global specs
		::SetIsClientServerModeOn( TRUE );
		::ForceOperationMode( TRUE );

		// we're done
		CBCGPDialog::OnOK();
		return;
	}

	// what is the user's working directory
	m_pUser->get_RootPath( &bstr );
	szPath = bstr;

	// is this user already being used (by another instance)
	szLck = szPath;
	szLck += _T("\\user.lck");
	// if so, warn & provide option to continue or not
	if( ff.FindFile( szLck ) )
	{
		nRes = BCGPMessageBox( _T("This user is already in use. Continuing could potentially result in file access conflicts. Continue?"), MB_YESNO );
		if( nRes == IDNO )
		{
			m_pUser->Release();
			m_pUser = NULL;
			return;
		}
	}

	// if previous user has data changed, ask to back it up
	if( ::HasDataChanged() && !NSMUsers::BackupDB() ) return;

	// this user is not a client-server user
	::SetIsClientServerModeOn( FALSE );
	// Change operation mode
	m_pUser->SetOperationMode( FALSE, NULL, NULL, NULL );

	CBCGPDialog::OnOK();
}

void UsersDlg::OnCancel()
{
	if( m_fDelCurUser )
	{
		BCGPMessageBox( _T("You have deleted the current user. You must select another user.") );
		return;
	}

	CBCGPDialog::OnCancel();
}

void UsersDlg::OnClickBnAdd() 
{
	NSMUsers u;
	if( u.DoAdd( this ) )
	{
		CString szID, szDesc, szStatus, szDefault, szTest,
				szRootPath, szBackupPath, szTooltip;
		BOOL fPrivate = FALSE, fProper = FALSE, fCanWrite = FALSE;
		int nImage = ID_USER_OPEN_ALLOW;
		szTooltip = _T("This user is in a valid and/or recommended location.");
		CStdioFile f;

		u.GetUserID( szID );
		u.GetDescription( szDesc );
		u.GetPrivate( fPrivate );
		u.GetRootPath( szRootPath );
		u.GetBackupPath( szBackupPath );
		if( szID == m_szCurUser ) m_fDelCurUser = FALSE;
		if( fPrivate ) szStatus = _T("Private (A)");
		else szStatus = _T("Public");

		// verify paths
		NSMUsers::VerifyPaths( szID, szRootPath, szBackupPath, fPrivate, fProper, fCanWrite );
		if( !fProper )
		{
			nImage = ID_USER_OPEN_IMPROPER;
			szTooltip = _T("This user is accessible, but is not in a recommended location.");
		}
		if( !fCanWrite )
		{
			nImage = ID_USER_OPEN_INVALID;
			szTooltip = _T("This user (or backup) is located in a non-accessible or non-existing location.\n You need to correct or move this user.");
		}

		m_fUpdateUsers = TRUE;

		// insert
		CBCGPGridRow* pRow = m_grid.CreateRow( m_grid.GetColumnCount() );
		pRow->GetItem( 0 )->SetImage( nImage );
		pRow->GetItem( 0 )->SetValue( szTooltip.GetBuffer() );
		pRow->GetItem( 1 )->SetValue( szID.GetBuffer(), FALSE );
		pRow->GetItem( 2 )->SetValue( szDesc.GetBuffer(), FALSE );
		pRow->GetItem( 3 )->SetValue( szStatus.GetBuffer(), FALSE );
		m_grid.AddRow( pRow, FALSE );
		// adjust layout
		m_grid.AdjustLayout();
		// sort
		m_grid.Sort( 1 );
		// Highlite new item
		m_grid.SetCurSel( pRow );
		m_grid.EnsureVisible( pRow );
	}
}

void UsersDlg::OnClickBnEdit() 
{
	CBCGPGridRow* pRow = m_grid.GetCurSel();
	if( !pRow ) return;
	CBCGPGridItem* pItem = pRow->GetItem( 1 );
	CString szID = pItem->GetValue();

	// verify this user can be accessed
	pItem = pRow->GetItem( 0 );
	DWORD dwData = (DWORD)pItem->GetImage();
	if( dwData == ID_USER_OPEN_NS )
	{
#ifndef	_DEBUG
		if( szID == _T("UUU") )
		{
			BCGPMessageBox( _T("This is a protected user and cannot be modified.") );
			return;
		}

		if( szID.Left( 2 ) == _T("UU") )
			BCGPMessageBox( _T("This is a protected user. Certain properties will not be changeable.") );
#endif
	}
	else if( dwData == ID_USER_OPEN_DENY )
	{
		BCGPMessageBox( _T("You do not have permission to access this user.") );
		return;
	}

	NSMUsers u;
	u.m_nPage = 0;
	if( u.DoEdit( szID, this ) )
	{
		CString szUser;
		::GetCurrentUser( szUser );
		CString szDesc, szDelim, szDelimOld, szPath, szPathOld, szPass, szPassOld;
		if( szUser == szID )
		{
			BOOL fPassSet = ::HasPassBeenSet();
			BOOL fPrivOn = ::IsPrivacyOn();

			u.GetRootPath( szPath );
			u.GetDelimiter( szDelim );
			u.GetSysPass( szPass );
			szPassOld = ::GetUserPassword();
			::SetUserPassword( szPass );
			::GetDataPathRoot( szPathOld );
			::GetDelimiter( szDelimOld );
			TRIM( szDelimOld );
			::SetDataPathRoot( szPath );
			::SetDelimiter( szDelim );
			// if pass hasn't changed, restore pass being set & privacy settings
			if( szPass == szPassOld )
			{
				::SetHasPassBeenSet( fPassSet );
				::SetIsPrivacyOn( fPrivOn );
			}
			// if any of these items has changed, we need to refresh
			if( ( szPath != szPathOld ) || ( szDelim != szDelimOld ) ||
				( szPass != szPassOld ) )
				m_fRefresh = TRUE;
		}

		u.GetDescription( szDesc );
		pItem = pRow->GetItem( 2 );
		pItem->SetValue( szDesc.GetBuffer(), TRUE );

		m_fUpdateUsers = TRUE;
	}
}

void UsersDlg::OnClickBnDel() 
{
	CBCGPGridRow* pRow = m_grid.GetCurSel();
	if( !pRow ) return;
	CBCGPGridItem* pItem = pRow->GetItem( 1 );
	CString szID = pItem->GetValue();

	// verify this user can be accessed
	pItem = pRow->GetItem( 0 );
	DWORD dwData = (DWORD)pItem->GetImage();
	if( dwData == ID_USER_OPEN_NS )
	{
		BCGPMessageBox( _T("This is a protected user.") );
		return;
	}
	else if( dwData == ID_USER_OPEN_DENY )
	{
		BCGPMessageBox( _T("You do not have permission to access this user.") );
		return;
	}

	if( BCGPMessageBox( _T("Are you sure you want to remove this user from this list?"), MB_YESNO ) == IDNO ) return;

	NSMUsers u;
	if( SUCCEEDED( u.Find( szID ) ) )
	{
		if( SUCCEEDED( u.Remove() ) )
		{
			m_grid.RemoveRow( pRow->GetRowId() );
			m_grid.AdjustLayout();
			if( m_szCurUser == szID ) m_fDelCurUser = TRUE;
			m_fUpdateUsers = TRUE;
		}
		else
		{
			BCGPMessageBox( _T("Unable to remove user.") );
			return;
		}
	}
	else
	{
		BCGPMessageBox( _T("Unable to locate user.") );
		return;
	}

	// Delete user from backup log
	BSTR bstrID = NULL;
	CString szBID;
	IBackup* pB = NULL;
	HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
								   IID_IBackup, (LPVOID*)&pB );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create Backup object.") );
		return;
	}
	hr = pB->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pB->get_UserID( &bstrID );
		szBID = bstrID;

		if( szID == szBID ) pB->Remove();

		hr = pB->GetNext();
	}
	pB->Release();
	::DataHasChanged();
}

BOOL UsersDlg::OnHelpInfo( HELPINFO* pHelpInfo ) 
{
	::GoToHelp( _T("newuser.html"), this );
	return CBCGPDialog::OnHelpInfo( pHelpInfo );
}

void UsersDlg::OnClickBnExit()
{
	exit( EXIT_FAILURE );
}

void UsersDlg::OnClickBnMove()
{
	CBCGPGridRow* pRow = m_grid.GetCurSel();
	if( !pRow ) return;
	CBCGPGridItem* pItem = pRow->GetItem( 1 );
	CString szID = pItem->GetValue();
	int nIdx = pRow->GetRowId();

	// verify they are not trying to move a user that is currently open
	if( szID == m_szCurUser )
	{
		BCGPMessageBox( _T("This user is currently open.\nPlease select another user first.") );
		return;
	}

	// verify this user can be accessed
	pItem = pRow->GetItem( 0 );
	DWORD dwData = (DWORD)pItem->GetImage();
	if( dwData == ID_USER_OPEN_NS )
	{
		BCGPMessageBox( _T("This is a protected user.") );
		return;
	}
	else if( dwData == ID_USER_OPEN_DENY )
	{
		BCGPMessageBox( _T("You do not have permission to access this user.") );
		return;
	}

	// create user
	HRESULT hr;
	if( !m_pUser )
		hr = ::CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
								 IID_INSMUser, (LPVOID*)&m_pUser );
	// locate user
	hr = m_pUser ? m_pUser->Find( szID.AllocSysString() ) : E_FAIL;
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to locate user.") );
		return;
	}

	// user properties
	CString szRootPath, szBackupPath;
	BSTR bstr = NULL;
	BOOL fPrivate = FALSE;
	m_pUser->get_RootPath( &bstr );
	szRootPath = bstr;
	m_pUser->get_BackupPath( &bstr );
	szBackupPath = bstr;
	m_pUser->get_Private( &fPrivate );

	// MOVE DATA
	MoveUserDlg d( this );
	d.m_szID = szID;
	// Root path first
	BOOL fCont = TRUE, fDidOne = FALSE;
	if( BCGPMessageBox( _T("Do you want to copy the data path to a new location?"), MB_YESNO ) == IDNO )
		fCont = FALSE;
	if( fCont )
	{
		d.m_szPathOld = szRootPath;
		d.m_fPrivate = fPrivate;
		if( d.DoModal() == IDCANCEL ) return;
		fDidOne = TRUE;
	}
	// now backup path (if not a subdirectory)
	if( BCGPMessageBox( _T("Do you want to copy the backup path to a new location?"), MB_YESNO ) == IDYES )
	{
		d.m_szPathOld = szBackupPath;
		d.m_fBackup = TRUE;
		if( d.DoModal() == IDOK ) fDidOne = TRUE;
	}

	if( fDidOne )
	{
		// refill list
		FillList();
		// Select last user
		pRow = m_grid.GetRow( nIdx );
		if( pRow ) m_grid.SetCurSel( pRow );
	}
}

BEGIN_MESSAGE_MAP( UserGrid, CBCGPReportCtrl )
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

void UserGrid::OnLButtonDblClk( UINT nHitTest, CPoint point )
{
	CBCGPGridRow* pSel = GetCurSel();
	if( pSel != NULL )
	{
		UsersDlg* pParent = (UsersDlg*)GetParent();
		if( pParent ) pParent->OnOK();
	}
	else CBCGPReportCtrl::OnLButtonDblClk( nHitTest, point );
}

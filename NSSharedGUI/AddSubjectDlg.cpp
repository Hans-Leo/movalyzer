// AddSubjectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "AddSubjectDlg.h"
#include "SubjectDlg.h"
#include "RelationshipDlg.h"
#include "PasswordDlg.h"
#include "Subjects.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddSubjectDlg dialog

BEGIN_MESSAGE_MAP(AddSubjectDlg, CBCGPDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_WM_HELPINFO()
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST, OnLvnKeydownList)
END_MESSAGE_MAP()

AddSubjectDlg::AddSubjectDlg(CStringList* pExisting, CWnd* pParent, BOOL fSingleSel)
	: AddDlg(pExisting, pParent, fSingleSel)
{
}

AddSubjectDlg::~AddSubjectDlg()
{
	Cleanup();
}

void AddSubjectDlg::Cleanup()
{
}

void AddSubjectDlg::InsertColumns()
{
	// List size (width)
	RECT rect;
	m_list.GetClientRect( &rect );
	int nWidth = rect.right - rect.left;

	CString szCol;
	// Column Headers
	szCol.LoadString( IDS_COL_ID );
	m_list.InsertColumn( 0, szCol, LVCFMT_LEFT, 70, 0 );
	szCol = _T("Code");
	m_list.InsertColumn( 1, szCol, LVCFMT_LEFT, 85, 1 );
	szCol = _T("Alias1/Last Name");
	m_list.InsertColumn( 2, szCol, LVCFMT_LEFT, 85, 2 );
	szCol = _T("Alias2/First Name");
	m_list.InsertColumn( 3, szCol, LVCFMT_LEFT, 75, 3 );
	szCol.LoadString( IDS_COL_NOTES );
	m_list.InsertColumn( 4, szCol, LVCFMT_LEFT, 200, 4 );
	szCol = _T("Prv Notes");
	m_list.InsertColumn( 5, szCol, LVCFMT_LEFT, 200, 5 );
	m_list.InsertColumn( 6, _T("Added"), LVCFMT_LEFT, 100, 6 );
}

void AddSubjectDlg::FillList()
{
	// Loop through all
	Subjects subj;
	if( !subj.IsValid() ) return;

	CString szID, szLast, szFirst, szNotes, szPrvNotes, szItem, szCode;
	COleDateTime dtAdd;
	int nItem = 0, nImage = GetImageNum();

	HRESULT hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetID( szID );
		if( m_pExisting && ( m_pExisting->Find( szID ) == NULL ) )
		{
			nItem = m_list.InsertItem( nItem, szID, nImage );
			if( nItem != -1 )
			{
				subj.GetCode( szCode );
				subj.GetNameLast( szLast, ::IsPrivacyOn() );
				subj.GetNameFirst( szFirst, ::IsPrivacyOn() );
				subj.GetNotes( szNotes );
				subj.GetPrvNotes( szPrvNotes, ::IsPrivacyOn() );
				subj.GetDateAdded( dtAdd );
				szItem = dtAdd.Format( VAR_DATEVALUEONLY );

				m_list.SetItem( nItem, 1, LVIF_TEXT, szCode, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 2, LVIF_TEXT, szLast, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 3, LVIF_TEXT, szFirst, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 4, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 5, LVIF_TEXT, szPrvNotes, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 6, LVIF_TEXT, szItem, 0, 0, 0, NULL );

				nItem++;
			}
		}

		hr = subj.GetNext();
	}

	if( nItem <= 0 ) OnClickBnAdd();
}

/////////////////////////////////////////////////////////////////////////////
// AddSubjectDlg message handlers

BOOL AddSubjectDlg::OnInitDialog() 
{
	// Window Title
	CString szTitle;
	szTitle.LoadString( IDS_ADD_SUBJ );
	SetWindowText( szTitle );

	m_tooltip.SetThisIcon( IDR_SUBJ );

	return AddDlg::OnInitDialog();
}

void AddSubjectDlg::OK()
{
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	CString szID, szLast, szFirst, szItem, szCode;
	int nIdx = 0;
	while( pos )
	{
		nIdx = m_list.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szID = m_list.GetItemText( nIdx, 0 );
			szCode = m_list.GetItemText( nIdx, 1 );
			szLast = m_list.GetItemText( nIdx, 2 );
			szFirst = m_list.GetItemText( nIdx, 3 );

			CString szDelim;
			::GetDelimiter( szDelim );
//			szItem.Format( _T("%s, %s (%s)%s%s"), szLast, szFirst, szCode, szDelim, szID );
			szItem.Format( _T("(%s) %s, %s%s%s"), szCode, szLast, szFirst, szDelim, szID );
			m_lstNew.AddTail( szItem );
		}
	}
}

int AddSubjectDlg::Add()
{
	Subjects subj;
	if( !subj.IsValid() ) return -1;

	if( subj.DoAdd( this ) )
	{
		CString szID;
		subj.GetID( szID );
		int nItem = m_list.GetItemCount(), nImage = GetImageNum();
		nItem = m_list.InsertItem( nItem, szID, nImage );
		if( nItem != -1 )
		{
			CString szLast, szFirst, szNotes, szPrvNotes, szItem, szCode;
			COleDateTime dt;

			subj.GetCode( szCode );
			subj.GetNameLast( szLast );
			subj.GetNameFirst( szFirst );
			subj.GetName( szNotes );
			subj.GetPrvNotes( szPrvNotes );
			subj.GetDateAdded( dt );
			szItem = dt.Format( VAR_DATEVALUEONLY );

			m_list.SetItem( nItem, 1, LVIF_TEXT, szCode, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 2, LVIF_TEXT, szLast, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 3, LVIF_TEXT, szFirst, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 4, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 5, LVIF_TEXT, szPrvNotes, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 6, LVIF_TEXT, szItem, 0, 0, 0, NULL );

			return nItem;
		}
	}

	return -1;
}

BOOL AddSubjectDlg::Edit( int nItem )
{
	Subjects subj;
	if( !subj.IsValid() ) return FALSE;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	if( subj.DoEdit( szID, this ) )
	{
		CString szLast, szFirst, szNotes, szPrvNotes, szItem, szCode;
		COleDateTime dt;

		subj.GetCode( szCode );
		subj.GetNameLast( szLast );
		subj.GetNameFirst( szFirst );
		subj.GetName( szNotes );
		subj.GetPrvNotes( szPrvNotes );
		subj.GetDateAdded( dt );
		szItem = dt.Format( VAR_DATEVALUEONLY );

		m_list.SetItem( nItem, 1, LVIF_TEXT, szCode, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 2, LVIF_TEXT, szLast, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 3, LVIF_TEXT, szFirst, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 4, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 5, LVIF_TEXT, szPrvNotes, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 6, LVIF_TEXT, szItem, 0, 0, 0, NULL );
	}

	return TRUE;
}

BOOL AddSubjectDlg::Delete( int nItem )
{
	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	Subjects subj;
	if( !subj.IsValid() ) return FALSE;

	if( SUCCEEDED( subj.Find( szID ) ) )
	{
		if( FAILED( subj.Remove() ) )
		{
			BCGPMessageBox( IDS_DEL_ERR );
			return FALSE;
		}
	}

	return TRUE;
}

void AddSubjectDlg::Relationships( int nItem )
{
	Subjects subj;
	if( !subj.IsValid() ) return;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( SUCCEEDED( subj.Find( szID ) ) )
	{
		CString szItem;
		subj.GetNameWithID( szItem, ::IsPrivacyOn() );

		RelationshipDlg d( this, REL_SUBJECT, szItem );
		d.DoModal();
	}
	else BCGPMessageBox( _T("Could not find subject.") );
}

BOOL AddSubjectDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newsubject.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

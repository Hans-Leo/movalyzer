// CustomStyle.cpp: implementation of the CCustomStyle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "CustomStyle.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CCustomStyle, CBCGPVisualManager2007)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCustomStyleSettings::CCustomStyleSettings()
{
	Reset();
}

void CCustomStyleSettings::Reset()
{
	m_appbg1 = RGB( 85, 124, 159 );
	m_appbg2 = RGB( 223, 231, 229 );
	m_splitter1 = RGB( 113, 175, 210 );
	m_splitter2 = RGB( 255, 255, 255 );
	m_barcaptionbg1 = RGB( 255, 255, 255 );
	m_barcaptionbg2 = RGB( 0, 159, 98 );
	m_taberase1 = RGB( 113, 175, 210 );
	m_taberase2 = RGB( 255, 255, 255 );
	m_menuhl1 = RGB( 255, 255, 255 );
	m_menuhl2 = RGB( 0, 159, 98 );
	m_barbg1 = RGB( 113, 175, 210 );
	m_barbg2 = RGB( 255, 255, 255 );
	m_gripper1 = RGB( 255, 255, 255 );
	m_gripper2 = RGB( 0, 159, 98 );
	m_menubartext = RGB( 255, 255, 255 );
/*
	m_appbg1 = RGB( 85, 124, 159 );
	m_appbg2 = RGB( 223, 231, 229 );
	m_splitter1 = RGB( 223, 231, 229 );
	m_splitter2 = RGB( 223, 231, 229 );
	m_barcaptionbg1 = RGB( 85, 124, 159 );
	m_barcaptionbg2 = RGB( 85, 124, 159 );
	m_taberase1 = RGB( 113, 175, 210 );
	m_taberase2 = RGB( 255, 255, 255 );
	m_menuhl1 = RGB( 255, 255, 255 );
	m_menuhl2 = RGB( 0, 159, 98 );
	m_barbg1 = RGB( 114, 139, 161 );
	m_barbg2 = RGB( 223, 231, 229 );
	m_gripper1 = RGB( 255, 255, 255 );
	m_gripper2 = RGB( 85, 124, 159 );
	m_menubartext = RGB( 255, 255, 255 );
*/
}

// global var for settings (visual managers are passed abstract-wise)
CCustomStyleSettings	_settings;

void CCustomStyle::ResetColors()
{
	CCustomStyleSettings s;
	_settings.m_appbg1 = s.m_appbg1;
	_settings.m_appbg2 = s.m_appbg2;
	_settings.m_splitter1 = s.m_splitter1;
	_settings.m_splitter2 = s.m_splitter2;
	_settings.m_barcaptionbg1 = s.m_barcaptionbg1;
	_settings.m_barcaptionbg2 = s.m_barcaptionbg2;
	_settings.m_taberase1 = s.m_taberase1;
	_settings.m_taberase2 = s.m_taberase2;
	_settings.m_menuhl1 = s.m_menuhl1;
	_settings.m_menuhl2 = s.m_menuhl2;
	_settings.m_barbg1 = s.m_barbg1;
	_settings.m_barbg2 = s.m_barbg2;
	_settings.m_gripper1 = s.m_gripper1;
	_settings.m_gripper2 = s.m_gripper2;
	_settings.m_menubartext = s.m_menubartext;
}

void CCustomStyle::SetColors( CCustomStyleSettings& s )
{
	_settings.m_appbg1 = s.m_appbg1;
	_settings.m_appbg2 = s.m_appbg2;
	_settings.m_splitter1 = s.m_splitter1;
	_settings.m_splitter2 = s.m_splitter2;
	_settings.m_barcaptionbg1 = s.m_barcaptionbg1;
	_settings.m_barcaptionbg2 = s.m_barcaptionbg2;
	_settings.m_taberase1 = s.m_taberase1;
	_settings.m_taberase2 = s.m_taberase2;
	_settings.m_menuhl1 = s.m_menuhl1;
	_settings.m_menuhl2 = s.m_menuhl2;
	_settings.m_barbg1 = s.m_barbg1;
	_settings.m_barbg2 = s.m_barbg2;
	_settings.m_gripper1 = s.m_gripper1;
	_settings.m_gripper2 = s.m_gripper2;
	_settings.m_menubartext = s.m_menubartext;
}

CCustomStyle::CCustomStyle()
{
	m_ctrlStatusBarPaneBorder.Create(
		CBCGPControlRendererParams( IDB_MAC_SB_PANE, CRect (0, 0, 30, 20), CRect (6, 6, 6, 6)));

	m_StatusBarSizeBox.SetTransparentColor ((COLORREF)(-1));
	m_StatusBarSizeBox.SetImageSize (CSize (12, 12));
	m_StatusBarSizeBox.Load (IDB_MAC_SB_SIZEGRIP);

	m_nMenuShadowDepth = 3;
}
//****************************************************************************************
CCustomStyle::~CCustomStyle()
{

}
CSize CCustomStyle::GetSystemBorders () const
{
	return CSize( ::GetSystemMetrics( SM_CYSIZEFRAME ), ::GetSystemMetrics( SM_CXSIZEFRAME ) );
}

//****************************************************************************************
// grip object to grab a menu/control/outlook bar (CUSTOMIZE)
void CCustomStyle::OnDrawBarGripper( CDC* pDC, CRect rectGripper, BOOL bHorz, CBCGPBaseControlBar* pBar )
{
	if (bHorz) rectGripper.DeflateRect( 3, 4 );
	else rectGripper.DeflateRect( 4, 3 );

	CBCGPDrawManager dm( *pDC );
	dm.FillGradient( rectGripper, _settings.m_gripper1, _settings.m_gripper2, bHorz );

	if (bHorz) rectGripper.InflateRect( 1, 0 );
	else rectGripper.InflateRect( 0, 1 );

	dm.DrawShadow( rectGripper, 3 );
}
//****************************************************************************************
// control/outlook bar caption background
COLORREF CCustomStyle::OnFillMiniFrameCaption (CDC* pDC, CRect rectCaption, 
											CBCGPMiniFrameWnd* pFrameWnd, 
											BOOL bActive)
{
	CBCGPDrawManager dm (*pDC);
	dm.FillGradient( rectCaption, _settings.m_appbg1, _settings.m_appbg2, FALSE );

	return RGB (0, 0, 0);
}
//*****************************************************************************************
// background of menu/control/outlook bars (CUSTOMIZE)
void CCustomStyle::OnFillBarBackground  (CDC* pDC, CBCGPBaseControlBar* pBar,
										CRect rectClient, CRect rectClip,
										BOOL bNCArea)
{
	ASSERT_VALID (pDC);
	ASSERT_VALID (pBar);

	CBCGPDrawManager dm (*pDC);

	if (!bNCArea)
	{
		CRgn rgn;
		rgn.CreateRectRgnIndirect (&rectClient);

		pDC->SelectClipRgn (&rgn);
	}

	CRect rectFill = rectClient;

	if (pBar->IsKindOf (RUNTIME_CLASS (CBCGPStatusBar)))
    {
		CSize szSysBorder (GetSystemBorders ());

		CRect rect (rectClient);
		dm.FillGradient( rect, _settings.m_barbg1, _settings.m_barbg2, FALSE );

		rect.InflateRect (szSysBorder.cx, 0, szSysBorder.cx, szSysBorder.cy);
		CRect rect2( rect.left, rect.top, rect.right, rect.top + 1 );
		dm.FillGradient( rect2, _settings.m_barbg2, _settings.m_barbg1, FALSE );
	}
	else if (!pBar->IsFloating () &&
		!pBar->IsKindOf (RUNTIME_CLASS (CBCGPPopupMenuBar)))
	{
		CRect rectMainFrame;
		pBar->GetTopLevelFrame ()->GetWindowRect (rectMainFrame);

		pBar->ScreenToClient (&rectMainFrame);
		rectFill = rectMainFrame;

		if (bNCArea)
		{
			CRect rectWindow;
			pBar->GetWindowRect (rectWindow);

			pBar->ScreenToClient (rectWindow);

			CRect rectClientActual;
			pBar->GetClientRect (rectClientActual);

			rectFill.left += rectClientActual.left - rectWindow.left;
			rectFill.top += rectClientActual.top - rectWindow.top;
			rectFill.right += 10;
		}
	}

	dm.FillGradient (rectFill, _settings.m_barbg1, _settings.m_barbg2, FALSE);

	if (!bNCArea) pDC->SelectClipRgn (NULL);
}
//************************************************************************************
// highlighted menu color (CUSTOMIZE)
void CCustomStyle::OnHighlightMenuItem (CDC* pDC, CBCGPToolbarMenuButton* pButton,
											CRect rect, COLORREF& clrText)
{
	CBCGPDrawManager dm (*pDC);

	rect.DeflateRect (1, 2);
	dm.FillGradient (rect, _settings.m_menuhl1, _settings.m_menuhl2, FALSE);

	rect.InflateRect (0, 1);
	dm.DrawShadow (rect, 3);

	clrText = RGB (0, 0, 0);
}

// menu separator
//**************************************************************************************
void CCustomStyle::OnDrawSeparator (CDC* pDC, CBCGPBaseControlBar* pBar, CRect rect, BOOL bHorz)
{
	rect.DeflateRect (2, 2);
	CBCGPVisualManager2007::OnDrawSeparator (pDC, pBar, rect, bHorz);
}

// color of empty area behind tabs (CUSTOMIZE)
//**************************************************************************************
void CCustomStyle::OnEraseTabsArea (CDC* pDC, CRect rect, const CBCGPBaseTabWnd* /*pTabWnd*/)
{
	CBCGPDrawManager dm (*pDC);
	dm.FillGradient (rect, _settings.m_taberase1, _settings.m_taberase2, FALSE);
}

// control bar caption background color (CUSTOMIZE)
//****************************************************************************************
COLORREF CCustomStyle::OnDrawControlBarCaption (CDC* pDC, CBCGPDockingControlBar* pBar, 
	BOOL bActive, CRect rectCaption, CRect rectButtons)
{
	CBCGPDrawManager dm (*pDC);
	dm.FillGradient (rectCaption, _settings.m_barcaptionbg1, _settings.m_barcaptionbg2, TRUE);
	return RGB (0, 0, 0);
}

// outlook bar category button fill
//*********************************************************************************
void CCustomStyle::OnFillOutlookPageButton (CDC* pDC, const CRect& rect,
										BOOL bIsHighlighted, BOOL bIsPressed,
										COLORREF& clrText)
{
	CBCGPDrawManager dm (*pDC);

	COLORREF clr1 = RGB (155, 255, 255);
	COLORREF clr2 = RGB (100, 159, 198);

	dm.FillGradient2 (rect, bIsPressed ? clr2 : clr1, bIsPressed ? clr1 : clr2,
		bIsHighlighted ? 135 : 45);
}

// border around outlook bar category buttons
//*********************************************************************************
void CCustomStyle::OnDrawOutlookPageButtonBorder (CDC* pDC, CRect& rectBtn, BOOL bIsHighlighted, BOOL bIsPressed)
{
	COLORREF clr = bIsHighlighted ? RGB (155, 255, 255) : RGB (100, 159, 198);
	pDC->Draw3dRect (rectBtn, clr, clr);
}

// color of text for menu/icon items (CUSTOMIZE)
//**************************************************************************************
COLORREF CCustomStyle::GetToolbarButtonTextColor (CBCGPToolbarButton* pButton,
												  CBCGPVisualManager2007::BCGBUTTON_STATE state)
{
	return _settings.m_menubartext;
}

// splitter-window/bar-sizer color (CUSTOMIZE)
//*********************************************************************************
void CCustomStyle::OnDrawSplitterBorder(CDC* pDC, CBCGPSplitterWnd* pSplitterWnd, CRect rect)
{
	CBCGPVisualManager2007::OnDrawSplitterBorder (pDC, pSplitterWnd, rect);
// 	pDC->Draw3dRect (rect, globalData.clrBarShadow, globalData.clrBarShadow);
// 	rect.InflateRect(-CX_BORDER, -CY_BORDER);
// 	pDC->Draw3dRect (rect, _settings.m_splitter1, _settings.m_splitter2);
}

void CCustomStyle::OnFillSplitterBackground (CDC* pDC, CBCGPSplitterWnd* /*pSplitterWnd*/, CRect rect)
{
	CBCGPDrawManager dm (*pDC);
	dm.FillGradient( rect, _settings.m_splitter1, _settings.m_splitter2, FALSE );
}

// slider
void CCustomStyle::OnDrawSlider (CDC* pDC, CBCGPSlider* pSlider, CRect rect, BOOL bAutoHideMode)
{
	if (bAutoHideMode)
	{
		CBCGPVisualManager2007::OnDrawSlider (pDC, pSlider, rect, bAutoHideMode);
		return;
	}

	CBCGPDrawManager dm (*pDC);
	dm.FillGradient( rect, _settings.m_splitter1, _settings.m_splitter2, FALSE );
}
void CCustomStyle::OnDrawStatusBarPaneBorder( CDC* pDC, CBCGPStatusBar* pBar, CRect rectPane, UINT uiID, UINT nStyle )
{
	if ((nStyle & SBPS_STRETCH) == 0)
	{
		rectPane.left--;
		if (rectPane.Width () < 
			(m_ctrlStatusBarPaneBorder.GetParams ().m_rectCorners.left + 
			 m_ctrlStatusBarPaneBorder.GetParams ().m_rectCorners.right - 2))
		{
			rectPane.right = rectPane.left + 
				m_ctrlStatusBarPaneBorder.GetParams ().m_rectCorners.left + 
			    m_ctrlStatusBarPaneBorder.GetParams ().m_rectCorners.right - 2;
		}
		m_ctrlStatusBarPaneBorder.Draw (pDC, rectPane, 0);
	}
}

void CCustomStyle::OnDrawRibbonCaption( CDC* pDC, CBCGPRibbonBar* pBar, CRect rect, CRect rectText )
{
	CRect rectCaption( rect );
	CSize szSysBorder( GetSystemBorders() );

//	pBar->IsMainRibbonBar(); >>>

	rectCaption.DeflateRect( szSysBorder.cx - 2, 0 );
	CBCGPDrawManager dm( *pDC );
	dm.FillGradient( rectCaption, _settings.m_appbg1, _settings.m_appbg2, FALSE );
}

void CCustomStyle::DrawNcCaption( CDC* pDC, CRect rectCaption, DWORD dwStyle, DWORD dwStyleEx,
								  const CString& strTitle, const CString& strDocument,
								  HICON hIcon, BOOL bPrefix, BOOL bActive, BOOL bTextCenter,
								  const CObList& lstSysButtons )
{
	const BOOL bIsRTL           = (dwStyleEx & WS_EX_LAYOUTRTL) == WS_EX_LAYOUTRTL;
	const BOOL bIsSmallCaption	= (dwStyleEx & WS_EX_TOOLWINDOW) != 0;
	const int nSysCaptionHeight = bIsSmallCaption ? ::GetSystemMetrics (SM_CYSMCAPTION) : ::GetSystemMetrics (SM_CYCAPTION);
	CSize szSysBorder( GetSystemBorders() );
	BOOL bMaximized = (dwStyle & WS_MAXIMIZE) == WS_MAXIMIZE;
	int indexBorder = 0;
	CDC memDC;
	CBitmap memBmp, *pBmpOld = NULL;
	CRect rectBorderCaption (rectCaption), rect( rectCaption ), rectCaptionB( rectCaption );

	memDC.CreateCompatibleDC( pDC );
	memBmp.CreateCompatibleBitmap( pDC, rectCaption.Width(), rectCaption.Height() );
	pBmpOld = memDC.SelectObject( &memBmp );
	memDC.BitBlt( 0, 0, rectCaption.Width(), rectCaption.Height(), pDC, 0, 0, SRCCOPY );

	if( bMaximized )
	{
		rectBorderCaption.OffsetRect( -rectBorderCaption.TopLeft() );
		rectBorderCaption.bottom -= szSysBorder.cy;
	}
	m_ctrlMainBorderCaption.Draw( &memDC, rectBorderCaption, indexBorder );

	rectCaptionB.DeflateRect( ( bMaximized ? 0 : szSysBorder.cx ) - 2, 0);
	CBCGPDrawManager dm( memDC );
	if( pDC->GetWindow() == AfxGetMainWnd() )
		dm.FillGradient( rectCaptionB, _settings.m_appbg1, _settings.m_appbg2, FALSE );
	else
		dm.FillGradient( rectCaptionB, _settings.m_barbg1, _settings.m_barbg2, FALSE );

	rect.DeflateRect( szSysBorder.cx, szSysBorder.cy, szSysBorder.cx, 0 );
	rect.top = rect.bottom - nSysCaptionHeight - 1;

	// Draw icon:
	if( hIcon != NULL && !bIsSmallCaption )
	{
		CSize szIcon( ::GetSystemMetrics( SM_CXSMICON ), ::GetSystemMetrics( SM_CYSMICON ) );

		long x = rect.left + ( bMaximized ? szSysBorder.cx : 0 ) + 2;
		long y = rect.top + max( 0, ( nSysCaptionHeight - szIcon.cy ) / 2 );

		::DrawIconEx( memDC.GetSafeHdc(), x, y, hIcon, szIcon.cx, szIcon.cy,
					  0, NULL, DI_NORMAL );

		rect.left = x + szIcon.cx + ( bMaximized ? szSysBorder.cx : 4 );
	}

	// Draw system buttons:
	int xButtonsRight = rect.right;

	for( POSITION pos = lstSysButtons.GetHeadPosition(); pos != NULL; )
	{
		CBCGPFrameCaptionButton* pButton = (CBCGPFrameCaptionButton*)lstSysButtons.GetNext( pos );
		ASSERT_VALID( pButton );

		BCGBUTTON_STATE state = ButtonsIsRegular;

		if( pButton->m_bPushed && pButton->m_bFocused ) state = ButtonsIsPressed;
		else if( pButton->m_bFocused ) state = ButtonsIsHighlighted;

		UINT uiHit = pButton->GetHit();
		UINT nButton = 0;

		switch (uiHit)
		{
			case HTCLOSE_BCG:
				nButton = SC_CLOSE;
				break;
			case HTMAXBUTTON_BCG:
				nButton = ( dwStyle & WS_MAXIMIZE ) == WS_MAXIMIZE ? SC_RESTORE : SC_MAXIMIZE;
				break;
			case HTMINBUTTON_BCG:
				nButton = ( dwStyle & WS_MINIMIZE ) == WS_MINIMIZE ? SC_RESTORE : SC_MINIMIZE;
				break;
			case HTHELPBUTTON_BCG:
				nButton = SC_CONTEXTHELP;
				break;
		}

		CRect rectBtn( pButton->GetRect() );
		if( bMaximized ) rectBtn.OffsetRect( szSysBorder.cx, szSysBorder.cy );

		DrawNcBtn( &memDC, rectBtn, nButton, state, FALSE, bActive, FALSE );
		xButtonsRight = min( xButtonsRight, pButton->GetRect().left );
	}

	// Draw text:
	if( ( !strTitle.IsEmpty() || !strDocument.IsEmpty() ) && ( rect.left < rect.right ) )
	{
		CFont* pOldFont = (CFont*)memDC.SelectObject( &m_AppCaptionFont );
		CRect rectText = rect;
		rectText.right = xButtonsRight - 1;
		DrawNcText( &memDC, rectText, strTitle, strDocument, bPrefix, bActive, bIsRTL, bTextCenter );
		memDC.SelectObject( pOldFont );
	}

	pDC->BitBlt( rectCaption.left, rectCaption.top, rectCaption.Width(), rectCaption.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject( pBmpOld );
}

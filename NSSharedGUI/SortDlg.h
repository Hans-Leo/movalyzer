// SortDlg.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// SortDlg dialog

#define	SORT_GROUPS			1
#define	SORT_CONDITIONS		2
#define	SORT_SUBJECTS		3

#define	SORT_CHRONO			IDC_RDO_CHRONO
#define	SORT_ALPHA			IDC_RDO_ALPHA
#define	SORT_CUSTOM			IDC_RDO_CUSTOM

class AFX_EXT_CLASS SortDlg : public CBCGPDialog
{
// Construction
public:
	SortDlg(UINT nSort, CString szExp, CStringList* pAll, CWnd* pParent = NULL);
	~SortDlg();

// Dialog Data
	enum { IDD = IDD_SORTLIST };
	CBCGPListCtrl	m_List;
	UINT			m_nSort;
	UINT			m_nMode;
	CString			m_szExp;
	CStringList*	m_pAll;
	CStringList		m_lstSorted;
	BOOL*			m_pLstSelected;
	BOOL*			m_pLstSelectedOrig;
	NSToolTipCtrl	m_tooltip;
	CImageList		m_ImageList;
	CBCGPButton		m_bnUp;
	CBCGPButton		m_bnDown;
	CBCGPButton		m_bnSel;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void InsertColumns();
	void FillList();
	void SortChrono();
	void SortAlpha();
	void OnModeChange();
	void OnMoveItem( int nDir );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnRdoAlpha();
	afx_msg void OnRdoChrono();
	afx_msg void OnRdoCustom();
	afx_msg void OnClickBnUp();
	afx_msg void OnClickBnDown();
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickBnSel();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

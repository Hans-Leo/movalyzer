// DrawOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"
#include "DrawOptionsDlg.h"
#include "..\Common\DefFun.h"

// DrawOptionsDlg dialog

IMPLEMENT_DYNCREATE(DrawOptionsDlg, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(DrawOptionsDlg, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_COLORBG, OnBnClickedBnColorBG)
	ON_BN_CLICKED(IDC_BN_COLOR1, OnBnClickedBnColor1)
	ON_BN_CLICKED(IDC_BN_COLOR2, OnBnClickedBnColor2)
	ON_BN_CLICKED(IDC_BN_COLOR3, OnBnClickedBnColor3)
	ON_BN_CLICKED(IDC_BN_COLORELLIPSE, OnBnClickedBnColorEllipse)
	ON_BN_CLICKED(IDC_BN_COLORMPP, OnBnClickedBnColorMPP)
	ON_CBN_SELCHANGE(IDC_CBO_LINE, OnCbnSelchangeCboLine)
	ON_BN_CLICKED(IDC_CHK_LINE_VARIABLE, OnBnClickedBnLineByPressure)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

DrawOptionsDlg::DrawOptionsDlg( IProcSetting* pPS )
	: CBCGPPropertyPage(DrawOptionsDlg::IDD), m_pPS( pPS )
{
	m_fLineFixed = FALSE;
	m_nLineSize = 2;
	m_nEllipseSize = 2;
	m_dwBGColor = RGB( 255, 255, 255 );
	m_dwLineColor1 = RGB( 0, 0, 255 );
	m_dwLineColor2 = RGB( 0, 255, 0 );
	m_dwLineColor3 = RGB( 255, 0, 0 );
	m_dwEllipseColor = RGB( 255, 0, 0 );
	m_dwMPPColor = ::GetSoftColor();
	m_fLineByPressure = FALSE;
	m_nMaxLine = 20;

	m_editSize.SetDefaultValue( m_nEllipseSize );
	m_chkLBP.SetDefaultAsOff( !m_fLineByPressure );
	m_chkMax.SetDefaultValue( m_nMaxLine );

	if( m_pPS )
	{
		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			pPSRE->GetDrawingOptions( &m_nLineSize, &m_nEllipseSize, &m_dwBGColor,
									  &m_dwLineColor1, &m_dwLineColor2, &m_dwLineColor3,
									  &m_dwEllipseColor, &m_dwMPPColor );
			pPSRE->get_VaryLineThickness( &m_fLineByPressure );
			pPSRE->get_MaxLineThickness( &m_nMaxLine );
			pPSRE->Release();
		}
	}
}

DrawOptionsDlg::~DrawOptionsDlg()
{
}

void DrawOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ELLIPSESIZE, m_nEllipseSize);
	DDX_Control(pDX, IDC_BN_COLORBG, m_bnBGColor);
	DDX_Control(pDX, IDC_BN_COLOR1, m_bnLineColor1);
	DDX_Control(pDX, IDC_BN_COLOR2, m_bnLineColor2);
	DDX_Control(pDX, IDC_BN_COLOR3, m_bnLineColor3);
	DDX_Control(pDX, IDC_BN_COLORELLIPSE, m_bnEllipseColor);
	DDX_Control(pDX, IDC_BN_COLORMPP, m_bnMPPColor);
	DDX_Check(pDX, IDC_CHK_LINE_VARIABLE, m_fLineByPressure);
	DDX_Text(pDX, IDC_EDIT_MAXLINETHICKNESS, m_nMaxLine);

	DDX_Control(pDX, IDC_EDIT_ELLIPSESIZE, m_editSize);
	DDX_Control(pDX, IDC_CHK_LINE_VARIABLE, m_chkLBP);
	DDX_Control(pDX, IDC_EDIT_MAXLINETHICKNESS, m_chkMax);
}

// DrawOptionsDlg message handlers

BOOL DrawOptionsDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPPropertyPage::PreTranslateMessage(pMsg);
}

BOOL DrawOptionsDlg::OnApply()
{
	if( !m_pPS ) return FALSE;

	if( ( m_nEllipseSize < 0 ) || ( m_nEllipseSize > 100 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 4 );
		BCGPMessageBox( _T("Ellipses size must be between 0 and 100.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ELLIPSESIZE );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nMaxLine < 1 ) || ( m_nMaxLine > 100 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 4 );
		BCGPMessageBox( _T("The line thickness at maximum pen pressure must be between 1 and 100.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MAXLINETHICKNESS );
		pWnd->SetFocus();
		return FALSE;
	}

	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->SetDrawingOptions( m_nLineSize, m_nEllipseSize, m_dwBGColor,
								  m_dwLineColor1, m_dwLineColor2, m_dwLineColor3,
								  m_dwEllipseColor, m_dwMPPColor );
		pPSRE->put_VaryLineThickness( m_fLineByPressure );
		pPSRE->put_MaxLineThickness( m_nMaxLine );
		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL DrawOptionsDlg::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_BN_COLORBG );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Color for recording window background.", _T("Background Color") );
	pWnd = GetDlgItem( IDC_BN_COLOR1 );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Color for main line (1st in grip-force experiments.", _T("Line 1 Color") );
	pWnd = GetDlgItem( IDC_BN_COLOR2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Color for second line (2nd in grip-force experiments.", _T("Line 2 Color") );
	pWnd = GetDlgItem( IDC_BN_COLOR3 );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Color for third line (3rd in grip-force experiments.", _T("Line 3 Color") );
	pWnd = GetDlgItem( IDC_BN_COLORELLIPSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Color for ellipses (samples).", _T("Ellipse Color") );
	pWnd = GetDlgItem( IDC_BN_COLORMPP );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Color for samples below minimum pen pressure", _T("Min. Pen Pressure Color") );
	pWnd = GetDlgItem( IDC_CBO_LINE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select a fixed line thickness for the recording window.", _T("Line Thickness") );
	pWnd = GetDlgItem( IDC_EDIT_ELLIPSESIZE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Enter the size of the ellipse for sample points (in pixels).", _T("Ellipse Size") );
	pWnd = GetDlgItem( IDC_CHK_LINE_VARIABLE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select for drawn sample points to be variable to pen pressure.", _T("Variable Thickness") );
	pWnd = GetDlgItem( IDC_EDIT_MAXLINETHICKNESS );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Specify the thickness of the drawn line at maximum pen pressure (in pixels).\nFor pen pressure above the set maximum, the line will continue to grow in size.", _T("Max Thickness") );

	// Line thickness options
	CComboBox* pLines = (CComboBox*)GetDlgItem( IDC_CBO_LINE );
	int nItem = 0, nSelItem = -1;
	if( pLines )
	{
		pLines->SetItemData( nItem = pLines->AddString( _T("None") ), 0 );
		if( m_nLineSize == 0 ) nSelItem = nItem;
		pLines->SetItemData( nItem = pLines->AddString( _T("Thin") ), 1 );
		if( m_nLineSize == 1 ) nSelItem = nItem;
		pLines->SetItemData( nItem = pLines->AddString( _T("Medium Thin") ), 2 );
		if( m_nLineSize == 2 ) nSelItem = nItem;
		pLines->SetItemData( nItem = pLines->AddString( _T("Medium") ), 3 );
		if( m_nLineSize == 3 ) nSelItem = nItem;
		pLines->SetItemData( nItem = pLines->AddString( _T("Medium Thick") ), 4 );
		if( m_nLineSize == 4 ) nSelItem = nItem;
		pLines->SetItemData( nItem = pLines->AddString( _T("Thick") ), 5 );
		if( m_nLineSize == 5 ) nSelItem = nItem;
		pLines->SetItemData( nItem = pLines->AddString( _T("Extra Thick") ), 6 );
		if( m_nLineSize == 6 ) nSelItem = nItem;

		if( nSelItem != -1 ) pLines->SetCurSel( nSelItem );
	}

	// button colors
	m_bnBGColor.SetColor( m_dwBGColor );
	m_bnBGColor.EnableAutomaticButton (_T("Default"), RGB( 255, 255, 255 ));
	m_bnBGColor.EnableOtherButton( _T("Custom...") );
	m_bnLineColor1.SetColor( m_dwLineColor1 );
	m_bnLineColor1.EnableAutomaticButton (_T("Default"), RGB( 0, 0, 255 ));
	m_bnLineColor1.EnableOtherButton( _T("Custom...") );
	m_bnLineColor2.SetColor( m_dwLineColor2 );
	m_bnLineColor2.EnableAutomaticButton (_T("Default"), RGB( 0, 255, 0 ));
	m_bnLineColor2.EnableOtherButton( _T("Custom...") );
	m_bnLineColor3.SetColor( m_dwLineColor3 );
	m_bnLineColor3.EnableAutomaticButton (_T("Default"), RGB( 255, 0, 0 ));
	m_bnLineColor3.EnableOtherButton( _T("Custom...") );
	m_bnEllipseColor.SetColor( m_dwEllipseColor );
	m_bnEllipseColor.EnableAutomaticButton (_T("Default"), RGB( 255, 0, 0 ));
	m_bnEllipseColor.EnableOtherButton( _T("Custom...") );
	m_bnMPPColor.SetColor( m_dwMPPColor );
	m_bnMPPColor.EnableAutomaticButton (_T("Default"), ::GetSoftColor());
	m_bnMPPColor.EnableOtherButton( _T("Custom...") );

	// enable/disable based upon variable line thickness
	pWnd = GetDlgItem( IDC_EDIT_ELLIPSESIZE );
	if( pWnd ) pWnd->EnableWindow( !m_fLineByPressure );
	pWnd = GetDlgItem( IDC_EDIT_MAXLINETHICKNESS );
	if( pWnd ) pWnd->EnableWindow( m_fLineByPressure );

	// disable line 2 & 3 if not gripper
	BOOL fEnable = ::IsGripperModeOn();
	pWnd = GetDlgItem( IDC_BN_COLOR2 );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_BN_COLOR3 );
	if( pWnd ) pWnd->EnableWindow( fEnable );

	return FALSE;
}

void DrawOptionsDlg::OnBnClickedBnColorBG()
{
	m_dwBGColor = m_bnBGColor.GetColor();
	if( m_dwBGColor == -1 ) m_dwBGColor = RGB( 255, 255, 255 );
}

void DrawOptionsDlg::OnBnClickedBnColor1()
{
	m_dwLineColor1 = m_bnLineColor1.GetColor();
	if( m_dwLineColor1 == -1 ) m_dwLineColor1 = RGB( 0, 0, 255 );
}

void DrawOptionsDlg::OnBnClickedBnColor2()
{
	m_dwLineColor2 = m_bnLineColor2.GetColor();
	if( m_dwLineColor2 == -1 ) m_dwLineColor2 = RGB( 0, 255, 0 );
}

void DrawOptionsDlg::OnBnClickedBnColor3()
{
	m_dwLineColor3 = m_bnLineColor3.GetColor();
	if( m_dwLineColor3 == -1 ) m_dwLineColor3 = RGB( 255, 0, 0 );
}

void DrawOptionsDlg::OnBnClickedBnColorEllipse()
{
	m_dwEllipseColor = m_bnEllipseColor.GetColor();
	if( m_dwEllipseColor == -1 ) m_dwEllipseColor = RGB( 255, 0, 0 );
}

void DrawOptionsDlg::OnBnClickedBnColorMPP()
{
	m_dwMPPColor = m_bnMPPColor.GetColor();
	if( m_dwMPPColor == -1 ) m_dwMPPColor = ::GetSoftColor();
}

void DrawOptionsDlg::OnCbnSelchangeCboLine()
{
	CComboBox* pLines = (CComboBox*)GetDlgItem( IDC_CBO_LINE );
	if( pLines ) m_nLineSize = (short)pLines->GetItemData( pLines->GetCurSel() );
}

BOOL DrawOptionsDlg::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL DrawOptionsDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_drawoptions.html"), this );
	return TRUE;
}

void DrawOptionsDlg::OnBnClickedBnLineByPressure()
{
	UpdateData( TRUE );
	// enable/disable based upon variable line thickness
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ELLIPSESIZE );
	if( pWnd ) pWnd->EnableWindow( !m_fLineByPressure );
	pWnd = GetDlgItem( IDC_EDIT_MAXLINETHICKNESS );
	if( pWnd ) pWnd->EnableWindow( m_fLineByPressure );
}

void DrawOptionsDlg::OnBnReset()
{
	m_fLineFixed = FALSE;
	m_nLineSize = 2;
	m_nEllipseSize = 2;
	m_dwBGColor = RGB( 255, 255, 255 );
	m_dwLineColor1 = RGB( 0, 0, 255 );
	m_dwLineColor2 = RGB( 0, 255, 0 );
	m_dwLineColor3 = RGB( 255, 0, 0 );
	m_dwEllipseColor = RGB( 255, 0, 0 );
	m_dwMPPColor = ::GetSoftColor();
	m_fLineByPressure = FALSE;
	m_nMaxLine = 20;

	CComboBox* pLines = (CComboBox*)GetDlgItem( IDC_CBO_LINE );
	if( pLines ) pLines->SetCurSel( m_nLineSize );
	m_bnBGColor.SetColor( m_dwBGColor );
	m_bnLineColor1.SetColor( m_dwLineColor1 );
	m_bnLineColor2.SetColor( m_dwLineColor2 );
	m_bnLineColor3.SetColor( m_dwLineColor3 );
	m_bnEllipseColor.SetColor( m_dwEllipseColor );
	m_bnMPPColor.SetColor( m_dwMPPColor );

	UpdateData( FALSE );
}

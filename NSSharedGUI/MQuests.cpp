// MQuestionnaires.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "MQuests.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::GetNext()
{
	return MQuestionnaires::GetNext( m_pMQuestionnaire );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Find( CString szID )
{
	return MQuestionnaires::Find( m_pMQuestionnaire, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Add()
{
	return MQuestionnaires::Add( m_pMQuestionnaire );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Modify()
{
	return MQuestionnaires::Modify( m_pMQuestionnaire );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Remove()
{
	return MQuestionnaires::Remove( m_pMQuestionnaire );
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::SetDataPath( CString szPath )
{
	MQuestionnaires::SetDataPath( m_pMQuestionnaire, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void MQuestionnaires::GetID( IMQuestionnaire* pMQuestionnaire, CString& szVal )
{
	if( pMQuestionnaire )
	{
		BSTR bstrItem;
		pMQuestionnaire->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::GetIsHeader( IMQuestionnaire* pMQuestionnaire, BOOL& val )
{
	if( pMQuestionnaire ) pMQuestionnaire->get_IsHeader( &val );
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::GetIsPrivate( IMQuestionnaire* pMQuestionnaire, BOOL& val )
{
	if( pMQuestionnaire ) pMQuestionnaire->get_IsPrivate( &val );
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::GetItemNum( IMQuestionnaire* pMQuestionnaire, short& val )
{
	if( pMQuestionnaire ) pMQuestionnaire->get_ItemNum( &val );
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::GetQuestion( IMQuestionnaire* pMQuestionnaire, CString& szVal )
{
	if( pMQuestionnaire )
	{
		BSTR bstrItem;
		pMQuestionnaire->get_Question( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::GetIsNumeric( IMQuestionnaire* pMQuestionnaire, BOOL& val )
{
	if( pMQuestionnaire ) pMQuestionnaire->get_IsNumeric( &val );
}


///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::GetColumnHeader( IMQuestionnaire* pMQuestionnaire, CString& szVal )
{
	if( pMQuestionnaire )
	{
		BSTR bstrItem;
		pMQuestionnaire->get_ColumnHeader( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::ResetToStart()
{
	return MQuestionnaires::ResetToStart( m_pMQuestionnaire );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::ResetToStart( IMQuestionnaire* pMQuestionnaire )
{
	if( pMQuestionnaire ) return pMQuestionnaire->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::GetNext( IMQuestionnaire* pMQuestionnaire )
{
	if( pMQuestionnaire ) return pMQuestionnaire->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Find( IMQuestionnaire* pMQuestionnaire, CString szID )
{
	if( pMQuestionnaire ) return pMQuestionnaire->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Add( IMQuestionnaire* pMQuestionnaire )
{
	if( !pMQuestionnaire ) return E_FAIL;

	if( FAILED( pMQuestionnaire->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" MQestionnaires::Add") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Modify( IMQuestionnaire* pMQuestionnaire )
{
	if( !pMQuestionnaire ) return E_FAIL;

	if( FAILED( pMQuestionnaire->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT MQuestionnaires::Remove( IMQuestionnaire* pMQuestionnaire )
{
	if( !pMQuestionnaire ) return E_FAIL;

	if( FAILED( pMQuestionnaire->Remove() ) )
	{
		BCGPMessageBox( IDS_GRPDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::SetDataPath( IMQuestionnaire* pMQuestionnaire, CString szPath )
{
	if( pMQuestionnaire ) pMQuestionnaire->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoAdd( CWnd* pOwner )
{
	return MQuestionnaires::DoAdd( m_pMQuestionnaire, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoAdd( IMQuestionnaire* pMQuestionnaire, CWnd* pOwner )
{
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoEdit( CString szID, CWnd* pOwner )
{
	return MQuestionnaires::DoEdit( m_pMQuestionnaire, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoEdit( IMQuestionnaire* pMQuestionnaire, CString szID, CWnd* pOwner )
{
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::Export( CStdioFile* pFile )
{
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::Import( CStdioFile* pFile, BOOL fOverWrite )
{
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::ForceLocal()
{
	if( !m_pMQuestionnaire ) return;

	HRESULT hr = m_pMQuestionnaire->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::ForceRemote()
{
	if( !m_pMQuestionnaire ) return;

	HRESULT hr = m_pMQuestionnaire->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void MQuestionnaires::ForceDefault()
{
	if( !m_pMQuestionnaire ) return;

	HRESULT hr = m_pMQuestionnaire->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		MQuestionnaires dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );

		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

BOOL MQuestionnaires::Repair()
{
	HRESULT hr = S_OK;
	CStringList ID, I;
	CString szTemp, szID, szI;
	short nPos = 0, nVal = 0;
	INT_PTR nCur = 0;
	POSITION pos = NULL, posLast = NULL;

	// through through database, and place based on sequence #
	hr = ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		nCur = 0;
		// id of current question
		GetID( szID );
		// sequence # of current question
		GetItemNum( nPos );
		szI.Format( _T("%d"), nPos );
		// loop through to find position to place
		pos = I.GetHeadPosition();
		while( pos )
		{
			szTemp = I.GetNext( pos );
			nVal = atoi( szTemp );
			if( nPos < nVal ) break;
			nCur++;
		}
		pos = I.FindIndex( nCur );
		if( pos )
		{
			I.InsertBefore( pos, szI );
			pos = ID.FindIndex( nCur );
			if( pos ) ID.InsertBefore( pos, szID );
		}
		else
		{
			I.AddTail( szI );
			ID.AddTail( szID );
		}

		hr = GetNext();
	}

	// now apply new sequence #s to list
	nCur = 1;
	pos = I.GetHeadPosition();
	while( pos )
	{
		posLast = pos;
		szI = I.GetNext( pos );
		szTemp.Format( _T("%d"), nCur );
		I.SetAt( posLast, szTemp );
		nCur++;
	}

	// now set in database
	for( int i = 0; i < I.GetCount(); i++ )
	{
		pos = ID.FindIndex( i );
		if( pos )
		{
			szID = ID.GetAt( pos );
			pos = I.FindIndex( i );
			if( pos )
			{
				szI = I.GetAt( pos );
				nPos = atoi( szI );

				if( SUCCEEDED( Find( szID ) ) )
				{
					PutItemNum( nPos );
					Modify();
				}
			}
		}
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG MQuestionnaires::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pMQuestionnaire ) m_pMQuestionnaire->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

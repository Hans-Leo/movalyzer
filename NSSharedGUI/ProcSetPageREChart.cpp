// ProcSetPageREChart.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageREChart.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREChart property page

IMPLEMENT_DYNCREATE(ProcSetPageREChart, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageREChart, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_DISPLAY, OnChkDisplay)
	ON_BN_CLICKED(IDC_CHK_TF, OnChkTf)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageREChart::ProcSetPageREChart( IProcSetting* pS )
	: CBCGPPropertyPage(ProcSetPageREChart::IDD), m_pPS( pS )
{
	m_fDisplay = FALSE;
	m_fRaw = TRUE;
	m_fTF = FALSE;
	m_fSeg = FALSE;

	m_chkDisplay.SetDefaultAsOff( !m_fDisplay );
	m_chkRaw.SetDefaultAsOff( !m_fRaw );
	m_chkTF.SetDefaultAsOff( !m_fTF );
	m_chkSeg.SetDefaultAsOff( !m_fSeg );

	if( m_pPS )
	{
		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			pPSRE->get_DisplayCharts( &m_fDisplay );
			pPSRE->get_DisplayRaw( &m_fRaw );
			pPSRE->get_DisplayTF( &m_fTF );
			pPSRE->get_DisplaySeg( &m_fSeg );

			pPSRE->Release();
		}
	}
}

ProcSetPageREChart::~ProcSetPageREChart()
{
}

void ProcSetPageREChart::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_DISPLAY, m_fDisplay);
	DDX_Check(pDX, IDC_CHK_HWR, m_fRaw);
	DDX_Check(pDX, IDC_CHK_TF, m_fTF);
	DDX_Check(pDX, IDC_CHK_SEG, m_fSeg);

	DDX_Control(pDX, IDC_CHK_DISPLAY, m_chkDisplay);
	DDX_Control(pDX, IDC_CHK_HWR, m_chkRaw);
	DDX_Control(pDX, IDC_CHK_TF, m_chkTF);
	DDX_Control(pDX, IDC_CHK_SEG, m_chkSeg);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREChart message handlers

BOOL ProcSetPageREChart::OnApply() 
{
	if( !m_pPS ) return FALSE;

	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->put_DisplayCharts( m_fDisplay );
		pPSRE->put_DisplayRaw( m_fRaw );
		pPSRE->put_DisplayTF( m_fTF );
		pPSRE->put_DisplaySeg( m_fSeg );

		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageREChart::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageREChart::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_CHK_DISPLAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display selected charts after each trial."), _T("Display Charts") );
	pWnd = GetDlgItem( IDC_CHK_HWR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_VIEWRAW, _T("Raw Data") );
	pWnd = GetDlgItem( IDC_CHK_TF );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_VIEWTF, _T("Processed Data") );
	pWnd = GetDlgItem( IDC_CHK_SEG );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_RV_TT_VIEWSEG, _T("Include Segmentation") );

	SetFieldStates();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageREChart::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageREChart::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_runexperiment_charting.html"), this );
	return TRUE;
}

void ProcSetPageREChart::SetFieldStates()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_CHK_HWR );
	if( pWnd ) pWnd->EnableWindow( m_fDisplay );
	pWnd = GetDlgItem( IDC_CHK_TF );
	if( pWnd ) pWnd->EnableWindow( m_fDisplay );
	pWnd = GetDlgItem( IDC_CHK_SEG );
	if( pWnd ) pWnd->EnableWindow( m_fDisplay && m_fTF );
}

void ProcSetPageREChart::OnChkDisplay() 
{
	SetFieldStates();
}

void ProcSetPageREChart::OnChkTf() 
{
	SetFieldStates();
}

void ProcSetPageREChart::OnBnReset() 
{
	m_fDisplay = FALSE;
	m_fRaw = TRUE;
	m_fTF = FALSE;
	m_fSeg = FALSE;

	UpdateData( FALSE );

	SetFieldStates();
}

#pragma once

// ProcSetPageRESumm.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageRESumm dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageRESumm : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageRESumm)

// Construction
public:
	ProcSetPageRESumm( IProcSetting* pS = NULL );
	~ProcSetPageRESumm();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_RE_SUMM };
	BOOL			m_fSumm;
	CColoredButton	m_chkSumm;
	BOOL			m_fOnly;
	CColoredButton	m_chkOnly;
	BOOL			m_fOnlyGrp;
	CColoredButton	m_chkOnlyGrp;
	BOOL			m_fNormDB;
	CColoredButton	m_chkNormDB;
	BOOL			m_fSelect;
	CColoredButton	m_chkSelect;
	BOOL			m_fAnalysis;
	CColoredButton	m_chkAnalysis;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void SetFieldStates();
	afx_msg void OnChkSumm();
	afx_msg void OnChkSelect();
	afx_msg void OnChkOnly();
	afx_msg void OnChkOnlyGrp();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

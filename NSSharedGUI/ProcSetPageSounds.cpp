// ProcSetPageSounds.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageSounds.h"
#include "ConditionSheet.h"
#include "Conditions.h"
#include "Experiments.h"
#include "ProcSetPageREExt.h"
#include "EditorDlg.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSounds property page

IMPLEMENT_DYNCREATE(ProcSetPageSounds, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageSounds, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_SOUND, OnSelchangeCboSound)
	ON_BN_CLICKED(IDC_CHK_USE, OnChkUse)
	ON_BN_CLICKED(IDC_RDO_TONES, OnRdoTones)
	ON_BN_CLICKED(IDC_RDO_WAVE, OnRdoWave)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnBrowse)
	ON_EN_KILLFOCUS(IDC_EDIT_WAVE, OnKillfocusEditWave)
	ON_BN_CLICKED(IDC_BN_PLAY, OnClickBnPlay)
	ON_CBN_SELCHANGE(IDC_CBO_EXTAPP_TYPE_RAW, OnChangeType)
	ON_BN_CLICKED(IDC_BN_EDIT_RAW, OnClickEditRaw)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ProcSetPageSounds::ProcSetPageSounds( INSCondition* pCond, int nEvent )
	: CBCGPPropertyPage(ProcSetPageSounds::IDD), m_pC( pCond ), m_pUse( NULL ),
	  m_pTone( NULL ), m_pFreq( NULL ), m_pDur( NULL ), m_pTrigger( NULL ),
	  m_pScriptType( NULL )
{
	m_fUse = FALSE;
	m_nFreq = 800;
	m_nDur = 20;
	m_szWave = _T("");
	m_fTrigger = FALSE;
	m_fTone = TRUE;
	m_nSelEvent = nEvent;
	m_nExtTypeRaw = 0;
	m_fInformedMatlab = FALSE;
}

ProcSetPageSounds::~ProcSetPageSounds()
{
	if( m_pUse ) delete [] m_pUse;
	if( m_pTone ) delete [] m_pTone;
	if( m_pFreq ) delete [] m_pFreq;
	if( m_pDur ) delete [] m_pDur;
	if( m_pTrigger ) delete [] m_pTrigger;
	if( m_pScriptType ) delete [] m_pScriptType;
}

void ProcSetPageSounds::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_SOUND, m_cbo);
	DDX_Check(pDX, IDC_CHK_USE, m_fUse);
	DDX_Text(pDX, IDC_EDIT_FREQSTART, m_nFreq);
	DDV_MinMaxInt(pDX, m_nFreq, 20, 20000);
	DDX_Text(pDX, IDC_EDIT_DURSTART, m_nDur);
	DDV_MinMaxInt(pDX, m_nDur, 0, 50000);
	DDX_Text(pDX, IDC_EDIT_WAVE, m_szWave);
	DDX_Control(pDX, IDC_EDIT_WAVE, m_cboWave);
	DDX_Control(pDX, IDC_BN_PLAY, m_bnPlay);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnBrowse);
	DDX_Check(pDX, IDC_CHK_TRIGGER, m_fTrigger);
	DDX_Control(pDX, IDC_CBO_EXTAPP_TYPE_RAW, m_cboETRaw);
	DDX_CBString(pDX, IDC_CBO_EXTAPP_SCRIPT_RAW, m_szExtScriptRaw);
	DDX_Control(pDX, IDC_CBO_EXTAPP_SCRIPT_RAW, m_cboESRaw);
	DDX_Control(pDX, IDC_BN_EDIT_RAW, m_bnEditRaw);
}

void ProcSetPageSounds::SetEnables()
{
	UpdateData( TRUE );

	CWnd* pWnd = NULL;

	pWnd = GetDlgItem( IDC_RDO_TONES );
	if( pWnd ) pWnd->EnableWindow( m_fUse );
	pWnd = GetDlgItem( IDC_RDO_WAVE );
	if( pWnd ) pWnd->EnableWindow( m_fUse );

	pWnd = GetDlgItem( IDC_EDIT_FREQSTART );
	if( pWnd ) pWnd->EnableWindow( m_fTone && m_fUse );
	pWnd = GetDlgItem( IDC_EDIT_DURSTART );
	if( pWnd ) pWnd->EnableWindow( m_fTone && m_fUse );

	pWnd = GetDlgItem( IDC_EDIT_WAVE );
	if( pWnd ) pWnd->EnableWindow( !m_fTone && m_fUse );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) pWnd->EnableWindow( !m_fTone && m_fUse );
	pWnd = GetDlgItem( IDC_BN_PLAY );
	CFileFind ff;
	if( pWnd )
	{
		BOOL fEnable = m_fUse;
		if( !m_fTone )
		{
			CString szWave;
			::GetDataPathRoot( szWave );
			if( szWave != _T("") && ( szWave.GetAt( szWave.GetLength() - 1 ) != '\\' ) )
				szWave += _T("\\");
			szWave += _T("sounds\\");
			szWave += m_szWave;
			fEnable |= ( ( m_szWave != _T("") ) && ff.FindFile( szWave ) );
		}
		pWnd->EnableWindow( fEnable );
	}

	CWnd* pWndT = GetDlgItem( IDC_CBO_EXTAPP_TYPE_RAW );
	CWnd* pWndS = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_RAW );
	CWnd* pWndB = GetDlgItem( IDC_BN_EDIT_RAW );
	BOOL fAvail = ::IsOA();
	BOOL fEnable = ( m_nExtTypeRaw != eEAT_None ) && fAvail;
	pWndT->EnableWindow( fAvail );
	pWndS->EnableWindow( fEnable );
	pWndB->EnableWindow( fEnable );
	if( !fEnable ) m_cboESRaw.SetCurSel( -1 );
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSounds message handlers

BOOL ProcSetPageSounds::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageSounds::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	
	// button image
	m_bnPlay.SetImage( IDB_SOUND, IDB_SOUND, IDB_SOUND );
	m_bnBrowse.SetImage( IDB_OPEN, IDB_OPEN, IDB_OPEN );
	m_bnEditRaw.SetImage( IDB_EDIT );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_CBO_SOUND );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select which sound category you would like to view/edit.", _T("Sound Event") );
	pWnd = GetDlgItem( IDC_CHK_USE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Toggle sound on or off for the selected event.", _T("Use Sound?") );
	pWnd = GetDlgItem( IDC_RDO_TONES );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Use a tone sound for the selected event.", _T("Use Tone") );
	pWnd = GetDlgItem( IDC_RDO_WAVE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Use a wave file for the selected event.", _T("Play Wave") );
	pWnd = GetDlgItem( IDC_EDIT_FREQSTART );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Specify the tone frequency for the selected event.", _T("Tone Frequency") );
	pWnd = GetDlgItem( IDC_EDIT_DURSTART );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Specify the tone duration for the selected event.", _T("Tone Duration") );
	pWnd = GetDlgItem( IDC_EDIT_WAVE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select the wave file for the selected event.\nYou can add additional sounds by clicking the browse button.", _T("Wave File") );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Browse for a wave file.", _T("Browse") );
	pWnd = GetDlgItem( IDC_BN_PLAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Play the selected sound (tone or wave).", _T("Test") );
	pWnd = GetDlgItem( IDC_CHK_TRIGGER );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Toggle trigger pulses to communication port on or off for the selected event.", _T("Trigger Pulse") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_TYPE_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the external application type to handle this event.\nSelect 'NONE' for no action."), _T("External Application Type") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the script to run for this event."), _T("Script") );
	pWnd = GetDlgItem( IDC_BN_EDIT_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the processing script for this event."), _T("Edit Script") );

	// combo box fill
	m_cbo.AddString( _T("Ready to begin recording") );
	m_cbo.AddString( _T("End of a recording") );
	m_cbo.AddString( _T("Start of warning stimulus") );
	m_cbo.AddString( _T("End of a warning stimulus") );
	m_cbo.AddString( _T("Start of precue stimulus") );
	m_cbo.AddString( _T("End of a precue stimulus") );
	m_cbo.AddString( _T("Reached correct target") );
	m_cbo.AddString( _T("Reached incorrect target") );
	m_cbo.AddString( _T("Pen is lifted from tablet") );
	m_cbo.AddString( _T("Pen is placed down on tablet") );
	m_cbo.SetCurSel( 0 );
	m_nLastSel = 0;

	// holder vars
	m_pUse = new BOOL[ SOUND_COUNT ];
	m_pTone = new BOOL[ SOUND_COUNT ];
	m_pFreq = new int[ SOUND_COUNT ];
	m_pDur = new int[ SOUND_COUNT ];
	m_pTrigger = new BOOL[ SOUND_COUNT ];
	m_pScriptType = new short[ SOUND_COUNT ];

	// get sound values
	CString szWave;
	short nType = 0;
	for( int i = 0; i < SOUND_COUNT; i++ )
	{
		NSConditions::GetSoundInfo( m_pC, eSC_Use, (eSoundType)( i + 1 ), m_pUse[ i ] );
		NSConditions::GetSoundInfo( m_pC, eSC_Tone, (eSoundType)( i + 1 ), m_pTone[ i ] );
		NSConditions::GetSoundInfo( m_pC, eSC_Frequency, (eSoundType)( i + 1 ), m_pFreq[ i ] );
		NSConditions::GetSoundInfo( m_pC, eSC_Duration, (eSoundType)( i + 1 ), m_pDur[ i ] );
		NSConditions::GetSoundInfo( m_pC, eSC_Media, (eSoundType)( i + 1 ), szWave );
		m_lstWave.AddTail( szWave );
		NSConditions::GetSoundInfo( m_pC, eSC_Trigger, (eSoundType)( i + 1 ), m_pTrigger[ i ] );

		NSConditions::get_SoundScript( m_pC, (eSoundType)( i + 1 ), m_pScriptType[ i ], szWave );
		m_lstScripts.AddTail( szWave );

	}

	if( m_nSelEvent != -1 )
	{
		m_fUse = m_pUse[ m_nSelEvent ];
		m_fTone = m_pTone[ m_nSelEvent ];
		m_nFreq = m_pFreq[ m_nSelEvent ];
		m_nDur = m_pDur[ m_nSelEvent ];
		m_szWave = m_lstWave.GetAt( m_lstWave.FindIndex( m_nSelEvent ) );
		m_fTrigger = m_pTrigger[ m_nSelEvent ];
		m_nLastSel = m_nSelEvent;

		m_nExtTypeRaw = m_pScriptType[ m_nSelEvent ];
		m_cboETRaw.SetCurSel( m_nSelEvent );
		m_szExtScriptRaw = m_lstScripts.GetAt( m_lstScripts.FindIndex( m_nSelEvent ) );
		m_cboESRaw.SelectString( -1, m_szExtScriptRaw );
	}
	else
	{
		// we start off with start recording
		m_fUse = m_pUse[ SOUND_RECORDING_START ];
		m_fTone = m_pTone[ SOUND_RECORDING_START ];
		m_nFreq = m_pFreq[ SOUND_RECORDING_START ];
		m_nDur = m_pDur[ SOUND_RECORDING_START ];
		m_szWave = m_lstWave.GetAt( m_lstWave.GetHeadPosition() );
		m_fTrigger = m_pTrigger[ SOUND_RECORDING_START ];

		m_nExtTypeRaw = m_pScriptType[ SOUND_RECORDING_START ];
		m_cboETRaw.SetCurSel( SOUND_RECORDING_START );
		m_szExtScriptRaw = m_lstScripts.GetAt( m_lstScripts.FindIndex( SOUND_RECORDING_START ) );
		m_cboESRaw.SelectString( -1, m_szExtScriptRaw );
	}
	UpdateData( FALSE );

	// check appropriate radio button
	if( m_fTone ) CheckRadioButton( IDC_RDO_TONES, IDC_RDO_WAVE, IDC_RDO_TONES );
	else CheckRadioButton( IDC_RDO_TONES, IDC_RDO_WAVE, IDC_RDO_WAVE );

	// fill image list
	FillSoundList();
	// select selected event if specified
	if( m_nSelEvent != -1 ) m_cbo.SetCurSel( m_nSelEvent );

	// fill type combos
	m_cboETRaw.AddString( _T("NONE") );
	m_cboETRaw.SetItemData( 0, eEAT_None );
	m_cboETRaw.AddString( _T("Matlab") );
	m_cboETRaw.SetItemData( 1, eEAT_Matlab );
	m_cboETRaw.AddString( _T("Batch") );
	m_cboETRaw.SetItemData( 2, eEAT_Batch );
	m_cboETRaw.SetCurSel( m_nExtTypeRaw );

	// fill script lists
	FillScriptLists();

	// enable/disable items appropriately
	SetEnables();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ProcSetPageSounds::FillSoundList()
{
	m_cboWave.ResetContent();

	CString szFind, szFile;
	::GetDataPathRoot( szFind );
	if( szFind != _T("") && ( szFind.GetAt( szFind.GetLength() - 1 ) != '\\' ) )
		szFind += _T("\\");
	szFind += _T("sounds\\");
	szFind += _T("*.WAV");
	int nSel = -1;
	CFileFind ff;
	BOOL fCont = ff.FindFile( szFind );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		if( !ff.IsDirectory() )
		{
			int nItem = m_cboWave.AddString( szFile );
			if( szFile == m_szWave ) nSel = nItem;
		}
	}
	if( nSel != -1 )
	{
		m_cboWave.SetCurSel( nSel );
	}
	else if( ( m_szWave != _T("") ) && ( m_nSelEvent == -1 ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following sound was specified as the sound to use for this condition:\n\n%s\n\nThis file cannot be found."), m_szWave );
		BCGPMessageBox( szMsg );
		m_szWave = _T("");
		UpdateData( FALSE );
	}
}

void ProcSetPageSounds::FillScriptLists()
{
	if( !::IsOA() ) return;

	// empty combo content
	m_cboESRaw.ResetContent();

	// base path
	CString szPath;
	::GetDataPathRoot( szPath );
	szPath += _T("\\scripts\\");
	// ensure exists
	_mkdir( szPath );

	// declare vars & setup search types
	CString szFindMatlab = szPath, szFindBatch = szPath, szFind, szFile;
	szFindMatlab += _T("*.m");
	szFindBatch += _T("*.bat");
	int nSel = -1, nItem = -1;
	CFileFind ff;
	BOOL fCont = FALSE, fMatlab = FALSE;
	CStringList lstMatlab, lstBatch, *pList = NULL;
	POSITION pos = NULL;

	// get files for search types
	// matlab
	fCont = ff.FindFile( szFindMatlab );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		if( !ff.IsDirectory() ) lstMatlab.AddTail( szFile );
	}
	// batch
	fCont = ff.FindFile( szFindBatch );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		if( !ff.IsDirectory() ) lstBatch.AddTail( szFile );
	}

	// NOW FILL COMBOS
	// RAW
	nSel = -1;
	if( m_nExtTypeRaw == eEAT_Matlab ) { pList = &lstMatlab; fMatlab = TRUE; }
	else if( m_nExtTypeRaw == eEAT_Batch ) pList = &lstBatch;
	else pList = NULL;
	pos = pList ? pList->GetHeadPosition() : NULL;
	while( pos )
	{
		szFile = pList->GetNext( pos );
		nItem = m_cboESRaw.AddString( szFile );
		if( szFile == m_szExtScriptRaw ) nSel = nItem;
	}

	m_cboESRaw.SetCurSel( nSel );
	// if any have matlab scripts and matlab is not installed, inform
	if( fMatlab && !::MatlabInstalled() && !m_fInformedMatlab && !::CheckedMatlab() )
	{
		if( BCGPMessageBox( _T("MovAlyzeR will now check to see if Matlab is installed.\nClick OK to begin, or Cancel to skip the check."), MB_OKCANCEL ) == IDOK )
		{
			if( Experiments::IsMatlabInstalled() ) ::SetMatlabInstalled();
			::SetCheckedMatlab();
			CString szMsg = ::MatlabInstalled() ? _T("Matlab is installed and can be used as an external application.") :
												  _T("Matlab is not installed or is an unsupported version.");
			BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
			return;
		}

		BCGPMessageBox( _T("At least one of your events is using a Matlab script.\nMatlab is either not installed or not accessible.\nThe scripts will produce errors during processing."), MB_OK | MB_ICONWARNING );
		m_fInformedMatlab = TRUE;
	}
}

BOOL ProcSetPageSounds::OnApply() 
{
	if( !m_pC ) return FALSE;

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	// verify that a wav file is selected if wav is checked
	if( m_fUse && !m_fTone && ( m_cboWave.GetCurSel() == -1 ) )
	{
		m_cbo.SetCurSel( m_nLastSel );
		BCGPMessageBox( "A wave file must be selected." );
		return FALSE;
	}

	if( ( m_nExtTypeRaw != eEAT_None ) && ( m_szExtScriptRaw == _T("") ) )
	{
		BCGPMessageBox( _T("You must specify a script for the RAW file."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_RAW );
		pWnd->SetFocus();
		return FALSE;
	}

	// update holder vars that were changed
	UpdateData( TRUE );
	m_pUse[ m_nLastSel ] = m_fUse;
	m_pTone[ m_nLastSel ] = m_fTone;
	m_pFreq[ m_nLastSel ] = m_nFreq;
	m_pDur[ m_nLastSel ] = m_nDur;
	m_lstWave.SetAt( m_lstWave.FindIndex( m_nLastSel ), m_szWave );
	m_pTrigger[ m_nLastSel ] = m_fTrigger;
	m_pScriptType[ m_nLastSel ] = m_nExtTypeRaw;
	m_lstScripts.SetAt( m_lstScripts.FindIndex( m_nLastSel ), m_szExtScriptRaw );

	// update values
	for( int i = 0; i < SOUND_COUNT; i++ )
	{
		NSConditions::PutSoundInfo( m_pC, eSC_Use, (eSoundType)( i + 1 ), m_pUse[ i ] );
		NSConditions::PutSoundInfo( m_pC, eSC_Tone, (eSoundType)( i + 1 ), m_pTone[ i ] );
		NSConditions::PutSoundInfo( m_pC, eSC_Frequency, (eSoundType)( i + 1 ), m_pFreq[ i ] );
		NSConditions::PutSoundInfo( m_pC, eSC_Duration, (eSoundType)( i + 1 ), m_pDur[ i ] );
		NSConditions::PutSoundInfo( m_pC, eSC_Trigger, (eSoundType)( i + 1 ), m_pTrigger[ i ] );
		NSConditions::PutSoundInfo( m_pC, eSC_Media, (eSoundType)( i + 1 ), m_lstWave.GetAt( m_lstWave.FindIndex( i ) ) );
		NSConditions::put_SoundScript( m_pC, (eSoundType)( i + 1 ), m_pScriptType[ i ], m_lstScripts.GetAt( m_lstScripts.FindIndex( i ) ) );
	}

	return CBCGPPropertyPage::OnApply();
}

void ProcSetPageSounds::OnSelchangeCboSound() 
{
	if( !m_pC ) return;

	// verify that a wav file is selected if wav is checked
	if( m_fUse && !m_fTone && ( m_cboWave.GetCurSel() == -1 ) )
	{
		m_cbo.SetCurSel( m_nLastSel );
		BCGPMessageBox( "A wave file must be selected." );
		return;
	}

	// update holder vars that were changed
	UpdateData( TRUE );
	m_pUse[ m_nLastSel ] = m_fUse;
	m_pTone[ m_nLastSel ] = m_fTone;
	m_pFreq[ m_nLastSel ] = m_nFreq;
	m_pDur[ m_nLastSel ] = m_nDur;
	m_lstWave.SetAt( m_lstWave.FindIndex( m_nLastSel ), m_szWave );
	m_pTrigger[ m_nLastSel ] = m_fTrigger;
	m_pScriptType[ m_nLastSel ] = m_nExtTypeRaw;
	m_lstScripts.SetAt( m_lstScripts.FindIndex( m_nLastSel ), m_szExtScriptRaw );

	// new selection index
	int nSel = m_cbo.GetCurSel();
	if( nSel < 0 ) return;
	if( nSel > ( SOUND_COUNT - 1 ) ) return;

	m_nLastSel = nSel;
	m_fUse = m_pUse[ nSel ];
	m_fTone = m_pTone[ nSel ];
	m_nFreq = m_pFreq[ nSel ];
	m_nDur = m_pDur[ nSel ];
	m_szWave = m_lstWave.GetAt( m_lstWave.FindIndex( nSel ) );
	m_cboWave.SelectString( -1, m_szWave );
	m_fTrigger = m_pTrigger[ nSel ];
	m_nExtTypeRaw = m_pScriptType[ nSel ];
	m_cboETRaw.SetCurSel( m_nExtTypeRaw );
	m_szExtScriptRaw = m_lstScripts.GetAt( m_lstScripts.FindIndex( nSel ) );
	m_cboESRaw.SelectString( -1, m_szExtScriptRaw );

	// check appropriate radio button
	if( m_fTone ) CheckRadioButton( IDC_RDO_TONES, IDC_RDO_WAVE, IDC_RDO_TONES );
	else CheckRadioButton( IDC_RDO_TONES, IDC_RDO_WAVE, IDC_RDO_WAVE );

	UpdateData( FALSE );
	SetEnables();
}

void ProcSetPageSounds::OnChkUse() 
{
	SetEnables();
}

void ProcSetPageSounds::OnRdoTones() 
{
	m_fTone = TRUE;
	SetEnables();
}

void ProcSetPageSounds::OnRdoWave() 
{
	m_fTone = FALSE;
	SetEnables();
}

void ProcSetPageSounds::OnBnBrowse() 
{
	CFileDialog d( TRUE, _T("WAV"), _T(""), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
				   _T("Wave files (*.WAV)|*.WAV|"), this );
	if( d.DoModal() == IDCANCEL ) return;

	m_szWave = d.GetPathName();

	// source & dest files
	CString szSource = d.GetPathName(), szDest, szMsg, szRep;
	::GetDataPathRoot( szDest );
	if( szDest != _T("") && ( szDest.GetAt( szDest.GetLength() - 1 ) != '\\' ) )
		szDest += _T("\\");
	szDest += _T("sounds\\");
	szRep = szDest;
	int nPos = szSource.ReverseFind( '\\' );
	if( nPos != -1 ) szSource = szSource.Left( nPos );
	if( szSource == szDest )
	{
		szMsg.Format( _T("You have selected a file that is already in the repository\n(%s)."), szRep );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONEXCLAMATION );
		return;
	}
	szSource = d.GetPathName();
	szDest += d.GetFileName();

	// is this file already in the image repository?
	BOOL fExists = FALSE;
	CFileFind ff;
	if( ff.FindFile( szDest ) )
	{
		fExists = TRUE;
		szMsg.Format( _T("This sound file already exists in the repository\n(%s).\n\nOverwrite?"), szRep );
		if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;
	}

	// warn user that we'll be copying the file into the folder
	szMsg.Format( _T("The selected sound will be copied to the sound repository\n(%s).\n\nProceed?"), szRep );
	if( !fExists && BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	// verify sounds directory
	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );
	int nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) {
		BCGPMessageBox( _T("Unable to create directory."), MB_OK | MB_ICONERROR );
		return;
	}
	szPath += _T("\\sounds");
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) {
		BCGPMessageBox( _T("Unable to create directory."), MB_OK | MB_ICONERROR );
		return;
	}
		
	// copy file
	CopyFile( szSource, szDest, FALSE );

	// update GUI
	m_szWave = d.GetFileName();
	UpdateData( FALSE );
	m_cboWave.SelectString( -1, m_szWave );

	if( !fExists ) FillSoundList();

	SetEnables();
}

BOOL ProcSetPageSounds::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageSounds::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_sound.html"), this );
	return TRUE;
}

void ProcSetPageSounds::OnClickBnPlay() 
{
	UpdateData( TRUE );
	int nRes = GetCheckedRadioButton( IDC_RDO_TONES, IDC_RDO_WAVE );

	if( ( nRes == IDC_RDO_WAVE ) && ( m_szWave != _T("") ) )
	{
		CString szFile;
		::GetDataPathRoot( szFile );
		if( szFile != _T("") && ( szFile.GetAt( szFile.GetLength() - 1 ) != '\\' ) )
			szFile += _T("\\");
		szFile += _T("sounds\\");
		szFile += m_szWave;
		CFileFind ff;
		if( ff.FindFile( szFile ) ) ::PlayASound( szFile );
	}
	else Beep( m_nFreq, m_nDur );
}

void ProcSetPageSounds::OnKillfocusEditWave() 
{
	UpdateData( TRUE );
	SetEnables();
}

void ProcSetPageSounds::OnChangeType()
{
	m_nExtTypeRaw = m_cboETRaw.GetItemData( m_cboETRaw.GetCurSel() );
	FillScriptLists();
	SetEnables();
}

void ProcSetPageSounds::OnClickEditRaw()
{
	UpdateData( TRUE );
	EditorDlg d( this );
	d.m_nType = m_nExtTypeRaw;
	d.m_szFile = m_szExtScriptRaw;
	if( d.DoModal() == IDOK ) FillScriptLists();
}

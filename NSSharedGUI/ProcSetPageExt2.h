#pragma once

// ProcSetPageExt2.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageExt2 dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageExt2 : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageExt2)

// Construction
public:
	ProcSetPageExt2( IProcSetting* pPS = NULL );
	~ProcSetPageExt2();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_EXT2 };
	double			initialpart;
	CColoredEdit	m_editInitial;
	double			maxfreq;
	CColoredEdit	m_editMaxFreq;
	IProcSetting*	m_pPS;
	CBCGPButton		m_linkAdv;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

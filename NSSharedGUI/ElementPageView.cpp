// ElementPageView.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "..\CTreeLink\DBCommon.h"
#include "ElementPageView.h"
#include "ElementSheet.h"
#include "ElementPageGeneral.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BOOL _fSkipSave = FALSE;

/////////////////////////////////////////////////////////////////////////////
// ElementPageView property page

IMPLEMENT_DYNCREATE(ElementPageView, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ElementPageView, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_MODE, OnSelchangeCboMode)
	ON_BN_CLICKED(IDC_BN_FONT, OnBnFont)
	ON_BN_CLICKED(IDC_BN_COLORL, OnClickBnColorl)
	ON_BN_CLICKED(IDC_BN_COLORBG, OnClickBnColorbg)
	ON_BN_CLICKED(IDC_BN_COLORT, OnClickBnColort)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ElementPageView::ElementPageView( IElement* pE )
	: CBCGPPropertyPage(ElementPageView::IDD), m_pE( pE )
{
	m_szText = _T("");
	m_nLineSize = 0;
	m_nItem = 0;
	m_szFont = _T("Arial");
	m_szSize = _T("12");
	m_nTextSize = 12;
	m_nLastIdx = 0;
	m_pColorsL = new long[ 3 ];
	m_pColorsBG = new long[ 3 ];
	m_pColorsT = new long[ 3 ];
	m_pLines = new short[ 3 ];
	m_pSizes = new short[ 3 ];
	memset( m_pColorsL, 0, 3 * sizeof( long ) );
	memset( m_pColorsBG, 0, 3 * sizeof( long ) );
	memset( m_pColorsT, 0, 3 * sizeof( long ) );
	memset( m_pLines, 0, 3 * sizeof( short ) );
	for( int i = 0; i < 3; m_pSizes[ i++ ] = m_nTextSize );
	memset( &m_lf1, 0, sizeof( LOGFONT ) );
	strcpy_s( m_lf1.lfFaceName, LF_FACESIZE, m_szFont );
	memset( &m_lf2, 0, sizeof( LOGFONT ) );
	strcpy_s( m_lf2.lfFaceName, LF_FACESIZE, m_szFont );
	memset( &m_lf3, 0, sizeof( LOGFONT ) );
	strcpy_s( m_lf3.lfFaceName, LF_FACESIZE, m_szFont );

	if( m_pE )
	{
		BSTR bstr = NULL;
		short nVal = 0;
		DWORD dwVal = 0;

		m_pE->get_Line1Size( &nVal );
		m_pLines[ 0 ] = nVal;
		m_pE->get_Line1Color( &dwVal );
		m_pColorsL[ 0 ] = dwVal;
		m_pE->get_Line2Size( &nVal );
		m_pLines[ 1 ] = nVal;
		m_pE->get_Line2Color( &dwVal );
		m_pColorsL[ 1 ] = dwVal;
		m_pE->get_Line3Size( &nVal );
		m_pLines[ 2 ] = nVal;
		m_pE->get_Line3Color( &dwVal );
		m_pColorsL[ 2 ] = dwVal;

		m_pE->get_Background1Color( &dwVal );
		m_pColorsBG[ 0 ] = dwVal;
		m_pE->get_Background2Color( &dwVal );
		m_pColorsBG[ 1 ] = dwVal;
		m_pE->get_Background3Color( &dwVal );
		m_pColorsBG[ 2 ] = dwVal;

		m_pE->get_Text1( &bstr );
		m_szText1 = bstr;
		m_pE->get_Text1Color( &dwVal );
		m_pColorsT[ 0 ] = dwVal;
		m_pE->get_Text1Size( &nVal );
		m_pSizes[ 0 ] = nVal;

		m_pE->get_Text2( &bstr );
		m_szText2 = bstr;
		m_pE->get_Text2Color( &dwVal );
		m_pColorsT[ 1 ] = dwVal;
		m_pE->get_Text2Size( &nVal );
		m_pSizes[ 1 ] = nVal;

		m_pE->get_Text3( &bstr );
		m_szText3 = bstr;
		m_pE->get_Text3Color( &dwVal );
		m_pColorsT[ 2 ] = dwVal;
		m_pE->get_Text3Size( &nVal );
		m_pSizes[ 2 ] = nVal;

		LOGFONT* lf = NULL;
		m_pE->get_Font1( (VARIANT**)&lf );
		if( lf )
		{
			m_lf1.lfHeight = lf->lfHeight;
			m_lf1.lfWidth = lf->lfWidth;
			m_lf1.lfEscapement = lf->lfEscapement;
			m_lf1.lfOrientation = lf->lfOrientation;
			m_lf1.lfWeight = lf->lfWeight;
			m_lf1.lfItalic = lf->lfItalic;
			m_lf1.lfUnderline = lf->lfUnderline;
			m_lf1.lfStrikeOut = lf->lfStrikeOut;
			m_lf1.lfCharSet = lf->lfCharSet;
			m_lf1.lfOutPrecision = lf->lfOutPrecision;
			m_lf1.lfClipPrecision = lf->lfClipPrecision;
			m_lf1.lfQuality = lf->lfQuality;
			m_lf1.lfPitchAndFamily = lf->lfPitchAndFamily;
			strcpy_s( m_lf1.lfFaceName, lf->lfFaceName );
		}
		m_pE->get_Font2( (VARIANT**)&lf );
		if( lf )
		{
			m_lf2.lfHeight = lf->lfHeight;
			m_lf2.lfWidth = lf->lfWidth;
			m_lf2.lfEscapement = lf->lfEscapement;
			m_lf2.lfOrientation = lf->lfOrientation;
			m_lf2.lfWeight = lf->lfWeight;
			m_lf2.lfItalic = lf->lfItalic;
			m_lf2.lfUnderline = lf->lfUnderline;
			m_lf2.lfStrikeOut = lf->lfStrikeOut;
			m_lf2.lfCharSet = lf->lfCharSet;
			m_lf2.lfOutPrecision = lf->lfOutPrecision;
			m_lf2.lfClipPrecision = lf->lfClipPrecision;
			m_lf2.lfQuality = lf->lfQuality;
			m_lf2.lfPitchAndFamily = lf->lfPitchAndFamily;
			strcpy_s( m_lf2.lfFaceName, lf->lfFaceName );
		}
		m_pE->get_Font3( (VARIANT**)&lf );
		if( lf )
		{
			m_lf3.lfHeight = lf->lfHeight;
			m_lf3.lfWidth = lf->lfWidth;
			m_lf3.lfEscapement = lf->lfEscapement;
			m_lf3.lfOrientation = lf->lfOrientation;
			m_lf3.lfWeight = lf->lfWeight;
			m_lf3.lfItalic = lf->lfItalic;
			m_lf3.lfUnderline = lf->lfUnderline;
			m_lf3.lfStrikeOut = lf->lfStrikeOut;
			m_lf3.lfCharSet = lf->lfCharSet;
			m_lf3.lfOutPrecision = lf->lfOutPrecision;
			m_lf3.lfClipPrecision = lf->lfClipPrecision;
			m_lf3.lfQuality = lf->lfQuality;
			m_lf3.lfPitchAndFamily = lf->lfPitchAndFamily;
			strcpy_s( m_lf3.lfFaceName, lf->lfFaceName );
		}
	}

	m_nLineSize = m_pLines[ 0 ];
	m_nTextSize = m_pSizes[ 0 ];
	m_szText = m_szText1;
	m_szFont = m_lf1.lfFaceName;
	m_szSize.Format( _T("%d"), m_pSizes[ 0 ] );
}

ElementPageView::~ElementPageView()
{
	if( m_pColorsL ) delete [] m_pColorsL;
	if( m_pColorsBG ) delete [] m_pColorsBG;
	if( m_pColorsT ) delete [] m_pColorsT;
	if( m_pLines ) delete [] m_pLines;
	if( m_pSizes ) delete [] m_pSizes;
}

void ElementPageView::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_MODE, m_cboMode);
	DDX_Text(pDX, IDC_EDIT_TEXT, m_szText);
	DDV_MaxChars(pDX, m_szText, szELEMENT_TEXT);
	DDX_Text(pDX, IDC_EDIT_LINESIZE, m_nLineSize);
	DDV_MinMaxInt(pDX, m_nLineSize, 0, 100);
	DDX_Control(pDX, IDC_BN_COLORL, m_bnColorL);
	DDX_Control(pDX, IDC_BN_COLORBG, m_bnColorBG);
	DDX_Control(pDX, IDC_BN_COLORT, m_bnColorT);
	DDX_CBIndex(pDX, IDC_CBO_MODE, m_nItem);
	DDX_Text(pDX, IDC_TXT_FONT, m_szFont);
	DDV_MaxChars(pDX, m_szFont, 50);
	DDX_Text(pDX, IDC_TXT_SIZE, m_szSize);
}

/////////////////////////////////////////////////////////////////////////////
// ElementPageView message handlers

BOOL ElementPageView::OnApply() 
{
	if( !m_pE ) return FALSE;

	UpdateData( TRUE );

	m_pLines[ m_nItem ] = m_nLineSize;
	m_pSizes[ m_nItem ] = m_nTextSize;
	switch( m_nItem )
	{
		case 0:
			m_szText1 = m_szText;
			strcpy_s( m_lf1.lfFaceName, LF_FACESIZE, m_szFont );
			break;
		case 1:
			m_szText2 = m_szText;
			strcpy_s( m_lf2.lfFaceName, LF_FACESIZE, m_szFont );
			break;
		case 2:
			m_szText3 = m_szText;
			strcpy_s( m_lf3.lfFaceName, LF_FACESIZE, m_szFont );
			break;
	}

	// Verify
	if( m_szText1.Find( _T(";") ) != -1 )
	{
		BCGPMessageBox( _T("Text cannot contain the \";\" character.") );
		m_nItem = 0;
		_fSkipSave = TRUE;
		m_cboMode.SetCurSel( 0 );
		OnSelchangeCboMode();
		UpdateData( FALSE );
		_fSkipSave = FALSE;
		CWnd* pWnd = GetDlgItem( IDC_EDIT_TEXT );
		pWnd->SetFocus();
		return FALSE;
	}
	if( m_szText2.Find( _T(";") ) != -1 )
	{
		BCGPMessageBox( _T("Text cannot contain the \";\" character.") );
		m_nItem = 1;
		_fSkipSave = TRUE;
		m_cboMode.SetCurSel( 0 );
		OnSelchangeCboMode();
		UpdateData( FALSE );
		_fSkipSave = FALSE;
		CWnd* pWnd = GetDlgItem( IDC_EDIT_TEXT );
		pWnd->SetFocus();
		return FALSE;
	}
	if( m_szText3.Find( _T(";") ) != -1 )
	{
		BCGPMessageBox( _T("Text cannot contain the \";\" character.") );
		m_nItem = 2;
		_fSkipSave = TRUE;
		m_cboMode.SetCurSel( 0 );
		OnSelchangeCboMode();
		UpdateData( FALSE );
		_fSkipSave = FALSE;
		CWnd* pWnd = GetDlgItem( IDC_EDIT_TEXT );
		pWnd->SetFocus();
		return FALSE;
	}

	// Set
	m_pE->put_Line1Color( m_pColorsL[ 0 ] );
	m_pE->put_Line2Color( m_pColorsL[ 1 ] );
	m_pE->put_Line3Color( m_pColorsL[ 2 ] );
	m_pE->put_Line1Size( (short)m_pLines[ 0 ] );
	m_pE->put_Line2Size( (short)m_pLines[ 1 ] );
	m_pE->put_Line3Size( (short)m_pLines[ 2 ] );
	m_pE->put_Background1Color( m_pColorsBG[ 0 ] );
	m_pE->put_Background2Color( m_pColorsBG[ 1 ] );
	m_pE->put_Background3Color( m_pColorsBG[ 2 ] );
	m_pE->put_Text1( m_szText1.AllocSysString() );
	m_pE->put_Text2( m_szText2.AllocSysString() );
	m_pE->put_Text3( m_szText3.AllocSysString() );
	m_pE->put_Text1Color( m_pColorsT[ 0 ] );
	m_pE->put_Text2Color( m_pColorsT[ 1 ] );
	m_pE->put_Text3Color( m_pColorsT[ 2 ] );
	m_pE->put_Text1Size( (short)m_pSizes[ 0 ] );
	m_pE->put_Text2Size( (short)m_pSizes[ 1 ] );
	m_pE->put_Text3Size( (short)m_pSizes[ 2 ] );
	m_pE->put_Font1( (VARIANT*)&m_lf1 );
	m_pE->put_Font2( (VARIANT*)&m_lf2 );
	m_pE->put_Font3( (VARIANT*)&m_lf3 );

	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

void ElementPageView::OnClickBnColorl() 
{
	if( m_nItem == -1 ) return;

	if( m_pColorsL )
	{
		m_pColorsL[ m_nItem ] = m_bnColorL.GetColor();
		if( m_pColorsL[ m_nItem ] == -1 ) m_pColorsL[ m_nItem ] = RGB( 0, 0, 0 );
	}
}

void ElementPageView::OnClickBnColorbg() 
{
	if( m_nItem == -1 ) return;

	if( m_pColorsBG )
	{
		m_pColorsBG[ m_nItem ] = m_bnColorBG.GetColor();
		if( m_pColorsBG[ m_nItem ] == -1 ) m_pColorsBG[ m_nItem ] = RGB( 255, 255, 255 );
	}
}

void ElementPageView::OnClickBnColort()
{
	if( m_nItem == -1 ) return;

	if( m_pColorsT )
	{
		m_pColorsT[ m_nItem ] = m_bnColorT.GetColor();
		if( m_pColorsBG[ m_nItem ] == -1 ) m_pColorsBG[ m_nItem ] = RGB( 0, 0, 0 );
	}
}

void ElementPageView::OnSelchangeCboMode() 
{
	m_nLastIdx = m_nItem;

	UpdateData( TRUE );

	if( !_fSkipSave )
	{
		m_pLines[ m_nLastIdx ] = m_nLineSize;
		m_pSizes[ m_nLastIdx ] = m_nTextSize;
		switch( m_nLastIdx )
		{
			case 0:
				m_szText1 = m_szText;
				strcpy_s( m_lf1.lfFaceName, LF_FACESIZE, m_szFont );
				break;
			case 1:
				m_szText2 = m_szText;
				strcpy_s( m_lf2.lfFaceName, LF_FACESIZE, m_szFont );
				break;
			case 2:
				m_szText3 = m_szText;
				strcpy_s( m_lf3.lfFaceName, LF_FACESIZE, m_szFont );
				break;
		}
	}

	m_nLineSize = m_pLines[ m_nItem ];
	m_nTextSize = m_pSizes[ m_nItem ];
	m_szSize.Format( _T("%d"), m_pSizes[ m_nItem ] );
	switch( m_nItem )
	{
		case 0:
			m_szText = m_szText1;
			m_szFont = m_lf1.lfFaceName;
			break;
		case 1:
			m_szText = m_szText2;
			m_szFont = m_lf2.lfFaceName;
			break;
		case 2:
			m_szText = m_szText3;
			m_szFont = m_lf3.lfFaceName;
			break;
	}

	if( m_pColorsL ) m_bnColorL.SetColor( m_pColorsL[ m_nItem ] );
	if( m_pColorsBG ) m_bnColorBG.SetColor( m_pColorsBG[ m_nItem ] );
	if( m_pColorsT ) m_bnColorT.SetColor( m_pColorsT[ m_nItem ] );
	
	UpdateData( FALSE );
}

BOOL ElementPageView::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ElementPageView::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_ELEM );
	m_tooltip.SetTitle( _T("ELEMENT") );
	CWnd* pWnd = GetDlgItem( IDC_CBO_MODE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select which category you would like to edit for this element (normal, successful, failure)."), _T("Category") );
	pWnd = GetDlgItem( IDC_EDIT_LINESIZE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the thickness of the shape's border in pixels."), _T("Line Size") );
	pWnd = GetDlgItem( IDC_EDIT_TEXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the text to be displayed within the shape."), _T("Text") );
	pWnd = GetDlgItem( IDC_BN_FONT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Choose font, size, color for this text."), _T("Font") );
	pWnd = GetDlgItem( IDC_BN_COLORL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to select a color for the shape's border."), _T("Line Color") );
	pWnd = GetDlgItem( IDC_BN_COLORBG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to select a color to fill the shape."), _T("Fill Color") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	// adjust logfont height from size (for new elements)
	CDC* pDC = GetDC();
	int nDC = pDC->GetDeviceCaps( LOGPIXELSY );
	ReleaseDC( pDC );
	if( m_lf1.lfHeight == 0 )
		m_lf1.lfHeight = -MulDiv( m_pSizes[ 0 ], nDC, 72 );
	if( m_lf2.lfHeight == 0 )
		m_lf2.lfHeight = -MulDiv( m_pSizes[ 1 ], nDC, 72 );
	if( m_lf3.lfHeight == 0 )
		m_lf3.lfHeight = -MulDiv( m_pSizes[ 2 ], nDC, 72 );

	m_cboMode.SetCurSel( 0 );

	m_bnColorL.SetColor( m_pColorsL[ 0 ] );
	m_bnColorL.EnableAutomaticButton (_T("Default"), RGB( 0, 0, 0 ));
	m_bnColorL.EnableOtherButton( _T("Custom...") );
	m_bnColorBG.SetColor( m_pColorsBG[ 0 ] );
	m_bnColorBG.EnableAutomaticButton (_T("Default"), RGB( 255, 255, 255 ));
	m_bnColorBG.EnableOtherButton( _T("Custom...") );
	m_bnColorT.SetColor( m_pColorsT[ 0 ] );
	m_bnColorT.EnableAutomaticButton (_T("Default"), RGB( 0, 0, 0 ));
	m_bnColorT.EnableOtherButton( _T("Custom...") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ElementPageView::OnBnFont() 
{
	CFontDialog fd;

	fd.m_cf.iPointSize = m_pSizes[ m_nItem ] * 10;
	fd.m_cf.rgbColors = m_pColorsT[ m_nItem ];
	fd.m_cf.Flags |= CF_INITTOLOGFONTSTRUCT;
	fd.m_cf.Flags |= CF_EFFECTS;
	switch( m_nItem )
	{
		case 0: fd.m_cf.lpLogFont = &m_lf1; break;
		case 1: fd.m_cf.lpLogFont = &m_lf2; break;
		case 2: fd.m_cf.lpLogFont = &m_lf3; break;
	}

	if( fd.DoModal() != IDOK ) return;

	UpdateData( TRUE );
	m_szFont = fd.GetFaceName();
	m_nTextSize = fd.GetSize() / 10;
	m_szSize.Format( _T("%d"), m_nTextSize );
	m_pColorsT[ m_nItem ] = fd.GetColor();
	m_bnColorT.SetColor( m_pColorsT[ m_nItem ] );
	switch( m_nItem )
	{
		case 0: CopyFonts( &m_lf1, fd.m_cf.lpLogFont ); break;
		case 1: CopyFonts( &m_lf2, fd.m_cf.lpLogFont ); break;
		case 2: CopyFonts( &m_lf3, fd.m_cf.lpLogFont ); break;
	}
	UpdateData( FALSE );
}

BOOL ElementPageView::OnSetActive() 
{
	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = pSheet ? (ElementPageGeneral*)pSheet->GetPage( 0 ) : NULL;
	if( pPage )
	{
		CWnd* pWnd = NULL;
		if( !pPage->IsTarget() )
		{
			pWnd = GetDlgItem( IDC_CBO_MODE );
			pWnd->EnableWindow( FALSE );
			m_cboMode.SetCurSel( 0 );
			OnSelchangeCboMode();
			pWnd = GetDlgItem( IDC_TXT_INFORM );
			pWnd->ShowWindow( SW_SHOW );
		}
		else if( !pPage->IsPattern() )
		{
			pWnd = GetDlgItem( IDC_CBO_MODE );
			pWnd->EnableWindow( TRUE );
			pWnd = GetDlgItem( IDC_TXT_INFORM );
			pWnd->ShowWindow( SW_HIDE );
		}
	}
	
	return CBCGPPropertyPage::OnSetActive();
}

BOOL ElementPageView::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ElementPageView::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("element_appearance.html"), this );
	return TRUE;
}

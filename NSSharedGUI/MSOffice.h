#pragma once

/*************************************************
   MSOffice - version 1.0 - gmb - 15Oct2007
**************************************************

   Purpose: Integration w/MS Office

   REVISIONS:
   15Oct2007 - GMB - Initial revision: Excel 2007 ONLY for open delimited text files

***********************************************************************/

// EXCEL
namespace Excel
{
#define	EXCEL_FORMAT_NONE				0
#define	EXCEL_FORMAT_SUBJECTREPORT		1
#define	EXCEL_FORMAT_QUESTIONNAIRE		2

BOOL AFX_EXT_API OpenDelimitedText( CString szFile, UINT uiFormat = EXCEL_FORMAT_NONE,
									BOOL fTab = TRUE, BOOL fSemicolon = FALSE,
									BOOL fComma = FALSE, BOOL fSpace = FALSE );
};
// end Excel

#pragma once

#include "NSTooltipCtrl.h"

// LoginDlg dialog

class AFX_EXT_CLASS LoginDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(LoginDlg)

public:
	LoginDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~LoginDlg();

// Dialog Data
	enum { IDD = IDD_LOGIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);

	DECLARE_MESSAGE_MAP()
public:
	CString			m_szServer;
	CString			m_szUser;
	CString			m_szPassword;
	NSToolTipCtrl	m_tooltip;
};

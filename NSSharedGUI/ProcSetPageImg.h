#pragma once

// ProcSetPageImg.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageImg dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageImg : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageImg)

	// Construction
public:
	ProcSetPageImg( IProcSetting* pPS = NULL );
	~ProcSetPageImg();

	// Dialog Data
	enum { IDD = IDD_PAGE_PS_IMG };
	short			m_nSmoothIter;
	short			m_nInkThreshold;
	short			m_nImgReso;
	CColoredEdit	m_editSI;
	CColoredEdit	m_editIT;
	CColoredEdit	m_editIR;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_linkAdv;

	// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

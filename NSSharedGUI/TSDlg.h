#pragma once

#include <afxhtml.h>

// TroubleShootingDlg dialog

class TroubleShootingView;

class AFX_EXT_CLASS TroubleShootingDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(TroubleShootingDlg)

public:
	TroubleShootingDlg( CWnd* pParent = NULL ) : CBCGPDialog( TroubleShootingDlg::IDD, pParent ) {}
	virtual ~TroubleShootingDlg() {}

	// Dialog Data
	enum { IDD = IDD_TS };

protected:
	virtual BOOL OnInitDialog();

public:
	DECLARE_MESSAGE_MAP()
};

class TroubleShootingView : public CHtmlView
{
	DECLARE_DYNCREATE( TroubleShootingView )
protected:
	TroubleShootingView();
	virtual ~TroubleShootingView();

	// Operations
public:
	void ShowPage( CString szURL );
	CString ResourceToURL( CString szFile );

	// Overrides
public:
	virtual void OnInitialUpdate();
	virtual void OnShowWindow( BOOL bShow, UINT nStatus );

	// Implementation
protected:
	BOOL	m_fInit;

	// message map
protected:
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
};

// ActivateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ActivateDlg.h"
#include "..\Common\Security.h"
#include "..\NSShared\Hyperlink.h"

// ActivateDlg dialog


IMPLEMENT_DYNAMIC(ActivateDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(ActivateDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_CHK_SCRIPT, OnItemClicked)
	ON_BN_CLICKED(IDC_CHK_OPTI, OnItemClicked)
	ON_BN_CLICKED(IDC_CHK_GRIP, OnItemClicked)
	ON_BN_CLICKED(IDC_CHK_CS, OnItemClicked)
	ON_BN_CLICKED(IDC_CHK_RX, OnItemClicked)
	ON_BN_CLICKED(IDC_CHK_UPGRADE, OnItemClicked)
	ON_BN_CLICKED(IDC_BN_PURCHASE, OnBnClickedBnPurchase)
	ON_CONTROL(STN_CLICKED, IDC_TXT_INFO, OnClickDetails)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ActivateDlg::ActivateDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ActivateDlg::IDD, pParent), m_fScript( FALSE ),
	  m_fOpti( FALSE ), m_fGrip( FALSE ), m_fCS( FALSE ),
	  m_fPPU( FALSE ), m_fRx( FALSE ), m_fUpgrade( FALSE )
{
}

ActivateDlg::~ActivateDlg()
{
}

void ActivateDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_SCRIPT, m_fScript);
	DDX_Check(pDX, IDC_CHK_OPTI, m_fOpti);
	DDX_Check(pDX, IDC_CHK_GRIP, m_fGrip);
	DDX_Check(pDX, IDC_CHK_CS, m_fCS);
	DDX_Check(pDX, IDC_CHK_PPU, m_fPPU);
	DDX_Check(pDX, IDC_CHK_RX, m_fRx);
	DDX_Check(pDX, IDC_CHK_UPGRADE, m_fUpgrade);
}

// ActivateDlg message handlers

void ActivateDlg::OnItemClicked()
{
	UpdateData( TRUE );

	BOOL fPurchase = FALSE;

	// ONLY CHANGE STATUS OF THOSE ITEMS THAT ARE NOT ALREADY ACTIVATED
	// movalyzer
	if( !m_acts.m_opti.m_fActivated )
	{
		// should never really get unchecked by force
		m_acts.m_opti.m_fChecked = m_fOpti;
		m_acts.m_opti.m_fEnabled = TRUE;
		if( m_acts.m_fWasOpti )
		{
			m_acts.m_upgrade.m_fChecked = m_fOpti;
			m_acts.m_upgrade.m_fEnabled = m_fOpti;
			m_fUpgrade = m_fOpti;
		}
		// purchase button
		fPurchase |= m_acts.m_opti.m_fChecked;
	}
	// scriptalyzer
	if( !m_acts.m_scripta.m_fActivated )
	{
		// checked if mova or scripta & not upgrade
		m_acts.m_scripta.m_fChecked = ( m_fScript || m_fOpti ) && !m_fUpgrade;
		// disabled if is mova or an upgrade
		m_acts.m_scripta.m_fEnabled = !m_fOpti && !m_fUpgrade;
		// purchase button
		fPurchase |= m_acts.m_scripta.m_fChecked;
	}
	// rx
	if( !m_acts.m_rx.m_fActivated )
	{
		m_fRx = ( ( m_fRx && !m_fGrip && !m_fScript ) || ( m_fRx && m_fOpti ) );
		// checked if mova, scripta or rx (& not gripa alone) & not upgrade
		m_acts.m_rx.m_fChecked = ( m_fRx || m_fOpti ) && !m_fUpgrade;
		// disabled if scripta/mova/gripa or an upgrade
		m_acts.m_rx.m_fEnabled = !m_fScript && !m_fOpti && !m_fGrip && !m_fUpgrade;
		// purchase button
		fPurchase |= m_acts.m_rx.m_fChecked;
	}
	// gripalyzer
	if( !m_acts.m_gripa.m_fActivated )
	{
		// checked if gripa & not upgrade
		m_acts.m_gripa.m_fChecked = m_fGrip && ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// disabled if an upgrade
		m_acts.m_gripa.m_fEnabled = ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// purchase button
		fPurchase |= m_acts.m_gripa.m_fChecked;
	}
	// client-server
	if( !m_acts.m_cs.m_fActivated )
	{
		m_fCS = m_fCS && ( m_fOpti || m_fScript || m_fGrip ) && ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// checked if cs
		m_acts.m_cs.m_fChecked = m_fCS;
		// disabled if no product selected (except rx) or upgrade
		m_acts.m_cs.m_fEnabled = ( m_fOpti || m_fScript || m_fGrip ) && ( !m_fUpgrade || m_acts.m_upgrade.m_fActivated );
		// purchase button
		fPurchase |= m_acts.m_cs.m_fChecked;
	}

	m_fOpti = m_acts.m_opti.m_fChecked;
	m_fUpgrade = m_acts.m_upgrade.m_fChecked;
	m_fScript = m_acts.m_scripta.m_fChecked;
	m_fRx = m_acts.m_rx.m_fChecked;
	m_fGrip = m_acts.m_gripa.m_fChecked;
	m_fCS = m_acts.m_cs.m_fChecked;

	// activate/deactivate purchase button
	CWnd* pWnd = GetDlgItem( IDC_BN_PURCHASE );
	pWnd->EnableWindow( fPurchase );

	// enable/disable checkboxes where appropriate
	pWnd = GetDlgItem( IDC_CHK_SCRIPT );
	pWnd->EnableWindow( m_acts.m_scripta.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_OPTI );
	pWnd->EnableWindow( m_acts.m_opti.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_RX );
	pWnd->EnableWindow( m_acts.m_rx.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_GRIP );
	pWnd->EnableWindow( m_acts.m_gripa.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_CS );
	pWnd->EnableWindow( m_acts.m_cs.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_UPGRADE );
	pWnd->EnableWindow( m_acts.m_upgrade.m_fEnabled );

	UpdateData( FALSE );
}

void ActivateDlg::OnBnClickedBnPurchase()
{
	UpdateData( TRUE );

	BOOL fRet = FALSE;
	NSSecurity* pSecurity = ::GetSecurity();
	if( !pSecurity )
	{
		BCGPMessageBox( _T("Security object not available.") );
		return;
	}

	if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
		m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated &&
		m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: MovAlyzeR, GripAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->AOGCS();
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: MovAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->AOCS();
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
			 m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: MovAlyzeR, GripAlyzeR" ), TRUE );
		fRet = pSecurity->AGO();
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated &&
			 m_acts.m_upgrade.m_fChecked && !m_acts.m_upgrade.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: MovAlyzeR Upgrade" ), TRUE );
		fRet = pSecurity->AOU();
	}
	else if( m_acts.m_opti.m_fChecked && !m_acts.m_opti.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: MovAlyzeR" ), TRUE );
		fRet = pSecurity->AO( FALSE );
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated && 
			 m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated &&
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: ScriptAlyzeR, GripAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->ASGCS();
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated && 
			 m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: ScriptAlyzeR, GripAlyzeR" ), TRUE );
		fRet = pSecurity->ASG();
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated && 
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: ScriptAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->ASCS();
	}
	else if( m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated && 
			 m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: GripAlyzeR, Client-Server" ), TRUE );
		fRet = pSecurity->AGCS();
	}
	else if( m_acts.m_scripta.m_fChecked && !m_acts.m_scripta.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: ScriptAlyzeR" ), TRUE );
		fRet = pSecurity->AS( FALSE );
	}
	else if( m_acts.m_gripa.m_fChecked && !m_acts.m_gripa.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: GripAlyzeR" ), TRUE );
		fRet = pSecurity->AG( FALSE );
	}
	else if( m_acts.m_cs.m_fChecked && !m_acts.m_cs.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: Client-Server" ), TRUE );
		fRet = pSecurity->ACS();
	}
	else if( m_acts.m_rx.m_fChecked && !m_acts.m_rx.m_fActivated )
	{
		OutputAMessage( _T("Attempting to activate: MovAlyzeRx" ), TRUE );
		fRet = pSecurity->ARX( FALSE );
	}

	if( fRet )
	{
		CWnd* pWnd = GetDlgItem( IDC_CHK_SCRIPT );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_OPTI );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_GRIP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_CS );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_RX );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_UPGRADE );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_PURCHASE );
		pWnd->EnableWindow( FALSE );

		UpdateData( FALSE );

		pWnd = GetDlgItem( IDCANCEL );
		if( pWnd ) pWnd->SetWindowText( _T("Close" ) );

		BCGPMessageBox( _T("Activation successful.") );
	}
	else
	{
		BCGPMessageBox( _T("Nothing has been activated.") );
		EndDialog( IDCANCEL );
	}
}

BOOL ActivateDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	NSSecurity* pSecurity = ::GetSecurity();
	if( pSecurity ) m_acts.m_fWasOpti = pSecurity->WasOldMova();

	// by default, upgrade is disabled
	m_acts.m_upgrade.m_fEnabled = FALSE;

	if( ::IsOA( FALSE ) )
	{
		m_acts.m_opti.m_fActivated = TRUE;
		m_acts.m_opti.m_fChecked = TRUE;
		m_acts.m_opti.m_fEnabled = FALSE;
		m_acts.m_upgrade.m_fActivated = TRUE;
		m_acts.m_upgrade.m_fChecked = m_acts.m_fWasOpti;
		m_acts.m_upgrade.m_fEnabled = FALSE;
		m_acts.m_scripta.m_fActivated = TRUE;
		m_acts.m_scripta.m_fChecked = TRUE;
		m_acts.m_scripta.m_fEnabled = FALSE;
		m_acts.m_rx.m_fActivated = TRUE;
		m_acts.m_rx.m_fChecked = TRUE;
		m_acts.m_rx.m_fEnabled = FALSE;
	}
	if( ::IsSA( FALSE ) )
	{
		m_acts.m_scripta.m_fActivated = TRUE;
		m_acts.m_scripta.m_fChecked = TRUE;
		m_acts.m_scripta.m_fEnabled = FALSE;
	}
	if( ::IsRxA( FALSE ) )
	{
		m_acts.m_rx.m_fActivated = TRUE;
		m_acts.m_rx.m_fChecked = TRUE;
		m_acts.m_rx.m_fEnabled = FALSE;
	}
	if( ::IsGA( FALSE ) )
	{
		m_acts.m_gripa.m_fActivated = TRUE;
		m_acts.m_gripa.m_fChecked = TRUE;
		m_acts.m_gripa.m_fEnabled = FALSE;
	}
	if( ::IsCS( FALSE ) && ( ::IsSA( FALSE ) || ::IsOA( FALSE ) || ::IsGA( FALSE ) ) )
	{
		m_acts.m_cs.m_fActivated = TRUE;
		m_acts.m_cs.m_fChecked = TRUE;
		m_acts.m_cs.m_fEnabled = FALSE;
	}

	m_fOpti = m_acts.m_opti.m_fChecked;
	m_fUpgrade = m_acts.m_upgrade.m_fChecked;
	m_fScript = m_acts.m_scripta.m_fChecked;
	m_fRx = m_acts.m_rx.m_fChecked;
	m_fGrip = m_acts.m_gripa.m_fChecked;
	m_fCS = m_acts.m_cs.m_fChecked;


	CWnd* pWnd = GetDlgItem( IDC_CHK_SCRIPT );
	pWnd->EnableWindow( m_acts.m_scripta.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_OPTI );
	pWnd->EnableWindow( m_acts.m_opti.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_GRIP );
	pWnd->EnableWindow( m_acts.m_gripa.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_CS );
	pWnd->EnableWindow( m_acts.m_cs.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_RX );
	pWnd->EnableWindow( m_acts.m_rx.m_fEnabled );
	pWnd = GetDlgItem( IDC_CHK_UPGRADE );
	pWnd->EnableWindow( FALSE );

	UpdateData( FALSE );

	return TRUE;
}

BOOL ActivateDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("activation.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void ActivateDlg::OnClickDetails()
{
	CWaitCursor crs;
	CHyperLink::GotoURL( _T("https://sales.neuroscript.net") );
}

// ActivationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ActivationDlg.h"
#include <afxinet.h>
#include "..\NSShared\Hyperlink.h"


// ActivationDlg dialog

IMPLEMENT_DYNAMIC(ActivationDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(ActivationDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_CONTACTS, OnBnClickedBnContacts)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ActivationDlg::ActivationDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ActivationDlg::IDD, pParent), m_szRequestCode( _T("") ), m_szActivationCode( _T("") )
{
}

ActivationDlg::~ActivationDlg()
{
}

void ActivationDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_REQUESTCODE, m_szRequestCode);
	DDX_Text(pDX, IDC_EDIT_ACTIVATIONCODE, m_szActivationCode);
}

// ActivationDlg message handlers

BOOL ActivationDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	if( m_szProducts != _T("") )
	{
		CString szTitle = _T("Activate - ");
		szTitle += m_szProducts;
		SetWindowText( szTitle );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void ActivationDlg::OnOK()
{
	// update data vars
	UpdateData( TRUE );

	// cleanup any whitespaces
	TRIM( m_szActivationCode );

	// verify the legitimacy of the activation code
	if( ( m_szActivationCode.GetLength() != 19 ) ||
		( m_szActivationCode.GetAt( 4 ) != '-' ) ||
		( m_szActivationCode.GetAt( 9 ) != '-' ) ||
		( m_szActivationCode.GetAt( 14 ) != '-' ) )
	{
		BCGPMessageBox( _T("The Activation/Unlock code provided is not in the appropriate format of ####-####-####-####.") );
		return;
	}
	for( int i = 0; i < 19; i++ )
	{
		if( ( i != 4 ) && ( i != 9 ) && ( i != 14 ) )
		{
			if( !isdigit( m_szActivationCode.GetAt( i ) ) &&
				!isalpha( m_szActivationCode.GetAt( i ) ) )
			{
				BCGPMessageBox( _T("The Activation/Unlock code provided is not in the appropriate format of ####-####-####-####.") );
				return;
			}
		}
	}

	CBCGPDialog::OnOK();
}

void ActivationDlg::OnBnClickedBnContacts()
{
	CWinApp* pApp = AfxGetApp();
	CWnd* pWnd = pApp ? pApp->GetMainWnd() : NULL;
	if( pWnd ) ::PostMessage( pWnd->m_hWnd, WM_COMMAND, ID_APP_ABOUT, 0 );
}

BOOL ActivationDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("activation.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

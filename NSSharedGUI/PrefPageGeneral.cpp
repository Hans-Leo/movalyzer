// PrefPageGeneral.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PrefSheet.h"
#include "PrefPageGeneral.h"
#include "PrefPageUser.h"
#include "..\Common\Shared.h"
#include "Users.h"
#include "shlobj.h" // For SHFunctions
#include "direct.h"
#include "errno.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static BOOL _fPathSet = FALSE;

/////////////////////////////////////////////////////////////////////////////
// PrefPageGeneral property page

IMPLEMENT_DYNCREATE(PrefPageGeneral, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(PrefPageGeneral, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnBrowse)
	ON_BN_CLICKED(IDC_BN_BROWSE2, OnBnBrowse2)
	ON_BN_CLICKED(IDC_CHK_PRIVATE, OnClickPrivate)
	ON_BN_CLICKED(IDC_BN_RESET, OnClickReset)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_CHK_MOVAIDS, OnClickGenID)
	ON_EN_KILLFOCUS(IDC_EDIT_IDSTART, OnKillfocusIDStart)
END_MESSAGE_MAP()

PrefPageGeneral::PrefPageGeneral( Preferences* pPref )
	: CBCGPPropertyPage(PrefPageGeneral::IDD), m_pPref( pPref )
{
	m_fLog = TRUE;
	m_fAlpha = FALSE;
	m_szDelim = _T(":");
	m_szPath = _T("");
	m_szBackup = _T("");
	m_fAutoUpdate = TRUE;
	m_fPrivate = FALSE;
	m_fMovaGen = TRUE;
	_fPathSet = FALSE;
	m_fAutoShow = TRUE;
	m_fShowSubjects = TRUE;
}

PrefPageGeneral::~PrefPageGeneral()
{
}

void PrefPageGeneral::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_RECORD, m_fLog);
	DDX_Check(pDX, IDC_CHK_ALPHA, m_fAlpha);
	DDX_Text(pDX, IDC_EDIT_DELIM, m_szDelim);
	DDV_MaxChars(pDX, m_szDelim, 1);
	DDX_Control(pDX, IDC_EDIT_PATH, m_wndFolderPath);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szPath);
	DDV_MaxChars(pDX, m_szPath, _MAX_PATH - 1);
	DDX_Control(pDX, IDC_EDIT_PATH2, m_wndFolderBackup);
	DDX_Text(pDX, IDC_EDIT_PATH2, m_szBackup);
	DDV_MaxChars(pDX, m_szBackup, _MAX_PATH - 1);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnBrowse);
	DDX_Control(pDX, IDC_BN_BROWSE2, m_bnBrowse2);
	DDX_Check(pDX, IDC_CHK_UPDATE, m_fAutoUpdate);
	DDX_Check(pDX, IDC_CHK_PRIVATE, m_fPrivate);
	DDX_Check(pDX, IDC_CHK_MOVAIDS, m_fMovaGen);
	DDX_Text(pDX, IDC_EDIT_IDSTART, m_szStart);
	DDV_MaxChars(pDX, m_szStart, 3);
	DDX_Text(pDX, IDC_EDIT_IDEND, m_szEnd);
	DDV_MaxChars(pDX, m_szEnd, 3);
	DDX_Text(pDX, IDC_EDIT_IDNEXT, m_szCur );
	DDX_Control(pDX, IDC_BN_RESET, m_bnReset);
	DDX_Check(pDX, IDC_CHK_HIDE, m_fAutoShow);
	DDX_Check(pDX, IDC_CHK_PRIVACY, m_fShowSubjects);
}

/////////////////////////////////////////////////////////////////////////////
// PrefPageGeneral message handlers

BOOL PrefPageGeneral::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_bnBrowse.SetImage( IDB_OPEN );
	m_bnBrowse2.SetImage( IDB_OPEN );
	m_bnReset.SetImage( IDB_REF );

	EnableVisualManagerStyle( TRUE );

	if( m_pPref )
	{
		m_fLog = m_pPref->fLog;
		m_fAlpha = m_pPref->fAlpha;
		m_szDelim = m_pPref->pszDelim;
		m_fPrivate = m_pPref->fPrivate;
		m_szPath = m_pPref->pszPath;
		m_szBackup = m_pPref->pszBackup;
		m_fAutoUpdate = m_pPref->fAutoUpdate;
		m_szStart = m_pPref->pszSubjStartID;
		m_szEnd = m_pPref->pszSubjEndID;
		m_szCur = m_pPref->pszSubjCurID;
	}
	// Get from registry
	if( ::IsAppRx() )
	{
		CBCGPWorkspace* pApp = GetWorkspaceApp();
		if( pApp )
		{
			CString szVal, szDef, szKey, szRB, szID;
			::GetCurrentUser( szID );
			szVal.Format( _T("Settings\\%s"), szID );
			szRB = pApp->GetRegistryBase();
			pApp->SetRegistryBase( szVal );
			m_fAutoShow = ( pApp->GetInt( _T("AutoShowViews"), 1 ) == 1 );
			m_fShowSubjects = ( pApp->GetInt( _T("ShowAllSubjects"), 1 ) == 1 );
			pApp->SetRegistryBase( szRB );
		}
	}
	UpdateData( FALSE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_USER );
	m_tooltip.SetTitle( _T("USER SETTINGS") );
	CWnd* pWnd = GetDlgItem( IDC_CHK_RECORD );
	m_tooltip.AddTool( pWnd, IDS_PPG_LOG );
	pWnd = GetDlgItem( IDC_CHK_ALPHA );
	m_tooltip.AddTool( pWnd, IDS_PPG_ALPHA, _T("Sort Alphabetically") );
	pWnd = GetDlgItem( IDC_EDIT_DELIM );
	m_tooltip.AddTool( pWnd, IDS_DELIM, _T("Delimiter") );
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	m_tooltip.AddTool( pWnd, _T("Specify the root location for data."), _T("Root Data Path") );
	pWnd = GetDlgItem( IDC_EDIT_PATH2 );
	m_tooltip.AddTool( pWnd, _T("Specify the backup location for data."), _T("Backup Data Path") );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	m_tooltip.AddTool( pWnd, _T("Browse for the root location."), _T("Browse") );
	pWnd = GetDlgItem( IDC_BN_BROWSE2 );
	m_tooltip.AddTool( pWnd, _T("Browse for the backup location."), _T("Browse") );
	pWnd = GetDlgItem( IDC_CHK_UPDATE );
	m_tooltip.AddTool( pWnd, _T("Toggle automatic update checking when program is run.") );
	pWnd = GetDlgItem( IDC_CHK_PRIVATE );
	m_tooltip.AddTool( pWnd, _T("Private MovAlyzeR users are hidden from and not available to other Windows users."),
					   _T("Private user") );
	pWnd = GetDlgItem( IDC_CHK_MOVAIDS );
	m_tooltip.AddTool( pWnd, _T("MovAlyzeR will generate subject IDs automatically."), _T("Subject ID Generation") );
	pWnd = GetDlgItem( IDC_EDIT_IDSTART );
	m_tooltip.AddTool( pWnd, _T("Starting range of generated subject IDs."), _T("Start of Range") );
	pWnd = GetDlgItem( IDC_EDIT_IDEND );
	m_tooltip.AddTool( pWnd, _T("Ending range of generated subject IDs."), _T("End of Range") );
	pWnd = GetDlgItem( IDC_EDIT_IDNEXT );
	m_tooltip.AddTool( pWnd, _T("Last ID generated for subject."), _T("Current ID") );
	pWnd = GetDlgItem( IDC_BN_RESET );
	m_tooltip.AddTool( pWnd, _T("Reset current/next subject ID to the start of the range. Existing subjects will NOT be overwritten."), _T("Reset Current") );
	pWnd = GetDlgItem( IDC_CHK_HIDE );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Check ON to have the test, task, visit and trial windows open automatically upon selecting a patient."), _T("Auto-Open Views") );
		if( !::IsAppRx() ) pWnd->ShowWindow( SW_HIDE );
	}
	pWnd = GetDlgItem( IDC_CHK_PRIVACY );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Check ON to display all patients in the Patient view when not filtering\nIf checked OFF, patients will be hidden until you type in the find box (to filter)."), _T("Show All Patients") );
		if( !::IsAppRx() ) pWnd->ShowWindow( SW_HIDE );
	}

	m_wndFolderPath.EnableFolderBrowseButton();
	m_wndFolderBackup.EnableFolderBrowseButton();

	// get movalyzer subject ID gen information
	NSMUsers user;
	if( SUCCEEDED( user.Find( m_pPref->pszID ) ) )
	{
		user.GetSubjectStartID( m_szStart );
		if( m_szStart == _T("") ) m_szStart = _T("000");
		user.GetSubjectEndID( m_szEnd );
		if( m_szEnd == _T("") ) m_szEnd = _T("ZZZ");
		user.GetSubjectCurID( m_szCur );
		if( m_szCur == _T("") ) m_szCur = m_szStart;
		user.GetGenerateSubjectIDs( m_fMovaGen );
		UpdateData( FALSE );
	}
	if( m_fMovaGen )
	{
		pWnd = GetDlgItem( IDC_EDIT_IDSTART );
		pWnd->EnableWindow( TRUE );
		pWnd = GetDlgItem( IDC_EDIT_IDEND );
		pWnd->EnableWindow( TRUE );
		pWnd = GetDlgItem( IDC_EDIT_IDNEXT );
		pWnd->EnableWindow( TRUE );
		pWnd = GetDlgItem( IDC_BN_RESET );
		pWnd->EnableWindow( TRUE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL PrefPageGeneral::OnSetActive() 
{
	PrefSheet* pSheet = (PrefSheet*)GetParent();
	PrefPageUser* p = (PrefPageUser*)pSheet->GetPage( 0 );
	if( !_fPathSet && p->m_fNew && m_pPref )
	{
		m_szPath = m_pPref->pszPath;
		m_szBackup = m_pPref->pszBackup;
		UpdateData( FALSE );
		_fPathSet = TRUE;
	}
	else if( !p->m_fNew )
	{
#ifndef	_DEBUG
		CWnd* pWnd = GetDlgItem( IDC_CHK_PRIVATE );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_BROWSE );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_BROWSE2 );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_PATH );
		if( pWnd ) ((CEdit*)pWnd)->SetReadOnly( TRUE );
		pWnd = GetDlgItem( IDC_EDIT_PATH2 );
		if( pWnd ) ((CEdit*)pWnd)->SetReadOnly( TRUE );
#endif
	}

	return CBCGPPropertyPage::OnSetActive();
}

BOOL PrefPageGeneral::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

void PrefPageGeneral::OnBnBrowse()
{
	BROWSEINFO bi;
	memset( (LPVOID)&bi, 0, sizeof( bi ) );
	TCHAR szDisplayName[ _MAX_PATH ];
	szDisplayName[ 0 ] = '\0';
	bi.hwndOwner = GetSafeHwnd();
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szDisplayName;
	bi.lpszTitle = _T("Select a folder:");
	bi.ulFlags = BIF_RETURNONLYFSDIRS;

	LPITEMIDLIST pIIL = ::SHBrowseForFolder( &bi );

	TCHAR szInitialDir[ _MAX_PATH ];
	BOOL bRet = ::SHGetPathFromIDList( pIIL, (char*)&szInitialDir );
	if( bRet )
	{
		if( szInitialDir != _T("") ) m_szPath = szInitialDir;

		LPMALLOC pMalloc;
		HRESULT hr = SHGetMalloc( &pMalloc );
		pMalloc->Free( pIIL );
		pMalloc->Release();

		UpdateData( FALSE );
	}
}

void PrefPageGeneral::OnBnBrowse2()
{
	BROWSEINFO bi;
	memset( (LPVOID)&bi, 0, sizeof( bi ) );
	TCHAR szDisplayName[ _MAX_PATH ];
	szDisplayName[ 0 ] = '\0';
	bi.hwndOwner = GetSafeHwnd();
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szDisplayName;
	bi.lpszTitle = _T("Select a folder:");
	bi.ulFlags = BIF_RETURNONLYFSDIRS;

	LPITEMIDLIST pIIL = ::SHBrowseForFolder( &bi );

	TCHAR szInitialDir[ _MAX_PATH ];
	BOOL bRet = ::SHGetPathFromIDList( pIIL, (char*)&szInitialDir );
	if( bRet )
	{
		if( szInitialDir != _T("") ) m_szBackup = szInitialDir;

		LPMALLOC pMalloc;
		HRESULT hr = SHGetMalloc( &pMalloc );
		pMalloc->Free( pIIL );
		pMalloc->Release();

		UpdateData( FALSE );
	}
}

BOOL PrefPageGeneral::OnApply() 
{
	UpdateData( TRUE );

	// reset data paths back to original if protected user
	PrefSheet* pSheet = (PrefSheet*)GetParent();
	PrefPageUser* p = (PrefPageUser*)pSheet->GetPage( 0 );
	BOOL fNewUser = p->m_fNew;
#ifndef	_DEBUG
	PrefPageUser* pP = (PrefPageUser*)pSheet->GetPage( 0 );
	if( m_pPref && pP && ( pP->m_szID.Left( 2 ) == _T("UU") ) )
	{
		m_szPath = m_pPref->pszPath;
		m_szBackup = m_pPref->pszBackup;
		UpdateData( FALSE );
	}
#endif

	// Delimiter
	if( m_szDelim.GetLength() < 1 )
	{
		BCGPMessageBox( IDS_ERR_DELIMLEN );
		return FALSE;
	}
	// we must check all databases for IDs and Descriptions/names to see if they contain the delimiter
	// not for new user
	if( !fNewUser && !::VerifyDatabaseForReserved( m_szDelim ) )
	{
		CString szMsg = _T("The specified delimiter is currently being used in existing IDs and descriptions.\nIf you use this delimiter, the behavior of the application will be unpredictable.\n\nDo you want to select a different delimiter?");
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
		{
			((PrefSheet*)GetParent())->SetActivePage( 1 );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_DELIM );
			if( pWnd ) pWnd->SetFocus();
			return FALSE;
		}
	}

	// Subject ID generation start/end IDs
	if( m_fMovaGen && ( m_szStart.GetLength() < 3 ) )
	{
		BCGPMessageBox( _T("Starting subject ID must be 3 characters") );
		((PrefSheet*)GetParent())->SetActivePage( 1 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_IDSTART );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_fMovaGen && !::VerifyStringForReserved( m_szStart ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Starting ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		((PrefSheet*)GetParent())->SetActivePage( 1 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_IDSTART );
		pWnd->SetWindowText( _T("") );
		pWnd->SetFocus();
	}
	if( m_fMovaGen && ( m_szEnd.GetLength() < 3 ) )
	{
		BCGPMessageBox( _T("Ending subject ID must be 3 characters") );
		((PrefSheet*)GetParent())->SetActivePage( 1 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_IDSTART );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_fMovaGen && !::VerifyStringForReserved( m_szEnd ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Ending ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		((PrefSheet*)GetParent())->SetActivePage( 1 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_IDEND );
		pWnd->SetWindowText( _T("") );
		pWnd->SetFocus();
	}
	if( m_szStart > m_szEnd )
	{
		BCGPMessageBox( _T("Starting subject ID must be less than ending subject ID.") );
		((PrefSheet*)GetParent())->SetActivePage( 1 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_IDSTART );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	// DATA PATHS
	// Ensure we cwd to exe dir
	CString szAppPath;
	::GetAppPath( szAppPath );
	_chdir( szAppPath );

	// Root data path
	if( m_szPath == _T("") )
	{
		BCGPMessageBox( _T("A data path must be specified.") );
		return FALSE;
	}
	CString szTest = m_szPath;
	szTest.MakeUpper();
	int nPos = szTest.Find( _T("\\CON\\") );
	if( nPos == -1 ) szTest.Find( _T("\\CON") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Data path cannot contain \'CON\'.") );
		return FALSE;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos == -1 ) szTest.Find( _T("\\NUL") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Data path cannot contain \'NUL\'.") );
		return FALSE;
	}
	CStringList lst;
	CString szDrive, szItem, szMsg;
	POSITION pos = NULL;
	BOOL fNWDrives = ::GetNetworkDrives( &lst );
	if( fNWDrives )
	{
		szDrive = szTest.Left( 2 );
		pos = lst.GetHeadPosition();
		while( pos )
		{
			szItem = lst.GetNext( pos );
			szItem.MakeUpper();
			if( szItem == szDrive )
			{
				szMsg = _T("The specified data path is a network drive.\nUsing network drives can yield unexpected results when your network is slow or down.\n\nDo you wish to use this network drive?");
				if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return FALSE;
			}
		}
	}
	nPos = m_szPath.ReverseFind( '\\' );
	while( nPos == ( m_szPath.GetLength() - 1 ) )
	{
		m_szPath = m_szPath.Left( m_szPath.GetLength() - 1 );
		nPos = m_szPath.ReverseFind( '\\' );
	}
	if( m_szPath[ m_szPath.GetLength() - 1 ] == '\\' )
		m_szPath = m_szPath.Left( m_szPath.GetLength() - 1 );
	CFileFind ff;
	if( !ff.FindFile( m_szPath ) )
	{
		szTest.Format( _T("Data path %s does not exist. Create?"), m_szPath );
		if( BCGPMessageBox( szTest, MB_YESNO ) == IDYES )
		{
			int nRes, nPos = 0;
			CString szTmp;

			if( m_szPath[ m_szPath.GetLength() - 1 ] != '\\' ) m_szPath += _T("\\");
			nPos = m_szPath.Find( _T("\\") );
			nPos = m_szPath.Find( _T("\\"), nPos + 1 );
			while( nPos != -1 )
			{
				szTmp = m_szPath.Left( nPos );
				if( !ff.FindFile( szTmp ) )
				{
					nRes = _mkdir( szTmp );
					if( ( nRes != 0 ) && ( errno != EEXIST ) )
					{
						BCGPMessageBox( _T("Error creating data path directory.") );
						return FALSE;
					}
				}

				nPos = m_szPath.Find( _T("\\"), nPos + 1 );
			}
		}
		else return FALSE;
	}
	// test for writeability
	CStdioFile f;
	szTest = m_szPath;
	szTest += _T("\\test.txt");
	if( !f.Open( szTest, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("You do not have the permission to create files in the specified root path.") );
	}
	else
	{
		f.Close();
		CFile::Remove( szTest );
	}

	// Backup data path
	if( m_szBackup == _T("") )
	{
		BCGPMessageBox( _T("A backup path must be specified.") );
		return FALSE;
	}
	szTest = m_szBackup;
	szTest.MakeUpper();
	nPos = szTest.Find( _T("\\CON\\") );
	if( nPos == -1 ) szTest.Find( _T("\\CON") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Backup path cannot contain \'CON\'.") );
		return FALSE;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos == -1 ) szTest.Find( _T("\\NUL") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Backup path cannot contain \'NUL\'.") );
		return FALSE;
	}
	if( fNWDrives )
	{
		szDrive = szTest.Left( 2 );
		pos = lst.GetHeadPosition();
		while( pos )
		{
			szItem = lst.GetNext( pos );
			szItem.MakeUpper();
			if( szItem == szDrive )
			{
				szMsg = _T("The specified backup path is a network drive.\nUsing network drives can yield unexpected results when your network is slow or down.\n\nDo you wish to use this network drive?");
				if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return FALSE;
			}
		}
	}
	nPos = m_szBackup.ReverseFind( '\\' );
	while( nPos == ( m_szBackup.GetLength() - 1 ) )
	{
		m_szBackup = m_szBackup.Left( m_szBackup.GetLength() - 1 );
		nPos = m_szBackup.ReverseFind( '\\' );
	}
	if( m_szBackup[ m_szBackup.GetLength() - 1 ] == '\\' )
		m_szBackup = m_szBackup.Left( m_szBackup.GetLength() - 1 );
	if( !ff.FindFile( m_szBackup ) )
	{
		szTest.Format( _T("Backup path %s does not exist. Create?"), m_szBackup );
		if( BCGPMessageBox( szTest, MB_YESNO ) == IDYES )
		{
			int nRes, nPos = 0;
			CString szTmp;

			if( m_szBackup[ m_szBackup.GetLength() - 1 ] != '\\' ) m_szBackup += _T("\\");
			nPos = m_szBackup.Find( _T("\\") );
			nPos = m_szBackup.Find( _T("\\"), nPos + 1 );
			while( nPos != -1 )
			{
				szTmp = m_szBackup.Left( nPos );
				if( !ff.FindFile( szTmp ) )
				{
					nRes = _mkdir( szTmp );
					if( ( nRes != 0 ) && ( errno != EEXIST ) )
					{
						BCGPMessageBox( _T("Error creating backup directory.") );
						return FALSE;
					}
				}

				nPos = m_szBackup.Find( _T("\\"), nPos + 1 );
			}
		}
		else return FALSE;
	}
	// test for writeability
	szTest = m_szBackup;
	szTest += _T("\\test.txt");
	if( !f.Open( szTest, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("You do not have the permission to create files in the specified backup path.") );
	}
	else
	{
		f.Close();
		CFile::Remove( szTest );
	}

	// is backup path the same as data path?
	CString szR = m_szPath, szB = m_szBackup;
	szR.MakeUpper();
	szB.MakeUpper();
	if( szR == szB )
	{
		BCGPMessageBox( _T("Backup path cannot be the same as the root data path.") );
		return FALSE;
	}

	if( m_pPref )
	{
		m_pPref->fLog = m_fLog;
		m_pPref->fAlpha = m_fAlpha;
		strcpy_s( m_pPref->pszDelim, 2, m_szDelim );
		strcpy_s( m_pPref->pszPath, _MAX_PATH, m_szPath );
		strcpy_s( m_pPref->pszBackup, _MAX_PATH, m_szBackup );
		m_pPref->fAutoUpdate = m_fAutoUpdate;

		m_pPref->fPrivate = m_fPrivate;
		if( m_fPrivate )
		{
			CString szUser;
			::GetWinUser( szUser );
			strcpy_s( m_pPref->pszWinUser, 50, szUser );
		}
		else strcpy_s( m_pPref->pszWinUser, 50, "" );

		m_pPref->fGenSubjID = m_fMovaGen;
		strncpy_s( m_pPref->pszSubjStartID, 4, m_szStart, _TRUNCATE );
		strncpy_s( m_pPref->pszSubjEndID, 4, m_szEnd, _TRUNCATE );
		if( m_szCur < m_szStart ) m_szCur = m_szStart;
		strncpy_s( m_pPref->pszSubjCurID, 4, m_szCur, _TRUNCATE );
	}
	if( ::IsAppRx() )
	{
		CBCGPWorkspace* pApp = GetWorkspaceApp();
		if( pApp )
		{
			CString szVal, szDef, szKey, szRB, szID;
			::GetCurrentUser( szID );
			szVal.Format( _T("Settings\\%s"), szID );
			szRB = pApp->GetRegistryBase();
			pApp->SetRegistryBase( szVal );
			pApp->WriteInt( _T("AutoShowViews"), m_fAutoShow ? 1 : 0 );
			pApp->WriteInt( _T("ShowAllSubjects"), m_fShowSubjects ? 1 : 0 );
			pApp->SetRegistryBase( szRB );
		}
	}

	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL PrefPageGeneral::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL PrefPageGeneral::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newuser.html"), this );
	return TRUE;
}

void PrefPageGeneral::OnClickPrivate()
{
	PrefSheet* pSheet = (PrefSheet*)GetParent();
	PrefPageUser* p = (PrefPageUser*)pSheet->GetPage( 0 );

	UpdateData( TRUE );
	m_pPref->fPrivate = m_fPrivate;

	CString szPath;
	::GetDataPath( szPath, m_pPref->fPrivate, TRUE );
	if( szPath[ szPath.GetLength() - 1 ] != '\\' ) szPath += _T("\\");
	szPath += p->m_szID;

	strcpy_s( m_pPref->pszPath, _MAX_PATH, szPath );
	szPath += _T("\\BACKUP");
	strcpy_s( m_pPref->pszBackup, _MAX_PATH, szPath );

	_fPathSet = FALSE;
	OnSetActive();
}

void PrefPageGeneral::OnClickReset()
{
	UpdateData( TRUE );

	// find the first available user ID within the range
	// get total list
	PrefSheet* pSheet = (PrefSheet*)GetParent();
	PrefPageUser* p = (PrefPageUser*)pSheet->GetPage( 0 );
	CString szWork = m_szStart;
	while( p->m_lstItems.Find( szWork ) )
	{
		NextID( szWork, m_szStart, m_szEnd, FALSE );
		if( szWork > m_szEnd )
		{
			BCGPMessageBox( _T("There are no available subject IDs within the specified range.") );
			return;
		}
	}
	m_szCur = szWork;
	szWork.Format( _T("Current subject ID to generate reset to %s"), m_szCur );
	::OutputAMessage( szWork );

	UpdateData( FALSE );
}

void PrefPageGeneral::OnClickGenID()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_IDSTART );
	pWnd->EnableWindow( m_fMovaGen );
	pWnd = GetDlgItem( IDC_EDIT_IDEND );
	pWnd->EnableWindow( m_fMovaGen );
	pWnd = GetDlgItem( IDC_EDIT_IDNEXT );
	pWnd->EnableWindow( m_fMovaGen );
	pWnd = GetDlgItem( IDC_BN_RESET );
	pWnd->EnableWindow( m_fMovaGen );
}

void PrefPageGeneral::OnKillfocusIDStart()
{
	CString szOld = m_szStart;
	UpdateData( TRUE );
	if( szOld != m_szStart )
	{
		m_szCur = m_szStart;
		UpdateData( FALSE );
	}
}

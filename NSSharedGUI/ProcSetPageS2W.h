#pragma once

// ProcSetPageS2W.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageS2W dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageS2W : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageS2W)

// Construction
public:
	ProcSetPageS2W( IProcSetting* pPS = NULL );
	~ProcSetPageS2W();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_S2W };
	double			maxtimepenup;
	double			mindownsp;
	double			mintimepenup;
	double			minleftpenup;
	double			minleftsp;
	double			minwidth;
	CColoredEdit	m_editMaxTP;
	CColoredEdit	m_editMinDSP;
	CColoredEdit	m_editMinTP;
	CColoredEdit	m_editMinLP;
	CColoredEdit	m_editMinLSP;
	CColoredEdit	m_editWidth;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBnCalc();
};

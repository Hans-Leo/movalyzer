// SQuestionnaireDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "SQuestionnaireDlg.h"
#include "..\CTreeLink\DBCommon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireDlg dialog

BEGIN_MESSAGE_MAP(SQuestionnaireDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_PREVQ, OnBnPrevq)
	ON_BN_CLICKED(IDC_BN_NEXTQ, OnBnNextq)
	ON_BN_CLICKED(IDC_CHK_USE, OnCheckNumeric)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

SQuestionnaireDlg::SQuestionnaireDlg(CStringList* pListH, CStringList* pListQ,
									 CStringList* pListA, CStringList* pListN,
									 CStringList* pListI, CStringList* pListCH,
									 BOOL fEdit, int nIdx, CWnd* pParent)
	: CBCGPDialog(SQuestionnaireDlg::IDD, pParent), m_pListH( pListH ), m_pListN( pListN ),
	  m_pListQ( pListQ ), m_pListA( pListA ), m_fEdit( fEdit ), m_nIdx( nIdx ),
	  m_pListID( pListI ), m_pListCH( pListCH )
{
	m_szID = _T("");
	m_szColHeader = _T("");
	m_fNum = FALSE;
	m_szQuestion = _T("");
	m_szAnswer = _T("");
	m_fClose = FALSE;
	m_fCanClose = TRUE;
	m_fHdr = FALSE;
	m_fNum = FALSE;
}

void SQuestionnaireDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_Q, m_szQuestion);
	DDV_MaxChars(pDX, m_szQuestion, szQUESTIONNAIRE_Q);
	DDX_Text(pDX, IDC_EDIT_A, m_szAnswer);
	DDV_MaxChars(pDX, m_szAnswer, szQUESTIONNAIRE_A);
	DDX_Control(pDX, IDC_BN_PREVQ, m_bnPrev);
	DDX_Control(pDX, IDC_BN_NEXTQ, m_bnNext);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szColHeader);
	DDX_Check(pDX, IDC_CHK_USE, m_fNum);
}

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireDlg message handlers

void SQuestionnaireDlg::OnBnPrevq() 
{
	if( !m_pListH || !m_pListQ || !m_pListA || !m_pListN ) return;
	if( m_nIdx < 0 ) return;

	POSITION pos = NULL;
	BOOL fDel = FALSE;

	UpdateData ( TRUE );

	pos = m_pListH->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListH->RemoveAt( pos );
		pos = m_pListH->FindIndex( m_nIdx );
		if( pos ) m_pListH->InsertBefore( pos, m_fHdr ? _T("TRUE") : _T("") );
		else m_pListH->AddTail( m_fHdr ? _T("TRUE") : _T("") );
	}

	pos = m_pListN->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListN->RemoveAt( pos );
		pos = m_pListN->FindIndex( m_nIdx );
		if( pos ) m_pListN->InsertBefore( pos, m_fNum ? _T("TRUE") : _T("") );
		else m_pListN->AddTail( m_fNum ? _T("TRUE") : _T("") );
	}

	pos = m_pListID->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListID->RemoveAt( pos );
		pos = m_pListID->FindIndex( m_nIdx );
		if( pos ) m_pListID->InsertBefore( pos, m_szID );
		else m_pListID->AddTail( m_szID );
	}

	pos = m_pListCH->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListCH->RemoveAt( pos );
		pos = m_pListCH->FindIndex( m_nIdx );
		if( pos ) m_pListCH->InsertBefore( pos, m_szColHeader );
		else m_pListCH->AddTail( m_szColHeader );
	}

	pos = m_pListQ->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListQ->RemoveAt( pos );
		pos = m_pListQ->FindIndex( m_nIdx );
		if( pos ) m_pListQ->InsertBefore( pos, m_szQuestion );
		else m_pListQ->AddTail( m_szQuestion );
	}

	pos = m_pListA->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListA->RemoveAt( pos );
		pos = m_pListA->FindIndex( m_nIdx );
		if( pos ) m_pListA->InsertBefore( pos, m_szAnswer );
		else m_pListA->AddTail( m_szAnswer );
	}

	m_nIdx--;

	CWnd* pWnd = GetDlgItem( IDC_BN_PREVQ );
	if( m_nIdx <= 0 )
	{
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	else if( pWnd ) pWnd->EnableWindow( TRUE );

	pos = m_pListH->FindIndex( m_nIdx );
	if( !pos ) return;
	CString szHdr = m_pListH->GetAt( pos );
	m_fHdr = szHdr != _T("");
	pos = m_pListQ->FindIndex( m_nIdx );
	if( !pos ) return;
	m_szQuestion = m_pListQ->GetAt( pos );
	pos = m_pListA->FindIndex( m_nIdx );
	if( !pos ) return;
	m_szAnswer = m_pListA->GetAt( pos );
	pos = m_pListN->FindIndex( m_nIdx );
	CString szN = m_pListN->GetAt( pos );
	m_fNum = szN != _T("");
	pos = m_pListID->FindIndex( m_nIdx );
	m_szID = m_pListID->GetAt( pos );
	pos = m_pListCH->FindIndex( m_nIdx );
	m_szColHeader = m_pListCH->GetAt( pos );

	UpdateData( FALSE );

	pWnd = GetDlgItem( IDC_BN_NEXTQ );
	if( pWnd ) pWnd->EnableWindow( TRUE );
	pWnd = GetDlgItem( IDC_EDIT_A );
	if( pWnd )
	{
		pWnd->EnableWindow( !m_fHdr );
		pWnd->SetFocus();
	}
}

void SQuestionnaireDlg::OnBnNextq() 
{
	if( !m_pListH || !m_pListQ || !m_pListA ) return;

	POSITION pos = NULL;
	BOOL fDel = FALSE;

	UpdateData ( TRUE );

	if( !IsValidAnswer() )
	{
		BCGPMessageBox( _T("The answer must contain a numeric value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_A );
		if( pWnd ) pWnd->SetFocus();
		m_fCanClose = FALSE;
		return;
	}

	pos = m_pListH->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListH->RemoveAt( pos );
		pos = m_pListH->FindIndex( m_nIdx );
		if( pos ) m_pListH->InsertBefore( pos, m_fHdr ? _T("TRUE") : _T("") );
		else m_pListH->AddTail( m_fHdr ? _T("TRUE") : _T("") );
	}

	pos = m_pListN->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListN->RemoveAt( pos );
		pos = m_pListN->FindIndex( m_nIdx );
		if( pos ) m_pListN->InsertBefore( pos, m_fNum ? _T("TRUE") : _T("") );
		else m_pListN->AddTail( m_fNum ? _T("TRUE") : _T("") );
	}

	pos = m_pListID->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListID->RemoveAt( pos );
		pos = m_pListID->FindIndex( m_nIdx );
		if( pos ) m_pListID->InsertBefore( pos, m_szID );
		else m_pListID->AddTail( m_szID );
	}

	pos = m_pListCH->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListCH->RemoveAt( pos );
		pos = m_pListCH->FindIndex( m_nIdx );
		if( pos ) m_pListCH->InsertBefore( pos, m_szColHeader );
		else m_pListCH->AddTail( m_szColHeader );
	}

	pos = m_pListQ->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListQ->RemoveAt( pos );
		pos = m_pListQ->FindIndex( m_nIdx );
		if( pos ) m_pListQ->InsertBefore( pos, m_szQuestion );
		else m_pListQ->AddTail( m_szQuestion );
	}

	pos = m_pListA->FindIndex( m_nIdx );
	if( pos )
	{
		m_pListA->RemoveAt( pos );
		pos = m_pListA->FindIndex( m_nIdx );
		if( pos ) m_pListA->InsertBefore( pos, m_szAnswer );
		else m_pListA->AddTail( m_szAnswer );
	}

	if( m_fClose ) return;

	m_nIdx++;

	CWnd* pWnd = GetDlgItem( IDC_BN_NEXTQ );
	if( m_nIdx >= ( m_pListH->GetCount() - 1 ) )
	{
		pWnd->EnableWindow( FALSE );
		SetDefID( IDOK );
	}
	else
	{
		pWnd->EnableWindow( TRUE );
		SetDefID( IDC_BN_NEXTQ );
	}

	pos = m_pListH->FindIndex( m_nIdx );
	if( !pos ) return;
	CString szHdr = m_pListH->GetAt( pos );
	m_fHdr = szHdr != _T("");
	pos = m_pListQ->FindIndex( m_nIdx );
	if( !pos ) return;
	m_szQuestion = m_pListQ->GetAt( pos );
	pos = m_pListA->FindIndex( m_nIdx );
	if( !pos ) return;
	m_szAnswer = m_pListA->GetAt( pos );
	pos = m_pListN->FindIndex( m_nIdx );
	CString szN = m_pListN->GetAt( pos );
	m_fNum = szN != _T("");
	pos = m_pListID->FindIndex( m_nIdx );
	m_szID = m_pListID->GetAt( pos );
	pos = m_pListCH->FindIndex( m_nIdx );
	m_szColHeader = m_pListCH->GetAt( pos );

	UpdateData( FALSE );

	pWnd = GetDlgItem( IDC_BN_PREVQ );
	if( pWnd ) pWnd->EnableWindow( TRUE );
	pWnd = GetDlgItem( IDC_EDIT_A );
	if( pWnd )
	{
		pWnd->EnableWindow( !m_fHdr );
		pWnd->SetFocus();
	}
}

BOOL SQuestionnaireDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	m_bnPrev.SetImage( IDB_PREV );
	m_bnNext.SetImage( IDB_NEXT );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_Q );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_Q );
	pWnd = GetDlgItem( IDC_EDIT_A );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_A );
	pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The ID of the question. This item cannot be edited.") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The column header of the question. This item cannot be edited.") );
	pWnd = GetDlgItem( IDC_CHK_USE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("If checked, it indicates that the answer must be numeric. This item cannot be edited.") );
	pWnd = GetDlgItem( IDC_BN_PREVQ );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PREV_Q );
	pWnd = GetDlgItem( IDC_BN_NEXTQ );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_NEXT_Q );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CANCEL );

	CWnd* pWndP = GetDlgItem( IDC_BN_PREVQ );
	CWnd* pWndN = GetDlgItem( IDC_BN_NEXTQ );
	if( m_pListH && m_pListQ && m_pListA && m_pListN && m_pListID && m_pListCH && m_fEdit )
	{
		pWndP->ShowWindow( SW_HIDE );
		pWndP->EnableWindow( FALSE );
		pWndN->ShowWindow( SW_HIDE );
		pWndN->EnableWindow( FALSE );
		SetDefID( IDOK );

		int nCnt = (int)m_pListQ->GetCount();
		if( ( m_nIdx >= 0 ) && ( m_nIdx < nCnt ) )
		{
			// is header/category?
			POSITION pos = m_pListH->FindIndex( m_nIdx );
			CString szHdr = m_pListH->GetAt( pos );
			// numeric
			pos = m_pListN->FindIndex( m_nIdx );
			CString szN = m_pListN->GetAt( pos );
			// question
			pos = m_pListQ->FindIndex( m_nIdx );
			m_szQuestion = m_pListQ->GetAt( pos );
			// id
			pos = m_pListID->FindIndex( m_nIdx );
			m_szID = m_pListID->GetAt( pos );
			// column header
			pos = m_pListCH->FindIndex( m_nIdx );
			m_szColHeader = m_pListCH->GetAt( pos );
			// handle if header
			if( szHdr == _T("") )
			{
				pos = m_pListA->FindIndex( m_nIdx );
				m_szAnswer = m_pListA->GetAt( pos );
				m_fHdr = FALSE;
			}
			else m_fHdr = TRUE;
			// if number
			if( szN == _T("") ) m_fNum = FALSE;
			else m_fNum = TRUE;

			UpdateData( FALSE );
		}
	}

	pWnd = GetDlgItem( IDOK );
	if( !m_fEdit )
	{
		pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDCANCEL );
		pWnd->SetWindowText( _T("Done") );

		POSITION pos = m_pListQ->FindIndex( m_nIdx );
		if( pos ) m_szQuestion = m_pListQ->GetAt( pos );

		pos = m_pListA->FindIndex( m_nIdx );
		if( pos ) m_szAnswer = m_pListA->GetAt( pos );

		CString szHdr;
		pos = m_pListH->FindIndex( m_nIdx );
		if( pos ) szHdr = m_pListH->GetAt( pos );
		if( szHdr != _T("") )
		{
			pWnd = GetDlgItem( IDC_EDIT_A );
			if( pWnd ) pWnd->EnableWindow( FALSE );
			m_fHdr = TRUE;
		}

		CString szN;
		pos = m_pListN->FindIndex( m_nIdx );
		if( pos ) szN = m_pListN->GetAt( pos );
		if( szN != _T("") ) m_fNum = TRUE;
		else m_fNum = FALSE;

		pos = m_pListID->FindIndex( m_nIdx );
		m_szID = m_pListID->GetAt( pos );

		pos = m_pListCH->FindIndex( m_nIdx );
		m_szColHeader = m_pListCH->GetAt( pos );

		UpdateData( FALSE );
	}

	pWnd = GetDlgItem( IDC_EDIT_A );
	if( pWnd ) pWnd->SetFocus();

	if( m_nIdx <= 0 ) pWndP->EnableWindow( FALSE );
	if( m_nIdx >= ( m_pListQ->GetCount() - 1 ) ) pWndN->EnableWindow( FALSE );

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL SQuestionnaireDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void SQuestionnaireDlg::OnOK()
{
	m_fCanClose = TRUE;
	m_fClose = TRUE;
	OnBnNextq();
	if( m_fCanClose ) CBCGPDialog::OnOK();
}

void SQuestionnaireDlg::OnCancel()
{
	m_fCanClose = TRUE;
	m_fClose = TRUE;
	if( !m_fEdit ) OnBnNextq();
	if( m_fCanClose ) CBCGPDialog::OnCancel();
}

BOOL SQuestionnaireDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("questionnaire.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

BOOL SQuestionnaireDlg::IsValidAnswer()
{
	// if this is non-numeric, we don't care
	if( !m_fNum ) return TRUE;
	// if answer is empty, we don't care
	if( m_szAnswer == _T("") ) return TRUE;

	// if numeric, verify that it is completely numeric
	double dVal = 0.;
	int nVal = 0;
	// since the conversion functions return 0 or 0.0 when they fail,
	// ...if the answer has these values, it's ok
	if( ( m_szAnswer == _T("0") ) || ( m_szAnswer == _T("0.") ) || ( m_szAnswer == _T("0.0") ) )
		return TRUE;
	// if there's a decimal in the string, we will do double conversion, otherwise int
	if( m_szAnswer.Find( _T(".") ) != -1 )
	{
		dVal = _tcstod( m_szAnswer, NULL );
		if( dVal == 0. ) return FALSE;
	}
	else
	{
		nVal = atoi( m_szAnswer );
		if( nVal == 0 ) return FALSE;
	}

	return TRUE;
}

void SQuestionnaireDlg::OnCheckNumeric()
{
	UpdateData( FALSE );
}

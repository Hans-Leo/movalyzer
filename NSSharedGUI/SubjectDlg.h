// SubjectDlg.h - Header file
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// SubjectDlg dialog

class AFX_EXT_CLASS SubjectDlg : public CBCGPDialog
{
// Construction
public:
	SubjectDlg(CWnd* pParent = NULL, BOOL fNew = FALSE);   // standard constructor

// Dialog Data
	enum { IDD = IDD_SUBJECT };
	CString			m_szExpID;
	CString			m_szGrpID;
	CString			m_szID;
	CString			m_szCode;
	CString			m_szFirst;
	CString			m_szLast;
	CString			m_szNotes;
	CString			m_szPrvNotes;
	BOOL			m_fInit;
	BOOL			m_fNew;
	CStringList		m_lstIDs;
	CStringList		m_lstCodes;
	COleDateTime	m_DateAdded;
	CDateTimeCtrl	m_ctrlDateTime;
	BOOL			m_fActive;
	CComboBox		m_cboDefExp;
	CString			m_szDefExp;
	short			m_nExpCount;
	double			m_dDevRes;
	int				m_nSampRate;
	CBCGPButton		m_linkEdit;
	CBCGPButton		m_linkEdit2;
	BOOL			m_fEditDRSR;
	NSToolTipCtrl	m_tooltip;
	CPrivateEdit	m_editLast;
	CPrivateEdit	m_editFirst;
	CPrivateEdit	m_editPrvNotes;
	short			m_nEncryptionMethod;
	CString			m_szSiteID;
	CString			m_szSiteDesc;
	CString			m_szSignature;
	CString			m_szPassword;
	BOOL			m_fEditCode;
	CFont			m_Font;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	BOOL CheckID( BOOL fMsg = TRUE);
	BOOL CheckCode( BOOL fMsg = TRUE );
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusEditId();
	afx_msg void OnKillfocusEditSiteId();
	afx_msg void OnKillfocusEditCode();
	afx_msg void OnChangeCode();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickNotes();
	afx_msg void OnClickEdit();
	afx_msg void OnClickEdit2();
	afx_msg void OnClickPassword();
	afx_msg void OnClickRecover();
	DECLARE_MESSAGE_MAP()
};

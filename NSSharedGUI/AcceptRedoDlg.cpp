// AcceptRedoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GraphDlg.h"
#include "..\Common\UserObj.h"
#include "AcceptRedoDlg.h"
#include "..\Common\MessageManager.h"

// AcceptRedoDlg dialog

IMPLEMENT_DYNAMIC(AcceptRedoDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(AcceptRedoDlg, CBCGPDialog)
	ON_BN_CLICKED(ID_BN_VIEW, &AcceptRedoDlg::OnBnClickedBnView)
END_MESSAGE_MAP()

AcceptRedoDlg::AcceptRedoDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(AcceptRedoDlg::IDD, pParent)
{
	m_dFF = 0.;
	m_fNoMsgConsis = FALSE;
}

void AcceptRedoDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
}

BOOL AcceptRedoDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );
	if( m_fNoMsgConsis )
	{
		SetWindowText( _T("Trial Accept/Redo") );
		CWnd* pWnd = GetDlgItem( IDC_CHK_CONSIS );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	}
	return TRUE;
}
// AcceptRedoDlg message handlers

void AcceptRedoDlg::OnBnClickedBnView()
{

	CGraphDlg gd;
	if( m_szTF == _T("") )
	{
		UserObj* pObj = ::GetCurrentUserObj();
		if( !pObj ) return;
		gd.ChartTf( pObj->m_szExpID, pObj->m_szGrpID, pObj->m_szSubjID, m_szTF,
			m_dFF, ::GetMinPenPressure(), FALSE );
	}
	else gd.ChartTf( m_szTF, m_dFF, m_szSeg, FALSE, FALSE, TRUE, this );
}

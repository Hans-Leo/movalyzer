// Globals.cpp : Defines the class behaviors for the application.
//

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include <direct.h>
#include <mmsystem.h>
#include <shlwapi.h>
#pragma comment(lib, "winmm")
#include "PasswordDlg.h"
#include "LoginDlg.h"
#include "Globals.h"
#include "Users.h"
#include "Experiments.h"
#include "Groups.h"
#include "Subjects.h"
#include "Conditions.h"
#include "Stimuli.h"
#include "Elements.h"
#include "Cats.h"
#include "Feedbacks.h"
#include "Ver.h"
#include "..\DataMod\DataMod.h"
#include "..\Common\Security.h"
#include "..\Common\Shared.h"
#include "..\Common\ModVer.h"
#include "..\InputMod\InputMod.h"
#include "..\InputMod\InputMod_i.c"
#include "..\ProcessMod\ProcessMod.h"
#define	MSWIN
#define	MSWIN32
#define	MSWINDLL
#include "..\Comm\comm.h" 

#ifdef	_CHILKAT_
	#include "..\chilkat\include\CkZip.h"
	#include "..\chilkat\include\CkZipEntry.h"
	#include "..\chilkat\include\CkStringArray.h"
	#include "..\chilkat\include\CkByteData.h"
	#include "ZipProgress.h"
	#include "..\chilkat\include\CkSettings.h"
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	LEFT_VIEW_SIZE_MAXIMIZE		75
#define MAX_SAMPLING_RATE			2000
#define MAX_DEVICE_RES				100.0
#define MAX_DEVICE_DIM				500.0

// separate thread for mm sound playing
UINT PlayASoundThread( LPVOID pParam );
// separate thread for executing a script
UINT RunAScript( LPVOID pParam );

CString		_szAppPath;
CString		_CurUser;
CString		_CurUserDesc;
CString		_CurUserSiteID;
CString		_CurUserSiteDesc;
CString		_Signature;
BOOL		_DataChanged = FALSE;
BOOL		_TabletModeOn = TRUE;
BOOL		_GripperModeOn = FALSE;
BOOL		_DesktopModeOn = TRUE;
BOOL		_TabletRemap = FALSE;
BOOL		_ContinueWithEnter = FALSE;
BOOL		_TrialAlphaOn = FALSE;
BOOL		_PrivateUserOn = FALSE;
CString		_Delimiter = _T(":");
CString		_Path;
CString		_Backup;
CString		_CommPort;
BOOL		_AutoUpdate = TRUE;
BOOL		_DetailedOutput = FALSE;
BOOL		_Verbose = FALSE;
BOOL		_LogUI = TRUE;
BOOL		_LogDB = TRUE;
BOOL		_LogProc = TRUE;
BOOL		_LogGraph = TRUE;
BOOL		_LogTablet = TRUE;
BOOL		_HelpOpen = FALSE;
CWnd*		_HelpWindow = NULL;
int			_HelpWindowCnt = 0;
CString		_Chart;
GripperSettings	_GripperSettings;
BOOL		_Privacy = TRUE;
CString		_Pass;
BOOL		_PassSet = FALSE;
DWORD		_BkColor = RGB(255,255,255);
// tablet & display dimensions
double		_T_X = 8. * 2.54;
double		_T_Y = 6. * 2.54;
double		_D_X = 16. * 2.54;
double		_D_Y = 12. * 2.54;
// client-server
CString		_szCSServer;
CString		_szCSUser;
CString		_szCSPassword;
BOOL		_CSModeOn = FALSE;
// serial communications
BOOL		_PortOpen = FALSE;
int			_Port = -1;
// subordinate window sizes during maximization
int			_LVMaxSize = LEFT_VIEW_SIZE_MAXIMIZE;
// drawing options
BOOL		_fLineFixed = TRUE;
short		_nLineThickness = 2;
BOOL		_fLineByPressure = FALSE;
DWORD		_dwLineColor1 = RGB( 0, 0, 255 );
DWORD		_dwLineColor2 = RGB( 0, 255, 0 );
DWORD		_dwLineColor3 = RGB( 255, 0, 0 );
DWORD		_dwEllipseColor = RGB( 255, 0, 0 );
DWORD		_dwMPPColor = ::GetSoftColor();
short		_nEllipseSize = 2;
BOOL		_fDeviceSetupRun = FALSE;
// security/purchase vars
// ..demo
BOOL		_IsD = FALSE;
BOOL		_IsDSet = FALSE;
// ..scriptalyzer
BOOL		_ScriptActivated = FALSE;
// ..gripalyzer
BOOL		_GripperActivated = FALSE;
// ..optilyzer/movalyzer
BOOL		_OptiActivated = FALSE;
// ...rx
BOOL		_RxActivated = FALSE;
// ..client/server
BOOL		_CSActivated = FALSE;
// ..writalyzer
BOOL		_WritActivated = FALSE;
// ..PPU scriptalyzer
BOOL		_fSPPU	= FALSE;
long		_nSPPU	= 0;
// ..PPU gripalyzer
BOOL		_fGPPU	= FALSE;
long		_nGPPU	= 0;
// ..PPU opti/movalyzer
BOOL		_fOPPU	= FALSE;
long		_nOPPU	= 0;
// Multimedia
char		_soundfile[ _MAX_PATH ] = "";
// Last Experiment Run
CString		_szLastExpID;
CString		_szLastGrpID;
CString		_szLastSubjID;

// GUIDs for typelibs for each module
const GUID guidAM	// adminmod
= { 0xE1395D98L, 0x5A10, 0x415C,
{ 0xB8, 0x62, 0xEA, 0x79, 0xD2, 0xE7, 0xA7, 0x4F } };
const GUID guidDM	// datamod
= { 0xDCB05EC3L, 0xA758, 0x11D3,
{ 0x8A, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
const GUID guidIM	// inputmod
= { 0x93FCD941L, 0x9BB1, 0x11D3,
{ 0x8A, 0x30, 0x00, 0x10, 0x4B, 0xC7, 0xE2, 0xC8 } };
const GUID guidPM	// processmod
= { 0xDAF87EEEL, 0x81A4, 0x11D3,
{ 0x8A, 0x20, 0x00, 0x10, 0x4B, 0xC7, 0xE2, 0xC8 } };
const GUID guidSM	// showmod
= { 0xE8B28D93L, 0x96E8, 0x11D3,
{ 0x8A, 0x27, 0x00, 0x10, 0x4B, 0xC7, 0xE2, 0xC8 } };


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    SetIsD
// FullName:  SetIsD
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsD( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_IsD = val;
	_IsDSet = TRUE;
}

//************************************
// Method:    IsD
// FullName:  IsD
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: int * nDays
//************************************
BOOL IsD( int* nDays )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	long yr = 0, mo = 0, day = 0, hr = 0, min = 0;
	NSSecurity* pSecurity = ::GetSecurity();
	if( !pSecurity ) return FALSE;
	if( ( nDays || !_IsDSet ) && pSecurity->DemoStatus( yr, mo, day, hr, min ) )
	{
		// compute days
		COleDateTime dtNow = COleDateTime::GetCurrentTime();
		COleDateTime dtExp;
		dtExp.SetDate( yr, mo, day );

		COleDateTimeSpan dts = dtExp - dtNow;
		int nDaysLeft = (int)dts.GetTotalDays();
		if( nDays ) *nDays = nDaysLeft;

		return( nDaysLeft >= 0 );
	}

	return _IsD;
#else
	return FALSE;
#endif
}

//************************************
// Method:    SetIsSA
// FullName:  SetIsSA
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsSA( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_ScriptActivated = val;
}

//************************************
// Method:    IsSA
// FullName:  IsSA
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fConsiderDemo
//************************************
BOOL IsSA( BOOL fConsiderDemo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	return ( ( !fConsiderDemo ? FALSE : IsD() ) || _ScriptActivated );
#else
	return TRUE;
#endif
}

//************************************
// Method:    SetIsGA
// FullName:  SetIsGA
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsGA( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_GripperActivated = val;
}

//************************************
// Method:    IsGA
// FullName:  IsGA
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fConsiderDemo
//************************************
BOOL IsGA( BOOL fConsiderDemo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	return ( ( !fConsiderDemo ? FALSE : IsD() ) || _GripperActivated );
#else
	return TRUE;
#endif
}

//************************************
// Method:    SetIsOA
// FullName:  SetIsOA
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsOA( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_OptiActivated = val;
}

//************************************
// Method:    IsOA
// FullName:  IsOA
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fConsiderDemo
//************************************
BOOL IsOA( BOOL fConsiderDemo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	return ( ( !fConsiderDemo ? FALSE : IsD() ) || _OptiActivated );
#else
	return TRUE;
#endif
}

//************************************
// Method:    SetIsRxA
// FullName:  SetIsRxA
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsRxA( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_RxActivated = val;
}

//************************************
// Method:    IsRxA
// FullName:  IsRxA
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fConsiderDemo
//************************************
BOOL IsRxA( BOOL fConsiderDemo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	return ( ( !fConsiderDemo ? FALSE : IsD() ) || _RxActivated || _OptiActivated );
#else
	return TRUE;
#endif
}

//************************************
// Method:    SetIsCS
// FullName:  SetIsCS
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsCS( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CSActivated = val;
}

//************************************
// Method:    IsCS
// FullName:  IsCS
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fConsiderDemo
//************************************
BOOL IsCS( BOOL fConsiderDemo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	return ( ( !fConsiderDemo ? FALSE : IsD() ) || _CSActivated );
	// TODO LATER: We are going to always allow the C/S piece to be useable
	// unless the demo expires and no products have been purchased
//	if( IsSA( TRUE ) || IsGA( TRUE ) || IsOA( TRUE ) ) return TRUE;
//	else return FALSE;
#else
	return TRUE;
#endif
}

// Writalyzer
//************************************
// Method:    SetIsWA
// FullName:  SetIsWA
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
//************************************
void SetIsWA( BOOL val )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_WritActivated = val;
}

//************************************
// Method:    IsWA
// FullName:  IsWA
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fConsiderDemo
//************************************
BOOL IsWA( BOOL fConsiderDemo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	return ( ( !fConsiderDemo ? FALSE : IsD() ) || _WritActivated );
#else
	return TRUE;
#endif
}

/////////////////////////////////
// PAY-PER-USE
/////////////////////////////////
//************************************
// Method:    SetIsSPPU
// FullName:  SetIsSPPU
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
// Parameter: long cnt
//************************************
void SetIsSPPU( BOOL val, long cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if( cnt > 0 ) _fSPPU = val;
	else _fSPPU = FALSE;
	_nSPPU = cnt;
}

//************************************
// Method:    IsSPPU
// FullName:  IsSPPU
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: long & cnt
//************************************
BOOL IsSPPU( long& cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	cnt = _nSPPU;
	return ( _nSPPU > 0 ) ? _fSPPU : FALSE;
#else
	cnt = 0;
	return FALSE;
#endif
}

//************************************
// Method:    SetIsGPPU
// FullName:  SetIsGPPU
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
// Parameter: long cnt
//************************************
void SetIsGPPU( BOOL val, long cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if( cnt > 0 ) _fGPPU = val;
	else _fGPPU = FALSE;
	_nGPPU = cnt;
}

//************************************
// Method:    IsGPPU
// FullName:  IsGPPU
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: long & cnt
//************************************
BOOL IsGPPU( long& cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	cnt = _nGPPU;
	return ( _nSPPU > 0 ) ? _fGPPU : FALSE;
#else
	cnt = 0;
	return FALSE;
#endif
}

//************************************
// Method:    SetIsOPPU
// FullName:  SetIsOPPU
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL val
// Parameter: long cnt
//************************************
void SetIsOPPU( BOOL val, long cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if( cnt > 0 ) _fOPPU = val;
	else _fOPPU = FALSE;
	_nOPPU = cnt;
}

//************************************
// Method:    IsOPPU
// FullName:  IsOPPU
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: long & cnt
//************************************
BOOL IsOPPU( long& cnt )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
#ifdef	_SECURITY_
	cnt = _nOPPU;
	return ( _nOPPU > 0 ) ? _fOPPU : FALSE;
#else
	cnt = 0;
	return FALSE;
#endif
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ConfirmModuleVersions
// FullName:  ConfirmModuleVersions
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: UINT nMods
// Parameter: CString & szMsg
//************************************
BOOL ConfirmModuleVersions( UINT nMods, CString& szMsg )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	HRESULT		hr = E_FAIL;
	ITypeLib*	pTLib = NULL;
	int			nMajor = 0, nMinor = 0;
	double		dVer = 0., dAdjust = 0.01, nMinorF=0.00;
	CString		szTmp;
	BOOL		fRet = TRUE;
	
	if( nMods & VER_DATAMOD )
	{
		dVer = DATAMOD_VER;
		nMajor = (int)dVer;
		nMinor = (int)( ( dVer - nMajor + dAdjust ) * 10 );
		hr = LoadRegTypeLib( guidDM, nMajor, nMinor, 0x409, &pTLib );
		if( FAILED( hr ) )
		{
			szTmp.Format( _T("DATAMOD - %.1f ###### hr=[%x]\n"), dVer, hr);
			szMsg += szTmp;
			fRet = FALSE;
		}
		else pTLib->Release();
	}
	if( nMods & VER_INPUTMOD )
	{
		dVer = INPUTMOD_VER;
		nMajor = (int)dVer;
		nMinor = (int)( ( dVer - nMajor + dAdjust ) * 10 );
		hr = LoadRegTypeLib( guidIM, nMajor, nMinor, 0x409, &pTLib );
		if( FAILED( hr ) )
		{
			szTmp.Format( _T("INPUTMOD - %.1f ###### hr=[%x]\n"), dVer, hr);
			szMsg += szTmp;
			fRet = FALSE;
		}
		else pTLib->Release();
	}
	if( nMods & VER_PROCMOD )
	{
		dVer = PROCMOD_VER;
		nMajor = (int)dVer;
		nMinor = (int)( ( dVer - nMajor + dAdjust ) * 10 );
		hr = LoadRegTypeLib( guidPM, nMajor, nMinor, 0x409, &pTLib );
		if( FAILED( hr ) )
		{
			szTmp.Format( _T("PROCMOD - %.1f ###### hr=[%x]\n"), dVer, hr);
			szMsg += szTmp;
			fRet = FALSE;
		}
		else pTLib->Release();
	}
	if( nMods & VER_SHOWMOD )
	{
		dVer = SHOWMOD_VER;
		nMajor = (int)dVer;
		nMinor = (int)( ( dVer - nMajor + dAdjust ) * 10 );
		nMinorF = ( dVer - nMajor + dAdjust ) * 10;
		hr = LoadRegTypeLib( guidSM, nMajor, nMinor, 0x409, &pTLib );
		if( FAILED( hr ) )
		{
			szTmp.Format( _T("SHOWMOD - %.1f ###### hr=[%x]\n"), dVer, hr);
			szMsg += szTmp;

			
			fRet = FALSE;

		}
		else pTLib->Release();
	}
	if( nMods & VER_ADMINMOD )
	{
		dVer = ADMINMOD_VER;
		nMajor = (int)dVer;
		nMinor = (int)( ( dVer - nMajor + dAdjust ) * 10 );
		hr = LoadRegTypeLib( guidAM, nMajor, nMinor, 0x409, &pTLib );
		if( FAILED( hr ) )
		{
			szTmp.Format( _T("ADMINMOD - %.1f ###### hr=[%x]\n"), dVer, hr);
			szMsg += szTmp;
			fRet = FALSE;
		}
		else pTLib->Release();
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    GetEncryptionMethod
// FullName:  GetEncryptionMethod
// Access:    public 
// Returns:   UINT
// Qualifier:
//************************************
UINT GetEncryptionMethod()
{
//	return ENCRYPTION_NONE;
//	return ENCRYPTION_DES;
//	return ENCRYPTION_AES;
	return ENCRYPTION_BLOWFISH;
}

//************************************
// Method:    GetWinUser
// FullName:  GetWinUser
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szUser
//************************************
void GetWinUser( CString& szUser )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	char pszUser[ 50 ];
	DWORD dwSize = 50;
	memset( pszUser, 0, 50 );
	::GetUserName( pszUser, &dwSize );
	szUser = pszUser;
}

//************************************
// Method:    SetAppPath
// FullName:  SetAppPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szPath
//************************************
void SetAppPath( CString szPath )
{
	_szAppPath = szPath;
}

//************************************
// Method:    GetAppPath
// FullName:  GetAppPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szAppPath
//************************************
void GetAppPath( CString& szAppPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( _szAppPath != _T("") ) szAppPath = _szAppPath;
	else
	{
		szAppPath = AfxGetApp()->m_pszHelpFilePath;
		int nPos = szAppPath.ReverseFind( '\\' );
		szAppPath = szAppPath.Left( nPos + 1 );
	}
}

//************************************
// Method:    GetDataPath
// FullName:  GetDataPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPath
// Parameter: BOOL fPrivate
// Parameter: BOOL fIgnorePU
//************************************
void GetDataPath( CString& szPath, BOOL fPrivate, BOOL fIgnorePU )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	char pszFile[ _MAX_PATH ];
	memset( pszFile, 0, _MAX_PATH * sizeof( char ) );

	if( ( !fIgnorePU && ::IsPrivateUserOn() ) || fPrivate )
		::SHGetFolderPath( NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, pszFile );
	else
		::SHGetFolderPath( NULL, CSIDL_COMMON_DOCUMENTS, NULL, SHGFP_TYPE_CURRENT, pszFile );
	szPath = pszFile;
	int nPos = szPath.ReverseFind( '\\' );
	if( nPos != ( szPath.GetLength() - 1 ) )
		szPath += _T("\\");
	szPath += _T("NeuroScript\\");
}

//************************************
// Method:    GetAllUserPath
// FullName:  GetAllUserPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPath
//************************************
void GetAllUserPath( CString& szPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	char pszFile[ _MAX_PATH ];
	memset( pszFile, 0, _MAX_PATH * sizeof( char ) );

	::SHGetFolderPath( NULL, CSIDL_COMMON_DOCUMENTS, NULL, SHGFP_TYPE_CURRENT, pszFile );

	szPath = pszFile;
	int nPos = szPath.ReverseFind( '\\' );
	if( nPos != ( szPath.GetLength() - 1 ) )
		szPath += _T("\\");
	szPath += _T("NeuroScript\\");
}

//************************************
// Method:    GetCommonPath
// FullName:  GetCommonPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPath
//************************************
void GetCommonPath( CString& szPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// Shared dir files
	HKEY hKey = HKEY_LOCAL_MACHINE;		// Parent key descriptor
	CString strKey = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion");	// Our key full name
	CRegKey rKey;
	if( rKey.Open( hKey, strKey, KEY_READ ) == ERROR_SUCCESS )
	{
		ULONG uLen;
		LPTSTR lpStr;
		CString strValue;
		if( rKey.QueryStringValue( _T("CommonFilesDir"), NULL, &uLen ) == ERROR_SUCCESS )
		{
			lpStr = strValue.GetBuffer( uLen + 1 );
			rKey.QueryStringValue( _T("CommonFilesDir"), lpStr, &uLen );
			strValue.ReleaseBuffer();
		}

		rKey.Close();

		szPath = strValue;
		szPath += _T("\\NeuroScript Shared\\");
	}
}

//************************************
// Method:    SetBkColor
// FullName:  SetBkColor
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: DWORD dwVal
//************************************
void SetBkColor( DWORD dwVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_BkColor = dwVal;
}

//************************************
// Method:    GetBkColor
// FullName:  GetBkColor
// Access:    public 
// Returns:   DWORD
// Qualifier:
//************************************
DWORD GetBkColor()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _BkColor;
}

//************************************
// Method:    IsPrivacyOn
// FullName:  IsPrivacyOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsPrivacyOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _Privacy;
}

//************************************
// Method:    SetIsPrivacyOn
// FullName:  SetIsPrivacyOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fVal
//************************************
void SetIsPrivacyOn( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Privacy = fVal;
}

//************************************
// Method:    OutputAMessage
// FullName:  OutputAMessage
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szMsg
// Parameter: BOOL fOverride
// Parameter: UINT nType
//************************************
void OutputAMessage( CString szMsg, BOOL fOverride, UINT nType )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( szMsg == _T("") ) return;
	if( !fOverride && !::GetDetailedOutput() ) return;
	if( ( nType == LOG_UI ) && !::GetLogUI() ) return;

	BOOL fWasDetailed = ::GetDetailedOutput();
	if( fOverride ) ::SetRapidProcessing( FALSE );
	::OutputMessage( szMsg, nType );
	if( fOverride ) ::SetRapidProcessing( !fWasDetailed );

	if( ::IsLoggingOn() )
	{
		CString szAppPath, szUser;
		::GetDataPath( szAppPath, ::IsPrivateUserOn() );
		::GetCurrentUser( szUser );
		szAppPath += szUser;
		szAppPath += _T("\\Actions.log");
		CFileFind ff;
		if( ff.FindFile( szAppPath ) )
		{
			ff.FindNextFile();
			CString szMsg;
			DWORD dwSize = (long)ff.GetLength();
			if( dwSize > 1000000 )
				szMsg.Format( _T("Message Log: %.2f MB"),
							  (double)( dwSize / 1000000 ) );
			else
				szMsg.Format( _T("Message Log: %.2f KB"),
							  (double)( dwSize / 1000 ) );
		}
	}
}

//************************************
// Method:    OutputAMessage
// FullName:  OutputAMessage
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nResourceID
// Parameter: BOOL fOverride
//************************************
void OutputAMessage( UINT nResourceID, BOOL fOverride )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString szTemp;
	szTemp.LoadString( nResourceID );
	OutputAMessage( szTemp );
}

int GetAppVersion()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	return APP_VERSION;
	return ::AppVersion();
}

//************************************
// Method:    SetCurrentUser
// FullName:  SetCurrentUser
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szUser
//************************************
void SetCurrentUser( CString szUser )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CurUser = szUser;
}

//************************************
// Method:    GetCurrentUser
// FullName:  GetCurrentUser
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szUser
//************************************
void GetCurrentUser( CString& szUser )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szUser = _CurUser;
}

//************************************
// Method:    SetCurrentUserDesc
// FullName:  SetCurrentUserDesc
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szDesc
//************************************
void SetCurrentUserDesc( CString szDesc )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CurUserDesc = szDesc;
}

//************************************
// Method:    GetCurrentUserDesc
// FullName:  GetCurrentUserDesc
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szDesc
//************************************
void GetCurrentUserDesc( CString& szDesc )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szDesc = _CurUserDesc;
}

//************************************
// Method:    DataHasChanged
// FullName:  DataHasChanged
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataHasChanged()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_DataChanged = TRUE;
}

//************************************
// Method:    DataHasNotChanged
// FullName:  DataHasNotChanged
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DataHasNotChanged()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_DataChanged = FALSE;
}

//************************************
// Method:    HasDataChanged
// FullName:  HasDataChanged
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL HasDataChanged()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _DataChanged;
}

//************************************
// Method:    SetCommPort
// FullName:  SetCommPort
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szPort
//************************************
void SetCommPort( CString szPort )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CommPort = szPort;
}

//************************************
// Method:    GetCommPort
// FullName:  GetCommPort
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPort
//************************************
void GetCommPort( CString& szPort )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szPort = _CommPort;
}

void SetAutoUpdateOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_AutoUpdate = fOn;
}

//************************************
// Method:    IsAutoUpdateOn
// FullName:  IsAutoUpdateOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsAutoUpdateOn()
{
	return FALSE;
//	return _AutoUpdate;
}

//************************************
// Method:    SetTabletModeOn
// FullName:  SetTabletModeOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetTabletModeOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_TabletModeOn = fOn;
}

//************************************
// Method:    IsTabletModeOn
// FullName:  IsTabletModeOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsTabletModeOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _TabletModeOn;
}

//************************************
// Method:    SetGripperModeOn
// FullName:  SetGripperModeOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetGripperModeOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_GripperModeOn = fOn;
	if( fOn ) _TabletModeOn = FALSE;
}

//************************************
// Method:    IsGripperModeOn
// FullName:  IsGripperModeOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsGripperModeOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _GripperModeOn;
}

//************************************
// Method:    SetDesktopModeOn
// FullName:  SetDesktopModeOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetDesktopModeOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_DesktopModeOn = fOn;
}

//************************************
// Method:    IsDesktopModeOn
// FullName:  IsDesktopModeOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsDesktopModeOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _DesktopModeOn;
}

//************************************
// Method:    SetTabletRemapOn
// FullName:  SetTabletRemapOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetTabletRemapOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_TabletRemap = fOn;
}

//************************************
// Method:    IsTabletRemapOn
// FullName:  IsTabletRemapOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsTabletRemapOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _TabletRemap;
}

//************************************
// Method:    SetContinueWithEnter
// FullName:  SetContinueWithEnter
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetContinueWithEnter( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_ContinueWithEnter = fOn;
}

//************************************
// Method:    IsContinueWithEnter
// FullName:  IsContinueWithEnter
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsContinueWithEnter()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _ContinueWithEnter;
}

//************************************
// Method:    SetTrialAlphaOn
// FullName:  SetTrialAlphaOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetTrialAlphaOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_TrialAlphaOn = fOn;
}

//************************************
// Method:    IsTrialAlphaOn
// FullName:  IsTrialAlphaOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsTrialAlphaOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _TrialAlphaOn;
}

//************************************
// Method:    SetPrivateUserOn
// FullName:  SetPrivateUserOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetPrivateUserOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_PrivateUserOn = fOn;
}

//************************************
// Method:    IsPrivateUserOn
// FullName:  IsPrivateUserOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsPrivateUserOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _PrivateUserOn;
}

//************************************
// Method:    SetDelimiter
// FullName:  SetDelimiter
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szDelim
//************************************
void SetDelimiter( CString szDelim )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Delimiter = szDelim != _T("") ? szDelim : _T(":");
}

//************************************
// Method:    GetDelimiter
// FullName:  GetDelimiter
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szDelim
//************************************
void GetDelimiter( CString& szDelim )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szDelim = _T(" ");
	szDelim += _Delimiter;
	szDelim += _T(" ");
}

//************************************
// Method:    GetDelimiter
// FullName:  GetDelimiter
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetDelimiter()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _Delimiter;
}

//************************************
// Method:    SetDataPathRoot
// FullName:  SetDataPathRoot
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szPath
//************************************
void SetDataPathRoot( CString szPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Path = szPath;
}

//************************************
// Method:    GetDataPathRoot
// FullName:  GetDataPathRoot
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPath
//************************************
void GetDataPathRoot( CString& szPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szPath = _Path;
}

//************************************
// Method:    SetBackupPath
// FullName:  SetBackupPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szPath
//************************************
void SetBackupPath( CString szPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Backup = szPath;
}

//************************************
// Method:    GetBackupPath
// FullName:  GetBackupPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPath
//************************************
void GetBackupPath( CString& szPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szPath = _Backup;
}

//************************************
// Method:    SetDetailedOutput
// FullName:  SetDetailedOutput
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetDetailedOutput( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_DetailedOutput = fOn;
}

//************************************
// Method:    GetDetailedOutput
// FullName:  GetDetailedOutput
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetDetailedOutput()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _DetailedOutput;
}

//************************************
// Method:    SetVerbosity
// FullName:  SetVerbosity
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetVerbosity( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Verbose = fOn;
}

//************************************
// Method:    GetVerbosity
// FullName:  GetVerbosity
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetVerbosity()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	return _Verbose;
	return FALSE;
}

//************************************
// Method:    SetLogUI
// FullName:  SetLogUI
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetLogUI( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_LogUI = fOn;
	::SetUILogOn( fOn && ::IsLoggingOn() );
}

//************************************
// Method:    GetLogUI
// FullName:  GetLogUI
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetLogUI()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _LogUI;
}

//************************************
// Method:    SetLogDB
// FullName:  SetLogDB
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetLogDB( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_LogDB = fOn;
	::SetDBLogOn( fOn && ::IsLoggingOn() );
}

//************************************
// Method:    GetLogDB
// FullName:  GetLogDB
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetLogDB()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _LogDB;
}

//************************************
// Method:    SetLogProc
// FullName:  SetLogProc
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetLogProc( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_LogProc = fOn;
	::SetProcLogOn( fOn && ::IsLoggingOn() );
}

//************************************
// Method:    GetLogProc
// FullName:  GetLogProc
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetLogProc()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _LogProc;
}

//************************************
// Method:    SetLogGraph
// FullName:  SetLogGraph
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetLogGraph( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_LogGraph = fOn;
	::SetGraphLogOn( fOn && ::IsLoggingOn() );
}

//************************************
// Method:    GetLogGraph
// FullName:  GetLogGraph
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetLogGraph()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _LogGraph;
}

//************************************
// Method:    SetLogTablet
// FullName:  SetLogTablet
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetLogTablet( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_LogTablet = fOn;
	::SetTabletLogOn( fOn && ::IsLoggingOn() );
}

//************************************
// Method:    GetLogTablet
// FullName:  GetLogTablet
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetLogTablet()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _LogTablet;
}

//************************************
// Method:    SetHelpOpen
// FullName:  SetHelpOpen
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOpen
//************************************
void SetHelpOpen( BOOL fOpen )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_HelpOpen = fOpen;
	if( !_HelpOpen ) _HelpWindow = NULL;
}

//************************************
// Method:    GetHelpOpen
// FullName:  GetHelpOpen
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL GetHelpOpen()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _HelpOpen;
}

//************************************
// Method:    SetHelpWindow
// FullName:  SetHelpWindow
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pWndHelp
//************************************
void SetHelpWindow( CWnd* pWndHelp )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_HelpWindow = pWndHelp;
}

//************************************
// Method:    GetHelpWindow
// FullName:  GetHelpWindow
// Access:    public 
// Returns:   CWnd*
// Qualifier:
//************************************
CWnd* GetHelpWindow()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _HelpWindow;
}

//************************************
// Method:    AddHelpWindow
// FullName:  AddHelpWindow
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void AddHelpWindow()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_HelpWindowCnt++;
	_HelpOpen = TRUE;
}

//************************************
// Method:    RemoveHelpWindow
// FullName:  RemoveHelpWindow
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void RemoveHelpWindow()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_HelpWindowCnt--;
	if( ( _HelpWindowCnt == 0 ) && _HelpWindow )
	{
		_HelpWindow = NULL;
		_HelpOpen = FALSE;
	}
}

//************************************
// Method:    SetUserPassword
// FullName:  SetUserPassword
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szVal
//************************************
void SetUserPassword( CString szVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Pass = szVal;
	SetHasPassBeenSet( FALSE );
	SetIsPrivacyOn( TRUE );
}

//************************************
// Method:    GetUserPassword
// FullName:  GetUserPassword
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szVal
//************************************
void GetUserPassword( CString& szVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szVal = _Pass;
}

//************************************
// Method:    GetUserPassword
// FullName:  GetUserPassword
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetUserPassword()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _Pass;
}

//************************************
// Method:    SetHasPassBeenSet
// FullName:  SetHasPassBeenSet
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fVal
//************************************
void SetHasPassBeenSet( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_PassSet = fVal;
}

//************************************
// Method:    HasPassBeenSet
// FullName:  HasPassBeenSet
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL HasPassBeenSet()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
//	return _PassSet;
	return FALSE;
}

//************************************
// Method:    SetCurrentUserSiteID
// FullName:  SetCurrentUserSiteID
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szVal
//************************************
void SetCurrentUserSiteID( CString szVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CurUserSiteID = szVal;
}

//************************************
// Method:    GetCurrentUserSiteID
// FullName:  GetCurrentUserSiteID
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetCurrentUserSiteID()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _CurUserSiteID;
}

//************************************
// Method:    SetCurrentUserSiteDesc
// FullName:  SetCurrentUserSiteDesc
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szVal
//************************************
void SetCurrentUserSiteDesc( CString szVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CurUserSiteDesc = szVal;
}

//************************************
// Method:    GetCurrentUserSiteDesc
// FullName:  GetCurrentUserSiteDesc
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetCurrentUserSiteDesc()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _CurUserSiteDesc;
}

//************************************
// Method:    SetSignature
// FullName:  SetSignature
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szVal
//************************************
void SetSignature( CString szVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Signature = szVal;
}

//************************************
// Method:    GetSignature
// FullName:  GetSignature
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetSignature()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _Signature;
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    CreateDirectory
// FullName:  CreateDirectory
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString pFolder
//************************************
BOOL CreateDirectory( CString pFolder )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// if exists return true anyway
	TCHAR szFolder[ MAX_PATH ];
	_tcscpy_s( szFolder, MAX_PATH, pFolder );
	TCHAR *pStart = szFolder;
	TCHAR *pEnd = pStart + _tcslen( szFolder );
	TCHAR *p = pEnd;

	// Try 15 times to create the directory (15 is about as deep as this should get)
	for( int i = 0; i < 10; i++ )
	{
		BOOL bOK = CreateDirectory( szFolder, NULL );
		DWORD dwLastError = GetLastError();
		if( !bOK && ( dwLastError == ERROR_PATH_NOT_FOUND ) )
		{
			while( *p != '\\' )
			{
				if( p == pStart ) return FALSE;
				p--;
			}
			*p = NULL;
		}
		else if( bOK || ( ERROR_ALREADY_EXISTS == dwLastError ) )
		{
			if( p == pEnd ) return TRUE;

			*p = '\\';
			while( *p ) p++;
		}
		else break;
	}

	return FALSE;
}

//************************************
// Method:    VerifyDirectory
// FullName:  VerifyDirectory
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString * szFullPath
//************************************
BOOL VerifyDirectory( CString szExp, CString szGrp, CString szSubj, CString* szFullPath )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath, szDir;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	int nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	szPath += _T("\\");
	szPath += szExp;
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	szPath += _T("\\");
	szPath += szGrp;
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	szPath += _T("\\");
	szPath += szSubj;
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	if( szFullPath ) *szFullPath = szPath;

	return( TRUE );
}

//************************************
// Method:    VerifyDirectory
// FullName:  VerifyDirectory
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
//************************************
BOOL VerifyDirectory( CString szExp, CString szGrp )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath, szDir;
	::GetDataPathRoot( szPath );

	int nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	szPath += _T("\\");
	szPath += szExp;
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	szPath += _T("\\");
	szPath += szGrp;
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	return( TRUE );
}

//************************************
// Method:    VerifyDirectory
// FullName:  VerifyDirectory
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
//************************************
BOOL VerifyDirectory( CString szExp )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath, szDir;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	int nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	szPath += _T("\\");
	szPath += szExp;
	nRes = _mkdir( szPath );
	if( ( nRes != 0 ) && ( errno != EEXIST ) ) return FALSE;

	return( TRUE );
}

//************************************
// Method:    VerifyStringForReserved
// FullName:  VerifyStringForReserved
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szString
// Parameter: CString szDelim
//************************************
BOOL VerifyStringForReserved( CString szString, CString szDelim )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szReserved = RESERVED;
	if( szDelim == _T("") ) ::GetDelimiter( szDelim );
	TRIM( szDelim );

	CString szC;
	for( int i = 0; i < szString.GetLength(); i++ )
	{
		szC = szString.GetAt( i );
		if( ( szReserved.Find( szC ) != -1 ) || ( szDelim == szC ) ) return FALSE;
	}

	return TRUE;
}

//************************************
// Method:    VerifyDatabaseForReserved
// FullName:  VerifyDatabaseForReserved
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szDelim
//************************************
BOOL VerifyDatabaseForReserved( CString szDelim )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	HRESULT hr = S_OK;
	CString szID, szDesc;

	// Experiments
	{
	Experiments db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// Groups
	{
	Groups db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// Subjects
	{
	Subjects db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// NSConditions
	{
	NSConditions db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// Stimuli
	{
	Stimuluss db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// Elements
	{
	Elements db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// Feedback
	{
	Feedbacks db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}
	// Categories
	{
	Cats db;
	hr = db.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		db.GetID( szID );
		db.GetDescription( szDesc );

		if( !::VerifyStringForReserved( szID, szDelim ) ) return FALSE;
		if( !::VerifyStringForReserved( szDesc, szDelim ) ) return FALSE;

		hr = db.GetNext();
	}
	}

	return TRUE;
}

//************************************
// Method:    GetSubjectMask
// FullName:  GetSubjectMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szFile
//************************************
void GetSubjectMask( CString szExp, CString szGrp, CString szSubj, CString& szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s\\%s\\%s%s%s*.*"), szExp, szGrp,
					   szSubj, szExp, szGrp, szSubj );
	else
		szFile.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.*"), szPath, szExp,
					   szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetHWR
// FullName:  GetHWR
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString szIdx
// Parameter: CString & szHWR
//************************************
void GetHWR( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szHWR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );

	if( szPath == _T("") )
		szHWR.Format( _T("%s\\%s\\%s\\%s%s%s%s%s.HWR"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond, szIdx );
	else
		szHWR.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.HWR"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond, szIdx );
}

//************************************
// Method:    GetHWR
// FullName:  GetHWR
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: CString & szHWR
//************************************
void GetHWR( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			 CString& szHWR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szHWR.Format( _T("%s\\%s\\%s\\%s"), szExp, szGrp, szSubj, szTrial );
	else
		szHWR.Format( _T("%s\\%s\\%s\\%s\\%s"), szPath, szExp, szGrp,
					  szSubj, szTrial );
}

//************************************
// Method:    GetPCX
// FullName:  GetPCX
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString szIdx
// Parameter: CString & szPCX
//************************************
void GetPCX( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szPCX )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );

	if( szPath == _T("") )
		szPCX.Format( _T("%s\\%s\\%s\\%s%s%s%s%s.PCX"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond, szIdx );
	else
		szPCX.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.PCX"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond, szIdx );
}

//************************************
// Method:    GetPCX
// FullName:  GetPCX
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: CString & szPCX
//************************************
void GetPCX( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			 CString& szPCX )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szPCX.Format( _T("%s\\%s\\%s\\%s"), szExp, szGrp, szSubj, szTrial );
	else
		szPCX.Format( _T("%s\\%s\\%s\\%s\\%s"), szPath, szExp, szGrp,
					  szSubj, szTrial );
}

//************************************
// Method:    GetHWRMask
// FullName:  GetHWRMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szHWR
//************************************
void GetHWRMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szHWR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szHWR.Format( _T("%s\\%s\\%s\\%s%s%s%s*.HWR"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond );
	else
		szHWR.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s*.HWR"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond );
}

//************************************
// Method:    GetHWRMask
// FullName:  GetHWRMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szHWR
//************************************
void GetHWRMask( CString szExp, CString szGrp, CString szSubj, CString& szHWR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szHWR.Format( _T("%s\\%s\\%s\\%s%s%s*.HWR"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj );
	else
		szHWR.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.HWR"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetPCXMask
// FullName:  GetPCXMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szPCX
//************************************
void GetPCXMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szPCX )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szPCX.Format( _T("%s\\%s\\%s\\%s%s%s%s*.PCX"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond );
	else
		szPCX.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s*.PCX"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond );
}

//************************************
// Method:    GetPCXMask
// FullName:  GetPCXMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szPCX
//************************************
void GetPCXMask( CString szExp, CString szGrp, CString szSubj, CString& szPCX )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szPCX.Format( _T("%s\\%s\\%s\\%s%s%s*.PCX"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj );
	else
		szPCX.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.PCX"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetPCXProcMask
// FullName:  GetPCXProcMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szPCX
//************************************
void GetPCXProcMask( CString szExp, CString szGrp, CString szSubj, CString& szPCX )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szPCX.Format( _T("%s\\%s\\%s\\%s%s%s*-F.PCX"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj );
	else
		szPCX.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*-F.PCX"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetFRD
// FullName:  GetFRD
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString szIdx
// Parameter: CString & szFRD
//************************************
void GetFRD( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szFRD )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFRD.Format( _T("%s\\%s\\%s\\%s%s%s%s%s.FRD"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond, szIdx );
	else
		szFRD.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.FRD"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond, szIdx );
}

//************************************
// Method:    GetFRD
// FullName:  GetFRD
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: CString & szFRD
//************************************
void GetFRD( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			 CString& szFRD )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFRD.Format( _T("%s\\%s\\%s\\%s"), szExp, szGrp, szSubj, szTrial );
	else
		szFRD.Format( _T("%s\\%s\\%s\\%s\\%s"), szPath, szExp, szGrp,
					  szSubj, szTrial );
}

//************************************
// Method:    GetFRDMask
// FullName:  GetFRDMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szFRD
//************************************
void GetFRDMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szFRD )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFRD.Format( _T("%s\\%s\\%s\\%s%s%s%s*.FRD"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond );
	else
		szFRD.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s*.FRD"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond );
}

//************************************
// Method:    GetFRDMask
// FullName:  GetFRDMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szFRD
//************************************
void GetFRDMask( CString szExp, CString szGrp, CString szSubj, CString& szFRD )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFRD.Format( _T("%s\\%s\\%s\\%s%s%s*.FRD"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj );
	else
		szFRD.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.FRD"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetTF
// FullName:  GetTF
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString szIdx
// Parameter: CString & szTF
//************************************
void GetTF( CString szExp, CString szGrp, CString szSubj, CString szCond,
			CString szIdx, CString& szTF )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szTF.Format( _T("%s\\%s\\%s\\%s%s%s%s%s.TF"), szExp, szGrp,
					 szSubj, szExp, szGrp, szSubj, szCond, szIdx );
	else
		szTF.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.TF"), szPath, szExp,
					 szGrp, szSubj, szExp, szGrp, szSubj, szCond, szIdx );
}

//************************************
// Method:    GetTF
// FullName:  GetTF
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: CString & szTF
//************************************
void GetTF( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			CString& szTF )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szTF.Format( _T("%s\\%s\\%s\\%s"), szExp, szGrp, szSubj, szTrial );
	else
		szTF.Format( _T("%s\\%s\\%s\\%s\\%s"), szPath, szExp, szGrp,
					 szSubj, szTrial );
}

//************************************
// Method:    GetTFMask
// FullName:  GetTFMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szTF
//************************************
void GetTFMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				CString& szTF )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szTF.Format( _T("%s\\%s\\%s\\%s%s%s%s*.TF"), szExp, szGrp,
					 szSubj, szExp, szGrp, szSubj, szCond );
	else
		szTF.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s*.TF"), szPath, szExp,
					 szGrp, szSubj, szExp, szGrp, szSubj, szCond );
}

//************************************
// Method:    GetTFMask
// FullName:  GetTFMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szTF
//************************************
void GetTFMask( CString szExp, CString szGrp, CString szSubj, CString& szTF )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szTF.Format( _T("%s\\%s\\%s\\%s%s%s*.TF"), szExp, szGrp,
					 szSubj, szExp, szGrp, szSubj );
	else
		szTF.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.TF"), szPath, szExp,
					 szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetSEG
// FullName:  GetSEG
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString szIdx
// Parameter: CString & szSEG
//************************************
void GetSEG( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szSEG )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szSEG.Format( _T("%s\\%s\\%s\\%s%s%s%s%s.SEG"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond, szIdx );
	else
		szSEG.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.SEG"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond, szIdx );
}

//************************************
// Method:    GetSEGMask
// FullName:  GetSEGMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szSEG
//************************************
void GetSEGMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				CString& szSEG )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szSEG.Format( _T("%s\\%s\\%s\\%s%s%s%s*.SEG"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond );
	else
		szSEG.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s*.SEG"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond );
}

//************************************
// Method:    GetSEGMask
// FullName:  GetSEGMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szSEG
//************************************
void GetSEGMask( CString szExp, CString szGrp, CString szSubj, CString& szSEG )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szSEG.Format( _T("%s\\%s\\%s\\%s%s%s*.SEG"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj );
	else
		szSEG.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.SEG"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj );
}

//************************************
// Method:    GetEXT
// FullName:  GetEXT
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szEXT
//************************************
void GetEXT( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString& szEXT )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szEXT.Format( _T("%s\\%s\\%s\\%s%s%s%s.EXT"), szExp, szGrp,
					  szSubj, szExp, szGrp, szSubj, szCond );
	else
		szEXT.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s.EXT"), szPath, szExp,
					  szGrp, szSubj, szExp, szGrp, szSubj, szCond );
}

//************************************
// Method:    GetSubjEXTMask
// FullName:  GetSubjEXTMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szEXT
//************************************
void GetSubjEXTMask( CString szExp, CString szGrp, CString szSubj, CString& szEXT )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szEXT.Format( _T("%s\\%s\\%s\\%s%s%s*.EXT"), szExp, szGrp, szSubj,
					  szExp, szGrp, szSubj );
	else
		szEXT.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.EXT"), szPath, szExp, szGrp, szSubj,
					  szExp, szGrp, szSubj );
}

//************************************
// Method:    GetCON
// FullName:  GetCON
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szCON
//************************************
void GetCON( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString& szCON )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetEXT( szExp, szGrp, szSubj, szCond, szCON );
	szCON = szCON.Left( szCON.GetLength() - 3 );
	szCON += _T("CON");
}

//************************************
// Method:    GetSubjCONMask
// FullName:  GetSubjCONMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szCON
//************************************
void GetSubjCONMask( CString szExp, CString szGrp, CString szSubj, CString& szCON )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetSubjEXTMask( szExp, szGrp, szSubj, szCON );
	szCON = szCON.Left( szCON.GetLength() - 3 );
	szCON += _T("CON");
}

//************************************
// Method:    GetERR
// FullName:  GetERR
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szERR
//************************************
void GetERR( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString& szERR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetEXT( szExp, szGrp, szSubj, szCond, szERR );
	szERR = szERR.Left( szERR.GetLength() - 3 );
	szERR += _T("ERR");
}

//************************************
// Method:    GetSubjERRMask
// FullName:  GetSubjERRMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szERR
//************************************
void GetSubjERRMask( CString szExp, CString szGrp, CString szSubj, CString& szERR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetSubjEXTMask( szExp, szGrp, szSubj, szERR );
	szERR = szERR.Left( szERR.GetLength() - 3 );
	szERR += _T("ERR");
}

//************************************
// Method:    GetSEN
// FullName:  GetSEN
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szSEN
//************************************
void GetSEN( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szTrial, CString& szSEN )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szSEN.Format( _T("%s\\%s\\%s\\%s%s01.SEN"), szExp, szGrp,
					  szSubj, szCond, szTrial );
	else
		szSEN.Format( _T("%s\\%s\\%s\\%s\\%s%s01.SEN"), szPath, szExp,
					  szGrp, szSubj, szCond, szTrial );
}

//************************************
// Method:    GetSENMask
// FullName:  GetSENMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szCond
// Parameter: CString & szSEN
//************************************
void GetSENMask( CString szExp, CString szGrp, CString szSubj, CString szCond, CString& szSEN )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szSEN.Format( _T("%s\\%s\\%s\\%s*.SEN"), szExp, szGrp,
					  szSubj, szCond );
	else
		szSEN.Format( _T("%s\\%s\\%s\\%s\\%s*.SEN"), szPath, szExp,
					  szGrp, szSubj, szCond );
}

//************************************
// Method:    GetExpNoExt
// FullName:  GetExpNoExt
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szFile
//************************************
void GetExpNoExt( CString szExp, CString& szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s"), szExp, szExp );
	else
		szFile.Format( _T("%s\\%s\\%s"), szPath, szExp, szExp );
}

//************************************
// Method:    GetExpINC
// FullName:  GetExpINC
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szINC
//************************************
void GetExpINC( CString szExp, CString& szINC )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szINC.Format( _T("%s\\%s.INC"), szExp, szExp );
	else
		szINC.Format( _T("%s\\%s\\%s.INC"), szPath, szExp, szExp );
}

//************************************
// Method:    GetExpERR
// FullName:  GetExpERR
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szERR
//************************************
void GetExpERR( CString szExp, CString& szERR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szERR.Format( _T("%s\\%s.ERR"), szExp, szExp );
	else
		szERR.Format( _T("%s\\%s\\%s.ERR"), szPath, szExp, szExp );
}

//************************************
// Method:    GetExpERS
// FullName:  GetExpERS
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szERS
//************************************
void GetExpERS( CString szExp, CString& szERS )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szERS.Format( _T("%s\\%s.ERS"), szExp, szExp );
	else
		szERS.Format( _T("%s\\%s\\%s.ERS"), szPath, szExp, szExp );
}

//************************************
// Method:    GetExpSET
// FullName:  GetExpSET
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szSET
//************************************
void GetExpSET( CString szExp, CString& szSET )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szSET.Format( _T("%s\\%s.SET"), szExp, szExp );
	else
		szSET.Format( _T("%s\\%s\\%s.SET"), szPath, szExp, szExp );
}

//************************************
// Method:    GetExpHWRMask
// FullName:  GetExpHWRMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szHWR
//************************************
void GetExpHWRMask( CString szExp, CString szGrp, CString szSubj, CString& szHWR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szHWR.Format( _T("%s\\%s\\%s\\%s*.HWR"), szExp, szGrp,
					  szSubj, szExp );
	else
		szHWR.Format( _T("%s\\%s\\%s\\%s\\%s*.HWR"), szPath, szExp, szGrp,
					  szSubj, szExp );
}

//************************************
// Method:    GetExpFRDMask
// FullName:  GetExpFRDMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szFRD
//************************************
void GetExpFRDMask( CString szExp, CString szGrp, CString szSubj, CString& szFRD )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFRD.Format( _T("%s\\%s\\%s\\%s*.FRD"), szExp, szGrp,
					  szSubj, szExp );
	else
		szFRD.Format( _T("%s\\%s\\%s\\%s\\%s*.FRD"), szPath, szExp, szGrp,
					  szSubj, szExp );
}

//************************************
// Method:    GetExpTFMask
// FullName:  GetExpTFMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szTF
//************************************
void GetExpTFMask( CString szExp, CString szGrp, CString szSubj, CString& szTF )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szTF );
	szTF = szTF.Left( szTF.GetLength() - 3 );
	szTF += _T("TF");
}

//************************************
// Method:    GetExpSEGMask
// FullName:  GetExpSEGMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szSEG
//************************************
void GetExpSEGMask( CString szExp, CString szGrp, CString szSubj, CString& szSEG )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szSEG );
	szSEG = szSEG.Left( szSEG.GetLength() - 3 );
	szSEG += _T("SEG");
}

//************************************
// Method:    GetExpEXTMask
// FullName:  GetExpEXTMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szEXT
//************************************
void GetExpEXTMask( CString szExp, CString szGrp, CString szSubj, CString& szEXT )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szEXT );
	szEXT = szEXT.Left( szEXT.GetLength() - 3 );
	szEXT += _T("EXT");
}

//************************************
// Method:    GetExpCONMask
// FullName:  GetExpCONMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szCON
//************************************
void GetExpCONMask( CString szExp, CString szGrp, CString szSubj, CString& szCON )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szCON );
	szCON = szCON.Left( szCON.GetLength() - 3 );
	szCON += _T("CON");
}

//************************************
// Method:    GetExpERRMask
// FullName:  GetExpERRMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szERR
//************************************
void GetExpERRMask( CString szExp, CString szGrp, CString szSubj, CString& szERR )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szERR );
	szERR = szERR.Left( szERR.GetLength() - 3 );
	szERR += _T("ERR");
}

//************************************
// Method:    GetExpERSMask
// FullName:  GetExpERSMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szERS
//************************************
void GetExpERSMask( CString szExp, CString szGrp, CString szSubj, CString& szERS )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szERS );
	szERS = szERS.Left( szERS.GetLength() - 3 );
	szERS += _T("ERS");
}

//************************************
// Method:    GetExpINCMask
// FullName:  GetExpINCMask
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString & szINC
//************************************
void GetExpINCMask( CString szExp, CString szGrp, CString szSubj, CString& szINC )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	GetExpHWRMask( szExp, szGrp, szSubj, szINC );
	szINC = szINC.Left( szINC.GetLength() - 3 );
	szINC += _T("INC");
}

//************************************
// Method:    GetGroupSort
// FullName:  GetGroupSort
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szFile
//************************************
void GetGroupSort( CString szExp, CString& szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFile.Format( _T("%s\\grp.SRT"), szExp );
	else
		szFile.Format( _T("%s\\%s\\grp.SRT"), szPath, szExp );
}

//************************************
// Method:    GetSubjSort
// FullName:  GetSubjSort
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szFile
//************************************
void GetSubjSort( CString szExp, CString& szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFile.Format( _T("%s\\subj.SRT"), szExp );
	else
		szFile.Format( _T("%s\\%s\\subj.SRT"), szPath, szExp );
}

//************************************
// Method:    GetCondSort
// FullName:  GetCondSort
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString & szFile
//************************************
void GetCondSort( CString szExp, CString& szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );

	if( szPath == _T("") )
		szFile.Format( _T("%s\\cond.SRT"), szExp );
	else
		szFile.Format( _T("%s\\%s\\cond.SRT"), szPath, szExp );
}

//************************************
// Method:    SetGripperSettings
// FullName:  SetGripperSettings
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: GripperSettings * gs
//************************************
void SetGripperSettings( GripperSettings* gs )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !gs ) return;

	_GripperSettings.nDevice = gs->nDevice;
	_GripperSettings.nChannels = gs->nChannels;
	_GripperSettings.lSamples = gs->lSamples;
	_GripperSettings.lSampleRate = gs->lSampleRate;
	_GripperSettings.lScanRate = gs->lScanRate;
	_GripperSettings.nBaseline = gs->nBaseline;
	_GripperSettings.nChanLower = gs->nChanLower;
	_GripperSettings.nChanUpper = gs->nChanUpper;
	_GripperSettings.nChanLoad = gs->nChanLoad;
	_GripperSettings.dGainLG = gs->dGainLG;
	_GripperSettings.dGainUG = gs->dGainUG;
	_GripperSettings.dGainL = gs->dGainL;
	_GripperSettings.dCalibrationConstantLG = gs->dCalibrationConstantLG;
	_GripperSettings.dCalibrationConstantUG = gs->dCalibrationConstantUG;
	_GripperSettings.dCalibrationConstantL = gs->dCalibrationConstantL;
	_GripperSettings.dExcitationVoltageLG = gs->dExcitationVoltageLG;
	_GripperSettings.dExcitationVoltageUG = gs->dExcitationVoltageUG;
	_GripperSettings.dExcitationVoltageL = gs->dExcitationVoltageL;
	_GripperSettings.dFullScaleLoadLG = gs->dFullScaleLoadLG;
	_GripperSettings.dFullScaleLoadUG = gs->dFullScaleLoadUG;
	_GripperSettings.dFullScaleLoadL = gs->dFullScaleLoadL;
	_GripperSettings.fVolts = gs->fVolts;
	_GripperSettings.fDoubleBuffer = gs->fDoubleBuffer;
	_GripperSettings.lDBSamples = gs->lDBSamples;
	_GripperSettings.fNewtons = gs->fNewtons;
}

//************************************
// Method:    GetGripperSettings
// FullName:  GetGripperSettings
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: GripperSettings * gs
//************************************
void GetGripperSettings( GripperSettings* gs )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !gs ) return;

	gs->nDevice = _GripperSettings.nDevice;
	gs->nChannels = _GripperSettings.nChannels;
	gs->lSamples = _GripperSettings.lSamples;
	gs->lSampleRate = _GripperSettings.lSampleRate;
	gs->lScanRate = _GripperSettings.lScanRate;
	gs->nBaseline = _GripperSettings.nBaseline;
	gs->nChanLower = _GripperSettings.nChanLower;
	gs->nChanUpper = _GripperSettings.nChanUpper;
	gs->nChanLoad = _GripperSettings.nChanLoad;
	gs->dGainLG = _GripperSettings.dGainLG;
	gs->dGainUG = _GripperSettings.dGainUG;
	gs->dGainL = _GripperSettings.dGainL;
	gs->dCalibrationConstantLG = _GripperSettings.dCalibrationConstantLG;
	gs->dCalibrationConstantUG = _GripperSettings.dCalibrationConstantUG;
	gs->dCalibrationConstantL = _GripperSettings.dCalibrationConstantL;
	gs->dExcitationVoltageLG = _GripperSettings.dExcitationVoltageLG;
	gs->dExcitationVoltageUG = _GripperSettings.dExcitationVoltageUG;
	gs->dExcitationVoltageL = _GripperSettings.dExcitationVoltageL;
	gs->dFullScaleLoadLG = _GripperSettings.dFullScaleLoadLG;
	gs->dFullScaleLoadUG = _GripperSettings.dFullScaleLoadUG;
	gs->dFullScaleLoadL = _GripperSettings.dFullScaleLoadL;
	gs->fVolts = _GripperSettings.fVolts;
	gs->fDoubleBuffer = _GripperSettings.fDoubleBuffer;
	gs->lDBSamples = _GripperSettings.lDBSamples;
	gs->fNewtons = _GripperSettings.fNewtons;
}

//************************************
// Method:    IsTabletInstalled
// FullName:  IsTabletInstalled
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsTabletInstalled()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	char szObj[ 500 ];
	::GetSystemDirectory( szObj, 500 );
	strcat_s( szObj, "\\WinTab32.DLL" );
	HINSTANCE hMod = LoadLibrary( szObj );
	if( hMod )
	{
		FreeLibrary( hMod );
		return TRUE;
	}
	else return FALSE;
}

//************************************
// Method:    IsGripperInstalled
// FullName:  IsGripperInstalled
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsGripperInstalled()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#if defined _DEBUG
	return TRUE;
#endif

	char szObj[ 500 ];
	::GetSystemDirectory( szObj, 500 );
	strcat_s( szObj, "\\NiDAQ32.DLL" );
	CFileFind ff;
	if( ff.FindFile( szObj ) ) return TRUE;
	else return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Compression

//************************************
// Method:    CompressData
// FullName:  CompressData
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szFileOut
// Parameter: CString szSubj
// Parameter: CString szGrp
//************************************
BOOL CompressData( CString szExp, CString szFileOut, CString szSubj, CString szGrp )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return FALSE;
#else
	CWaitCursor crs;
	// vars & paths
	CString szUserPath, szFiles, szExpPath = szExp;
	CkString sMsg;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	::GetDataPathRoot( szUserPath );
	if( ( szSubj != _T("") ) && ( szGrp != _T("") ) )
		szExpPath.Format( _T("%s\\%s\\%s"), szExp, szGrp, szSubj );
	// zip progress object
	ZipProgress myzp;
	CkZipProgress* zp = &myzp;
	// zip object
	CkZip zip;
	zip.put_CaseSensitive( FALSE );
	// unlock features
	if( !zip.UnlockComponent( CHILKAT_ZIP_UNLOCK ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// set zip file name/location
	if( !zip.NewZip( szFileOut ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// append from directory
	zip.put_AppendFromDir( szUserPath );
	// exclude backup directory - MUST BE CALLED BEFORE APPEND
	szFiles.Format( _T("%s\\backup"), szUserPath );
	zip.ExcludeDir( szFiles );
	// file types to include
	CkStringArray sArray;
	szFiles.Format( _T("%s\\*.hwr"), szExpPath );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.frd"), szExpPath );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.dis"), szExpPath );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.tgt"), szExpPath );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.pcx"), szExpPath );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.txt"), szExpPath );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.seq"), szExpPath );
	sArray.Append( szFiles );
	// add file types
	if( !zip.AppendMultiple( sArray, true, zp ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// create zip
	if( !zip.WriteZipAndClose( zp ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// cleanup
	CkSettings::cleanupMemory();
#endif

	return TRUE;
}

//************************************
// Method:    CompressSupport
// FullName:  CompressSupport
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szFileOut
//************************************
BOOL CompressSupport( CString szExp, CString szFileOut )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return FALSE;
#else
	CWaitCursor crs;
	// vars & user path
	CString szUserPath, szFiles;
	CkString sMsg;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	::GetDataPathRoot( szUserPath );
	// zip progress object
	ZipProgress myzp;
	CkZipProgress* zp = &myzp;
	// zip object
	CkZip zip;
	zip.put_CaseSensitive( FALSE );
	// unlock features
	if( !zip.UnlockComponent( CHILKAT_ZIP_UNLOCK ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// set zip file name/location
	if( !zip.NewZip( szFileOut ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// append from directory
	zip.put_AppendFromDir( szUserPath );
	// file types to include
	CkStringArray sArray;
	szFiles.Format( _T("%s\\*.mem"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.srt"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.txt"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.ins"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.rtf"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.xmd"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.xmh"), szExp );
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\*.sum"), szExp );
	sArray.Append( szFiles );
	szFiles = _T("stimuli\\*.bmp");
	sArray.Append( szFiles );
	szFiles = _T("sounds\\*.wav");
	sArray.Append( szFiles );
	szFiles = _T("scripts\\*.*");
	sArray.Append( szFiles );
	szFiles.Format( _T("%s\\attachments\\*.*"), szExp );
	sArray.Append( szFiles );
	// add file types
	if( !zip.AppendMultiple( sArray, true, zp ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// create zip
	if( !zip.WriteZipAndClose( zp ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// cleanup
	CkSettings::cleanupMemory();
#endif

	return TRUE;
}

//************************************
// Method:    CompressAll
// FullName:  CompressAll
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szPath
// Parameter: CString szFileOut
// Parameter: CString szFiles
// Parameter: CString szExpFile
// Parameter: CMemFile * pMemFile
//************************************
BOOL CompressAll( CString szPath, CString szFileOut, CString szFiles, CString szExpFile, CMemFile* pMemFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return FALSE;
#else
	if( szFileOut == _T("") ) return FALSE;
	// file types to include
	CWaitCursor crs;
	CkStringArray sArray;
	CString szFile;
	int nPos = szFiles.Find( _T("=") );
	while( ( szFiles == _T("") ) ? FALSE : TRUE )
	{
		if( nPos == -1 )
		{
			szFile = szFiles;
			szFile.MakeUpper();
			sArray.Append( szFile );
			break;
		}
		else
		{
			szFile = szFiles.Left( nPos );
			szFile.MakeUpper();
			sArray.Append( szFile );
			szFiles = szFiles.Mid( nPos + 1 );
			nPos = szFiles.Find( _T("=") );
		}
	}
	CkString sMsg;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	// zip progress object
	ZipProgress myzp;
	CkZipProgress* zp = &myzp;
	// zip object
	CkZip zip;
	zip.put_CaseSensitive( FALSE );
	// unlock features
	if( !zip.UnlockComponent( CHILKAT_ZIP_UNLOCK ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// set zip file name/location
	if( !zip.NewZip( szFileOut ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// add files
	CkString theFile;
	long nFiles = sArray.get_Count();
	for( long i = 0; i < nFiles; i++ )
	{
		sArray.GetString( i, theFile );
		if( !zip.AppendOneFileOrDir( theFile ) )
		{
			zip.LastErrorText( sMsg );
			BCGPMessageBox( sMsg );
			CkSettings::cleanupMemory();
			return FALSE;
		}
	}
	// add memory file
	ULONGLONG ullLen = pMemFile->GetLength();
	BYTE* data = pMemFile->Detach();
	CkZipEntry* pEntry = zip.AppendData( szExpFile, data, (ULONG)ullLen );
	pMemFile->Close();
	if( data ) delete data;
	// security
//#ifndef	_DEBUG
	CString szPass = ::LoadAndDecode( IDS_FTP_PASSWORD );
	zip.put_PasswordProtect( true );
	zip.SetPassword( szPass );
	zip.put_Encryption( 4 );
	zip.put_EncryptKeyLength( 128 );
//#endif
	// create zip
	if( !zip.WriteZipAndClose( zp ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// cleanup
	if( pEntry ) delete pEntry;
	CkSettings::cleanupMemory();
#endif

	return TRUE;
}

//************************************
// Method:    UncompressSize
// FullName:  UncompressSize
// Access:    public 
// Returns:   ULONGLONG
// Qualifier:
// Parameter: CString szFileIn
//************************************
ULONGLONG UncompressSize( CString szFileIn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	ULONGLONG lVal = 0L;
#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return FALSE;
#else
	CWaitCursor crs;
	CkString sMsg;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	// zip object
	CkZip zip;
	// unlock features
	if( !zip.UnlockComponent( CHILKAT_ZIP_UNLOCK ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// security
	CString szPass = ::LoadAndDecode( IDS_FTP_PASSWORD );
	zip.SetPassword( szPass );
	zip.put_Encryption( 4 );
	zip.put_EncryptKeyLength( 128 );
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// open zip
	if( !zip.OpenZip( szFileIn ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return FALSE;
	}
	// iterate through each file and get sizes
	CkZipEntry* entry = zip.FirstEntry();
	while( entry )
	{
		lVal += entry->get_UncompressedLength();
		CkZipEntry* temp = entry;
		entry = entry->NextEntry();
		delete temp;
	}
	// close
	zip.CloseZip();
	// cleanup
	CkSettings::cleanupMemory();
#endif

	return lVal;
}

//************************************
// Method:    Uncompress
// FullName:  Uncompress
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szFileIn
// Parameter: CString szLocation
//************************************
BOOL Uncompress( CString szFileIn, CString szLocation )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return FALSE;
#else
	CWaitCursor crs;
	// vars & user path
	CString szUserPath, szFiles;
	::GetDataPathRoot( szUserPath );
	if( szLocation == _T("") ) szLocation = szUserPath;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	// zip progress object
	ZipProgress myzp;
	CkZipProgress* zp = &myzp;
	// zip object
	CkZip zip;
	zip.put_CaseSensitive( FALSE );
	// unlock features
	zip.UnlockComponent( CHILKAT_ZIP_UNLOCK );
	// security
	CString szPass = ::LoadAndDecode( IDS_FTP_PASSWORD );
	zip.SetPassword( szPass );
	zip.put_Encryption( 4 );
	zip.put_EncryptKeyLength( 128 );
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// open zip
	zip.OpenZip( szFileIn );
	// unzip
	zip.UnzipNewer( szLocation, zp );
	// close zip
	zip.CloseZip();
	// cleanup
	CkSettings::cleanupMemory();
#endif

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    UncompressFile
// FullName:  UncompressFile
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szFileIn
// Parameter: CString szFile
// Parameter: CString szLocation
//************************************
BOOL UncompressFile( CString szFileIn, CString szFile, CString szLocation )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return FALSE;
#else
	// vars & user path
	szFile.MakeUpper();
	CString szUserPath, szFiles;
	::GetDataPathRoot( szUserPath );
	if( szLocation == _T("") ) szLocation = szUserPath;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	// zip progress object
	ZipProgress myzp;
	CkZipProgress* zp = &myzp;
	// zip object
	CkZip zip;
	zip.put_CaseSensitive( FALSE );
	// unlock features
	zip.UnlockComponent( CHILKAT_ZIP_UNLOCK );
	// security
	CString szPass = ::LoadAndDecode( IDS_FTP_PASSWORD );
	zip.SetPassword( szPass );
	zip.put_Encryption( 4 );
	zip.put_EncryptKeyLength( 128 );
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// open zip
	zip.OpenZip( szFileIn );
	// unzip
	CkZipEntry* entry = zip.GetEntryByName( szFile );
	if( !entry )
	{
		CString szMsg;
		szMsg.Format( _T("Unable to locate %s in import file (%s)"), szFile, szFileIn );
//		BCGPMessageBox( szMsg );
		OutputAMessage( szMsg, TRUE, LOG_UI );
		zip.CloseZip();
		CkSettings::cleanupMemory();
		return FALSE;
	}
	BOOL fSuccess = entry->Extract( szLocation );
	delete entry;
	// close zip
	zip.CloseZip();
	// cleanup
	CkSettings::cleanupMemory();

	return fSuccess;
#endif

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ZIPFileList
// FullName:  ZIPFileList
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szZIP
// Parameter: CStringList & lstFiles
//************************************
void ZIPFileList( CString szZIP, CStringList& lstFiles )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return;
#else
	CkString sMsg;
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	// zip object
	CkZip zip;
	// unlock features
	if( !zip.UnlockComponent( CHILKAT_ZIP_UNLOCK ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return;
	}
	// security
	CString szPass = ::LoadAndDecode( IDS_FTP_PASSWORD );
	zip.SetPassword( szPass );
	zip.put_Encryption( 4 );
	zip.put_EncryptKeyLength( 128 );
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// open zip
	if( !zip.OpenZip( szZIP ) )
	{
		zip.LastErrorText( sMsg );
		BCGPMessageBox( sMsg );
		CkSettings::cleanupMemory();
		return;
	}
	// iterate through each file
	CkString sFile;
	CkZipEntry* entry = zip.FirstEntry();
	while( entry )
	{
		entry->get_FileName( sFile );
		lstFiles.AddTail( sFile.getString() );
		CkZipEntry* temp = entry;
		entry = entry->NextEntry();
		delete temp;
	}
	// close
	zip.CloseZip();
	// cleanup
	CkSettings::cleanupMemory();
#endif
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ExtractToMemoryFile
// FullName:  ExtractToMemoryFile
// Access:    public 
// Returns:   CMemFile*
// Qualifier:
// Parameter: CString szZIP
// Parameter: CString szFile
//************************************
CMemFile* ExtractToMemoryFile( CString szZIP, CString szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

#ifndef _CHILKAT_
	BCGPMessageBox( _T("Compression library not included in this build.") );
	return NULL;
#else
	char pszTempPath[ _MAX_PATH ];
	::GetTempPath( _MAX_PATH, pszTempPath );
	// zip progress object
	ZipProgress myzp;
	CkZipProgress* zp = &myzp;
	// zip object
	CkZip zip;
	zip.put_CaseSensitive( FALSE );
	// unlock features
	zip.UnlockComponent( CHILKAT_ZIP_UNLOCK );
	// security
	CString szPass = ::LoadAndDecode( IDS_FTP_PASSWORD );
	zip.SetPassword( szPass );
	zip.put_Encryption( 4 );
	zip.put_EncryptKeyLength( 128 );
	// set zip temporary directory
	zip.put_TempDir( pszTempPath );
	// open zip
	zip.OpenZip( szZIP );
	// unzip
	szFile.MakeUpper();
	CkZipEntry* entry = zip.GetEntryByName( szFile );
	if( !entry )
	{
		CString szMsg;
		szMsg.Format( _T("Unable to locate %s in MEF file (%s)"), szFile, szZIP );
		BCGPMessageBox( szMsg );
		zip.CloseZip();
		CkSettings::cleanupMemory();
		return FALSE;
	}
	ULONGLONG lVal = entry->get_UncompressedLength();
	CMemFile* pMemFile = NULL;
	if( lVal > 0 )
	{
		CkString str;
		if( entry->InflateToString2( str ) )
		{
			BYTE* data = new BYTE[ (UINT)lVal + 1 ];
			memcpy( data, str.getString(), (UINT)lVal );
			pMemFile = new CMemFile( data, (UINT)lVal );
		}
	}
	delete entry;
	// close zip
	zip.CloseZip();
	// cleanup
	CkSettings::cleanupMemory();

	return pMemFile;
#endif
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// mimic CStdioFile readstring by searching for CRLF and returning at that point
//************************************
// Method:    MemReadString
// FullName:  MemReadString
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CMemFile * pFile
// Parameter: CString & szData
//************************************
BOOL MemReadString( CMemFile* pFile, CString& szData )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !pFile ) return FALSE;

	szData = _T("");

	// max size
	ULONGLONG ullMax = pFile->GetLength();
	// buffer
	char buf[ 2 ], c;
	// read until a CRLF is found
	UINT nBytes = 0, nBytesRead = 0;
	while( ( nBytes = pFile->Read( buf, 1 ) ) > 0 )
	{
		nBytesRead += nBytes;
		c = buf[ 0 ];
		if( ( c == '\r' ) || ( c == '\n' ) )
		{
			// move file pointer since it is at carriage return line feed spot
			if( ( c != '\n' ) && ( pFile->GetPosition() < ullMax ) )
				pFile->Seek( 1, CFile::current );
			break;
		}
		szData += c;
	}

	if( nBytesRead > 0 ) return TRUE;
	else return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// mimic CStdioFile writestring
//************************************
// Method:    MemWriteString
// FullName:  MemWriteString
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CMemFile * pFile
// Parameter: CString szData
//************************************
void MemWriteString( CMemFile* pFile, CString szData )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if( !pFile ) return;
	// write
	pFile->Write( szData.GetBuffer(), szData.GetLength() );
}

/////////////////////////////////////////////////////////////////////////////
// Multimedia

//************************************
// Method:    PlayASoundThread
// FullName:  PlayASoundThread
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT PlayASoundThread( LPVOID pParam )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	char* file = (char*)pParam;
	if( file ) PlaySound( file, NULL, SND_FILENAME );
	return 0;
}

//************************************
// Method:    RunAScript
// FullName:  RunAScript
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
TrialRunInfo*	__tri = NULL;
int				__typeScript = 0;
char			__pszCondID[ szIDS + 1 ] = "";
char			__pszScript[ _MAX_PATH + 1 ] = "";
UINT RunAScript( LPVOID pParam )
{
	if( !pParam || !__tri ) return 0;
	CString szCondID = __pszCondID;
	char* pszScript = __pszScript;
	CString szScript = pszScript;
	CString szFileExt = ::IsGripperModeOn() ? _T("FRD") : _T("HWR");
	CString szIdx;
	szIdx.Format( _T("%02d"), __tri->nRep );
	ExternalAppType eScript = (ExternalAppType)__typeScript;
	switch( eScript ) {
		case eEAT_Matlab: {
			Experiments::ExecuteMatlabScript( szScript, __tri->szExpID, __tri->szGrpID,
											  __tri->szSubjID, szCondID, szIdx, szFileExt );
			break;
		}
		case eEAT_Batch: {
			Experiments::ExecuteBatchScript( szScript, __tri->szExpID, __tri->szGrpID,
											 __tri->szSubjID, szCondID, szIdx, szFileExt, TRUE );
			break;
		}
	}
	__tri = NULL;
	__typeScript = 0;
	strcpy_s( __pszCondID, szIDS + 1, "" );
	strcpy_s( __pszScript, _MAX_PATH + 1, "" );
	return 0;
}

//************************************
// Method:    PlayASound
// FullName:  PlayASound
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szFile
//************************************
void PlayASound( CString szFile )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	strcpy_s( _soundfile, _MAX_PATH, szFile );
	CWinThread* pThread = AfxBeginThread( PlayASoundThread, _soundfile );
}

//************************************
// Method:    PlayASound
// FullName:  PlayASound
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szCondID
// Parameter: UINT nSound
//************************************
void PlayASound( CString szCondID, UINT nSound, TrialRunInfo* pInfo )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	BOOL fUse = FALSE, fTone = FALSE, fTrigger = FALSE;
	int nFreq = 0, nDur = 0, nType = 0;
	CString szFile, szScript, szBase;

	::GetDataPathRoot( szBase );
	if( szBase != _T("") && ( szBase.GetAt( szBase.GetLength() - 1 ) != '\\' ) ) szBase += _T("\\");


	UserObj* pObj = ::GetCurrentUserObj();
	TrialRunEvents* tre = pObj ? ( pObj->m_pTRI ? pObj->m_pTRI->pEvents : NULL ) : NULL;
	CondEvents* ce = tre ? tre->GetConditionEvents( szCondID ) : NULL;
	if( ce ) {
		if( ce->pEvents[ nSound ].szSound != _T("") ) {
			szFile = szBase;
			szFile += _T("sounds\\");
			szFile += ce->pEvents[ nSound ].szSound;
		}
		fUse = ce->pEvents[ nSound ].fUse;
		fTone = ce->pEvents[ nSound ].fTone;
		fTrigger = ce->pEvents[ nSound ].fTrigger;
		nFreq = ce->pEvents[ nSound ].nFreq;
		nDur = ce->pEvents[ nSound ].nDur;
		if( ce->pEvents[ nSound ].szScript != _T("") ) {
			szScript = szBase;
			szScript += _T("scripts\\");
			szScript += ce->pEvents[ nSound ].szScript;
			nType = ce->pEvents[ nSound ].nScriptType;
		}
	}

	// Process Post-Record, Pre-TF external app script, if any
	if( szScript != _T("") ) {
		__tri = pInfo;
		__typeScript = nType;
		strcpy_s( __pszCondID, szIDS + 1, szCondID );
		strcpy_s( __pszScript, _MAX_PATH + 1, szScript );
		CWinThread* pThread = AfxBeginThread( RunAScript, __pszScript );
	}

	// send a trigger pulse first if need be
	if( fTrigger )
	{
		// open port if not open
		if( !_PortOpen )
		{
			// what is set port?
			int nPort = 0;
			CString szPort;

			// get port # from string
			::GetCommPort( szPort );
			szPort.MakeUpper();
			if( szPort.Find( _T("COM1") ) != -1 ) nPort = 1;
			else if( szPort.Find( _T("COM2") ) != -1 ) nPort = 2;
			else if( szPort.Find( _T("COM3") ) != -1 ) nPort = 3;
			else if( szPort.Find( _T("COM4") ) != -1 ) nPort = 4;
			if( nPort == 0 ) return;

			// open port
			_Port = OpenComPort( nPort, 1024, 1024 );
			if( _Port < 0 )
			{
				OutputAMessage( _T("Error opening communications port."), TRUE, LOG_UI );
			}
			else
			{
				_PortOpen = TRUE;

				// initialize port
				InitializePort( _Port, nPort - 1, 0, 0, CARD_WINAPI, 0, 1024, 1024, 0 );
			}
		}
		if( _PortOpen && ( _Port != -1 ) )
		{
			OutputAMessage( _T("Firing Trigger Pulse"), TRUE );
			// pulse off
			DtrOff( _Port );
			RtsOff( _Port );

			//loop
			Delay( 25000 );

			// pulse
			DtrOn( _Port );
			RtsOn( _Port );
		}
	}

	// now handle sounds
	if( fUse && !fTone && ( szFile != _T("") ) ) PlayASound( szFile );
	else if( fUse && fTone ) Beep( nFreq, nDur );
}

//************************************
// Method:    SystemCleanup
// FullName:  SystemCleanup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void SystemCleanup()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( _PortOpen )
	{
		UnInitializePort( _Port );
		CloseComPort( _Port );
		_PortOpen = FALSE;
		_Port = -1;
	}
}

//************************************
// Method:    StripNewlinesAndTabs
// FullName:  StripNewlinesAndTabs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szData
//************************************
void StripNewlinesAndTabs( CString& szData )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// try to remove cr/lf combo first
	szData.Replace( "\r\f", " " );
	// then individual items
	szData.Replace( "\r", " " );
	szData.Replace( "\f", " " );
	szData.Replace( "\n", " " );
	szData.Replace( "\t", " " );
}

//************************************
// Method:    SetButtonBitmap
// FullName:  SetButtonBitmap
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CButton * pButton
// Parameter: UINT nImageResourceID
//************************************
void SetButtonBitmap( CButton* pButton, UINT nImageResourceID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !pButton || ( nImageResourceID < 0 ) ) return;

	HANDLE hbmp = NULL;
	hbmp = ::LoadImage( AfxGetApp()->m_hInstance,
						MAKEINTRESOURCE( nImageResourceID ),
						IMAGE_BITMAP,
						0,
						0,
						LR_LOADTRANSPARENT | LR_LOADMAP3DCOLORS );
	if( !hbmp ) return;
	pButton->SetBitmap( (HBITMAP)hbmp );
}

//************************************
// Method:    SetButtonBitmap
// FullName:  SetButtonBitmap
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CStatic * pButton
// Parameter: UINT nImageResourceID
//************************************
void SetButtonBitmap( CStatic* pButton, UINT nImageResourceID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !pButton || ( nImageResourceID < 0 ) ) return;

	HANDLE hbmp = NULL;
	hbmp = ::LoadImage( AfxGetApp()->m_hInstance,
						MAKEINTRESOURCE( nImageResourceID ),
						IMAGE_BITMAP,
						0,
						0,
						LR_LOADTRANSPARENT | LR_LOADMAP3DCOLORS );
	if( !hbmp ) return;
	pButton->SetBitmap( (HBITMAP)hbmp );
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    SetLastExperimentInfo
// FullName:  SetLastExperimentInfo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExpID
// Parameter: CString szGrpID
// Parameter: CString szSubjID
//************************************
void SetLastExperimentInfo( CString szExpID, CString szGrpID, CString szSubjID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_szLastExpID = szExpID;
	_szLastGrpID = szGrpID;
	_szLastSubjID = szSubjID;
}

//************************************
// Method:    GetLastExperimentInfo
// FullName:  GetLastExperimentInfo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szExpID
// Parameter: CString & szGrpID
// Parameter: CString & szSubjID
//************************************
void GetLastExperimentInfo( CString& szExpID, CString& szGrpID, CString& szSubjID )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szExpID = _szLastExpID;
	szGrpID = _szLastGrpID;
	szSubjID = _szLastSubjID;
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    SetLVMaximizeSize
// FullName:  SetLVMaximizeSize
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: int nSize
//************************************
void SetLVMaximizeSize( int nSize )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_LVMaxSize = nSize;
}

//************************************
// Method:    GetLVMaximizeSize
// FullName:  GetLVMaximizeSize
// Access:    public 
// Returns:   int
// Qualifier:
//************************************
int GetLVMaximizeSize()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _LVMaxSize;
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    SetTabletDimensions
// FullName:  SetTabletDimensions
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double dX
// Parameter: double dY
//************************************
void SetTabletDimensions( double dX, double dY )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_T_X = dX;
	_T_Y = dY;
}

//************************************
// Method:    GetTabletDimensions
// FullName:  GetTabletDimensions
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double & dX
// Parameter: double & dY
//************************************
void GetTabletDimensions( double& dX, double& dY )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	dX = _T_X;
	dY = _T_Y;
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    SetDisplayDimensions
// FullName:  SetDisplayDimensions
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double dX
// Parameter: double dY
//************************************
void SetDisplayDimensions( double dX, double dY )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_D_X = dX;
	_D_Y = dY;
}

//************************************
// Method:    GetDisplayDimensions
// FullName:  GetDisplayDimensions
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double & dX
// Parameter: double & dY
//************************************
void GetDisplayDimensions( double& dX, double& dY )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	dX = _D_X;
	dY = _D_Y;
}

//************************************
// Method:    GetMouseResolution
// FullName:  GetMouseResolution
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dWidth
//************************************
double GetMouseResolution( double dWidth )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	double dHeight = 0., dMouseRes = 0.;

	// width
	if( dWidth <= 0. ) ::GetDisplayDimensions( dWidth, dHeight );

	// desktop resolution
	CWnd* pDesktop = AfxGetMainWnd()->GetDesktopWindow();
	RECT rect;
	pDesktop->GetClientRect( &rect );
	double dRes = (double)rect.right;

	// mouse resolution
	if( dRes != 0.0 ) dMouseRes = dWidth / dRes;

	return dMouseRes;
}

//************************************
// Method:    SetDeviceSetupRun
// FullName:  SetDeviceSetupRun
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fVal
//************************************
void SetDeviceSetupRun( BOOL fVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_fDeviceSetupRun = fVal;
}

//************************************
// Method:    WasDeviceSetupRun
// FullName:  WasDeviceSetupRun
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL WasDeviceSetupRun()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _fDeviceSetupRun;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    CheckPass
// FullName:  CheckPass
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: BOOL fSubject
// Parameter: CString szPass
//************************************
BOOL CheckPass( BOOL fSubject, CString szPass )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

//#ifndef	_DEBUG
	if( !fSubject )
	{
		// if example user, this is unnecessary
		CString szUser;
		::GetCurrentUser( szUser );
		if( szUser == _T("UU1") ) return TRUE;
	}

	if( fSubject || ( ::IsPrivacyOn() && !::HasPassBeenSet() ) )
	{
		PasswordDlg d( AfxGetApp()->m_pMainWnd );
		d.m_fSubject = fSubject;
		if( fSubject ) d.m_szConfirm = szPass;
		else ::GetUserPassword( d.m_szConfirm );
		if( d.DoModal() != IDOK ) return FALSE;
		if( !fSubject ) ::SetHasPassBeenSet( TRUE );
	}
//#endif

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    SetClientServerInfo
// FullName:  SetClientServerInfo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szServer
// Parameter: CString szUser
// Parameter: CString szPassword
//************************************
void SetClientServerInfo( CString szServer, CString szUser, CString szPassword )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_szCSServer = szServer;
	_szCSUser = szUser;
	_szCSPassword = szPassword;
}

//************************************
// Method:    GetClientServerInfo
// FullName:  GetClientServerInfo
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString & szServer
// Parameter: CString & szUser
// Parameter: CString & szPassword
//************************************
BOOL GetClientServerInfo( CString& szServer, CString& szUser, CString& szPassword )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if( ::IsCS() )
	{
		szServer = _szCSServer;
		szUser = _szCSUser;
		szPassword = _szCSPassword;
		return TRUE;
	}
	else return FALSE;
}

//************************************
// Method:    GetServerName
// FullName:  GetServerName
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString & szServer
//************************************
BOOL GetServerName( CString& szServer )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if( ::IsCS() )
	{
		szServer = _szCSServer;
		return TRUE;
	}
	else return FALSE;
}

//************************************
// Method:    SetIsClientServerModeOn
// FullName:  SetIsClientServerModeOn
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fOn
//************************************
void SetIsClientServerModeOn( BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CSModeOn = fOn;
}

//************************************
// Method:    IsClientServerModeOn
// FullName:  IsClientServerModeOn
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsClientServerModeOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _CSModeOn;
}

//************************************
// Method:    VerifyLogin
// FullName:  VerifyLogin
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL VerifyLogin()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString szServer, szUser, szPassword;
	// if we've already obtained the information, no point in asking for it again
	if( !::GetClientServerInfo( szServer, szUser, szPassword ) )
	{
		BCGPMessageBox( _T("You do not have the client-server version activated.") );
		return FALSE;
	}
	if( szServer == _T("") )
	{
		BOOL fWasOn = ::IsClientServerModeOn();
		Experiments exp;

		// bring up login dialog
		LoginDlg d;
Login:
		d.m_szServer = szServer;
		d.m_szUser = szUser;
		d.m_szPassword = szPassword;
		if( d.DoModal() == IDCANCEL )
		{
			// reset global vars
//			::SetClientServerInfo( _T(""),  _T(""),  _T("") );
			return FALSE;
		}
		szServer = d.m_szServer;
		szUser = d.m_szUser;
		szPassword = d.m_szPassword;
		// update global vars
		::SetClientServerInfo( szServer, szUser, szPassword );

		// if we are not already in c/s server mode
		if( !fWasOn && !ForceOperationMode( TRUE ) ) return FALSE;

		// test login
		CWaitCursor crs;
		if( FAILED( exp.TestLogin() ) )
		{
			BCGPMessageBox( _T("Error logging in. Please be sure server is running and everything is correct. Server and password are case-sensitive.") );
			goto Login;
		}

		// reset operation mode if necessary
		if( !fWasOn ) ForceOperationMode( FALSE );
	}

	return TRUE;
}

//************************************
// Method:    ForceOperationMode
// FullName:  ForceOperationMode
// Access:    public 
// Returns:   BOOL
// Qualifier: /* this can only be called if you've logged in */
// Parameter: BOOL fClientServerOn
//************************************
BOOL ForceOperationMode( BOOL fClientServerOn )	// this can only be called if you've logged in
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// get login info
	CString szServer, szUser, szPassword;
	if( !::GetClientServerInfo( szServer, szUser, szPassword ) ) return FALSE;

	// create user
	INSMUser* pUser = NULL;
	::CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
						IID_INSMUser, (LPVOID*)&pUser );


	// Change operation mode to specified
	pUser->SetOperationMode( fClientServerOn, szServer.AllocSysString(),
							 szUser.AllocSysString(),
							 szPassword.AllocSysString() );

	// cleanup
	pUser->Release();

	return TRUE;
}

/************************************************************************/
/*=====Delay() using QueryPerformanceCounter()				*/
/*     & QueryPerformanceFrequency() (When USE_PERFORMANCE_COUNTER	*/
/*     defined.								*/
/************************************************************************/
int Delay(long MicroSeconds)
{
	LARGE_INTEGER	TicsPerSecond;
	LARGE_INTEGER	BaseTics;
	LARGE_INTEGER	CurrentTics;
	long			DeltaMicroSecondTics;


	if( !QueryPerformanceFrequency( &TicsPerSecond ) )
	{
		OutputAMessage( "Performance Frequency not available.", TRUE, LOG_UI );
		return( 1 );
	}
	if( !QueryPerformanceCounter( &BaseTics ) )
	{
		OutputAMessage( "Performance Counter not available.", TRUE, LOG_UI );
		return( 1 );
	}
		

	while( TRUE )
	{
		QueryPerformanceCounter( &CurrentTics );
		DeltaMicroSecondTics = (long)(((CurrentTics.QuadPart-BaseTics.QuadPart) *
								1000000) / TicsPerSecond.QuadPart );
		if( DeltaMicroSecondTics >= MicroSeconds )
			return( 0 );
		else if( ( MicroSeconds - DeltaMicroSecondTics ) > 20000 )
		{
			//Prevent CPU hogging
			long delay = ( ( MicroSeconds - DeltaMicroSecondTics ) - 20000 ) / 1000;
			Sleep( delay );
		}
	}
}

//************************************
// Method:    SetCurrentChart
// FullName:  SetCurrentChart
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szChart
//************************************
void SetCurrentChart( CString szChart )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_Chart = szChart;
}

//************************************
// Method:    GetCurrentChart
// FullName:  GetCurrentChart
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szChart
//************************************
void GetCurrentChart( CString& szChart )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	szChart = _Chart;
}

//************************************
// Method:    HasTrialPassed
// FullName:  HasTrialPassed
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szTrial
//************************************
BOOL HasTrialPassed( CString szTrial )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	BOOL fRet = FALSE;

	// Check ERR file (if exists) for validity
	CString szERR = szTrial;
	szERR = szERR.Left( szERR.GetLength() - 4 ); // removes .EXT
	CString szIdx = szERR.Right( 2 );
	int nIdx = atoi( szIdx );
	szERR = szERR.Left( szERR.GetLength() - 2 ); // removes ##.EXT
	szERR += _T(".ERR");
	CFileFind ff;
	if( ff.FindFile( szERR ) )
	{
		CStdioFile f;
		if( f.Open( szERR, CFile::modeRead ) )
		{
			CString szTemp, szItem;
			int nPos = -1;
			while( f.ReadString( szTemp ) )
			{
				nPos = szTemp.Find( _T(" ") );
				if( nPos != -1 )
				{
					szItem = szTemp.Left( nPos );
					if( nIdx == atoi( szItem ) )
					{
						nPos = szTemp.Find( _T("OK") );
						if( nPos != -1 ) fRet = TRUE;
						else fRet = FALSE;
						// 04/28/03 GMB: Since re-processing a single trial while appending
						// adds to the err file, previous bad info on that trial could appear
						// before hand..so we cannot stop at first sighting...must read through
						// entire ERR file to find last occurance
						//							f.Close();
						//							break;
					}
				}
			}
		}
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////
// drawing options
void SetLineTypeFixed( BOOL fVal ) { _fLineFixed = fVal; }

BOOL IsLineTypeFixed() { return _fLineFixed; }

void SetLineThickness( short nVal ) { _nLineThickness = nVal; }

short GetLineThickness() { return _nLineThickness; }

void SetIsLineByPressure( BOOL fVal ) { _fLineByPressure = fVal; }

BOOL IsLineByPressure() { return _fLineByPressure; }

void SetLineColor1( DWORD dwVal ) { _dwLineColor1 = dwVal; }

DWORD GetLineColor1() { return _dwLineColor1; }

void SetLineColor2( DWORD dwVal ) { _dwLineColor2 = dwVal; }

DWORD GetLineColor2() { return _dwLineColor2; }

void SetLineColor3( DWORD dwVal ) { _dwLineColor3 = dwVal; }

DWORD GetLineColor3() { return _dwLineColor3; }

void SetEllipseColor( DWORD dwVal ) { _dwEllipseColor = dwVal; }

DWORD GetEllipseColor() { return _dwEllipseColor; }

void SetMPPColor( DWORD dwVal ) { _dwMPPColor = dwVal; }

DWORD GetMPPColor() { return _dwMPPColor; }

void SetEllipseSize( short nVal ) { _nEllipseSize = nVal; }

short GetEllipseSize() { return _nEllipseSize; }

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    CoCreateInstanceAsAdmin
// FullName:  CoCreateInstanceAsAdmin
// Access:    public 
// Returns:   HRESULT
// Qualifier:
// Parameter: HWND hwnd
// Parameter: REFCLSID rclsid
// Parameter: REFIID riid
// Parameter: __out void * * ppv
//************************************
HRESULT CoCreateInstanceAsAdmin( HWND hwnd, REFCLSID rclsid, REFIID riid, __out void ** ppv )
{
	BIND_OPTS3	bo;
	WCHAR		wszCLSID[ 50 ];
	WCHAR		wszMonikerName[ 300 ];

	StringFromGUID2( rclsid, wszCLSID, sizeof( wszCLSID ) / sizeof( wszCLSID[ 0 ] ) ); 
// 	HRESULT hr = StringCchPrintfW( wszMonikerName, sizeof( wszMonikerName ) / sizeof( wszMonikerName[ 0 ] ),
// 								   L"Elevation:Administrator!new:%s", wszCLSID );
//	if( FAILED( hr ) ) return hr;
	int hr = swprintf_s( wszMonikerName, sizeof( wszMonikerName ) / sizeof( wszMonikerName[ 0 ] ),
						 L"Elevation:Administrator!new:%s", wszCLSID );
	if( hr == -1 ) return E_FAIL;

	memset( &bo, 0, sizeof( bo ) );
	bo.cbStruct = sizeof( bo );
	bo.hwnd = hwnd;
	bo.dwClassContext = CLSCTX_LOCAL_SERVER;
	return CoGetObject( wszMonikerName, &bo, riid, ppv );
}

/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    NextID
// FullName:  NextID
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString & szID
// Parameter: CString szMin
// Parameter: CString szMax
// Parameter: BOOL fRangeOnly
//************************************
BOOL NextID( CString& szID, CString szMin, CString szMax, BOOL fRangeOnly )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( szID.GetLength() != 3 ) return FALSE;
	if( szMin.GetLength() != 3 ) return FALSE;
	if( szMax.GetLength() != 3 ) return FALSE;

	// min & max of letters (caps) & numbers
	int nCharMax = 'Z', nCharMin = 'A';
	int nNumMax = '9', nNumMin = '0';
	CString szNext = szID;

	if( fRangeOnly ) goto CheckRange;

	// get each char for the 3 char ID
	int nChar[ 3 ];
	nChar[ 0 ] = szNext.GetAt( 0 );
	nChar[ 1 ] = szNext.GetAt( 1 );
	nChar[ 2 ] = szNext.GetAt( 2 );

	// increment lowest-order char and check if out of range then adjust
	nChar[ 2 ]++;
	if( nChar[ 2 ] > nCharMax )
	{
		nChar[ 2 ] = nNumMin;
		nChar[ 1 ]++;
	}
	else if( ( nChar[ 2 ] > nNumMax ) && ( nChar[ 2 ] < nCharMin ) )
	{
		nChar[ 2 ] = nCharMin;
	}
	// adjust 2nd-order as necessary
	if( nChar[ 1 ] > nCharMax )
	{
		nChar[ 1 ] = nNumMin;
		nChar[ 0 ]++;
	}
	else if( ( nChar[ 1 ] > nNumMax ) && ( nChar[ 1 ] < nCharMin ) )
	{
		nChar[ 1 ] = nCharMin;
	}
	// adjust highest-order
	if( nChar[ 0 ] > nCharMax ); // do nothing
	else if( ( nChar[ 0 ] > nNumMax ) && ( nChar[ 0 ] < nCharMin ) )
		nChar[ 0 ] = nCharMin;

	// update string
	szNext.SetAt( 0, (char)nChar[ 0 ] );
	szNext.SetAt( 1, (char)nChar[ 1 ] );
	szNext.SetAt( 2, (char)nChar[ 2 ] );

	szID = szNext;

CheckRange:
	if( ( szID < szMin ) || ( szID > szMax ) ) return FALSE;
	else return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Device Information
/////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// GetDigitizerInfo		- queries the digitizer for it's info.
// Returns		- TRUE for success
// szDesc		- Tablet identification string
// lRes			- Device resolution in lines per cm (obtained from X axis)
// lX			- Range of X axis ( / res = x dimension)
// lX			- Range of Y axis ( / res = y dimension)
// nRate		- Sampling rate
// fInches		- Inches used (cm otherwise)
// fTilt		- Tilt supported
// nMaxPP		- Max pen pressure of device
BOOL GetDigitizerInfo( CString& szDesc, long& lRes, long& lX,
					   long& lY, short& nRate, BOOL& fInches,
					   BOOL& fTilt, short& nMaxPP )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !::IsTabletModeOn() || !::IsTabletInstalled() || ::IsGripperModeOn() ) return FALSE;

	IDigitizer* pDigitizer = NULL;
	HRESULT hr = CoCreateInstance( CLSID_Digitizer, NULL, CLSCTX_ALL,
								   IID_IDigitizer, (LPVOID*)&pDigitizer );
	if( FAILED( hr ) ) return FALSE;

	// authorization
	CString szCode = ::GetAuthorizationInputMod();
	hr = pDigitizer->SetAuthorization( szCode.AllocSysString() );
	if( FAILED( hr ) )
	{
		pDigitizer->Release();
		return FALSE;
	}

	BSTR bstr;
	hr = pDigitizer->get_TabletDesc( &bstr );
	if( SUCCEEDED( hr ) ) szDesc = bstr;

	hr = pDigitizer->get_TabletResolution( &lRes );
	pDigitizer->get_TabletXRange( &lX );
	pDigitizer->get_TabletYRange( &lY );
	pDigitizer->get_TabletRate( &nRate );
	pDigitizer->get_MaxPenPressure( &nMaxPP );
	if( hr == S_FALSE ) fInches = TRUE;
	else fInches = FALSE;

	pDigitizer->SupportsTilt( &fTilt );

	pDigitizer->Release();

	return TRUE;
}

//************************************
// Method:    GetDeviceInfo
// FullName:  GetDeviceInfo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: short & nRate	- Sampling Rate
// Parameter: short & nPressure - Min Pen Pressure
// Parameter: double & dRes - Device Resolution
// Parameter: CString & szDev - Device String
// Parameter: CString & szDim - Dimensions String
// Parameter: CString & szMaxPP - Max Pen Pressure String
//************************************
void GetDeviceInfo( short& nRate, short& nPressure, double& dRes,
				    CString& szDev, CString& szDim, BOOL& fTilt, CString& szMaxPP )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( ::IsGripperModeOn() )
	{
		GripperSettings gs;
		::GetGripperSettings( &gs );
		nRate = (short)gs.lScanRate;
		nPressure = GRIPPER_PRESS;
		dRes = GRIPPER_RES;
		fTilt = FALSE;
	}
	else if( ::IsTabletModeOn() )
	{
		nRate = TABLET_RATE;
		nPressure = 0;
		dRes = TABLET_RES;

		long lRes = 0, lX = 0, lY = 0;
		double dX = 0., dY = 0.;
		short nRate2 = 0, nMaxPP = 0;
		BOOL fInches = FALSE;
		if( ::GetDigitizerInfo( szDev, lRes, lX, lY, nRate2, fInches, fTilt, nMaxPP ) )
		{
			szMaxPP.Format( _T("Max Pen Pressure: %d"), nMaxPP );
			if( ( lX > 0 ) && ( lY > 0 ) )
			{
				nRate = nRate2;
				dRes = 1.0 / ( ( lRes != 0 ) ? lRes : 1.0 );
				dX = ( 1.0 * lX ) / ( 1.0 * ( ( lRes != 0 ) ? lRes : 1 ) );
				dY = ( 1.0 * lY ) / ( 1.0 * ( ( lRes != 0 ) ? lRes : 1 ) );

				if( fInches )
				{
					dRes *= 2.54;
					dX *= 2.54;
					dY *= 2.54;
				}

				szDim.Format( _T("Dimensions: X = %.2f cm - Y = %.2f cm"), dX, dY );
			}

			// verify that the values obtained are not bogus
			if( ( nRate < 0 ) || ( nRate > MAX_SAMPLING_RATE ) ||
				( dRes < 0. ) || ( dRes > MAX_DEVICE_RES ) ||
				( dX < 0. ) || ( dX > MAX_DEVICE_DIM ) ||
				( dY < 0. ) || ( dY > MAX_DEVICE_DIM ) )
			{
				CString szMsg = _T("Acquired input device settings appear to be invalid or out-of-range.\nCheck that your driver is installed and working properly.");
				OutputAMessage( szMsg, TRUE );
				BCGPMessageBox( szMsg );
			}
		}
	}
	else
	{
		nRate = MOUSE_RATE;
		nPressure = MOUSE_PRESS;
		dRes = MOUSE_RES;
		fTilt = FALSE;
		// calculate anticipated resolution: screen width / windows resolution (horiz)
		dRes = ::GetMouseResolution();
		szDev = _T("MOUSE");
		szDim = _T("Resolution = System resolution / display width");
	}
}

//************************************
// Method:    GetDeviceDimensions
// FullName:  GetDeviceDimensions
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double & dWidth
// Parameter: double & dHeight
//************************************
void GetDeviceDimensions( double& dWidth, double& dHeight )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	long lRes = 0, lX = 0, lY = 0;
	double dX = 0., dY = 0.;
	short nRate = 0, nMaxPP = 0;
	BOOL fInches = FALSE, fTilt = FALSE;
	CString szDev;
	if( ::GetDigitizerInfo( szDev, lRes, lX, lY, nRate, fInches, fTilt, nMaxPP ) )
	{
		if( fInches )
		{
			dWidth = (double)( ( 1.0 * lX )  / ( 1.0 * lRes ) * 2.54 );
			dHeight = (double)( ( 1.0 * lY ) / ( 1.0 * lRes ) * 2.54 );
		}
		else
		{
			dWidth = (double)( 1.0 * lX / ( 1.0 * lRes ) );
			dHeight = (double)( 1.0 * lY / ( 1.0 * lRes ) );
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// User Settings/Preferences
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    GetPreferences
// FullName:  GetPreferences
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: Preferences * prefs
// Parameter: GripperSettings * gs
//************************************
void GetPreferences( Preferences* prefs, GripperSettings* gs )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( prefs )
	{
		CString szItem;
		::GetCurrentUser( szItem );
		strcpy_s( prefs->pszID, 4, szItem );
		::GetCurrentUserDesc( szItem );
		strcpy_s( prefs->pszDesc, 26, szItem );
		::GetDataPathRoot( szItem );
		strcpy_s( prefs->pszPath, _MAX_PATH, szItem );
		::GetBackupPath( szItem );
		strcpy_s( prefs->pszBackup, _MAX_PATH, szItem );
		::GetDelimiter( szItem );
		TRIM( szItem );
		strcpy_s( prefs->pszDelim, 2, szItem );
		if( ::IsGripperModeOn() )
			prefs->ip = Preferences::itGripper;
		else
			prefs->ip = ::IsTabletModeOn() ? Preferences::itTablet : Preferences::itMouse;
		prefs->fLog = ::IsLoggingOn();
		prefs->fLogUI = ::GetLogUI();
		prefs->fLogDB = ::GetLogDB();
		prefs->fLogProc = ::GetLogProc();
		prefs->fLogGraph = ::GetLogGraph();
		prefs->fLogTablet = ::GetLogTablet();
		prefs->fAlpha = ::IsTrialAlphaOn();
		prefs->fPrivate = ::IsPrivateUserOn();
		::GetWinUser( szItem );
		strcpy_s( prefs->pszWinUser, 50, szItem );
		prefs->tm = ::IsDesktopModeOn() ? Preferences::tmDesktop : Preferences::tmTablet;
		prefs->fEntireTablet = ::IsTabletRemapOn();
		::GetUserPassword( szItem );
		strcpy_s( prefs->pszPass, 31, szItem );
		::GetDisplayDimensions( prefs->dD_X, prefs->dD_Y );
		::GetTabletDimensions( prefs->dT_X, prefs->dT_Y );
		::GetCommPort( szItem );
		strcpy_s( prefs->pszComPort, 32, szItem );
		prefs->fAutoUpdate = ::IsAutoUpdateOn();
		strcpy_s( prefs->pszSiteID, 26, ::GetCurrentUserSiteID() );
		strcpy_s( prefs->pszSiteDesc, 51, ::GetCurrentUserSiteDesc() );
		strcpy_s( prefs->pszSignature, 151, ::GetSignature() );

		NSMUsers user;
		if( SUCCEEDED( user.Find( prefs->pszID ) ) )
		{
			user.GetGenerateSubjectIDs( prefs->fGenSubjID );
			user.GetSubjectStartID( szItem );
			strncpy_s( prefs->pszSubjStartID, 4, szItem, _TRUNCATE );
			user.GetSubjectEndID( szItem );
			strncpy_s( prefs->pszSubjEndID, 4, szItem, _TRUNCATE );
			user.GetSubjectCurID( szItem );
			strncpy_s( prefs->pszSubjCurID, 4, szItem, _TRUNCATE );
			user.GetEncrypted( prefs->fEncrypted );
			user.GetEncryptionMethod( prefs->nEncryptionMethod );
		}
	}

	// Gripper Settings
	::GetGripperSettings( gs );
}

//************************************
// Method:    SetPreferences
// FullName:  SetPreferences
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: Preferences * prefs
// Parameter: GripperSettings * gs
//************************************
void SetPreferences( Preferences* prefs, GripperSettings* gs )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( prefs )
	{
		BOOL fLog = ::IsLoggingOn();

		::SetCurrentUser( prefs->pszID );
		::SetCurrentUserDesc( prefs->pszDesc );
		::SetTabletModeOn( prefs->ip == Preferences::itTablet );
		::SetGripperModeOn( prefs->ip == Preferences::itGripper );
		::SetPrivateUserOn( prefs->fPrivate );
		CString szAppPath;
		::GetDataPathRoot( szAppPath );
		szAppPath += _T("\\Actions.log");
		::SetLoggingOn( prefs->fLog, szAppPath );
		::SetLogUI( prefs->fLogUI );
		::SetLogDB( prefs->fLogDB );
		::SetLogProc( prefs->fLogProc );
		::SetLogGraph( prefs->fLogGraph );
		::SetLogTablet( prefs->fLogTablet );
		::SetTrialAlphaOn( prefs->fAlpha );
		::SetDesktopModeOn( prefs->tm == Preferences::tmDesktop );
		::SetTabletRemapOn( prefs->fEntireTablet );
		::SetDelimiter( prefs->pszDelim );
		::SetDataPathRoot( prefs->pszPath );
		::SetBackupPath( prefs->pszBackup );
		::SetUserPassword( prefs->pszPass );
		::SetTabletDimensions( prefs->dT_X, prefs->dT_Y );
		::SetDisplayDimensions( prefs->dD_X, prefs->dD_Y );
		::SetDisplayDimension( prefs->dD_X, prefs->dD_Y );
		::SetCommPort( prefs->pszComPort );
		::SetAutoUpdateOn( prefs->fAutoUpdate );
		::SetCurrentUserSiteID( prefs->pszSiteID );
		::SetCurrentUserSiteDesc( prefs->pszSiteDesc );
		::SetSignature( prefs->pszSignature );

		if( fLog != prefs->fLog )
		{
			IProcess* pProcess = Experiments::GetProcessObject( ::GetOutputWindow() );
			CString szAppPath;
			::GetDataPathRoot( szAppPath );
			szAppPath += _T("\\Actions.log");
			if( pProcess ) pProcess->SetLoggingOn( prefs->fLog, szAppPath.AllocSysString() );
		}
	}

	// Gripper Settings
	if( ::IsGripperInstalled() ) ::SetGripperSettings( gs );
}

//************************************
// Method:    WritePreferences
// FullName:  CMainFrame::WritePreferences
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: Preferences * prefs
// Parameter: GripperSettings * gs
//************************************
BOOL WritePreferences( Preferences* prefs, GripperSettings* gs )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// Preferences
	if( prefs )
	{
		CString szID;
		::GetCurrentUser( szID );
		if( szID == _T("") ) return FALSE;

		INSMUser* pUser = NULL;
		HRESULT hr = CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
			IID_INSMUser, (LPVOID*)&pUser );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create user object.") );
			return FALSE;
		}
		CString szTemp = prefs->pszPath;
		hr = pUser->Find( szID.AllocSysString() );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to locate user.") );
			pUser->Release();
			return FALSE;
		}

		szTemp = prefs->pszID;
		pUser->put_UserID( szTemp.AllocSysString() );
		szTemp = prefs->pszDesc;
		pUser->put_Description( szTemp.AllocSysString() );
		szTemp = prefs->pszPath;
		pUser->put_RootPath( szTemp.AllocSysString() );
		szTemp = prefs->pszBackup;
		pUser->put_BackupPath( szTemp.AllocSysString() );
		szTemp = prefs->pszDelim;
		pUser->put_Delimiter( szTemp.AllocSysString() );
		pUser->put_InputType( prefs->ip );
		pUser->put_TabletMode( prefs->tm );
		pUser->put_EntireTablet( prefs->fEntireTablet );
		pUser->put_Log( prefs->fLog );
		pUser->put_LogUI( prefs->fLogUI );
		pUser->put_LogDB( prefs->fLogDB );
		pUser->put_LogProc( prefs->fLogProc );
		pUser->put_LogGraph( prefs->fLogGraph );
		pUser->put_LogTablet( prefs->fLogTablet );
		pUser->put_Alpha( prefs->fAlpha );
		pUser->put_Private( prefs->fPrivate );
		szTemp = prefs->pszWinUser;
		pUser->put_WinUser( szTemp.AllocSysString() );
		szTemp = prefs->pszPass;
		pUser->put_SysPass( ( Objects::Encode( szTemp ) ).AllocSysString() );
		pUser->put_AspectRatioWidth( prefs->dAR_X );
		pUser->put_AspectRatioHeight( prefs->dAR_Y );
		pUser->put_TabletWidth( prefs->dT_X );
		pUser->put_TabletHeight( prefs->dT_Y );
		pUser->put_DisplayWidth( prefs->dD_X );
		pUser->put_DisplayHeight( prefs->dD_Y );
		szTemp = prefs->pszComPort;
		pUser->put_CommPort( szTemp.AllocSysString() );
		pUser->put_AutoUpdate( prefs->fAutoUpdate );
		pUser->put_GenerateSubjectIDs( prefs->fGenSubjID );
		szTemp = prefs->pszSubjStartID;
		pUser->put_SubjectStartID( szTemp.AllocSysString() );
		szTemp = prefs->pszSubjEndID;
		pUser->put_SubjectEndID( szTemp.AllocSysString() );
		szTemp = prefs->pszSubjCurID;
		pUser->put_SubjectCurID( szTemp.AllocSysString() );
		pUser->put_Encrypted( TRUE );
		pUser->put_EncryptionMethod( (short)::GetEncryptionMethod() );
		szTemp = prefs->pszSiteID;
		pUser->put_SiteID( szTemp.AllocSysString() );
		szTemp = prefs->pszSiteDesc;
		pUser->put_SiteDesc( szTemp.AllocSysString() );
		szTemp = prefs->pszSignature;
		pUser->put_Signature( ( Objects::Encode( szTemp ) ).AllocSysString() );

		hr = pUser->Modify();
		pUser->Release();
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to modify preferences.") );
			return FALSE;
		}
	}

	// Gripper Settings
	return ::WriteGripperPreferences( gs );
}

//************************************
// Method:    WriteGripperPreferences
// FullName:  WriteGripperPreferences
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: Preferences * prefs
// Parameter: GripperSettings * gs
//************************************
BOOL WriteGripperPreferences( GripperSettings* gs )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// Gripper Settings
	if( gs && ::IsGripperInstalled() )
	{
		CFile f;
		CString szWin;
		::GetAllUserPath( szWin );
		CString szGS = szWin;
		szGS += _T("\\gripper.dat");
		if( !f.Open( szGS, CFile::modeWrite | CFile::modeCreate ) )
		{
			BCGPMessageBox( _T("Unable to write gripper settings.") );
			return FALSE;
		}

		f.Write( gs, sizeof( *gs ) );

		f.Close();
	}

	return TRUE;
}

//************************************
// Method:    SendToRecycleBin
// FullName:  SendToRecycleBin
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szFile
//************************************
BOOL SendToRecycleBin( CString szFile )
{
	// shfileopstruct for trash
	SHFILEOPSTRUCT fos;
	memset( (LPVOID)&fos, 0, sizeof( fos ) );
	TCHAR szFrom[ _MAX_PATH ];
	fos.pFrom = szFrom;
	fos.wFunc = FO_DELETE;
	fos.fFlags = FOF_ALLOWUNDO | FOF_NOCONFIRMATION | FOF_FILESONLY;
	// delete
	strcpy_s( szFrom, _MAX_PATH, szFile );
	szFrom[ szFile.GetLength() ] = 0;
	szFrom[ szFile.GetLength() + 1 ] = 0;
	int nRes = ::SHFileOperation( &fos );
	return( nRes == 0 );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// PROCESS ENUMERATION
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

typedef LONG    NTSTATUS;
typedef LONG    KPRIORITY;

#define NT_SUCCESS(Status) ((NTSTATUS)(Status) >= 0)

#define STATUS_INFO_LENGTH_MISMATCH      ((NTSTATUS)0xC0000004L)

#define SystemProcessesAndThreadsInformation    5

typedef struct _CLIENT_ID {
	DWORD         UniqueProcess;
	DWORD         UniqueThread;
} CLIENT_ID;

typedef struct _UNICODE_STRING {
	USHORT        Length;
	USHORT        MaximumLength;
	PWSTR         Buffer;
} UNICODE_STRING;

typedef struct _VM_COUNTERS {
	SIZE_T        PeakVirtualSize;
	SIZE_T        VirtualSize;
	ULONG         PageFaultCount;
	SIZE_T        PeakWorkingSetSize;
	SIZE_T        WorkingSetSize;
	SIZE_T        QuotaPeakPagedPoolUsage;
	SIZE_T        QuotaPagedPoolUsage;
	SIZE_T        QuotaPeakNonPagedPoolUsage;
	SIZE_T        QuotaNonPagedPoolUsage;
	SIZE_T        PagefileUsage;
	SIZE_T        PeakPagefileUsage;
} VM_COUNTERS;

typedef struct _SYSTEM_THREAD_INFORMATION {
	LARGE_INTEGER KernelTime;
	LARGE_INTEGER UserTime;
	LARGE_INTEGER CreateTime;
	ULONG         WaitTime;
	PVOID         StartAddress;
	CLIENT_ID     ClientId;
	KPRIORITY     Priority;
	KPRIORITY     BasePriority;
	ULONG         ContextSwitchCount;
	LONG          State;
	LONG          WaitReason;
} SYSTEM_THREAD_INFORMATION, * PSYSTEM_THREAD_INFORMATION;

typedef struct _SYSTEM_PROCESS_INFORMATION {
	ULONG             NextEntryDelta;
	ULONG             ThreadCount;
	ULONG             Reserved1[6];
	LARGE_INTEGER     CreateTime;
	LARGE_INTEGER     UserTime;
	LARGE_INTEGER     KernelTime;
	UNICODE_STRING    ProcessName;
	KPRIORITY         BasePriority;
	ULONG             ProcessId;
	ULONG             InheritedFromProcessId;
	ULONG             HandleCount;
	ULONG             Reserved2[2];
	VM_COUNTERS       VmCounters;
#if _WIN32_WINNT >= 0x500
	IO_COUNTERS       IoCounters;
#endif
	SYSTEM_THREAD_INFORMATION Threads[1];
} SYSTEM_PROCESS_INFORMATION, * PSYSTEM_PROCESS_INFORMATION;

BOOL EnumProcesses_NtApi( CStringList& lstProcesses )
{
	HINSTANCE hNtDll;
	NTSTATUS (WINAPI * pZwQuerySystemInformation)(UINT, PVOID, 
		ULONG, PULONG);

	// get NTDLL.DLL handle
	hNtDll = GetModuleHandle(_T("ntdll.dll"));
	_ASSERTE(hNtDll != NULL);

	// find ZwQuerySystemInformation address
	*(FARPROC *)&pZwQuerySystemInformation =
		GetProcAddress(hNtDll, "ZwQuerySystemInformation");
	if (pZwQuerySystemInformation == NULL)
		return SetLastError(ERROR_PROC_NOT_FOUND), FALSE;

	// get default process heap handle
	HANDLE hHeap = GetProcessHeap();

	NTSTATUS Status;
	ULONG cbBuffer = 0x8000;
	PVOID pBuffer = NULL;

	// it is difficult to predict what buffer size will be
	// enough, so we start with 32K buffer and increase its
	// size as needed
	do
	{
		pBuffer = HeapAlloc(hHeap, 0, cbBuffer);
		if (pBuffer == NULL)
			return SetLastError(ERROR_NOT_ENOUGH_MEMORY), FALSE;

		Status = pZwQuerySystemInformation(
			SystemProcessesAndThreadsInformation,
			pBuffer, cbBuffer, NULL);

		if (Status == STATUS_INFO_LENGTH_MISMATCH)
		{
			HeapFree(hHeap, 0, pBuffer);
			cbBuffer *= 2;
		}
		else if (!NT_SUCCESS(Status))
		{
			HeapFree(hHeap, 0, pBuffer);
			return SetLastError(Status), FALSE;
		}
	}
	while (Status == STATUS_INFO_LENGTH_MISMATCH);

	PSYSTEM_PROCESS_INFORMATION pProcesses = 
		(PSYSTEM_PROCESS_INFORMATION)pBuffer;

	for (;;)
	{
		PCWSTR pszProcessName = pProcesses->ProcessName.Buffer;
		if (pszProcessName == NULL)
			pszProcessName = L"Idle";

		CHAR szProcessName[MAX_PATH];
		WideCharToMultiByte(CP_ACP, 0, pszProcessName, -1,
			szProcessName, MAX_PATH, NULL, NULL);

		CString szProcess = szProcessName;
		szProcess.MakeUpper();
		lstProcesses.AddTail( szProcess );

		if (pProcesses->NextEntryDelta == 0)
			break;

		// find the address of the next process structure
		pProcesses = (PSYSTEM_PROCESS_INFORMATION)(((LPBYTE)pProcesses)
			+ pProcesses->NextEntryDelta);
	}

	HeapFree(hHeap, 0, pBuffer);
	return TRUE;
}


//************************************
// Method:    IsProcessRunning
// FullName:  IsProcessRunning
// Access:    public 
// Returns:   BOOL AFX_EXT_API
// Qualifier:
// Parameter: CString szProcess
//************************************
BOOL AFX_EXT_API IsProcessRunning( CString szProcess )
{
	CStringList lstProcesses;
	if( EnumProcesses_NtApi( lstProcesses ) )
	{
		szProcess.MakeUpper();
		if( lstProcesses.Find( szProcess ) ) return TRUE;
	}
	return FALSE;
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

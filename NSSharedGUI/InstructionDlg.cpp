// InstructionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "InstructionDlg.h"
#include "..\Common\Shared.h"
#include "..\Common\ImageDataObject.h"

IMPLEMENT_DYNAMIC(InstructionDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(InstructionDlg, CBCGPDialog)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_CBO_FONT, &InstructionDlg::OnCbnSelchangeCboFont)
	ON_CBN_SELCHANGE(IDC_CBO_SIZE, &InstructionDlg::OnCbnSelchangeCboFontSize)
	ON_BN_CLICKED(IDC_BN_COLOR, OnChangeColor)
	ON_BN_CLICKED(IDC_BN_BOLD, OnClickBold)
	ON_BN_CLICKED(IDC_BN_ITALIC, OnClickItalic)
	ON_BN_CLICKED(IDC_BN_UNDERLINE, OnClickUnderline)
	ON_BN_CLICKED(IDC_BN_ALEFT, OnClickAlign)
	ON_BN_CLICKED(IDC_BN_ARIGHT, OnClickAlign)
	ON_BN_CLICKED(IDC_BN_ACENTER, OnClickAlign)
	ON_BN_CLICKED(IDC_BN_PIC, OnClickPic)
	ON_BN_CLICKED(IDC_BN_PRINT, OnClickPrint)
	ON_NOTIFY(EN_SELCHANGE, IDC_RE, OnEnSelchangeRe)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

InstructionDlg::InstructionDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(InstructionDlg::IDD, pParent)
{
	m_fCond = FALSE;
	m_fReadOnly = FALSE;
	m_fOld = FALSE;
	m_fJustDisplay = FALSE;
	m_nTop = 0;
	m_nLeft = 0;
	m_nRightBuffer = 0;
	m_nBottomBuffer = 0;
	m_nTopBuffer = 0;
	m_nBnBuffer = 0;
}

InstructionDlg::~InstructionDlg()
{
}

void InstructionDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RE, m_re);
	DDX_Control(pDX, IDC_CBO_FONT, m_cboFont);
	DDX_Control(pDX, IDC_CBO_SIZE, m_cboFontSize);
	DDX_Control(pDX, IDC_BN_BOLD, m_bnBold);
	DDX_Control(pDX, IDC_BN_ITALIC, m_bnItalic);
	DDX_Control(pDX, IDC_BN_UNDERLINE, m_bnUnderline);
	DDX_Control(pDX, IDC_BN_ALEFT, m_bnLeft );
	DDX_Control(pDX, IDC_BN_ACENTER, m_bnCenter );
	DDX_Control(pDX, IDC_BN_ARIGHT, m_bnRight );
	DDX_Control(pDX, IDC_BN_COLOR, m_bnColor );
	DDX_Control(pDX, IDC_BN_PIC, m_bnPic );
	DDX_Control(pDX, IDC_BN_PRINT, m_bnPrint );
}


// InstructionDlg message handlers

BOOL InstructionDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

CString InstructionDlg::GetFile()
{
	CString szPath;
	if( !m_fJustDisplay )
	{
		::GetDataPathRoot( szPath );
		if( szPath == _T("") )
		{
			m_szFile.Format( _T("%s\\%s.ins"), m_szExpID, m_szFileName );
			m_szFileRTF.Format( _T("%s\\%s.rtf"), m_szExpID, m_szFileName );
		}
		else
		{
			m_szFile.Format( _T("%s\\%s\\%s.ins"), szPath, m_szExpID, m_szFileName );
			m_szFileRTF.Format( _T("%s\\%s\\%s.rtf"), szPath, m_szExpID, m_szFileName );
		}
	}
	else
	{
		m_szFile = m_szFileName;
		if( m_szFileRTF == _T("") ) m_fOld = TRUE;
	}

	return m_szFileRTF;
}

BOOL InstructionDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// dialog title
	CString szOldTitle, szNewTitle;
	GetWindowText( szOldTitle );
	if( !m_fJustDisplay )
	{
		szNewTitle = m_fCond ? _T("Condition ") : _T("Experiment ");
		szNewTitle += szOldTitle;
	}
	else szNewTitle = m_szFileName;
	SetWindowText( szNewTitle );

	// what are we displaying for instruction?
	GetFile();

	CFileFind ff;
	// does file exist? if not, and just displaying, end dialog
	if( m_fReadOnly && !ff.FindFile( m_szFileRTF ) )
	{
		EndDialog( IDOK );
		return FALSE;
	}

	// check if old "INS" file (text) is still around. if so, inform user and prepare to convert/rename
	if( !m_fJustDisplay && ff.FindFile( m_szFile ) )
	{
		BCGPMessageBox( _T("Instruction file is in old text format. Converting to rich text format.\n\nNOTE: Newlines (paragraph breaks) might need to be reinserted.") );
		m_fOld = TRUE;
	}

	// callback function for inserting images into rich edit control
	REOLECallback* mREOLECallback = new REOLECallback;
	::SendMessage( m_re.m_hWnd, EM_SETOLECALLBACK, 0, (LPARAM) mREOLECallback);

	// read file
	if( ff.FindFile( m_szFile ) || ff.FindFile( m_szFileRTF ) )
	{
		try
		{
			CFile f( m_fOld ? m_szFile : m_szFileRTF, CFile::modeRead );
			EDITSTREAM es;
#pragma warning( disable: 4311 )
			es.dwCookie = (DWORD)&f;
#pragma warning( default: 4311 )
			es.pfnCallback = StreamInCallback;
			m_re.StreamIn( m_fOld ? SF_TEXT : SF_RTF, es );
		}
		catch( CFileException* e )
		{
			TCHAR szCause[ 255 ];
			CString szMsg;
			e->GetErrorMessage( szCause, 255 );
			szMsg.Format( _T("The file could not be opened for the following reason\n: %s"), szCause );
			BCGPMessageBox( szMsg );
			e->Delete();
			EndDialog( IDOK );
			return FALSE;
		}
	}

	// readonly if we're presenting before an experiment
	CWnd* pWnd = NULL;
	if( m_fReadOnly || m_fJustDisplay )
	{
		m_re.SetReadOnly();
		pWnd = GetDlgItem( IDC_CBO_FONT );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_SIZE );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_COLOR );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_BOLD );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ITALIC );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_UNDERLINE );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ALEFT );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ACENTER );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_ARIGHT );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_PIC );
		pWnd->EnableWindow( FALSE );
		// hide cancel (if condition)
		if( m_fCond )
		{
			pWnd = GetDlgItem( IDCANCEL );
			pWnd->ShowWindow( SW_HIDE );
		}
	}

	if( !m_fReadOnly )
	{
		// font list selection
		m_cboFont.SelectFont( m_re.GetSelectionFontName() );

		// font size
		for( int i = 6; i <= 48; i++ )
		{
			CString szItem;
			szItem.Format( _T("%d"), i );
			m_cboFontSize.SetItemData( m_cboFontSize.AddString( szItem ), i );
		}
// 21Aug09: GMB: HLT requests default size be 24
//		m_cboFontSize.SelectString( -1, _T("8") );
		m_cboFontSize.SelectString( -1, _T("24") );
		m_re.SetFontSize( 24 );

		// color
		m_bnColor.SetColor( RGB( 0, 0, 0 ) );
		m_bnColor.EnableAutomaticButton( _T("Default"), RGB( 0 , 0, 0 ) );
		m_bnColor.EnableOtherButton( _T("Custom...") );

		// default alignment (left)
		CheckRadioButton( IDC_BN_ALEFT, IDC_BN_ARIGHT, IDC_BN_ALEFT );
	}

	m_re.SetSel( 0, 0 );
	if( !m_fReadOnly )
	{
		pWnd = GetDlgItem( IDOK );
		pWnd->SetWindowText( _T("OK") );
		pWnd = GetDlgItem( IDC_RE );
		pWnd->SetFocus();
	}
	else 
	{
		pWnd = GetDlgItem( IDOK );
		pWnd->SetFocus();
	}

	// button images
	m_bnBold.SetImage( IDB_BOLD );
	m_bnBold.SetCheckedImage( IDB_BOLD );
	m_bnBold.SizeToContent();
	m_bnItalic.SetImage( IDB_ITALIC );
	m_bnItalic.SetCheckedImage( IDB_ITALIC );
	m_bnItalic.SizeToContent();
	m_bnUnderline.SetImage( IDB_UNDERLINE );
	m_bnUnderline.SetCheckedImage( IDB_UNDERLINE );
	m_bnUnderline.SizeToContent();
	m_bnLeft.SetImage( IDB_ALEFT );
	m_bnLeft.SetCheckedImage( IDB_ALEFT );
	m_bnLeft.SizeToContent();
	m_bnCenter.SetImage( IDB_ACENTER );
	m_bnCenter.SetCheckedImage( IDB_ACENTER );
	m_bnCenter.SizeToContent();
	m_bnRight.SetImage( IDB_ARIGHT );
	m_bnRight.SetCheckedImage( IDB_ARIGHT );
	m_bnRight.SizeToContent();
	m_bnPic.SetImage( IDB_PIC );
	m_bnPic.SizeToContent();
	m_bnPrint.SetImage( IDB_PRINT );
	m_bnPrint.SizeToContent();

	// so rich edit control will notify when a selection within has changed
	if( !m_fReadOnly ) ::SendMessage( m_re.m_hWnd, EM_SETEVENTMASK, 0, ENM_SELCHANGE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	pWnd = GetDlgItem( IDC_CBO_FONT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the font for the selected text.") );
	pWnd = GetDlgItem( IDC_CBO_SIZE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the font size for the selected text.") );
	pWnd = GetDlgItem( IDC_BN_COLOR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the font color for the selected text.") );
	pWnd = GetDlgItem( IDC_BN_BOLD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Bold. Toggle the selected text.") );
	pWnd = GetDlgItem( IDC_BN_ITALIC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Italicize. Toggle the selected text.") );
	pWnd = GetDlgItem( IDC_BN_UNDERLINE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Underline. Toggle the selected text.") );
	pWnd = GetDlgItem( IDC_BN_ALEFT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Left-align the paragraph.") );
	pWnd = GetDlgItem( IDC_BN_ACENTER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Center-align the paragraph.") );
	pWnd = GetDlgItem( IDC_BN_ARIGHT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Right-align the paragraph.") );
	pWnd = GetDlgItem( IDC_BN_PIC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Insert a picture at the cursor location.") );
	pWnd = GetDlgItem( IDC_BN_PRINT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Print this document.") );

	 // get initial positions/sizes
	RECT rect, rect2;
	m_re.GetWindowRect( &rect );
	GetWindowRect( &rect2 );
	m_nTopBuffer = rect.top - rect2.top;
	m_nRightBuffer = rect2.right - rect.right;
	m_nBottomBuffer = rect2.bottom - rect.bottom;
	ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	pWnd = GetDlgItem( IDOK );
	if( pWnd )
	{
		m_re.GetWindowRect( &rect );
		pWnd->GetWindowRect( &rect2 );
		m_nBnBuffer = ( rect2.top - rect.bottom );
	}

	EnableVisualManagerStyle( TRUE, TRUE );

	return FALSE;
}

void InstructionDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	if( !m_re.m_hWnd ) return;

	CRect rectWnd, rectRE, rect;
	GetWindowRect( rectWnd );

	m_re.SetWindowPos( NULL, m_nLeft, m_nTop,
					   cx - m_nRightBuffer,
					   rectWnd.Height() - m_nBottomBuffer - m_nTopBuffer,
					   SWP_NOOWNERZORDER | SWP_NOZORDER );
	m_re.GetWindowRect( rectRE );

	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd )
	{
		pWnd->GetWindowRect( rect );

		pWnd->SetWindowPos( NULL,
							cx - rect.Width() - 9 - rect.Width() - 4,
							cy - rect.Height() - m_nBnBuffer + 3,
							0, 0,
							SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE );
	}
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd )
	{
		pWnd->GetWindowRect( rect );

		pWnd->SetWindowPos( NULL,
							cx - rect.Width() - 9,
							cy - rect.Height() - m_nBnBuffer + 3,
							0, 0,
							SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE );
	}
}

void InstructionDlg::OnCancel()
{
	CBCGPDialog::OnCancel();
}

void InstructionDlg::OnOK()
{
	if( !m_fJustDisplay && ( m_fOld || !m_fReadOnly ) ) Convert();
	CBCGPDialog::OnOK();
}

void InstructionDlg::Convert()
{
	// old & new file names
	CString szPath, szFile, szFileRTF;
	::GetDataPathRoot( szPath );
	if( szPath == _T("") )
	{
		szFile.Format( _T("%s\\%s.ins"), m_szExpID, m_szFileName );
		szFileRTF.Format( _T("%s\\%s.rtf"), m_szExpID, m_szFileName );
	}
	else
	{
		szFile.Format( _T("%s\\%s\\%s.ins"), szPath, m_szExpID, m_szFileName );
		szFileRTF.Format( _T("%s\\%s\\%s.rtf"), szPath, m_szExpID, m_szFileName );
	}

	// delete old "INS" file
	if( m_fOld ) CFile::Remove( szFile );

	// write new "RTF" file
	CFile f( szFileRTF, CFile::modeCreate | CFile::modeWrite );
	EDITSTREAM es;
#pragma warning( disable: 4311 )
	es.dwCookie = (DWORD)&f;
#pragma warning( default: 4311 )
	es.pfnCallback = StreamOutCallback;
	m_re.StreamOut( SF_RTF, es );
}

STDMETHODIMP REOLECallback::GetNewStorage(LPSTORAGE FAR *lplpstg)
{
	// Initialize a Storage Object from a DocFile in memory
	LPLOCKBYTES lpLockBytes = NULL;
	SCODE sc = ::CreateILockBytesOnHGlobal(NULL, TRUE, &lpLockBytes);
	if (sc != S_OK) return sc;
	sc = ::StgCreateDocfileOnILockBytes(lpLockBytes, STGM_SHARE_EXCLUSIVE|STGM_CREATE|STGM_READWRITE, 0, lplpstg);
	if (sc != S_OK) lpLockBytes->Release();
	return sc;
} 

void InstructionDlg::OnCbnSelchangeCboFont()
{
	int nItem = m_cboFont.GetCurSel();
	if( nItem < 0 ) return;

	CString szFont;
	m_cboFont.GetLBText( nItem, szFont );
	m_re.SetFontName( szFont );
}

void InstructionDlg::OnCbnSelchangeCboFontSize()
{
	int nItem = m_cboFontSize.GetCurSel();
	if( nItem < 0 ) return;

	m_re.SetFontSize( (int)( m_cboFontSize.GetItemData( nItem ) ) );
}

void InstructionDlg::OnChangeColor()
{
	COLORREF col = m_bnColor.GetColor();
	m_re.SetSelectionColor( col );
}

void InstructionDlg::OnClickAlign()
{
	int nSel = GetCheckedRadioButton( IDC_BN_ALEFT, IDC_BN_ARIGHT );
	if( nSel == IDC_BN_ALEFT ) m_re.SetParagraphLeft();
	else if( nSel == IDC_BN_ACENTER ) m_re.SetParagraphCenter();
	else if( nSel == IDC_BN_ARIGHT ) m_re.SetParagraphRight();
}

void InstructionDlg::OnClickPic()
{
	CString szFilter;
	szFilter = _T("Bitmap files (*.BMP) |*.BMP |");

	CFileDialog d( TRUE, _T("BMP"), _T("*.BMP"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this );
	if( d.DoModal() == IDCANCEL ) return;

	CString szPic = d.GetPathName();
	HBITMAP hBMP = (HBITMAP)LoadImage( AfxGetInstanceHandle(), szPic, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
	if( hBMP )
	{
		IRichEditOle* pREO = m_re.GetIRichEditOle();
		CImageDataObject::InsertBitmap( pREO, hBMP );
		pREO->Release();
	}
}
BOOL InstructionDlg::PrintRich( const CString& Title )
{
	// create a device context to use
	CDC ThePrintDC;
	// print dialog
	CPrintDialog PrintDialog( FALSE );

	if( PrintDialog.DoModal() == IDOK )
		ThePrintDC.Attach( PrintDialog.GetPrinterDC() );
	else return FALSE;

	long CharRange = 0;
	long LastChar = 0;
	DOCINFO di;
	::ZeroMemory( &di, sizeof( DOCINFO ) );
	di.cbSize = sizeof( DOCINFO );
	di.lpszDocName = Title;
	FORMATRANGE fr;
	::ZeroMemory( &fr, sizeof( FORMATRANGE ) );
	HDC hdc = ThePrintDC.GetSafeHdc();
	fr.hdc = hdc;
	fr.hdcTarget = hdc;

	// get all the dimensions of the printer setup
	int nHorizRes = ThePrintDC.GetDeviceCaps( HORZRES ),		//width P in MM
		nVertRes = ThePrintDC.GetDeviceCaps( VERTRES ),			//height in raster lines
		nLogPixelsX = ThePrintDC.GetDeviceCaps( LOGPIXELSX ),	//pixels per inch along x
		nLogPixelsY = ThePrintDC.GetDeviceCaps( LOGPIXELSY );	//pixels per inch along y
	// set the printable area of printer in the FormatRange struct
	fr.rcPage.left = 0;
	fr.rcPage.top = 0;
	fr.rcPage.right    = (nHorizRes/nLogPixelsX) * 1440;
	fr.rcPage.bottom   = (nVertRes/nLogPixelsY) * 1440;
	// Set up some margins all around. Make them one inch - results vary on printers depending on setup
	fr.rc.left   = fr.rcPage.left + 1440;  // 1440 TWIPS = 1 inch.
	fr.rc.top    = fr.rcPage.top + 1440;
	fr.rc.right  = fr.rcPage.right - 1440;
	fr.rc.bottom = fr.rcPage.bottom - 1440;
	//select all text for printing
	CHARRANGE cr;
	cr.cpMin = 0;
	cr.cpMax = -1;	//-1 selects all
	fr.chrg = cr;
	//get length of document, used for more than one page
	CharRange = m_re.GetTextLength();
	int ErrorStatus = 0;
	//Start Printing
	ThePrintDC.StartDoc( &di );
	do
	{
		ThePrintDC.StartPage();
		LastChar = m_re.FormatRange( &fr, TRUE );
		ErrorStatus=ThePrintDC.EndPage();
		cr.cpMin = LastChar;
		cr.cpMax = CharRange;
		fr.chrg = cr;
	}
	while( LastChar = 0 );

	switch( ErrorStatus )
	{
		case SP_ERROR:
		{
			ThePrintDC.AbortDoc();
			BCGPMessageBox("There was a general printing error, please check printer is working properly, connected, on line etc.", MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		case SP_APPABORT:
		case SP_USERABORT:
		{
			ThePrintDC.AbortDoc();
			return FALSE;
		}
		case SP_OUTOFDISK:
		{
			ThePrintDC.AbortDoc();
			BCGPMessageBox("The print spooler is out of disk space, free up some disk space and try again.", MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		case SP_OUTOFMEMORY:
		{
			ThePrintDC.AbortDoc();
			BCGPMessageBox("Your computer is out of memory, shut down some applications and/or some windows and try again.", MB_OK | MB_ICONEXCLAMATION);
			return FALSE;
		}
		default:
		{
			ThePrintDC.EndDoc();
			return TRUE;
		}
	}
}

void InstructionDlg::OnClickPrint()
{
	CString szTitle;
	GetWindowText( szTitle );
	PrintRich( szTitle );
}

void InstructionDlg::OnEnSelchangeRe(NMHDR *pNMHDR, LRESULT *pResult)
{
	if( m_fReadOnly ) return;

	SELCHANGE *pSelChange = reinterpret_cast<SELCHANGE *>(pNMHDR);

	// update based upon selection
	CString szItem = m_re.GetSelectionFontName();
	if( !m_cboFont.SelectFont( szItem ) ) m_cboFont.SelectFont( _T("Times New Roman") );
	szItem.Format( _T("%u"), m_re.GetSelectionFontSize() );
	m_cboFontSize.SelectString( -1, szItem );
	COLORREF col = m_re.GetSelectionColor();
	m_bnColor.SetColor( col );
	m_bnBold.SetCheck( m_re.SelectionIsBold() ? BST_CHECKED : BST_UNCHECKED );
	m_bnItalic.SetCheck( m_re.SelectionIsItalic() ? BST_CHECKED : BST_UNCHECKED );
	m_bnUnderline.SetCheck( m_re.SelectionIsUnderlined() ? BST_CHECKED : BST_UNCHECKED );
	int nSel = IDC_BN_ALEFT;
	if( m_re.ParagraphIsCentered() ) nSel = IDC_BN_ACENTER;
	else if( m_re.ParagraphIsRight() ) nSel = IDC_BN_ARIGHT;
	CheckRadioButton( IDC_BN_ALEFT, IDC_BN_ARIGHT, nSel );

	*pResult = 0;
}

BOOL InstructionDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	if( !m_fJustDisplay )
	{
		if( !m_fCond ) ::GoToHelp( _T("experiment_instructions.html"), this );
		else ::GoToHelp( _T("condition_instructions.html"), this );
	}
	else ::GoToHelp( _T("viewingtrials.html"), this );

	return CBCGPDialog::OnHelpInfo( pHelpInfo );
}

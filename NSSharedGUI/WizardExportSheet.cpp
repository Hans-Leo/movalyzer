// WizardExportSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardExportSheet.h"
#include "WizardExportExp.h"
#include "WizardExportSettings.h"
#include "WizardExportSummary.h"
#include "WizardExportUploadSubj.h"
#include "WizardExportSave.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardExportSheet

IMPLEMENT_DYNAMIC(WizardExportSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardExportSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardExportSheet::WizardExportSheet( CWnd* pParentWnd, UINT iSelectPage,
									  CString szExpID, CString szSubjID, CString szGrpID )
	: CBCGPPropertySheet( _T("Subject Export Wizard"), pParentWnd, iSelectPage )
{
	m_szExpID = szExpID;
	m_szSubjID = szSubjID;
	m_szGrpID = szGrpID;
	AddPages();
}

WizardExportSheet::~WizardExportSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void WizardExportSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, FALSE );

	AddPage( new WizardExportExp( m_szExpID, m_szSubjID, m_szGrpID ) );
	AddPage( new WizardExportSettings );
	AddPage( new WizardExportSummary );
	AddPage( new WizardExportUploadSubj );
	AddPage( new WizardExportSave );

	SetWizardMode();
}

/////////////////////////////////////////////////////////////////////////////
// WizardExportSheet message handlers

BOOL WizardExportSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardExportExp ) ) )
			return ((WizardExportExp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardExportSettings ) ) )
			return ((WizardExportSettings*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardExportSummary ) ) )
			return ((WizardExportSummary*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardExportUploadSubj ) ) )
			return ((WizardExportUploadSubj*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardExportSave ) ) )
			return ((WizardExportSave*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

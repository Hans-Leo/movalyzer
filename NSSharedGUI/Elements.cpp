// Elements.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ElementSheet.h"
#include "OverwriteDlg.h"
#include "Elements.h"
#include "Stimuli.h"
#include <direct.h>
#include "Cats.h"
#include "GraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CStringList	_lstExportedCats;

///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	Elements::GetDescriptionWithID( m_pElement, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

int Elements::GenerateXYZ( double** x, double** y, double**z, BOOL fDelete )
{
	return Elements::GenerateXYZ( m_pElement, x, y, z, fDelete );
}

///////////////////////////////////////////////////////////////////////////////

int Elements::GenerateXYZWithError( double** x, double** y, double**z, BOOL fDelete )
{
	return Elements::GenerateXYZWithError( m_pElement, x, y, z, fDelete );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::GenerateFile( CString& szFile, int& nLines )
{
	return Elements::GenerateFile( m_pElement, szFile, nLines );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ViewData()
{
	Elements::ViewData( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ChartData()
{
	Elements::ChartData( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::GetNext()
{
	return Elements::GetNext( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Find( CString szID )
{
	return Elements::Find( m_pElement, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Add()
{
	return Elements::Add( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Modify()
{
	return Elements::Modify( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Remove()
{
	return Elements::Remove( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::SetDataPath( CString szPath )
{
	Elements::SetDataPath( m_pElement, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Elements::GetID( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDescription( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDescriptionWithID( IElement* pElement, CString& szVal, BOOL fReverse )
{
	if( pElement )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		Elements::GetID( pElement, szID );
		pElement->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

#pragma warning( disable: 4996 )	// disable deprecated warning
int Elements::GenerateXYZ( IElement* pElement, double** x, double** y, double**z, BOOL fDelete )
{
	if( !pElement ) return 0;
	int nLines = 0;

	// Center/Widths/Pattern
	double cx = 0., cy = 0., wx = 0., wy = 0.;
	double *dx = NULL, *dy = NULL, *dz = NULL;
	Elements::GetCenterX( pElement, cx );
	Elements::GetCenterY( pElement, cy );
	Elements::GetWidthX( pElement, wx );
	Elements::GetWidthY( pElement, wy );
	BOOL fIsImage = FALSE;
	Elements::GetIsImage( pElement, fIsImage );
	CString szPattern;
	Elements::GetPattern( pElement, szPattern );

	// If pattern, loop through file twice (once to get count, 2nd to assign vals)
	if( !fIsImage && ( szPattern != _T("") ) )
	{
		CString szLine;
		CStdioFile f;
		if( f.Open( szPattern, CFile::modeRead ) )
		{
			while( f.ReadString( szLine ) )
				nLines++;

			f.Close();
		}

		if( nLines > 0 )
		{
			// 1st line should have a 0 pressure for separation
			nLines++;
			dx = new double[ nLines ];
			dy = new double[ nLines ];
			dz = new double[ nLines ];
			double nx = 0., ny = 0., nz = 0.;
			int nIdx = 0;

			f.Open( szPattern, CFile::modeRead );
			while( f.ReadString( szLine ) && ( nIdx < nLines ) )
			{
				// 1st line should have a 0 pressure for separation
				if( nIdx == 0 )
				{
					sscanf( szLine, "%lf %lf %lf", &nx, &ny, &nz );
					dx[ nIdx ] = nx;
					dy[ nIdx ] = ny;
					dz[ nIdx ] = 0;
					nIdx++;
				}
				sscanf( szLine, "%lf %lf %lf", &nx, &ny, &nz );
				dx[ nIdx ] = nx;
				dy[ nIdx ] = ny;
				dz[ nIdx ] = nz;

				nIdx++;
			}
			f.Close();
		}
	}
	else	// Do shape calcs
	{
		// Data object to hold points for file
		// ...1 extra line per shape to indicate 0 pressure so draws correctly
		int nPoints = 5;	// TODO: 5 points for a rectangle - other shapes come later
		nLines = nPoints + 1;
		dx = new double[ nLines ];
		dy = new double[ nLines ];
		dz = new double[ nLines ];

		double* px = new double[ nPoints ];
		double* py = new double[ nPoints ];
		int xmod, ymod;
		// Data points
		for( int i = 0; i < nPoints; i++ )
		{
			switch( i )
			{
				case 0: xmod = -1; ymod = -1; break;
				case 1: xmod = -1; ymod = 1; break;
				case 2: xmod = 1; ymod = 1; break;
				case 3: xmod = 1; ymod = -1; break;
				case 4: xmod = -1; ymod = -1; break;
			}

			px[ i ] = cx + ( xmod * wx );
			py[ i ] = cy + ( ymod * wy );
		}
		// For each point
		for( int p = 0; p < nLines; p++ )
		{
			// 1st line is for 0-pressure
			if( p == 0 )
			{
				dx[ p ] = px[ p ];
				dy[ p ] = py[ p ];
				dz[ p ] = 0;
			}
			else
			{
				dx[ p ] = px[ p - 1 ];
				dy[ p ] = py[ p - 1 ];
				dz[ p ] = 100;
			}
		}

		delete [] px;
		delete [] py;
	}

	if( !fDelete )
	{
		if( x ) *x = dx;
		if( y ) *y = dy;
		if( z ) *z = dz;
	}
	else
	{
		delete [] dx;
		delete [] dy;
		delete [] dz;
	}

	return nLines;
}
#pragma warning( default: 4996 )	// disable deprecated warning

///////////////////////////////////////////////////////////////////////////////

int Elements::GenerateXYZWithError( IElement* pElement, double** x, double** y, double**z, BOOL fDelete )
{
	if( !pElement ) return 0;
	int nLines = 0;

	// only process if a target
	BOOL fIsTarget = FALSE;
	Elements::GetIsTarget( pElement, fIsTarget );
	if( !fIsTarget ) return 0;

	// error margins
	double dErrX = 0., dErrY = 0.;
	Elements::GetErrorX( pElement, dErrX );
	Elements::GetErrorY( pElement, dErrY );

	// get bounding perimeter (rectangle only now - nlines should only be 4 (ignoring 1st & last) )
	nLines = Elements::GenerateXYZ( pElement, x, y, z, fDelete );
	if( nLines <= 0 ) return 0;
	if( !x || !y || !z ) return 0;

	// now add errors (skipping first)
	// order of data = LL, UL, UR, LR
	double *xx = *x, *yy = *y, *zz = *z;
	for( int i = 0; i < nLines; i++ )
	{
		// TODO: this might change with different shapes
		switch( i )
		{
			// lower left
			case 1:
				xx[ i ] -= dErrX;
				yy[ i ] -= dErrY;
				break;
			// upper left
			case 2:
				xx[ i ] -= dErrX;
				yy[ i ] += dErrY;
				break;
			// upper right
			case 3:
				xx[ i ] += dErrX;
				yy[ i ] += dErrY;
				break;
			// lower right
			case 4:
				xx[ i ] += dErrX;
				yy[ i ] -= dErrY;
				break;
			default:
				break;
		}
	}

	return nLines;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateTargets( CStdioFile* pFile )
{
	Elements::GenerateTargets( m_pElement, pFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateTargets( IElement* pElement, CStdioFile* pFile )
{
	if( !pElement || !pFile ) return;

	double *xx = NULL, *yy = NULL, *zz = NULL;
	int nLines = Elements::GenerateXYZWithError( pElement, &xx, &yy, &zz );
	if( nLines > 0 )
	{
		CString szItem;
		// TODO: this is only for rectangle now (skipping 1st & last lines)
		for( int i = 1; i < ( nLines - 1 ); i++ )
		{
			szItem.Format( _T("%lf %lf\n"), xx[ i ], yy[ i ] );
			WRITE_STRING( pFile, szItem );
		}

		// cleanup
		delete [] xx;
		delete [] yy;
		delete [] zz;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateBehavior( CStdioFile* pFile )
{
	Elements::GenerateDecorations( m_pElement, pFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateBehavior( IElement* pElement, CStdioFile* pFile )
{
	if( !pElement || !pFile ) return;

	// WRITE TO FILE
	CString szItem, szItem1;
	double dVal = 0.;
	BOOL fVal = FALSE;
	// ID of element
	Elements::GetID( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
	// Is Target
	Elements::GetIsTarget( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Start time
	Elements::GetStart( pElement, dVal );
	szItem.Format( _T("%g\n"), dVal );
	WRITE_STRING( pFile, szItem );
	// Duration
	Elements::GetDuration( pElement, dVal );
	szItem.Format( _T("%g\n"), dVal );
	WRITE_STRING( pFile, szItem );
	// Hide right target
	Elements::GetHideRightTarget( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Hide wrong target
	Elements::GetHideWrongTarget( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Hide if any right target
	Elements::GetHideIfAnyRightTarget( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Hide if any wrong target
	Elements::GetHideIfAnyWrongTarget( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Right target
	Elements::GetRightTarget( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
	// Wrong target
	Elements::GetWrongTarget( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateAnimation( CStdioFile* pFile )
{
	Elements::GenerateAnimation( m_pElement, pFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateAnimation( IElement* pElement, CStdioFile* pFile )
{
	if( !pElement || !pFile ) return;

	// WRITE TO FILE
	CString szItem, szItem1;
	double dVal = 0.;
	BOOL fVal = FALSE;
	// ID of element
	Elements::GetID( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
	// Is Animated
	Elements::GetAnimate( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Sampling Rate
	Elements::GetAnimationRate( pElement, dVal );
	szItem.Format( _T("%g\n"), dVal );
	WRITE_STRING( pFile, szItem );
	// Animation File
	Elements::GetAnimationFile( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
	// Random File
	Elements::GetAnimationRandom( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Generate Random File
	Elements::GetAnimationGenerate( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal ? 1 : 0 );
	WRITE_STRING( pFile, szItem );
	// Exp/Grp/Subj
	Elements::GetAnimationSubjID( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateDecorations( CStdioFile* pFile, int nLines )
{
	Elements::GenerateDecorations( m_pElement, pFile, nLines );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GenerateDecorations( IElement* pElement, CStdioFile* pFile, int nLines )
{
	if( !pElement || !pFile ) return;

	// TODO: (maybe) if no #of lines provided (-1), calculate self
	if( nLines == -1 ) return;

	// WRITE TO FILE
	short nVal1 = 0, nVal2 = 0, nVal3 = 0;
	DWORD dwVal1 = 0, dwVal2 = 0, dwVal3 = 0;
	CString szItem1, szItem2, szItem3, szItem, szTemp;
	BOOL fText2 = FALSE, fText3 = FALSE, fVal = FALSE;
	// ID of element
	Elements::GetID( pElement, szItem1 );
	szItem.Format( _T("%s\n"), szItem1 );
	WRITE_STRING( pFile, szItem );
	// Number of lines in data file
	szItem.Format( _T("%d\n"), nLines );
	WRITE_STRING( pFile, szItem );
	// Line sizes
	Elements::GetLine1Size( pElement, nVal1 );
	Elements::GetLine2Size( pElement, nVal2 );
	Elements::GetLine3Size( pElement, nVal3 );
	szItem.Format( _T("%d %d %d\n"), nVal1, nVal2, nVal3 );
	WRITE_STRING( pFile, szItem );
	// Line colors
	Elements::GetLine1Color( pElement, dwVal1 );
	Elements::GetLine2Color( pElement, dwVal2 );
	Elements::GetLine3Color( pElement, dwVal3 );
	szItem.Format( _T("%u %u %u\n"), dwVal1, dwVal2, dwVal3 );
	WRITE_STRING( pFile, szItem );
	// Background colors
	Elements::GetBackground1Color( pElement, dwVal1 );
	Elements::GetBackground2Color( pElement, dwVal2 );
	Elements::GetBackground3Color( pElement, dwVal3 );
	szItem.Format( _T("%u %u %u\n"), dwVal1, dwVal2, dwVal3 );
	WRITE_STRING( pFile, szItem );
	// Text
	Elements::GetText1( pElement, szItem1 );
	Elements::GetText2( pElement, szItem2 );
	fText2 = ( szItem2 != _T("") );
	Elements::GetText3( pElement, szItem3 );
	fText3 = ( szItem3 != _T("") );
	szItem.Format( _T("%s;%s;%s\n"), szItem1, szItem2, szItem3 );
	WRITE_STRING( pFile, szItem );
	// Text sizes
	Elements::GetText1Size( pElement, nVal1 );
	if( fText2 ) Elements::GetText2Size( pElement, nVal2 );
	else nVal2 = 0;
	if( fText3 ) Elements::GetText3Size( pElement, nVal3 );
	else nVal3 = 0;
	szItem.Format( _T("%d %d %d\n"), nVal1, nVal2, nVal3 );
	WRITE_STRING( pFile, szItem );
	// Text colors
	Elements::GetText1Color( pElement, dwVal1 );
	if( fText2 ) Elements::GetText2Color( pElement, dwVal2 );
	else dwVal2 = 0;
	if( fText3 ) Elements::GetText3Color( pElement, dwVal3 );
	else dwVal3 = 0;
	szItem.Format( _T("%u %u %u\n"), dwVal1, dwVal2, dwVal3 );
	WRITE_STRING( pFile, szItem );
	// LOGFONT STUFF
	LOGFONT* plf;
	CString szFont1, szFont2, szFont3;
	// Write font1 info
	pElement->get_Font1( (VARIANT**)&plf );
	if( plf )
	{
		szFont1 = plf->lfFaceName;
		szItem.Format( _T("%u %d %d %d %d\n"),
					   plf->lfWeight,
					   plf->lfItalic,
					   plf->lfUnderline,
					   plf->lfStrikeOut,
					   plf->lfPitchAndFamily );
	}
	else szItem.Format( _T("%u %d %d %d %d\n"),
						FW_NORMAL, 0, 0, 0, FF_MODERN | DEFAULT_PITCH );
	WRITE_STRING( pFile, szItem );
	// Write font2 info
	pElement->get_Font2( (VARIANT**)&plf );
	if( plf )
	{
		szFont2 = plf->lfFaceName;
		szItem.Format( _T("%u %d %d %d %d\n"),
					   plf->lfWeight,
					   plf->lfItalic,
					   plf->lfUnderline,
					   plf->lfStrikeOut,
					   plf->lfPitchAndFamily );
	}
	else szItem.Format( _T("%u %d %d %d %d\n"),
						FW_NORMAL, 0, 0, 0, FF_MODERN | DEFAULT_PITCH );
	WRITE_STRING( pFile, szItem );
	// Write font3 info
	pElement->get_Font3( (VARIANT**)&plf );
	if( plf )
	{
		szFont3 = plf->lfFaceName;
		szItem.Format( _T("%u %d %d %d %d\n"),
					   plf->lfWeight,
					   plf->lfItalic,
					   plf->lfUnderline,
					   plf->lfStrikeOut,
					   plf->lfPitchAndFamily );
	}
	else szItem.Format( _T("%u %d %d %d %d\n"),
						FW_NORMAL, 0, 0, 0, FF_MODERN | DEFAULT_PITCH );
	WRITE_STRING( pFile, szItem );
	// Write font faces
	szItem.Format( _T("%s;%s;%s\n"), szFont1, szFont2, szFont3 );
	WRITE_STRING( pFile, szItem );
	// Is pattern?
	Elements::GetIsImage( pElement, fVal );
	Elements::GetPattern( pElement, szItem1 );
	fVal = fVal ? 0 : ( szItem1 != _T("") );
	szItem.Format( _T("%d\n"), fVal );
	WRITE_STRING( pFile, szItem );
	// Image info
	Elements::GetIsImage( pElement, fVal );
	szItem.Format( _T("%d\n"), fVal );
	WRITE_STRING( pFile, szItem );
	if( fVal )
	{
		Elements::GetPattern( pElement, szTemp );
		Stimuluss::GetBaseFile( szItem );
		szItem += szTemp;
	}
	else szItem = _T("");
	szItem += _T("\n");
	WRITE_STRING( pFile, szItem );
	// Centers
	double dVal1, dVal2;
	Elements::GetCenterX( pElement, dVal1 );
	Elements::GetCenterY( pElement, dVal2 );
	szItem.Format( _T("%f %f\n"), dVal1, dVal2 );
	WRITE_STRING( pFile, szItem );
	// Shape
	Elements::GetShape( pElement, nVal1 );
	szItem.Format( _T("%d\n"), nVal1 );
	WRITE_STRING( pFile, szItem );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::GenerateFile( IElement* pElement, CString& szFile, int& nLines )
{
	if( !pElement ) return FALSE;
	BOOL fRet = FALSE;

	// ensure directory first
	Elements::GetDataFile( pElement, szFile );

	// generate
	double *x = NULL, *y = NULL, *z = NULL;
	CString szItem;
	nLines = Elements::GenerateXYZ( pElement, &x, &y, &z );
	if( nLines > 0 )
	{
		// Write file
		CStdioFile f;
		if( f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
		{
			// Write to file
			for( int p = 0; p < nLines; p++ )
			{
				szItem.Format( _T("%lf %lf %lf\n"), x[ p ], y[ p ], z[ p ] );
				f.WriteString( szItem );
			}
			f.Close();

			fRet = TRUE;
		}

		// Cleanup
		delete [] x;
		delete [] y;
		delete [] z;
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::GenerateFiles()
{
	return Elements::GenerateFiles( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::GenerateFiles( IElement* pElement )
{
	if( !pElement ) return FALSE;

	CString szData, szDecor, szBehav, szTarg, szAnim;
	Elements::GetDataFile( pElement, szData );
	Elements::GetDecFile( pElement, szDecor );
	Elements::GetBehaviorFile( pElement, szBehav );
	Elements::GetTargetFile( pElement, szTarg );
	Elements::GetAnimationsFile( pElement, szAnim );

	// generate data file
	int nLines = 0;
	if( !Elements::GenerateFile( pElement, szData, nLines ) ) return FALSE;
	if( nLines == 0 ) return FALSE;

	// generate decorations/appearance file
	CStdioFile f;
	if( f.Open( szDecor, CFile::modeWrite | CFile::modeCreate ) )
		Elements::GenerateDecorations( pElement, &f, nLines );
	else
		return FALSE;
	f.Close();

	// generate behavior file
	if( f.Open( szBehav, CFile::modeWrite | CFile::modeCreate ) )
		Elements::GenerateBehavior( pElement, &f );
	else
		return FALSE;
	f.Close();

	// generate target file
	if( f.Open( szTarg, CFile::modeWrite | CFile::modeCreate ) )
		Elements::GenerateTargets( pElement, &f );
	else
		return FALSE;
	f.Close();

	// generate animations file
	if( f.Open( szAnim, CFile::modeWrite | CFile::modeCreate ) )
		Elements::GenerateAnimation( pElement, &f );
	else
		return FALSE;
	f.Close();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBaseFile( CString& szBase )
{
	::GetDataPathRoot( szBase );
	szBase += _T("\\stimuli\\");
	// ensure exists
	_mkdir( szBase );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDataFile( CString& szFile )
{
	Elements::GetDataFile( m_pElement, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDataFile( IElement* pElement, CString& szFile )
{
	if( !pElement ) return;

	CString szPath, szID;
	Elements::GetBaseFile( szPath );
	Elements::GetID( pElement, szID );
	szPath += szID;
	szPath += _T(".STI");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDecFile( CString& szFile )
{
	Elements::GetDecFile( m_pElement, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDecFile( IElement* pElement, CString& szFile )
{
	if( !pElement ) return;

	CString szPath, szID;
	Elements::GetBaseFile( szPath );
	Elements::GetID( pElement, szID );
	szPath += szID;
	szPath += _T(".FMT");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBehaviorFile( CString& szFile )
{
	Elements::GetBehaviorFile( m_pElement, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBehaviorFile( IElement* pElement, CString& szFile )
{
	if( !pElement ) return;

	CString szPath, szID;
	Elements::GetBaseFile( szPath );
	Elements::GetID( pElement, szID );
	szPath += szID;
	szPath += _T(".BEH");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetTargetFile( CString& szFile )
{
	Elements::GetTargetFile( m_pElement, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetTargetFile( IElement* pElement, CString& szFile )
{
	if( !pElement ) return;

	CString szPath, szID;
	Elements::GetBaseFile( szPath );
	Elements::GetID( pElement, szID );
	szPath += szID;
	szPath += _T(".TGT");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDataFileFromID( CString szID, CString& szFile )
{
	Elements::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".STI");
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationsFile( CString& szFile )
{
	Elements::GetTargetFile( m_pElement, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationsFile( IElement* pElement, CString& szFile )
{
	if( !pElement ) return;

	CString szPath, szID;
	Elements::GetBaseFile( szPath );
	Elements::GetID( pElement, szID );
	szPath += szID;
	szPath += _T(".ANI");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationsFileFromID( CString szID, CString& szFile )
{
	Elements::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".ANI");
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDecFileFromID( CString szID, CString& szFile )
{
	Elements::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".FMT");
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBehaviorFileFromID( CString szID, CString& szFile )
{
	Elements::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".BEH");
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetTargetFileFromID( CString szID, CString& szFile )
{
	Elements::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".TGT");
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ViewData( IElement* pElement )
{
	CString szFile;
	int nLines;
	if( !Elements::GenerateFile( pElement, szFile, nLines ) ) return;

	// Display file
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ChartData( IElement* pElement )
{
	CString szFile;
	int nLines;
	if( !Elements::GenerateFile( pElement, szFile, nLines ) ) return;

	// Chart file
	CGraphDlg gd;
	gd.ChartHwr( _T(""), _T(""), _T(""), szFile,
		::GetDeviceResolution(), ::GetMinPenPressure() );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetPattern( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_Pattern( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetShape( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Shape( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetCenterX( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_CenterX( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetCenterY( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_CenterY( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetWidthX( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_WidthX( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetWidthY( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_WidthY( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetIsTarget( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_IsTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetErrorX( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_ErrorX( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetErrorY( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_ErrorY( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetLine1Size( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Line1Size( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetLine2Size( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Line2Size( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetLine3Size( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Line3Size( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText1Size( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Text1Size( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText2Size( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Text2Size( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText3Size( IElement* pElement, short& nVal )
{
	if( pElement ) pElement->get_Text3Size( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetLine1Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Line1Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetLine2Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Line2Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetLine3Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Line3Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBackground1Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Background1Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBackground2Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Background2Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetBackground3Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Background3Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText1Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Text1Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText2Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Text2Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText3Color( IElement* pElement, DWORD& dwVal )
{
	if( pElement ) pElement->get_Text3Color( &dwVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText1( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_Text1( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText2( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_Text2( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetText3( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_Text3( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetIsImage( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_IsImage( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetStart( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_Start( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetDuration( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_Duration( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetHideRightTarget( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_HideRightTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetHideWrongTarget( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_HideWrongTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetHideIfAnyRightTarget( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_HideIfAnyRightTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetHideIfAnyWrongTarget( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_HideIfAnyWrongTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetRightTarget( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_RightTarget( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetWrongTarget( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_WrongTarget( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetCatID( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_CatID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimate( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_Animate( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationRate( IElement* pElement, double& dVal )
{
	if( pElement ) pElement->get_AnimationRate( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationFile( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_AnimationFile( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationRandom( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_AnimationRandom( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationGenerate( IElement* pElement, BOOL& fVal )
{
	if( pElement ) pElement->get_AnimationGenerate( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::GetAnimationSubjID( IElement* pElement, CString& szVal )
{
	if( pElement )
	{
		BSTR bstrItem;
		pElement->get_AnimationSubjID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::ResetToStart()
{
	return Elements::ResetToStart( m_pElement );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::ResetToStart( IElement* pElement )
{
	if( pElement ) return pElement->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::GetNext( IElement* pElement )
{
	if( pElement ) return pElement->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Find( IElement* pElement, CString szID )
{
	if( pElement ) return pElement->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Add( IElement* pElement )
{
	if( !pElement ) return E_FAIL;

	if( FAILED( pElement->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Elements::Add") );

		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Modify( IElement* pElement )
{
	if( !pElement ) return E_FAIL;

	if( FAILED( pElement->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Elements::Remove( IElement* pElement )
{
	if( !pElement ) return E_FAIL;

	if( FAILED( pElement->Remove() ) )
	{
		BCGPMessageBox( _T("Unable to delete element.") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::SetDataPath( IElement* pElement, CString szPath )
{
	if( pElement ) pElement->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoAdd( CWnd* pOwner )
{
	return Elements::DoAdd( m_pElement, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoAdd( IElement* pElement, CWnd* pOwner )
{
	if( !pElement ) return FALSE;

	ElementSheet d( pElement, TRUE, FALSE, pOwner );
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = Elements::Add( pElement );
		if( SUCCEEDED( hr ) )
		{
			GenerateFiles( pElement );
			return TRUE;
		}
		else {
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Elements::DoAdd") );
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoEdit( CString szID, CWnd* pOwner )
{
	return Elements::DoEdit( m_pElement, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoEdit( IElement* pElement, CString szID, CWnd* pOwner )
{
	if( !pElement ) return FALSE;

	CString szDesc, szNotes;
	BOOL fTarget = FALSE;

	if( FAILED( Elements::Find( pElement, szID ) ) ) return FALSE;
	ElementSheet d( pElement, FALSE, FALSE, pOwner );
	if( FAILED( Elements::Find( pElement, szID ) ) ) return FALSE;
	Elements::GetDescription( pElement, szDesc );
	Elements::GetIsTarget( pElement, fTarget );
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = Elements::Modify( pElement );
		if( SUCCEEDED( hr ) )
		{
			// check if was target and now is not (remove from target db)
			if( fTarget )
			{
				Elements::GetIsTarget( pElement, fTarget );
				if( !fTarget )
				{
					IStimulusTarget* pST = NULL;
					hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
											 IID_IStimulusTarget, (LPVOID*)&pST );
					if( SUCCEEDED( hr ) )
					{
						BSTR bstr;
						CString szCurID;
						hr = pST->ResetToStart();
						while( SUCCEEDED( hr ) )
						{
							pST->get_ElementID( &bstr );
							szCurID = bstr;
							if( szCurID == szID )
							{
								hr = pST->Remove();
								if( FAILED( hr ) )
									BCGPMessageBox( _T("Unable to remove target from link table.") );
							}
							hr = pST->GetNext();
						}

						pST->Release();
					}
					else BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL Elements::Export( CMemFile* pFile, BOOL fFirst )
{
	if( !m_pElement || !pFile ) return FALSE;

	CString szItem, szCatID;
	double dVal;
	short nVal;
	BOOL fVal;
	DWORD dwVal;

	WRITE_STRING( pFile, TAG_ELEMENT );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	Elements::GetID( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Desc
	Elements::GetDescription( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Pattern
	Elements::GetPattern( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Shape
	Elements::GetShape( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Center X
	Elements::GetCenterX( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Center Y
	Elements::GetCenterY( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Width X
	Elements::GetWidthX( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Width Y
	Elements::GetWidthY( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Is Target
	Elements::GetIsTarget( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Error X
	Elements::GetErrorX( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Error Y
	Elements::GetErrorY( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Line 1 Size
	Elements::GetLine1Size( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Line 2 Size
	Elements::GetLine2Size( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Line 3 Size
	Elements::GetLine3Size( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Line 1 Color
	Elements::GetLine1Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Line 2 Color
	Elements::GetLine2Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Line 3 Color
	Elements::GetLine3Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Background 1 Color
	Elements::GetBackground1Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Background 2 Color
	Elements::GetBackground2Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Background 3 Color
	Elements::GetBackground3Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 1
	Elements::GetText1( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 2
	Elements::GetText2( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 3
	Elements::GetText3( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 1 Color
	Elements::GetText1Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 2 Color
	Elements::GetText2Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 3 Color
	Elements::GetText3Color( m_pElement, dwVal );
	szItem.Format( _T("%u"), dwVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 1 Size
	Elements::GetText1Size( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 2 Size
	Elements::GetText2Size( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Text 3 Size
	Elements::GetText3Size( m_pElement, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Is Image
	Elements::GetIsImage( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Start Time
	Elements::GetStart( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Duration
	Elements::GetDuration( m_pElement, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide Right Target
	Elements::GetHideRightTarget( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide Wrong Target
	Elements::GetHideWrongTarget( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide If Any Right Target
	Elements::GetHideIfAnyRightTarget( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide If Any Wrong Target
	Elements::GetHideIfAnyWrongTarget( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Right Target
	Elements::GetRightTarget( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Wrong Target
	Elements::GetWrongTarget( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Category ID
	Elements::GetCatID( m_pElement, szItem );
	szCatID = szItem;
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Animate
	Elements::GetAnimate( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Animation Rate
	Elements::GetAnimationRate( m_pElement, dVal );
	szItem.Format( _T("%.2f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Animation File
	Elements::GetAnimationFile( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Animation Random
	Elements::GetAnimationRandom( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Animation Generate
	Elements::GetAnimationGenerate( m_pElement, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Animation Subject ID
	Elements::GetAnimationSubjID( m_pElement, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// LOGFONTs
	LOGFONT* lf = NULL;
	m_pElement->get_Font1( (VARIANT**)&lf );
	szItem.Format( _T("%u %u %u %u %u %d %d %d %d %d %d %d %d\n"),
				   lf->lfHeight, lf->lfWidth, lf->lfEscapement, lf->lfOrientation,
				   lf->lfWeight, lf->lfItalic, lf->lfUnderline,
				   lf->lfStrikeOut, lf->lfCharSet, lf->lfOutPrecision,
				   lf->lfClipPrecision, lf->lfQuality, lf->lfPitchAndFamily );
	WRITE_STRING( pFile, szItem );	
	WRITE_STRING( pFile, lf->lfFaceName );
	WRITE_STRING( pFile, _T("\n") );
	m_pElement->get_Font2( (VARIANT**)&lf );
	szItem.Format( _T("%u %u %u %u %u %d %d %d %d %d %d %d %d\n"),
					lf->lfHeight, lf->lfWidth, lf->lfEscapement, lf->lfOrientation,
					lf->lfWeight, lf->lfItalic, lf->lfUnderline,
					lf->lfStrikeOut, lf->lfCharSet, lf->lfOutPrecision,
					lf->lfClipPrecision, lf->lfQuality, lf->lfPitchAndFamily );
	WRITE_STRING( pFile, szItem );	
	WRITE_STRING( pFile, lf->lfFaceName );
	WRITE_STRING( pFile, _T("\n") );
	m_pElement->get_Font3( (VARIANT**)&lf );
	szItem.Format( _T("%u %u %u %u %u %d %d %d %d %d %d %d %d\n"),
					lf->lfHeight, lf->lfWidth, lf->lfEscapement, lf->lfOrientation,
					lf->lfWeight, lf->lfItalic, lf->lfUnderline,
					lf->lfStrikeOut, lf->lfCharSet, lf->lfOutPrecision,
					lf->lfClipPrecision, lf->lfQuality, lf->lfPitchAndFamily );
	WRITE_STRING( pFile, szItem );	
	WRITE_STRING( pFile, lf->lfFaceName );
	WRITE_STRING( pFile, _T("\n") );
	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	// if first time called, clear the static memory of exported categories
	if( fFirst ) _lstExportedCats.RemoveAll();

	// Element categories
	Cats cat;
	if( !_lstExportedCats.Find( szCatID ) )
	{
		if( SUCCEEDED( cat.Find( szCatID ) ) )
		{
			if( cat.Export( pFile ) )
			{
				_lstExportedCats.AddTail( szCatID );
#ifdef _DEBUG
				szItem.Format( _T("EXPORTED: Category %s"), szCatID );
				OutputAMessage( szItem );
#endif
			}
			else
			{
#ifdef _DEBUG
				szItem.Format( _T("ERROR: Error in exporting category %s"), szCatID );
				OutputAMessage( szItem );
#endif
			}
		}
		else
		{
#ifdef _DEBUG
			szItem.Format( _T("ERROR: Category %s not found in database"), szCatID );
			OutputAMessage( szItem );
#endif
		}
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan )
{
	if( !m_pElement || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	double dVal = 0.;
	short nVal = 0;
	BOOL fVal = FALSE, fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;
	DWORD dwVal = 0;
	LOGFONT lf;
	Elements comp;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Element %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );
	// Pattern
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutPattern( szLine );
	comp.PutPattern( szLine );
	// Shape
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutShape( nVal );
	comp.PutShape( nVal );
	// Center X
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutCenterX( dVal );
	comp.PutCenterX( dVal );
	// Center Y
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutCenterY( dVal );
	comp.PutCenterY( dVal );
	// Width X
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutWidthX( dVal );
	comp.PutWidthX( dVal );
	// Width Y
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutWidthY( dVal );
	comp.PutWidthY( dVal );
	// Is Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutIsTarget( fVal );
	comp.PutIsTarget( fVal );
	// Error X
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutErrorX( dVal );
	comp.PutErrorX( dVal );
	// Error Y
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutErrorY( dVal );
	comp.PutErrorY( dVal );
	// Line 1 Size
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutLine1Size( nVal );
	comp.PutLine1Size( nVal );
	// Line 2 Size
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutLine2Size( nVal );
	comp.PutLine2Size( nVal );
	// Line 3 Size
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutLine3Size( nVal );
	comp.PutLine3Size( nVal );
	// Line 1 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutLine1Color( dwVal );
	comp.PutLine1Color( dwVal );
	// Line 2 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutLine2Color( dwVal );
	comp.PutLine2Color( dwVal );
	// Line 3 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutLine3Color( dwVal );
	comp.PutLine3Color( dwVal );
	// Background 1 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutBackground1Color( dwVal );
	comp.PutBackground1Color( dwVal );
	// Background 2 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutBackground2Color( dwVal );
	comp.PutBackground2Color( dwVal );
	// Background 3 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutBackground3Color( dwVal );
	comp.PutBackground3Color( dwVal );
	// Text 1
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutText1( szLine );
	comp.PutText1( szLine );
	// Text 2
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutText2( szLine );
	comp.PutText2( szLine );
	// Text 3
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutText3( szLine );
	comp.PutText3( szLine );
	// Text 1 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutText1Color( dwVal );
	comp.PutText1Color( dwVal );
	// Text 2 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutText2Color( dwVal );
	comp.PutText2Color( dwVal );
	// Text 3 Color
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dwVal = (DWORD)atol( szLine );
	PutText3Color( dwVal );
	comp.PutText3Color( dwVal );
	// Text 1 Size
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutText1Size( nVal );
	comp.PutText1Size( nVal );
	// Text 2 Size
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutText2Size( nVal );
	comp.PutText2Size( nVal );
	// Text 3 Size
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutText3Size( nVal );
	comp.PutText3Size( nVal );
	// Is Image
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutIsImage( fVal );
	comp.PutIsImage( fVal );
	// Start Time
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutStart( dVal );
	comp.PutStart( dVal );
	// Duration
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutDuration( dVal );
	comp.PutDuration( dVal );
	// Hide Right Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutHideRightTarget( fVal );
	comp.PutHideRightTarget( fVal );
	// Hide Wrong Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutHideWrongTarget( fVal );
	comp.PutHideWrongTarget( fVal );
	// Hide If Any Right Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutHideIfAnyRightTarget( fVal );
	comp.PutHideIfAnyRightTarget( fVal );
	// Hide If Any Wrong Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutHideIfAnyWrongTarget( fVal );
	comp.PutHideIfAnyWrongTarget( fVal );
	// Right Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutRightTarget( szLine );
	comp.PutRightTarget( szLine );
	// Wrong Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutWrongTarget( szLine );
	comp.PutWrongTarget( szLine );
	// Category ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutCatID( szLine );
	comp.PutCatID( szLine );
	// Animate
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutAnimate( fVal );
	comp.PutAnimate( fVal );
	// Animation Rate
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutAnimationRate( dVal );
	comp.PutAnimationRate( dVal );
	// Animation File
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutAnimationFile( szLine );
	comp.PutAnimationFile( szLine );
	// Animation Random
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutAnimationRandom( fVal );
	comp.PutAnimationRandom( fVal );
	// Animation Generate
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fVal = atoi( szLine );
	PutAnimationGenerate( fVal );
	comp.PutAnimationGenerate( fVal );
	// Animation Subject ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutAnimationSubjID( szLine );
	comp.PutAnimationSubjID( szLine );
	// LOGFONTS
	ZeroMemory( &lf, sizeof( LOGFONT ) );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	sscanf( szLine, "%u %u %u %u %u %d %d %d %d %d %d %d %d",
			&lf.lfHeight, &lf.lfWidth, &lf.lfEscapement, &lf.lfOrientation,
			&lf.lfWeight, &lf.lfItalic, &lf.lfUnderline,
			&lf.lfStrikeOut, &lf.lfCharSet, &lf.lfOutPrecision,
			&lf.lfClipPrecision, &lf.lfQuality, &lf.lfPitchAndFamily );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	strcpy_s( lf.lfFaceName, LF_FACESIZE, szLine );
 	m_pElement->put_Font1( (VARIANT*)&lf );
 	comp.m_pElement->put_Font1( (VARIANT*)&lf );
	ZeroMemory( &lf, sizeof( LOGFONT ) );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	sscanf( szLine, "%u %u %u %u %u %d %d %d %d %d %d %d %d",
			&lf.lfHeight, &lf.lfWidth, &lf.lfEscapement, &lf.lfOrientation,
			&lf.lfWeight, &lf.lfItalic, &lf.lfUnderline,
			&lf.lfStrikeOut, &lf.lfCharSet, &lf.lfOutPrecision,
			&lf.lfClipPrecision, &lf.lfQuality, &lf.lfPitchAndFamily );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	strcpy_s( lf.lfFaceName, LF_FACESIZE, szLine );
 	m_pElement->put_Font2( (VARIANT*)&lf );
 	comp.m_pElement->put_Font2( (VARIANT*)&lf );
	ZeroMemory( &lf, sizeof( LOGFONT ) );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	sscanf( szLine, "%u %u %u %u %u %d %d %d %d %d %d %d %d",
			&lf.lfHeight, &lf.lfWidth, &lf.lfEscapement, &lf.lfOrientation,
			&lf.lfWeight, &lf.lfItalic, &lf.lfUnderline,
			&lf.lfStrikeOut, &lf.lfCharSet, &lf.lfOutPrecision,
			&lf.lfClipPrecision, &lf.lfQuality, &lf.lfPitchAndFamily );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	strcpy_s( lf.lfFaceName, LF_FACESIZE, szLine );
 	m_pElement->put_Font3( (VARIANT*)&lf );
	comp.m_pElement->put_Font3( (VARIANT*)&lf );

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_ELEMENT, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		CFileFind ff;
		if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
		if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			comp.GetPattern( szLine );
			PutPattern( szLine );
			comp.GetShape( nVal );
			PutShape( nVal );
			comp.GetCenterX( dVal );
			PutCenterX( dVal );
			comp.GetCenterY( dVal );
			PutCenterY( dVal );
			comp.GetWidthX( dVal );
			PutWidthX( dVal );
			comp.GetWidthY( dVal );
			PutWidthY( dVal );
			comp.GetIsTarget( fVal );
			PutIsTarget( fVal );
			comp.GetErrorX( dVal );
			PutErrorX( dVal );
			comp.GetErrorY( dVal );
			PutErrorY( dVal );
			comp.GetLine1Size( nVal );
			PutLine1Size( nVal );
			comp.GetLine2Size( nVal );
			PutLine2Size( nVal );
			comp.GetLine3Size( nVal );
			PutLine3Size( nVal );
			comp.GetLine1Color( dwVal );
			PutLine1Color( dwVal );
			comp.GetLine2Color( dwVal );
			PutLine2Color( dwVal );
			comp.GetLine3Color( dwVal );
			PutLine3Color( dwVal );
			comp.GetBackground1Color( dwVal );
			PutBackground1Color( dwVal );
			comp.GetBackground2Color( dwVal );
			PutBackground2Color( dwVal );
			comp.GetBackground3Color( dwVal );
			PutBackground3Color( dwVal );
			comp.GetText1( szLine );
			PutText1( szLine );
			comp.GetText2( szLine );
			PutText2( szLine );
			comp.GetText3( szLine );
			PutText3( szLine );
			comp.GetText1Color( dwVal );
			PutText1Color( dwVal );
			comp.GetText2Color( dwVal );
			PutText2Color( dwVal );
			comp.GetText3Color( dwVal );
			PutText3Color( dwVal );
			comp.GetText1Size( nVal );
			PutText1Size( nVal );
			comp.GetText2Size( nVal );
			PutText2Size( nVal );
			comp.GetText3Size( nVal );
			PutText3Size( nVal );
			comp.GetIsImage( fVal );
			PutIsImage( fVal );
			comp.GetStart( dVal );
			PutStart( dVal );
			comp.GetDuration( dVal );
			PutDuration( dVal );
			comp.GetHideRightTarget( fVal );
			PutHideRightTarget( fVal );
			comp.GetHideWrongTarget( fVal );
			PutHideWrongTarget( fVal );
			comp.GetHideIfAnyRightTarget( fVal );
			PutHideIfAnyRightTarget( fVal );
			comp.GetHideIfAnyWrongTarget( fVal );
			PutHideIfAnyWrongTarget( fVal );
			comp.GetRightTarget( szLine );
			PutRightTarget( szLine );
			comp.GetWrongTarget( szLine );
			PutWrongTarget( szLine );
			comp.GetCatID( szLine );
			PutCatID( szLine );
			comp.GetAnimate( fVal );
			PutAnimate( fVal );
			comp.GetAnimationRate( dVal );
			PutAnimationRate( dVal );
			comp.GetAnimationFile( szLine );
			PutAnimationFile( szLine );
			comp.GetAnimationRandom( fVal );
			PutAnimationRandom( fVal );
			comp.GetAnimationGenerate( fVal );
			PutAnimationGenerate( fVal );
			comp.GetAnimationSubjID( szLine );
			PutAnimationSubjID( szLine );
			LOGFONT* lf = NULL;
			comp.m_pElement->get_Font1( (VARIANT**)&lf );
			m_pElement->put_Font1( (VARIANT*)lf );
			comp.m_pElement->get_Font2( (VARIANT**)&lf );
			m_pElement->put_Font2( (VARIANT*)lf );
			comp.m_pElement->get_Font3( (VARIANT**)&lf );
			m_pElement->put_Font3( (VARIANT*)lf );

			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ForceLocal()
{
	if( !m_pElement ) return;

	HRESULT hr = m_pElement->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ForceRemote()
{
	if( !m_pElement ) return;

	HRESULT hr = m_pElement->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Elements::ForceDefault()
{
	if( !m_pElement ) return;

	HRESULT hr = m_pElement->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		Elements dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );
		
		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		// category
		CString szCat;
		GetCatID( szCat );
		Cats cat;
		if( SUCCEEDED( cat.Find( szCat ) ) )
		{
			if( fToLocal ) cat.DoCopy( szCat, szUserPath, fOverwrite );
			else cat.DoUpload( szCat, fOverwrite );
		}

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::SetTemp( BOOL fVal )
{
	if( m_pElement ) m_pElement->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::DumpRecord( CString szID, CString szFile )
{
	if( !m_pElement ) return FALSE;
	return SUCCEEDED( m_pElement->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Elements::RemoveTemp()
{
	if( m_pElement ) m_pElement->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Elements::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pElement ) m_pElement->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Elements::ValidateData( CStringList* pListErrors, BOOL fFirstOnly, BOOL fNotify )
{

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

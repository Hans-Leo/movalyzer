// ConditionPageMain.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "..\CTreeLink\DBCommon.h"
#include "ConditionPageMain.h"
#include "Conditions.h"
#include "RelationshipDlg.h"
#include "ConditionSheet.h"
#include "InstructionDlg.h"
#include "NotesDlg.h"
#include "..\Common\MessageManager.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ConditionPageMain property page

IMPLEMENT_DYNCREATE(ConditionPageMain, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageMain, CBCGPPropertyPage)
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnKillfocusEditId)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_INSTR, OnClickInstr)
	ON_BN_CLICKED(IDC_BN_NOTES, OnClickNotes)
	ON_BN_CLICKED(IDC_BN_TEST, OnClickTest)
	ON_BN_CLICKED(IDC_TXT_STIMINSTR, OnClickHint)
	ON_BN_CLICKED(IDC_TXT_EXPINSTR, OnClickHintInstr)
END_MESSAGE_MAP()

ConditionPageMain::ConditionPageMain( INSCondition* pC, CString szExpID, BOOL fNew, BOOL fReps, BOOL fDup )
	: CBCGPPropertyPage(ConditionPageMain::IDD), m_pC( pC ), m_fNew( fNew ),
	  m_fReps( fReps ), m_fDup( fDup ), m_szExpID( szExpID )
{
	m_nReps = 3;
	m_szInstr.LoadString( IDS_RV_INSTR );
	m_fInit = TRUE;

	if( !fNew && pC )
	{
		/*if( !m_fDup )*/ NSConditions::GetID( pC, m_szID );
		NSConditions::GetDescription( pC, m_szDesc );
		NSConditions::GetNotes( pC, m_szNotes );
		NSConditions::GetInstruction( pC, m_szInstr );
	}

	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	NSConditions cond;
	cond.SetDataPath( szPath );
	HRESULT hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetID( szItem );
		m_lstItems.AddTail( szItem );
		hr = cond.GetNext();
	}
}

ConditionPageMain::~ConditionPageMain()
{
}

void ConditionPageMain::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_Text(pDX, IDC_EDIT_REPS, m_nReps);
	DDX_Text(pDX, IDC_EDIT_NOTES, m_szNotes);
	DDV_MaxChars(pDX, m_szNotes, szNOTES);
	DDX_Control(pDX, IDC_TXT_STIMINSTR, m_linkPattern);
	DDX_Control(pDX, IDC_TXT_EXPINSTR, m_linkInstr);
	DDX_Text(pDX, IDC_EDIT_INSTR, m_szInstr);
	DDV_MaxChars(pDX, m_szInstr, szCONDITION_INSTR);
}

/////////////////////////////////////////////////////////////////////////////
// ConditionPageMain message handlers

BOOL ConditionPageMain::OnApply() 
{
	if( !m_pC ) return FALSE;

	UpdateData( TRUE );

	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The condition ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("The condition description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return FALSE;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nReps < 0 ) || ( m_nReps > 99 ) )
	{
		BCGPMessageBox( _T("The number of trials must be between 0 and 99.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_REPS );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	m_pC->put_ID( m_szID.AllocSysString() );
	m_pC->put_Description( m_szDesc.AllocSysString() );
	pSheet->m_nReps = m_nReps;
	m_pC->put_Notes( m_szNotes.AllocSysString() );
	m_pC->put_Instruction( m_szInstr.AllocSysString() );
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageMain::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageMain::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle();

	// Reps
	if( m_fReps )
	{
		ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
		m_nReps = pSheet->m_nReps;
		UpdateData( FALSE );
	}

	// with no URL set, parent will handle click command (since control uses on_control_ex)
	m_linkPattern.SizeToContent();
	m_linkPattern.m_bDefaultClickProcess = TRUE;
	m_linkInstr.SizeToContent();
	m_linkInstr.m_bDefaultClickProcess = TRUE;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_ID, _T("ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_DESC, _T("Description") );
	pWnd = GetDlgItem( IDC_EDIT_INSTR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_INSTR, _T("Instruction") );
	pWnd = GetDlgItem( IDC_EDIT_REPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_COND_REP, _T("Number of Trials") );
	pWnd = GetDlgItem( IDC_EDIT_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Any additional information you would like.", _T("Notes") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_INSTR );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Set the instructions for this condition while running an experiment. Shown in a window before trials.", _T("Instruction Window") );
	pWnd = GetDlgItem( IDC_BN_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Add some extended notes to the condition.", _T("Extended Notes") );
	pWnd = GetDlgItem( IDC_BN_TEST );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Show instruction in recording window pane (NOTE: you might have to move this dialog in order to see).", _T("Test Instruction") );

	if( !m_fNew && !m_fDup )
	{
		pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( m_fNew )
	{
		pWnd = GetDlgItem( IDC_BN_TEST );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( m_fReps )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_NOTES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_INSTR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_TEST );
		if( pWnd ) pWnd->EnableWindow( FALSE  );
		pWnd = GetDlgItem( IDC_BN_INSTR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_TXT_STIMINSTR );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );

		pWnd = GetDlgItem( IDC_TXT_REPS );
		if( pWnd ) pWnd->EnableWindow( TRUE );
		pWnd = GetDlgItem( IDC_EDIT_REPS );
		if( pWnd ) pWnd->EnableWindow( TRUE );
	}
	else
	{
		pWnd = GetDlgItem( IDC_TXT_REPS );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_EDIT_REPS );
		if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	}

	if( m_szExpID == _T("") )
	{
		pWnd = GetDlgItem( IDC_BN_INSTR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ConditionPageMain::OnKillfocusEditId() 
{
	if( !m_fInit )
	{
		m_fInit = TRUE;
		return;
	}

	UpdateData( TRUE );

	if( m_szID == _T("") ) return;

	if( m_lstItems.Find( m_szID ) )
	{
		int nResp = BCGPMessageBox( _T("This ID already exists. Would you like to see how it is currently being used?"), MB_YESNO );
		if( nResp == IDYES )
		{
			NSConditions cond;
			if( cond.IsValid() )
			{
				if( SUCCEEDED( cond.Find( m_szID ) ) )
				{
					CString szItem;
					cond.GetDescriptionWithID( szItem );

					RelationshipDlg d( this, REL_CONDITION, szItem );
					d.DoModal();
				}
				else BCGPMessageBox( _T("Could not find condition.") );
			}
			else BCGPMessageBox( IDS_C_ERR );
		}
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
	/*
	char c;
	int nCnt = m_szID.GetLength();
	for( int i = 0; i < nCnt; i++ )
	{
		c = m_szID.GetAt( i );
		if( !isalnum( c ) )
		{
			BCGPMessageBox( _T("ID can only contain alpha-numeric characters.") );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
			if( pWnd )
			{
				pWnd->SetWindowText( _T("") );
				pWnd->SetFocus();
			}
		}
	}
	*/
}

BOOL ConditionPageMain::DoHelp(HELPINFO* pHelpInfo)
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageMain::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_general.html"), this );
	return TRUE;
}

void ConditionPageMain::OnClickInstr()
{
	// verify ID first
	UpdateData( TRUE );
	if( m_szExpID == _T("") ) return;
	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CString szData = _T("SRC: Condition - Instructions");
	LogToFile( szData, LOG_UI );

	// Check for directory structure
	if( !VerifyDirectory( m_szExpID ) )
	{
		BCGPMessageBox( _T("Unable to create directory for files.") );
		return;
	}

	CString szFile;
	szFile.Format( _T("%s%s"), m_szExpID, m_szID );
	InstructionDlg d( this );
	d.m_fCond = TRUE;
	d.m_szExpID = m_szExpID;
	d.m_szFileName = szFile;
	d.DoModal();
}

void ConditionPageMain::OnClickNotes()
{
	// verify ID first
	UpdateData( TRUE );
	if( m_szExpID == _T("") ) return;
	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CString szData = _T("SRC: Experiment - Notes");
	LogToFile( szData, LOG_UI );

	// Check for directory structure
	if( !VerifyDirectory( m_szExpID ) )
	{
		BCGPMessageBox( _T("Unable to create directory for files.") );
		return;
	}

	CString szPath, szFile;
	::GetDataPathRoot( szPath );
	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s%s.txt"), m_szExpID, m_szExpID, m_szID );
	else
		szFile.Format( _T("%s\\%s\\%s%s.txt"), szPath, m_szExpID, m_szExpID, m_szID );

	NotesDlg d( N_CONDITION, m_szID, szFile, this );
	d.DoModal();
}

void ConditionPageMain::OnClickHint()
{
	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 2 );
}

void ConditionPageMain::OnClickHintInstr()
{
	BCGPMessageBox( _T("In order to control the usage of condition instructions, you must go to the experiment properties.\n\nGo to experiment properties -> Running Experiment -> Procedure"), MB_OK | MB_ICONINFORMATION );
}

void ConditionPageMain::OnClickTest()
{
	UpdateData( TRUE );
	MessageManager* pMM = MessageManager::GetMessageManager();
	if( pMM ) pMM->PostMessage( MAKEWPARAM( MSG_ACTION, UM_INSTRUCTION_UPDATE ),
								(LPARAM)m_szInstr.GetBuffer() );
}

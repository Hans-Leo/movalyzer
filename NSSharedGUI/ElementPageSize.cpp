// ElementPageSize.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ElementPageSize.h"
#include "ElementSheet.h"
#include "ElementPageGeneral.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ElementPageSize property page

IMPLEMENT_DYNCREATE(ElementPageSize, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ElementPageSize, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_SHAPE, OnSelchangeCboShape)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
END_MESSAGE_MAP()

ElementPageSize::ElementPageSize( IElement* pE )
	: CBCGPPropertyPage(ElementPageSize::IDD), m_pE( pE )
{
	m_dX = 0.0;
	m_dY = 0.0;
	m_dXWidth = 0.0;
	m_dYWidth = 0.0;
	m_dXErr = 0.0;
	m_dYErr = 0.0;
	m_nShape = -1;

	if( m_pE )
	{
		short nVal = 0;
		m_pE->get_Shape( &nVal );
		m_nShape = nVal - 1;
		m_pE->get_CenterX( &m_dX );
		m_pE->get_CenterY( &m_dY );
		m_pE->get_WidthX( &m_dXWidth );
		m_pE->get_WidthY( &m_dYWidth );
		m_pE->get_ErrorX( &m_dXErr );
		m_pE->get_ErrorY( &m_dYErr );
	}
}

ElementPageSize::~ElementPageSize()
{
}

void ElementPageSize::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_SHAPE, m_cboShape);
	DDX_Text(pDX, IDC_EDIT_X, m_dX);
	DDV_MinMaxDouble(pDX, m_dX, 0., 100.);
	DDX_Text(pDX, IDC_EDIT_Y, m_dY);
	DDV_MinMaxDouble(pDX, m_dY, 0., 100.);
	DDX_Text(pDX, IDC_EDIT_XWIDTH, m_dXWidth);
	DDV_MinMaxDouble(pDX, m_dXWidth, 0., 100.);
	DDX_Text(pDX, IDC_EDIT_YWIDTH, m_dYWidth);
	DDV_MinMaxDouble(pDX, m_dYWidth, 0., 100.);
	DDX_Text(pDX, IDC_EDIT_XERR, m_dXErr);
	DDV_MinMaxDouble(pDX, m_dXErr, -100., 100.);
	DDX_Text(pDX, IDC_EDIT_YERR, m_dYErr);
	DDV_MinMaxDouble(pDX, m_dYErr, -100., 100.);
	DDX_CBIndex(pDX, IDC_CBO_SHAPE, m_nShape);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
}

/////////////////////////////////////////////////////////////////////////////
// ElementPageSize message handlers

BOOL ElementPageSize::OnApply() 
{
	if( !m_pE ) return FALSE;

	UpdateData( TRUE );

	// TODO
	// 1) Verify values
	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = (ElementPageGeneral*)pSheet->GetPage( 0 );
	if( !pPage->IsPattern() && !pPage->IsImage() )
	{
		if( m_nShape < 0 )
		{
			BCGPMessageBox( _T("Please select a shape.") );
			CWnd* pWnd = GetDlgItem( IDC_CBO_SHAPE );
			pWnd->SetFocus();
			return FALSE;
		}
	}

	m_pE->put_Shape( (short)m_nShape + 1 );
	m_pE->put_CenterX( m_dX );
	m_pE->put_CenterY( m_dY );
	m_pE->put_WidthX( m_dXWidth );
	m_pE->put_WidthY( m_dYWidth );
	m_pE->put_ErrorX( m_dXErr );
	m_pE->put_ErrorY( m_dYErr );

	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

void ElementPageSize::OnSelchangeCboShape() 
{
	// Do nothing?
}

BOOL ElementPageSize::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ElementPageSize::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	// button images
	m_bnCalc.SetImage( IDB_CALC );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_ELEM );
	m_tooltip.SetTitle( _T("ELEMENT") );
	CWnd* pWnd = GetDlgItem( IDC_CBO_SHAPE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the shape for this element."), _T("Shape") );
	pWnd = GetDlgItem( IDC_EDIT_X );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the center-point (cm) X-coordinate for a shape or pattern.\nFor patterns, this represents the starting X-point of the pattern."), _T("X-Center") );
	pWnd = GetDlgItem( IDC_EDIT_Y );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the center-point (cm) Y-coordinate for a shape or pattern.\nFor patterns, this represents the starting Y-point of the pattern."), _T("Y-Center") );
	pWnd = GetDlgItem( IDC_EDIT_XWIDTH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify 1/2 the total X length (cm) for a shape (cm)."), _T("X-Radius") );
	pWnd = GetDlgItem( IDC_EDIT_YWIDTH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify 1/2 the total Y length (cm) for a shape (cm)."), _T("Y-Radius") );
	pWnd = GetDlgItem( IDC_EDIT_XERR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the margin of error allowed for X (cm)."), _T("X-Error") );
	pWnd = GetDlgItem( IDC_EDIT_YERR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the margin of error allowed for Y (cm)."), _T("Y-Error") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );

	// Only have rectangle & circle right now
	m_cboShape.AddString( _T("Rectangle") );
	m_cboShape.AddString( _T("Ellipse") );
	m_cboShape.SetCurSel( m_nShape );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ElementPageSize::OnSetActive() 
{
	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = pSheet ? (ElementPageGeneral*)pSheet->GetPage( 0 ) : NULL;
	if( pPage )
	{
		CWnd* pWnd = NULL;
		if( !pPage->IsTarget() )
		{
			pWnd = GetDlgItem( IDC_EDIT_XERR );
			pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_EDIT_YERR );
			pWnd->EnableWindow( FALSE );
		}
		else
		{
			pWnd = GetDlgItem( IDC_EDIT_XERR );
			pWnd->EnableWindow( TRUE );
			pWnd = GetDlgItem( IDC_EDIT_YERR );
			pWnd->EnableWindow( TRUE );
		}

		if( pPage->IsPattern() || pPage->IsImage() )
		{
			m_nShape = -1;
			m_dXWidth = 0.;
			m_dYWidth = 0.;
			UpdateData( FALSE );
			pWnd = GetDlgItem( IDC_CBO_SHAPE );
			pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_EDIT_XWIDTH );
			pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_EDIT_YWIDTH );
			pWnd->EnableWindow( FALSE );
		}
		else
		{
			pWnd = GetDlgItem( IDC_CBO_SHAPE );
			pWnd->EnableWindow( TRUE );
			if( m_nShape == -1 )
			{
				m_nShape = 0;
				UpdateData( FALSE );
			}
			pWnd = GetDlgItem( IDC_EDIT_XWIDTH );
			pWnd->EnableWindow( TRUE );
			pWnd = GetDlgItem( IDC_EDIT_YWIDTH );
			pWnd->EnableWindow( TRUE );
		}
	}
	
	return CBCGPPropertyPage::OnSetActive();
}

BOOL ElementPageSize::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ElementPageSize::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("element_dimensions.html"), this );
	return TRUE;
}

void ElementPageSize::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

// ProcSetPageNormDB.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageNormDB.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageNormDB property page

IMPLEMENT_DYNCREATE(ProcSetPageNormDB, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageNormDB, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_CALC_Z, SetFieldStates)
	ON_BN_CLICKED(IDC_CHK_CHECK_TO_UPDATE, SetFieldStates)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageNormDB::ProcSetPageNormDB( IProcSetting* pS )
	: CBCGPPropertyPage(ProcSetPageNormDB::IDD), m_pPS( pS )
{
	m_fScore = TRUE;
	m_fScoreGlobal = TRUE;
	m_fNoUpdateDB = FALSE;
	m_dThreshold = 1.0;

	m_chkScore.SetDefaultAsOff( !m_fScore );
	m_chkScoreGlobal.SetDefaultAsOff( !m_fScoreGlobal );
	m_chkNoUpdateDB.SetDefaultAsOff( !m_fNoUpdateDB );
	m_editThreshold.SetDefaultValue( m_dThreshold );

	if( m_pPS )
	{
		IProcSetFlags* pPSF = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_NormDBCalcScore( &m_fScore );
			pPSF->get_NormDBCalcScoreGlobal( &m_fScoreGlobal );
			pPSF->get_NormDBNoUpdate( &m_fNoUpdateDB );
			pPSF->get_NormDBThreshold( &m_dThreshold );
			pPSF->Release();
		}
	}
}

ProcSetPageNormDB::~ProcSetPageNormDB()
{
}

void ProcSetPageNormDB::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_CALC_Z, m_fScore);
	DDX_Check(pDX, IDC_CHK_CALC_Z_GLOBAL, m_fScoreGlobal);
	DDX_Check(pDX, IDC_CHK_CHECK_TO_UPDATE, m_fNoUpdateDB);
	DDX_Text(pDX, IDC_EDIT_P_THRESHOLD, m_dThreshold);

	DDX_Control(pDX, IDC_CHK_CALC_Z, m_chkScore);
	DDX_Control(pDX, IDC_CHK_CALC_Z_GLOBAL, m_chkScoreGlobal);
	DDX_Control(pDX, IDC_CHK_CHECK_TO_UPDATE, m_chkNoUpdateDB);
	DDX_Control(pDX, IDC_EDIT_P_THRESHOLD, m_editThreshold);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageNormDB message handlers

BOOL ProcSetPageNormDB::OnApply() 
{
	if( !m_pPS ) return FALSE;

	UpdateData( TRUE );

	// Verify threshold value
	if( m_dThreshold <= 0. ) {
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 14 );
		BCGPMessageBox( _T("Critical Z-score must be more than 0."), MB_OK );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_P_THRESHOLD );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	IProcSetFlags* pPSF = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( SUCCEEDED( hr ) )
	{
		pPSF->put_NormDBCalcScore( m_fScore );
		pPSF->put_NormDBCalcScoreGlobal( m_fScoreGlobal );
		pPSF->put_NormDBNoUpdate( m_fNoUpdateDB );
		pPSF->put_NormDBThreshold( m_dThreshold );
		pPSF->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageNormDB::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageNormDB::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_CHK_CALC_Z );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Calculate the Z-score in comparison to subject's previous averages."), _T("Calculate Z-Score") );
	pWnd = GetDlgItem( IDC_CHK_CALC_Z_GLOBAL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Calculate the Z-score in comparison to all subjects."), _T("Compare to All Subjects") );
	pWnd = GetDlgItem( IDC_CHK_CHECK_TO_UPDATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Do not average in the subject's results if the personal Z-score is more than the critical Z-score."), _T("Do Not Update Normals") );
	pWnd = GetDlgItem( IDC_EDIT_P_THRESHOLD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the Z-score above which a test is considered not normal."), _T("Update Threshold") );

	SetFieldStates();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageNormDB::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageNormDB::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	// TODO
	::GoToHelp( _T("experimentsettings_processing_normdb.html"), this );
	return TRUE;
}

void ProcSetPageNormDB::SetFieldStates()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_CALC_Z_GLOBAL );
	if( pWnd ) pWnd->EnableWindow( m_fScore );
	pWnd = GetDlgItem( IDC_CHK_CHECK_TO_UPDATE );
	if( pWnd ) pWnd->EnableWindow( m_fScore );
	pWnd = GetDlgItem( IDC_EDIT_P_THRESHOLD );
	if( pWnd ) pWnd->EnableWindow( m_fScore );
}

void ProcSetPageNormDB::OnBnReset() 
{
	m_fScore = TRUE;
	m_fScoreGlobal = TRUE;
	m_fNoUpdateDB = FALSE;
	m_dThreshold = 1.0;

	UpdateData( FALSE );

	SetFieldStates();
}

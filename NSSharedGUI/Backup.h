///////////////////////////////////////////////////////////////////////////////
// Backup.h

#pragma once

// Backup Management
UINT AFX_EXT_API BackupExperiment( CString szExp, BOOL fCopy, int& nCount, BOOL fDisp );
UINT AFX_EXT_API BackupGroup( CString szExp, CString szGrp, BOOL fCopy, int& nCount, BOOL fDisp );
UINT AFX_EXT_API BackupSubject( CString szExp, CString szGrp, CString szSubj, BOOL fCopy, int& nCount, BOOL fDisp );
void AFX_EXT_API BackupSystem();
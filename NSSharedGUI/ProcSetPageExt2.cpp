// ProcSetPageExt2.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageExt2.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageExt2 property page

IMPLEMENT_DYNCREATE(ProcSetPageExt2, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageExt2, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_WM_HELPINFO()
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageExt2::ProcSetPageExt2( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageExt2::IDD), m_pPS( pPS )
{
	initialpart = 0.080;
	maxfreq = 20.0;

	m_editInitial.SetDefaultValue( initialpart );
	m_editMaxFreq.SetDefaultValue( maxfreq );

	if( m_pPS )
	{
		m_pPS->get_StrokeBeginning( &initialpart );
		m_pPS->get_MaxFrequency( &maxfreq );
	}
}

ProcSetPageExt2::~ProcSetPageExt2()
{
}

void ProcSetPageExt2::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_BS, initialpart);
	DDX_Text(pDX, IDC_EDIT_MF, maxfreq);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);

	DDX_Control(pDX, IDC_EDIT_BS, m_editInitial);
	DDX_Control(pDX, IDC_EDIT_MF, m_editMaxFreq);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageExt2 message handlers

BOOL ProcSetPageExt2::OnApply() 
{
	if( !m_pPS ) return FALSE;

	m_pPS->put_StrokeBeginning( initialpart );
	m_pPS->put_MaxFrequency( maxfreq );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageExt2::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageExt2::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	double dinitialpart = 0., dmaxfreq = 0.;
	if( pPS )
	{
		pPS->get_StrokeBeginning( &dinitialpart );
		pPS->get_MaxFrequency( &dmaxfreq );

		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_BS );
	szData = _T("Initial part of stroke for departure of initial (s)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dinitialpart );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Initial Part") );

	pWnd = GetDlgItem( IDC_EDIT_MF );
	szData = _T("Maximum frequency of acceleration and deceleration");
	szTmp.Format( _T(" [DEFAULT=%g]"), dmaxfreq );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Frequency") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageExt2::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageExt2::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_extraction.html"), this );
	return TRUE;
}

void ProcSetPageExt2::OnBnReset() 
{
	initialpart = 0.080;
	maxfreq = 20.0;

	UpdateData( FALSE );
}

void ProcSetPageExt2::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 10 );
}

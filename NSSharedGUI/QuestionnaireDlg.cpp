QuestionnaireDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "QuestionnaireDlg.h"
#include "QuestionnaireFullDlg.h"
#include "..\CTreeLink\DBCommon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireDlg dialog

BEGIN_MESSAGE_MAP(QuestionnaireDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_PREVQ, OnBnPrevq)
	ON_BN_CLICKED(IDC_BN_NEXTQ, OnBnNextq)
	ON_BN_CLICKED(IDC_CHK_HDR, OnChkHdr)
	ON_BN_CLICKED(IDC_CHK_PRIVATE, OnChkPrivate)
	ON_BN_CLICKED(IDC_CHK_USE, OnChkNumeric)
	ON_EN_CHANGE(IDC_EDIT_ID, OnChangeEditID)
	ON_EN_CHANGE(IDC_EDIT_Q, OnChangeEditQ)
	ON_EN_CHANGE(IDC_EDIT_DESC, OnChangeEditCH)
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnKillfocusEditId)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

QuestionnaireDlg::QuestionnaireDlg(CStringList* pListH, CStringList* pListQ,
								   CStringList* pListID, CStringList* pListS,
								   CStringList* pListI, CStringList* pListP,
								   CStringList* pListN, CStringList* pListCH,
								   BOOL fEdit, int nIdx, BOOL fMaster, CWnd* pParent)
	: CBCGPDialog(QuestionnaireDlg::IDD, pParent), m_pListH( pListH ), m_pListP( pListP ),
	  m_pListQ( pListQ ), m_pListID( pListID ), m_pListS( pListS ), m_pListI( pListI ),
	  m_fEdit( fEdit ), m_nIdx( nIdx ), m_fMaster( fMaster ), m_pListN( pListN ), m_pListCH( pListCH )
{
	m_szQuestion = _T("");
	m_szID = _T("");
	m_fHdr = FALSE;
	m_fPrivate = FALSE;
	m_fNumeric = TRUE;
	m_szColHeader = _T("");
	m_fModified = FALSE;
	m_fAdd = !m_fEdit;
	m_fNew = TRUE;
	m_fClose = FALSE;
	m_fCanClose = TRUE;

//	if( !fEdit ) m_nIdx++;
}

void QuestionnaireDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_Q, m_szQuestion);
	DDV_MaxChars(pDX, m_szQuestion, szQUESTIONNAIRE_Q);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Check(pDX, IDC_CHK_HDR, m_fHdr);
	DDX_Check(pDX, IDC_CHK_PRIVATE, m_fPrivate);
	DDX_Control(pDX, IDC_BN_PREVQ, m_bnPrev);
	DDX_Control(pDX, IDC_BN_NEXTQ, m_bnNext);
	DDX_Check(pDX, IDC_CHK_USE, m_fNumeric);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szColHeader);
	DDV_MaxChars(pDX, m_szColHeader, szCODE);
}

/////////////////////////////////////////////////////////////////////////////
// QuestionnaireDlg message handlers

void QuestionnaireDlg::OnBnPrevq() 
{
	if( !m_pListH || !m_pListQ || !m_pListID || !m_pListCH || !m_pListN ) return;
	if( m_nIdx < 0 ) return;

	POSITION pos = NULL;
	BOOL fDel = FALSE;

	if( m_fModified )
	{
		UpdateData ( TRUE );

		if( m_szID.GetLength() != szIDS )
		{
			CString szMsg;
			if( m_fClose ) szMsg = _T("There is an invalid ID. Click YES to continue without saving, click NO to correct the ID.");
			else szMsg = _T("Invalid ID. The ID must be 3 characters.");
			int nRes = BCGPMessageBox( szMsg, m_fClose ? MB_YESNO : MB_ICONERROR );
			if( !m_fClose || ( nRes == IDNO ) )
			{
				CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
				if( pWnd ) pWnd->SetFocus();
				m_fCanClose = FALSE;
				return;
			}
		}

		if( !m_fHdr && ( m_szColHeader == _T("") ) )
		{
			CString szMsg;
			if( m_fClose ) szMsg = _T("You must specify a column header. Click YES to continue without saving, click NO to correct the header.");
			else szMsg = _T("You must specify a column header.");
			int nRes = BCGPMessageBox( szMsg, m_fClose ? MB_YESNO : MB_ICONERROR );
			if( !m_fClose || ( nRes == IDNO ) )
			{
				CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
				if( pWnd ) pWnd->SetFocus();
				m_fCanClose = FALSE;
				return;
			}
		}

		pos = m_pListID->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListID->RemoveAt( pos );
				pos = m_pListID->FindIndex( m_nIdx );
				if( pos ) m_pListID->InsertBefore( pos, m_szID );
				else m_pListID->AddTail( m_szID );
			}
			else m_pListID->InsertBefore( pos, m_szID );
		}
		else m_pListID->AddTail( m_szID );

		pos = m_pListH->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListH->RemoveAt( pos );
				pos = m_pListH->FindIndex( m_nIdx );
				if( pos ) m_pListH->InsertBefore( pos, m_fHdr ? _T("TRUE") : _T("") );
				else m_pListH->AddTail( m_fHdr ? _T("TRUE") : _T("") );
			}
			else m_pListH->InsertBefore( pos, m_fHdr ? _T("TRUE") : _T("") );
		}
		else m_pListH->AddTail( m_fHdr ? _T("TRUE") : _T("") );

		pos = m_pListP->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListP->RemoveAt( pos );
				pos = m_pListP->FindIndex( m_nIdx );
				if( pos ) m_pListP->InsertBefore( pos, m_fPrivate ? _T("TRUE") : _T("") );
				else m_pListP->AddTail( m_fPrivate ? _T("TRUE") : _T("") );
			}
			else m_pListP->InsertBefore( pos, m_fPrivate ? _T("TRUE") : _T("") );
		}
		else m_pListP->AddTail( m_fPrivate ? _T("TRUE") : _T("") );

		pos = m_pListN->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListN->RemoveAt( pos );
				pos = m_pListN->FindIndex( m_nIdx );
				if( pos ) m_pListN->InsertBefore( pos, m_fNumeric ? _T("TRUE") : _T("") );
				else m_pListN->AddTail( m_fNumeric ? _T("TRUE") : _T("") );
			}
			else m_pListN->InsertBefore( pos, m_fNumeric ? _T("TRUE") : _T("") );
		}
		else m_pListN->AddTail( m_fNumeric ? _T("TRUE") : _T("") );

		pos = m_pListQ->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListQ->RemoveAt( pos );
				pos = m_pListQ->FindIndex( m_nIdx );
				if( pos ) m_pListQ->InsertBefore( pos, m_szQuestion );
				else m_pListQ->AddTail( m_szQuestion );
			}
			else m_pListQ->InsertBefore( pos, m_szQuestion );
		}
		else m_pListQ->AddTail( m_szQuestion );

		pos = m_pListCH->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListCH->RemoveAt( pos );
				pos = m_pListCH->FindIndex( m_nIdx );
				if( pos ) m_pListCH->InsertBefore( pos, m_szColHeader );
				else m_pListCH->AddTail( m_szColHeader );
			}
			else m_pListCH->InsertBefore( pos, m_szColHeader );
		}
		else m_pListCH->AddTail( m_szColHeader );

		CString szS = m_fNew ? Q_STATE_NEW : Q_STATE_DIFF;
		pos = m_pListS->FindIndex( m_nIdx );
		if( pos )
		{
			CString szOldS = m_pListS->GetAt( pos );
			if( szOldS == Q_STATE_NEW ) szS = Q_STATE_NEW;
			if( m_fEdit )
			{
				m_pListS->RemoveAt( pos );
				pos = m_pListS->FindIndex( m_nIdx );
				if( pos ) m_pListS->InsertBefore( pos, szS );
				else m_pListS->AddTail( szS );
			}
			else m_pListS->InsertBefore( pos, szS );
		}
		else m_pListS->AddTail( szS );

		pos = m_pListI->FindIndex( m_nIdx );
		if( pos )
		{
			CString szI = m_pListI->GetAt( pos );
			if( m_fEdit )
			{
				m_pListI->RemoveAt( pos );
				pos = m_pListS->FindIndex( m_nIdx );
				if( pos ) m_pListI->InsertBefore( pos, szI );
				else m_pListI->AddTail( szI );
			}
			else m_pListI->InsertBefore( pos, szI );
		}
		else
		{
			CString szI = _T("");
			m_pListI->AddTail( szI );
		}

		m_fModified = FALSE;
		m_fEdit = TRUE;
	}

	m_nIdx--;
	pos = m_pListS->FindIndex( m_nIdx );
	if( pos )
	{
		CWnd* pWndH = GetDlgItem( IDC_CHK_HDR );
		CWnd* pWndP = GetDlgItem( IDC_CHK_PRIVATE );
		CWnd* pWndQ = GetDlgItem( IDC_EDIT_Q );
		CWnd* pWndN = GetDlgItem( IDC_CHK_USE );
		CWnd* pWndCH = GetDlgItem( IDC_EDIT_DESC );
		CString szS = m_pListS->GetNext( pos );
		if( szS == Q_STATE_DEL )
		{
			pWndH->EnableWindow( FALSE );
			pWndP->EnableWindow( FALSE );
			pWndQ->EnableWindow( FALSE );
			pWndN->EnableWindow( FALSE );
			pWndCH->EnableWindow( FALSE );
			fDel = TRUE;
		}
		else
		{
			pWndH->EnableWindow( TRUE );
			pWndP->EnableWindow( TRUE );
			pWndQ->EnableWindow( TRUE );
			pWndN->EnableWindow( TRUE );
			pWndCH->EnableWindow( TRUE );
		}
	}

	if( m_nIdx < 0 )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_PREVQ );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	else
	{
		pos = m_pListH->FindIndex( m_nIdx );
		CString szHdr = m_pListH->GetAt( pos );
		m_fHdr = szHdr != _T("");
		pos = m_pListP->FindIndex( m_nIdx );
		CString szPrivate = m_pListP->GetAt( pos );
		m_fPrivate = szPrivate != _T("");
		pos = m_pListN->FindIndex( m_nIdx );
		CString szNumeric = m_pListN->GetAt( pos );
		m_fNumeric = szNumeric != _T("");
		pos = m_pListQ->FindIndex( m_nIdx );
		m_szQuestion = m_pListQ->GetAt( pos );
		pos = m_pListCH->FindIndex( m_nIdx );
		m_szColHeader = m_pListCH->GetAt( pos );
		pos = m_pListID->FindIndex( m_nIdx );
		m_szID = m_pListID->GetAt( pos );
	}

	UpdateData( FALSE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd )
	{
		pWnd->EnableWindow( FALSE );
		pWnd->SetFocus();
	}
	pWnd = GetDlgItem( IDC_BN_PREVQ );
	if( pWnd ) pWnd->EnableWindow( !( m_nIdx <= 0 ) );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	pWnd->EnableWindow( !m_fHdr );
}

void QuestionnaireDlg::OnBnNextq() 
{
	if( !m_pListH || !m_pListQ || !m_pListID || !m_pListCH || !m_pListN ) return;

	POSITION pos = NULL;
	BOOL fDel = FALSE;

	UpdateData( TRUE );

	if( m_fModified )
	{
		if( m_szID.GetLength() != szIDS )
		{
			CString szMsg;
			if( m_fClose ) szMsg = _T("There is an invalid ID. Click YES to continue without saving, click NO to correct the ID.");
			else szMsg = _T("Invalid ID. The ID must be 3 characters.");
			int nRes = BCGPMessageBox( szMsg, m_fClose ? MB_YESNO : MB_ICONERROR );
			if( !m_fClose || ( nRes == IDNO ) )
			{
				CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
				if( pWnd ) pWnd->SetFocus();
				m_fCanClose = FALSE;
				return;
			}
		}

		if( !m_fHdr && ( m_szColHeader == _T("") ) )
		{
			CString szMsg;
			if( m_fClose ) szMsg = _T("You must specify a column header. Click YES to continue without saving, click NO to correct the header.");
			else szMsg = _T("You must specify a column header.");
			int nRes = BCGPMessageBox( szMsg, m_fClose ? MB_YESNO : MB_ICONERROR );
			if( !m_fClose || ( nRes == IDNO ) )
			{
				CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
				if( pWnd ) pWnd->SetFocus();
				m_fCanClose = FALSE;
				return;
			}
		}
	}

	if( m_fModified && ( m_szID.GetLength() == szIDS ) )
	{
		pos = m_pListID->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListID->RemoveAt( pos );
				pos = m_pListID->FindIndex( m_nIdx );
				if( pos ) m_pListID->InsertBefore( pos, m_szID );
				else m_pListID->AddTail( m_szID );
			}
			else m_pListID->InsertBefore( pos, m_szID );
		}
		else m_pListID->AddTail( m_szID );

		pos = m_pListH->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListH->RemoveAt( pos );
				pos = m_pListH->FindIndex( m_nIdx );
				if( pos ) m_pListH->InsertBefore( pos, m_fHdr ? _T("TRUE") : _T("") );
				else m_pListH->AddTail( m_fHdr ? _T("TRUE") : _T("") );
			}
			else m_pListH->InsertBefore( pos, m_fHdr ? _T("TRUE") : _T("") );
		}
		else m_pListH->AddTail( m_fHdr ? _T("TRUE") : _T("") );

		pos = m_pListP->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListP->RemoveAt( pos );
				pos = m_pListP->FindIndex( m_nIdx );
				if( pos ) m_pListP->InsertBefore( pos, m_fPrivate ? _T("TRUE") : _T("") );
				else m_pListP->AddTail( m_fPrivate ? _T("TRUE") : _T("") );
			}
			else m_pListP->InsertBefore( pos, m_fPrivate ? _T("TRUE") : _T("") );
		}
		else m_pListP->AddTail( m_fPrivate ? _T("TRUE") : _T("") );

		pos = m_pListN->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListN->RemoveAt( pos );
				pos = m_pListN->FindIndex( m_nIdx );
				if( pos ) m_pListN->InsertBefore( pos, m_fNumeric ? _T("TRUE") : _T("") );
				else m_pListN->AddTail( m_fNumeric ? _T("TRUE") : _T("") );
			}
			else m_pListN->InsertBefore( pos, m_fNumeric ? _T("TRUE") : _T("") );
		}
		else m_pListN->AddTail( m_fNumeric ? _T("TRUE") : _T("") );

		pos = m_pListQ->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListQ->RemoveAt( pos );
				pos = m_pListQ->FindIndex( m_nIdx );
				if( pos ) m_pListQ->InsertBefore( pos, m_szQuestion );
				else m_pListQ->AddTail( m_szQuestion );
			}
			else m_pListQ->InsertBefore( pos, m_szQuestion );
		}
		else m_pListQ->AddTail( m_szQuestion );

		pos = m_pListCH->FindIndex( m_nIdx );
		if( pos )
		{
			if( m_fEdit )
			{
				m_pListCH->RemoveAt( pos );
				pos = m_pListCH->FindIndex( m_nIdx );
				if( pos ) m_pListCH->InsertBefore( pos, m_szColHeader );
				else m_pListCH->AddTail( m_szColHeader );
			}
			else m_pListCH->InsertBefore( pos, m_szColHeader );
		}
		else m_pListCH->AddTail( m_szColHeader );


		CString szS = m_fNew ? Q_STATE_NEW : Q_STATE_DIFF;
		pos = m_pListS->FindIndex( m_nIdx );
		if( pos )
		{
			CString szOldS = m_pListS->GetAt( pos );
			if( szOldS == Q_STATE_NEW ) szS = Q_STATE_NEW;
			if( m_fEdit )
			{
				m_pListS->RemoveAt( pos );
				pos = m_pListS->FindIndex( m_nIdx );
				if( pos ) m_pListS->InsertBefore( pos, szS );
				else m_pListS->AddTail( szS );
			}
			else m_pListS->InsertBefore( pos, szS );
		}
		else m_pListS->AddTail( szS );

		pos = m_pListI->FindIndex( m_nIdx );
		if( pos )
		{
			CString szI = m_pListI->GetAt( pos );
			if( m_fEdit )
			{
				m_pListI->RemoveAt( pos );
				pos = m_pListI->FindIndex( m_nIdx );
				if( pos ) m_pListI->InsertBefore( pos, szI );
				else m_pListI->AddTail( szI );
			}
			else m_pListI->InsertBefore( pos, szI );
		}
		else
		{
			CString szI = _T("");
			m_pListI->AddTail( szI );
		}

		m_fModified = FALSE;
//		m_fEdit = TRUE;
	}
	else if( m_nIdx > ( m_pListH->GetCount() - 1 ) ) return;

	if( m_fClose ) return;

	m_nIdx++;
	pos = m_pListS->FindIndex( m_nIdx );
	if( pos )
	{
		CWnd* pWndH = GetDlgItem( IDC_CHK_HDR );
		CWnd* pWndP = GetDlgItem( IDC_CHK_PRIVATE );
		CWnd* pWndQ = GetDlgItem( IDC_EDIT_Q );
		CWnd* pWndN = GetDlgItem( IDC_CHK_USE );
		CWnd* pWndCH = GetDlgItem( IDC_EDIT_DESC );
		CString szS = m_pListS->GetNext( pos );
		if( szS == Q_STATE_DEL )
		{
			pWndH->EnableWindow( FALSE );
			pWndP->EnableWindow( FALSE );
			pWndQ->EnableWindow( FALSE );
			pWndN->EnableWindow( FALSE );
			pWndCH->EnableWindow( FALSE );
			fDel = TRUE;
		}
		else
		{
			pWndH->EnableWindow( TRUE );
			pWndP->EnableWindow( TRUE );
			pWndQ->EnableWindow( TRUE );
			pWndN->EnableWindow( TRUE );
			pWndCH->EnableWindow( TRUE );
		}
	}

	if( m_fAdd || ( m_nIdx >= m_pListH->GetCount() ) )
	{
		m_szQuestion = _T("");
		m_szID = _T("");
		m_szColHeader = _T("");
		m_fHdr = FALSE;
		m_fPrivate = FALSE;
		m_fNew = TRUE;
	}
	else
	{
		pos = m_pListH->FindIndex( m_nIdx );
		CString szHdr = m_pListH->GetAt( pos );
		m_fHdr = szHdr != _T("");
		pos = m_pListP->FindIndex( m_nIdx );
		CString szPrivate = m_pListP->GetAt( pos );
		m_fPrivate = szPrivate != _T("");
		pos = m_pListN->FindIndex( m_nIdx );
		CString szNumeric = m_pListN->GetAt( pos );
		m_fNumeric = szNumeric != _T("");
		pos = m_pListQ->FindIndex( m_nIdx );
		m_szQuestion = m_pListQ->GetAt( pos );
		pos = m_pListCH->FindIndex( m_nIdx );
		m_szColHeader = m_pListCH->GetAt( pos );
		pos = m_pListID->FindIndex( m_nIdx );
		m_szID = m_pListID->GetAt( pos );
		m_fNew = FALSE;
	}

	UpdateData( FALSE );

	CWnd* pWnd = GetDlgItem( IDC_BN_PREVQ );
	if( pWnd && !m_fAdd ) pWnd->EnableWindow( TRUE );
	pWnd = GetDlgItem( IDC_CHK_PRIVATE );
	if( pWnd ) pWnd->EnableWindow( !m_fHdr );
	pWnd = GetDlgItem( IDC_CHK_USE );
	if( pWnd ) pWnd->EnableWindow( !m_fHdr );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) pWnd->EnableWindow( !m_fHdr );
	pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) pWnd->EnableWindow( m_fNew );
	pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) pWnd->SetFocus();
}

BOOL QuestionnaireDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	m_bnPrev.SetImage( IDB_PREV );
	m_bnNext.SetImage( IDB_NEXT );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_Q );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_Q, _T("Question/Topic") );
	pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_QID, _T("ID") );
	pWnd = GetDlgItem( IDC_BN_PREVQ );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PREV_Q, _T("Previous Question") );
	pWnd = GetDlgItem( IDC_BN_NEXTQ );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_NEXT_Q, _T("Next Question") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close dialog."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CANCEL, _T("Cancel") );
	pWnd = GetDlgItem( IDC_CHK_HDR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this to make this a topic category instead of a question."), _T("Category") );
	pWnd = GetDlgItem( IDC_CHK_PRIVATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this to make this a private question."), _T("Private") );
	pWnd = GetDlgItem( IDC_CHK_USE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this if all answers are to be numeric."), _T("Numeric") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the desired column header for statistical analysis."), _T("Column Header") );


	CWnd* pWndP = GetDlgItem( IDC_BN_PREVQ );
	if( m_nIdx <= 0 ) pWndP->EnableWindow( FALSE );
	CWnd* pWndN = GetDlgItem( IDC_BN_NEXTQ );
	CWnd* pWndPriv = GetDlgItem( IDC_CHK_PRIVATE );
	CWnd* pWndNum = GetDlgItem( IDC_CHK_USE );
	CWnd* pWndCH = GetDlgItem( IDC_EDIT_DESC );
	if( m_pListH && m_pListQ && m_pListID && m_fEdit )
	{
		m_fNew = FALSE;

// 		pWndP->ShowWindow( SW_HIDE );
// 		pWndN->ShowWindow( SW_HIDE );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );

		int nCnt = (int)m_pListQ->GetCount();
		if( ( m_nIdx >= 0 ) && ( m_nIdx < nCnt ) )
		{
			POSITION pos = m_pListH->FindIndex( m_nIdx );
			CString szHdr = m_pListH->GetAt( pos );
			pos = m_pListP->FindIndex( m_nIdx );
			CString szPrivate = m_pListP->GetAt( pos );
			pos = m_pListN->FindIndex( m_nIdx );
			CString szNumeric = m_pListN->GetAt( pos );
			pos = m_pListQ->FindIndex( m_nIdx );
			m_szQuestion = m_pListQ->GetAt( pos );
			pos = m_pListCH->FindIndex( m_nIdx );
			m_szColHeader = m_pListCH->GetAt( pos );
			pos = m_pListID->FindIndex( m_nIdx );
			m_szID = m_pListID->GetAt( pos );
			if( szHdr == _T("") ) m_fHdr = FALSE;
			else m_fHdr = TRUE;
			if( szPrivate == _T("") ) m_fPrivate = FALSE;
			else m_fPrivate = TRUE;
			if( szNumeric == _T("") ) m_fNumeric = FALSE;
			else m_fNumeric = TRUE;

			pWndPriv->EnableWindow( !m_fHdr );
			pWndNum->EnableWindow( !m_fHdr );
			pWndCH->EnableWindow( !m_fHdr );
			if( m_fHdr )
			{
				m_fPrivate = FALSE;
				m_fNumeric = FALSE;
			}

			UpdateData( FALSE );
		}

		SetDefID( IDOK );
	}
	else if( !m_fEdit )
	{
		pWnd = GetDlgItem( IDOK );
		pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDCANCEL );
		pWnd->SetWindowText( _T("Done") );
		pWnd = GetDlgItem( IDC_BN_PREVQ );
		pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL QuestionnaireDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void QuestionnaireDlg::OnChkHdr() 
{
	UpdateData( TRUE );

	m_fModified = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_CHK_PRIVATE );
	if( pWnd ) pWnd->EnableWindow( !m_fHdr );
	if( m_fHdr )
	{
		m_fPrivate = FALSE;
		m_fNumeric = FALSE;
		m_szColHeader = _T("");
	}

	pWnd = GetDlgItem( IDC_EDIT_Q );
	if( pWnd ) pWnd->SetFocus();

	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) pWnd->EnableWindow( !m_fHdr );

	UpdateData( FALSE );
}

void QuestionnaireDlg::OnChkPrivate() 
{
	m_fModified = TRUE;
}

void QuestionnaireDlg::OnChkNumeric()
{
	m_fModified = TRUE;
}

void QuestionnaireDlg::OnChangeEditID() 
{
	m_fModified = TRUE;
}

void QuestionnaireDlg::OnChangeEditQ() 
{
	m_fModified = TRUE;
}

void QuestionnaireDlg::OnChangeEditCH()
{
	m_fModified = TRUE;
}

void QuestionnaireDlg::OnOK()
{
	m_fCanClose = TRUE;
	m_fClose = TRUE;
	OnBnNextq();
	if( m_fCanClose ) CBCGPDialog::OnOK();
}

void QuestionnaireDlg::OnCancel()
{
	m_fCanClose = TRUE;
	m_fClose = TRUE;
	if( m_fAdd ) OnBnNextq();
	if( m_fCanClose ) CBCGPDialog::OnCancel();
	else m_fCanClose = TRUE;
}

void QuestionnaireDlg::OnKillfocusEditId() 
{
	UpdateData( TRUE );

	// Check for existence of ID if a new item
	if( m_fNew && m_fModified )
	{
		POSITION pos = m_pListID->Find( m_szID );
		if( pos )
		{
			BCGPMessageBox( IDS_ID_EXISTS );
			m_szID = _T("");
			UpdateData( FALSE );
			m_fModified = FALSE;
			CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
			if( pWnd ) pWnd->SetFocus();
			return;
		}
	}
	// auto-fill/suggest column header if not already filled
	if( m_fNew && ( m_szID.GetLength() == szIDS ) && !m_fHdr && ( m_szColHeader == _T("") ) )
	{
		m_szColHeader = _T("Q_");
		m_szColHeader += m_szID;
		UpdateData( FALSE );
		CEdit* pEdit = (CEdit*)GetDlgItem( IDC_EDIT_DESC );
		if( pEdit ) pEdit->SetSel( 0, -1 );
	}
}

BOOL QuestionnaireDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("questionnaire.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

#pragma once

// AddDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// AddDlg dialog

class AFX_EXT_CLASS AddDlg : public CBCGPDialog
{
// Construction
public:
	AddDlg(CStringList* pExisting, CWnd* pParent = NULL, BOOL fSingleSel = FALSE );

// Dialog Data
	enum { IDD = IDD_ADDLIST };
	CBCGPListCtrl	m_list;
	CStringList*	m_pExisting;
	CStringList		m_lstNew;
	BOOL			m_fChanged;
	NSToolTipCtrl	m_tooltip;
	CImageList		m_ImageList;
	BOOL			m_fSingleSel;
	CBCGPButton		m_bnAdd;
	CBCGPButton		m_bnDel;
	CBCGPButton		m_bnEdit;
	CBCGPButton		m_bnRel;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	void SetSingleSelect( BOOL fVal );
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual int GetImageNum() = 0;
	virtual void InsertColumns() = 0;
	virtual void FillList() = 0;
	virtual int Add() { return -1; }					// return is idx of new item
	virtual BOOL Edit( int nItem ) { return FALSE; }	// return is wheter modified
	virtual BOOL Delete( int nItem ) { return FALSE; }	// return is whether deleted
	virtual void Relationships( int nItem ) { return; }
	virtual void OK() {}
	virtual void Cleanup() = 0;

protected:
	afx_msg void OnClickList(NMHDR*, LRESULT*);
	afx_msg void OnDblclkList( NMHDR*, LRESULT* );
	virtual BOOL OnInitDialog();
	afx_msg void OnClickBnEdit();
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnDel();
	afx_msg void OnClickBnRel();
	afx_msg void OnLvnKeydownList(NMHDR *pNMHDR, LRESULT *pResult);
	DECLARE_MESSAGE_MAP()
};

// Cats.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "CatDlg.h"
#include "OverwriteDlg.h"
#include "Cats.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

void Cats::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	Cats::GetDescriptionWithID( m_pCat, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::GetNext()
{
	return Cats::GetNext( m_pCat );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Find( CString szID )
{
	return Cats::Find( m_pCat, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Add()
{
	return Cats::Add( m_pCat );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Modify()
{
	return Cats::Modify( m_pCat );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Remove()
{
	return Cats::Remove( m_pCat );
}

///////////////////////////////////////////////////////////////////////////////

void Cats::SetDataPath( CString szPath )
{
	Cats::SetDataPath( m_pCat, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Cats::GetID( ICat* pCat, CString& szVal )
{
	if( pCat )
	{
		BSTR bstrItem;
		pCat->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Cats::GetDescription( ICat* pCat, CString& szVal )
{
	if( pCat )
	{
		BSTR bstrItem;
		pCat->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Cats::GetDescriptionWithID( ICat* pCat, CString& szVal, BOOL fReverse )
{
	if( pCat )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		Cats::GetID( pCat, szID );
		pCat->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Cats::GetType( ICat* pCat, CatType& val )
{
	if( pCat ) pCat->get_Type( &val );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::ResetToStart()
{
	return Cats::ResetToStart( m_pCat );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::ResetToStart( ICat* pCat )
{
	if( pCat ) return pCat->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::GetNext( ICat* pCat )
{
	if( pCat ) return pCat->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Find( ICat* pCat, CString szID )
{
	if( pCat ) return pCat->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Add( ICat* pCat )
{
	if( !pCat ) return E_FAIL;

	if( FAILED( pCat->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Cats::Add") );

		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Modify( ICat* pCat )
{
	if( !pCat ) return E_FAIL;

	if( FAILED( pCat->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Cats::Remove( ICat* pCat )
{
	if( !pCat ) return E_FAIL;

	if( FAILED( pCat->Remove() ) )
	{
		BCGPMessageBox( IDS_GRPDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Cats::SetDataPath( ICat* pCat, CString szPath )
{
	if( pCat ) pCat->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoAdd( CWnd* pOwner )
{
	return Cats::DoAdd( m_pCat, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoAdd( ICat* pCat, CWnd* pOwner )
{
	if( !pCat ) return FALSE;

	CatDlg d( pOwner, TRUE );
	if( d.DoModal() == IDOK )
	{
		if( d.m_szID.GetLength() == 0 )
		{
			BCGPMessageBox( IDS_GRPID_ERR );
			return FALSE;
		}

		pCat->put_ID( d.m_szID.AllocSysString() );
		pCat->put_Description( d.m_szDesc.AllocSysString() );
		pCat->put_Type( (CatType)d.m_nType );

		HRESULT hr = Cats::Add( pCat );
		if( SUCCEEDED( hr ) ) return TRUE;
		else {
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Cats::DoAdd") );
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoEdit( CString szID, CWnd* pOwner )
{
	return Cats::DoEdit( m_pCat, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoEdit( ICat* pCat, CString szID, CWnd* pOwner )
{
	if( !pCat ) return FALSE;

	CString szDesc;
	CatType nType = (CatType)0 ;

	CatDlg d( pOwner );
	if( FAILED( Cats::Find( pCat, szID ) ) ) return FALSE;
	Cats::GetDescription( pCat, szDesc );
	Cats::GetType( pCat, nType );
	d.m_szID = szID;
	d.m_szDesc = szDesc;
	d.m_nType = nType;
	if( d.DoModal() == IDOK )
	{
		pCat->put_Description( d.m_szDesc.AllocSysString() );
		pCat->put_Type( (CatType)d.m_nType );
		HRESULT hr = Cats::Modify( pCat );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_UPD_ERR );
			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::Export( CMemFile* pFile )
{
	if( !m_pCat || !pFile ) return FALSE;

	CString szItem;

	WRITE_STRING( pFile, TAG_CAT );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	Cats::GetID( m_pCat, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Desc
	Cats::GetDescription( m_pCat, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Type
	CatType nType = (CatType)0;
	Cats::GetType( m_pCat, nType );
	szItem.Format( _T("%d"), (short)nType );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan )
{
//	if( !m_pCat || !pFile || !pFile->m_pStream ) return FALSE;
	if( !m_pCat || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	Cats comp;
	BOOL fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Category %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );
	// Type
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	short nType = atoi( szLine );
	PutType( (CatType)nType );
	comp.PutType( (CatType)nType );

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}
	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_CAT, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		CFileFind ff;
		if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
		if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			CatType nT;
			comp.GetType( nT );
			PutType( nT );
			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Cats::ForceLocal()
{
	if( !m_pCat ) return;

	HRESULT hr = m_pCat->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Cats::ForceRemote()
{
	if( !m_pCat ) return;

	HRESULT hr = m_pCat->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Cats::ForceDefault()
{
	if( !m_pCat ) return;

	HRESULT hr = m_pCat->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		Cats dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else dest.ForceRemote();

		// does it already exist?
		hr = dest.Find( szID );
		
		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

void Cats::SetTemp( BOOL fVal )
{
	if( m_pCat ) m_pCat->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Cats::DumpRecord( CString szID, CString szFile )
{
	if( !m_pCat ) return FALSE;
	return SUCCEEDED( m_pCat->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Cats::RemoveTemp()
{
	if( m_pCat ) m_pCat->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Cats::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pCat ) m_pCat->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

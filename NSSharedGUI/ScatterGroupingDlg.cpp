// ScatterGroupingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ScatterGroupingDlg.h"
#include "ScatterOptionsDlg.h"
#include "AGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ScatterGroupingDlg dialog

BEGIN_MESSAGE_MAP(ScatterGroupingDlg, CBCGPDialog)
	ON_CBN_SELCHANGE(IDC_CBO_ITEM, OnSelchangeCboItem)
	ON_CBN_SELCHANGE(IDC_CBO_POINT, OnSelchangeCboPoint)
	ON_CBN_SELCHANGE(IDC_CBO_SIZE, OnSelchangeCboSize)
	ON_BN_CLICKED(IDC_CHK_GROUPING, OnChkGrouping)
	ON_BN_CLICKED(IDC_BN_COLOR, OnClickBnColor)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ScatterGroupingDlg::ScatterGroupingDlg( CWnd* pParent )
	: CBCGPDialog( ScatterGroupingDlg::IDD, pParent )
{
	m_fGrouping = FALSE;
	m_nPoints = 0;
	m_nItem = -1;
	m_nPoint = -1;
	m_nSize = -1;
	m_nGrouping = -1;
	m_pPoints = NULL;
	m_pSizes = NULL;
	m_pColors = NULL;
}

ScatterGroupingDlg::~ScatterGroupingDlg()
{
	if( m_pPoints ) delete [] m_pPoints;
	if( m_pSizes ) delete [] m_pSizes;
	if( m_pColors ) delete [] m_pColors;
}

void ScatterGroupingDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_CBO_ITEM, m_nItem);
	DDX_CBIndex(pDX, IDC_CBO_POINT, m_nPoint );
	DDX_CBIndex(pDX, IDC_CBO_SIZE, m_nSize );
	DDX_Check(pDX, IDC_CHK_GROUPING, m_fGrouping);
	DDX_Control(pDX, IDC_BN_COLOR, m_bnColor);
}

/////////////////////////////////////////////////////////////////////////////
// ScatterGroupingDlg message handlers

BOOL ScatterGroupingDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL ScatterGroupingDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	EnableVisualManagerStyle( TRUE, TRUE );

	CWaitCursor crs;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_GROUPING );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Apply specific colors, point types, and point sizes to groupings.") );
	pWnd = GetDlgItem( IDC_CBO_ITEM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an item of the specified group to assign point type, size, and color.") );
	pWnd = GetDlgItem( IDC_CBO_POINT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Point type assigned to the specified item.") );
	pWnd = GetDlgItem( IDC_CBO_SIZE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Point size assigned to the specified item.") );
	pWnd = GetDlgItem( IDC_BN_COLOR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Point color assigned to the specified item.") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// Point types
	CComboBox* pCBO = (CComboBox*)GetDlgItem( IDC_CBO_POINT );
	if( pCBO )
	{
		pCBO->AddString( _T("Plus") );
		pCBO->AddString( _T("Cross") );
		pCBO->AddString( _T("Dot") );
		pCBO->AddString( _T("Solid Dot") );
		pCBO->AddString( _T("Square") );
		pCBO->AddString( _T("Solid Square") );
		pCBO->AddString( _T("Diamond") );
		pCBO->AddString( _T("Solid Diamond") );
		pCBO->AddString( _T("Upward Triangle") );
		pCBO->AddString( _T("Solid Upward Triangle") );
		pCBO->AddString( _T("Downward Triangle") );
		pCBO->AddString( _T("Solid Downward Triangle") );
		if( ( m_nGrouping != -1 ) && ( m_nItem != -1 ) && m_pPoints )
			pCBO->SetCurSel( m_pPoints[ m_nItem ] );
	}
	// Point sizes
	pCBO = (CComboBox*)GetDlgItem( IDC_CBO_SIZE );
	if( pCBO )
	{
		pCBO->AddString( _T("Small") );
		pCBO->AddString( _T("Medium") );
		pCBO->AddString( _T("Large") );
		pCBO->AddString( _T("Micro") );
		if( ( m_nGrouping != -1 ) && ( m_nItem != -1 ) && m_pSizes )
			pCBO->SetCurSel( m_pSizes[ m_nItem ] );
	}

	// Items
	AGraphDlg* pParent = (AGraphDlg*)GetParent();
	m_nGrouping = pParent ? pParent->m_nFactor1 - 1 : -1;
	ScatterOptionsDlg* pOpts = &pParent->m_dlgOptions;
	if( pOpts && ( m_nGrouping != -1 ) )
	{
		if( pOpts->m_fHasChanged )
		{
			m_nItem = -1;
			m_fGrouping = FALSE;
			UpdateData( FALSE );

			if( m_pPoints ) delete [] m_pPoints;
			m_pPoints = NULL;
			if( m_pSizes ) delete [] m_pSizes;
			m_pSizes = NULL;
			if( m_pColors ) delete [] m_pColors;
			m_pColors = NULL;
		}

		int nPoints = 0;
		CStringList* pList = NULL;
		switch( m_nGrouping )
		{
			case 0:		// Group
				nPoints = (int)pOpts->m_lstGrp.GetCount();
				pList = &pOpts->m_lstGrp;
				break;
			case 1:		// Subject
				nPoints = (int)pOpts->m_lstSubj.GetCount();
				pList = &pOpts->m_lstSubj;
				break;
			case 2:		// Condition
				nPoints = (int)pOpts->m_lstCond.GetCount();
				pList = &pOpts->m_lstCond;
				break;
			case 3:		// Trial
				nPoints = (int)pOpts->m_lstTrial.GetCount();
				pList = &pOpts->m_lstTrial;
				break;
			case 4:		// Stroke
				nPoints = (int)pOpts->m_lstStroke.GetCount();
				pList = &pOpts->m_lstStroke;
				break;
// 			case 5:		// submovement
// 				if( pParent->m_fsma )
// 				{
// 					nPoints = (int)pOpts->m_lstSubMvmt.GetCount();
// 					pList = &pOpts->m_lstSubMvmt;
// 				}
		}

		if( nPoints > 0 )
		{
			if( pOpts->m_fHasChanged )
			{
				m_nPoints = nPoints;
				m_pPoints = new int[ nPoints ];
				m_pSizes = new int[ nPoints ];
				m_pColors = new DWORD[ nPoints ];
				for( int i = 0; i < nPoints; i++ )
				{
					int nP = i * 2;
					nP = nP % 11;
					m_pPoints[ i ] = nP;
					m_pSizes[ i ] = 3;
					m_pColors[ i ] = ::GetColorByIndex( i, nPoints );
				}
				pOpts->m_fHasChanged = FALSE;
			}

			CComboBox* pCBO = (CComboBox*)GetDlgItem( IDC_CBO_ITEM );
			if( pCBO && pList )
			{
				pCBO->ResetContent();
				CString szItem;
				POSITION pos = pList->GetHeadPosition();
				// Skip the "All"
				if( pos ) szItem = pList->GetNext( pos );
				while( pos )
				{
					szItem = pList->GetNext( pos );
					int nItem = atoi( szItem );
					if( nItem < 10 )
					{
						CString szTemp = _T("0");
						szTemp += szItem;
						szItem = szTemp;
					}
					pCBO->AddString( szItem );
				}
				// select item
				if( ( m_nItem != -1 ) && ( m_nItem < pCBO->GetCount() ) )
					pCBO->SetCurSel( m_nItem );
				else pCBO->SetCurSel( 0 );
			}
		}
	
		OnSelchangeCboItem();
	}

	// Color buttons
	if( pParent && ( m_nGrouping != -1 ) && ( m_nItem != -1 ) && m_pColors )
		m_bnColor.SetColor( m_pColors[ m_nItem ] );

	OnChkGrouping();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ScatterGroupingDlg::OnSelchangeCboItem() 
{
	OnChkGrouping();

	UpdateData( TRUE );
	if( m_nItem == -1 ) return;

	if( m_pPoints ) m_nPoint = m_pPoints[ m_nItem ];
	if( m_pSizes ) m_nSize = m_pSizes[ m_nItem ];
	if( m_pColors ) m_bnColor.SetColor( m_pColors[ m_nItem ] );
	UpdateData( FALSE );
}

void ScatterGroupingDlg::OnClickBnColor() 
{
	if( m_pColors ) m_pColors[ m_nItem ] = m_bnColor.GetColor();
}

void ScatterGroupingDlg::OnSelchangeCboPoint() 
{
	if( m_pPoints )
	{
		UpdateData( TRUE );

		m_pPoints[ m_nItem ] = m_nPoint;
	}
}

void ScatterGroupingDlg::OnSelchangeCboSize() 
{
	if( m_pSizes )
	{
		UpdateData( TRUE );

		for( int i = 0; i < m_nPoints; m_pSizes[ i++ ] = m_nSize );
//		m_pSizes[ m_nItem ] = m_nSize;
	}
}

void ScatterGroupingDlg::OnChkGrouping() 
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_CHK_GROUPING );
	if( pWnd ) pWnd->EnableWindow( m_nGrouping != -1 );
	pWnd = GetDlgItem( IDC_CBO_ITEM );
	if( pWnd ) pWnd->EnableWindow( m_fGrouping && ( m_nGrouping != -1 ) );
	pWnd = GetDlgItem( IDC_CBO_POINT );
	if( pWnd ) pWnd->EnableWindow( m_fGrouping && ( m_nGrouping != -1 ) && ( m_nItem != -1 ) );
	pWnd = GetDlgItem( IDC_CBO_SIZE );
	if( pWnd ) pWnd->EnableWindow( m_fGrouping && ( m_nGrouping != -1 ) && ( m_nItem != -1 ) );
	pWnd = GetDlgItem( IDC_BN_COLOR );
	if( pWnd ) pWnd->EnableWindow( m_fGrouping && ( m_nGrouping != -1 ) && ( m_nItem != -1 ) );
}

BOOL ScatterGroupingDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("analysischarts.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

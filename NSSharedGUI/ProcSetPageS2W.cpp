// ProcSetPageS2W.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageS2W.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageS2W property page

IMPLEMENT_DYNCREATE(ProcSetPageS2W, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageS2W, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
END_MESSAGE_MAP()

ProcSetPageS2W::ProcSetPageS2W( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageS2W::IDD), m_pPS( pPS )
{
	minleftpenup = 0.5;
	minleftsp = 0.25;
	minwidth = 0.1;
	mindownsp = 1.0;
	mintimepenup = 0.6;
	maxtimepenup = 0.01;

	m_editMaxTP.SetDefaultValue( maxtimepenup );
	m_editMinDSP.SetDefaultValue( mindownsp );
	m_editMinTP.SetDefaultValue( mintimepenup );
	m_editMinLP.SetDefaultValue( minleftpenup );
	m_editMinLSP.SetDefaultValue( minleftsp );
	m_editWidth.SetDefaultValue( minwidth );

	if( m_pPS )
	{
		m_pPS->get_MaxTimePenup( &maxtimepenup );
		m_pPS->get_MinDownSpacing( &mindownsp );
		m_pPS->get_MinTimePenup( &mintimepenup );
		m_pPS->get_MinLeftPenup( &minleftpenup );
		m_pPS->get_MinLeftSpacing( &minleftsp );
		m_pPS->get_MinWordWidth( &minwidth );
	}
}

ProcSetPageS2W::~ProcSetPageS2W()
{
}

void ProcSetPageS2W::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MAXTP, maxtimepenup);
	DDX_Text(pDX, IDC_EDIT_MDS, mindownsp);
	DDX_Text(pDX, IDC_EDIT_MINTP, mintimepenup);
	DDX_Text(pDX, IDC_EDIT_MLP, minleftpenup);
	DDX_Text(pDX, IDC_EDIT_MLS, minleftsp);
	DDX_Text(pDX, IDC_EDIT_MWW, minwidth);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);

	DDX_Control(pDX, IDC_EDIT_MAXTP, m_editMaxTP);
	DDX_Control(pDX, IDC_EDIT_MDS, m_editMinDSP);
	DDX_Control(pDX, IDC_EDIT_MINTP, m_editMinTP);
	DDX_Control(pDX, IDC_EDIT_MLP, m_editMinLP);
	DDX_Control(pDX, IDC_EDIT_MLS, m_editMinLSP);
	DDX_Control(pDX, IDC_EDIT_MWW, m_editWidth);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageS2W message handlers

BOOL ProcSetPageS2W::OnApply() 
{
	if( !m_pPS ) return FALSE;

	m_pPS->put_MaxTimePenup( maxtimepenup );
	m_pPS->put_MinDownSpacing( mindownsp );
	m_pPS->put_MinTimePenup( mintimepenup );
	m_pPS->put_MinLeftPenup( minleftpenup );
	m_pPS->put_MinLeftSpacing( minleftsp );
	m_pPS->put_MinWordWidth( minwidth );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageS2W::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageS2W::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	double dmaxtimepenup = 0., dmindownsp = 0., dmintimepenup = 0., dminleftpenup = 0., dminleftsp = 0., dminwidth = 0.;
	if( pPS )
	{
		pPS->get_MaxTimePenup( &dmaxtimepenup );
		pPS->get_MinDownSpacing( &dmindownsp );
		pPS->get_MinTimePenup( &dmintimepenup );
		pPS->get_MinLeftPenup( &dminleftpenup );
		pPS->get_MinLeftSpacing( &dminleftsp );
		pPS->get_MinWordWidth( &dminwidth );

		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_MAXTP );
	szData = _T("Maximum duration of penlifts within a word (s)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dmaxtimepenup );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Duration") );

	pWnd = GetDlgItem( IDC_EDIT_MDS );
	szData = _T("Minimum leftward distance between words (cm)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dmindownsp );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Distance") );

	pWnd = GetDlgItem( IDC_EDIT_MINTP );
	szData = _T("Minimum duration of penlifts between words (s)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dmintimepenup );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData , _T("Min Duration") );

	pWnd = GetDlgItem( IDC_EDIT_MLP );
	szData = _T("Minimum leftward penlift movement between words (cm)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dminleftpenup );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Penlift") );

	pWnd = GetDlgItem( IDC_EDIT_MLS );
	szData = _T("Minimum leftward distance between words (cm)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dminleftsp );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Distance") );

	pWnd = GetDlgItem( IDC_EDIT_MWW );
	szData = _T("Minimum word width (cm)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dminwidth );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Width") );

	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );
	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageS2W::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageS2W::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_wordextraction.html"), this );
	return TRUE;
}

void ProcSetPageS2W::OnBnReset() 
{
	minleftpenup = 0.5;
	minleftsp = 0.25;
	minwidth = 0.1;
	mindownsp = 1.0;
	mintimepenup = 0.6;
	maxtimepenup = 0.01;

	UpdateData( FALSE );
}

void ProcSetPageS2W::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

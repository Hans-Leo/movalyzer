// ProcSetPageSum.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageSum.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSum property page

IMPLEMENT_DYNCREATE(ProcSetPageSum, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageSum, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_SUMT, OnChkSumt)
	ON_BN_CLICKED(IDC_CHK_SUMS, OnChkSums)
	ON_BN_CLICKED(IDC_CHK_SUMD, OnChkSumd)
	ON_BN_CLICKED(IDC_CHK_SUMD2, OnChkSumd2)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_BN_CLICKED(IDC_CHK_UDSTROKES, OnChkUDStrokes)
	ON_BN_CLICKED(IDC_CHK_STROKES, OnChkStrokes)
	ON_BN_CLICKED(IDC_CHK_TRIALS, OnChkTrials)
	ON_BN_CLICKED(IDC_CHK_DISC_LIFT, OnChkDiscWOLift)
	ON_BN_CLICKED(IDC_CHK_DISC_LIFT2, OnChkDiscWLift)
END_MESSAGE_MAP()

ProcSetPageSum::ProcSetPageSum( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageSum::IDD), m_pPS( pPS )
{
	m_fA = FALSE;
	m_fR = FALSE;
	m_fT = FALSE;
	m_fZ = FALSE;
	m_fS = FALSE;
	m_fD = FALSE;
	m_nDiscAfter = 0;
	m_fD2 = FALSE;
	m_nDiscAfter2 = 99;
	m_fDiscWOLift = FALSE;
	m_fDiscWLift = FALSE;
	m_dThreshold = 0.0;
	m_fCollapseStrokes = FALSE;
	m_fCollapseUDStrokes = FALSE;
	m_fCollapseTrials = FALSE;
	m_fMedian = FALSE;

	m_chkA.SetDefaultAsOff( !m_fA );
	m_chkR.SetDefaultAsOff( !m_fR );
	m_chkT.SetDefaultAsOff( !m_fT );
	m_chkZ.SetDefaultAsOff( !m_fZ );
	m_chkS.SetDefaultAsOff( !m_fS );
	m_chkD.SetDefaultAsOff( !m_fD );
	m_editDA.SetDefaultValue( m_nDiscAfter );
	m_chkD2.SetDefaultAsOff( !m_fD2 );
	m_editDA2.SetDefaultValue( m_nDiscAfter2 );
	m_chkWOL.SetDefaultAsOff( !m_fDiscWOLift );
	m_chkWL.SetDefaultAsOff( !m_fDiscWLift );
	m_editThreshold.SetDefaultValue( m_dThreshold );
	m_chkCollUD.SetDefaultAsOff( !m_fCollapseUDStrokes );
	m_chkCollS.SetDefaultAsOff( !m_fCollapseStrokes );
	m_chkCollT.SetDefaultAsOff( !m_fCollapseTrials );
	m_rdoAvg.SetDefaultRadio( IDC_RDO_AVG );
	m_rdoMed.SetDefaultRadio( IDC_RDO_AVG );

	if( m_pPS )
	{
		pPS->get_CollapseUDStrokes( &m_fCollapseUDStrokes );
		pPS->get_CollapseStrokes( &m_fCollapseStrokes );
		pPS->get_CollapseTrials( &m_fCollapseTrials );
		pPS->get_CollapseMedian( &m_fMedian );

		IProcSetFlags* pPSF = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_ST_A( &m_fA );
			pPSF->get_ST_R( &m_fR );
			pPSF->get_ST_Z( &m_fZ );
			pPSF->get_ST_T( &m_fT );
			pPSF->get_ST_S( &m_fS );
			pPSF->get_ST_D( &m_fD );
			short nVal = 0;
			pPSF->get_DiscardAfter( &nVal );
			m_nDiscAfter = nVal;
			pPSF->get_ST_D2( &m_fD2 );
			pPSF->get_DiscardAfter2( &nVal );
			m_nDiscAfter2 = nVal;
			if( ( m_nDiscAfter2 <= 0 ) || ( m_nDiscAfter2 > 99 ) ) m_nDiscAfter2 = 99;
			BOOL fVal = 0;
			pPSF->get_DiscardPenDownDurMin( &fVal );
			pPSF->get_PenDownDurMin( &m_dThreshold );
			if( fVal == 1 ) m_fDiscWOLift = TRUE;
			else if( fVal == 2 ) m_fDiscWLift = TRUE;

			pPSF->Release();
		}
	}
}

ProcSetPageSum::~ProcSetPageSum()
{
}

void ProcSetPageSum::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_SUMA, m_fA);
	DDX_Check(pDX, IDC_CHK_SUMR, m_fR);
	DDX_Check(pDX, IDC_CHK_SUMT, m_fT);
	DDX_Check(pDX, IDC_CHK_SUMZ, m_fZ);
	DDX_Check(pDX, IDC_CHK_SUMS, m_fS);
	DDX_Check(pDX, IDC_CHK_SUMD, m_fD);
	DDX_Text(pDX, IDC_EDIT_DISC, m_nDiscAfter);
	DDX_Check(pDX, IDC_CHK_SUMD2, m_fD2);
	DDX_Text(pDX, IDC_EDIT_DISC2, m_nDiscAfter2);
	DDX_Check(pDX, IDC_CHK_UDSTROKES, m_fCollapseUDStrokes);
	DDX_Check(pDX, IDC_CHK_STROKES, m_fCollapseStrokes);
	DDX_Check(pDX, IDC_CHK_TRIALS, m_fCollapseTrials);
	DDX_Check(pDX, IDC_CHK_DISC_LIFT, m_fDiscWOLift);
	DDX_Check(pDX, IDC_CHK_DISC_LIFT2, m_fDiscWLift);
	DDX_Text(pDX, IDC_EDIT_DISC_LIFT, m_dThreshold);

	DDX_Control(pDX, IDC_CHK_SUMA, m_chkA);
	DDX_Control(pDX, IDC_CHK_SUMR, m_chkR);
	DDX_Control(pDX, IDC_CHK_SUMT, m_chkT);
	DDX_Control(pDX, IDC_CHK_SUMZ, m_chkZ);
	DDX_Control(pDX, IDC_CHK_SUMS, m_chkS);
	DDX_Control(pDX, IDC_CHK_SUMD, m_chkD);
	DDX_Control(pDX, IDC_EDIT_DISC, m_editDA);
	DDX_Control(pDX, IDC_CHK_SUMD2, m_chkD2);
	DDX_Control(pDX, IDC_EDIT_DISC2, m_editDA2);
	DDX_Control(pDX, IDC_CHK_DISC_LIFT, m_chkWOL);
	DDX_Control(pDX, IDC_CHK_DISC_LIFT2, m_chkWL);
	DDX_Control(pDX, IDC_EDIT_DISC_LIFT, m_editThreshold);
	DDX_Control(pDX, IDC_CHK_UDSTROKES, m_chkCollUD);
	DDX_Control(pDX, IDC_CHK_STROKES, m_chkCollS);
	DDX_Control(pDX, IDC_CHK_TRIALS, m_chkCollT);
	DDX_Control(pDX, IDC_RDO_AVG, m_rdoAvg);
	DDX_Control(pDX, IDC_RDO_MED, m_rdoMed);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSum message handlers

BOOL ProcSetPageSum::OnApply() 
{
	if( !m_pPS ) return FALSE;

	if( ( m_nDiscAfter2 < 1 ) || ( m_nDiscAfter2 > 99 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 13 );
		BCGPMessageBox( _T("Trial # after which trials are not included must be between 1 and 99."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DISC2 );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dThreshold < 0. ) || ( m_dThreshold > 1. ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 13 );
		BCGPMessageBox( _T("Threshold Relative Pen Down Duration must be greater than 0 and less than or equal to 1."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DISC_LIFT );
		pWnd->SetFocus();
		return FALSE;
	}

	m_pPS->put_CollapseUDStrokes( m_fCollapseUDStrokes );
	m_pPS->put_CollapseStrokes( m_fCollapseStrokes );
	m_pPS->put_CollapseTrials( m_fCollapseTrials );
	int nUse = GetCheckedRadioButton( IDC_RDO_AVG, IDC_RDO_MED );
	BOOL fMed = ( nUse == IDC_RDO_MED );
	m_pPS->put_CollapseMedian( fMed );

	IProcSetFlags* pPSF = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( SUCCEEDED( hr ) )
	{
		pPSF->put_ST_A( m_fA );
		pPSF->put_ST_R( m_fR );
		pPSF->put_ST_Z( m_fZ );
		pPSF->put_ST_T( m_fT );
		pPSF->put_ST_S( m_fS );
		pPSF->put_ST_D( m_fD );
		pPSF->put_DiscardAfter( (short)m_nDiscAfter );
		pPSF->put_ST_D2( m_fD2 );
		pPSF->put_DiscardAfter2( (short)m_nDiscAfter2 );
		BOOL fVal = 0;
		if( m_fDiscWOLift ) fVal = 1;
		else if( m_fDiscWLift ) fVal = 2;
		pPSF->put_DiscardPenDownDurMin( fVal );
		pPSF->put_PenDownDurMin( m_dThreshold );

		pPSF->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageSum::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageSum::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	BOOL fA = FALSE, fR = FALSE, fT = FALSE, fZ = FALSE, fS = FALSE, fD = FALSE, fD2 = FALSE;
	BOOL fUDStrokes = FALSE, fStrokes = FALSE, fTrials = FALSE, fMedian = FALSE;
	BOOL fDiscWOLift = FALSE, fDiscWLift = FALSE;
	double dThreshold = 0.5;
	int nDiscAfter = 0, nDiscAfter2 = 0;
	if( pPS )
	{
		pPS->get_CollapseUDStrokes( &fUDStrokes );
		pPS->get_CollapseStrokes( &fStrokes );
		pPS->get_CollapseTrials( &fTrials );
		pPS->get_CollapseMedian( &fMedian );

		IProcSetFlags* pPSF = NULL;
		HRESULT hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_ST_A( &fA );
			pPSF->get_ST_R( &fR );
			pPSF->get_ST_Z( &fZ );
			pPSF->get_ST_T( &fT );
			pPSF->get_ST_S( &fS );
			pPSF->get_ST_D( &fD );
			short nVal = 0;
			pPSF->get_DiscardAfter( &nVal );
			nDiscAfter = nVal;
			pPSF->get_ST_D2( &fD2 );
			pPSF->get_DiscardAfter2( &nVal );
			nDiscAfter2 = nVal;
			pPSF->get_PenDownDurMin( &dThreshold );

			pPSF->Release();
		}
		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_CHK_SUMA );
	szData.LoadString( IDS_PSPST_ABS );
	szTmp.Format( _T(" [DEFAULT=%s]"), fA ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Absolute Values") );

	pWnd = GetDlgItem( IDC_CHK_SUMR );
	szData.LoadString( IDS_PSPST_REL );
	szTmp.Format( _T(" [DEFAULT=%s]"), fR ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Relative Data") );

	pWnd = GetDlgItem( IDC_CHK_SUMZ );
	szData.LoadString( IDS_PSPST_STROKES );
	szTmp.Format( _T(" [DEFAULT=%s]"), fZ ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Missing Strokes") );

	pWnd = GetDlgItem( IDC_CHK_SUMT );
	szData.LoadString( IDS_PSPST_TRIALS );
	szTmp.Format( _T(" [DEFAULT=%s]"), fT ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Missing Trials") );

	pWnd = GetDlgItem( IDC_CHK_SUMS );
	szData.LoadString( IDS_PSPST_SUBST );
	szTmp.Format( _T(" [DEFAULT=%s]"), fS ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Substitute") );

	pWnd = GetDlgItem( IDC_CHK_SUMD );
	if( pWnd )
	{
		szData.LoadString( IDS_PSPST_DISC );
		szTmp.Format( _T(" [DEFAULT=%s]"), fD ? _T("TRUE") : _T("FALSE") );
		szData += szTmp;
		m_tooltip.AddTool( pWnd, szData, _T("Discard Substituted") );
		if( !m_fS ) pWnd->EnableWindow( FALSE );
	}

	pWnd = GetDlgItem( IDC_EDIT_DISC );
	if( pWnd )
	{
		szData.LoadString( IDS_PSPST_AFTER );
		szTmp.Format( _T(" [DEFAULT=%d]"), nDiscAfter );
		szData += szTmp;
		m_tooltip.AddTool( pWnd, szData, _T("Trial") );
		if( !m_fD ) pWnd->EnableWindow( FALSE );
	}

	pWnd = GetDlgItem( IDC_CHK_SUMD2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_PSPST_DISC2, _T("Discard") );

	pWnd = GetDlgItem( IDC_EDIT_DISC2 );
	if( pWnd )
	{
		szData.LoadString( IDS_PSPST_AFTER2 );
		szTmp.Format( _T(" [DEFAULT=%d]"), nDiscAfter2 );
		szData += szTmp;
		m_tooltip.AddTool( pWnd, szData, _T("Trial") );
		if( !m_fD2 ) pWnd->EnableWindow( FALSE );
	}
	pWnd = GetDlgItem( IDC_CHK_UDSTROKES );
	if( pWnd )
	{
		szData = _T("Collapse across odd and even strokes. (HINT: Use 'Remove initial downstroke' in consistency settings.)");
		szTmp.Format( _T(" [DEFAULT=%s]"), fUDStrokes ? _T("TRUE") : _T("FALSE") );
		szData += szTmp;
		m_tooltip.AddTool( pWnd, szData, _T("Odd/Even Strokes") );
		if( m_fCollapseStrokes ) pWnd->EnableWindow( FALSE );
	}
	pWnd = GetDlgItem( IDC_CHK_STROKES );
	if( pWnd )
	{
		szData = _T("Collapse across all strokes. (HINT: Use 'Absolute values'.)");
		szTmp.Format( _T(" [DEFAULT=%s]"), fStrokes ? _T("TRUE") : _T("FALSE") );
		szData += szTmp;
		m_tooltip.AddTool( pWnd, szData, _T("Strokes") );
		if( m_fCollapseUDStrokes ) pWnd->EnableWindow( FALSE );
	}
	pWnd = GetDlgItem( IDC_CHK_TRIALS );
	if( pWnd )
	{
		szData = _T("Collapse across trials.");
		szTmp.Format( _T(" [DEFAULT=%s]"), fTrials ? _T("TRUE") : _T("FALSE") );
		szData += szTmp;
		m_tooltip.AddTool( pWnd, szData, _T("Trials") );
	}
	pWnd = GetDlgItem( IDC_RDO_AVG );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Use averages for collapsing data."), _T("Averages") );
		pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
	}
	pWnd = GetDlgItem( IDC_RDO_MED );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Use medians for collapsing data."), _T("Medians") );
		pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
	}

	pWnd = GetDlgItem( IDC_CHK_DISC_LIFT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Exclude all air-strokes."), _T("Without Pen-lifts") );
	pWnd = GetDlgItem( IDC_CHK_DISC_LIFT2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Include only air-strokes."), _T("With Pen-lifts") );
	pWnd = GetDlgItem( IDC_EDIT_DISC_LIFT );
	if( pWnd )
	{
		szData = _T("Threshold Relative Pen Down Duration.\n0.0 for pen-lifted; 1.0 for pen pressed for the entire stroke.");
		szTmp.Format( _T(" [DEFAULT=%.2f]"), dThreshold );
		szData += szTmp;
		szData += _T(" Threshold is counted as penlift.");
		m_tooltip.AddTool( pWnd, szData, _T("Threshold") );
		if( !m_fDiscWOLift && !m_fDiscWLift ) pWnd->EnableWindow( FALSE );
	}

	CheckRadioButton( IDC_RDO_AVG, IDC_RDO_MED, m_fMedian ? IDC_RDO_MED : IDC_RDO_AVG );

// 	pWnd = GetDlgItem( IDC_CHK_SUMT );
// 	pWnd->EnableWindow( m_fZ );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ProcSetPageSum::OnChkSumt() 
{
	UpdateData( TRUE );

	if( m_fT )
	{
		m_fS = FALSE;
		m_fD = FALSE;
		UpdateData( FALSE );

		CWnd* pWnd = GetDlgItem( IDC_CHK_SUMD );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_DISC );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
}

void ProcSetPageSum::OnChkSums() 
{
	UpdateData( TRUE );

	if( m_fS )
	{
		m_fT = FALSE;
		UpdateData( FALSE );

		CWnd* pWnd = GetDlgItem( IDC_CHK_SUMD );
		if( pWnd ) pWnd->EnableWindow( TRUE );
		pWnd = GetDlgItem( IDC_EDIT_DISC );
		if( pWnd ) pWnd->EnableWindow( m_fD );
	}
	else
	{
		m_fD = FALSE;
		UpdateData( FALSE );

		CWnd* pWnd = GetDlgItem( IDC_CHK_SUMD );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_DISC );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
}

void ProcSetPageSum::OnChkSumd() 
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DISC );
	if( pWnd ) pWnd->EnableWindow( m_fD );
}

void ProcSetPageSum::OnChkSumd2() 
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DISC2 );
	if( pWnd ) pWnd->EnableWindow( m_fD2 );
}

BOOL ProcSetPageSum::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageSum::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_summarization.html"), this );
	return TRUE;
}

void ProcSetPageSum::OnBnReset() 
{
	m_fA = FALSE;
	m_fR = FALSE;
	m_fT = FALSE;
	m_fZ = FALSE;
	m_fS = FALSE;
	m_fD = FALSE;
	m_nDiscAfter = 0;
	m_fD2 = FALSE;
	m_nDiscAfter2 = 99;
	m_fDiscWOLift = FALSE;
	m_fDiscWLift = FALSE;
	m_dThreshold = 0.0;
	m_fCollapseStrokes = FALSE;
	m_fCollapseUDStrokes = FALSE;
	m_fCollapseTrials = FALSE;
	m_fMedian = FALSE;

	UpdateData( FALSE );

	CWnd* pWnd = GetDlgItem( IDC_CHK_SUMD );
	pWnd->EnableWindow( m_fS );
	pWnd = GetDlgItem( IDC_EDIT_DISC );
	pWnd->EnableWindow( m_fD );
	pWnd = GetDlgItem( IDC_EDIT_DISC2 );
	pWnd->EnableWindow( m_fD2 );
	pWnd = GetDlgItem( IDC_EDIT_DISC_LIFT );
	pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_RDO_AVG );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
	pWnd = GetDlgItem( IDC_RDO_MED );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );

	CheckRadioButton( IDC_RDO_AVG, IDC_RDO_MED, IDC_RDO_AVG );
}

void ProcSetPageSum::OnChkUDStrokes()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_STROKES );
	if( pWnd ) pWnd->EnableWindow( !m_fCollapseUDStrokes );
	pWnd = GetDlgItem( IDC_RDO_AVG );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
	pWnd = GetDlgItem( IDC_RDO_MED );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
}

void ProcSetPageSum::OnChkStrokes()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_UDSTROKES );
	if( pWnd ) pWnd->EnableWindow( !m_fCollapseStrokes );
	pWnd = GetDlgItem( IDC_RDO_AVG );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
	pWnd = GetDlgItem( IDC_RDO_MED );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );

	if( m_fCollapseStrokes && !m_fA )
		BCGPMessageBox( _T("It is recommended that you also set Absolute Values to ON.") );
}

void ProcSetPageSum::OnChkTrials()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_RDO_AVG );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
	pWnd = GetDlgItem( IDC_RDO_MED );
	if( pWnd ) pWnd->EnableWindow( m_fCollapseUDStrokes || m_fCollapseStrokes || m_fCollapseTrials );
}

void ProcSetPageSum::OnChkDiscWLift()
{
	UpdateData( TRUE );
	if( m_fDiscWLift && m_fDiscWOLift )
	{
		m_fDiscWOLift = FALSE;
		UpdateData( FALSE );
	}
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DISC_LIFT );
	pWnd->EnableWindow( m_fDiscWOLift || m_fDiscWLift );
}

void ProcSetPageSum::OnChkDiscWOLift()
{
	UpdateData( TRUE );
	if( m_fDiscWOLift && m_fDiscWLift )
	{
		m_fDiscWLift = FALSE;
		UpdateData( FALSE );
	}
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DISC_LIFT );
	pWnd->EnableWindow( m_fDiscWOLift || m_fDiscWLift );
}

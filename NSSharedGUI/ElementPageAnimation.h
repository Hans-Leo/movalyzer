#pragma once

#include "NSTooltipCtrl.h"

interface IElement;

// ElementPageAnimation dialog

class AFX_EXT_CLASS ElementPageAnimation : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(ElementPageAnimation)

public:
	ElementPageAnimation( IElement* = NULL );
	virtual ~ElementPageAnimation();

// Dialog Data
	enum { IDD = IDD_PAGE_ELMT_ANIMATION };
	NSToolTipCtrl	m_tooltip;
	IElement*		m_pE;
	CBCGPButton		m_bnBrowse;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void GetIDs( CString& szExpID, CString& szGrpID, CString& szSubjID );

	DECLARE_MESSAGE_MAP()
public:
	BOOL	m_fAnimate;
	double	m_dAnimationRate;
	BOOL	m_fExisting;
	BOOL	m_fGen;
	CString m_szPattern;
	CComboBox m_cboExp;
	CComboBox m_cboGrp;
	CComboBox m_cboSubj;
	CString		szExp, szGrp, szSubj, szTrial;

public:
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnClickedRdoExist();
	afx_msg void OnBnClickedRdoRandom();
	afx_msg void OnBnClickedRdoRandomgen();
	afx_msg void OnBnClickedRdoRandomsubj();
	afx_msg void OnBnClickedBnBrowse();
	afx_msg void OnCbnSelchangeCboExp();
	afx_msg void OnCbnSelchangeCboGrp();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	virtual BOOL OnApply();
	virtual BOOL OnInitDialog();
	virtual BOOL OnSetActive();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedChkAnimate();
};

// Progress.cpp

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include "Progress.h"

namespace Progress {

// progress dialog in cases where we don't application is not going to use the built-in status bar one
class ProgressDlg : public CBCGPDialog
{
public:
	ProgressDlg( CWnd* pParent = NULL );
	enum { IDD = IDD_PROGRESS };
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
public:
	void Kill();
	CProgressCtrl*	m_pProgress;
	CString			m_szStatus;
};

// global dialog var
static ProgressDlg*	_pProgressDlg = NULL;

ProgressDlg::ProgressDlg( CWnd* pParent )
{
// Jerry hMod = AfxLoadLibrary("NSSHaredGUI.dll");
	HINSTANCE hMod; 
	hMod = AfxLoadLibrary("NSSHaredGUI.dll");
	if(NULL == hMod)
	{
	 MessageBox("Object Not Created","Caption",MB_OK);
	 return;
	 }
//

	m_pProgress = NULL;
	if( Create( ProgressDlg::IDD, pParent ) )
		ShowWindow( SW_SHOW );
}

void ProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_STATUS, m_szStatus);
}

BOOL ProgressDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );
	CenterWindow();
	m_pProgress = (CProgressCtrl*)GetDlgItem( IDC_PROGRESS );
	m_pProgress->SetBkColor( RGB( 255, 255, 255 ) );
	return TRUE;
}
void ProgressDlg::OnOK() { CBCGPDialog::OnCancel(); }
void ProgressDlg::OnCancel() { DestroyWindow(); }
void ProgressDlg::Kill() { OnCancel(); }
void ProgressDlg::PostNcDestroy()
{
	if( _pProgressDlg ) delete _pProgressDlg;
	_pProgressDlg = NULL;
}

// global vars
static BOOL						_fUseCB = FALSE;
static long						_lProgress = 0;
static long						_lProgressTotal = 0;

// global callback function pointers
static SHOWPROGRESSCB			_pFnShowProgress;
static SETSTATUSTEXTCB			_pFnSetStatusText;
static ENABLEPROGRESSCB			_pFnEnableProgress;
static SETPROGRESSCB			_pFnSetProgress;
static DISABLEPROGRESSCB		_pFnDisableProgress;

void UseCallbacks( BOOL fUse ) { _fUseCB = fUse; }
void SetShowProgressCB( SHOWPROGRESSCB pFn ) { _pFnShowProgress = pFn; }
void SetSetStatusTextCB( SETSTATUSTEXTCB pFn ) { _pFnSetStatusText = pFn; }
void SetEnableProgressCB( ENABLEPROGRESSCB pFn ) { _pFnEnableProgress = pFn; }
void SetSetProgressCB( SETPROGRESSCB pFn ) { _pFnSetProgress = pFn; }
void SetDisableProgressCB( DISABLEPROGRESSCB pFn ) { _pFnDisableProgress = pFn; }

//************************************
// Method:    SetStatusText
// FullName:  Progress::SetStatusText
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szTxt
//************************************
void SetStatusText( CString szTxt )
{
	if( !_fUseCB && _pProgressDlg )
	{
		_pProgressDlg->m_szStatus = szTxt;
		_pProgressDlg->UpdateData( FALSE );
	}
	else if( _fUseCB && _pFnSetStatusText ) _pFnSetStatusText( szTxt );
}

//************************************
// Method:    EnableProgress
// FullName:  Progress::EnableProgress
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: long nTotal
//************************************
void EnableProgress( long nTotal )
{
	_lProgress = 0;
	_lProgressTotal = (long)nTotal;

	if( !_fUseCB ) ShowProgress( TRUE );
	else if( _pFnEnableProgress ) _pFnEnableProgress( nTotal );
}

//************************************
// Method:    SetProgress
// FullName:  Progress::SetProgress
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: long nCur
// Parameter: CString szTxt
//************************************
void SetProgress( long nCur, CString szTxt )
{
	if( (long)nCur > _lProgressTotal ) return;
	_lProgress = (long)nCur;

	if( !_fUseCB && _pProgressDlg )
	{
		_pProgressDlg->m_pProgress->SetPos( _lProgress );
		SetStatusText( szTxt );
	}
	else if( _fUseCB && _pFnSetProgress ) _pFnSetProgress( nCur, szTxt );
}

//************************************
// Method:    DisableProgress
// FullName:  Progress::DisableProgress
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void DisableProgress()
{
	if( !_fUseCB ) ShowProgress( FALSE );
	else if( _pFnDisableProgress ) _pFnDisableProgress();
}

//************************************
// Method:    GetProgress
// FullName:  Progress::GetProgress
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: long & nCur
// Parameter: long & nTotal
//************************************
void GetProgress( long& nCur, long& nTotal )
{
	nCur = _lProgress;
	nTotal = _lProgressTotal;
}

//************************************
// Method:    ShowProgress
// FullName:  Progress::ShowProgress
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fShow
//************************************
void ShowProgress( BOOL fShow /*= TRUE */ )
{
	if( !_fUseCB )
	{
		if( fShow )
		{
			_pProgressDlg = new ProgressDlg( AfxGetMainWnd() );
			_pProgressDlg->m_pProgress->SetRange32( 0, _lProgressTotal );
		}
		else
		{
			if( _pProgressDlg )
			{
				_pProgressDlg->Kill();
				delete _pProgressDlg;
				_pProgressDlg = NULL;
			}
		}
	}
	else if( _pFnShowProgress ) _pFnShowProgress( fShow );
}

};	// namespace

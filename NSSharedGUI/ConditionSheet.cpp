// ConditionSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ConditionPageMain.h"
#include "ConditionPageWord.h"
#include "ConditionPageFeedback.h"
#include "ConditionPageVFeedback.h"
#include "ProcSetPageSounds.h"
#include "..\DataMod\DataMod.h"
#include "ConditionSheet.h"
#include "Conditions.h"
#include "ConditionPageStimuli.h"
#include "ConditionPageConsis.h"
#include "ConditionPageGripper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ConditionSheet

IMPLEMENT_DYNAMIC(ConditionSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(ConditionSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ConditionSheet::ConditionSheet(UINT nIDCaption, CWnd* pParentWnd, BOOL fGripper, UINT iSelectPage)
	: CBCGPPropertySheet(nIDCaption, pParentWnd, iSelectPage), m_fNew( TRUE ),
	  m_fReps( FALSE ), m_fDup( FALSE ), m_fGripper( fGripper ), m_pList( NULL ), m_pListWord( NULL )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	AddPages();
}

ConditionSheet::ConditionSheet(LPCTSTR pszCaption, CWnd* pParentWnd, BOOL fGripper, UINT iSelectPage)
	: CBCGPPropertySheet(pszCaption, pParentWnd, iSelectPage), m_fNew( TRUE ),
	  m_fReps( FALSE ), m_fDup( FALSE ), m_fGripper( fGripper ), m_pListWord( NULL ), m_pList( NULL )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	AddPages();
}

ConditionSheet::ConditionSheet( INSCondition* pC, CString szExpID, BOOL fNew, BOOL fReps, BOOL fDup,
							    CStringList* pList, CStringList* pListWord, CWnd* pParentWnd,
								BOOL fGripper, UINT iSelectPage, int nEvent )
	: CBCGPPropertySheet( _T("Condition"), pParentWnd, iSelectPage ), m_fNew( fNew ),
	  m_fReps( fReps ), m_pList( pList ), m_fDup( fDup ), m_fGripper( fGripper ),
	  m_pListWord( pListWord ), m_nSelEvent( nEvent )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_szExpID = szExpID;
	AddPages( pC );
}

ConditionSheet::~ConditionSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void ConditionSheet::AddPages( INSCondition* pC )
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	// add main page
	CBCGPPropSheetCategory* pCat1 = AddTreeCategory( _T("Condition"), 0, 1 );
	AddPageToTree( pCat1, new ConditionPageMain( pC, m_szExpID, m_fNew, m_fReps, m_fDup ), -1, 2 );

	// Dont show other pages if repetition mod or wizard
	if( !m_fReps )
	{
		AddPageToTree( pCat1, new ConditionPageStimuli( pC, m_fNew ), -1, 2 );
		AddPageToTree( pCat1, new ConditionPageConsis( pC, m_fNew ), -1, 2 );
		AddPageToTree( pCat1, new ConditionPageWord( pC, m_fNew, m_pList, m_pListWord ), -1, 2 );
		AddPageToTree( pCat1, new ConditionPageFeedback( pC, m_fNew ), -1, 2 );
		AddPageToTree( pCat1, new ConditionPageVFeedback( pC, m_fNew ), -1, 2 );
		AddPageToTree( pCat1, new ProcSetPageSounds( pC, m_nSelEvent ), -1, 2 );
		if( m_fGripper ) AddPageToTree( pCat1, new ConditionPageGripper( pC, m_fNew ), -1, 2 );
	}

	m_fModified = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// ConditionSheet message handlers

BOOL ConditionSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( ConditionPageMain ) ) )
			return ((ConditionPageMain*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ConditionPageWord ) ) )
			return ((ConditionPageWord*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ConditionPageFeedback ) ) )
			return ((ConditionPageFeedback*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ConditionPageVFeedback ) ) )
			return ((ConditionPageVFeedback*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageSounds ) ) )
			return ((ProcSetPageSounds*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ConditionPageStimuli ) ) )
			return ((ConditionPageStimuli*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ConditionPageConsis ) ) )
			return ((ConditionPageConsis*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

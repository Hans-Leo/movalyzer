#pragma once

#include "..\NSSharedGUI\NSTooltipCtrl.h"

class WindowManager;

// DataTabGridDlg dialog

class AFX_EXT_CLASS DataTabGridDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(DataTabGridDlg)

public:
	DataTabGridDlg( CString szFile, CWnd* pParent = NULL, WindowManager* pWM = NULL, int nCurChart = 0 );   // standard constructor
	virtual ~DataTabGridDlg();

// Dialog Data
	enum { IDD = IDD_DATATAB_GRID };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	virtual void PostNcDestroy();

	DECLARE_MESSAGE_MAP()
public:
	CStdioFile		m_file;
	BOOL			m_fInit;
	CString			m_szFile;
	NSToolTipCtrl	m_tooltip;
	CStatic			m_wndGridLocation;
	CBCGPGridCtrl	m_wndGrid;
	int				m_nRows;
	int				m_nTop;
	int				m_nLeft;
	int				m_nRightBuffer;
	int				m_nBottomBuffer;
	WindowManager*	m_pWM;
	int				m_nCurChart;
	// former global vars (to handle multiple windows open)
	int				_nStartRow;
	int				_nLastRow ;
	CStdioFile*		_pFile;
	CBCGPGridCtrl*	_pGrid;
	int				_nRows;
	CStringList*	_pDataList;
	int				_nListSize;

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage( MSG* );
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedBnEdit();
	void FillList( CString szFile );
	void FillColumns( CString szFile );
	afx_msg void OnBnClickedBnRefresh();
	afx_msg void OnClickExcel();
	afx_msg void OnClickSort();
};

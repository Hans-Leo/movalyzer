// UserMessages.h

#pragma once

#define	UM_INSTRUCTION_UPDATE		(WM_USER + 5001)
#define	UM_SUBJECT_OUTOFRANGE		(WM_USER + 5002)
#define	UM_DEVICE_SETUP				(WM_USER + 5003)
#define	UM_DEVICE_SETUP_WIZ			(WM_USER + 5004)
#define	UM_REFRESH					(WM_USER + 5005)

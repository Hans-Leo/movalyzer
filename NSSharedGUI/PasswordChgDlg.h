#pragma once

// PasswordChgDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// PasswordChgDlg dialog

class AFX_EXT_CLASS PasswordChgDlg : public CBCGPDialog
{
// Construction
public:
	PasswordChgDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_PASSWORD_CHG };
	CString			m_szPassOld;
	CString			m_szPassNew;
	CString			m_szPassNew2;
	CString			m_szPass;
	BOOL			m_fForTempPass;
	CBCGPURLLinkButton	m_txtHelp;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

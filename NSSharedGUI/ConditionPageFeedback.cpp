// ConditionPageFeedback.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ConditionPageFeedback.h"
#include "Conditions.h"
#include "ConditionSheet.h"
#include "Feedbacks.h"
#include "RelationshipDlg.h"

// ConditionPageFeedback dialog

IMPLEMENT_DYNCREATE(ConditionPageFeedback, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageFeedback, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_VISIBILITY, OnBnClickedChkVisibility)
	ON_BN_CLICKED(IDC_CHK_HIDESHOWAFTER, OnBnClickedChkHideshowafter)
	ON_BN_CLICKED(IDC_CHK_TRANSFORM, OnBnClickedChkTransform)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_WM_HELPINFO()
	ON_CBN_SELCHANGE(IDC_CBO_FEEDBACK, &ConditionPageFeedback::OnCbnSelchangeCboFeedback)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ConditionPageFeedback::ConditionPageFeedback( INSCondition* pC, BOOL fNew )
	: CBCGPPropertyPage(ConditionPageFeedback::IDD), m_pC( pC )
{
	m_CountStrokes = FALSE;
	m_HideFeedback = FALSE;
	m_HideShowAfter = FALSE;
	m_HideShowCount = 0.;
	m_Transform = FALSE;
	m_Xgain = 1.;
	m_Ygain = 1.;
	m_Xrotation = 0.;
	m_Yrotation = 0.;
	m_HideShowAfterStrokes = FALSE;
	m_HideShowAfterShow = FALSE;

	m_chkCountStrokes.SetDefaultAsOff( !m_CountStrokes );
	m_chkHideFeedback.SetDefaultAsOff( !m_HideFeedback );
	m_chkHideShowAfter.SetDefaultAsOff( !m_HideShowAfter );
	m_rdoShow.SetDefaultRadio( m_HideShowAfterShow ? IDC_RDO_SHOW : IDC_RDO_HIDE );
	m_rdoHide.SetDefaultRadio( m_HideShowAfterShow ? IDC_RDO_SHOW : IDC_RDO_HIDE );
	m_rdoSeconds.SetDefaultRadio( m_HideShowAfterStrokes ? IDC_RDO_STROKES : IDC_RDO_SECONDS );
	m_rdoStrokes.SetDefaultRadio( m_HideShowAfterStrokes ? IDC_RDO_STROKES : IDC_RDO_SECONDS );
	m_editHideShowCount.SetDefaultValue( m_HideShowCount );
	m_chkTransform.SetDefaultAsOff( !m_Transform );
	m_editXgain.SetDefaultValue( m_Xgain );
	m_editYgain.SetDefaultValue( m_Ygain );
	m_editXrot.SetDefaultValue( m_Xrotation );
	m_editYrot.SetDefaultValue( m_Yrotation );

	if( !fNew && pC )
	{
		NSConditions::GetCountStrokes( pC, m_CountStrokes );
		NSConditions::GetHideFeedback( pC, m_HideFeedback );
		NSConditions::GetHideShowAfter( pC, m_HideShowAfter );
		NSConditions::GetHideShowAfterShow( pC, m_HideShowAfterShow );
		NSConditions::GetHideShowAfterStrokes( pC, m_HideShowAfterStrokes );
		NSConditions::GetHideShowCount( pC, m_HideShowCount );
		NSConditions::GetTransform( pC, m_Transform );
		NSConditions::GetXgain( pC, m_Xgain );
		NSConditions::GetYgain( pC, m_Ygain );
		NSConditions::GetXrotation( pC, m_Xrotation );
		NSConditions::GetYrotation( pC, m_Yrotation );
		NSConditions::GetFeedback( pC, m_szFB );
	}
}

ConditionPageFeedback::~ConditionPageFeedback()
{
}

void ConditionPageFeedback::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_COUNTSTROKES, m_CountStrokes);
	DDX_Check(pDX, IDC_CHK_VISIBILITY, m_HideFeedback);
	DDX_Check(pDX, IDC_CHK_HIDESHOWAFTER, m_HideShowAfter);
	DDX_Text(pDX, IDC_EDIT_HIDESHOWCOUNT, m_HideShowCount);
	DDX_Check(pDX, IDC_CHK_TRANSFORM, m_Transform);
	DDX_Text(pDX, IDC_EDIT_XX, m_Xgain);
	DDX_Text(pDX, IDC_EDIT_XY, m_Xrotation);
	DDX_Text(pDX, IDC_EDIT_YX, m_Ygain);
	DDX_Text(pDX, IDC_EDIT_YY, m_Yrotation);
	DDX_Control(pDX, IDC_CBO_FEEDBACK, m_cboFB);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_REL, m_bnRel);

	DDX_Control(pDX, IDC_CHK_COUNTSTROKES, m_chkCountStrokes);
	DDX_Control(pDX, IDC_CHK_VISIBILITY, m_chkHideFeedback);
	DDX_Control(pDX, IDC_CHK_HIDESHOWAFTER, m_chkHideShowAfter);
	DDX_Control(pDX, IDC_RDO_SHOW, m_rdoShow);
	DDX_Control(pDX, IDC_RDO_HIDE, m_rdoHide);
	DDX_Control(pDX, IDC_RDO_SECONDS, m_rdoSeconds);
	DDX_Control(pDX, IDC_RDO_STROKES, m_rdoStrokes);
	DDX_Control(pDX, IDC_EDIT_HIDESHOWCOUNT, m_editHideShowCount);
	DDX_Control(pDX, IDC_CHK_TRANSFORM, m_chkTransform);
	DDX_Control(pDX, IDC_EDIT_XX, m_editXgain);
	DDX_Control(pDX, IDC_EDIT_YX, m_editYgain);
	DDX_Control(pDX, IDC_EDIT_XY, m_editXrot);
	DDX_Control(pDX, IDC_EDIT_YY, m_editYrot);
}

// ConditionPageFeedback message handlers

BOOL ConditionPageFeedback::OnApply() 
{
	if( !m_pC ) return FALSE;

	UpdateData( TRUE );

	m_HideShowAfterShow = 
		( GetCheckedRadioButton( IDC_RDO_SHOW, IDC_RDO_HIDE ) == IDC_RDO_SHOW );
	m_HideShowAfterStrokes =
		( GetCheckedRadioButton( IDC_RDO_SECONDS, IDC_RDO_STROKES ) == IDC_RDO_STROKES );

	// extract feedback id
	if( m_cboFB.GetCurSel() > 0 )
	{
		CString szDelim, szItem;
		::GetDelimiter( szDelim );
		TRIM( szDelim );
		m_cboFB.GetLBText( m_cboFB.GetCurSel(), szItem );
		int nPos = szItem.Find( szDelim );
		if( nPos != -1 ) m_szFB = szItem.Mid( nPos + 2 );
		else m_szFB = _T("");
	}
	else m_szFB = _T("");

	m_pC->put_CountStrokes( m_CountStrokes );
	m_pC->put_HideFeedback( m_HideFeedback );
	m_pC->put_HideShowAfter( m_HideShowAfter );
	m_pC->put_HideShowAfterShow( m_HideShowAfterShow );
	m_pC->put_HideShowAfterStrokes( m_HideShowAfterStrokes );
	m_pC->put_HideShowCount( m_HideShowCount );
	m_pC->put_Transform( m_Transform );
	m_pC->put_Xgain( m_Xgain );
	m_pC->put_Ygain( m_Ygain );
	m_pC->put_Xrotation( m_Xrotation );
	m_pC->put_Yrotation( m_Yrotation );
	m_pC->put_Feedback( m_szFB.AllocSysString() );

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageFeedback::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageFeedback::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle();

	// button images
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnEdit.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnRel.SetImage( IDB_REL, IDB_REL, IDB_REL );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_CHK_COUNTSTROKES );
	m_tooltip.AddTool( pWnd, _T("End the recording when maximum # of strokes has been reached." ), _T("Max Strokes") );
	pWnd = GetDlgItem( IDC_CHK_VISIBILITY );
	m_tooltip.AddTool( pWnd, _T("Hide the pen/mouse drawings until the end of the recording.\n\"Delay next trial until ENTER key is pressed\" is required in the experiment properties -> Running Experiment -> Procedure" ), _T("Delayed Feedback") );
	pWnd = GetDlgItem( IDC_CHK_HIDESHOWAFTER );
	m_tooltip.AddTool( pWnd, _T("Hide or show the recording after n seconds or strokes." ), _T("Change Visibility") );
	pWnd->EnableWindow( m_HideFeedback );
	pWnd = GetDlgItem( IDC_RDO_SHOW );
	m_tooltip.AddTool( pWnd, _T("Show the recording after n seconds or strokes." ), _T("Show") );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_HIDE );
	m_tooltip.AddTool( pWnd, _T("Hide the recording after n seconds or strokes." ), _T("Hide") );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_EDIT_HIDESHOWCOUNT );
	m_tooltip.AddTool( pWnd, _T("Enter the number of seconds or strokes." ), _T("Count") );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_SECONDS );
	m_tooltip.AddTool( pWnd, _T("Hide or show the recording after n seconds." ), _T("Seconds") );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_STROKES );
	m_tooltip.AddTool( pWnd, _T("Hide or show the recording after n strokes." ), _T("Strokes") );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_CHK_TRANSFORM );
	m_tooltip.AddTool( pWnd, _T("Apply gain and/or rotation to the recording.\nRotation is around the first recorded point. Transformation occurs for the entire duration of a trial." ), _T("Transform") );
	pWnd = GetDlgItem( IDC_EDIT_XX );
	m_tooltip.AddTool( pWnd, _T("Specify the transformation gain of the X axis." ), _T("X-Gain") );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_XY );
	m_tooltip.AddTool( pWnd, _T("Specify the transformation rotation around the X axis." ), _T("X-Rotation") );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_YX );
	m_tooltip.AddTool( pWnd, _T("Specify the transformation gain of the Y axis." ), _T("Y-Gain") );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_YY );
	m_tooltip.AddTool( pWnd, _T("Specify the transformation rotation around the Y axis." ), _T("Y-Rotation") );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator.") );
	pWnd = GetDlgItem( IDC_CBO_FEEDBACK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a feedback method for process charts (if any)."), _T("Feedback") );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify the feedback."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new feedback."), _T("Create New") );
	pWnd = GetDlgItem( IDC_BN_REL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the feedback's relationships."), _T("Relationships") );

	CheckRadioButton( IDC_RDO_SHOW, IDC_RDO_HIDE,
					  m_HideShowAfterShow ? IDC_RDO_SHOW : IDC_RDO_HIDE );
	CheckRadioButton( IDC_RDO_SECONDS, IDC_RDO_STROKES,
					  m_HideShowAfterStrokes ? IDC_RDO_STROKES : IDC_RDO_SECONDS );

	// fill the feedback combo
	FillFeedbacks();
	// now search for feedback to select in list
	if( m_szFB != _T("") )
	{
		Feedbacks fb;
		HRESULT hr = fb.Find( m_szFB );
		if( SUCCEEDED( hr ) )
		{
			CString szItem;
			fb.GetDescriptionWithID( szItem );
			int nSel = m_cboFB.FindStringExact( -1, szItem );
			if( nSel >= 0 ) m_cboFB.SetCurSel( nSel );
			else m_cboFB.SetCurSel( 0 );	// <NONE>
		}
	}
	else m_cboFB.SetCurSel( 0 );	// <NONE>

	// update buttons based on feedback selection
	pWnd = GetDlgItem( IDC_BN_EDIT );
	pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
	pWnd = GetDlgItem( IDC_BN_REL );
	pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );

	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ConditionPageFeedback::FillFeedbacks()
{
	m_cboFB.ResetContent();
	m_cboFB.AddString( _T("<NONE>") );
	Feedbacks fb;
	CString szItem;
	HRESULT hr = fb.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		fb.GetDescriptionWithID( szItem );
		m_cboFB.AddString( szItem );
		hr = fb.GetNext();
	}
}

BOOL ConditionPageFeedback::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageFeedback::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_feedback.html"), this );
	return TRUE;
}

void ConditionPageFeedback::OnBnClickedChkVisibility()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_CHK_HIDESHOWAFTER );
	pWnd->EnableWindow( m_HideFeedback );
	pWnd = GetDlgItem( IDC_RDO_SHOW );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_HIDE );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_EDIT_HIDESHOWCOUNT );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_SECONDS );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_STROKES );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
}

void ConditionPageFeedback::OnBnClickedChkHideshowafter()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_RDO_SHOW );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_HIDE );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_EDIT_HIDESHOWCOUNT );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_SECONDS );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_STROKES );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
}

void ConditionPageFeedback::OnBnClickedChkTransform()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_XX );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_XY );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_YX );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_YY );
	pWnd->EnableWindow( m_Transform );
}

void ConditionPageFeedback::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void ConditionPageFeedback::OnClickBnAdd() 
{
	Feedbacks fb;
	if( !fb.IsValid() ) return;

	// do add
	if( fb.DoAdd( this ) )
	{
		// get info
		CString szItem;
		fb.GetDescriptionWithID( szItem );

		// add item, get index, and select
		int nItem = m_cboFB.AddString( szItem );
		if( nItem >= 0 ) m_cboFB.SetCurSel( nItem );

		// update buttons based on feedback selection
		CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
		pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
		pWnd = GetDlgItem( IDC_BN_REL );
		pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
	}
}

void ConditionPageFeedback::OnClickBnEdit() 
{
	Feedbacks fb;
	if( !fb.IsValid() ) return;

	int nIdx = m_cboFB.GetCurSel();
	if( nIdx < 0 ) return;

	CString szID, szDelim;
	m_cboFB.GetLBText( nIdx, szID );
	if( szID == _T("<NONE>") ) return;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos = szID.Find( szDelim );
	szID = szID.Mid( nPos + 2 );

	if( fb.DoEdit( szID, this ) )
	{
		FillFeedbacks();
		m_cboFB.SetCurSel( nIdx );
	}
}

void ConditionPageFeedback::OnClickBnRel()
{
	Feedbacks fb;
	if( !fb.IsValid() ) return;

	int nIdx = m_cboFB.GetCurSel();
	if( nIdx < 0 ) return;

	CString szID, szDelim;
	m_cboFB.GetLBText( nIdx, szID );
	if( szID == _T("<NONE>") ) return;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nPos = szID.Find( szDelim );
	szID = szID.Mid( nPos + 2 );

	if( SUCCEEDED( fb.Find( szID ) ) )
	{
		CString szItem;
		fb.GetDescriptionWithID( szItem );

		RelationshipDlg d( this, REL_FEEDBACK, szItem );
		d.DoModal();
	}
	else BCGPMessageBox( _T("Could not find feedback.") );
}

void ConditionPageFeedback::OnCbnSelchangeCboFeedback()
{
	// update buttons based on feedback selection
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
	pWnd = GetDlgItem( IDC_BN_REL );
	pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
}

void ConditionPageFeedback::OnBnReset() 
{
	m_CountStrokes = FALSE;
	m_HideFeedback = FALSE;
	m_HideShowAfter = FALSE;
	m_HideShowCount = 0.;
	m_Transform = FALSE;
	m_Xgain = 1.;
	m_Ygain = 1.;
	m_Xrotation = 0.;
	m_Yrotation = 0.;
	m_HideShowAfterStrokes = FALSE;
	m_HideShowAfterShow = FALSE;

	UpdateData( FALSE );

	CheckRadioButton( IDC_RDO_SHOW, IDC_RDO_HIDE,
					  m_HideShowAfterShow ? IDC_RDO_SHOW : IDC_RDO_HIDE );
	CheckRadioButton( IDC_RDO_SECONDS, IDC_RDO_STROKES,
					  m_HideShowAfterStrokes ? IDC_RDO_STROKES : IDC_RDO_SECONDS );
	m_cboFB.SetCurSel( 0 );
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
	pWnd = GetDlgItem( IDC_BN_REL );
	pWnd->EnableWindow( m_cboFB.GetCurSel() != 0 );
	pWnd = GetDlgItem( IDC_CHK_HIDESHOWAFTER );
	pWnd->EnableWindow( m_HideFeedback );
	pWnd = GetDlgItem( IDC_RDO_SHOW );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_HIDE );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_EDIT_HIDESHOWCOUNT );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_SECONDS );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_RDO_STROKES );
	pWnd->EnableWindow( m_HideFeedback && m_HideShowAfter );
	pWnd = GetDlgItem( IDC_EDIT_XX );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_XY );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_YX );
	pWnd->EnableWindow( m_Transform );
	pWnd = GetDlgItem( IDC_EDIT_YY );
	pWnd->EnableWindow( m_Transform );
}

// CustomControls.cpp - Implementation File
///////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include "CustomControls.h"

//#define NON_DEF_COLOR		RGB( 255, 165, 0 )
//#define NON_DEF_COLOR		RGB( 249, 109, 40 )
#define NON_DEF_COLOR		RGB( 249, 109, 0 )
//#define NON_DEF_COLOR		RGB( 255, 111, 26 )
#define	NON_DEF_COLOR_BG	RGB( 255, 254, 220 )

///////////////////////////////////////////////////////////////////////////////
// CPrivateEdit
///////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CPrivateEdit, CEdit)
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

CPrivateEdit::CPrivateEdit()
{
	m_crText = RGB( 0, 0, 0 );
	m_crBackGnd = PVT_COLOR;
	m_brBackGnd.CreateSolidBrush( m_crBackGnd );
}

CPrivateEdit::~CPrivateEdit()
{
	if( m_brBackGnd.GetSafeHandle() ) m_brBackGnd.DeleteObject();
}

HBRUSH CPrivateEdit::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	pDC->SetTextColor( m_crText );
	pDC->SetBkColor( m_crBackGnd );
	return m_brBackGnd;
}

///////////////////////////////////////////////////////////////////////////////
// CColoredButton
///////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CColoredButton, CButton)
	ON_MESSAGE(BM_SETCHECK, OnSetCheck)
	ON_MESSAGE(BM_GETCHECK, OnGetCheck)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

CColoredButton::CColoredButton() : CBCGPButton()
{
	m_fDefOff = TRUE;
	m_crBackGndDef = (COLORREF)-1;
	m_crBackGndNonDef = NON_DEF_COLOR;
}

CColoredButton::~CColoredButton()
{
}

void CColoredButton::SetColors( COLORREF crBackGndDef, COLORREF crBackGndNonDef )
{
	m_crBackGndDef = crBackGndDef;
	m_crBackGndNonDef = crBackGndNonDef;
}

void CColoredButton::SetDefaultAsOff( BOOL fVal )
{
	m_fDefOff = fVal;
}

void CColoredButton::SetDefaultRadio( int nVal )
{
	m_nDefRdo = nVal;
}

LRESULT CColoredButton::OnSetCheck( WPARAM fCheck, LPARAM lp )
{
	return CBCGPButton::OnSetCheck( fCheck, lp );
}

LRESULT CColoredButton::OnGetCheck( WPARAM wp, LPARAM lp )
{
	return CBCGPButton::OnGetCheck( wp, lp );
}

void CColoredButton::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CBCGPButton::OnLButtonDown( nFlags, point );
}

void CColoredButton::OnLButtonUp( UINT nFlags, CPoint point ) 
{
	CBCGPButton::OnLButtonUp( nFlags, point );
}

void CColoredButton::DoDrawItem (CDC* pDCPaint, CRect rectClient, UINT itemState)
{
	CBCGPButton::DoDrawItem( pDCPaint, rectClient, itemState );

	if( !m_bCheckButton && !m_bRadioButton ) return;
	if( !IsWindowEnabled() ) return;

	COLORREF cr = (COLORREF)-1;
	if( m_bCheckButton )
	{
		if( ( GetCheck() == BST_CHECKED ) && m_fDefOff ) cr = m_crBackGndNonDef;
		else if( ( GetCheck() == BST_UNCHECKED ) && !m_fDefOff ) cr = m_crBackGndNonDef;
	}
	else if( GetCheck() && ( GetDlgCtrlID() != m_nDefRdo ) ) cr = m_crBackGndNonDef;
	if( cr == (COLORREF)-1 ) return;

	CRect rectCheck = rectClient;
	const CSize sizeCheck = CBCGPVisualManager::GetInstance()->GetCheckRadioDefaultSize();

	if( m_bIsLeftText ) rectCheck.left = rectCheck.right - sizeCheck.cx;
	else rectCheck.right = rectCheck.left + sizeCheck.cx;
	BOOL bMultiLine = (GetStyle () & BS_MULTILINE) != 0;
	if( bMultiLine )
	{
		if ((GetStyle () & BS_VCENTER) == BS_VCENTER)
			rectCheck.top = rectCheck.CenterPoint ().y - sizeCheck.cy / 2;
		else if ((GetStyle () & BS_BOTTOM) == BS_BOTTOM)
			rectCheck.top = rectCheck.bottom - sizeCheck.cy;
		else if ((GetStyle () & BS_TOP) == BS_TOP) rectCheck.top = rectClient.top;
		else
			rectCheck.top = rectCheck.CenterPoint ().y - sizeCheck.cy / 2;
	}
	else rectCheck.top = rectCheck.CenterPoint ().y - sizeCheck.cy / 2;
	rectCheck.bottom = rectCheck.top + sizeCheck.cy;

	CDC* pDC = GetDC();
	HPEN hPen = ::CreatePen( PS_SOLID, 1, cr );
	HPEN hPenOld = (HPEN)pDC->SelectObject( hPen );
	if( m_bCheckButton )
	{
		rectCheck.DeflateRect( 1, 1, 1, 1 );
		pDC->MoveTo( rectCheck.left, rectCheck.top );
		pDC->LineTo( rectCheck.left, rectCheck.bottom );
		pDC->LineTo( rectCheck.right, rectCheck.bottom );
		pDC->LineTo( rectCheck.right, rectCheck.top );
		pDC->LineTo( rectCheck.left, rectCheck.top );
	}
	else
	{
		HBRUSH hbrOld = (HBRUSH)pDC->SelectStockObject( HOLLOW_BRUSH );
		pDC->Ellipse( rectCheck );
		pDC->SelectObject( hbrOld );
	}
	pDC->SelectObject( hPenOld );
	::DeleteObject( hPen );
	ReleaseDC( pDC );
}

///////////////////////////////////////////////////////////////////////////////
// CColoredEdit
///////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CColoredEdit, CEdit)
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

CColoredEdit::CColoredEdit()
{
	m_crText = ::GetSysColor( COLOR_WINDOWTEXT );
	m_crBackGnd = ::GetSysColor( COLOR_WINDOW );
	m_brBackGnd.CreateSolidBrush( m_crBackGnd );
}

CColoredEdit::~CColoredEdit()
{
	if( m_brBackGnd.GetSafeHandle() ) m_brBackGnd.DeleteObject();
}

void CColoredEdit::SetDefaultValue( CString szVal )
{
	vt.vt = VT_BSTR;
	m_szDefVal = szVal;
}

void CColoredEdit::SetDefaultValue( int nVal )
{
	vt.vt = VT_I2;
	vt.iVal = nVal;
}

void CColoredEdit::SetDefaultValue( short nVal )
{
	vt.vt = VT_I2;
	vt.iVal = nVal;
}

void CColoredEdit::SetDefaultValue( long lVal )
{
	vt.vt = VT_I4;
	vt.lVal = lVal;
}

void CColoredEdit::SetDefaultValue( double dVal )
{
	vt.vt = VT_R8;
	vt.dblVal = dVal;
}

void CColoredEdit::SetColors( COLORREF clrText, COLORREF clrBg )
{
	m_crText = clrText;
	m_crBackGnd = clrBg;
}

HBRUSH CColoredEdit::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	if( ::GetFocus() == this->m_hWnd ) return m_brBackGnd;

	COLORREF cr = m_crText, crbg = m_crBackGnd;
	CString szVal;
	GetWindowText( szVal );

	if( IsWindowEnabled() )
	{
		switch( vt.vt )
		{
			case VT_I2:
			{
				int nVal = atoi( szVal );
				if( nVal != vt.iVal ) { cr = NON_DEF_COLOR; crbg = NON_DEF_COLOR_BG; }
				break;
			}
			case VT_I4:
			{
				long lVal = atol( szVal );
				if( lVal != vt.lVal ) { cr = NON_DEF_COLOR; crbg = NON_DEF_COLOR_BG; }
				break;
			}
			case VT_R8:
			{
				double dVal = atof( szVal );
				if( dVal != vt.dblVal ) { cr = NON_DEF_COLOR; crbg = NON_DEF_COLOR_BG; }
				break;
			}
			case VT_BSTR:
			{
				if( szVal != m_szDefVal ) { cr = NON_DEF_COLOR; crbg = NON_DEF_COLOR_BG; }
				break;
			}
		}
	}

	m_brBackGnd.DeleteObject();
	m_brBackGnd.CreateSolidBrush( crbg );

	pDC->SetTextColor( cr );
	pDC->SetBkColor( crbg );
	return m_brBackGnd;
}

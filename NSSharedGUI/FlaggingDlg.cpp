// FlaggingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "FlaggingDlg.h"
#include "AGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FlaggingDlg dialog

BEGIN_MESSAGE_MAP(FlaggingDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_CHK_FLAG, OnChkFlag)
	ON_CBN_SELCHANGE(IDC_CBO_Y, OnSelchangeCboY)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_COLOR, OnClickBnColor)
END_MESSAGE_MAP()

FlaggingDlg::FlaggingDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(FlaggingDlg::IDD, pParent)
{
	m_nY = -1;
	m_dMin = 0.0;
	m_dMax = 0.0;
	m_nYPoint = 3;
	m_nYSize = 3;
	m_fFlag = FALSE;
	m_nYColor = RGB( 63,63,191 );
}

void FlaggingDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_CBO_Y, m_nY );
	DDX_Text(pDX, IDC_EDIT_MIN, m_dMin);
	DDX_Text(pDX, IDC_EDIT_MAX, m_dMax);
	DDX_CBIndex(pDX, IDC_CBO_POINT2, m_nYPoint );
	DDX_CBIndex(pDX, IDC_CBO_SIZE2, m_nYSize );
	DDX_Control(pDX, IDC_BN_COLOR, m_bnColor);
	DDX_Check(pDX, IDC_CHK_FLAG, m_fFlag);
}

/////////////////////////////////////////////////////////////////////////////
// FlaggingDlg message handlers


BOOL FlaggingDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL FlaggingDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	CWaitCursor crs;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_BN_COLOR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Point color assigned to the specified item.") );
	pWnd = GetDlgItem( IDC_CBO_Y );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an x/y axis item to flag data within a specified range.") );
	pWnd = GetDlgItem( IDC_CHK_FLAG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Flag certain data values as specified.") );
	pWnd = GetDlgItem( IDC_EDIT_MIN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Minimum value to flag (inclusive).") );
	pWnd = GetDlgItem( IDC_EDIT_MAX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Maximum value to flag (inclusive).") );
	pWnd = GetDlgItem( IDC_CBO_POINT2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Point type assigned to the specified item.") );
	pWnd = GetDlgItem( IDC_CBO_SIZE2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Point size assigned to the specified item.") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// Point types
	CComboBox* pCBO = (CComboBox*)GetDlgItem( IDC_CBO_POINT2 );
	if( pCBO )
	{
		pCBO->AddString( _T("Plus") );
		pCBO->AddString( _T("Cross") );
		pCBO->AddString( _T("Dot") );
		pCBO->AddString( _T("Solid Dot") );
		pCBO->AddString( _T("Square") );
		pCBO->AddString( _T("Solid Square") );
		pCBO->AddString( _T("Diamond") );
		pCBO->AddString( _T("Solid Diamond") );
		pCBO->AddString( _T("Upward Triangle") );
		pCBO->AddString( _T("Solid Upward Triangle") );
		pCBO->AddString( _T("Downward Triangle") );
		pCBO->AddString( _T("Solid Downward Triangle") );
		pCBO->SetCurSel( m_nYPoint );
	}
	// Point sizes
	pCBO = (CComboBox*)GetDlgItem( IDC_CBO_SIZE2 );
	if( pCBO )
	{
		pCBO->AddString( _T("Small") );
		pCBO->AddString( _T("Medium") );
		pCBO->AddString( _T("Large") );
		pCBO->AddString( _T("Micro") );
		pCBO->SetCurSel( m_nYSize );
	}
	// Y-values for flagging
	pCBO = (CComboBox*)GetDlgItem( IDC_CBO_Y );
	AGraphDlg* pGD = (AGraphDlg*)m_pParentWnd;
	if( pCBO && pGD )
	{
		CString szItem;
		POSITION pos = pGD->m_lstAxis.GetHeadPosition();
		while( pos )
		{
			szItem = pGD->m_lstAxis.GetNext( pos );
			pCBO->AddString( szItem );
		}
		pCBO->SetCurSel( m_nY );
	}

	// Color buttons
	m_bnColor.SetColor( m_nYColor );

	OnChkFlag();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FlaggingDlg::OnClickBnColor() 
{
	m_nYColor = m_bnColor.GetColor();
}

void FlaggingDlg::OnChkFlag() 
{
	UpdateData( TRUE );
	
	CWnd* pWnd = GetDlgItem( IDC_CBO_Y );
	if( pWnd ) pWnd->EnableWindow( m_fFlag );
	pWnd = GetDlgItem( IDC_EDIT_MIN );
	if( pWnd ) pWnd->EnableWindow( m_fFlag && ( m_nY != -1 ) );
	pWnd = GetDlgItem( IDC_EDIT_MAX );
	if( pWnd ) pWnd->EnableWindow( m_fFlag && ( m_nY != -1 ) );
	pWnd = GetDlgItem( IDC_CBO_POINT2 );
	if( pWnd ) pWnd->EnableWindow( m_fFlag && ( m_nY != -1 ) );
	pWnd = GetDlgItem( IDC_CBO_SIZE2 );
	if( pWnd ) pWnd->EnableWindow( m_fFlag && ( m_nY != -1 ) );
	pWnd = GetDlgItem( IDC_BN_COLOR );
	if( pWnd ) pWnd->EnableWindow( m_fFlag && ( m_nY != -1 ) );
}

void FlaggingDlg::OnSelchangeCboY() 
{
	OnChkFlag();
}

BOOL FlaggingDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("analysischarts.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

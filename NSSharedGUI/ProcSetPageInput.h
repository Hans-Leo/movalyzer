#pragma once

// ProcSetPageInput.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageInput dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageInput : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageInput)

// Construction
public:
	ProcSetPageInput( BOOL fNew = TRUE, IProcSetting* pPS = NULL );
	~ProcSetPageInput();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_INPUT };
	BOOL			m_fNew;
	int				m_nRate;
	int				m_nPressure;
	double			m_dResolution;
	BOOL			m_fTilt;
	CString			m_szDev;
	CString			m_szDim;
	int				m_nMaxPP;
	int				m_nMaxPPDevice;
	CString			m_szMaxPP;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnBnClickedBnCalc();
	afx_msg void OnClickDSW();
	DECLARE_MESSAGE_MAP()
};

#pragma once

#include "NSTooltipCtrl.h"

// FeedbackDlg dialog

interface IFeedback;

class AFX_EXT_CLASS FeedbackDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(FeedbackDlg)

public:
	FeedbackDlg( IFeedback*, BOOL fNew, CWnd* pParent = NULL);
	virtual ~FeedbackDlg();

// Dialog Data
	enum { IDD = IDD_FEEDBACK };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	IFeedback*		m_pF;
	BOOL			m_fNew;
	CString			m_szID;
	CString			m_szDesc;
	CComboBox		m_cboType;
	int				m_nType;
	CString			m_szType;
	DWORD			m_MinColor;
	DWORD			m_MaxColor;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	CBCGPColorButton	m_bnMin;
	CBCGPColorButton	m_bnMax;

	afx_msg void OnBnClickedBnColormin();
	afx_msg void OnBnClickedBnColormax();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	virtual void OnOK();
public:
	afx_msg void OnEnKillfocusEditID();
};

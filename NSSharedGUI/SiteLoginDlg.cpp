// SiteLoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "SiteLoginDlg.h"
#include <afxinet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SiteLoginDlg dialog

BEGIN_MESSAGE_MAP(SiteLoginDlg, CBCGPDialog)
END_MESSAGE_MAP()

SiteLoginDlg::SiteLoginDlg( CWnd* pParent, CInternetSession* pIS )
	: CBCGPDialog( SiteLoginDlg::IDD, pParent ), m_pIS( pIS )
{
	m_szUser = _T("");
	m_szPass = _T("");
}

void SiteLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_PASS, m_szPass);
	DDX_Control(pDX, IDC_TXT_HELP, m_linkPass);
}

/////////////////////////////////////////////////////////////////////////////
// SiteLoginDlg message handlers

BOOL SiteLoginDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	m_linkPass.SetURL( _T("www.neuroscript.net/password.php") );
	m_linkPass.SetUnderline( TRUE );
	m_linkPass.SetLinkCursor( AfxGetApp()->LoadCursor(IDC_HAND2) );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SiteLoginDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szUser == _T("") )
	{
		BCGPMessageBox( _T("You must enter a user name.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_USER );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	if( m_szPass == _T("") )
	{
		BCGPMessageBox( _T("You must enter a password.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASS );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	TRIM( m_szUser );
	TRIM( m_szPass );

	// verify login credentials
	if( m_pIS )
	{
		CString szURL;
		szURL.Format( _T("http://www.neuroscript.net/lc.php?l=%s&p=%s"), m_szUser, m_szPass );
		CStdioFile* pFile = m_pIS->OpenURL( szURL );
		if( pFile )
		{
			CString szLine;
			pFile->ReadString( szLine );
			pFile->Close();
			delete pFile;
			szLine.MakeUpper();
			if( szLine.Find( _T("ERR") ) >= 0 )
			{
				BCGPMessageBox( _T("Invalid login. Please try again.") );
				return;
			}
		}
		else
		{
			BCGPMessageBox( _T("Unable to verify login. Please try again.") );
			return;
		}
	}

	CBCGPDialog::OnOK();
}

// AddConditionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "AddConditionDlg.h"
#include "..\DataMod\DataMod.h"
#include "RelationshipDlg.h"
#include "RelationshipDlg.h"
#include "Conditions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddConditionDlg dialog

BEGIN_MESSAGE_MAP(AddConditionDlg, CBCGPDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_WM_HELPINFO()
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST, OnLvnKeydownList)
END_MESSAGE_MAP()

AddConditionDlg::AddConditionDlg(CStringList* pExisting, CWnd* pParent, BOOL fSingleSel, BOOL fNoOptions)
	: AddDlg(pExisting, pParent, fSingleSel), m_fNoOptions( fNoOptions )
{
}

AddConditionDlg::~AddConditionDlg()
{
	Cleanup();
}

void AddConditionDlg::Cleanup()
{
}

void AddConditionDlg::InsertColumns()
{
	CString szCol;
	// Column Headers
	szCol.LoadString( IDS_COL_ID );
	m_list.InsertColumn( 0, szCol, LVCFMT_LEFT, 70, 0 );
	szCol.LoadString( IDS_COL_DESC );
	m_list.InsertColumn( 1, szCol, LVCFMT_LEFT, 85, 1 );
	szCol.LoadString( IDS_COL_INSTR );
	m_list.InsertColumn( 2, szCol, LVCFMT_LEFT, 75, 2 );
	szCol.LoadString( IDS_COL_LEX );
	m_list.InsertColumn( 3, szCol, LVCFMT_LEFT, 75, 3 );
	szCol.LoadString( IDS_COL_STROKEMIN );
	m_list.InsertColumn( 4, szCol, LVCFMT_LEFT, 80, 4 );
	szCol.LoadString( IDS_COL_STROKEMAX );
	m_list.InsertColumn( 5, szCol, LVCFMT_LEFT, 80, 5 );
	szCol.LoadString( IDS_COL_LEN );
	m_list.InsertColumn( 6, szCol, LVCFMT_LEFT, 70, 6 );
	szCol.LoadString( IDS_COL_RANGE );
	m_list.InsertColumn( 7, szCol, LVCFMT_LEFT, 70, 7 );
	szCol.LoadString( IDS_COL_DIR );
	m_list.InsertColumn( 8, szCol, LVCFMT_LEFT, 80, 8 );
	szCol.LoadString( IDS_COL_RANGE );
	m_list.InsertColumn( 9, szCol, LVCFMT_LEFT, 80, 9 );
	szCol.LoadString( IDS_COL_SKIP );
	m_list.InsertColumn( 10, szCol, LVCFMT_LEFT, 80, 10 );
	szCol.LoadString( IDS_COL_NOTES );
	m_list.InsertColumn( 11, szCol, LVCFMT_LEFT, 200, 11 );
	m_list.InsertColumn( 12, _T("Use Stimulus"), 80, 12 );
	m_list.InsertColumn( 13, _T("Stimulus"), 200 );
}

void AddConditionDlg::FillList()
{
	// Loop through all
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CString szID, szDesc, szInstr, szLex, szItem, szNotes, szStim;
	short nMin = 0, nMax = 0, nItem = 0, nImage = GetImageNum(), nSkip = 0;
	double dLength = 0.0, dRange = 0.0, dDir = 0.0, dRangeDir = 0.0;
	BOOL fStim, fRecord, fProcess;

	HRESULT hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetID( szID );
		cond.GetRecord( fRecord );
		cond.GetProcess( fProcess );

		nImage = IMG_CONDITION;
		if( fRecord && !fProcess ) nImage = IMG_CONDITION_RO;
		else if( fProcess && !fRecord ) nImage = IMG_CONDITION_PO;
		if( m_pExisting && ( m_pExisting->Find( szID ) == NULL ) )
		{
			nItem = m_list.InsertItem( nItem, szID, nImage );
			if( nItem != -1 )
			{
				cond.GetDescription( szDesc );
				cond.GetInstruction( szInstr );
				cond.GetLex( szLex );
				cond.GetStrokeMin( nMin );
				cond.GetStrokeMax( nMax );
				cond.GetStrokeLength( dLength );
				cond.GetRangeLength( dRange );
				cond.GetStrokeDirection( dDir );
				cond.GetRangeDirection( dRangeDir );
				cond.GetStrokeSkip( nSkip );
				cond.GetNotes( szNotes );
				cond.GetUseStimulus( fStim );
				cond.GetStimulus( szStim );

				m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 2, LVIF_TEXT, szInstr, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 3, LVIF_TEXT, szLex, 0, 0, 0, NULL );
				szItem.Format( _T("%d"), nMin );
				m_list.SetItem( nItem, 4, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				szItem.Format( _T("%d"), nMax );
				m_list.SetItem( nItem, 5, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				szItem.Format( _T("%.1f"), dLength );
				m_list.SetItem( nItem, 6, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				szItem.Format( _T("%.1f"), dRange );
				m_list.SetItem( nItem, 7, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				szItem.Format( _T("%.1f"), dDir );
				m_list.SetItem( nItem, 8, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				szItem.Format( _T("%.1f"), dRangeDir );
				m_list.SetItem( nItem, 9, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				szItem.Format( _T("%d"), nSkip );
				m_list.SetItem( nItem, 10, LVIF_TEXT, szItem, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 11, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
				szItem = fStim ? _T("True") : _T("False");
				m_list.SetItem( nItem, 12, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 13, LVIF_TEXT, szStim, 0, 0, 0, NULL );
				nItem++;
			}
		}

		hr = cond.GetNext();
	}

	if( nItem <= 0 ) OnClickBnAdd();
}

/////////////////////////////////////////////////////////////////////////////
// AddConditionDlg message handlers

BOOL AddConditionDlg::OnInitDialog() 
{
	// Window Title
	CString szTitle;
	szTitle.LoadString( IDS_ADD_CON );
	SetWindowText( szTitle );

	// icon
	m_tooltip.SetThisIcon( IDR_COND );

	// disable buttons if no options set
	if( m_fNoOptions )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_ADD );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_DEL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	return AddDlg::OnInitDialog();
}

void AddConditionDlg::OK()
{
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	CString szID, szDesc, szItem;
	short nIdx = 0;

	while( pos )
	{
		nIdx = m_list.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szID = m_list.GetItemText( nIdx, 0 );
			szDesc = m_list.GetItemText( nIdx, 1 );

			CString szDelim;
			::GetDelimiter( szDelim );
			szItem.Format( _T("%s%s%s"), szDesc, szDelim, szID );
			m_lstNew.AddTail( szItem );
		}
	}
}

int AddConditionDlg::Add() 
{
	NSConditions cond;
	if( !cond.IsValid() ) return -1;

	if( cond.DoAdd( this ) )
	{
		CString szID;
		cond.GetID( szID );
		int nItem = m_list.GetItemCount(), nImage = GetImageNum();
		nItem = m_list.InsertItem( nItem, szID, nImage );
		if( nItem != -1 )
		{
			CString szDesc, szInstr, szLex, szNotes, szStim;
			short nMin, nMax, nSkip;
			double dStrokeLength, dRangeLength, dStrokeDirection, dRangeDirection;
			BOOL fStim;

			cond.GetDescription( szDesc );
			cond.GetInstruction( szInstr );
			cond.GetLex( szLex );
			cond.GetStrokeMin( nMin );
			cond.GetStrokeMax( nMax );
			cond.GetStrokeSkip( nSkip );
			cond.GetStrokeLength( dStrokeLength );
			cond.GetStrokeDirection( dStrokeDirection );
			cond.GetRangeLength( dRangeLength );
			cond.GetRangeDirection( dRangeDirection );
			cond.GetUseStimulus( fStim );
			cond.GetStimulus( szStim );


			m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 2, LVIF_TEXT, szInstr, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 3, LVIF_TEXT, szLex, 0, 0, 0, NULL );
			CString szItem;
			szItem.Format( _T("%d"), nMin );
			m_list.SetItem( nItem, 4, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			szItem.Format( _T("%d"), nMax );
			m_list.SetItem( nItem, 5, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			szItem.Format( _T("%.1f"), dStrokeLength );
			m_list.SetItem( nItem, 6, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			szItem.Format( _T("%.1f"), dRangeLength );
			m_list.SetItem( nItem, 7, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			szItem.Format( _T("%.1f"), dStrokeDirection );
			m_list.SetItem( nItem, 8, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			szItem.Format( _T("%.1f"), dRangeDirection );
			m_list.SetItem( nItem, 9, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			szItem.Format( _T("%d"), nSkip );
			m_list.SetItem( nItem, 10, LVIF_TEXT, szItem, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 11, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
			szItem = fStim ? _T("True") : _T("False");
			m_list.SetItem( nItem, 12, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 13, LVIF_TEXT, szStim, 0, 0, 0, NULL );

			return nItem;
		}
	}

	return -1;
}

BOOL AddConditionDlg::Edit( int nItem )
{
	NSConditions cond;
	if( !cond.IsValid() ) return FALSE;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	if( cond.DoEdit( szID, this ) )
	{
		CString szDesc, szInstr, szLex, szNotes, szStim;
		short nMin, nMax, nSkip;
		double dStrokeLength, dRangeLength, dStrokeDirection, dRangeDirection;
		BOOL fStim;

		cond.GetDescription( szDesc );
		cond.GetInstruction( szInstr );
		cond.GetLex( szLex );
		cond.GetStrokeMin( nMin );
		cond.GetStrokeMax( nMax );
		cond.GetStrokeSkip( nSkip );
		cond.GetStrokeLength( dStrokeLength );
		cond.GetStrokeDirection( dStrokeDirection );
		cond.GetRangeLength( dRangeLength );
		cond.GetRangeDirection( dRangeDirection );
		cond.GetUseStimulus( fStim );
		cond.GetStimulus( szStim );

		m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 2, LVIF_TEXT, szInstr, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 3, LVIF_TEXT, szLex, 0, 0, 0, NULL );
		CString szItem;
		szItem.Format( _T("%d"), nMin );
		m_list.SetItem( nItem, 4, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		szItem.Format( _T("%d"), nMax );
		m_list.SetItem( nItem, 5, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		szItem.Format( _T("%.1f"), dStrokeLength );
		m_list.SetItem( nItem, 6, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		szItem.Format( _T("%.1f"), dRangeLength );
		m_list.SetItem( nItem, 7, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		szItem.Format( _T("%.1f"), dStrokeDirection );
		m_list.SetItem( nItem, 8, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		szItem.Format( _T("%.1f"), dRangeDirection );
		m_list.SetItem( nItem, 9, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		szItem.Format( _T("%d"), nSkip );
		m_list.SetItem( nItem, 10, LVIF_TEXT, szItem, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 11, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
		szItem = fStim ? _T("True") : _T("False");
		m_list.SetItem( nItem, 12, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 13, LVIF_TEXT, szStim, 0, 0, 0, NULL );
	}

	return TRUE;
}

BOOL AddConditionDlg::Delete( int nItem )
{
	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	NSConditions cond;
	if( !cond.IsValid() ) return FALSE;

	if( SUCCEEDED( cond.Find( szID ) ) )
	{
		if( FAILED( cond.Remove() ) )
		{
			BCGPMessageBox( IDS_DEL_ERR );
			return FALSE;
		}
	}

	return TRUE;
}

BOOL AddConditionDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentconditions.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void AddConditionDlg::Relationships( int nItem )
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( SUCCEEDED( cond.Find( szID ) ) )
	{
		CString szItem;
		cond.GetDescriptionWithID( szItem );

		RelationshipDlg d( this, REL_CONDITION, szItem );
		d.DoModal();
	}
	else BCGPMessageBox( _T("Could not find condition.") );
}

// AddGroupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "AddGroupDlg.h"
#include "GroupDlg.h"
#include "RelationshipDlg.h"
#include "Groups.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddGroupDlg dialog

BEGIN_MESSAGE_MAP(AddGroupDlg, CBCGPDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_WM_HELPINFO()
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST, OnLvnKeydownList)
END_MESSAGE_MAP()

AddGroupDlg::AddGroupDlg(CStringList* pExisting, CWnd* pParent, BOOL fSingleSel)
	: AddDlg(pExisting, pParent, fSingleSel)
{
}

AddGroupDlg::~AddGroupDlg()
{
	Cleanup();
}

void AddGroupDlg::Cleanup()
{
}

void AddGroupDlg::InsertColumns()
{
	// List size (width)
	RECT rect;
	m_list.GetClientRect( &rect );
	int nWidth = rect.right - rect.left;

	CString szCol;
	// Column Headers
	szCol.LoadString( IDS_COL_ID );
	m_list.InsertColumn( 0, szCol, LVCFMT_LEFT, 70, 0 );
	szCol.LoadString( IDS_COL_DESC );
	m_list.InsertColumn( 1, szCol, LVCFMT_LEFT, nWidth - 70, 1 );
	szCol.LoadString( IDS_COL_NOTES );
	m_list.InsertColumn( 2, szCol, LVCFMT_LEFT, 200, 2 );
}

void AddGroupDlg::FillList()
{
	// Loop through all
	Groups grp;
	if( !grp.IsValid() ) return;

	CString szID, szDesc, szItem, szNotes;
	int nItem = 0, nImage = GetImageNum();


	HRESULT hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetID( szID );
		if( m_pExisting && ( m_pExisting->Find( szID ) == NULL ) )
		{
			nItem = m_list.InsertItem( nItem, szID, nImage );
			if( nItem != -1 )
			{
				grp.GetDescription( szDesc );
				grp.GetNotes( szNotes );

				m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
				m_list.SetItem( nItem, 2, LVIF_TEXT, szNotes, 0, 0, 0, NULL );

				nItem++;
			}
		}

		hr = grp.GetNext();
	}

	if( nItem <= 0 ) OnClickBnAdd();
}

/////////////////////////////////////////////////////////////////////////////
// AddGroupDlg message handlers

BOOL AddGroupDlg::OnInitDialog() 
{
	// Window Title
	CString szTitle;
	szTitle.LoadString( IDS_ADD_GRP );
	SetWindowText( szTitle );

	m_tooltip.SetThisIcon( IDR_GRP );

	return AddDlg::OnInitDialog();
}

void AddGroupDlg::OK()
{
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	CString szID, szDesc, szItem;
	int nIdx = 0;
	while( pos )
	{
		nIdx = m_list.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szID = m_list.GetItemText( nIdx, 0 );
			szDesc = m_list.GetItemText( nIdx, 1 );

			CString szDelim;
			::GetDelimiter( szDelim );
			szItem.Format( _T("%s%s%s"), szDesc, szDelim, szID );
			m_lstNew.AddTail( szItem );
		}
	}
}

int AddGroupDlg::Add()
{
	Groups grp;
	if( !grp.IsValid() ) return -1;

	if( grp.DoAdd( this ) )
	{
		CString szID;
		grp.GetID( szID );
		int nItem = m_list.GetItemCount(), nImage = GetImageNum();
		nItem = m_list.InsertItem( nItem, szID, nImage );
		if( nItem != -1 )
		{
			CString szDesc, szNotes;
			grp.GetDescription( szDesc );
			grp.GetNotes( szNotes );

			m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 2, LVIF_TEXT, szNotes, 0, 0, 0, NULL );

			return nItem;
		}
	}

	return -1;
}

BOOL AddGroupDlg::Edit( int nItem )
{
	Groups grp;
	if( !grp.IsValid() ) return FALSE;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	if( grp.DoEdit( szID, this ) )
	{
		CString szDesc, szNotes;
		grp.GetDescription( szDesc );
		grp.GetNotes( szNotes );

		m_list.SetItem( nItem, 1, LVIF_TEXT, szDesc, 0, 0, 0, NULL );
		m_list.SetItem( nItem, 2, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
	}

	return TRUE;
}

BOOL AddGroupDlg::Delete( int nItem )
{
	CString szID = m_list.GetItemText( nItem, 0 );
	if( szID == _T("") ) return FALSE;

	Groups grp;
	if( !grp.IsValid() ) return FALSE;

	if( SUCCEEDED( grp.Find( szID ) ) )
	{
		if( FAILED( grp.Remove() ) )
		{
			BCGPMessageBox( IDS_DEL_ERR );
			return FALSE;
		}
	}

	return TRUE;
}

BOOL AddGroupDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("subjectgroups.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void AddGroupDlg::Relationships( int nItem )
{
	Groups grp;
	if( !grp.IsValid() ) return;

	CString szID = m_list.GetItemText( nItem, 0 );
	if( SUCCEEDED( grp.Find( szID ) ) )
	{
		CString szItem;
		grp.GetDescriptionWithID( szItem );

		RelationshipDlg d( this, REL_GROUP, szItem );
		d.DoModal();
	}
	else BCGPMessageBox( _T("Could not find group.") );
}

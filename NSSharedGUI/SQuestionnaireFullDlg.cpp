// SQuestionnaireFullDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "SQuestionnaireFullDlg.h"
#include "SQuestionnaireDlg.h"
#include "SQuestionnaireListDlg.h"
#include "..\DataMod\DataMod.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireFullDlg dialog

BEGIN_MESSAGE_MAP(SQuestionnaireFullDlg, CBCGPDialog)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_EXCEL, OnClickBnList)
END_MESSAGE_MAP()

SQuestionnaireFullDlg::SQuestionnaireFullDlg(ISQuestionnaire* pQ, CString szExpID, CString szGrpID, CWnd* pParent)
	: CBCGPDialog(SQuestionnaireFullDlg::IDD, pParent), m_pQ( pQ ), m_szExpID( szExpID ), m_szGrpID( szGrpID )
{
}

SQuestionnaireFullDlg::~SQuestionnaireFullDlg()
{
	if( m_lstID ) delete m_lstID;
	if( m_lstQ ) delete m_lstQ;
	if( m_lstA ) delete m_lstA;
	if( m_lstH ) delete m_lstH;
	if( m_lstI ) delete m_lstI;
	if( m_lstN ) delete m_lstN;
	if( m_lstCH ) delete m_lstCH;
}

void SQuestionnaireFullDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_EXCEL, m_bnGrid);
}

void SQuestionnaireFullDlg::OnCancel()
{
	if( m_lstH->GetCount() > 0 )
	{
		if( BCGPMessageBox( _T("Cancel and lose all changes?"), MB_YESNO ) == IDNO )
			return;
	}
	CBCGPDialog::OnCancel();
}

/////////////////////////////////////////////////////////////////////////////
// SQuestionnaireFullDlg message handlers

void SQuestionnaireFullDlg::OnClickBnAdd() 
{
	// selected item
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) nIdx = 0;

	// add/edit dialog
	SQuestionnaireDlg d( m_lstH, m_lstQ, m_lstA, m_lstN, m_lstID, m_lstCH, FALSE, nIdx, this );
	d.DoModal();

	// fill list
	FillList();

	// select item & ensure visible
	if( nIdx >= 0 )
	{
		LVITEM lvi;
		lvi.mask = LVIF_STATE;
		lvi.state = LVIS_SELECTED;
		lvi.stateMask = (UINT)-1;
		m_List.SetItemState( nIdx, &lvi );
		m_List.EnsureVisible( nIdx, FALSE );
	}

	// set focus to OK button
	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->SetFocus();
}

void SQuestionnaireFullDlg::OnClickBnEdit() 
{
	// selected item
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	// find header at index
	POSITION pos = m_lstH->FindIndex( nIdx );
	if( !pos ) return;
	// get header...if it IS a legit header, we dont edit it
	CString szH = m_lstH->GetAt( pos );
	if( szH != _T("") )
	{
		BCGPMessageBox( _T("This is a header. You can only provide answers to questions.") );
		return;
	}

	// add/edit dialog
	SQuestionnaireDlg d( m_lstH, m_lstQ, m_lstA, m_lstN, m_lstID, m_lstCH, TRUE, nIdx, this );
	if( d.DoModal() == IDOK )
	{
		// fill list
		FillList();

		// select item & ensure visible
		if( nIdx >= 0 )
		{
			LVITEM lvi;
			lvi.mask = LVIF_STATE;
			lvi.state = LVIS_SELECTED;
			lvi.stateMask = (UINT)-1;
			m_List.SetItemState( nIdx, &lvi );
			m_List.EnsureVisible( nIdx, FALSE );
		}
	}
}

BOOL SQuestionnaireFullDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// button images
	m_bnAdd.SetImage( IDB_ADD );
	m_bnEdit.SetImage( IDB_EDIT );
	m_bnGrid.SetImage( IDB_LIST );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ANSWER1 );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ANSWER );
	pWnd = GetDlgItem( IDC_BN_DEL );
	if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	pWnd = GetDlgItem( IDC_BN_UP );
	if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	if( pWnd ) pWnd->ShowWindow( SW_HIDE );
	pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_QUESTLIST );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CANCEL );
	pWnd = GetDlgItem( IDC_BN_EXCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter/edit the answers in a spreadsheet format.") );

	// List size (width)
	RECT rect;
	m_List.GetClientRect( &rect );
	int nWidth = rect.right - rect.left;

	// Column Headers
	CString szCol;
	szCol.LoadString( IDS_Q );
	m_List.InsertColumn( 0, szCol, LVCFMT_LEFT, (int)(nWidth / 2) - 10, 0 );
	szCol.LoadString( IDS_A );
	m_List.InsertColumn( 1, szCol, LVCFMT_LEFT, (int)(nWidth / 2) - 10, 1 );

	// Fill Lists
	m_lstID = new CStringList;
	m_lstQ = new CStringList;
	m_lstA = new CStringList;
	m_lstH = new CStringList;
	m_lstI = new CStringList;
	m_lstN = new CStringList;
	m_lstCH = new CStringList;

	if( m_pQ )
	{
		HRESULT hr = S_OK, hr2 = S_OK;
		BSTR bstrID = NULL, bstrQ = NULL, bstrA = NULL, bstrCH = NULL;
		CString szID, szQ, szA, szH, szI, szTemp, szN, szCH;
		CStringList ID, Q, A, H, I, N, CH;
		BOOL fHeader = FALSE, fNumeric = FALSE;
		short nPos = 0, nHighest = 0;
		IMQuestionnaire* pMQ = NULL;
		IGQuestionnaire* pGQ = NULL;

		hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
							   IID_IMQuestionnaire, (LPVOID*)&pMQ );
		if( FAILED( hr ) ) return TRUE;

		hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
							   IID_IGQuestionnaire, (LPVOID*)&pGQ );
		if( FAILED( hr ) )
		{
			pMQ->Release();
			return TRUE;
		}

		while( SUCCEEDED( hr ) )
		{
			m_pQ->get_ID( &bstrID );
			m_pQ->get_Answer( &bstrA );
			
			szID = bstrID;
			szA = bstrA;

			hr2 = pGQ->FindForGroup( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString() );
			while( SUCCEEDED( hr2 ) )
			{
				pGQ->get_ID( &bstrID );
				szTemp = bstrID;
				if( szTemp == szID )
				{
					hr2 = pMQ->Find( bstrID );
					if( SUCCEEDED( hr2 ) )
					{
						pMQ->get_ItemNum( &nPos );
						pMQ->get_IsHeader( &fHeader );
						pMQ->get_Question( &bstrQ );
						pMQ->get_IsNumeric( &fNumeric );
						pMQ->get_ColumnHeader( &bstrCH );
			
						szI.Format( _T("%d"), nPos );
						szH = fHeader ? _T("TRUE") : _T("");
						szN = fNumeric ? _T("TRUE") : _T("");
						szQ = bstrQ;
						szCH = bstrCH;

						ID.AddTail( szID );
						Q.AddTail( szQ );
						A.AddTail( szA );
						H.AddTail( szH );
						I.AddTail( szI );
						N.AddTail( szN );
						CH.AddTail( szCH );

						if( atoi( szI ) > nHighest ) nHighest = atoi( szI );

						break;
					}
				}

				hr2 = pGQ->GetNext();
			}

			hr = m_pQ->GetNext();
		}

		pMQ->Release();
		pGQ->Release();

		// Sort
		szTemp = _T("");
		for( int i = 0; i < nHighest; i++ )
		{
			m_lstID->AddTail( szTemp );
			m_lstQ->AddTail( szTemp );
			m_lstA->AddTail( szTemp );
			m_lstH->AddTail( szTemp );
			m_lstI->AddTail( szTemp );
			m_lstN->AddTail( szTemp );
			m_lstCH->AddTail( szTemp );
		}
		POSITION posID = ID.GetHeadPosition();
		POSITION posQ = Q.GetHeadPosition();
		POSITION posA = A.GetHeadPosition();
		POSITION posH = H.GetHeadPosition();
		POSITION posI = I.GetHeadPosition();
		POSITION posN = N.GetHeadPosition();
		POSITION posCH = CH.GetHeadPosition();
		POSITION pos = NULL;
		while( posI )
		{
			szI = I.GetNext( posI );
			szID = ID.GetNext( posID );
			szQ = Q.GetNext( posQ );
			szA = A.GetNext( posA );
			szH = H.GetNext( posH );
			szN = N.GetNext( posN );
			szCH = CH.GetNext( posCH );

			nPos = atoi( szI );

			pos = m_lstI->FindIndex( nPos - 1 );
			if( pos ) m_lstI->SetAt( pos, szI );
			pos = m_lstID->FindIndex( nPos - 1 );
			if( pos ) m_lstID->SetAt( pos, szID );
			pos = m_lstQ->FindIndex( nPos - 1 );
			if( pos ) m_lstQ->SetAt( pos, szQ );
			pos = m_lstA->FindIndex( nPos - 1 );
			if( pos ) m_lstA->SetAt( pos, szA );
			pos = m_lstH->FindIndex( nPos - 1 );
			if( pos ) m_lstH->SetAt( pos, szH );
			pos = m_lstN->FindIndex( nPos - 1 );
			if( pos ) m_lstN->SetAt( pos, szN );
			pos = m_lstCH->FindIndex( nPos - 1 );
			if( pos ) m_lstCH->SetAt( pos, szCH );
		}
		// remove empty items
		posID = m_lstID->GetHeadPosition();
		posQ = m_lstQ->GetHeadPosition();
		posA = m_lstA->GetHeadPosition();
		posH = m_lstH->GetHeadPosition();
		posI = m_lstI->GetHeadPosition();
		posN = m_lstN->GetHeadPosition();
		posCH = m_lstCH->GetHeadPosition();
		POSITION posID2, posQ2, posA2, posH2, posI2, posN2, posCH2;
		while( posID )
		{
			posID2 = posID;
			posI2 = posI;
			posQ2 = posQ;
			posA2 = posA;
			posH2 = posH;
			posN2 = posN;
			posCH2 = posCH;
			szID = m_lstID->GetNext( posID );
			szI = m_lstI->GetNext( posI );
			szQ = m_lstQ->GetNext( posQ );
			szA = m_lstA->GetNext( posA );
			szH = m_lstH->GetNext( posH );
			szN = m_lstN->GetNext( posN );
			szCH = m_lstCH->GetNext( posCH );
			if( szID == _T("") )
			{
				if( posID2 ) m_lstID->RemoveAt( posID2 );
				if( posQ2 ) m_lstQ->RemoveAt( posQ2 );
				if( posA2 ) m_lstA->RemoveAt( posA2 );
				if( posH2 ) m_lstH->RemoveAt( posH2 );
				if( posI2 ) m_lstI->RemoveAt( posI2 );
				if( posN2 ) m_lstN->RemoveAt( posN2 );
				if( posCH2 ) m_lstCH->RemoveAt( posCH2 );
			}
		}

		FillList();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL SQuestionnaireFullDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void SQuestionnaireFullDlg::FillList()
{
	m_List.DeleteAllItems();

	POSITION posQ = m_lstQ->GetHeadPosition();
	POSITION posA = m_lstA->GetHeadPosition();
	POSITION posH = m_lstH->GetHeadPosition();
	CString szQ, szA, szH, szTemp;
	int nItem = 0;

	while( posH )
	{
		szQ = m_lstQ->GetNext( posQ );
		szA = m_lstA->GetNext( posA );
		szH = m_lstH->GetNext( posH );

		if( szH == _T("") )
		{
			szTemp = _T("     ");
			szTemp += szQ;
		}
		else szTemp = szQ;
		nItem = m_List.InsertItem( nItem, szTemp );
		m_List.SetItem( nItem, 1, LVIF_TEXT, szA, 0, 0, 0, NULL );

		nItem++;
	}
}

void SQuestionnaireFullDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnClickBnEdit();

	*pResult = 0;
}

BOOL SQuestionnaireFullDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("questionnaire.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void SQuestionnaireFullDlg::OnClickBnList()
{
	SQuestListDlg d( m_lstH, m_lstQ, m_lstA, m_lstN, m_lstID, this );
	if( d.DoModal() == IDOK ) FillList();
}

// EditorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "EditorDlg.h"
#include "InstructionDlg.h"
#include "TextEntryDlg.h"
#include "..\DataMod\DataMod.h"
#include <direct.h>

IMPLEMENT_DYNAMIC(EditorDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(EditorDlg, CBCGPDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_BROWSE, OnClickOpen)
	ON_BN_CLICKED(IDC_BN_PASTE, OnClickPaste)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

EditorDlg::EditorDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(EditorDlg::IDD, pParent)
{
	m_nTop = 0;
	m_nLeft = 0;
	m_nRightBuffer = 0;
	m_nBottomBuffer = 0;
	m_nTopBuffer = 0;
	m_nBnBuffer = 0;
	m_fNew = TRUE;
	m_nType = 0;
}

EditorDlg::~EditorDlg()
{
}

void EditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RE, m_re);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnOpen );
	DDX_Control(pDX, IDC_BN_PASTE, m_bnPaste );
}


// EditorDlg message handlers

BOOL EditorDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL EditorDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();

	// dialog title
	CString szFile;
	if( m_szFile != _T("") )
	{
		::GetDataPathRoot( szFile );
		szFile += _T("\\scripts\\");
		szFile += m_szFile;

		CFileFind ff;
		if( ff.FindFile( szFile ) )
		{
			CString szOldTitle, szNewTitle;
			GetWindowText( szOldTitle );
			szNewTitle.Format( _T("%s - %s"), szOldTitle, m_szFile );
			SetWindowText( szNewTitle );
			m_fNew = FALSE;
		}
		else m_szFile = _T("");
	}

	// set instructions / hints
	CWnd* pWnd = GetDlgItem( IDC_TXT_INFO );
	if( pWnd )
	{
		CString szMsg;
		switch( m_nType )
		{
			case eEAT_Matlab:
				szMsg = _T("USE: %HWR%, %TF%, %SEG%, %EXT% in your scripts for the corresponding file. See HELP for more details.");
				break;
			case eEAT_Batch:
				szMsg = _T("NOTE: File path, exp., group, subject, cond. and trial will be sent as parameters. See HELP for more details.");
				break;
		}
		pWnd->SetWindowText( szMsg );
	}

	// read file (if exists)
	if( !m_fNew )
	{
		try
		{
			CFile f( szFile, CFile::modeRead );
			EDITSTREAM es;
#pragma warning( disable: 4311 )
			es.dwCookie = (DWORD)&f;
#pragma warning( default: 4311 )
			es.pfnCallback = StreamInCallback;
			m_re.StreamIn( SF_TEXT, es );
		}
		catch( CFileException* e )
		{
			TCHAR szCause[ 255 ];
			CString szMsg;
			e->GetErrorMessage( szCause, 255 );
			szMsg.Format( _T("The file could not be opened for the following reason\n: %s"), szCause );
			BCGPMessageBox( szMsg );
			e->Delete();
			EndDialog( IDOK );
			return FALSE;
		}
	}

	m_re.SetSel( 0, 0 );
	pWnd = GetDlgItem( IDC_RE );
	pWnd->SetFocus();

	// button images
	m_bnOpen.SetImage( IDB_OPEN );
//	m_bnPaste.SetImage( IDB_DATA );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Open an existing script file."), _T("Open Script") );
	pWnd = GetDlgItem( IDC_BN_PASTE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Paste contents of clipboard into editor."), _T("Paste") );

	 // get initial positions/sizes
	RECT rect, rect2;
	m_re.GetWindowRect( &rect );
	GetWindowRect( &rect2 );
	m_nTopBuffer = rect.top - rect2.top;
	m_nRightBuffer = rect2.right - rect.right;
	m_nBottomBuffer = rect2.bottom - rect.bottom;
	ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	pWnd = GetDlgItem( IDOK );
	if( pWnd )
	{
		m_re.GetWindowRect( &rect );
		pWnd->GetWindowRect( &rect2 );
		m_nBnBuffer = ( rect2.top - rect.bottom );
	}
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	pWnd->GetWindowRect( &rect );
	ScreenToClient( &rect );
	m_nBnOpenLeft = rect.left;
 	pWnd = GetDlgItem( IDC_BN_PASTE );
	pWnd->ShowWindow( SW_HIDE );
// 	pWnd->GetWindowRect( &rect );
// 	ScreenToClient( &rect );
// 	m_nBnPasteLeft = rect.left;

	EnableVisualManagerStyle( TRUE, TRUE );

	return FALSE;
}

void EditorDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	if( !m_re.m_hWnd ) return;

	CRect rectWnd, rectRE, rect;
	GetWindowRect( rectWnd );

	m_re.SetWindowPos( NULL, m_nLeft, m_nTop,
					   cx - m_nRightBuffer,
					   rectWnd.Height() - m_nBottomBuffer - m_nTopBuffer,
					   SWP_NOOWNERZORDER | SWP_NOZORDER );
	m_re.GetWindowRect( rectRE );

	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd )
	{
		pWnd->GetWindowRect( rect );

		pWnd->SetWindowPos( NULL,
							cx - rect.Width() - 9 - rect.Width() - 4,
							cy - rect.Height() - m_nBnBuffer + 3,
							0, 0,
							SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE );
	}
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd )
	{
		pWnd->GetWindowRect( rect );

		pWnd->SetWindowPos( NULL,
							cx - rect.Width() - 9,
							cy - rect.Height() - m_nBnBuffer + 3,
							0, 0,
							SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE );
	}
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd )
	{
		pWnd->GetWindowRect( rect );

		pWnd->SetWindowPos( NULL,
							m_nBnOpenLeft,
							cy - rect.Height() - m_nBnBuffer + 3,
							0, 0,
							SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE );
	}
// 	pWnd = GetDlgItem( IDC_BN_PASTE );
// 	if( pWnd )
// 	{
// 		pWnd->GetWindowRect( rect );
// 
// 		pWnd->SetWindowPos( NULL,
// 							m_nBnPasteLeft,
// 							cy - rect.Height() - m_nBnBuffer + 3,
// 							0, 0,
// 							SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE );
// 	}
}

void EditorDlg::OnCancel()
{
	CBCGPDialog::OnCancel();
}

void EditorDlg::OnOK()
{
	// verify that there is at least SOMETHING in there
	if( m_re.GetTextLengthEx( GTL_NUMCHARS ) == 0 )
	{
		BCGPMessageBox( _T("There is nothing to save."), MB_OK | MB_ICONINFORMATION );
		return;
	}

	// if files already exists (not new), ask if they just want to overwrite
	BOOL fOverwrite = FALSE;
	if( !m_fNew )
	{
		if( BCGPMessageBox( _T("Save to original file?"), MB_YESNO | MB_ICONQUESTION ) == IDYES )
			fOverwrite = TRUE;
	}

	CString szFile, szPath;
	// base path..ensure exists
	::GetDataPathRoot( szPath );
	szPath += _T("\\scripts\\");
	_mkdir( szPath );

	// if new or to not overwrite, ask for file name
	if( m_fNew || ( !m_fNew && !fOverwrite ) )
	{
		CTextEntryDlg d;
		if( d.Show( this, _T("Save Script"), _T("File Name (no extension):") ) == IDOK )
		{
			szFile = d.GetText();
			if( szFile == _T("") )
			{
				BCGPMessageBox( _T("No file name specified.") );
				return;
			}
		}
		else return;

		// full path
		szPath += szFile;
		// extension
		CString szExt;
		switch( m_nType )
		{
			case eEAT_Matlab: szExt = _T(".m"); break;
			case eEAT_Batch: szExt = _T(".bat"); break;
		}
		szPath += szExt;
		// check if exists
		CFileFind ff;
		if( ff.FindFile( szPath ) )
		{
			if( BCGPMessageBox( _T("File exists. Overwrite?"), MB_YESNO | MB_ICONWARNING ) == IDNO )
				return;
		}
	}
	else szPath += m_szFile;
	// write file
	try
	{
		CFile f( szPath, CFile::modeCreate | CFile::modeWrite );
		EDITSTREAM es;
#pragma warning( disable: 4311 )
		es.dwCookie = (DWORD)&f;
#pragma warning( default: 4311 )
		es.pfnCallback = StreamOutCallback;
		m_re.StreamOut( SF_TEXT, es );
	}
	catch (CFileException* e)
	{
		TCHAR szCause[ 255 ];
		CString szMsg;
		e->GetErrorMessage( szCause, 255 );
		szMsg.Format( _T("The file could not be saved for the following reason\n: %s"), szCause );
		BCGPMessageBox( szMsg );
		e->Delete();
		return;
	}
	// inform
	szFile.Format( _T("The file was saved successfully to:\n%s"), szPath );
	BCGPMessageBox( szFile, MB_ICONINFORMATION );

	CBCGPDialog::OnOK();
}

void EditorDlg::OnClickOpen()
{
	CString szExt, szType, szFilter;
	switch( m_nType )
	{
		case eEAT_Matlab: szExt = _T("m"); szType = _T("Matlab"); break;
		case eEAT_Batch: szExt = _T("bat"); szType = _T("Batch"); break;
	}
	szFilter.Format( _T("%s Files (*.%s)|*.%s|"), szType, szExt, szExt );

	CFileDialog d( TRUE, szFilter, _T(""), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this );
	if( d.DoModal() == IDCANCEL ) return;
	CString szFile = d.GetPathName();

	m_re.Clear();
	try
	{
		CFile f( szFile, CFile::modeRead );
		EDITSTREAM es;
#pragma warning( disable: 4311 )
		es.dwCookie = (DWORD)&f;
#pragma warning( default: 4311 )
		es.pfnCallback = StreamInCallback;
		m_re.StreamIn( SF_TEXT, es );
	}
	catch( CFileException* e )
	{
		TCHAR szCause[ 255 ];
		CString szMsg;
		e->GetErrorMessage( szCause, 255 );
		szMsg.Format( _T("The file could not be opened for the following reason\n: %s"), szCause );
		BCGPMessageBox( szMsg );
		e->Delete();
		EndDialog( IDOK );
	}
}
void EditorDlg::OnClickPaste()
{
	// not using this for now (hidden)
}

BOOL EditorDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("experimentsettings_processing_external.html"), this );
	return CBCGPDialog::OnHelpInfo( pHelpInfo );
}

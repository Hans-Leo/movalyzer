// GraphDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "DataTabDlg.h"
#include "WindowManager.h"
#include "..\Common\UserObj.h"
#include "GraphDlg.h"
#include "Experiments.h"
#include "Conditions.h"
#include "Feedbacks.h"
#include "..\ProEssentials5\VC\pegrpapi.h"
#include "..\ShowMod\ShowMod.h"
#include "..\ShowMod\ShowMod_i.c"
#include "..\Common\MessageManager.h"
#include "InstructionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	NUM_HWR_GRAPHS		5
#define	NUM_TF_GRAPHS		18
int	nNumTFGraphs = NUM_TF_GRAPHS;
#define NFREQMAX			(2048 / 2 + 1)

static int _Timer = 0;
static LONG _Cnt = 0;
static LONG _CurCnt = 0;

DWORD CGraphDlg::_MinColor = 0;
DWORD CGraphDlg::_MaxColor = 0;
CString CGraphDlg::_szFeature = _T("");
BOOL CGraphDlg::m_fDoNotNotify = FALSE;

// menu defines
#define GMENU_VIEW					1
#define GMENU_THICKNESS				3
#define GMENU_ANNOTATIONS			8
#define GMENU_SUBMVNT				4
#define GMENU_FEEDBACK				4
#define GMENU_TF					2

static CGraphDlg*		_pActiveChart = NULL;

/////////////////////////////////////////////////////////////////////////////
// CGraphDlg dialog

BEGIN_MESSAGE_MAP(CGraphDlg, CBCGPDialog)
	ON_WM_ACTIVATE()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_PREV, MovePrev)
	ON_BN_CLICKED(IDC_BN_NEXT, MoveNext)
	ON_BN_CLICKED(IDC_BN_PREV_TRIAL, PrevTrial)
	ON_BN_CLICKED(IDC_BN_NEXT_TRIAL, NextTrial)
	ON_CBN_SELCHANGE(IDC_CBO_GRAPH, OnSelchangeCboGraph)
	ON_CBN_SELCHANGE(IDC_CBO_X, OnSelchangeCboX)
	ON_CBN_SELCHANGE(IDC_CBO_Y, OnSelchangeCboY)
	ON_CBN_SELCHANGE(IDC_CBO_FEATURE, OnSelchangeCboFeature)
	ON_WM_TIMER()
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_GO, OnBnGo)
	ON_MESSAGE( WM_WINDOWMANAGER, OnWMMessage )
	ON_BN_CLICKED(IDC_BN_ERR, OnClickErr)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////

//************************************
// Method:    CGraphDlg
// FullName:  CGraphDlg::CGraphDlg
// Access:    public 
// Returns:   
// Qualifier: : CBCGPDialog(IDD, pParent), m_hPE( NULL ), m_nChart( nChart ), m_pFileList( NULL ), m_pGraph( NULL ), m_pShowHWR( NULL ), m_pShowTF( NULL ), m_posCur( NULL ), m_pWM( pWM )
// Parameter: UINT nChart
// Parameter: CWnd * pParent
// Parameter: WindowManager * pWM
//************************************
CGraphDlg::CGraphDlg(UINT nChart, CWnd* pParent /*=NULL*/, WindowManager* pWM /*=NULL*/)
	: CBCGPDialog(CGraphDlg::IDD, pParent), m_hPE( NULL ), m_nChart( nChart ), m_pFileList( NULL ),
	  m_pGraph( NULL ), m_pShowHWR( NULL ), m_pShowTF( NULL ), m_posCur( NULL ), m_pWM( pWM )
{
	m_fAnnValues = FALSE;
	m_nCurChart = 0;
	m_dFrequency = 0.0;
	m_nPenPressure = 0;
	m_nTop = 0;
	m_nLeft = 0;
	m_nRightBuffer = 0;
	m_nBottomBuffer = 0;
	m_nToolbar = 0;
	m_nErrTxt = 0;
	m_nErrBn = 0;
	m_fModeless = FALSE;
	m_fMultiple = FALSE;
	m_fOverlay = FALSE;
	m_dwPenUpColor = ::GetSoftColor();
	m_fChangePenUpColor = FALSE;
	m_fProportional = TRUE;
	m_fOptionChanged = FALSE;
	m_fMonochrome = FALSE;
	m_fFeedback = FALSE;
	m_MinColor = 0;
	m_MaxColor = 0;
	m_fGraphOnly = FALSE;
	m_pWndOther = NULL;
	m_pWndNotify = NULL;
	m_fSupressAnn = FALSE;
	m_fPassedConsis = FALSE;
	m_fInverted = FALSE;
	m_nThickness = 3;
	m_fSMA = FALSE;
	m_szErr = _T("OK");
	m_nSymbolSeg = IDM_CHART_SYMBOL_SEG_3;
	m_nSymbolSubMvmt = IDM_CHART_SYMBOL_SUB_2;
	m_fShowChartData = TRUE;
	m_nFeature = TF_MENU_BASE;
}

//************************************
// Method:    CGraphDlg
// FullName:  CGraphDlg::CGraphDlg
// Access:    public 
// Returns:   
// Qualifier: : CBCGPDialog(), m_hPE( NULL ), m_nChart( nChart ), m_dFrequency( dFreq ), m_pGraph( NULL ), m_pShowHWR( NULL ), m_pShowTF( NULL ), m_pFileList( NULL ), m_szFile( szFile ),
// Parameter: double dFreq
// Parameter: int nPrs
// Parameter: CString szFile
// Parameter: UINT nChart
// Parameter: BOOL fFeedback
// Parameter: CString szFeature
// Parameter: DWORD minColor
// Parameter: DWORD maxColor
// Parameter: CWnd * pParent
// Parameter: CString szSeg
// Parameter: DWORD dwPenUpColor
// Parameter: BOOL fChangePenUpColor
// Parameter: BOOL fGraphOnly
// Parameter: CWnd * pWndOther
// Parameter: CWnd * pWndNotify
// Parameter: BOOL fPassed
// Parameter: BOOL fForceShowErr
//************************************
CGraphDlg::CGraphDlg(double dFreq, int nPrs, CString szFile, UINT nChart,
					 BOOL fFeedback, CString szFeature, DWORD minColor, DWORD maxColor,
					 CWnd* pParent, CString szSeg, DWORD dwPenUpColor,
					 BOOL fChangePenUpColor, BOOL fGraphOnly, CWnd* pWndOther,
					 CWnd* pWndNotify, BOOL fPassed, BOOL fForceShowErr )
	: CBCGPDialog(), m_hPE( NULL ), m_nChart( nChart ), m_dFrequency( dFreq ),
	  m_pGraph( NULL ), m_pShowHWR( NULL ), m_pShowTF( NULL ), m_pFileList( NULL ),
	  m_szFile( szFile ), m_szFile2( szSeg ), m_posCur( NULL ), m_nPenPressure( nPrs ),
	  m_fFeedback( fFeedback ), m_szFeature( szFeature ), m_MinColor( minColor ),
	  m_MaxColor( maxColor ), m_pWndOther( (CWnd*)pWndOther ), m_pWndNotify( pWndNotify ),
	  m_fGraphOnly( fGraphOnly ), m_pWM( NULL ), m_fPassedConsis( fPassed )
{
	m_fAnnValues = FALSE;
	m_nCurChart = 0;
	m_nTop = 0;
	m_nLeft = 0;
	m_nRightBuffer = 0;
	m_nBottomBuffer = 0;
	m_nToolbar = 0;
	m_nErrTxt = 0;
	m_nErrBn = 0;
	m_fInverted = FALSE;
	m_fModeless = TRUE;
	m_fMultiple = FALSE;
	m_fOverlay = FALSE;
	m_fChangePenUpColor = fChangePenUpColor;
	m_dwPenUpColor = dwPenUpColor;
	m_fProportional = TRUE;
	m_fOptionChanged = FALSE;
	m_fMonochrome = FALSE;
	m_fSupressAnn = FALSE;
	m_fSMA = FALSE;
	m_szErr = _T("OK");
	m_nSymbolSeg = IDM_CHART_SYMBOL_SEG_3;
	m_nSymbolSubMvmt = IDM_CHART_SYMBOL_SUB_2;
	m_fShowChartData = TRUE;
	m_nFeature = TF_MENU_BASE;

	// if not provided a window handle, create a modeless dialog to display
	if( Create( CGraphDlg::IDD, pParent ) ) {
		if( !m_pWM ) {
			m_pWM = ::GetWindowManager();
			if( m_pWM ) m_pWM->AddItem( this );
		}
		ShowWindow( m_pWndOther ? SW_HIDE : SW_SHOW );
	}
}

//************************************
// Method:    CGraphDlg
// FullName:  CGraphDlg::CGraphDlg
// Access:    public 
// Returns:   
// Qualifier: : CBCGPDialog(), m_hPE( NULL ), m_nChart( nChart ), m_dFrequency( dFreq ), m_pGraph( NULL ), m_pShowHWR( NULL ), m_pShowTF( NULL ), m_pFileList( pFileL
// Parameter: double dFreq
// Parameter: int nPrs
// Parameter: CStringList * pFileList
// Parameter: CString szStart
// Parameter: UINT nChart
// Parameter: BOOL fFeedback
// Parameter: CString szFeature
// Parameter: DWORD minColor
// Parameter: DWORD maxColor
// Parameter: CWnd * pParent
// Parameter: BOOL fMultiple
// Parameter: DWORD dwPenUpColor
// Parameter: BOOL fChangePenUpColor
// Parameter: BOOL fGraphOnly
// Parameter: CWnd * pWndOther
// Parameter: CWnd * pWndNotify
// Parameter: WindowManager * pWM
// Parameter: BOOL fPassed
//************************************
CGraphDlg::CGraphDlg(double dFreq, int nPrs, CStringList* pFileList, CString szStart,
					 UINT nChart, BOOL fFeedback, CString szFeature, DWORD minColor,
					 DWORD maxColor, CWnd* pParent, BOOL fMultiple, DWORD dwPenUpColor,
					 BOOL fChangePenUpColor, BOOL fGraphOnly, CWnd* pWndOther, CWnd* pWndNotify,
					 WindowManager* pWM, BOOL fPassed )
	: CBCGPDialog(), m_hPE( NULL ), m_nChart( nChart ), m_dFrequency( dFreq ),
	  m_pGraph( NULL ), m_pShowHWR( NULL ), m_pShowTF( NULL ), m_pFileList( pFileList ),
	  m_posCur( NULL ), m_fOverlay( fMultiple ), m_nPenPressure( nPrs ),
	  m_fFeedback( fFeedback ), m_szFeature( szFeature ), m_MinColor( minColor ),
	  m_MaxColor( maxColor ), m_pWndOther( (CWnd*)pWndOther ), m_pWndNotify( pWndNotify ),
	  m_fGraphOnly( fGraphOnly ), m_pWM( pWM ), m_fPassedConsis( fPassed )
{
	m_fAnnValues = FALSE;
	m_nCurChart = 0;
	m_nTop = 0;
	m_nLeft = 0;
	m_nRightBuffer = 0;
	m_nBottomBuffer = 0;
	m_nToolbar = 0;
	m_nErrTxt = 0;
	m_nErrBn = 0;
	m_fInverted = FALSE;
	m_fModeless = TRUE;
	m_fMultiple = pFileList ? ( pFileList->GetCount() > 1 ) : FALSE;
	m_fChangePenUpColor = fChangePenUpColor;
	m_dwPenUpColor = dwPenUpColor;
	m_fProportional = TRUE;
	m_fOptionChanged = FALSE;
	m_fMonochrome = FALSE;
	m_fSupressAnn = FALSE;
	m_fSMA = FALSE;
	m_szErr = _T("OK");
	m_nSymbolSeg = IDM_CHART_SYMBOL_SEG_3;
	m_nSymbolSubMvmt = IDM_CHART_SYMBOL_SUB_2;
	if( m_pWndNotify ) m_fShowChartData = FALSE;
	else m_fShowChartData = TRUE;
	m_nFeature = TF_MENU_BASE;

	if( m_fOverlay && pFileList )
	{
		POSITION pos = pFileList->GetHeadPosition();
		if( pos ) m_szFile = pFileList->GetNext( pos );
	}
	else m_szFile = szStart;

	// if not provided a window handle, create a modeless dialog to display
	if( Create( CGraphDlg::IDD, pParent ) )
		ShowWindow( m_pWndOther ? SW_HIDE : SW_SHOW );
}

//************************************
// Method:    ~CGraphDlg
// FullName:  CGraphDlg::~CGraphDlg
// Access:    public 
// Returns:   
// Qualifier:
//************************************
CGraphDlg::~CGraphDlg()
{
	if( m_hPE ) ::DestroyWindow( m_hPE );
	if( m_pShowHWR ) m_pShowHWR->Release();
	if( m_pShowTF ) m_pShowTF->Release();
	if( m_fModeless && m_pFileList ) delete m_pFileList;
	POSITION pos = m_lstGraphs.GetHeadPosition();
	while( pos )
	{
		GraphSelection* pSel = (GraphSelection*)m_lstGraphs.GetNext( pos );
		if( pSel ) delete pSel;
	}
	m_lstGraphs.RemoveAll();
	pos = m_lstAxes.GetHeadPosition();
	while( pos )
	{
		GraphAxis* pA = (GraphAxis*)m_lstAxes.GetNext( pos );
		if( pA ) delete pA;
	}
	m_lstAxes.RemoveAll();
}

//************************************
// Method:    DoDataExchange
// FullName:  CGraphDlg::DoDataExchange
// Access:    virtual protected 
// Returns:   void
// Qualifier:
// Parameter: CDataExchange * pDX
//************************************
void CGraphDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_GRAPH, m_cbo);
	DDX_Control(pDX, IDC_CBO_X, m_cboX);
	DDX_Control(pDX, IDC_CBO_Y, m_cboY);
	DDX_Control(pDX, IDC_CBO_FEATURE, m_cboFeature);
	DDX_Control(pDX, IDC_BN_PREV, m_bnPrev);
	DDX_Control(pDX, IDC_BN_NEXT, m_bnNext);
	DDX_Control(pDX, IDC_BN_PREV_TRIAL, m_bnPrevT);
	DDX_Control(pDX, IDC_BN_NEXT_TRIAL, m_bnNextT);
	DDX_Control(pDX, IDC_BN_ERR, m_bnErr);
	DDX_Text(pDX, IDC_TXT_ERR, m_szErr);
	DDX_Control(pDX, IDC_BN_GO, m_bnGo);
}

//************************************
// Method:    GetAxis
// FullName:  CGraphDlg::GetAxis
// Access:    protected 
// Returns:   GraphAxis*
// Qualifier:
// Parameter: int nIndex
//************************************
GraphAxis* CGraphDlg::GetAxis( int nIndex )
{
	if( ( nIndex < 0 ) || ( nIndex > ( m_lstAxes.GetCount() - 1 ) ) ) return NULL;

	POSITION pos = m_lstAxes.FindIndex( nIndex );
	if( pos ) return (GraphAxis*)m_lstAxes.GetAt( pos );
	else return NULL;
}

//************************************
// Method:    GetAxis
// FullName:  CGraphDlg::GetAxis
// Access:    protected 
// Returns:   GraphAxis*
// Qualifier:
// Parameter: CString szLabel
//************************************
GraphAxis* CGraphDlg::GetAxis( CString szLabel, BOOL fTFLabel )
{
	GraphAxis* pAxis = NULL;
	POSITION pos = m_lstAxes.GetHeadPosition();
	while( pos )
	{
		GraphAxis* pA = (GraphAxis*)m_lstAxes.GetNext( pos );
		CString szLbl = fTFLabel ? pA->m_szTFLabel : pA->m_szLabel;
		szLbl.MakeLower();
		szLabel.MakeLower();
		if( szLbl == szLabel )
		{
			pAxis = pA;
			break;
		}
	}
	return pAxis;
}

//************************************
// Method:    GetGraph
// FullName:  CGraphDlg::GetGraph
// Access:    protected 
// Returns:   GraphSelection*
// Qualifier:
// Parameter: int nIndex
//************************************
GraphSelection* CGraphDlg::GetGraph( int nIndex )
{
	if( ( nIndex < 0 ) || ( nIndex > ( m_lstGraphs.GetCount() - 1 ) ) ) return NULL;

	POSITION pos = m_lstGraphs.FindIndex( nIndex );
	if( pos ) return (GraphSelection*)m_lstGraphs.GetAt( pos );
	else return NULL;
}

//************************************
// Method:    GetGraph
// FullName:  CGraphDlg::GetGraph
// Access:    protected 
// Returns:   GraphSelection*
// Qualifier:
// Parameter: GraphAxis * pAxisX
// Parameter: GraphAxis * pAxisY
//************************************
GraphSelection* CGraphDlg::GetGraph( GraphAxis* pAxisX, GraphAxis* pAxisY )
{
	if( !pAxisX || !pAxisY ) return NULL;

	GraphSelection* pSel = NULL;
	POSITION pos = m_lstGraphs.GetHeadPosition();
	while( pos )
	{
		GraphSelection* pS = (GraphSelection*)m_lstGraphs.GetNext( pos );
		if( pS && ( pS->m_pAxisX == pAxisX ) && ( pS->m_pAxisY == pAxisY ) )
		{
			pSel = pS;
			break;
		}
	}
	return pSel;
}

/////////////////////////////////////////////////////////////////////////////
// CGraphDlg message handlers

//************************************
// Method:    OnInitDialog
// FullName:  CGraphDlg::OnInitDialog
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL CGraphDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	// Dialog icon
	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDI_CHART ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_SUBJ );
	m_tooltip.SetTitle( _T("CHART") );
	CWnd* pWnd = GetDlgItem( IDC_GRAPH );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_GRAPH, _T("Chart") );
	pWnd = GetDlgItem( IDC_BN_PREV );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_PREVG, _T("Previous Chart") );
	pWnd = GetDlgItem( IDC_BN_NEXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_NEXTG, _T("Next Chart") );
	pWnd = GetDlgItem( IDC_CBO_GRAPH );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_LIST, _T("Chart Selection") );
	pWnd = GetDlgItem( IDC_CBO_X );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the X-Axis to display in the chart."), _T("X-Axis") );
	pWnd = GetDlgItem( IDC_CBO_Y );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the Y-Axis to display in the chart."), _T("Y-Axis") );
	pWnd = GetDlgItem( IDC_CBO_FEATURE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the feedback time function."), _T("Feedback") );
	pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_PREVT, _T("Previous Trial") );
	if( m_fMultiple && pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GD_TT_NEXTT, _T("Next Trial") );
	if( m_fMultiple && pWnd ) pWnd->EnableWindow( TRUE );
	pWnd = GetDlgItem( IDC_BN_ERR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to view the complete consistency error message."), _T("Consistency Error") );

	// extension - gripper/stimulus/tf file?
	CString szExt = m_szFile.Right( 3 );
	szExt.MakeUpper();
	BOOL fGripper = ( szExt == _T("FRD") );
	BOOL fStimulus = ( szExt == _T("STI") );
	BOOL fTF = ( szExt.Right( 2 ) == _T("TF") );

	// if TF, check to see if it was from HWR or FRD
	if( fTF )
	{
		CString szTmp = m_szFile.Left( m_szFile.GetLength() - 2 );
		szTmp += _T("FRD");
		CFileFind ff;
		if( ff.FindFile( szTmp ) ) fGripper = TRUE;
	}

	// chart name w/o extension
	if( ( m_nChart == CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
		m_szFile = m_szFile.Left( m_szFile.GetLength() - 4 );
	else if( ( m_nChart == CHART_TF ) || ( m_nChart == CHART_TF3D ) )
		m_szFile = m_szFile.Left( m_szFile.GetLength() - 3 );

	// if there is a file list, locate in the list the starting file
	if( m_pFileList )
	{
		m_posCur = m_pFileList->Find( m_szFile );
		if( !m_posCur ) m_posCur = m_pFileList->GetHeadPosition();
	}

	// update extensions and second/additional file
	m_szFile2 = m_szFile;
	if( ( m_nChart == CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		if( fGripper )
			m_szFile += _T(".FRD");
		else if( fStimulus )
			m_szFile += _T(".STI");
		else
			m_szFile += _T(".HWR");
	}
	else if( ( m_nChart == CHART_TF ) || ( m_nChart == CHART_TF3D ) )
		m_szFile += _T(".TF");
	m_szFile2 += _T(".SEG");

	// enable/disable certain features of multiple files to be shown
	if( m_fMultiple && m_pFileList && !m_fOverlay )
	{
		if( m_pFileList->GetHeadPosition() == m_posCur )
		{
			pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
			if( pWnd ) pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
			if( pWnd ) pWnd->EnableWindow( TRUE );
		}
		else if( m_pFileList->GetTailPosition() == m_posCur )
		{
			pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
			if( pWnd ) pWnd->EnableWindow( TRUE );
			pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
			if( pWnd ) pWnd->EnableWindow( FALSE );
		}
		else
		{
			pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
			if( pWnd ) pWnd->EnableWindow( TRUE );
			pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
			if( pWnd ) pWnd->EnableWindow( TRUE );
		}
	}
	else if( m_fOverlay )
	{
		pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	else
	{
		pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// button colors
	if( CGraphDlg::_MinColor != 0 ) m_MinColor = CGraphDlg::_MinColor;
	else m_MinColor = RGB( 255, 128, 128 );
	if( CGraphDlg::_MaxColor != 0 ) m_MaxColor = CGraphDlg::_MaxColor;
	else m_MaxColor = RGB( 0, 0, 255 );

	// Determine size, location differences for later use in resizing
	// ...the constant numbers below represent the pixel distance of
	// ...the edges of the graph window from the edge of the dialog window
	RECT rect, rect2;
	m_pGraph = (CWnd*)GetDlgItem( IDC_GRAPH );
	m_pGraph->GetWindowRect( &rect );
	GetWindowRect( &rect2 );
	m_nToolbar = rect.top - rect2.top;
	m_nRightBuffer = ( ( rect2.right - rect2.left ) - ( rect.right - rect.left ) ) - 4;
	m_nBottomBuffer = ( ( rect2.bottom - rect2.top ) - ( rect.bottom - rect.top ) ) - 4;
	ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	m_nToolbar -= m_nTop;
	m_nBottomBuffer -= m_nToolbar;
	pWnd = GetDlgItem( IDC_TXT_ERR );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		ScreenToClient( &rect );
		m_nErrTxt = rect.left;
	}
	pWnd = GetDlgItem( IDC_BN_ERR );
	if( pWnd )
	{
		pWnd->GetWindowRect( &rect );
		ScreenToClient( &rect );
		m_nErrBn = rect.left;
	}

	// action menu
	m_mnuGo.LoadMenu( IDR_GRAPH_ACTION );
	m_bnGo.m_hMenu = m_mnuGo.GetSubMenu( 0 )->GetSafeHmenu();
	m_mnuGo.CheckMenuItem( IDC_CHK_ANN, MF_BYCOMMAND | m_fAnnValues ? MF_CHECKED : MF_UNCHECKED );
	m_mnuGo.CheckMenuItem( IDC_CHK_SUP, MF_BYCOMMAND | m_fSupressAnn ? MF_CHECKED : MF_UNCHECKED );
	m_mnuGo.CheckMenuItem( IDC_CHK_MONOCHROME, MF_BYCOMMAND | m_fMonochrome ? MF_CHECKED : MF_UNCHECKED );
	m_mnuGo.CheckMenuItem( IDC_CHK_PROPORTIONAL, MF_BYCOMMAND | m_fProportional ? MF_CHECKED : MF_UNCHECKED );
	m_mnuGo.CheckMenuItem( IDC_CHK_INVERT, MF_BYCOMMAND | m_fInverted ? MF_CHECKED : MF_UNCHECKED );
	m_mnuGo.CheckMenuItem( IDM_THICKNESS2, MF_BYCOMMAND | MF_CHECKED );

	if( m_fOverlay ) RemoveViewMenu();
	m_bnGo.m_bOSMenu = FALSE;

	// enabling/disabling
	POSITION pos = NULL;
	CString szTemp;
	switch( m_nChart )
	{
		case CHART_HWR: case CHART_HWRRT: case CHART_HWR3D:
		{
			CMenu* pSubMenu = m_mnuGo.GetSubMenu( 0 );
			if( pSubMenu )
			{
				// delete annotations menu
				pSubMenu->DeleteMenu( m_fOverlay ? GMENU_ANNOTATIONS - 1 : GMENU_ANNOTATIONS, MF_BYPOSITION );
				// delete feedback menu
				pSubMenu->DeleteMenu( m_fOverlay ? GMENU_FEEDBACK - 1 : GMENU_FEEDBACK, MF_BYPOSITION );
			}
			if( m_nChart != CHART_HWR3D ) m_mnuGo.DeleteMenu( IDC_CHK_INVERT, MF_BYCOMMAND );

			m_cbo.Clear();
			m_cboX.Clear();
			m_cboX.EnableWindow( FALSE );
			m_cboY.Clear();
			m_cboY.EnableWindow( FALSE );
			m_cboFeature.Clear();
			m_cboFeature.EnableWindow( FALSE );
			if( m_nChart == CHART_HWR )
			{
				// axes
				int nChartID = 0;
				pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_NONE, _T("X coord."), _T("x(cm)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_NONE, _T("Y coord."), _T("y(cm)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_NONE, _T("Axial Press."), _T("z") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_NONE, _T("X and Y") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_NONE, _T("Sample #") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );

				// graphs
				nChartID = 0;
				GraphSelection* pSel = NULL;
				pSel = new GraphSelection( nChartID++, (int)eHWR_XY, _T("Y coord. vs X coord."),
					GetAxis( _T("X") ), GetAxis( _T("Y") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eHWR_XZ, _T("Z coord. vs X coord."),
					GetAxis( _T("X") ), GetAxis( _T("Z") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eHWR_YZ, _T("Z coord. vs Y coord."),
					GetAxis( _T("Y") ), GetAxis( _T("Z") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eHWR_XYt, _T("X and Y vs Sample #"),
					GetAxis( _T("Sample #") ), GetAxis( _T("X and Y") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eHWR_Zt, _T("Z coord. vs Sample #"),
					GetAxis( _T("Sample #") ), GetAxis( _T("Z") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );

				if( fGripper )
				{
					m_nCurChart = 3;
					m_cbo.SetCurSel( 3 );
				}
				else m_cbo.SetCurSel( 0 );
				m_cboX.SetCurSel( 0 );
				m_cboY.SetCurSel( 1 );

				m_mnuGo.DeleteMenu( IDC_BN_REPLAY, MF_BYCOMMAND );
			}
			else if( m_nChart == CHART_HWR3D )
			{
				// axes
				pos = m_lstAxes.AddTail( new GraphAxis( 0, (int)eTF_COL_NONE, _T("X coord."), _T("x(cm)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				pos = m_lstAxes.AddTail( new GraphAxis( 1, (int)eTF_COL_NONE, _T("Y coord."), _T("y(cm)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );

				// graphs
				GraphSelection* pSel = NULL;
				pSel = new GraphSelection( 0, (int)eHWR_XY, _T("Y coord. vs X coord."),
					GetAxis( _T("X") ), GetAxis( _T("Y") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );

				m_cbo.SetCurSel( 0 );
				m_cboX.SetCurSel( 0 );
				m_cboY.SetCurSel( 1 );

				pWnd = GetDlgItem( IDC_BN_PREV );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_BN_NEXT );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				m_mnuGo.DeleteMenu( IDC_BN_REPLAY, MF_BYCOMMAND );
				m_mnuGo.DeleteMenu( IDC_CHK_PROPORTIONAL, MF_BYCOMMAND );
				m_mnuGo.DeleteMenu( IDC_CHK_SUP, MF_BYCOMMAND );
			}
			else
			{
				m_mnuGo.DeleteMenu( IDC_CHK_INVERT, MF_BYCOMMAND );
				pWnd = GetDlgItem( IDC_BN_PREV );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_BN_NEXT );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_CBO_GRAPH );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				m_mnuGo.DeleteMenu( IDC_CHK_MONOCHROME, MF_BYCOMMAND );
				CMenu* pSubMenu = m_mnuGo.GetSubMenu( 0 );
				if( pSubMenu ) pSubMenu->DeleteMenu( GMENU_THICKNESS, MF_BYPOSITION );
			}
			break;
		}		
		case CHART_TF: case CHART_TFRT: case CHART_TF3D:
		{
			m_cbo.Clear();
			m_cboX.Clear();
			m_cboY.Clear();
// 29Jan09: GMB: We want the real-time chart to have axes & feedback as well
//			if( ( m_nChart == CHART_TF ) || ( m_nChart == CHART_TF3D ) )
			{
				int nChartID = 0;
				// axes
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_X, _T("X coord."), _T("x=LowerGrip(N)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_X, _T("X coord."), _T("x(cm)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_Y, _T("Y coord."), _T("y=UpperGrip(N)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_Y, _T("Y coord."), _T("y(cm)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_Z, _T("Z coord."), _T("z=Lift(N)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_Z, _T("Axial press."), _T("z") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_VX, _T("X vel."), _T("vx(N/s)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_VX, _T("X vel."), _T("vx(cm/s)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_VY, _T("Y vel."), _T("vy(N/s)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_VY, _T("Y vel."), _T("vy(cm/s)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_VZ, _T("Z vel."), _T("vz(N/s)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_VZ, _T("Abs. vel."), _T("vabs(cm/s)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_AX, _T("X accel."), _T("ax(N/s**2)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_AX, _T("X accel."), _T("ax(cm/s**2)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_AY, _T("Y accel."), _T("ay(N/s**2)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_AY, _T("Y accel."), _T("ay(cm/s**2)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_AZ, _T("Z accel."), _T("az(N/s**2)") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_AZ, _T("Abs. accel."), _T("aabs(cm/s**2)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_JX, _T("X jerk") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_JX, _T("X jerk"), _T("jx(cm/s**3)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_JY, _T("Y jerk") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_JY, _T("Y jerk"), _T("jy(cm/s**3)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( fGripper )
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_JY, _T("Abs. jerk") ) );
				else
					pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_JY, _T("Abs. jerk"), _T("jabs(cm/s**3)") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				if( pos ) m_cboY.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );
				pos = m_lstAxes.AddTail( new GraphAxis( nChartID++, (int)eTF_COL_Time, _T("Time") ) );
				if( pos ) m_cboX.AddString( ((GraphAxis*)(m_lstAxes.GetAt( pos )))->m_szLabel );

				// graphs
				nChartID = 0;
				GraphSelection* pSel = NULL;
				pSel = new GraphSelection( nChartID++, (int)eTF_XY, _T("Y coord. vs X coord."),
										   GetAxis( _T("X coord.") ), GetAxis( _T("Y coord.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				if( fGripper )
				{
					pSel = new GraphSelection( nChartID++, (int)eTF_XZ, _T("Z coord. vs X coord."),
						GetAxis( _T("X coord.") ), GetAxis( _T("Z coord.") ) );
					if( pSel ) m_lstGraphs.AddTail( pSel );
					if( pSel ) m_cbo.AddString( pSel->m_szLabel );
					pSel = new GraphSelection( nChartID++, (int)eTF_YZ, _T("Z coord. vs Y coord."),
						GetAxis( _T("Y coord.") ), GetAxis( _T("Z coord.") ) );
					if( pSel ) m_lstGraphs.AddTail( pSel );
					if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				}
				pSel = new GraphSelection( nChartID++, (int)eTF_Xt, _T("X coord. vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("X coord.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_Yt, _T("Y coord. vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("Y coord.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				if( fGripper )
				{
					pSel = new GraphSelection( nChartID++, (int)eTF_Zt, _T("Z coord. vs Time"),
						GetAxis( _T("Time") ), GetAxis( _T("Z coord.") ) );
				}
				else
				{
					pSel = new GraphSelection( nChartID++, (int)eTF_Zt, _T("Axial press. vs Time"),
						GetAxis( _T("Time") ), GetAxis( _T("Axial press.") ) );
				}
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_vx, _T("X vel. vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("X vel.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_vy, _T("Y vel. vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("Y vel.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				if( fGripper )
				{
					pSel = new GraphSelection( nChartID++, (int)eTF_vz, _T("Z vel. vs Time"),
						GetAxis( _T("Time") ), GetAxis( _T("Z vel.") ) );
				}
				else
				{
					pSel = new GraphSelection( nChartID++, (int)eTF_vz, _T("Abs. vel. vs Time"),
						GetAxis( _T("Time") ), GetAxis( _T("Abs. vel.") ) );
				}
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_ax, _T("X accel. vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("X accel.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_ay, _T("Y accel. vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("Y accel.") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				if( fGripper )
				{
					pSel = new GraphSelection( nChartID++, (int)eTF_az, _T("Z accel. vs Time"),
						GetAxis( _T("Time") ), GetAxis( _T("Z accel.") ) );
				}
 				else
 				{
 					pSel = new GraphSelection( nChartID++, (int)eTF_az, _T("Abs. accel. vs Time"),
 						GetAxis( _T("Time") ), GetAxis( _T("Abs. accel.") ) );
 				}
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_JXt, _T("X jerk vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("X jerk") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_JYt, _T("Y jerk vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("Y jerk") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );
				pSel = new GraphSelection( nChartID++, (int)eTF_JABSt, _T("Abs. jerk vs Time"),
					GetAxis( _T("Time") ), GetAxis( _T("Abs. jerk") ) );
				if( pSel ) m_lstGraphs.AddTail( pSel );
				if( pSel ) m_cbo.AddString( pSel->m_szLabel );

				if( m_nChart != CHART_TF3D )
				{
					if( fGripper )
					{
						pSel = new GraphSelection( nChartID++, (int)eTF_aspec, _T("Lower Grip Spectrum"), NULL, NULL );
						if( pSel ) m_lstGraphs.AddTail( pSel );
						if( pSel ) m_cbo.AddString( pSel->m_szLabel );
						pSel = new GraphSelection( nChartID++, (int)eTF_vaspec, _T("Upper Grip Spectrum"), NULL, NULL );
						if( pSel ) m_lstGraphs.AddTail( pSel );
						if( pSel ) m_cbo.AddString( pSel->m_szLabel );
						pSel = new GraphSelection( nChartID++, (int)eTF_aaspec, _T("Lift Spectrum"), NULL, NULL );
						if( pSel ) m_lstGraphs.AddTail( pSel );
						if( pSel ) m_cbo.AddString( pSel->m_szLabel );
					}
					else
					{
						pSel = new GraphSelection( nChartID++, (int)eTF_aspec, _T("Pos. Spectrum"), NULL, NULL );
						if( pSel ) m_lstGraphs.AddTail( pSel );
						if( pSel ) m_cbo.AddString( pSel->m_szLabel );
						pSel = new GraphSelection( nChartID++, (int)eTF_vaspec, _T("Vel. Spectrum"), NULL, NULL );
						if( pSel ) m_lstGraphs.AddTail( pSel );
						if( pSel ) m_cbo.AddString( pSel->m_szLabel );
						pSel = new GraphSelection( nChartID++, (int)eTF_aaspec, _T("Accel. Spectrum"), NULL, NULL );
						if( pSel ) m_lstGraphs.AddTail( pSel );
						if( pSel ) m_cbo.AddString( pSel->m_szLabel );
					}
				}
				else nNumTFGraphs -= 3;

				m_cbo.SetCurSel( 0 );
				m_cboX.SetCurSel( 0 );
				m_cboY.SetCurSel( 1 );
			}
			if( m_nChart == CHART_TFRT )
			{
				pWnd = GetDlgItem( IDC_BN_PREV );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_BN_NEXT );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_CBO_GRAPH );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_CBO_X );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				pWnd = GetDlgItem( IDC_CBO_Y );
				if( pWnd ) pWnd->EnableWindow( FALSE );
				CMenu* pSubMenu = m_mnuGo.GetSubMenu( 0 );
				if( pSubMenu )
				{
					pSubMenu->DeleteMenu( GMENU_ANNOTATIONS, MF_BYPOSITION );
 					pSubMenu->DeleteMenu( GMENU_THICKNESS, MF_BYPOSITION );
				}
				m_mnuGo.DeleteMenu( IDC_CHK_MONOCHROME, MF_BYCOMMAND );
				m_mnuGo.DeleteMenu( IDC_CHK_SUP, MF_BYCOMMAND );
			}
			if( m_nChart != CHART_TF3D ) m_mnuGo.DeleteMenu( IDC_CHK_INVERT, MF_BYCOMMAND );
			// only delete replay if not real-time
			if( m_nChart != CHART_TFRT ) m_mnuGo.DeleteMenu( IDC_BN_REPLAY, MF_BYCOMMAND );
			break;
		}
		default:
			break;
	}

	// if test file, update accordingly
	CString szTmp = m_szFile;
	szTmp.MakeUpper();
	int a = m_szFile.ReverseFind( '\\' );
	if( a != -1 ) szTmp = szTmp.Mid( a + 1 );
	if( ( szTmp == _T("TEST.HWR") ) || ( szTmp == _T("TEST.FRD") ) )
	{
		szTemp = ("Test Results");
		SetWindowText( szTemp );
		pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	else UpdateWindowTitle();

	// remove sub-movement annotation symbol menu if not sub-movement experiment
	if( !m_fSMA )
	{
		CMenu* pSubMenu = m_mnuGo.GetSubMenu( 0 );
		CMenu* pSubSubMenu = pSubMenu ? pSubMenu->GetSubMenu( GMENU_ANNOTATIONS ) : NULL;
		if( pSubSubMenu ) pSubSubMenu->DeleteMenu( GMENU_SUBMVNT, MF_BYPOSITION );
	}

	// FILL FEEDBACK FEATURE LIST
	CStringList lstFeatures;
	CString szFeature;
	pos = m_lstAxes.GetHeadPosition();
	while( pos )
	{
		GraphAxis* pA = (GraphAxis*)m_lstAxes.GetNext( pos );
		if( pA && ( pA->m_szTFLabel != _T("") ) )
		{
			lstFeatures.AddTail( pA->m_szLabel );
			if( pA->m_szTFLabel == m_szFeature ) szFeature = pA->m_szLabel;
		}
	}
	m_szFeature = szFeature;

	// fill & select
	m_cboFeature.AddString( _T("NONE") );
	pos = lstFeatures.GetHeadPosition();
	while( pos )
	{
		szTemp = lstFeatures.GetNext( pos );
		m_cboFeature.AddString( szTemp );
	}
	if( m_szFeature == _T("") )
	{
		if( CGraphDlg::_szFeature != _T("") ) m_szFeature = CGraphDlg::_szFeature;
		else m_szFeature = _T("Y vel.");
	}
	if( m_szFeature != _T("") )
	{
		int nFeature = m_cboFeature.FindString( -1, m_szFeature );
		CGraphDlg::_szFeature = m_szFeature;
		m_cboFeature.SetCurSel( nFeature );
		m_nFeature = nFeature + TF_MENU_BASE + 1;
	}
	else m_cboFeature.SetCurSel( 0 );
	m_fFeedback = TRUE;

	// add feedback time functions to menu
	CMenu* pSMenu = m_mnuGo.GetSubMenu( 0 );
	CMenu* pFBMenu = pSMenu ? pSMenu->GetSubMenu( ( m_nChart == CHART_TFRT ) ? GMENU_FEEDBACK - 1 : GMENU_FEEDBACK ) : NULL;
	CMenu* pTFMenu = pFBMenu ? pFBMenu->GetSubMenu( GMENU_TF ) : NULL;
	if( pTFMenu )
	{
		UINT nItem = 1;
		pos = lstFeatures.GetHeadPosition();
		while( pos )
		{
			szTemp = lstFeatures.GetNext( pos );
			nItem++;
			UINT nID = TF_MENU_BASE + nItem;
			UINT nFlags = MF_STRING;
			if( szTemp == m_szFeature ) nFlags |= MF_CHECKED;
			pTFMenu->AppendMenu( nFlags, nID, szTemp );
		}
	}

	// show chart
	MoveNext();

	// show chart data
	ShowChartData();

	// button images
	m_bnPrev.SetImage( IDB_UP );
	m_bnNext.SetImage( IDB_DOWN );
	m_bnPrevT.SetImage( IDB_LEFT );
	m_bnNextT.SetImage( IDB_RIGHT );
//	m_bnErr.SetImage( IDB_DATA );

	EnableVisualManagerStyle( TRUE, TRUE );

	return FALSE;

//	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGraphDlg::RemoveViewMenu()
{
	CMenu* pSubMenu = m_mnuGo.GetSubMenu( 0 );
	if( pSubMenu ) pSubMenu->DeleteMenu( GMENU_VIEW, MF_BYPOSITION );
	m_mnuGo.DeleteMenu( IDM_T_VIEW, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDM_T_VIEW_TF, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDM_T_VIEW_SEG, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDC_BN_DATAEXT, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDM_T_VIEW_CON, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDM_T_VIEW_ERR, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDM_T_VIEW_DIS, MF_BYCOMMAND );
	m_mnuGo.DeleteMenu( IDM_T_VIEW_WRD, MF_BYCOMMAND );
}

//************************************
// Method:    OnSetFocus
// FullName:  CGraphDlg::OnSetFocus
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pOldWnd
//************************************
void CGraphDlg::OnSetFocus( CWnd* pOldWnd )
{
	_pActiveChart = this;
	NotifyOfTrial();
}

void CGraphDlg::OnActivate( UINT nState, CWnd *pWndOther, BOOL bMinimized )
{
	if( ( nState == WA_ACTIVE ) || ( nState == WA_CLICKACTIVE ) )
	{
		_pActiveChart = this;
		NotifyOfTrial();
	}
}

//************************************
// Method:    PreTranslateMessage
// FullName:  CGraphDlg::PreTranslateMessage
// Access:    virtual public 
// Returns:   BOOL
// Qualifier:
// Parameter: MSG * pMsg
//************************************
BOOL CGraphDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

//************************************
// Method:    OnSize
// FullName:  CGraphDlg::OnSize
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nType
// Parameter: int cx
// Parameter: int cy
//************************************
void CGraphDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	// Hack...for some strage reaons, sometimes we lose the handle to HWND
	m_pGraph = m_pWndOther ? m_pWndOther : (CWnd*)GetDlgItem( IDC_GRAPH );

	if( m_pGraph && m_pGraph->GetSafeHwnd() )
	{
		m_pGraph->SetWindowPos( NULL, m_nLeft + 1, m_nTop + 1,
								cx - m_nRightBuffer + 4,
								cy - m_nBottomBuffer - 1,
								SWP_NOOWNERZORDER | SWP_NOZORDER );
		if( m_hPE )
		{
			::SetWindowPos( m_hPE, NULL, m_nLeft + 1, m_nTop + 1,
							cx - m_nRightBuffer + 4,
							cy - m_nBottomBuffer - 1,
							SWP_NOOWNERZORDER | SWP_NOZORDER );
		}
	}

	CRect rect;

	CWnd* pBn = GetDlgItem( IDC_BN_GO );
	if( pBn )
	{
		pBn->GetWindowRect( rect );
		pBn->MoveWindow( cx - rect.Width() - 4, 15, rect.Width(),
						 rect.Height(), TRUE );
	}
	pBn = GetDlgItem( IDC_TXT_ERR );
	if( pBn )
	{
		pBn->GetWindowRect( rect );
		pBn->MoveWindow( m_nErrTxt, cy - m_nToolbar - 1,
						 cx - m_nRightBuffer - 26, rect.Height(), TRUE );
	}
	pBn = GetDlgItem( IDC_BN_ERR );
	if( pBn )
	{
 		pBn->GetWindowRect( rect );
		pBn->MoveWindow( m_nErrTxt - rect.Width() - 4, cy - m_nToolbar - 1,
						 rect.Width(), rect.Height(), TRUE );
	}

	Invalidate();

	// in case proportional is checked
	int nChart = m_nCurChart - 1;
	if( ( nChart >= 0 ) && m_fProportional )
	{
		m_nCurChart--;
		m_fOptionChanged = TRUE;
		MoveNext();
	}
}

//************************************
// Method:    OnOK
// FullName:  CGraphDlg::OnOK
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnOK()
{
	OnCancel();
}

//************************************
// Method:    OnCancel
// FullName:  CGraphDlg::OnCancel
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnCancel()
{
	if( m_fModeless ) DestroyWindow();
	else CBCGPDialog::OnCancel();
}

//************************************
// Method:    PostNcDestroy
// FullName:  CGraphDlg::PostNcDestroy
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::PostNcDestroy()
{
	if( !m_fModeless ) CBCGPDialog::PostNcDestroy();
	else
	{
		if( m_pWM ) m_pWM->RemoveItem( this );
		delete this;
	}
}

//************************************
// Method:    GetShowHWRObject
// FullName:  CGraphDlg::GetShowHWRObject
// Access:    protected 
// Returns:   IShowHWR*
// Qualifier:
//************************************
IShowHWR* CGraphDlg::GetShowHWRObject()
{
	if( !m_pShowHWR )
	{
		HRESULT hr = E_FAIL;
		hr = CoCreateInstance( CLSID_ShowHWR, NULL, CLSCTX_ALL,
							   IID_IShowHWR, (LPVOID*)&m_pShowHWR );
		if( FAILED( hr ) )
		{
			CString szMsg;
			szMsg.Format( IDS_HWR_ERR, hr );
			BCGPMessageBox( szMsg );
			return NULL;
		}

		CString szAppPath, szUser;
		::GetDataPath( szAppPath, ::IsPrivateUserOn() );
		::GetCurrentUser( szUser );
		szAppPath += szUser;
		szAppPath += _T("\\Actions.log");
		m_pShowHWR->SetLoggingOn( ::GetLogGraph(), szAppPath.AllocSysString() );
	}

	m_pShowHWR->put_Frequency( m_dFrequency );
	m_pShowHWR->put_MinPenPressure( m_nPenPressure );
	m_pShowHWR->put_HWR_InFile( m_szFile.AllocSysString() );
	m_pShowHWR->UseOptimalScaling( !m_fProportional );
	m_pShowHWR->put_Monochrome( m_fMonochrome );
	m_pShowHWR->PenPressureLineThickness( ::IsLineByPressure() );
	m_pShowHWR->SetMissingDataValue( ::GetMissingDataValue() );

	return m_pShowHWR;
}

//************************************
// Method:    GetShowTFObject
// FullName:  CGraphDlg::GetShowTFObject
// Access:    protected 
// Returns:   IShowTF*
// Qualifier:
//************************************
IShowTF* CGraphDlg::GetShowTFObject()
{
	if( !m_pShowTF )
	{
		HRESULT hr = E_FAIL;
		hr = CoCreateInstance( CLSID_ShowTF, NULL, CLSCTX_ALL,
							   IID_IShowTF, (LPVOID*)&m_pShowTF );
		if( FAILED( hr ) )
		{
			CString szMsg;
			szMsg.Format( IDS_TF_ERR, hr );
			BCGPMessageBox( szMsg );
			return NULL;
		}

		CString szAppPath, szUser;
		::GetDataPath( szAppPath, ::IsPrivateUserOn() );
		::GetCurrentUser( szUser );
		szAppPath += szUser;
		szAppPath += _T("\\Actions.log");
		m_pShowTF->SetLoggingOn( ::GetLogGraph(), szAppPath.AllocSysString() );
	}

	m_pShowTF->put_TF_InFile( m_szFile.AllocSysString() );
	if( m_szFile2 != _T("") )
		m_pShowTF->put_SEG_InFile( m_szFile2.AllocSysString() );
	m_pShowTF->put_Frequency( m_dFrequency );
	m_pShowTF->put_MinPenPressure( m_nPenPressure );
	m_pShowTF->UseOptimalScaling( !m_fProportional );
	m_pShowTF->put_Monochrome( m_fMonochrome );
	m_pShowTF->put_SupressAnn( m_fSupressAnn );
	m_pShowTF->put_Feedback( m_fFeedback );
	GraphAxis* pAxis = GetAxis( m_szFeature );
	if( pAxis && ( pAxis->m_szTFLabel != _T("") ) )
		m_pShowTF->put_Feature( pAxis->m_szTFLabel.AllocSysString() );
	m_pShowTF->put_MinColor( m_MinColor );
	m_pShowTF->put_MaxColor( m_MaxColor );
	if( m_fGraphOnly ) m_pShowTF->put_ShowOnlyGraph( TRUE );
	m_pShowTF->put_RTFrequency( (short)m_dFrequency );
	m_pShowTF->SetMissingDataValue( ::GetMissingDataValue() );

	return m_pShowTF;
}

//************************************
// Method:    UpdateWindowTitle
// FullName:  CGraphDlg::UpdateWindowTitle
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::UpdateWindowTitle()
{
	if( m_pWndOther ) return;

	CString szTitle;
	switch( m_nChart )
	{
		case CHART_HWR: case CHART_HWRRT: case CHART_HWR3D:
		{
			szTitle.LoadString( IDS_HWR_DESC );

			CString szFile2 = m_szFile.Left( m_szFile.GetLength() - 3 );
			szFile2 += _T("unr");
			CFileFind ff;
			if( ff.FindFile( szFile2 ) ) szTitle = _T("Linearity Chart");
			break;
		}
		case CHART_TF: case CHART_TFRT: case CHART_TF3D:
			if( m_szFile2 == _T("") ) szTitle.LoadString( IDS_TF_DESC1 );
			else szTitle.LoadString( IDS_TF_DESC2 );
		default:
			break;
	}

	CString szExt = m_szFile.Right( 3 );
	szExt.MakeUpper();
	BOOL fStimulus = ( szExt == _T("STI") );
	CString szTmp = m_szFile, szTemp;
	szTmp.MakeUpper();
	int a = m_szFile.ReverseFind( '\\' );
	if( a != -1 ) szTmp = szTmp.Mid( a + 1 );
	if( m_fOverlay && ( szTmp.GetLength() >= 12 ) )
	{
		CString szExp = szTmp.Left( 3 );
		CString szGrp = szTmp.Mid( 3, 3 );
		CString szSubj = szTmp.Mid( 6, 3 );
		szTmp.Format( _T(" : exp=%s, grp=%s, subj=%s"), szExp, szGrp, szSubj );
		szTemp += szTmp;
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExp ) ) )
		{
			double dMDV = -1.e6;
			exp.GetMissingDataValue( dMDV );
			::SetMissingDataValue( dMDV );
		}
	}
	else if( ( szTmp != _T("TEST.HWR") ) && ( szTmp != _T("TEST.FRD") ) &&
		( szTmp.GetLength() >= 12 ) && !fStimulus )
	{
		CString szExp = szTmp.Left( 3 );
		CString szGrp = szTmp.Mid( 3, 3 );
		CString szSubj = szTmp.Mid( 6, 3 );
		CString szCond = szTmp.Mid( 9, 3 );
		CString szTrial = szTmp.Mid( 12, 2 );
		szTmp.Format( _T(" : exp=%s, grp=%s, subj=%s, cond=%s, trial=%d"),
					  szExp, szGrp, szSubj, szCond, atoi( szTrial ) );
		szTemp += szTmp;
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExp ) ) )
		{
			double dMDV = -1.e6;
			exp.GetMissingDataValue( dMDV );
			::SetMissingDataValue( dMDV );

			// sub-movement analysis?
			IProcSetting* pPS = NULL;
			HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
											 IID_IProcSetting, (LPVOID*)&pPS );
			if( SUCCEEDED( hr ) )
			{
				pPS->Find( szExp.AllocSysString() );
				IProcSetFlags* pPSF = NULL;
				hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
				if( SUCCEEDED( hr ) )
				{
					pPSF->get_SEG_S( &m_fSMA );
					pPSF->Release();
				}
				pPS->Release();
			}
		}
	}
	else
	{
		szTemp = ("Test Results");
		::SetMissingDataValue( -1.e6 );
	}

	szTitle += szTemp;
	SetWindowText( szTitle );
//	HACK: Since BCG is retarded, I have to fake the system
	RedrawWindow( NULL, NULL, RDW_FRAME | RDW_INVALIDATE | RDW_UPDATENOW );
}

//************************************
// Method:    MoveNext
// FullName:  CGraphDlg::MoveNext
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::MoveNext()
{
	CWaitCursor crs;

	if( m_nChart == CHART_NONE ) return;
	if( !m_pGraph && !m_pWndOther ) return;

	// although this should already be set, for some reason the handle to the window
	// keeps getting set to NULL
	if( !m_pWndOther ) m_pGraph = (CWnd*)GetDlgItem( IDC_GRAPH );

	CWnd* pPrev = m_pWndOther ? NULL : GetDlgItem( IDC_BN_PREV );
	CWnd* pNext = m_pWndOther ? NULL : GetDlgItem( IDC_BN_NEXT );

	if( m_nChart >= CHART_TFRT )
	{
		TFRTChart();
	}
	else if( m_nChart >= CHART_HWRRT )
	{
		HWRRTChart();
	}
	else if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
	{
		int nCurChart = m_nCurChart;
		if( ( m_nCurChart != -1 ) && ( m_nCurChart < nNumTFGraphs ) ) m_nCurChart++;
		if( !m_pWndOther ) m_cbo.SetCurSel( nCurChart );

		GraphSelection* pSel = GetGraph( nCurChart );
		GraphAxis* pAxisX = NULL, *pAxisY = NULL;
		if( pSel )
		{
			pAxisX = pSel->m_pAxisX;
			pAxisY = pSel->m_pAxisY;
		}
		else
		{
			pAxisX = GetAxis( m_cboX.GetCurSel() );
			pAxisY = GetAxis( m_cboY.GetCurSel() );
		}
		m_cboX.SetCurSel( pAxisX ? pAxisX->m_nIndex : -1 );
		m_cboY.SetCurSel( pAxisY ? pAxisY->m_nIndex : -1 );

		if( pNext && pPrev )
		{
			if( pSel )
			{
				if( pSel->m_nID == eTF_XY )
				{
					pNext->EnableWindow( TRUE );
					pPrev->EnableWindow( FALSE );
				}
				else if( ( m_nChart == CHART_TF3D ) && ( pSel->m_nID == eTF_JABSt ) )
				{
					pNext->EnableWindow( FALSE );
					pPrev->EnableWindow( TRUE );
				}
				else if( pSel->m_nID == eTF_aaspec )
				{
					pNext->EnableWindow( FALSE );
					pPrev->EnableWindow( TRUE );
				}
				else
				{
					pNext->EnableWindow( TRUE );
					pPrev->EnableWindow( TRUE );
				}
			}
			else
			{
				pNext->EnableWindow( FALSE );
				pPrev->EnableWindow( FALSE );
			}
		}
		TFChart( nCurChart );
		OnBnError( FALSE );
	}
	else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		int nCurChart = m_nCurChart;
		if( ( m_nCurChart != -1 ) && ( m_nCurChart < NUM_HWR_GRAPHS ) ) m_nCurChart++;
		if( !m_pWndOther ) m_cbo.SetCurSel( nCurChart );

		GraphSelection* pSel = GetGraph( nCurChart );
		GraphAxis* pAxisX = NULL, *pAxisY = NULL;
		if( pSel )
		{
			pAxisX = pSel->m_pAxisX;
			pAxisY = pSel->m_pAxisY;
		}
		else
		{
			pAxisX = GetAxis( m_cboX.GetCurSel() );
			pAxisY = GetAxis( m_cboY.GetCurSel() );
		}
		m_cboX.SetCurSel( pAxisX ? pAxisX->m_nIndex : -1 );
		m_cboY.SetCurSel( pAxisY ? pAxisY->m_nIndex : -1 );

		switch( nCurChart )
		{
			case 0:
				if( pNext && ( m_nChart != CHART_HWR3D ) ) pNext->EnableWindow( TRUE );
				if( pPrev ) pPrev->EnableWindow( FALSE );
				break;
			case 1:	case 3:
				if( pNext ) pNext->EnableWindow( TRUE );
				if( pPrev ) pPrev->EnableWindow( TRUE );
				break;
			case 4:
				if( pNext ) pNext->EnableWindow( FALSE );
				if( pPrev ) pPrev->EnableWindow( TRUE );
				break;
		};
		HWRChart( nCurChart );
	}

	// update window title to reflect new chart
	UpdateWindowTitle();

	// show consis error
	OnBnError( TRUE );
}

//************************************
// Method:    MovePrev
// FullName:  CGraphDlg::MovePrev
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::MovePrev()
{
	CWaitCursor crs;

	if( m_nChart == CHART_NONE ) return;
	if( !m_pGraph && !m_pWndOther ) return;

	// although this should already be set, for some reason the handle to the window
	// keeps getting set to NULL
	if( !m_pWndOther ) m_pGraph = (CWnd*)GetDlgItem( IDC_GRAPH );

	CWnd* pPrev = m_pWndOther ? NULL : GetDlgItem( IDC_BN_PREV );
	CWnd* pNext = m_pWndOther ? NULL : GetDlgItem( IDC_BN_NEXT );

	if( m_nChart >= CHART_TFRT )
	{
		TFRTChart();
	}
	else if( m_nChart >= CHART_HWRRT )
	{
		HWRRTChart();
	}
	else if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
	{
		int nCurChart = m_nCurChart;
		if( ( m_nCurChart != -1 ) && ( m_nCurChart > 1 ) ) m_nCurChart--;
		if( !m_pWndOther ) m_cbo.SetCurSel( ( m_nCurChart > 0 ) ? m_nCurChart - 1 : 0 );

		GraphSelection* pSel = GetGraph( ( m_nCurChart > 0 ) ? m_nCurChart - 1 : 0 );
		GraphAxis* pAxisX = NULL, *pAxisY = NULL;
		if( pSel )
		{
			pAxisX = pSel->m_pAxisX;
			pAxisY = pSel->m_pAxisY;
		}
		else
		{
			pAxisX = GetAxis( m_cboX.GetCurSel() );
			pAxisY = GetAxis( m_cboY.GetCurSel() );
		}
		m_cboX.SetCurSel( pAxisX ? pAxisX->m_nIndex : -1 );
		m_cboY.SetCurSel( pAxisY ? pAxisY->m_nIndex : -1 );

		switch( nCurChart )
		{
			case 2:
				if( pPrev ) pPrev->EnableWindow( FALSE );
			case 15: case 16: case 17: case 18:
				if( pNext ) pNext->EnableWindow( TRUE );
				break;
		};
		TFChart( ( m_nCurChart > 0 ) ? m_nCurChart - 1 : 0 );
		OnBnError( FALSE );
	}
	else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		int nCurChart = m_nCurChart;
		if( ( m_nCurChart != -1 ) && ( m_nCurChart > 1 ) ) m_nCurChart--;
		if( !m_pWndOther ) m_cbo.SetCurSel( ( m_nCurChart > 0 ) ? m_nCurChart - 1 : 0 );

		GraphSelection* pSel = GetGraph( ( m_nCurChart > 0 ) ? m_nCurChart - 1 : 0 );
		GraphAxis* pAxisX = NULL, *pAxisY = NULL;
		if( pSel )
		{
			pAxisX = pSel->m_pAxisX;
			pAxisY = pSel->m_pAxisY;
		}
		else
		{
			pAxisX = GetAxis( m_cboX.GetCurSel() );
			pAxisY = GetAxis( m_cboY.GetCurSel() );
		}
		m_cboX.SetCurSel( pAxisX ? pAxisX->m_nIndex : -1 );
		m_cboY.SetCurSel( pAxisY ? pAxisY->m_nIndex : -1 );

		switch( nCurChart )
		{
			case 2:
				if( pPrev ) pPrev->EnableWindow( FALSE );
				break;
			case 3: case 5:
				if( pNext ) pNext->EnableWindow( TRUE );
				break;
		};
		HWRChart( ( m_nCurChart > 0 ) ? m_nCurChart - 1 : 0 );
	}

	// update window title to reflect new chart
	UpdateWindowTitle();
}

//************************************
// Method:    HWRChart
// FullName:  CGraphDlg::HWRChart
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: ULONG nChart
//************************************
void CGraphDlg::HWRChart( ULONG nChart )
{
	CString szExt = m_szFile.Right( 3 );
	BOOL fGripper = ( szExt == _T("FRD") );
	GraphSelection* pSel = GetGraph( nChart );
	GraphAxis* pAxisX = NULL, *pAxisY = NULL;
	HWRGraph gt = (HWRGraph)0;
	if( pSel )
	{
		pAxisX = pSel->m_pAxisX;
		pAxisY = pSel->m_pAxisY;
		gt = (HWRGraph)pSel->m_nID;
	}
	else
	{
		pAxisX = GetAxis( m_cboX.GetCurSel() );
		pAxisY = GetAxis( m_cboY.GetCurSel() );
	}

	// proportional?
	m_fProportional = m_fOptionChanged ? m_fProportional : FALSE;
	if( gt == eHWR_XY )
		m_fProportional = m_fOptionChanged ? m_fProportional : TRUE;
	if( fGripper ) m_fProportional = m_fOptionChanged ? m_fProportional : FALSE;

	if( m_nChart == CHART_HWR3D ) gt = (HWRGraph)( eHWR_3D | gt );

	IShowHWR* pShow = GetShowHWRObject();
	if( !pShow ) return;

	HRESULT hr;
	if( m_fChangePenUpColor ) pShow->put_PenUpColor( m_dwPenUpColor );
	if( m_fOverlay && m_pFileList )
	{
		CString szItem;
		POSITION pos = m_pFileList->GetHeadPosition();
		pShow->AddFile( NULL, TRUE );
		szExt.MakeUpper();
		while( pos )
		{
			szItem = m_pFileList->GetNext( pos );
			if( fGripper ) szItem += _T(".FRD");
			else szItem += _T(".HWR");
			pShow->AddFile( szItem.AllocSysString(), FALSE );
		}
		hr = pShow->ShowHWRMultiple( m_pWndOther ? (VARIANT*)m_pWndOther : (VARIANT*)m_pGraph, gt,
									 (VARIANT*)this, (VARIANT*)&m_hPE );
	}
	else
		hr = pShow->ShowHWRGraph( m_pWndOther ? (VARIANT*)m_pWndOther : (VARIANT*)m_pGraph, gt,
								  (VARIANT*)this, (VARIANT*)&m_hPE );

	pShow->SetLineThickness( (VARIANT*)&m_hPE, (short)m_nThickness );

	RECT ScreenRectangle;
	m_pGraph->GetClientRect( &ScreenRectangle );
	::SetWindowPos( m_hPE, NULL, m_nLeft + 1, m_nTop + 1,
					ScreenRectangle.right - ScreenRectangle.left - 2,
					ScreenRectangle.bottom - ScreenRectangle.top - 1,
					SWP_NOOWNERZORDER | SWP_NOZORDER );

	UpdateData( FALSE );
	m_fOptionChanged = FALSE;
}

//************************************
// Method:    TFChart
// FullName:  CGraphDlg::TFChart
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: ULONG nChart
//************************************
void CGraphDlg::TFChart( ULONG nChart )
{
	CString szExt = m_szFile.Right( 3 );
	BOOL fGripper = ( szExt == _T("FRD") );
	GraphSelection* pSel = GetGraph( nChart );
	GraphAxis* pAxisX = NULL, *pAxisY = NULL;
	TFGraph gt = (TFGraph)0;
	if( pSel )
	{
		pAxisX = pSel->m_pAxisX;
		pAxisY = pSel->m_pAxisY;
		gt = (TFGraph)pSel->m_nID;
	}
	else
	{
		pAxisX = GetAxis( m_cboX.GetCurSel() );
		pAxisY = GetAxis( m_cboY.GetCurSel() );
	}

	// proportional?
	m_fProportional = m_fOptionChanged ? m_fProportional : FALSE;
	if( gt == eTF_XY )
		m_fProportional = m_fGraphOnly ? TRUE : m_fOptionChanged ? m_fProportional : TRUE;


	// hide color stuff if aspec chart
	if( m_fFeedback && ( ( gt == eTF_aspec ) || ( gt == eTF_vaspec ) || ( gt == eTF_aaspec ) ) )
	{
		m_mnuGo.EnableMenuItem( IDC_CHK_ANN, MF_GRAYED | MF_BYCOMMAND );
		m_mnuGo.EnableMenuItem( IDC_CHK_SUP, MF_GRAYED | MF_BYCOMMAND );
	}
	else if( m_fFeedback )
	{
		if( m_nChart != CHART_TF3D )
		{
			m_mnuGo.EnableMenuItem( IDC_CHK_ANN, m_fSupressAnn ? MF_GRAYED : MF_ENABLED | MF_BYCOMMAND );
			m_mnuGo.EnableMenuItem( IDC_CHK_SUP, MF_ENABLED | MF_BYCOMMAND );
		}
	}

	if( m_nChart == CHART_TF3D ) gt = (TFGraph)( eTF_3D | gt );
	if( fGripper ) m_fProportional = m_fOptionChanged ? m_fProportional : FALSE;

	IShowTF* pShow = GetShowTFObject();
	if( !pShow ) return;

	if( m_fChangePenUpColor ) pShow->put_PenUpColor( m_dwPenUpColor );

	pShow->SetAnnotationSymbols( m_nSymbolSeg - SYMBOL_START_SEG, m_nSymbolSubMvmt - SYMBOL_START_SUB );

	HRESULT hr = E_FAIL;
	if( !pSel && pAxisX && pAxisY )
	{
		TFAxis xa = (TFAxis)pAxisX->m_nID, ya = (TFAxis)pAxisY->m_nID;
		hr = pShow->ShowTFGraph2( m_pWndOther ? (VARIANT*)m_pWndOther : (VARIANT*)m_pGraph,
								  xa, ya, (VARIANT*)this, m_fAnnValues, (VARIANT*)&m_hPE );
	}
	else
		hr = pShow->ShowTFGraph( m_pWndOther ? (VARIANT*)m_pWndOther : (VARIANT*)m_pGraph,
								 gt, (VARIANT*)this, m_fAnnValues, (VARIANT*)&m_hPE );

	pShow->SetLineThickness( (VARIANT*)&m_hPE, (short)m_nThickness );

	ShowChartData();

	RECT ScreenRectangle;
	m_pGraph->GetClientRect( &ScreenRectangle );
	::SetWindowPos( m_hPE, NULL, m_nLeft + 1, m_nTop + 1,
		ScreenRectangle.right - ScreenRectangle.left - 2,
		ScreenRectangle.bottom - ScreenRectangle.top - 1,
		SWP_NOOWNERZORDER | SWP_NOZORDER );

	UpdateData( FALSE );
	m_fOptionChanged = FALSE;
}

//************************************
// Method:    HWRRTChart
// FullName:  CGraphDlg::HWRRTChart
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::HWRRTChart()
{
	m_fProportional = m_fOptionChanged ? m_fProportional : TRUE;
	UpdateData( FALSE );

	IShowHWR* pShow = GetShowHWRObject();
	if( !pShow ) return;

	_Cnt = 0;
	_CurCnt = 0;

	// Hack...after playing once completely, then hitting
	// replay...we lose the handle to HWND for some reason
	m_pGraph = m_pWndOther ? m_pWndOther : (CWnd*)GetDlgItem( IDC_GRAPH );

	HRESULT hr = pShow->ShowHWRGraphRT( (VARIANT*)m_pGraph, eHWR_XY,
										&_Cnt, (VARIANT*)&m_hPE );
	if( SUCCEEDED( hr ) )
	{
		if( m_fChangePenUpColor ) pShow->put_PenUpColor( m_dwPenUpColor );
		UINT nFreq = (UINT) (( 1.0 / m_dFrequency ) * 1000);
		_Timer = (int)SetTimer( 1, nFreq, NULL );
	}
}

//************************************
// Method:    TFRTChart
// FullName:  CGraphDlg::TFRTChart
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::TFRTChart()
{
	m_fProportional = m_fOptionChanged ? m_fProportional : TRUE;
	UpdateData( FALSE );

	IShowTF* pShow = GetShowTFObject();
	if( !pShow ) return;

	_Cnt = 0;
	_CurCnt = 0;

	// Hack...after playing once completely, then hitting
	// replay...we lose the handle to HWND for some reason
	m_pGraph = m_pWndOther ? m_pWndOther : (CWnd*)GetDlgItem( IDC_GRAPH );

	HRESULT hr = pShow->ShowTFGraphRT( (VARIANT*)m_pGraph, eTF_XY,
										&_Cnt, (VARIANT*)&m_hPE );
	if( SUCCEEDED( hr ) )
	{
		UINT nFreq = (UINT) (( 1.0 / m_dFrequency ) * 1000);
		_Timer = (int)SetTimer( 1, nFreq, NULL );
	}
}

//************************************
// Method:    OnSelchangeCboGraph
// FullName:  CGraphDlg::OnSelchangeCboGraph
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnSelchangeCboGraph() 
{
	int nSel = m_cbo.GetCurSel();
	if( nSel < 0 ) return;
	GraphSelection* pGraph = GetGraph( nSel );
	if( !pGraph ) return;
	GraphAxis* pAxisX = pGraph->m_pAxisX;
	GraphAxis* pAxisY = pGraph->m_pAxisY;

	m_nCurChart = nSel;
	m_cboX.SetCurSel( pAxisX ? pAxisX->m_nIndex : -1 );
	m_cboY.SetCurSel( pAxisY ? pAxisY->m_nIndex : -1 );

	if( ( pGraph->m_nID == eTF_aspec ) || ( pGraph->m_nID == eTF_vaspec ) ||
		( pGraph->m_nID == eTF_aaspec ) )
	{
		m_cboFeature.SetCurSel( -1 );
	}
	else
	{
		int nFeature = m_cboFeature.FindString( -1, m_szFeature );
		m_cboFeature.SetCurSel( nFeature );
	}

	MoveNext();
}

//************************************
// Method:    OnSelchangeCboX
// FullName:  CGraphDlg::OnSelchangeCboX
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnSelchangeCboX() 
{
	int nSel = m_cboX.GetCurSel();
	if( nSel < 0 ) return;
	GraphAxis* pAxisX = GetAxis( nSel );
	if( !pAxisX ) return;
	int nSelY = m_cboY.GetCurSel();
	if( nSelY < 0 ) return;
	GraphAxis* pAxisY = GetAxis( nSelY );
	if( !pAxisY ) return;

	GraphSelection* pSel = GetGraph( pAxisX, pAxisY );
	if( pSel ) m_nCurChart = pSel->m_nIndex;
	else m_nCurChart = -1;
	m_cbo.SetCurSel( m_nCurChart );

	MoveNext();
}

//************************************
// Method:    OnSelchangeCboY
// FullName:  CGraphDlg::OnSelchangeCboY
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnSelchangeCboY() 
{
	int nSel = m_cboY.GetCurSel();
	if( nSel < 0 ) return;
	GraphAxis* pAxisY = GetAxis( nSel );
	if( !pAxisY ) return;
	int nSelX = m_cboX.GetCurSel();
	if( nSelX < 0 ) return;
	GraphAxis* pAxisX = GetAxis( nSelX );
	if( !pAxisX ) return;

	GraphSelection* pSel = GetGraph( pAxisX, pAxisY );
	if( pSel ) m_nCurChart = pSel->m_nIndex;
	else m_nCurChart = -1;
	m_cbo.SetCurSel( m_nCurChart );

	MoveNext();
}

//************************************
// Method:    OnSelchangeCboFeature
// FullName:  CGraphDlg::OnSelchangeCboFeature
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnSelchangeCboFeature()
{
	// if spectral graph, we do nothing
	if( m_nCurChart != -1 )
	{
		GraphSelection* pGraph = GetGraph( m_nCurChart );
		if( !pGraph ) return;
		if( ( pGraph->m_nID == eTF_aspec ) || ( pGraph->m_nID == eTF_vaspec ) ||
			( pGraph->m_nID == eTF_aaspec ) )
		{
			m_cboFeature.SetCurSel( -1 );
			BCGPMessageBox( _T("Feedback is not used for spectral charts.") );
			return;
		}
	}

	int nSel = m_cboFeature.GetCurSel();
	if( nSel < 0 )
	{
		m_fFeedback = FALSE;
		return;
	}
	m_nFeature = nSel + TF_MENU_BASE + 1;
	m_cboFeature.GetLBText( nSel, m_szFeature );
	CGraphDlg::_szFeature = m_szFeature;
	m_fFeedback = ( m_szFeature != _T("NONE") );

	m_nCurChart--;
	MoveNext();
}

//************************************
// Method:    PrevTrial
// FullName:  CGraphDlg::PrevTrial
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::PrevTrial()
{
	if( !m_fMultiple || !m_pFileList || !m_posCur ) return;

	CString szExt = m_szFile.Right( 3 );
	szExt.MakeUpper();
	BOOL fGripper = ( szExt == _T("FRD") );

	POSITION pos = m_posCur;
	// GetNext/GetPrev always increment POSITION to next point
	// ...we will reset member var (m_posCur) to position of current chart
	// ...this means that we must do GetNext/GetPrev 2x
	m_szFile = m_pFileList->GetPrev( pos );	// This actually returns the current chart
	if( !pos ) return;						// Check to be sure we weren't at first one
	m_szFile = m_pFileList->GetPrev( pos );
	m_posCur = m_pFileList->Find( m_szFile );	// Resetting

	// Add extensions...File 2 for SEG file (if TF charts)
	m_szFile2 = m_szFile;
	if( ( m_nChart == CHART_HWR ) || ( m_nChart == CHART_HWRRT ) || ( m_nChart == CHART_HWR3D ) )
	{
		if( fGripper ) m_szFile += _T(".FRD");
		else m_szFile += _T(".HWR");
	}
	else if( ( m_nChart == CHART_TF ) || ( m_nChart == CHART_TFRT ) || ( m_nChart == CHART_TF3D ) )
		m_szFile += _T(".TF");
	m_szFile2 += _T(".SEG");

	// Enable NextTrial button
	CWnd* pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
	if( pWnd ) pWnd->EnableWindow( TRUE );

	// Disable PrevTrial button if necessary
	if( !pos )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// highlight in left view if movalyzer
	NotifyOfTrial();

	// determine whether this trial has passed consistency checking
	CString szTemp = m_szFile;
	if( m_nChart == CHART_TF )
	{
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T(".HWR");
	}
	m_fPassedConsis = ::HasTrialPassed( szTemp );

	m_nCurChart++;
	MovePrev();
}

//************************************
// Method:    NextTrial
// FullName:  CGraphDlg::NextTrial
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::NextTrial()
{
	if( !m_fMultiple || !m_pFileList || !m_posCur ) return;

	CString szExt = m_szFile.Right( 3 );
	szExt.MakeUpper();
	BOOL fGripper = ( szExt == _T("FRD") );

	POSITION pos = m_posCur;
	// GetNext/GetPrev always increment POSITION to next point
	// ...we will reset member var (m_posCur) to position of current chart
	// ...this means that we must do GetNext/GetPrev 2x
	m_szFile = m_pFileList->GetNext( pos );	// This actually returns the current chart
	if( !pos ) return;						// Check to be sure we weren't at last one
	m_szFile = m_pFileList->GetNext( pos );
	m_posCur = m_pFileList->Find( m_szFile );	// Resetting

	// Add extensions...File 2 for SEG file (if TF charts)
	m_szFile2 = m_szFile;
	if( ( m_nChart == CHART_HWR ) || ( m_nChart == CHART_HWRRT ) || ( m_nChart == CHART_HWR3D ) )
	{
		if( fGripper ) m_szFile += _T(".FRD");
		else m_szFile += _T(".HWR");
	}
	else if( ( m_nChart == CHART_TF ) || ( m_nChart == CHART_TFRT ) || ( m_nChart == CHART_TF3D ) )
		m_szFile += _T(".TF");
	m_szFile2 += _T(".SEG");

	// Enable PrevTrial button
	CWnd* pWnd = GetDlgItem( IDC_BN_PREV_TRIAL );
	if( pWnd ) pWnd->EnableWindow( TRUE );

	// Disable NextTrial button if necessary
	if( !pos )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_NEXT_TRIAL );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// highlight in left view if movalyzer
	NotifyOfTrial();

	CString szTemp = m_szFile;
	if( ( m_nChart == CHART_TF ) || ( m_nChart == CHART_TFRT ) )
	{
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T(".HWR");
	}
	m_fPassedConsis = ::HasTrialPassed( szTemp );

	m_nCurChart--;
	MoveNext();
}

//************************************
// Method:    OnChkAnn
// FullName:  CGraphDlg::OnChkAnn
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnChkAnn()
{
	UpdateData( TRUE );
	m_nCurChart--;
	MoveNext();
}

//************************************
// Method:    OnChkProportional
// FullName:  CGraphDlg::OnChkProportional
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnChkProportional()
{
	UpdateData( TRUE );
	m_nCurChart--;
	m_fOptionChanged = TRUE;
	MoveNext();
}

//************************************
// Method:    OnChkMonochrome
// FullName:  CGraphDlg::OnChkMonochrome
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnChkMonochrome()
{
	UpdateData( TRUE );

	if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;
		GraphSelection* pSel = GetGraph( m_nCurChart - 1 );
		if( !pSel ) return;

		// graph type
		TFGraph gt = (TFGraph)pSel->m_nID;
		if( m_nChart == CHART_TF3D ) gt = (TFGraph)(eTF_3D | gt);

		HRESULT hr = pShow->SetMonochrome( (VARIANT*)&m_hPE, gt, m_fMonochrome );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting monochorome.") );
	}
	else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		IShowHWR* pShow = GetShowHWRObject();
		if( !pShow ) return;

		HWRGraph gt = (HWRGraph)0;
		switch( m_nCurChart  + CHART_HWR_BASE - 1 )
		{
			case CHART_HWR_XY: gt = eHWR_XY; break;
			case CHART_HWR_XZ: gt = eHWR_XZ; break;
			case CHART_HWR_YZ: gt = eHWR_YZ; break;
			case CHART_HWR_XYt: gt = eHWR_XYt; break;
			case CHART_HWR_Zt: gt = eHWR_Zt; break;
		}
		if( m_nChart == CHART_HWR3D ) gt = (HWRGraph)(eHWR_3D | gt);

		HRESULT hr = pShow->SetMonochrome( (VARIANT*)&m_hPE, gt, m_fOverlay, m_fMonochrome );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting monochorome.") );
	}
}

//************************************
// Method:    OnChkSupress
// FullName:  CGraphDlg::OnChkSupress
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnChkSupress()
{
	UpdateData( TRUE );
	m_nCurChart--;
	MoveNext();
}

//************************************
// Method:    OnTimer
// FullName:  CGraphDlg::OnTimer
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nIDEvent
//************************************
void CGraphDlg::OnTimer(UINT nIDEvent) 
{
	if( _CurCnt > _Cnt )
	{
		KillTimer( _Timer );
		_Timer = 0;

		// notify of completion if applicable
		if( m_pWndOther && m_pWndNotify ) m_pWndNotify->PostMessage( GM_REALTIMECOMPLETE );

		return;
	}

	if( m_nChart == CHART_TFRT )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;
		HRESULT hr = pShow->OnTimer( (VARIANT*)&m_hPE );
	}
	else
	{
		IShowHWR* pShow = GetShowHWRObject();
		if( !pShow ) return;
		HRESULT hr = pShow->OnTimer( (VARIANT*)&m_hPE );
	}
	_CurCnt++;
}

//************************************
// Method:    OnBnReplay
// FullName:  CGraphDlg::OnBnReplay
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnBnReplay()
{
	if( m_nChart == CHART_HWRRT ) HWRRTChart();
	else TFRTChart();
}

//************************************
// Method:    OnBnPrint
// FullName:  CGraphDlg::OnBnPrint
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnBnPrint()
{
	CWaitCursor crs;

	if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;
		HRESULT hr = pShow->PrintGraph( (VARIANT*)&m_hPE );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in printing.") );
	}
	else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		IShowHWR* pShow = GetShowHWRObject();
		if( !pShow ) return;
		HRESULT hr = pShow->PrintGraph( (VARIANT*)&m_hPE );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in printing.") );
	}
}

//************************************
// Method:    OnHelpInfo
// FullName:  CGraphDlg::OnHelpInfo
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: HELPINFO * pHelpInfo
//************************************
BOOL CGraphDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("chartingtrials.html"), this );
	PostMessage( WM_SETFOCUS, 0, 0 );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

//************************************
// Method:    OnBnData
// FullName:  CGraphDlg::OnBnData
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fTF
// Parameter: BOOL fSeg
// Parameter: BOOL fDis
// Parameter: BOOL fWrd
//************************************
void CGraphDlg::OnBnData( BOOL fTF, BOOL fSeg, BOOL fDis, BOOL fWrd )
{
	CFileFind ff;
	CString szFile;

	// add appropriate extension
	int nPos = m_szFile.ReverseFind( '.' );
	if( nPos >= 0 ) szFile = m_szFile.Left( nPos + 1 );
	else szFile = m_szFile;

	if( fWrd ) szFile += _T("WRD");
	else if( fDis ) szFile += _T("DIS");
	else if( fSeg ) szFile += _T("SEG");
	else if( fTF ) szFile += _T("TF");
	else
	{
		// check to see if it was from HWR or FRD
		CString szTmp = szFile;
		szTmp += _T("FRD");
		if( ff.FindFile( szTmp ) ) szFile += _T("FRD");
		else szFile += _T("HWR");
	}

	// does file actually exist?
	if( !ff.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file is missing (or has not been processed):\n\n%s"), szFile );
		BCGPMessageBox( szMsg );
		return;
	}

	CWaitCursor crs;
	if( fTF || fSeg || fWrd )
	{
// 		CString szExec;
// 		szExec.Format( _T("notepad.exe %s"), szFile );
// 		WinExec( szExec, SW_SHOW );
		InstructionDlg d( this );
		d.m_fJustDisplay = TRUE;
		d.m_szFileName = szFile;
		d.DoModal();
		CWaitCursor crs;
	}
	else
	{
		if( m_pWM )
		{
			DataTabDlg* d = new DataTabDlg( szFile, this, m_pWM, m_nCurChart );
			m_pWM->AddItem( d );
		}
	}
}

//************************************
// Method:    OnCommand
// FullName:  CGraphDlg::OnCommand
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: WPARAM wParam
// Parameter: LPARAM lParam
//************************************
BOOL CGraphDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	if( HIWORD( wParam ) == PEWN_CURSORMOVE )
	{
		::SetCurrentChart( m_szFile );

		LONG nPos = 0;
		short nStroke = -1;

		if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
		{
			TFGraph gt = (TFGraph)0;
			switch( m_nCurChart )
			{
				case 1: gt = eTF_XY; break;
				case 2: gt = eTF_XZ; break;
				case 3: gt = eTF_YZ; break;
				case 4: gt = eTF_Xt; break;
				case 5: gt = eTF_Yt; break;
				case 6: gt = eTF_Zt; break;
				case 7: gt = eTF_vx; break;
				case 8: gt = eTF_vy; break;
				case 9: gt = eTF_vabs; break;
				case 10: gt = eTF_ax; break;
				case 11: gt = eTF_ay; break;
				case 12: gt = eTF_az; break;
				case 13: gt = eTF_JXt; break;
				case 14: gt = eTF_JYt; break;
				case 15: gt = eTF_JABSt; break;
				case 16: gt = eTF_aspec; break;
			};

			IShowTF* pShow = GetShowTFObject();
			if( pShow && m_hPE )
			{
				pShow->OnCursorMove( (VARIANT*)&m_hPE, gt );
				pShow->GetCursorPos( (VARIANT*)&m_hPE, &nPos );
				pShow->GetStroke( &nStroke );
			}
		}
		else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
		{
			HWRGraph gt = (HWRGraph)0;
			switch( m_nChart )
			{
				case 0: gt = eHWR_XY; break;
				case 1: gt = eHWR_XZ; break;
				case 2: gt = eHWR_YZ; break;
				case 3: gt = eHWR_XYt; break;
				case 4: gt = eHWR_Zt; break;
			}

			IShowHWR* pShow = GetShowHWRObject();
			if( pShow && m_hPE )
			{
				pShow->OnCursorMove( (VARIANT*)&m_hPE, gt );
				pShow->GetCursorPos( (VARIANT*)&m_hPE, &nPos );
			}
		}

		if( m_pWM )
		{
			if( !CGraphDlg::m_fDoNotNotify )
			{
				// for raw data
				m_pWM->PostMessage( MAKEWPARAM( MSG_WND_GRAPH, MSG_WND_SUB_CURSOR ),
									MAKELPARAM( nPos, m_fSMA ), this );
			}

			if( CGraphDlg::m_fDoNotNotify ) CGraphDlg::m_fDoNotNotify = FALSE;

			// for extracted data
//			if( nStroke > 0 )
			{
				m_pWM->PostMessage( MAKEWPARAM( MSG_WND_GRAPH, MSG_WND_SUB_CURSOR_EXT ),
									MAKELPARAM( nStroke, m_fSMA ), this );
			}
		}
	}

	return CBCGPDialog::OnCommand( wParam, lParam );
}

//************************************ 
// Method:    OnWMMessage
// FullName:  CGraphDlg::OnWMMessage
// Access:    public 
// Returns:   LRESULT
// Qualifier:
// Parameter: WPARAM wParam
// Parameter: LPARAM lParam
//************************************
LRESULT CGraphDlg::OnWMMessage( WPARAM wParam, LPARAM lParam )
{
	if( m_nCurChart == 16 ) return 0;

	CString szCurChart;
	::GetCurrentChart( szCurChart );
	int nPos = szCurChart.ReverseFind( '.' );
	if( nPos != -1 ) szCurChart = szCurChart.Left( nPos );
	szCurChart.MakeUpper();
	CString szFile = m_szFile;
	nPos = szFile.ReverseFind( '.' );
	if( nPos != -1 ) szFile = szFile.Left( nPos );
	szFile.MakeUpper();
	if( szCurChart != szFile ) return 0;

	if( HIWORD( wParam ) == MSG_WND_GRAPH )
	{
		if( LOWORD( wParam ) == MSG_WND_SUB_CURSOR )
		{
			if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
			{
				IShowTF* pShow = GetShowTFObject();
				if( pShow && m_hPE )
				{
					CGraphDlg::m_fDoNotNotify = TRUE;
					pShow->SetCursorPos( (VARIANT*)&m_hPE, LOWORD( lParam ) );
				}
			}
			else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
			{
				IShowHWR* pShow = GetShowHWRObject();
				if( pShow && m_hPE )
				{
					CGraphDlg::m_fDoNotNotify = TRUE;
					pShow->SetCursorPos( (VARIANT*)&m_hPE, LOWORD( lParam ) );
				}
			}
		}
	}

	return 0;
}

//************************************
// Method:    OnBnError
// FullName:  CGraphDlg::OnBnError
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: BOOL fInit
//************************************
void CGraphDlg::OnBnError( BOOL fInit )
{
	CWaitCursor crs;
	// drop extension and trial # (& get trial #)
	int nPos = m_szFile.ReverseFind( '.' );
	if( nPos == -1 ) return;
	CString szTrial = m_szFile.Mid( nPos - 2, 2 );
	int nTrial = atoi( szTrial );
	if( ( nTrial <= 0 ) || ( nTrial > 99 ) ) return;
	CString szFile = m_szFile.Left( nPos - 2 );
	szFile += _T(".ERR");
	// verify file exists
	CFileFind ff;
	if( !ff.FindFile( szFile ) ) return;

	// open and loop through file to get the LAST error (or OK) for this trial
	CStdioFile f;
	if( !f.Open( szFile, CFile::modeRead ) ) return;
	CString szLine, szErr;
	int nCurTrial = 0;
	while( f.ReadString( szLine ) )
	{
		// 1st is trial #, then space, then message
		nPos = szLine.Find( _T(" ") );
		if( nPos != -1 )
		{
			nCurTrial = atoi( szLine.Left( nPos ) );
			if( nCurTrial == nTrial ) szErr = szLine.Mid( nPos + 1 );
		}
	}

	// close file
	f.Close();

	// display message
	if( szErr == _T("") )
	{
		szErr = _T("ERROR: Trial not found in consistency. Report under SUPPORT at www.neuroscript.net.");
		m_bnErr.SetImage( IDB_TRIALB );
	}
	else if( szErr == _T("OK") )
	{
		szErr = _T("OK");
		m_bnErr.SetImage( IDB_TRIALG );
	}
	else m_bnErr.SetImage( IDB_TRIALB );

	m_szErr = szErr;
	UpdateData( FALSE );
}

//************************************
// Method:    OnBnExport
// FullName:  CGraphDlg::OnBnExport
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnBnExport()
{
	CWaitCursor crs;
	if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;
		HRESULT hr = pShow->ExportGraph( (VARIANT*)&m_hPE );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in exporting.") );
	}
	else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		IShowHWR* pShow = GetShowHWRObject();
		if( !pShow ) return;
		HRESULT hr = pShow->ExportGraph( (VARIANT*)&m_hPE );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in exporting.") );
	}
}

//************************************
// Method:    OnChart
// FullName:  CGraphDlg::OnChart
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: UINT nWhich
//************************************
void CGraphDlg::OnChart( UINT nWhich )
{
	CGraphDlg* pObj = this;
	CString szExt = m_szFile.Right( 3 );
	szExt.MakeUpper();
	BOOL fStimulus = ( szExt == _T("STI") );
	CString szTmp = m_szFile;
	szTmp.MakeUpper();
	int nPos = m_szFile.ReverseFind( '\\' );
	if( nPos != -1 ) szTmp = szTmp.Mid( nPos + 1 );
	CString szExp, szGrp, szSubj;
	if( ( szTmp != _T("TEST.HWR") ) && ( szTmp != _T("TEST.FRD") ) && ( szTmp.GetLength() >= 12 ) && !fStimulus )
	{
		// TODO: (elsewhere in file too) get rid of ID size dependencies
		szExp = szTmp.Left( 3 );
		szGrp = szTmp.Mid( 3, 3 );
		szSubj = szTmp.Mid( 6, 3 );
	}

	// hwr/tf file names (since initial chart could be either)
	CString szHWR, szFRD, szTF;
	nPos = m_szFile.ReverseFind( '.' );
	if( nPos != -1 )
	{
		szHWR = m_szFile.Left( nPos + 1 );
		szHWR += _T("HWR");
		szFRD = m_szFile.Left( nPos + 1 );
		szFRD += _T("FRD");
		szTF = m_szFile.Left( nPos + 1 );
		szTF += _T("TF");
	}

	CFileFind ff;
	if( nWhich == CHART_TF3D )
	{
		if( !ff.FindFile( szTF ) )
		{
			BCGPMessageBox( _T("This chart is not available.\nYou must process this trial first.") );
			return;
		}
		pObj->ChartTf3D( szExp, szGrp, szSubj, szTF, m_dFrequency, m_fPassedConsis );
	}
	else if( nWhich == CHART_HWR3D )
	{
		if( ff.FindFile( szHWR ) )
			pObj->ChartHwr3D( szExp, szGrp, szSubj, szHWR, m_dFrequency );
		else
			pObj->ChartHwr3D( szExp, szGrp, szSubj, szFRD, m_dFrequency );
	}
	else if( nWhich == CHART_TFRT )
	{
		if( !ff.FindFile( szTF ) )
		{
			BCGPMessageBox( _T("This chart is not available.\nYou must process this trial first.") );
			return;
		}
		pObj->ChartTfRT( szExp, szGrp, szSubj, szTF, m_dFrequency );
	}
	else if( nWhich == CHART_HWRRT )
	{
		if( ff.FindFile( szHWR ) )
			pObj->ChartHwrRT( szExp, szGrp, szSubj, szHWR, m_dFrequency );
		else
			pObj->ChartHwrRT( szExp, szGrp, szSubj, szFRD, m_dFrequency );
	}
	else if( nWhich >= CHART_TF )
	{
		if( !ff.FindFile( szTF ) )
		{
			BCGPMessageBox( _T("This chart is not available.\nYou must process this trial first.") );
			return;
		}
		pObj->ChartTf( szExp, szGrp, szSubj, szTF, m_dFrequency, m_nPenPressure, m_fPassedConsis );
	}
	else if( nWhich >= CHART_HWR )
	{
		if( ff.FindFile( szHWR ) )
			pObj->ChartHwr( szExp, szGrp, szSubj, szHWR, m_dFrequency, m_nPenPressure );
		else
			pObj->ChartHwr( szExp, szGrp, szSubj, szFRD, m_dFrequency, m_nPenPressure );
	}
}

//************************************
// Method:    OnBnDataExt
// FullName:  CGraphDlg::OnBnDataExt
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fCon
// Parameter: BOOL fErr
//************************************
void CGraphDlg::OnBnDataExt( BOOL fCon, BOOL fErr )
{
	CString szFile, szLaunch;
	int nPos = m_szFile.ReverseFind( '.' );
	if( nPos == -1 ) return;
	szFile = m_szFile.Left( nPos - 2 );	// + trial #

	if( fErr ) szFile += _T(".ERR");
	else if( fCon ) szFile += _T(".CON");
	else szFile += _T(".EXT");

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file is missing (or has not been processed):\n\n%s"), szFile );
		BCGPMessageBox( szMsg );
		return;
	}

	CWaitCursor crs;
	if( !m_pWM || fErr )
	{
//		szLaunch.Format( _T("notepad.exe %s"), szFile );
//		WinExec( szLaunch, SW_SHOW );
		InstructionDlg d( this );
		d.m_fJustDisplay = TRUE;
		d.m_szFileName = szFile;
		d.DoModal();
		CWaitCursor crs;
	}
	else
	{
		DataTabDlg* d = new DataTabDlg( szFile, this, m_pWM, m_nCurChart );
		m_pWM->AddItem( d );
	}
}

//************************************
// Method:    OnLines
// FullName:  CGraphDlg::OnLines
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: int nThickness
//************************************
void CGraphDlg::OnLines( int nThickness )
{
	if( ( m_nChart == CHART_HWR3D ) && ( nThickness > 2 ) )
		nThickness += 2;
	m_nThickness = nThickness;

	if( m_nChart >= CHART_TF )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;

		HRESULT hr = pShow->SetLineThickness( (VARIANT*)&m_hPE, (short)nThickness );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting line thickness.") );
	}
	else if( m_nChart >= CHART_HWR )
	{
		IShowHWR* pShow = GetShowHWRObject();
		if( !pShow ) return;

		HRESULT hr = pShow->SetLineThickness( (VARIANT*)&m_hPE, (short)nThickness );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting line thickness.") );
	}
}

//************************************
// Method:    ShowChartData
// FullName:  CGraphDlg::ShowChartData
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::ShowChartData()
{
	if( m_nChart >= CHART_TF )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;

		CString szData;
		if( m_fShowChartData )
		{
			CString szSiteID = ::GetCurrentUserSiteID();
			CString szUserID;
			::GetCurrentUser( szUserID );
			COleDateTime dt = COleDateTime::GetCurrentTime();
			CString szDate = dt.Format( "%d%b%y %H:%M" );
			szData.Format( _T("Site=%s User=%s Date=%s"), szSiteID, szUserID, szDate );
		}
		pShow->ShowChartData( (VARIANT*)&m_hPE, szData.AllocSysString() );
	}
}

//************************************
// Method:    OnBnGo
// FullName:  CGraphDlg::OnBnGo
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnBnGo()
{
	switch( m_bnGo.m_nMenuResult )
	{
		case IDM_THICKNESS2:
		case IDM_THICKNESS3:
		case IDM_THICKNESS4:
		case IDM_THICKNESS5:
		case IDM_THICKNESS6:
		case IDM_THICKNESS7:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS2, MF_BYCOMMAND | MF_UNCHECKED );
			m_mnuGo.CheckMenuItem( IDM_THICKNESS3, MF_BYCOMMAND | MF_UNCHECKED );
			m_mnuGo.CheckMenuItem( IDM_THICKNESS4, MF_BYCOMMAND | MF_UNCHECKED );
			m_mnuGo.CheckMenuItem( IDM_THICKNESS5, MF_BYCOMMAND | MF_UNCHECKED );
			m_mnuGo.CheckMenuItem( IDM_THICKNESS6, MF_BYCOMMAND | MF_UNCHECKED );
			m_mnuGo.CheckMenuItem( IDM_THICKNESS7, MF_BYCOMMAND | MF_UNCHECKED );
			break;
		case IDM_CHART_SYMBOL_SEG_0: case IDM_CHART_SYMBOL_SEG_1: case IDM_CHART_SYMBOL_SEG_2:
		case IDM_CHART_SYMBOL_SEG_3: case IDM_CHART_SYMBOL_SEG_4: case IDM_CHART_SYMBOL_SEG_5:
		case IDM_CHART_SYMBOL_SEG_6: case IDM_CHART_SYMBOL_SEG_7: case IDM_CHART_SYMBOL_SEG_8:
		case IDM_CHART_SYMBOL_SEG_9: case IDM_CHART_SYMBOL_SEG_10: case IDM_CHART_SYMBOL_SEG_11:
		case IDM_CHART_SYMBOL_SEG_12: case IDM_CHART_SYMBOL_SEG_13: case IDM_CHART_SYMBOL_SEG_14:
		case IDM_CHART_SYMBOL_SEG_15: case IDM_CHART_SYMBOL_SEG_16: case IDM_CHART_SYMBOL_SEG_17:
		case IDM_CHART_SYMBOL_SEG_18: case IDM_CHART_SYMBOL_SEG_19: case IDM_CHART_SYMBOL_SEG_20:
		case IDM_CHART_SYMBOL_SEG_21: case IDM_CHART_SYMBOL_SEG_22: case IDM_CHART_SYMBOL_SEG_23:
		case IDM_CHART_SYMBOL_SEG_24: case IDM_CHART_SYMBOL_SEG_25: case IDM_CHART_SYMBOL_SEG_26:
		case IDM_CHART_SYMBOL_SEG_27: case IDM_CHART_SYMBOL_SEG_28: case IDM_CHART_SYMBOL_SEG_29:
		case IDM_CHART_SYMBOL_SEG_30: case IDM_CHART_SYMBOL_SEG_31: case IDM_CHART_SYMBOL_SEG_32:
		case IDM_CHART_SYMBOL_SEG_33: case IDM_CHART_SYMBOL_SEG_34: case IDM_CHART_SYMBOL_SEG_35:
		case IDM_CHART_SYMBOL_SEG_36:
		{
			for( int i = 0; i <= 36; i++ )
			{
				m_mnuGo.CheckMenuItem( SYMBOL_START_SEG + i, MF_BYCOMMAND | MF_UNCHECKED );
			}
			break;
		}
		case IDM_CHART_SYMBOL_SUB_0: case IDM_CHART_SYMBOL_SUB_1: case IDM_CHART_SYMBOL_SUB_2:
		case IDM_CHART_SYMBOL_SUB_3: case IDM_CHART_SYMBOL_SUB_4: case IDM_CHART_SYMBOL_SUB_5:
		case IDM_CHART_SYMBOL_SUB_6: case IDM_CHART_SYMBOL_SUB_7: case IDM_CHART_SYMBOL_SUB_8:
		case IDM_CHART_SYMBOL_SUB_9: case IDM_CHART_SYMBOL_SUB_10: case IDM_CHART_SYMBOL_SUB_11:
		case IDM_CHART_SYMBOL_SUB_12: case IDM_CHART_SYMBOL_SUB_13: case IDM_CHART_SYMBOL_SUB_14:
		case IDM_CHART_SYMBOL_SUB_15: case IDM_CHART_SYMBOL_SUB_16: case IDM_CHART_SYMBOL_SUB_17:
		case IDM_CHART_SYMBOL_SUB_18: case IDM_CHART_SYMBOL_SUB_19: case IDM_CHART_SYMBOL_SUB_20:
		case IDM_CHART_SYMBOL_SUB_21: case IDM_CHART_SYMBOL_SUB_22: case IDM_CHART_SYMBOL_SUB_23:
		case IDM_CHART_SYMBOL_SUB_24: case IDM_CHART_SYMBOL_SUB_25: case IDM_CHART_SYMBOL_SUB_26:
		case IDM_CHART_SYMBOL_SUB_27: case IDM_CHART_SYMBOL_SUB_28: case IDM_CHART_SYMBOL_SUB_29:
		case IDM_CHART_SYMBOL_SUB_30: case IDM_CHART_SYMBOL_SUB_31: case IDM_CHART_SYMBOL_SUB_32:
		case IDM_CHART_SYMBOL_SUB_33: case IDM_CHART_SYMBOL_SUB_34: case IDM_CHART_SYMBOL_SUB_35:
		case IDM_CHART_SYMBOL_SUB_36:
		{
			for( int i = 0; i <= 36; i++ )
			{
				m_mnuGo.CheckMenuItem( SYMBOL_START_SUB + i, MF_BYCOMMAND | MF_UNCHECKED );
			}
			break;
		}
	}

	switch( m_bnGo.m_nMenuResult )
	{
		case IDC_CHK_ANN:
			m_fAnnValues = !m_fAnnValues;
			m_mnuGo.CheckMenuItem( IDC_CHK_ANN, MF_BYCOMMAND | m_fAnnValues ? MF_CHECKED : MF_UNCHECKED );
			OnChkAnn();
			break;
		case IDC_CHK_SUP:
			m_fSupressAnn = !m_fSupressAnn;
			m_mnuGo.CheckMenuItem( IDC_CHK_SUP, MF_BYCOMMAND | m_fSupressAnn ? MF_CHECKED : MF_UNCHECKED );
			OnChkSupress();
			break;
		case IDC_CHK_MONOCHROME:
			m_fMonochrome = !m_fMonochrome;
			m_mnuGo.CheckMenuItem( IDC_CHK_MONOCHROME, MF_BYCOMMAND | m_fMonochrome ? MF_CHECKED : MF_UNCHECKED );
			OnChkMonochrome();
			break;
		case IDC_CHK_PROPORTIONAL:
			m_fProportional = !m_fProportional;
			m_mnuGo.CheckMenuItem( IDC_CHK_PROPORTIONAL, MF_BYCOMMAND | m_fProportional ? MF_CHECKED : MF_UNCHECKED );
			OnChkProportional();
			break;
		case IDC_CHK_INVERT:
			m_fInverted = !m_fInverted;
			m_mnuGo.CheckMenuItem( IDC_CHK_INVERT, MF_BYCOMMAND | m_fInverted ? MF_CHECKED : MF_UNCHECKED );
			OnChkInvert();
			break;
		case IDC_BN_DATA: case IDM_T_VIEW:
			OnBnData();
			break;
		case IDM_T_VIEW_TF:
			OnBnData( TRUE );
			break;
		case IDM_T_VIEW_SEG:
			OnBnData( TRUE, TRUE );
			break;
		case IDC_BN_DATAEXT:
			OnBnDataExt();
			break;
		case IDM_T_VIEW_CON:
			OnBnDataExt( TRUE );
			break;
		case IDM_T_VIEW_ERR:
			OnBnDataExt( FALSE, TRUE );
			break;
		case IDM_T_VIEW_DIS:
			OnBnData( FALSE, FALSE, TRUE );
			break;
		case IDM_T_VIEW_WRD:
			OnBnData( FALSE, FALSE, FALSE, TRUE );
			break;
		case IDC_BN_EXPORT:
			OnBnExport();
			break;
		case IDM_THICKNESS2:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS2, MF_BYCOMMAND | MF_CHECKED );
			OnLines( 2 );
			break;
		case IDM_THICKNESS3:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS3, MF_BYCOMMAND | MF_CHECKED );
			OnLines( 3 );
			break;
		case IDM_THICKNESS4:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS4, MF_BYCOMMAND | MF_CHECKED );
			OnLines( 4 );
			break;
		case IDM_THICKNESS5:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS5, MF_BYCOMMAND | MF_CHECKED );
			OnLines( 5 );
			break;
		case IDM_THICKNESS6:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS6, MF_BYCOMMAND | MF_CHECKED );
			OnLines( 6 );
			break;
		case IDM_THICKNESS7:
			m_mnuGo.CheckMenuItem( IDM_THICKNESS7, MF_BYCOMMAND | MF_CHECKED );
			OnLines( 7 );
			break;
		case IDM_CHART_HWR:
			OnChart( CHART_HWR );
			break;
		case IDM_CHART_HWR3D:
			OnChart( CHART_HWR3D );
			break;
		case IDM_CHART_HWRR:
			OnChart( CHART_HWRRT );
			break;
		case IDM_CHART_TF:
			OnChart( CHART_TF );
			break;
		case IDM_CHART_TF3D:
			OnChart( CHART_TF3D );
			break;
		case IDM_CHART_TFR:
			OnChart( CHART_TFRT );
			break;
		case IDC_BN_PRINT:
			OnBnPrint();
			break;
		case IDM_AXIS_CUSTOMIZE:
		{
			CWaitCursor crs;
			if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
			{
				IShowTF* pShow = GetShowTFObject();
				if( pShow ) pShow->Customize( (VARIANT*)&m_hPE );
			}
			else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
			{
				IShowHWR* pShow = GetShowHWRObject();
				if( pShow ) pShow->Customize( (VARIANT*)&m_hPE );
			}
			break;
		}
		case IDM_CHART_SYMBOL_SEG_0: case IDM_CHART_SYMBOL_SEG_1: case IDM_CHART_SYMBOL_SEG_2:
		case IDM_CHART_SYMBOL_SEG_3: case IDM_CHART_SYMBOL_SEG_4: case IDM_CHART_SYMBOL_SEG_5:
		case IDM_CHART_SYMBOL_SEG_6: case IDM_CHART_SYMBOL_SEG_7: case IDM_CHART_SYMBOL_SEG_8:
		case IDM_CHART_SYMBOL_SEG_9: case IDM_CHART_SYMBOL_SEG_10: case IDM_CHART_SYMBOL_SEG_11:
		case IDM_CHART_SYMBOL_SEG_12: case IDM_CHART_SYMBOL_SEG_13: case IDM_CHART_SYMBOL_SEG_14:
		case IDM_CHART_SYMBOL_SEG_15: case IDM_CHART_SYMBOL_SEG_16: case IDM_CHART_SYMBOL_SEG_17:
		case IDM_CHART_SYMBOL_SEG_18: case IDM_CHART_SYMBOL_SEG_19: case IDM_CHART_SYMBOL_SEG_20:
		case IDM_CHART_SYMBOL_SEG_21: case IDM_CHART_SYMBOL_SEG_22: case IDM_CHART_SYMBOL_SEG_23:
		case IDM_CHART_SYMBOL_SEG_24: case IDM_CHART_SYMBOL_SEG_25: case IDM_CHART_SYMBOL_SEG_26:
		case IDM_CHART_SYMBOL_SEG_27: case IDM_CHART_SYMBOL_SEG_28: case IDM_CHART_SYMBOL_SEG_29:
		case IDM_CHART_SYMBOL_SEG_30: case IDM_CHART_SYMBOL_SEG_31: case IDM_CHART_SYMBOL_SEG_32:
		case IDM_CHART_SYMBOL_SEG_33: case IDM_CHART_SYMBOL_SEG_34: case IDM_CHART_SYMBOL_SEG_35:
		case IDM_CHART_SYMBOL_SEG_36:
		{
			m_nSymbolSeg = m_bnGo.m_nMenuResult;
			m_mnuGo.CheckMenuItem( m_nSymbolSeg, MF_BYCOMMAND | MF_CHECKED );
			if( m_nChart >= CHART_TF )
			{
				IShowTF* pShow = GetShowTFObject();
				if( !pShow ) return;

				HRESULT hr = pShow->SetAnnotationSymbols( m_nSymbolSeg, m_nSymbolSubMvmt );
				if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting segmentation symbol.") );

				int nChart = m_nCurChart - 1;
				if( nChart >= 0 )
				{
					m_nCurChart--;
					m_fOptionChanged = TRUE;
					MoveNext();
				}
			}
			break;
		}
		case IDM_CHART_SYMBOL_SUB_0: case IDM_CHART_SYMBOL_SUB_1: case IDM_CHART_SYMBOL_SUB_2:
		case IDM_CHART_SYMBOL_SUB_3: case IDM_CHART_SYMBOL_SUB_4: case IDM_CHART_SYMBOL_SUB_5:
		case IDM_CHART_SYMBOL_SUB_6: case IDM_CHART_SYMBOL_SUB_7: case IDM_CHART_SYMBOL_SUB_8:
		case IDM_CHART_SYMBOL_SUB_9: case IDM_CHART_SYMBOL_SUB_10: case IDM_CHART_SYMBOL_SUB_11:
		case IDM_CHART_SYMBOL_SUB_12: case IDM_CHART_SYMBOL_SUB_13: case IDM_CHART_SYMBOL_SUB_14:
		case IDM_CHART_SYMBOL_SUB_15: case IDM_CHART_SYMBOL_SUB_16: case IDM_CHART_SYMBOL_SUB_17:
		case IDM_CHART_SYMBOL_SUB_18: case IDM_CHART_SYMBOL_SUB_19: case IDM_CHART_SYMBOL_SUB_20:
		case IDM_CHART_SYMBOL_SUB_21: case IDM_CHART_SYMBOL_SUB_22: case IDM_CHART_SYMBOL_SUB_23:
		case IDM_CHART_SYMBOL_SUB_24: case IDM_CHART_SYMBOL_SUB_25: case IDM_CHART_SYMBOL_SUB_26:
		case IDM_CHART_SYMBOL_SUB_27: case IDM_CHART_SYMBOL_SUB_28: case IDM_CHART_SYMBOL_SUB_29:
		case IDM_CHART_SYMBOL_SUB_30: case IDM_CHART_SYMBOL_SUB_31: case IDM_CHART_SYMBOL_SUB_32:
		case IDM_CHART_SYMBOL_SUB_33: case IDM_CHART_SYMBOL_SUB_34: case IDM_CHART_SYMBOL_SUB_35:
		case IDM_CHART_SYMBOL_SUB_36:
		{
			m_nSymbolSubMvmt = m_bnGo.m_nMenuResult;
			m_mnuGo.CheckMenuItem( m_nSymbolSubMvmt, MF_BYCOMMAND | MF_CHECKED );
			if( m_nChart >= CHART_TF )
			{
				IShowTF* pShow = GetShowTFObject();
				if( !pShow ) return;

				HRESULT hr = pShow->SetAnnotationSymbols( m_nSymbolSeg, m_nSymbolSubMvmt );
				if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting segmentation symbol.") );

				int nChart = m_nCurChart - 1;
				if( nChart >= 0 )
				{
					m_nCurChart--;
					m_fOptionChanged = TRUE;
					MoveNext();
				}
			}
			break;
		}
		case IDC_BN_VIEW:
		{
			m_fShowChartData = !m_fShowChartData;
			m_mnuGo.CheckMenuItem( IDC_BN_VIEW, MF_BYCOMMAND | m_fShowChartData ? MF_CHECKED : MF_UNCHECKED );
			ShowChartData();
			// in case proportional is checked
			int nChart = m_nCurChart - 1;
			if( ( nChart >= 0 ) && m_fProportional )
			{
				m_nCurChart--;
				m_fOptionChanged = TRUE;
				MoveNext();
			}
			break;
		}
		case IDC_BN_COLORMIN:
		{
			CBCGPColorDialog d( m_MinColor );
			if( d.DoModal() == IDOK )
			{
				m_MinColor = d.GetColor();
				CGraphDlg::_MinColor = m_MinColor;
				m_nCurChart--;
				MoveNext();
			}
			break;
		}
		case IDC_BN_COLORMAX:
		{
			CBCGPColorDialog d( m_MaxColor );
			if( d.DoModal() == IDOK )
			{
				m_MaxColor = d.GetColor();
				CGraphDlg::_MaxColor = m_MaxColor;
				m_nCurChart--;
				MoveNext();
			}
			break;
		}
		case TF_MENU_BASE: case TF_MENU_BASE + 1: case TF_MENU_BASE + 2: case TF_MENU_BASE + 3:
		case TF_MENU_BASE + 4: case TF_MENU_BASE + 5: case TF_MENU_BASE + 6: case TF_MENU_BASE + 7:
		case TF_MENU_BASE + 8: case TF_MENU_BASE + 9: case TF_MENU_BASE + 10: case TF_MENU_BASE + 11:
		case TF_MENU_BASE + 12: case TF_MENU_BASE + 13: case TF_MENU_BASE + 14: case TF_MENU_BASE + 15:
		{
			// if spectral graph, we do nothing
			GraphSelection* pGraph = GetGraph( m_nCurChart );
			if( !pGraph ) return;
			if( ( pGraph->m_nID == eTF_aspec ) || ( pGraph->m_nID == eTF_vaspec ) ||
				( pGraph->m_nID == eTF_aaspec ) )
			{
				m_cboFeature.SetCurSel( -1 );
				BCGPMessageBox( _T("Feedback is not used for spectral charts.") );
				return;
			}

			CMenu* pSMenu = m_mnuGo.GetSubMenu( 0 );
			CMenu* pFBMenu = pSMenu ? pSMenu->GetSubMenu( ( m_nChart == CHART_TFRT ) ? GMENU_FEEDBACK - 1 : GMENU_FEEDBACK ) : NULL;
			CMenu* pTFMenu = pFBMenu ? pFBMenu->GetSubMenu( GMENU_TF ) : NULL;
			if( pTFMenu )
			{
				m_nFeature = m_bnGo.m_nMenuResult;
				pTFMenu->GetMenuString( m_bnGo.m_nMenuResult, m_szFeature, MF_BYCOMMAND );
				CGraphDlg::_szFeature = m_szFeature;
				m_cboFeature.SetCurSel( m_cboFeature.FindString( -1, m_szFeature ) );
				m_fFeedback = ( m_szFeature != _T("NONE") );
				m_nCurChart--;
				MoveNext();
			}
			break;
		}
		case IDC_BN_REPLAY:
			OnBnReplay();
			break;
	}
}

//************************************
// Method:    OnChkInvert
// FullName:  CGraphDlg::OnChkInvert
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnChkInvert()
{
	UpdateData( TRUE );

	if( ( m_nChart >= CHART_TF ) || ( m_nChart == CHART_TF3D ) )
	{
		IShowTF* pShow = GetShowTFObject();
		if( !pShow ) return;

		// graph type
		TFGraph gt = (TFGraph)0;
		switch( m_nCurChart  + CHART_TF_BASE - 1 )
		{
			case CHART_TF_XY: gt = eTF_XY; break;
			case CHART_TF_XZ: gt = eTF_XZ; break;
			case CHART_TF_YZ: gt = eTF_YZ; break;
			case CHART_TF_Xt: gt = eTF_Xt; break;
			case CHART_TF_Yt: gt = eTF_Yt; break;
			case CHART_TF_Zt: gt = eTF_Zt; break;
			case CHART_TF_VX: gt = eTF_vx; break;
			case CHART_TF_VY: gt = eTF_vy; break;
			case CHART_TF_VABS: gt = eTF_vabs; break;
			case CHART_TF_AX: gt = eTF_ax; break;
			case CHART_TF_AY: gt = eTF_ay; break;
			case CHART_TF_AZ: gt = eTF_az; break;
			case CHART_TF_JXt: gt = eTF_JXt; break;
			case CHART_TF_JYt: gt = eTF_JYt; break;
			case CHART_TF_JABSt: gt = eTF_JABSt; break;
			case CHART_TF_ASPEC: gt = eTF_aspec; break;
		}
		if( m_nChart == CHART_TF3D ) gt = (TFGraph)(eTF_3D | gt);

		HRESULT hr = pShow->SetYZInverted( (VARIANT*)&m_hPE, gt, m_fInverted );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting monochorome.") );
	}
	else if( ( m_nChart >= CHART_HWR ) || ( m_nChart == CHART_HWR3D ) )
	{
		IShowHWR* pShow = GetShowHWRObject();
		if( !pShow ) return;

		HWRGraph gt = (HWRGraph)0;
		switch( m_nCurChart  + CHART_HWR_BASE - 1 )
		{
			case CHART_HWR_XY: gt = eHWR_XY; break;
			case CHART_HWR_XZ: gt = eHWR_XZ; break;
			case CHART_HWR_YZ: gt = eHWR_YZ; break;
			case CHART_HWR_XYt: gt = eHWR_XYt; break;
			case CHART_HWR3D: gt = eHWR_Zt; break;
		}
		if( m_nChart == CHART_HWR3D ) gt = (HWRGraph)(eHWR_3D | gt);

		HRESULT hr = pShow->SetYZInverted( (VARIANT*)&m_hPE, gt, m_fOverlay, m_fInverted );
		if( FAILED( hr ) ) BCGPMessageBox( _T("Error in setting monochorome.") );
	}
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartTf3D
// FullName:  CGraphDlg::ChartTf3D
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: double dFrequency
// Parameter: BOOL fPassed
//************************************
void CGraphDlg::ChartTf3D( CString szExp, CString szGrp, CString szSubj,
						   CString szTrial, double dFrequency, BOOL fPassed )
{
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// File structure
	CString szData;
	::GetTFMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() ) ::SortFilesAlpha( pFileList, szData );
	else ::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		::GetTF( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 3 );
		pFileList->SetAt( pLastPos, szData );
	}

	// extract condition from file name
	// ending part of file is CCC###.HWR or .FRD
	// so condition (CCC) should be last 3 chars after next statement
	if( szTrial.GetLength() < 8 ) return;
	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) ) cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}

	WindowManager* pWM = ::GetWindowManager();
	CGraphDlg* gd = new CGraphDlg( dFrequency, 0, pFileList, szTrial, CHART_TF3D,
								   ( szFB != _T("") ) ? TRUE : FALSE, szFeature, dwMin,
								   dwMax, AfxGetMainWnd(), FALSE, m_dwPenUpColor, TRUE, FALSE,
								   NULL, NULL, pWM, fPassed );
	if( gd && ( this->GetSafeHwnd() != NULL ) )
	{
		CRect rect;
		GetWindowRect( rect );
		gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	}

	// add to window manager
	if( pWM ) pWM->AddItem( gd );
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartHwr3D
// FullName:  CGraphDlg::ChartHwr3D
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: double dFrequency
//************************************
void CGraphDlg::ChartHwr3D( CString szExp, CString szGrp, CString szSubj,
							CString szTrial, double dFrequency )
{
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// File structure
	CString szData;
	if( szTrial.Right( 3 ) == _T("HWR") )
		::GetHWRMask( szExp, szGrp, szSubj, szData );
	else if( szTrial.Right( 3 ) == _T("FRD") )
		::GetFRDMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() ) ::SortFilesAlpha( pFileList, szData );
	else ::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		if( szTrial.Right( 3 ) == _T("HWR") )
			::GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		else if( szTrial.Right( 3 ) == _T("FRD") )
			::GetFRD( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	WindowManager* pWM = ::GetWindowManager();
	CGraphDlg* gd = new CGraphDlg( dFrequency, 0, pFileList, szTrial, CHART_HWR3D,
								   FALSE, _T(""), 0, 0, AfxGetMainWnd(), FALSE,
								   m_dwPenUpColor, TRUE, FALSE, NULL, NULL, pWM );
	if( gd && ( this->GetSafeHwnd() != NULL ) )
	{
		CRect rect;
		GetWindowRect( rect );
		gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	}

	// add to window manager
	if( pWM ) pWM->AddItem( gd );
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartTfRT
// FullName:  CGraphDlg::ChartTfRT
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: double dFrequency
//************************************
void CGraphDlg::ChartTfRT( CString szExp, CString szGrp, CString szSubj,
						   CString szTrial, double dFrequency )
{
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// File structure
	CString szData;
	::GetTFMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() ) ::SortFilesAlpha( pFileList, szData );
	else ::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		::GetTF( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 3 );
		pFileList->SetAt( pLastPos, szData );
	}

	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) ) cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}

	CGraphDlg* gd = new CGraphDlg( dFrequency, 1, pFileList, szTrial, CHART_TFRT,
								   ( szFB != _T("") ) ? TRUE : FALSE, szFeature, dwMin,
								   dwMax, AfxGetMainWnd(), FALSE, m_dwPenUpColor, TRUE );
	if( gd && ( this->GetSafeHwnd() != NULL ) )
	{
		CRect rect;
		GetWindowRect( rect );
		gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	}
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartHwrRT
// FullName:  CGraphDlg::ChartHwrRT
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: double dFrequency
//************************************
void CGraphDlg::ChartHwrRT( CString szExp, CString szGrp, CString szSubj,
							CString szTrial, double dFrequency )
{
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// File structure
	CString szData;
	::GetHWRMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() ) ::SortFilesAlpha( pFileList, szData );
	else ::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	CGraphDlg* gd = new CGraphDlg( dFrequency, 1, pFileList, szTrial, CHART_HWRRT, FALSE,
								   _T(""), 0, 0, AfxGetMainWnd(), FALSE, m_dwPenUpColor, TRUE );
	if( gd && ( this->GetSafeHwnd() != NULL ) )
	{
		CRect rect;
		GetWindowRect( rect );
		gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	}
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartTf
// FullName:  CGraphDlg::ChartTf
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: double dFrequency
// Parameter: int nMinPenPressure
// Parameter: BOOL fPassed
//************************************
void CGraphDlg::ChartTf( CString szExp, CString szGrp, CString szSubj, CString szTrial,
						 double dFrequency, int nMinPenPressure, BOOL fPassed )
{
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// File structure
	CString szData;
	::GetTFMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() ) ::SortFilesAlpha( pFileList, szData );
	else ::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		::GetTF( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 3 );
		pFileList->SetAt( pLastPos, szData );
	}

	// extract condition from file name
	// ending part of file is CCC###.HWR or .FRD
	// so condition (CCC) should be last 3 chars after next statement
	if( szTrial.GetLength() < 8 ) return;
	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) ) cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}
	else szFeature = m_szFeature;

	WindowManager* pWM = ::GetWindowManager();
	CGraphDlg* gd = new CGraphDlg( dFrequency, nMinPenPressure, pFileList, szTrial, CHART_TF,
								   ( szFB != _T("") ) ? TRUE : FALSE, szFeature, dwMin,
								   dwMax, AfxGetMainWnd(), FALSE, m_dwPenUpColor, TRUE, FALSE,
								   NULL, NULL, pWM, fPassed );
	if( gd && ( this->GetSafeHwnd() != NULL ) )
	{
		CRect rect;
		GetWindowRect( rect );
		gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	}

	// add to window manager
	if( pWM ) pWM->AddItem( gd );
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartTf
// FullName:  CGraphDlg::ChartTf
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szFile
// Parameter: double dFrequency
// Parameter: CString szSeg
// Parameter: BOOL fModeless
// Parameter: BOOL fPassed
// Parameter: BOOL fForceShowErr
// Parameter: CWnd * pWndOwner
//************************************
void CGraphDlg::ChartTf( CString szFile, double dFrequency, CString szSeg,
						 BOOL fModeless, BOOL fPassed, BOOL fForceShowErr,
						 CWnd* pWndOwner )
{
	// extract condition from file name
	// ending part of file is CCC##.TF
	// so condition (CCC) should be last 3 chars after next statement
	if( szFile.GetLength() < 8 ) return;
	CString szTmp = szFile.Left( szFile.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) )
			cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}
	else szFeature = m_szFeature;

	CString szData;
	szData.Format( _T("Charting %s"), szFile );
	OutputAMessage( szData, TRUE );

	if( !fModeless )
	{
		CGraphDlg dlg( CHART_TF, pWndOwner );
		dlg.m_szFile = szFile;
		dlg.m_szFile2 = szSeg;
		dlg.m_dFrequency = dFrequency;
		dlg.m_fFeedback = ( szFB != _T("") ) ? TRUE : FALSE;
		dlg.m_szFeature = szFeature;
		dlg.m_MinColor = dwMin;
		dlg.m_MaxColor = dwMax;
		dlg.m_fPassedConsis = fPassed;
		dlg.DoModal();
	}
	else
	{
		CGraphDlg* gd = new CGraphDlg( dFrequency, 1, szFile, CHART_TF,
										( szFB!= _T("") ) ? TRUE : FALSE, szFeature, dwMin, dwMax,
										this, szSeg, m_dwPenUpColor, TRUE, NULL, NULL, NULL,
										fPassed, fForceShowErr );
		if( gd && ( this->GetSafeHwnd() != NULL ) )
		{
			CRect rect;
			GetWindowRect( rect );
			gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
			gd->PostMessage( WM_SETFOCUS, 0, 0 );
		}
		// add to window manager
		WindowManager* pWM = ::GetWindowManager();
		if( pWM ) pWM->AddItem( gd );
	}
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ChartHwr
// FullName:  CGraphDlg::ChartHwr
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szTrial
// Parameter: double dFrequency
// Parameter: int nMinPenPressure
//************************************
void CGraphDlg::ChartHwr( CString szExp, CString szGrp, CString szSubj,
						  CString szTrial, double dFrequency, int nMinPenPressure )
{
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// File structure
	CString szData;
	if( szTrial.Right( 3 ) == _T("HWR") )
		::GetHWRMask( szExp, szGrp, szSubj, szData );
	else if( szTrial.Right( 3 ) == _T("FRD") )
		::GetFRDMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() ) ::SortFilesAlpha( pFileList, szData );
	else ::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		if( szTrial.Right( 3 ) == _T("HWR") )
			GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		else if( szTrial.Right( 3 ) == _T("FRD") )
			GetFRD( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	WindowManager* pWM = ::GetWindowManager();
	CGraphDlg* gd = new CGraphDlg( dFrequency, nMinPenPressure, pFileList, szTrial, CHART_HWR,
								   FALSE, _T(""), 0, 0, AfxGetMainWnd(), FALSE, m_dwPenUpColor, TRUE,
								   FALSE, NULL, NULL, pWM );
	if( gd && ( this->GetSafeHwnd() != NULL ) )
	{
		CRect rect;
		GetWindowRect( rect );
		gd->SetWindowPos( NULL, rect.left + 20, rect.top + 20, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
	}

	// add to window manager
	if( pWM ) pWM->AddItem( gd );
}

//************************************
// Method:    NotifyOfTrial
// FullName:  CGraphDlg::NotifyOfTrial
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::NotifyOfTrial()
{
	CString szApp = AfxGetApp()->m_pszAppName;
	szApp.MakeLower();
	if( ( szApp.Find( _T("movalyzerx") ) == -1 ) &&
		( szApp.Find( _T("writalyzer") ) == -1 ) )
	{
		MessageManager* pMM = MessageManager::GetMessageManager();
		if( pMM )
		{
			CString szTemp = m_szFile;
			if( m_nChart == CHART_TF )
			{
				szTemp = m_szFile.Left( m_szFile.GetLength() - 3 );
				szTemp += _T(".HWR");
			}
			pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_TRIAL_SELECT ), (WPARAM)szTemp.GetBuffer() );
		}
	}
}

//************************************
// Method:    OnClickErr
// FullName:  CGraphDlg::OnClickErr
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CGraphDlg::OnClickErr()
{
	BCGPMessageBox( m_szErr );
}

//************************************
// Method:    GetActiveChart
// FullName:  CGraphDlg::GetActiveChart
// Access:    public 
// Returns:   CGraphDlg*
// Qualifier:
//************************************
CGraphDlg* CGraphDlg::GetActiveChart()
{
	return _pActiveChart;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#pragma once

// ProcSetPageSeg.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSeg dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageSeg : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageSeg)

// Construction
public:
	ProcSetPageSeg( IProcSetting* pPS = NULL );
	~ProcSetPageSeg();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_SEG };
	BOOL			m_fF;
	CColoredButton	m_chkF;
	BOOL			m_fL;
	CColoredButton	m_chkL;
	BOOL			m_fO;
	CColoredButton	m_chkO;
	BOOL			m_fS;
	CColoredButton	m_chkS;
	BOOL			m_fM;
	BOOL			m_fA;
	BOOL			m_fV;
	CColoredButton	m_chkV;
	BOOL			m_fMM;
	CColoredButton	m_chkMM;
	BOOL			m_fJ;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_linkAdv;
	CColoredButton	m_rdoM;
	CColoredButton	m_rdoZ;
	CColoredButton	m_rdoJ;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnBnClickedChkSegs();
	afx_msg void OnBnClickedMethod();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

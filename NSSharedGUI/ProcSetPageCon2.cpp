// ProcSetPageCon2.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageCon2.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageCon2 property page

IMPLEMENT_DYNCREATE(ProcSetPageCon2, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageCon2, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageCon2::ProcSetPageCon2( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageCon2::IDD), m_pPS( pPS )
{
	slanterror = PI/8.;
	spiralincreasefactor = 1.1;
	minlooparean = 0.001;
	straightcritstraight = 0.1;
	straightcritcurved = 0.01;

	m_editMLA.SetDefaultValue( minlooparean );
	m_editSCC.SetDefaultValue( straightcritcurved );
	m_editSCS.SetDefaultValue( straightcritstraight );
	m_editSE.SetDefaultValue( slanterror );
	m_editSIF.SetDefaultValue( spiralincreasefactor );

	if( m_pPS )
	{
		m_pPS->get_MinLoopArea( &minlooparean );
		m_pPS->get_MinStraightErrorCurved( &straightcritcurved );
		m_pPS->get_MaxStraightErrorStraight( &straightcritstraight );
		m_pPS->get_SlantError( &slanterror );
		m_pPS->get_SpiralIncreaseFactor( &spiralincreasefactor );
	}
}

ProcSetPageCon2::~ProcSetPageCon2()
{
}

void ProcSetPageCon2::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MLA, minlooparean);
	DDX_Text(pDX, IDC_EDIT_MSDC, straightcritcurved);
	DDX_Text(pDX, IDC_EDIT_MSES, straightcritstraight);
	DDX_Text(pDX, IDC_EDIT_MSE, slanterror);
	DDX_Text(pDX, IDC_EDIT_MVSSIR, spiralincreasefactor);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);

	DDX_Control(pDX, IDC_EDIT_MLA, m_editMLA);
	DDX_Control(pDX, IDC_EDIT_MSDC, m_editSCC);
	DDX_Control(pDX, IDC_EDIT_MSES, m_editSCS);
	DDX_Control(pDX, IDC_EDIT_MSE, m_editSE);
	DDX_Control(pDX, IDC_EDIT_MVSSIR, m_editSIF);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageCon2 message handlers

BOOL ProcSetPageCon2::OnApply() 
{
	if( !m_pPS ) return FALSE;

	m_pPS->put_MinLoopArea( minlooparean );
	m_pPS->put_MinStraightErrorCurved( straightcritcurved );
	m_pPS->put_MaxStraightErrorStraight( straightcritstraight );
	m_pPS->put_SlantError( slanterror );
	m_pPS->put_SpiralIncreaseFactor( spiralincreasefactor );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageCon2::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageCon2::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );
	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	double dminlooparean = 0., dstraightcritcurved = 0., dstraightcritstraight = 0.,
			 dslanterror = 0., dspiralincreasefactor = 0.;
	if( pPS )
	{
		pPS->get_MinLoopArea( &dminlooparean );
		pPS->get_MinStraightErrorCurved( &dstraightcritcurved );
		pPS->get_MaxStraightErrorStraight( &dstraightcritstraight );
		pPS->get_SlantError( &dslanterror );
		pPS->get_SpiralIncreaseFactor( &dspiralincreasefactor );

		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_MLA );
	szData = _T("Minimum loop area (cm2)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dminlooparean );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Loop Area") );

	pWnd = GetDlgItem( IDC_EDIT_MSDC );
	szData = _T("Minimum straightness departure for curved segments");
	szTmp.Format( _T(" [DEFAULT=%g]"), dstraightcritcurved );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Straightness") );

	pWnd = GetDlgItem( IDC_EDIT_MSES );
	szData = _T("Maximum straightness error for straight segments");
	szTmp.Format( _T(" [DEFAULT=%g]"), dstraightcritstraight );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Straightness") );

	pWnd = GetDlgItem( IDC_EDIT_MSE );
	szData = _T("Maximum slant error for strokes of a certain direction");
	szTmp.Format( _T(" [DEFAULT=%g]"), dslanterror );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Slant") );

	pWnd = GetDlgItem( IDC_EDIT_MVSSIR );
	szData = _T("Minimum relative stroke size increase ratio between succesive strokes in outward spiral");
	szTmp.Format( _T(" [DEFAULT=%g]"), dspiralincreasefactor );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Relative Stroke") );

	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );
	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageCon2::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageCon2::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_consistencychecking.html"), this );
	return TRUE;
}

void ProcSetPageCon2::OnBnReset() 
{
	slanterror = PI/8.;
	spiralincreasefactor = 1.1;
	minlooparean = 0.001;
	straightcritstraight = 0.1;
	straightcritcurved = 0.01;

	UpdateData( FALSE );
}

void ProcSetPageCon2::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void ProcSetPageCon2::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 11 );
}

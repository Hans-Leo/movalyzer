#pragma once

// GrpQuestionnaireDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// GrpQuestionnaireDlg dialog
#define	LIST_DIRECTION_NONE		0
#define LIST_DIRECTION_DOWN		1
#define LIST_DIRECTION_UP		2

class GroupQuestListBox : public CBCGPListBox
{
public:
	GroupQuestListBox() : CBCGPListBox(), 	m_fMouseDown( FALSE ), m_nLastItem( -1 ),
		m_nDir( LIST_DIRECTION_NONE )
	{}
	virtual ~GroupQuestListBox() {}

	BOOL	m_fMouseDown;
	UINT	m_nLastItem;
	UINT	m_nDir;

	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class AFX_EXT_CLASS GrpQuestionnaireDlg : public CBCGPDialog
{
// Construction
public:
	GrpQuestionnaireDlg(CString szExpID, CString szGrpID, CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_GRPQUEST };
	GroupQuestListBox		m_List;
	CString			m_szExpID;
	CString			m_szGrpID;
	CStringList		m_lstID;
	CStringList		m_lstQ;
	CStringList		m_lstH;
	CStringList		m_lstI;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	int FindItem( CString szID );
	virtual BOOL OnInitDialog();
	afx_msg void OnOK2();
	afx_msg void OnBnSelectall();
	afx_msg void OnBnSelectnone();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

// ElementPageAnimation.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ElementPageAnimation.h"
#include "ElementSheet.h"
#include "ElementPageGeneral.h"
#include "Experiments.h"
#include "Groups.h"
#include "Subjects.h"

// ElementPageAnimation dialog

IMPLEMENT_DYNAMIC(ElementPageAnimation, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ElementPageAnimation, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_RDO_EXIST, OnBnClickedRdoExist)
	ON_BN_CLICKED(IDC_RDO_RANDOM, OnBnClickedRdoRandom)
	ON_BN_CLICKED(IDC_RDO_RANDOMGEN, OnBnClickedRdoRandomgen)
	ON_BN_CLICKED(IDC_RDO_RANDOMSUBJ, OnBnClickedRdoRandomsubj)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnClickedBnBrowse)
	ON_CBN_SELCHANGE(IDC_CBO_EXP, OnCbnSelchangeCboExp)
	ON_CBN_SELCHANGE(IDC_CBO_GRP, OnCbnSelchangeCboGrp)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_CHK_ANIMATE, OnBnClickedChkAnimate)
END_MESSAGE_MAP()

ElementPageAnimation::ElementPageAnimation( IElement* pE )
	: CBCGPPropertyPage(ElementPageAnimation::IDD)
	, m_fAnimate(FALSE)
	, m_szPattern(_T(""))
	, m_pE( pE )
	, m_fExisting( TRUE )
	, m_fGen( TRUE )
	, m_dAnimationRate( 120. )
{
	if( m_pE )
	{
		BSTR bstr = NULL;

		m_pE->get_Animate( &m_fAnimate );
		m_pE->get_AnimationRate( &m_dAnimationRate );
		m_pE->get_AnimationFile( &bstr );
		m_szPattern = bstr;
		m_pE->get_AnimationRandom( &m_fExisting );	// prolly should have done in reverse..but oh well
		m_fExisting = !m_fExisting;
		m_pE->get_AnimationGenerate( &m_fGen );
		m_pE->get_AnimationSubjID( &bstr );
		CString szTemp = bstr;

		if( ( szTemp != _T("") ) && ( szTemp.GetLength() == 9 ) )
		{
			szExp = szTemp.Left( 3 );
			szGrp = szTemp.Mid( 3, 3 );
			szSubj = szTemp.Right( 3 );
		}
	}
}

ElementPageAnimation::~ElementPageAnimation()
{
}

void ElementPageAnimation::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_ANIMATE, m_fAnimate);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szPattern);
	DDX_Control(pDX, IDC_CBO_EXP, m_cboExp);
	DDX_Control(pDX, IDC_CBO_GRP, m_cboGrp);
	DDX_Control(pDX, IDC_CBO_SUBJ, m_cboSubj);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnBrowse);
	DDX_Text(pDX, IDC_EDIT_RATE, m_dAnimationRate);
}

// ElementPageAnimation message handlers

void ElementPageAnimation::OnBnClickedRdoExist()
{
	m_fExisting = TRUE;
	OnSetActive();
}

void ElementPageAnimation::OnBnClickedRdoRandom()
{
	m_fExisting = FALSE;
	OnSetActive();
}

void ElementPageAnimation::OnBnClickedRdoRandomgen()
{
	m_fGen = TRUE;
	OnSetActive();
}

void ElementPageAnimation::OnBnClickedRdoRandomsubj()
{
	m_fGen = FALSE;
	OnSetActive();
}

void ElementPageAnimation::OnBnClickedBnBrowse()
{
	UpdateData( TRUE );

	CString szFilter;
	szFilter = _T("HWR files (*.HWR)|*.HWR|");

	CString szFile;
	if( m_szPattern == _T("") ) szFile = _T("*.HWR");
	else szFile = m_szPattern ;

	CFileDialog d( TRUE, _T("HWR"), szFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
				   szFilter, this );
	if( d.DoModal() == IDCANCEL ) return;

	m_szPattern = d.GetPathName();

	UpdateData( FALSE );
}

void ElementPageAnimation::OnCbnSelchangeCboExp()
{
	CString szItem, szExpID, szGrpID, szSubjID;
	HRESULT hr, hr2;
	int nCnt = 0;

	Groups grp;
	if( !grp.IsValid() ) return;

	// Reset content
	m_cboGrp.ResetContent();
	m_cboSubj.ResetContent();

	// Getselection IDs
	GetIDs( szExpID, szGrpID, szSubjID );
	if( szExpID == _T("") ) return;

	// Find the groups for this experiment
	IExperimentMember* pEM = NULL;
	hr2 = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							  IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr2 ) ) return;
	hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetID( szGrpID );

		hr2 = pEM->FindForGroup( szExpID.AllocSysString(),
								 szGrpID.AllocSysString() );
		if( SUCCEEDED( hr2 ) )
		{
			grp.GetDescriptionWithID( szItem, TRUE );
			m_cboGrp.AddString( szItem );
			nCnt++;
		}

		hr = grp.GetNext();
	}
	pEM->Release();

	// If no groups, indicate
	if( nCnt == 0 )
	{
		szItem = _T("No Groups");
		m_cboGrp.AddString( szItem );
		m_cboGrp.SelectString( -1, szItem );
	}
}

void ElementPageAnimation::OnCbnSelchangeCboGrp()
{
	CString szItem, szExpID, szGrpID, szSubjID;
	HRESULT hr, hr2;
	int nCnt = 0;

	Subjects subj;
	if( !subj.IsValid() ) return;

	// Reset content
	m_cboSubj.ResetContent();

	// Getselection IDs
	GetIDs( szExpID, szGrpID, szSubjID );
	if( ( szExpID == _T("") ) || ( szGrpID == _T("") ) ) return;

	// Find the subjects for this experiment/group
	IExperimentMember* pEM = NULL;
	hr2 = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							  IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr2 ) ) return;
	hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetID( szSubjID );

		hr2 = pEM->Find( szExpID.AllocSysString(),
						 szGrpID.AllocSysString(),
						 szSubjID.AllocSysString() );
		if( SUCCEEDED( hr2 ) )
		{
			subj.GetNameWithID( szItem, ::IsPrivacyOn(), TRUE );
			m_cboSubj.AddString( szItem );
			nCnt++;
		}

		hr = subj.GetNext();
	}
	pEM->Release();

	// If no subjects, indicate
	if( nCnt == 0 )
	{
		szItem = _T("No Subjects");
		m_cboSubj.AddString( szItem );
		m_cboSubj.SelectString( -1, szItem );
	}
}

BOOL ElementPageAnimation::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ElementPageAnimation::OnHelpInfo(HELPINFO* pHelpInfo)
{
	::GoToHelp( _T("element_animation.html"), this );
	return TRUE;
}

BOOL ElementPageAnimation::OnApply()
{
	if( !m_pE ) return FALSE;

	UpdateData( TRUE );

	BOOL fExisting = ( GetCheckedRadioButton( IDC_RDO_EXIST, IDC_RDO_RANDOM ) == IDC_RDO_EXIST );
	BOOL fGen = ( GetCheckedRadioButton( IDC_RDO_RANDOMGEN, IDC_RDO_RANDOMSUBJ ) == IDC_RDO_RANDOMGEN );

	int nSel = m_cboExp.GetCurSel();
	if( nSel >= 0 ) m_cboExp.GetLBText( nSel, szExp );
	else szExp = _T("");
	nSel = m_cboGrp.GetCurSel();
	if( nSel >= 0 ) m_cboGrp.GetLBText( nSel, szGrp );
	else szGrp = _T("");
	nSel = m_cboSubj.GetCurSel();
	if( nSel >= 0 ) m_cboSubj.GetLBText( nSel, szSubj );
	else szSubj = _T("");

	CFileFind ff;
	if( m_fAnimate && fExisting && !ff.FindFile( m_szPattern ) )
	{
		BCGPMessageBox( _T("Pattern does not exist.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
		pWnd->SetFocus();
		return FALSE;
	}
	if( m_fAnimate && !fExisting && !fGen && ( szSubj == _T("") ) )
	{
		BCGPMessageBox( _T("Please select an appropriate subject.") );
		CWnd* pWnd = NULL;
		if( szGrp == _T("") ) GetDlgItem( IDC_CBO_GRP );
		else if( szExp == _T("") ) GetDlgItem( IDC_CBO_EXP );
		else pWnd = GetDlgItem( IDC_CBO_SUBJ );
		pWnd->SetFocus();
		return FALSE;
	}

	szExp = szExp.Left( 3 );
	szGrp = szGrp.Left( 3 );
	szSubj = szSubj.Left( 3 );

	m_pE->put_Animate( m_fAnimate );
	m_pE->put_AnimationRate( m_dAnimationRate );
	m_pE->put_AnimationFile( m_szPattern.AllocSysString() );
	m_pE->put_AnimationRandom( !m_fExisting );
	m_pE->put_AnimationGenerate( m_fGen );
	CString szItem;
	szItem.Format( _T("%s%s%s"), szExp, szGrp, szSubj );
	m_pE->put_AnimationSubjID( szItem.AllocSysString() );

	return CBCGPPropertyPage::OnApply();
}

BOOL ElementPageAnimation::OnInitDialog()
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	m_bnBrowse.SetImage( IDB_OPEN );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_ELEM );
	m_tooltip.SetTitle( _T("ELEMENT") );
	CWnd* pWnd = GetDlgItem( IDC_CHK_ANIMATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Animate this element via a pattern."), _T("Animate") );
	pWnd = GetDlgItem( IDC_RDO_EXIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Animation will be defined by an existing pattern (raw data file)."), _T("Use Existing Pattern") );
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Path to an existing pattern for use in animation."), _T("Pattern Path") );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Browse for pattern."), _T("Browse") );
	pWnd = GetDlgItem( IDC_RDO_RANDOM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Animation will be defined by a random pattern."), _T("Use Random Pattern") );
	pWnd = GetDlgItem( IDC_RDO_RANDOMGEN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Random pattern will be generated each time by system."), _T("Generate")  );
	pWnd = GetDlgItem( IDC_RDO_RANDOMSUBJ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Random pattern will be selected from pool of selected subject."), _T("Subject Pattern") );
	pWnd = GetDlgItem( IDC_CBO_EXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an experiment."), _T("Experiment") );
	pWnd = GetDlgItem( IDC_CBO_GRP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the group of the experiment."), _T("Group") );
	pWnd = GetDlgItem( IDC_CBO_SUBJ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the subject whose trials you wish to use for random selection."), _T("Subject") );
	pWnd = GetDlgItem( IDC_EDIT_RATE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Sampling rate used for pattern file for animation."), _T("Sampling Rate") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	// FILL STATIC COMBO
	// Experiments
	Experiments exp;
	if( !exp.IsValid() ) return TRUE;
	CString szItem;
	HRESULT hr = exp.ResetToStart();
	int nCnt = 0;
	while( SUCCEEDED( hr ) )
	{
		exp.GetDescriptionWithID( szItem, TRUE );
		m_cboExp.AddString( szItem );
		hr = exp.GetNext();
		nCnt++;
	}
	if( nCnt == 0 )
	{
		szItem = _T("No Experiments");
		m_cboExp.AddString( szItem );
		m_cboExp.SelectString( -1, szItem );
	}

	// select appropriate items from combos
	if( szExp != _T("") )
	{
		m_cboExp.SelectString( -1, szExp );
		OnCbnSelchangeCboExp();
		m_cboGrp.SelectString( -1, szGrp );
		OnCbnSelchangeCboGrp();
		m_cboSubj.SelectString( -1, szSubj );
	}

	// Set checks/etc
	CheckRadioButton( IDC_RDO_EXIST, IDC_RDO_RANDOM, m_fExisting ? IDC_RDO_EXIST : IDC_RDO_RANDOM );
	CheckRadioButton( IDC_RDO_RANDOMGEN, IDC_RDO_RANDOMSUBJ, m_fGen ? IDC_RDO_RANDOMGEN : IDC_RDO_RANDOMSUBJ );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ElementPageAnimation::OnSetActive()
{
	UpdateData( TRUE );
	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = pSheet ? (ElementPageGeneral*)pSheet->GetPage( 0 ) : NULL;

	CWnd* pWnd = NULL;
	BOOL fCanAnimate = pPage ? ( !pPage->IsImage() && !pPage->IsPattern() ) : FALSE;
	if( !fCanAnimate ) m_fAnimate = FALSE;
	BOOL fExisting = ( GetCheckedRadioButton( IDC_RDO_EXIST, IDC_RDO_RANDOM ) == IDC_RDO_EXIST );
	BOOL fGen = ( GetCheckedRadioButton( IDC_RDO_RANDOMGEN, IDC_RDO_RANDOMSUBJ ) == IDC_RDO_RANDOMGEN );
	BOOL fExp = m_cboExp.GetCurSel() != CB_ERR;
	BOOL fGrp = m_cboGrp.GetCurSel() != CB_ERR;
	CString szItem;
	if( fExp )
	{
		m_cboExp.GetLBText( m_cboExp.GetCurSel(), szItem );
		fExp &= (BOOL)( szItem != _T("No Experiments") );
	}
	if( fGrp )
	{
		m_cboGrp.GetLBText( m_cboGrp.GetCurSel(), szItem );
		fGrp &= (BOOL)( szItem != _T("No Groups") );
	}

	pWnd = GetDlgItem( IDC_EDIT_RATE );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate );
	pWnd = GetDlgItem( IDC_RDO_EXIST );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate );
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && fExisting );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && fExisting );
//	pWnd = GetDlgItem( IDC_RDO_RANDOM );
//	if( pWnd ) pWnd->EnableWindow( m_fAnimate );
	pWnd = GetDlgItem( IDC_RDO_RANDOMGEN );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && !fExisting );
	pWnd = GetDlgItem( IDC_RDO_RANDOMSUBJ );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && !fExisting );
	pWnd = GetDlgItem( IDC_CBO_EXP );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && !fExisting && !fGen );
	pWnd = GetDlgItem( IDC_CBO_GRP );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && !fExisting && !fGen && fExp );
	pWnd = GetDlgItem( IDC_CBO_SUBJ );
	if( pWnd ) pWnd->EnableWindow( m_fAnimate && !fExisting && !fGen && fExp && fGrp );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL ElementPageAnimation::PreTranslateMessage(MSG* pMsg)
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPPropertyPage::PreTranslateMessage(pMsg);
}

void ElementPageAnimation::GetIDs( CString& szExpID, CString& szGrpID, CString& szSubjID )
{
	CString szDelim, szItem;
	int nPos;
	CWnd* pWnd;
	BOOL fExp = FALSE, fGrp = FALSE, fSubj = FALSE;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	if( m_cboExp.GetCurSel() >= 0 )
	{
		m_cboExp.GetLBText( m_cboExp.GetCurSel(), szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szExpID = szItem.Left( nPos - 1 );
			fExp = TRUE;
		}
	}

	// Which group
	if( m_cboGrp.GetCurSel() >= 0 )
	{
		m_cboGrp.GetLBText( m_cboGrp.GetCurSel(), szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Left( nPos - 1 );
			fGrp = TRUE;
		}
	}

	// Which subject
	if( m_cboSubj.GetCurSel() >= 0 )
	{
		m_cboSubj.GetLBText( m_cboSubj.GetCurSel(), szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szSubjID = szItem.Left( nPos - 1 );
			fSubj = TRUE;
		}
	}

	// Window enable/disable
	pWnd = GetDlgItem( IDC_CBO_GRP );
	if( pWnd ) pWnd->EnableWindow( fExp );
	pWnd = GetDlgItem( IDC_CBO_SUBJ );
	if( pWnd ) pWnd->EnableWindow( fGrp );
}

void ElementPageAnimation::OnBnClickedChkAnimate()
{
	OnSetActive();
}

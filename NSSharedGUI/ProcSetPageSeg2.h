#pragma once

// ProcSetPageSeg2.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSeg2 dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageSeg2 : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageSeg2)

// Construction
public:
	ProcSetPageSeg2( IProcSetting* pPS = NULL );
	~ProcSetPageSeg2();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_SEG2 };
	double			rvymin2;
	CColoredEdit	m_editRvymin2;
	double			rvymin;
	CColoredEdit	m_editRvymin;
	double			timemin;
	CColoredEdit	m_editTimemin;
	double			absdistmin;
	CColoredEdit	m_editAbsdistmin;
	double			rdistmin;
	CColoredEdit	m_editRdistmin;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;
	CBCGPButton		m_linkAdv;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnBnClickedBnCalc();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

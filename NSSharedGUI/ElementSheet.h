#if !defined(AFX_ELEMENTSHEET_H__10ADC4B1_A81D_11D3_8A59_000000000000__INCLUDED_)
#define AFX_ELEMENTSHEET_H__10ADC4B1_A81D_11D3_8A59_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ElementSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ElementSheet

interface IElement;

class AFX_EXT_CLASS ElementSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(ElementSheet)

// Construction
public:
	ElementSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	ElementSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	ElementSheet( IElement* pE, BOOL fNew = TRUE, BOOL fDup = FALSE, CWnd* pParentWnd = NULL );

// Attributes
public:
	BOOL	m_fModified;
	BOOL	m_fNew;
	BOOL	m_fDup;

// Operations
public:
	void AddPages( IElement* pE = NULL );

// Overrides

// Implementation
public:
	virtual ~ElementSheet();

// message map functions
protected:
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_ELEMENTSHEET_H__10ADC4B1_A81D_11D3_8A59_000000000000__INCLUDED_)

#pragma once

#include "..\NSSharedGUI\NSTooltipCtrl.h"

class WindowManager;

// CSortListCtrl class
class CSortListCtrl : public CBCGPListCtrl
{
public:
	CSortListCtrl() : hwndParent( NULL ), nSortedCol( 0 ), bSortAscending( TRUE ),
					  fSMA( FALSE ), fExt( FALSE ) {}
	~CSortListCtrl() {}

	HWND	hwndParent;
	int		nSortedCol; 
	BOOL	bSortAscending;
	CString	szFile;
	BOOL	fSMA;
	BOOL	fExt;

	virtual COLORREF OnGetCellTextColor( int nRow, int nColum );
	virtual COLORREF OnGetCellBkColor( int nRow, int nColum );
	virtual HFONT OnGetCellFont( int nRow, int nColum, DWORD dwData = 0 );

	BOOL SortItems( int nCol, BOOL bAscending, int low = 0, int high = -1 );

	void OnHeaderClicked(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDropFiles(HDROP dropInfo);
	DECLARE_MESSAGE_MAP()
};

// DataTabDlg dialog

class AFX_EXT_CLASS DataTabDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(DataTabDlg)

public:
	DataTabDlg( CString szFile, CWnd* pParent = NULL, WindowManager* pWM = NULL, int nCurChart = 0 );   // standard constructor
	virtual ~DataTabDlg();

// Dialog Data
	enum { IDD = IDD_DATATAB };
	BOOL			m_fInit;
	CSortListCtrl	m_list;
	CString			m_szFile;
	NSToolTipCtrl	m_tooltip;
	int				m_nTop;
	int				m_nLeft;
	int				m_nRightBuffer;
	int				m_nBottomBuffer;
	WindowManager*	m_pWM;
	int				m_nCurChart;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	virtual void PostNcDestroy();
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage( MSG* );

	void FillList( CString szFile );
	LRESULT OnFillList( WPARAM, LPARAM );

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedBnEdit();
	afx_msg void OnBnClickedBnRefresh();
	afx_msg LRESULT OnWMMessage( WPARAM, LPARAM );
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnClickExcel();
	afx_msg void OnClickSort();
	DECLARE_MESSAGE_MAP()
};

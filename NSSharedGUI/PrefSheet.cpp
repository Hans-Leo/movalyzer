// PrefSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PrefSheet.h"
#include "PrefPageUser.h"
#include "PrefPageInput.h"
#include "PrefPageGeneral.h"
#include "PrefPageGripper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrefSheet

IMPLEMENT_DYNAMIC(PrefSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(PrefSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

PrefSheet::PrefSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CBCGPPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_fgs = TRUE;

	AddPages();
}

PrefSheet::PrefSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CBCGPPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_fgs = TRUE;

	AddPages();
}

PrefSheet::PrefSheet( CString szID, CWnd* pParentWnd, UINT iSelectPage, BOOL fSettings )
	: CBCGPPropertySheet( _T("User"), pParentWnd, iSelectPage ), m_szID( szID )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_fgs = fSettings;

	AddPages();
}

PrefSheet::~PrefSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void PrefSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	CString szPath;
	::GetDataPath( szPath );
	szPath += _T("Data");

	strcpy_s( m_pref.pszID, m_szID );
	strcpy_s( m_pref.pszDesc, _T("") );
	strcpy_s( m_pref.pszPath, _MAX_PATH, szPath );
	szPath += _T("\\BACKUP");
	strcpy_s( m_pref.pszBackup, _MAX_PATH, szPath );
	strcpy_s( m_pref.pszDelim, _T(":") );
	m_pref.fLog = FALSE;
	m_pref.fAlpha = FALSE;
	m_pref.fPrivate = FALSE;
	m_pref.ip = Preferences::itMouse;
	m_pref.tm = Preferences::tmDesktop;
	strcpy_s( m_pref.pszPass, _T("userpass") );
	m_pref.dAR_X = 8.;
	m_pref.dAR_Y = 6.;
	m_pref.dD_Y = 12. * 2.54;
	m_pref.dT_Y = 6. * 2.54;
	m_pref.dD_X = 16. * 2.54;
	m_pref.dT_X = 8. * 2.54;
	strcpy_s( m_pref.pszComPort, _T("") );
	m_pref.fAutoUpdate = TRUE;
	m_pref.fPrivate = 0;
	strcpy_s( m_pref.pszWinUser, _T("") );
	m_pref.fGenSubjID = TRUE;
	strcpy_s( m_pref.pszSubjStartID, _T("000") );
	strcpy_s( m_pref.pszSubjEndID, _T("ZZZ") );
	strcpy_s( m_pref.pszSubjCurID, m_pref.pszSubjStartID );
	strcpy_s( m_pref.pszSiteID, _T("LOCAL") );
	strcpy_s( m_pref.pszSiteDesc, _T("Independent Use") );

	if( m_fgs )
	{
		m_gs.nDevice = 1;
		m_gs.nChannels = 3;
		m_gs.lSamples = 1000L;
		m_gs.lSampleRate = 100000L;
		m_gs.lScanRate = 100L;
		m_gs.dCalibrationConstantL = 1000.;
		m_gs.dCalibrationConstantLG = 1000.;
		m_gs.dCalibrationConstantUG = 1000.;
		m_gs.dExcitationVoltageL = 5.0;
		m_gs.dExcitationVoltageLG = 5.0;
		m_gs.dExcitationVoltageUG = 5.0;
		m_gs.dFullScaleLoadL = 25.0;
		m_gs.dFullScaleLoadLG = 25.0;
		m_gs.dFullScaleLoadUG = 25.0;
		m_gs.dGainL = 1.0;
		m_gs.dGainLG = 1.0;
		m_gs.dGainUG = 1.0;
		m_gs.nBaseline = 50;
		m_gs.nChanLower = 2;
		m_gs.nChanUpper = 0;
		m_gs.nChanLoad = 1;
		m_gs.fVolts = FALSE;
		m_gs.fNewtons = TRUE;
		m_gs.lDBSamples = 20;
	}

	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;

	CBCGPPropSheetCategory* pCat1 = AddTreeCategory( _T("Properties"), 0, 1 );
	AddPageToTree( pCat1, new PrefPageUser( &m_pref ), -1, 2 );
	AddPageToTree( pCat1, new PrefPageGeneral( &m_pref ), -1, 2 );
	AddPageToTree( pCat1, new PrefPageInput( &m_pref ), -1, 2 );
	if( m_fgs && ::IsGripperInstalled() &&::IsGripperModeOn() )
		AddPageToTree( pCat1, new PrefPageGripper( &m_gs ), -1, 2 );

	m_fModified = FALSE;
}

void PrefSheet::GripperMode( BOOL fOn )
{
	// how many pages are being displayed? (4 if gripper is, 3 if not)
	int nNumPages = GetPageCount();

	// gripper mode was turned on
	if( fOn )
	{
		// if it's not already there, add it
		if( ( nNumPages != 4 ) && ::IsGripperInstalled() )
		{
			// get the category
			CBCGPPropSheetCategory* pCat =
				m_lstTreeCategories.GetAt( m_lstTreeCategories.FindIndex( 0 ) );
			// add the page to the category & property sheet
			if( pCat ) AddPageToTree( pCat, new PrefPageGripper( &m_gs ), -1, 2 );
		}
	}
	// gripper mode was turned off & it is already there
	else if( nNumPages == 4 )
	{
		// get category
		CBCGPPropSheetCategory* pCat =
			m_lstTreeCategories.GetAt( m_lstTreeCategories.FindIndex( 0 ) );
		if( pCat )
		{
			// get page
			CBCGPPropertyPage* pPage = (CBCGPPropertyPage*)GetPage( 3 );
			if( !pPage ) return;

			// remove from category list
			POSITION pos = pCat->m_lstPages.FindIndex( 3 );
			if( pos ) pCat->m_lstPages.RemoveAt( pos );

			// remove from tree control
			// NOTE: since bcg didnt bother to actually use their "m_hTreeNode"
			//		 variable...loop through until we reach the last one
			HTREEITEM hItem = m_wndTree.GetRootItem();
			if( hItem ) hItem = m_wndTree.GetChildItem( hItem );		// user
			if( hItem ) hItem = m_wndTree.GetNextSiblingItem( hItem );	// settings
			if( hItem ) hItem = m_wndTree.GetNextSiblingItem( hItem );	// input
			if( hItem ) hItem = m_wndTree.GetNextSiblingItem( hItem );	// gripper
			if( hItem ) m_wndTree.DeleteItem( hItem );

			// delete page (bypass BCG version)
			CPropertySheet::RemovePage( pPage );
			delete pPage;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// PrefSheet message handlers

BOOL PrefSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( PrefPageUser ) ) )
			return ((PrefPageUser*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( PrefPageInput ) ) )
			return ((PrefPageInput*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( PrefPageGeneral ) ) )
			return ((PrefPageGeneral*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( PrefPageGripper ) ) )
			return ((PrefPageGripper*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

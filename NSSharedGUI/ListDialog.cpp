// ListDialog.cpp : implementation file
//

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include "ListDialog.h"
#include "OverwriteDlg.h"
#include "RelationshipDlg.h"


// ListDialog dialog

IMPLEMENT_DYNAMIC(ListDialog, CBCGPDialog)

BEGIN_MESSAGE_MAP(ListDialog, CBCGPDialog)
	ON_BN_CLICKED(IDC_CHK_HIDE, OnBnClickedChkHide)
	ON_BN_CLICKED(IDC_BN_REL, OnBnClickedRel)
END_MESSAGE_MAP()

ListDialog::ListDialog(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ListDialog::IDD, pParent), m_fShowOnly( TRUE )
{
	m_fDiffNameFirst = FALSE;
	m_fDiffNameLast = FALSE;
	m_fDiffNotesPrv = FALSE;
	m_fDiffSig = FALSE;
	m_fDiffPass = FALSE;
}

ListDialog::~ListDialog()
{
}

void ListDialog::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Check(pDX, IDC_CHK_HIDE, m_fShowOnly);
}

// ListDialog message handlers
BOOL ListDialog::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// COLUMN HEADERS
	// Column Headers
	m_list.InsertColumn( 0, _T("Field"), LVCFMT_LEFT, 100, 0 );
	m_list.InsertColumn( 1, _T("Original"), LVCFMT_LEFT, 100, 1 );
	m_list.InsertColumn( 2, _T("To Import"), LVCFMT_LEFT, 100, 2 );

	// Image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );

	// Fill list
	FillFileCompareList();

	// disable relationship button for those w/o support
	OverwriteDlg* pParent = (OverwriteDlg*)GetParent();
	if( pParent )
	{
		switch( pParent->m_nType )
		{
			case OW_UNKNOWN:
			case OW_EXPERIMENT:
			case OW_SETTINGS:
			case OW_QUEST:
			case OW_GQUEST:
			case OW_SQUEST:
			case OW_STIMTARG:
			case OW_CAT:
			case OW_USER:
			{
				CWnd* pWnd = GetDlgItem( IDC_BN_REL );
				if( pWnd ) pWnd->EnableWindow( FALSE );
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void ListDialog::FillFileCompareList()
{
	m_list.DeleteAllItems();

	// open files
	CStdioFile fOrig, fNew;
	if( !fOrig.Open( m_szFileOrig, CFile::modeRead ) ) return;
	if( !fNew.Open( m_szFileNew, CFile::modeRead ) ) return;
	CString szLineOrig, szLineNew, szName, szValOrig, szValNew;
	BOOL fContOrig = TRUE, fContNew = TRUE, fFlag = FALSE, fHide = FALSE,
		 fDidID = FALSE, fIsID = FALSE;
	int nPos = 0, nItem = 0, nImage = -1;

	// type
	UINT nType = OW_UNKNOWN;
	OverwriteDlg* pParent = (OverwriteDlg*)GetParent();
	if( pParent ) nType = pParent->m_nType;

	// loop through each line, extract name & values, compare and add to list
	while( fContOrig || fContNew )
	{
		fIsID = FALSE;
		fFlag = FALSE;
		fHide = FALSE;
		szName = szValOrig = szValNew = _T("");

		if( fContOrig && !fOrig.ReadString( szLineOrig ) ) fContOrig = FALSE;
		if( fContNew && !fNew.ReadString( szLineNew ) ) fContNew = FALSE;

		// get name & original val
		if( fContOrig )
		{
			nPos = szLineOrig.Find( _T(" : ") );
			if( nPos != -1 )
			{
				szName = szLineOrig.Left( nPos );
				szValOrig = szLineOrig.Mid( nPos + 3 );
				// if a subject && a private field, hide val, but flag
				if( ( nType == OW_SUBJECT ) &&
					( ( szName == _T("LastName") ) || ( szName == _T("FirstName") ) ||
					  ( szName == _T("PrvNotes") ) || ( szName == _T("Signature") ) ||
					  ( szName == _T("Password") ) ) )
				{
					szValOrig = _T("<Encrypted>");
					fHide = TRUE;
				}
				if( !fDidID && szName.Find( _T("ID") ) != -1 )
				{
					fFlag = TRUE;
					fDidID = TRUE;
					fIsID = TRUE;
				}
			}
			else fFlag = TRUE;
		}
		else fFlag = TRUE;
		// get name (if necessary) & new val
		if( fContNew )
		{
			nPos = szLineNew.Find( _T(" : ") );
			if( nPos != -1 )
			{
				if( szName == _T("") ) szName = szLineNew.Left( nPos );
				szValNew = szLineNew.Mid( nPos + 3 );
				// if a subject && a private field, hide val, but flag
				if( ( nType == OW_SUBJECT ) &&
					( ( szName == _T("LastName") ) || ( szName == _T("FirstName") ) ||
					  ( szName == _T("PrvNotes") ) || ( szName == _T("Signature") ) ||
					  ( szName == _T("Password") ) ) )
				{
					szValNew = _T("<Encrypted>");
					fHide = TRUE;
				}
			}
			else fFlag = TRUE;
		}
		else fFlag = TRUE;
		// compare
		if( fHide )
		{
			if( ( ( szName == _T("LastName") ) && m_fDiffNameLast ) ||
				( ( szName == _T("FirstName") ) && m_fDiffNameFirst ) ||
				( ( szName == _T("PrvNotes") ) && m_fDiffNotesPrv ) ||
				( ( szName == _T("Signature") ) && m_fDiffSig ) ||
				( ( szName == _T("Password") ) && m_fDiffPass ) )
				fFlag = TRUE;

		}
		else if( szValOrig != szValNew ) fFlag = TRUE;
		// add to list
		if( ( fContOrig || fContNew ) && ( fFlag || ( !fFlag && !m_fShowOnly ) ) )
		{
			nImage = -1;
			if( fFlag && !fIsID ) nImage = 6;

			nItem = m_list.InsertItem( m_list.GetItemCount(), szName, nImage );
			m_list.SetItem( nItem, 1, LVIF_TEXT, szValOrig, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 2, LVIF_TEXT, szValNew, 0, 0, 0, NULL );
		}
	}
}

void ListDialog::OnBnClickedChkHide()
{
	UpdateData( TRUE );
	FillFileCompareList();
}

void ListDialog::OnBnClickedRel()
{
	OverwriteDlg* pParent = (OverwriteDlg*)GetParent();
	if( pParent )
	{
		RelationshipDlg d( this );
		d.m_szItem = m_szItem;
		switch( pParent->m_nType )
		{
			case OW_GROUP:		d.m_nType = REL_GROUP;		break;
			case OW_SUBJECT:	d.m_nType = REL_SUBJECT;	break;
			case OW_CONDITION:	d.m_nType = REL_CONDITION;	break;
			case OW_STIMULUS:	d.m_nType = REL_STIMULUS;	break;
			case OW_ELEMENT:	d.m_nType = REL_ELEMENT;	break;
			case OW_CAT:		d.m_nType = REL_CAT;		break;
			case OW_FEEDBACK:	d.m_nType = REL_FEEDBACK;	break;
			default: return;
		}
		d.DoModal();
	}
}

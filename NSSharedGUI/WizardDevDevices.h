#pragma once

#include "NSTooltipCtrl.h"

// WizardDevDevices dialog

class AFX_EXT_CLASS WizardDevDevices : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardDevDevices)

public:
	WizardDevDevices();
	virtual ~WizardDevDevices();

// Dialog Data
	enum { IDD = IDD_WIZ_DEV_DEVICES };
	int		m_nDevice;	// IDC_RDO_MOUSE, IDC_RDO_TABLET, IDC_RDO_GRIPPER
	UINT	m_nCurImg;
	NSToolTipCtrl	m_tooltip;

public:
	virtual LRESULT OnWizardBack();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void ShowHideImages( UINT nID = -1 );
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedDevice();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnMouseMove(UINT, CPoint);

	DECLARE_MESSAGE_MAP()
};

#pragma once

#include "..\Common\GUI\XSuperTooltip.h"

struct COLOR_SCHEME
{
	TCHAR *		lpszName;
	COLORREF	rgbStart;
	COLORREF	rgbMiddle;
	COLORREF	rgbEnd;
	COLORREF	rgbText;
};

#define UNM_HYPERLINK_CLICKED	( WM_APP + 0x100 )

/////////////////////////////////////////////////////////////////////////////
// NSToolTipCtrl.h : header file

class AFX_EXT_CLASS NSToolTipCtrl : public CXSuperTooltip
{
// construction/destruction
public:
	NSToolTipCtrl();
	virtual ~NSToolTipCtrl() {}

// attributes
public:
	static COLOR_SCHEME m_ColorSchemes[];
	UINT	m_nIcon;
	BOOL	m_fActivated;

// operations
public:
	// legacy support for old tooltip methods
	void SetThisIcon( UINT nIconResourceID );
	void SetTitle( CString ) {}
	void Activate( BOOL fActivate ) { m_fActivated = fActivate; }
	// add tools
	CString AddTool( CWnd* pWnd, LPCTSTR lpszText, LPCTSTR lpszTitle = NULL, UINT nImage = 0 );
	CString AddTool( CWnd* pWnd, UINT nIDText, LPCTSTR lpszTitle, UINT nImage = 0 );
	CString AddTool( CWnd* pWnd, UINT nIDText, UINT nIDTitle = 0, UINT nImage = 0 );
	CString AddDefaultTool( SUPER_TOOLTIP_INFO* pTI );
	CString AddDefaultTool( CString szTitle, CString szMsg,
							UINT nID, CWnd* pWnd, UINT nImage = 0 );
};

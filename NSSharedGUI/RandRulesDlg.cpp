// RandRulesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "RandRulesDlg.h"
#include "..\DataMod\DataMod.h"
#include "Experiments.h"

// RandRulesDlg dialog

IMPLEMENT_DYNAMIC( RandRulesDlg, CBCGPDialog )

BEGIN_MESSAGE_MAP( RandRulesDlg, CBCGPDialog )
	ON_BN_CLICKED(IDC_CHK_RULE_2_1, OnBnClickedChkRule)
	ON_BN_CLICKED(IDC_CHK_RULE_3_1, OnBnClickedChkRule)
	ON_BN_CLICKED(IDC_CHK_RULE_2_2, OnBnClickedChkRule)
	ON_BN_CLICKED(IDC_CHK_RULE_3_2, OnBnClickedChkRule)
	ON_BN_CLICKED(IDC_CHK_RULE_2_3, OnBnClickedChkRule)
	ON_BN_CLICKED(IDC_CHK_RULE_3_3, OnBnClickedChkRule)
	ON_BN_CLICKED(IDC_BN_TEST, OnBnClickedBnTest)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

RandRulesDlg::RandRulesDlg( IProcSetting* pPS, CWnd* pParent )
	: CBCGPDialog( RandRulesDlg::IDD, pParent ), m_pPS( pPS )
{
	CString szVal, szItem, szTemp, szExp, szChar, szDelim = GetDelimiter();
	BSTR bstr = NULL;
	int nPos = 0, nItem = 0;
	short nVal = 0;

	memset( m_fUseRules, 0, sizeof( BOOL ) * 3 * 3 );
	memset( m_nBlocks, 0, sizeof( int ) * 3 );
	m_nTrials = 0;

	// get data from database
	if( m_pPS )
	{
		// Experiment ID
		m_pPS->get_ExperimentID( &bstr );
		szExp = bstr;

		// RULES
		m_pPS->get_RandRules( &bstr );
		szVal = bstr;
		m_szRules = szVal;
		if( szVal != _T("") )
		{
			// Extract Rules from string (each char is separated by a delimiter)
			// rules are or'd
			// ID CHAR # 1
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			nItem = atoi( szItem );
			if( ( nItem & RAND_RULE_1 ) != 0 ) m_fUseRules[ 0 ][ 0 ] = TRUE;
			else m_fUseRules[ 0 ][ 0 ] = FALSE;
			if( ( nItem & RAND_RULE_2 ) != 0 ) m_fUseRules[ 0 ][ 1 ] = TRUE;
			else m_fUseRules[ 0 ][ 1 ] = FALSE;
			if( ( nItem & RAND_RULE_3 ) != 0 ) m_fUseRules[ 0 ][ 2 ] = TRUE;
			else m_fUseRules[ 0 ][ 2 ] = FALSE;
			szVal = szVal.Mid( nPos + 1 );
			// ID CHAR # 2
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			nItem = atoi( szItem );
			if( ( nItem & RAND_RULE_1 ) != 0 ) m_fUseRules[ 1 ][ 0 ] = TRUE;
			else m_fUseRules[ 1 ][ 0 ] = FALSE;
			if( ( nItem & RAND_RULE_2 ) != 0 ) m_fUseRules[ 1 ][ 1 ] = TRUE;
			else m_fUseRules[ 1 ][ 1 ] = FALSE;
			if( ( nItem & RAND_RULE_3 ) != 0 ) m_fUseRules[ 1 ][ 2 ] = TRUE;
			else m_fUseRules[ 1 ][ 2 ] = FALSE;
			szVal = szVal.Mid( nPos + 1 );
			// ID CHAR # 3
			if( szVal == _T("") ) return;
			szItem = szVal;
			nItem = atoi( szItem );
			if( ( nItem & RAND_RULE_1 ) != 0 ) m_fUseRules[ 2 ][ 0 ] = TRUE;
			else m_fUseRules[ 2 ][ 0 ] = FALSE;
			if( ( nItem & RAND_RULE_2 ) != 0 ) m_fUseRules[ 2 ][ 1 ] = TRUE;
			else m_fUseRules[ 2 ][ 1 ] = FALSE;
			if( ( nItem & RAND_RULE_3 ) != 0 ) m_fUseRules[ 2 ][ 2 ] = TRUE;
			else m_fUseRules[ 2 ][ 2 ] = FALSE;
		}

		// Extract blocks from string (each char separated by delim)
		m_pPS->get_RandBlockCounts( &bstr );
		szVal = bstr;
		m_szBlocks = szVal;
		if( szVal != _T("") )
		{
			// ID CHAR # 1
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			m_nBlocks[ 0 ] = atoi( szItem );
			szVal = szVal.Mid( nPos + 1 );
			// ID CHAR # 2
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			m_nBlocks[ 1 ] = atoi( szItem );
			szVal = szVal.Mid( nPos + 1 );
			// ID CHAR # 3
			szItem = szVal;
			m_nBlocks[ 2 ] = atoi( szItem );
		}

		// Extract chars for rule # 3 selection
		m_pPS->get_RandSelectionChar( &bstr );
		szVal = bstr;
		m_szSelChars = szVal;
		if( szVal != _T("") )
		{
			// ID CHAR # 1
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			m_szSelChar[ 0 ] = szItem[ 0 ];
			szVal = szVal.Mid( nPos + 1 );
			// ID CHAR # 2
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			m_szSelChar[ 1 ] = szItem[ 0 ];
			szVal = szVal.Mid( nPos + 1 );
			// ID CHAR # 3
			szItem = szVal;
			m_szSelChar[ 2 ] = szItem[ 0 ];
		}

		// Extract trials to select for sel char for rule # 3
		// ...trials are separated by a period "."
		m_pPS->get_RandSelectionTrials( &bstr );
		szVal = bstr;
		m_szSelTrials = szVal;
		if( szVal != _T("") )
		{
			// ID CHAR # 1
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			szVal = szVal.Mid( nPos + 1 );
			do 
			{
				nPos = szItem.Find( _T(".") );
				if( nPos != -1 ) szTemp = szItem.Left( nPos );
				else szTemp = szItem;
				if( szTemp != _T("") ) m_listSelTrials[ 0 ].AddTail( szTemp );
				if( nPos != -1 ) szItem = szItem.Mid( nPos + 1 );
			} while ( nPos != -1 );
			// ID CHAR # 2
			nPos = szVal.Find( szDelim );
			if( nPos == -1 ) return;
			szItem = szVal.Left( nPos );
			szVal = szVal.Mid( nPos + 1 );
			do 
			{
				nPos = szItem.Find( _T(".") );
				if( nPos != -1 ) szTemp = szItem.Left( nPos );
				else szTemp = szItem;
				if( szTemp != _T("") ) m_listSelTrials[ 1 ].AddTail( szTemp );
				if( nPos != -1 ) szItem = szItem.Mid( nPos + 1 );
			} while ( nPos != -1 );
			// ID CHAR # 3
			szItem = szVal;
			do 
			{
				nPos = szItem.Find( _T(".") );
				if( nPos != -1 ) szTemp = szItem.Left( nPos );
				else szTemp = szItem;
				if( szTemp != _T("") ) m_listSelTrials[ 2 ].AddTail( szTemp );
				if( nPos != -1 ) szItem = szItem.Mid( nPos + 1 );

			} while ( nPos != -1 );
		}
	}

	// get experiment conditions
	IExperimentCondition* pEC = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
									 IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return;
	}
	hr = pEC->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOCONDS );
		pEC->Release();
		return;
	}
	while( SUCCEEDED( hr ) )
	{
		pEC->get_ConditionID( &bstr );
		szVal = bstr;
		pEC->get_Replications( &nVal );
		szItem.Format( _T("%d"), nVal );

		m_listCond[ 0 ].AddTail( szVal );
		m_listCond[ 1 ].AddTail( szItem );

		hr = pEC->GetNext();
	}
	pEC->Release();

	// if no conditions, we are done
	int nCount = m_listCond[ 0 ].GetCount();
	if( nCount <= 0 ) return;

	// loop through each condition, extract each char
	// and add to char list with counts
	// also, calculate total # of trials = SUM( cond * reps )
	POSITION pos = NULL;
	for( int i = 0; i < nCount; i++ )
	{
		// get condition and reps
		pos = m_listCond[ 0 ].FindIndex( i );
		if( !pos ) return;
		szItem = m_listCond[ 0 ].GetAt( pos );
		pos = m_listCond[ 1 ].FindIndex( i );
		if( !pos ) return;
		szVal = m_listCond[ 1 ].GetAt( pos );
		nVal = atoi( szVal );

		// # trials
		m_nTrials += nVal;

		// extract each letter and add/update list w/counts
		for( int j = 0; j < 3; j++ )
		{
			szChar = szItem[ j ];
			nItem = nVal;
			if( m_listChars[ j ].Lookup( szChar, szTemp ) ) nItem += atoi( szTemp );
			szTemp.Format( _T("%d"), nItem );
			m_listChars[ j ][ szChar ] = szTemp;
		}
	}
}

RandRulesDlg::~RandRulesDlg()
{
}

void RandRulesDlg::DoDataExchange( CDataExchange* pDX )
{
	CBCGPDialog::DoDataExchange( pDX );
	DDX_Check( pDX, IDC_CHK_RULE_1_1, m_fUseRules[ 0 ][ 0 ] );
	DDX_Check( pDX, IDC_CHK_RULE_2_1, m_fUseRules[ 0 ][ 1 ] );
	DDX_Check( pDX, IDC_CHK_RULE_3_1, m_fUseRules[ 0 ][ 2 ] );
	DDX_Check( pDX, IDC_CHK_RULE_1_2, m_fUseRules[ 1 ][ 0 ] );
	DDX_Check( pDX, IDC_CHK_RULE_2_2, m_fUseRules[ 1 ][ 1 ] );
	DDX_Check( pDX, IDC_CHK_RULE_3_2, m_fUseRules[ 1 ][ 2 ] );
	DDX_Check( pDX, IDC_CHK_RULE_1_3, m_fUseRules[ 2 ][ 0 ] );
	DDX_Check( pDX, IDC_CHK_RULE_2_3, m_fUseRules[ 2 ][ 1 ] );
	DDX_Check( pDX, IDC_CHK_RULE_3_3, m_fUseRules[ 2 ][ 2 ] );
	DDX_Text( pDX, IDC_EDIT_BLOCK_1, m_nBlocks[ 0 ] );
	DDX_Text( pDX, IDC_EDIT_BLOCK_2, m_nBlocks[ 1 ] );
	DDX_Text( pDX, IDC_EDIT_BLOCK_3, m_nBlocks[ 2 ] );
	DDX_CBString( pDX, IDC_CBO_CHAR_1, m_szSelChar[ 0 ] );
	DDX_CBString( pDX, IDC_CBO_CHAR_2, m_szSelChar[ 1 ] );
	DDX_CBString( pDX, IDC_CBO_CHAR_3, m_szSelChar[ 2 ] );
	DDX_Control( pDX, IDC_CBO_CHAR_1, m_cboSelChar[ 0 ] );
	DDX_Control( pDX, IDC_CBO_CHAR_2, m_cboSelChar[ 1 ] );
	DDX_Control( pDX, IDC_CBO_CHAR_3, m_cboSelChar[ 2 ] );
	DDX_Control( pDX, IDC_LIST_1, m_lbSelTrials[ 0 ] );
	DDX_Control( pDX, IDC_LIST_2, m_lbSelTrials[ 1 ] );
	DDX_Control( pDX, IDC_LIST_3, m_lbSelTrials[ 2 ] );
	DDX_Control( pDX, IDC_LIST, m_lbResults );
}

BOOL RandRulesDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify import file location here."), _T("Import File") );

	// Populate dialog
	FillData();

	// ensure the 1st item in the 3 trial selection lists are visible
	if( m_nTrials > 0 )
		for( int i = 0; i < 3; m_lbSelTrials[ i++ ].SetCaretIndex( 0 ) );

	return TRUE;	// return TRUE unless you set the focus to a control
					// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL RandRulesDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent( pMsg );
	return CBCGPDialog::PreTranslateMessage( pMsg );
}

void RandRulesDlg::FillData()
{
	int i = 0, nSel = -1, nItem = 0;
	POSITION pos = NULL;
	CString szKey, szVal;

	// if no trials, nothing to do except enable/disable fields
	if( m_nTrials <= 0 )
	{
		EnableFields();
		return;
	}

	// fill in combo of chars for rule # 3 for each character
	for( i = 0; i < 3; i++ )
	{
		nSel = -1;
		nItem = -1;
		// fill combo
		pos = m_listChars[ i ].GetStartPosition();
		while( pos )
		{
			nItem++;
			m_listChars[ i ].GetNextAssoc( pos, szKey, szVal );
			m_cboSelChar[ i ].AddString( szKey );
			if( szKey == m_szSelChar[ i ] ) nSel = nItem;
		}
		// select appropriate one if exists
		if( nSel != -1 ) m_cboSelChar[ i ].SetCurSel( nSel );
	}

	// Fill lists of trials
	for( i = 0; i < m_nTrials; i++ )
	{
		szVal.Format( _T("%d"), i + 1 );
		for( int j = 0; j < 3; j++ )
		{
			nSel = m_lbSelTrials[ j ].AddString( szVal );
			// select this item if in selected trial array
			if( m_listSelTrials[ j ].Find( szVal ) )
				m_lbSelTrials[ j ].SetSel( nSel );
		}
	}

	// Find the pixel width of the largest item in the list (all 3 lists are the same content)
	CString str;
	CSize   sz;
	int     dx = 0;
	CDC*    pDC = m_lbSelTrials[ 0 ].GetDC();
	for( i = 0; i < m_lbSelTrials[ 0 ].GetCount(); i++ )
	{
		m_lbSelTrials[ 0 ].GetText( i, str );
		sz = pDC->GetTextExtent( str );
		if( sz.cx > dx ) dx = sz.cx;
	}
	m_lbSelTrials[ 0 ].ReleaseDC( pDC );

	// Set the column width of the first column to be one and 1/8 units
	// of the largest string
	for( i = 0; ( i < 3 ) && ( dx > 0 ); i++ )
	{
		m_lbSelTrials[ i ].SetColumnWidth( dx * 9 / 8 );
	}

	// enable/disable fields
	EnableFields();
}

void RandRulesDlg::EnableFields()
{
	// Disable blocks where rule # 2 is not selected
	CWnd* pWnd = GetDlgItem( IDC_EDIT_BLOCK_1 );
	pWnd->EnableWindow( m_fUseRules[ 0 ][ 1 ] );
	pWnd = GetDlgItem( IDC_EDIT_BLOCK_2 );
	pWnd->EnableWindow( m_fUseRules[ 1 ][ 1 ] );
	pWnd = GetDlgItem( IDC_EDIT_BLOCK_3 );
	pWnd->EnableWindow( m_fUseRules[ 2 ][ 1 ] );

	// Disable char & trial selections where rule # 3 is not selected
	for( int i = 0; i < 3; i++ )
	{
		m_cboSelChar[ i ].EnableWindow( m_fUseRules[ i ][ 2 ] );
		m_lbSelTrials[ i ].EnableWindow( m_fUseRules[ i ][ 2 ] );
		if( !m_fUseRules[ i ][ 2 ] ) m_lbSelTrials[ i ].SetCurSel( -1 );
	}
}

BOOL RandRulesDlg::VerifyValues()
{
	UpdateData( TRUE );

	// if no trials, pointless
	if( m_nTrials <= 0 ) return FALSE;

	int i = 0, nCount = 0, nVal = 0;
	CString szMsg, szVal;

	// VERIFY VALUES/SELECTIONS
	for( i = 0; i < 3; i++ )
	{
		// IF Rule # 2 is selected, ...
		// ... count must be > 0
		// ... count must be >= min count of letters and 
		// ...... <= max count of letters  ???? how will our rand. logic work with different counts?
		if( m_fUseRules[ i ][ 1 ] && ( m_nBlocks[ i ] <= 0 ) )
		{
			szMsg.Format( _T("You have specified an invalid block count for Rule # 2 of character %d.\nPlease press F1 for help if you need assistance."), i + 1 );
			BCGPMessageBox( szMsg );
			return FALSE;
		}

		// IF Rule # 3 is selected, ...
		// ... a character must be selected
		// ... # of selected trials must be the total count of that character
		if( m_fUseRules[ i ][ 2 ] )
		{
			// letter selected?
			if( m_szSelChar[ i ] == _T("") )
			{
				szMsg.Format( _T("You must specify an item for selection for Rule # 3 of character %d.\nPlease press F1 for help if you need assistance."), i + 1 );
				BCGPMessageBox( szMsg );
				return FALSE;
			}

			// # of instances of this letter
			m_listChars[ i ].Lookup( m_szSelChar[ i ], szVal );
			nCount = atoi( szVal );

			// confirm that # of instances matches # of selected items in list
			nVal = m_lbSelTrials[ i ].GetSelCount();
			if( nVal != nCount )
			{
				szMsg.Format( _T("You must select %d trials for the selection of %s for Rule # 3 of character %d.\nPlease press F1 for help if you need assistance."), nCount, m_szSelChar[ i ], i + 1 );
				BCGPMessageBox( szMsg );
				return FALSE;
			}
		}
	}

	return TRUE;
}

BOOL RandRulesDlg::ConstructParams()
{
	int i = 0, j = 0, nCount = 0, nVal = 0, nRules[ 3 ];
	CString szMsg, szVal, szTemp, szDelim = ::GetDelimiter();
	memset( nRules, 0, sizeof( int ) * 3 );

	// if no trials, pointless
	if( m_nTrials <= 0 ) return FALSE;

	// Rules ... 1 string, 3 columns (1 for each ID char), separated by delimiter
	szVal = _T("");
	for( i = 0; i < 3; i++ )
	{
		if( m_fUseRules[ i ][ 0 ] ) nRules[ i ] |= RAND_RULE_1;
		if( m_fUseRules[ i ][ 1 ] ) nRules[ i ] |= RAND_RULE_2;
		if( m_fUseRules[ i ][ 2 ] ) nRules[ i ] |= RAND_RULE_3;
		szTemp.Format( _T("%d"), nRules[ i ] );
		szVal += szTemp;
		if( i < 2 ) szVal += szDelim;
	}
	m_szRules = szVal;

	// Block counts
	szVal = _T("");
	for( i = 0; i < 3; i++ )
	{
		szTemp.Format( _T("%d"), m_nBlocks[ i ] );
		szVal += szTemp;
		if( i < 2 ) szVal += szDelim;
	}
	m_szBlocks = szVal;

	// Selection chars
	szVal = _T("");
	for( i = 0; i < 3; i++ )
	{
		szVal += m_szSelChar[ i ];
		if( i < 2 ) szVal += szDelim;
	}
	m_szSelChars = szVal;

	// Selection char trials
	szVal = _T("");
	for( i = 0; i < 3; i++ )
	{
		nCount = m_lbSelTrials[ i ].GetSelCount();
		if( nCount > 0 )
		{
			int* pSel = new int[ nCount ];
			m_lbSelTrials[ i ].GetSelItems( nCount, pSel );
			for( j = 0; j < nCount; j++ )
			{
				m_lbSelTrials[ i ].GetText( pSel[ j ], szTemp );
				szVal += szTemp;
				if( j < ( nCount - 1 ) ) szVal += _T(".");
			}
			delete [] pSel;
		}
		if( i < 2 ) szVal += szDelim;
	}
	m_szSelTrials = szVal;

	return TRUE;
}

void RandRulesDlg::OnOK()
{
	UpdateData( TRUE );

	// If there were no conditions, then exit w/o saving
	if( m_nTrials <= 0 ) EndDialog( IDCANCEL );

	// VERIFY VALUES/SELECTIONS
	if( !VerifyValues() ) return;

	// WRITE DATA TO DATABASE OBJECT
	// Rules
	if( !ConstructParams() ) return;
	m_pPS->put_RandRules( m_szRules.AllocSysString() );
	// Block counts
	m_pPS->put_RandBlockCounts( m_szBlocks.AllocSysString() );
	// Selection chars
	m_pPS->put_RandSelectionChar( m_szSelChars.AllocSysString() );
	// Selection char trials
	m_pPS->put_RandSelectionTrials( m_szSelTrials.AllocSysString() );

	CBCGPDialog::OnOK();
}

// RandRulesDlg message handlers

void RandRulesDlg::OnBnClickedChkRule()
{
	UpdateData( TRUE );
	EnableFields();
}

void RandRulesDlg::OnBnClickedBnTest()
{
	// clear list
	m_lbResults.ResetContent();

	// verify & construct
	if( !VerifyValues() || !ConstructParams() || !m_pPS ) return;

	CStringList lstConds;
	CString szExp, szErrFile;
	BSTR bstr = NULL;
	BOOL fNothing = FALSE;

	// experiment ID
	m_pPS->get_ExperimentID( &bstr );
	szExp = bstr;

	// Get list of conditions for use with randomization
	if( !Experiments::GetListOfConditions( szExp, &lstConds ) ) return;

	// Run test
	CStringList* lstRes = new CStringList;
	BOOL fRes = Experiments::RandomizeWithRules( &lstConds, lstRes, m_szRules,
		m_szBlocks, m_szSelChars, m_szSelTrials, fNothing, szExp, _T(""),
		_T(""), TRUE, szErrFile );

	CWaitCursor crs;
	CString szLine;

	// if failed, write error file in results list
	if( !fRes )
	{
		// if no file name returned, do nothing
		if( szErrFile == _T("") ) { delete lstRes; return; }
		// if file cannot be found, do nothing
		CFileFind ff;
		if( !ff.FindFile( szErrFile ) ) { delete lstRes; return; }
		// inform user
		CString szLine = _T("Would you like to view the step-by-step process that led to the failure?");
		if( BCGPMessageBox( szLine, MB_YESNO ) == IDYES )
		{
			// open file
			crs.Restore();
			szLine.Format( _T("notepad.exe %s"), szErrFile );
			WinExec( szLine, SW_SHOW );
		}
	}
	// else write results
	else
	{
		// inform user
		BCGPMessageBox( _T("The randomization rules were successfully applied.\nThe list box will show the results.") );
		// fill list
		crs.Restore();
		POSITION pos = lstRes->GetHeadPosition();
		while( pos )
		{
			szLine = lstRes->GetNext( pos );
			m_lbResults.AddString( szLine );
		}
		// cleanup
		delete lstRes;
	}
}

BOOL RandRulesDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_runexperiment_procedure.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

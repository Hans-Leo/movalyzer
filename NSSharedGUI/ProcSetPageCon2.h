#pragma once

// ProcSetPageCon2.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageCon2 dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageCon2 : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageCon2)

// Construction
public:
	ProcSetPageCon2( IProcSetting* pPS = NULL );
	~ProcSetPageCon2();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_CON2 };
	double			minlooparean;
	double			straightcritcurved;
	double			straightcritstraight;
	double			slanterror;
	double			spiralincreasefactor;
	CColoredEdit	m_editMLA;
	CColoredEdit	m_editSCC;
	CColoredEdit	m_editSCS;
	CColoredEdit	m_editSE;
	CColoredEdit	m_editSIF;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;
	CBCGPButton		m_linkAdv;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnBnClickedBnCalc();
	afx_msg void OnClickAdvanced();
	DECLARE_MESSAGE_MAP()
};

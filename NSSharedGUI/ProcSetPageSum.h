#pragma once

// ProcSetPageSum.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSum dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageSum : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageSum)

// Construction
public:
	ProcSetPageSum( IProcSetting* pPS = NULL );
	~ProcSetPageSum();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_SUM };
	BOOL			m_fA;
	BOOL			m_fR;
	BOOL			m_fT;
	BOOL			m_fZ;
	BOOL			m_fS;
	BOOL			m_fD;
	int				m_nDiscAfter;
	BOOL			m_fD2;
	int				m_nDiscAfter2;
	BOOL			m_fDiscWOLift;
	BOOL			m_fDiscWLift;
	double			m_dThreshold;
	BOOL			m_fCollapseUDStrokes;
	BOOL			m_fCollapseStrokes;
	BOOL			m_fCollapseTrials;
	BOOL			m_fMedian;
	CColoredButton	m_chkA;
	CColoredButton	m_chkR;
	CColoredButton	m_chkT;
	CColoredButton	m_chkZ;
	CColoredButton	m_chkS;
	CColoredButton	m_chkD;
	CColoredEdit	m_editDA;
	CColoredButton	m_chkD2;
	CColoredEdit	m_editDA2;
	CColoredButton	m_chkWOL;
	CColoredButton	m_chkWL;
	CColoredEdit	m_editThreshold;
	CColoredButton	m_chkCollUD;
	CColoredButton	m_chkCollS;
	CColoredButton	m_chkCollT;
	CColoredButton	m_rdoAvg;
	CColoredButton	m_rdoMed;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnChkSumt();
	afx_msg void OnChkSums();
	afx_msg void OnChkSumd();
	afx_msg void OnChkSumd2();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnChkUDStrokes();
	afx_msg void OnChkStrokes();
	afx_msg void OnChkTrials();
	afx_msg void OnChkDiscWOLift();
	afx_msg void OnChkDiscWLift();
	DECLARE_MESSAGE_MAP()
};

// PrefPageUser.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PrefSheet.h"
#include "PrefPageUser.h"
#include "PasswordChgDlg.h"
#include "PrefPageGeneral.h"
#include "..\CTreeLink\DBCommon.h"
#include "..\DataMod\DataMod.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrefPageUser dialog

IMPLEMENT_DYNCREATE(PrefPageUser, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(PrefPageUser, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnKillfocusEditId)
	ON_EN_CHANGE(IDC_EDIT_ID, OnChangeEditId)
	ON_BN_CLICKED(IDC_BN_PASS, OnBnPass)
END_MESSAGE_MAP()

PrefPageUser::PrefPageUser( Preferences* pPref )
	: CBCGPPropertyPage(PrefPageUser::IDD), m_pPref( pPref )
{
	m_fCancel = FALSE;
	m_fNew = FALSE;
	m_fWarned = FALSE;

	if( m_pPref ) m_szID = m_pPref->pszID;
}

void PrefPageUser::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szUSER_DESC);
	DDX_Text(pDX, IDC_EDIT_SITEID, m_szSiteID);
	DDV_MaxChars(pDX, m_szSiteID, szCODE);
	DDX_Text(pDX, IDC_EDIT_SITEDESC, m_szSiteDesc);
	DDV_MaxChars(pDX, m_szSiteDesc, szDESC);
}

/////////////////////////////////////////////////////////////////////////////
// PrefPageUser message handlers

BOOL PrefPageUser::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	if( m_pPref )
	{
		m_szDesc = m_pPref->pszDesc;
		m_szSiteID = m_pPref->pszSiteID;
		m_szSiteDesc = m_pPref->pszSiteDesc;
	}
	UpdateData( FALSE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_USER );
	m_tooltip.SetTitle( _T("USER SETTINGS") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unique identifier of user."), _T("User ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Description/name of user."), _T("Description") );
	pWnd = GetDlgItem( IDC_BN_PASS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to change this user's privacy protection password."), _T("Privacy Protection Password") );
	pWnd = GetDlgItem( IDC_EDIT_SITEID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("An identifier for this site/location [DEFAULT=LOCAL]."), _T("Site ID") );
	pWnd = GetDlgItem( IDC_EDIT_SITEDESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("A description for this site/location [DEFAULT=Independent Use]."), _T("Site Description") );

	// Edit or New
	if( m_szID != _T("") )
	{
		pWnd = GetDlgItem( IDC_EDIT_ID );
		pWnd->EnableWindow( FALSE );
	}
	else m_fNew = TRUE;

	// if editing user that is not current user, we need to disable the password and site info
	CString szUser;
	::GetCurrentUser( szUser );
	if( !m_fNew && ( szUser != m_szID ) )
	{
		BCGPMessageBox( _T("Password, site, and input device information can only be altered if logged in as this user.") );
		pWnd = GetDlgItem( IDC_BN_PASS );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_SITEID );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_SITEDESC );
		pWnd->EnableWindow( FALSE );
	}

	INSMUser* pUser = NULL;
	HRESULT hr = CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
								   IID_INSMUser, (LPVOID*)&pUser );
	if( SUCCEEDED( hr ) )
	{
		BSTR bstrItem = NULL;
		hr = pUser->ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			pUser->get_UserID( &bstrItem );
			m_lstItems.AddTail( bstrItem );

			hr = pUser->GetNext();
		}
		pUser->Release();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL PrefPageUser::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL PrefPageUser::OnApply() 
{
	UpdateData( TRUE );

	PrefSheet* pSheet = (PrefSheet*)GetParent();

	if( m_szID == _T("") )
	{
		pSheet->SetActivePage( 0 );
		BCGPMessageBox( _T("The User ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID.GetLength() < 3 )
	{
		pSheet->SetActivePage( 0 );
		BCGPMessageBox( _T("ID must be 3 characters.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szDesc == _T("") )
	{
		pSheet->SetActivePage( 0 );
		BCGPMessageBox( _T("A description/name must be specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szSiteID == _T("") )
	{
		pSheet->SetActivePage( 0 );
		BCGPMessageBox( _T("A site ID must be specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SITEID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szSiteDesc == _T("") )
	{
		pSheet->SetActivePage( 0 );
		BCGPMessageBox( _T("A site description/name must be specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SITEDESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return FALSE;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		PrefSheet* pSheet = (PrefSheet*)GetParent();
		pSheet->SetActivePage( 0 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	PrefPageGeneral* p = (PrefPageGeneral*)pSheet->GetPage( 1 );
	if( m_fNew && ( p->m_hWnd == NULL ) )
	{
		BCGPMessageBox( _T("Please confirm data locations.") );
		pSheet->SetActivePage( 1 );
		return FALSE;
	}

	// force user to be aware that password has not been set if new user
	if( m_fNew && !m_fWarned )
	{
		CString szMsg;
		szMsg = _T("You have not set a password. The default is \'userpass\'.\nWould you like to change it?");
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
		{
			PrefSheet* pSheet = (PrefSheet*)GetParent();
			pSheet->SetActivePage( 0 );
			CWnd* pWnd = GetDlgItem( IDC_BN_PASS );
			pWnd->SetFocus();
			PostMessage( WM_COMMAND, MAKEWPARAM( IDC_BN_PASS, BN_CLICKED ), 0 );
			return FALSE;
		}
	}

	if( m_pPref )
	{
		strncpy_s( m_pPref->pszID, 4, m_szID, _TRUNCATE );
		strncpy_s( m_pPref->pszDesc, 26, m_szDesc, _TRUNCATE );
		strncpy_s( m_pPref->pszSiteID, 26, m_szSiteID, _TRUNCATE );
		strncpy_s( m_pPref->pszSiteDesc, 51, m_szSiteDesc, _TRUNCATE );
	}

	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL PrefPageUser::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL PrefPageUser::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newuser.html"), this );
	return TRUE;
}

void PrefPageUser::OnBnPass()
{
	if( !m_pPref ) return;

	PasswordChgDlg d( this );
	d.m_szPass = m_pPref->pszPass;
	if( m_fNew ) d.m_szPassOld = _T("userpass");
	m_fWarned = TRUE;
	if( d.DoModal() != IDOK ) return;

	strcpy_s( m_pPref->pszPass, 31, d.m_szPassNew );
}

void PrefPageUser::OnKillfocusEditId() 
{
	UpdateData( TRUE );
	if( m_fCancel ) return;

	if( m_szID.Left( 2 ) == _T("UU") )
	{
		BCGPMessageBox( _T("UUx users are protected.") );
		PrefSheet* pSheet = (PrefSheet*)GetParent();
		pSheet->SetActivePage( 0 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
		BCGPMessageBox( _T("This ID already exists.") );
	}
	else if( m_lstItems.Find( m_szID ) )
	{
		PrefSheet* pSheet = (PrefSheet*)GetParent();
		pSheet->SetActivePage( 0 );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
		BCGPMessageBox( _T("This ID already exists.") );
	}
	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
}

void PrefPageUser::OnChangeEditId() 
{
	UpdateData( TRUE );
	if( m_fNew && ( m_szID != _T("") ) )
	{
		CString szPath;
		::GetDataPath( szPath, m_pPref->fPrivate, TRUE );
		if( szPath[ szPath.GetLength() - 1 ] != '\\' ) szPath += _T("\\");
		szPath += m_szID;

		strcpy_s( m_pPref->pszPath, _MAX_PATH, szPath );
		szPath += _T("\\BACKUP");
		strcpy_s( m_pPref->pszBackup, _MAX_PATH, szPath );
	}
}

void PrefPageUser::OnCancel()
{
	m_fCancel = TRUE;

	CBCGPPropertyPage::OnCancel();
}
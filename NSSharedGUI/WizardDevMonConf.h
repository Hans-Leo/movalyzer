#pragma once

#include "NSTooltipCtrl.h"

// WizardDevMonConf dialog

class AFX_EXT_CLASS WizardDevMonConf : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardDevMonConf)

public:
	WizardDevMonConf();
	virtual ~WizardDevMonConf();

// Dialog Data
	enum { IDD = IDD_WIZ_DEV_MONCONF };
	int				m_nAspect;
	double			m_dHeightDisplay;
	double			m_dWidthDisplay;
	double			m_dDiagDisplay;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;
	CBCGPComboBox	m_cboChoose;
	CBCGPComboBox	m_cboAspect;

public:
	virtual BOOL PreTranslateMessage( MSG*  );
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
	BOOL DoHelp( HELPINFO* );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnCalc();
	afx_msg void OnEnKillfocusEditDiagonal();
	afx_msg void OnChangeChoose();
	afx_msg void OnChangeAspect();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);

	DECLARE_MESSAGE_MAP()
};

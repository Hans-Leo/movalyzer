// PasswordChgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PasswordChgDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PasswordChgDlg dialog

BEGIN_MESSAGE_MAP(PasswordChgDlg, CBCGPDialog)
END_MESSAGE_MAP()

PasswordChgDlg::PasswordChgDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(PasswordChgDlg::IDD, pParent)
{
	m_fForTempPass = FALSE;
	m_szPassOld = _T("");
	m_szPassNew = _T("");
	m_szPassNew2 = _T("");
	m_szPass = _T("");
}

void PasswordChgDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PASSOLD, m_szPassOld);
	DDV_MaxChars(pDX, m_szPassOld, 10);
	DDX_Text(pDX, IDC_EDIT_PASSNEW, m_szPassNew);
	DDV_MaxChars(pDX, m_szPassNew, 10);
	DDX_Text(pDX, IDC_EDIT_PASSNEW2, m_szPassNew2);
	DDV_MaxChars(pDX, m_szPassNew2, 10);
	DDX_Control(pDX, IDC_TXT_HELP, m_txtHelp);
}

/////////////////////////////////////////////////////////////////////////////
// PasswordChgDlg message handlers

BOOL PasswordChgDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// help string URL
	m_txtHelp.SetURL( _T("http://www.neuroscript.net/forum/showthread.php?t=859") );
	m_txtHelp.SizeToContent();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_SUBJ );
	m_tooltip.SetTitle( _T("CHANGE PASSWORD") );
	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );
	pWnd = GetDlgItem( IDC_EDIT_PASSOLD );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Enter current password here."), _T("Current Password") );
		if( m_fForTempPass ) pWnd->EnableWindow( FALSE );
	}
	pWnd = GetDlgItem( IDC_EDIT_PASSNEW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter new password here."), _T("New Password") );
	pWnd = GetDlgItem( IDC_EDIT_PASSNEW2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Reenter new password here."), _T("New Password") );

	// if old pass already set, set focus to new pass & hide old pass
	if( m_szPassOld != _T("") )
	{
		pWnd = GetDlgItem( IDC_EDIT_PASSOLD );
		pWnd->ShowWindow( SW_HIDE );
		pWnd = GetDlgItem( IDC_EDIT_PASSNEW );
		pWnd->SetFocus();
		return FALSE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL PasswordChgDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void PasswordChgDlg::OnOK()
{
	UpdateData( TRUE );

//	if( m_szPassOld == _T("") )
//	{
//		BCGPMessageBox( _T("You must enter the original password.") );
//		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASSOLD );
//		if( pWnd ) pWnd->SetFocus();
//		return;
//	}

//	if( m_szPassNew == _T("") )
//	{
//		BCGPMessageBox( _T("You must enter a new password.") );
//		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASSNEW );
//		if( pWnd ) pWnd->SetFocus();
//		return;
//	}

	if( !m_fForTempPass && ( m_szPassOld != m_szPass ) )
	{
		BCGPMessageBox( _T("You entered the incorrect original password.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASSOLD );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	if( m_szPassNew != m_szPassNew2 )
	{
		BCGPMessageBox( _T("You did not reenter your new password correctly.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASSNEW2 );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	if( m_szPassNew == _T("") )
	{
		if( !m_fForTempPass )
		{
			if( BCGPMessageBox( _T("You have entered an empty password.\n\nIs this what you want?"), MB_YESNO ) == IDNO )
			{
				CWnd* pWnd = GetDlgItem( IDC_EDIT_PASSNEW );
				pWnd->SetFocus();
				return;
			}
		}
		else
		{
			BCGPMessageBox( _T("You cannot use an empty password.") );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_PASSNEW );
			pWnd->SetFocus();
			return;
		}
	}

	CBCGPDialog::OnOK();
}

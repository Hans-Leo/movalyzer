// WizardExportSave.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardExportSave.h"
#include "WizardExportSheet.h"
#include "WizardExportExp.h"
#include "WizardExportUploadSubj.h"
#include "..\AdminMod\AdminMod.h"
#include "..\AdminMod\AdminMod_i.c"
#include <atlsafe.h>
#include <afxinet.h>

// WizardExportSave dialog

IMPLEMENT_DYNAMIC(WizardExportSave, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardExportSave, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_TEST, &WizardExportSave::OnBnClickedBnTest)
END_MESSAGE_MAP()

WizardExportSave::WizardExportSave() : CBCGPPropertyPage(WizardExportSave::IDD)
{
	m_szRemote = _T("ftp://www.domain.com");
#ifdef	_DEBUG
	m_szLogin = _T("neuroscript");
	// 4Apr11: hlt: Updated from nsdev1
	m_szPassword = _T("Neuro*4718");
#endif
}

WizardExportSave::~WizardExportSave()
{
}

void WizardExportSave::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_LOC_REMOTE, m_szRemote);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szPath);
	DDX_Text(pDX, IDC_EDIT_LOGIN, m_szLogin);
	DDX_Text(pDX, IDC_EDIT_PASSWORD, m_szPassword);
	DDX_Text(pDX, IDC_TXT_STATUS, m_szStatus);
}

// WizardExportSave message handlers


BOOL WizardExportSave::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.SetTitle( _T("Export Wizard") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_LOC_REMOTE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the desired remote FTP location to save the export files."), _T("FTP Address") );
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the relative path from the root directory of the FTP (can be empty)."), _T("FTP Relative Path") );
	pWnd = GetDlgItem( IDC_EDIT_LOGIN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Login (username) for remote location."), _T("Username") );
	pWnd = GetDlgItem( IDC_EDIT_PASSWORD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Password for remote location."), _T("Password") );

	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("UploadURL");
		m_szRemote = app->GetString( szKey, m_szRemote );
		if( m_szRemote == _T("") ) m_szRemote = _T("ftp://www.domain.com");
		szKey = _T("UploadPath");
		m_szPath = app->GetString( szKey, m_szPath );
		app->SetRegistryBase( szRB );
	}
	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardExportSave::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardExportSave::OnWizardFinish()
{
	UpdateData( TRUE );

	// create admin elevate object (which handles ftp)
	IAdminElevator* pAE = NULL;
	HRESULT hr = ::CoCreateInstance( __uuidof( AdminElevator ), NULL, CLSCTX_ALL,
									 IID_IAdminElevator, (LPVOID*)&pAE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create AdminElevate object") );
		return FALSE;
	}

	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	WizardExportExp* pExp = (WizardExportExp*)pParent->GetPage( 0 );
	WizardExportUploadSubj* pSubj = (WizardExportUploadSubj*)pParent->GetPage( 3 );

	// verify upload path has a / if set
	if( m_szPath != _T("") )
	{
		// leading
		if( ( m_szPath[ 0 ] != '\\' ) && ( m_szPath[ 0 ] != '/' ) )
		{
			CString szTmp = _T("/");
			szTmp += m_szPath;
			m_szPath = szTmp;
			UpdateData( FALSE );
		}
		// trailing
		if( ( m_szPath[ m_szPath.GetLength() - 1 ] != '\\' ) &&
			( m_szPath[ m_szPath.GetLength() - 1 ] != '/' ) )
		{
			m_szPath += _T("/");
			UpdateData( FALSE );
		}
	}

	// set upload locations to registry
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("UploadURL");
		szVal = m_szRemote;
		app->WriteString( szKey, szVal );
		szKey = _T("UploadPath");
		szVal = m_szPath;
		app->WriteString( szKey, szVal );
		app->SetRegistryBase( szRB );
	}

	// create list of files to be copied/uploaded
	CComSafeArray<BSTR> sa( (ULONG)( pSubj->m_lstSubjects.GetCount() ) );
	POSITION pos = pSubj->m_lstSubjects.GetHeadPosition();
	int nCount = 0;
	while( pos )
	{
		// extract exp,grp,subj
		CString szItem = pSubj->m_lstSubjects.GetNext( pos );
		sa[ nCount++ ] = szItem.AllocSysString();
	}

	// do upload
	CString szDataPath;
	::GetDataPathRoot( szDataPath );
	hr = pAE->DoUpload( szDataPath.AllocSysString(), m_szPath.AllocSysString(),
						m_szRemote.AllocSysString(), m_szLogin.AllocSysString(),
						m_szPassword.AllocSysString(), sa.GetSafeArrayPtr() );
	pAE->Release();

	// cleanup other page list resources (stupid MS cant support this naturally)
	if( SUCCEEDED( hr ) )
	{
		pExp->OnWizardFinish();
		pSubj->OnWizardFinish();
		return CBCGPPropertyPage::OnWizardFinish();
	}
	else return FALSE;
}

BOOL WizardExportSave::OnSetActive() 
{
	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardExportSave::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("export_wizard.html"), this );
	return TRUE;
}

BOOL WizardExportSave::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardExportSave::OnBnClickedBnTest()
{
	Test( TRUE );
}

BOOL WizardExportSave::Test( BOOL fInformOnSuccess )
{
	UpdateData( TRUE );

	BOOL fFail = FALSE;

	// verify location, login, password (warn for last 2)
	if( m_szRemote == _T("") )
	{
		BCGPMessageBox( _T("No remote location has been specified.") );
		return FALSE;
	}
	if( ( m_szLogin == _T("") ) || ( m_szPassword == _T("") ) )
	{
		if( BCGPMessageBox( _T("Login information has not been provided. Continue?"), MB_YESNO ) == IDNO )
			return FALSE;
	}

	// TEST CONNECTION
	CString szServerName, szObject;
	INTERNET_PORT nPort = 0;
	DWORD dwServiceType = 0;
	CFtpConnection* pFtpConnection = NULL;
	// create internet session
	CWaitCursor cursor;
	CInternetSession* pInetSession = new CInternetSession( _T("MovAlyzeR"), 1, PRE_CONFIG_INTERNET_ACCESS );
	if( !pInetSession )
	{
		BCGPMessageBox( _T("Cannot open an internet session.  Please check your internet configuration.") );
		fFail = TRUE;
		goto Cleanup;
	}
	// parse URL
	if( !AfxParseURL( m_szRemote, dwServiceType, szServerName, szObject, nPort ) )
	{
		BCGPMessageBox( _T("The remote location FTP URL is invalid.") );
		fFail = TRUE;
		goto Cleanup;
	}
	// connect
	if( ( dwServiceType != INTERNET_SERVICE_FTP ) || szServerName.IsEmpty() )
	{
		BCGPMessageBox( _T("The remote location FTP URL is invalid.") );
		fFail = TRUE;
		goto Cleanup;
	}
	try
	{
		pFtpConnection = pInetSession->GetFtpConnection( szServerName, m_szLogin, m_szPassword );
	}
	catch( CInternetException* ie )
	{
		TCHAR szErr[ 1024 ];
		if( ie->GetErrorMessage( szErr, 1024 ) )
			BCGPMessageBox( szErr );
		else
			BCGPMessageBox( _T("An exception occurred when attempting to create an FTP connection.\r\n  Please check your internet configuration and make sure your environment\r\n is set up with WININET.DLL in the path.") );
		ie->Delete();
		fFail = TRUE;
		goto Cleanup;
	}

	// success
	if( fInformOnSuccess ) BCGPMessageBox( _T("Username and password accepted.") );

	// cleanup
Cleanup:
	if( pFtpConnection )
	{
		pFtpConnection->Close();
		delete pFtpConnection;
	}
	if( pInetSession )
	{
		pInetSession->Close();
		delete pInetSession;
	}

	return !fFail;
}

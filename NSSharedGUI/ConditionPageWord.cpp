// ConditionPageWord.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ConditionPageWord.h"
#include "ConditionSheet.h"
#include "Conditions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ConditionPageWord property page

IMPLEMENT_DYNCREATE(ConditionPageWord, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageWord, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_CHK_PROCESS, OnChkProcess)
	ON_BN_CLICKED(IDC_CHK_RECORD, OnChkRecord)
	ON_CBN_SELCHANGE(IDC_CBO_PARENT, OnSelchangeCboParent)
	ON_EN_KILLFOCUS(IDC_EDIT_WORD, OnEnKillfocusEditWord)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ConditionPageWord::ConditionPageWord( INSCondition* pC, BOOL fNew,
									  CStringList* pList, CStringList* pListWord )
	: CBCGPPropertyPage(ConditionPageWord::IDD), m_pC( pC ), m_plstConds( pList ),
	  m_plstCondsWord( pListWord ), m_fNew( fNew )
{
	m_fRecord = TRUE;
	m_fProcess = TRUE;
	m_nWord = 1;

	m_chkRecord.SetDefaultAsOff( !m_fRecord );
	m_chkProcess.SetDefaultAsOff( !m_fProcess );

	if( !m_fNew && pC )
	{
		NSConditions::GetID( pC, m_szID );
		NSConditions::GetRecord( pC, m_fRecord );
		NSConditions::GetProcess( pC, m_fProcess );
		NSConditions::GetParent( pC, m_szParent );
		short nVal = 0;
		NSConditions::GetWord( pC, nVal );
		m_nWord = nVal;
	}
}

ConditionPageWord::~ConditionPageWord()
{
}

void ConditionPageWord::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_RECORD, m_fRecord);
	DDX_Check(pDX, IDC_CHK_PROCESS, m_fProcess);
	DDX_Control(pDX, IDC_CBO_PARENT, m_cboParent);
	DDX_Text(pDX, IDC_EDIT_WORD, m_nWord);

	DDX_Control(pDX, IDC_CHK_RECORD, m_chkRecord);
	DDX_Control(pDX, IDC_CHK_PROCESS, m_chkProcess);
}

/////////////////////////////////////////////////////////////////////////////
// ConditionPageWord message handlers

BOOL ConditionPageWord::OnApply() 
{
	if( !m_pC ) return FALSE;

	UpdateData( TRUE );

	if( !m_fRecord && !m_fProcess )
	{
		BCGPMessageBox( _T("This condition must record, process or extract, or both.") );
		CWnd* pWnd = GetDlgItem( IDC_CHK_RECORD );
		pWnd->SetFocus();
		return FALSE;
	}

	if( !m_fRecord && m_fProcess && ( ( m_nWord <= 0 ) || ( m_nWord > 500 ) ) )
	{
		BCGPMessageBox( _T("Word to process is out of range (1-500).") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_WORD );
		pWnd->SetFocus();
		return FALSE;
	}

	if( !m_fRecord && m_fProcess && ( m_szParent == _T("") ) )
	{
		BCGPMessageBox( _T("A parent condition must be specified.") );
		CWnd* pWnd = GetDlgItem( IDC_CBO_PARENT );
		pWnd->SetFocus();
		return FALSE;
	}

	m_pC->put_Record( m_fRecord );
	m_pC->put_Process( m_fProcess );
	m_pC->put_Word( m_nWord );
	m_pC->put_Parent( m_szParent.AllocSysString() );

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageWord::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageWord::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	
	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_CHK_RECORD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select to include this condition for recording." ), _T("Record") );
	pWnd = GetDlgItem( IDC_CHK_PROCESS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select to include this condition for processing or word extraction." ), _T("Process/Extract") );
	pWnd = GetDlgItem( IDC_EDIT_WORD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify which word to process of multi-word condition (1-200)." ), _T("Word") );
	pWnd = GetDlgItem( IDC_CBO_PARENT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the parent condition for this process-only condition." ), _T("Parent Condition") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	// Fill parent condition combo
	int nSelCur = -1, nPos;
	CString szItem, szID, szDelim;
	POSITION pos = m_plstConds ? m_plstConds->GetHeadPosition() : NULL;
	int nItem = -1;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	while( pos )
	{
		szItem = m_plstConds->GetNext( pos );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szID = szItem.Mid( nPos + 2 );
			if( szID != m_szID )
			{
				nItem = m_cboParent.AddString( szItem );
				if( szID == m_szParent ) nSelCur = nItem;
			}
		}
	}
	if( nSelCur != -1 ) m_cboParent.SetCurSel( nSelCur );

	// Field enable/disable depending on checks
	if( m_fRecord )
	{
		pWnd = GetDlgItem( IDC_EDIT_WORD );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_PARENT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ConditionPageWord::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageWord::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_wordextraction.html"), this );
	return TRUE;
}

void ConditionPageWord::OnChkProcess() 
{
	UpdateData( TRUE );

	if( !::IsSA() && !::IsOA() )
	{
		NSSecurity* pSecurity = ::GetSecurity();
		if( pSecurity && !pSecurity->AO() )
		{
			m_fProcess = TRUE;
			UpdateData( FALSE );
			return;
		}
	}

	BOOL fEnable = TRUE;
	if( m_fRecord || ( !m_fRecord && !m_fProcess ) )
		fEnable = FALSE;
	else
		fEnable = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_WORD );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_CBO_PARENT );
	if( pWnd ) pWnd->EnableWindow( fEnable );

	if( !fEnable )
	{
		m_nWord = 1;
		m_szParent = _T("");
		UpdateData( FALSE );
		m_cboParent.SetCurSel( -1 );
	}
}

void ConditionPageWord::OnChkRecord() 
{
	UpdateData( TRUE );

	if( !::IsSA() && !::IsOA() )
	{
		NSSecurity* pSecurity = ::GetSecurity();
		if( pSecurity && !pSecurity->AO() )
		{
			m_fRecord = TRUE;
			UpdateData( FALSE );
		}
	}

	// check if any NSConditions rely upon this one as a recording
	if( !m_fNew && !m_fRecord )
	{
		BOOL fFound = FALSE;
		CString szID, szParent, szMsg, szConds;
		POSITION pos = m_plstCondsWord->GetHeadPosition();
		while( pos )
		{
			szID = m_plstCondsWord->GetNext( pos );
			fFound = TRUE;
			if( szConds != _T("") )
			{
				szConds += _T(", ");
				szConds += szID;
			}
			else szConds = szID;
		}
		if( fFound )
		{
			szMsg.Format( _T("The following conditions use this conditon as a recording parent:\n\n%s.\n\nYou must change these to not use this condition as a parent before you can turn off recording."), szConds );
			BCGPMessageBox( szMsg );
			m_fRecord = TRUE;
		}
	}

	BOOL fEnable = TRUE;
	if( m_fRecord || ( !m_fRecord && !m_fProcess ) )
		fEnable = FALSE;
	else
		fEnable = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_WORD );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_CBO_PARENT );
	if( pWnd ) pWnd->EnableWindow( fEnable );

	if( !fEnable )
	{
		m_nWord = 1;
		m_szParent = _T("");
		UpdateData( FALSE );
		m_cboParent.SetCurSel( -1 );
	}
}

void ConditionPageWord::OnSelchangeCboParent() 
{
	int nSel = m_cboParent.GetCurSel();

	if( !::IsSA() && !::IsOA() )
	{
		NSSecurity* pSecurity = ::GetSecurity();
		if( pSecurity && !pSecurity->AO() )
		{
			m_fRecord = TRUE;
			m_nWord = 1;
			m_szParent = _T("");
			m_cboParent.SetCurSel( -1 );
			UpdateData( FALSE );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_WORD );
			if( pWnd ) pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_CBO_PARENT );
			if( pWnd ) pWnd->EnableWindow( FALSE );
			return;
		}
	}

	if( nSel != CB_ERR )
	{
		CString szDelim, szItem;
		::GetDelimiter( szDelim );
		TRIM( szDelim );

		m_cboParent.GetLBText( nSel, szItem );
		int nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return;
		m_szParent = szItem.Mid( nPos + 2 );
	}
}

void ConditionPageWord::OnEnKillfocusEditWord()
{
	if( !::IsSA() && !::IsOA() )
	{
		NSSecurity* pSecurity = ::GetSecurity();
		if( pSecurity && !pSecurity->AO() )
		{
			m_fRecord = TRUE;
			m_nWord = 1;
			m_szParent = _T("");
			m_cboParent.SetCurSel( -1 );
			UpdateData( FALSE );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_WORD );
			if( pWnd ) pWnd->EnableWindow( FALSE );
			pWnd = GetDlgItem( IDC_CBO_PARENT );
			if( pWnd ) pWnd->EnableWindow( FALSE );
			return;
		}
	}
}

void ConditionPageWord::OnBnReset() 
{
	m_fRecord = TRUE;
	m_fProcess = TRUE;
	m_nWord = 1;

	UpdateData( FALSE );

	CComboBox* pCB = (CComboBox*)GetDlgItem( IDC_CBO_PARENT );
	pCB->SetCurSel( -1 );

	if( m_fRecord )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_WORD );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_PARENT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
}

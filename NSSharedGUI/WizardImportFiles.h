#pragma once

#include "NSTooltipCtrl.h"

// WizardImportFiles dialog

class AFX_EXT_CLASS WizardImportFiles : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardImportFiles)

public:
	WizardImportFiles();
	virtual ~WizardImportFiles();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_FILES };
	CBCGPListCtrl	m_list;
	CImageList		m_ImageList;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fProcess;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();

	// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );

protected:
	void FillList();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult);
};

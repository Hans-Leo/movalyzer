#pragma once

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

// ConditionPageFeedback dialog

interface INSCondition;

class AFX_EXT_CLASS ConditionPageFeedback : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ConditionPageFeedback)

public:
	ConditionPageFeedback( INSCondition* = NULL, BOOL fNew = TRUE );
	~ConditionPageFeedback();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_FEEDBACK };
	BOOL			m_CountStrokes;
	BOOL			m_HideFeedback;
	BOOL			m_HideShowAfter;
	BOOL			m_HideShowAfterShow;
	BOOL			m_HideShowAfterStrokes;
	double			m_HideShowCount;
	BOOL			m_Transform;
	double			m_Xgain;
	double			m_Ygain;
	double			m_Xrotation;
	double			m_Yrotation;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;
	CBCGPComboBox	m_cboFB;
	CString			m_szFB;
	CBCGPButton		m_bnCalc;
	CBCGPButton		m_bnAdd;
	CBCGPButton		m_bnEdit;
	CBCGPButton		m_bnRel;
	CColoredButton	m_chkCountStrokes;
	CColoredButton	m_chkHideFeedback;
	CColoredButton	m_chkHideShowAfter;
	CColoredButton	m_rdoHide;
	CColoredButton	m_rdoShow;
	CColoredButton	m_rdoSeconds;
	CColoredButton	m_rdoStrokes;
	CColoredEdit	m_editHideShowCount;
	CColoredButton	m_chkTransform;
	CColoredEdit	m_editXgain;
	CColoredEdit	m_editYgain;
	CColoredEdit	m_editXrot;
	CColoredEdit	m_editYrot;

public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void FillFeedbacks();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedChkVisibility();
	afx_msg void OnBnClickedChkHideshowafter();
	afx_msg void OnBnClickedChkTransform();
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnEdit();
	afx_msg void OnClickBnRel();
	afx_msg void OnBnClickedBnCalc();
	afx_msg void OnCbnSelchangeCboFeedback();
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

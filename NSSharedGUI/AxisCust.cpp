// AxisCust.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "AxisCust.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AxisCust dialog

BEGIN_MESSAGE_MAP(AxisCust, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_DEFAULT, OnBnDefault)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
	ON_BN_CLICKED(IDC_BN_CHANGE, OnBnChange)
END_MESSAGE_MAP()

AxisCust::AxisCust(CStringList* pD, CStringList* pU, int nSel, CWnd* pParent /*=NULL*/)
	: CBCGPDialog(AxisCust::IDD, pParent), m_pLstD( pD ), m_pLstU( pU ), m_nSel( nSel )
{
	m_szVal = _T("");
	m_szDesc = _T("");
}


void AxisCust::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_lst);
	DDX_Text(pDX, IDC_EDIT_AXIS, m_szVal);
	DDX_Text(pDX, IDC_TXT_DESC, m_szDesc);
}

/////////////////////////////////////////////////////////////////////////////
// AxisCust message handlers

void AxisCust::OnBnDefault() 
{
	m_lstT.RemoveAll();
	POSITION pos = m_pLstD->GetHeadPosition();
	CString szItem;
	while( pos )
	{
		szItem = m_pLstD->GetNext( pos );
		m_lstT.AddTail( szItem );
	}
	OnSelchangeList();

	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->EnableWindow( TRUE );
}

void AxisCust::OnSelchangeList() 
{
	int nItem = m_lst.GetCurSel();
	POSITION pos = m_lstT.GetHeadPosition();
	POSITION posDesc = m_lstDesc.GetHeadPosition();
	CString szItem, szDesc;
	int nCnt = 0;
	while( pos )
	{
		szItem = m_lstT.GetNext( pos );
		szDesc = m_lstDesc.GetNext( posDesc );
		if( nItem == nCnt )
		{
			m_szVal = szItem;
			m_szDesc = szDesc;
			UpdateData( FALSE );

			CWnd* pWnd = GetDlgItem( IDC_EDIT_AXIS );
			if( pWnd )
			{
				pWnd->SetFocus();
				CEdit* pEdit = (CEdit*)pWnd;
				pEdit->SetSel( 0, -1 );
			}

			break;
		}
		nCnt++;
	}
}

BOOL AxisCust::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_CHART );
	m_tooltip.SetTitle( _T("ANALYSIS - AXIS CUSTOMIZATION") );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Shows the default axis label values."), _T("Default Axis Labels")  );
	pWnd = GetDlgItem( IDC_EDIT_AXIS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter custom axis lable value here."), _T("Custom Axis Label") );
	pWnd = GetDlgItem( IDC_BN_CHANGE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Change selected axis label to custom."), _T("Customize") );
	pWnd = GetDlgItem( IDC_BN_DEFAULT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Reset all axis label values to default values."), _T("Reset") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	// Fill list
	POSITION pos = m_pLstD ? m_pLstD->GetHeadPosition() : NULL;
	CString szItem;
	while( pos )
	{
		szItem = m_pLstD->GetNext( pos );
		m_lst.AddString( szItem );
	}
	pos = m_pLstU ? m_pLstU->GetHeadPosition() : NULL;
	while( pos )
	{
		szItem = m_pLstU->GetNext( pos );
		m_lstT.AddTail( szItem );
	}
	szItem = _T("Group, Session");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Subject, Patient, Participant");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Condition, Word, Complexity, Speed, Size,...");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Trial, Replication");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Stroke, Submovement");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Max number of strokes");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Start Time (s)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Duration (s)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Start Vertical Position (cm)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Vertical Size (cm)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Peak Vertical Acceleration (cm/s**2)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Peak Vertical Velocity (cm/s)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Start Horizontal Position (cm)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Relative Penup Duration ");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Straightness Error");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Slant");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Efficiency of Vertical Acceleration/Normalized Jerk");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Loop Surface (cm**2)");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Relative slant of the first 80 ms");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Relative Time of Vertical Peak Velocity");
	m_lstDesc.AddTail( szItem );
	szItem = _T("Number of Observations");
	m_lstDesc.AddTail( szItem );

	if( m_nSel != -1 )
	{
		m_lst.SetCurSel( m_nSel );
		OnSelchangeList();
		CWnd* pWnd = GetDlgItem( IDC_EDIT_AXIS );
		if( pWnd )
		{
			pWnd->SetFocus();
			CEdit* pEdit = (CEdit*)pWnd;
			pEdit->SetSel( 0, -1 );
		}
		return FALSE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL AxisCust::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void AxisCust::OnBnChange() 
{
	UpdateData( TRUE );

	int nItem = m_lst.GetCurSel();
	POSITION pos = m_lstT.GetHeadPosition(), posL = NULL;
	int nCnt = 0;
	CString szItem;
	while( pos )
	{
		posL = pos;
		szItem = m_lstT.GetNext( pos );
		if( nCnt == nItem )
		{
			m_lstT.RemoveAt( posL );
			m_lstT.InsertBefore( pos, m_szVal );
			break;
		}
		nCnt++;
	}

	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->EnableWindow( TRUE );
}

///////////////////////////////////////////////////////////////////////////////
// Backup.cpp

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include <direct.h>
#include "Backup.h"
#include "Users.h"
#include "Progress.h"
#include "Experiments.h"
#include "..\Common\UserObj.h"


static BOOL _TerminateThread = FALSE;

void SetTerminateBackupThread( BOOL fTerm )
{
	_TerminateThread = fTerm;
}

//************************************
// Method:    BackupSubject
// FullName:  BackupSubject
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: BOOL fCopy
// Parameter: int & nCount
// Parameter: BOOL fDisp
//************************************
UINT BackupSubject( CString szExp, CString szGrp, CString szSubj, BOOL fCopy, int& nCount, BOOL fDisp )
{
	CFileFind ff;
	CString szOrig, szNew, szFile, szRoot, szBackup, szTemp;
	BOOL fCont = FALSE;
	long nCur = 0, nSteps = 0;

	::GetDataPathRoot( szRoot );
	::GetBackupPath( szBackup );
	Progress::GetProgress( nCur, nSteps );

	// Verify directory first
	szFile.Format( _T("%s\\%s\\%s\\%s"), szBackup, szExp, szGrp, szSubj );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return ACTION_NONE;
		}
	}

	// SEQ files
	szFile.Format( _T("%s\\%s\\%s\\%s\\*.seq"), szRoot, szExp, szGrp, szSubj );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
			szSubj, ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// DIS files
	szFile.Format( _T("%s\\%s\\%s\\%s\\*.dis"), szRoot, szExp, szGrp, szSubj );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
			szSubj, ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// PCX files
	szFile.Format( _T("%s\\%s\\%s\\%s\\*.pcx"), szRoot, szExp, szGrp, szSubj );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szTemp = szOrig;
		szTemp.MakeUpper();
		if( szTemp.Find( _T("-F.PCX") ) < 0 )
		{
			szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
				szSubj, ff.GetFileName() );
			if( fCopy )
			{
				CopyFile( szOrig, szNew, FALSE );
				if( fDisp )
				{
					nCur++;
					Progress::SetProgress( nCur, ff.GetFileName() );
				}
			}
			else nCount++;
		}
	}

	// HWR/FRD files
	if( ::IsGripperModeOn() )
		szFile.Format( _T("%s\\%s\\%s\\%s\\*.frd"), szRoot, szExp, szGrp, szSubj );
	else
		szFile.Format( _T("%s\\%s\\%s\\%s\\*.hwr"), szRoot, szExp, szGrp, szSubj );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
			szSubj, ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// Target results files (TGT)
	szFile.Format( _T("%s\\%s\\%s\\%s\\*.tgt"), szRoot, szExp, szGrp, szSubj );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
			szSubj, ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	return ACTION_BACKUP;
}

//************************************
// Method:    BackupGroup
// FullName:  BackupGroup
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: BOOL fCopy
// Parameter: int & nCount
// Parameter: BOOL fDisp
//************************************
UINT BackupGroup( CString szExp, CString szGrp, BOOL fCopy, int& nCount, BOOL fDisp )
{
	CFileFind ff;
	CString szOrig, szNew, szRoot, szBackup, szFile, szSubj;
	BOOL fCont = FALSE;
	long nCur = 0, nSteps = 0;

	::GetDataPathRoot( szRoot );
	::GetBackupPath( szBackup );
	Progress::GetProgress( nCur, nSteps );

	// Verify directory first
	szFile.Format( _T("%s\\%s\\%s"), szBackup, szExp, szGrp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return ACTION_NONE;
		}
	}

	// TXT files
	szFile.Format( _T("%s\\%s\\%s\\*.txt"), szRoot, szExp, szGrp );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
			ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// RTF files
	szFile.Format( _T("%s\\%s\\%s\\*.rtf"), szRoot, szExp, szGrp );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s\\%s"), szBackup, szExp, szGrp,
			ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// SUBJECTS
	szFile.Format( _T("%s\\%s\\%s\\*.*"), szRoot, szExp, szGrp );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		// PER SUBJECT
		fCont = ff.FindNextFile();
		szSubj = ff.GetFileName();
		if( ff.IsDirectory() && ( szSubj != _T(".") ) && ( szSubj != _T("..") ) )
		{
			if( BackupSubject( szExp, szGrp, szSubj, fCopy, nCount, fDisp ) != ACTION_BACKUP )
				return ACTION_NONE;
		}
	}

	return ACTION_BACKUP;
}


//************************************
// Method:    BackupExperiment
// FullName:  BackupExperiment
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: CString szExp
// Parameter: BOOL fCopy
// Parameter: int & nCount
// Parameter: BOOL fDisp
//************************************
UINT BackupExperiment( CString szExp, BOOL fCopy, int& nCount, BOOL fDisp )
{
	CFileFind ff;
	CString szOrig, szNew, szRoot, szBackup, szFile, szGrp;
	BOOL fCont = FALSE;
	long nCur = 0, nSteps = 0;

	::GetDataPathRoot( szRoot );
	::GetBackupPath( szBackup );
	Progress::GetProgress( nCur, nSteps );

	// Verify directory first
	szFile.Format( _T("%s\\%s"), szBackup, szExp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return ACTION_NONE;
		}
	}

	// TXT files
	szFile.Format( _T("%s\\%s\\*.txt"), szRoot, szExp );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s"), szBackup, szExp, ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// RTF files
	szFile.Format( _T("%s\\%s\\*.rtf"), szRoot, szExp );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s\\%s"), szBackup, szExp, ff.GetFileName() );
		if( fCopy )
		{
			CopyFile( szOrig, szNew, FALSE );
			if( fDisp )
			{
				nCur++;
				Progress::SetProgress( nCur, ff.GetFileName() );
			}
		}
		else nCount++;
	}

	// GROUPS
	szFile.Format( _T("%s\\%s\\*.*"), szRoot, szExp );
	fCont = ff.FindFile( szFile );
	while( fCont && !_TerminateThread )
	{
		// PER GROUP
		fCont = ff.FindNextFile();
		szGrp = ff.GetFileName();
		if( ff.IsDirectory() && ( szGrp != _T(".") ) && ( szGrp != _T("..") ) )
		{
			if( BackupGroup( szExp, szGrp, fCopy, nCount, fDisp ) != ACTION_BACKUP )
				return ACTION_NONE;
		}
	}

	return ACTION_BACKUP;
}

//************************************
// Method:    BackupSystem
// FullName:  BackupSystem
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void BackupSystem()
{
	UserObj* pObj = ::GetCurrentUserObj();
	if( !pObj ) return;
	if( !NSMUsers::VerifyBackupPath() ) return;

	OutputAMessage( _T("Backup system."), TRUE );
	pObj->m_nState = STATE_BACKUP;

	Experiments exp;

	CString szIDs, szFile;
	BOOL fCopy = FALSE;
	int nCount = 0;
	UINT nRes = ACTION_BACKUP;

BeginHere:
	if( fCopy )
	{
		// Progress Meter steps
		Progress::EnableProgress( nCount );
		nCount = 0;
	}

	// Backup
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) && ( nRes == ACTION_BACKUP ) && !_TerminateThread )
	{
		exp.GetID( szIDs );
		nRes = BackupExperiment( szIDs, fCopy, nCount, FALSE );
		hr = exp.GetNext();
	}

	if( !fCopy )
	{
		fCopy = TRUE;
		goto BeginHere;
	}
	else
	{
		IBackup* pB = NULL;
		HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
									   IID_IBackup, (LPVOID*)&pB );
		if( FAILED( hr ) )
		{
			// stop progress / cleanup
			Progress::DisableProgress();
			return;
		}

		CString szItem;
		::GetCurrentUser( szItem );
		pB->put_UserID( szItem.AllocSysString() );
		COleDateTime d = COleDateTime::GetCurrentTime();
		szItem = d.Format( _T("%m-%d-%Y %H:%M:%S") );
		pB->put_BackupDate( szItem.AllocSysString() );
		::GetBackupPath( szItem );
		pB->put_Path( szItem.AllocSysString() );
		pB->put_BackupType( eB_Group );
		pB->put_Exp( szIDs.AllocSysString() );
		szFile = _T("System backup.");
		pB->put_Notes( szFile.AllocSysString() );
		hr = pB->Add();
		if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to add backup to backup history table.") );
		pB->Release();
	}

	// stop progress / cleanup
	Progress::DisableProgress();
}

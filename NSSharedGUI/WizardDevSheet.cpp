// WizardDevSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardDevSheet.h"
#include "WizardDevStart.h"
#include "WizardDevDevices.h"
#include "WizardDevConfDev.h"
#include "WizardDevMonConf.h"
#include "WizardDevMapping.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardDevSheet

IMPLEMENT_DYNAMIC(WizardDevSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardDevSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardDevSheet::WizardDevSheet( CWnd* pParentWnd, UINT iSelectPage )
	: CBCGPPropertySheet( _T("MovAlyzeR Device Setup Wizard"), pParentWnd, iSelectPage )
{
	AddPages();
}

WizardDevSheet::~WizardDevSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void WizardDevSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	m_dHeightDisplay = 12. * 2.54;
	m_dWidthDisplay = 16. * 2.54;
	m_fRecWin = TRUE;

	AddPage( new WizardDevStart );
	AddPage( new WizardDevMonConf );
	AddPage( new WizardDevDevices );
	AddPage( new WizardDevConfDev );
	AddPage( new WizardDevMapping );

	SetWizardMode();
}

/////////////////////////////////////////////////////////////////////////////
// WizardDevSheet message handlers

BOOL WizardDevSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardDevStart ) ) )
			return ((WizardDevStart*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardDevDevices ) ) )
			return ((WizardDevDevices*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardDevConfDev ) ) )
			return ((WizardDevConfDev*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardDevMonConf ) ) )
			return ((WizardDevMonConf*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardDevMapping ) ) )
			return ((WizardDevMapping*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

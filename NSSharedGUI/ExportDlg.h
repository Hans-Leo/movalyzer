#pragma once

// ExportDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ExportDlg dialog

class AFX_EXT_CLASS ExportDlg : public CBCGPDialog
{
// Construction
public:
	ExportDlg(CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_EXPORT };
	CString			m_szFile;
	CBCGPEdit		m_wndFolderEdit;
	CString			m_szExp;
	CString			m_szGrp;
	CString			m_szSubj;
	static BOOL		m_fFiles;
	static BOOL		m_fTrials;
	static BOOL		m_fMembers;
	static BOOL		m_fShow;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnClickTrials();
	afx_msg void OnClickMembers();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

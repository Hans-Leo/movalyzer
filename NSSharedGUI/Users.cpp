// Users.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PrefSheet.h"
#include "OverwriteDlg.h"
#include "Users.h"
#include "Subjects.h"
#include "Stimuli.h"
#include "Elements.h"
#include "Conditions.h"
#include <direct.h>
#include "..\Common\Path.h"
#include "Progress.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// page for opening the property sheet
int		_nPage = 0;

///////////////////////////////////////////////////////////////////////////////
// Implementation

// scripts for quickness (TODO: port to other class objects)
#define	DO_GET_STRING( theinterface, theitem ) \
	void theinterface##s::Get##theitem( I##theinterface* pObj, CString& val ) \
	{ if( pObj ) { BSTR bstr; pObj->get_##theitem( &bstr ); val = bstr; } }
#define	DO_GET_SHORT( theinterface, theitem ) \
	void theinterface##s::Get##theitem( I##theinterface* pObj, short& val ) \
	{ if( pObj ) { pObj->get_##theitem( &val ); } }
#define	DO_GET_BOOL( theinterface, theitem ) \
	void theinterface##s::Get##theitem( I##theinterface* pObj, BOOL& val ) \
	{ if( pObj ) { pObj->get_##theitem( &val ); } }
#define	DO_GET_DOUBLE( theinterface, theitem ) \
	void theinterface##s::Get##theitem( I##theinterface* pObj, double& val ) \
	{ if( pObj ) { pObj->get_##theitem( &val ); } }


///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

DO_GET_STRING( NSMUser, UserID );
DO_GET_STRING( NSMUser, Description );
DO_GET_STRING( NSMUser, RootPath );
DO_GET_STRING( NSMUser, BackupPath );
DO_GET_STRING( NSMUser, Delimiter );
DO_GET_SHORT( NSMUser, InputType );
DO_GET_SHORT( NSMUser, TabletMode );
DO_GET_BOOL( NSMUser, EntireTablet );
DO_GET_BOOL( NSMUser, Log );
DO_GET_BOOL( NSMUser, Alpha );
DO_GET_DOUBLE( NSMUser, TabletWidth );
DO_GET_DOUBLE( NSMUser, TabletHeight );
DO_GET_DOUBLE( NSMUser, DisplayWidth );
DO_GET_DOUBLE( NSMUser, DisplayHeight );
DO_GET_DOUBLE( NSMUser, AspectRatioWidth );
DO_GET_DOUBLE( NSMUser, AspectRatioHeight );
DO_GET_STRING( NSMUser, CommPort );
DO_GET_BOOL( NSMUser, AutoUpdate );
DO_GET_BOOL( NSMUser, Private );
DO_GET_STRING( NSMUser, WinUser );
DO_GET_BOOL( NSMUser, Virgin );
DO_GET_BOOL( NSMUser, GenerateSubjectIDs );
DO_GET_STRING( NSMUser, SubjectStartID );
DO_GET_STRING( NSMUser, SubjectEndID );
DO_GET_STRING( NSMUser, SubjectCurID );
DO_GET_BOOL( NSMUser, Encrypted );
DO_GET_SHORT( NSMUser, EncryptionMethod );
DO_GET_STRING( NSMUser, SiteID );
DO_GET_STRING( NSMUser, SiteDesc );

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::PutSignature( CString szVal, BOOL fSkipEncode )
{
	if( m_pNSMUser )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pNSMUser->put_Signature( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::GetSignature( CString& szVal, BOOL fSkipDecode )
{
	NSMUsers::GetSignature( m_pNSMUser, szVal, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::GetSignature( INSMUser* pNSMUser, CString& szVal, BOOL fSkipDecode )
{
	if( pNSMUser )
	{
		BSTR bstrItem;
		pNSMUser->get_Signature( &bstrItem );
		short nMethod = -1;
		pNSMUser->get_EncryptionMethod( &nMethod );
		BOOL fEnc = FALSE;
		pNSMUser->get_Encrypted( &fEnc );
		szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::PutSysPass( CString szVal, BOOL fSkipEncode )
{
	if( m_pNSMUser )
	{
		if( !fSkipEncode ) szVal = Objects::Encode( szVal );
		m_pNSMUser->put_SysPass( szVal.AllocSysString() );
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::GetSysPass( CString& szVal, BOOL fSkipDecode )
{
	NSMUsers::GetSysPass( m_pNSMUser, szVal, fSkipDecode );
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::GetSysPass( INSMUser* pNSMUser, CString& szVal, BOOL fSkipDecode )
{
	if( pNSMUser )
	{
		BSTR bstrItem;
		pNSMUser->get_SysPass( &bstrItem );
		short nMethod = -1;
		pNSMUser->get_EncryptionMethod( &nMethod );
		BOOL fEnc = FALSE;
		pNSMUser->get_Encrypted( &fEnc );
		szVal = fSkipDecode ? bstrItem : Objects::Decode( bstrItem, fEnc, nMethod );
	}
}

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::ResetSubjectIDStart()
{
	NSMUsers::ResetSubjectIDStart( m_pNSMUser );
}

void NSMUsers::ResetSubjectIDStart( INSMUser* pUser )
{
	if( !pUser ) return;
	BSTR bstr = NULL;
	pUser->get_SubjectStartID( &bstr );
	pUser->put_SubjectCurID( bstr );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::GetNext()
{
	return NSMUsers::GetNext( m_pNSMUser );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Find( CString szID )
{
	return NSMUsers::Find( m_pNSMUser, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Add()
{
	return NSMUsers::Add( m_pNSMUser );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Modify()
{
	return NSMUsers::Modify( m_pNSMUser );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Remove()
{
	return NSMUsers::Remove( m_pNSMUser );
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::SetDataPath( CString szPath )
{
	NSMUsers::SetDataPath( m_pNSMUser, szPath );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::ResetToStart()
{
	return NSMUsers::ResetToStart( m_pNSMUser );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::ResetToStart( INSMUser* pUser )
{
	if( pUser ) return pUser->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::GetNext( INSMUser* pUser )
{
	if( pUser ) return pUser->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Find( INSMUser* pUser, CString szID )
{
	if( pUser ) return pUser->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Add( INSMUser* pUser )
{
	if( !pUser ) return E_FAIL;

	if( FAILED( pUser->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" NSMUser::Add") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Modify( INSMUser* pUser )
{
	if( !pUser ) return E_FAIL;

	if( FAILED( pUser->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSMUsers::Remove( INSMUser* pUser )
{
	if( !pUser ) return E_FAIL;

	if( FAILED( pUser->Remove() ) )
	{
		BCGPMessageBox( _T("Unable to delete group.") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::SetDataPath( INSMUser* pUser, CString szPath )
{
	if( pUser ) pUser->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::DoAdd( CWnd* pOwner )
{
	return NSMUsers::DoAdd( m_pNSMUser, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::DoAdd( INSMUser* pUser, CWnd* pOwner )
{
	if( !pUser ) return FALSE;

	PrefSheet d( _T(""), pOwner, 0, FALSE );
	if( d.DoModal() == IDOK )
	{
		CString szTemp = d.m_pref.pszID;
		pUser->put_UserID( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszDesc;
		pUser->put_Description( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszPath;
		pUser->put_RootPath( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszBackup;
		pUser->put_BackupPath( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszDelim;
		pUser->put_Delimiter( szTemp.AllocSysString() );
		pUser->put_InputType( d.m_pref.ip );
		pUser->put_TabletMode( d.m_pref.tm );
		pUser->put_EntireTablet( d.m_pref.fEntireTablet );
		pUser->put_Log( d.m_pref.fLog );
		pUser->put_Alpha( d.m_pref.fAlpha );
		szTemp = d.m_pref.pszPass;
		pUser->put_SysPass( ( Objects::Encode( szTemp ) ).AllocSysString() );
		pUser->put_AspectRatioWidth( d.m_pref.dAR_X );
		pUser->put_AspectRatioHeight( d.m_pref.dAR_Y );
		pUser->put_TabletWidth( d.m_pref.dT_X );
		pUser->put_TabletHeight( d.m_pref.dT_Y );
		pUser->put_DisplayWidth( d.m_pref.dD_X );
		pUser->put_DisplayHeight( d.m_pref.dD_Y );
		szTemp = d.m_pref.pszComPort;
		pUser->put_CommPort( szTemp.AllocSysString() );
		pUser->put_AutoUpdate( d.m_pref.fAutoUpdate );
		pUser->put_Private( d.m_pref.fPrivate );
		szTemp = d.m_pref.pszWinUser;
		pUser->put_WinUser( szTemp.AllocSysString() );
		pUser->put_GenerateSubjectIDs( d.m_pref.fGenSubjID );
		szTemp = d.m_pref.pszSubjStartID;
		pUser->put_SubjectStartID( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszSubjEndID;
		pUser->put_SubjectEndID( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszSubjCurID;
		pUser->put_SubjectCurID( szTemp.AllocSysString() );
		pUser->put_Encrypted( TRUE );
		pUser->put_EncryptionMethod( (short)::GetEncryptionMethod() );
		szTemp = d.m_pref.pszSiteID;
		pUser->put_SiteID( szTemp.AllocSysString() );
		szTemp = d.m_pref.pszSiteDesc;
		pUser->put_SiteDesc( szTemp.AllocSysString() );
		UUID uuid;
		::UuidCreate( &uuid );
		RPC_CSTR pszUuid;
		::UuidToString( &uuid, &pszUuid );
		szTemp = pszUuid;
		::RpcStringFree( &pszUuid );
		pUser->put_Signature( ( Objects::Encode( szTemp ) ).AllocSysString() );

		if( SUCCEEDED( NSMUsers::Add( pUser ) ) ) return TRUE;
		else {
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" NSMUser::DoAdd") );
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::DoEdit( CString szID, CWnd* pOwner )
{
	_nPage = m_nPage;
	return NSMUsers::DoEdit( m_pNSMUser, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::DoEdit( INSMUser* pUser, CString szID, CWnd* pOwner )
{
	if( !pUser ) return FALSE;
	if( FAILED( NSMUsers::Find( pUser, szID ) ) ) return FALSE;

	CString szDesc, szPath, szBackup, szDelim, szPass, szCommPort, szWinUser, szSiteID, szSiteDesc, szSig;
	CString szStart, szEnd, szCur, szItem;
	BSTR bstrItem = NULL;
	short nItem;

	NSMUsers::GetDescription( pUser, szDesc );
	NSMUsers::GetRootPath( pUser, szPath );
	NSMUsers::GetBackupPath( pUser, szBackup );
	NSMUsers::GetDelimiter( pUser, szDelim );
	TRIM( szDelim );
	NSMUsers::GetSysPass( pUser, szPass, FALSE );
	NSMUsers::GetCommPort( pUser, szCommPort );
	NSMUsers::GetWinUser( pUser, szWinUser );
	NSMUsers::GetSubjectStartID( pUser, szStart );
	NSMUsers::GetSubjectEndID( pUser, szEnd );
	NSMUsers::GetSubjectCurID( pUser, szCur );
	NSMUsers::GetSiteID( pUser, szSiteID );
	NSMUsers::GetSiteDesc( pUser, szSiteDesc );
	NSMUsers::GetSignature( pUser, szSig, FALSE );

	PrefSheet d( szID, pOwner, ( ( _nPage >= 0 ) && ( _nPage <= 3 ) ) ? _nPage : 0 , ::IsGripperModeOn() );
	::GetPreferences( &d.m_pref, &d.m_gs );
	strncpy_s( d.m_pref.pszID, 4, szID, _TRUNCATE );
	strncpy_s( d.m_pref.pszDesc, 26, szDesc, _TRUNCATE );
	strncpy_s( d.m_pref.pszPath, _MAX_PATH, szPath, _TRUNCATE );
	strncpy_s( d.m_pref.pszBackup, _MAX_PATH, szBackup, _TRUNCATE );
	strncpy_s( d.m_pref.pszDelim, 2, szDelim, _TRUNCATE );
	pUser->get_InputType( &nItem );
	d.m_pref.ip = (Preferences::eInputType)nItem;
	NSMUsers::GetLog( pUser, d.m_pref.fLog );
	NSMUsers::GetAlpha( pUser, d.m_pref.fAlpha );
	pUser->get_TabletMode( &nItem );
	d.m_pref.tm = (Preferences::eTabletMode)nItem;
	NSMUsers::GetEntireTablet( pUser, d.m_pref.fEntireTablet );
	strncpy_s( d.m_pref.pszPass, 31, szPass, _TRUNCATE );
	NSMUsers::GetAspectRatioWidth( pUser, d.m_pref.dAR_X );
	NSMUsers::GetAspectRatioHeight( pUser, d.m_pref.dAR_Y );
	NSMUsers::GetTabletWidth( pUser, d.m_pref.dT_X );
	NSMUsers::GetTabletHeight( pUser, d.m_pref.dT_Y );
	NSMUsers::GetDisplayWidth( pUser, d.m_pref.dD_X );
	NSMUsers::GetDisplayHeight( pUser, d.m_pref.dD_Y );
	strncpy_s( d.m_pref.pszComPort, 32, szCommPort, _TRUNCATE );
	NSMUsers::GetAutoUpdate( pUser, d.m_pref.fAutoUpdate );
	NSMUsers::GetPrivate( pUser, d.m_pref.fPrivate );
	NSMUsers::GetGenerateSubjectIDs( pUser, d.m_pref.fGenSubjID );
	strncpy_s( d.m_pref.pszWinUser, 50, szWinUser, _TRUNCATE );
	strncpy_s( d.m_pref.pszSubjStartID, 4, szStart, _TRUNCATE );
	strncpy_s( d.m_pref.pszSubjEndID, 4, szEnd, _TRUNCATE );
	strncpy_s( d.m_pref.pszSubjCurID, 4, szCur, _TRUNCATE );
	NSMUsers::GetEncrypted( pUser, d.m_pref.fEncrypted );
	NSMUsers::GetEncryptionMethod( pUser, d.m_pref.nEncryptionMethod );
	strncpy_s( d.m_pref.pszSiteID, 26, szSiteID, _TRUNCATE );
	strncpy_s( d.m_pref.pszSiteDesc, 51, szSiteDesc, _TRUNCATE );
	strncpy_s( d.m_pref.pszSignature, 151, szSig, _TRUNCATE );

	if( d.DoModal() == IDOK )
	{
		INSMUser* pUser2 = NULL;
		HRESULT hr = ::CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
										 IID_INSMUser, (LPVOID*)&pUser2 );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create User object.") );
			return FALSE;
		}
		hr = pUser2->Find( szID.AllocSysString() );
		if( FAILED( hr ) ) return FALSE;

		CString szPassNew, szSiteIDNew, szSiteDescNew;

		pUser2->put_UserID( szID.AllocSysString() );
		szDesc = d.m_pref.pszDesc;
		pUser2->put_Description( szDesc.AllocSysString() );
		szPath = d.m_pref.pszPath;
		pUser2->put_RootPath( szPath.AllocSysString() );
		szBackup = d.m_pref.pszBackup;
		pUser2->put_BackupPath( szBackup.AllocSysString() );
		szDelim = d.m_pref.pszDelim;
		pUser2->put_Delimiter( szDelim.AllocSysString() );
		pUser2->put_InputType( d.m_pref.ip );
		pUser2->put_TabletMode( d.m_pref.tm );
		pUser2->put_EntireTablet( d.m_pref.fEntireTablet );
		pUser2->put_Log( d.m_pref.fLog );
		pUser2->put_Alpha( d.m_pref.fAlpha );
		szPassNew = d.m_pref.pszPass;
		pUser2->put_SysPass( ( Objects::Encode( szPassNew ) ).AllocSysString() );
		pUser2->put_AspectRatioWidth( d.m_pref.dAR_X );
		pUser2->put_AspectRatioHeight( d.m_pref.dAR_Y );
		pUser2->put_TabletWidth( d.m_pref.dT_X );
		pUser2->put_TabletHeight( d.m_pref.dT_Y );
		pUser2->put_DisplayWidth( d.m_pref.dD_X );
		pUser2->put_DisplayHeight( d.m_pref.dD_Y );
		szCommPort = d.m_pref.pszComPort;
		pUser2->put_CommPort( szCommPort.AllocSysString() );
		pUser2->put_AutoUpdate( d.m_pref.fAutoUpdate );
		pUser2->put_Private( d.m_pref.fPrivate );
		szWinUser = d.m_pref.pszWinUser;
		pUser2->put_WinUser( szWinUser.AllocSysString() );
		pUser2->put_GenerateSubjectIDs( d.m_pref.fGenSubjID );
		szStart = d.m_pref.pszSubjStartID;
		pUser2->put_SubjectStartID( szStart.AllocSysString() );
		szEnd = d.m_pref.pszSubjEndID;
		pUser2->put_SubjectEndID( szEnd.AllocSysString() );
		szCur = d.m_pref.pszSubjCurID;
		pUser2->put_SubjectCurID( szCur.AllocSysString() );
		pUser2->put_Encrypted( TRUE );
		pUser2->put_EncryptionMethod( (short)::GetEncryptionMethod() );
		szSiteIDNew = d.m_pref.pszSiteID;
		pUser2->put_SiteID( szSiteIDNew.AllocSysString() );
		szSiteDescNew = d.m_pref.pszSiteDesc;
		pUser2->put_SiteDesc( szSiteDescNew.AllocSysString() );
		szSig = d.m_pref.pszSignature;
		pUser2->put_Signature( ( Objects::Encode( szSig ) ).AllocSysString() );

		hr = NSMUsers::Modify( pUser2 );
		pUser2->Release();
		if( FAILED( hr ) ) return FALSE;

		// only update preferences if current user
		CString szCurUser;
		::GetCurrentUser( szCurUser );
		if( szID == szCurUser )
		{
	 		::SetPreferences( &d.m_pref, &d.m_gs );
 			::WriteGripperPreferences( &d.m_gs );
		}

		// if pass changed, determine whether subjects of same pass should change
		if( szPass != szPassNew )
		{
			szItem = _T("You have changed the user password.\n\nWould you like to change the password of all subjects using the old password?");
			if( BCGPMessageBox( szItem, MB_YESNO ) == IDYES )
			{
				CString szPassOld = szPass;
				Subjects subj;
				hr = subj.ResetToStart();
				while( SUCCEEDED( hr ) )
				{
					subj.GetPassword( szPass );
					if( szPass == szPassOld )
					{
						subj.PutPassword( szPassNew );
						if( FAILED( subj.Modify() ) )
							BCGPMessageBox( _T("Unable to alter subject password.") );
					}

					hr = subj.GetNext();
				}
			}
		}
		// if site info has changed, determine whether subjects of same site should change
		if( ( szSiteID != szSiteIDNew ) || ( szSiteDesc != szSiteDescNew ) )
		{
			szItem = _T("You have changed the user site information.\n\nWould you like to change the site information of all subjects using the old information?");
			if( BCGPMessageBox( szItem, MB_YESNO ) == IDYES )
			{
				CString szSiteIDOld = szSiteID;
				CString szSiteDescOld = szSiteDesc;
				Subjects subj;
				hr = subj.ResetToStart();
				while( SUCCEEDED( hr ) )
				{
					subj.GetSiteID( szSiteID );
					subj.GetSiteDesc( szSiteDesc );
					if( ( szSiteID == szSiteIDOld ) && ( szSiteDesc == szSiteDescOld ) )
					{
						subj.PutSiteID( szSiteIDNew );
						subj.PutSiteDesc( szSiteDescNew );
						if( FAILED( subj.Modify() ) )
							BCGPMessageBox( _T("Unable to alter subject site information.") );
					}

					hr = subj.GetNext();
				}
			}
		}

		return( SUCCEEDED( NSMUsers::Find( pUser, szID ) ) );
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::Export( CStdioFile* pFile )
{
	// TODO
	if( !m_pNSMUser || !pFile || !pFile->m_pStream ) return FALSE;

	CString szItem;
	double dVal;
	short nVal;
	BOOL fVal;

	NSMUsers::GetUserID( m_pNSMUser, szItem );
	if( szItem == HOLDER ) return TRUE;

	pFile->WriteString( TAG_USER );
	pFile->WriteString( _T("\n") );
	// ID
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Desc
	NSMUsers::GetDescription( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Private
	NSMUsers::GetPrivate( m_pNSMUser, fVal );
	szItem = fVal ? _T("1") : _T("0");
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Win User
	NSMUsers::GetWinUser( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Root Path
	NSMUsers::GetRootPath( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Backup Path
	NSMUsers::GetBackupPath( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Delimiter
	NSMUsers::GetDelimiter( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Input Type
	NSMUsers::GetInputType( m_pNSMUser, nVal );
	szItem.Format( _T("%d"), nVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Tablet Mode
	NSMUsers::GetTabletMode( m_pNSMUser, nVal );
	szItem.Format( _T("%d"), nVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Log
	NSMUsers::GetLog( m_pNSMUser, fVal );
	szItem = fVal ? _T("1") : _T("0");
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Alpha
	NSMUsers::GetAlpha( m_pNSMUser, fVal );
	szItem = fVal ? _T("1") : _T("0");
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// System password
	NSMUsers::GetSysPass( m_pNSMUser, szItem, TRUE );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Tablet width
	NSMUsers::GetTabletWidth( m_pNSMUser, dVal );
	szItem.Format( _T("%f"), dVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Tablet height
	NSMUsers::GetTabletHeight( m_pNSMUser, dVal );
	szItem.Format( _T("%f"), dVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Display width
	NSMUsers::GetDisplayWidth( m_pNSMUser, dVal );
	szItem.Format( _T("%f"), dVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Display height
	NSMUsers::GetDisplayHeight( m_pNSMUser, dVal );
	szItem.Format( _T("%f"), dVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Aspect ratio width
	NSMUsers::GetAspectRatioWidth( m_pNSMUser, dVal );
	szItem.Format( _T("%f"), dVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Aspect ratio height
	NSMUsers::GetAspectRatioHeight( m_pNSMUser, dVal );
	szItem.Format( _T("%f"), dVal );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Comm port
	NSMUsers::GetCommPort( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Auto update
	NSMUsers::GetAutoUpdate( m_pNSMUser, fVal );
	szItem = fVal ? _T("1") : _T("0");
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Subject ID generation
	NSMUsers::GetGenerateSubjectIDs( m_pNSMUser, fVal );
	szItem = fVal ? _T("1") : _T("0");
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Subject ID start
	NSMUsers::GetSubjectStartID( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Subject ID end
	NSMUsers::GetSubjectEndID( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Subject ID current
	NSMUsers::GetSubjectCurID( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Encrypted
	NSMUsers::GetEncrypted( m_pNSMUser, fVal );
	szItem.Format( fVal ? _T("1\n") : _T("0\n") );
	pFile->WriteString( szItem );
	// Encryption Method
	NSMUsers::GetEncryptionMethod( m_pNSMUser, nVal );
	szItem.Format( _T("%d\n"), nVal );
	pFile->WriteString( szItem );
	// Site ID
	NSMUsers::GetSiteID( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Site Desc
	NSMUsers::GetSiteDesc( m_pNSMUser, szItem );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );
	// Signature
	NSMUsers::GetSignature( m_pNSMUser, szItem, TRUE );
	pFile->WriteString( szItem );
	pFile->WriteString( _T("\n") );

	// Record termination
	pFile->WriteString( EXPORT_SEPARATOR );
	pFile->WriteString( _T("\n") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo )
{
//	if( !m_pNSMUser || !pFile || !pFile->m_pStream ) return FALSE;
	if( !m_pNSMUser || !pFile ) return FALSE;

	CString szLine, szID;
	BOOL fRet = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( szLine == HOLDER ) return TRUE;
	HRESULT hr = Find( szLine );
	PutUserID( szLine );
	szID = szLine;
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	// Private
	BOOL fTmp;
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutPrivate( fTmp );
	// Win User
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutWinUser( szLine );
	// Root Path
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutRootPath( szLine );
	// Backup Path
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutBackupPath( szLine );
	// Delimiter
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDelimiter( szLine );
	// Input Type
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	short nVal = atoi( szLine );
	PutInputType( nVal );
	// Tablet Mode
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutTabletMode( nVal );
	// Log
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutLog( fTmp );
	// Alpha
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutAlpha( fTmp );
	// System password
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSysPass( szLine, TRUE );
	// Tablet width
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	double dVal = atof( szLine );
	PutTabletWidth( dVal );
	// Tablet height
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutTabletHeight( dVal );
	// Display width
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutDisplayWidth( dVal );
	// Display height
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutDisplayHeight( dVal );
	// Aspect ratio width
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutAspectRatioWidth( dVal );
	// Aspect ratio height
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutAspectRatioHeight( dVal );
	// Comm port
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutCommPort( szLine );
	// Auto update
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutAutoUpdate( fTmp );
	// Subject ID generation
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutGenerateSubjectIDs( fTmp );
	// Subject ID start
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSubjectStartID( szLine );
	// Subject ID end
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSubjectEndID( szLine );
	// Subject ID current
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSubjectCurID( szLine );
	// Encrypted
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutEncrypted( (short)atoi( szLine ) );
	// Encryption Method
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutEncryptionMethod( (short)atoi( szLine ) );
	// Site ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSiteID( szLine );
	// Site Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSiteDesc( szLine );
	// Signature
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutSignature( szLine, TRUE );

DoImport:
	// ask to overwrite (if necessary)
	BOOL fOverWrite = FALSE;
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) )
	{
		OverwriteDlg od( OW_USER, szID, _T(""), _T("") );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) ) hr = Modify();
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" NSMUser::Import") );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::EncryptAll()
{
	BOOL fEnc = FALSE;
	CString szPass, szSig;

	// user path
	CString szPath;
	::GetDataPath( szPath );

	// verify user database file exists
	CString szUserDB = szPath;
	szUserDB += _T("user.dat");
	CFileFind ff;
	if( !ff.FindFile( szUserDB ) ) return;

	// loop through users to get count
	Progress::EnableProgress( 2 );
	Progress::SetProgress( 1, _T("Scanning users...") );
	NSMUsers user;
	user.SetDataPath( szPath );
	HRESULT hr = user.ResetToStart();
	long nSteps = 0;
	while( SUCCEEDED( hr ) )
	{
		nSteps++;
		hr = user.GetNext();
	}
	Progress::SetProgress( 2, _T("Scan complete.") );
	Progress::DisableProgress();

	// loop through users to test
	Progress::EnableProgress( nSteps );
	long nStep = 0;
	CString szMsg, szID;
	hr = user.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		nStep++;
		user.GetUserID( szID );
		szMsg.Format( _T("Verifying user: %s"), szID );
		Progress::SetProgress( nStep, szMsg );

		// get whether encrypted & signature
		user.GetEncrypted( fEnc );
		user.GetSignature( szSig );
		// if not encrypted or sig is empty, we are encrypting and creating a sig
		if( !fEnc || ( szSig == _T("") ) )
		{
			// get existing, non-encrypted password
			user.GetSysPass( szPass );
			// set to "now encrypted"
			user.PutEncrypted( TRUE );
			// set encryption method
			user.PutEncryptionMethod( (short)::GetEncryptionMethod() );
			// set newly encrypted password
			user.PutSysPass( szPass );

			// create a unique signature GUID
			if( szSig == _T("") )
			{
				UUID uuid;
				::UuidCreate( &uuid );
				RPC_CSTR pszUuid;
				::UuidToString( &uuid, &pszUuid );
				szSig = pszUuid;
				user.PutSignature( szSig );
				::RpcStringFree( &pszUuid );
			}

			// update record
			hr = user.Modify();
		}

		// next user
		hr = user.GetNext();
	}
	Progress::SetProgress( nSteps, _T("User verification complete.") );
	Progress::DisableProgress();
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::VerifyElementImages()
{
	// data path
	CString szPath;
	::GetDataPathRoot( szPath );
	if( szPath != _T("") && ( szPath.GetAt( szPath.GetLength() - 1 ) != '\\' ) )
		szPath += _T("\\");

	// image path
	CString szImagePath;
	Stimuluss::GetBaseFile( szImagePath );

	// verify element database file exists
	CFileFind ff;
	CString szDB = szPath;
	szDB += _T("\\element.dat");
	if( !ff.FindFile( szDB ) ) return TRUE;

	// count of elements
	Elements elem;
	elem.SetDataPath( szPath );
	HRESULT hr = elem.ResetToStart();
	long nSteps = (long)elem.GetNumRecords();

	// loop through elements to check
	Progress::EnableProgress( nSteps );
	BOOL fInformed = FALSE, fIsImage = FALSE, fConverted = FALSE;
	CString szSource, szDest, szName, szMsg, szID;
	int nPos = 0, nRes = 0, nItems = 0;
	long nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		// get ID
		elem.GetID( szID );
		nStep++;
		szMsg.Format( _T("Verifying element: %s"), szID );
		Progress::SetProgress( nStep, szMsg );

		// is this element an image
		elem.GetIsImage( fIsImage );
		if( fIsImage )
		{
			// get image file
			elem.GetPattern( szSource );

			// if there are any directory slashes in the name, then we need to fix it
			if( szSource.Find( _T("\\") ) != -1 )
			{
				nItems++;
				if( !fInformed )
				{
					szMsg = _T("Images used in stimulus elements will be copied to the Stimuli directory in your User Data Path:\n\n");
					szMsg += szImagePath;
					szMsg += _T("\n\nNew changes must now be made to the images in the Stimuli subfolder.");
					szMsg += _T("\n\nProceed with image copy?");
					if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO )
					{
						Progress::DisableProgress();
						return FALSE;
					}
					fInformed = TRUE;
				}

				// verify that the source exists
				if( ff.FindFile( szSource ) )
				{
					nPos = szSource.ReverseFind( '\\' );
					if( nPos != -1 ) szName = szSource.Mid( nPos + 1 );
					else szName = szSource;
					szDest = szImagePath;
					szDest += szName;
					BOOL fCopy = TRUE;
					if( ff.FindFile( szDest ) )
					{
						szMsg.Format( _T("File already exists:\n\n%s\n\nOverwrite?"), szDest );
						nRes = BCGPMessageBox( szMsg, MB_YESNOCANCEL );
						if( nRes == IDCANCEL )
						{
							Progress::DisableProgress();
							return FALSE;
						}
						if( nRes == IDNO ) fCopy = FALSE;
					}
					if( fCopy ) CopyFile( szSource, szDest, FALSE );

					// now change the element pattern to just the name
					elem.PutPattern( szName );
					fConverted = TRUE;

					// update record
					hr = elem.Modify();
				}
				else
				{
					szMsg.Format( _T("The following image file does not exist:\n\n%s\n\nDo you want to update the element (ID=%s) manually?"), szSource, szID );
					if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
					{
						elem.DoEdit( szID );
						elem.Find( szID );
					}
				}
			}
		}

		// next element
		hr = elem.GetNext();
	}
	Progress::SetProgress( nSteps, _T("Element verification complete.") );
	Progress::DisableProgress();

	// inform user we're done and successful
	if( fConverted ) BCGPMessageBox( _T("Copy and conversion successfully completed.") );

	return ( nItems == 0 );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::VerifyConditionSounds()
{
	// data path
	CString szPath;
	::GetDataPathRoot( szPath );
	if( szPath != _T("") && ( szPath.GetAt( szPath.GetLength() - 1 ) != '\\' ) )
		szPath += _T("\\");

	// sound path
	CString szSoundPath = szPath;
	szSoundPath += _T("sounds\\");
	// ensure exists
	_mkdir( szSoundPath );

	// verify condition database file exists
	CFileFind ff;
	CString szDB = szPath;
	szDB += _T("\\condition.dat");
	if( !ff.FindFile( szDB ) ) return TRUE;

	// condition count
	NSConditions cond;
	cond.SetDataPath( szPath );
	HRESULT hr = cond.ResetToStart();
	long nSteps = (long)cond.GetNumRecords();

	// loop through conditions and check
	Progress::EnableProgress( nSteps );
	BOOL fInformed = FALSE, fConverted = FALSE;
	CString szSource, szDest, szName, szMsg, szID;
	int nPos = 0, nRes = 0, i = 0, nItems = 0;
	long nStep = 0;
	BOOL fUse = FALSE, fTone = FALSE;
	hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// get ID
		cond.GetID( szID );
		nStep++;
		szMsg.Format( _T("Verifying condition: %s"), szID );
		Progress::SetProgress( nStep, szMsg );

		// get sound values
		for( i = 0; i < SOUND_COUNT; i++ )
		{
			cond.GetSoundInfo( eSC_Use, (eSoundType)( i + 1 ), fUse );
			cond.GetSoundInfo( eSC_Tone, (eSoundType)( i + 1 ), fTone );
			cond.GetSoundInfo( eSC_Media, (eSoundType)( i + 1 ), szSource );

			// if using sound & not tone...
			// if there are any directory slashes in the name, then we need to fix it
			if( fUse && !fTone && ( szSource.Find( _T("\\") ) != -1 ) )
			{
				nItems++;
				if( !fInformed )
				{
					szMsg = _T("Sounds used in conditions will be copied to the Sounds directory in your User Data Path:\n\n");
					szMsg += szSoundPath;
					szMsg += _T("\n\nNew changes must now be made to the sounds in the Sounds subfolder.");
					szMsg += _T("\n\nProceed with sound copy?");
					if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO )
					{
						Progress::DisableProgress();
						return FALSE;
					}
					fInformed = TRUE;
				}

				// verify that the source exists
				if( ff.FindFile( szSource ) )
				{
					nPos = szSource.ReverseFind( '\\' );
					if( nPos != -1 ) szName = szSource.Mid( nPos + 1 );
					else szName = szSource;
					szDest = szSoundPath;
					szDest += szName;
					BOOL fCopy = TRUE;
					if( ff.FindFile( szDest ) )
					{
						szMsg.Format( _T("File already exists:\n\n%s\n\nOverwrite?"), szDest );
						nRes = BCGPMessageBox( szMsg, MB_YESNOCANCEL );
						if( nRes == IDCANCEL )
						{
							Progress::DisableProgress();
							return FALSE;
						}
						if( nRes == IDNO ) fCopy = FALSE;
					}
					if( fCopy ) CopyFile( szSource, szDest, FALSE );

					// now change the condition sound to just the name
					cond.PutSoundInfo( eSC_Media, (eSoundType)( i + 1 ), szName );
					fConverted = TRUE;

					// update record
					hr = cond.Modify();
				}
				else
				{
					szMsg.Format( _T("The following sound file does not exist:\n\n%s\n\nOther events of this condition could have missing sound files as well.\n\nDo you want to update the condition (ID=%s) manually?"), szSource, szID );
					if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
					{
						cond.DoEdit( szID, _T(""), NULL, 6, i );
						cond.Find( szID );
					}
				}
			}
		}

		// next element
		hr = cond.GetNext();
	}
	Progress::SetProgress( nSteps, _T("Condition verification complete.") );
	Progress::DisableProgress();

	// inform user we're done and successful
	if( fConverted ) BCGPMessageBox( _T("Copy and conversion successfully completed.") );

	return ( nItems == 0 );
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::VerifyPaths( CString szID, CString szRootPath, CString szBackupPath,
						    BOOL fPrivate, BOOL& fRecommendedLocation, BOOL& fWriteable )
{
	CString szTest, szDefault;
	CStdioFile f;

	// default paths (also different device)
	::GetDataPath( szDefault, fPrivate );
	if( szDefault[ szDefault.GetLength() - 1 ] != '\\' ) szDefault += _T("\\");
	szDefault += szID;
	if( szRootPath[ szRootPath.GetLength() - 1 ] == '\\' ) szDefault += _T("\\");
	CPath pathRoot( szRootPath ), pathDefault( szDefault );
	CString szDriveRoot = pathRoot.GetDrive();
	CString szDriveDefault = pathDefault.GetDrive();
	CString szDir1 = szRootPath, szDir2 = szDefault;
	szDir1.MakeUpper();
	szDir2.MakeUpper();
	if( szDir1 != szDir2 )
	{
		if( szDriveRoot == szDriveDefault ) fRecommendedLocation = FALSE;
		else fRecommendedLocation = TRUE;
	}
	else fRecommendedLocation = TRUE;

	// test for writeability
	// data path
	szTest = szRootPath;
	szTest += _T("\\");
	szTest += szID;
	szTest += _T("test.txt");
	if( !f.Open( szTest, CFile::modeWrite | CFile::modeCreate ) )
	{
		fWriteable = FALSE;
	}
	else
	{
		fWriteable = TRUE;
		f.Close();
		try { CFile::Remove( szTest ); } catch( ... ) {}
	}
	// backup path (only if root path was writeable)
	if( fWriteable )
	{
		szTest = szBackupPath;
		szTest += _T("\\");
		szTest += szID;
		szTest += _T("test.txt");
		if( !f.Open( szTest, CFile::modeWrite | CFile::modeCreate ) )
		{
			fWriteable = FALSE;
		}
		else
		{
			fWriteable = TRUE;
			f.Close();
			try { CFile::Remove( szTest ); } catch( ... ) {}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::VerifyBackupPath()
{
	// Check for backup directory first
	CString szBackup;
	::GetBackupPath( szBackup );
	if( szBackup == _T("") )
	{
		BCGPMessageBox( _T("No backup path has been specified.") );
		return FALSE;
	}
	if( szBackup.GetAt( szBackup.GetLength() - 1 ) == '\\' )
		szBackup = szBackup.Left( szBackup.GetLength() - 1 );
	CFileFind ff;
	if( !ff.FindFile( szBackup ) )
	{
		if( BCGPMessageBox( _T("The backup path specified in your user preferences does not exist. Create?"), MB_YESNO ) == IDNO )
			return FALSE;

		if( _mkdir( szBackup ) != 0 )
		{
			BCGPMessageBox( _T("Unable to create backup directory. Aborting backup.") );
			return FALSE;
		}
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::BackupRaw( CString szExp, CString szGrp, CString szSubj, CString szRaw )
{
	static BOOL fVerified = FALSE;
	static BOOL fComfirmed = TRUE;

	if( !fComfirmed ) return FALSE;

	if( !fVerified )
	{
		fVerified = VerifyBackupPath();
		fComfirmed = fVerified;
		if( !fVerified ) return FALSE;
	}

	// BACKUP
	CFileFind ff;
	CString szFile, szBackup, szRoot;

	::GetBackupPath( szBackup );
	::GetDataPathRoot( szRoot );

	// Verify experiment backup directory
	szFile.Format( _T("%s\\%s"), szBackup, szExp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	// Verify group backup directory
	szFile.Format( _T("%s\\%s\\%s"), szBackup, szExp, szGrp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	// Verify subject backup directory
	szFile.Format( _T("%s\\%s\\%s\\%s"), szBackup, szExp, szGrp, szSubj );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}

	// Backup
	CString szOrig, szNew;
	szOrig.Format( _T("%s\\%s\\%s\\%s\\%s"), szRoot, szExp, szGrp, szSubj, szRaw );
	szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szBackup, szExp, szGrp, szSubj, szRaw );
	return CopyFile( szOrig, szNew, FALSE );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSMUsers::BackupDB( BOOL fAsk )
{
	if( fAsk )
	{
		int nRes = BCGPMessageBox( _T("Data has changed.\nDo you want to backup the database files?"), MB_YESNOCANCEL );
		if( nRes == IDNO ) return TRUE;
		else if( nRes == IDCANCEL ) return FALSE;
	}

	if( !VerifyBackupPath() ) return FALSE;

	OutputAMessage( _T("Backing up database."), TRUE );

	CString szFile, szRoot, szBackup, szOrig, szNew;
	::GetDataPathRoot( szRoot );
	::GetBackupPath( szBackup );
	CFileFind ff;
	BOOL fCont = FALSE;

	// backup path extended for current date & time
	COleDateTime dt = COleDateTime::GetCurrentTime();
	CString szToday = dt.Format( _T("%Y%m%d%H%M%S") );
	szBackup += _T("\\");
	szBackup += szToday;

	if( !ff.FindFile( szBackup ) )
	{
		if( _mkdir( szBackup ) != 0 )
		{
			BCGPMessageBox( _T("Unable to create backup directory. Aborting backup.") );
			return FALSE;
		}
	}

	CWaitCursor crs;

	// Backup DAT/IDX files
	szFile.Format( _T("%s\\*.dat"), szRoot );
	fCont = ff.FindFile( szFile );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFileName();
		szOrig.MakeUpper();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s"), szBackup, ff.GetFileName() );
		CopyFile( szOrig, szNew, FALSE );
	}
	szFile.Format( _T("%s\\*.idx"), szRoot );
	fCont = ff.FindFile( szFile );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFileName();
		szOrig.MakeUpper();
		szOrig = ff.GetFilePath();
		szNew.Format( _T("%s\\%s"), szBackup, ff.GetFileName() );
		CopyFile( szOrig, szNew, FALSE );
	}

	// add to backup history
	IBackup* pB = NULL;
	HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
								   IID_IBackup, (LPVOID*)&pB );
	if( FAILED( hr ) ) return TRUE;

	CString szItem;
	::GetCurrentUser( szItem );
	pB->put_UserID( szItem.AllocSysString() );
	dt.SetDateTime( dt.GetYear(), dt.GetMonth(), dt.GetDay(), dt.GetHour(), dt.GetMinute(), dt.GetSecond() + 1 );
	szItem = dt.Format( _T("%m-%d-%Y %H:%M:%S") );
	pB->put_BackupDate( szItem.AllocSysString() );
	pB->put_Path( szBackup.AllocSysString() );
	pB->put_BackupType( eB_DB );
	szItem = _T("");
	pB->put_Exp( szItem.AllocSysString() );
	pB->put_Grp( szItem.AllocSysString() );
	pB->put_Subj( szItem.AllocSysString() );
	szItem = _T("Database backup");
	pB->put_Notes( szItem.AllocSysString() );
	hr = pB->Add();
	if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to add backup to backup history table.") );
	else BCGPMessageBox( _T("Backup of database completed sucessfully.") );
	pB->Release();

	::DataHasNotChanged();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void NSMUsers::RestoreDB()
{
	if( BCGPMessageBox( _T("Are you sure you want to restore the database files? This process cannot be undone."), MB_YESNO ) == IDNO )
		return;

	OutputAMessage( _T("Restoring database."), TRUE );

	// RESTORE
	CString szRoot, szBackup, szOrig, szNew, szFile, szName;
	CFileFind ff;
	BOOL fCont;
	// Root/backup data path
	::GetDataPathRoot( szRoot );
	::GetBackupPath( szBackup );

	CWaitCursor crs;

	// Restore DAT/IDX files
	// DAT files

	szFile.Format( _T("%s\\*.dat"), szBackup );
	fCont = ff.FindFile( szFile );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szName = ff.GetFileName();
		szName.MakeUpper();
		szNew.Format( _T("%s\\%s"), szRoot, szName );
		CopyFile( szOrig, szNew, FALSE );
	}
	// IDX files
	szFile.Format( _T("%s\\*.idx"), szBackup );
	fCont = ff.FindFile( szFile );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szOrig = ff.GetFilePath();
		szName = ff.GetFileName();
		szName.MakeUpper();
		szNew.Format( _T("%s\\%s"), szRoot, szName );
		CopyFile( szOrig, szNew, FALSE );
	}

	::DataHasNotChanged();
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG NSMUsers::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pNSMUser ) m_pNSMUser->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

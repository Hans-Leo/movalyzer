#pragma once

class CFileFinder;

// MoveUserDlg dialog

class AFX_EXT_CLASS MoveUserDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(MoveUserDlg)

// Construction
public:
	MoveUserDlg( CWnd* pParent = NULL );
	virtual ~MoveUserDlg();

// Dialog Data
	enum { IDD = IDD_MOVEDATA };
	CToolTipCtrl	m_tooltip;
	CProgressCtrl	m_progress;
	BOOL			m_fBackup;
	BOOL			m_fPrivate;
	CString			m_szPath;
	CString			m_szPathOld;
	CBCGPButton		m_bnBrowse;
	CString			m_szID;
	__int64			m_bytes;
	BOOL			m_fDoMove;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void OnOK();
	void OnCancel();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
	static void FileFinderProc( CFileFinder *pFinder, DWORD dwCode, void *pCustomParam );
protected:
	void Start();
	void Reset();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnBrowse();
	afx_msg void OnBnDefault();
	afx_msg void OnClickPrivate();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

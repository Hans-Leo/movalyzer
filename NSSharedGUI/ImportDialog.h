#pragma once

// ImportDialog.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ImportDialog dialog

class AFX_EXT_CLASS ImportDialog : public CBCGPDialog
{
// Construction
public:
	ImportDialog(CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_EIMPORT };
	CString			m_szFile;
	static CString	_szFile;
	CBCGPEdit		m_wndFolderEdit;
	CString			m_szFileName;
	CString			m_szExp;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fFiles;
	BOOL			m_fTrials;
	BOOL			m_fSummary;
	BOOL			m_fZip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void Browse();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnBrowse();
	afx_msg void OnUpdateFile();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

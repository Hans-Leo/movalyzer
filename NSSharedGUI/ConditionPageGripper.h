#if !defined(AFX_CONDITIONPAGEGRIPPER_H__66AA4CB9_475F_4A4F_A409_6DC348520CA7__INCLUDED_)
#define AFX_CONDITIONPAGEGRIPPER_H__66AA4CB9_475F_4A4F_A409_6DC348520CA7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// ConditionPageGripper dialog
#include "NSTooltipCtrl.h"

interface INSCondition;

class AFX_EXT_CLASS ConditionPageGripper : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ConditionPageGripper)

public:
	ConditionPageGripper( INSCondition* = NULL, BOOL fNew = TRUE );
	~ConditionPageGripper();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_GRIPPER };
	double			m_dMagnetForce;
	short			m_nTask;
	CComboBox		m_cboTask;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;

public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#endif

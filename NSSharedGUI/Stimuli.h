#ifndef _STIMULUS_H_
#define	_STIMULUS_H_

#include "Objects.h"

#define	TAG_STIMULUS	_T("[STIMULUS]")
#define	TAG_STIMELMT	_T("[STIMULUSELEMENT]")
#define	TAG_STIMTARG	_T("[STIMULUSTARGET]")

interface IStimulus;

// This class is intended to encapsulate all view & functionality for a subject
class AFX_EXT_CLASS Stimuluss : public Objects
{
	PUT_BASE( Stimulus )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();

	// Interface methods
	PUT_GET( Stimulus, ID, CString, val.AllocSysString() )

	PUT_GET( Stimulus, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( IStimulus*, CString&, BOOL fReverse = FALSE );

	PUT_GET( Stimulus, Demo, BOOL, val );

	// Importing/Exporting
	BOOL Export( CMemFile* pFile, BOOL fFirst = FALSE );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, BOOL fRecurse, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );
	virtual BOOL DoCopyAll( CString szID, CString szPath, BOOL fOverwrite = FALSE, BOOL fRecurse = FALSE );
	virtual BOOL DoUploadAll( CString szID, BOOL fOverwrite = FALSE, BOOL fRecurse = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();

	// temp patch for animated precue
	// NOTE: this is now outdated
	void GetDemoCount( int& nCount );

	// Generate the X,Y,Z matrices - return value = # of points
	// responsibility of caller to delete x,y,z
	int GenerateXYZ( /*[out, retval]*/ double** x,
					 /*[out, retval]*/ double** y,
					 /*[out, retval]*/ double** z );
	static int GenerateXYZ( /*[in]*/ IStimulus*,
							/*[out, retval]*/ double** x,
							/*[out, retval]*/ double** y,
							/*[out, retval]*/ double** z );
	int GenerateXYZ( /*[in]*/ CStringList* pElements,
					 /*[out, retval]*/ double** x,
					 /*[out, retval]*/ double** y,
					 /*[out, retval]*/ double** z );
	static int GenerateXYZ( /*[in]*/ IStimulus*,
							/*[in]*/ CStringList* pElements,
							/*[out, retval]*/ double** x,
							/*[out, retval]*/ double** y,
							/*[out, retval]*/ double** z );
	BOOL GenerateFile( CString& szFile, BOOL fAsk = TRUE );
	static BOOL GenerateFile( IStimulus*, CString& szFile, BOOL fAsk = TRUE );
	BOOL GenerateFile( CStringList* pElements, CString& szFile, BOOL fAsk = TRUE );
	static BOOL GenerateFile( IStimulus*, CStringList* pElements, CString& szFile, BOOL fAsk = TRUE );
	BOOL GenerateFiles();
	static BOOL GenerateFiles( IStimulus* );

	// View/Chart the results
	void ViewData( BOOL fAsk = TRUE );
	static void ViewData( IStimulus*, BOOL fAsk = TRUE );
	void ChartData( BOOL fAsk = TRUE );
	static void ChartData( IStimulus*, BOOL fAsk = TRUE );

	void GetElements( CStringList* pElements, CStringList* pOtherElements );
	static void GetElements( IStimulus*, CStringList* pElements, CStringList* pOtherElements );
	void GetTargets( CStringList* pTargets );
	static void GetTargets( IStimulus*, CStringList* pTargets );

	void GetStimFile( CString& szFile );
	static void GetStimFile( IStimulus*, CString& szFile );
	static void GetStimFileFromID( CString szID, CString& szFile );
	void GetDecFile( CString& szFile );
	static void GetDecFile( IStimulus*, CString& szFile );
	static void GetDecFileFromID( CString szID, CString& szFile );
	void GetTargFile( CString& szFile );
	static void GetTargFile( IStimulus*, CString& szFile );
	static void GetTargFileFromID( CString szID, CString& szFile );
	void GetViewFile( CString& szFile );
	static void GetViewFile( IStimulus*, CString& szFile );
	static void GetViewFileFromID( CString szID, CString& szFile );
	static void GetBaseFile( CString& szBase );
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _Stimulus_H_

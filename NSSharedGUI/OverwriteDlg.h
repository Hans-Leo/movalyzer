#pragma once

#include "NSTooltipCtrl.h"

#define	OW_UNKNOWN			0
#define	OW_EXPERIMENT		1
#define	OW_SETTINGS			2
#define	OW_CONDITION		3
#define	OW_GROUP			4
#define	OW_SUBJECT			5
#define	OW_STIMULUS			6
#define	OW_ELEMENT			7
#define	OW_CAT				8
#define	OW_QUEST			9
#define	OW_GQUEST			10
#define	OW_SQUEST			11
#define	OW_FEEDBACK			12
#define	OW_STIMTARG			13
#define	OW_USER				14

#define	OW_YES				1
#define	OW_NO				2
#define	OW_YESALL			3
#define	OW_NOALL			4
#define	OW_CANCEL			5

// OverwriteDlg dialog

class AFX_EXT_CLASS OverwriteDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC( OverwriteDlg )

public:
	OverwriteDlg( UINT nType, CString szID, CString szFile1, CString szFile2, CWnd* pParent = NULL );
	virtual ~OverwriteDlg();

// Dialog Data
	enum { IDD = IDD_OVERWRITE };

protected:
	virtual void DoDataExchange( CDataExchange* pDX );
public:
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
	virtual void OnCancel();

public:
	UINT			m_nType;
	CString			m_szType;
	UINT			m_nResult;
	CString			m_szFileOrig;
	CString			m_szFileNew;
	CString			m_szItem;
	BOOL			m_fAll;
	CBCGPButton		m_bnRel;
	CBCGPListCtrl	m_list;
	CImageList		m_ImageList;
	static BOOL		m_fShowOnly;
	NSToolTipCtrl	m_tooltip;
	// subject-specific
	BOOL			m_fDiffNameFirst;
	BOOL			m_fDiffNameLast;
	BOOL			m_fDiffNotesPrv;
	BOOL			m_fDiffSig;
	BOOL			m_fDiffPass;

	// for last section (condition, group, etc) displayed
	static UINT	m_nLastType;
	static void Reset() { OverwriteDlg::m_nLastType = 0; }

public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillFileCompareList();
	afx_msg void OnBnClickedChkHide();
	afx_msg void OnBnClickedRel();
DECLARE_MESSAGE_MAP()
};

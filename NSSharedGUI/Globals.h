// Globals.h
//

#if !defined(GLOBALS_H_)
#define GLOBALS_H_

// encryption technology
#define	ENCRYPTION_NONE			0
#define	ENCRYPTION_DES			1
#define	ENCRYPTION_AES			2
#define	ENCRYPTION_BLOWFISH		3
UINT AFX_EXT_API GetEncryptionMethod();

// this is used to create a bogus subject (that is not displayed)
// within each newly added group of an experiment
// the purpose is to ensure the "empty" folder is displayed
#define	HOLDER	_T("___")
#define	CHILKAT_ZIP_UNLOCK		_T("SNEUROZIP_n88OqGHanSpp")
#define	CHILKAT_CRYPT_UNLOCK	_T("SNEUROCrypt_ckMP0GkyYXNG")
#define	CHILKAT_MAIL_UNLOCK		_T("SNEUROMAILQ_acq7DmZs8U6g")

// COM/DCOM DLL version checking
#define	VER_DATAMOD		0x01
#define	VER_INPUTMOD	0x02
#define	VER_PROCMOD		0x04
#define	VER_SHOWMOD		0x08
#define	VER_ADMINMOD	0x10

#define	VER_ALL	( VER_DATAMOD | VER_INPUTMOD | VER_PROCMOD | VER_SHOWMOD | VER_ADMINMOD )
BOOL AFX_EXT_API ConfirmModuleVersions( UINT nMods, CString& szMsg );

// reserved characters (cannot use in IDs or descriptions/names)
#define	RESERVED		_T("/\\?%:|\"<>")

interface IProcSetting;

// this structure is used for remembering the state of the leftview tree
// for each user - stored in mem.sta
struct AFX_EXT_CLASS TreeMemory
{
	HTREEITEM	hItem;
	BOOL		fChildren;
	BOOL		fOpen;
};

// this class houses the system preferences (like input type, etc)
// and is NOT user-specfic, but machine specific
class AFX_EXT_CLASS Preferences
{
public:
	Preferences() : ip( itTablet ), tm( tmDesktop ),
					fEntireTablet( FALSE ), fLog( TRUE ), fLogUI( TRUE ),
					fLogProc( TRUE ), fLogDB( TRUE ), fLogGraph( TRUE ),
					fLogTablet( TRUE ), fAlpha( FALSE ), dAR_X( 0.0 ),
					dAR_Y( 0.0 ), fPrivate( FALSE ), fGenSubjID( FALSE ),
					fEncrypted( FALSE ), nEncryptionMethod( ENCRYPTION_NONE )
	{
		memset( pszDelim, 0, 2 );
		memset( pszPath, 0, MAX_PATH );
		memset( pszID, 0, 4 );
		memset( pszDesc, 0, 26 );
		memset( pszBackup, 0, MAX_PATH );
		memset( pszPass, 0, 31 );
		memset( pszComPort, 0, 32 );
		memset( pszWinUser, 0, 50 );
		memset( pszSubjStartID, 0, 4 );
		memset( pszSubjEndID, 0, 4 );
		memset( pszSubjCurID, 0, 4 );
		memset( pszSiteID, 0, 26 );
		memset( pszSiteDesc, 0, 51 );
		memset( pszSignature, 0, 151 );
	}

	enum eInputType { itNone = 0, itTablet = 1, itMouse = 2, itGripper = 3 } ip;
	enum eTabletMode { tmNone = 0, tmDesktop = 1, tmTablet = 3 } tm;
	BOOL fEntireTablet;
	BOOL fLog;
	BOOL fLogUI;
	BOOL fLogProc;
	BOOL fLogDB;
	BOOL fLogGraph;
	BOOL fLogTablet;
	BOOL fAlpha;
	BOOL fPrivate;
	char pszDelim[ 2 ];
	char pszPath[ MAX_PATH ];
	char pszID[ 4 ];
	char pszDesc[ 26 ];
	char pszBackup[ MAX_PATH ];
	char pszPass[ 31 ];
	// ascpect ratios for mouse..relative to screen (for drawing stimuli)
	// stored in inches
	double	dAR_X;
	double	dAR_Y;
	// the following stored in cm (I know i know....)
	// tablet & display dimensions
	double	dT_X;
	double	dT_Y;
	double	dD_X;
	double	dD_Y;
	// communications port
	char pszComPort[ 32 ];
	// automatic update flag
	BOOL fAutoUpdate;
	// windows user who created mova user
	char pszWinUser[ 50 ];
	// subject ID generation stuff
	BOOL fGenSubjID;
	char pszSubjStartID[ 4 ];
	char pszSubjEndID[ 4 ];
	char pszSubjCurID[ 4 ];
	// encryption & site
	BOOL fEncrypted;
	short nEncryptionMethod;
	char pszSiteID[ 26 ];
	char pszSiteDesc[ 51 ];
	char pszSignature[ 151 ];
};

// We "originally" only had recording window maximized for each trial
// Then we added the option to maximize or not (so we had a boolean setting in the experiment settings)
// Then we added "real size" but did not allow the selection to NOT resize
// In this particular case a BOOL can actually have an integer value of, eg. 3
// Normally the variable was 0 if it was to be scaled to real size, or 1 if it's to be maximized
// Now, it will be 2 if it's "leave as is"
// IMPORTANT: If Microslop ever redefines their BOOL type to something else, who knows what could happen
//            although I highly doubt that would ever happen
// #define for leave windows as is
#define	WINDOW_ASIS		2

// same idea as above but for grip-defice only
struct AFX_EXT_CLASS GripperSettings
{
	int		nDevice;
	int		nChannels;
	long	lSamples;
	long	lSampleRate;
	long	lScanRate;
	int		nBaseline;
	int		nChanLower;
	int		nChanUpper;
	int		nChanLoad;
	double	dGainLG;
	double	dGainUG;
	double	dGainL;
	double	dCalibrationConstantLG;
	double	dCalibrationConstantUG;
	double	dCalibrationConstantL;
	double	dExcitationVoltageLG;
	double	dExcitationVoltageUG;
	double	dExcitationVoltageL;
	double	dFullScaleLoadLG;
	double	dFullScaleLoadUG;
	double	dFullScaleLoadL;
	BOOL	fVolts;
	BOOL	fDoubleBuffer;
	long	lDBSamples;
	BOOL	fNewtons;
};

/////////////////////////////////////////////////////////////////////////////
// WHICH PRODUCTS HAVE BEEN ACTIVATED
/////////////////////////////////////////////////////////////////////////////

void AFX_EXT_API SetIsD( BOOL );
BOOL AFX_EXT_API IsD( int* nDays = NULL );

void AFX_EXT_API SetIsSA( BOOL active );
BOOL AFX_EXT_API IsSA( BOOL fConsiderDemo = TRUE );

void AFX_EXT_API SetIsGA( BOOL );
BOOL AFX_EXT_API IsGA( BOOL fConsiderDemo = TRUE );

void AFX_EXT_API SetIsOA( BOOL );
BOOL AFX_EXT_API IsOA( BOOL fConsiderDemo = TRUE );

void AFX_EXT_API SetIsRxA( BOOL );
BOOL AFX_EXT_API IsRxA( BOOL fConsiderDemo = TRUE );

void AFX_EXT_API SetIsCS( BOOL );
BOOL AFX_EXT_API IsCS( BOOL fConsiderDemo = TRUE );

// Pay-Per-Use
void AFX_EXT_API SetIsSPPU( BOOL, long = 0 );
BOOL AFX_EXT_API IsSPPU( long& );

void AFX_EXT_API SetIsGPPU( BOOL, long = 0 );
BOOL AFX_EXT_API IsGPPU( long& );

void AFX_EXT_API SetIsOPPU( BOOL, long = 0 );
BOOL AFX_EXT_API IsOPPU( long& );

// Writalyzer
void AFX_EXT_API SetIsWA( BOOL active );
BOOL AFX_EXT_API IsWA( BOOL fConsiderDemo = TRUE );

/////////////////////////////////////////////////////////////////////////////
// Special COM elevation monikor for creating out-of-process COM servers
// and elevating them to admin level - Vista+ applicable only
HRESULT AFX_EXT_API CoCreateInstanceAsAdmin(HWND hwnd, REFCLSID rclsid, REFIID riid, __out void ** ppv);

/////////////////////////////////////////////////////////////////////////////

// cleanup of anything in globals
void AFX_EXT_API SystemCleanup();
// is another specified process running?
BOOL AFX_EXT_API IsProcessRunning( CString szProcess );
// strip strings of newline chars & tabs
void AFX_EXT_API StripNewlinesAndTabs( CString& szData );
// accurate delay
int AFX_EXT_API Delay( long MicroSeconds );
// output messages to the results view
void AFX_EXT_API OutputAMessage( CString szMsg, BOOL fOverrideRapid = FALSE, UINT nType = LOG_UI );
void AFX_EXT_API OutputAMessage( UINT nResourceID, BOOL fOverrideRapid = FALSE );
// APP/User Globals
void AFX_EXT_API GetWinUser( CString& );
int AFX_EXT_API GetAppVersion();
void AFX_EXT_API SetAppPath( CString );
void AFX_EXT_API GetAppPath( CString& );
void AFX_EXT_API GetCommonPath( CString& );
void AFX_EXT_API GetDataPath( CString&, BOOL fPrivate = FALSE, BOOL fIgnorePU = FALSE );
void AFX_EXT_API GetAllUserPath( CString& );
void AFX_EXT_API SetCurrentUser( CString szUser );
void AFX_EXT_API GetCurrentUser( CString& szUser );
void AFX_EXT_API SetCurrentUserDesc( CString szDesc );
void AFX_EXT_API GetCurrentUserDesc( CString& szDesc );
void AFX_EXT_API SetCurrentUserSiteID( CString szVal );
AFX_EXT_API CString& GetCurrentUserSiteID();
void AFX_EXT_API SetCurrentUserSiteDesc( CString szVal );
AFX_EXT_API CString& GetCurrentUserSiteDesc();
void AFX_EXT_API SetSignature( CString szVal );
AFX_EXT_API CString& GetSignature();
void AFX_EXT_API DataHasChanged();
void AFX_EXT_API DataHasNotChanged();
BOOL AFX_EXT_API HasDataChanged();
void AFX_EXT_API SetCurrentChart( CString );
void AFX_EXT_API GetCurrentChart( CString& );
BOOL AFX_EXT_API HasTrialPassed( CString szTrial );
// Comm Port
void AFX_EXT_API SetCommPort( CString szPort );
void AFX_EXT_API GetCommPort( CString& szPort );
// Global Settings
BOOL AFX_EXT_API IsTabletInstalled();
BOOL AFX_EXT_API IsGripperInstalled();
void AFX_EXT_API SetTabletModeOn( BOOL fOn );
BOOL AFX_EXT_API IsTabletModeOn();
void AFX_EXT_API SetDesktopModeOn( BOOL fOn );
BOOL AFX_EXT_API IsDesktopModeOn();
void AFX_EXT_API SetTabletRemapOn( BOOL fOn );
BOOL AFX_EXT_API IsTabletRemapOn();
void AFX_EXT_API SetContinueWithEnter( BOOL fOn );
BOOL AFX_EXT_API IsContinueWithEnter();
void AFX_EXT_API SetGripperModeOn( BOOL fOn );
BOOL AFX_EXT_API IsGripperModeOn();
void AFX_EXT_API SetTrialAlphaOn( BOOL fOn );
BOOL AFX_EXT_API IsTrialAlphaOn();
void AFX_EXT_API SetPrivateUserOn( BOOL fOn );
BOOL AFX_EXT_API IsPrivateUserOn();
void AFX_EXT_API SetDelimiter( CString szDelim );
void AFX_EXT_API GetDelimiter( CString& szDelim );
AFX_EXT_API CString& GetDelimiter();
void AFX_EXT_API SetDataPathRoot( CString szPath );
void AFX_EXT_API GetDataPathRoot( CString& szPath );
void AFX_EXT_API SetBackupPath( CString szPath );
void AFX_EXT_API GetBackupPath( CString& szPath );
void AFX_EXT_API SetAutoUpdateOn( BOOL fOn );
BOOL AFX_EXT_API IsAutoUpdateOn();
// tablet & display dimensions
void AFX_EXT_API SetTabletDimensions( double dX, double dY );	// cm
void AFX_EXT_API GetTabletDimensions( double& dX, double& dY );
void AFX_EXT_API SetDisplayDimensions( double dX, double dY );	// cm
void AFX_EXT_API GetDisplayDimensions( double& dX, double& dY );
// help related
void AFX_EXT_API SetHelpOpen( BOOL fOpen );
BOOL AFX_EXT_API GetHelpOpen();
void AFX_EXT_API SetHelpWindow( CWnd* pWndHelp );
AFX_EXT_API CWnd* GetHelpWindow();
void AFX_EXT_API AddHelpWindow();
void AFX_EXT_API RemoveHelpWindow();
void AFX_EXT_API SetBkColor( DWORD dwVal );
DWORD AFX_EXT_API GetBkColor();
// Window sizes while maximizing
void AFX_EXT_API SetLVMaximizeSize( int nSize );
int AFX_EXT_API GetLVMaximizeSize();
// Logging
void AFX_EXT_API SetDetailedOutput( BOOL fOn );
BOOL AFX_EXT_API GetDetailedOutput();
void AFX_EXT_API SetVerbosity( BOOL fOn );
BOOL AFX_EXT_API GetVerbosity();
void AFX_EXT_API SetLogUI( BOOL fOn );
BOOL AFX_EXT_API GetLogUI();
void AFX_EXT_API SetLogDB( BOOL fOn );
BOOL AFX_EXT_API GetLogDB();
void AFX_EXT_API SetLogProc( BOOL fOn );
BOOL AFX_EXT_API GetLogProc();
void AFX_EXT_API SetLogGraph( BOOL fOn );
BOOL AFX_EXT_API GetLogGraph();
void AFX_EXT_API SetLogTablet( BOOL fOn );
BOOL AFX_EXT_API GetLogTablet();
// Gripper Settings
void AFX_EXT_API SetGripperSettings( GripperSettings* gs );
void AFX_EXT_API GetGripperSettings( GripperSettings* gs );
void AFX_EXT_API GetPreferences( Preferences* prefs, GripperSettings* gs );
void AFX_EXT_API SetPreferences( Preferences* prefs, GripperSettings* gs );
AFX_EXT_API BOOL WritePreferences( Preferences* prefs, GripperSettings* gs );
AFX_EXT_API BOOL WriteGripperPreferences( GripperSettings* gs );

// FOR AUTOMATIC SUBJECT ID GENERATION
// add 1 to string, return TRUE if within range, return updated string (ref)
BOOL AFX_EXT_API NextID( CString& szID, CString szMin, CString szMax, BOOL fRangeOnly );

//////////////////////////////////////////////////////

BOOL AFX_EXT_API VerifyDirectory( CString szExp, CString szGrp, CString szSubj, CString* szFullPath = NULL );
BOOL AFX_EXT_API VerifyDirectory( CString szExp, CString szGrp );
BOOL AFX_EXT_API VerifyDirectory( CString szExp );
BOOL AFX_EXT_API CreateDirectory( CString szDir );

// Verify that string does not contain reserved and delimiter chars (can override delim)
BOOL AFX_EXT_API VerifyStringForReserved( CString szString, CString szDelim = _T("") );
// Verify database entries do not contain reserved/delimiter chars (can override delim)
BOOL AFX_EXT_API VerifyDatabaseForReserved( CString szDelim = _T("") );

void AFX_EXT_API GetSubjectMask( CString szExp, CString szGrp, CString szSubj, CString& szFile );
void AFX_EXT_API GetHWR( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szHWR );
void AFX_EXT_API GetHWR( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			 CString& szHWR );
void AFX_EXT_API GetPCX( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szPCX );
void AFX_EXT_API GetPCX( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			 CString& szPCX );
void AFX_EXT_API GetHWRMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szHWR );
void AFX_EXT_API GetHWRMask( CString szExp, CString szGrp, CString szSubj, CString& szHWR );
void AFX_EXT_API GetPCXMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szPCX );
void AFX_EXT_API GetPCXMask( CString szExp, CString szGrp, CString szSubj, CString& szPCX );
void AFX_EXT_API GetPCXProcMask( CString szExp, CString szGrp, CString szSubj, CString& szPCX );

void AFX_EXT_API GetTF( CString szExp, CString szGrp, CString szSubj, CString szCond,
			CString szIdx, CString& szTF );
void AFX_EXT_API GetTF( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			CString& szTF );
void AFX_EXT_API GetTFMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				CString& szTF );
void AFX_EXT_API GetTFMask( CString szExp, CString szGrp, CString szSubj, CString& szTF );

void AFX_EXT_API GetSEG( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szSEG );
void AFX_EXT_API GetSEGMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szSEG );
void AFX_EXT_API GetSEGMask( CString szExp, CString szGrp, CString szSubj, CString& szSEG );

void AFX_EXT_API GetEXT( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString& szEXT );
void AFX_EXT_API GetSubjEXTMask( CString szExp, CString szGrp, CString szSubj, CString& szEXT );

void AFX_EXT_API GetCON( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString& szCON );
void AFX_EXT_API GetSubjCONMask( CString szExp, CString szGrp, CString szSubj, CString& szCON );

void AFX_EXT_API GetERR( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString& szERR );
void AFX_EXT_API GetSubjERRMask( CString szExp, CString szGrp, CString szSubj, CString& szERR );
void AFX_EXT_API GetSEN( CString szExp, CString szGrp, CString szSubj, CString szCond,
						 CString szTrial, CString& szSEN );
void AFX_EXT_API GetSENMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
							 CString& szSEN );

void AFX_EXT_API GetFRD( CString szExp, CString szGrp, CString szSubj, CString szCond,
			 CString szIdx, CString& szFRD );
void AFX_EXT_API GetFRD( CString szExp, CString szGrp, CString szSubj, CString szTrial,
			 CString& szFRD );
void AFX_EXT_API GetFRDMask( CString szExp, CString szGrp, CString szSubj, CString szCond,
				 CString& szFRD );
void AFX_EXT_API GetFRDMask( CString szExp, CString szGrp, CString szSubj, CString& szFRD );

void AFX_EXT_API GetExpHWRMask( CString szExp, CString szGrp, CString szSubj, CString& szHWR );
void AFX_EXT_API GetExpFRDMask( CString szExp, CString szGrp, CString szSubj, CString& szFRD );
void AFX_EXT_API GetExpTFMask( CString szExp, CString szGrp, CString szSubj, CString& szTF );
void AFX_EXT_API GetExpSEGMask( CString szExp, CString szGrp, CString szSubj, CString& szSEG );
void AFX_EXT_API GetExpEXTMask( CString szExp, CString szGrp, CString szSubj, CString& szEXT );
void AFX_EXT_API GetExpCONMask( CString szExp, CString szGrp, CString szSubj, CString& szCON );
void AFX_EXT_API GetExpERRMask( CString szExp, CString szGrp, CString szSubj, CString& szERR );
void AFX_EXT_API GetExpINCMask( CString szExp, CString szGrp, CString szSubj, CString& szINC );
void AFX_EXT_API GetExpERSMask( CString szExp, CString szGrp, CString szSubj, CString& szERS );

void AFX_EXT_API GetExpNoExt( CString szExp, CString& szFile );
void AFX_EXT_API GetExpINC( CString szExp, CString& szINC );
void AFX_EXT_API GetExpERR( CString szExp, CString& szERR );
void AFX_EXT_API GetExpSET( CString szExp, CString& szSET );
void AFX_EXT_API GetExpERS( CString szExp, CString& szERS );

void AFX_EXT_API GetGroupSort( CString szExp, CString& szFile );
void AFX_EXT_API GetCondSort( CString szExp, CString& szFile );
void AFX_EXT_API GetSubjSort( CString szExp, CString& szFile );


// Privacy
#define	PVT_COLOR			RGB( 239, 241, 181 )
#define	PVT_NAME_FIRST		_T("*")
#define	PVT_NAME_LAST		_T("*")
#define	PVT_ITEM			_T("*****")
BOOL AFX_EXT_API IsPrivacyOn();
void AFX_EXT_API SetIsPrivacyOn( BOOL fVal );
void AFX_EXT_API SetUserPassword( CString szVal );
void AFX_EXT_API GetUserPassword( CString& szVal );
AFX_EXT_API CString& GetUserPassword();
void AFX_EXT_API SetHasPassBeenSet( BOOL fVal );
BOOL AFX_EXT_API HasPassBeenSet();
BOOL AFX_EXT_API CheckPass( BOOL fSubject = FALSE, CString szPass = _T("") );


// default values for rate/resolution/pressure
#define	MOUSE_RATE		100
#define	TABLET_RATE		100
#define	GRIPPER_RATE	200
#define	MOUSE_RES		0.025
#define	TABLET_RES		0.001
#define	GRIPPER_RES		1
#define	MOUSE_PRESS		1
#define	TABLET_PRESS	1
#define	GRIPPER_PRESS	-10000

// device setup wizard run? (or, default rate & reso set?)
void AFX_EXT_API SetDeviceSetupRun( BOOL fVal );
BOOL AFX_EXT_API WasDeviceSetupRun();

// mouse resolution (calculated from desktop window resolution)
// -- if width is not specified, it will use the current application monitor width (setting)
// -- otherwise it will use the width specified
AFX_EXT_API double GetMouseResolution( double dWidth = 0. );

///////////////////////////////////////////////////////////////////////////////
// Compression

BOOL AFX_EXT_API CompressData( CString szExp, CString szFileOut, CString szSubj = _T(""), CString szGrp = _T("") );
BOOL AFX_EXT_API CompressSupport( CString szExp, CString szFileOut );
BOOL AFX_EXT_API CompressAll( CString szPath, CString szFileOut, CString szFiles, CString szExpFile, CMemFile* );
ULONGLONG AFX_EXT_API UncompressSize( CString szFileIn );
BOOL AFX_EXT_API Uncompress( CString szFileIn, CString szLocation = _T("") );
BOOL AFX_EXT_API UncompressFile( CString szFileIn, CString szFile, CString szLocation );
void AFX_EXT_API ZIPFileList( CString szZIP, CStringList& lstFiles );
// caller responsible for cleanup of memory file & buffer
AFX_EXT_API CMemFile* ExtractToMemoryFile( CString szZIP, CString szFile );
// readstring implementation for CMemFile
BOOL AFX_EXT_API MemReadString( CMemFile*, CString& szData );
#define	READ_STRING( x, d ) \
	x->IsKindOf( RUNTIME_CLASS( CMemFile ) ) ? MemReadString( (CMemFile*)x, d ) : \
	( x->IsKindOf( RUNTIME_CLASS( CStdioFile ) ) ? ((CStdioFile*)x)->ReadString( d ) : \
	FALSE )
// writestring implementation for CMemFile
void AFX_EXT_API MemWriteString( CMemFile*, CString szData );
#define	WRITE_STRING( x, d ) \
	x->IsKindOf( RUNTIME_CLASS( CMemFile ) ) ? MemWriteString( (CMemFile*)x, d ) : \
	((CStdioFile*)x)->WriteString( d )

///////////////////////////////////////////////////////////////////////////////
// Multimedia

// available sounds
#define	SOUND_COUNT				10
#define	SOUND_RECORDING_START	0
#define	SOUND_RECORDING_END		1
#define	SOUND_STIMULUSW_START	2
#define	SOUND_STIMULUSW_END		3
#define	SOUND_STIMULUSP_START	4
#define	SOUND_STIMULUSP_END		5
#define	SOUND_TARGET_CORRECT	6
#define	SOUND_TARGET_WRONG		7
#define	SOUND_PENUP				8
#define	SOUND_PENDOWN			9

struct TrialRunInfo;

void AFX_EXT_API PlayASound( CString szFile );
void AFX_EXT_API PlayASound( CString szCondID, UINT nSound, TrialRunInfo* pInfo = NULL );
void AFX_EXT_API SetButtonBitmap( CButton* pButton, UINT nImageResourceID );
void AFX_EXT_API SetButtonBitmap( CStatic* pButton, UINT nImageResourceID );

///////////////////////////////////////////////////////////////////////////////
// temporary for now until made persistent (TODO)
// last experiment run info
void AFX_EXT_API SetLastExperimentInfo( CString szExpID, CString szGrpID, CString szSubjID );
void AFX_EXT_API GetLastExperimentInfo( CString& szExpID, CString& szGrpID, CString& szSubjID );

///////////////////////////////////////////////////////////////////////////////
// Tree/List Images
#define	IMG_ROOT				0
#define	IMG_FOLDER				1
#define	IMG_FOLDER_OPEN			2
#define	IMG_SHEET				3
#define	IMG_SUBJECT				4
#define	IMG_TRIAL				5
#define	IMG_TRIAL_BAD			6
#define	IMG_FOLDER_RED			7
#define	IMG_FOLDER_RED_OPEN		8
#define	IMG_EXPERIMENT			9
#define	IMG_GROUP				10
#define	IMG_CONDITION			11
#define	IMG_TRIAL_GOOD			12
#define	IMG_TRIAL_BLUE			13
#define	IMG_CONDITION_RO		14
#define	IMG_CONDITION_PO		15
#define	IMG_STIMULUS			16
#define	IMG_ELEMENT				17
#define	IMG_EXPERIMENT_GRIP		18
#define	IMG_EXPERIMENT_IMG		19
#define	IMG_CATEGORY			IMG_ROOT		// for now
#define	IMG_FEEDBACK			IMG_SHEET		// for now
#define	IMG_IMAGE				IMG_ROOT		// for now
#define IMG_TRIAL_D				20
#define IMG_TRIAL_BAD_D			21
#define IMG_TRIAL_GOOD_D		22
#define IMG_TRIAL_BLUE_D		23

///////////////////////////////////////////////////////////////////////////////
// client/server-related
void AFX_EXT_API SetClientServerInfo( CString szServer, CString szUser, CString szPassword );
BOOL AFX_EXT_API GetClientServerInfo( CString& szServer, CString& szUser, CString& szPassword );
BOOL AFX_EXT_API GetServerName( CString& szServer );
void AFX_EXT_API SetIsClientServerModeOn( BOOL );
BOOL AFX_EXT_API IsClientServerModeOn();
BOOL AFX_EXT_API VerifyLogin();
BOOL AFX_EXT_API ForceOperationMode( BOOL fClientServerOn = FALSE );

///////////////////////////////////////////////////////////////////////////////
// drawing options
void AFX_EXT_API SetLineTypeFixed( BOOL fVal );		// these 2 functions aren't useful at this point
BOOL AFX_EXT_API IsLineTypeFixed();
void AFX_EXT_API SetLineThickness( short nVal );
short AFX_EXT_API GetLineThickness();
void AFX_EXT_API SetIsLineByPressure( BOOL fVal );
BOOL AFX_EXT_API IsLineByPressure();
void AFX_EXT_API SetLineColor1( DWORD dwVal );
DWORD AFX_EXT_API GetLineColor1();
void AFX_EXT_API SetLineColor2( DWORD dwVal );
DWORD AFX_EXT_API GetLineColor2();
void AFX_EXT_API SetLineColor3( DWORD dwVal );
DWORD AFX_EXT_API GetLineColor3();
void AFX_EXT_API SetEllipseColor( DWORD dwVal );
DWORD AFX_EXT_API GetEllipseColor();
void AFX_EXT_API SetMPPColor( DWORD dwVal );
DWORD AFX_EXT_API GetMPPColor();
void AFX_EXT_API SetEllipseSize( short nVal );
AFX_EXT_API short GetEllipseSize();

///////////////////////////////////////////////////////////////////////////////
// Input Device Info

AFX_EXT_API BOOL GetDigitizerInfo( CString& szDesc, long& lRes, long& lX,
								   long& lY, short& nRate, BOOL& fInches,
								   BOOL& fTilt, short& nMaxPP );
void AFX_EXT_API GetDeviceInfo( short& nRate, short& nPressure, double& dRes,
							    CString& szDev, CString& szDim, BOOL& fTilt,
								CString& szMaxPP );
void AFX_EXT_API GetDeviceDimensions( double& dWidth, double& dHeight );

///////////////////////////////////////////////////////////////////////////////
// File removal macro - with exception

#define REMOVE_FILE( x ) \
	try { CFile::Remove( x ); } \
	catch( CFileException* e ) { e->ReportError(); }

// send to recycle bin
AFX_EXT_API BOOL SendToRecycleBin( CString szFile );

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


#endif // !defined(GLOBALS_H_)

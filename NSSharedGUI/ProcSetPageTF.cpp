// ProcSetPageTF.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageTF.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageTF property page

IMPLEMENT_DYNCREATE(ProcSetPageTF, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageTF, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_BN_CLICKED(IDC_RDO_FFT, OnBnClickedRdoFft)
	ON_BN_CLICKED(IDC_RDO_NOTFFT, OnBnClickedRdoNotfft)
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageTF::ProcSetPageTF( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageTF::IDD), m_pPS( pPS )
{
	m_dFilterFreq = 12.0;
	m_nDecimate = 1;
	m_fU = FALSE;
	m_fR = FALSE;
	m_fJ = FALSE;
	m_fA = FALSE;
	m_fO = FALSE;
	m_dRotBeta = 0.0;
	m_fFFT = TRUE;
	m_dSharpness = 1.75;
	m_nUpSampleFactor = 1;
	m_fRemTrailLift = TRUE;

	// check box/edit defaults
	m_editFF.SetDefaultValue( m_dFilterFreq );
	m_editSharpness.SetDefaultValue( m_dSharpness );
	m_editDec.SetDefaultValue( m_nDecimate );
	m_editRB.SetDefaultValue( m_dRotBeta );
	m_editUSF.SetDefaultValue( m_nUpSampleFactor );
	m_chkU.SetDefaultAsOff( !m_fU );
	m_chkRem.SetDefaultAsOff( !m_fRemTrailLift );

	if( m_pPS )
	{
		// get appropriate interface
		IProcSetFlags* pPSF = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		m_pPS->get_FilterFrequency( &m_dFilterFreq );
		m_pPS->get_FFT( &m_fFFT );
		m_pPS->get_Sharpness( &m_dSharpness );
		m_pPS->get_RemoveTrailingPenlift( &m_fRemTrailLift );
		short nVal = 0;
		m_pPS->get_Decimate( &nVal );
		m_nDecimate = nVal;
		pPS->get_UpSampleFactor( &nVal );
		m_nUpSampleFactor = nVal;
		if( m_nUpSampleFactor == 0 ) m_nUpSampleFactor = 1;
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_TF_U( &m_fU );
			pPSF->get_TF_R( &m_fR );
			pPSF->get_TF_J( &m_fJ );
			pPSF->get_TF_A( &m_fA );
			pPSF->get_TF_O( &m_fO );

			pPSF->Release();
		}
		m_pPS->get_RotationBeta( &m_dRotBeta );
	}
}

ProcSetPageTF::~ProcSetPageTF()
{
}

void ProcSetPageTF::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_FILTERFREQ, m_dFilterFreq);
	DDX_Text(pDX, IDC_EDIT_DECIMATE, m_nDecimate);
	DDX_Check(pDX, IDC_CHK_TFU, m_fU);
	DDX_Check(pDX, IDC_CHK_TFJ, m_fJ);
	DDX_Check(pDX, IDC_CHK_TFO, m_fO);
	DDX_Text(pDX, IDC_EDIT_ROTBETA, m_dRotBeta);
	DDX_Text(pDX, IDC_EDIT_SHARPNESS, m_dSharpness);
	DDX_Text(pDX, IDC_EDIT_UPSAMPLE, m_nUpSampleFactor);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Check(pDX, IDC_CHK_TF_RTP, m_fRemTrailLift);

	DDX_Control(pDX, IDC_EDIT_FILTERFREQ, m_editFF);
	DDX_Control(pDX, IDC_EDIT_SHARPNESS, m_editSharpness);
	DDX_Control(pDX, IDC_EDIT_DECIMATE, m_editDec);
	DDX_Control(pDX, IDC_EDIT_ROTBETA, m_editRB);
	DDX_Control(pDX, IDC_EDIT_UPSAMPLE, m_editUSF);
	DDX_Control(pDX, IDC_CHK_TFU, m_chkU);
	DDX_Control(pDX, IDC_CHK_TF_RTP, m_chkRem);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageTF message handlers

BOOL ProcSetPageTF::OnApply() 
{
	if( !m_pPS ) return FALSE;

	UpdateData( TRUE );

	if( ( m_nUpSampleFactor != 1 ) && ( m_nUpSampleFactor != 2 ) &&
		( m_nUpSampleFactor != 4 ) && ( m_nUpSampleFactor != 8 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 8 );
		BCGPMessageBox( _T("Up-Sample Factor can only be 1, 2, 4 or 8.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_UPSAMPLE );
		pWnd->SetFocus();
		return FALSE;
	}

	IProcSetFlags* pPSF = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) ) return FALSE;

	m_pPS->put_FilterFrequency( m_dFilterFreq );
	m_pPS->put_Decimate( m_nDecimate );
	m_pPS->put_FFT( GetCheckedRadioButton( IDC_RDO_FFT, IDC_RDO_NOTFFT ) == IDC_RDO_FFT );
	m_pPS->put_Sharpness( m_dSharpness );
	m_pPS->put_RemoveTrailingPenlift( m_fRemTrailLift );
	m_pPS->put_UpSampleFactor( m_nUpSampleFactor );
	pPSF->put_TF_U( m_fU );
	pPSF->put_TF_J( m_fJ );
	pPSF->put_TF_R( GetCheckedRadioButton( IDC_RDO_TFR, IDS_PSPT_ACCEL ) == IDC_RDO_TFR );
	pPSF->put_TF_A( GetCheckedRadioButton( IDC_RDO_TFR, IDS_PSPT_ACCEL ) == IDC_RDO_TFA );
	pPSF->put_TF_O( m_fO );
	m_pPS->put_RotationBeta( m_dRotBeta );

	pPSF->Release();

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageTF::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageTF::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );
	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	double dFilterFreq = 12.0, dRotBeta = 0.0, dSharpness = 1.75;
	int nDecimate = 1, nUpSampleFactor = 1;
	BOOL fU = FALSE, fR = FALSE, fJ = FALSE, fA = FALSE, fO = FALSE, fFFT = TRUE, fRemLift = TRUE;
	if( SUCCEEDED ( hr ) )
	{
		IProcSetFlags* pPSF = NULL;
		hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		pPS->get_FilterFrequency( &dFilterFreq );
		pPS->get_FFT( &fFFT );
		pPS->get_Sharpness( & dSharpness );
		pPS->get_RemoveTrailingPenlift( &fRemLift );
		short nVal = 0;
		pPS->get_Decimate( &nVal );
		nDecimate = nVal;
		pPS->get_UpSampleFactor( &nVal );
		nUpSampleFactor = nVal;
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_TF_U( &fU );
			pPSF->get_TF_R( &fR );
			pPSF->get_TF_J( &fJ );
			pPSF->get_TF_A( &fA );
			pPSF->get_TF_O( &fO );

			pPSF->Release();
		}
		pPS->get_RotationBeta( &dRotBeta );
		pPS->Release();
	}

	// Tooltip support
	CString szData, szTmp;
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_FILTERFREQ );
	szData.LoadString( IDS_PSPT_FF );
	szTmp.Format( _T(" [DEFAULT=%g]"), dFilterFreq );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Filter Frequency") );

	pWnd = GetDlgItem( IDC_EDIT_DECIMATE );
	szData.LoadString( IDS_PSPT_NTH );
	szTmp.Format( _T(" [DEFAULT=%d]"), nDecimate );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Decimate") );

	pWnd = GetDlgItem( IDC_RDO_TFR );
	szData.LoadString( IDS_PSPT_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Position") );
	pWnd = GetDlgItem( IDC_RDO_TFV );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Calculate spectrum of velocity [DEFAULT]."), _T("Velocity") );
	pWnd = GetDlgItem( IDC_RDO_TFA );
	szData.LoadString( IDS_PSPT_ACCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Acceleration") );
// 	CheckRadioButton( IDC_RDO_TFR, IDC_RDO_TFA,
// 							m_fR ? IDC_RDO_TFR : ( m_fA ? IDC_RDO_TFA : IDC_RDO_TFV ) );

	pWnd = GetDlgItem( IDC_CHK_TFU );
	szData.LoadString( IDS_PSPT_AUTO );
	szTmp.Format( _T(" [DEFAULT=%s]"), fU ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Unrotate") );

	pWnd = GetDlgItem( IDC_CHK_TF_RTP );
	szData.Format( _T("Remove the trailing penlift from the recording.\nThis will remove all points after the last sample with a pen pressure above the minimum. [DEFAULT=%s]"), fRemLift ? _T("TRUE") : _T("FALSE") );
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Remove Trailing Penlift") );

	pWnd = GetDlgItem( IDC_EDIT_ROTBETA );
	szData.LoadString( IDS_PSPG_ROT );
	szTmp.Format( _T(" [DEFAULT=%g]"), dRotBeta );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Additional Rotation") );

	pWnd = GetDlgItem( IDC_RDO_FFT );
	szData = _T("Use FFT low pass filter method");
	szTmp.Format( _T(" [DEFAULT=%s]"), fFFT ? _T("FFT") : _T("Butterworth") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("FFT") );

	pWnd = GetDlgItem( IDC_RDO_NOTFFT );
	szData = _T("Use Butterworth low pass filter method");
	szTmp.Format( _T(" [DEFAULT=%s]"), fFFT ? _T("FFT") : _T("Butterworth") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Butterworth") );

	pWnd = GetDlgItem( IDC_EDIT_SHARPNESS );
	szData = _T("Specify sharpness for FFT low pass filter method. NOTE: Half transition width = Filter Frequency / Sharpness.");
	szTmp.Format( _T(" [DEFAULT=%g]"), dSharpness );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Sharpness") );

	pWnd = GetDlgItem( IDC_EDIT_UPSAMPLE );
	szData = _T("Specify up-sample factor. Valid values are 1, 2, 4 and 8.");
	szTmp.Format( _T(" [DEFAULT=%d]"), nUpSampleFactor );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Up-Sample Factor") );

	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	CheckRadioButton( IDC_RDO_FFT, IDC_RDO_NOTFFT, m_fFFT ? IDC_RDO_FFT : IDC_RDO_NOTFFT );

	if( !m_fFFT )
	{
		pWnd = GetDlgItem( IDC_EDIT_SHARPNESS );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageTF::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageTF::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_timefun.html"), this );
	return TRUE;
}

void ProcSetPageTF::OnBnReset() 
{
	m_dFilterFreq = 12.0;
	m_nDecimate = 1;
	m_fU = FALSE;
	m_fR = FALSE;
	m_fJ = FALSE;
	m_fA = FALSE;
	m_fO = FALSE;
	m_dRotBeta = 0.0;
	m_fFFT = TRUE;
	m_dSharpness = 1.75;
	m_nUpSampleFactor = 1;
	m_fRemTrailLift = TRUE;

	CheckRadioButton( IDC_RDO_FFT, IDC_RDO_NOTFFT, IDC_RDO_FFT );
// 	CheckRadioButton( IDC_RDO_TFR, IDS_PSPT_ACCEL,
// 							m_fR ? IDC_RDO_TFR : ( m_fA ? IDC_RDO_TFA : IDC_RDO_TFV ) );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_SHARPNESS );
	if( pWnd ) pWnd->EnableWindow( TRUE );

	UpdateData( FALSE );
}

void ProcSetPageTF::OnBnClickedRdoFft()
{
	CWnd* pWnd = GetDlgItem( IDC_EDIT_SHARPNESS );
	if( pWnd ) pWnd->EnableWindow( TRUE );
}

void ProcSetPageTF::OnBnClickedRdoNotfft()
{
	CWnd* pWnd = GetDlgItem( IDC_EDIT_SHARPNESS );
	if( pWnd ) pWnd->EnableWindow( FALSE );
}

void ProcSetPageTF::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void ProcSetPageTF::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 15 );
}

// ElementPageGeneral.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ElementPageGeneral.h"
#include "ElementSheet.h"
#include "..\CTreeLink\DBCommon.h"
#include "Elements.h"
#include "Stimuli.h"
#include "RelationshipDlg.h"
#include "Cats.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ElementPageGeneral property page

IMPLEMENT_DYNCREATE(ElementPageGeneral, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ElementPageGeneral, CBCGPPropertyPage)
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnKillfocusEditId)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnBrowse)
	ON_BN_CLICKED(IDC_RDO_HWR, OnRdoClick)
	ON_BN_CLICKED(IDC_RDO_SHAPE, OnRdoClick)
	ON_BN_CLICKED(IDC_RDO_IMG, OnRdoClick)
	ON_BN_CLICKED(IDC_CHK_INDEFINITE, OnClickIndefinite)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ElementPageGeneral::ElementPageGeneral( IElement* pE, BOOL fDup )
	: CBCGPPropertyPage(ElementPageGeneral::IDD), m_pE( pE ), m_fDup( fDup )
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_fTarget = FALSE;
	m_szBMP = _T("");
	m_dStart = 0.;
	m_dDuration = 0.;
	m_fIndefinite = FALSE;
	m_fIsImage = FALSE;
	m_fInit = FALSE;
	m_fIndefinite = ( m_dDuration == 0. );

	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	Elements elem;
	elem.SetDataPath( szPath );
	HRESULT hr = elem.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		elem.GetID( szItem );
		m_lstItems.AddTail( szItem );
		hr = elem.GetNext();
	}
}

ElementPageGeneral::~ElementPageGeneral()
{
}

void ElementPageGeneral::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_Check(pDX, IDC_CHK_TARGET, m_fTarget);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szBMP);
	DDX_Control(pDX, IDC_EDIT_PATH, m_cboBMP);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnBrowse);
	DDX_Text(pDX, IDC_EDIT_START, m_dStart);
	DDX_Text(pDX, IDC_EDIT_DURATION, m_dDuration);
	DDX_Check(pDX, IDC_CHK_INDEFINITE, m_fIndefinite);
	DDX_Control(pDX, IDC_CBO_CAT, m_cboCat);
}

/////////////////////////////////////////////////////////////////////////////
// ElementPageGeneral message handlers

BOOL ElementPageGeneral::OnApply() 
{
	if( !m_pE ) return FALSE;

	UpdateData( TRUE );

	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The element ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("The element description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if(  m_dStart < 0. )
	{
		BCGPMessageBox( _T("Start time cannot be negative.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_START );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if(  m_dDuration < 0. )
	{
		BCGPMessageBox( _T("Duration cannot be negative.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DURATION );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return FALSE;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	if( IsImage() && ( m_szBMP == _T("") ) )
	{
		BCGPMessageBox( _T("Please specify a bitmap file.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	m_pE->put_ID( m_szID.AllocSysString() );
	m_pE->put_Description( m_szDesc.AllocSysString() );
	m_pE->put_IsTarget( m_fTarget );
	if( IsImage() )
	{
		m_pE->put_IsImage( TRUE );
		m_pE->put_Pattern( m_szBMP.AllocSysString() );
	}
	else m_pE->put_IsImage( FALSE );
	m_pE->put_Start( m_dStart );
	m_pE->put_Duration( m_dDuration );

	int nPos = m_cboCat.GetCurSel();
	if( nPos > 0 )
	{
		m_cboCat.GetLBText( nPos, m_szCatID );
		CString szDelim;
		::GetDelimiter( szDelim );
		nPos = m_szCatID.Find( szDelim );
		if( nPos != -1 ) m_szCatID = m_szCatID.Right( 3 );
	}
	else m_szCatID = _T("");
	m_pE->put_CatID( m_szCatID.AllocSysString() );

	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	// TODO: If target is turned off and it is in a stimulus as a target,
	// TODO: what do we do?

	return CBCGPPropertyPage::OnApply();
}

void ElementPageGeneral::OnKillfocusEditId() 
{
	if( !m_fInit ) return;
	UpdateData( TRUE );
	if( m_szID == _T("") ) return;

	if( m_lstItems.Find( m_szID ) )
	{
		// TODO: Add relationship stuff
		int nResp = BCGPMessageBox( _T("This ID already exists. Would you like to see how it is currently being used?"), MB_YESNO );
		if( nResp == IDYES )
		{
			m_fInit = FALSE;
			Elements elem;
			if( elem.IsValid() )
			{
				HRESULT hr = elem.Find( m_szID );
				if( SUCCEEDED( hr ) )
				{
					CString szItem;
					elem.GetDescriptionWithID( szItem );

					RelationshipDlg d( this, REL_ELEMENT, szItem );
					d.DoModal();
				}
				else BCGPMessageBox( _T("Could not find element.") );
			}
			else BCGPMessageBox( _T("Unable to create element object.") );
			m_fInit = TRUE;
		}

		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
}

BOOL ElementPageGeneral::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ElementPageGeneral::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	m_bnBrowse.SetImage( IDB_OPEN );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_ELEM );
	m_tooltip.SetTitle( _T("ELEMENT") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("ID/abbreviation of element."), _T("ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Description of element."), _T("Description") );
	pWnd = GetDlgItem( IDC_CHK_TARGET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check here to make this an active target, which changes appearance when reached."), _T("Target") );
	pWnd = GetDlgItem( IDC_RDO_SHAPE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("This will create a definable shape for the element."), _T("Shape") );
	pWnd = GetDlgItem( IDC_RDO_HWR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("This will cause the element to be defined by a pattern (raw data file)."), _T("Pattern") );
	pWnd = GetDlgItem( IDC_RDO_IMG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("This will cause the element to be defined by a bitmap."), _T("Bitmap") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an image to be displayed.\nYou can add additional images by clicking the browse button."), _T("Bitmap File") );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Browse for an image to add to the repository. The repository is the \"stimuli\" folder in the current user's data path."), _T("Browse") );
	pWnd = GetDlgItem( IDC_EDIT_START );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The time (in seconds) this element is displayed after the stimulus is shown."), _T("Start Time") );
	pWnd = GetDlgItem( IDC_EDIT_DURATION );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The duration (in seconds) after this element is displayed it will remain visible."), _T("Duration") );
	pWnd = GetDlgItem( IDC_CHK_INDEFINITE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to set the visibility of this element to the lifetime of the stimulus AFTER it is initially shown."), _T("Indefinite") );
	pWnd = GetDlgItem( IDC_CBO_CAT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the category (if any) to which this element belongs (tree organization)."), _T("Category") );

	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	if( pSheet && !pSheet->m_fNew && !m_fDup )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// get values from element
	if( m_pE )
	{
		BSTR bstr = NULL;
		if( !m_fDup )
		{
			m_pE->get_ID( &bstr );
			m_szID = bstr;
		}
		m_pE->get_Description( &bstr );
		m_szDesc = bstr;
		m_pE->get_IsTarget( &m_fTarget );
		m_pE->get_IsImage( &m_fIsImage );
		if( m_fIsImage )
		{
			m_pE->get_Pattern( &bstr );
			m_szBMP = bstr;
		}
		m_pE->get_Start( &m_dStart );
		m_pE->get_Duration( &m_dDuration );
		m_pE->get_CatID( &bstr );
		m_szCatID = bstr;
		m_fIndefinite = ( m_dDuration == 0. );
		UpdateData( FALSE );
	}

	// shape
	int nItem = IDC_RDO_SHAPE;
	if( m_pE && pSheet && !pSheet->m_fNew )
	{
		BSTR bstr = NULL;
		m_pE->get_Pattern( &bstr );
		CString szItem = bstr;
		if( !m_fIsImage && szItem != _T("") ) nItem = IDC_RDO_HWR;
		else if( m_fIsImage ) nItem = IDC_RDO_IMG;
	}
	CheckRadioButton( IDC_RDO_SHAPE, IDC_RDO_IMG, nItem );

	// fill image list
	FillImageList();

	// enable disable
	pWnd = GetDlgItem( IDC_EDIT_PATH );
	pWnd->EnableWindow( m_fIsImage );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	pWnd->EnableWindow( m_fIsImage );
	if( m_dDuration == 0. )
	{
		pWnd = GetDlgItem( IDC_EDIT_DURATION );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	BOOL fEnable = ( nItem == IDC_RDO_SHAPE );
	pWnd = GetDlgItem( IDC_CHK_TARGET );
	pWnd->EnableWindow( fEnable );

	// CATEGORIES
	m_cboCat.AddString( _T("No Category") );
	m_cboCat.SetCurSel( 0 );	// None

	Cats cat;
	int nSelCur = -1, nPos;
	CString szID, szDelim, szItem;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	HRESULT hr = cat.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cat.GetDescriptionWithID( szItem );

		nItem = m_cboCat.AddString( szItem );
		nPos = szItem.Find( szDelim );
		szID = szItem.Mid( nPos + 2 );
		if( szID == m_szCatID ) m_cboCat.SetCurSel( nItem );

		hr = cat.GetNext();
	}

	m_fInit = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ElementPageGeneral::FillImageList()
{
	m_cboBMP.ResetContent();

	CString szFind, szFile;
	Stimuluss::GetBaseFile( szFind );
	szFind += _T("*.BMP");
	int nSel = -1;
	CFileFind ff;
	BOOL fCont = ff.FindFile( szFind );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		if( !ff.IsDirectory() )
		{
			int nItem = m_cboBMP.AddString( szFile );
			if( szFile == m_szBMP ) nSel = nItem;
		}
	}
	if( nSel != -1 )
	{
		m_cboBMP.SetCurSel( nSel );
	}
	else if( m_szBMP != _T("") )
	{
		CString szMsg;
		szMsg.Format( _T("The following image was specified as the image to use for this element:\n\n%s\n\nThis file cannot be found."), m_szBMP );
		BCGPMessageBox( szMsg );
		m_szBMP = _T("");
		UpdateData( FALSE );
	}
}

BOOL ElementPageGeneral::IsTarget()
{
	UpdateData( TRUE );
	return m_fTarget;
}

BOOL ElementPageGeneral::IsPattern()
{
	int nItem = GetCheckedRadioButton( IDC_RDO_SHAPE, IDC_RDO_IMG );
	return( nItem == IDC_RDO_HWR );
}

BOOL ElementPageGeneral::IsImage()
{
	int nItem = GetCheckedRadioButton( IDC_RDO_SHAPE, IDC_RDO_IMG );
	return( nItem == IDC_RDO_IMG );
}

void ElementPageGeneral::OnBnBrowse() 
{
	UpdateData( TRUE );

	CString szFilter, szMsg;
	szFilter = _T("BMP files (*.BMP)|*.BMP|");

	CString szFile;
	if( m_szBMP == _T("") ) szFile = _T("*.BMP");
	else szFile = m_szBMP ;

	CFileDialog d( TRUE, _T("BMP"), szFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
				   szFilter, this );
	if( d.DoModal() == IDCANCEL ) return;

	// source & dest files
	CString szSource = d.GetPathName(), szDest;
	Stimuluss::GetBaseFile( szDest );
	int nPos = szSource.ReverseFind( '\\' );
	if( nPos != -1 ) szSource = szSource.Left( nPos );
	if( szSource == szDest )
	{
		szMsg.Format( _T("You have selected a file that is already in the repository \n(%s)."), szDest );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONEXCLAMATION );
		return;
	}
	szSource = d.GetPathName();
	CString szRep = szDest;
	szDest += d.GetFileName();

	// is this file already in the image repository?
	BOOL fExists = FALSE;
	CFileFind ff;
	if( ff.FindFile( szDest ) )
	{
		fExists = TRUE;
		szMsg.Format( _T("This image file already exists in the repository\n(%s).\n\nOverwrite?"), szRep );
		if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;
	}

	// warn user that we'll be copying the file into the folder
	szMsg.Format( _T("The selected image will be copied to the image repository\n(%s).\n\nProceed?"), szRep );
	if( !fExists && BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	// copy file
	CopyFile( szSource, szDest, FALSE );

	// update GUI
	m_szBMP = d.GetFileName();
	UpdateData( FALSE );
	m_cboBMP.SelectString( -1, m_szBMP );

	if( !fExists ) FillImageList();
}

void ElementPageGeneral::OnRdoClick() 
{
	UpdateData( TRUE );

	int nItem = GetCheckedRadioButton( IDC_RDO_SHAPE, IDC_RDO_IMG );
	BOOL fEnable = ( nItem == IDC_RDO_IMG );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
	pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	pWnd->EnableWindow( fEnable );

	fEnable = ( nItem == IDC_RDO_SHAPE );
	pWnd = GetDlgItem( IDC_CHK_TARGET );
	pWnd->EnableWindow( fEnable );
	if( !fEnable )
	{
		m_fTarget = FALSE;
		UpdateData( FALSE );
	}
}

BOOL ElementPageGeneral::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ElementPageGeneral::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("element_general.html"), this );
	return TRUE;
}

void ElementPageGeneral::OnClickIndefinite()
{
	UpdateData( TRUE );

	if( m_fIndefinite )
	{
		m_dDuration = 0.;
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DURATION );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		UpdateData( FALSE );
	}
	else
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DURATION );
		if( pWnd ) pWnd->EnableWindow( TRUE );
	}
}

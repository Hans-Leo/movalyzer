// RelationshipDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\Common\UserObj.h"
#include "RelationshipDlg.h"
#include "..\DataMod\DataMod.h"
#include "Subjects.h"
#include "Groups.h"
#include "Experiments.h"
#include "Conditions.h"
#include "Stimuli.h"
#include "Elements.h"
#include "Cats.h"
#include "Feedbacks.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	DO_FILL( item, cbo ) \
	item itm; \
	if( itm.IsValid() ) \
	{ \
		CString szItem; \
		nItem = 0; \
		hr = itm.ResetToStart(); \
		while( SUCCEEDED( hr ) ) \
		{ \
			itm.GetDescriptionWithID( szItem ); \
			cbo.AddString( szItem ); \
			hr = itm.GetNext(); \
		} \
	}

#define	DO_FILL_SUBJ( item, cbo ) \
	item itm; \
	if( itm.IsValid() ) \
	{ \
		CString szItem; \
		nItem = 0; \
		hr = itm.ResetToStart(); \
		while( SUCCEEDED( hr ) ) \
		{ \
			itm.GetNameWithCodeID( szItem ); \
			cbo.AddString( szItem ); \
			hr = itm.GetNext(); \
		} \
	}

#define CHECK_ID( item ) \
	if( !fDelim ) \
	{ \
		item itm; \
		if( SUCCEEDED( itm.Find( m_szItem ) ) ) \
			itm.GetDescriptionWithID( m_szItem ); \
	}

#define CHECK_ID_SUBJ( item ) \
	if( !fDelim ) \
	{ \
		item itm; \
		if( SUCCEEDED( itm.Find( m_szItem ) ) ) \
			itm.GetNameWithCodeID( m_szItem ); \
	}

#define	ENABLE_START(cbo, method) \
	cbo.SelectString( -1, m_szItem ); \
	cbo.SetFocus(); \
	method( m_szItem );

#define ON_SEL_CHANGE(cbo, method) \
	SetCombos( &cbo ); \
	m_tree.DeleteAllItems(); \
	int nItem = cbo.GetCurSel(); \
	if( nItem == CB_ERR ) return; \
	CString szItem; \
	cbo.GetLBText( nItem, szItem ); \
	if( szItem == _T("") ) return; \
	method( szItem ); \


/////////////////////////////////////////////////////////////////////////////
// RelationshipDlg dialog

BEGIN_MESSAGE_MAP(RelationshipDlg, CBCGPDialog)
	ON_CBN_SELCHANGE(IDC_CBO_SUBJECTS, OnSelchangeCboSubjects)
	ON_CBN_SELCHANGE(IDC_CBO_GROUPS, OnSelchangeCboGroups)
	ON_CBN_SELCHANGE(IDC_CBO_CONDITIONS, OnSelchangeCboConditions)
	ON_WM_HELPINFO()
	ON_CBN_SELCHANGE(IDC_CBO_STIMULI, OnSelchangeCboStimuli)
	ON_CBN_SELCHANGE(IDC_CBO_ELEMENTS, OnSelchangeCboElements)
	ON_CBN_SELCHANGE(IDC_CBO_CATS, OnSelchangeCboCats)
	ON_CBN_SELCHANGE(IDC_CBO_FEEDBACK, OnSelchangeCboFeedbacks)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE, OnNMDblclkTree)
END_MESSAGE_MAP()

RelationshipDlg::RelationshipDlg(CWnd* pParent, UINT nType, CString szItem )
	: CBCGPDialog(RelationshipDlg::IDD, pParent), m_hGrps( NULL ), m_hSubjs( NULL ),
	  m_hConds( NULL ), m_nType( nType ), m_szItem( szItem ), m_hStim( NULL ),
	  m_hElem( NULL ), m_hCat( NULL ), m_hFb( NULL )
{
}

void RelationshipDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_FEEDBACK, m_cboFeedback);
	DDX_Control(pDX, IDC_CBO_CATS, m_cboCat);
	DDX_Control(pDX, IDC_CBO_ELEMENTS, m_cboElem);
	DDX_Control(pDX, IDC_CBO_STIMULI, m_cboStim);
	DDX_Control(pDX, IDC_CBO_CONDITIONS, m_cboConditions);
	DDX_Control(pDX, IDC_CBO_SUBJECTS, m_cboSubjects);
	DDX_Control(pDX, IDC_CBO_GROUPS, m_cboGroups);
	DDX_Control(pDX, IDC_TREE, m_tree);
}

/////////////////////////////////////////////////////////////////////////////
// RelationshipDlg message handlers

void RelationshipDlg::ShowGroup( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hGrps = TVI_ROOT;
	else m_hGrps = hRoot;

	// Get ID of selected group
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_GROUP, IMG_GROUP, m_hGrps );
	m_tree.SetItemData( hItem, TI_GROUP );

	// Create objects
	IExperimentMember* pEM = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return;
	}
	Experiments exp;
	if( !exp.IsValid() ) return;

	// Find all group associations
	BSTR bstrExpID = NULL, bstrGrpID = NULL;
	CString szExpID, szExp, szGrpID;
	HTREEITEM hItemE = NULL;
	CStringList usedL;	// A given group can only participate once under a given exp

	hr = pEM->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// Current record Group ID
		pEM->get_GroupID( &bstrGrpID );
		szGrpID = bstrGrpID;

		if( szGrpID == szID )
		{
			// Experiment ID
			pEM->get_ExperimentID( &bstrExpID );
			szExpID = bstrExpID;

			// Skip if already used
			if( !usedL.Find( szExpID ) )
			{
				// Find experiment
				hr = exp.Find( bstrExpID );
				if( SUCCEEDED( hr ) )
				{
					exp.GetDescriptionWithID( szItem );
					hItemE = m_tree.InsertItem( szItem, IMG_EXPERIMENT, IMG_EXPERIMENT, hItem );
					m_tree.SetItemData( hItemE, TI_EXPERIMENT );
					usedL.AddTail( szExpID );
				}
			}
		}

		// Get next record
		hr = pEM->GetNext();
	}

	m_tree.Expand( m_hGrps, TVE_EXPAND );
	m_tree.Expand( hItem, TVE_EXPAND );

	// Cleanup
	pEM->Release();
}

void RelationshipDlg::ShowCondition( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hConds = TVI_ROOT;
	else m_hConds = hRoot;

	// Get ID of selected group
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, m_hConds );
	m_tree.SetItemData( hItem, TI_CONDITION );

	// Create objects
	IExperimentCondition* pEC = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
								   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return;
	}
	Experiments exp;
	if( !exp.IsValid() ) return;

	// Find all group associations
	BSTR bstrExpID = NULL, bstrCondID = NULL;
	CString szExpID, szExp, szCondID;
	HTREEITEM hItemE = NULL;

	hr = pEC->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// Current record Group ID
		pEC->get_ConditionID( &bstrCondID );
		szCondID = bstrCondID;

		if( szCondID == szID )
		{
			// Experiment ID
			pEC->get_ExperimentID( &bstrExpID );
			szExpID = bstrExpID;

			// Find experiment
			hr = exp.Find( bstrExpID );
			if( SUCCEEDED( hr ) )
			{
				exp.GetDescriptionWithID( szItem );
				hItemE = m_tree.InsertItem( szItem, IMG_EXPERIMENT, IMG_EXPERIMENT, hItem );
				m_tree.SetItemData( hItemE, TI_EXPERIMENT );
			}

		}

		// Get next record
		hr = pEC->GetNext();
	}

	m_tree.Expand( m_hConds, TVE_EXPAND );
	m_tree.Expand( hItem, TVE_EXPAND );

	// Cleanup
	pEC->Release();
}

void RelationshipDlg::ShowSubject( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hSubjs = TVI_ROOT;
	else m_hSubjs = hRoot;

	// Get ID of selected subject
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_SUBJECT, IMG_SUBJECT, m_hGrps );
	m_tree.SetItemData( hItem, TI_SUBJECT );

	// Create objects
	// 27Sep07 - GMB: For SOME reason, initial calls to this dialog results
	// in the list not starting from the first record EVEN THOUGH the calls
	// are correct to ctree functions. This *HACK* (ALERT) solution solves
	// the problem by creating the object, calling reset (first record),
	// destroying the object, then recreating
	// NOTE: It is likely this is related to the life span of the EML
	// object in LeftView

	// Create ExperimentMember object
	IExperimentMember* pEM = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return;
	}
	// reset to first record (see aboe)
	pEM->ResetToStart();
	// destroy
	pEM->Release();
	// recreate
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return;
	}
	Experiments exp;
	if( !exp.IsValid() )
	{
		pEM->Release();
		return;
	}
	Groups grp;
	if( !grp.IsValid() )
	{
		pEM->Release();
		return;
	}

	// We attempt to sort so we dont repeat (should be fun...oooyah!)
	BSTR bstrExpID = NULL, bstrGrpID = NULL, bstrSubjID = NULL;
	CString szExpID, szGrpID, szSubjID, szTemp;
	CStringList sortL;
	POSITION pos = NULL, posLast = NULL;
	CStringList regL;

	// Fill temp list with appropriate data based on subject (not sorted)
	hr = pEM->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// Current subject ID
		pEM->get_SubjectID( &bstrSubjID );
		szSubjID = bstrSubjID;

		// Do we want this one
		if( szSubjID == szID )
		{
			// Other IDs
			pEM->get_ExperimentID( &bstrExpID );
			szExpID = bstrExpID;
			pEM->get_GroupID( &bstrGrpID );
			szGrpID = bstrGrpID;

			// Place in list
			szExpID += _T("-");
			szExpID += szGrpID;
			regL.AddTail( szExpID );
		}

		// Next record
		hr = pEM->GetNext();
	}

	// Now fill sort list
	CString szTemp2;
	while( regL.GetCount() > 0 )
	{
		// Get head from temp list
		szTemp = regL.GetHead();

		// Now traverse sort list to locate where to place
		BOOL fInserted = FALSE;
		pos = sortL.GetHeadPosition();
		while( pos )
		{
			szTemp2 = sortL.GetNext( pos );

			if( szTemp <= szTemp2 )
			{
				sortL.InsertBefore( pos, szTemp );
				fInserted = TRUE;
				break;
			}
		}

		if( !pos && !fInserted ) sortL.AddTail( szTemp );
		regL.RemoveHead();
	}

	// Find all group associations
	BSTR bstrExp = NULL;
	CString szExp, szGrp;
	HTREEITEM hGrpF = NULL, hItemE = NULL;
	int nPos = -1;

	pos = sortL.GetHeadPosition();
	while( pos )
	{
		// Get exp, grp IDs
		szTemp = sortL.GetNext( pos );
		nPos = szTemp.Find( _T("-") );
		szExpID = szTemp.Left( nPos );
		szGrpID = szTemp.Mid( nPos + 1 );

		// Add to tree
		hr = grp.Find( szGrpID );
		if( SUCCEEDED( hr ) )
		{
			grp.GetDescriptionWithID( szTemp );
			hGrpF = m_tree.InsertItem( szTemp, IMG_GROUP, IMG_GROUP, hItem );
			m_tree.SetItemData( hGrpF, TI_GROUP );

			hr = exp.Find( szExpID );
			if( SUCCEEDED( hr ) )
			{
				exp.GetDescriptionWithID( szTemp );
				hItemE = m_tree.InsertItem( szTemp, IMG_EXPERIMENT, IMG_EXPERIMENT, hGrpF );
				m_tree.SetItemData( hItemE, TI_EXPERIMENT );
			}
		}
	}

	// Expand tree
	m_tree.Expand( hItem, TVE_EXPAND );
	hItem = m_tree.GetNextItem( hItem, TVGN_CHILD );
	while( hItem )
	{
		m_tree.Expand( hItem, TVE_EXPAND );
		hItem = m_tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// Cleanup
	pEM->Release();
}

void RelationshipDlg::ShowStimulus( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hStim = TVI_ROOT;
	else m_hStim = hRoot;

	// Get ID of selected stimuls
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_STIMULUS, IMG_STIMULUS, m_hStim );
	m_tree.SetItemData( hItem, TI_STIMULUS );

	// Create objects
	NSConditions cond;
	if( !cond.IsValid() ) return;

	// 27Sep07 - GMB: For SOME reason, initial calls to this dialog results
	// in the list not starting from the first record EVEN THOUGH the calls
	// are correct to ctree functions. This *HACK* (ALERT) solution solves
	// the problem by creating the object, calling reset (first record),
	// destroying the object, then recreating
	// NOTE: It is likely this is related to the life span of the ECL
	// object in LeftView

	// Create ExperimentCondition object
	IExperimentCondition* pEC = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
								   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return;
	}
	// reset to first record (see above)
	pEC->ResetToStart();
	// destroy
	pEC->Release();
	// recreate
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return;
	}
	Experiments exp;
	if( !exp.IsValid() )
	{
		pEC->Release();
		return;
	}

	// Find all stimulus associations
	CString szStimW, szStimP, szStim, szCond, szCondID, szExpID, szTemp;
	HTREEITEM hItemC = NULL, hItemE = NULL;
	BSTR bstr = NULL;
	CStringList lstUsed;
	hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetID( szCond );
		cond.GetStimulusWarning( szStimW );
		cond.GetStimulusPrecue( szStimP );
		cond.GetStimulus( szStim );

		if( ( szStimW == szID ) || ( szStimP == szID ) || ( szStim == szID ) )
		{
			// Add
			cond.GetDescriptionWithID( szItem );
			hItemC = m_tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, hItem );
			m_tree.SetItemData( hItemC, TI_CONDITION );

			// Find all experiment associations
			hr = pEC->ResetToStart();
			while( SUCCEEDED( hr ) )
			{
				// Current condition ID
				pEC->get_ConditionID( &bstr );
				szCondID = bstr;

				// Do we want this one
				if( ( szCondID == szCond ) && !lstUsed.Find( szCondID ) )
				{
					// Experiment ID
					pEC->get_ExperimentID( &bstr );
					szExpID = bstr;

					// add to list
					if( SUCCEEDED( exp.Find( szExpID ) ) )
					{
						exp.GetDescriptionWithID( szTemp );
						hItemC = m_tree.InsertItem( szTemp, IMG_EXPERIMENT, IMG_EXPERIMENT, hItemC );
						m_tree.SetItemData( hItemC, TI_EXPERIMENT );
						lstUsed.AddTail( szCondID );
					}
				}

				// Next record
				hr = pEC->GetNext();
			}
		}

		// Get next record
		hr = cond.GetNext();
	}

	// Expand tree
	m_tree.Expand( m_hStim, TVE_EXPAND );
	m_tree.Expand( hItem, TVE_EXPAND );
	hItem = m_tree.GetNextItem( hItem, TVGN_CHILD );
	while( hItem )
	{
		m_tree.Expand( hItem, TVE_EXPAND );
		hItem = m_tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// Cleanup
	pEC->Release();
}

void RelationshipDlg::ShowElement( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hElem = TVI_ROOT;
	else m_hElem = hRoot;

	// Get ID of selected element
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_ELEMENT, IMG_ELEMENT, m_hElem );
	m_tree.SetItemData( hItem, TI_ELEMENT );

	// Create objects
	IStimulusElement* pSE = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
									 IID_IStimulusElement, (LPVOID*)&pSE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return;
	}
	Stimuluss stim;
	if( !stim.IsValid() ) return;
	// 27Sep07 - GMB: For SOME reason, initial calls to this dialog results
	// in the list not starting from the first record EVEN THOUGH the calls
	// are correct to ctree functions. This *HACK* (ALERT) solution solves
	// the problem by creating the object, calling reset (first record),
	// destroying the object, then recreating
	// NOTE: It is likely this is related to the life span of the ECL
	// object in LeftView

	// Create ExperimentCondition object
	IExperimentCondition* pEC = NULL;
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return;
	}
	// reset to first record (see above)
	pEC->ResetToStart();
	// destroy
	pEC->Release();
	// recreate
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return;
	}
	NSConditions cond;
	if( !cond.IsValid() )
	{
		pSE->Release();
		pEC->Release();
		return;
	}
	Experiments exp;
	if( !exp.IsValid() )
	{
		pSE->Release();
		pEC->Release();
		return;
	}

	// Find all element associations
	BSTR bstrStimID = NULL, bstrElemID = NULL;
	CString szStimID, szStim, szElemID;
	HTREEITEM hItemS = NULL;
	CStringList lstUsed;
	hr = pSE->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// Current record Element ID
		pSE->get_ElementID( &bstrElemID );
		szElemID = bstrElemID;

		if( szElemID == szID )
		{
			// Stimulus ID
			pSE->get_StimulusID( &bstrStimID );
			szStimID = bstrStimID;

			// Find stimulus
			hr = stim.Find( szStimID );
			if( SUCCEEDED( hr ) )
			{
				stim.GetDescriptionWithID( szItem );
				hItemS = m_tree.InsertItem( szItem, IMG_STIMULUS, IMG_STIMULUS, hItem );
				m_tree.SetItemData( hItemS, TI_STIMULUS );

				// Find all stimulus associations
				CString szStimW, szStimP, szStim, szCond, szCondID, szExpID, szTemp;
				HTREEITEM hItemC = NULL, hItemE = NULL;
				BSTR bstr = NULL;

				hr = cond.ResetToStart();
				while( SUCCEEDED( hr ) )
				{
					lstUsed.RemoveAll();

					cond.GetID( szCond );
					cond.GetStimulusWarning( szStimW );
					cond.GetStimulusPrecue( szStimP );
					cond.GetStimulus( szStim );

					if( ( szStimW == szStimID ) || ( szStimP == szStimID ) || ( szStim == szStimID ) )
					{
						// Add
						cond.GetDescriptionWithID( szItem );
						hItemC = m_tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, hItemS );
						m_tree.SetItemData( hItemC, TI_CONDITION );

						// Find all experiment associations
						hr = pEC->ResetToStart();
						while( SUCCEEDED( hr ) )
						{
							// Current condition ID
							pEC->get_ConditionID( &bstr );
							szCondID = bstr;

							// Do we want this one
							if( szCondID == szCond )
							{
								// Experiment ID
								pEC->get_ExperimentID( &bstr );
								szExpID = bstr;

								// add to list
								if( SUCCEEDED( exp.Find( szExpID ) ) && !lstUsed.Find( szExpID ) )
								{
									exp.GetDescriptionWithID( szTemp );
									HTREEITEM hItemE = m_tree.InsertItem( szTemp, IMG_EXPERIMENT, IMG_EXPERIMENT, hItemC );
									m_tree.SetItemData( hItemE, TI_EXPERIMENT );
									lstUsed.AddTail( szExpID );
								}
							}

							// Next record
							hr = pEC->GetNext();
						}
					}

					// Get next record
					hr = cond.GetNext();
				}
			}
		}

		// Get next record
		hr = pSE->GetNext();
	}

	// Expand tree
	m_tree.Expand( m_hElem, TVE_EXPAND );
	m_tree.Expand( hItem, TVE_EXPAND );
	hItem = m_tree.GetNextItem( hItem, TVGN_CHILD );
	HTREEITEM hItem2 = NULL;
	while( hItem )
	{
		m_tree.Expand( hItem, TVE_EXPAND );
		hItem2 = m_tree.GetNextItem( hItem, TVGN_CHILD );
		while( hItem2 )
		{
			m_tree.Expand( hItem2, TVE_EXPAND );
			hItem2 = m_tree.GetNextItem( hItem2, TVGN_NEXT );
		}
		hItem = m_tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// Cleanup
	pSE->Release();
	pEC->Release();
}

void RelationshipDlg::ShowCategory( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hCat = TVI_ROOT;
	else m_hCat = hRoot;

	// Get ID of selected element
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_CATEGORY, IMG_CATEGORY, m_hCat );
	m_tree.SetItemData( hItem, TI_CATEGORY );

	// Create objects
	Elements elem;
	if( !elem.IsValid() ) return;

	// Find all category associations
	CString szCatID;
	HTREEITEM hItemE = NULL;

	HRESULT hr = elem.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// Current category id
		elem.GetCatID( szCatID );

		if( szCatID == szID )
		{
			elem.GetDescriptionWithID( szItem );
			hItemE = m_tree.InsertItem( szItem, IMG_ELEMENT, IMG_ELEMENT, hItem );
			m_tree.SetItemData( hItemE, TI_ELEMENT );
		}

		// Get next record
		hr = elem.GetNext();
	}

	m_tree.Expand( m_hCat, TVE_EXPAND );
	m_tree.Expand( hItem, TVE_EXPAND );
}

void RelationshipDlg::ShowFeedback( CString szItem, HTREEITEM hRoot )
{
	// Root folder
	if( hRoot == NULL ) m_hFb = TVI_ROOT;
	else m_hFb = hRoot;

	// Get ID of selected feedback
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem = szItem.Find( szDelim );
	if( nItem == -1 ) return;
	CString szID = szItem.Mid( nItem + 2 );
	HTREEITEM hItem = m_tree.InsertItem( szItem, IMG_FEEDBACK, IMG_FEEDBACK, m_hFb );
	m_tree.SetItemData( hItem, TI_FEEDBACK );

	// Create objects
	NSConditions cond;
	if( !cond.IsValid() ) return;

	// Find all feedback associations
	CString szFb;
	HTREEITEM hItemC = NULL;

	HRESULT hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetFeedback( szFb );

		if( szFb == szID )
		{
			// Add
			cond.GetDescriptionWithID( szItem );
			hItemC = m_tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, hItem );
			m_tree.SetItemData( hItemC, TI_CONDITION );
		}

		// Get next record
		hr = cond.GetNext();
	}

	m_tree.Expand( m_hFb, TVE_EXPAND );
	m_tree.Expand( hItem, TVE_EXPAND );

}

void RelationshipDlg::SetCombos( CComboBox* pCB )
{
	if( pCB != &m_cboGroups ) m_cboGroups.SetCurSel( -1 );
	if( pCB != &m_cboSubjects ) m_cboSubjects.SetCurSel( -1 );
	if( pCB != &m_cboConditions ) m_cboConditions.SetCurSel( -1 );
	if( pCB != &m_cboStim ) m_cboStim.SetCurSel( -1 );
	if( pCB != &m_cboElem ) m_cboElem.SetCurSel( -1 );
	if( pCB != &m_cboCat ) m_cboCat.SetCurSel( -1 );
	if( pCB != &m_cboFeedback ) m_cboFeedback.SetCurSel( -1 );

	m_tooltip.UpdateTipText( _T("Select a group to view its relationships."), &m_cboGroups );
	m_tooltip.UpdateTipText( _T("Select a subject to view its relationships."), &m_cboSubjects );
	m_tooltip.UpdateTipText( _T("Select a condition to view its relationships."), &m_cboConditions );
	m_tooltip.UpdateTipText( _T("Select a stimulus to view its relationships."), &m_cboStim );
	m_tooltip.UpdateTipText( _T("Select a element to view its relationships."), &m_cboElem );
	m_tooltip.UpdateTipText( _T("Select a category to view its relationships."), &m_cboCat );
	m_tooltip.UpdateTipText( _T("Select a feedback to view its relationships."), &m_cboFeedback );
}

void RelationshipDlg::OnSelchangeCboSubjects() 
{
	ON_SEL_CHANGE( m_cboSubjects, ShowSubject )
	m_tooltip.UpdateTipText( szItem, &m_cboSubjects );
}

void RelationshipDlg::OnSelchangeCboGroups() 
{
	ON_SEL_CHANGE( m_cboGroups, ShowGroup )
	m_tooltip.UpdateTipText( szItem, &m_cboGroups );
}

void RelationshipDlg::OnSelchangeCboConditions() 
{
	ON_SEL_CHANGE( m_cboConditions, ShowCondition )
	m_tooltip.UpdateTipText( szItem, &m_cboConditions );
}

void RelationshipDlg::OnSelchangeCboStimuli() 
{
	ON_SEL_CHANGE( m_cboStim, ShowStimulus )
	m_tooltip.UpdateTipText( szItem, &m_cboStim );
}

void RelationshipDlg::OnSelchangeCboElements() 
{
	ON_SEL_CHANGE( m_cboElem, ShowElement )
	m_tooltip.UpdateTipText( szItem, &m_cboElem );
}

void RelationshipDlg::OnSelchangeCboCats() 
{
	ON_SEL_CHANGE( m_cboCat, ShowCategory )
	m_tooltip.UpdateTipText( szItem, &m_cboCat );
}

void RelationshipDlg::OnSelchangeCboFeedbacks() 
{
	ON_SEL_CHANGE( m_cboFeedback, ShowFeedback )
	m_tooltip.UpdateTipText( szItem, &m_cboFeedback );
}

BOOL RelationshipDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL RelationshipDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_CBO_SUBJECTS );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_REL_SELSUBJ );
	pWnd = GetDlgItem( IDC_CBO_GROUPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_REL_SELGRP );
	pWnd = GetDlgItem( IDC_CBO_CONDITIONS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a condition to view its relationships.") );
	pWnd = GetDlgItem( IDC_CBO_STIMULI );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a stimulus to view its relationships.") );
	pWnd = GetDlgItem( IDC_CBO_ELEMENTS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an element to view its relationships.") );
	pWnd = GetDlgItem( IDC_CBO_CATS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a category to view its relationships.") );
	pWnd = GetDlgItem( IDC_CBO_FEEDBACK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select a feedback to view its relationships.") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close this dialog.") );
	pWnd = GetDlgItem( IDC_TREE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the selected identity's relationships here.") );

	// Set up the image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_tree.SetImageList( &m_ImageList, TVSIL_NORMAL );

	// FILL COMBOS
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	int nItem;
	HRESULT hr;
	// Subjects
	{ DO_FILL_SUBJ( Subjects, m_cboSubjects ) }
	// Groups
	{ DO_FILL( Groups, m_cboGroups ) }
	// NSConditions
	{ DO_FILL( NSConditions, m_cboConditions ) }
	// Stimuluss
	{ DO_FILL( Stimuluss, m_cboStim ) }
	// Elements
	{ DO_FILL( Elements, m_cboElem ) }
	// Categories
	{ DO_FILL( Cats, m_cboCat ) }
	// Feedback
	{ DO_FILL( Feedbacks, m_cboFeedback ) }

	// What is enabled or not
	if( m_szItem != _T("") )
	{
		BOOL fDelim = TRUE;
		CString szDelim;
		::GetDelimiter( szDelim );
		int nItem = m_szItem.Find( szDelim );
		if( nItem == -1 ) fDelim = FALSE;

		if( m_nType == REL_GROUP )
		{
			CHECK_ID( Groups )
			ENABLE_START( m_cboGroups, ShowGroup )
		}
		else if( m_nType == REL_SUBJECT )
		{
			CHECK_ID_SUBJ( Subjects )
			ENABLE_START( m_cboSubjects, ShowSubject )
		}
		else if( m_nType == REL_CONDITION )
		{
			CHECK_ID( NSConditions )
			ENABLE_START( m_cboConditions, ShowCondition )
		}
		else if( m_nType == REL_STIMULUS )
		{
			CHECK_ID( Stimuluss )
			ENABLE_START( m_cboStim, ShowStimulus )
		}
		else if( m_nType == REL_ELEMENT )
		{
			CHECK_ID( Elements )
			ENABLE_START( m_cboElem, ShowElement )
		}
		else if( m_nType == REL_CAT )
		{
			CHECK_ID( Cats )
			ENABLE_START( m_cboCat, ShowCategory )
		}
		else if( m_nType == REL_FEEDBACK )
		{
			CHECK_ID( Feedbacks )
			ENABLE_START( m_cboFeedback, ShowFeedback )
		}

		return FALSE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL RelationshipDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("relationships.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void RelationshipDlg::OnNMDblclkTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// Selected item
	HTREEITEM hItem = m_tree.GetSelectedItem();
	if( !hItem ) return;

	// Check cursor location to ensure we are not double-clicking a +/-
	CPoint pt;
	::GetCursorPos( &pt );
	m_tree.ScreenToClient( &pt );
	UINT uFlags;
	HTREEITEM hItemCur = m_tree.HitTest( pt, &uFlags );
	if( hItemCur != hItem ) return;

	// Type of selected item
	DWORD_PTR dwType = m_tree.GetItemData( hItem );

	// Extract ID
	CString szData;
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = m_tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos != -1 ) szItem = szItem.Mid( nPos + 2 );

	switch( dwType )
	{
		case TI_EXPERIMENT:
		{
			szData.Format( IDS_AL_EXP, szItem );
			LogToFile( szData, LOG_UI );

			Experiments item;
			if( SUCCEEDED( item.Find( szItem ) ) ) item.DoEdit( szItem, this, 0 );
			break;
		}
		case TI_GROUP:
		{
			szData.Format( IDS_AL_GRP, szItem );
			LogToFile( szData, LOG_UI );

			Groups item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
		}
		case TI_SUBJECT:
		{
			szData.Format( IDS_AL_SUBJ, szItem );
			LogToFile( szData, LOG_UI );

			Subjects item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
		}
		case TI_CONDITION:
		{
			szData.Format( IDS_AL_COND, szItem );
			LogToFile( szData, LOG_UI );

			NSConditions item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
			break;
		}
		case TI_STIMULUS:
		{
			szData.Format( _T("DC: Stimulus %s."), szItem );
			LogToFile( szData, LOG_UI );

			Stimuluss item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
			break;
		}
		case TI_ELEMENT:
		{
			szData.Format( _T("DC: Element %s."), szItem );
			LogToFile( szData, LOG_UI );

			Elements item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
			break;
		}
		case TI_CATEGORY:
		{
			szData.Format( _T("DC: Category %s."), szItem );
			LogToFile( szData, LOG_UI );

			Cats item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
			break;
		}
		case TI_FEEDBACK:
		{
			szData.Format( _T("DC: Feedback %s."), szItem );
			LogToFile( szData, LOG_UI );

			Feedbacks item;
			if( SUCCEEDED( item.Find( szItem ) ) )
				item.DoEdit( szItem, this );
			break;
			break;
		}
	}

	*pResult = 0;
}

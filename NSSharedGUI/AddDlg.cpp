// AddDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "AddDlg.h"
#include "..\DataMod\DataMod.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddDlg dialog

BEGIN_MESSAGE_MAP(AddDlg, CBCGPDialog)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_REL, OnClickBnRel)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST, OnLvnKeydownList)
END_MESSAGE_MAP()

AddDlg::AddDlg(CStringList* pExisting, CWnd* pParent, BOOL fSingleSel )
	: CBCGPDialog(AddDlg::IDD, pParent), m_pExisting( pExisting ),
	  m_fChanged( FALSE ), m_fSingleSel( fSingleSel )
{
}

void AddDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_REL, m_bnRel);
}

/////////////////////////////////////////////////////////////////////////////
// AddDlg message handlers

BOOL AddDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	// button images
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnDel.SetImage( IDB_DEL, IDB_DEL, IDB_DEL_DIS );
	m_bnEdit.SetImage( IDB_EDIT );
	m_bnRel.SetImage( IDB_REL );
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetTitle( _T("Add Item") );
	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify the item's properties."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new item"), _T("Create New") );
	pWnd = GetDlgItem( IDC_BN_DEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Delete the selected item."), _T("Delete") );
	pWnd = GetDlgItem( IDC_BN_REL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View relationships with other items."), _T("Relationships") );
	pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The list of available items to add."), _T("Available Items") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add the selected items."), _T("Select Items") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSENOSEL, _T("Cancel") );

	// Image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );

	// list settings
	m_list.SendMessage( LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT );

	// Column Headers
	InsertColumns();

	// Fill List
	FillList();

	// single selection if applicable
	SetSingleSelect( m_fSingleSel );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AddDlg::SetSingleSelect( BOOL fVal )
{
	if( fVal )
		m_list.ModifyStyle( 0, LVS_SINGLESEL, 0 );
	else
		m_list.ModifyStyle( LVS_SINGLESEL, 0, 0 );
}

BOOL AddDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void AddDlg::OnClickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->EnableWindow( m_list.GetSelectedCount() > 0 );

	if( pResult ) *pResult = 0;
}

void AddDlg::OnDblclkList( NMHDR*, LRESULT* pResult ) 
{
	OnOK();

	if( pResult ) *pResult = 0;
}

void AddDlg::OnOK()
{
	int nCount = m_list.GetSelectedCount();
	if( nCount <= 0 )
	{
		BCGPMessageBox( IDS_ONESEL_ERR );
		return;
	}

	OK();

	CBCGPDialog::OnOK();
}

void AddDlg::OnClickBnAdd() 
{
	int nItem = Add();
	if( nItem == -1 ) return;

	// Highlite new item
	m_fChanged = TRUE;
	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED;
	lvi.stateMask = (UINT)-1;
	m_list.SetItemState( nItem, &lvi );
	m_list.EnsureVisible( nItem, FALSE );

	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) pWnd->EnableWindow( TRUE );
}

void AddDlg::OnClickBnEdit() 
{
	int nCount = m_list.GetSelectedCount();
	if( nCount == 0 ) return;
	if( nCount > 1 )
	{
		BCGPMessageBox( IDS_ONEMOD_ERR );
		return;
	}

	int nIdx = m_list.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	if( Edit( nIdx ) ) m_fChanged = TRUE;
}

void AddDlg::OnClickBnDel() 
{
	int nCount = m_list.GetSelectedCount();
	if( nCount == 0 ) return;
	if( nCount > 1 )
	{
		BCGPMessageBox( IDS_ONEDEL_ERR );
		return;
	}

	int nIdx = m_list.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	if( BCGPMessageBox( IDS_DEL_SURE, MB_YESNO ) == IDNO )
		return;

	if( Delete( nIdx ) )
	{
		m_list.DeleteItem( nIdx );
		m_fChanged = TRUE;
	}
}

void AddDlg::OnClickBnRel()
{
	int nCount = m_list.GetSelectedCount();
	if( nCount == 0 ) return;
	if( nCount > 1 )
	{
		BCGPMessageBox( _T("You can only view one item at a time.") );
		return;
	}

	int nIdx = m_list.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	Relationships( nIdx );
}

void AddDlg::OnLvnKeydownList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnClickList( pNMHDR, pResult );
}

// WizardImportFiles.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardImportRemoteSheet.h"
#include "WizardImportFiles.h"
#include "WizardImportLocation.h"
#include "Experiments.h"
#include "..\AdminMod\AdminMod.h"
#include "..\DataMod\DataMod.h"
#include <atlsafe.h>
#include <afxinet.h>


// WizardImportFiles dialog

IMPLEMENT_DYNAMIC(WizardImportFiles, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportFiles, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_NOTIFY(NM_CLICK, IDC_LIST, &WizardImportFiles::OnNMClickList)
END_MESSAGE_MAP()

WizardImportFiles::WizardImportFiles() : CBCGPPropertyPage( WizardImportFiles::IDD )
{
	m_fProcess = TRUE;
}

WizardImportFiles::~WizardImportFiles()
{
}

void WizardImportFiles::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Check(pDX, IDC_CHK_PROCESS, m_fProcess);
}

// WizardImportFiles message handlers

BOOL WizardImportFiles::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.SetTitle( _T("Remote Data Import Wizard") );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	m_tooltip.AddTool( pWnd, _T("Select the files that you would like to download for import."), _T("Select Files") );
	pWnd = GetDlgItem( IDC_CHK_PROCESS );
	m_tooltip.AddTool( pWnd, _T("Check to process the imported subjects."), _T("Process Subjects") );

	// image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );

	// Column Headers
	m_list.InsertColumn( 0, _T("File"), LVCFMT_LEFT, 300, 0 );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportFiles::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardImportFiles::OnWizardFinish()
{
	WizardImportRemoteSheet* pParent = (WizardImportRemoteSheet*)GetParent();
	if( !pParent ) return FALSE;
	WizardImportLocation* pLoc = (WizardImportLocation*)pParent->GetPage( 0 );
	if( !pLoc ) return FALSE;

	// verify that there are files selected
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if( !pos )
	{
		BCGPMessageBox( _T("No files have been selected.") );
		return FALSE;
	}

	// temp directory (for downloaded files)
	char pszTempPath[ 500 ];
	::GetTempPath( 500, pszTempPath );
	CString szTempPath = pszTempPath;

	// create admin elevate object (which handles ftp)
	IAdminElevator* pAE = NULL;
	HRESULT hr = ::CoCreateInstance( __uuidof( AdminElevator ), NULL, CLSCTX_ALL,
									 IID_IAdminElevator, (LPVOID*)&pAE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create AdminElevate object") );
		return FALSE;
	}

	// create list of files to be copied/uploaded
	CComSafeArray<BSTR> sa( (ULONG)( m_list.GetSelectedCount() ) );
	CStringList lstFiles;
	int nCount = 0, nItem = 0;
	CString szItem;
	while( pos )
	{
		nItem = m_list.GetNextSelectedItem( pos );
		szItem = m_list.GetItemText( nItem, 0 );
		sa[ nCount++ ] = szItem.AllocSysString();
		lstFiles.AddTail( szItem );
	}

	// close this instance's network connections
	pParent->CloseInet();

	// do download
	CWaitCursor crs;
 	hr = pAE->DoDownload( szTempPath.AllocSysString(), pLoc->m_szPath.AllocSysString(),
 						  pLoc->m_szRemote.AllocSysString(), pLoc->m_szLogin.AllocSysString(),
 						  pLoc->m_szPassword.AllocSysString(), sa.GetSafeArrayPtr() );
	pAE->Release();

	// if succeeded, import each file
	CStringList lstToProcess;
	BOOL fFailure = FALSE;
	if( SUCCEEDED( hr ) )
	{
		CString szFile, szExp;
		BOOL fTrials = FALSE;
		CStringList lstImported;
		pos = lstFiles.GetHeadPosition();
		while( pos )
		{
			// next file
			szItem = lstFiles.GetNext( pos );
			szFile = szTempPath;
			if( szFile.GetAt( szFile.GetLength() - 1 ) != '\\' ) szFile += _T("\\");
			szFile += szItem;
			// import this file
			if( Experiments::Import( szExp, fTrials, &lstImported, FALSE, szFile ) )
			{
				POSITION posS = lstImported.GetHeadPosition();
				while( posS )
				{
					szItem = lstImported.GetNext( posS );
					if( lstToProcess.Find( szItem ) == NULL )
						lstToProcess.AddTail( szItem );
				}
			}
			else fFailure = TRUE;
			// remove this file
			REMOVE_FILE( szFile );
		}
	}

	// process the imported subjects if specified
	if( m_fProcess )
	{
		// were there any subjects imported?
		if( lstToProcess.GetCount() > 0 )
		{
			IExperimentMember* pEM = NULL;
			HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
											 IID_IExperimentMember, (LPVOID*)&pEM );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( IDS_EM_ERR );
				return FALSE;
			}
			int nPos = 0;
			CString szExpID, szGrpID, szSubjID, szMsg;
			CStringList lstExp, lstGrp, lstSubj;
			CString szDelim = ::GetDelimiter();
			szDelim.Trim();

			// loop through the list of subjects
			POSITION pos = lstToProcess.GetHeadPosition();
			while( pos )
			{
				// each item is EXP::GRP::SUBJ
				// get next item
				szItem = lstToProcess.GetNext( pos );
				// extract exp ID
				nPos = szItem.Find( szDelim );
				if( nPos != -1 )
				{
					szExpID = szItem.Left( nPos );
					szItem = szItem.Mid( nPos + 1 );
				}
				else
				{
					pEM->Release();
					break;
				}
				// extract grp ID
				nPos = szItem.Find( szDelim );
				if( nPos != -1 )
				{
					szGrpID = szItem.Left( nPos );
					szItem = szItem.Mid( nPos + 1 );
					szItem.Trim();
				}
				else
				{
					pEM->Release();
					break;
				}
				// extract subj ID
				if( szItem != _T("") ) 
				{
					szSubjID = szItem;
				}
				else
				{
					pEM->Release();
					break;
				}

				// add to lists
				lstExp.AddTail( szExpID );
				lstGrp.AddTail( szGrpID );
				lstSubj.AddTail( szSubjID );
			}

			// loop through lists & reprocess each
			POSITION posExp = lstExp.GetHeadPosition();
			POSITION posGrp = lstGrp.GetHeadPosition();
			POSITION posSubj = lstSubj.GetHeadPosition();
			while( posExp && posGrp && posSubj )
			{
				// get exp, grp & subj IDs
				szExpID = lstExp.GetNext( posExp );
				szGrpID = lstGrp.GetNext( posGrp );
				szSubjID = lstSubj.GetNext( posSubj );

				// get subject sampling rate & device res
				double dDevRes = 0.;
				int nSampRate = 0;
				hr = pEM->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
								szSubjID.AllocSysString() );
				if( SUCCEEDED( hr ) )
				{
					pEM->get_DeviceRes( &dDevRes );
					short nTemp = 0;
					pEM->get_SamplingRate( &nTemp );
					nSampRate = nTemp;
				}
				// if they are 0, use experiments
				if( ( dDevRes == 0. ) || ( nSampRate == 0 ) )
				{
					IProcSetting* pPS = NULL;
					hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
											 IID_IProcSetting, (LPVOID*)&pPS );
					if( SUCCEEDED( hr ) )
					{
						pPS->Find( szExpID.AllocSysString() );
						pPS->get_DeviceResolution( &dDevRes );
						short nTemp = 0;
						pPS->get_SamplingRate( &nTemp );
						nSampRate = nTemp;
						pPS->Release();
					}
				}

				// reprocess
				Experiments::ReprocessSubject( szExpID, szSubjID, szGrpID,
											   dDevRes, nSampRate, NULL, NULL,
											   ::GetOutputWindow(), 1, 1, TRUE,
											   TRUE, FALSE, TRUE );
			}
		}
	}

	if( m_fProcess ) szItem = _T("Download, import and processing completed.");
	else szItem = _T("Download and import completed.");
	if( fFailure ) szItem += _T(" However, there were errors in the import process.");
	BCGPMessageBox( szItem );

	// close
	return CBCGPPropertyPage::OnWizardFinish();
}

BOOL WizardImportFiles::OnSetActive() 
{
	FillList();
	return CBCGPPropertyPage::OnSetActive();
}

void WizardImportFiles::FillList()
{
	WizardImportRemoteSheet* pParent = (WizardImportRemoteSheet*)GetParent();
	if( !pParent || !pParent->m_pInetSession || !pParent->m_pFtpConnection ) return;

	::ShowPopupWindow( this, _T("Please wait..."), 0, 0, TRUE );

	CWaitCursor crs;

	// issue command to do a directory listing
	CInternetFile* pFile = pParent->m_pFtpConnection->Command( _T("NLST"), CFtpConnection::CmdRespRead );
	if( !pFile )
	{
		BCGPMessageBox( _T("No files were found.") );
		::KillPopupWindow();
		return;
	}

	// get list of files in buffer
	CString szData;
	char readBuf[ 256 ];
	unsigned int rd = 0;
	ULONGLONG a = pFile->GetLength();
	do 
	{
		rd = pFile->Read( readBuf, 256 );
		if( rd > 0 ) szData += readBuf;
	}
	while( rd > 0 );

	// parse buffer
	CString szDelim = _T("\r\n"), szFile, szExt;
	int nPos = szData.Find( szDelim );
	while( nPos != -1 )
	{
		// get file
		szFile = szData.Left( nPos );
		szFile.Trim();
		if( szFile == _T("") ) continue;
		// if not a directory (ends in slash if directory)
		if( szFile.GetAt( szFile.GetLength() - 1 ) != '\\' )
		{
			// we're only adding to list if a ZIP, EXP or MEF extension
			if( szFile.GetLength() > 3 )
			{
				szExt = szFile.Right( 3 );
				szExt.MakeUpper();
				if( ( szExt == _T("ZIP") ) || ( szExt == _T("MEF") ) || ( szExt == _T("EXP") ) )
				{
					if( pParent->m_lstFiles.Find( szFile ) == NULL )
						pParent->m_lstFiles.AddTail( szFile );
				}
			}
		}
		// remove processed file from buffer
		szData = szData.Mid( nPos + szDelim.GetLength() );
		// get next position
		nPos = szData.Find( szDelim );
	}

	// add to list
	POSITION pos = pParent->m_lstFiles.GetHeadPosition();
	int nItem = 0, nImage = 18;
	while( pos )
	{
		szFile = pParent->m_lstFiles.GetNext( pos );
		szExt = szFile.Right( 3 );
		szExt.MakeUpper();
// 		if( szExt == _T("ZIP") ) nImage = 5;
// 		else if( szExt == _T("MEF") ) nImage = 12;
// 		else if( szExt == _T("EXP") ) nImage = 6;
		nItem = m_list.InsertItem( nItem, szFile, nImage );
	}

	// if no files, inform
	if( pParent->m_lstFiles.GetCount() == 0 )
		BCGPMessageBox( _T("No files were found.") );

	// Cleanup
	if( pFile )
	{
		pFile->Close();
		delete pFile;
	}

	::KillPopupWindow();
}

BOOL WizardImportFiles::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("remote_import_wizard.html"), this );
	return TRUE;
}

BOOL WizardImportFiles::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardImportFiles::OnNMClickList(NMHDR *pNMHDR, LRESULT *pResult)
{
	WizardImportRemoteSheet* pParent = (WizardImportRemoteSheet*)GetParent();
	POSITION pos = m_list.GetFirstSelectedItemPosition();
	if( pos ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );
	*pResult = 0;
}

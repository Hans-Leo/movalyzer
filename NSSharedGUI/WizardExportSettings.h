#pragma once

#include "NSTooltipCtrl.h"

// WizardExportSettings dialog

class AFX_EXT_CLASS WizardExportSettings : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardExportSettings)

public:
	WizardExportSettings();
	virtual ~WizardExportSettings();

// Dialog Data
	enum { IDD = IDD_WIZ_EXPORT_SETTINGS };
	NSToolTipCtrl		m_tooltip;
	CBCGPEdit			m_wndFolderEdit;
	CString				m_szFolder;
	static BOOL			m_fFiles;
	static BOOL			m_fTrials;
	static BOOL			m_fMembers;
	static BOOL			m_fShow;
	CString				m_szSummary;
	CString				m_szStatus;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();

	// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );

protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnClickTrials();
	afx_msg void OnClickMembers();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedBnDefault();
	DECLARE_MESSAGE_MAP()
};

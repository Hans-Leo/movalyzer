#ifndef _SUBJECTS_H_
#define	_SUBJECTS_H_

#include "Objects.h"
#include "..\CTreeLink\DBCommon.h"

#define	TAG_SUBJECT	_T("[SUBJECT]")

interface ISubject;

// This class is intended to encapsulate all view & functionality for a subject
class AFX_EXT_CLASS Subjects : public Objects
{
	PUT_BASE( Subject )

	// Attributes
public:
	CString	m_szExpID;
	CString	m_szGrpID;
	double	m_dDevRes;
	int		m_nSampRate;

	// Operations
public:
	virtual ULONGLONG GetNumRecords();
	// Interface methods
	PUT_GET( Subject, ID, CString, val.AllocSysString() );

	void PutNameLast( CString, BOOL fSkipEncode = FALSE );
	void GetNameLast( CString&, BOOL fPrv = TRUE, BOOL fSkipDecode = FALSE,
					  BOOL fIgnorePass = FALSE );
	static void GetNameLast( ISubject*, CString&, BOOL fPrv = TRUE,
							 BOOL fSkipDecode = FALSE, BOOL fIgnorePass = FALSE );

	void PutNameFirst( CString, BOOL fSkipEncode = FALSE );
	void GetNameFirst( CString&, BOOL fPrv = TRUE, BOOL fSkipDecode = FALSE,
					   BOOL fIgnorePass = FALSE );
	static void GetNameFirst( ISubject*, CString&, BOOL fPrv = TRUE,
							  BOOL fSkipDecode = FALSE, BOOL fIgnorePass = FALSE );

	void GetName( CString&, BOOL fPrv = TRUE, BOOL fSkipDecode = FALSE );
	static void GetName( ISubject*, CString&, BOOL fPrv = TRUE, BOOL fSkipDecode = FALSE );

	void GetNameWithID( CString&, BOOL fPrv = TRUE, BOOL fReverse = FALSE, BOOL fSkipDecode = FALSE );
	static void GetNameWithID( ISubject*, CString&, BOOL fPrv = TRUE, BOOL fReverse = FALSE, BOOL fSkipDecode = FALSE );
	void GetNameWithCodeID( CString&, BOOL fPrv = TRUE, BOOL fReverse = FALSE, BOOL fSkipDecode = FALSE );
	static void GetNameWithCodeID( ISubject*, CString&, BOOL fPrv = TRUE, BOOL fReverse = FALSE, BOOL fSkipDecode = FALSE );
	void GetDescriptionWithID( CString&, BOOL fPrv = TRUE, BOOL fReverse = FALSE, BOOL fSkipDecode = FALSE );
	static void GetDescriptionWithID( ISubject*, CString&, BOOL fPrv = TRUE, BOOL fReverse = FALSE, BOOL fSkipDecode = FALSE );

	PUT_GET( Subject, Notes, CString, val.AllocSysString() );

	void PutPrvNotes( CString, BOOL fSkipEncode = FALSE );
	void GetPrvNotes( CString&, BOOL fPrv = TRUE, BOOL fSkipDecode = FALSE,
					  BOOL fIgnorePass = FALSE );
	static void GetPrvNotes( ISubject*, CString&, BOOL fPrv = TRUE,
							 BOOL fSkipDecode = FALSE, BOOL fIgnorePass = FALSE );

	PUT_GET( Subject, DateAdded, DATE, val);
	PUT_GET( Subject, DateAdded, COleDateTime, val );

	PUT_GET( Subject, DefaultExperiment, CString, val.AllocSysString() );
	PUT_GET( Subject, Active, BOOL, val );
	PUT_GET( Subject, ExperimentCount, short, val );
	void IncrementExperimentCount( CString szID );

	PUT_GET( Subject, Encrypted, BOOL, val );
	PUT_GET( Subject, Code, CString, val.AllocSysString() );

	PUT_GET( Subject, EncryptionMethod, short, val );
	PUT_GET( Subject, SiteID, CString, val.AllocSysString() );
	PUT_GET( Subject, SiteDesc, CString, val.AllocSysString() );

	void PutSignature( CString, BOOL fSkipEncode = FALSE );
	void GetSignature( CString&, BOOL fSkipDecode = FALSE );
	static void GetSignature( ISubject*, CString&, BOOL fSkipDecode = FALSE );

	void PutPassword( CString, BOOL fSkipEncode = FALSE );
	void GetPassword( CString&, BOOL fSkipDecode = FALSE );
	static void GetPassword( ISubject*, CString&, BOOL fSkipDecode = FALSE );

	BOOL DoAdd( CString szCode, CWnd* pOwnder = NULL );
	static BOOL DoAdd( CString szCode, ISubject* pSubject, CWnd* pOwner = NULL );

	BOOL UpdateMissingPassword( CString szPass );
	static BOOL UpdateMissingPasswords( CString szRootPath, CString szPass );

	// Importing/Exporting
	BOOL Export( CMemFile* pFile, BOOL fFirst = FALSE );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, int nVer, BOOL fScan );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );

// Running/Processing/Etc-Experiment related
public:
	// get the last experiment that was run (NOT STATIC)
	CString& GetLastRunExperiment( CString szExpID, CString& szLastGroup );
	static CString& GetLastRunExperiment( CString szExpID, CString szSubjID, CString& szLastGroup );
	// clean raw & process files
	static void CleanProcessFiles( CString szExpID, CString szGrpID,
								   CString szSubjID, short nExpType, BOOL fHWR, BOOL fTF,
								   BOOL fSeg, BOOL fExt, BOOL fCon, BOOL fErr );
	// determining next group # for auto-group-generating
	static int GetNextGroupNum( CString szSubjID, CString szExpID, CString szStartLetter = _T("G") );
	// how many trials does a subject have?
	static int GetTrialCount( CString szExpID, CString szGrpID, CString szSubjID, short nExpType );
	// subject trial info (summary)
	static void GetTrialInfo( CString szExpID, CString szGrpID, CString szSubjID,
							  CStringList* pLstCond, short& nTrials, short& nGoodTrials,
							  short& nBadTrials, COleDateTime& dtExpStart, COleDateTime& dtExpEnd,
							  COleDateTime& dtProcessed, BOOL& fInSummarize, short& nExpectedTrials );

	static void Questionnaire( CString szExp, CString szGrp, CString szSubj );

	static BOOL GenerateTrialSequence( CString szExp, CString szGrp, CString szSubj, short nExpType );

// Password recovery
	BOOL RecoverPassword( CString szUserID, CString szUserSiteID, CString szUserSiteDesc,
						  CString szUserSig, CString szFilePath, CString& szFile, BOOL fUser = FALSE );
	BOOL RestorePassword( CString szUserID, CString szUserSiteID, CString szUserSiteDesc,
						  CString szUserSig, BOOL fUser = FALSE );
	BOOL ReadRecoverFile();

public:
	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _SUBJECTS_H_

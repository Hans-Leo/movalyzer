// AxisCust.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// AxisCust dialog

class AFX_EXT_API AxisCust : public CBCGPDialog
{
// Construction
public:
	AxisCust(CStringList* pD, CStringList* pU, int nSel, CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_AXIS };
	CBCGPListBox	m_lst;
	CString			m_szVal;
	CString			m_szDesc;
	CStringList*	m_pLstD;
	CStringList*	m_pLstU;
	CStringList		m_lstT;
	CStringList		m_lstDesc;
	int				m_nSel;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	afx_msg void OnBnDefault();
	afx_msg void OnSelchangeList();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnChange();
	DECLARE_MESSAGE_MAP()
};

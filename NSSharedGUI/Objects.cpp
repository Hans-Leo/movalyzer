// Objects.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\Common\Des\Des.h"
#include "Objects.h"
#include "..\DataMod\DataMod_i.c"

#ifdef	_CHILKAT_
	#include "..\chilkat\include\CkByteData.h"
	#include "..\chilkat\include\CkCrypt2.h"
	#include "..\chilkat\include\CkSettings.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// ENCRYPTION/DECRYPTION-RELATED
// DES key
const unsigned char _deskey[] = {0x45,0xcd,0x01,0xef,0x23,0x67,0x89,0xab};
// global var for holding enc/dec data
CString _szCRYPTData;


///////////////////////////////////////////////////////////////////////////////
// Construction/destruction

BOOL Objects::IsValid()
{
	return( SUCCEEDED( m_hrCreate ) );
}

///////////////////////////////////////////////////////////////////////////////
// Implementation

// static create
HRESULT Objects::CreateObject( Objects* pObj, LPVOID* pObject )
{
	if( !pObj ) return E_FAIL;

	::CoInitialize( NULL );
	HRESULT hr = CoCreateInstance( pObj->GetCLSID(),
								   NULL,
								   CLSCTX_ALL,
								   pObj->GetREFIID(),
								   (LPVOID*)pObject );

	if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to create object.") );

	return hr;
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::Encode( CString szSource )
{
	_szCRYPTData = _T("");

	if( ::GetEncryptionMethod() == ENCRYPTION_DES )
		_szCRYPTData = Objects::DESEncode( szSource );
	else if( ::GetEncryptionMethod() == ENCRYPTION_AES )
		_szCRYPTData = Objects::AESEncode( szSource );
	else if( ::GetEncryptionMethod() == ENCRYPTION_BLOWFISH )
		_szCRYPTData = Objects::BFEncode( szSource );
	else
		_szCRYPTData = szSource;

	return _szCRYPTData;
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::Decode( CString szSource, BOOL fEncrypted, short nMethod )
{
	_szCRYPTData = _T("");
	if( nMethod == -1 ) nMethod = ::GetEncryptionMethod();

	if( nMethod == ENCRYPTION_DES )
		_szCRYPTData = Objects::DESDecode( szSource, fEncrypted );
	else if( nMethod == ENCRYPTION_AES )
		_szCRYPTData = Objects::AESDecode( szSource );
	else if( nMethod == ENCRYPTION_BLOWFISH )
		_szCRYPTData = Objects::BFDecode( szSource );
	else
		_szCRYPTData = szSource;

	return _szCRYPTData;
}


///////////////////////////////////////////////////////////////////////////////

CString& Objects::DESEncode( CString szSource )
{
	_szCRYPTData = _T("");

	if( szSource.GetLength() <= 0 ) return _szCRYPTData;

	unsigned char cipher_data[ 200 ], plain_data[ 200 ];
	::ZeroMemory( cipher_data, 200 * sizeof( unsigned char ) );
	::ZeroMemory( plain_data, 200 * sizeof( unsigned char ) );
	_mbsncpy_s( plain_data, 200, (UCHAR*)szSource.GetBuffer(), _TRUNCATE );

	CDes des;
	des.encrypt( plain_data, _deskey, cipher_data );
	_szCRYPTData = cipher_data;

	return _szCRYPTData;
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::DESDecode( CString szSource, BOOL fEncrypted )
{
	_szCRYPTData = _T("");

	if( szSource.GetLength() <= 0 ) return _szCRYPTData;

	if( fEncrypted )
	{
		unsigned char cipher_data[ 200 ], plain_data[ 200 ];
		::ZeroMemory( cipher_data, 200 * sizeof( unsigned char ) );
		::ZeroMemory( plain_data, 200 * sizeof( unsigned char ) );
		_mbsncpy_s( cipher_data, 200, (UCHAR*)szSource.GetBuffer(), _TRUNCATE );

		CDes des;
		des.decrypt( cipher_data, _deskey, plain_data );
		_szCRYPTData = plain_data;
	}
	else _szCRYPTData = szSource;

	return _szCRYPTData;
}

///////////////////////////////////////////////////////////////////////////////

const int encval24 = 9237;
const char* encval14 = "Qdaw";
const char* encval34 = "qZON";
const int encval44 = 3066;

CString& Objects::EncDecrypt( CString szSource, CString szMode, BOOL fEncode )
{
	_szCRYPTData = _T("");

#ifdef	_CHILKAT_
	CkCrypt2 crypt;
	// unlock component
	crypt.UnlockComponent( CHILKAT_CRYPT_UNLOCK );
	// Use Blowfish encryption.
	crypt.put_CryptAlgorithm( szMode );
	// Use 128-bit Blowfish encryption.
	crypt.put_KeyLength( 128 );
	// Create a binary secret key from a password string.
	char buffer1[ 17 ] = {'\0'}, temp[ 10 ];
	strcpy_s( buffer1, 17, encval14 );
	_itoa_s( encval24, temp, 10 );
	strcat_s( buffer1, 17, temp );
	strcat_s( buffer1, 17, encval34 );
	_itoa_s( encval44, temp, 10 );
	strcat_s( buffer1, 17, temp );
	CkByteData secretKey;
	crypt.GenerateSecretKey( buffer1, secretKey );
	crypt.put_SecretKey( secretKey );
	// Tell the component what string-encoding to use for the encrypted output.
	// Encrypted data is binary, and will not be a printable string.  If you want
	// a printable string, it must be encoded using Base64, Hex, Quoted-Printable, etc.
	crypt.put_EncodingMode( "Base64" );
	// Encrypt
	// Note: Hex encoding increases the output length by
	// 2 times (2 characters per byte).
	CkString strEncrypted;
	if( fEncode ) crypt.EncryptStringENC( szSource, strEncrypted );
	else crypt.DecryptStringENC( szSource, strEncrypted );
	_szCRYPTData = strEncrypted.getString();
	// cleanup
	CkSettings::cleanupMemory();
#endif

	return _szCRYPTData;
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::AESEncode( CString szSource )
{
	return EncDecrypt( szSource, _T("rijndael"), TRUE );
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::AESDecode( CString szSource )
{
	return EncDecrypt( szSource, _T("rijndael"), FALSE );
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::BFEncode( CString szSource )
{
	return EncDecrypt( szSource, _T("blowfish"), TRUE );
}

///////////////////////////////////////////////////////////////////////////////

CString& Objects::BFDecode( CString szSource )
{
	return EncDecrypt( szSource, _T("blowfish"), FALSE );
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Objects::GetNumRecords()
{
	// TODO - WHEN ALL DATAMOD TABLES SUPPORT
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

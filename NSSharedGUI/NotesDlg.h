#if !defined(AFX_NOTESDLG_H__3B6BAA6B_CCC3_4BAF_8063_0D6154DD16C0__INCLUDED_)
#define AFX_NOTESDLG_H__3B6BAA6B_CCC3_4BAF_8063_0D6154DD16C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NotesDlg.h : header file
//

#define	N_EXPERIMENT		1
#define	N_GROUP				2
#define	N_SUBJECT			3
#define	N_CONDITION			4
#define	N_EXPMEMBER			5
#define	N_BACKUP			6
#define	N_INSTRUCTIONS		7
#define	N_TRIAL				8

/////////////////////////////////////////////////////////////////////////////
// NotesDlg dialog

class AFX_EXT_CLASS NotesDlg : public CBCGPDialog
{
// Construction
public:
	NotesDlg(UINT nType, CString szItem, CString szFile, CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_NOTES };
	CString			m_szNotes;
	UINT			m_nType;
	CString			m_szItem;
	CString			m_szFile;
	BOOL			m_fExists;
	CToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void OnOK();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_NOTESDLG_H__3B6BAA6B_CCC3_4BAF_8063_0D6154DD16C0__INCLUDED_)

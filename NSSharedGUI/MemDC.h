#pragma once

class AFX_EXT_CLASS CMemDC : public CDC
{
protected:
   CBitmap  m_bitmap;       // Offscreen bitmap
   CBitmap* m_oldBitmap;    // bitmap originally found in CMemDC
   CDC*     m_pDC;          // Saves CDC passed in constructor
   CRect    m_rect;         // Rectangle of drawing area.
   BOOL     m_bMemDC;       // TRUE if CDC really is a Memory DC.
    
   void Construct(CDC* pDC);

// TRK begin
public:
   CMemDC(CDC* pDC                  ) : CDC() { pDC->GetClipBox(&m_rect); Construct(pDC); }
   CMemDC(CDC* pDC, const RECT& rect) : CDC() { m_rect = rect           ; Construct(pDC); }
// TRK end
    
   virtual ~CMemDC();
    
    // Allow usage as a pointer    
    CMemDC* operator->() 
    {
        return this;
    }    

    // Allow usage as a pointer    
    operator CMemDC*() 
    {
        return this;
    }
};

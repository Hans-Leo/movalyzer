#pragma once

// ConditionPageStimuli.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ConditionPageStimuli dialog

interface INSCondition;

class AFX_EXT_CLASS ConditionPageStimuli : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ConditionPageStimuli)

// Construction
public:
	ConditionPageStimuli( INSCondition* = NULL, BOOL fNew = TRUE );
	~ConditionPageStimuli();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_STIMULI };
	CComboBox		m_cboStimulus;
	CComboBox		m_cboStimulusW;
	CComboBox		m_cboStimulusP;
	double			m_dPDuration;
	double			m_dPLatency;
	BOOL			m_fOnPenDown;
	BOOL			m_fStopWrongTarget;
	BOOL			m_fStartFirstTarget;
	BOOL			m_fStopLastTarget;
	double			m_dWDuration;
	double			m_dWLatency;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fStimulus;
	CString			m_szStimulus;
	CString			m_szStimulusW;
	CString			m_szStimulusP;
	CBCGPButton		m_bnAdd;
	CBCGPButton		m_bnAddW;
	CBCGPButton		m_bnAddP;
	CBCGPButton		m_bnEdit;
	CBCGPButton		m_bnEditW;
	CBCGPButton		m_bnEditP;
	CBCGPButton		m_bnRel;
	CBCGPButton		m_bnRelW;
	CBCGPButton		m_bnRelP;
	CBCGPButton		m_bnChart;
	CBCGPButton		m_bnChartW;
	CBCGPButton		m_bnChartP;
	CBCGPButton		m_bnSE;
	CBCGPButton		m_bnSEW;
	CBCGPButton		m_bnSEP;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void FillStimuli( int nWhich = -1, int nSel = 0 );
	void DoAdd( int nWhich );
	void DoEdit( int nWhich );
	void DoRel( int nWhich );
	void DoChart( int nWhich );
	void DoSE( int nWhich );

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* pHelpInfo );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnSelchangeCboPrecue();
	afx_msg void OnSelchangeCboStimulus();
	afx_msg void OnSelchangeCboWarning();
	afx_msg void OnClickBnAdd();
	afx_msg void OnClickBnEdit();
	afx_msg void OnClickBnRel();
	afx_msg void OnClickBnChart();
	afx_msg void OnClickBnSE();
	afx_msg void OnClickBnAddW();
	afx_msg void OnClickBnEditW();
	afx_msg void OnClickBnRelW();
	afx_msg void OnClickBnChartW();
	afx_msg void OnClickBnSEW();
	afx_msg void OnClickBnAddP();
	afx_msg void OnClickBnEditP();
	afx_msg void OnClickBnRelP();
	afx_msg void OnClickBnChartP();
	afx_msg void OnClickBnSEP();
	afx_msg void OnBnClickedChkFirsttarget();
	afx_msg void OnBnClickedChkOnpendown();
	DECLARE_MESSAGE_MAP()
};

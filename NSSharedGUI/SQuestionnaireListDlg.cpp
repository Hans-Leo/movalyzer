// SQuestListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "SQuestionnaireListDlg.h"
#include "..\Common\GUI\Clipboard.h"
#include <math.h>

// SQuestListDlg dialog
IMPLEMENT_DYNAMIC(SQuestListDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(SQuestListDlg, CBCGPDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_PASTE, OnClickPaste)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

BOOL SQuestListDlg::m_fTab = FALSE;

SQuestListDlg::SQuestListDlg( CStringList* pListH, CStringList* pListQ,
							  CStringList* pListA, CStringList* pListN,
							  CStringList* pListID, CWnd* pParent )
	: CBCGPDialog( SQuestListDlg::IDD, pParent ), m_pListH( pListH ), m_pListN( pListN ),
	  m_pListQ( pListQ ), m_pListA( pListA ), m_pListID( pListID )
{
	m_fInit = FALSE;
	m_nRows = 0;
}

SQuestListDlg::~SQuestListDlg()
{
}

void SQuestListDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control( pDX, IDC_GRID_RECT, m_wndGridLocation );
	DDX_Check(pDX, IDC_CHK_TAB, SQuestListDlg::m_fTab);
}

// SQuestListDlg message handlers

BOOL SQuestListDlg::PreTranslateMessage( MSG* pMsg )
{
//	m_tooltip.RelayEvent(pMsg);
	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL SQuestListDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// create grid control
	CRect rectGrid;
	m_wndGridLocation.GetClientRect( &rectGrid );
	m_wndGridLocation.MapWindowPoints( this, &rectGrid );
	m_wndGrid.Create( WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER, rectGrid, this, (UINT)999 );
	m_wndGrid.EnableHeader( TRUE );
	m_wndGrid.EnableDragHeaderItems( FALSE );
	m_wndGrid.EnableMultipleSort( FALSE );
	m_wndGrid.SetSingleSel( TRUE );
	m_wndGrid.EnableGroupByBox( FALSE );
	m_wndGrid.EnableMarkSortedColumn( FALSE );

	// Determine size, location differences for later use in resizing
	// ...the constant numbers below represent the pixel distance of
	// ...the edges of the graph window from the edge of the dialog window
	RECT rect, rect2;
	CWnd* pList = GetDlgItem( IDC_GRID_RECT );
	pList->GetWindowRect( &rect );
	GetWindowRect( &rect2 );
	int nToolbar = rect.top - rect2.top;
	m_nRightBuffer = ( ( rect2.right - rect2.left ) - ( rect.right - rect.left ) ) - 4;
	m_nBottomBuffer = ( ( rect2.bottom - rect2.top ) - ( rect.bottom - rect.top ) ) - 4;
	ScreenToClient( &rect );
	m_nTop = rect.top;
	m_nLeft = rect.left;
	nToolbar -= m_nTop;
	m_nBottomBuffer -= nToolbar;

	// List size (width)
	m_wndGrid.GetClientRect( &rect );
	int nWidth = rect.right - rect.left;

	// columns
	m_wndGrid.InsertColumn( 0, _T("Question"), (int)( nWidth / 2 ) - 25 );
	m_wndGrid.InsertColumn( 1, _T("#?"), 35 );
	m_wndGrid.SetColumnAlign( 1, HDF_CENTER );
	m_wndGrid.InsertColumn( 2, _T("Answer"), (int)( nWidth / 2 ) - 30 );

	// fill list
	FillList();

	// Select the 1st question's answer and ready for edit if exists
	BOOL fRet = TRUE;
	if( m_nRows > 0 )
	{
		m_wndGrid.SetFocus();
		SQuestGridRow* pRow = (SQuestGridRow*)m_wndGrid.GetRow( 0 );
		SQuestGridItem* pItem = pRow ? (SQuestGridItem*)pRow->GetItem( 2 ) : NULL;
		if( pItem )
		{
			// set the current selection (row)
			m_wndGrid.SetCurSel( pRow, SM_FIRST_CLICK | SM_SINGE_SEL_GROUP );
			// set the new answer item of next row to edit mode
			// so we can obtain the edit box to get the window coordinates
			// then convert them to client coordinates and call the default
			// grid ctrl on left button down handler
			pItem->OnEdit( NULL );
			CEdit* pEdit = (CEdit*)pItem->GetInPlaceWnd();
			if( !pEdit ) return TRUE;
			CRect rect;
			pEdit->GetWindowRect( &rect );
			pItem->OnEndEdit();
			m_wndGrid.ScreenToClient( &rect );
			CPoint pt( rect.left + 5, rect.top + 5 );
			m_wndGrid.OnLButtonDown( 1, pt );
			// redraw the row
			pRow->Redraw();
			// end the editing
			pItem->OnEndEdit();
			// start the editing again
			pItem->OnEdit( &pt );
			// get the edit object of the item
			pEdit = (CEdit*)pItem->GetInPlaceWnd();
			if( pEdit )
			{
				// set focus to the edit control
				pEdit->SetFocus();
				// select the text, if any, in the edit control
				pEdit->SetSel( 0, -1 );
			}
			fRet = FALSE;
		}
	}

	m_fInit = TRUE;

	return fRet;
}

void SQuestListDlg::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDialog::OnSize(nType, cx, cy);

	if( m_fInit )
	{
		m_wndGrid.MoveWindow( m_nLeft, m_nTop, cx - m_nRightBuffer,
							  cy - m_nBottomBuffer, FALSE );
		CRect rect;
		m_wndGrid.GetClientRect( &rect );
		int nWidth = rect.right - rect.left;
		m_wndGrid.SetColumnWidth( 0, (int)( nWidth / 2 ) - 25 );
		m_wndGrid.SetColumnWidth( 1, 35 );
		m_wndGrid.SetColumnWidth( 2, (int)( nWidth / 2 ) - 30 );
		m_wndGrid.AdjustLayout();
		RedrawWindow();
	}
}

BOOL VerifyNumber( CString szVal, BOOL fDouble = FALSE )
{
	// length of string
	int nLength = szVal.GetLength(), i = 0;
	if( nLength <= 0 ) return TRUE;

	// there can be ONE negative symbol at the beginning
	if( szVal[ 0 ] == '-' )
	{
		if( !fDouble )
		{
			int nVal = abs( atoi( szVal ) );
			szVal.Format( _T("%d"), nVal );
		}
		else
		{
			double dVal = fabs( atof( szVal ) );
			szVal.Format( _T("%g"), dVal );
		}
		nLength = szVal.GetLength();
	}

	// integer
	if( !fDouble )
	{
		// loop through each char...if any are a non-digit, return false
		for( i = 0; i < nLength; i++ )
		{
			if( !isdigit( szVal[ i ] ) ) return FALSE;
		}
	}
	// non-integer, double-precision floating point
	else
	{
		BOOL fDecimal = FALSE;
		// loop through each char...if any are a non-digit, return false
		// ...unless it is a decimal separator (which should only occur once)
		for( i = 0; i < nLength; i++ )
		{
			char c = szVal[ i ];
			if( ( c == '.' ) || ( c == ',' ) )
			{
				if( fDecimal ) return FALSE;
				fDecimal = TRUE;
				continue;
			}
			if( !isdigit( c ) ) return FALSE;
		}
	}

	return TRUE;
}

void SQuestListDlg::OnOK()
{
	POSITION posH = m_pListH->GetHeadPosition();
	POSITION posA = m_pListA->GetHeadPosition();
	POSITION posALast = NULL;
	CString szH, szA, szVal, szValLbl;
	int nCol = -1;

	// First, verify numeric values
	while( posA )
	{
		posALast = posA;
		szH = m_pListH->GetNext( posH );
		szA = m_pListA->GetNext( posA );
		if( szH == _T("") )
		{
			nCol++;
			SQuestGridRow* pRow = (SQuestGridRow*)m_wndGrid.GetRow( nCol );
			if( pRow )
			{
				BOOL fNumeric = pRow->GetData();
				if( fNumeric )
				{
					SQuestGridItem* pItem = (SQuestGridItem*)pRow->GetItem( 2 );
					SQuestGridItem* pItemLbl = (SQuestGridItem*)pRow->GetItem( 0 );
					if( pItem && pItemLbl )
					{
						szVal = pItem->GetValue();
						szValLbl = pItemLbl->GetValue();

						// if no data, do nothing
						if( szVal == _T("") ) continue;
						// if numeric, verify that it is completely numeric
						double dVal = 0.;
						int nVal = 0;
						// since the conversion functions return 0 or 0.0 when they fail,
						// ...if the answer has these values, it's ok
						if( ( szVal == _T("0") ) || ( szVal == _T("0.") ) || ( szVal == _T("0.0") ) )
							continue;
						// if there's a decimal in the string, we will do double conversion, otherwise int
						if( szVal.Find( _T(".") ) != -1 )
						{
							dVal = _tcstod( szVal, NULL );
							if( ( dVal == 0. ) || !VerifyNumber( szVal, TRUE ) )
							{
								Beep( 800, 200 );
								CString szMsg;
								szMsg.Format( _T("The answer to question \"%s\" must be numeric"), szValLbl );
								BCGPMessageBox( szMsg );
								m_wndGrid.SetCurSel( pRow );
								return;
							}
						}
						else
						{
							nVal = atoi( szVal );
							if( ( nVal == 0 ) || !VerifyNumber( szVal ) )
							{
								Beep( 800, 200 );
								CString szMsg;
								szMsg.Format( _T("The answer to question \"%s\" must be numeric"), szValLbl );
								BCGPMessageBox( szMsg );
								m_wndGrid.SetCurSel( pRow );
								return;
							}
						}
					}
				}
			}
		}

	}

	// update the data
	posH = m_pListH->GetHeadPosition();
	posA = m_pListA->GetHeadPosition();
	nCol = -1;
	while( posA )
	{
		posALast = posA;
		szH = m_pListH->GetNext( posH );
		szA = m_pListA->GetNext( posA );
		if( ( szH == _T("") ) && posALast )
		{
			nCol++;
			SQuestGridRow* pRow = (SQuestGridRow*)m_wndGrid.GetRow( nCol );
			if( pRow )
			{
				SQuestGridItem* pItem = (SQuestGridItem*)pRow->GetItem( 2 );
				if( pItem )
				{
					szVal = pItem->GetValue();
					m_pListA->SetAt( posALast, szVal );
				}
			}
		}
	}

	CDialog::OnOK();
}

void SQuestListDlg::OnCancel()
{
	if( BCGPMessageBox( _T("Cancel and lose all changes?"), MB_YESNO ) == IDYES )
		CBCGPDialog::OnCancel();
}

BOOL SQuestListDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("questionnaire.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void SQuestListDlg::FillList()
{
	// reset grid
	m_wndGrid.RemoveAll();

	// fill list
	POSITION posQ = m_pListQ->GetHeadPosition();
	POSITION posA = m_pListA->GetHeadPosition();
	POSITION posH = m_pListH->GetHeadPosition();
	POSITION posN = m_pListN->GetHeadPosition();
	CString szQ, szA, szH, szN;
	int nColCount = m_wndGrid.GetColumnCount();
	m_nRows = 0;

	while( posH )
	{
		szQ = m_pListQ->GetNext( posQ );
		szA = m_pListA->GetNext( posA );
		szH = m_pListH->GetNext( posH );
		szN = m_pListN->GetNext( posN );

		if( szH == _T("") )
		{
			CBCGPGridRow* pRow = m_wndGrid.CreateRow( nColCount );
			pRow->GetItem( 0 )->SetValue( szQ.GetBuffer() );
			pRow->GetItem( 0 )->AllowEdit( FALSE );
			pRow->GetItem( 1 )->SetValue( ( szN == _T("") ) ? _T("N") : _T("Y") );
			pRow->GetItem( 1 )->AllowEdit( FALSE );
			pRow->GetItem( 2 )->SetValue( szA.GetBuffer() );
			pRow->SetData( ( szN == _T("") ) ? FALSE : TRUE );
			m_wndGrid.AddRow( pRow, FALSE );

			m_nRows++;
		}
	}


	// adjust layout
	m_wndGrid.AdjustLayout();
}

void SQuestListDlg::OnClickPaste()
{
	UpdateData( TRUE );

	// First we must exit edit mode if in it
	m_wndGrid.EndEditItem();

	// Is there any textual data on the clipboard?
	int nLength = CClipboard::GetTextLength();
	if( nLength == 0 )
	{
		BCGPMessageBox( _T("No textual data on the clipboard.") );
		return;
	}

	// get data
	char* pszData = new char[ nLength + 1 ];
	CClipboard::GetText( pszData, nLength + 1 );

	// loop through text (one line = one cell OR delim by tab) and add to grid
	CString szDelim = SQuestListDlg::m_fTab ? _T("\t") : _T("\r\n");
	CBCGPGridRow* pRow = NULL;
	CString szData = pszData, szItem;
	delete [] pszData;
	int nPos = szData.Find( szDelim ), nRow = 0;
	if( nPos != -1 )
	{
		while( nPos != -1 )
		{
			// current line item
			szItem = szData.Left( nPos );

			// paste contents
			CBCGPGridRow* pRow = m_wndGrid.GetRow( nRow );
			if( !pRow )
			{
				BCGPMessageBox( _T("WARNING: Number of pasted items exceeds the number of questions.") );
				goto EndOfPaste;
			}
			pRow->GetItem( 2 )->SetValue( szItem.GetBuffer() );
			nRow++;

			// remainder of line
			szData = szData.Mid( nPos + ( SQuestListDlg::m_fTab ? 1 : 2 ) );
			// search again
			nPos = szData.Find( szDelim );
		}
		// just in case last item does not have a CR/LF
		if( szData != _T("") )
		{
			pRow = m_wndGrid.GetRow( nRow );
			if( !pRow )
			{
				BCGPMessageBox( _T("WARNING: Number of pasted items exceeds the number of questions.") );
				goto EndOfPaste;
			}
			// remove trailing CRLF
			nPos = szData.Find( _T("\r\n") );
			if( nPos != -1 ) szData = szData.Left( nPos );
			pRow->GetItem( 2 )->SetValue( szData.GetBuffer() );
			nRow++;
		}
	}
	else
	{
		pRow = m_wndGrid.GetRow( nRow );
		if( !pRow ) return;
		// remove trailing CRLF
		nPos = szData.Find( _T("\r\n") );
		if( nPos != -1 ) szData = szData.Left( nPos );
		pRow->GetItem( 2 )->SetValue( szData.GetBuffer() );
		nRow++;
	}

	// select last row of paste
EndOfPaste:
	nRow--;
	if( nRow == -1 ) return;
	if( nRow >= ( m_wndGrid.GetRowCount() ) ) nRow = m_wndGrid.GetRowCount() - 1;
	pRow = m_wndGrid.GetRow( nRow );
	if( pRow ) 
	{
		m_wndGrid.SetCurSel( pRow );
		m_wndGrid.EnsureVisible( pRow );
		SQuestGridItem* pItem = pRow ? (SQuestGridItem*)pRow->GetItem( 2 ) : NULL;
		if( !pItem ) return;
		pItem->OnEdit( NULL );
		CEdit* pEdit = (CEdit*)pItem->GetInPlaceWnd();
		if( !pEdit ) return;
		CRect rect;
		pEdit->GetWindowRect( &rect );
		pItem->OnEndEdit();
		ScreenToClient( &rect );
		CPoint pt( rect.left, rect.top );
		OnLButtonDown( 1, pt );
		// redraw the row
		pRow->Redraw();
		// end the editing
		pItem->OnEndEdit();
	}
}

IMPLEMENT_DYNAMIC( SQuestGridCtrl, CBCGPGridCtrl )
IMPLEMENT_DYNAMIC( SQuestGridItem, CBCGPGridItem )
IMPLEMENT_DYNAMIC( SQuestGridRow, CBCGPGridRow )

BEGIN_MESSAGE_MAP( SQuestGridCtrl, CBCGPGridCtrl )
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

void SQuestGridCtrl::OnLButtonDown( UINT nFlags, CPoint point )
{
	CBCGPGridCtrl::OnLButtonDown( nFlags, point );
}

BOOL SQuestGridCtrl::ValidateItemData( CBCGPGridRow* pRow )
{
	if( !pRow ) return FALSE;
	// which item
	CBCGPGridItem* pItem = GetCurSelItem();
	if( !pItem ) return FALSE;
	// in-place editing window
	CEdit* pWnd = (CEdit*)pItem->GetInPlaceWnd();
	if( !pWnd ) return FALSE;
	// is numeric?
	BOOL fNumeric = pRow->GetData();
	// if so, then verify answer is numeric
	if( fNumeric )
	{
		// item value
		CString szVal;
		pWnd->GetWindowText( szVal );
		// if no data, do nothing
		if( szVal == _T("") ) return TRUE;
		// if numeric, verify that it is completely numeric
		double dVal = 0.;
		int nVal = 0;
		// since the conversion functions return 0 or 0.0 when they fail,
		// ...if the answer has these values, it's ok
		if( ( szVal == _T("0") ) || ( szVal == _T("0.") ) || ( szVal == _T("0.0") ) )
			return TRUE;
		// if there's a decimal in the string, we will do double conversion, otherwise int
		if( szVal.Find( _T(".") ) != -1 )
		{
			dVal = _tcstod( szVal, NULL );
			if( ( dVal == 0. ) || !VerifyNumber( szVal, TRUE ) )
			{
				if( fNotified ) fNotified = FALSE;
				else
				{
					fNotified = TRUE;
					Beep( 800, 200 );
				}
				pWnd->SetSel( 0, -1 );
				return FALSE;
			}
		}
		else
		{
			nVal = atoi( szVal );
			if( ( nVal == 0 ) || !VerifyNumber( szVal ) )
			{
				if( fNotified ) fNotified = FALSE;
				else
				{
					fNotified = TRUE;
					Beep( 800, 200 );
				}
				pWnd->SetSel( 0, -1 );
				return FALSE;
			}
		}
	}
	// otherwise (not numeric) we do not care
	return TRUE;
}

BOOL SQuestGridCtrl::PreTranslateMessage( MSG* pMsg ) 
{
	if( ( m_nDraggedColumn >= 0 ) || m_bTracking || m_bSelecting )
		return CWnd::PreTranslateMessage(pMsg);

	switch( pMsg->message )
	{
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:
		case WM_NCLBUTTONDOWN:
		case WM_NCRBUTTONDOWN:
		case WM_NCMBUTTONDOWN:
		case WM_NCLBUTTONUP:
		case WM_NCRBUTTONUP:
		case WM_NCMBUTTONUP:
			if( m_ToolTip.GetSafeHwnd() != NULL &&
				m_ToolTip.IsWindowVisible() )
			{
				m_ToolTip.Hide();
				if( ::GetCapture () == GetSafeHwnd () ) ReleaseCapture ();
				return CWnd::PreTranslateMessage( pMsg );
			}
			break;
		case WM_MOUSEMOVE:
			if( pMsg->wParam == 0 )	// No buttons pressed
			{
				CPoint ptCursor;
				::GetCursorPos( &ptCursor );
				ScreenToClient( &ptCursor );
				TrackToolTip( ptCursor );
			}
			break;
	}

	//////////***CUSTOM***Custom class***///////////
	SQuestGridRow* pSel = (SQuestGridRow*)GetCurSel();
	//////////***CUSTOM***Custom class***///////////

	if( pMsg->message == WM_SYSKEYDOWN && 
		( pMsg->wParam == VK_DOWN || pMsg->wParam == VK_RIGHT ) && 
		pSel != NULL && pSel->IsEnabled() )
	{
		//////////***CUSTOM***Custom class***///////////
		SQuestGridItem* pItem = (SQuestGridItem*)GetCurSelItem( pSel );
		//////////***CUSTOM***Custom class***///////////

		if( pItem != NULL && 
			( ( pItem->m_dwFlags ) & BCGP_GRID_ITEM_HAS_LIST ) &&
			EditItem( pSel ) )
		{
			pItem->DoClickButton( CPoint( -1, -1 ) );
		}

		return TRUE;
	}

	if (pSel != NULL && pSel->m_bInPlaceEdit && pSel->m_bEnabled)
	{
		ASSERT_VALID (pSel);

		if (pMsg->message == WM_KEYDOWN)
		{
			switch (pMsg->wParam)
			{
			///////////////////////////////////////////////////////////
			///////////////////////***CUSTOM***////////////////////////
			///////////////////////////////////////////////////////////
			case VK_RETURN:
			case VK_TAB:
			case VK_UP:
			case VK_DOWN:
				{
					BOOL fShiftKey = FALSE;
					if( ( ( pMsg->wParam == VK_TAB ) || (pMsg->wParam == VK_RETURN ) ) &&
						( ( GetKeyState( VK_SHIFT ) & ~1 ) ) != 0 )
						fShiftKey = TRUE;
					if( pMsg->wParam == VK_UP ) fShiftKey = TRUE;
					///////////////////////////////////////////////////
					////////////////// DEFAULT ////////////////////////
					CComboBox* pWndCombo = pSel->GetComboWnd();
					if( pWndCombo != NULL && pWndCombo->GetDroppedState() )
					{
						pSel->OnSelectCombo();
						CWnd* pWndInPlace = pSel->GetInPlaceWnd();
						ASSERT_VALID( pWndInPlace );
						pWndInPlace->SetFocus();
						return TRUE;
					}
					////////////////// DEFAULT ////////////////////////
					///////////////////////////////////////////////////

					// in case we're editing, end it...if failed, beep and exit
					if( !EndEditItem() )
					{
						SetFocus();
						MessageBeep( (UINT)-1 );
						break;
					}

					// current row
					int nCurRow = pSel->GetRowId();
					// next row
					int nNextRow = nCurRow + ( 1 * ( fShiftKey ? -1 : 1 ) );
					// if the next row is beyond the limit, we assume we're done and exit
					if( nNextRow >= GetRowCount() )
					{
						if( ( pMsg->wParam != VK_UP ) && (pMsg->wParam != VK_DOWN ) )
						{
							SQuestListDlg* pParent = (SQuestListDlg*)GetParent();
							if( pParent )
							{
								Beep( 300, 200 );
								if( pMsg->wParam == VK_TAB )
								{
									CWnd* pOK = pParent->GetDlgItem( IDOK );
									if( pOK ) pOK->SetFocus();
								}
								else pParent->OK();
							}
						}
						break;
					}

					// get next row
					SQuestGridRow* pRow = (SQuestGridRow*)GetRow( nNextRow );
					// get answer column of next row
					SQuestGridItem* pItem = pRow ? (SQuestGridItem*)pRow->GetItem( 2 ) : NULL;
					if( pItem )
					{
						// set the current selection (row)
						SetCurSel( pRow );
						// and ensure it's visible
						EnsureVisible( pRow );

						// set the new answer item of next row to edit mode
						// so we can obtain the edit box to get the window coordinates
						// then convert them to client coordinates and call the default
						// grid ctrl on left button down handler
						pItem->OnEdit( NULL );
						CEdit* pEdit = (CEdit*)pItem->GetInPlaceWnd();
						if( !pEdit ) break;
						CRect rect;
						pEdit->GetWindowRect( &rect );
						pItem->OnEndEdit();
						ScreenToClient( &rect );
						CPoint pt( rect.left + 5, rect.top + ( fShiftKey ? -1 : 5 ) );
						OnLButtonDown( 1, pt );
						// redraw the row
						pRow->Redraw();
						// end the editing
						pItem->OnEndEdit();
						// start the editing again
						pItem->OnEdit( &pt );
						// get the edit object of the item
						pEdit = (CEdit*)pItem->GetInPlaceWnd();
						if( pEdit )
						{
							// set focus to the edit control
							pEdit->SetFocus();
							// select the text, if any, in the edit control
							pEdit->SetSel( 0, -1 );
						}
					}

					break;
				}
				///////////////////////////////////////////////////////////
				///////////////////////***CUSTOM***////////////////////////
				///////////////////////////////////////////////////////////

			case VK_ESCAPE:
				EndEditItem( FALSE );
				SetFocus();
				return TRUE;

			default:
				if (!pSel->m_bAllowEdit)
				{
					pSel->PushChar ((UINT) pMsg->wParam);
					return TRUE;
				}

				if (ProcessClipboardAccelerators ((UINT) pMsg->wParam))
				{
					return TRUE;
				}

				return FALSE;
			}

			return TRUE;
		}
		else if (pMsg->message >= WM_MOUSEFIRST &&
			pMsg->message <= WM_MOUSELAST)
		{
			CPoint ptCursor;
			::GetCursorPos (&ptCursor);
			ScreenToClient (&ptCursor);

			CSpinButtonCtrl* pWndSpin = pSel->GetSpinWnd ();
			if (pWndSpin != NULL)
			{
				ASSERT_VALID (pWndSpin);
				ASSERT (pWndSpin->GetSafeHwnd () != NULL);

				CRect rectSpin;
				pWndSpin->GetClientRect (rectSpin);
				pWndSpin->MapWindowPoints (this, rectSpin);

				if (rectSpin.PtInRect (ptCursor))
				{
					MapWindowPoints (pWndSpin, &ptCursor, 1); 

					pWndSpin->SendMessage (pMsg->message, pMsg->wParam, 
						MAKELPARAM (ptCursor.x, ptCursor.y));
					return TRUE;
				}
			}

			CWnd* pWndInPlaceEdit = pSel->GetInPlaceWnd ();
			if (pWndInPlaceEdit == NULL)
			{
				return CWnd::PreTranslateMessage(pMsg);
			}

			ASSERT_VALID (pWndInPlaceEdit);

			if (!pSel->m_bAllowEdit)
			{
				pWndInPlaceEdit->HideCaret ();
			}

			CRect rectEdit;
			pWndInPlaceEdit->GetClientRect (rectEdit);
			pWndInPlaceEdit->MapWindowPoints (this, rectEdit);

			if (rectEdit.PtInRect (ptCursor) &&
				pMsg->message == WM_LBUTTONDBLCLK)
			{
				if (pSel->OnDblClick (ptCursor))
				{
					return TRUE;
				}
			}

			if (rectEdit.PtInRect (ptCursor) && 
				pMsg->message == WM_RBUTTONDOWN &&
				!pSel->m_bAllowEdit)
			{
				return TRUE;
			}

			if (!rectEdit.PtInRect (ptCursor) &&
				(pMsg->message == WM_LBUTTONDOWN ||
				pMsg->message == WM_NCLBUTTONDOWN))
			{
				SQuestGridItem* pItem = (SQuestGridItem*)GetCurSelItem (pSel);
				if (pItem!= NULL && pItem->m_rectButton.PtInRect (ptCursor))
				{
					pItem->DoClickButton (ptCursor);
					return TRUE;
				}

				if (!EndEditItem ())
				{
					m_bNoUpdateWindow = FALSE;
					return TRUE;
				}
			}
			else
			{
				MapWindowPoints (pWndInPlaceEdit, &ptCursor, 1); 
				pWndInPlaceEdit->SendMessage (pMsg->message, pMsg->wParam, 
					MAKELPARAM (ptCursor.x, ptCursor.y));
				return TRUE;
			}
		}
	}

	return CWnd::PreTranslateMessage(pMsg);
}

#pragma once


// ConvCalc dialog

typedef void ( CALLBACK* CLOSETHECALC )	();

class AFX_EXT_CLASS ConvCalc : public CBCGPDialog
{
	DECLARE_DYNAMIC(ConvCalc)

public:
	ConvCalc(CWnd* pParent = NULL);   // standard constructor
	virtual ~ConvCalc();

// Dialog Data
	enum { IDD = IDD_CALC };

	void SetCloseCalcFunc( CLOSETHECALC );
	CLOSETHECALC _pFnClose;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();

	DECLARE_MESSAGE_MAP()

protected:
	BOOL		m_fChanged;
	CString		m_szInDeg;
	CString		m_szCmRad;
	double		m_dInDeg;
	double		m_dCmRad;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedRdoIncm();
	afx_msg void OnBnClickedRdoDegrad();
	afx_msg void OnEnKillfocusEditIndeg();
	afx_msg void OnEnChangeEditIndeg();
	afx_msg void OnEnChangeEditCmrad();
	afx_msg void OnEnKillfocusEditCmrad();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
};

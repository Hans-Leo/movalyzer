// SortDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "SortDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SortDlg dialog

BEGIN_MESSAGE_MAP(SortDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_RDO_ALPHA, OnRdoAlpha)
	ON_BN_CLICKED(IDC_RDO_CHRONO, OnRdoChrono)
	ON_BN_CLICKED(IDC_RDO_CUSTOM, OnRdoCustom)
//	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_BN_SEL, OnClickBnSel)
	ON_BN_CLICKED(IDC_BN_UP, OnClickBnUp)
	ON_BN_CLICKED(IDC_BN_DOWN, OnClickBnDown)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

SortDlg::SortDlg(UINT nSort, CString szExp, CStringList* pAll, CWnd* pParent)
	: CBCGPDialog(SortDlg::IDD, pParent), m_pAll( pAll ), m_pLstSelected( NULL ),
	  m_pLstSelectedOrig( NULL )
{
	m_nSort = nSort;
	m_szExp = szExp;
	m_nMode = SORT_CHRONO;

	if( pAll && !pAll->IsEmpty() )
	{
		m_pLstSelected = new BOOL[ pAll->GetCount() ];
		m_pLstSelectedOrig = new BOOL[ pAll->GetCount() ];
		for( int i = 0; i < pAll->GetCount(); i++ )
		{
			m_pLstSelected[ i ] = FALSE;
			m_pLstSelectedOrig[ i ] = TRUE;
		}
	}
}

SortDlg::~SortDlg()
{
	if( m_pLstSelected ) delete [] m_pLstSelected;
	if( m_pLstSelectedOrig ) delete [] m_pLstSelectedOrig;
}

void SortDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_BN_UP, m_bnUp);
	DDX_Control(pDX, IDC_BN_DOWN, m_bnDown);
	DDX_Control(pDX, IDC_BN_SEL, m_bnSel);
}

void SortDlg::InsertColumns()
{
	m_List.InsertColumn( 0, _T("Item ID"), LVCFMT_LEFT, 150 );
}

void SortDlg::FillList()
{
	if( m_lstSorted.IsEmpty() ) return;
	m_List.DeleteAllItems();

	POSITION pos = m_lstSorted.GetHeadPosition();
	CString szItem;
	int nItem = 0;
	while( pos )
	{
		szItem = m_lstSorted.GetNext( pos );
		if( szItem != HOLDER )
		{
			if( m_pLstSelected[ nItem ] )
				nItem = m_List.InsertItem( nItem, szItem, 12 );
			else
				nItem = m_List.InsertItem( nItem, szItem, 5 );
			nItem++;
		}
	}
}

void SortDlg::SortChrono()
{
	if( !m_pAll || !m_pLstSelected || ( m_pAll->GetCount() == 0 ) ) return;

	CStringList lstTemp;
	CString szItem, szItem1;
	POSITION pos, pos1;
	int nItem = 0, nIdx;

	// Temp list to maintain selections
	pos = m_lstSorted.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstSorted.GetNext( pos );
		lstTemp.AddTail( szItem );
		m_pLstSelectedOrig[ nItem ] = m_pLstSelected[ nItem ];
		nItem++;
	}

	// Sort the IDs
	// Input stringlist variable is already sorted chrono
	m_lstSorted.RemoveAll();
	pos1 = m_pAll->GetHeadPosition();
	while( pos1 )
	{
		szItem1 = m_pAll->GetNext( pos1 );
		m_lstSorted.AddTail( szItem1 );
	}

	// Sort the selections
	nItem = 0;
	pos1 = m_lstSorted.GetHeadPosition();
	while( pos1 )
	{
		nIdx = 0;
		szItem1 = m_lstSorted.GetNext( pos1 );
		pos = lstTemp.GetHeadPosition();
		while( pos )
		{
			szItem = lstTemp.GetNext( pos );
			if( szItem == szItem1 ) break;
			nIdx++;
		}

		m_pLstSelected[ nItem ] = m_pLstSelectedOrig[ nIdx ];
		nItem++;
	}
}

void SortDlg::SortAlpha()
{
	if( !m_pAll || !m_pLstSelected || ( m_pAll->GetCount() == 0 ) ) return;

	CStringList lstTemp;
	CString szItem, szItem1, szItem2;
	POSITION pos, pos1, pos2, posLast;
	int nItem = 0, nIdx;
	BOOL fIn;

	// Temp list to maintain selections
	pos = m_lstSorted.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstSorted.GetNext( pos );
		lstTemp.AddTail( szItem );
		m_pLstSelectedOrig[ nItem ] = m_pLstSelected[ nItem ];
		nItem++;
	}

	// Sort the IDs
	m_lstSorted.RemoveAll();
	pos1 = m_pAll->GetHeadPosition();
	while( pos1 )
	{
		fIn = FALSE;

		szItem1 = m_pAll->GetNext( pos1 );
		pos2 = m_lstSorted.GetHeadPosition();
		while( pos2 )
		{
			posLast = pos2;
			szItem2 = m_lstSorted.GetNext( pos2 );
			if( szItem1 < szItem2 )
			{
				m_lstSorted.InsertBefore( posLast, szItem1 );
				fIn = TRUE;
				break;
			}
		}
		if( !fIn ) m_lstSorted.AddTail( szItem1 );
	}

	// Sort the selections
	nItem = 0;
	pos1 = m_lstSorted.GetHeadPosition();
	while( pos1 )
	{
		nIdx = 0;
		szItem1 = m_lstSorted.GetNext( pos1 );
		pos = lstTemp.GetHeadPosition();
		while( pos )
		{
			szItem = lstTemp.GetNext( pos );
			if( szItem == szItem1 ) break;
			nIdx++;
		}

		m_pLstSelected[ nItem ] = m_pLstSelectedOrig[ nIdx ];
		nItem++;
	}
}

void SortDlg::OnModeChange()
{
	CWnd* pUp = GetDlgItem( IDC_BN_UP );
	CWnd* pDown = GetDlgItem( IDC_BN_DOWN );

//	if( pUp ) pUp->EnableWindow( m_nMode == SORT_CUSTOM );
//	if( pDown ) pDown->EnableWindow( m_nMode == SORT_CUSTOM );

	CheckRadioButton( IDC_RDO_CHRONO, IDC_RDO_CUSTOM, m_nMode );
}

/////////////////////////////////////////////////////////////////////////////
// SortDlg message handlers

BOOL SortDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	// button images
	m_bnUp.SetImage( IDB_UP );
	m_bnDown.SetImage( IDB_DOWN );
	m_bnSel.SetImage( IDB_TRIALG );
	EnableVisualManagerStyle( TRUE, TRUE );

	// Title
	CString szTitle = _T("Sort and Select");
	switch( m_nSort )
	{
		case SORT_GROUPS: szTitle += _T(" Groups"); break;
		case SORT_CONDITIONS: szTitle += _T(" Conditions"); break;
		case SORT_SUBJECTS: szTitle += _T(" Subjects"); break;
	};
	SetWindowText( szTitle );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Current sort order of items starting from top of list."), _T("Sort List") );
	pWnd = GetDlgItem( IDC_RDO_CHRONO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Sort items chronologically (when added to system)."), _T("Chronologically") );
	pWnd = GetDlgItem( IDC_RDO_ALPHA );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Sort items alphabetically."), _T("Alphabetically") );
	pWnd = GetDlgItem( IDC_RDO_CUSTOM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Sort items manually."), _T("Manually") );
	pWnd = GetDlgItem( IDC_BN_UP );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEUP, _T("Move Up") );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEDOWN, _T("Move Down") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Accept changes."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Cancel changes."), _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_SEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Toggle selection of currently selected item."), _T("Selection") );

	// Image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_List.SetImageList( &m_ImageList, LVSIL_SMALL );

	// Column Headers
	InsertColumns();

	// If sort file exists, we sort custom
	CString szFile;
	switch( m_nSort )
	{
		case SORT_GROUPS: ::GetGroupSort( m_szExp, szFile ); break;
		case SORT_CONDITIONS: ::GetCondSort( m_szExp, szFile ); break;
		case SORT_SUBJECTS: ::GetSubjSort( m_szExp, szFile ); break;
	};
	CStdioFile f;
	CString szItem;
	int nIdx = 0, nPos = -1;
	BOOL fInc = FALSE;
	if( f.Open( szFile, CFile::modeRead | CFile::typeText ) )
	{
		m_nMode = SORT_CUSTOM;		// Custom mode by default

		// Get items from file
		while( f.ReadString( szItem ) )
		{
			// read sort mode from file (older versions wont have this so need to check)
			if( szItem.Find( _T(":") ) == -1 )
			{
				m_nMode = atoi( szItem );
				f.ReadString( szItem );
			}

			if( szItem != _T("") &&
				( szItem[ szItem.GetLength() - 1 ] == '1' ) )
				fInc = TRUE;
			else fInc = FALSE;
			nPos = szItem.Find( _T(":") );
			if( nPos != -1 ) szItem = szItem.Left( nPos );

			// We only add if this item still exists
			if( ( szItem != HOLDER ) && m_pAll && m_pAll->Find( szItem ) )
			{
				m_lstSorted.AddTail( szItem );
				m_pLstSelected[ nIdx++ ] = fInc;
			}
		}
		f.Close();

		// Now we add to the end of the list those items that are new not in sort file
		POSITION pos = m_pAll ? m_pAll->GetHeadPosition() : NULL;
		while( pos )
		{
			szItem = m_pAll->GetNext( pos );
			if( ( szItem != HOLDER ) && !m_lstSorted.Find( szItem ) )
				m_lstSorted.AddTail( szItem );
		}
	} else
	{
		for( int i = 0; i < this->m_pAll->GetCount(); i++ )
			m_pLstSelected[ i ] = TRUE;
		SortChrono();
	}

	// Fill List
	FillList();

	// Update based on mode
	OnModeChange();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL SortDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void SortDlg::OnOK()
{
	if( m_lstSorted.GetCount() == 0 )
	{
		BCGPMessageBox( _T("Nothing to save.") );
		return;
	}

	BOOL fAny = FALSE;
	for( int i = 0; i < m_pAll->GetCount(); i++ )
	{
		if( m_pLstSelected[ i ] )
		{
			fAny = TRUE;
			break;
		}
	}
	if( !fAny )
	{
		BCGPMessageBox( _T("At least one item must be included.") );
		return;
	}

	CString szFile;
	switch( m_nSort )
	{
		case SORT_GROUPS: ::GetGroupSort( m_szExp, szFile ); break;
		case SORT_CONDITIONS: ::GetCondSort( m_szExp, szFile ); break;
		case SORT_SUBJECTS: ::GetSubjSort( m_szExp, szFile ); break;
	};

	CStdioFile f;
	if( !f.Open( szFile, CFile::modeCreate | CFile::modeWrite | CFile::typeText ) )
	{
		BCGPMessageBox( _T("Error in opening sort file.") );
		return;
	}

	// SORT FILE
	int nIdx = -1;
	CString szItem;
	// write sorting method to sort file
	szItem.Format( _T("%d\n"), m_nMode );
	f.WriteString( szItem );
	// write items to sort file
	POSITION pos = m_lstSorted.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstSorted.GetNext( pos );
		szItem += _T(":");
		nIdx++;
		if( m_pLstSelected[ nIdx ] ) szItem += _T("1");
		else szItem += _T("0");
		szItem += _T("\n");
		f.WriteString( szItem );
	}

	f.Close();

	CBCGPDialog::OnOK();
}

void SortDlg::OnRdoAlpha() 
{
	m_nMode = SORT_ALPHA;
	OnModeChange();
	SortAlpha();
	FillList();
}

void SortDlg::OnRdoChrono() 
{
	m_nMode = SORT_CHRONO;
	OnModeChange();
	SortChrono();
	FillList();
}

void SortDlg::OnRdoCustom() 
{
	m_nMode = SORT_CUSTOM;
	OnModeChange();
	// No need to sort or refresh tree...user from here can manipulate
}

void SortDlg::OnMoveItem( int nDir )
{
	if( ( nDir < -1 ) || ( nDir > 1 ) ) return;
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;
	if( ( nDir > 0 ) && ( nIdx == ( m_lstSorted.GetCount() - 1 ) ) ) return;

	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL;

	pos1 = m_lstSorted.FindIndex( nIdx );
	pos2 = m_lstSorted.FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 ) return;
	szItem1 = m_lstSorted.GetAt( pos1 );
	szItem2 = m_lstSorted.GetAt( pos2 );
	m_lstSorted.SetAt( pos2, szItem1 );
	m_lstSorted.SetAt( pos1, szItem2 );

	BOOL f1 = m_pLstSelected[ nIdx ];
	BOOL f2 = m_pLstSelected[ nIdx + nDir ];
	m_pLstSelected[ nIdx ] = f2;
	m_pLstSelected[ nIdx + nDir ] = f1;

	FillList();

	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED;
	lvi.stateMask = (UINT)-1;
	m_List.SetItemState( nIdx + nDir, &lvi );
	m_List.EnsureVisible( nIdx + nDir, FALSE );
}

void SortDlg::OnClickBnUp() 
{
	m_nMode = SORT_CUSTOM;
	OnModeChange();
	OnMoveItem( -1 );
}

void SortDlg::OnClickBnDown() 
{
	m_nMode = SORT_CUSTOM;
	OnModeChange();
	OnMoveItem( 1 );
}

void SortDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	m_pLstSelected[ nIdx ] = !m_pLstSelected[ nIdx ];
	FillList();

	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED;
	lvi.stateMask = (UINT)-1;
	m_List.SetItemState( nIdx, &lvi );
	m_List.EnsureVisible( nIdx, FALSE );

	*pResult = 0;
}

void SortDlg::OnClickBnSel()
{
	int nIdx = m_List.GetNextItem( -1, LVNI_SELECTED );
	if( nIdx < 0 ) return;

	m_pLstSelected[ nIdx ] = !m_pLstSelected[ nIdx ];
	FillList();

	LVITEM lvi;
	lvi.mask = LVIF_STATE;
	lvi.state = LVIS_SELECTED;
	lvi.stateMask = (UINT)-1;
	m_List.SetItemState( nIdx, &lvi );
	m_List.EnsureVisible( nIdx, FALSE );
}

BOOL SortDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("analysischarts.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

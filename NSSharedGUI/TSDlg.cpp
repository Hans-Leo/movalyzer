// TroubleShootingDlg.cpp : implementation file
//

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include "TSDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


TroubleShootingView* _pTSView = NULL;

/////////////////////////////////////////////////////////////////////////////
// TroubleShootingDlg

IMPLEMENT_DYNAMIC( TroubleShootingDlg, CBCGPDialog )

BEGIN_MESSAGE_MAP( TroubleShootingDlg, CBCGPDialog )
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TroubleShootingDlg message handlers

BOOL TroubleShootingDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	class CMyDoc : public CDocument
	{ public: CMyDoc() {} };
	// create view
	CCreateContext pContext;
	CFrameWnd* pFrameWnd = (CFrameWnd*)this;
	pContext.m_pCurrentDoc = ((CFrameWnd*)AfxGetMainWnd())->GetActiveDocument();
	pContext.m_pNewViewClass = RUNTIME_CLASS( TroubleShootingView );
	_pTSView = (TroubleShootingView*)pFrameWnd->CreateView( &pContext );
	_pTSView->ShowWindow( SW_NORMAL );

	// resize view using frame (pic ctrl)
	CWnd* pFrame = GetDlgItem( IDC_HTML );
	CRect rectWindow;
	pFrame->GetClientRect( rectWindow );
	// Leave a little space for border and title
	rectWindow.left += 10;
	rectWindow.top += 5;
	_pTSView->MoveWindow( rectWindow );
	_pTSView->OnInitialUpdate();

	return TRUE;  // return TRUE  unless you set the focus to a control}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// TroubleShootingView

CString	_szTSURL;

IMPLEMENT_DYNCREATE( TroubleShootingView, CHtmlView )

BEGIN_MESSAGE_MAP( TroubleShootingView, CHtmlView )
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

TroubleShootingView::TroubleShootingView()
{
	m_fInit = FALSE;
}

TroubleShootingView::~TroubleShootingView()
{
}

/////////////////////////////////////////////////////////////////////////////
// TroubleShootingView message handlers

CString TroubleShootingView::ResourceToURL( CString szFile )
{
	_szTSURL = _T("");
	HINSTANCE hInstance = AfxGetResourceHandle();
	LPTSTR lpszModule = new TCHAR[_MAX_PATH];
	if( GetModuleFileName( hInstance, lpszModule, _MAX_PATH ) )
		_szTSURL.Format(_T("res://%s/%s"), lpszModule, szFile );
	delete []lpszModule;
	return _szTSURL;
}

void TroubleShootingView::ShowPage( CString szURL )
{
	Navigate2( szURL );
	UpdateWindow();
}

void TroubleShootingView::OnInitialUpdate()
{
	CHtmlView::OnInitialUpdate();
	m_fInit = TRUE;
	OnShowWindow( TRUE, 0 );
}

void TroubleShootingView::OnDestroy() 
{
	CHtmlView::OnDestroy();
	CView::OnDestroy();		// Fixes CHtmlView bug
}

void TroubleShootingView::OnShowWindow( BOOL bShow, UINT nStatus )
{
	CHtmlView::OnShowWindow( bShow, nStatus );
	if( !m_fInit || !bShow ) return;

	CString szPage = ResourceToURL( _T("ts.html") );
	ShowPage( szPage );
}

#pragma once

#include "NSTooltipCtrl.h"

// ScatterGroupingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ScatterGroupingDlg dialog

class AFX_EXT_CLASS ScatterGroupingDlg : public CBCGPDialog
{
// Construction
public:
	ScatterGroupingDlg(CWnd* pParent);
	~ScatterGroupingDlg();

// Dialog Data
	enum { IDD = IDD_SCATGROUPING };
	BOOL				m_fGrouping;
	int					m_nPoints;
	int					m_nItem;
	int					m_nPoint;
	int					m_nSize;
	int					m_nGrouping;
	int*				m_pPoints;
	int*				m_pSizes;
	DWORD*				m_pColors;
	CBCGPColorButton	m_bnColor;
	NSToolTipCtrl		m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboItem();
	afx_msg void OnClickBnColor();
	afx_msg void OnSelchangeCboPoint();
	afx_msg void OnSelchangeCboSize();
	afx_msg void OnChkGrouping();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

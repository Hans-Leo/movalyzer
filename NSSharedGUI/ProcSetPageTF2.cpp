// ProcSetPageTF2.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageTF2.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageTF2 property page

IMPLEMENT_DYNCREATE(ProcSetPageTF2, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageTF2, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageTF2::ProcSetPageTF2( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageTF2::IDD), m_pPS( pPS )
{
	m_nDisCorrection = 0;
	m_dDisFactor = 0.2;
	m_dDisZInsertPoint = -1.0;
	m_dDisRelError = 20.0;
	m_dDisAbsError = 100.0;
	m_dDisError = 0.3;
	m_fNotify = FALSE;

	m_editDisFactor.SetDefaultValue( m_dDisFactor );
	m_editDisZInsertPoint.SetDefaultValue( m_dDisZInsertPoint );
	m_editDisRelError.SetDefaultValue( m_dDisRelError );
	m_editDisAbsError.SetDefaultValue( m_dDisAbsError );
	m_editDisError.SetDefaultValue( m_dDisError );
	m_chkNotify.SetDefaultAsOff( !m_fNotify );
	m_rdoDiscard.SetDefaultRadio( IDC_RDO_DISCARD );
	m_rdoInsert.SetDefaultRadio( IDC_RDO_DISCARD );
	m_rdoMove.SetDefaultRadio( IDC_RDO_DISCARD );
	m_rdoAccept.SetDefaultRadio( IDC_RDO_DISCARD );

	if( m_pPS )
	{
		// get appropriate interface
		m_pPS->get_DisCorrection( &m_nDisCorrection );
		m_pPS->get_DisFactor( &m_dDisFactor );
		m_pPS->get_DisZInsertPoint( &m_dDisZInsertPoint );
		m_pPS->get_DisRelError( &m_dDisRelError );
		m_pPS->get_DisAbsError( &m_dDisAbsError );
		m_pPS->get_DisError( &m_dDisError );
		m_pPS->get_DisNotify( &m_fNotify );
	}
}

ProcSetPageTF2::~ProcSetPageTF2()
{
}

void ProcSetPageTF2::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INTCONST, m_dDisFactor);
	DDX_Text(pDX, IDC_EDIT_ZINSPOINT, m_dDisZInsertPoint);
	DDX_Text(pDX, IDC_EDIT_RELERR, m_dDisRelError);
	DDX_Text(pDX, IDC_EDIT_ABSERR, m_dDisAbsError);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);
	DDX_Text(pDX, IDC_EDIT_DISERR, m_dDisError);
	DDX_Check(pDX, IDC_CHK_NOTIFY, m_fNotify);

	DDX_Control(pDX, IDC_EDIT_INTCONST, m_editDisFactor);
	DDX_Control(pDX, IDC_EDIT_ZINSPOINT, m_editDisZInsertPoint);
	DDX_Control(pDX, IDC_EDIT_RELERR, m_editDisRelError);
	DDX_Control(pDX, IDC_EDIT_ABSERR, m_editDisAbsError);
	DDX_Control(pDX, IDC_EDIT_DISERR, m_editDisError);
	DDX_Control(pDX, IDC_CHK_NOTIFY, m_chkNotify);
	DDX_Control(pDX, IDC_RDO_DISCARD, m_rdoDiscard);
	DDX_Control(pDX, IDC_RDO_CONST, m_rdoInsert);
	DDX_Control(pDX, IDC_RDO_TO, m_rdoMove);
	DDX_Control(pDX, IDC_RDO_ACCEPT, m_rdoAccept);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageTF2 message handlers

BOOL ProcSetPageTF2::OnApply() 
{
	UpdateData( TRUE );

	CString szMsg;
	if( ( m_dDisZInsertPoint < -1000. ) || ( m_dDisZInsertPoint > -1. ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 14 );
		szMsg = _T("Pressure value to indicate raw data manipulation must be between -1000 and -1.");
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ZINSPOINT );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_dDisError <= 0. )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 14 );
		szMsg = _T("Max. relative intersample period error is out of range.\nIt must be greater than 0.");
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DISERR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_dDisAbsError <= 1. )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 14 );
		szMsg = _T("Max. absolute intersample distance is out of range.\nIt must be greater than 1.");
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABSERR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_dDisRelError <= 1. )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 14 );
		szMsg = _T("Max. relative intersample distance is out of range.\nIt must be greater than 1.");
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_RELERR );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_dDisFactor <= 0. ) || ( m_dDisFactor > 0.999 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 14 );
		szMsg = _T("Running avg. distance integration speed is out of range.\nAccepted values are between 0.001 and 0.999.");
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_INTCONST );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	if( !m_pPS ) return FALSE;
	int nCorr = GetCheckedRadioButton( IDC_RDO_DISCARD, IDC_RDO_ACCEPT );
	if( nCorr == IDC_RDO_DISCARD ) m_nDisCorrection = 0;
	else if( nCorr == IDC_RDO_CONST ) m_nDisCorrection = 1;
	else if( nCorr == IDC_RDO_TO ) m_nDisCorrection = 2;
	else if( nCorr == IDC_RDO_ACCEPT ) m_nDisCorrection = 3;

	m_pPS->put_DisCorrection( m_nDisCorrection );
	m_pPS->put_DisFactor( m_dDisFactor );
	m_pPS->put_DisZInsertPoint( m_dDisZInsertPoint );
	m_pPS->put_DisRelError( m_dDisRelError );
	m_pPS->put_DisAbsError( m_dDisAbsError );
	m_pPS->put_DisError( m_dDisError );
	m_pPS->put_DisNotify( m_fNotify );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageTF2::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageTF2::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	double dDisFactor = 0.2, dDisZInsertPoint = -1.0, dDisRelErr = 20.0, dDisAbsErr = 100.0, dDisErr = 0.3;
	BOOL fNotify = TRUE;
	if( SUCCEEDED ( hr ) )
	{
		pPS->get_DisFactor( &dDisFactor );
		pPS->get_DisZInsertPoint( &dDisZInsertPoint );
		pPS->get_DisRelError( &dDisRelErr );
		pPS->get_DisAbsError( &dDisAbsErr );
		pPS->get_DisError( &dDisErr );
		pPS->get_DisNotify( &fNotify );
		pPS->Release();
	}

	// Tooltip support
	CString szData, szTmp;
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_RDO_DISCARD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Discontinuities due to pen lifts disturb filtering and should be omitted or corrected. Discard the discontinuities."), _T("Discard") );
	pWnd = GetDlgItem( IDC_RDO_CONST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Discontinuities due to pen lifts disturb filtering and should be omitted or corrected.\nUse timestamps of delayed samples recorded in .DIS file if it exists, otherwise velocity = running average.\nDelays occur at high pen lifts. Only total time will be restored. Additional discontinuity may still exist."), _T("Insert") );
	pWnd = GetDlgItem( IDC_RDO_TO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Discontinuities due to pen lifts disturb filtering and should be omitted or corrected.\nMove toward the start of the discontinuities."), _T("Move") );
	pWnd = GetDlgItem( IDC_RDO_ACCEPT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Discontinuities due to pen lifts disturb filtering and should be omitted or corrected.\nAccept the discontinuities."), _T("Accept") );

	pWnd = GetDlgItem( IDC_EDIT_DISERR );
	szData = _T("Intersample periods outside MaxRelativeInterSamplePeriod / SamplingRate are detected and stored in the Discotinuity File during recording.");
	szTmp.Format( _T(" [DEFAULT=%g]"), dDisErr );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Rel. Intersample Period Error") );

	pWnd = GetDlgItem( IDC_CHK_NOTIFY );
	szData = _T("Using a device beep, notify the user that a discontinuity in the input device samples has been detected using the criteria detailed in the Max Relative Intersample Period Error");
	szTmp.Format( _T(" [DEFAULT=%s]"), fNotify ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Notify Of Discontinuity") );

	pWnd = GetDlgItem( IDC_EDIT_INTCONST );
	szData = _T("Rel. intersample speed for running sample difference");
	szTmp.Format( _T(" [DEFAULT=%g]"), dDisFactor );
	szData += szTmp;
	szData += _T("\nWhen intersample distance exceeds MaxAbsIntersampleDistance + MaxRelInterSampleDistance * RunningAvgDistance a discontinuity is detected.\nCurrent running avg. intersample distance = Integration speed * Current intersample distance + (1 - Integration speed) * previous Running avg. intersample distance.");
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Running Avg. Distance Integration Speed") );

	pWnd = GetDlgItem( IDC_EDIT_ZINSPOINT );
	szData = _T("Set pressure of inserted or first moved sample negative to mark raw data manipulation");
	szTmp.Format( _T(" [DEFAULT=%g]"), dDisZInsertPoint );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Pressure") );

	pWnd = GetDlgItem( IDC_EDIT_RELERR );
	szData = _T("Maximum step size (inter sample distance) of the next sample compared to the running average of step size.");
	szTmp.Format( _T(" [DEFAULT=%g]"), dDisRelErr );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Relative Error") );

	pWnd = GetDlgItem( IDC_EDIT_ABSERR );
	szData = _T("Maximum step size (inter sample distance) when the pen is still.");
	szTmp.Format( _T(" [DEFAULT=%g]"), dDisAbsErr );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Absolute Error") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	int nRdo = IDC_RDO_DISCARD;
	if( m_nDisCorrection == 1 ) nRdo = IDC_RDO_CONST;
	else if( m_nDisCorrection == 2 ) nRdo = IDC_RDO_TO;
	else if( m_nDisCorrection == 3 ) nRdo = IDC_RDO_ACCEPT;
	CheckRadioButton( IDC_RDO_DISCARD, IDC_RDO_ACCEPT, nRdo );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageTF2::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageTF2::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_timefun.html"), this );
	return TRUE;
}

void ProcSetPageTF2::OnBnReset() 
{
	m_nDisCorrection = 0;
	m_dDisFactor = 0.2;
	m_dDisZInsertPoint = -1.0;
	m_dDisRelError = 20.0;
	m_dDisAbsError = 100.0;
	m_dDisError = 0.3;
	m_fNotify = FALSE;

	UpdateData( FALSE );

	CheckRadioButton( IDC_RDO_DISCARD, IDC_RDO_ACCEPT, IDC_RDO_DISCARD );
}

void ProcSetPageTF2::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 8 );
}

// ProcSetPageCon.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageCon.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageCon property page

IMPLEMENT_DYNCREATE(ProcSetPageCon, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageCon, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_BN_CLICKED(IDC_CHK_DLTM, OnBnClickedChkDltm)
	ON_BN_CLICKED(IDC_CHK_DGTM, OnBnClickedChkDgtm)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED2, OnClickDis)
END_MESSAGE_MAP()

ProcSetPageCon::ProcSetPageCon( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageCon::IDD), m_pPS( pPS )
{
	m_fA = FALSE;
	m_fC = FALSE;
	m_fM = FALSE;
	m_fN = FALSE;
	m_fS = FALSE;
	m_fU = FALSE;
	m_fD = FALSE;
	m_fDLTM = FALSE;
	m_fDGTM = FALSE;
	m_dMINRT = 0.1;
	m_dMAXRT = 3.0;

	m_chkA.SetDefaultAsOff( !m_fA );
	m_chkC.SetDefaultAsOff( !m_fC );
	m_chkM.SetDefaultAsOff( !m_fM );
	m_chkN.SetDefaultAsOff( !m_fN );
	m_chkS.SetDefaultAsOff( !m_fS );
	m_chkU.SetDefaultAsOff( !m_fU );
	m_chkD.SetDefaultAsOff( !m_fD );
	m_chkDLTM.SetDefaultAsOff( !m_fDLTM );
	m_chkDGTM.SetDefaultAsOff( !m_fDGTM );
	m_editMINRT.SetDefaultValue( m_dMINRT );
	m_editMAXRT.SetDefaultValue( m_dMAXRT );

	if( m_pPS )
	{
		m_pPS->get_DiscardLTMin( &m_fDLTM );
		m_pPS->get_DiscardGTMax( &m_fDGTM );
		m_pPS->get_MinReactionTime( &m_dMINRT );
		m_pPS->get_MaxReactionTime( &m_dMAXRT );

		IProcSetFlags* pPSF = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_CON_A( &m_fA );
			pPSF->get_CON_C( &m_fC );
			pPSF->get_CON_M( &m_fM );
			pPSF->get_CON_N( &m_fN );
			pPSF->get_CON_S( &m_fS );
			pPSF->get_CON_U( &m_fU );
			pPSF->get_CON_D( &m_fD );

			pPSF->Release();
		}
	}
}

ProcSetPageCon::~ProcSetPageCon()
{
}

void ProcSetPageCon::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_CONA, m_fA);
	DDX_Check(pDX, IDC_CHK_CONC, m_fC);
	DDX_Check(pDX, IDC_CHK_CONM, m_fM);
	DDX_Check(pDX, IDC_CHK_CONN, m_fN);
	DDX_Check(pDX, IDC_CHK_CONS, m_fS);
	DDX_Check(pDX, IDC_CHK_CONU, m_fU);
	DDX_Check(pDX, IDC_CHK_COND, m_fD);
	DDX_Check(pDX, IDC_CHK_DLTM, m_fDLTM);
	DDX_Check(pDX, IDC_CHK_DGTM, m_fDGTM);
	DDX_Text(pDX, IDC_EDIT_MINRT, m_dMINRT);
	DDX_Text(pDX, IDC_EDIT_MAXRT, m_dMAXRT);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);
	DDX_Control(pDX, IDC_TXT_ADVANCED2, m_linkDis);

	DDX_Control(pDX, IDC_CHK_CONA, m_chkA);
	DDX_Control(pDX, IDC_CHK_CONC, m_chkC);
	DDX_Control(pDX, IDC_CHK_CONM, m_chkM);
	DDX_Control(pDX, IDC_CHK_CONN, m_chkN);
	DDX_Control(pDX, IDC_CHK_CONS, m_chkS);
	DDX_Control(pDX, IDC_CHK_CONU, m_chkU);
	DDX_Control(pDX, IDC_CHK_COND, m_chkD);
	DDX_Control(pDX, IDC_CHK_DLTM, m_chkDLTM);
	DDX_Control(pDX, IDC_CHK_DGTM, m_chkDGTM);
	DDX_Control(pDX, IDC_EDIT_MINRT, m_editMINRT);
	DDX_Control(pDX, IDC_EDIT_MAXRT, m_editMAXRT);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageCon message handlers

BOOL ProcSetPageCon::OnApply() 
{
	if( !m_pPS ) return FALSE;

	m_pPS->put_DiscardLTMin( m_fDLTM );
	m_pPS->put_DiscardGTMax( m_fDGTM );
	m_pPS->put_MinReactionTime( m_dMINRT );
	m_pPS->put_MaxReactionTime( m_dMAXRT );

	IProcSetFlags* pPSF = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( SUCCEEDED( hr ) )
	{
		pPSF->put_CON_A( m_fA );
		pPSF->put_CON_C( m_fC );
		pPSF->put_CON_M( m_fM );
		pPSF->put_CON_N( m_fN );
		pPSF->put_CON_S( m_fS );
		pPSF->put_CON_U( m_fU );
		pPSF->put_CON_D( m_fD );

		pPSF->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageCon::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageCon::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();
	m_linkDis.SetImage( IDB_DATA );
	m_linkDis.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	BOOL fA = FALSE, fC = FALSE, fM = FALSE, fN = FALSE, fS = FALSE, fU = FALSE, fD = TRUE;
	BOOL fDLTM = FALSE, fDGTM = FALSE;
	double dMINRT = 0.1, dMAXRT = 3.0;
	if( pPS )
	{
		pPS->get_DiscardLTMin( &fDLTM );
		pPS->get_DiscardGTMax( &fDGTM );
		pPS->get_MinReactionTime( &dMINRT );
		pPS->get_MaxReactionTime( &dMAXRT );

		IProcSetFlags* pPSF = NULL;
		HRESULT hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( SUCCEEDED( hr ) )
		{
			pPSF->get_CON_A( &fA );
			pPSF->get_CON_C( &fC );
			pPSF->get_CON_M( &fM );
			pPSF->get_CON_N( &fN );
			pPSF->get_CON_S( &fS );
			pPSF->get_CON_U( &fU );
			pPSF->get_CON_D( &fD );

			pPSF->Release();
		}
		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_CHK_CONA );
	szData.LoadString( IDS_PSPC_ABS );
	szTmp.Format( _T(" [DEFAULT=%s]"), fA ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Absolute Measures") );

	pWnd = GetDlgItem( IDC_CHK_CONM );
	szData.LoadString( IDS_PSPC_DISCARD );
	szTmp.Format( _T(" [DEFAULT=%s]"), fM ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Out of Range") );

	pWnd = GetDlgItem( IDC_CHK_CONU );
	szData.LoadString( IDS_PSPC_NOREM );
	szTmp.Format( _T(" [DEFAULT=%s]"), fU ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Initial Downstroke") );

	pWnd = GetDlgItem( IDC_CHK_COND );
	szData.Format( _T("Discard trial if missing samples (discontinuity during recording) from high pen lift [DEFAULT=%s]"), fD ? _T("TRUE") : _T("FALSE") );
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Discontinuity") );

	pWnd = GetDlgItem( IDC_CHK_DLTM );
	szData = _T("Discard this trial if reaction time is less than specified minimum reaction time");
	szTmp.Format( _T(" [DEFAULT=%s]"), fDLTM ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Reaction Time") );

	pWnd = GetDlgItem( IDC_CHK_DGTM );
	szData = _T("Discard this trial if reaction time is greater than specified maximum reaction time");
	szTmp.Format( _T(" [DEFAULT=%s]"), fDGTM ? _T("TRUE") : _T("FALSE") );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Reaction Time") );

	pWnd = GetDlgItem( IDC_EDIT_MINRT );
	szData = _T("Specify minimum reaction time");
	szTmp.Format( _T(" [DEFAULT=%g]"), dMINRT );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData,  _T("Min Reaction Time") );

	pWnd = GetDlgItem( IDC_EDIT_MAXRT );
	szData = _T("Specify maximum reaction time");
	szTmp.Format( _T(" [DEFAULT=%g"), dMAXRT );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Reaction Time") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	if( !m_fDLTM )
	{
		pWnd = GetDlgItem( IDC_EDIT_MINRT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	if( !m_fDGTM )
	{
		pWnd = GetDlgItem( IDC_EDIT_MAXRT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageCon::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageCon::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_consistencychecking.html"), this );
	return TRUE;
}

void ProcSetPageCon::OnBnReset() 
{
	m_fA = FALSE;
	m_fC = FALSE;
	m_fM = FALSE;
	m_fN = FALSE;
	m_fS = FALSE;
	m_fU = FALSE;
	m_fD = TRUE;
	m_fDLTM = FALSE;
	m_fDGTM = FALSE;
	m_dMINRT = 0.1;
	m_dMAXRT = 3.0;

	UpdateData( FALSE );
}

void ProcSetPageCon::OnBnClickedChkDltm()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_MINRT );
	if( pWnd ) pWnd->EnableWindow( m_fDLTM );
}

void ProcSetPageCon::OnBnClickedChkDgtm()
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_MAXRT );
	if( pWnd ) pWnd->EnableWindow( m_fDGTM );
}

void ProcSetPageCon::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 18 );
}

void ProcSetPageCon::OnClickDis()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 15 );
}

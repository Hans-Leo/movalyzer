// Experiments.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\Common\UserObj.h"
#include "..\NSShared\hlib.h"
#include "ProcSetSheet.h"
#include "OverwriteDlg.h"
#include "ExcludeDlg.h"
#include "ExportDlg.h"
#include "ImportDialog.h"
#include "..\NSShared\SFINX.h"
#include "..\Common\MessageManager.h"
#include "..\COmmon\randomc.h"
#include "Experiments.h"
#include "Groups.h"
#include "Subjects.h"
#include "Conditions.h"
#include "Stimuli.h"
#include "Elements.h"
#include "Cats.h"
#include "MQuests.h"
#include "Feedbacks.h"
#include "Users.h"
#include <direct.h>
#include <math.h>
#include "..\ProcessMod\ProcessMod.h"
#include "..\ProcessMod\ProcessMod_i.c"
#include "Progress.h"
#include "UserMessages.h"
#import "..\NSSharedGUI\mlapp.tlb"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _DO_EPS		1

// main thread object to call things if w/in thread
CWinThread* _pMainThread = NULL;

// MatLab object
MLApp::DIMLAppPtr _ml = NULL;

// write string item to memory file
#define WRITE_ITEM(a) \
	WRITE_STRING( pMemFile, a ); \
	WRITE_STRING( pMemFile, _T("\n") );

// write boolean item to memory file
#define WRITE_ITEM_BOOL(a) \
	szItem = a ? _T("1") : _T("0"); \
	WRITE_ITEM( szItem );

// write integer item to memory file
#define WRITE_ITEM_INT(a) \
	szItem.Format( _T("%d"), a ); \
	WRITE_ITEM( szItem );

// write dword/colorref item to memory file
#define	WRITE_ITEM_DWORD(a) \
	szItem.Format( _T("%u"), a ); \
	WRITE_ITEM( szItem );

// write real # to memory file
#define	WRITE_ITEM_DOUBLE(a) \
	szItem.Format( _T("%f"), a ); \
	WRITE_ITEM( szItem );

// write date item to memory file
#define	WRITE_ITEM_DATE(a) \
	szItem = a.Format( "%m/%d/%Y %H:%M:%S" ); \
	WRITE_ITEM( szItem );

// write BSTR item to memory file
#define	WRITE_ITEM_BSTR(a) \
	szItem = a; \
	WRITE_ITEM( szItem );


///////////////////////////////////////////////////////////////////////////////
// Implementation

HTREEITEM Experiments::_hItem = NULL;
IProcess* Experiments::_pProcess = NULL;
short Experiments::_Type = EXP_TYPE_HANDWRITING;
BOOL Experiments::_Reproc = FALSE;
BOOL Experiments::_ReprocAll = FALSE;
BOOL Experiments::_TerminateThread = FALSE;
CString Experiments::_ExpID = _T("");
LPVOID Experiments::_ItemView = NULL;
LPVOID Experiments::_MsgView = NULL;

void SetReprocAll( BOOL fVal ) { Experiments::_ReprocAll = fVal; }
void SetTermThread( BOOL fVal ) { Experiments::_TerminateThread = fVal; }

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

void Experiments::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	Experiments::GetDescriptionWithID( m_pExperiment, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::GetNext()
{
	return Experiments::GetNext( m_pExperiment );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Find( CString szID )
{
	return Experiments::Find( m_pExperiment, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Add()
{
	return Experiments::Add( m_pExperiment );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Modify()
{
	return Experiments::Modify( m_pExperiment );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Remove()
{
	return Experiments::Remove( m_pExperiment );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetDataPath( CString szPath )
{
	Experiments::SetDataPath( m_pExperiment, szPath );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetBackupPath( CString szPath )
{
	Experiments::SetBackupPath( m_pExperiment, szPath );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetAppWindow( HWND AppWindow )
{
	Experiments::SetAppWindow( m_pExperiment, AppWindow );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetAppWindow( IExperiment* pExperiment, HWND AppWindow )
{
	if( pExperiment ) pExperiment->SetAppWindow( (DWORD)AppWindow );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Experiments::GetID( IExperiment* pExperiment, CString& szVal )
{
	if( pExperiment )
	{
		BSTR bstrItem;
		pExperiment->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::GetDescription( IExperiment* pExperiment, CString& szVal )
{
	if( pExperiment )
	{
		BSTR bstrItem;
		pExperiment->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::GetDescriptionWithID( IExperiment* pExperiment, CString& szVal, BOOL fReverse )
{
	if( pExperiment )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		Experiments::GetID( pExperiment, szID );
		pExperiment->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::GetNotes( IExperiment* pExperiment, CString& szVal )
{
	if( pExperiment )
	{
		BSTR bstrItem;
		pExperiment->get_Notes( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::GetType( IExperiment* pExperiment, short& nVal )
{
	if( pExperiment ) pExperiment->get_Type( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::GetMissingDataValue( IExperiment* pExperiment, double& dVal )
{
	if( pExperiment ) pExperiment->get_MissingDataValue( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::ResetToStart()
{
	return Experiments::ResetToStart( m_pExperiment );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::ResetToStart( IExperiment* pExperiment )
{
	if( pExperiment ) return pExperiment->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::GetNext( IExperiment* pExperiment )
{
	if( pExperiment ) return pExperiment->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Find( IExperiment* pExperiment, CString szID )
{
	if( pExperiment ) return pExperiment->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Add( IExperiment* pExperiment )
{
	if( !pExperiment ) return E_FAIL;

	if( FAILED( pExperiment->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Experiments::Add") );

		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Modify( IExperiment* pExperiment )
{
	if( !pExperiment ) return E_FAIL;

	if( FAILED( pExperiment->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Experiments::Remove( IExperiment* pExperiment )
{
	if( !pExperiment ) return E_FAIL;

	if( FAILED( pExperiment->Remove() ) )
	{
		BCGPMessageBox( IDS_EXPDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetDataPath( IExperiment* pExperiment, CString szPath )
{
	if( pExperiment ) pExperiment->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetBackupPath( IExperiment* pExperiment, CString szPath )
{
	if( pExperiment ) pExperiment->SetBackupPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoAdd( CWnd* pOwner )
{
	return Experiments::DoAdd( m_pExperiment, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoAdd( IExperiment* pExperiment, CWnd* pOwner )
{
	if( !pExperiment ) return FALSE;
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
											 IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return FALSE;
	}

	ProcSetSheet d( pPS, pOwner, FALSE, TRUE );
	d.DoModal();
	if( d.m_fModified )
	{
		pPS->put_ExperimentID( d.m_szID.AllocSysString() );
		// create new exp settings object and adjust gripper (input) settings
		// if a gripper-type exp
		if( d.m_nType == EXP_TYPE_GRIPPER )
		{
			pPS->put_SamplingRate( GRIPPER_RATE );
			pPS->put_MinPenPressure( GRIPPER_PRESS );
			pPS->put_DeviceResolution( GRIPPER_RES );
			IProcSetRunExp* pPSRE = NULL;
			pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( pPSRE )
			{
				pPSRE->put_MaxRecArea( TRUE );
				pPSRE->Release();
			}
		}
		hr = pPS->Add();
		if( FAILED( hr ) )
		{
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Experiments::DoAdd") );

			pPS->Release();
			return FALSE;
		}
		pPS->Release();

		pExperiment->put_ID( d.m_szID.AllocSysString() );
		pExperiment->put_Description( d.m_szDesc.AllocSysString() );
		pExperiment->put_Notes( d.m_szNotes.AllocSysString() );
		pExperiment->put_Type( d.m_nType );
		pExperiment->put_MissingDataValue( d.m_dMDV );
		hr = Experiments::Add( pExperiment );
		if( FAILED( hr ) ) return FALSE;

		return TRUE;
	}
	if( pPS ) pPS->Release();

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoEdit( CString szID, CWnd* pOwner )
{
	return Experiments::DoEdit( m_pExperiment, szID, pOwner, 0 );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoEdit( CString szID, CWnd* pOwner, UINT iSelectPage )
{
	return Experiments::DoEdit( m_pExperiment, szID, pOwner, iSelectPage );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoEdit( IExperiment* pExperiment, CString szID, CWnd* pOwner )
{
	if( !pExperiment ) return FALSE;
	if( FAILED( Experiments::Find( pExperiment, szID ) ) ) return FALSE;

	// Create ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
											 IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return FALSE;
	}

	// experiment type
	CString szDesc, szNotes;
	short nType = 0;
	double dMVD = 0.;
	Experiments::GetDescription( pExperiment, szDesc );
	Experiments::GetNotes( pExperiment, szNotes );
	Experiments::GetType( pExperiment, nType );
	Experiments::GetMissingDataValue( pExperiment, dMVD );

	// Attempt to locate existing proc settings
	// ...if failed here, means not found (will add)
	hr = pPS->Find( szID.AllocSysString() );
	if( FAILED( hr ) ) pPS->put_ExperimentID( szID.AllocSysString() );

	ProcSetSheet d( pPS, pOwner, nType == EXP_TYPE_GRIPPER );
	d.m_szID = szID;
	d.m_szDesc = szDesc;
	d.m_szNotes = szNotes;
	d.m_nType = nType;
	d.m_dMDV = dMVD;
	d.DoModal();
	if( d.m_fModified )
	{
		// modify exp settings
		hr = pPS->Modify();
		if( FAILED( hr ) ) BCGPMessageBox( IDS_PSAPPLY_ERR );
		pPS->Release();

		// modify exp
		{
			Experiments exp;
			if( SUCCEEDED( exp.Find( szID ) ) )
			{
				exp.PutDescription( d.m_szDesc );
				exp.PutNotes( d.m_szNotes );
				exp.PutType( d.m_nType );
				exp.PutMissingDataValue( d.m_dMDV );
				hr = exp.Modify();
				if( FAILED( hr ) ) return FALSE;
				pExperiment->put_Description( d.m_szDesc.AllocSysString() );
			}
		}

		CString szData;
		szData.Format( IDS_AL_PSEDITED, szID );
		LogToFile( szData, LOG_UI );

		return TRUE;
	}
	pPS->Release();

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoEdit( IExperiment* pExperiment, CString szID, CWnd* pOwner, UINT iSelectPage )
{
	if( !pExperiment ) return FALSE;
	if( FAILED( Experiments::Find( pExperiment, szID ) ) ) return FALSE;

	// Create ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return FALSE;
	}

	// experiment type
	CString szDesc, szNotes;
	short nType = 0;
	double dMVD = 0.;
	Experiments::GetDescription( pExperiment, szDesc );
	Experiments::GetNotes( pExperiment, szNotes );
	Experiments::GetType( pExperiment, nType );
	Experiments::GetMissingDataValue( pExperiment, dMVD );

	// Attempt to locate existing proc settings
	// ...if failed here, means not found (will add)
	hr = pPS->Find( szID.AllocSysString() );
	if( FAILED( hr ) ) pPS->put_ExperimentID( szID.AllocSysString() );

	ProcSetSheet d( pPS, pOwner, nType == EXP_TYPE_GRIPPER, FALSE, iSelectPage );
	d.m_szID = szID;
	d.m_szDesc = szDesc;
	d.m_szNotes = szNotes;
	d.m_nType = nType;
	d.m_dMDV = dMVD;
	d.DoModal();
	if( d.m_fModified )
	{
		// modify exp settings
		hr = pPS->Modify();
		if( FAILED( hr ) ) BCGPMessageBox( IDS_PSAPPLY_ERR );
		pPS->Release();

		// modify exp
		{
			Experiments exp;
			if( SUCCEEDED( exp.Find( szID ) ) )
			{
				exp.PutDescription( d.m_szDesc );
				exp.PutNotes( d.m_szNotes );
				exp.PutType( d.m_nType );
				exp.PutMissingDataValue( d.m_dMDV );
				hr = exp.Modify();
				if( FAILED( hr ) ) return FALSE;
				pExperiment->put_Description( d.m_szDesc.AllocSysString() );
			}
		}

		CString szData;
		szData.Format( IDS_AL_PSEDITED, szID );
		LogToFile( szData, LOG_UI );

		return TRUE;
	}
	pPS->Release();

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ReportSubjects( CString& szFile )
{
	return Experiments::ReportSubjects( m_pExperiment, szFile );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ReportSubjects( IExperiment* pExp, CString& szFile )
{
	if( !pExp ) return FALSE;

	// Path
	CString szID, szDesc;
	double dExpDevReso = 0.;
	short nType = 0;
	Experiments::GetID( pExp, szID );
	Experiments::GetDescription( pExp, szDesc );
	Experiments::GetType( pExp, nType );
	::GetExpNoExt( szID, szFile );
	// ensure path is created
	int nPos = szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		CString szPath = szFile.Left( nPos );
		_mkdir( szPath );
	}
	szFile += _T("SubjRpt.RPT");

	// exp dev res & samp rate
	double dExpDevRes = 0.;
	short nExpSampRate = 0;
	IProcSetting* pPS = NULL;
	HRESULT hr = E_FAIL;
	BOOL fUserObj = FALSE;
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj && pObj->m_pPS )
	{
		pPS = pObj->m_pPS;
		fUserObj = TRUE;
		hr = S_OK;
	}
	else
	{
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
							   IID_IProcSetting, (LPVOID*)&pPS );
		if( pObj ) pObj->m_pPS = pPS;
	}
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return FALSE;
	}
	pPS->Find( szID.AllocSysString() );	// use default if not found
	pPS->get_DeviceResolution( &dExpDevRes );
	pPS->get_SamplingRate( &nExpSampRate );
	if( !pObj ) pPS->Release();

	// Create list of grp/members
	CString szGrpID, szSubjID, szItem, szDelim;
	CStringList lst;;
	BSTR bstr;
	IExperimentMember* pEM = NULL;
	hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							 IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return FALSE;
	}

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	hr = pEM->FindForExperiment( szID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEM->get_GroupID( &bstr );
		szGrpID = bstr;
		pEM->get_SubjectID( &bstr );
		szSubjID = bstr;

		szItem.Format( _T("%s%s%s"), szGrpID, szDelim, szSubjID );
		lst.AddTail( szItem );

		hr = pEM->GetNext();
	}

	if( lst.GetCount() <= 0 )
	{
		pEM->Release();
		return FALSE;
	}

	// Sort list
	::SortStringListAlpha( &lst );

	// Loop through list...get data...write to file
	CStdioFile f;
	CString szGrp, szSubj, szNotes, szPrvNotes, szDefExp, szConds, szCode, szSiteID, szSite;
	CStringList lstCond;
	COleDateTime dtAdded, dtExpStart, dtExpEnd, dtProcessed;
	COleDateTimeSpan dtsDur;
	double dGroupDur = 0., dTotalDur = 0.;
	BOOL fPrv = ::IsPrivacyOn(), fActive = FALSE, fInSummarize = FALSE;
	short nSampRate = 0, nExpCount = 0, nTrials = 0, nGoodTrials = 0, nBadTrials = 0, nExpTrials = 0;
	double dDevRes = 0.;

	// if privacy is on, warn user
	if( !fPrv )
	{
		CString szMsg;
		szMsg.Format( _T("WARNING: Privacy Protection is OFF.\n\nThis report will contain non-protected private information in:\n%s\n\nDo you wish to continue?"), szFile );
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO )
		{
			pEM->Release();
			return FALSE;
		}
	}

	// open file for writing
	if( !f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Unable to open file for writing.") );
		pEM->Release();
		return FALSE;
	}

	// Progress Meter steps
	int nSteps = (int)lst.GetCount();
	Progress::EnableProgress( nSteps );
	int nCurStep = 0;

	// 1st line = user
	CString szData;
	f.WriteString( _T("User: ") );
	::GetCurrentUser( szData );
	f.WriteString( szData );
	f.WriteString( _T(" - ") );
	::GetCurrentUserDesc( szData );
	f.WriteString( szData );
	f.WriteString( _T("\n") );

	// 2nd line = date
	COleDateTime dt = COleDateTime::GetCurrentTime();
	f.WriteString( dt.Format( _T("Date: %d %b %Y %I:%M %p") ) );
	f.WriteString( _T("\n") );

	// 3rd line = experiment type
	f.WriteString( _T("Exp. Type: ") );
	if( nType == EXP_TYPE_HANDWRITING ) f.WriteString( _T("Handwriting\n") );
	else if( nType == EXP_TYPE_GRIPPER ) f.WriteString( _T("Grip-Force\n") );
	else if( nType == EXP_TYPE_IMAGE ) f.WriteString( _T("Handwriting Image\n") );
	else f.WriteString( _T("UNKNOWN") );

	// 4th line = data location (root for exp)
	f.WriteString( _T("Data: ") );
	::GetDataPathRoot( szData );
	szData += _T("\\");
	szData += szID;
	f.WriteString( szData );
	f.WriteString( _T("\n") );

	// 5th line = privacy
	f.WriteString( _T("Subject Privacy Protection (***): ") );
	f.WriteString( ::IsPrivacyOn() ? _T("ON") : _T("OFF") );
	f.WriteString( _T("\n") );

	// 6th line = samp rate & dev reso of exp.
	szData.Format( _T("Sampling Rate = %d, Device Resolution = %g (Subject settings that differ are shown)"), nExpSampRate, dExpDevRes );
	f.WriteString( szData );
	f.WriteString( _T("\n") );
	f.WriteString( _T("\n") );
	

	// column headers
	f.WriteString( _T("ExpID\tExpDesc\tGrpID\tGrpDesc\tSubjID\tSubjCode\tSubjName\tDateAdded\tSiteID\tSiteDesc\tNotes\tPrivNotes\tExpDate\tExpDur(Min)\tProcDate\tNumExpTrials\tNumTrials\tNumConsTrials\tInconsCond\tDiffSampRate\tDiffDevRes\tInclSum\n\n") );

	Groups grp;
	Subjects subj;
	CString szLastGrp = _T(""), szMsg;
	int nCount = 0, nTotal = 0, nGrpInclCount = 0, nTotalAvgRem = 0, nGrpAvgRem = 0;
	int nTotalInclCount = 0, nTotalGrpGoodTrials = 0, nTotalGrpTrials = 0, nTotalGrpExpTrials = 0;
	POSITION pos = lst.GetHeadPosition();
	while( pos )
	{
		szConds = _T("");
		szItem = lst.GetNext( pos );

		// parse out id's
		szGrpID = _T("");
		szSubjID = _T("");
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Left( nPos );
			if( szLastGrp == _T("") ) szLastGrp = szGrpID;
			szSubjID = szItem.Mid( nPos + 1 );
		}

		// progress
		nCurStep++;
		szMsg.Format( _T("SUBJECT REPORT: Processing subject %s of group %s"), szSubjID, szGrpID );
		Progress::SetProgress( nCurStep, szMsg );

		// if new group...
		if( szLastGrp != szGrpID )
		{
			f.WriteString( _T("\nGROUP SUMMARY\n") );
			// write summary line with count of subjects for group
			szItem.Format( _T("There are %d subjects (%d included) for group %s.\n"),
						   nCount, nGrpInclCount, szLastGrp );
			f.WriteString( szItem );
			// write summary line for average exp duration
			szItem.Format( _T("Average duration of experiment %s for group %s is %.2f minutes for %d subjects.\n"), szID, szLastGrp,
						   ( nGrpInclCount > 0 ) ? dGroupDur / ( nGrpInclCount - nGrpAvgRem ) : 0., nGrpInclCount - nGrpAvgRem );
			f.WriteString( szItem );
			// write summary of trials
			double dPcnt = ( nTotalGrpTrials > 0 ) ? ( 100 * ( 1. - ( 1.0 * nTotalGrpGoodTrials / nTotalGrpTrials ) ) ) : 0.;
			szItem.Format( _T("Total included trials = %d, Consistent Trials = %d, Pcnt. of Inconsistent Trials = %.2f%% for group %s.\n\n\n"),
						   nTotalGrpTrials, nTotalGrpGoodTrials, dPcnt, szLastGrp );
			f.WriteString( szItem );
		}
		// update last group if necessary
		if( szLastGrp != szGrpID )
		{
			szLastGrp = szGrpID;
			nCount = 0;
			nGrpInclCount = 0;
			nGrpAvgRem = 0;
			dGroupDur = 0.;
			nTotalGrpTrials = 0;
			nTotalGrpGoodTrials = 0;
		}

		// group info
		hr = grp.Find( szGrpID );
		if( SUCCEEDED( hr ) )
		{
			grp.GetDescription( szGrp );

			// subject info
			hr = subj.Find( szSubjID );
			if( SUCCEEDED( hr ) )
			{
				if( SUCCEEDED( pEM->Find( szID.AllocSysString(), szGrpID.AllocSysString(), szSubjID.AllocSysString() ) ) )
				{
					pEM->get_DeviceRes( &dDevRes );
					pEM->get_SamplingRate( &nSampRate );
//					if( dDevRes == 0. ) dDevRes = dExpDevRes;
//					if( nSampRate == 0 ) nSampRate = nExpSampRate;
				}

				subj.GetCode( szCode );
				subj.GetName( szSubj, fPrv );
				subj.GetSiteID( szSiteID );
				subj.GetSiteDesc( szSite );
				subj.GetNotes( szNotes );
				::StripNewlinesAndTabs( szNotes );
				subj.GetPrvNotes( szPrvNotes, fPrv );
				::StripNewlinesAndTabs( szPrvNotes );
				subj.GetDateAdded( dtAdded );
				subj.GetActive( fActive );
				subj.GetDefaultExperiment( szDefExp );
				subj.GetExperimentCount( nExpCount );
				Subjects::GetTrialInfo( szID, szGrpID, szSubjID, &lstCond, nTrials,
										nGoodTrials, nBadTrials, dtExpStart, dtExpEnd,
										dtProcessed, fInSummarize, nExpTrials );
				dtsDur = dtExpEnd - dtExpStart;
				if( fInSummarize )
				{
					nGrpInclCount++;
					nTotalInclCount++;
					dGroupDur += dtsDur.GetTotalMinutes();
					dGroupDur += ( 1. * dtsDur.GetSeconds() / 60. );
					dTotalDur += dtsDur.GetTotalMinutes();
					dTotalDur += ( 1. * dtsDur.GetSeconds() / 60. );
					nTotalGrpTrials += nTrials;
					nTotalGrpExpTrials += nExpTrials;
					nTotalGrpGoodTrials += nGoodTrials;
					// remove subject if 0 trials
					if( nTrials == 0 )
					{
						nGrpAvgRem++;
						nTotalAvgRem++;
					}
				}
				POSITION pos = lstCond.GetHeadPosition();
				while( pos )
				{
					szItem = lstCond.GetNext( pos );
					szConds += szItem;
					if( pos ) szConds += _T(",");
				}
				nCount++;
				nTotal++;
			}
		}

		// construct line to write
		if( SUCCEEDED( hr ) )
		{
			CString szExpDate, szProcDate;
// 08May08: GMB: Changed to use default locale info
//			szExpDate = ( dtExpStart != 0 ) ? dtExpStart.Format( _T("%m-%d-%y %H:%M") ) : _T("");
//			szProcDate = ( dtProcessed != 0 ) ? dtProcessed.Format( _T("%m-%d-%y %H:%M") ) : _T("");
			szExpDate = ( dtExpStart != 0 ) ? dtExpStart.Format() : _T("");
			szProcDate = ( dtProcessed != 0 ) ? dtProcessed.Format() : _T("");
			double dDur = 0.;
			if( szExpDate != _T("") )
			{
				dDur = dtsDur.GetTotalMinutes();
				dDur += ( 1. * dtsDur.GetSeconds() / 60. );
			}
			CString szSampRate, szDevRes;
			if( nExpSampRate != nSampRate ) szSampRate.Format( _T("%d"), nSampRate );
			if( dExpDevRes != dDevRes ) szDevRes.Format( _T("%g"), dDevRes );
			szItem.Format( _T("\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\t%.2f\t\"%s\"\t%d\t%d\t%d\t\"%s\"\t\"%s\"\t\"%s\"\t\"%s\"\n"),
						   szID, szDesc, szGrpID, szGrp, szSubjID, szCode, szSubj, dtAdded.Format(),
						   szSiteID, szSite, szNotes, szPrvNotes, szExpDate, dDur, szProcDate,
						   nExpTrials, nTrials, nGoodTrials, szConds, szSampRate, szDevRes,
						   fInSummarize ? _T("YES") : _T("NO") );
			f.WriteString( szItem );
		}
	}
	// write summary line with count of subjects for last group (if applicable)
	if( szGrpID != _T("") )
	{
		f.WriteString( _T("\nGROUP SUMMARY\n") );
		// write summary line with count of subjects for group
		szItem.Format( _T("There are %d subjects (%d included) for group %s.\n"),
					   nCount, nGrpInclCount, szLastGrp );
		f.WriteString( szItem );
		// write summary line for average exp duration
		szItem.Format( _T("Average duration of experiment %s for group %s is %.2f minutes for %d subjects.\n"), szID, szLastGrp,
					   ( nGrpInclCount > 0 ) ? dGroupDur / ( nGrpInclCount - nGrpAvgRem ) : 0., nGrpInclCount - nGrpAvgRem );
		f.WriteString( szItem );
		// write summary of trials
		double dPcnt = ( nTotalGrpTrials > 0 ) ? ( 100 * ( 1. - ( 1.0 * nTotalGrpGoodTrials / nTotalGrpTrials ) ) ) : 0.;
		szItem.Format( _T("Total included trials = %d, Consistent Trials = %d, Pcnt. of Inconsistent Trials = %.2f%% for group %s.\n\n"),
					   nTotalGrpTrials, nTotalGrpGoodTrials, dPcnt, szLastGrp );
		f.WriteString( szItem );
	}
	// EXPERIMENT SUMMARY
	f.WriteString( _T("\nEXPERIMENT SUMMARY\n") );
	// write summary line with count of total subjects
	szItem.Format( _T("There are %d (%d included) subjects for experiment %s.\n"),
				   nTotal, nTotalInclCount, szID );
	f.WriteString( szItem );
	// write summary line with average duration for each subject
	szItem.Format( _T("Average duration of experiment %s is %.2f minutes for %d subjects."), szID,
				   ( nTotalInclCount > 0 ) ? dTotalDur / ( nTotalInclCount - nTotalAvgRem ) : 0., nTotalInclCount - nTotalAvgRem );
	f.WriteString( szItem );

	// we're done with progress meter
	Progress::DisableProgress();

	// close file
	f.Close();

	// release ExpMember object
	pEM->Release();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ReportQuestionnaires( CString& szFile )
{
	return Experiments::ReportQuestionnaires( m_pExperiment, szFile );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ReportQuestionnaires( IExperiment* pExp, CString& szFile )
{
	if( !pExp ) return FALSE;

	// Path
	CString szExpID, szExp;
	short nType = 0;
	Experiments::GetID( pExp, szExpID );
	Experiments::GetDescription( pExp, szExp );
	Experiments::GetType( pExp, nType );
	::GetExpNoExt( szExpID, szFile );
	// ensure path is created
	int nPos = szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		CString szPath = szFile.Left( nPos );
		_mkdir( szPath );
	}
	szFile += _T("QuestRpt.RPT");

	// Create list of grp/members
	CString szGrpID, szSubjID, szCode, szItem, szDelim;
	Subjects subj;
	CStringList lst;
	BSTR bstr;
	IExperimentMember* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return FALSE;
	}

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	hr = pEM->FindForExperiment( szExpID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEM->get_GroupID( &bstr );
		szGrpID = bstr;
		pEM->get_SubjectID( &bstr );
		szSubjID = bstr;

		szItem.Format( _T("%s%s%s"), szGrpID, szDelim, szSubjID );
		lst.AddTail( szItem );

		hr = pEM->GetNext();
	}
	pEM->Release();

	if( lst.GetCount() <= 0 ) return FALSE;

	// Sort list
	::SortStringListAlpha( &lst );

	// Loop through list...get data...write to file
	CStdioFile f;
	CString szGrp, szSubj, szQID, szA, szTemp;
	BOOL fPrv = ::IsPrivacyOn(), fPrivate = FALSE, fHeader = FALSE;
	
	if( !f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Unable to open file for writing.") );
		return FALSE;
	}

	// subject questionnaire
	ISQuestionnaire* pSQ = NULL;
	hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
						   IID_ISQuestionnaire, (LPVOID*)&pSQ );
	if( FAILED( hr ) )
	{
		szTemp.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szTemp );
		return FALSE;
	}
	// master questionnaire
	IMQuestionnaire* pQ = NULL;
	hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
						   IID_IMQuestionnaire, (LPVOID*)&pQ );
	if( FAILED( hr ) )
	{
		szTemp.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szTemp );
		pSQ->Release();
		return FALSE;
	}

	// Get master questions for future reference
	CStringList lstID, lstH, lstP, lstQ, lstI, lstCH;
	BSTR bstrQ = NULL, bstrID = NULL, bstrCH = NULL;
	CString szQ, szID, szH, szI, szP, szCH;
	CStringList Q, ID, H, I, P, CH;
	short npos = 0;

	hr = pQ->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pQ->get_IsHeader( &fHeader );
		pQ->get_IsPrivate( &fPrivate );
		pQ->get_ItemNum( &npos );
		pQ->get_Question( &bstrQ );
		pQ->get_ID( &bstrID );
		pQ->get_ColumnHeader( &bstrCH );

		szQ = bstrQ;
		szID = bstrID;
		szI.Format( _T("%d"), npos );
		szH = fHeader ? _T("TRUE") : _T("");
		szP = fPrivate ? _T("TRUE") : _T("");
		szCH = bstrCH;

		H.AddTail( szH );
		P.AddTail( szP );
		Q.AddTail( szQ );
		ID.AddTail( szID );
		I.AddTail( szI );
		CH.AddTail( szCH );

		hr = pQ->GetNext();
	}

	// Sort
	int nCnt = (int)H.GetCount();
	for( int i = 0; i < nCnt; i++ )
	{
		lstH.AddTail( szTemp );
		lstP.AddTail( szTemp );
		lstQ.AddTail( szTemp );
		lstID.AddTail( szTemp );
		lstI.AddTail( szTemp );
		lstCH.AddTail( szTemp );
	}
	POSITION posH = H.GetHeadPosition();
	POSITION posP = P.GetHeadPosition();
	POSITION posQ = Q.GetHeadPosition();
	POSITION posID = ID.GetHeadPosition();
	POSITION posI = I.GetHeadPosition();
	POSITION posCH = CH.GetHeadPosition();
	POSITION pos = NULL;
	while( posI )
	{
		szI = I.GetNext( posI );
		szH = H.GetNext( posH );
		szP = P.GetNext( posP );
		szQ = Q.GetNext( posQ );
		szID = ID.GetNext( posID );
		szCH = CH.GetNext( posCH );

		nPos = atoi( szI );

		pos = lstI.FindIndex( nPos - 1 );
		if( pos ) lstI.SetAt( pos, szI );
		pos = lstH.FindIndex( nPos - 1 );
		if( pos ) lstH.SetAt( pos, szH );
		pos = lstP.FindIndex( nPos - 1 );
		if( pos ) lstP.SetAt( pos, szP );
		pos = lstQ.FindIndex( nPos - 1 );
		if( pos ) lstQ.SetAt( pos, szQ );
		pos = lstID.FindIndex( nPos - 1 );
		if( pos ) lstID.SetAt( pos, szID );
		pos = lstCH.FindIndex( nPos - 1 );
		if( pos ) lstCH.SetAt( pos, szCH );
	}

	// write header
	// MAIN
	szTemp.Format( _T("QUESTIONNAIRE REPORT FOR EXPERIMENT: %s (%s)\n"), szExp, szExpID );
	f.WriteString( szTemp );
	// 1st line = user
	CString szData;
	f.WriteString( _T("User: ") );
	::GetCurrentUser( szData );
	f.WriteString( szData );
	f.WriteString( _T(" - ") );
	::GetCurrentUserDesc( szData );
	f.WriteString( szData );
	f.WriteString( _T("\n") );
	// 2nd line = date
	COleDateTime dt = COleDateTime::GetCurrentTime();
	f.WriteString( dt.Format( _T("Date: %d %b %Y %I:%M %p") ) );
	f.WriteString( _T("\n") );
	// 3rd line = experiment type
	f.WriteString( _T("Exp. Type: ") );
	if( nType == EXP_TYPE_HANDWRITING ) f.WriteString( _T("Handwriting\n") );
	else if( nType == EXP_TYPE_GRIPPER ) f.WriteString( _T("Grip-Force\n") );
	else if( nType == EXP_TYPE_IMAGE ) f.WriteString( _T("Handwriting Image\n") );
	else f.WriteString( _T("UNKNOWN") );
	// 4th line = data location (root for exp)
	f.WriteString( _T("Data: ") );
	::GetDataPathRoot( szData );
	szData += _T("\\");
	szData += szID;
	f.WriteString( szData );
	f.WriteString( _T("\n") );
	// 5th line = privacy
	f.WriteString( _T("Subject Privacy Protection (***): ") );
	f.WriteString( ::IsPrivacyOn() ? _T("ON") : _T("OFF") );
	f.WriteString( _T("\n\n") );

	f.WriteString( _T("\nSUMMARY\n\n") );

	// write grp/subj info headers
	f.WriteString( _T("GrpID\tSubjID\tSubjCode") );

	// loop through each question of (sorted) master questionnaire and write headers
	posCH = lstCH.GetHeadPosition();
	posH = lstH.GetHeadPosition();
	while( posCH )
	{
		szCH = lstCH.GetNext( posCH );
		szH = lstH.GetNext( posH );
		fHeader = szH != _T("");
		if( !fHeader )
		{
			f.WriteString( _T("\t") );
			f.WriteString( szCH );
		}
	}
	f.WriteString( _T("\n") );

	// SUMMARY
	pos = lst.GetHeadPosition();
	while( pos )
	{
		szItem = lst.GetNext( pos );

		// parse out id's
		szGrpID = _T("");
		szSubjID = _T("");
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Left( nPos );
			szSubjID = szItem.Mid( nPos + 1 );
		}

		if( ( szGrpID == _T("___") ) || ( szSubjID == _T("___") ) ) continue;

		// grp & subj IDs/Codes
		if( SUCCEEDED( subj.Find( szSubjID ) ) ) subj.GetCode( szCode );
		else szCode = _T("");
		szTemp.Format( _T("%s\t%s\t%s"), szGrpID, szSubjID, szCode );
		f.WriteString( szTemp );

		// loop through each question of (sorted) master questionnaire and search for
		// answers in subject questionnaire
		posH = lstH.GetHeadPosition();
		posP = lstP.GetHeadPosition();
		posID = lstID.GetHeadPosition();
		while( posH )
		{
			szH = lstH.GetNext( posH );
			szP = lstP.GetNext( posP );
			szID = lstID.GetNext( posID );
			fHeader = szH != _T("");
			fPrivate = szP != _T("");

			// now search in subject questionnaire
			HRESULT hr2 = pSQ->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
									 szSubjID.AllocSysString(), szID.AllocSysString() );
			if( !fHeader && SUCCEEDED( hr2 ) )
			{
				if( fPrivate && fPrv )
				{
					szA = PVT_ITEM;
				}
				else
				{
					pSQ->get_Answer( &bstr );
					szA = bstr;
				}
				f.WriteString( _T("\t") );
				f.WriteString( szA );
			}
			else if( !fHeader )
			{
				f.WriteString( _T("\t") );
				f.WriteString( _T(" ") );
			}
		}
		f.WriteString( _T("\n") );
	}

	f.WriteString( _T("\n\nDETAILS\n") );

	// DETAILED
	Groups grp;
	pos = lst.GetHeadPosition();
	while( pos )
	{
		szItem = lst.GetNext( pos );

		// parse out id's
		szGrpID = _T("");
		szSubjID = _T("");
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Left( nPos );
			szSubjID = szItem.Mid( nPos + 1 );
		}

		// group info
		hr = grp.Find( szGrpID );
		if( SUCCEEDED( hr ) )
		{
			grp.GetDescription( szGrp );

			hr = subj.Find( szSubjID );
			if( SUCCEEDED( hr ) )
			{
				subj.GetName( szSubj, fPrv );
				subj.GetCode( szCode );
			}
			else szCode = _T("");

			if( ( szGrpID == _T("___") ) || ( szSubjID == _T("___") ) ) hr = E_FAIL;
		}

		// loop through each question of (sorted) master questionnaire and search for
		// answers in subject questionnaire
		posH = lstH.GetHeadPosition();
		posP = lstP.GetHeadPosition();
		posQ = lstQ.GetHeadPosition();
		posID = lstID.GetHeadPosition();
		posCH = lstCH.GetHeadPosition();
		int nQuest = 0;
		while( posH && SUCCEEDED( hr ) )
		{
			nQuest++;
			szH = lstH.GetNext( posH );
			szP = lstP.GetNext( posP );
			szQ = lstQ.GetNext( posQ );
			szID = lstID.GetNext( posID );
			fHeader = szH != _T("");
			fPrivate = szP != _T("");
			szCH = lstCH.GetNext( posCH );

			// now search in subject questionnaire
            HRESULT hr2 = pSQ->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
									 szSubjID.AllocSysString(), szID.AllocSysString() );
			if( SUCCEEDED( hr2 ) )
			{
				// group/subject header
				if( ( nQuest == 1 ) && SUCCEEDED( hr ) )
				{
					szTemp.Format( _T("\nQuestionnaire for Subject (%s)%s:%s of group %s (%s)\n"),
								szCode, szSubj, szSubjID, szGrp, szGrpID );
					f.WriteString( szTemp );
					f.WriteString( _T("-----------------------------------------------------------------------\n") );
					f.WriteString( _T("ID\tQUESTION\tANSWER\n") );
				}

				pSQ->get_Answer( &bstr );
				szA = bstr;

				// construct line to write
				if( !fHeader )
				{
					szTemp = _T("     ");
					szTemp += szQ;
					szQ = szTemp;
				}
				if( fPrivate && fPrv && !fHeader ) szA = PVT_ITEM;
				szItem.Format( _T("%s\t%s\t%s\n"), szID, szQ, szA );
				f.WriteString( szItem );
			}
		}

	}
	f.Close();

	pQ->Release();
	pSQ->Release();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::Export( CMemFile* pFile )
{
	if( !m_pExperiment || !pFile ) return FALSE;

	CString szItem;

	WRITE_STRING( pFile, TAG_EXPERIMENT );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	Experiments::GetID( m_pExperiment, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Desc
	Experiments::GetDescription( m_pExperiment, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Notes
	Experiments::GetNotes( m_pExperiment, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Type
	short nVal;
	Experiments::GetType( nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Missing Data Value
	double dVal = 0.;
	Experiments::GetMissingDataValue( dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::Import( CStdioFile* pFile, CString& szExpID, BOOL& fOWAllYes,
						  BOOL& fOWAllNo, BOOL fScan )
{
	if( !m_pExperiment || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	Experiments comp;
	BOOL fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	szExpID = szLine;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );
	// Notes
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	char charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
	if( charEnd == '\r' )
	{
		szLine = szLine.Left( szLine.GetLength() - 1 );
		szLine += _T(" ");
	}
	szTemp = szLine;
	while( charEnd == '\r' )
	{
		fRet = READ_STRING( pFile, szLine );
		if( !fRet ) return FALSE;
		charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
		if( charEnd == '\r' )
		{
			szLine = szLine.Left( szLine.GetLength() - 1 );
			szLine += _T(" ");;
		}
		szTemp += szLine;
	}
	PutNotes( szTemp );
	comp.PutNotes( szTemp );
	// Type
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	short nVal = atoi( szLine );
	PutType( nVal );
	comp.PutType( nVal );

	if( ( nVal == EXP_TYPE_GRIPPER ) && !::IsGA() )
	{
		if( BCGPMessageBox( _T("You are attempting to import a gripper experiment. GripAlyzeR is not activated on this system. Would you like to purchase an activation?"), MB_YESNO ) == IDNO )
			return FALSE;
		else
		{
			NSSecurity* pSecurity = ::GetSecurity();
			if( pSecurity && !pSecurity->AG( FALSE ) ) return FALSE;
		}
	}
	else if( ( nVal == EXP_TYPE_HANDWRITING ) && !::IsSA() && !::IsOA() )
	{
		if( BCGPMessageBox( _T("You are attempting to import a gripper experiment. OPTIlyzeR nor ScriptAlyzeR are activated on this system. Would you like to purchase an activation?"), MB_YESNO ) == IDNO )
			return FALSE;
		else
		{
			NSSecurity* pSecurity = ::GetSecurity();
			if( pSecurity && !pSecurity->AO( FALSE ) ) return FALSE;
		}
	}

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_EXPERIMENT, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		try
		{
			CFileFind ff;
			if( ff.FindFile( szFile1 ) ) REMOVE_FILE( szFile1 );
			if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
		}
		catch( ... ) {}
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			comp.GetNotes( szLine );
			PutNotes( szLine );
			comp.GetType( nVal );
			PutType( nVal );
			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;
	else return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ForceLocal()
{
	if( !m_pExperiment ) return;

	HRESULT hr = m_pExperiment->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ForceRemote()
{
	if( !m_pExperiment ) return;

	HRESULT hr = m_pExperiment->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ForceDefault()
{
	if( !m_pExperiment ) return;

	HRESULT hr = m_pExperiment->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, BOOL fRecurse, CString szUserPath )
{
	CString szItem;
	short nVal = 0;
	BOOL fRet = FALSE;
	BOOL fVal;

	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		/////////////////////////////////////////////////////////////////////////////
		// EXPERIMENT
		/////////////////////////////////////////////////////////////////////////////

		// search object
		Experiments dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );
		
		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		/////////////////////////////////////////////////////////////////////////////
		// EXPERIMENT SETTINGS
		/////////////////////////////////////////////////////////////////////////////

		// search/destination object
		IProcSetting* pPS = NULL;
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								IID_IProcSetting, (LPVOID*)&pPS );

		// locate item (destination)
		if( fToLocal )
		{
			pPS->ForceLocal();
			pPS->SetDataPath( szUserPath.AllocSysString() );
		}
		else pPS->ForceRemote();
		hr = pPS->Find( szID.AllocSysString() );

		// now reset back to default mode and locate
		if( !fToLocal )
		{
			pPS->ForceLocal();
			::GetDataPathRoot( szUserPath );
			pPS->SetDataPath( szUserPath.AllocSysString() );
		}
		else pPS->ForceRemote();
		pPS->Find( szID.AllocSysString() );

		// if not found, use defaults

		// now reset to destination mode (confusing i know lewl)
		if( fToLocal )
		{
			pPS->ForceLocal();
			pPS->SetDataPath( szUserPath.AllocSysString() );
		}
		else pPS->ForceRemote();

		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = pPS->Modify();
		else if( FAILED( hr ) )
			hr = pPS->Add();

		// return operation mode to normal
		pPS->ForceDefault();

		pPS->Release();
		fRet = TRUE;
	}

	// include sub-items?
	if( fRecurse )
	{
		Groups grp;
		Subjects subj;
		NSConditions cond;
		BSTR bstr;
		CString szGrp, szSubj, szCond, szExpID;
		CStringList lstGrp, lstSubj, lstCond, lstReps, lstDidGrp, lstDidSubj;
		BOOL* pExists = NULL;
		int nCount = 0, nItem;

		/////////////////////////////////////////////////////////////////////////////
		// EXPERIMENT MEMBERS
		/////////////////////////////////////////////////////////////////////////////

		// Experiment member object
		IExperimentMember *pEM = NULL;
		{
			hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEM );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Unable to create ExperimentMember object.") );
				return FALSE;
			}
		}
		
		// find list of members
		hr = pEM->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			// get info
			pEM->get_ExperimentID( &bstr );
			szExpID = bstr;
			pEM->get_GroupID( &bstr );
			szGrp = bstr;
			pEM->get_SubjectID( &bstr );
			szSubj = bstr;

			// we can end up having multiple additions of the same group or subj
			// we'll check that later
			if( szExpID == szID )
			{
				lstGrp.AddTail( szGrp );
				lstSubj.AddTail( szSubj );
			}

			hr = pEM->GetNext();
		}

		// loop through and xfer the groups & subjects
		// track which ones have been done to avoid duplicates
		POSITION posG = lstGrp.GetHeadPosition();
		while( posG )
		{
			szGrp = lstGrp.GetNext( posG );
			if( !lstDidGrp.Find( szGrp ) )
			{
				if( fToLocal ) grp.DoCopy( szGrp, szUserPath, fOverwrite );
				else grp.DoUpload( szGrp, fOverwrite );
				lstDidGrp.AddTail( szGrp );
			}
		}
		POSITION posS = lstSubj.GetHeadPosition();
		while( posS )
		{
			szSubj = lstSubj.GetNext( posS );
			if( !lstDidSubj.Find( szSubj ) )
			{
				if( fToLocal ) subj.DoCopy( szSubj, szUserPath, fOverwrite );
				else subj.DoUpload( szSubj, fOverwrite );
				lstDidSubj.AddTail( szSubj );
			}
		}

		// create exists list (hack) based upon # of items
		nCount = (int)lstGrp.GetCount();
		if( nCount > 1 )
			pExists = new BOOL[ nCount ];
		else if( nCount == 1 )
			pExists = &fVal;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			pEM->ForceLocal();
			pEM->SetDataPath( szUserPath.AllocSysString() );
		}
		else pEM->ForceRemote();

		// now loop through to create our hack exist list
		nItem = 0;
		posG = lstGrp.GetHeadPosition();
		posS = lstSubj.GetHeadPosition();
		while( posG )
		{
			szGrp = lstGrp.GetNext( posG );
			szSubj = lstSubj.GetNext( posS );

			// find relationship (if exists)
			hr = pEM->Find( szID.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );

			if( nCount > 1 ) pExists[ nItem++ ] = SUCCEEDED( hr );
			else *pExists = SUCCEEDED( hr );
		}

		// now loop through and transfer
		nItem = 0;
		posG = lstGrp.GetHeadPosition();
		posS = lstSubj.GetHeadPosition();
		while( posG )
		{
			szGrp = lstGrp.GetNext( posG );
			szSubj = lstSubj.GetNext( posS );

			// assigns vals
			pEM->put_ExperimentID( szID.AllocSysString() );
			pEM->put_GroupID( szGrp.AllocSysString() );
			pEM->put_SubjectID( szSubj.AllocSysString() );

			// check exists list
			if( nCount > 1 ) hr = pExists[ nItem++ ] ? S_OK : E_FAIL;
			else hr = *pExists ? S_OK : E_FAIL;

			// add if does not exist (do nothing if it exists)
			if( FAILED( hr ) ) hr = pEM->Add();

			if( FAILED( hr ) )
			{
				CString szMsg;
				szMsg.Format( _T("Error transferring ExperimentMember for experiment %s, group %s and subject %s."), szID, szGrp, szSubj );
				OutputAMessage( szMsg, TRUE );
			}
		}

		// reset operation mode
		pEM->ForceDefault();

		// cleanup
		pEM->Release();
		if( pExists && ( nCount > 1 ) ) delete [] pExists;


		/////////////////////////////////////////////////////////////////////////////
		// EXPERIMENT NSConditions
		/////////////////////////////////////////////////////////////////////////////

		// Experiment member object
		IExperimentCondition *pEC = NULL;
		{
			hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
									 IID_IExperimentCondition, (LPVOID*)&pEC );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Unable to create ExperimentCondition object.") );
				return FALSE;
			}
		}
		
		// find list of members
		hr = pEC->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			// get info
			pEC->get_ExperimentID( &bstr );
			szExpID = bstr;
			pEC->get_ConditionID( &bstr );
			szCond = bstr;
			pEC->get_Replications( &nVal );
			szItem.Format( _T("%d"), nVal );

			if( szExpID == szID )
			{
				lstCond.AddTail( szCond );
				lstReps.AddTail( szItem );
			}

			hr = pEC->GetNext();
		}

		// loop through and xfer the NSConditions
		POSITION posC = lstCond.GetHeadPosition();
		while( posC )
		{
			szCond = lstCond.GetNext( posC );
			if( fToLocal ) cond.DoCopy( szCond, szUserPath, fOverwrite );
			else cond.DoUpload( szCond, fOverwrite );
		}

		// create exists list (hack) based upon # of items
		nCount = (int)lstCond.GetCount();
		if( nCount > 1 )
			pExists = new BOOL[ nCount ];
		else if( nCount == 1 )
			pExists = &fVal;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			pEC->ForceLocal();
			pEC->SetDataPath( szUserPath.AllocSysString() );
		}
		else pEC->ForceRemote();

		// now loop through to create our hack exist list
		nItem = 0;
		posC = lstCond.GetHeadPosition();
		while( posC )
		{
			szCond = lstCond.GetNext( posC );

			// find relationship (if exists)
			hr = pEC->Find( szID.AllocSysString(), szCond.AllocSysString() );

			if( nCount > 1 ) pExists[ nItem++ ] = SUCCEEDED( hr );
			else *pExists = SUCCEEDED( hr );
		}

		// now loop through and transfer
		nItem = 0;
		posC = lstCond.GetHeadPosition();
		POSITION posR = lstReps.GetHeadPosition();
		while( posC )
		{
			szCond = lstCond.GetNext( posC );
			szItem = lstReps.GetNext( posR );
			nVal = atoi( szItem );

			// assigns vals
			pEC->put_ExperimentID( szID.AllocSysString() );
			pEC->put_ConditionID( szCond.AllocSysString() );
			pEC->put_Replications( nVal );

			// check exists list
			if( nCount > 1 ) hr = pExists[ nItem++ ] ? S_OK : E_FAIL;
			else hr = *pExists ? S_OK : E_FAIL;

			// add if does not exist (do nothing if it exists)
			if( FAILED( hr ) ) hr = pEC->Add();

			if( FAILED( hr ) )
			{
				CString szMsg;
				szMsg.Format( _T("Error transferring ExperimentCondition for experiment %s, group %s and subject %s."), szID, szGrp, szSubj );
				OutputAMessage( szMsg, TRUE );
			}
		}

		// reset operation mode
		pEC->ForceDefault();

		// cleanup
		pEC->Release();
		if( pExists && ( nCount > 1 ) ) delete [] pExists;
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, FALSE, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoCopyAll( CString szID, CString szPath, BOOL fOverwrite, BOOL fIncludeElements )
{
	return DoTransfer( TRUE, szID, fOverwrite, fIncludeElements, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite, FALSE );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoUploadAll( CString szID, BOOL fOverwrite, BOOL fIncludeElements )
{
	return DoTransfer( FALSE, szID, fOverwrite, fIncludeElements );
}

///////////////////////////////////////////////////////////////////////////////
// Write sequence file
void Experiments::WriteTrialSequence( CStdioFile* pFile, CStringList* pList )
{
	if( !pFile || !pList ) return;

	// Write condition items w/sequence
	CMapStringToString	m_listCondReps;
	CString szItem, szCond, szRep;
	int nTotal = 0, nRep = 0;
	POSITION pos = pList->GetHeadPosition();
	while( pos )
	{
		nTotal++;
		szCond = pList->GetNext( pos );

		nRep = 1;
		if( m_listCondReps.Lookup( szCond, szRep ) ) nRep += atoi( szRep );
		szRep.Format( _T("%d"), nRep );
		m_listCondReps[ szCond ] = szRep;

		szItem.Format( _T("Trial#: %d --- Condition: %s --- Replication: %d\n"),
					   nTotal, szCond, nRep );
		pFile->WriteString( szItem );
	}
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Run-Experiment
///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::RunExperiment( CString szExpID, CString szExp, CString szGrpID,
								 CString szGrp, CString szSubjID, CString szSubj,
								 LPVOID param )
{
	UserObj* pObj = ::GetCurrentUserObj();

	// log running experiment
	CString szData;
	szData.LoadString( IDS_AL_RUNEXP );
	LogToFile( szData, LOG_UI );

	// delimiter
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Is input device and experiment type appropriate
	BOOL fGripper = FALSE;
	{
	Experiments exp;
	if( !exp.IsValid() ) return FALSE;
	if( FAILED( exp.Find( szExpID ) ) )
	{
		OutputMessage( _T("ERROR: Run Experiment: Cannot locate experiment in database."), LOG_UI );
		return FALSE;
	}
	short nType = 0;
	exp.GetType( nType );
	if( nType == EXP_TYPE_IMAGE )
	{
		BCGPMessageBox( _T("ERROR: Run Experiment: This is an image experiment. Image experiments cannot be run for subjects.") );
		return FALSE;
	}
	else if( nType == EXP_TYPE_HANDWRITING )
	{
		if( ::IsGripperModeOn() )
		{
			BCGPMessageBox( _T("ERROR: Run Experiment: This is a Handwriting experiment. Please select the appropriate input device in Settings.") );
			return FALSE;
		}
	}
	else if( nType == EXP_TYPE_GRIPPER )
	{
		fGripper = TRUE;
		if( !::IsGripperModeOn() )
		{
			BCGPMessageBox( _T("ERROR: Run Experiment: This is a Grip-Force experiment. Please select the appropriate input device in Settings.") );
			return FALSE;
		}
	}

	// missing data value
	double dMDV = -1.e6;
	exp.GetMissingDataValue( dMDV );
	::SetMissingDataValue( dMDV );
	}

	// if gripper experiment, ensure NiMax is not running
	if( fGripper && ::IsProcessRunning( _T("NIMax.exe") ) )
	{
		BCGPMessageBox(_T("ERROR: Run Experiment: You cannot run an experiment while National Instruments Measurements and Automation Explorer (NiMax.exe) is running.") );
		return FALSE;
	}

	// If tablet installed and mouse selected, ask to continue
	if( ::IsTabletInstalled() && !::IsTabletModeOn() && !::IsGripperModeOn() )
	{
		if( BCGPMessageBox( _T("Tablet is installed but mouse is selected. Continue anyway?"), MB_YESNO ) == IDNO )
			return FALSE;
	}

	// Process Settings
	IProcSetting* pPS = NULL;
	HRESULT hr = E_FAIL;
	BOOL fUserObj = FALSE;
	if( pObj && pObj->m_pPS )
	{
		pPS = pObj->m_pPS;
		hr = S_OK;
		fUserObj = TRUE;
	}
	else
	{
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
							   IID_IProcSetting, (LPVOID*)&pPS );
		if( pObj ) pObj->m_pPS = pPS;
	}
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return FALSE;
	}
	pPS->Find( szExpID.AllocSysString() );
	// Use default if not found

	// NSConditions
	// Get the list first (bug/hack fix 2/4/03)
	// ... for some reason (unknown) using NSConditions object causes
	// ... condition list to not go from record to record properly
	CStringList lstConds;
	if( !Experiments::GetListOfConditions( szExpID, &lstConds ) )
	{
		pPS->Release();
		if( pObj ) pObj->m_pPS = NULL;
		return FALSE;
	}

	// Now go through each one and get condition info
	CStringList* lstConditions = new CStringList;
	CString szStim;
	Stimuluss stim;
	BOOL fRand = FALSE, fRandT = FALSE, fRandRules = FALSE, fSen2Word = FALSE, fSpecify;
	BSTR bstr = NULL;
	CString szRules, szBlocks, szSels, szSelTrials, szTmp, szCondID, szSequence;
	int nPos = 0, nReps = 0, nRecCond = 0;
	pPS->get_SpecifySequence( &fSpecify );
	pPS->get_Randomize( &fRand );
	pPS->get_RandomizeTrials( &fRandT );
	pPS->get_RandWithRules( &fRandRules );
 	pPS->get_RandRules( &bstr );
 	szRules = bstr;
	pPS->get_RandBlockCounts( &bstr );
	szBlocks = bstr;
	pPS->get_RandSelectionChar( &bstr );
	szSels = bstr;
	pPS->get_RandSelectionTrials( &bstr );
	szSelTrials = bstr;
	pPS->get_ConditionSequence( &bstr );
	szSequence = bstr;
	if( !fSpecify && !fRand && !fRandRules )
	{
		BOOL fRecord = FALSE, fProcess = TRUE;
		CString szItem;
		POSITION pos = lstConds.GetHeadPosition();
		while( pos )
		{
			szTmp = lstConds.GetNext( pos );
			nPos = szTmp.Find( szDelim );
			szCondID = szTmp.Left( nPos );
			nReps = atoi( szTmp.Mid( nPos + 1 ) );

			NSConditions cond;
			hr = cond.Find( szCondID.AllocSysString() );
			if( SUCCEEDED( hr ) )
			{
				// sen2word info
				cond.GetRecord( fRecord );
				if( fRecord ) nRecCond++;
				cond.GetProcess( fProcess );
				if( !fRecord && !fProcess )
				{
					szTmp.Format( _T("Error with condition: %s.\nThe condition is set to both NOT record and NOT process or extract."), szCondID );
					BCGPMessageBox( szTmp, MB_OK | MB_ICONERROR );
					delete lstConditions;
					if( !fUserObj ) pPS->Release();
					return FALSE;
				}
				if( !fGripper && !fRecord && fProcess ) fSen2Word = TRUE;

				// stimuli
				cond.GetStimulusWarning( szStim );
				if( ( szStim != "" ) && !stim.Find( szStim ) && !stim.GenerateFiles() )
				{
					BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
					delete lstConditions;
					if( !fUserObj ) pPS->Release();
					return FALSE;
				}
				cond.GetStimulusPrecue( szStim );
				if( ( szStim != "" ) && !stim.Find( szStim ) && !stim.GenerateFiles() )
				{
					BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
					delete lstConditions;
					if( fUserObj ) pPS->Release();
					return FALSE;
				}
				cond.GetStimulus( szStim );
				if( ( szStim != "" ) && !stim.Find( szStim ) && !stim.GenerateFiles() )
				{
					BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
					delete lstConditions;
					if( fUserObj ) pPS->Release();
					return FALSE;
				}
			}

			if( fGripper || fRecord )
			{
				szItem.Format( _T("%s"), szCondID );
				for( short i = 0; i < nReps; i++ )
					lstConditions->AddTail( szItem );
			}
		}

		// if there are no conditions set to record, we cannot run experiment
		if( nRecCond == 0 )
		{
			BCGPMessageBox( _T("The experiment has no recordable conditions.\nThe experiment cannot continue.\n\nYou must set at least one condition as recordable.") );
			delete lstConditions;
			if( fUserObj ) pPS->Release();
			return FALSE;
		}

		// We will write the results to the subject folder
		CStdioFile fOut;
		CString szTemp;
		// verify directory exists
		::VerifyDirectory( szExpID, szGrpID, szSubjID );
		// we'll use the ERR file to avoid writing more code
		::GetERR( szExpID, szGrpID, szSubjID, _T(""), szTemp );
		// remove the extension and add "SEQ"
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T("SEQ");
		if( !fOut.Open( szTemp, CFile::modeWrite | CFile::modeCreate ) )
		{
			BCGPMessageBox( _T("ERROR: Unable to open sequence file for writing.") );
			delete lstConditions;
			if( !fUserObj ) pPS->Release();
			return FALSE;
		}
		// write header
		fOut.WriteString( _T("// TRIAL SEQUENCE:\n//\n") );
		// write trial sequence
		Experiments::WriteTrialSequence( &fOut, lstConditions );
	}
	else
	{
		if( fSpecify )
		{
			if( !Experiments::SequenceTrials( lstConditions, szSequence, fSen2Word,
											  szExpID, szGrpID, szSubjID ) )
				return FALSE;
		}
		else if( fRandRules )
		{
			CString szFile;
			if( !Experiments::RandomizeWithRules( &lstConds, lstConditions, szRules, szBlocks,
				szSels, szSelTrials, fSen2Word, szExpID, szGrpID, szSubjID, TRUE, szFile ) )
				return FALSE;
		}
		else
		{
			if( !Experiments::Randomize( &lstConds, lstConditions, fRandT, fSen2Word,
										 szExpID, szGrpID, szSubjID ) )
				return FALSE;
		}
	}

	// update user object as necessary
	if( pObj )
	{
		if( !pObj->PrepareForExperiment( lstConditions ) )
		{
			delete lstConditions;
			return FALSE;
		}
		pObj->m_pTRI->fSen2Word = fSen2Word;
	}

	MessageManager* pMM = MessageManager::GetMessageManager();
	pMM->PostMessage( MAKELPARAM( MSG_RUN_EXPERIMENT, MSG_SUB_RUN_EXPERIMENT ), (WPARAM)lstConditions );

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////

BOOL Experiments::GetListOfConditions( CString szExp, CStringList* pList )
{
	if( !pList ) return FALSE;

	// empty first (just in case)
	pList->RemoveAll();

	// delimiter
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// NSConditions exist?
	IExperimentCondition* pECL = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
								   IID_IExperimentCondition, (LPVOID*)&pECL );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return FALSE;
	}
	hr = pECL->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOCONDS );
		pECL->Release();
		return FALSE;
	}

	// NSConditions
	CString szCondID, szTmp;
	BSTR bstrCondID = NULL;
	short nReps = 0;
	while( SUCCEEDED( hr ) )
	{
		pECL->get_ConditionID( &bstrCondID );
		szCondID = bstrCondID;
		pECL->get_Replications( &nReps );

		szTmp.Format( _T("%s%s%d"), szCondID, szDelim, nReps );
		pList->AddTail( szTmp );

		hr = pECL->GetNext();
	}

	// release experiment condition object
	pECL->Release();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::SequenceTrials( CStringList *pList, CString szSequence, BOOL& fSen2Word,
								  CString szExp, CString szGrp, CString szSubj )
{
	// variables
	CString		szSeq;					// temporary file for parsing sequence
	CString		szCondID;				// condition ID
	CString		szCond;					// condition description
	CString		szDelim;				// delimiter
	CString		szItem;					// working item
	CString		szStim;					// stimulus
	short		nRecCond	= 0;		// # of conditions for recording
	short		nCondSeq	= 0;		// # of conditions in sequence
	short		nCondAll	= 0;		// # of conditions SHOULD be
	CStringList lstConds;				// list for obtaining list of all conditions
	int			nPos		= 0;		// used for parsing strings
	int			nReps		= 0;		// # of reps for a condition
	BOOL		fRecord		= FALSE;	// condition records?
	BOOL		fProcess	= TRUE;		// condition processes?
	HRESULT		hr			= S_OK;		// COM return value
	Stimuluss	stim;					// stimulus object
	NSConditions	cond;				// condition object

	// verify pointers
	if( !pList ) return FALSE;

	// get field delimiter (& trim)
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// create condition object and validate
	if( !cond.IsValid() )
	{
		OutputMessage( _T("ERROR: Condition Sequencing: Invalid condition object."), LOG_UI );
		return FALSE;
	}

	// Loop through sequence string to construct list of conditions
	szSeq = szSequence;
	while( szSeq != _T("") )
	{
		szCondID = szSeq.Left( 3 );
		hr = cond.Find( szCondID );
		if( SUCCEEDED( hr ) )
		{
			nCondSeq++;
			cond.GetDescription( szCond );
			cond.GetRecord( fRecord );
			if( fRecord ) nRecCond++;
			cond.GetProcess( fProcess );
			if( !fRecord && fProcess ) fSen2Word = TRUE;
			cond.GetStimulusWarning( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
			cond.GetStimulusPrecue( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
			cond.GetStimulus( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
		}

		if( fRecord ) pList->AddTail( szCondID );

		szSeq = szSeq.Mid( 3 );
	}

	// if there are no conditions set to record, we cannot run experiment
	if( nRecCond == 0 )
	{
		BCGPMessageBox( _T("The experiment has no recordable conditions or the condition sequence was not set.\nThe experiment cannot continue.\n\nYou must set at least one condition as recordable and set the sequence in the experiment properties.") );
		delete pList;
		return FALSE;
	}

	// count # of conditions there SHOULD be and compare to what we got from sequence
	Experiments::GetListOfConditions( szExp, &lstConds );
	POSITION pos = lstConds.GetHeadPosition();
	while( pos )
	{
		// get values already set in list
		szCond = lstConds.GetNext( pos );
		nPos = szCond.Find( szDelim );
		szCondID = szCond.Left( nPos );
		nReps = atoi( szCond.Mid( nPos + 1 ) );
		nCondAll += nReps;
	}
	// If the #s don't match inform
	if( nCondSeq != nCondAll )
	{
		szItem.Format( _T("The list of manually sequenced conditions (%d trials) does not\nmatch the number of trials based upon the conditions of the experiment (%d).\n\nDo you wish to continue?"), nCondSeq, nCondAll );
		if( BCGPMessageBox( szItem, MB_YESNO | MB_ICONWARNING ) == IDNO )
		{
			delete pList;
			return FALSE;
		}
	}

	// We will write the results to the subject folder if group, subject
	// and experiment IDs are specified: Test to make sure it opens
	CStdioFile	fOut;
	CString szTemp;
	if( ( szExp != _T("") ) && ( szGrp != _T("") ) && ( szSubj != _T("") ) )
	{
		// verify directory exists
		::VerifyDirectory( szExp, szGrp, szSubj );
		// we'll use the ERR file to avoid writing more code
		::GetERR( szExp, szGrp, szSubj, _T(""), szTemp );
		// remove the extension and add "SEQ"
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T("SEQ");
		if( !fOut.Open( szTemp, CFile::modeWrite | CFile::modeCreate ) )
		{
			BCGPMessageBox( _T("ERROR: Unable to open sequence file for writing.") );
			return FALSE;
		}
		// write header
		fOut.WriteString( _T("// TRIAL SEQUENCE RANDOMLY GENERATED:\n//\n") );
		// write trial sequence
		Experiments::WriteTrialSequence( &fOut, pList );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::Randomize( CStringList *pLstConds, CStringList *pList, BOOL fTrials,
							BOOL& fSen2Word, CString szExp, CString szGrp, CString szSubj )
{
	// variables
	CString		szCondID;				// condition ID
	CString		szCond;					// condition description
	CString		szDelim;				// delimiter
	CString		szItem;					// working item
	CString		szStim;					// stimulus
	CStringList	lstSrc;					// working source list
	CStringList	lstDest;				// working destination list
	short		nRecCond	= 0;		// # of conditions for recording
	short		nReps		= 0;		// condition reps
	int			i			= 0;		// loop index
	short		nPos		= 0;		// position var (strings)
	BOOL		fRecord		= FALSE;	// condition records?
	BOOL		fProcess	= TRUE;		// condition processes?
	POSITION	pos			= NULL;		// list position
	HRESULT		hr			= S_OK;		// COM return value
	Stimuluss	stim;					// stimulus object
	NSConditions	cond;				// condition object

	// verify pointers
	if( !pList || !pLstConds ) return FALSE;

	// get field delimiter (& trim)
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// create condition object and validate
	if( !cond.IsValid() )
	{
		OutputMessage( _T("ERROR: Randomize: Invalid condition object."), LOG_UI );
		return FALSE;
	}

	// First fill list with all NSConditions (one for each rep as well)
	pos = pLstConds->GetHeadPosition();
	while( pos )
	{
		// get values already set in list
		szCond = pLstConds->GetNext( pos );
		nPos = szCond.Find( szDelim );
		szCondID = szCond.Left( nPos );
		nReps = atoi( szCond.Mid( nPos + 1 ) );
		// locate condition in db to get other properties
		hr = cond.Find( szCondID.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			cond.GetRecord( fRecord );
			if( fRecord ) nRecCond++;
			cond.GetProcess( fProcess );
			if( !fRecord && fProcess ) fSen2Word = TRUE;
			cond.GetStimulusWarning( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
			cond.GetStimulusPrecue( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
			cond.GetStimulus( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
		}

		if( fRecord && fTrials )
		{
			for( i = 0; i < nReps; i++ )
			{
				szItem.Format( _T("%s"), szCondID );
				lstSrc.AddTail( szItem );
			}
		}
		else if( fRecord )
		{
			szItem.Format( _T("%s!%d"), szCondID, nReps );
			lstSrc.AddTail( szItem );
		}
	}

	// if there are no conditions set to record, we cannot run experiment
	if( nRecCond == 0 )
	{
		BCGPMessageBox( _T("The experiment has no recordable conditions.\nThe experiment cannot continue.\n\nYou must set at least one condition as recordable.") );
		delete pList;
		return FALSE;
	}

	// now randomly place into destination
// 24Jul08: GMB: New randomization technology to improve randomness
//	time_t t;
//	srand( (unsigned)time( &t ) );
	TRandomCombined< CRandomMersenne, CRandomMother > RG( (uint32)time( 0 ) );
	for( i = (int)lstSrc.GetCount(); i > 0; i-- )
	{
//		int irand = rand() % i;
		int irand = RG.IRandom( 0, i - 1 );
		pos = lstSrc.FindIndex( irand );
		szItem = lstSrc.GetAt( pos );
		lstDest.AddTail( szItem );
		lstSrc.RemoveAt( pos );
	}

	// Now expand if not randomizing trials
	if( !fTrials )
	{
		pos = lstDest.GetHeadPosition();
		while( pos )
		{
			szItem = lstDest.GetNext( pos );
			
			nPos = szItem.ReverseFind( '!' );
			nReps = atoi( szItem.Mid( nPos + 1 ) );
			szItem = szItem.Left( nPos );

			for( i = 0; i < nReps; i++ )
			{
				pList->AddTail( szItem );
			}
		}
	}
	// Just copy into main list
	else
	{
		pos = lstDest.GetHeadPosition();
		while( pos )
		{
			szItem = lstDest.GetNext( pos );
			pList->AddTail( szItem );
		}
	}

	// We will write the results to the subject folder if group, subject
	// and experiment IDs are specified: Test to make sure it opens
	CStdioFile	fOut;
	CString szTemp;
	if( ( szExp != _T("") ) && ( szGrp != _T("") ) && ( szSubj != _T("") ) )
	{
		// verify directory exists
		::VerifyDirectory( szExp, szGrp, szSubj );
		// we'll use the ERR file to avoid writing more code
		::GetERR( szExp, szGrp, szSubj, _T(""), szTemp );
		// remove the extension and add "SEQ"
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T("SEQ");
		if( !fOut.Open( szTemp, CFile::modeWrite | CFile::modeCreate ) )
		{
			BCGPMessageBox( _T("ERROR: Unable to open sequence file for writing.") );
			return FALSE;
		}
		// write header
		fOut.WriteString( _T("// TRIAL SEQUENCE RANDOMLY GENERATED:\n//\n") );
		// write trial sequence
		Experiments::WriteTrialSequence( &fOut, pList );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::RandomizeWithRules( CStringList *pLstConds, CStringList *pList,
									  CString szRules, CString szBlocks, CString szSels,
									  CString szSelTrials, BOOL& fSen2Word, CString szExp,
									  CString szGrp, CString szSubj, BOOL fInform,
									  CString& szErrFile )
{
	// variables
	CString		szCondID;				// condition ID
	CString		szCond;					// condition description
	CString		szDelim;				// delimiter
	CString		szItem;					// working item
	CString		szStim;					// stimulus
	CStringList	lstSrc;					// working source list
	CStringList	lstDest;				// working destination list
	short		nRecCond	= 0;		// # of conditions for recording
	short		nReps		= 0;		// condition reps
	int			i			= 0;		// loop index
	short		nPos		= 0;		// position var (strings)
	BOOL		fRecord		= FALSE;	// condition records?
	BOOL		fProcess	= TRUE;		// condition processes?
	POSITION	pos			= NULL;		// list position
	HRESULT		hr			= S_OK;		// COM return value
	Stimuluss	stim;					// stimulus object
	NSConditions	cond;				// condition object
	CStringList	lstChars[ 3 ];			// array of 3 lists for existing letters for 3 char ID
	BOOL		fRules[ 3 ][ 3 ];		// array of 3 x 3 for whether to use rules 1-3 for chars 1-3
	int			nBlocks[ 3 ];			// array of 3 for blocks to use for rule # 2 (if applied)
	char		pszSel[ 3 ];			// chars for selection of trials (rule # 3)
	CStringList	lstSelTrials[ 3 ];		// array of 3 lists for those trials to apply char to
	CString		szChar;					// scratch var
	int			nItem		= 0;		// scratch var
	int			nTotal		= 0;		// total # of cond/trial items (# of conds * # of trials per)
	int			nStepsInt	= 0;		// # of times trying to locate a random trial to match rules
	int			nStepsExt	= 0;		// # of times trying to redo process before returning failure
	int			nRand		= 0;		// var for random calcs
	CString		szCur;					// var for current condition
	CString		szNext;					// var for randomly chosen condition
	CString		szTemp;					// scratch vars
	CString		szTemp2;
	CStdioFile	fOut;					// output file for sequence
	BOOL		fWrite		= FALSE;	// flag whether to write sequence to file
	CStdioFile	fErr;					// output file for steps if failed

	// verify pointers
	if( !pList || !pLstConds ) return FALSE;

	// We will write the steps to a file for viewing if randomization fails
	// if randomize succeeds, delete file and clear file name reference
	::GetDataPathRoot( szTemp );
	if( ( szTemp != _T("") ) && ( szTemp.GetAt( szTemp.GetLength() - 1 ) == '\\') )
		szTemp = szTemp.Left( szTemp.GetLength() - 1 );
	if( szTemp == _T("") )
		szErrFile.Format( _T("%s\\SEQ.ERR"), szExp );
	else
		szErrFile.Format( _T("%s\\%s\\SEQ.ERR"), szTemp, szExp );
	// verify directory exists
	::VerifyDirectory( szExp );
	// open file for writing
	if( !fErr.Open( szErrFile, CFile::modeCreate | CFile::modeWrite ) )
	{
		if( fInform ) BCGPMessageBox( _T("ERROR: Unable to open sequence-steps file for writing.") );
		return FALSE;
	}

	// We will write the results to the subject folder if group, subject
	// and experiment IDs are specified: Test to make sure it opens
	if( ( szExp != _T("") ) && ( szGrp != _T("") ) && ( szSubj != _T("") ) )
	{
		// verify directory exists
		::VerifyDirectory( szExp, szGrp, szSubj );
		// we'll use the ERR file to avoid writing more code
		::GetERR( szExp, szGrp, szSubj, _T(""), szTemp );
		// remove the extension and add "SEQ"
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T("SEQ");
		if( !fOut.Open( szTemp, CFile::modeWrite | CFile::modeCreate ) )
		{
			if( fInform ) BCGPMessageBox( _T("ERROR: Unable to open sequence file for writing.") );
			return FALSE;
		}
		fWrite = TRUE;
	}

	// get field delimiter (& trim)
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// create condition object and validate
	if( !cond.IsValid() ) return FALSE;

	// First fill list with all NSConditions (one for each rep as well)
	pos = pLstConds->GetHeadPosition();
	while( pos )
	{
		// get values already set in list
		szCond = pLstConds->GetNext( pos );
		nPos = szCond.Find( szDelim );
		szCondID = szCond.Left( nPos );
		nReps = atoi( szCond.Mid( nPos + 1 ) );
		// locate condition in db to get other properties
		hr = cond.Find( szCondID.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			cond.GetRecord( fRecord );
			if( fRecord ) nRecCond++;
			cond.GetProcess( fProcess );
			if( !fRecord && fProcess ) fSen2Word = TRUE;
			cond.GetStimulusWarning( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				if( fInform ) BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
			cond.GetStimulusPrecue( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				if( fInform ) BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
			cond.GetStimulus( szStim );
			if( ( szStim != "" ) && ( FAILED( stim.Find( szStim ) ) || !stim.GenerateFiles() ) )
			{
				if( fInform ) BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				delete pList;
				return FALSE;
			}
		}

		if( fRecord )
		{
			for( i = 0; i < nReps; i++ )
			{
				// add to main list
				szItem.Format( _T("%s"), szCondID );
				lstSrc.AddTail( szItem );

				// 1st char
				szChar = szCondID.GetAt( 0 );
				if( lstChars[ 0 ].Find( szChar ) == NULL )
					lstChars[ 0 ].AddTail( szChar );
				// 2nd char
				szChar = szCondID.GetAt( 1 );
				if( lstChars[ 1 ].Find( szChar ) == NULL )
					lstChars[ 1 ].AddTail( szChar );
				// 3rd char
				szChar = szCondID.GetAt( 2 );
				if( lstChars[ 2 ].Find( szChar ) == NULL )
					lstChars[ 2 ].AddTail( szChar );
			}
		}
	}

	// if there are no conditions set to record, we cannot run experiment
	if( nRecCond == 0 )
	{
		BCGPMessageBox( _T("The experiment has no recordable conditions.\nThe experiment cannot continue.\n\nYou must set at least one condition as recordable.") );
		delete pList;
		return FALSE;
	}

	// Initialize rules arrays
	memset( fRules, 0, sizeof( BOOL ) * 3 * 3 );
	memset( nBlocks, 0, sizeof( int ) * 3 );

	// Extract Rules from string (each char is separated by a delimiter)
	// rules are or'd
	// ID CHAR # 1
	nPos = szRules.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szRules.Left( nPos );
	nItem = atoi( szItem );
	if( ( nItem & RAND_RULE_1 ) != 0 ) fRules[ 0 ][ 0 ] = TRUE;
	else fRules[ 0 ][ 0 ] = FALSE;
	if( ( nItem & RAND_RULE_2 ) != 0 ) fRules[ 0 ][ 1 ] = TRUE;
	else fRules[ 0 ][ 1 ] = FALSE;
	if( ( nItem & RAND_RULE_3 ) != 0 ) fRules[ 0 ][ 2 ] = TRUE;
	else fRules[ 0 ][ 2 ] = FALSE;
	szRules = szRules.Mid( nPos + 1 );
	// ID CHAR # 2
	nPos = szRules.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szRules.Left( nPos );
	nItem = atoi( szItem );
	if( ( nItem & RAND_RULE_1 ) != 0 ) fRules[ 1 ][ 0 ] = TRUE;
	else fRules[ 1 ][ 0 ] = FALSE;
	if( ( nItem & RAND_RULE_2 ) != 0 ) fRules[ 1 ][ 1 ] = TRUE;
	else fRules[ 1 ][ 1 ] = FALSE;
	if( ( nItem & RAND_RULE_3 ) != 0 ) fRules[ 1 ][ 2 ] = TRUE;
	else fRules[ 1 ][ 2 ] = FALSE;
	szRules = szRules.Mid( nPos + 1 );
	// ID CHAR # 3
	if( szRules == _T("") ) { delete pList; return FALSE; }
	szItem = szRules;
	nItem = atoi( szItem );
	if( ( nItem & RAND_RULE_1 ) != 0 ) fRules[ 2 ][ 0 ] = TRUE;
	else fRules[ 2 ][ 0 ] = FALSE;
	if( ( nItem & RAND_RULE_2 ) != 0 ) fRules[ 2 ][ 1 ] = TRUE;
	else fRules[ 2 ][ 1 ] = FALSE;
	if( ( nItem & RAND_RULE_3 ) != 0 ) fRules[ 2 ][ 2 ] = TRUE;
	else fRules[ 2 ][ 2 ] = FALSE;

	// Extract blocks from string (each char separated by delim)
	// ID CHAR # 1
	nPos = szBlocks.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szBlocks.Left( nPos );
	nBlocks[ 0 ] = atoi( szItem );
	szBlocks = szBlocks.Mid( nPos + 1 );
	// ID CHAR # 2
	nPos = szBlocks.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szBlocks.Left( nPos );
	nBlocks[ 1 ] = atoi( szItem );
	szBlocks = szBlocks.Mid( nPos + 1 );
	// ID CHAR # 3
	szItem = szBlocks;
	nBlocks[ 2 ] = atoi( szItem );

	// Extract chars for rule # 3 selection
	// ID CHAR # 1
	nPos = szSels.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szSels.Left( nPos );
	pszSel[ 0 ] = szItem[ 0 ];
	szSels = szSels.Mid( nPos + 1 );
	// ID CHAR # 2
	nPos = szSels.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szSels.Left( nPos );
	pszSel[ 1 ] = szItem[ 0 ];
	szSels = szSels.Mid( nPos + 1 );
	// ID CHAR # 3
	szItem = szSels;
	pszSel[ 2 ] = szItem[ 0 ];

	// Extract trials to select for sel char for rule # 3
	// ...trials are separated by a period "."
	// ID CHAR # 1
	nPos = szSelTrials.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szSelTrials.Left( nPos );
	szSelTrials = szSelTrials.Mid( nPos + 1 );
	do 
	{
		nPos = szItem.Find( _T(".") );
		if( nPos != -1 ) szTemp = szItem.Left( nPos );
		else szTemp = szItem;
		if( szTemp != _T("") ) lstSelTrials[ 0 ].AddTail( szTemp );
		if( nPos != -1 ) szItem = szItem.Mid( nPos + 1 );
	} while ( nPos != -1 );
	// ID CHAR # 2
	nPos = szSelTrials.Find( szDelim );
	if( nPos == -1 ) { delete pList; return FALSE; }
	szItem = szSelTrials.Left( nPos );
	szSelTrials = szSelTrials.Mid( nPos + 1 );
	do 
	{
		nPos = szItem.Find( _T(".") );
		if( nPos != -1 ) szTemp = szItem.Left( nPos );
		else szTemp = szItem;
		if( szTemp != _T("") ) lstSelTrials[ 1 ].AddTail( szTemp );
		if( nPos != -1 ) szItem = szItem.Mid( nPos + 1 );
	} while ( nPos != -1 );
	// ID CHAR # 3
	szItem = szSelTrials;
	do 
	{
		nPos = szItem.Find( _T(".") );
		if( nPos != -1 ) szTemp = szItem.Left( nPos );
		else szTemp = szItem;
		if( szTemp != _T("") ) lstSelTrials[ 2 ].AddTail( szTemp );
		if( nPos != -1 ) szItem = szItem.Mid( nPos + 1 );

	} while ( nPos != -1 );


	//////////////////////////////////////////////////////////////////////////
	// BEGIN THE PROCESSING HERE
	//////////////////////////////////////////////////////////////////////////
	CWaitCursor crs;
	OutputAMessage( _T("Randomization of trials begins.") );

#define ITER_INT		1000	// # of times to try for a random var to match rules
#define ITER_EXT		100		// # of times to restart process before returning failure

	// total # of items we must have
	nTotal = lstSrc.GetCount();

	// New randomization technology to improve randomness
	// ...(construction of template object seeds both generators)
	TRandomCombined< CRandomMersenne, CRandomMother > RG( (uint32)time( 0 ) );

DoProc:
	// check to make sure we are not past the # of total attempts
	nStepsExt++;
	if( nStepsExt > ITER_EXT )
	{
		CString szMsg;
		szMsg.Format( _T("ERROR: Randomization rules could not be sucessfully applied after %d random generation attempts for %d iterations."), ITER_INT, ITER_EXT );
		OutputAMessage( _T("ERROR: Randomization rules could not be successfully applied.") );
		if( fInform ) BCGPMessageBox( szMsg );
		delete pList;
		return FALSE;
	}
	// clear list (in case of repeats for failure of rules)
	pList->RemoveAll();
	// refill list with source list
	pos = lstSrc.GetHeadPosition();
	while( pos ) pList->AddTail( lstSrc.GetNext( pos ) );
	// reset internal testing var
	nStepsInt = 0;
	szTemp = _T("");
	szTemp2 = _T("");
	// loop through until we include all conditions/trials
	for( int i = 0; i < ( nTotal - 1 ); /* not incrementing auto */ )
	{
DoIntProc:
		// if we are redoing a loop after a failure from ITER_INT randomization attempts,
		// ...and szTemp is not empty, write the contents of szTemp to the err file
		if( szTemp != _T("") ) fErr.WriteString( szTemp );

		// Error messaging: Trial #
		szTemp2.Format( _T("TRIAL#: %d - "), i + 1 );
		szTemp = szTemp2;

		// verify we haven't exceeded the # of times to try for the same condition index
		nStepsInt++;
		if( nStepsInt > ITER_INT ) goto DoProc;

		// current condition (based on i)
		pos = pList->FindIndex( i );
		if( !pos ) { delete pList; return FALSE; }
		szCur = pList->GetNext( pos );

		// random selection of a condition from list from i to Total - 1
		// ...ONLY if not the last item
		if( i < ( nTotal - 1 ) ) nRand = RG.IRandom( i, nTotal - 1 );
		else nRand = i;
		pos = pList->FindIndex( nRand );
		if( !pos ) { delete pList; return FALSE; }
		szNext = pList->GetNext( pos );

		// Error messaging: append random index # and retrieved condition ID
		szTemp2.Format( _T("RAND#: %d - COND: %s - "), nRand, szNext );
		szTemp += szTemp2;

		// VERIFY RULES (i > 0 --- excluding first --- handled in rule functions)
		// Rule #1 (for each char...if specified)
		for( int j = 0; j < 3; j++ )
		{
			if( fRules[ j ][ 0 ] &&
				!ConditionSequenceRuleDoAllOnce( pList, i, szNext, j, &(lstChars[ j ]) ) )
			{
				szTemp += _T("RULE#: DoAllOnce\n");
				goto DoIntProc;
			}
		}
		// Rule #2 (for each char...if specified)
		for( int j = 0; j < 3; j++ )
		{
			if( fRules[ j ][ 1 ] &&
				!ConditionSequenceRuleBlockOfSame( pList, i, szNext, j, &(lstChars[ j ]), nBlocks[ j ] ) )
			{
				szTemp += _T("RULE#: BlockOfSame\n");
				goto DoIntProc;
			}
		}
		// Rule #3 (for each char...if specified)
		for( int j = 0; j < 3; j++ )
		{
			if( fRules[ j ][ 2 ] &&
				!ConditionSequenceRuleOnTrialNumber( i + 1, szNext, j, pszSel[ j ], &(lstSelTrials[ j ]) ) )
			{
				szTemp += _T("RULE#: OnTrialNumber\n");
				goto DoIntProc;
			}
		}

		// swap conditions in list
		pos = pList->FindIndex( i );
		pList->SetAt( pos, szNext );
		pos = pList->FindIndex( nRand );
		pList->SetAt( pos, szCur );

		// Since we have succeeded, we will write the successful condition to the err file
		// and clear szTemp so no error data (unsuccessful attempts) is written
		szTemp.Format( _T("ACCEPTED CONDITION TRIAL: %s\n"), szNext );
		fErr.WriteString( szTemp );
		szTemp = _T("");

		// we've succeeded so we can move forward
		i++;
		nStepsInt = 0;
	}

	// we've succeeded....so, remove err file and write sequence file (if specified)
	// ...also, set szErrFile to empty to indicate there is no file
	// ERROR FILE
	fErr.Close();
	REMOVE_FILE( szErrFile );
	szErrFile = _T("");
	// SEQUENCE FILE
	if( fWrite )
	{
		// Write header
		fOut.WriteString( _T("// TRIAL SEQUENCE RANDOMLY GENERATED WITH THE FOLLOWING RULES:\n//\n") );
		for( i = 0; i < 3; i++ )
		{
			szTemp.Format( _T("//CHARACTER # %d:\n"), i + 1 );
			fOut.WriteString( szTemp );

			fOut.WriteString( _T("//\tRULE # 1 (DoAllOnce): ") );
			if( fRules[ i ][ 0 ] ) fOut.WriteString( _T("YES\n") );
			else fOut.WriteString( _T("NO\n") );

			fOut.WriteString( _T("//\tRULE # 2 (BlockOfSame): ") );
			if( fRules[ i ][ 1 ] )
			{
				szTemp.Format( _T("YES - BlocksOf: %d\n"), nBlocks[ i ] );
				fOut.WriteString( szTemp );
			}
			else fOut.WriteString( _T("NO\n") );

			fOut.WriteString( _T("//\tRULE # 3 (OnTrialNumber): ") );
			if( fRules[ i ][ 2 ] )
			{
				szCur = pszSel[ i ];
				szTemp.Format( _T("YES - ChosenChar: %s - Trials: "), szCur );
				pos = lstSelTrials[ i ].GetHeadPosition();
				while( pos )
				{
					szTemp2 = lstSelTrials[ i ].GetNext( pos );
					szTemp += szTemp2;
					if( pos ) szTemp += _T(",");
				}
				fOut.WriteString( szTemp );
				fOut.WriteString( _T("\n") );
			}
			else fOut.WriteString( _T("NO\n") );
		}
		fOut.WriteString( _T("\n\n") );
		// Write items
		Experiments::WriteTrialSequence( &fOut, pList );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ConditionSequenceRuleDoAllOnce( CStringList* pListConds, int nPosIdx, CString szCond,
												  int nChar, CStringList* pListChars )
{
	// if 1st item, we always succeed
	if( nPosIdx == 0 ) return TRUE;

	// verify pointers & values
	if( !pListConds || !pListChars ) return FALSE;
	if( ( nPosIdx < 0 ) || ( nPosIdx >= pListConds->GetCount() ) ||
		( nChar < 0 ) || ( nChar >= 3 ) || ( szCond == _T("") ) )
		return FALSE;

	// nStart is based upon the count of the list of characters of nChar character
	// so, if there are 5 different characters for the nChar character of the ID,
	// the list would be in increments of 5 to check: 0-4, 5-9, etc.
	int nCount = pListChars->GetCount();
	int nStart = (int)( nPosIdx / nCount ) * nCount;

	// loop through the conditions from nStart to nPosIdx and add the nChar character
	// to the temp list if it does not exist and continue
	// if it exists, we have not succeeded
	CStringList lstTemp;
	CString szTemp, szCur, szChar;
	POSITION pos = NULL;
	for( int i = nStart; i < nPosIdx; i++ )
	{
		// condition at index i
		pos = pListConds->FindIndex( i );
		if( !pos ) return FALSE;
		szCur = pListConds->GetAt( pos );
		// add char nChar to temp list
		szChar = szCur.GetAt( nChar );
		if( lstTemp.Find( szChar ) ) return FALSE;
		lstTemp.AddTail( szChar );
	}

	// now check if new cond's nChar character is in list
	szChar = szCond.GetAt( nChar );
	if( lstTemp.Find( szChar ) ) return FALSE;

	// if we've made it this far, we've succeeded
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ConditionSequenceRuleBlockOfSame( CStringList* pListConds, int nPosIdx, CString szCond,
													int nChar, CStringList* pListChars, int nBlocks )
{
	// if 1st item, we always succeed
	if( nPosIdx == 0 ) return TRUE;

	// verify pointers & values
	if( !pListConds || !pListChars ) return FALSE;
	if( ( nPosIdx < 0 ) || ( nPosIdx >= pListConds->GetCount() ) ||
		( nChar < 0 ) || ( nChar >= 3 ) || ( szCond == _T("") ) ||
		( nBlocks <= 0 ) )
		return FALSE;

	// Get nChar characters of last and current items
	POSITION pos = pListConds->FindIndex( nPosIdx - 1 );
	if( !pos ) return FALSE;
	CString szItem = pListConds->GetAt( pos );
	CString szLastChar = szItem.GetAt( nChar );
	CString szCurChar = szCond.GetAt( nChar );

	// if this is the 1st of the block, then it will succeed
	// ...IF it is not a duplicate of the last sequence
	if( ( nPosIdx % nBlocks ) == 0 )
	{
		if( szCurChar == szLastChar ) return FALSE;
		else return TRUE;
	}

	// if the nChar character of the condition to add does not match the previous, we fail
	if( szCurChar != szLastChar ) return FALSE;

	// if we've made it this far, we've succeeded
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ConditionSequenceRuleOnTrialNumber( int nTrial, CString szCond, int nChar,
													  char c, CStringList* pListSelTrials )
{
	// if, for some reason, no char was specified, return true
	CString szC = c;
	if( szC == "" ) return TRUE;

	// verify pointers & values
	if( !pListSelTrials ) return FALSE;
	if( ( nTrial < 0 ) || ( nChar < 0 ) || ( nChar >= 3 ) || ( szCond == _T("") ) )
		return FALSE;

	// Get nChar character of item
	CString szCurChar = szCond.GetAt( nChar );

	// COND[nChar] MUST = c IF nTrial IS IN LIST OF SELECTED TRIALS
	// COND[nChar] CANNOT = c IF nTrial IS NOT IN LIST
	// if nTrial is in the list pListSels...
	CString szTrial;
	szTrial.Format( _T("%d"), nTrial );
	if( pListSelTrials->Find( szTrial ) )
	{
		// Ensure this character matches the specified character c
		if( szC != szCurChar ) return FALSE;
	}
	// otherwise, if item character is c, then we fail
	else if( szC == szCurChar ) return FALSE;

	// if we've made it this far, we've succeeded
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

IProcess* Experiments::GetProcessObject( CWnd* pMsgList, IProcSetting* pPS,
										 CString szExp, BOOL fSettings )
{
	// create if necessary
	if( !_pProcess )
	{
		HRESULT hr = E_FAIL;
		hr = CoCreateInstance( CLSID_Process, NULL, CLSCTX_ALL,
							   IID_IProcess, (LPVOID*)&_pProcess );
		if( SUCCEEDED( hr ) )
		{
			CString szAuthCode = ::GetAuthorizationProcessMod();
			hr = _pProcess->SetAuthorization( szAuthCode.AllocSysString() );
			if( FAILED( hr ) )
			{
				_pProcess->Release();
				_pProcess = NULL;
				return NULL;
			}

			if( !_Reproc ) _pProcess->SetOutputWindow( (VARIANT*)pMsgList );
			CString szAppPath;
			::GetDataPathRoot( szAppPath );
			szAppPath += _T("\\Actions.log");
			_pProcess->SetLoggingOn( IsLoggingOn(), szAppPath.AllocSysString() );
		}
		else
		{
			CString szMsg;
			szMsg.Format( IDS_PROC_ERR, hr );
			BCGPMessageBox( szMsg );
		}
	}

	// missing data value
	_pProcess->SetMissingDataValue( ::GetMissingDataValue() );

	// Create ProcSettings object
	if( fSettings )
	{
		BOOL fCreated = FALSE;
		if( !pPS )
		{
			HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
										   IID_IProcSetting, (LPVOID*)&pPS );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( IDS_PS_ERR );
				return NULL;
			}
			// if not found, use default
			hr = pPS->Find( szExp.AllocSysString() );
			fCreated = TRUE;
		}

		// apply settings
		double dVal = 0.;
		BOOL fVal = FALSE;
		short nVal = 0;
		// sen2word
		pPS->get_MinLeftPenup( &dVal );
		_pProcess->put_MinLeftPenup( dVal );
		pPS->get_MinLeftSpacing( &dVal );
		_pProcess->put_MinLeftSpacing( dVal );
		pPS->get_MinWordWidth( &dVal );
		_pProcess->put_MinWordWidth( dVal );
		pPS->get_MinDownSpacing( &dVal );
		_pProcess->put_MinDownSpacing( dVal );
		pPS->get_MinTimePenup( &dVal );
		_pProcess->put_MinTimePenup( dVal );
		pPS->get_MaxTimePenup( &dVal );
		_pProcess->put_MaxTimePenup( dVal );
		// timfun
		pPS->get_FFT( &fVal );
		_pProcess->put_FFT( fVal );
		pPS->get_Sharpness( &dVal );
		_pProcess->put_Sharpness( dVal );
		pPS->get_UpSampleFactor( &nVal );
		_pProcess->put_UpSampleFactor( nVal );
		pPS->get_RemoveTrailingPenlift( &fVal );
		_pProcess->put_RemoveTrailingPenlift( fVal );
		// segment
		pPS->get_MaxVertVelocity( &dVal );
		_pProcess->put_MaxVertVelocity( dVal );
		pPS->get_MinVertVelocity( &dVal );
		_pProcess->put_MinVertVelocity( dVal );
		pPS->get_MinStrokeDuration( &dVal );
		_pProcess->put_MinStrokeDuration( dVal );
		pPS->get_MinStrokeSize( &dVal );
		_pProcess->put_MinStrokeSize( dVal );
		pPS->get_MinStrokeSizeRelativeToMax( &dVal );
		_pProcess->put_MinStrokeSizeRelativeToMax( dVal );
		// extract
		pPS->get_StrokeBeginning( &dVal );
		_pProcess->put_StrokeBeginning( dVal );
		pPS->get_MaxFrequency( &dVal );
		_pProcess->put_MaxFrequency( dVal );
		// consis
		pPS->get_MinLoopArea( &dVal );
		_pProcess->put_MinLoopArea( dVal );
		pPS->get_MinStraightErrorCurved( &dVal );
		_pProcess->put_MinStraightErrorCurved( dVal );
		pPS->get_MaxStraightErrorStraight( &dVal );
		_pProcess->put_MaxStraightErrorStraight( dVal );
		pPS->get_SlantError( &dVal );
		_pProcess->put_SlantError( dVal );
		pPS->get_SpiralIncreaseFactor( &dVal );
		_pProcess->put_SpiralIncreaseFactor( dVal );
		pPS->get_DiscardLTMin( &fVal );
		_pProcess->put_DiscardLTMin( fVal );
		pPS->get_MinReactionTime( &dVal );
		_pProcess->put_MinReactionTime( dVal );
		pPS->get_DiscardGTMax( &fVal );
		_pProcess->put_DiscardGTMax( fVal );
		pPS->get_MaxReactionTime( &dVal );
		_pProcess->put_MaxReactionTime( dVal );

		short nDisCorrection = 0;
		double dDisFactor = 0.0, dDisZInsPoint = 0.0, dDisRelErr = 0.0, dDisAbsErr = 0.0;
		pPS->get_SamplingRate( &nVal );
		_pProcess->put_SamplingRate( nVal );
		pPS->get_DeviceResolution( &dVal );
		_pProcess->put_DeviceResolution( dVal );
		pPS->get_MinPenPressure( &nVal );
		_pProcess->put_MinPenPressure( nVal );
		pPS->get_DisCorrection( &nDisCorrection );
		pPS->get_DisFactor( &dDisFactor );
		pPS->get_DisZInsertPoint( &dDisZInsPoint );
		pPS->get_DisRelError( &dDisRelErr );
		pPS->get_DisAbsError( &dDisAbsErr );
		_pProcess->Discontinuity( nDisCorrection, dDisFactor, dDisZInsPoint, dDisRelErr, dDisAbsErr );
		double dDisError = 0.;
		pPS->get_DisError( &dDisError );
		_pProcess->put_DisError( dDisError );

		if( fCreated ) pPS->Release();
	}

	// return process object
	return _pProcess;
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::Cleanup()
{
	if( _ml != NULL )
	{
		_ml.Release();
	}
	if( _pProcess )
	{
		_pProcess->Release();
		_pProcess = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ProcessSen2wrd( CString szIn, CString szOut, CWnd* pMsgList )
{
	HRESULT hr = E_FAIL;
	CWaitCursor crs;

	IProcess* pProcess = GetProcessObject( pMsgList );
	if( pProcess )
	{
		CString szData;
		szData.Format( IDS_AL_S2W, szIn, szOut );
		OutputAMessage( szData, FALSE );
		hr = pProcess->Sentence2Word( szIn.AllocSysString(),
									  szOut.AllocSysString(),
									  TRUE );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ProcessUnrot( CString szIn, CString szOut, BOOL fVelocity,
							   BOOL fDiff, double dRotBeta, CWnd* pMsgList )
{
	HRESULT hr = E_FAIL;
	CWaitCursor crs;

	IProcess* pProcess = GetProcessObject( pMsgList );
	if( pProcess )
	{
		pProcess->put_MinPenPressure( (short)::GetMinPenPressure() );
		pProcess->put_SamplingRate( (short)::GetSamplingRate() );
		pProcess->put_DeviceResolution( ::GetDeviceResolution() );

		CString szData;
		szData.Format( IDS_AL_UNROT, szIn, szOut );
		OutputAMessage( szData, FALSE );
		hr = pProcess->Unrotate( szIn.AllocSysString(),
								 szOut.AllocSysString(),
								 fVelocity,
								 fDiff,
								 dRotBeta );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ProcessTimfun( CString szIn, CString szOut, double dFreq,
								 int nDecimate, UINT tfSwitch, double dRotBeta, CWnd* pMsgList )
{
	HRESULT hr = E_FAIL;

	IProcess* pProcess = GetProcessObject( pMsgList );
	if( pProcess )
	{
		CString szData;
		szData.Format( IDS_AL_TF, szIn, szOut );
		OutputAMessage( szData, FALSE );
		CString szExt = szIn.Right( 3 );
		szExt.MakeUpper();
		if( szExt == _T("FRD") )
		{
			_Type = EXP_TYPE_GRIPPER;
			hr = pProcess->eTimeFunctionGripper( szIn.AllocSysString(),
												 szOut.AllocSysString(),
												 dFreq,
												 nDecimate,
												 (TFSwitch)tfSwitch );
		}
		else if( szExt == _T("HWR") )
		{
			_Type = EXP_TYPE_HANDWRITING;
			hr = pProcess->eTimeFunction( szIn.AllocSysString(),
										  szOut.AllocSysString(),
										  dFreq,
										  nDecimate,
										  (TFSwitch)tfSwitch,
										  dRotBeta );
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ProcessSegmen( CString szIn, CString szOut, UINT segSwitch, CWnd* pMsgList, UINT gripSwitch )
{
	HRESULT hr = E_FAIL;
	CWaitCursor crs;

	IProcess* pProcess = GetProcessObject( pMsgList );
	if( pProcess )
	{
		CString szData;
		szData.Format( IDS_AL_SEG, szIn, szOut );
		OutputAMessage( szData, FALSE );
		if( _Type == EXP_TYPE_GRIPPER )
			hr = pProcess->eSegmentGripper( szIn.AllocSysString(),
											szOut.AllocSysString(),
											(SegSwitch)segSwitch,
											(GripperSwitch)gripSwitch );
		else if( _Type == EXP_TYPE_HANDWRITING )
			hr = pProcess->eSegment( szIn.AllocSysString(),
									 szOut.AllocSysString(),
									 (SegSwitch)segSwitch );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ProcessExtract( CString szTF, CString szSeg, CString szExt,
								  UINT extSwitch, CWnd* pMsgList, UINT gripSwitch )
{
	HRESULT hr = E_FAIL;
	CWaitCursor crs;

	IProcess* pProcess = GetProcessObject( pMsgList );
	if( pProcess )
	{
		CString szData;
		szData.Format( IDS_AL_EXT, szTF, szSeg, szExt );
		OutputAMessage( szData, FALSE );
		if( _Type == EXP_TYPE_GRIPPER )
			hr = pProcess->eExtractGripper( szTF.AllocSysString(),
											szSeg.AllocSysString(),
											szExt.AllocSysString(),
											(ExtSwitch)extSwitch,
											(GripperSwitch)gripSwitch);
		else if( _Type == EXP_TYPE_HANDWRITING )
			hr = pProcess->eExtract( szTF.AllocSysString(),
									 szSeg.AllocSysString(),
									 szExt.AllocSysString(),
									 (ExtSwitch)extSwitch );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ProcessConsis( CString szExtIn, CString szConOut, CString szLexIn,
							    short nStrokeMin, short nStrokeMax, double dLength, double dRange,
								double dDir, double dRangeDir, short nSkip, BOOL fFlagBadTarget,
							    CString szErrOut, CString szCondition, UINT conSwitch,
								BSTR trials, BOOL fLastOneOnly, BOOL& fPassed, CWnd* pMsgList,
								BOOL fDisDiscont, double dMagnetForce, UINT gripSwitch )
{
	HRESULT hr = E_FAIL;
	CWaitCursor crs;

	IProcess* pProcess = GetProcessObject( pMsgList );
	if( pProcess )
	{
		CString szData;
		szData.Format( IDS_AL_CON, szExtIn, szConOut, szCondition, szErrOut );
		OutputAMessage( szData, FALSE );
		pProcess->put_FlagBadTarget( fFlagBadTarget );
		pProcess->DiscardIfDiscontinuity( fDisDiscont );
		pProcess->put_MagnetForce( dMagnetForce );
		if( _Type == EXP_TYPE_GRIPPER )
			hr = pProcess->eConsisGripper( szExtIn.AllocSysString(),
											szConOut.AllocSysString(),
											szLexIn.AllocSysString(),
											nStrokeMin, nStrokeMax,
											dLength, dRange, dDir, dRangeDir, nSkip,
											szCondition.AllocSysString(),
											szErrOut.AllocSysString(),
											(ConSwitch)conSwitch,
											trials,
											fLastOneOnly,
											(GripperSwitch)gripSwitch,
											&fPassed );
		else if( _Type == EXP_TYPE_HANDWRITING )
			hr = pProcess->eConsis( szExtIn.AllocSysString(),
									szConOut.AllocSysString(),
									szLexIn.AllocSysString(),
									nStrokeMin, nStrokeMax,
									dLength, dRange, dDir, dRangeDir, nSkip,
									szCondition.AllocSysString(),
									szErrOut.AllocSysString(),
									(ConSwitch)conSwitch,
									trials,
									fLastOneOnly,
									&fPassed );
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ProcessTrial( CString szExpID, CString szSubjID, CString szGrpID,
								CString szCondID, CString szTrial, BOOL fAppend,
								BOOL fFlagBadTarget, IProcSetting* pPS, LPVOID param,
								CWnd* pMsgList, int& nSkipped, BOOL fDisplay,
								HTREEITEM hItem, BOOL fForceReproc, BOOL fResetSkip, UINT nGripSwitch )
{
	if( !pPS ) return FALSE;

	CFileFind ff;
	CStdioFile f;
	CString szRaw, szTF, szSeg, szExt, szCon, szErr, szLex, szData, szFileExt;
	short nMin = 0, nMax = 0, nPos = 0, nSkip = 0, nSteps = 0, nType = EXP_TYPE_HANDWRITING;
	double dLength = 0.0, dRange = 0.0, dDir = 0.0, dRangeDir = 0.0, dVal = 0.0, dMagnetForce = 0.;
	BOOL fImage = FALSE;

	// find experiment
	{
		Experiments exp;
		HRESULT hr = exp.Find( szExpID );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPFIND_ERR );
			return FALSE;
		}
		// experiment type
		exp.GetType( nType );
		fImage = ( nType == EXP_TYPE_IMAGE );
		switch( nType )
		{
			case EXP_TYPE_HANDWRITING: szFileExt = _T("HWR"); break;
			case EXP_TYPE_GRIPPER: szFileExt = _T("FRD"); break;
			case EXP_TYPE_IMAGE: szFileExt = _T("HWR"); break;
		}
		// missing data value
		double dMDV = -1.e6;
		exp.GetMissingDataValue( dMDV );
		::SetMissingDataValue( dMDV );
	}

	// static vars for skip notification
	static BOOL		fSkipped = FALSE;
	// if we're resetting skip vars, do it
	if( fResetSkip ) fSkipped = FALSE;

	// files names (raw, tf, seg, ext)
	szRaw = szTrial;
	szTF = szRaw.Left( szRaw.GetLength() - 3 );
	szSeg = szTF;
	szTF += _T("TF");
	szSeg += _T("SEG");
	GetEXT( szExpID, szGrpID, szSubjID, szCondID, szExt );

	// verify that none of the output files are open in some other window/application
	// we will do this by deleting them if they exist
	// except EXT file which can be appended...just check if writeable
	if( fForceReproc && ff.FindFile( szTF ) )
	{
		try { CFile::Remove( szTF ); }
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			CString szMsg = pszErr;
			szMsg += _T("\n\nProcessing cannot continue.\nPlease ensure all windows and applications viewing this file are closed.");
			BCGPMessageBox( szMsg, MB_ICONERROR );
			return FALSE;
		}
	}
	if( fForceReproc && ff.FindFile( szSeg ) )
	{
		try { CFile::Remove( szSeg ); }
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			CString szMsg = pszErr;
			szMsg += _T("\n\nProcessing cannot continue.\nPlease ensure all windows and applications viewing this file are closed.");
			BCGPMessageBox( szMsg, MB_ICONERROR );
			return FALSE;
		}
	}
	if( fForceReproc && ff.FindFile( szExt ) )
	{
		if( !f.Open( szExt, CFile::modeWrite ) )
		{
			BCGPMessageBox( _T("Processing cannot continue.\nPlease ensure all windows and applications viewing this file are closed."), MB_ICONERROR );
			return FALSE;
		}
		else f.Close();
	}

	// if TF file of newer date already exists, skip this file
	if( !fForceReproc && ff.FindFile( szTF ) )
	{
		CTime tfTime, rawTime;
		ff.FindNextFile();
		ff.GetLastWriteTime( tfTime );
		if( ff.FindFile( szRaw ) )
		{
			ff.FindNextFile();
			CString szRawName = ff.GetFileName();
			ff.GetLastWriteTime( rawTime );
			if( rawTime < tfTime )
			{
				if( !fSkipped )
				{
					szData.Format( _T("SKIPPING: Trial(s) %s and on - Already Processed"), szRawName );
					OutputAMessage( szData, TRUE );
					fSkipped = TRUE;
				}
				nSkipped++;
				return TRUE;
			}
		}
	}

	// if we're going to process, then reset the skip message
	fSkipped = FALSE;

	// get appropriate interfaces
	IProcSetFlags* pPSF = NULL;
	HRESULT hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) ) return FALSE;
	IProcSetRunExp* pPSRE = NULL;
	hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( FAILED( hr ) ) return FALSE;

	// Get external processing scripts if movalyzer
	ExternalAppType eHWRScript = eEAT_None, eTFScript = eEAT_None,
					eSegScript = eEAT_None, eExtScript = eEAT_None;
	CString szHWRScript, szTFScript, szSegScript, szExtScript, szTrialNum, szScriptRoot;
	::GetDataPathRoot( szScriptRoot );
	szScriptRoot += _T("\\scripts\\");
	BOOL fRes = FALSE;
	if( ::IsOA() )
	{
		BSTR bstr = NULL;
		pPSRE->get_ExtAppTypeRaw( &eHWRScript );
		if( eHWRScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptRaw( &bstr );
			szHWRScript = szScriptRoot;
			szHWRScript += bstr;
		}
		pPSRE->get_ExtAppTypeTF( &eTFScript );
		if( eTFScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptTF( &bstr );
			szTFScript = szScriptRoot;
			szTFScript += bstr;
		}
		pPSRE->get_ExtAppTypeSeg( &eSegScript );
		if( eSegScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptSeg( &bstr );
			szSegScript = szScriptRoot;
			szSegScript += bstr;
		}
		pPSRE->get_ExtAppTypeExt( &eExtScript );
		if( eExtScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptExt( &bstr );
			szExtScript = szScriptRoot;
			szExtScript += bstr;
		}

		szTrialNum = szRaw.Mid( szRaw.GetLength() - 6, 2 );
	}

	// Process Post-Record, Pre-TF external app script, if any
	switch( eHWRScript )
	{
		case eEAT_Matlab:
		{
			fRes = Experiments::ExecuteMatlabScript( szHWRScript, szExpID, szGrpID, szSubjID,
													 szCondID, szTrialNum, szFileExt );
			break;
		}
		case eEAT_Batch:
		{
			fRes = Experiments::ExecuteBatchScript( szHWRScript, szExpID, szGrpID, szSubjID,
				szCondID, szTrialNum, szFileExt );
			break;
		}
		case eEAT_None: fRes = TRUE; break;
	}
	if( !fRes )
	{
		if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
		{
			pPSRE->Release();
			pPSF->Release();
			return FALSE;
		}
	}

	// Process TF
	double dFF = 0.0, dBeta = 0.0;
	short nDec = 0;
	BOOL fTFA, fTFJ, fTFR, fTFU, fTFO;
	pPS->get_FilterFrequency( &dFF );
	pPS->get_RotationBeta( &dBeta );
	pPS->get_Decimate( &nDec );
	pPSF->get_TF_A( &fTFA );
	pPSF->get_TF_J( &fTFJ );
	pPSF->get_TF_R( &fTFR );
	pPSF->get_TF_U( &fTFU );
	pPSF->get_TF_O( &fTFO );
	UINT nSwitch = eTF_None;
	if( fTFA ) nSwitch |= eTF_A;
	if( fTFJ ) nSwitch |= eTF_J;
	if( fTFR ) nSwitch |= eTF_R;
	if( fTFU ) nSwitch |= eTF_U;
	if( fTFO ) nSwitch |= eTF_O;

	szData.Format( IDS_AL_PROCHWR, szTF );
	OutputAMessage( szData, FALSE );
	ProcessTimfun( szRaw, szTF, dFF, nDec, nSwitch, dBeta, pMsgList );

	// Check for existence of TF file
// 	if( !ff.FindFile( szTF ) )
// 	{
// 		szData.Format( IDS_AL_FILEMISSING, szTF );
// 		OutputAMessage( szData, TRUE );
// 		BCGPMessageBox( szData );
// 		return FALSE;
// 	}

	// Process Post-TF, Pre-Seg external app script, if any
	switch( eTFScript )
	{
		case eEAT_Matlab:
		{
			fRes = Experiments::ExecuteMatlabScript( szTFScript, szExpID, szGrpID, szSubjID,
													 szCondID, szTrialNum, szFileExt );
			break;
		}
		case eEAT_Batch:
		{
			fRes = Experiments::ExecuteBatchScript( szTFScript, szExpID, szGrpID, szSubjID,
													szCondID, szTrialNum, szFileExt );
			break;
		}
		case eEAT_None: fRes = TRUE; break;
	}
	if( !fRes )
	{
		if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
		{
			pPSRE->Release();
			pPSF->Release();
			return FALSE;
		}
	}

	// Segment
	BOOL fSegF, fSegL, fSegO, fSegS, fSegM, fSegA, fSegV, fSegMM, fSegJ;
	pPSF->get_SEG_F( &fSegF );
	pPSF->get_SEG_L( &fSegL );
	pPSF->get_SEG_O( &fSegO );
	pPSF->get_SEG_S( &fSegS );
	pPSF->get_SEG_M( &fSegM );
	pPSF->get_SEG_A( &fSegA );
	pPSF->get_SEG_V( &fSegV );
	pPSF->get_SEG_MM( &fSegMM );
	pPSF->get_SEG_J( &fSegJ );
	nSwitch = eSeg_None;
	if( fSegF ) nSwitch |= eSeg_F;
	if( fSegL ) nSwitch |= eSeg_L;
	if( fSegO ) nSwitch |= eSeg_O;
	if( fSegS && ::IsOA() ) nSwitch |= eSeg_S;
	if( fSegM ) nSwitch |= eSeg_M;
	if( fSegA ) nSwitch |= eSeg_A;
	if( fSegV ) nSwitch |= eSeg_V;
	if( fSegMM ) nSwitch |= eSeg_MM;
	if( fSegJ ) nSwitch |= eSeg_J;
	if( ::GetVerbosity() ) nSwitch |= eSeg_D;

	szData.Format( IDS_AL_PROCTF, szSeg );
	OutputAMessage( szData, FALSE );
	ProcessSegmen( szTF, szSeg, nSwitch, pMsgList, nGripSwitch );

	// Check for existence of SEG file
// 	if( !ff.FindFile( szSeg ) )
// 	{
// 		szData.Format( IDS_AL_FILEMISSING, szSeg );
// 		OutputAMessage( szData, TRUE );
// 		BCGPMessageBox( szData );
// 		pPSF->Release();
//		pPSRE->Release();
// 		return FALSE;
// 	}

	BOOL fPassed = FALSE;
	if( fAppend )
	{
		// Process Post-Seg, Pre-Ext external app script, if any
		switch( eSegScript )
		{
			case eEAT_Matlab:
			{
				fRes = Experiments::ExecuteMatlabScript( szSegScript, szExpID, szGrpID, szSubjID,
														 szCondID, szTrialNum, szFileExt );
				break;
			}
			case eEAT_Batch:
			{
				fRes = Experiments::ExecuteBatchScript( szSegScript, szExpID, szGrpID, szSubjID,
														szCondID, szTrialNum, szFileExt );
				break;
			}
			case eEAT_None: fRes = TRUE; break;
		}
		if( !fRes )
		{
			if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
			{
				pPSRE->Release();
				pPSF->Release();
				return FALSE;
			}
		}

		// Extract
		BOOL fExtS, fExt3, fExtO, fExt2;
		pPSF->get_EXT_S( &fExtS );
		pPSF->get_EXT_3( &fExt3 );
		pPSF->get_EXT_O( &fExtO );
		pPSF->get_EXT_2( &fExt2 );
		nSwitch = eExt_None;
		if( fExtS ) nSwitch |= eExt_S;
		if( fExt3 ) nSwitch |= eExt_3;
		if( fExtO ) nSwitch |= eExt_O;
		if( fExt2 ) nSwitch |= eExt_2;

		szData.Format( IDS_AL_PROCEXT, szExt );
		OutputAMessage( szData, FALSE );
		ProcessExtract( szTF, szSeg, szExt, nSwitch, pMsgList, nGripSwitch );

		// Check for existence of EXT file
// 		if( !ff.FindFile( szExt ) )
// 		{
// 			szData.Format( IDS_AL_FILEMISSING, szSeg );
// 			OutputAMessage( szData, TRUE );
// 			BCGPMessageBox( szData );
// 			pPSF->Release();
//			pPSRE->Release();
// 			return FALSE;
// 		}

		// Process Post-Ext external app script, if any
		switch( eExtScript )
		{
			case eEAT_Matlab:
			{
				fRes = Experiments::ExecuteMatlabScript( szExtScript, szExpID, szGrpID, szSubjID,
														 szCondID, szTrialNum, szFileExt );
				break;
			}
			case eEAT_Batch:
			{
				fRes = Experiments::ExecuteBatchScript( szExtScript, szExpID, szGrpID, szSubjID,
														szCondID, szTrialNum, szFileExt );
				break;
			}
			case eEAT_None: fRes = TRUE; break;
		}
		if( !fRes )
		{
			if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
			{
				pPSRE->Release();
				pPSF->Release();
				return FALSE;
			}
		}

		// Consis
		BOOL fConA, fConC, fConM, fConN, fConS, fConU, fConO, fConD;
		pPSF->get_CON_A( &fConA );
		pPSF->get_CON_C( &fConC );
		pPSF->get_CON_M( &fConM );
		pPSF->get_CON_N( &fConN );
		pPSF->get_CON_S( &fConS );
		pPSF->get_CON_U( &fConU );
		pPSF->get_CON_O( &fConO );
		pPSF->get_CON_D( &fConD );
		UINT conSwitch = eCon_None;
		if( fConA ) conSwitch |= eCon_A;
		if( fConC ) conSwitch |= eCon_C;
		if( fConM ) conSwitch |= eCon_M;
		if( fConN ) conSwitch |= eCon_N;
		if( fConS ) conSwitch |= eCon_S;
		if( fConU ) conSwitch |= eCon_U;
		if( fConO ) conSwitch |= eCon_O;

		// file names for consis and error
		GetCON( szExpID, szGrpID, szSubjID, szCondID, szCon );
		GetERR( szExpID, szGrpID, szSubjID, szCondID, szErr );

		// verify that none of the output files are open in some other window/application
		if( ff.FindFile( szCon ) )
		{
			if( !f.Open( szCon, CFile::modeWrite ) )
			{
				BCGPMessageBox( _T("Processing cannot continue.\nPlease ensure all windows and applications viewing this file are closed."), MB_ICONERROR );
				return FALSE;
			}
			else f.Close();
		}
		if( ff.FindFile( szErr ) )
		{
			if( !f.Open( szErr, CFile::modeWrite ) )
			{
				BCGPMessageBox( _T("Processing cannot continue.\nPlease ensure all windows and applications viewing this file are closed."), MB_ICONERROR );
				return FALSE;
			}
			else f.Close();
		}

		// Consis (if not image)
		NSConditions cond;
		if( !fImage && SUCCEEDED( cond.Find( szCondID.AllocSysString() ) ) )
		{
			cond.GetLex( szLex );
			cond.GetStrokeMin( nMin );
			cond.GetStrokeMax( nMax );
			cond.GetStrokeLength( dLength );
			cond.GetRangeLength( dRange );
			cond.GetStrokeDirection( dDir );
			cond.GetRangeDirection( dRangeDir );
			cond.GetStrokeSkip( nSkip );
			cond.GetMagnetForce( dMagnetForce );

			szData.Format( IDS_AL_PROCCON, szExt, szCon, szCondID );
			OutputAMessage( szData, FALSE );
			ProcessConsis( szExt, szCon, szLex, nMin, nMax, dLength,
						   dRange, dDir, dRangeDir, nSkip, fFlagBadTarget,
						   szErr, szCondID, conSwitch, NULL, TRUE,
						   fPassed, pMsgList, fConD, dMagnetForce, nGripSwitch );
		}
		if( fImage ) fPassed = TRUE;

		if( fPassed ) OutputAMessage( IDS_AL_TRIALS, FALSE );
		else OutputAMessage( IDS_AL_TRIALF, FALSE );

		if( fDisplay && hItem && ::GetDetailedOutput() )
		{
			int nPos = szRaw.ReverseFind( '\\' );
			if( nPos != -1 )
			{
				szTrial = szRaw.Mid( nPos + 1 );
				if( fImage )
				{
					szTrial = szTrial.Left( szTrial.GetLength() - 3 );
					szTrial += _T("PCX");
				}
			}

			CString szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );

			szData.Format( _T("%s%s%s%s%s%s%s%s%s%s%s%s%s%s0%s0%s%s"), szExpID,
				szDelim, szSubjID, szDelim, szGrpID, szDelim, szCondID, szDelim,
				_T(" "), szDelim, szTrial, szDelim, fPassed ? _T("1") : _T("0"),
				szDelim, szDelim, szDelim, fImage ? _T("1") : _T("0") );
			_hItem = hItem;

			MessageManager* pMM = MessageManager::GetMessageManager();
			pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_SHOWTRIAL ),
							  (WPARAM)( szData.GetBuffer() ) );
		}
	}
	else if( fDisplay && hItem && ::GetDetailedOutput() )
	{
		int nPos = szRaw.ReverseFind( '\\' );
		if( nPos != -1 )
		{
			szTrial = szRaw.Mid( nPos + 1 );
			if( fImage )
			{
				szTrial = szTrial.Left( szTrial.GetLength() - 3 );
				szTrial += _T("PCX");
			}
		}

		CString szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );

		szData.Format( _T("%s%s%s%s%s%s%s%s%s%s%s%s%s%s0%s1%s%s"), szExpID,
			szDelim, szSubjID, szDelim, szGrpID, szDelim, szCondID, szDelim,
			_T(" "), szDelim, szTrial, szDelim, fPassed ? _T("1") : _T("0"),
			szDelim, szDelim, szDelim, fImage ? _T("1") : _T("0") );
		_hItem = hItem;

		MessageManager* pMM = MessageManager::GetMessageManager();
		pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_SHOWTRIAL ),
			(WPARAM)( szData.GetBuffer() ) );
	}

	pPSF->Release();
	pPSRE->Release();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void RemoveTheFiles( CString szMask )
{
	CString szItem, szMsg;
	CFileFind ff;

	if( ff.FindFile( szMask ) )
	{
		while( ff.FindNextFile() )
		{
			szItem = ff.GetFilePath();
			REMOVE_FILE( szItem );
// 			szMsg.Format( _T("Deleting file: %s"), szItem );
// 			OutputAMessage( szMsg, FALSE );
		}
		// Last one
		szItem = ff.GetFilePath();
		REMOVE_FILE( szItem );
// 		szMsg.Format( _T("Deleting file: %s"), szItem );
// 		OutputAMessage( szMsg, FALSE );
	}
}

void Experiments::CleanProcessFiles( CString szRootPath, CString szExpID, BOOL fHWR,
									 BOOL fTF, BOOL fSeg, BOOL fExt, BOOL fCon,
									 BOOL fErr, BOOL fInc )
{
	short nExpType = EXP_TYPE_HANDWRITING;
	{
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExpID ) ) ) exp.GetType( nExpType );
	}

	// REMOVE FILES ACCORDINGLY
	CString szPath = szRootPath;
	CFileFind ff, ffGDir, ffSDir;
	CString szDir, szGDir, szSDir, szItem, szMsg;
	BOOL fQuitG = FALSE, fQuitS = FALSE;
	if( szPath == _T("") ) szDir.Format( _T("%s\\*"), szExpID );
	else szDir.Format( _T("%s\\%s\\*"), szPath, szExpID );
	if( !ffGDir.FindFile( szDir ) ) return;	// group directories

	szMsg.Format( _T("Deleting processing files of experiment %s."), szExpID );
	OutputAMessage( szMsg, FALSE );

	while( TRUE )
	{
		if( !ffGDir.FindNextFile() ) fQuitG = TRUE;
		szGDir = ffGDir.GetFileName();
		if( ffGDir.IsDirectory() && ( szGDir != _T(".") ) && ( szGDir != _T("..") ) )
		{
			fQuitS = FALSE;
			if( szPath == _T("") ) szDir.Format( _T("%s\\%s\\*"), szExpID, szGDir );
			else szDir.Format( _T("%s\\%s\\%s\\*"), szPath, szExpID, szGDir );
			if( ffSDir.FindFile( szDir ) )	// subject directories
			{
				while( TRUE )
				{
					if( !ffSDir.FindNextFile() ) fQuitS = TRUE;
					szSDir = ffSDir.GetFileName();
					if( ffSDir.IsDirectory() && ( szSDir != _T(".") ) && ( szSDir != _T("..") ) )\
					{
						Subjects::CleanProcessFiles( szExpID, szGDir, szSDir, nExpType,
													 fHWR, fTF, fSeg, fExt, fCon, fErr );
					}
					if( fQuitS ) break;
				}
			}
		}
		if( fQuitG ) break;
	}

	// INC
	if( fInc )
	{
		CString szBase, szTmp;
		GetExpNoExt( szExpID, szBase );

		GetExpINC( szExpID, szItem );
		RemoveTheFiles( szItem );
		GetExpERR( szExpID, szItem );
		RemoveTheFiles( szItem );
		GetExpERS( szExpID, szItem );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.TMP"), szBase );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.XME"), szBase );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.XM1"), szBase );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.ANA"), szBase );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.EVA"), szBase );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.GRP"), szBase );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%s.LST"), szBase );
		RemoveTheFiles( szItem );

		szTmp = szBase.Left( szBase.GetLength() - 3 );
		szItem.Format( _T("%sgraph.mem"), szTmp );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%sseq.err"), szTmp );
		RemoveTheFiles( szItem );
		szItem.Format( _T("%sdata.txt"), szTmp );
		RemoveTheFiles( szItem );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::SetTemp( BOOL fVal )
{
	if( m_pExperiment ) m_pExperiment->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DumpRecord( CString szID, CString szFile )
{
	if( !m_pExperiment ) return FALSE;
	return SUCCEEDED( m_pExperiment->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::RemoveTemp()
{
	if( m_pExperiment ) m_pExperiment->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DidTrialPass( CString szTrial, CString szExpID, CString szGrpID,
								CString szSubjID, CString szCondID, BOOL& fFound )
{
	// Check ERR file (if exists) for validity
	BOOL fPass = FALSE;
	CString szERR, szTemp, szItem;
	int nPos = szTrial.ReverseFind( '.' );
	if( nPos == -1 ) return FALSE;
	int nIdx = atoi( szTrial.Mid( nPos - 2, 2 ) );
	GetERR( szExpID, szGrpID, szSubjID, szCondID, szERR );
	CFileFind ff;
	if( ff.FindFile( szERR ) )
	{
		// open file and read to get OK or not
		CStdioFile f;
		if( f.Open( szERR, CFile::modeRead ) )
		{
			int nPos = -1;
			fFound = FALSE;
			while( f.ReadString( szTemp ) )
			{
				nPos = szTemp.Find( _T(" ") );
				if( nPos != -1 )
				{
					szItem = szTemp.Left( nPos );
					if( nIdx == atoi( szItem ) )
					{
						nPos = szTemp.Find( _T("OK") );
						if( nPos != -1 ) fPass = TRUE;
						else fPass = FALSE;
						fFound = TRUE;
					}
				}
			}
		}
	}

	return fPass;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::Export( CString& szWMsg, CString szExp, CString& szDefExpPath,
						  CString szSubjOnly, CString szSubjGroup,
						  BOOL fWizard, BOOL fWFiles, BOOL fWTrials, BOOL fWMembers,
						  BOOL fWShow, CString szWPath )
{
	CString szData = _T("SRC: Experiment - Export");
	LogToFile( szData, LOG_UI );

	CString szPath, szItem, szFile, szFile2, szTemp, szCond, szGrp,
			szSubj, szStim, szElem, szFiles;
	CString szSettings, szSettingsF, szTrials, szTrialsF, szExpFile,
			szUserID, szUserDesc;
	CFileFind ff, ffGrp, ffSubj, ffTrials;
	double dVal = 0.0;
	int nPos = 0, nCnt = 0;
	short nVal = 0;
	BOOL fVal = FALSE, fFiles = FALSE, fTrials = FALSE, fMembers = FALSE,
		 fContGrp = FALSE, fContSubj = FALSE, fContTrial = FALSE;
	BOOL fFirst = TRUE, fFailed = FALSE;
	HRESULT hr, hr2;
	BSTR bstrItem = NULL;
	COleDateTime dt;
	Experiments exp;
	Groups grp;
	Subjects subj;
	NSConditions cond;
	Stimuluss stim;
	Elements elem;
	IProcSetting* pPS = NULL;
	IProcSetFlags* pPSF = NULL;
	IProcSetRunExp* pPSRE = NULL;
	IMQuestionnaire* pMQ = NULL;
	IGQuestionnaire* pGQ = NULL;
	ISQuestionnaire* pSQ = NULL;
	IExperimentCondition* pEC = NULL;
	IExperimentMember* pEM = NULL;
	CStringList lstConds, lstGroups, lstEM;
	POSITION pos, pos2;
	TrialsMode tm;
	BOOL fSO = ( szSubjOnly != _T("") );

	if( !grp.IsValid() || !subj.IsValid() || !exp.IsValid() || !cond.IsValid() ||
		!stim.IsValid() || !elem.IsValid() ) return FALSE;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// initial subject privacy state (return it to original state after export)
	BOOL fPrivacyOn = ::IsPrivacyOn();

	ExportDlg d( AfxGetMainWnd() );
	// if wizard, set the dialog vals as if it were opened and selected
	if( fWizard )
	{
		szPath = szWPath;
		d.m_fTrials = fWTrials;
		d.m_fMembers = fWMembers;
		d.m_fFiles = fWFiles;
		if( fWShow ) ::SetIsPrivacyOn( FALSE );
	}
	else szPath = szDefExpPath;
	d.m_szFile = szPath;
	d.m_szExp = szExp;
	d.m_szGrp = szSubjGroup;
	d.m_szSubj = szSubjOnly;
	if( !fWizard && ( d.DoModal() == IDCANCEL ) ) return FALSE;
	szPath = d.m_szFile;
	// update registry for last export location
	szDefExpPath = szPath;

	// Export files/trials too?
	fFiles = d.m_fFiles;
	fTrials = d.m_fTrials;
	fMembers = d.m_fMembers;

	// Create the file/open
	BOOL fExtraSlash = FALSE;
	if( ( szPath.GetLength() > 1 ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\' ) )
		fExtraSlash = TRUE;
	if( fSO ) szFile.Format( _T("%s%s%s%s%s.EXP"), szPath, fExtraSlash ? _T("") : _T("\\"),
							 szExp, szSubjGroup, szSubjOnly );
	else szFile.Format( _T("%s%s%s.EXP"), szPath, fExtraSlash ? _T("") : _T("\\"), szExp );
	szExpFile = szFile;
	
	CString szMsg;
	CWaitCursor crs;
	CMemFile* pMemFile = new CMemFile();

	// write version info
	szMsg.Format( _T("%d"), ::GetAppVersion() );
	WRITE_ITEM( szMsg );

	// user/exp info & device settings (for internal use only - not imported)
	WRITE_ITEM( _T("[DEVICE SETTINGS]") );
	szMsg.Format( _T("Export App - %s"), AfxGetAppName() );
	WRITE_ITEM( szMsg );
	szTemp = (COleDateTime::GetCurrentTime()).Format( _T("%d-%b-%Y") );
	szMsg.Format( _T("Export Date - %s"), szTemp );
	WRITE_ITEM( szMsg );
	::GetCurrentUser( szUserID );
	::GetCurrentUserDesc( szUserDesc );
	szMsg.Format( _T("User ID - %s"), szUserID );
	WRITE_ITEM( szMsg );
	szMsg.Format( _T("User Desc - %s"), szUserDesc );
	WRITE_ITEM( szMsg );
	fVal = ::IsTabletModeOn();
	if( fVal ) { WRITE_ITEM( _T("Device = Tablet") ); }
	else if( ::IsGripperModeOn() ) { WRITE_ITEM( _T("Device = Gripper") ); }
	else { WRITE_ITEM( _T("Device = Mouse") ); }
	fVal = ::IsDesktopModeOn();
	if( fVal ) { WRITE_ITEM( _T("Mapping = Desktop") ); }
	else { WRITE_ITEM( _T("Mapping = Recording Window") ); }
	double dx = 0., dy = 0.;
	::GetDisplayDimensions( dx, dy );
	szMsg.Format( _T("Display Dimensions = %.2f cm (width) x %.2f cm (height)"), dx, dy );
	WRITE_ITEM( szMsg );
	::GetTabletDimensions( dx, dy );
	szMsg.Format( _T("Tablet Dimensions = %.2f cm (width) x %.2f cm (height)"), dx, dy );
	WRITE_ITEM( szMsg );
	WRITE_ITEM( EXPORT_SEPARATOR );

	// Get experiment info and add to file
	if( FAILED( exp.Find( szExp ) ) ) goto ExpError;
	if( !exp.Export( pMemFile ) ) goto ExpError;
//#ifdef _DEBUG
	szMsg.Format( _T("EXPORTED: Experiment %s"), szExp );
	OutputAMessage( szMsg );
//#endif

	// Get experiment proc settings and add to file
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		goto ExpError;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	// use default (if proc set not found)

	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		goto ExpError;
	}
	hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		goto ExpError;
	}

	WRITE_ITEM( _T("[PROCSETTING]") );
	WRITE_ITEM( szExp );
	// time function settings
	pPS->get_Velocity( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_Differentiate( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// recording options
	pPS->get_Randomize( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_RandomizeTrials( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_InstructionAlwaysVisible( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// input device settings
	pPS->get_SamplingRate( &nVal );
	WRITE_ITEM_INT( nVal );
	pPS->get_MinPenPressure( &nVal );
	WRITE_ITEM_INT( nVal );
	pPS->get_DeviceResolution( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// time function settings
	pPS->get_FilterFrequency( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_Decimate( &nVal );
	WRITE_ITEM_INT( nVal );
	pPS->get_RotationBeta( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// time function flags
	pPSF->get_TF_J( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_TF_R( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_TF_A( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_TF_U( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_TF_O( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_FFT( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_Sharpness( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// segmentation flags
	pPSF->get_SEG_F( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_SEG_L( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_SEG_S( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_SEG_O( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_SEG_M( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_SEG_A( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_SEG_V( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// extraction flags
	pPSF->get_EXT_S( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_EXT_3( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_EXT_O( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// consistency checking flags
	pPSF->get_CON_A( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_CON_N( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_CON_M( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_CON_U( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_CON_C( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_CON_S( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_CON_O( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// summarization flags
	pPSF->get_ST_A( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_ST_R( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_ST_Z( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_ST_T( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_ST_S( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_ST_D( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_DiscardAfter( &nVal );
	WRITE_ITEM_INT( nVal );
	pPSF->get_ST_D2( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_DiscardAfter2( &nVal );
	WRITE_ITEM_INT( nVal );
	// sen2word settings
	pPS->get_MaxTimePenup( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinDownSpacing( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinTimePenup( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinLeftPenup( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinLeftSpacing( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinWordWidth( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// segmentation settings
	pPS->get_MaxVertVelocity( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinVertVelocity( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinStrokeDuration( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinStrokeSize( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinStrokeSizeRelativeToMax( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// extraction settings
	pPS->get_StrokeBeginning( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MaxFrequency( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// consis settings
	pPS->get_MinLoopArea( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MinStraightErrorCurved( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_MaxStraightErrorStraight( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_SlantError( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_SpiralIncreaseFactor( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_DiscardLTMin( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_MinReactionTime( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_DiscardGTMax( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_MaxReactionTime( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// run experiment options
	pPSRE->get_TimeoutStart( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPSRE->get_TimeoutRecord( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPSRE->get_TimeoutPenlift( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPSRE->get_TimeoutLatency( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPSRE->get_ProcImmediately( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_MaxRecArea( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_DisplayCharts( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_DisplayRaw( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_DisplayTF( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_DisplaySeg( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_Summarize( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_SummarizeOnly( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_SummarizeSelect( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_Analyze( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_ViewSubjExt( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_ViewSubjCon( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_ViewExpExt( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_ViewExpCon( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_DoQuestionnaire( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_TrialMode( &tm );
	WRITE_ITEM_INT( tm );
	pPSF->get_EXT_2( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// drawing options
	short ls = 0, es = 0;
	DWORD bg = 0, l1, l2, l3, e, mpp;
	pPSRE->GetDrawingOptions( &ls, &es, &bg, &l1, &l2, &l3, &e, &mpp );
	WRITE_ITEM_INT( ls );
	WRITE_ITEM_INT( es );
	WRITE_ITEM_DWORD( bg );
	WRITE_ITEM_DWORD( l1 );
	WRITE_ITEM_DWORD( l2 );
	WRITE_ITEM_DWORD( l3 );
	WRITE_ITEM_DWORD( e );
	WRITE_ITEM_DWORD( mpp );
	// TF discontinuity
	pPS->get_DisCorrection( &nVal );
	WRITE_ITEM_INT( nVal );
	pPS->get_DisFactor( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_DisZInsertPoint( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_DisRelError( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_DisAbsError( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPSF->get_CON_D( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_VaryLineThickness( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_MaxLineThickness( &nVal );
	WRITE_ITEM_INT( nVal );
	// randomization rules
	pPS->get_RandWithRules( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_RandRules( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPS->get_RandBlockCounts( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPS->get_RandSelectionChar( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPS->get_RandSelectionTrials( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	// timfun up-sample factor
	pPS->get_UpSampleFactor( &nVal );
	WRITE_ITEM_INT( nVal );
	// run-experiment settings
	pPSRE->get_QuestAtEnd( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSRE->get_FullScreen( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// summarization data collapsing
	pPS->get_CollapseUDStrokes( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_CollapseStrokes( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_CollapseTrials( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_CollapseMedian( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_UseTilt( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// max pen pressure
	pPS->get_MaxPenPressure( &nVal );
	WRITE_ITEM_INT( nVal );
	// discontinuity relative intersample period error
	pPS->get_DisError( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	pPS->get_DisNotify( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// image processing
	pPS->get_SmoothingIter( &nVal );
	WRITE_ITEM_INT( nVal );
	pPS->get_InkThreshold( &nVal );
	WRITE_ITEM_INT( nVal );
	pPS->get_ImgResolution( &nVal );
	WRITE_ITEM_INT( nVal );
	// summarization settings
	pPSF->get_DiscardPenDownDurMin( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_PenDownDurMin( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// external processing
	ExternalAppType eat;
	pPSRE->get_ExtAppTypeRaw( &eat );
	nVal = (short)eat;
	WRITE_ITEM_INT( nVal );
	pPSRE->get_ExtAppScriptRaw( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPSRE->get_ExtAppTypeTF( &eat );
	nVal = (short)eat;
	WRITE_ITEM_INT( nVal );
	pPSRE->get_ExtAppScriptTF( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPSRE->get_ExtAppTypeSeg( &eat );
	nVal = (short)eat;
	WRITE_ITEM_INT( nVal );
	pPSRE->get_ExtAppScriptSeg( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPSRE->get_ExtAppTypeExt( &eat );
	nVal = (short)eat;
	WRITE_ITEM_INT( nVal );
	pPSRE->get_ExtAppScriptExt( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	pPS->get_SpecifySequence( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPS->get_ConditionSequence( &bstrItem );
	szItem = bstrItem;
	WRITE_ITEM( szItem );
	// remove trailing penlift
	pPS->get_RemoveTrailingPenlift( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// summarize group only
	pPSRE->get_SummarizeOnlyGroup( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// summarize norm db
	pPSRE->get_SummarizeNormDB( &fVal );
	WRITE_ITEM_BOOL( fVal );
	// norm DB stuff
	pPSF->get_NormDBCalcScore( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_NormDBCalcScoreGlobal( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_NormDBNoUpdate( &fVal );
	WRITE_ITEM_BOOL( fVal );
	pPSF->get_NormDBThreshold( &dVal );
	WRITE_ITEM_DOUBLE( dVal );
	// Record termination
	WRITE_ITEM( EXPORT_SEPARATOR );
//#ifdef _DEBUG
	szMsg.Format( _T("EXPORTED: Experiment settings for experiment %s"), szExp );
	OutputAMessage( szMsg );
//#endif

	// export master questionnaire
	hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
						   IID_IMQuestionnaire, (LPVOID*)&pMQ );
	if( SUCCEEDED( hr ) )
	{
		CString szIDs, szQ, szCH;
		BOOL fHdr = FALSE, fPriv = FALSE, fNum = FALSE;
		BSTR bstr = NULL;
		hr = pMQ->ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			pMQ->get_ID( &bstr );
			szIDs = bstr;
			pMQ->get_IsHeader( &fHdr );
			pMQ->get_IsPrivate( &fPriv );
			pMQ->get_IsNumeric( &fNum );
			pMQ->get_Question( &bstr );
			szQ = bstr;
			pMQ->get_ItemNum( &nVal );
			pMQ->get_ColumnHeader( &bstr );
			szCH = bstr;

			if( szIDs != _T("") )
			{
				WRITE_ITEM( _T("[MQUESTIONNAIRE]") );
				WRITE_ITEM( szIDs );
				WRITE_ITEM_BOOL( fHdr );
				WRITE_ITEM( szQ );
				WRITE_ITEM_INT( nVal );
				WRITE_ITEM_BOOL( fPriv );
				WRITE_ITEM_BOOL( fNum );
				WRITE_ITEM( szCH );
				// Record termination
				WRITE_ITEM( EXPORT_SEPARATOR );
			}

			hr = pMQ->GetNext();
		}

		pMQ->Release();
	}

	// Loop through each experiment condition and get a list of IDs
	// NOTE: This is sort of a hack because there are several nested database
	// calls within the conditions.export method
	hr = pEC->FindForExperiment( szExp.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		// Find & write condition
		pEC->get_ConditionID( &bstrItem );
		szCond = bstrItem;
		lstConds.AddTail( szCond );

		// Next record
		hr = pEC->GetNext();
	}
	// Now loop through the list to write the condition information
	pos = lstConds.GetHeadPosition();
	while( pos )
	{
		pos2 = pos;
		szCond = lstConds.GetNext( pos );
		hr2 = cond.Find( szCond );
		if( SUCCEEDED( hr2 ) )
		{
			if( cond.Export( pMemFile, TRUE, fFirst ) ) fFirst = FALSE;
			else goto ExpError;
//#ifdef _DEBUG
			szMsg.Format( _T("EXPORTED: Condition %s"), szCond );
			OutputAMessage( szMsg );
//#endif
		}
		else
		{
			lstConds.RemoveAt( pos2 );
#ifdef _DEBUG
			szMsg.Format( _T("ERROR: Condition %s not found in database"), szCond );
			OutputAMessage( szMsg );
#endif
		}
	}
	// Now loop through each experiment condition again and write each relationship
	hr = pEC->FindForExperiment( szExp.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		// Find & write condition
		pEC->get_ConditionID( &bstrItem );
		szCond = bstrItem;

		// Write ExpCond
		if( lstConds.Find( szCond ) && ( szExp != _T("") ) &&
			( szCond != _T("") ) && ( szCond != HOLDER ) )
		{
			WRITE_ITEM( _T("[EXPERIMENTCONDITION]") );
			WRITE_ITEM( szExp );
			WRITE_ITEM( szCond );
			pEC->get_Replications( &nVal );
			WRITE_ITEM_INT( nVal );
			// Record termination
			WRITE_ITEM( EXPORT_SEPARATOR );
#ifdef _DEBUG
			szMsg.Format( _T("EXPORTED: Experiment/Condition relationship %s/%s"), szExp, szCond );
			OutputAMessage( szMsg );
#endif
		}

		// Next record
		hr = pEC->GetNext();
	}

	// Loop through each experiment member and write each subject, group and the relationship and questionnaire
	fFirst = TRUE;
	hr = pEM->FindForExperiment( szExp.AllocSysString() );
	while( ( fTrials || fMembers ) && SUCCEEDED( hr ) )
	{
		pEM->get_GroupID( &bstrItem );
		szGrp = bstrItem;
		pEM->get_SubjectID( &bstrItem );
		szSubj = bstrItem;
		pEM->get_DeviceRes( &dVal );
 		pEM->get_SamplingRate( &nVal );		// Find & write group
		if( ( !fSO || ( fSO && ( szSubj == szSubjOnly ) && ( szGrp == szSubjGroup ) ) ) && !lstGroups.Find( szGrp ) )
		{
			hr2 = grp.Find( szGrp );
			if( SUCCEEDED( hr2 ) && !grp.Export( pMemFile ) ) goto ExpError;
			lstGroups.AddTail( szGrp );

//#ifdef _DEBUG
			szMsg.Format( _T("EXPORTED: Group %s"), szGrp );
			OutputAMessage( szMsg );
//#endif
		}

		// Find & write subject
		if( !fSO || ( fSO && ( szSubj == szSubjOnly ) && ( szGrp == szSubjGroup ) ) )
		{
			hr2 = subj.Find( szSubj );
			if( SUCCEEDED( hr2 ) )
			{
				if( subj.Export( pMemFile, fFirst ) ) fFirst = FALSE;
				else goto ExpError;
//#ifdef _DEBUG
				szMsg.Format( _T("EXPORTED: Subject %s"), szSubj );
				OutputAMessage( szMsg );
//#endif
			}
		}

		// Write ExpMember & Questionnaire
		if( ( szExp != _T("") ) && ( szGrp != _T("") ) && ( szSubj != _T("") ) &&
			( szGrp != HOLDER ) && ( szSubj != HOLDER ) &&
			( !fSO || ( fSO && ( szSubj == szSubjOnly ) && ( szGrp == szSubjGroup ) ) ) )
		{
			WRITE_ITEM( _T("[EXPERIMENTMEMBER]") );
			WRITE_ITEM( szExp );
			WRITE_ITEM( szGrp );
			WRITE_ITEM( szSubj );
			WRITE_ITEM_DOUBLE( dVal );
			WRITE_ITEM_INT( nVal );
			// Record termination
			WRITE_ITEM( EXPORT_SEPARATOR );

			szMsg.Format( _T("%s%s"), szGrp, szSubj );
			lstEM.AddTail( szMsg );

#ifdef _DEBUG
			szMsg.Format( _T("EXPORTED: Experiment/Group/Subject relationship %s/%s/%s"), szExp, szGrp, szSubj );
			OutputAMessage( szMsg );
#endif
		}

		// Next record
		hr = pEM->GetNext();
	}

	// group questionnaires
	hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
						   IID_IGQuestionnaire, (LPVOID*)&pGQ );
	if( FAILED( hr ) ) pGQ = NULL;
	if( pGQ )
	{
		CString szQID;
		pos = lstGroups.GetHeadPosition();
		while( pos )
		{
			szGrp = lstGroups.GetNext( pos );
			hr = pGQ->FindForGroup( szExp.AllocSysString(), szGrp.AllocSysString() );
			while( SUCCEEDED( hr ) )
			{
				pGQ->get_ID( &bstrItem );
				szQID = bstrItem;
				pGQ->get_ItemNum( &nVal );

				WRITE_ITEM( _T("[GQUESTIONNAIRE]") );
				WRITE_ITEM( szExp );
				WRITE_ITEM( szGrp );
				WRITE_ITEM( szQID );
				WRITE_ITEM_INT( nVal );
				// Record termination
				WRITE_ITEM( EXPORT_SEPARATOR );

				hr = pGQ->GetNext();
			}
		}
	}
	if( pGQ ) pGQ->Release();

	// subject questionnaires
	hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
						   IID_ISQuestionnaire, (LPVOID*)&pSQ );
	if( FAILED( hr ) ) pSQ = NULL;
	if( pSQ )
	{
		CString szQID, szA;
		pos = lstEM.GetHeadPosition();
		while( pos )
		{
			szMsg = lstEM.GetNext( pos );
			if( szMsg.GetLength() == 6 )
			{
				szGrp = szMsg.Left( 3 );
				szSubj = szMsg.Right( 3 );
			}
			else
			{
				szGrp = _T("");
				szSubj = _T("");
			}
			hr = pSQ->FindForSubject( szExp.AllocSysString(), szGrp.AllocSysString(),
									  szSubj.AllocSysString() );
			while( SUCCEEDED( hr ) )
			{
				pSQ->get_ID( &bstrItem );
				szQID = bstrItem;
				pSQ->get_Answer( &bstrItem );
				szA = bstrItem;

				WRITE_ITEM( _T("[SQUESTIONNAIRE]") );
				WRITE_ITEM( szExp );
				WRITE_ITEM( szGrp );
				WRITE_ITEM( szSubj );
				WRITE_ITEM( szQID );
				WRITE_ITEM( szA );
				// Record termination
				WRITE_ITEM( EXPORT_SEPARATOR );

				hr = pSQ->GetNext();
			}
		}
	}
	if( pSQ ) pSQ->Release();

	if( !fExtraSlash ) szPath += _T("\\");
	// Files
	if( fFiles )
	{
		CWaitCursor crs;

		// Files
		// output file
		if( fSO ) szFile.Format( _T("%s%s%s%s-SETTINGS.ZIP"), szPath, szExp, szSubjGroup, szSubjOnly );
		else szFile.Format( _T("%s%s-SETTINGS.ZIP"), szPath, szExp );
		szSettings = szFile;
		szSettingsF = szSettings;
		// compress
		if( !::CompressSupport( szExp, szFile ) )
			BCGPMessageBox( _T("Error in compressing data files.") );
	}

	// Trials
	if( fTrials )
	{
		CWaitCursor crs;

		// Trials
		// output file
		if( fSO ) szFile.Format( _T("%s%s%s%s-TRIALS.ZIP"), szPath, szExp, szSubjGroup, szSubjOnly );
		else szFile.Format( _T("%s%s-TRIALS.ZIP"), szPath, szExp );
		szTrials = szFile;
		szTrialsF = szFile;
		// compress
		if( !::CompressData( szExp, szFile, szSubjOnly, szSubjGroup ) )
			BCGPMessageBox( _T("Error in compressing data files.") );
	}

	// compress resultant files into one
	szFiles = _T("");
	if( ff.FindFile( szSettingsF ) )
	{
		szFiles = szSettingsF;
	}
	if( ff.FindFile( szTrialsF ) )
	{
		if( szFiles != _T("") ) szFiles += _T("=");
		szFiles += szTrialsF;
	}
	if( fSO )
	{
		szFile.Format( _T("%s%s%s%s.MEF"), szPath, szExp, szSubjGroup, szSubjOnly );
		szExpFile.Format( _T("%s%s%s.EXP"), szExp, szSubjGroup, szSubjOnly );
	}
	else
	{
		szFile.Format( _T("%s%s.MEF"), szPath, szExp );
		szExpFile.Format( _T("%s.EXP"), szExp );
	}
	if( !::CompressAll( szPath, szFile, szFiles, szExpFile, pMemFile ) )
		BCGPMessageBox( _T("Error in compressing files.") );

	// remove other files (since they are in combined ZIP)
	if( ff.FindFile( szSettingsF ) ) REMOVE_FILE( szSettingsF );
	if( ff.FindFile( szTrialsF ) ) REMOVE_FILE( szTrialsF );

	// message to user
	if( szSettings != _T("") )
	{
		if( fSO ) szSettings.Format( _T("%s%s%s-SETTINGS.ZIP"), szExp, szSubjGroup, szSubjOnly );
		else szSettings.Format( _T("%s-SETTINGS.ZIP"), szExp );
	}
	else szSettings = _T("No Support");
	if( szTrials != _T("") )
	{
		if( fSO ) szTrials.Format( _T("%s%s%s-TRIALS.ZIP"), szExp, szSubjGroup, szSubjOnly );
		else szTrials.Format( _T("%s-TRIALS.ZIP"), szExp );
	}
	else szTrials = _T("No Trials");
	if( fSO ) szExpFile.Format( _T("%s%s%s.exp"), szExp, szSubjGroup, szSubjOnly );
	else szExpFile.Format( _T("%s.exp"), szExp );
	szFile.MakeUpper();
	szTemp.Format( _T("%s %s successfully exported to:\n\n%s\n\nThis file is encrypted and password-protected.\n\nDo not change the name of the exported file, otherwise imports will not succeed."),
				   fSO ? _T("Subject") : _T("Experiment"), fSO ? szSubjOnly : szExp, szFile );
	OutputAMessage( szTemp );
	if( !fWizard )
	{
		BCGPMessageBox( szTemp );
		if( BCGPMessageBox( _T("Would you like to open windows explorer to this export file?"), MB_YESNO ) == IDYES )
		{
			CString szRoot;
			nPos = szFile.ReverseFind( '\\' );
			if( nPos != -1 )
			{
				szRoot = szFile.Left( nPos );
				szTemp.Format( _T("explorer /ROOT,%s,/SELECT,%s"), szRoot, szFile );
				WinExec( szTemp, SW_SHOW );
			}
		}
	}
	else szWMsg += szTemp;
	goto Cleanup;

ExpError:
	if( fWizard )
	{
		szTemp.Format( _T("There was an error in the export process for subject %s."), szSubjOnly );
		szWMsg += szTemp;
	}
	else BCGPMessageBox( _T("There was an error in the export process.") );
	fFailed = TRUE;

Cleanup:
	BYTE* data = pMemFile->Detach();
	if( data ) delete data;
	pMemFile->Close();
	delete pMemFile;
	::SetIsPrivacyOn( fPrivacyOn );
	if( pPSF ) pPSF->Release();
	if( pPSRE ) pPSRE->Release();
	if( pPS ) pPS->Release();
	if( pEC ) pEC->Release();
	if( pEM ) pEM->Release();

	return !fFailed;
}

BOOL Experiments::Import( CString& szExpID, BOOL& fTrials, CStringList* pListImported,
						  BOOL fAskBackup, CString szImportFile )
{
	if( !pListImported ) return FALSE;

	CString szData = _T("File - Import");
	LogToFile( szData, LOG_UI );

	CString szFile, szLine, szTemp, szMID1, szMID2, szMID3, szSrc, szFiles,
			szTrials, szPath, szExp, szMID4, szFileName, szMsg, szID, szDesc;
	BSTR bstr = NULL;
	CStdioFile f;
	CFile* pFile = NULL;
	CFileFind ff;
	double dVal = 0.0;
	short nVal = 0, nCnt = 0;
	BOOL fVal = FALSE, fFiles = FALSE, fAddedExp = FALSE, fDataAdded = FALSE,
		 fOverWrite = FALSE, fOWAllYes = FALSE, fOWAllNo = FALSE, fScan = TRUE, fRet = FALSE;
	BOOL fSummary = FALSE, fZip = FALSE, fSkipUI = FALSE, fDelimProblem = FALSE;
	COleDateTime dt;
	HRESULT hr;
	int nPos = 0, nItems = 0, nItem = 0;
	POSITION pos = NULL;
	TrialsMode tm;
	CWaitCursor crs;

	// delimiter
	CString szDelim = ::GetDelimiter();

	// backup option first
	if( fAskBackup )
	{
		int nRes = BCGPMessageBox( _T("Do you want to backup the database files first?\n\nTo restore: File >Backup >Restore Database"), MB_YESNOCANCEL );
		if( nRes == IDCANCEL ) return FALSE;
		else if( ( nRes == IDYES ) && !NSMUsers::BackupDB( FALSE ) ) return FALSE;
	}

	// reset overwrite dialog
	OverwriteDlg::Reset();

	// import dialog (unless file specified)
	if( szImportFile == _T("") )
	{
		ImportDialog d( AfxGetApp()->m_pMainWnd );
		if( d.DoModal() == IDCANCEL ) return FALSE;
		szFile = d.m_szFile;
		nPos = szFile.ReverseFind( '\\' );
		szPath = szFile.Left( nPos );
		szFileName = d.m_szFileName;
		fFiles = d.m_fFiles;
		fTrials = d.m_fTrials;
		fSummary = d.m_fSummary;
		fZip = d.m_fZip;
	}
	else
	{
		fSkipUI = TRUE;
		szFile = szImportFile;
		nPos = szFile.ReverseFind( '\\' );
		szPath = szFile.Left( nPos );
		szFileName = szFile.Mid( nPos + 1 );
		nPos = szFileName.ReverseFind( '.' );
		szFileName = szFileName.Left( nPos );
		fFiles = TRUE;
		fTrials = TRUE;
		fSummary = FALSE;
		CString szExt = szFile.Right( 3 );
		szExt.MakeUpper();
		if( ( szExt == _T("ZIP") ) || ( szExt == _T("MEF") ) ) fZip = TRUE;
	}
	CString szFilesName, szTrialsName;
	if( fFiles )
	{
		szFilesName.Format( _T("%s-SETTINGS.ZIP"), szFileName );
		szFiles = szPath;
		szFiles += _T("\\");
		szFiles += szFilesName;
	}
	if( fTrials )
	{
		szTrialsName.Format( _T("%s-TRIALS.ZIP"), szFileName );
		szTrials = szPath;
		szTrials += _T("\\");
		szTrials += szTrialsName;
	}

	// if zip file, need to extract the EXP file into memory file
	// cleanup of file & buffer must be done here (this function)
	CMemFile* pMemFile = NULL;
	if( fZip )
	{
		crs.Restore();
		Progress::EnableProgress( 1 );
		Progress::SetProgress( 1, _T("IMPORT - Reading input file...") );
		CString szExp;
		szExp.Format( _T("%s.EXP"), szFileName );
		pMemFile = ::ExtractToMemoryFile( szFile, szExp );
		if( !pMemFile )
		{
			Progress::DisableProgress();
			return FALSE;
		}

		// need to extract the trials/settings files
		if( fFiles ) ::UncompressFile( szFile, szFilesName, szPath );
		if( fTrials ) ::UncompressFile( szFile, szTrialsName, szPath );
		Progress::DisableProgress();
	}

	// Open file for reading (if not zip)
	if( !fZip && !f.Open( szFile, CFile::modeRead | CFile::typeText ) )
	{
		BCGPMessageBox( _T("Unable to open import file.") );
		return FALSE;
	}
	szSrc = szFile;

	// Create objects
	IProcSetting* pPS = NULL;
	IProcSetFlags* pPSF = NULL;
	IProcSetRunExp* pPSRE = NULL;
	IProcSetting* pPS2 = NULL;
	IProcSetFlags* pPSF2 = NULL;
	IProcSetRunExp* pPSRE2 = NULL;
	IExperimentMember* pEM = NULL;
	IExperimentCondition* pEC = NULL;
	IStimulusElement* pSE = NULL;
	IStimulusTarget* pST = NULL;
	IMQuestionnaire* pMQ = NULL;
	IMQuestionnaire* pMQ2 = NULL;
	IGQuestionnaire* pGQ = NULL;
	IGQuestionnaire* pGQ2 = NULL;
	ISQuestionnaire* pSQ = NULL;
	ISQuestionnaire* pSQ2 = NULL;

	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		goto ExpError;
	}
	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		goto ExpError;
	}
	hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCOND_ERR );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		goto ExpError;
	}
	hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
							 IID_IStimulusElement, (LPVOID*)&pSE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusElement object.") );
		goto ExpError;
	}
	hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
							 IID_IStimulusTarget, (LPVOID*)&pST );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
							IID_IMQuestionnaire, (LPVOID*)&pMQ );
	if( FAILED( hr ) )
	{
		szData.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szData );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
							IID_IGQuestionnaire, (LPVOID*)&pGQ );
	if( FAILED( hr ) )
	{
		szData.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szData );
		goto ExpError;
	}
	hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
							IID_ISQuestionnaire, (LPVOID*)&pSQ );
	if( FAILED( hr ) )
	{
		szData.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szData );
		goto ExpError;
	}

	// FILE POINTER: points to mem file or stdiofile so we can use both w/o checking
	// checks come in the way of IsKindOf...used specifically for ReadString
	// which does not exist for CMemFile
	if( fZip ) pFile = pMemFile;
	else pFile = &f;

StartHere:
	// if scanning, we're just counting the # of items to import
	// if we've scanned, then we need to setup the progress meter
	if( fScan )
	{
		Progress::EnableProgress( 1 );
		Progress::SetProgress( 1, _T("IMPORT - Scanning for number of items to import...") );
	}
	else
	{
		// message for # of items to import (first)
		szMsg.Format( _T("There are %d items to import."), nItems );

		// get uncompressed data size (trials only) and confirm disk space and user to continue
		if( fTrials )
		{
			crs.Restore();

			ULONGLONG lSize = UncompressSize( szTrials );
			CString szPath, szTemp;
			::GetDataPathRoot( szPath );
			__int64 i64FreeBytesToCaller, i64TotalBytes, i64FreeBytes;
			::GetDiskFreeSpaceEx( szPath, (PULARGE_INTEGER)&i64FreeBytesToCaller,
				(PULARGE_INTEGER)&i64TotalBytes,
				(PULARGE_INTEGER)&i64FreeBytes);
			double dSizeMB1 = lSize / 1000000., dSizeMB2 = i64FreeBytes / 1000000.;

			szTemp.Format( _T("\n\nThe amount of disk space required for the trials is %I64d bytes (%.2f MB).\nYou have %I64d bytes (%.2f MB) available.\n\n* You will need sufficient disk space for this to install correctly.\n* Large sets of data can take several minutes to decompress.\n\nDo you want to continue?"), lSize, dSizeMB1, i64FreeBytes, dSizeMB2 );
			szMsg += szTemp;
		}
		else szMsg += _T("\n\nDo you want to continue?");
		if( !fSkipUI && ( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) )
		{
			::DataHasNotChanged();
			goto ExpError;
		}
		Progress::EnableProgress( nItems );
	}

	// 1st line should always be the app version exported with (2.85 and higher)
	READ_STRING( pFile, szLine );
	nPos = atoi( szLine );
	int nVer = nPos;
	szTemp.Format( _T("Version - %d\n"), nVer );

	// Read through file and update db depending on type of item and where necessary
	while( READ_STRING( pFile, szLine ) )
	{
		if( szLine == _T("[DEVICE SETTINGS]") )
		{
			if( fSummary )
			{
				while( TRUE )
				{
					fRet = READ_STRING( pFile, szLine );
					if( !fRet ) goto ExpError;
					if( szLine == EXPORT_SEPARATOR ) break;
					// to support older versions that didnt use separator
					if( szLine == TAG_EXPERIMENT )
					{
						pFile->Seek( -( szLine.GetLength() + 2 ), CFile::current );
						break;
					}
					szTemp += szLine;
					szTemp += _T("\n");
				}
				if( !fScan )
				{
					nItem++;
					dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
					szMsg.Format( _T("IMPORT Summary - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
					Progress::SetProgress( nItem, szMsg );
					szTemp += _T("\nContinue with import?");
					if( BCGPMessageBox( szTemp, MB_YESNO ) == IDNO ) {
						Progress::DisableProgress();
						goto Cleanup;
					}
				}
				else nItems++;
			}
		}
		else if( szLine == TAG_EXPERIMENT )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Experiment - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			Experiments exp;
			if( exp.Import( (CStdioFile*)pFile, szExpID, fOWAllYes, fOWAllNo, fScan ) )
			{
				exp.GetDescription( szDesc );
				if( ( szExpID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
				if( fScan ) nItems++;
				fAddedExp = TRUE;
				fDataAdded = TRUE;
				crs.Restore();
			}
			else goto ExpError;
		}
		else if( szLine == _T("[PROCSETTING]") )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Experiment Settings - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			// 2nd set of proc settings for overwrite detail comparison
			if( !pPS2 )
			{
				hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									   IID_IProcSetting, (LPVOID*)&pPS2 );
				if( FAILED( hr ) )
				{
					BCGPMessageBox( IDS_PS_ERR );
					goto ExpError;
				}
				hr = pPS2->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF2 );
				if( FAILED( hr ) )
				{
					BCGPMessageBox( IDS_PS_ERR );
					goto ExpError;
				}
				hr = pPS2->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE2 );
				if( FAILED( hr ) )
				{
					BCGPMessageBox( IDS_PS_ERR );
					goto ExpError;
				}
			}

			CString szID, szFile1, szFile2, szTemp;
			BOOL fFiles = FALSE, fComp = FALSE, fOverWrite = FALSE;
			// ID
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			hr = pPS->Find( szMID1.AllocSysString() );
			if( SUCCEEDED( hr ) ) fComp = TRUE;
			pPS->put_ExperimentID( szMID1.AllocSysString() );
			pPS2->put_ExperimentID( szMID1.AllocSysString() );
			szID = szMID1;
			szTemp.Format( _T("IMPORT: Experiment Settings %s"), szMID1 );
			if( !fScan ) OutputAMessage( szTemp );
			// Velocity
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_Velocity( fVal );
			pPS2->put_Velocity( fVal );
			// Differentiate
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_Differentiate( fVal );
			pPS2->put_Differentiate( fVal );
			// Randomize
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_Randomize( fVal );
			pPS2->put_Randomize( fVal );
			// Randomize Trials
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_RandomizeTrials( fVal );
			pPS2->put_RandomizeTrials( fVal );
			// Instruction Always Visible
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_InstructionAlwaysVisible( fVal );
			pPS2->put_InstructionAlwaysVisible( fVal );
			// Sampling Rate
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_SamplingRate( nVal );
			pPS2->put_SamplingRate( nVal );
			// Min Pen Pressure
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_MinPenPressure( nVal );
			pPS2->put_MinPenPressure( nVal );
			// Divice Res
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_DeviceResolution( dVal );
			pPS2->put_DeviceResolution( dVal );
			// Filter Freq
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_FilterFrequency( dVal );
			pPS2->put_FilterFrequency( dVal );
			// Decimate
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_Decimate( nVal );
			pPS2->put_Decimate( nVal );
			// Rotation Beta
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_RotationBeta( dVal );
			pPS2->put_RotationBeta( dVal );
			// TF J
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_TF_J( fVal );
			pPSF2->put_TF_J( fVal );
			// TF R
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_TF_R( fVal );
			pPSF2->put_TF_R( fVal );
			// TF A
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_TF_A( fVal );
			pPSF2->put_TF_A( fVal );
			// TF U
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_TF_U( fVal );
			pPSF2->put_TF_U( fVal );
			// TF O
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_TF_O( fVal );
			pPSF2->put_TF_O( fVal );
			// TF FFT
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_FFT( fVal );
			pPS2->put_FFT( fVal );
			// TF Sharpness
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_Sharpness( dVal );
			pPS2->put_Sharpness( dVal );
			// SEG F
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_F( fVal );
			pPSF2->put_SEG_F( fVal );
			// SEG L
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_L( fVal );
			pPSF2->put_SEG_L( fVal );
			// SEG S
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_S( fVal );
			pPSF2->put_SEG_S( fVal );
			// SEG O
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_O( fVal );
			pPSF2->put_SEG_O( fVal );
			// SEG M
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_M( fVal );
			pPSF2->put_SEG_M( fVal );
			// SEG A
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_A( fVal );
			pPSF2->put_SEG_A( fVal );
			// SEG V
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_SEG_V( fVal );
			pPSF2->put_SEG_V( fVal );
			// EXT S
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_EXT_S( fVal );
			pPSF2->put_EXT_S( fVal );
			// EXT 3
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_EXT_3( fVal );
			pPSF2->put_EXT_3( fVal );
			// EXT O
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_EXT_O( fVal );
			pPSF2->put_EXT_O( fVal );
			// CON A
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_A( fVal );
			pPSF2->put_CON_A( fVal );
			// CON N
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_N( fVal );
			pPSF2->put_CON_N( fVal );
			// CON M
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_M( fVal );
			pPSF2->put_CON_M( fVal );
			// CON U
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_U( fVal );
			pPSF2->put_CON_U( fVal );
			// CON C
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_C( fVal );
			pPSF2->put_CON_C( fVal );
			// CON S
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_S( fVal );
			pPSF2->put_CON_S( fVal );
			// CON O
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_O( fVal );
			pPSF2->put_CON_O( fVal );
			// ST A
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_A( fVal );
			pPSF2->put_ST_A( fVal );
			// ST R
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_R( fVal );
			pPSF2->put_ST_R( fVal );
			// ST Z
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_Z( fVal );
			pPSF2->put_ST_Z( fVal );
			// ST T
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_T( fVal );
			pPSF2->put_ST_T( fVal );
			// ST S
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_S( fVal );
			pPSF2->put_ST_S( fVal );
			// ST D
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_D( fVal );
			pPSF2->put_ST_D( fVal );
			// Discard After
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSF->put_DiscardAfter( nVal );
			pPSF2->put_DiscardAfter( nVal );
			// ST D2
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_ST_D2( fVal );
			pPSF2->put_ST_D2( fVal );
			// Discard After 2
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSF->put_DiscardAfter2( nVal );
			pPSF2->put_DiscardAfter2( nVal );

			// sen2word settings
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MaxTimePenup( dVal );
			pPS2->put_MaxTimePenup( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinDownSpacing( dVal );
			pPS2->put_MinDownSpacing( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinTimePenup( dVal );
			pPS2->put_MinTimePenup( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinLeftPenup( dVal );
			pPS2->put_MinLeftPenup( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinLeftSpacing( dVal );
			pPS2->put_MinLeftSpacing( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinWordWidth( dVal );
			pPS2->put_MinWordWidth( dVal );
			// segmentation settings
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MaxVertVelocity( dVal );
			pPS2->put_MaxVertVelocity( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinVertVelocity( dVal );
			pPS2->put_MinVertVelocity( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinStrokeDuration( dVal );
			pPS2->put_MinStrokeDuration( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinStrokeSize( dVal );
			pPS2->put_MinStrokeSize( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinStrokeSizeRelativeToMax( dVal );
			pPS2->put_MinStrokeSizeRelativeToMax( dVal );
			// extraction settings
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_StrokeBeginning( dVal );
			pPS2->put_StrokeBeginning( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MaxFrequency( dVal );
			pPS2->put_MaxFrequency( dVal );
			// consis settings
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinLoopArea( dVal );
			pPS2->put_MinLoopArea( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinStraightErrorCurved( dVal );
			pPS2->put_MinStraightErrorCurved( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MaxStraightErrorStraight( dVal );
			pPS2->put_MaxStraightErrorStraight( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_SlantError( dVal );
			pPS2->put_SlantError( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_SpiralIncreaseFactor( dVal );
			pPS2->put_SpiralIncreaseFactor( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_DiscardLTMin( fVal );
			pPS2->put_DiscardLTMin( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MinReactionTime( dVal );
			pPS2->put_MinReactionTime( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_DiscardGTMax( fVal );
			pPS2->put_DiscardGTMax( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_MaxReactionTime( dVal );
			pPS2->put_MaxReactionTime( dVal );
			// run experiment options
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPSRE->put_TimeoutStart( dVal );
			pPSRE2->put_TimeoutStart( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPSRE->put_TimeoutRecord( dVal );
			pPSRE2->put_TimeoutRecord( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPSRE->put_TimeoutPenlift( dVal );
			pPSRE2->put_TimeoutPenlift( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPSRE->put_TimeoutLatency( dVal );
			pPSRE2->put_TimeoutLatency( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_ProcImmediately( fVal );
			pPSRE2->put_ProcImmediately( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_MaxRecArea( fVal );
			pPSRE2->put_MaxRecArea( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_DisplayCharts( fVal );
			pPSRE2->put_DisplayCharts( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_DisplayRaw( fVal );
			pPSRE2->put_DisplayRaw( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_DisplayTF( fVal );
			pPSRE2->put_DisplayTF( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_DisplaySeg( fVal );
			pPSRE2->put_DisplaySeg( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_Summarize( fVal );
			pPSRE2->put_Summarize( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_SummarizeOnly( fVal );
			pPSRE2->put_SummarizeOnly( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_SummarizeSelect( fVal );
			pPSRE2->put_SummarizeSelect( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_Analyze( fVal );
			pPSRE2->put_Analyze( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_ViewSubjExt( fVal );
			pPSRE2->put_ViewSubjExt( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_ViewSubjCon( fVal );
			pPSRE2->put_ViewSubjCon( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_ViewExpExt( fVal );
			pPSRE2->put_ViewExpExt( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_ViewExpCon( fVal );
			pPSRE2->put_ViewExpCon( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_DoQuestionnaire( fVal );
			pPSRE2->put_DoQuestionnaire( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			tm = (TrialsMode)nVal;
			pPS->put_TrialMode( tm );
			pPS2->put_TrialMode( tm );
			// EXT 2
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
 			pPSF->put_EXT_2( fVal );
			pPSF2->put_EXT_2( fVal );
			// drawing options
			short ls = 0, es = 0;
			DWORD bg = 0, l1, l2, l3, e, mpp;
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			ls = atoi( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			es = atoi( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			bg = (DWORD)atol( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			l1 = (DWORD)atol( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			l2 = (DWORD)atol( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			l3 = (DWORD)atol( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			e = (DWORD)atol( szLine );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			mpp = (DWORD)atol( szLine );
			pPSRE->SetDrawingOptions( ls, es, bg, l1, l2, l3, e, mpp );
			pPSRE2->SetDrawingOptions( ls, es, bg, l1, l2, l3, e, mpp );
			// TF Discontinuity
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_DisCorrection( nVal );
			pPS2->put_DisCorrection( nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_DisFactor( dVal );
			pPS2->put_DisFactor( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_DisZInsertPoint( dVal );
			pPS2->put_DisZInsertPoint( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_DisRelError( dVal );
			pPS2->put_DisRelError( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_DisAbsError( dVal );
			pPS2->put_DisAbsError( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_CON_D( fVal );
			pPSF2->put_CON_D( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_VaryLineThickness( fVal );
			pPSRE2->put_VaryLineThickness( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSRE->put_MaxLineThickness( nVal );
			pPSRE2->put_MaxLineThickness( nVal );
			// randomization rules
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_RandWithRules( fVal );
			pPS2->put_RandWithRules( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPS->put_RandRules( szLine.AllocSysString() );
			pPS2->put_RandRules( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPS->put_RandBlockCounts( szLine.AllocSysString() );
			pPS2->put_RandBlockCounts( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPS->put_RandSelectionChar( szLine.AllocSysString() );
			pPS2->put_RandSelectionChar( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPS->put_RandSelectionTrials( szLine.AllocSysString() );
			pPS2->put_RandSelectionTrials( szLine.AllocSysString() );
			// timfun up-sample factor
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_UpSampleFactor( nVal );
			pPS2->put_UpSampleFactor( nVal );
			// run-experiment settings
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_QuestAtEnd( fVal );
			pPSRE2->put_QuestAtEnd( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_FullScreen( fVal );
			pPSRE2->put_FullScreen( fVal );
			// summarization data collapsing
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_CollapseUDStrokes( fVal );
			pPS2->put_CollapseUDStrokes( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_CollapseStrokes( fVal );
			pPS2->put_CollapseStrokes( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_CollapseTrials( fVal );
			pPS2->put_CollapseTrials( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_CollapseMedian( fVal );
			pPS2->put_CollapseMedian( fVal );
			// tilt
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_UseTilt( fVal );
			pPS2->put_UseTilt( fVal );
			// max pen pressure
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_MaxPenPressure( nVal );
			pPS2->put_MaxPenPressure( nVal );
			// discontinuity relative intersample period error
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPS->put_DisError( dVal );
			pPS2->put_DisError( dVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_DisNotify( fVal );
			pPS2->put_DisNotify( fVal );
			// image processing
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_SmoothingIter( nVal );
			pPS2->put_SmoothingIter( nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_InkThreshold( nVal );
			pPS2->put_InkThreshold( nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPS->put_ImgResolution( nVal );
			pPS2->put_ImgResolution( nVal );
			// SUMMARIZATION: DiscardPenDownDurMin
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_DiscardPenDownDurMin( fVal );
			pPSF2->put_DiscardPenDownDurMin( fVal );
			// PenDownDurMin
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPSF->put_PenDownDurMin( dVal );
			pPSF2->put_PenDownDurMin( dVal );
			// EXTERNAL APPLICATION PROCESSING
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSRE->put_ExtAppTypeRaw( (ExternalAppType)nVal );
			pPSRE2->put_ExtAppTypeRaw( (ExternalAppType)nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPSRE->put_ExtAppScriptRaw( szLine.AllocSysString() );
			pPSRE2->put_ExtAppScriptRaw( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSRE->put_ExtAppTypeTF( (ExternalAppType)nVal );
			pPSRE2->put_ExtAppTypeTF( (ExternalAppType)nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPSRE->put_ExtAppScriptTF( szLine.AllocSysString() );
			pPSRE2->put_ExtAppScriptTF( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSRE->put_ExtAppTypeSeg( (ExternalAppType)nVal );
			pPSRE2->put_ExtAppTypeSeg( (ExternalAppType)nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPSRE->put_ExtAppScriptSeg( szLine.AllocSysString() );
			pPSRE2->put_ExtAppScriptSeg( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			nVal = atoi( szLine );
			pPSRE->put_ExtAppTypeExt( (ExternalAppType)nVal );
			pPSRE2->put_ExtAppTypeExt( (ExternalAppType)nVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPSRE->put_ExtAppScriptExt( szLine.AllocSysString() );
			pPSRE2->put_ExtAppScriptExt( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_SpecifySequence( fVal );
			pPS2->put_SpecifySequence( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			pPS->put_ConditionSequence( szLine.AllocSysString() );
			pPS2->put_ConditionSequence( szLine.AllocSysString() );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPS->put_RemoveTrailingPenlift( fVal );
			pPS2->put_RemoveTrailingPenlift( fVal );
			// summarize group only & norm db
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_SummarizeOnlyGroup( fVal );
			pPSRE2->put_SummarizeOnlyGroup( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSRE->put_SummarizeNormDB( fVal );
			pPSRE2->put_SummarizeNormDB( fVal );
			// Norm DB stuff
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_NormDBCalcScore( fVal );
			pPSF2->put_NormDBCalcScore( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_NormDBCalcScoreGlobal( fVal );
			pPSF2->put_NormDBCalcScoreGlobal( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			fVal = atoi( szLine );
			pPSF->put_NormDBNoUpdate( fVal );
			pPSF2->put_NormDBNoUpdate( fVal );
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportProcSet;
			dVal = atof( szLine );
			pPSF->put_NormDBThreshold( dVal );
			pPSF2->put_NormDBThreshold( dVal );
ImportProcSet:
			if( fScan )
			{
				nItems++;
				continue;
			}
			// if a record already exists, save imported one to temp database
			// and dump records from both for file compares to present to user
			::GetDataPathRoot( szFile1 );	// original
			::GetDataPathRoot( szFile2 );	// imported
			szFile1 += _T("\\comp1.txt");
			szFile2 += _T("\\comp2.txt");
			BOOL fDuplicate = FALSE;
			if( !fOWAllYes && !fOWAllNo && fComp )
			{
				// dump found record in normal db table
				BOOL fRes = pPS->DumpRecord( szID.AllocSysString(), szFile1.AllocSysString() );
				// add (or find) imported item into temp table (new object set as temp)
				pPS2->SetTemp( TRUE );
				HRESULT hr2 = pPS2->Find( szID.AllocSysString() );	// just in case the files were not deleted before
				if( FAILED( hr2 ) ) hr2 = pPS2->Add();
				// dump temp record just added
				fRes = pPS2->DumpRecord( szID.AllocSysString(), szFile2.AllocSysString() );
				// remove temp db tables & clear temp flag
				pPS2->RemoveTemp();
				pPS2->SetTemp( FALSE );
				// we have files
				fFiles = TRUE;
				// compare files to see if we need to ask (might be there but might be duplicate)
				CStdioFile f1, f2;
				if( f1.Open( szFile1, CFile::modeRead ) )
				{
					if( f2.Open( szFile2, CFile::modeRead ) )
					{
						CString sz1, sz2;
						while( f1.ReadString( sz1 ) )
						{
							if( !f2.ReadString( sz2 ) ) goto Overwrite;
							if( sz1 != sz2 ) goto Overwrite;
						}
						fDuplicate = TRUE;
						f2.Close();
					}
					f1.Close();
				}
			}
Overwrite:
			if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
			{
				OverwriteDlg od( OW_SETTINGS, szID, szFile1, szFile2 );
				od.DoModal();
				if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
				else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
				else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
				else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
				else if( od.m_nResult == OW_CANCEL ) goto ExpError;
				crs.Restore();
			}
			else fOverWrite = fOWAllYes;
			// cleanup of comparison files if applicable
			if( fFiles )
			{
				CFileFind ff;
				if( ff.FindFile( szFile1 ) ) REMOVE_FILE( szFile1 );
				if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
			}
			// exists & overwrite - modify
			if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
			{
				if( fComp )
				{
					// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
					// then must snag values from temp copy (of new data) and reset orig then modify
					hr = pPS->Find( szID.AllocSysString() );

					pPS2->get_Velocity( &fVal );
					pPS->put_Velocity( fVal );
					pPS2->get_Differentiate( &fVal );
					pPS->put_Differentiate( fVal );
					pPS2->get_Randomize( &fVal );
					pPS->put_Randomize( fVal );
					pPS2->get_RandomizeTrials( &fVal );
					pPS->put_RandomizeTrials( fVal );
					pPS2->get_InstructionAlwaysVisible( &fVal );
					pPS->put_InstructionAlwaysVisible( fVal );
					pPS2->get_SamplingRate( &nVal );
					pPS->put_SamplingRate( nVal );
					pPS2->get_MinPenPressure( &nVal );
					pPS->put_MinPenPressure( nVal );
					pPS2->get_DeviceResolution( &dVal );
					pPS->put_DeviceResolution( dVal );
					pPS2->get_FilterFrequency( &dVal );
					pPS->put_FilterFrequency( dVal );
					pPS2->get_Decimate( &nVal );
					pPS->put_Decimate( nVal );
					pPS2->get_RotationBeta( &dVal );
					pPS->put_RotationBeta( dVal );
					pPSF2->get_TF_J( &fVal );
					pPSF->put_TF_J( fVal );
					pPSF2->get_TF_R( &fVal );
					pPSF->put_TF_R( fVal );
					pPSF2->get_TF_A( &fVal );
					pPSF->put_TF_A( fVal );
					pPSF2->get_TF_U( &fVal );
					pPSF->put_TF_U( fVal );
					pPSF2->get_TF_O( &fVal );
					pPSF->put_TF_O( fVal );
					pPS2->get_FFT( &fVal );
					pPS->put_FFT( fVal );
					pPS2->get_Sharpness( &dVal );
					pPS->put_Sharpness( dVal );
					pPSF2->get_SEG_F( &fVal );
					pPSF->put_SEG_F( fVal );
					pPSF2->get_SEG_L( &fVal );
					pPSF->put_SEG_L( fVal );
					pPSF2->get_SEG_S( &fVal );
					pPSF->put_SEG_S( fVal );
					pPSF2->get_SEG_O( &fVal );
					pPSF->put_SEG_O( fVal );
					pPSF2->get_SEG_M( &fVal );
					pPSF->put_SEG_M( fVal );
					pPSF2->get_SEG_A( &fVal );
					pPSF->put_SEG_A( fVal );
					pPSF2->get_SEG_V( &fVal );
					pPSF->put_SEG_V( fVal );
					pPSF2->get_EXT_S( &fVal );
					pPSF->put_EXT_S( fVal );
					pPSF2->get_EXT_3( &fVal );
					pPSF->put_EXT_3( fVal );
					pPSF2->get_EXT_O( &fVal );
					pPSF->put_EXT_O( fVal );
					pPSF2->get_CON_A( &fVal );
					pPSF->put_CON_A( fVal );
					pPSF2->get_CON_N( &fVal );
					pPSF->put_CON_N( fVal );
					pPSF2->get_CON_M( &fVal );
					pPSF->put_CON_M( fVal );
					pPSF2->get_CON_U( &fVal );
					pPSF->put_CON_U( fVal );
					pPSF2->get_CON_C( &fVal );
					pPSF->put_CON_C( fVal );
					pPSF2->get_CON_S( &fVal );
					pPSF->put_CON_S( fVal );
					pPSF2->get_CON_O( &fVal );
					pPSF->put_CON_O( fVal );
					pPSF2->get_ST_A( &fVal );
					pPSF->put_ST_A( fVal );
					pPSF2->get_ST_R( &fVal );
					pPSF->put_ST_R( fVal );
					pPSF2->get_ST_Z( &fVal );
					pPSF->put_ST_Z( fVal );
					pPSF2->get_ST_T( &fVal );
					pPSF->put_ST_T( fVal );
					pPSF2->get_ST_S( &fVal );
					pPSF->put_ST_S( fVal );
					pPSF2->get_ST_D( &fVal );
					pPSF->put_ST_D( fVal );
					pPSF2->get_DiscardAfter( &nVal );
					pPSF->put_DiscardAfter( nVal );
					pPSF2->get_ST_D2( &fVal );
					pPSF->put_ST_D2( fVal );
					pPSF2->get_DiscardAfter2( &nVal );
					pPSF->put_DiscardAfter2( nVal );
					pPSF2->get_DiscardPenDownDurMin( &fVal );
					pPSF->put_DiscardPenDownDurMin( fVal );
					pPSF2->get_PenDownDurMin( &dVal );
					pPSF->put_PenDownDurMin( dVal );
					pPS2->get_MaxTimePenup( &dVal );
					pPS->put_MaxTimePenup( dVal );
					pPS2->get_MinDownSpacing( &dVal );
					pPS->put_MinDownSpacing( dVal );
					pPS2->get_MinTimePenup( &dVal );
					pPS->put_MinTimePenup( dVal );
					pPS2->get_MinLeftPenup( &dVal );
					pPS->put_MinLeftPenup( dVal );
					pPS2->get_MinLeftSpacing( &dVal );
					pPS->put_MinLeftSpacing( dVal );
					pPS2->get_MinWordWidth( &dVal );
					pPS->put_MinWordWidth( dVal );
					pPS2->get_MaxVertVelocity( &dVal );
					pPS->put_MaxVertVelocity( dVal );
					pPS2->get_MinVertVelocity( &dVal );
					pPS->put_MinVertVelocity( dVal );
					pPS2->get_MinStrokeDuration( &dVal );
					pPS->put_MinStrokeDuration( dVal );
					pPS2->get_MinStrokeSize( &dVal );
					pPS->put_MinStrokeSize( dVal );
					pPS2->get_MinStrokeSizeRelativeToMax( &dVal );
					pPS->put_MinStrokeSizeRelativeToMax( dVal );
					pPS2->get_StrokeBeginning( &dVal );
					pPS->put_StrokeBeginning( dVal );
					pPS2->get_MaxFrequency( &dVal );
					pPS->put_MaxFrequency( dVal );
					pPS2->get_MinLoopArea( &dVal );
					pPS->put_MinLoopArea( dVal );
					pPS2->get_MinStraightErrorCurved( &dVal );
					pPS->put_MinStraightErrorCurved( dVal );
					pPS2->get_MaxStraightErrorStraight( &dVal );
					pPS->put_MaxStraightErrorStraight( dVal );
					pPS2->get_SlantError( &dVal );
					pPS->put_SlantError( dVal );
					pPS2->get_SpiralIncreaseFactor( &dVal );
					pPS->put_SpiralIncreaseFactor( dVal );
					pPS2->get_DiscardLTMin( &fVal );
					pPS->put_DiscardLTMin( fVal );
					pPS2->get_MinReactionTime( &dVal );
					pPS->put_MinReactionTime( dVal );
					pPS2->get_DiscardGTMax( &fVal );
					pPS->put_DiscardGTMax( fVal );
					pPS2->get_MaxReactionTime( &dVal );
					pPS->put_MaxReactionTime( dVal );
					pPSRE2->get_TimeoutStart( &dVal );
					pPSRE->put_TimeoutStart( dVal );
					pPSRE2->get_TimeoutRecord( &dVal );
					pPSRE->put_TimeoutRecord( dVal );
					pPSRE2->get_TimeoutPenlift( &dVal );
					pPSRE->put_TimeoutPenlift( dVal );
					pPSRE2->get_TimeoutLatency( &dVal );
					pPSRE->put_TimeoutLatency( dVal );
					pPSRE2->get_ProcImmediately( &fVal );
					pPSRE->put_ProcImmediately( fVal );
					pPSRE2->get_MaxRecArea( &fVal );
					pPSRE->put_MaxRecArea( fVal );
					pPSRE2->get_DisplayCharts( &fVal );
					pPSRE->put_DisplayCharts( fVal );
					pPSRE2->get_DisplayRaw( &fVal );
					pPSRE->put_DisplayRaw( fVal );
					pPSRE2->get_DisplayTF( &fVal );
					pPSRE->put_DisplayTF( fVal );
					pPSRE2->get_DisplaySeg( &fVal );
					pPSRE->put_DisplaySeg( fVal );
					pPSRE2->get_Summarize( &fVal );
					pPSRE->put_Summarize( fVal );
					pPSRE2->get_SummarizeOnly( &fVal );
					pPSRE->put_SummarizeOnly( fVal );
					pPSRE2->get_SummarizeSelect( &fVal );
					pPSRE->put_SummarizeSelect( fVal );
					pPSRE2->get_Analyze( &fVal );
					pPSRE->put_Analyze( fVal );
					pPSRE2->get_ViewSubjExt( &fVal );
					pPSRE->put_ViewSubjExt( fVal );
					pPSRE2->get_ViewSubjCon( &fVal );
					pPSRE->put_ViewSubjCon( fVal );
					pPSRE2->get_ViewExpExt( &fVal );
					pPSRE->put_ViewExpExt( fVal );
					pPSRE2->get_ViewExpCon( &fVal );
					pPSRE->put_ViewExpCon( fVal );
					pPSRE2->get_DoQuestionnaire( &fVal );
					pPSRE->put_DoQuestionnaire( fVal );
					pPS2->get_TrialMode( &tm );
					pPS->put_TrialMode( tm );
					pPSF2->get_EXT_2( &fVal );
					pPSF->put_EXT_2( fVal );
					pPSRE2->GetDrawingOptions( &ls, &es, &bg, &l1, &l2, &l3, &e, &mpp );
					pPSRE->SetDrawingOptions( ls, es, bg, l1, l2, l3, e, mpp );
					pPS2->get_DisCorrection( &nVal );
					pPS->put_DisCorrection( nVal );
					pPS2->get_DisFactor( &dVal );
					pPS->put_DisFactor( dVal );
					pPS2->get_DisZInsertPoint( &dVal );
					pPS->put_DisZInsertPoint( dVal );
					pPS2->get_DisRelError( &dVal );
					pPS->put_DisRelError( dVal );
					pPS2->get_DisAbsError( &dVal );
					pPS->put_DisAbsError( dVal );
					pPSF2->get_CON_D( &fVal );
					pPSF->put_CON_D( fVal );
					pPSRE2->get_VaryLineThickness( &fVal );
					pPSRE->put_VaryLineThickness( fVal );
					pPSRE2->get_MaxLineThickness( &nVal );
					pPSRE->put_MaxLineThickness( nVal );
					pPS2->get_RandWithRules( &fVal );
					pPS->put_RandWithRules( fVal );
					pPS2->get_RandRules( &bstr );
					pPS->put_RandRules( bstr );
					pPS2->get_RandBlockCounts( &bstr );
					pPS->put_RandBlockCounts( bstr );
					pPS2->get_RandSelectionChar( &bstr );
					pPS->put_RandSelectionChar( bstr );
					pPS2->get_RandSelectionTrials( &bstr );
					pPS->put_RandSelectionTrials( bstr );
					pPS2->get_UpSampleFactor( &nVal );
					pPS->put_UpSampleFactor( nVal );
					pPSRE2->get_QuestAtEnd( &fVal );
					pPSRE->put_QuestAtEnd( fVal );
					pPSRE2->get_FullScreen( &fVal );
					pPSRE->put_FullScreen( fVal );
					pPS2->get_CollapseUDStrokes( &fVal );
					pPS->put_CollapseUDStrokes( fVal );
					pPS2->get_CollapseStrokes( &fVal );
					pPS->put_CollapseStrokes( fVal );
					pPS2->get_CollapseTrials( &fVal );
					pPS->put_CollapseTrials( fVal );
					pPS2->get_CollapseMedian( &fVal );
					pPS->put_CollapseMedian( fVal );
					pPS2->get_UseTilt( &fVal );
					pPS->put_UseTilt( fVal );
					pPS2->get_MaxPenPressure( &nVal );
					pPS->put_MaxPenPressure( nVal );
					pPS2->get_DisError( &dVal );
					pPS->put_DisError( dVal );
					pPS2->get_DisNotify( &fVal );
					pPS->put_DisNotify( fVal );
					pPS2->get_SmoothingIter( &nVal );
					pPS->put_SmoothingIter( nVal );
					pPS2->get_InkThreshold( &nVal );
					pPS->put_InkThreshold( nVal );
					pPS2->get_ImgResolution( &nVal );
					pPS->put_ImgResolution( nVal );
					pPSF2->get_DiscardPenDownDurMin( &fVal );
					pPSF->put_DiscardPenDownDurMin( fVal );
					pPSF2->get_PenDownDurMin( &dVal );
					pPSF->put_PenDownDurMin( dVal );
					ExternalAppType eat;
					pPSRE2->get_ExtAppTypeRaw( &eat );
					pPSRE->put_ExtAppTypeRaw( eat );
					pPSRE2->get_ExtAppScriptRaw( &bstr );
					pPSRE->put_ExtAppScriptRaw( bstr );
					pPSRE2->get_ExtAppTypeTF( &eat );
					pPSRE->put_ExtAppTypeTF( eat );
					pPSRE2->get_ExtAppScriptTF( &bstr );
					pPSRE->put_ExtAppScriptTF( bstr );
					pPSRE2->get_ExtAppTypeSeg( &eat );
					pPSRE->put_ExtAppTypeSeg( eat );
					pPSRE2->get_ExtAppScriptSeg( &bstr );
					pPSRE->put_ExtAppScriptSeg( bstr );
					pPSRE2->get_ExtAppTypeExt( &eat );
					pPSRE->put_ExtAppTypeExt( eat );
					pPSRE2->get_ExtAppScriptExt( &bstr );
					pPSRE->put_ExtAppScriptExt( bstr );
					pPS2->get_SpecifySequence( &fVal );
					pPS->put_SpecifySequence( fVal );
					pPS2->get_ConditionSequence( &bstr );
					pPS->put_ConditionSequence( bstr );
					pPS2->get_RemoveTrailingPenlift( &fVal );
					pPS->put_RemoveTrailingPenlift( fVal );

					hr = pPS->Modify();
				}
				else hr = pPS->Modify();
			}
			// does not exist - add
			else if( FAILED( hr ) ) hr = pPS->Add();
			// exists & not overwrite - do nothing
			else hr = S_OK;

			if( FAILED( hr ) ) {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" Experiments::Import") );
			}
			else fDataAdded = TRUE;
		}
		else if( szLine == _T("[MQUESTIONNAIRE]") )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Master Questionnaire - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			CString szFile1, szFile2;
			BSTR bstr = NULL;
			BOOL fComp = FALSE;
			// 2nd set of questionnaire for overwrite detail comparison
			if( !pMQ2 )
			{
				hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
									   IID_IMQuestionnaire, (LPVOID*)&pMQ2 );
				if( FAILED( hr ) )
				{
					szData.Format( IDS_QUEST_ERR, hr );
					BCGPMessageBox( szData );
					goto ExpError;
				}
			}
			// ID
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			hr = pMQ->Find( szMID1.AllocSysString() );
			if( SUCCEEDED( hr ) ) fComp = TRUE;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportMQuest;
			pMQ->put_ID( szMID1.AllocSysString() );
			pMQ2->put_ID( szMID1.AllocSysString() );
			szTemp.Format( _T("IMPORT: Master Questionnaire %s"), szMID1 );
			if( !fScan ) OutputAMessage( szTemp );
			// Header
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportMQuest;
			fVal = atoi( szLine );
			pMQ->put_IsHeader( fVal ); 
			pMQ2->put_IsHeader( fVal ); 
			// Question
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportMQuest;
			pMQ->put_Question( szLine.AllocSysString() );
			pMQ2->put_Question( szLine.AllocSysString() );
			// Item Num
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportMQuest;
			nVal = atoi( szLine );
			pMQ->put_ItemNum( nVal );
			pMQ2->put_ItemNum( nVal );
			// Private
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportMQuest;
			fVal = atoi( szLine );
			pMQ->put_IsPrivate( fVal );
			pMQ2->put_IsPrivate( fVal );
			// Numeric
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportMQuest;
			fVal = atoi( szLine );
			pMQ->put_IsNumeric( fVal );
			pMQ2->put_IsNumeric( fVal );
			// Header
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportMQuest;
			pMQ->put_ColumnHeader( szLine.AllocSysString() );
			pMQ2->put_ColumnHeader( szLine.AllocSysString() );

ImportMQuest:
			if( fScan )
			{
				nItems++;
				continue;
			}
			// if a record already exists, save imported one to temp database
			// and dump records from both for file compares to present to user
			::GetDataPathRoot( szFile1 );	// original
			::GetDataPathRoot( szFile2 );	// imported
			szFile1 += _T("\\comp1.txt");
			szFile2 += _T("\\comp2.txt");
			BOOL fDuplicate = FALSE;
			if( !fOWAllYes && !fOWAllNo && fComp )
			{
				// dump found record in normal db table
				BOOL fRes = pMQ->DumpRecord( szMID1.AllocSysString(), szFile1.AllocSysString() );
				// add (or find) imported item into temp table (new object set as temp)
				pMQ2->SetTemp( TRUE );
				HRESULT hr2 = pMQ2->Find( szMID1.AllocSysString() );	// just in case the files were not deleted before
				if( FAILED( hr2 ) ) hr2 = pMQ2->Add();
				// dump temp record just added
				fRes = pMQ2->DumpRecord( szMID1.AllocSysString(), szFile2.AllocSysString() );
				// remove temp db tables & clear temp flag
				pMQ2->RemoveTemp();
				pMQ2->SetTemp( FALSE );
				// we have files
				fFiles = TRUE;
				// compare files to see if we need to ask (might be there but might be duplicate)
				CStdioFile f1, f2;
				if( f1.Open( szFile1, CFile::modeRead ) )
				{
					if( f2.Open( szFile2, CFile::modeRead ) )
					{
						CString sz1, sz2;
						while( f1.ReadString( sz1 ) )
						{
							if( !f2.ReadString( sz2 ) ) goto OverwriteMQ;
							if( sz1 != sz2 ) goto OverwriteMQ;
						}
						fDuplicate = TRUE;
						f2.Close();
					}
					f1.Close();
				}
			}
OverwriteMQ:
			if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
			{
				OverwriteDlg od( OW_QUEST, szMID1, szFile1, szFile2 );
				od.DoModal();
				if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
				else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
				else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
				else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
				else if( od.m_nResult == OW_CANCEL ) goto ExpError;
				crs.Restore();
			}
			else fOverWrite = fOWAllYes;
			// cleanup of comparison files if applicable
			if( fFiles )
			{
				CFileFind ff;
				if( ff.FindFile( szFile1 ) ) REMOVE_FILE( szFile1 );
				if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
			}
			// exists & overwrite - modify
			if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
			{
				if( fComp )
				{
					// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
					// then must snag values from temp copy (of new data) and reset orig then modify
					hr = pMQ->Find( szMID1.AllocSysString() );

					pMQ2->get_IsHeader( &fVal );
					pMQ->put_IsHeader( fVal ); 
					pMQ2->get_Question( &bstr );
					pMQ->put_Question( bstr );
					pMQ2->get_ItemNum( &nVal );
					pMQ->put_ItemNum( nVal );
					pMQ2->get_IsPrivate( &fVal );
					pMQ->put_IsPrivate( fVal );
					pMQ2->get_IsNumeric( &fVal );
					pMQ->put_IsNumeric( fVal );
 					pMQ2->get_ColumnHeader( &bstr );
 					pMQ->put_ColumnHeader( bstr );

					hr = pMQ->Modify();
				}
				else hr = pMQ->Modify();
			}
			// does not exist - add
			else if( FAILED( hr ) ) hr = pMQ->Add();
			// exists & not overwrite - do nothing
			else hr = S_OK;

			if( FAILED( hr ) ) {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" Elements::Import:OverwriteMQ") );
			}
			else fDataAdded = TRUE;
		}
		else if( szLine == _T("[GQUESTIONNAIRE]") )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Group Questionnaire - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			CString szFile1, szFile2;
			BOOL fComp = FALSE;
			// 2nd set of questionnaire for overwrite detail comparison
			if( !pGQ2 )
			{
				hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
									   IID_IGQuestionnaire, (LPVOID*)&pGQ2 );
				if( FAILED( hr ) )
				{
					szData.Format( IDS_QUEST_ERR, hr );
					BCGPMessageBox( szData );
					goto ExpError;
				}
			}
			// Exp ID
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportGQuest;
			pGQ->put_ExperimentID( szMID1.AllocSysString() );
			pGQ2->put_ExperimentID( szMID1.AllocSysString() );
			// Group ID
			fRet = READ_STRING( pFile, szMID2 );
			if( !fRet ) goto ExpError;
			if( szMID2 == EXPORT_SEPARATOR ) goto ImportGQuest;
			pGQ->put_GroupID( szMID2.AllocSysString() );
			pGQ2->put_GroupID( szMID2.AllocSysString() );
			// Question ID
			fRet = READ_STRING( pFile, szMID3 );
			if( !fRet ) goto ExpError;
			hr = pGQ->Find( szMID1.AllocSysString(), szMID2.AllocSysString(),
							szMID3.AllocSysString() );
			if( SUCCEEDED( hr ) ) fComp = TRUE;
			if( szMID3 == EXPORT_SEPARATOR ) goto ImportGQuest;
			pGQ->put_ID( szMID3.AllocSysString() );
			pGQ2->put_ID( szMID3.AllocSysString() );
			if( !fScan ) szTemp.Format( _T("IMPORT: Group Questionnaire %s %s %s"), szMID1, szMID2, szMID3 );
			OutputAMessage( szTemp );
			// Item Num
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportGQuest;
			nVal = atoi( szLine );
			pGQ->put_ItemNum( nVal );
			pGQ2->put_ItemNum( nVal );

ImportGQuest:
			if( fScan )
			{

				nItems++;
				continue;
			}
			// if a record already exists, save imported one to temp database
			// and dump records from both for file compares to present to user
			::GetDataPathRoot( szFile1 );	// original
			::GetDataPathRoot( szFile2 );	// imported
			szFile1 += _T("\\comp1.txt");
			szFile2 += _T("\\comp2.txt");
			BOOL fDuplicate = FALSE;
			if( !fOWAllYes && !fOWAllNo && fComp )
			{
				// dump found record in normal db table
				hr = pGQ->Find( szMID1.AllocSysString(), szMID2.AllocSysString(), szMID3.AllocSysString() );
				pGQ->get_ItemNum( &nVal );
				BOOL fRes = pGQ->DumpRecord( szMID1.AllocSysString(), szFile1.AllocSysString() );
				// add (or find) imported item into temp table (new object set as temp)
				pGQ2->SetTemp( TRUE );
				HRESULT hr2 = pGQ2->Find( szMID1.AllocSysString(), szMID2.AllocSysString(), // just in case the files were not deleted before
										  szMID3.AllocSysString() );
				pGQ2->put_ItemNum( nVal );
				if( FAILED( hr2 ) ) hr2 = pGQ2->Add();
				// dump temp record just added
				fRes = pGQ2->DumpRecord( szMID1.AllocSysString(), szFile2.AllocSysString() );
				// remove temp db tables & clear temp flag
				pGQ2->RemoveTemp();
				pGQ2->SetTemp( FALSE );
				// we have files
				fFiles = TRUE;
				// compare files to see if we need to ask (might be there but might be duplicate)
				CStdioFile f1, f2;
				if( f1.Open( szFile1, CFile::modeRead ) )
				{
					if( f2.Open( szFile2, CFile::modeRead ) )
					{
						CString sz1, sz2;
						while( f1.ReadString( sz1 ) )
						{
							if( !f2.ReadString( sz2 ) ) goto OverwriteGQ;
							if( sz1 != sz2 ) goto OverwriteGQ;
						}
						fDuplicate = TRUE;
						f2.Close();
					}
					f1.Close();
				}
			}
OverwriteGQ:
			if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
			{
				OverwriteDlg od( OW_GQUEST, szMID1, szFile1, szFile2 );
				od.DoModal();
				if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
				else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
				else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
				else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
				else if( od.m_nResult == OW_CANCEL ) goto ExpError;
				crs.Restore();
			}
			else fOverWrite = fOWAllYes;
			// cleanup of comparison files if applicable
			if( fFiles )
			{
				CFileFind ff;
				if( ff.FindFile( szFile1 ) ) REMOVE_FILE( szFile1 );
				if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
			}
			// exists & overwrite - modify
			if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
			{
				if( fComp )
				{
					// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
					// then must snag values from temp copy (of new data) and reset orig then modify
					hr = pGQ->Find( szMID1.AllocSysString(), szMID2.AllocSysString(), szMID3.AllocSysString() );

					pGQ2->get_ItemNum( &nVal );
					pGQ->put_ItemNum( nVal );

					hr = pGQ->Modify();
				}
				else hr = pGQ->Modify();
			}
			// does not exist - add
			else if( FAILED( hr ) ) hr = pGQ->Add();
			// exists & not overwrite - do nothing
			else hr = S_OK;

			if( FAILED( hr ) ) {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" Experiments::Import:OverwriteGQ") );
			}
			else fDataAdded = TRUE;
		}
		else if( szLine == _T("[SQUESTIONNAIRE]") )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Subject Questionnaire - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			CString szFile1, szFile2;
			BSTR bstr = NULL;
			BOOL fComp = FALSE;
			// 2nd set of questionnaire for overwrite detail comparison
			if( !pSQ2 )
			{
				hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
									   IID_ISQuestionnaire, (LPVOID*)&pSQ2 );
				if( FAILED( hr ) )
				{
					szData.Format( IDS_QUEST_ERR, hr );
					BCGPMessageBox( szData );
					goto ExpError;
				}
			}
			// Exp ID
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportSQuest;
			pSQ->put_ExperimentID( szMID1.AllocSysString() );
			pSQ2->put_ExperimentID( szMID1.AllocSysString() );
			// Group ID
			fRet = READ_STRING( pFile, szMID2 );
			if( !fRet ) goto ExpError;
			if( szMID2 == EXPORT_SEPARATOR ) goto ImportSQuest;
			pSQ->put_GroupID( szMID2.AllocSysString() );
			pSQ2->put_GroupID( szMID2.AllocSysString() );
			// Subject ID
			fRet = READ_STRING( pFile, szMID3 );
			if( !fRet ) goto ExpError;
			if( szMID3 == EXPORT_SEPARATOR ) goto ImportSQuest;
			pSQ->put_SubjectID( szMID3.AllocSysString() );
			pSQ2->put_SubjectID( szMID3.AllocSysString() );
			// Question ID
			fRet = READ_STRING( pFile, szMID4 );
			if( !fRet ) goto ExpError;
			hr = pSQ->Find( szMID1.AllocSysString(), szMID2.AllocSysString(),
							szMID3.AllocSysString(), szMID4.AllocSysString() );
			if( SUCCEEDED( hr ) ) fComp = TRUE;
			if( szMID4 == EXPORT_SEPARATOR ) goto ImportSQuest;
			pSQ->put_ID( szMID4.AllocSysString() );
			pSQ2->put_ID( szMID4.AllocSysString() );
			if( !fScan ) szTemp.Format( _T("IMPORT: Subject Questionnaire %s %s %s"), szMID1, szMID2, szMID3, szMID4 );
			OutputAMessage( szTemp );
			// Answer
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportSQuest;
			pSQ->put_Answer( szLine.AllocSysString() );
			pSQ2->put_Answer( szLine.AllocSysString() );

ImportSQuest:
			if( fScan )
			{
				nItems++;
				continue;
			}
			// if a record already exists, save imported one to temp database
			// and dump records from both for file compares to present to user
			::GetDataPathRoot( szFile1 );	// original
			::GetDataPathRoot( szFile2 );	// imported
			szFile1 += _T("\\comp1.txt");
			szFile2 += _T("\\comp2.txt");
			BOOL fDuplicate = FALSE;
			if( !fOWAllYes && !fOWAllNo && fComp )
			{
				// dump found record in normal db table
				BOOL fRes = pSQ->DumpRecord( szMID1.AllocSysString(), szFile1.AllocSysString() );
				// add (or find) imported item into temp table (new object set as temp)
				pSQ2->SetTemp( TRUE );
				HRESULT hr2 = pSQ2->Find( szMID1.AllocSysString(), szMID2.AllocSysString(), // just in case the files were not deleted before
										  szMID3.AllocSysString(), szMID4.AllocSysString() );
				if( FAILED( hr2 ) ) hr2 = pSQ2->Add();
				// dump temp record just added
				fRes = pSQ2->DumpRecord( szMID1.AllocSysString(), szFile2.AllocSysString() );
				// remove temp db tables & clear temp flag
				pSQ2->RemoveTemp();
				pSQ2->SetTemp( FALSE );
				// we have files
				fFiles = TRUE;
				// compare files to see if we need to ask (might be there but might be duplicate)
				CStdioFile f1, f2;
				if( f1.Open( szFile1, CFile::modeRead ) )
				{
					if( f2.Open( szFile2, CFile::modeRead ) )
					{
						CString sz1, sz2;
						while( f1.ReadString( sz1 ) )
						{
							if( !f2.ReadString( sz2 ) ) goto OverwriteSQ;
							if( sz1 != sz2 ) goto OverwriteSQ;
						}
						fDuplicate = TRUE;
						f2.Close();
					}
					f1.Close();
				}
			}
OverwriteSQ:
			if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
			{
				OverwriteDlg od( OW_SQUEST, szMID1, szFile1, szFile2 );
				od.DoModal();
				if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
				else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
				else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
				else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
				else if( od.m_nResult == OW_CANCEL ) goto ExpError;
				crs.Restore();
			}
			else fOverWrite = fOWAllYes;
			// cleanup of comparison files if applicable
			if( fFiles )
			{
				CFileFind ff;
				if( ff.FindFile( szFile1 ) ) REMOVE_FILE( szFile1 );
				if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
			}
			// exists & overwrite - modify
			if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
			{
				if( fComp )
				{
					// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
					// then must snag values from temp copy (of new data) and reset orig then modify
					hr = pSQ->Find( szMID1.AllocSysString(), szMID2.AllocSysString(),
									szMID3.AllocSysString(), szMID4.AllocSysString() );

					pSQ2->get_Answer( &bstr );
					pSQ->put_Answer( bstr );

					hr = pSQ->Modify();
				}
				else hr = pSQ->Modify();
			}
			// does not exist - add
			else if( FAILED( hr ) ) hr = pSQ->Add();
			// exists & not overwrite - do nothing
			else hr = S_OK;

			if( FAILED( hr ) ) {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" Experiments::Import:Overwrite:SQ") );
			}
			else fDataAdded = TRUE;
		}
		else if( szLine == TAG_CONDITION )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Condition - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			NSConditions cond;
			if( cond.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, fScan ) ) {
				cond.GetID( szID );
				cond.GetDescription( szDesc );
				if( ( szID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
				fDataAdded = TRUE;
			}
			else goto ExpError;
			crs.Restore();
			if( fScan ) nItems++;
		}
		else if( szLine == TAG_GROUP )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Group - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			Groups grp;
			if( grp.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, fScan ) ) {
				grp.GetID( szID );
				grp.GetDescription( szDesc );
				if( ( szID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
				fDataAdded = TRUE;
			}
			else goto ExpError;
			crs.Restore();
			if( fScan ) nItems++;
		}
		else if( szLine == TAG_SUBJECT )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Subject - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			Subjects subj;
			if( subj.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, nVer, fScan ) ) {
				subj.GetID( szID );
				subj.GetName( szDesc, FALSE );
				if( ( szID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
				fDataAdded = TRUE;
			}
			else goto ExpError;
			crs.Restore();
			if( fScan ) nItems++;
		}
		else if( szLine == TAG_STIMULUS )
		{
			if( ::IsOA() )
			{
				// progress meter
				if( !fScan )
				{
					nItem++;
					dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
					szMsg.Format( _T("IMPORT Stimulus - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
					Progress::SetProgress( nItem, szMsg );
				}
				Stimuluss stim;
				if( stim.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, fScan ) )
				{
					stim.GetID( szMID1 );
					stim.GetDescription( szDesc );
					if( ( szMID1.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
					fDataAdded = TRUE;
				}
				else goto ExpError;
				crs.Restore();
				if( fScan ) nItems++;
			}
		}
		else if( szLine == TAG_ELEMENT )
		{
			if( ::IsOA() )
			{
				// progress meter
				if( !fScan )
				{
					nItem++;
					dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
					szMsg.Format( _T("IMPORT Element - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
					Progress::SetProgress( nItem, szMsg );
				}
				Elements elem;
				if( elem.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, fScan ) ) {
					elem.GetID( szID );
					elem.GetDescription( szDesc );
					if( ( szID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
					fDataAdded = TRUE;
				}
				else goto ExpError;
				crs.Restore();
				if( fScan ) nItems++;
			}
		}
		else if( szLine == TAG_CAT )
		{
			if( ::IsOA() )
			{
				// progress meter
				if( !fScan )
				{
					nItem++;
					dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
					szMsg.Format( _T("IMPORT Category - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
					Progress::SetProgress( nItem, szMsg );
				}
				Cats cat;
				if( cat.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, fScan ) ) {
					cat.GetID( szID );
					cat.GetDescription( szDesc );
					if( ( szID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
					fDataAdded = TRUE;
				}
				else goto ExpError;
				crs.Restore();
				if( fScan ) nItems++;
			}
		}
		else if( szLine == TAG_FEEDBACK )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Feedback - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			Feedbacks fb;
			if( fb.Import( (CStdioFile*)pFile, fOWAllYes, fOWAllNo, fScan ) ) {
				fb.GetID( szID );
				fb.GetDescription( szDesc );
				if( ( szID.Find( szDelim ) >= 0 ) || ( szDesc.Find( szDelim ) >= 0 ) ) goto DelimError;
				fDataAdded = TRUE;
			}
			else goto ExpError;
			crs.Restore();
			if( fScan ) nItems++;
		}
		else if( szLine == TAG_STIMELMT )
		{
			if( ::IsOA() )
			{
				// progress meter
				if( !fScan )
				{
					nItem++;
					dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
					szMsg.Format( _T("IMPORT Stimulus Element - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
					Progress::SetProgress( nItem, szMsg );
				}
				// Stimulus ID
				fRet = READ_STRING( pFile, szMID1 );
				if( !fRet ) goto ExpError;
				if( szMID1 == EXPORT_SEPARATOR ) goto ImportStimElem;
				pSE->put_StimulusID( szMID1.AllocSysString() );
				// Element ID
				fRet = READ_STRING( pFile, szMID2 );
				if( !fRet ) goto ExpError;
				hr = pSE->Find( szMID1.AllocSysString(), szMID2.AllocSysString()  );
				if( szMID2 == EXPORT_SEPARATOR ) goto ImportStimElem;
				pSE->put_ElementID( szMID2.AllocSysString() );

ImportStimElem:
				if( fScan )
				{
					nItems++;
					continue;
				}
				// exists & overwrite - modify
				fOverWrite = TRUE;
				if( SUCCEEDED( hr ) && fOverWrite ) hr = pSE->Modify();
				// does not exist - add
				else if( FAILED( hr ) ) hr = pSE->Add();
				// exists & not overwrite - do nothing
				else hr = S_OK;

				if( FAILED( hr ) ) {
					CString szTempMsg;
					szTempMsg.LoadString(IDS_ADD_ERR);
					BCGPMessageBox( szTempMsg+_T(" Experiments::Import:ImportStimElem") );
				}
				else fDataAdded = TRUE;
			}
		}
		else if( szLine == TAG_STIMTARG )
		{
			if( ::IsOA() )
			{
				// progress meter
				if( !fScan )
				{
					nItem++;
					dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
					szMsg.Format( _T("IMPORT Stimulus Target - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
					Progress::SetProgress( nItem, szMsg );
				}
				// Stimulus ID
				fRet = READ_STRING( pFile, szMID1 );
				if( !fRet ) goto ExpError;
				if( szMID1 == EXPORT_SEPARATOR ) goto ImportStimTarg;
				pST->put_StimulusID( szMID1.AllocSysString() );
				// Element ID
				fRet = READ_STRING( pFile, szMID2 );
				if( !fRet ) goto ExpError;
				if( szMID2 == EXPORT_SEPARATOR ) goto ImportStimTarg;
				pST->put_ElementID( szMID2.AllocSysString() );
				// Sequence
				fRet = READ_STRING( pFile, szMID3 );
				if( !fRet ) goto ExpError;
				if( szMID3 == EXPORT_SEPARATOR ) goto ImportStimTarg;
				nVal = atoi( szMID3 );
				pST->put_Sequence( nVal );
				hr = pST->Find( szMID1.AllocSysString(), szMID2.AllocSysString(), nVal );

ImportStimTarg:
				if( fScan )
				{
					nItems++;
					continue;
				}
				// exists & overwrite - modify
				if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) ) hr = pST->Modify();
				// does not exist - add
				else if( FAILED( hr ) ) hr = pST->Add();
				// exists & not overwrite - do nothing
				else hr = S_OK;

				if( FAILED( hr ) ) {
					CString szTempMsg;
					szTempMsg.LoadString(IDS_ADD_ERR);
					BCGPMessageBox( szTempMsg+_T(" Experiments::Import:ImportStimTarget") );
				}
				else fDataAdded = TRUE;
			}
		}
		else if( szLine == _T("[EXPERIMENTCONDITION]") )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Experiment Condition - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			// Exp ID
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportExpCond;
			pEC->put_ExperimentID( szMID1.AllocSysString() );
			// Cond ID
			fRet = READ_STRING( pFile, szMID2 );
			if( !fRet ) goto ExpError;
			hr = pEC->Find( szMID1.AllocSysString(), szMID2.AllocSysString() );
			if( szMID2 == EXPORT_SEPARATOR ) goto ImportExpCond;
			pEC->put_ConditionID( szMID2.AllocSysString() );
			// Replications
			fRet = READ_STRING( pFile, szLine );
			if( !fRet ) goto ExpError;
			if( szLine == EXPORT_SEPARATOR ) goto ImportExpCond;
			nVal = atoi( szLine );
			pEC->put_Replications( nVal );

ImportExpCond:
			if( fScan )
			{
				nItems++;
				continue;
			}
			// exists & overwrite - modify
			fOverWrite = TRUE;
			if( SUCCEEDED( hr ) && fOverWrite ) hr = pEC->Modify();
			// does not exist - add
			else if( FAILED( hr ) ) hr = pEC->Add();
			// exists & not overwrite - do nothing
			else hr = S_OK;

			if( FAILED( hr ) ) {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" Experiments::Import:ImportExpCond") );
			}
			else fDataAdded = TRUE;
		}
		else if( szLine == _T("[EXPERIMENTMEMBER]") )
		{
			// progress meter
			if( !fScan )
			{
				nItem++;
				dVal = (double)( ( nItem * 1. ) / ( nItems * 1. ) * 100. );
				szMsg.Format( _T("IMPORT Experiment Member - %d of %d complete (%.0f%%)"), nItem, nItems, dVal );
				Progress::SetProgress( nItem, szMsg );
			}
			// Exp ID
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportExpMem;
			pEM->put_ExperimentID( szMID1.AllocSysString() );
			szExp = szMID1;
			// Grp ID
			fRet = READ_STRING( pFile, szMID2 );
			if( !fRet ) goto ExpError;
			if( szMID2 == EXPORT_SEPARATOR ) goto ImportExpMem;
			pEM->put_GroupID( szMID2.AllocSysString() );
			// Subj ID
			fRet = READ_STRING( pFile, szMID3 );
			if( !fRet ) goto ExpError;
			hr = pEM->Find( szMID1.AllocSysString(), szMID2.AllocSysString(), szMID3.AllocSysString() );
			if( szMID3 == EXPORT_SEPARATOR ) goto ImportExpMem;
			pEM->put_SubjectID( szMID3.AllocSysString() );
			// Device resolution
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportExpMem;
			pEM->put_DeviceRes( atof( szMID1 ) );
			// Sampling rate
			fRet = READ_STRING( pFile, szMID1 );
			if( !fRet ) goto ExpError;
			if( szMID1 == EXPORT_SEPARATOR ) goto ImportExpMem;
			pEM->put_SamplingRate( (short)( atoi( szMID1 ) ) );

ImportExpMem:
			if( fScan )
			{
				nItems++;
				continue;
			}
			// exists & overwrite - modify
			fOverWrite = TRUE;
			if( SUCCEEDED( hr ) && fOverWrite ) hr = pEM->Modify();
			// does not exist - add
			else if( FAILED( hr ) ) hr = pEM->Add();
			// exists & not overwrite - do nothing
			else hr = S_OK;

			if( SUCCEEDED( hr ) )
			{
				fDataAdded = TRUE;
				szMsg.Format( _T("%s%s%s%s%s"), szExp, szDelim, szMID2, szDelim, szMID3 );
				pListImported->AddTail( szMsg );
			}
			else {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" Experiments::Import:ImportExpMem") );
			}
		}
	}

	// if scanning for number of items, just reset file pointer and begin again
	if( fScan )
	{
		Progress::DisableProgress();
		fScan = FALSE;
		pFile->SeekToBegin();
		goto StartHere;
	}
	else Progress::DisableProgress();

	// Files/Trials
	if( fFiles && ff.FindFile( szFiles ) )
	{
		// compress
		if( !::Uncompress( szFiles ) && !fSkipUI )
			BCGPMessageBox( _T("Error in uncompressing files.") );
	}
	if( fTrials && ff.FindFile( szTrials ) )
	{
		// compress
		if( !::Uncompress( szTrials ) && !fSkipUI )
			BCGPMessageBox( _T("Error in uncompressing data files.") );
	}

	// Info user regarding results
	Progress::DisableProgress();
	if( !fZip ) f.Close();
	szTemp.Format( _T("Imported from %s."), szFile );
	OutputAMessage( szTemp );
	if( !fSkipUI ) BCGPMessageBox( _T("The import was successful.") );

	// reset verification for images and sounds
	CBCGPWorkspace* pApp = GetWorkspaceApp();
	if( pApp )
	{
		CString szVal, szDef, szKey, szRB, szID;
		::GetCurrentUser( szID );
		szVal.Format( _T("Settings\\%s"), szID );
		szRB = pApp->GetRegistryBase();
		pApp->SetRegistryBase( szVal );
		pApp->WriteInt( _T("VerifiedImages"), 0 );
		pApp->WriteInt( _T("VerifiedSounds"), 0 );
		pApp->SetRegistryBase( szRB );
	}

	fVal = TRUE;
	::DataHasChanged();
	goto Cleanup;

DelimError:
	Progress::DisableProgress();
	szMsg.Format( _T("The delimter for this user (%s) conflicts with the experiment being imported.\n"), szDelim );
	szMsg += _T("You will need to change the delimiter (MENU:Settings->Data Path & Settings) in order to import.");
	Beep( 800, 200 );
	BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
	fVal = FALSE;
	goto Cleanup;

ExpError:
	Progress::DisableProgress();
	Beep( 800, 200 );
	BCGPMessageBox( _T("There was an error in the import process or it was canceled."), MB_OK | MB_ICONERROR );
	fVal = FALSE;

Cleanup:
	// repair questionnaire for item nums just in case
	{
		MQuestionnaires q;
		q.Repair();
	}

	// cleanup temporary trial/settings files if from zip
	if( fZip )
	{
		BYTE* data = pMemFile->Detach();
		delete data;
		pMemFile->Close();
		delete pMemFile;

		if( szFiles != _T("") && ff.FindFile( szFiles ) ) REMOVE_FILE( szFiles );
		if( szTrials != _T("") && ff.FindFile( szTrials ) ) REMOVE_FILE( szTrials );
	}

	if( pEM ) pEM->Release();
	if( pEC ) pEC->Release();
	if( pPSF ) pPSF->Release();
	if( pPSRE ) pPSRE->Release();
	if( pPS2 ) pPS2->Release();
	if( pPSF2 ) pPSF2->Release();
	if( pPSRE2 ) pPSRE2->Release();
	if( pPS ) pPS->Release();
	if( pSE ) pSE->Release();
	if( pST ) pST->Release();
	if( pMQ ) pMQ->Release();
	if( pMQ2 ) pMQ2->Release();
	if( pGQ ) pGQ->Release();
	if( pGQ2 ) pGQ2->Release();
	if( pSQ ) pSQ->Release();
	if( pSQ2 ) pSQ2->Release();

	return ( fVal && fDataAdded );
}

///////////////////////////////////////////////////////////////////////////////

// dynamic double array object
class VSDataObject
{
public:
	VSDataObject()
	{
		m_pData = new double[ m_nSize = dyn_array_start ];
		m_nCount = 0;
	}
	~VSDataObject()
	{
		if( m_pData ) delete [] m_pData;
	}

	UINT GetCount() { return m_nCount; }
	double* GetData() { return m_pData; }

	double operator[]( UINT nIdx )
	{
		ATLASSERT( ( nIdx >= 0 ) && ( nIdx <= m_nCount ) );
		if( ( nIdx < 0 ) || ( nIdx >= m_nCount ) ) AtlThrow( E_INVALIDARG );
		return m_pData[ nIdx ];
	}

	void Add( double dVal )
	{
		// increase size of array as necessary by step size
		if( ( m_nCount + 1 ) > m_nSize )
		{
			// new size of array = current + step
			UINT nNewSize = m_nSize + dyn_array_step;
			// new array of new size
			double* pTemp = new double[ nNewSize ];
			// zero out (is this necessary?)
			memset( pTemp, 0, nNewSize * sizeof( double ) );
			// copy old array into new
			memcpy( pTemp, m_pData, m_nSize * sizeof( double ) );
			// delete old array
			delete [] m_pData;
			// assign array pointer to new array
			m_pData = pTemp;
			// set new size
			m_nSize = nNewSize;
		}
		// add value
		m_pData[ m_nCount++ ] = dVal;
	}

	// member vars
private:
	double*		m_pData;	// the data
	UINT		m_nCount;	// # of items being used
	UINT		m_nSize;	// # of items allocated in memory
	const static int dyn_array_start = 500;
	const static int dyn_array_step = 500;
};

// class for velocity scaling score
class VSObject : public CObject
{
public:
	VSObject( CString szGroup, CString szSubj ) : CObject()
	{
		m_dSlope = 0.;
		m_szGroup = szGroup;
		m_szSubject = szSubj;
	}
	~VSObject() {}

	double		GetSlope() { return m_dSlope; }
	CString&	GetGroup() { return m_szGroup; }
	CString&	GetSubject() { return m_szSubject; }
	void		Add( double dSize, double dVel )
	{
		m_aVSize.Add( dSize );
		m_aVelPeak.Add( dVel );
	}

	BOOL FindCondition( CString szCond )
	{
		if( m_lstCond.GetHeadPosition() == NULL ) return FALSE;
		CString szC = m_lstCond.GetHead();
		if( ( szC.GetLength() >= 2 ) && ( szCond.GetLength() >= 2 ) )
		{
			if( szC.Left( 2 ) == szCond.Left( 2 ) ) return TRUE;
		}
		return FALSE;
	}
	void AddCondition( CString szCond )
	{
		if( m_lstCond.Find( szCond ) == NULL ) m_lstCond.AddTail( szCond );
	}

	static VSObject* FindObject( CObList* pList, CString szGroup, CString szSubj, CString szCond )
	{
		if( !pList ) return NULL;
		POSITION pos = pList->GetHeadPosition();
		while( pos )
		{
			VSObject* pObj = (VSObject*)pList->GetNext( pos );
			if( pObj && ( pObj->GetGroup() == szGroup ) && ( pObj->GetSubject() == szSubj ) )
			{
				if( pObj->FindCondition( szCond ) ) return pObj;
			}
		}
		return NULL;
	}

	void RunRegression( BOOL fOutput, BOOL fHdr )
	{
		double a = 0., r = 0.;
		rregr( m_aVSize.GetData(), m_aVelPeak.GetData(), m_aVSize.GetCount(), 1,
			   &a, &m_dSlope, &r );

		if( fOutput )
		{
			double dMean1 = means( m_aVSize.GetData(), m_aVSize.GetCount(), -1.e6 );
			double dMean2 = means( m_aVelPeak.GetData(), m_aVelPeak.GetCount(), -1.e6 );
			double dVar1 = vars( m_aVSize.GetData(), m_aVSize.GetCount(), dMean1, -1.e6 );
			double dVar2 = vars( m_aVelPeak.GetData(), m_aVelPeak.GetCount(), dMean2, -1.e6 );
			double dSD1 = sqrt( abs( dVar1 ) );
			double dSD2 = sqrt( abs( dVar2 ) );
			CString szMsg, szTmp;
			if( fHdr )
			{
				OutputAMessage( _T("VELOCITY SCALING - 1st Subject") );
				szMsg.Format( _T("Group=%s, Subject=%s"), m_szGroup, m_szSubject );
				OutputAMessage( szMsg );
			}
			szMsg = _T("Lumping Conditions=");
			POSITION pos = m_lstCond.GetHeadPosition();
			while( pos )
			{
				szTmp = m_lstCond.GetNext( pos );
				szMsg += szTmp;
				if( pos ) szMsg += _T(", ");
			}
			szTmp.Format( _T(": n=%d"), m_aVSize.GetCount() );
			szMsg += szTmp;
			OutputAMessage( szMsg );
			szMsg.Format( _T(" Avg VertSize=%g, SD VertSize=%g, Avg PeakVertVel=%g, SD PeakVertVel=%g, offset=%g, slope=%g, corr=%g"),
						  dMean1, dSD1, dMean2, dSD2, a, m_dSlope, r );
			OutputAMessage( szMsg );
		}
	}

	// member vars
private:
	CString			m_szGroup;		// for a specific group AND ...
	CString			m_szSubject;	// for a specific subject
	CStringList		m_lstCond;		// group of common/similar conditions
	VSDataObject	m_aVSize;		// vertical size array
	VSDataObject	m_aVelPeak;		// velocity peak array
	double			m_dSlope;
};

//************************************
// Method:    SummarizeExperiment
// FullName:  Experiments::SummarizeExperiment
// Access:    public static 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: BOOL fDlg
// Parameter: BOOL fOnly
// Parameter: BOOL fInc
// Parameter: BOOL fErr
// Parameter: CString szSubjID
// Parameter: CWnd * pMsgList
// Parameter: BOOL fDoConsis
// Parameter: BOOL fNormDB
// Parameter: CString szGrpID
//************************************
BOOL Experiments::SummarizeExperiment( CString szExp, BOOL fDlg, BOOL fOnly, BOOL fInc,
									   BOOL fErr, CString szSubjID, CWnd* pMsgList,
									   BOOL fDoConsis, BOOL fNormDB, CString szGrpID )
{
	CFileFind ff;

	// group/subj only?
	BOOL fThisGroup = FALSE;
	if( ( szSubjID != _T("") ) && fOnly && ( szGrpID != _T("") ) ) fThisGroup = TRUE;

	// current privacy setting
	BOOL fPrivate = ::IsPrivacyOn();
	// turn off privacy for summarization
	::SetIsPrivacyOn( TRUE );

	// Subjects to exclude (cancel summarization if user cancels this dialog)
	// ...if from run-experiment..this dialog is optional
	ExcludeDlg exDlg( szExp, AfxGetMainWnd() );
	if( !fOnly && fDlg && ( exDlg.DoModal() == IDCANCEL ) )
	{
		// set privacy back to original value
		::SetIsPrivacyOn( fPrivate );
		if( !fPrivate ) ::SetHasPassBeenSet( TRUE );
		return FALSE;
	}

	// set privacy back to original value
	::SetIsPrivacyOn( fPrivate );
	if( !fPrivate ) ::SetHasPassBeenSet( TRUE );

	// delimiter
	CString szDelim;
	::GetDelimiter( szDelim );

	// Objects
	HRESULT hr = E_FAIL;
	IExperimentCondition* pEC = NULL;
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		return FALSE;
	}
	IExperimentMember* pEM = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		pEC->Release();
		return FALSE;
	}

	// Subjects of experiment
	hr = pEM->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOSUBJS );
		pEC->Release();
		pEM->Release();
		return FALSE;
	}
	// Get subjects/groups
	CStringList lstSubjs, lstGroups;
	BSTR bstrSubj = NULL, bstrGroup = NULL;
	CString szSubj, szGroup;
	while( SUCCEEDED( hr ) )
	{
		pEM->get_SubjectID( &bstrSubj );
		szSubj = bstrSubj;
		if( ( szSubj != _T("") ) && ( szSubj != _T("___") ) )
		{
			lstSubjs.AddTail( szSubj );

			pEM->get_GroupID( &bstrGroup );
			szGroup = bstrGroup;
			lstGroups.AddTail( szGroup );
		}

		hr = pEM->GetNext();
	}
	pEM->Release();
	int nNumSubjs = (int)lstSubjs.GetCount();

	// NSConditions of experiment
	hr = pEC->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOCONDS );
		pEC->Release();
		return FALSE;
	}
	// Get NSConditions
	CStringList lstConds;
	BSTR bstrCond = NULL;
	CString szCond, szTemp;
	short nCount = 0;
	while( SUCCEEDED( hr ) )
	{
		pEC->get_ConditionID( &bstrCond );
		szCond = bstrCond;
		if( szCond != HOLDER )
		{
			pEC->get_Replications( &nCount );
			szTemp.Format( _T("!%d"), nCount );
			szCond += szTemp;
			lstConds.AddTail( szCond );
		}

		hr = pEC->GetNext();
	}
	pEC->Release();
	int nNumConds = (int)lstConds.GetCount();

	// experiment type
	BOOL fGripper = FALSE;
	{
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExp ) ) )
		{
			short nType = EXP_TYPE_HANDWRITING;
			exp.GetType( nType );
			if( nType == EXP_TYPE_GRIPPER ) fGripper = TRUE;
		}
	}

	// Process Settings
	IProcSetting* pPS = NULL;
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return FALSE;
	}
	pPS->Find( szExp.AllocSysString() );
	// Use default if not found

	IProcess* pProcess = Experiments::GetProcessObject( pMsgList, pPS, szExp, TRUE );
	if( !pProcess )
	{
		pPS->Release();
		return FALSE;
	}

	// Process object
	short nRateVal = 0, nPressVal = 0;
	double dResoVal = 0.0;
	pPS->get_SamplingRate( &nRateVal );
	pProcess->put_SamplingRate( nRateVal );
	pPS->get_MinPenPressure( &nPressVal );
	pProcess->put_MinPenPressure( nPressVal );
	pPS->get_DeviceResolution( &dResoVal );
	pProcess->put_DeviceResolution( dResoVal );

	// get appropriate interface
	IProcSetFlags* pPSF = NULL;
	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) ) { pPS->Release(); return FALSE; }

	// Get settings
	BOOL fA, fR, fZ, fT, fS, fD, fD2, fSubM, fUDStrokes, fStrokes, fTrials, fMedian;
	short nDiscAfter = 0, nDiscAfter2 = 0;
	BOOL fDiscWOLift = FALSE, fDiscWLift = FALSE, fVal = FALSE;
	double dThreshold = 0.;
	pPSF->get_SEG_S( &fSubM );
	pPSF->get_ST_A( &fA );
	pPSF->get_ST_R( &fR );
	pPSF->get_ST_Z( &fZ );
	pPSF->get_ST_T( &fT );
	pPSF->get_ST_S( &fS );
	pPSF->get_ST_D( &fD );
	pPSF->get_DiscardAfter( &nDiscAfter );
	pPSF->get_ST_D2( &fD2 );
	pPSF->get_DiscardAfter2( &nDiscAfter2 );
	pPSF->get_DiscardPenDownDurMin( &fVal );
	if( fVal == 1 ) fDiscWOLift = TRUE;
	else if( fVal == 2 ) fDiscWLift = TRUE;
	pPSF->get_PenDownDurMin( &dThreshold );
	if( !fNormDB ) {
		pPS->get_CollapseUDStrokes( &fUDStrokes );
		pPS->get_CollapseStrokes( &fStrokes );
		pPS->get_CollapseTrials( &fTrials );
		pPS->get_CollapseMedian( &fMedian );
	}
	else {
		fUDStrokes = FALSE;
		fStrokes = FALSE;
		fTrials = FALSE;
		fMedian = FALSE;
	}
	UINT nSwitch = eST_None;
	if( fA ) nSwitch |= eST_A;
	if( fR ) nSwitch |= eST_R;
	if( fZ ) nSwitch |= eST_Z;
	if( fT ) nSwitch |= eST_T;
	if( fS ) nSwitch |= eST_S;
	if( fD ) nSwitch |= eST_D;
	if( fD2 ) nSwitch |= eST_D2;
	if( fSubM ) nSwitch |= eST_SM;
	// Consis parameters
	BOOL fConA, fConC, fConM, fConN, fConS, fConU, fConO, fConD;
	pPSF->get_CON_A( &fConA );
	pPSF->get_CON_C( &fConC );
	pPSF->get_CON_M( &fConM );
	pPSF->get_CON_N( &fConN );
	pPSF->get_CON_S( &fConS );
	pPSF->get_CON_U( &fConU );
	pPSF->get_CON_O( &fConO );
	pPSF->get_CON_D( &fConD );
	UINT conSwitch = eCon_None;
	if( fConA ) conSwitch |= eCon_A;
	if( fConC ) conSwitch |= eCon_C;
	if( fConM ) conSwitch |= eCon_M;
	if( fConN ) conSwitch |= eCon_N;
	if( fConS ) conSwitch |= eCon_S;
	if( fConU ) conSwitch |= eCon_U;
	if( fConO ) conSwitch |= eCon_O;
	pPSF->Release();
	pPS->Release();

	//////////////////////////////////////////////////
	// Loop through each subject and run summarization
	POSITION pos = NULL, pos2 = NULL;
	BOOL fSuccess = TRUE;
	CString szExt, szInc, szData, szErr;

	CWaitCursor crs;

	POSITION posSubj = lstSubjs.GetHeadPosition();
	POSITION posGrp = lstGroups.GetHeadPosition();
	CStringList lstFiles;
	while( posSubj )
	{
		// Get group & subject
		szSubj = lstSubjs.GetNext( posSubj );
		szGroup = lstGroups.GetNext( posGrp );
		szTemp.Format( _T("%s%s%s"), szGroup, szDelim, szSubj );

		// If from runexperiment..option to do this subject only...remove all others
		if( !fDlg && fOnly && ( szSubjID != _T("") ) )
		{
			// Correct subject?
			if( szSubj == szSubjID )
			{
				// Correct group?
				if( !fThisGroup || ( fThisGroup && ( szGroup == szGrpID ) ) )
				{
					// Now loop through each condition
					pos = lstConds.GetHeadPosition();
					while( pos )
					{
						szCond = lstConds.GetNext( pos );
						int nPos = szCond.Find( _T("!") );
						szTemp = szCond.Mid( nPos + 1 );
						szCond = szCond.Left( nPos );
						nCount = atoi( szTemp );

						// File names
						GetCON( szExp, szGroup, szSubj, szCond, szExt );

						if( !lstFiles.Find( szExt ) ) lstFiles.AddTail( szExt );
					}
				}
			}
		}
		else
		{
			// In exlude list?
			if( ( exDlg.m_lstExclude.Find( szTemp ) == NULL ) && ( szSubj != HOLDER ) )
			{
				// Now loop through each condition
				pos = lstConds.GetHeadPosition();
				while( pos )
				{
					szCond = lstConds.GetNext( pos );
					int nPos = szCond.Find( _T("!") );
					szTemp = szCond.Mid( nPos + 1 );
					szCond = szCond.Left( nPos );
					nCount = atoi( szTemp );

					// File names
					GetCON( szExp, szGroup, szSubj, szCond, szExt );

					if( !lstFiles.Find( szExt ) ) lstFiles.AddTail( szExt );
				}
			}
		}
	}

	// remove any files that dont exist
	BOOL fCont = TRUE;
	while( fCont )
	{
		CString szFile;
		POSITION pos = lstFiles.GetHeadPosition(), posLast = NULL;
		if( !pos ) fCont = FALSE;
		while( pos )
		{
			posLast = pos;
			szFile = lstFiles.GetNext( pos );
			if( !ff.FindFile( szFile ) )
			{
				lstFiles.RemoveAt( posLast );
				break;
			}
			else
			{
				fCont = FALSE;
				break;
			}
		}
	}

	// if no files, do nothing
	if( lstFiles.GetCount() <= 0 )
	{
		BCGPMessageBox( _T("No files to summarize.") );
		return FALSE;
	}

	// Now determine which feature columns to be excluded
	exDlg.m_fHeaders = TRUE;
	exDlg.m_szExtFile = lstFiles.GetHead();
	if( !fOnly && fDlg && ( exDlg.DoModal() == IDCANCEL ) ) return FALSE;


	// now determine which questions to be included/excluded (only if questions exist)
	if( !fOnly && fDlg )
	{
		BOOL fQuests = FALSE;
		{
		MQuestionnaires mq;
		if( SUCCEEDED( mq.ResetToStart() ) ) fQuests = TRUE;
		}
		if( fQuests )
		{
			exDlg.m_fQuests = TRUE;
			exDlg.m_fHeaders = FALSE;
			if( exDlg.DoModal() == IDCANCEL ) return FALSE;
		}
	}

	// Remove INC file
	GetExpINC( szExp, szInc );
	if( ff.FindFile( szInc ) )
	{
		try
		{
			CFile::Remove( szInc );
		}
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			CString szMsg = pszErr;
			szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
			BCGPMessageBox( szMsg, MB_ICONERROR );
			return FALSE;
		}
	}
	// Remove TMP file
	CString szTmp = szInc.Left( szInc.GetLength() - 3 );
	szTmp += _T("tmp");
	if( ff.FindFile( szTmp ) )
	{
		try
		{
			CFile::Remove( szTmp );
		}
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			CString szMsg = pszErr;
			szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
			BCGPMessageBox( szMsg, MB_ICONERROR );
			return FALSE;
		}
	}
	// Remove ERR file
	GetExpERR( szExp, szErr );
	if( ff.FindFile( szErr ) )
	{
		try
		{
			CFile::Remove( szErr );
		}
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			CString szMsg = pszErr;
			szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
			BCGPMessageBox( szMsg, MB_ICONERROR );
			return FALSE;
		}
	}
	// Remove AXS memory file
	szTmp = szInc.Left( szInc.GetLength() - 7 );	// exp.axs
	szTmp += _T("graph.axs");
	if( ff.FindFile( szTmp ) )
	{
		try
		{
			CFile::Remove( szTmp );
		}
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			CString szMsg = pszErr;
			szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
			BCGPMessageBox( szMsg, MB_ICONERROR );
			return FALSE;
		}
	}

	// Do we need to run consis?
	BOOL fRunConsis = FALSE;
	if( fDoConsis )
	{
		if( !fNormDB ) {
			szTmp = _T("Have the consistency settings changed since the last processing of trials?");
			if( BCGPMessageBox( szTmp, MB_YESNO | MB_ICONQUESTION ) == IDYES )
				fRunConsis = TRUE;
		}
		else fRunConsis = TRUE;
	}

	// Progress Meter steps
	int nSteps = (int)lstFiles.GetCount();
	Progress::EnableProgress( nSteps );
	int nStep = MAX( 1, MAX( nSteps / 100, 5 ) );

	szTemp.LoadString( IDS_AL_SUMM2 );
	szData.Format( _T("EXPERIMENT: %s"), szTemp );
	int nPos = szInc.ReverseFind( '\\' );
	szTemp = szInc.Mid( nPos + 1 );
	CString szBase = szData, szTemp2;
	szData.Format( _T("%s : %s"), szBase, szTemp );
	OutputAMessage( szData, TRUE );

	// ERR write headers
	CStdioFile fo;
//	if( fo.Open( szErr, CFile::modeWrite | CFile::modeCreate ) )
//	{
//		fo.WriteString( _T("'Group Subject Condition Trial ErrorType Description . . . . .\n") );
//		fo.Close();
//	}

	// Summarize
	CString szErr2, szLex, szLastSubj;
	short nMin = 0, nMax = 0, nSkip = 0, nGripTask = 0;
	double dLength = 0.0, dRange = 0.0, dDir = 0.0, dRangeDir = 0.0, dMagnetForce = 0.;
	long nCur = 0;
	BOOL fRunSum = FALSE, fPassed = FALSE, fWroteHeaders = FALSE, fFlagBadTarget = FALSE;

	NSConditions cond;
	if( !cond.IsValid() ) return FALSE;
	::SortStringListAlpha( &lstFiles );
	pos = lstFiles.GetHeadPosition();
	while( pos )
	{
		fPassed = FALSE;

		szTemp = lstFiles.GetNext( pos );
		nPos = szTemp.ReverseFind( '\\' );
		szTemp2 = szTemp.Mid( nPos + 1 );

		szGroup = szTemp2.Mid( 3, 3 );
		szSubj = szTemp2.Mid( 6, 3 );
		szCond = szTemp2.Mid( 9, 3 );

		// get count (search cond list to get # reps)
		pos2 = lstConds.GetHeadPosition();
		while( pos2 )
		{
			szTmp = lstConds.GetNext( pos2 );
			int nPos = szTmp.Find( _T("!") );
			szTemp2 = szTmp.Left( nPos );
			szTmp = szTmp.Mid( nPos + 1 );
			if( szTemp2 == szCond ) nCount = atoi( szTmp );
		}

		if( szLastSubj != szSubj )
		{
			szData.Format( _T("EXPERIMENT: Summarizing subject %s of group %s."), szSubj, szGroup );
			OutputAMessage( szData, TRUE );
		}
		szLastSubj = szSubj;

		// Run consis
		if( fRunConsis && SUCCEEDED( cond.Find( szCond ) ) )
		{
			cond.GetLex( szLex );
			cond.GetStrokeMin( nMin );
			cond.GetStrokeMax( nMax );
			cond.GetStrokeLength( dLength );
			cond.GetRangeLength( dRange );
			cond.GetStrokeDirection( dDir );
			cond.GetRangeDirection( dRangeDir );
			cond.GetStrokeSkip( nSkip );
			cond.GetFlagBadTarget( fFlagBadTarget );
			cond.GetGripperTask( nGripTask );
			cond.GetMagnetForce( dMagnetForce );
			UINT nGripSwitch = nGripTask;

			szExt = szTemp.Left( szTemp.GetLength() - 3 );
			szErr2 = szExt;
			szExt += _T("EXT");
			szErr2 += _T("ERR");

			if( ff.FindFile( szErr2 ) ) REMOVE_FILE( szErr2 );
			if( ff.FindFile( szTemp ) ) REMOVE_FILE( szTemp );

			Experiments::ProcessConsis( szExt, szTemp, szLex, nMin, nMax, dLength,
										dRange, dDir, dRangeDir, nSkip, fFlagBadTarget,
										szErr2, szCond, conSwitch, NULL, FALSE, fPassed,
										pMsgList, fConD, dMagnetForce, nGripSwitch );
		}
		else fPassed = TRUE;

		// set averaging
		pProcess->SetAveraging( fUDStrokes, fStrokes, fTrials, fMedian );

		// set include only w/out pen-lifts
		pProcess->IncludeOnlyStrokesPenlifts( fDiscWOLift, fDiscWLift, dThreshold );

		// call summarize
		hr = pProcess->SumTrial( szTemp.AllocSysString(), szInc.AllocSysString(),
								 szErr.AllocSysString(), szGroup.AllocSysString(),
								 szSubj.AllocSysString(), szCond.AllocSysString(),
								 nSwitch, nCount, nDiscAfter, nDiscAfter2, fOnly );
		fSuccess &= ( hr == S_OK );

		if( FAILED( hr ) )
		{
			CString szFileName = szTemp;
			nPos = szTemp.ReverseFind( '\\' );
			if( nPos != -1 ) szFileName = szTemp.Mid( nPos + 1 );
			szData.Format( _T("ERROR: # of columns in %s is inconsistent.\nThis is likely due to processing from an older application version.\n\nPlease reprocess all trials and repeat summarize."), szFileName );
			BCGPMessageBox( szData, MB_OK | MB_ICONERROR );
			Progress::DisableProgress();
			return FALSE;
		}

		nCur++;
		if( ( nCur % nStep ) == 0 )
		{
			szData.Format( _T("SUMMARIZING: Subject %s of Group %s"), szSubj, szGroup );
			Progress::SetProgress( nCur, szData );
		}

		// we only need 1 trial to have passed consis for there to be a valid inc file
		if( !fRunSum && fPassed ) fRunSum = TRUE;
	}

	// end progress
	Progress::DisableProgress();

#undef _DO_EPS
#if defined _DO_EPS
#pragma warning( disable: 4996 )
	CObList listVS;		// collection of VSObject's
	CStdioFile fileInc, fileIncTemp;
	CFileException fe;
	CString szLine, szWord, szSize = _T("VerticalSize"), szVel = _T("PeakVerticalVelocity");
	CString szIncTemp, szCurSubj;
	double dValSize = 0., dValVel = 0., dVal = 0.;
	FILE* infile = NULL;
	int nRet = 0;
	int nCol = -1, nSize = -1, nVel = -1;
	char pszGroup[ 4 ], pszSubj[ 4 ], pszCond[ 4 ], pszJunk[ 500 ];
	int nTrial = 0, nSegment = 0;
	BOOL fFirst = TRUE;

	// do not do this if gripper experiment
	if( fGripper ) goto NextStep;

	// VELOCITY SCALING
	// Progress Meter steps
	nSteps = 4;
	Progress::EnableProgress( nSteps );
	nStep = MAX( 1, MAX( nSteps / 100, nSteps ) );

	// open and loop through INC file to accumulate the data
	if( !fileInc.Open( szInc, CFile::modeRead ) ) goto SkipEPS;

	// progress meter
	szData = _T("SUMMARIZING: Velocity Scaling Score - Reading INC");
	Progress::SetProgress( 1, szData );

	// get first line (headers) and determine the column indices for vertical size & peak velocity
	if( !fileInc.ReadString( szLine ) ) goto SkipEPS;
	szLine.Trim();
	nPos = szLine.Find( _T(" ") );
	while( nPos >= 0 )
	{
		nCol++;
		szWord = szLine.Left( nPos );
		if( szWord == szSize ) nSize = nCol;
		else if( szWord == szVel ) nVel = nCol;
		szLine = szLine.Mid( nPos + 1 );
		nPos = szLine.Find( _T(" " ) );
		if( ( nPos == -1 ) && ( szLine.GetLength() != 0 ) )
		{
			nCol++;
			szWord = szLine;
			if( szWord == szSize ) nSize = nCol;
			else if( szWord == szVel ) nVel = nCol;
		}
	}
	if( ( nSize == -1 ) || ( nVel == -1 ) ) goto SkipEPS;

	// now go through each line of data, extract info and assign to appropriate object
	double dMissing = ::GetMissingDataValue();
	infile = fileInc.m_pStream;
	while( nRet != EOF )
	{
		// reset vars
		dValSize = dValVel = 0.;

		// scan in the group, subj, cond, trial & segment
		nRet = fscanf( infile, _T("%s %s %s %d %d"), pszGroup, pszSubj, pszCond, &nTrial, &nSegment );
		if( nRet <= 0 ) break;
		// loop through remainder of values...locating the size/vel columns
		for( int nCurCol = 5; nCurCol <= nCol; nCurCol++ )
		{
			// get next val
			nRet = fscanf( infile, _T("%lf"), &dVal );
			if( nRet <= 0 )
			{
				OutputAMessage( _T("ERROR: Summarize: VelocityScale calculation failed on reading the input file."), TRUE );
				goto SkipEPS;
			}
			// if curcol = size col...
			if( nCurCol == nSize )
			{
				dValSize = dVal;
			}
			// if curcol = vel col...
			else if( nCurCol == nVel )
			{
				dValVel = dVal;

				// since peak velocity comes after size, we are done searching, now process

				// if either are MISSING DATA VALUE, do not process
				if( ( dValVel != dMissing ) && ( dValSize != dMissing ) )
				{
					// search for existing VSObject based upon group, subj, cond.
					// if none found, create new and add condition and values
					VSObject* vs = VSObject::FindObject( &listVS, pszGroup, pszSubj, pszCond );
					if( !vs )
					{
						vs = new VSObject( pszGroup, pszSubj );
						listVS.AddTail( vs );
					}
					vs->AddCondition( pszCond );
					vs->Add( dValSize, dValVel );
				}

				// no point going through the rest of columns
				break;
			}
		}
		nRet = fscanf( infile, _T("%[^\n]"), pszJunk );
	}

	// progress meter
	szData = _T("SUMMARIZING: Velocity Scaling Score - Regression");
	Progress::SetProgress( 2, szData );

	// Run regression (RREGR) on each VSObject
	pos = listVS.GetHeadPosition();
	while( pos )
	{
		VSObject* vs = (VSObject*)listVS.GetNext( pos );
		if( vs )
		{
			szCurSubj = vs->GetSubject();
			if( szLastSubj == _T("") ) szLastSubj = szCurSubj;
			BOOL fOutput = TRUE;
			if( szCurSubj != szLastSubj ) fOutput = FALSE;
			vs->RunRegression( fOutput, fFirst );
			if( fFirst ) fFirst = FALSE;
		}
	}

	// Seek to beginning of file
	fileInc.SeekToBegin();

	// Open temporary file
	szIncTemp = szInc.Left( szInc.GetLength() - 4 );
	szIncTemp += _T("temp.INC");
	if( !fileIncTemp.Open( szIncTemp, CFile::modeWrite | CFile::modeCreate, &fe ) )
		goto SkipEPS;

	// progress meter
	szData = _T("SUMMARIZING: Velocity Scaling Score - Write Data");
	Progress::SetProgress( 3, szData );

	// Read column line (1st) and add new column and write to temp
	fileInc.ReadString( szLine );
	szLine.Trim();
	szLine += _T(" VelocityScalingScore\n");
	fileIncTemp.WriteString( szLine );
	// Loop through remainder of lines
	while( fileInc.ReadString( szLine ) )
	{
		szLine.Trim();
		// write line
		fileIncTemp.WriteString( szLine );
		// ...Get cond, subj, group and search for assoc. VSObject and get slope
		sscanf( szLine, _T("%s %s %s"), pszGroup, pszSubj, pszCond );
		VSObject* vs = VSObject::FindObject( &listVS, pszGroup, pszSubj, pszCond );
		if( vs )
		{
			// ...Get slope and write to end of line
			szTemp.Format( _T(" %g"), vs->GetSlope() );
			fileIncTemp.WriteString( szTemp );
		}
		fileIncTemp.WriteString( _T("\n") );
	}
	// close files
	fileInc.Close();
	fileIncTemp.Close();
	// delete original INC
	REMOVE_FILE( szInc );
	// rename temp to original INC
	MoveFile( szIncTemp, szInc );
	// progress meter
	szData = _T("SUMMARIZING: Velocity Scaling Score - Completed");
	goto NextStep;

SkipEPS:
	// close file
	if( fileInc.m_pStream ) fileInc.Close();
NextStep:
	// cleanup
	Progress::SetProgress( 4, szData );
	Progress::DisableProgress();
	pos = listVS.GetHeadPosition();
	while( pos )
	{
		VSObject* vs = (VSObject*)listVS.GetNext( pos );
		if( vs ) delete vs;
	}
#pragma warning( default: 4996 )
#endif

	// error summary
	if( fRunSum && ff.FindFile( szErr ) )
	{
		// output file
		CString szERS = szErr.Left( szErr.GetLength() - 1 );
		szERS += _T("s");
		szERS.MakeUpper();

		// open files
		CStdioFile fi;
		if( fi.Open( szErr, CFile::modeRead ) && fo.Open( szERS, CFile::modeWrite | CFile::modeCreate ) )
		{
			CString szLine, szTrial;
			CStringList lstErrors;
			int nCount = 0, nOK = 0, nItem = 0;

			// skip err headers
			fi.ReadString( szLine );

			// loop through the error file
			while( fi.ReadString( szLine ) )
			{
				// COLUMNS (source file):
				// 1 - group, 2 - subject, 3 - condition, 4 - trial
				// 8th column (currently) is error or OK

				// TODO: just doing all trials now

				// PARSE
				// group
				nPos = szLine.Find( _T(" ") );
				if( nPos != -1 )
				{
					szGroup = szLine.Left( nPos );
					szLine = szLine.Mid( nPos + 1 );
				}
				// subject
				nPos = szLine.Find( _T(" ") );
				if( nPos != -1 )
				{
					szSubj = szLine.Left( nPos );
					szLine = szLine.Mid( nPos + 1 );
				}
				// condition
				nPos = szLine.Find( _T(" ") );
				if( nPos != -1 )
				{
					szCond = szLine.Left( nPos );
					szLine = szLine.Mid( nPos + 1 );
				}
				// trial
				nPos = szLine.Find( _T(" ") );
				if( nPos != -1 )
				{
					szTrial = szLine.Left( nPos );
					szLine = szLine.Mid( nPos + 1 );
				}
				// error/ok
				if( nPos != -1 )
				{
					nPos = szLine.Find( _T(";") );
					if( nPos != -1 ) szTemp = szLine.Mid( nPos + 1 );
					else szTemp = _T("");
					TRIM( szTemp );
				}

				// now...check for success of trial (errors we add to list to evaluate at end)
				if( szTemp != _T("") )
				{
					nCount++;
					szTemp.MakeUpper();
					if( szTemp != _T("OK") )
					{
						// trim it (for brevity)
						nPos = szTemp.Find( _T(" ") );
						if( nPos != -1 ) szTemp = szTemp.Left( nPos );
						// add it
						lstErrors.AddTail( szTemp );
					}
					else nOK++;
				}
			}

			// summarize
			if( nCount > 0 )
			{
				// extract the most common error
				int nMost = Experiments::GetMostFrequentOccurrence( &lstErrors, szTemp );
				if( szTemp == _T("OK") ) szTemp = _T("");

				int nBad = nCount - nOK;
				double dOK = (double)nOK / (double)nCount * 100.;
				double dBad = (double)nBad / (double)nCount * 100.;
				fo.WriteString( _T("Nr - #Trials #OK Relative#OK #Errors Relative#Errors #MostFrequentError\n") );
				fo.WriteString( _T("All Trials\n") );
				szLine.Format( _T("1 - %d %d %.1f%% %d %.1f%% %s\n"), nCount, nOK, dOK, nBad, dBad, szTemp );
				fo.WriteString( szLine );
			}
		}
	}

	CListCtrl* pList = ::GetOutputWindow();
	if( pList ) pList->EnsureVisible( pList->GetItemCount() - 1, FALSE );

	if( !fSuccess ) BCGPMessageBox( IDS_SUMM_ERR );
	if( !fRunSum )
	{
		if( ff.FindFile( szInc ) ) REMOVE_FILE( szInc );
		BCGPMessageBox( _T("No trials were consistent. See error file.") );
		return FALSE;
	}

	CString szCmd;
	if( fRunSum && fInc && ff.FindFile( szInc ) )
	{
		szCmd.LoadString( IDS_EDITOR );
		szCmd += szInc;
		WinExec( szCmd, SW_SHOW );
	}
	if( fErr && ff.FindFile( szErr ) )
	{
		szCmd.LoadString( IDS_EDITOR );
		szCmd += szErr;
		WinExec( szCmd, SW_SHOW );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

int Experiments::GetMostFrequentOccurrence( CStringList* pList, CString& szError )
{
	if( !pList )
	{
		szError = _T("");
		return 0;
	}

	// get unique ones and place in separate list
	CString szItem;
	CStringList lstDone;
	int nCount = 0, nMax = 0;
	POSITION pos = pList->GetHeadPosition();
	while( pos )
	{
		szItem = pList->GetNext( pos );
		if( !lstDone.Find( szItem ) ) lstDone.AddTail( szItem );
	}

	// now loop through unique list and count from orig list
	CString szItem2;
	POSITION pos2 = lstDone.GetHeadPosition();
	while( pos2 )
	{
		nCount = 0;
		szItem2 = lstDone.GetNext( pos2 );
		pos = pList->GetHeadPosition();
		while( pos )
		{
			szItem = pList->GetNext( pos );
			if( szItem == szItem2 ) nCount++;
		}

		if( nCount > nMax )
		{
			nMax = nCount;
			szError = szItem2;
		}
	}

	return nMax;
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::Reprocess( CString szExpID, LPVOID itemView, LPVOID msgView )
{
	Experiments::_Reproc = TRUE;
	Experiments::_TerminateThread = FALSE;
	Experiments::_ExpID = szExpID;
	Experiments::_ItemView = itemView;
	Experiments::_MsgView = msgView;

	_pMainThread = ::AfxGetThread();
	CWinThread* pThread = AfxBeginThread( Experiments::ReprocessExperiment,
										  AfxGetApp()->m_pMainWnd );
}

///////////////////////////////////////////////////////////////////////////////

class SubjGrpItem : public CObject
{
public:
	CString	szGrpID;
	CString	szSubjID;
	double	dDevRes;
	short	nSampRate;
};

UINT Experiments::ReprocessExperiment( LPVOID pParam )
{
	HRESULT hRes = CoInitialize( NULL );

	UserObj* pObj = ::GetCurrentUserObj();
	if( !pObj ) return -1;
	pObj->m_nState = STATE_REPROCESS;

	// Which experiment
	CString szExpID = Experiments::_ExpID;

	// Delete ERR file
	CString szErr;
	::GetExpERR( szExpID, szErr );
	CFileFind ff;
	if( ff.FindFile( szErr ) ) REMOVE_FILE( szErr );

	// Loop through all subjects in experiment to get a count of total subjects
	CObList lstItems;
	BSTR bstr = NULL;
	CString szSubjID, szGrpID;
	int nCount = 0, nCur = 0;
	double dDevRes = 0.;
	short nSampRate = 0;
	IExperimentMember* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) ) return -1;
	if( !pObj || ( pObj->m_szDataPath == _T("") ) )
	{
		CString szPath;
		::GetDataPathRoot( szPath );
		pEM->SetDataPath( szPath.AllocSysString() );
	}
	else pEM->SetDataPath( pObj->m_szDataPath.AllocSysString() );
	hr = pEM->FindForExperiment( szExpID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEM->get_GroupID( &bstr );
		szGrpID = bstr;
		pEM->get_SubjectID( &bstr );
		szSubjID = bstr;
		pEM->get_DeviceRes( &dDevRes );
		pEM->get_SamplingRate( &nSampRate );

		if( ( szGrpID != HOLDER ) && ( szSubjID != HOLDER ) )
		{
			SubjGrpItem* pItem = new SubjGrpItem;
			if( pItem )
			{
				nCount++;
				pItem->szGrpID = szGrpID;
				pItem->szSubjID = szSubjID;
				pItem->dDevRes = dDevRes;
				pItem->nSampRate = nSampRate;
				lstItems.AddTail( pItem );
			}
		}

		// next record
		hr = pEM->GetNext();
	}
	pEM->Release();

	// Loop through all subjects in experiment
	Progress::UseCallbacks( TRUE );
	POSITION pos = lstItems.GetHeadPosition();
	while( pos && !Experiments::_TerminateThread )	// loop through each group
	{
		SubjGrpItem* pItem = (SubjGrpItem*)lstItems.GetNext( pos );

		// Reprocess this subject if appropriate (not holder)
		if( pItem && ( pItem->szGrpID != HOLDER ) && ( pItem->szSubjID != HOLDER ) )
		{
			nCur++;
			Experiments::ReprocessSubject( szExpID, pItem->szSubjID, pItem->szGrpID,
										   pItem->dDevRes, pItem->nSampRate, NULL,
										   Experiments::_ItemView, (CWnd*)Experiments::_MsgView,
										   nCount, nCur, FALSE, TRUE, TRUE,
										   Experiments::_ReprocAll );
		}
	}
	Progress::UseCallbacks( FALSE );

	// cleanup
	pos = lstItems.GetHeadPosition();
	while( pos )
	{
		SubjGrpItem* pItem = (SubjGrpItem*)lstItems.GetNext( pos );
		if( pItem ) delete pItem;
	}
	lstItems.RemoveAll();
	pObj->m_nState = STATE_NONE;
	Experiments::_Reproc = FALSE;
	Experiments::_ReprocAll = FALSE;
	Experiments::_TerminateThread = FALSE;
	Experiments::Cleanup();

	CoUninitialize();

	// inform all we imported
	if( _pMainThread )
	{
		CWnd* pMainWnd = _pMainThread->GetMainWnd();
		if( pMainWnd ) ::PostMessage( pMainWnd->m_hWnd, WM_COMMAND, UM_REFRESH, 0 );
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

void Experiments::ReprocessSubject( CString szExpID, CString szSubjID, CString szGrpID,
									double dDevRes, int nSampRate, HTREEITEM hItem, LPVOID param,
									CWnd* pMsgList, int nSubjCount, int nCurSubj, BOOL fInformIfEmpty,
									BOOL fSubCond, BOOL fSeparateThread, BOOL fReprocAll )
{
	CWaitCursor crs;

	CFileFind ff;
	CString szRaw, szLex, szFile, szData, szTitle, szSeq;
	short nMin = 0, nMax = 0, nPos = 0, nSkip = 0, nGripTask = 0;
	double dLength = 0.0, dRange = 0.0, dDir = 0.0, dRangeDir = 0.0, dVal = 0.0;
	BOOL fRecord = FALSE, fProcess = FALSE, fFlagBadTarget = FALSE;
	short nSmoothIter = 0 , nInkThreshold = 0, nImgReso = 0;
	double dMinSegLen = 0., dMissing = 0.;
	short nWord = 0;
	CString szParent, szSen, szIdx, szTemp;
	CStringList lstRaw;
	POSITION pos, pos2;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	szData.Format( IDS_AL_REPROC, szSubjID, szGrpID, szExpID, nCurSubj, nSubjCount );
	OutputAMessage( szData, TRUE );

	// find experiment
	short nType = EXP_TYPE_HANDWRITING;
	BOOL fImage = FALSE, fGripper = FALSE;
	HRESULT hr = E_FAIL;
	{
	Experiments exp;
	if( !exp.IsValid() ) return;
	hr = exp.Find( szExpID );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPFIND_ERR );
		return;
	}
	// experiment type
	exp.GetType( nType );
	exp.GetMissingDataValue( dMissing );
	fImage = ( nType == EXP_TYPE_IMAGE );
	fGripper = ( nType == EXP_TYPE_GRIPPER );
	}

	Experiments::_Reproc = TRUE;

	// Process Settings
	IProcSetting* pPS = NULL;
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		Experiments::_Reproc = FALSE;
		return;
	}
	pPS->Find( szExpID.AllocSysString() );
	// Use default if not found

	// image processing settings
	if( nType == EXP_TYPE_IMAGE )
	{
		pPS->get_SmoothingIter( &nSmoothIter );
		pPS->get_InkThreshold( &nInkThreshold );
		pPS->get_ImgResolution( &nImgReso );
		pPS->get_MinStrokeSize( &dMinSegLen );
	}

	// Process object
	IProcess* pProcess = Experiments::GetProcessObject( pMsgList, pPS, szExpID, TRUE );
	hr = pProcess ? pProcess->SetOutputWindow( (VARIANT*)pMsgList ) : E_FAIL;
	if( FAILED( hr ) )
	{
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}
	if( pProcess )
	{
		if( !fImage && ( nSampRate == 0 ) )
		{
			szData = "EXPERIMENT: Subject sampling rate is not set - using experiment value.";
			OutputAMessage( szData );
		}
		if( !fImage && ( dDevRes == 0. ) )
		{
			szData = "EXPERIMENT: Subject device resolution is not set - using experiment value.";
			OutputAMessage( szData );
		}

		short nRateVal = 0, nPressVal = 0;
		double dResoVal = 0.0;
		pPS->get_SamplingRate( &nRateVal );
		if( nSampRate != 0 )
		{
			if( !fImage && ( nRateVal != nSampRate ) )
			{
				szData.Format( _T("EXPERIMENT: Subject sampling rate (%d) differs from experiment (%d). Using subject value."), nSampRate, nRateVal );
				OutputAMessage( szData );
			}
			nRateVal = nSampRate;
		}
		pProcess->put_SamplingRate( nRateVal );
		pPS->get_MinPenPressure( &nPressVal );
		pProcess->put_MinPenPressure( nPressVal );
		pPS->get_DeviceResolution( &dResoVal );
		if( dDevRes != 0. )
		{
			if( !fImage && ( dDevRes != dResoVal ) )
			{
				szData.Format( _T("EXPERIMENT: Subject device resolution (%g) differs from experiment (%g). Using subject value."), dDevRes, dResoVal );
				OutputAMessage( szData );
			}
			dResoVal = dDevRes;
		}
		pProcess->put_DeviceResolution( dResoVal );
		short nDisCorrection = 0;
		double dDisFactor = 0.0, dDisZInsPoint = 0.0, dDisRelErr = 0.0, dDisAbsErr = 0.0;
		pPS->get_DisCorrection( &nDisCorrection );
		pPS->get_DisFactor( &dDisFactor );
		pPS->get_DisZInsertPoint( &dDisZInsPoint );
		pPS->get_DisRelError( &dDisRelErr );
		pPS->get_DisAbsError( &dDisAbsErr );
		pProcess->Discontinuity( nDisCorrection, dDisFactor, dDisZInsPoint, dDisRelErr, dDisAbsErr );
		double dDisError = 0.;
		pPS->get_DisError( &dDisError );
		pProcess->put_DisError( dDisError );
	}
	else
	{
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}

	// NSConditions
	IExperimentCondition* pECL = NULL;
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pECL );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCONDL_ERR );
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}
	hr = pECL->FindForExperiment( szExpID.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOCONDS );
		pECL->Release();
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}
	NSConditions cond;
	if( !cond.IsValid() )
	{
		pPS->Release();
		pECL->Release();
		Experiments::_Reproc = FALSE;
		return;
	}

	// generate sequence file if does not exist
	::GetERR( szExpID, szGrpID, szSubjID, _T(""), szSeq );
	szSeq = szSeq.Left( szSeq.GetLength() - 3 );
	szSeq += _T("SEQ");
	if( !ff.FindFile( szSeq ) )
	{
		Subjects::GenerateTrialSequence( szExpID, szGrpID, szSubjID, nType );
	}
	
	CStringList lstConditions, lstDeleted;
	BSTR bstrCondID = NULL;
	CString szCondID;
	BOOL fCont = TRUE;
	int nSteps = 0;
	// loop through NSConditions to see how many (if any) sen2word steps there are
	// SKIP if image-type experiment
	while( !fImage && !fGripper && SUCCEEDED( hr ) )
	{
		pECL->get_ConditionID( &bstrCondID );
		szCondID = bstrCondID;

		if( SUCCEEDED( cond.Find( bstrCondID ) ) )
		{
			cond.GetRecord( fRecord );
			cond.GetProcess( fProcess );
			cond.GetParent( szParent );

			// Only if process/handwriting/not record/and legit parent
			if( ( nType == EXP_TYPE_HANDWRITING ) && fProcess && !fRecord &&
				( szParent != _T("") ) && fSubCond )
			{
				// how many trials
				GetHWRMask( szExpID, szGrpID, szSubjID, szParent, szFile );
				fCont = ff.FindFile( szFile );
				while( fCont )
				{
					fCont = ff.FindNextFile();
					szRaw = ff.GetFilePath();
					nSteps++;
				}
			}
		}

		// Next condition
		hr = pECL->GetNext();
	}

	// Progress Meter steps
	Progress::EnableProgress( nSteps );
	int nStep = 0;
	if( !fSeparateThread || fReprocAll ) nStep = MAX( 1, MAX( nSteps / 100, 5 ) );
	else nStep = MAX( 1, nSteps / 5 );
	int nCurStep = 0;
	CStringList lstCondWords;
	CString szCondWord, szWordDis, szWordDisOut;

	// now loop through NSConditions again & actually do work
	hr = pECL->FindForExperiment( szExpID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pECL->get_ConditionID( &bstrCondID );
		szCondID = bstrCondID;

		if( SUCCEEDED( cond.Find( bstrCondID ) ) )
		{
			cond.GetLex( szLex );
			cond.GetStrokeMin( nMin );
			cond.GetStrokeMax( nMax );
			cond.GetStrokeLength( dLength );
			cond.GetRangeLength( dRange );
			cond.GetStrokeDirection( dDir );
			cond.GetRangeDirection( dRangeDir );
			cond.GetStrokeSkip( nSkip );
			cond.GetRecord( fRecord );
			cond.GetProcess( fProcess );
			cond.GetWord( nWord );
			cond.GetParent( szParent );
			cond.GetFlagBadTarget( fFlagBadTarget );
			cond.GetGripperTask( nGripTask );

			szData.Format( _T("%s!%s!%d!%d!%f!%f!%f!%f!%d!%d!%d!%d"), szCondID, szLex,
						   nMin, nMax, dLength, dRange, dDir, dRangeDir, nSkip, fProcess,
						   fFlagBadTarget, nGripTask );
			lstConditions.AddTail( szData );

			// Only if process
			if( fProcess )
			{
				// TODO: Note...this is not likely the most efficient manner at doing this
				// ....IDEA: make some table with all sub-NSConditions for a given parent
				// ..........then run sen2word once for that condition & copy appropriately
				// Do we run sen2word? parent would not be null...only handwriting...
				// ...and only ones with process but not record (should be forced by dialog)
				if( ( nType == EXP_TYPE_HANDWRITING ) && !fRecord &&
					( szParent != _T("") ) && fSubCond )
				{
					// Get all trials for this condition (recorded)
					lstRaw.RemoveAll();
					GetHWRMask( szExpID, szGrpID, szSubjID, szParent, szFile );
					fCont = ff.FindFile( szFile );
					while( fCont )
					{
						fCont = ff.FindNextFile();
						szRaw = ff.GetFilePath();
						lstRaw.AddTail( szRaw );
					}

					// Delete sub-condition trials (from previous processing)
					if( !lstDeleted.Find( szCondID ) )
					{
						GetHWRMask( szExpID, szGrpID, szSubjID, szCondID, szFile );
						fCont = ff.FindFile( szFile );
						while( fCont )
						{
							fCont = ff.FindNextFile();
							szRaw = ff.GetFilePath();
							REMOVE_FILE( szRaw );
						}

						lstDeleted.AddTail( szCondID );
					}

					// Loop through each trial
					pos = lstRaw.GetHeadPosition();
					while( pos )
					{
						// Trial
						szRaw = lstRaw.GetNext( pos );
						szData = szRaw;

						// Sen2word file naming convention
						szIdx = szRaw.Mid( szRaw.GetLength() - 6, 2 );
						GetSEN( szExpID, szGrpID, szSubjID, szParent, szIdx, szSen );

						// Run sen2word (TODO: Does sen2word return FAIL if only one word)
						// ...if this trial for parent condition has not already been run
						szCondWord = szParent;
						szCondWord += szIdx;
						if( !lstCondWords.Find( szCondWord ) )
						{
							lstCondWords.AddTail( szCondWord );
							Experiments::ProcessSen2wrd( szRaw, szSen, pMsgList );
						}

						// Copy appropriate word file (if exists)
						// Idx based upon word (3 chars = 1-999)
						szIdx.Format( _T("%03d"), nWord );
						// Construct source file name
						szRaw = szSen.Left( szSen.GetLength() - 4 );
						szRaw += szIdx;
						szWordDis = szRaw;
						szRaw += _T(".SEN");
						szWordDis += _T(".DIS");
						// Construct target file names
						szIdx = szData.Mid( szData.GetLength() - 6, 2 );
						GetHWR( szExpID, szGrpID, szSubjID, szCondID, szIdx, szData );
						szWordDisOut = szData.Left( szData.GetLength() - 3 );
						szWordDisOut += _T("DIS");
						// Copy raw file (if exists)
						if( ff.FindFile( szRaw ) ) CopyFile( szRaw, szData, FALSE );
						// Copy dis file (if exists)
						if( ff.FindFile( szWordDis ) )
						{
							CopyFile( szWordDis, szWordDisOut, FALSE );
							// delete the file
							try { CFile::Remove( szWordDis ); } catch( CFileException* e ) { e->Delete(); }
						}

						// Update progress on status bar
						nCurStep++;
						if( ( nCurStep % nStep ) == 0 )
						{
							dVal = ( ( 1.0 * nCurStep ) / ( 1.0 * nSteps ) ) * 100.0;
							szData.Format( _T("Progress %.1f %% - Word extraction completed for condition %s"), dVal, szCondID );
							Progress::SetProgress( nCurStep, szData );
						}
					}
				}
			}

			// Next condition
			hr = pECL->GetNext();
		}
		else
		{
			CString szMsg;
			szMsg.Format( _T("Unable to locate condition %s"), szCondID );
			BCGPMessageBox( szMsg );
			pPS->Release();
			pECL->Release();
			Experiments::_Reproc = FALSE;
			Progress::DisableProgress();
			return;
		}
	}
	pECL->Release();

	// Delete any SEN files that were created
	pos = lstCondWords.GetHeadPosition();
	while( pos )
	{
		szParent = lstCondWords.GetNext( pos );
		GetSENMask( szExpID, szGrpID, szSubjID, szParent, szData );
		if( ff.FindFile( szData ) )
		{
			while( ff.FindNextFile() )
			{
				szData = ff.GetFilePath();
				REMOVE_FILE( szData );
			}

			// last one
			szData = ff.GetFilePath();
			REMOVE_FILE( szData );
		}
	}

	// end progress meter for sen2word
	Progress::DisableProgress();

	// Sort the NSConditions alpha if selected
	if( ::IsTrialAlphaOn() ) ::SortStringListAlpha( &lstConditions );

	// Delete processing files if reprocessing all or image experiment
	if( fReprocAll || fImage )
	{
		Subjects::CleanProcessFiles( szExpID, szGrpID, szSubjID, nType, FALSE,
									 TRUE, TRUE, TRUE, TRUE, TRUE );
	}

	// Loop through all PCX files and SFINX each if necessary
	if( fImage )
	{
		// PCX file mask (all PCXs)
		::GetPCXMask( szExpID, szGrpID, szSubjID, szFile );

		CString szCurPCX, szCurHWR, szTrialNum;
		CFileFind ffHWR;
		if( ff.FindFile( szFile ) )
		{
			int nTrial = 0;
			fCont = TRUE;
			while( fCont )
			{
				fCont = ff.FindNextFile();
				szCurPCX = ff.GetFilePath();
				szCurPCX.MakeUpper();
				// make sure this is not a processed PCX (xxxx-F.PCX)
				if( szCurPCX.Right( 6 ) != _T("-F.PCX") )
				{
					// search for HWR for this PCX
					// if not there, SFINX IT
					szCurHWR = szCurPCX.Left( szCurPCX.GetLength() - 3 );
					szCurHWR += _T("HWR");
					szTrialNum = szCurPCX.Mid( szCurPCX.GetLength() - 6, 2 );
					szCondID = szCurPCX.Mid( szCurPCX.GetLength() - 9, 3 );
					nTrial = atoi( szTrialNum );
					SFINX::RunSFINX( ff.GetRoot(), szCurPCX, nTrial, nSmoothIter, nInkThreshold,
									 nImgReso, dMinSegLen, dMissing );

					if( hItem && ::GetDetailedOutput() )
					{
						CString szTrial;
						int nPos = szCurPCX.ReverseFind( '\\' );
						if( nPos != -1 ) szTrial = szCurPCX.Mid( nPos + 1 );

						szData.Format( _T("%s%s%s%s%s%s%s%s%s%s%s%s%s%s0%s1%s%s"), szExpID,
							szDelim, szSubjID, szDelim, szGrpID, szDelim, szCondID, szDelim,
							_T(" "), szDelim, szTrial, szDelim, _T("1"),
							szDelim, szDelim, szDelim, fImage ? _T("1") : _T("0") );
						_hItem = hItem;

						MessageManager* pMM = MessageManager::GetMessageManager();
						pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_SHOWTRIAL ),
							(WPARAM)( szData.GetBuffer() ) );
					}
				}
			}
		}

		pPS->Release();
		Experiments::_Reproc = FALSE;
		Experiments::Cleanup();
		return;
	}

	// Loop through each persisted trial to get count for progress meter
	nSteps = 0;
	if( nType == EXP_TYPE_GRIPPER ) ::GetFRDMask( szExpID, szGrpID, szSubjID, szFile );
	else ::GetHWRMask( szExpID, szGrpID, szSubjID, szFile );

	if( ff.FindFile( szFile ) )
	{
		while( ff.FindNextFile() ) nSteps++;
		// Last one
		nSteps++;
	}
	else
	{
		if( fInformIfEmpty ) BCGPMessageBox( IDS_NOTRIALS );
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}

	// Progress Meter steps
	Progress::EnableProgress( nSteps );
	if( !fSeparateThread || fReprocAll ) nStep = MAX( 1, MAX( nSteps / 100, 5 ) );
	else nStep = MAX( 1, nSteps / 5 );
	nCurStep = 0;

	// Process each
	int nRep = 1, nSkipped = 0, nToProcess = 0;
	pos = lstConditions.GetHeadPosition();
	while( pos && !Experiments::_TerminateThread )
	{
		BOOL fCanDel = TRUE;

		szData = lstConditions.GetNext( pos );
		// ID
		nPos = szData.Find( _T("!") );
		szCondID = szData.Left( nPos );
		szData = szData.Mid( nPos + 1 );
		// Lex
		nPos = szData.Find( _T("!") );
		szLex = szData.Left( nPos );
		szData = szData.Mid( nPos + 1 );
		// StrokeMin
		nPos = szData.Find( _T("!") );
		nMin = atoi( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// StrokeMax
		nPos = szData.Find( _T("!") );
		nMax = atoi( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Target Stroke Length
		nPos = szData.Find( _T("!") );
		dLength = atof( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Range of Target Stroke Length
		nPos = szData.Find( _T("!") );
		dRange = atof( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Target Stroke Direction
		nPos = szData.Find( _T("!") );
		dDir = atof( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Range of Target Stroke Direction
		nPos = szData.Find( _T("!") );
		dRangeDir = atof( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Segments to skip
		nPos = szData.Find( _T("!") );
		nSkip = atoi( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Process?
		nPos = szData.Find( _T("!") );
        fProcess = atoi( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Flag Bad Target
		nPos = szData.Find( _T("!") );
		fFlagBadTarget = atoi( szData.Left( nPos ) );
		szData = szData.Mid( nPos + 1 );
		// Gripper Task
		nGripTask = atoi( szData );
		UINT nGripSwitch = nGripTask;

		// Remove ext/err files
		if( fReprocAll )
		{
			CString szExt;
			GetEXT( szExpID, szGrpID, szSubjID, szCondID, szExt );
			if( ff.FindFile( szExt ) ) REMOVE_FILE( szExt );
			GetERR( szExpID, szGrpID, szSubjID, szCondID, szExt );
			if( ff.FindFile( szExt ) ) REMOVE_FILE( szExt );
		}

		// Loop through each data file for this cond
		lstRaw.RemoveAll();
		if( nType == EXP_TYPE_GRIPPER )
			GetFRDMask( szExpID, szGrpID, szSubjID, szCondID, szFile );
		else if( nType == EXP_TYPE_HANDWRITING )
			GetHWRMask( szExpID, szGrpID, szSubjID, szCondID, szFile );
		if( ff.FindFile( szFile ) )
		{
			while( ff.FindNextFile() )
			{
				szRaw = ff.GetFilePath();
				lstRaw.AddTail( szRaw );
			}
			// Last one
			szRaw = ff.GetFilePath();
			lstRaw.AddTail( szRaw );
		}

		BOOL fFirst = TRUE;
		static CString szLastSubj;
		if( szLastSubj == szSubjID ) fFirst = FALSE;
		szLastSubj = szSubjID;

		pos2 = lstRaw.GetHeadPosition();
		while( pos2 )
		{
			szRaw = lstRaw.GetNext( pos2 );
			// Only do if process flag checked
			if( fProcess )
			{
				nToProcess++;
				Experiments::ProcessTrial( szExpID, szSubjID, szGrpID, szCondID, szRaw, TRUE,
										   fFlagBadTarget, pPS, param, pMsgList, nSkipped,
										   TRUE, hItem, fReprocAll, fFirst, nGripSwitch );
				fFirst = FALSE;

				// Update progress on status bar
				int nPos = szRaw.ReverseFind( '\\' );
				if( nPos != -1 ) szFile = szRaw.Mid( nPos + 1 );
				nCurStep++;
				if( ( nCurStep % nStep ) == 0 )
				{
					dVal = ( ( 1.0 * nCurStep ) / ( 1.0 * nSteps ) ) * 100.0;
					szData.Format( _T("Progress %.1f %% - Processing trial %s"), dVal, szFile );
					Progress::SetProgress( nCurStep, szData );
				}
			}
			// else just redisplay if flag not set
			else if( hItem && ::GetDetailedOutput() )
			{
				CString szTrial;
				int nPos = szRaw.ReverseFind( '\\' );
				if( nPos != -1 ) szTrial = szRaw.Mid( nPos + 1 );

				szData.Format( _T("%s%s%s%s%s%s%s%s%s%s%s%s%s%s0%s1%s%s"), szExpID,
					szDelim, szSubjID, szDelim, szGrpID, szDelim, szCondID, szDelim,
					_T(" "), szDelim, szTrial, szDelim, _T("1"),
					szDelim, szDelim, szDelim, fImage ? _T("1") : _T("0") );
				_hItem = hItem;

				MessageManager* pMM = MessageManager::GetMessageManager();
				pMM->PostMessage( MAKELPARAM( MSG_ACTION, MSG_SUB_SHOWTRIAL ),
					(WPARAM)( szData.GetBuffer() ) );
			}
		}
	}

	// inform that some trials might not be visible
	if( !fSeparateThread && ( nSkipped > 0 ) && ::GetDetailedOutput() )
		BCGPMessageBox( _T("Some trials were already processed and are not visible in the tree view.\nTo show all trials, View -> Refresh.") );

	// Cleanup
	pPS->Release();

	// Stop status bar animation
	Progress::DisableProgress();

	Experiments::_Reproc = FALSE;
}

///////////////////////////////////////////////////////////////////////////////

CTime Experiments::LastRun( CString szSubjID, CString& szGrpID )
{
	CTime tmLast = 0, tmFind;
	CString szTrials, szID, szSubj, szGrp;
	BSTR bstr = NULL;
	IExperimentMember* pEM = NULL;
	HRESULT hr = S_OK;
	BOOL fOnlyOne = ( szGrpID != _T("") );

	// experiment ID
	GetID( szID );

	// grp/subj list if group is empty
	if( !fOnlyOne )
	{
		hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								 IID_IExperimentMember, (LPVOID*)&pEM );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPMEM_ERR );
			return 0;
		}
		hr = pEM->FindForExperiment( szID.AllocSysString() );
		if( FAILED( hr ) )
		{
			pEM->Release();
			return 0;
		}
	}

	// loop through (only one if group specified)
	while( SUCCEEDED( hr ) )
	{
		// get subject
		if( !fOnlyOne )
		{
			pEM->get_SubjectID( &bstr );
			szSubj = bstr;
		}
		else szSubj = szSubjID;
		// if the correct subject, proceed
		if( szSubj == szSubjID )
		{
			// get group
			if( !fOnlyOne )
			{
				pEM->get_GroupID( &bstr );
				szGrp = bstr;
			}
			else szGrp = szGrpID;

			// trials to search
			::GetHWRMask( szID, szGrp, szSubjID, szTrials );

			// exp type
			short nType = EXP_TYPE_HANDWRITING;
			GetType( nType );
			if( nType != EXP_TYPE_HANDWRITING ) ::GetFRDMask( szID, szGrpID, szSubjID, szTrials );

			// search for oldest trial
			CFileFind ff;
			if( ff.FindFile( szTrials ) )
			{
				BOOL fCont = TRUE;
				while( fCont )
				{
					fCont = ff.FindNextFile();
					ff.GetLastWriteTime( tmFind );
					if( tmFind > tmLast )
					{
						tmLast = tmFind;
						szGrpID = szGrp;
					}
				}
			}
		}

		if( !fOnlyOne ) hr = pEM->GetNext();
		else hr = E_FAIL;
	}

	if( pEM ) pEM->Release();

	return tmLast;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::GetInstructionFile( CString szExpID, CString& szFile, BOOL fNoExt )
{
	CString szCheck;

	// path
	::GetExpNoExt( szExpID, szCheck );

	// instruction file
	szFile = szCheck;
	szCheck += _T(".rtf");

	// does file exist?
	CFileFind ff;
	BOOL fFound = ff.FindFile( szCheck );

	// add extension if specified
	if( !fNoExt ) szFile = szCheck;

	return fFound;
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Experiments::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pExperiment ) m_pExperiment->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoEditDup( CString szID, CWnd* pOwner )
{
	return Experiments::DoEditDup( m_pExperiment, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::DoEditDup( IExperiment* pExperiment, CString szID, CWnd* pOwner )
{
	if( !pExperiment ) return FALSE;

	CStringList lstExps;
	CString szItem, szEID, szDesc, szDelim;
	BSTR bstr;
	::GetDelimiter( szDelim );
	HRESULT hr = pExperiment->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pExperiment->get_ID( &bstr );
		szEID = bstr;
		pExperiment->get_Description( &bstr );
		szDesc = bstr;
		szItem.Format( _T("%s%s%s"), szDesc, szDelim, szEID );
		lstExps.AddTail( szItem );

		hr = pExperiment->GetNext();
	}

	if( FAILED( Experiments::Find( pExperiment, szID ) ) ) return FALSE;

	// Create ProcSettings object
	IProcSetting* pPS = NULL;
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR, MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// experiment type
	CString szNotes;
	short nType = 0;
	double dMVD = 0.;
	Experiments::GetDescription( pExperiment, szDesc );
	Experiments::GetNotes( pExperiment, szNotes );
	Experiments::GetType( pExperiment, nType );
	Experiments::GetMissingDataValue( pExperiment, dMVD );

	// Attempt to locate existing proc settings
	// ...if failed here, means not found (will add)
	hr = pPS->Find( szID.AllocSysString() );

	ProcSetSheet d( pPS, pOwner, nType == EXP_TYPE_GRIPPER, FALSE, 0, TRUE );
	d.m_szID = szID;
	d.m_szDesc = szDesc;
	d.m_szNotes = szNotes;
	d.m_nType = nType;
	d.m_dMDV = dMVD;
	if( d.DoModal() == IDOK )
	{
		CWaitCursor crs;

		// add experiment
		pExperiment->put_ID( d.m_szID.AllocSysString() );
		pExperiment->put_Description( d.m_szDesc.AllocSysString() );
		pExperiment->put_Notes( d.m_szNotes.AllocSysString() );
		pExperiment->put_Type( d.m_nType );
		pExperiment->put_MissingDataValue( d.m_dMDV );
		HRESULT hr = pExperiment->Add();
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to add experiment."), MB_OK | MB_ICONERROR );
			pPS->Release();
			return FALSE;
		}

		// add experiment settings
		pPS->put_ExperimentID( d.m_szID.AllocSysString() );
		hr = pPS->Add();
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to add experiment settings."), MB_OK | MB_ICONERROR );
			return FALSE;
		}
		pPS->Release();
		
		// copy the children of the experiment
		IExperimentCondition* pEC = NULL;
		IExperimentMember* pEM = NULL;
		hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
								 IID_IExperimentCondition, (LPVOID*)&pEC );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create ExperimentCondition object." ), MB_OK | MB_ICONERROR );
			return FALSE;
		}
		hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								 IID_IExperimentMember, (LPVOID*)&pEM );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create ExperimentMember object." ), MB_OK | MB_ICONERROR );
			pEC->Release();
			return FALSE;
		}
//		CStringList lstItems;
		HRESULT hr2 = S_OK;
//		short nReps = 0;
//		CString szCID;
		hr = pEC->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
// 			pEC->get_ConditionID( &bstr );
// 			szCID = bstr;
// 			pEC->get_Replications( &nReps );
			pEC->put_ExperimentID( d.m_szID.AllocSysString() );
			hr2 = pEC->Add();
			if( FAILED( hr2 ) )
				BCGPMessageBox( _T("Unable to add experiment-condition relationship."),
								MB_OK | MB_ICONERROR );
			hr = pEC->GetNext();
		}
		pEC->Release();

		hr = pEM->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			pEM->put_ExperimentID( d.m_szID.AllocSysString() );
			hr2 = pEM->Add();
			if( FAILED( hr2 ) )
				BCGPMessageBox( _T("Unable to add experiment-member relationship."),
								MB_OK | MB_ICONERROR );
			hr = pEM->GetNext();
		}
		pEM->Release();

		return TRUE;
	}
	pPS->Release();

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::IsMatlabInstalled()
{
	CWaitCursor crs;
	try
	{
		// ensure we don't get the "switch to...retry" dialog
		COleMessageFilter* pMsgFilter = AfxOleGetMessageFilter();
		if( pMsgFilter )
		{
			pMsgFilter->EnableBusyDialog( FALSE );
			pMsgFilter->EnableNotRespondingDialog( FALSE );
		}

		// create object to see if it exists
		MLApp::DIMLAppPtr ml = NULL;
		HRESULT hr = ml.CreateInstance( _T("Matlab.Application") );
		if( SUCCEEDED( hr ) && ( ml.GetInterfacePtr() != NULL ) )
		{
			// try a function call to see if it works
			CString szCmd = _T("x=1;");
			ml->Execute( szCmd.AllocSysString() );
			// release object
			ml.Release();

			return TRUE;
		}
	}
	catch( CMemoryException* e )
	{
		e->Delete();
		BCGPMessageBox( _T("You do not have enough system memory to load Matlab."), MB_OK | MB_ICONERROR );
		return FALSE;
	}
	catch( CFileException* e )
	{
		e->Delete();
		BCGPMessageBox( _T("There was an error accessing a file while trying to open Matlab."), MB_OK | MB_ICONERROR );
		return FALSE;
	}
	catch( CException* e )
	{
		char pszError[ 200 ];
		e->GetErrorMessage( pszError, 200 );
		e->Delete();
		BCGPMessageBox( pszError, MB_OK | MB_ICONERROR );
		return FALSE;
	}
	catch( ... )
	{
		BCGPMessageBox( _T("An unexpected error occured while executing the Matlab script."), MB_OK | MB_ICONERROR );
		return FALSE;
	}
	
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

void ExpandMacros( CString szSrc, CString& szDest, CString szExp, CString szGrp,
				   CString szSubj, CString szCond, CString szTrial, CString szExt )
{
	// construct possible macro expansions
	CString szHWR, szTF, szSeg, szExtract, szBase, szRoot;
	::GetDataPathRoot( szRoot );
	szBase.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s"), szRoot, szExp, szGrp, szSubj,
		szExp, szGrp, szSubj, szCond, szTrial );
	szHWR.Format( _T("%s.%s"), szBase, szExt );			// %HWR%
	szTF.Format( _T("%s.TF"), szBase );					// %TF%
	szSeg.Format( _T("%s.SEG"), szBase );				// %SEG%
	GetEXT( szExp, szGrp, szSubj, szCond, szExtract );	// %EXT%

	// searchable macros
	CString szHWRSearch = _T("%HWR%"), szTFSearch = _T("%TF%"), szSegSearch = _T("%SEG%"),
			szExtSearch = _T("%EXT%"), szTemp = szSrc, szWorking = szSrc;
	szTemp.MakeUpper();
	// %HWR%
	if( szTemp.Find( szHWRSearch ) != -1 ) szWorking.Replace( szHWRSearch, szHWR );
	// %TF%
	if( szTemp.Find( szTFSearch ) != -1 ) szWorking.Replace( szTFSearch, szTF );
	// %SEG%
	if( szTemp.Find( szSegSearch ) != -1 ) szWorking.Replace( szSegSearch, szSeg );
	// %EXT%
	if( szTemp.Find( szExtSearch ) != -1 )szWorking.Replace( szExtSearch, szExtract );

	szDest = szWorking;
}

BOOL Experiments::ExecuteMatlabScript( CString szScript, CString szExp, CString szGrp,
									   CString szSubj, CString szCond, CString szTrial,
									   CString szExt )
{
	// verify: file name not empty, file exists, ends in .M
	if( szScript == _T("") )
	{
		OutputAMessage( _T("WARNING: Matlab script file name is empty. Cannot process."), TRUE );
		return FALSE;
	}
	int nPos = szScript.ReverseFind( '.' );
	if( nPos == -1 )
	{
		OutputAMessage( _T("WARNING: Matlab script file name is invalid. Cannot process."), TRUE );
		return FALSE;
	}
	CString szFExt = szScript.Mid( nPos + 1 );
	szFExt.MakeUpper();
	if( szFExt != _T("M") )
	{
		OutputAMessage( _T("WARNING: Matlab script file name does not have the correct extension. Cannot process."), TRUE );
		return FALSE;
	}
	CFileFind ff;
	if( !ff.FindFile( szScript ) )
	{
		OutputAMessage( _T("WARNING: Matlab script file does not exist. Cannot process."), TRUE );
		return FALSE;
	}

	// create matlab app object (if not already created)
	CWaitCursor crs;
	try
	{
		// We need to create the matlab com object if it has not yet been created,
		// or created but was closed manually. To determine that, we will issue a command
		// to the matlab object if not null, and if it fails, attempt to recreate.
		BOOL fCreate = ( _ml == NULL );
		if( _ml )
		{
			try { CString szCmd = _T(";"); _ml->Execute( szCmd.AllocSysString() ); }
			catch( ... ) { _ml.Release(); _ml = NULL; fCreate = TRUE; }
		}

		if( fCreate )
		{
			// ensure we don't get the "switch to...retry" dialog
			COleMessageFilter* pMsgFilter = AfxOleGetMessageFilter();
			if( pMsgFilter )
			{
				pMsgFilter->EnableBusyDialog( FALSE );
				pMsgFilter->EnableNotRespondingDialog( FALSE );
			}

			// create object
			HRESULT hr = _ml.CreateInstance( _T("Matlab.Application") );
			if( FAILED( hr ) || ( _ml.GetInterfacePtr() == NULL ) )
			{
				BCGPMessageBox( _T("Unable to create Matlab Application object.\nYou must have Matlab installed before using this feature."), MB_OK | MB_ICONERROR );
				return FALSE;
			}
		}

		// minimize the command window
//		ml->MinimizeCommandWindow();

		// open file for reading
		CStdioFile f;
		if( !f.Open( szScript, CFile::modeRead ) )
		{
			OutputAMessage( _T("WARNING: Unable to open Matlab script file. Cannot process."), TRUE );
			return FALSE;
		}

		// loop through each line of the file and send to matlab
		CString szCmd, szRes;
		BSTR bstrRes;
		while( f.ReadString( szCmd ) )
		{
			// expand macros, if any
			ExpandMacros( szCmd, szCmd, szExp, szGrp, szSubj, szCond, szTrial, szExt );
			szRes.Format( _T("MATLAB COMMAND: %s"), szCmd );
			OutputAMessage( szRes );
			// execute
			bstrRes = _ml->Execute( szCmd.AllocSysString() );
			szRes = bstrRes;
			// return string will be empty if successful, otherwise an error
			if( szRes != _T("") )
			{
				szCmd = _T("ERROR IN MATLAB SCRIPT: ");
				szCmd += szRes;
				OutputAMessage( szCmd, TRUE );
				return FALSE;
			}
		}
	}
	catch( CMemoryException* e )
	{
		e->Delete();
		BCGPMessageBox( _T("You do not have enough system memory to execute the Matlab script."), MB_OK | MB_ICONERROR );
		return FALSE;
	}
	catch( CFileException* e )
	{
		e->Delete();
		BCGPMessageBox( _T("There was an error accessing a file while executing the Matlab script."), MB_OK | MB_ICONERROR );
		return FALSE;
	}
	catch( CException* e )
	{
		char pszError[ 200 ];
		e->GetErrorMessage( pszError, 200 );
		e->Delete();
		BCGPMessageBox( pszError, MB_OK | MB_ICONERROR );
		return FALSE;
	}
	catch( ... )
	{
		BCGPMessageBox( _T("An unexpected error occured while executing the Matlab script."), MB_OK | MB_ICONERROR );
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Experiments::ExecuteBatchScript( CString szScript, CString szExp, CString szGrp,
									  CString szSubj, CString szCond, CString szTrial,
									  CString szExt, BOOL fNoWait )
{
	// verify: file name not empty, file exists, ends in .BAT
	if( szScript == _T("") )
	{
		OutputAMessage( _T("WARNING: Batch file name is empty. Cannot process."), TRUE );
		return FALSE;
	}
	int nPos = szScript.ReverseFind( '.' );
	if( nPos == -1 )
	{
		OutputAMessage( _T("WARNING: Batch file name is invalid. Cannot process."), TRUE );
		return FALSE;
	}
	CString szFExt = szScript.Mid( nPos + 1 );
	szFExt.MakeUpper();
	if( szFExt != _T("BAT") )
	{
		OutputAMessage( _T("WARNING: Batch file name does not have the correct extension. Cannot process."), TRUE );
		return FALSE;
	}
	CFileFind ff;
	if( !ff.FindFile( szScript ) )
	{
		OutputAMessage( _T("WARNING: Batch file does not exist. Cannot process."), TRUE );
		return FALSE;
	}

	// get subject's sampling rate & device resolution
	double dDevRes = 0.;
	short nSampRate = 0;
	IExperimentMember* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEM );
	if( SUCCEEDED( hr ) )
	{
		if( SUCCEEDED( pEM->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() ) ) )
		{
			pEM->get_SamplingRate( &nSampRate );
			pEM->get_DeviceRes( &dDevRes );
		}
		pEM->Release();
	}

	// Construct parameters (raw-file exp grp subj cond trial)
	CString szParams, szFile, szRoot;
	::GetDataPathRoot( szRoot );
// 	szFile.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.%s"), szRoot, szExp, szGrp, szSubj,
// 				   szExp, szGrp, szSubj, szCond, szTrial, szExt );
// 22 Oct 09: GMB: Changed to just path as per Somesh's request
	szFile.Format( _T("%s\\%s\\%s\\%s"), szRoot, szExp, szGrp, szSubj );
// 22 Oct 09: GMB: Added sec/cm (sampl. rate / dev. res.)
	szParams.Format( _T("\"%s\" %s %s %s %s %s %d %.4f"),
					 szFile, szExp, szGrp, szSubj, szCond, szTrial, nSampRate, dDevRes );

	SHELLEXECUTEINFO si;
	memset( &si, 0, sizeof( SHELLEXECUTEINFO ) );
	si.cbSize = sizeof( SHELLEXECUTEINFO );
//	si.hwnd = NULL;
	si.lpVerb = _T("open");
	si.lpFile = szScript;
	si.lpParameters = szParams;
	si.nShow = SW_NORMAL;
	si.fMask = fNoWait ? SEE_MASK_FLAG_NO_UI : SEE_MASK_NOCLOSEPROCESS;
	CWaitCursor crs;
	BOOL fOK = ::ShellExecuteEx( &si );
	if( fOK ) {
		while( !fNoWait && fOK ) {
			DWORD dwRes = ::WaitForSingleObject( si.hProcess, 10000 );
			if( dwRes == WAIT_TIMEOUT ) {
				int nRes = BCGPMessageBox( "The external process is still running.\n\nContinue waiting?", MB_YESNOCANCEL | MB_ICONQUESTION );
				if( nRes == IDNO ) fOK = FALSE;
				if( nRes == IDCANCEL ) fOK = FALSE;
				if( !fOK ) ::CloseHandle( si.hProcess );
			}
			else if( dwRes == WAIT_OBJECT_0 ) break;
		}
	}
	else if( fNoWait ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

#define VALIDATE_ERR()	\
	if( fNotify ) BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR ); \
	pListErrors->AddTail( szMsg ); \
	if( fFirstOnly ) { pPSRE->Release(); pPSF->Release(); pPS->Release(); return FALSE; }

#define VALIDATE_ERR_MSG(msg)	\
	szMsg = msg; \
	if( fNotify ) BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR ); \
	pListErrors->AddTail( szMsg ); \
	if( fFirstOnly ) { pPSRE->Release(); pPSF->Release(); pPS->Release(); return FALSE; }


BOOL Experiments::ValidateData( CStringList* pListErrors, BOOL fFirstOnly, BOOL fNotify )
{
	ASSERT( pListErrors != NULL );
	if( !pListErrors ) return FALSE;

	CString szItem, szMsg;
	BSTR bstr = NULL;
	BOOL fVal = FALSE, fRecord = FALSE, fProcess = FALSE;
	short nVal = 0, nVal1 = 0, nVal2 = 0;
	double dVal = 0.;
	HRESULT hr = E_FAIL;

	// get appropriate proc settings interfaces
	IProcSetting* pPS = NULL;
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) ) { BCGPMessageBox( IDS_PS_ERR ); return FALSE; }
	IProcSetFlags* pPSF = NULL;
	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) ) { pPS->Release(); BCGPMessageBox( IDS_PS_ERR ); return FALSE; }
	IProcSetRunExp* pPSRE = NULL;
	hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( FAILED( hr ) ) { pPSF->Release(); pPS->Release(); BCGPMessageBox( IDS_PS_ERR ); return FALSE; }

	// VALIDATION
	// Main page
	Experiments::GetID( m_pExperiment, szItem );
	szMsg.Format( _T("DATA VALIDATION: Experiment %s"), szItem );
	OutputMessage( szMsg, LOG_DB );
	if( szItem == _T("") )
	{
		VALIDATE_ERR_MSG( _T("The experiment ID must have a value.") );
	}		
	if( szItem.GetLength() < szIDS )
	{
		szMsg.Format( IDS_ID_LEN, szIDS );
		VALIDATE_ERR();
	}
	if( szItem == _T("CON") )
	{
		VALIDATE_ERR_MSG( _T("ID cannot be \'CON\'.") );
	}
	if( szItem == _T("NUL") )
	{
		VALIDATE_ERR_MSG( _T("ID cannot be \'NUL\'.") );
	}
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( szItem ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		VALIDATE_ERR();
	}
	Experiments::GetDescription( m_pExperiment, szItem );
	if( szItem == _T("") )
	{
		VALIDATE_ERR_MSG( _T("The condition description must have a value.") );
	}
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( szItem ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		VALIDATE_ERR();
	}

	// input device page
	pPS->get_MaxPenPressure( &nVal );
	if( nVal > ::GetMaxPenPressure() )
	{
		szMsg.Format( _T("The max pen pressure for notifications (%d) is more than the max device pressure (%d).\nIf you keep this value, you will receive no notifications for pen pressure.\n\nDo you wish to keep this max pen pressure setting?"), nVal, ::GetMaxPenPressure() );
		if( ( fNotify && BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) || fFirstOnly )
		{
			pListErrors->AddTail( szMsg );
			pPSRE->Release();
			pPSF->Release();
			pPS->Release();
			return FALSE;
		}
	}

	// trial page
	pPSRE->get_TimeoutStart( &dVal );
	if( ( dVal <= 0. ) || ( dVal > 600. ) )
	{
		VALIDATE_ERR_MSG( _T("Start timeout must be between 0 and 600 seconds.") );
	}
	pPSRE->get_TimeoutRecord( &dVal );
	if( ( dVal <= 0. ) || ( dVal > 1800 ) )
	{
		VALIDATE_ERR_MSG( _T("Recording timeout must be between 0 and 1800 seconds.") );
	}
    //2Nov2012:hlt: Changed from 600 s to 1800 s. old version was 5.2.0.0 Now 5.2.0.1
	pPSRE->get_TimeoutPenlift( &dVal );
	if( ( dVal < 0.01 ) || ( dVal > 1800. ) )
	{
		VALIDATE_ERR_MSG( _T("Penlift timeout must be between 0.01 and 1800 seconds.") );
	}
	pPSRE->get_TimeoutLatency( &dVal );
	if( ( dVal < 0. ) || ( dVal > 600. ) )
	{
		VALIDATE_ERR_MSG( _T("Trial-to-Trial timeout must be between 0 and 600 seconds.") );
	}

	// drawing options page
	short ls = 0, es = 0;
	DWORD bg = 0, l1, l2, l3, e, mpp;
	pPSRE->GetDrawingOptions( &ls, &es, &bg, &l1, &l2, &l3, &e, &mpp );
	if( ( es < 0 ) || ( es > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Ellipses size must be between 0 and 100.") );
	}
	pPSRE->get_MaxLineThickness( &nVal );
	if( ( nVal < 1 ) || ( nVal > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Maximum size of the sample point thickness must be between 1 and 100.") );
	}

	// TF page
	pPS->get_UpSampleFactor( &nVal );
	if( ( nVal != 1 ) && ( nVal != 2 ) && ( nVal != 4 ) && ( nVal != 8 ) )
	{
		VALIDATE_ERR_MSG( _T("Up-Sample Factor can only be 1, 2, 4 or 8.") );
	}

	// External apps page
	ExternalAppType eat = (ExternalAppType)0;
	pPSRE->get_ExtAppTypeRaw( &eat );
	pPSRE->get_ExtAppScriptRaw( &bstr );
	szItem = bstr;
	if( ( eat != eEAT_None ) && ( szItem == _T("") ) )
	{
		VALIDATE_ERR_MSG( _T("You must specify an external app script for the RAW file.") );
	}
	pPSRE->get_ExtAppTypeTF( &eat );
	pPSRE->get_ExtAppScriptTF( &bstr );
	szItem = bstr;
	if( ( eat != eEAT_None ) && ( szItem == _T("") ) )
	{
		VALIDATE_ERR_MSG( _T("You must specify an external app script for the TF file.") );
	}
	pPSRE->get_ExtAppTypeSeg( &eat );
	pPSRE->get_ExtAppScriptSeg( &bstr );
	szItem = bstr;
	if( ( eat != eEAT_None ) && ( szItem == _T("") ) )
	{
		VALIDATE_ERR_MSG( _T("You must specify an external app script for the SEG file.") );
	}
	pPSRE->get_ExtAppTypeExt( &eat );
	pPSRE->get_ExtAppScriptExt( &bstr );
	szItem = bstr;
	if( ( eat != eEAT_None ) && ( szItem == _T("") ) )
	{
		VALIDATE_ERR_MSG( _T("You must specify an external app script for the EXT file.") );
	}

	// Summarization page
	pPSF->get_DiscardAfter2( &nVal );
	if( ( nVal < 1 ) || ( nVal > 99 ) )
	{
		VALIDATE_ERR_MSG( _T("Trial # after which trials are not included must be between 1 and 99.") );
	}
	pPSF->get_PenDownDurMin( &dVal );
	if( ( dVal < 0. ) || ( dVal > 1. ) )
	{
		VALIDATE_ERR_MSG( _T("Threshold Relative Pen Down Duration must be greater than 0 and less than or equal to 1.") );
	}

	// TF2 (discontinuity) page
	pPS->get_DisZInsertPoint( &dVal );
	if( ( dVal < -1000. ) || ( dVal > -1. ) )
	{
		VALIDATE_ERR_MSG( _T("Pressure value to indicate raw data manipulation must be between -1000 and -1.") );
	}
	pPS->get_DisError( &dVal );
	if( dVal <= 0. )
	{
		VALIDATE_ERR_MSG( _T("Max. relative intersample period error is out of range.\nIt must be greater than 0.") );
	}
	pPS->get_DisAbsError( &dVal );
	if( dVal <= 1. )
	{
		VALIDATE_ERR_MSG( _T("Max. absolute intersample distance is out of range.\nIt must be greater than 1.") );
	}
	pPS->get_DisRelError( &dVal );
	if( dVal <= 1. )
	{
		VALIDATE_ERR_MSG( _T("Max. relative intersample distance is out of range.\nIt must be greater than 1.") );
	}
	pPS->get_DisFactor( &dVal );
	if( ( dVal <= 0. ) || ( dVal > 0.999 ) )
	{
		VALIDATE_ERR_MSG( _T("Running avg. distance integration speed is out of range.\nAccepted values are between 0.001 and 0.999.") );
	}

	// Image analysis page
	pPS->get_SmoothingIter( &nVal );
	if( ( nVal < 0 ) || ( nVal > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Smoothing iterations must be between 0 and 100.") );
	}
	pPS->get_InkThreshold( &nVal );
	if( ( nVal < 0 ) || ( nVal > 255 ) )
	{
		VALIDATE_ERR_MSG( _T("Graylevel separating ink and paper must be between 0 and 255.") );
	}
	pPS->get_ImgResolution( &nVal );
	if( ( nVal != 0 ) && ( ( nVal < 75 ) || ( nVal > 1200 ) ) )
	{
		VALIDATE_ERR_MSG( _T("Image resolution must be 0 (automatic) or between 75 and 1200 dpi.") );
	}

	// delete objects
	pPSRE->Release();
	pPSF->Release();
	pPS->Release();

	return( pListErrors->GetCount() == 0 );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#pragma once

// PrefPageGripper.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// PrefPageGripper dialog

struct GripperSettings;

class AFX_EXT_CLASS PrefPageGripper : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(PrefPageGripper)

// Construction
public:
	PrefPageGripper( GripperSettings* pGS = NULL );
	~PrefPageGripper();

// Dialog Data
	enum { IDD = IDD_PAGE_PREF_SETTINGS };
	CComboBox	m_cboUpper;
	CComboBox	m_cboLower;
	CComboBox	m_cboLoad;
	int			m_nDevice;
	int			m_nChannels;
	long		m_lSamples;
	long		m_lSampleRate;
	long		m_lScanRate;
	double		m_dCalibrationConstantL;
	double		m_dCalibrationConstantLG;
	double		m_dCalibrationConstantUG;
	double		m_dExcitationVoltageL;
	double		m_dExcitationVoltageLG;
	double		m_dExcitationVoltageUG;
	double		m_dFullScaleLoadL;
	double		m_dFullScaleLoadLG;
	double		m_dFullScaleLoadUG;
	double		m_dGainL;
	double		m_dGainLG;
	double		m_dGainUG;
	int			m_nBaseline;
	int			m_nChanLoad;
	int			m_nChanLower;
	int			m_nChanUpper;
	long		m_lDBSamples;
	BOOL		m_fVolts;
	BOOL		m_fNewtons;
	BOOL		m_fDBMode;
	GripperSettings*	m_pGS;
	NSToolTipCtrl		m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedRdoSbmode();
	afx_msg void OnBnClickedRdoDbmode();
	afx_msg void OnCbnSelchangeCboLoad();
	afx_msg void OnCbnSelchangeCboLower();
	afx_msg void OnCbnSelchangeCboUpper();
	afx_msg void OnEnKillfocusEditChannels();
	DECLARE_MESSAGE_MAP()
};

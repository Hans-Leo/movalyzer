// WizardImportRemoteSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardImportRemoteSheet.h"
#include "WizardImportLocation.h"
#include "WizardImportFiles.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportRemoteSheet

IMPLEMENT_DYNAMIC(WizardImportRemoteSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardImportRemoteSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportRemoteSheet::WizardImportRemoteSheet( CWnd* pParentWnd, UINT iSelectPage )
	: CBCGPPropertySheet( _T("Subject Export Wizard"), pParentWnd, iSelectPage )
{
	m_pInetSession = NULL;
	m_pFtpConnection = NULL;
	AddPages();
}

WizardImportRemoteSheet::~WizardImportRemoteSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
	CloseInet();
}

void WizardImportRemoteSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, FALSE );

	AddPage( new WizardImportLocation );
	AddPage( new WizardImportFiles );

	SetWizardMode();
}

void WizardImportRemoteSheet::CloseInet()
{
	if( m_pFtpConnection )
	{
		m_pFtpConnection->Close();
		delete m_pFtpConnection;
		m_pFtpConnection = NULL;
	}
	if( m_pInetSession )
	{
		m_pInetSession->Close();
		delete m_pInetSession;
		m_pInetSession = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportRemoteSheet message handlers

BOOL WizardImportRemoteSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportLocation ) ) )
			return ((WizardImportLocation*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportFiles ) ) )
			return ((WizardImportFiles*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

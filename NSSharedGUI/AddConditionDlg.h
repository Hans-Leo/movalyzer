#if !defined(AFX_ADDCONDITIONDLG_H__10ADC4A4_A81D_11D3_8A59_000000000000__INCLUDED_)
#define AFX_ADDCONDITIONDLG_H__10ADC4A4_A81D_11D3_8A59_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddConditionDlg.h : header file
//

#include "AddDlg.h"

/////////////////////////////////////////////////////////////////////////////
// AddConditionDlg dialog

class AFX_EXT_CLASS AddConditionDlg : public AddDlg
{
// Construction
public:
	AddConditionDlg(CStringList* pExisting, CWnd* pParent = NULL, BOOL fSingleSel = FALSE,
					BOOL fNoOptions = FALSE);
	~AddConditionDlg();

// Dialog Data
protected:
	BOOL	m_fNoOptions;

// Overrides

// Implementation
protected:
	virtual int GetImageNum() { return 11; }
	virtual void InsertColumns();
	virtual void FillList();
	virtual int Add();
	virtual BOOL Edit( int nItem );
	virtual BOOL Delete( int nItem );
	virtual void Relationships( int nItem );
	virtual void OK();
	virtual void Cleanup();
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_ADDCONDITIONDLG_H__10ADC4A4_A81D_11D3_8A59_000000000000__INCLUDED_)

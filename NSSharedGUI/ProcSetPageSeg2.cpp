// ProcSetPageSeg2.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageSeg2.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSeg2 property page

IMPLEMENT_DYNCREATE(ProcSetPageSeg2, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageSeg2, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageSeg2::ProcSetPageSeg2( IProcSetting* pPS )
	: CBCGPPropertyPage(ProcSetPageSeg2::IDD), m_pPS( pPS )
{
	absdistmin = 0.05;
	rdistmin = 0.1;
	timemin = 0.04;
	rvymin = 0.05;
	rvymin2 = 0.1;

	m_editRvymin2.SetDefaultValue( rvymin2 );
	m_editRvymin.SetDefaultValue( rvymin );
	m_editTimemin.SetDefaultValue( timemin );
	m_editAbsdistmin.SetDefaultValue( absdistmin );
	m_editRdistmin.SetDefaultValue( rdistmin );

	if( m_pPS )
	{
		m_pPS->get_MaxVertVelocity( &rvymin2 );
		m_pPS->get_MinVertVelocity( &rvymin );
		m_pPS->get_MinStrokeDuration( &timemin );
		m_pPS->get_MinStrokeSize( &absdistmin );
		m_pPS->get_MinStrokeSizeRelativeToMax( &rdistmin );
	}
}

ProcSetPageSeg2::~ProcSetPageSeg2()
{
}

void ProcSetPageSeg2::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MVV2, rvymin2);
	DDX_Text(pDX, IDC_EDIT_MVV, rvymin);
	DDX_Text(pDX, IDC_EDIT_MSD, timemin);
	DDX_Text(pDX, IDC_EDIT_MSS, absdistmin);
	DDX_Text(pDX, IDC_EDIT_MSSRMS, rdistmin);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);

	DDX_Control(pDX, IDC_EDIT_MVV2, m_editRvymin2);
	DDX_Control(pDX, IDC_EDIT_MVV, m_editRvymin);
	DDX_Control(pDX, IDC_EDIT_MSD, m_editTimemin);
	DDX_Control(pDX, IDC_EDIT_MSS, m_editAbsdistmin);
	DDX_Control(pDX, IDC_EDIT_MSSRMS, m_editRdistmin);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSeg2 message handlers

BOOL ProcSetPageSeg2::OnApply() 
{
	if( !m_pPS ) return FALSE;

	m_pPS->put_MaxVertVelocity( rvymin2 );
	m_pPS->put_MinVertVelocity( rvymin );
	m_pPS->put_MinStrokeDuration( timemin );
	m_pPS->put_MinStrokeSize( absdistmin );
	m_pPS->put_MinStrokeSizeRelativeToMax( rdistmin );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageSeg2::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageSeg2::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_bnCalc.SetImage( IDB_CALC, IDB_CALC, IDB_CALC );
	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	double drvymin2 = 0., drvymin = 0., dtimemin = 0., dabsdistmin = 0., drdistmin = 0.;
	if( pPS )
	{
		pPS->get_MaxVertVelocity( &drvymin2 );
		pPS->get_MinVertVelocity( &drvymin );
		pPS->get_MinStrokeDuration( &dtimemin );
		pPS->get_MinStrokeSize( &dabsdistmin );
		pPS->get_MinStrokeSizeRelativeToMax( &drdistmin );

		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_MSS );
	szData = _T("Min. stroke size (cm)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dabsdistmin );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Stroke Size") );

	pWnd = GetDlgItem( IDC_EDIT_MSSRMS );
	szData = _T("Min. stroke size relative to max stroke");
	szTmp.Format( _T(" [DEFAULT=%g]"), drdistmin );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Stroke Size") );

	pWnd = GetDlgItem( IDC_EDIT_MSD );
	szData = _T("Min. stroke duration (s)");
	szTmp.Format( _T(" [DEFAULT=%g]"), dtimemin );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Stroke Duration") );

	pWnd = GetDlgItem( IDC_EDIT_MVV );
	szData = _T("Min. vertical velocity of coarse beg/end of first/last stroke");
	szTmp.Format( _T(" [DEFAULT=%g]"), drvymin );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Min Velocity") );

	pWnd = GetDlgItem( IDC_EDIT_MVV2 );
	szData = _T("Max. vertical velocity of coarse beg/end of first/last stroke");
	szTmp.Format( _T(" [DEFAULT=%g]"), drvymin2 );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Max Velocity") );

	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageSeg2::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageSeg2::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_segmentation.html"), this );
	return TRUE;
}

void ProcSetPageSeg2::OnBnReset() 
{
	absdistmin = 0.05;
	rdistmin = 0.1;
	timemin = 0.04;
	rvymin = 0.05;
	rvymin2 = 0.1;

	UpdateData( FALSE );
}

void ProcSetPageSeg2::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void ProcSetPageSeg2::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 9 );
}

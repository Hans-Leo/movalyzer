// StimulusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "StimulusDlg.h"
#include "..\DataMod\DataMod.h"
#include "..\CTreeLink\DBCommon.h"
#include "Elements.h"
#include "Stimuli.h"
#include "ElementSheet.h"
#include "RelationshipDlg.h"
#include "AddElementDlg.h"
#include "direct.h"
#include "GraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define		MAX_TARGETS		50
#define		MAX_ELEMENTS	50

/////////////////////////////////////////////////////////////////////////////
// StimulusDlg dialog

BEGIN_MESSAGE_MAP(StimulusDlg, CBCGPDialog)
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnKillfocusEditId)
	ON_BN_CLICKED(IDC_BN_GRAPH, OnBnGraph)
	ON_BN_CLICKED(IDC_BN_VIEW, OnBnView)
	ON_LBN_DBLCLK(IDC_LIST_ELEMENTS, OnDblclkListElements)
	ON_LBN_DBLCLK(IDC_LIST_TARGETS, OnDblclkListTargets)
	ON_LBN_DBLCLK(IDC_LIST_TARGETSAVAIL, OnDblclkListTargetsavail)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_EDIT, OnClickBnEdit)
	ON_BN_CLICKED(IDC_BN_ADD, OnClickBnAdd)
	ON_BN_CLICKED(IDC_BN_DEL, OnClickBnDel)
	ON_BN_CLICKED(IDC_BN_LEFT, OnClickBnLeft)
	ON_BN_CLICKED(IDC_BN_RIGHT, OnClickBnRight)
	ON_BN_CLICKED(IDC_BN_LEFTALL, OnClickBnLeftAll)
	ON_BN_CLICKED(IDC_BN_RIGHTALL, OnClickBnRightAll)
	ON_BN_CLICKED(IDC_BN_UP, OnClickBnUp)
	ON_BN_CLICKED(IDC_BN_DOWN, OnClickBnDown)
	ON_LBN_SELCHANGE(IDC_LIST_ELEMENTS, &StimulusDlg::OnLbnSelchangeListElements)
	ON_LBN_SELCHANGE(IDC_LIST_TARGETSAVAIL, &StimulusDlg::OnLbnSelchangeListTargetsavail)
	ON_LBN_SELCHANGE(IDC_LIST_TARGETS, &StimulusDlg::OnLbnSelchangeListTargets)
END_MESSAGE_MAP()

StimulusDlg::StimulusDlg( CWnd* pParent, BOOL fNew, BOOL fDup )
	: CBCGPDialog(StimulusDlg::IDD, pParent), m_fNew( fNew ), m_fDup( fDup )
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_fGenerated = FALSE;
	m_fInit = FALSE;

	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	Stimuluss stim;
	stim.SetDataPath( szPath );
	HRESULT hr = stim.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		stim.GetID( szItem );
		m_lstItems.AddTail( szItem );
		hr = stim.GetNext();
	}
}

void StimulusDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_TARGETS, m_lstTargets);
	DDX_Control(pDX, IDC_LIST_TARGETSAVAIL, m_lstTargetsAvail);
	DDX_Control(pDX, IDC_LIST_ELEMENTS, m_lstElements);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_Control(pDX, IDC_BN_ADD, m_bnAdd);
	DDX_Control(pDX, IDC_BN_DEL, m_bnDel);
	DDX_Control(pDX, IDC_BN_EDIT, m_bnEdit);
	DDX_Control(pDX, IDC_BN_RIGHT, m_bnRight);
	DDX_Control(pDX, IDC_BN_RIGHTALL, m_bnRightAll);
	DDX_Control(pDX, IDC_BN_LEFT, m_bnLeft);
	DDX_Control(pDX, IDC_BN_LEFTALL, m_bnLeftAll);
	DDX_Control(pDX, IDC_BN_UP, m_bnUp);
	DDX_Control(pDX, IDC_BN_DOWN, m_bnDown);
}

/////////////////////////////////////////////////////////////////////////////
// StimulusDlg message handlers

BOOL StimulusDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// button images
	m_bnAdd.SetImage( IDB_ADD, IDB_ADD, IDB_ADD_DIS );
	m_bnDel.SetImage( IDB_DEL, IDB_DEL, IDB_DEL_DIS );
	m_bnEdit.SetImage( IDB_EDIT, IDB_EDIT, IDB_EDIT );
	m_bnRight.SetImage( IDB_RIGHT, IDB_RIGHT, IDB_RIGHT );
	m_bnRightAll.SetImage( IDB_RIGHTALL, IDB_RIGHTALL, IDB_RIGHTALL );
	m_bnLeft.SetImage( IDB_LEFT, IDB_LEFT, IDB_LEFT );
	m_bnLeftAll.SetImage( IDB_LEFTALL, IDB_LEFTALL, IDB_LEFTALL );
	m_bnUp.SetImage( IDB_UP, IDB_UP, IDB_UP );
	m_bnDown.SetImage( IDB_DOWN, IDB_DOWN, IDB_DOWN );


	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_STIM );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("ID/abbreviation of stimulus."), _T("ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Description of stimulus."), _T("Description") );
	pWnd = GetDlgItem( IDC_LIST_ELEMENTS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The list of elements this stimulus contains."), _T("Used Elements") );
	pWnd = GetDlgItem( IDC_LIST_TARGETSAVAIL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The list of elements available as targets."), _T("Available Elements") );
	pWnd = GetDlgItem( IDC_LIST_TARGETS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The list of elements used as targets with their sequence."), _T("Targets") );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/modify the properties of this element."), _T("Properties") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add elements to this stimulus."), _T("Add") );
	pWnd = GetDlgItem( IDC_BN_DEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Remove the selected element from this stimulus."), _T("Remove") );
	pWnd = GetDlgItem( IDC_BN_UP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Move this target up in sequence (higher priority)."), _T("Move Up") );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Move this target down in sequence (lower priority)."), _T("Move Down") );
	pWnd = GetDlgItem( IDC_BN_LEFT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Remove the selected target from the sequence."), _T("Remove") );
	pWnd = GetDlgItem( IDC_BN_RIGHT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add the selected target to the end of the sequence."), _T("Add") );
	pWnd = GetDlgItem( IDC_BN_LEFTALL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Remove all targets from the sequence."), _T("Remove All") );
	pWnd = GetDlgItem( IDC_BN_RIGHTALL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add all targetws to the end of the sequence."), _T("Add All") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_VIEW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the generated raw data of this stimulus."), _T("View Raw Data") );
	pWnd = GetDlgItem( IDC_BN_GRAPH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Chart results of this stimulus."), _T("Chart") );

	if( !m_fNew && !m_fDup )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// Fill the 3 lists: elements, avail targets, targets
	FillLists();
	EnableDisable();

	// Buttons
	if( m_lstElem.GetCount() > 0 )
	{
		pWnd = GetDlgItem( IDC_BN_VIEW );
		pWnd->EnableWindow( TRUE );
		pWnd = GetDlgItem( IDC_BN_GRAPH );
		pWnd->EnableWindow( TRUE );
	}

	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_STIM ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	m_fInit = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL StimulusDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void StimulusDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The stimulus ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("The stimulus description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CBCGPDialog::OnOK();
}

void StimulusDlg::OnKillfocusEditId() 
{
	if( !m_fInit ) return;
	CString szOld = m_szID;
	UpdateData( TRUE );

	if( m_szID != szOld ) m_fGenerated = FALSE;
	if( m_szID == _T("") ) return;

	if( m_lstItems.Find( m_szID ) )
	{
		int nResp = BCGPMessageBox( _T("This ID already exists. Would you like to see how it is currently being used?"), MB_YESNO );
		if( nResp == IDYES )
		{
			m_fInit = FALSE;
			Stimuluss stim;
			if( stim.IsValid() )
			{
				HRESULT hr = stim.Find( m_szID );
				if( SUCCEEDED( hr ) )
				{
					CString szItem;
					stim.GetDescriptionWithID( szItem );

					RelationshipDlg d( this, REL_STIMULUS, szItem );
					d.DoModal();
				}
				else BCGPMessageBox( _T("Could not find stimulus.") );
			}
			else BCGPMessageBox( _T("Unable to create stimulus object.") );
			m_fInit = TRUE;
		}

		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	m_fGenerated = FALSE;
	BOOL fEnable = ( ( m_szID != _T("") ) && ( m_lstElem.GetCount() > 0 ) );
	CWnd* pWnd = GetDlgItem( IDC_BN_VIEW );
	pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_BN_GRAPH );
	pWnd->EnableWindow( fEnable );
}

void StimulusDlg::OnClickBnEdit() 
{
	int nSel = m_lstElements.GetCurSel();
	if( nSel < 0 ) return;

	CString szItem, szDelim, szData;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_lstElements.GetText( nSel, szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szElem = szItem.Left( nPos - 1 );

	Elements elem;
	if( !elem.IsValid() ) return;

	szData.Format( _T("View element %s properties."), szElem );
	LogToFile( szData, LOG_UI );

	if( elem.DoEdit( szElem, this ) )
	{
		::DataHasChanged();
		m_fGenerated = FALSE;

		BOOL fTarget = FALSE;
		elem.GetIsTarget( fTarget );
		// if was a target and is not now, must remove from the targets lists
		if( !fTarget )
		{
			if( ( nPos = m_lstTargetsAvail.FindString( -1, szItem ) ) != LB_ERR )
				m_lstTargetsAvail.DeleteString( nPos );
			while( ( nPos = m_lstTargets.FindString( -1, szItem ) ) != LB_ERR )
				m_lstTargets.DeleteString( nPos );
		}
		 else
		// otherwise, if now a target, must add to list if not there
		{
			if( m_lstTargetsAvail.FindString( -1, szItem ) == LB_ERR )
				m_lstTargetsAvail.AddString( szItem );
		}

		m_lstElements.SetCurSel( nSel );

		szData.Format( _T("Modified element %s."), szElem );
		LogToFile( szData, LOG_UI );
	}

	EnableDisable();
}

void StimulusDlg::OnClickBnAdd() 
{
	if( m_lstElements.GetCount() >= MAX_ELEMENTS )
	{
		CString szMsg;
		szMsg.Format( _T("You cannot exceed the maximum # of elements allowed (%d)"), MAX_ELEMENTS );
		BCGPMessageBox( szMsg );
		return;
	}

	// Existing
	CStringList exist;
	CString szItem, szDelim, szID;
	int nPos = 0;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	int nCount = m_lstElements.GetCount();
	for( int i = 0; i < nCount; i++ )
	{
		m_lstElements.GetText( i, szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szItem = szItem.Left( nPos - 1 );
			exist.AddTail( szItem );
		}
	}

	// Add
	AddElementDlg d( &exist, this );
	if( d.DoModal() == IDOK )
	{
		if( d.m_fChanged ) m_fGenerated = FALSE;

		Elements elem;
		if( !elem.IsValid() ) return;
		HRESULT hr;
		BOOL fTarget = FALSE;

		if( ( d.m_lstNew.GetCount() + m_lstElements.GetCount() ) > MAX_ELEMENTS )
		{
			szItem.Format( _T("You are adding too many elements exceeding the total limit of %d."), MAX_ELEMENTS );
			BCGPMessageBox( szItem );
			return;
		}
		POSITION pos = d.m_lstNew.GetHeadPosition();
		while( pos )
		{
			szItem = d.m_lstNew.GetNext( pos );
			m_lstElements.AddString( szItem );
			m_lstElem.AddTail( szItem );

			szID = szItem.Left( szItem.Find( szDelim ) - 1 );
			hr = elem.Find( szID );
			if( SUCCEEDED( hr ) )
			{
				elem.GetIsTarget( fTarget );
				if( fTarget ) m_lstTargetsAvail.AddString( szItem );
			}	
		}

		BOOL fEnable = ( ( m_szID != _T("") ) && ( m_lstElem.GetCount() > 0 ) );
		CWnd* pWnd = GetDlgItem( IDC_BN_VIEW );
		pWnd->EnableWindow( fEnable );
		pWnd = GetDlgItem( IDC_BN_GRAPH );
		pWnd->EnableWindow( fEnable );
	}

	EnableDisable();
}

void StimulusDlg::OnClickBnDel() 
{
	// Which item
	int nSel = m_lstElements.GetCurSel();
	int nElSel = nSel;
	if( nSel < 0 ) return;

	if( BCGPMessageBox( _T("Removing will also impact targets. Are you sure?"), MB_YESNO ) == IDNO )
		return;

	// Item text
	CString szItem;
	m_lstElements.GetText( nSel, szItem );

	// Locate in element lists & remove
	POSITION pos = m_lstElem.Find( szItem );
	if( pos ) m_lstElem.RemoveAt( pos );
	m_lstElements.DeleteString( nSel );

	// Locate in avail target list & remove
	nSel = m_lstTargetsAvail.FindString( -1, szItem );
	if( nSel != LB_ERR ) m_lstTargetsAvail.DeleteString( nSel );

	// Locate all instances in target lists & remove
	pos = m_lstTarg.Find( szItem );
	while( pos )
	{
		m_lstTarg.RemoveAt( pos );
		pos = m_lstTarg.Find( szItem );
	}
	while( ( nSel = m_lstTargets.FindString( -1, szItem ) ) != LB_ERR )
		m_lstTargets.DeleteString( nSel );

	m_fGenerated = FALSE;
	BOOL fEnable = ( ( m_szID != _T("") ) && ( m_lstElem.GetCount() > 0 ) );
	CWnd* pWnd = GetDlgItem( IDC_BN_VIEW );
	pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_BN_GRAPH );
	pWnd->EnableWindow( fEnable );

	m_lstElements.SetFocus();
	m_lstElements.SetCurSel( nElSel );

	EnableDisable();
}

void StimulusDlg::OnClickBnLeft() 
{
	int nSel = m_lstTargets.GetCurSel();
	if( nSel < 0 ) return;

	m_lstTargets.DeleteString( nSel );

	POSITION pos = m_lstTarg.FindIndex( nSel );
	if( pos ) m_lstTarg.RemoveAt( pos );

	EnableDisable();
}

void StimulusDlg::OnClickBnLeftAll()
{
	m_lstTargets.ResetContent();
	m_lstTarg.RemoveAll();

	EnableDisable();
}

void StimulusDlg::OnClickBnRight() 
{
	int nSel = m_lstTargetsAvail.GetCurSel();
	if( nSel < 0 ) return;

	CString szItem;
	m_lstTargetsAvail.GetText( nSel, szItem );
	m_lstTargets.AddString( szItem );
	m_lstTarg.AddTail( szItem );

	m_lstTargets.SetCurSel( m_lstTargets.GetCount() - 1 );

	EnableDisable();
}

void StimulusDlg::OnClickBnRightAll()
{
	CString szItem;
	int nCount = m_lstTargetsAvail.GetCount();
	for( int i = 0; i < nCount; i++ )
	{
		m_lstTargetsAvail.GetText( i, szItem );
		m_lstTargets.AddString( szItem );
		m_lstTarg.AddTail( szItem );
	}

	m_lstTargets.SetCurSel( m_lstTargets.GetCount() - 1 );

	EnableDisable();
}

void StimulusDlg::FillLists()
{
	// remove all first
	m_lstTargets.ResetContent();
	m_lstTargetsAvail.ResetContent();
	m_lstElements.ResetContent();
	m_lstOrigElem.RemoveAll();
	m_lstElem.RemoveAll();
	m_lstOrigTarg.RemoveAll();
	m_lstTarg.RemoveAll();

	// Fill the 3 lists: elements, avail targets, targets
	if( !m_fNew || ( m_szID != _T("") ) )
	{
		Elements elem;
		if( !elem.IsValid() ) return;

		HRESULT hr;
		BSTR bstr;
		CString szItem, szDelim, szStim;
		BOOL fTarget;
		::GetDelimiter( szDelim );
		TRIM( szDelim );

		// Fill element list (& avail targets)
		IStimulusElement* pSEL = NULL;
		hr = CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
							   IID_IStimulusElement, (LPVOID*)&pSEL );
		if( pSEL )
		{
			hr = pSEL->FindForStimulus( m_szID.AllocSysString() );
			while( SUCCEEDED( hr ) )
			{
				pSEL->get_ElementID( &bstr );
				szItem = bstr;
				pSEL->get_StimulusID( &bstr );
				szStim = bstr;

				if( szStim == m_szID )
				{
					hr = elem.Find( szItem );
					if( SUCCEEDED( hr ) )
					{
						elem.GetDescriptionWithID( szItem, TRUE );
						elem.GetIsTarget( fTarget );

						m_lstElements.AddString( szItem );
						m_lstElem.AddTail( szItem );
						m_lstOrigElem.AddTail( szItem );

						if( fTarget ) m_lstTargetsAvail.AddString( szItem );
					}
				}
				hr = pSEL->GetNext();
			}
		}
		else BCGPMessageBox( _T("Unable to create StimulusElement object.") );
		pSEL->Release();

		// Fill target list
		IStimulusTarget* pSTL = NULL;
		hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
								 IID_IStimulusTarget, (LPVOID*)&pSTL );
		if( SUCCEEDED( hr ) )
		{
			// Now create nCnt # of items in list so we can insert
			CString szEmpty;
			for( int i = 0; i < MAX_TARGETS; i++ )
			{
				m_lstOrigTarg.AddTail( szEmpty );
			}

			// now reloop thru & insert based upon sequence #
			POSITION pos;
			short nSeq = 0, nCnt = 0;
			hr = pSTL->FindForStimulus( m_szID.AllocSysString() );
			while( SUCCEEDED( hr ) )
			{
				pSTL->get_ElementID( &bstr );
				szItem = bstr;
				pSTL->get_Sequence( &nSeq );
				nSeq--;

				hr = elem.Find( szItem );
				if( SUCCEEDED( hr ) )
				{
					elem.GetIsTarget( fTarget );
					if( fTarget )
					{
						elem.GetDescriptionWithID( szItem, TRUE );
						if( m_lstTargetsAvail.FindString( -1, szItem ) != LB_ERR )
						{
							pos = m_lstOrigTarg.FindIndex( nSeq );
							if( pos )
							{
								m_lstOrigTarg.SetAt( pos, szItem );
								nCnt = MAX( nCnt, nSeq + 1 );
							}
						}
					}
				}

				hr = pSTL->GetNext();
			}
			pSTL->Release();

			// Now cleanup empty spots
			for( int j = nCnt; j < MAX_TARGETS; j++ )
			{
				pos = m_lstOrigTarg.FindIndex( nCnt );
				if( pos ) m_lstOrigTarg.RemoveAt( pos );
			}

			// Now fill current list
			pos = m_lstOrigTarg.GetHeadPosition();
			while( pos )
			{
				szItem = m_lstOrigTarg.GetNext( pos );
				if( szItem != _T("") ) m_lstTarg.AddTail( szItem );
			}

			// Fill list
			FillTargets();
		}
		else BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
	}
}

void StimulusDlg::FillTargets()
{
	m_lstTargets.ResetContent();

	CString szItem;
	POSITION pos = m_lstTarg.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstTarg.GetNext( pos );
		m_lstTargets.AddString( szItem );
	}
}

void StimulusDlg::OnClickBnUp() 
{
	int nIdx = m_lstTargets.GetCurSel();
	if( nIdx == LB_ERR ) return;
	if( nIdx <= 0 ) return;

	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL;

	pos1 = m_lstTarg.FindIndex( nIdx );
	pos2 = m_lstTarg.FindIndex( nIdx - 1 );
	szItem1 = m_lstTarg.GetAt( pos1 );
	szItem2 = m_lstTarg.GetAt( pos2 );
	m_lstTarg.SetAt( pos2, szItem1 );
	m_lstTarg.SetAt( pos1, szItem2 );

	FillTargets();

	m_lstTargets.SetCurSel( nIdx - 1 );
}

void StimulusDlg::OnClickBnDown() 
{
	int nIdx = m_lstTargets.GetCurSel();
	if( nIdx == LB_ERR ) return;
	int nCnt = m_lstTargets.GetCount();
	if( nIdx >= ( nCnt - 1 ) ) return;

	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL;

	pos1 = m_lstTarg.FindIndex( nIdx );
	pos2 = m_lstTarg.FindIndex( nIdx + 1 );
	szItem1 = m_lstTarg.GetAt( pos1 );
	szItem2 = m_lstTarg.GetAt( pos2 );
	m_lstTarg.SetAt( pos2, szItem1 );
	m_lstTarg.SetAt( pos1, szItem2 );

	FillTargets();

	m_lstTargets.SetCurSel( nIdx + 1 );
}

void StimulusDlg::OnBnGen()
{
	if( !Confirm() ) return;

	CString szFile;
	Stimuluss stim;
	stim.PutID( m_szID );
	if( stim.GenerateFile( &m_lstElem, szFile ) )
	{
		m_fGenerated = TRUE;
	}
}

BOOL StimulusDlg::Confirm()
{
	// Confirm we have an ID (used for name...for now)
	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The stimulus ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	int nPos = m_szID.Find( szDelim );
	if( nPos != -1 )
	{
		CString szMsg;
		szMsg.Format( IDS_ERR_DELIM, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	// Confirm we have elements listed
	if( m_lstElements.GetCount() <= 0 )
	{
		BCGPMessageBox( _T("You must have at least one element.") );
		CWnd* pWnd = GetDlgItem( IDC_LIST_ELEMENTS );
		if( pWnd ) pWnd->SetFocus();
		return FALSE;
	}

	return TRUE;
}

void StimulusDlg::OnBnGraph() 
{
	if( !Confirm() ) return;
	if( !m_fGenerated )
	{
		OnBnGen();
		if( !m_fGenerated ) return;
	}

	CString szFile;
	Stimuluss::GetStimFileFromID( m_szID, szFile );
	if( szFile != _T("") )
	{
		CGraphDlg gd;
		gd.ChartHwr( _T(""), _T(""), _T(""), szFile,
			::GetDeviceResolution(), ::GetMinPenPressure() );

//		CFile::Remove( szFile );
	}
}

void StimulusDlg::OnBnView() 
{
	if( !Confirm() ) return;
	if( !m_fGenerated )
	{
		OnBnGen();
		if( !m_fGenerated ) return;
	}

	CString szFile;
	Stimuluss::GetStimFileFromID( m_szID, szFile );
	if( szFile != _T("") )
	{
		CString szCmd;
		szCmd.LoadString( IDS_EDITOR );
		szCmd += szFile;
		WinExec( szCmd, SW_SHOW );

//		CFile::Remove( szFile );
	}
}

void StimulusDlg::OnDblclkListElements() 
{
	OnClickBnEdit();
}

void StimulusDlg::OnDblclkListTargets() 
{
	OnClickBnLeft();
}

void StimulusDlg::OnDblclkListTargetsavail() 
{
	OnClickBnRight();
}

BOOL StimulusDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("stimuli.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void StimulusDlg::OnLbnSelchangeListElements()
{
	EnableDisable();
}

void StimulusDlg::OnLbnSelchangeListTargetsavail()
{
	EnableDisable();
}

void StimulusDlg::OnLbnSelchangeListTargets()
{
	EnableDisable();
}

void StimulusDlg::EnableDisable()
{
	int nSelElem = m_lstElements.GetCurSel();
	int nSelTargA = m_lstTargetsAvail.GetCurSel();
	int nSelTarg = m_lstTargets.GetCurSel();
	BOOL fElemSel = ( nSelElem != LB_ERR );
	BOOL fTargASel = ( nSelTargA != LB_ERR );
	BOOL fTargSel = ( nSelTarg != LB_ERR );
	int nElems = m_lstElements.GetCount();
	int nTargsA = m_lstTargetsAvail.GetCount();
	int nTargs = m_lstTargets.GetCount();
	BOOL fFirst = ( ( nTargs > 0 ) && fTargSel && ( nSelTarg == 0 ) );
	BOOL fLast = ( ( nTargs > 0 ) && fTargSel && ( nSelTarg == ( nTargs - 1 ) ) );

	CWnd* pWnd = GetDlgItem( IDC_BN_EDIT );
 	pWnd->EnableWindow( TRUE );
 	pWnd = GetDlgItem( IDC_BN_DEL );
 	pWnd->EnableWindow( TRUE );

	pWnd = GetDlgItem( IDC_BN_RIGHTALL );
	pWnd->EnableWindow( nTargsA > 0 );
	pWnd = GetDlgItem( IDC_BN_LEFTALL );
	pWnd->EnableWindow( nTargs > 0 );
	pWnd = GetDlgItem( IDC_BN_RIGHT );
	pWnd->EnableWindow( fTargASel );
	pWnd = GetDlgItem( IDC_BN_LEFT );
	pWnd->EnableWindow( fTargSel );

	pWnd = GetDlgItem( IDC_BN_UP );
	pWnd->EnableWindow( fTargSel && ( nTargs > 1 ) && !fFirst );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	pWnd->EnableWindow( fTargSel &&  ( nTargs > 1 ) && !fLast );
}

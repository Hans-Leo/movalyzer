#pragma once

// PasswordDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// PasswordDlg dialog

class AFX_EXT_CLASS PasswordDlg : public CBCGPDialog
{
// Construction
public:
	PasswordDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_PASSWORD };
	CString			m_szPass;
	CString			m_szConfirm;
	CString			m_szDefault;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fSubject;
	BOOL			m_fPassChange;
	BOOL			m_fUser;
	CString			m_szUserID;
	CBCGPURLLinkButton		m_txtHelp;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnPass();
	DECLARE_MESSAGE_MAP()
};

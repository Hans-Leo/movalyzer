// ElementPagePattern.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ElementPagePattern.h"
#include "ElementSheet.h"
#include "ElementPageGeneral.h"
#include "Experiments.h"
#include "Groups.h"
#include "Subjects.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ElementPagePattern property page

IMPLEMENT_DYNCREATE(ElementPagePattern, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ElementPagePattern, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_EXP, OnSelchangeCboExp)
	ON_CBN_SELCHANGE(IDC_CBO_GRP, OnSelchangeCboGrp)
	ON_CBN_SELCHANGE(IDC_CBO_SUBJ, OnSelchangeCboSubj)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ElementPagePattern::ElementPagePattern( IElement* pE )
	: CBCGPPropertyPage(ElementPagePattern::IDD), m_pE( pE )
{
	// Extract out experiment, subject, trial...and set combos
	if( m_pE )
	{
		BOOL fIsImage = FALSE;
		m_pE->get_IsImage( &fIsImage );

		if( !fIsImage )
		{
			BSTR bstr = NULL;
			m_pE->get_Pattern( &bstr );
			CString szTmp = bstr;

			int a = szTmp.ReverseFind( '\\' );
			if( a != -1 ) szTmp = szTmp.Mid( a + 1 );
			szTrial = szTmp;
			szTmp.MakeUpper();
			if( szTmp.GetLength() >= 12 )
			{
				szExp = szTmp.Left( 3 );
				szGrp = szTmp.Mid( 3, 3 );
				szSubj = szTmp.Mid( 6, 3 );
			}
		}
	}
}

ElementPagePattern::~ElementPagePattern()
{
}

void ElementPagePattern::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_TRIAL, m_cboTrial);
	DDX_Control(pDX, IDC_CBO_SUBJ, m_cboSubj);
	DDX_Control(pDX, IDC_CBO_GRP, m_cboGrp);
	DDX_Control(pDX, IDC_CBO_EXP, m_cboExp);
}

/////////////////////////////////////////////////////////////////////////////
// ElementPagePattern message handlers

BOOL ElementPagePattern::OnApply() 
{
	if( !m_pE ) return FALSE;

	UpdateData( TRUE );

	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = pSheet ? (ElementPageGeneral*)pSheet->GetPage( 0 ) : NULL;
	if( !pPage ) return FALSE;

	CString szItem;
	if( pPage->IsPattern() )
	{
		CString szExpID, szGrpID, szSubjID, szTrial;
		GetIDs( szExpID, szGrpID, szSubjID, szTrial );
		GetHWRMask( szExpID, szGrpID, szSubjID, szItem );
		int nPos = szItem.ReverseFind( '\\' );
		szItem = szItem.Left( nPos + 1 );
		szItem += szTrial;
		CFileFind ff;
		if( !ff.FindFile( szItem ) )
		{
			BCGPMessageBox( _T("Invalid pattern/source data file.") );
			return FALSE;
		}
	}
	else if( pPage->IsImage() )
	{
		szItem = pPage->m_szBMP;
	}
	else szItem = _T("");

	m_pE->put_Pattern( szItem.AllocSysString() );

	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

void ElementPagePattern::OnSelchangeCboExp() 
{
	CString szItem, szExpID, szGrpID, szSubjID, szTrial;
	HRESULT hr, hr2;
	int nCnt = 0;

	Groups grp;
	if( !grp.IsValid() ) return;

	// Reset content
	m_cboGrp.ResetContent();
	m_cboSubj.ResetContent();
	m_cboTrial.ResetContent();

	// Getselection IDs
	GetIDs( szExpID, szGrpID, szSubjID, szTrial );
	if( szExpID == _T("") ) return;

	// Find the groups for this experiment
	IExperimentMember* pEM = NULL;
	hr2 = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							  IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr2 ) ) return;
	hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetID( szGrpID );

		hr2 = pEM->FindForGroup( szExpID.AllocSysString(),
								 szGrpID.AllocSysString() );
		if( SUCCEEDED( hr2 ) )
		{
			grp.GetDescriptionWithID( szItem, TRUE );
			m_cboGrp.AddString( szItem );
			nCnt++;
		}

		hr = grp.GetNext();
	}
	pEM->Release();

	// If no groups, indicate
	if( nCnt == 0 )
	{
		szItem = _T("No Groups");
		m_cboGrp.AddString( szItem );
		m_cboGrp.SelectString( -1, szItem );
	}
}

void ElementPagePattern::OnSelchangeCboGrp() 
{
	CString szItem, szExpID, szGrpID, szSubjID, szTrial;
	HRESULT hr, hr2;
	int nCnt = 0;

	Subjects subj;
	if( !subj.IsValid() ) return;

	// Reset content
	m_cboSubj.ResetContent();
	m_cboTrial.ResetContent();

	// Getselection IDs
	GetIDs( szExpID, szGrpID, szSubjID, szTrial );
	if( ( szExpID == _T("") ) || ( szGrpID == _T("") ) ) return;

	// Find the subjects for this experiment/group
	IExperimentMember* pEM = NULL;
	hr2 = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							  IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr2 ) ) return;
	hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetID( szSubjID );

		hr2 = pEM->Find( szExpID.AllocSysString(),
						 szGrpID.AllocSysString(),
						 szSubjID.AllocSysString() );
		if( SUCCEEDED( hr2 ) )
		{
			subj.GetNameWithID( szItem, ::IsPrivacyOn(), TRUE );
			m_cboSubj.AddString( szItem );
			nCnt++;
		}

		hr = subj.GetNext();
	}
	pEM->Release();

	// If no subjects, indicate
	if( nCnt == 0 )
	{
		szItem = _T("No Subjects");
		m_cboSubj.AddString( szItem );
		m_cboSubj.SelectString( -1, szItem );
	}
}

void ElementPagePattern::OnSelchangeCboSubj() 
{
	CString szItem, szExpID, szGrpID, szSubjID, szTrial;
	CStringList lstTrialsHWR;
	POSITION pos;
	int nCnt = 0;

	// Reset content
	m_cboTrial.ResetContent();

	// Getselection IDs
	GetIDs( szExpID, szGrpID, szSubjID, szTrial );
	if( ( szExpID == _T("") ) || ( szGrpID == _T("") ) || ( szSubjID == _T("") ) ) return;

	// Find the trials for this experiment/group/subject
	GetHWRMask( szExpID, szGrpID, szSubjID, szTrial );
	::SortFilesAlpha( &lstTrialsHWR, szTrial );
	pos = lstTrialsHWR.GetHeadPosition();
	while( pos )
	{
		szItem = lstTrialsHWR.GetNext( pos );
		szItem.MakeUpper();
		m_cboTrial.AddString( szItem );
		nCnt++;
	}

	// If no trials, indicate
	if( nCnt == 0 )
	{
		szItem = _T("No Trials");
		m_cboTrial.AddString( szItem );
		m_cboTrial.SelectString( -1, szItem );
	}
}

BOOL ElementPagePattern::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ElementPagePattern::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle( TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_ELEM );
	m_tooltip.SetTitle( _T("ELEMENT") );
	CWnd* pWnd = GetDlgItem( IDC_CBO_TRIAL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the trial to convert."), _T("Trial") );
	pWnd = GetDlgItem( IDC_CBO_SUBJ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the subject whose trials you wish to convert."), _T("Subject") );
	pWnd = GetDlgItem( IDC_CBO_GRP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the group of the experiment."), _T("Group") );
	pWnd = GetDlgItem( IDC_CBO_EXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an experiment."), _T("Experiment") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	// FILL STATIC COMBO
	// Experiments
	Experiments exp;
	if( !exp.IsValid() ) return TRUE;
	CString szItem;
	HRESULT hr = exp.ResetToStart();
	int nCnt = 0;
	while( SUCCEEDED( hr ) )
	{
		exp.GetDescriptionWithID( szItem, TRUE );
		m_cboExp.AddString( szItem );
		hr = exp.GetNext();
		nCnt++;
	}
	if( nCnt == 0 )
	{
		szItem = _T("No Experiments");
		m_cboExp.AddString( szItem );
		m_cboExp.SelectString( -1, szItem );
	}

	if( szTrial != _T("") )
	{
		m_cboExp.SelectString( -1, szExp );
		OnSelchangeCboExp();
		m_cboGrp.SelectString( -1, szGrp );
		OnSelchangeCboGrp();
		m_cboSubj.SelectString( -1, szSubj );
		OnSelchangeCboSubj();
		m_cboTrial.SelectString( -1, szTrial );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ElementPagePattern::GetIDs( CString& szExpID, CString& szGrpID, CString& szSubjID, CString& szTrial )
{
	CString szDelim, szItem;
	int nPos;
	CWnd* pWnd;
	BOOL fExp = FALSE, fGrp = FALSE, fSubj = FALSE, fTrial = FALSE;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	if( m_cboExp.GetCurSel() >= 0 )
	{
		m_cboExp.GetLBText( m_cboExp.GetCurSel(), szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szExpID = szItem.Left( nPos - 1 );
			fExp = TRUE;
		}
	}

	// Which group
	if( m_cboGrp.GetCurSel() >= 0 )
	{
		m_cboGrp.GetLBText( m_cboGrp.GetCurSel(), szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Left( nPos - 1 );
			fGrp = TRUE;
		}
	}

	// Which subject
	if( m_cboSubj.GetCurSel() >= 0 )
	{
		m_cboSubj.GetLBText( m_cboSubj.GetCurSel(), szItem );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szSubjID = szItem.Left( nPos - 1 );
			fSubj = TRUE;
		}
	}

	// Which trial
	if( m_cboTrial.GetCurSel() >= 0 )
	{
		m_cboTrial.GetLBText( m_cboTrial.GetCurSel(), szItem );
		szTrial = szItem;
		fTrial = TRUE;
	}

	// Window enable/disable
	pWnd = GetDlgItem( IDC_CBO_GRP );
	if( pWnd ) pWnd->EnableWindow( fExp );
	pWnd = GetDlgItem( IDC_CBO_SUBJ );
	if( pWnd ) pWnd->EnableWindow( fGrp );
	pWnd = GetDlgItem( IDC_CBO_TRIAL );
	if( pWnd ) pWnd->EnableWindow( fSubj );
}

BOOL ElementPagePattern::OnSetActive() 
{
	ElementSheet* pSheet = (ElementSheet*)this->GetParent();
	ElementPageGeneral* pPage = pSheet ? (ElementPageGeneral*)pSheet->GetPage( 0 ) : NULL;
	if( pPage && !pPage->IsPattern() )
	{
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_GRP );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_SUBJ );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_TRIAL );
		pWnd->EnableWindow( FALSE );
	}
	else
	{
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXP );
		pWnd->EnableWindow( TRUE );
	}
	
	return CBCGPPropertyPage::OnSetActive();
}

BOOL ElementPagePattern::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ElementPagePattern::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("element_pattern.html"), this );
	return TRUE;
}

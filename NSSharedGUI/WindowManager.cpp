// WindowManager.cpp

#include "StdAfx.h"
#include "NSSharedGUI.h"
#include "WindowManager.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

WindowManager::WindowManager()
{
}

///////////////////////////////////////////////////////////////////////////////

WindowManager::~WindowManager()
{
	Clear();
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    Clear
// FullName:  WindowManager::Clear
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fDestroy
//************************************
void WindowManager::Clear( BOOL fDestroy )
{
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		CWnd* pWnd = (CWnd*)m_lstItems.GetNext( pos );
		if( pWnd )
		{
			RemoveItem( pWnd );
			if( fDestroy && !pWnd->IsKindOf( RUNTIME_CLASS( CBCGPDockingControlBar ) ) )
				pWnd->DestroyWindow();
		}
	}
	m_lstItems.RemoveAll();
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    PostMessage
// FullName:  WindowManager::PostMessage
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: WPARAM msg
// Parameter: LPARAM val
// Parameter: CWnd * pWndCaller
//************************************
void WindowManager::PostMessage( WPARAM msg, LPARAM val, CWnd* pWndCaller )
{
	int nCnt = 0 ;

	CWnd* pWnd = NULL;
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pWnd = (CWnd*)m_lstItems.GetNext( pos );
		if( pWnd && pWnd->m_hWnd && ( pWnd != pWndCaller ) )
			pWnd->PostMessage( WM_WINDOWMANAGER, msg, val );
	}
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    AddItem
// FullName:  WindowManager::AddItem
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CWnd * pWnd
//************************************
BOOL WindowManager::AddItem( CWnd* pWnd )
{
	if( pWnd )
	{
		m_lstItems.AddTail( (CObject*)pWnd );
		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    RemoveItem
// FullName:  WindowManager::RemoveItem
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CWnd * pWnd
//************************************
BOOL WindowManager::RemoveItem( CWnd* pWnd )
{
	if( !pWnd ) return FALSE;

	POSITION pos = m_lstItems.GetHeadPosition();
	POSITION posLast = NULL;
	while( pos )
	{
		posLast = pos;
		CWnd* pCurWnd = (CWnd*)m_lstItems.GetNext( pos );
		if( pCurWnd == pWnd )
		{
			m_lstItems.RemoveAt( posLast );
			break;
		}
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

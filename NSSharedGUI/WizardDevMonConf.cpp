// WizardDevMonConf.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardDevMonConf.h"
#include "WizardDevSheet.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define OPT_CHOOSE_WH		0
#define OPT_CHOOSE_DIAG		1

#define OPT_ASPECT_43		0
#define OPT_ASPECT_53		1
#define OPT_ASPECT_54		2
#define OPT_ASPECT_169		3

// WizardDevMonConf dialog

IMPLEMENT_DYNAMIC(WizardDevMonConf, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardDevMonConf, CBCGPPropertyPage)
	ON_CBN_SELCHANGE( IDC_CBO_DISP, OnChangeChoose)
	ON_CBN_SELCHANGE( IDC_CBO_DISP2, OnChangeAspect)
	ON_EN_KILLFOCUS(IDC_EDIT_DIAGONAL, OnEnKillfocusEditDiagonal)
	ON_BN_CLICKED(IDC_BN_CALC, OnBnClickedBnCalc)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardDevMonConf::WizardDevMonConf() : CBCGPPropertyPage(WizardDevMonConf::IDD)
{
	m_nAspect = OPT_ASPECT_43;
	m_dHeightDisplay = 12. * 2.54;
	m_dWidthDisplay = 16. * 2.54;
	m_dDiagDisplay = 21. * 2.54;
}

WizardDevMonConf::~WizardDevMonConf()
{
}

void WizardDevMonConf::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DIAGONAL, m_dDiagDisplay);
	DDX_Text(pDX, IDC_EDIT_HEIGHT_DISPLAY, m_dHeightDisplay);
	DDX_Text(pDX, IDC_EDIT_WIDTH_DISPLAY, m_dWidthDisplay);
	DDX_Control(pDX, IDC_BN_CALC, m_bnCalc);
	DDX_Control(pDX, IDC_CBO_DISP, m_cboChoose);
	DDX_Control(pDX, IDC_CBO_DISP2, m_cboAspect);
}

// WizardDevMonConf message handlers

BOOL WizardDevMonConf::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	m_bnCalc.SetImage( IDB_CALC );
	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_DEV_SETUP );
	CWnd* pWnd= GetDlgItem( IDC_EDIT_HEIGHT_DISPLAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the height of the display in centimeters."), _T("Display Height") );
	pWnd= GetDlgItem( IDC_EDIT_WIDTH_DISPLAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the width of the display in centimeters."), _T("Display Width") );
	pWnd= GetDlgItem( IDC_EDIT_DIAGONAL );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Specify the diagonal of the display in centimeters.\nThe aspect ratio and diagonal size will be used to calculate the width and height of the display."), _T("Display Diagonal") );
		pWnd->ShowWindow( SW_HIDE );
	}
	pWnd = GetDlgItem( IDC_BN_CALC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unit conversion calculator."), _T("Calculator") );
	pWnd = GetDlgItem( IDC_CBO_DISP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the option of which you know to set the size of your display."), _T("Select Option") );
	pWnd = GetDlgItem( IDC_CBO_DISP2 );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Select the aspect ratio of your display.\nThe aspect ratio and diagonal size will be used to calculate the width and height of the display."), _T("Aspect Ratio") );
		pWnd->ShowWindow( SW_HIDE );
	}
	pWnd = GetDlgItem( IDC_TXT_EDIT );
	if( pWnd ) pWnd->ShowWindow( FALSE );
	pWnd = GetDlgItem( IDC_TXT_EDIT2 );
	if( pWnd ) pWnd->ShowWindow( FALSE );

	// data
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	if( pParent )
	{
		m_dHeightDisplay = pParent->m_dHeightDisplay;
		m_dWidthDisplay = pParent->m_dWidthDisplay;
		m_dDiagDisplay = sqrt( ( m_dWidthDisplay * m_dWidthDisplay ) + ( m_dHeightDisplay * m_dHeightDisplay ) );
		double dFactor = m_dWidthDisplay / m_dHeightDisplay;
		CString szFactor;
		szFactor.Format( _T("%.2f"), dFactor );
		if( szFactor == _T("1.25") ) m_nAspect = OPT_ASPECT_54;
		else if( szFactor == _T("1.67") ) m_nAspect = OPT_ASPECT_53;
		else if( szFactor == _T("1.78") ) m_nAspect = OPT_ASPECT_169;

		UpdateData( FALSE );
	}

	// Fill choice combo box (default is width and height)
	m_cboChoose.AddString( _T("Width and height.") );
	m_cboChoose.AddString( _T("Diagonal size and aspect ratio.") );
	m_cboChoose.SetCurSel( 0 );

	// Fill aspect ratio combo
	m_cboAspect.AddString( _T("4 x 3") );
	m_cboAspect.AddString( _T("5 x 3") );
	m_cboAspect.AddString( _T("5 x 4") );
	m_cboAspect.AddString( _T("16 x 9") );
	m_cboAspect.SetCurSel( 0 );

	// Highlight the main sentence
	CStatic* pDis = (CStatic*)GetDlgItem( IDC_TXT_HELP );
	CDC* pDC = GetDC();
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_BOLD;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	pDis->SetFont( &font, TRUE );
	pDC->SelectObject( pOldFont );
	ReleaseDC( pDC );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardDevMonConf::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardDevMonConf::OnWizardBack() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

LRESULT WizardDevMonConf::OnWizardNext() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardNext();
}

void WizardDevMonConf::OnBnClickedBnCalc()
{
	::ViewCalc( this );
}

void WizardDevMonConf::OnEnKillfocusEditDiagonal()
{
	UpdateData( TRUE );

	// diagonal = d, width = w, height = h, aspect ration = ar (eg 4:3)
	// d^2 = w^2 + h^2
	// w = ar * h;
	// d^2 = (ar * h)^2 + h^2 = ar^2 * h^2 + h^2 = h^2 * ( ar^2 + 1 )
	// h^2 = d^2 / ( ar^2 + 1 )
	// h = sqrt( d^2 / ( ar^2 + 1 ) )
	double dFactor = 1.;
	switch( m_nAspect )
	{
		case OPT_ASPECT_43: dFactor = 4. / 3.; break;
		case OPT_ASPECT_54: dFactor = 5. / 4.; break;
		case OPT_ASPECT_53: dFactor = 5. / 3.; break;
		case OPT_ASPECT_169: dFactor = 16. / 9.; break;
	}

	m_dHeightDisplay = sqrt( ( m_dDiagDisplay * m_dDiagDisplay ) / ( ( dFactor * dFactor ) + 1 ) );
	m_dWidthDisplay = dFactor * m_dHeightDisplay;

	UpdateData( FALSE );
}

BOOL WizardDevMonConf::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("devicesetup.html"), this );
	return TRUE;
}

BOOL WizardDevMonConf::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardDevMonConf::OnChangeChoose()
{
	BOOL fShow = TRUE;
	switch( m_cboChoose.GetCurSel() )
	{
		case OPT_CHOOSE_WH: fShow = FALSE; break;
		case OPT_CHOOSE_DIAG: fShow = TRUE; break;
	}
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DIAGONAL );
	if( pWnd ) pWnd->ShowWindow( fShow );
	m_cboAspect.ShowWindow( fShow );
	pWnd = GetDlgItem( IDC_TXT_EDIT );
	if( pWnd ) pWnd->ShowWindow( fShow );
	pWnd = GetDlgItem( IDC_TXT_EDIT2 );
	if( pWnd ) pWnd->ShowWindow( fShow );

	pWnd = GetDlgItem( IDC_EDIT_WIDTH_DISPLAY );
	if( pWnd ) pWnd->EnableWindow( !fShow );
	pWnd = GetDlgItem( IDC_EDIT_HEIGHT_DISPLAY );
	if( pWnd ) pWnd->EnableWindow( !fShow );
}

void WizardDevMonConf::OnChangeAspect()
{
	m_nAspect = m_cboAspect.GetCurSel();
	OnEnKillfocusEditDiagonal();
}

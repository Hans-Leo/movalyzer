#include "StdAfx.h"
#include "MSOffice.h"

// Office Versions
#define	OFFICE_97		1
#define	OFFICE_2000		2
#define	OFFICE_2002		3
#define	OFFICE_2007		4
#define	OFFICE_2010		5
#define	OFFICE_2012		6

// Default Settings
#define OFFICE_VER OFFICE_2010

// Paths to required MS OFFICE files.
#if		OFFICE_VER == OFFICE_2010
#define _MSDLL_PATH "C:\Program Files (x86)\Common Files\Microsoft Shared\Office14\MSO.DLL"
#define	_MSVBA_PATH "C:\Program Files (x86)\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define	_MSEXL_PATH "C:\Program Files (x86)\Microsoft Office\Office14\EXCEL.EXE"
#elif	OFFICE_VER == OFFICE_2007
#define _MSDLL_PATH "C:\Program Files\Common Files\Microsoft Shared\Office12\MSO.DLL"
#define	_MSVBA_PATH "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define	_MSEXL_PATH "C:\Program Files\Microsoft Office\Office12\EXCEL.EXE"
#elif	OFFICE_VER == OFFICE_2003
#define _MSDLL_PATH "C:\Program Files\Common Files\Microsoft Shared\Office10\MSO.DLL"
#define _MSVBA_PATH "C:\Program Files\Common Files\Microsoft Shared\VBA\VBA6\VBE6EXT.olb"
#define _MSEXL_PATH "C:\Program Files\Microsoft Office\Office10\EXCEL.EXE"
#endif

// Delete the *.tlh files when changing import qualifiers
//#import _MSDLL_PATH rename("RGB", "MSRGB") rename("DocumentProperties", "WordDocumentProperties") raw_interfaces_only
#import _MSDLL_PATH auto_rename
#import _MSVBA_PATH auto_rename
#import _MSEXL_PATH exclude( "IFont", "IPicture" ) auto_rename


// EXCEL
BOOL Excel::OpenDelimitedText( CString szFile, UINT uiFormat, BOOL fTab, BOOL fSemicolon, BOOL fComma, BOOL fSpace )
{
	try
	{
		Excel::_ApplicationPtr app( __uuidof( Excel::Application ) );
		if( !app ) return FALSE;
		CString szDec = _T("."), szThous = _T(",");
		app->Workbooks->OpenText( szFile.AllocSysString(),
								  xlWindows,
								  (_variant_t)1,
								  xlDelimited,
								  xlTextQualifierDoubleQuote,
								  VARIANT_FALSE,
								  fTab ? VARIANT_TRUE : VARIANT_FALSE,
								  fSemicolon ? VARIANT_TRUE : VARIANT_FALSE,
								  fComma ? VARIANT_TRUE : VARIANT_FALSE,
								  fSpace ? VARIANT_TRUE : VARIANT_FALSE,
								  vtMissing	/*Other*/,
								  vtMissing /*OtherChar*/,
								  vtMissing /*FieldInfo*/,
								  vtMissing /*TextVisualLayout*/,
								  szDec.AllocSysString(),
								  szThous.AllocSysString(),
								  VARIANT_FALSE /*TrailingMinusNumbers*/);

		app->put_Visible( 0, VARIANT_TRUE );
		app->put_UserControl( TRUE );

		if( uiFormat == EXCEL_FORMAT_SUBJECTREPORT )
		{
			Excel::_WorksheetPtr ws = app->ActiveSheet;
			if( !ws ) return TRUE;
			for( int i = 2; i <= 22; i++ )
			{
				Excel::RangePtr cell = ws->Cells->GetItem( i, i );
				if( cell && ( i != 3 ) && ( i != 5  ) ) cell->PutColumnWidth( 16 );
			}
		}
		else if( uiFormat == EXCEL_FORMAT_QUESTIONNAIRE )
		{
 			Excel::_WorksheetPtr ws = app->ActiveSheet;
 			if( !ws ) return TRUE;
			for( int i = 2; i < 50; i++ )
			{
 				Excel::RangePtr cell = ws->Cells->GetItem( i, i );
	 			if( !cell ) continue;
				cell->PutColumnWidth( 15 );
			}
		}
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;
}

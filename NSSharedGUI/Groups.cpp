// Groups.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GroupDlg.h"
#include "OverwriteDlg.h"
#include "GrpQuestionnaireDlg.h"
#include "Groups.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString	_szGrpExpID;

///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

///////////////////////////////////////////////////////////////////////////////

void Groups::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	Groups::GetDescriptionWithID( m_pGroup, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::GetNext()
{
	return Groups::GetNext( m_pGroup );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Find( CString szID )
{
	return Groups::Find( m_pGroup, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Add()
{
	return Groups::Add( m_pGroup );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Modify()
{
	return Groups::Modify( m_pGroup );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Remove()
{
	return Groups::Remove( m_pGroup );
}

///////////////////////////////////////////////////////////////////////////////

void Groups::SetDataPath( CString szPath )
{
	Groups::SetDataPath( m_pGroup, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Groups::GetID( IGroup* pGroup, CString& szVal )
{
	if( pGroup )
	{
		BSTR bstrItem;
		pGroup->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Groups::GetDescription( IGroup* pGroup, CString& szVal )
{
	if( pGroup )
	{
		BSTR bstrItem;
		pGroup->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Groups::GetDescriptionWithID( IGroup* pGroup, CString& szVal, BOOL fReverse )
{
	if( pGroup )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		Groups::GetID( pGroup, szID );
		pGroup->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void Groups::GetNotes( IGroup* pGroup, CString& szVal )
{
	if( pGroup )
	{
		BSTR bstrItem;
		pGroup->get_Notes( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::ResetToStart()
{
	return Groups::ResetToStart( m_pGroup );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::ResetToStart( IGroup* pGroup )
{
	if( pGroup ) return pGroup->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::GetNext( IGroup* pGroup )
{
	if( pGroup ) return pGroup->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Find( IGroup* pGroup, CString szID )
{
	if( pGroup ) return pGroup->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Add( IGroup* pGroup )
{
	if( !pGroup ) return E_FAIL;

	if( FAILED( pGroup->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Groups::Add") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Modify( IGroup* pGroup )
{
	if( !pGroup ) return E_FAIL;

	if( FAILED( pGroup->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Groups::Remove( IGroup* pGroup )
{
	if( !pGroup ) return E_FAIL;

	if( FAILED( pGroup->Remove() ) )
	{
		BCGPMessageBox( IDS_GRPDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Groups::SetDataPath( IGroup* pGroup, CString szPath )
{
	if( pGroup ) pGroup->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoAdd( CWnd* pOwner )
{
	return Groups::DoAdd( m_pGroup, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoAdd( IGroup* pGroup, CWnd* pOwner )
{
	if( !pGroup ) return FALSE;

	GroupDlg d( pOwner, TRUE );
	if( d.DoModal() == IDOK )
	{
		if( d.m_szAbbr.GetLength() == 0 )
		{
			BCGPMessageBox( IDS_GRPID_ERR );
			return FALSE;
		}

		pGroup->put_ID( d.m_szAbbr.AllocSysString() );
		pGroup->put_Description( d.m_szDesc.AllocSysString() );
		pGroup->put_Notes( d.m_szNotes.AllocSysString() );

		HRESULT hr = Groups::Add( pGroup );
		if( SUCCEEDED( hr ) ) return TRUE;
		else {
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Groups::DoAdd") );
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoEdit( CString szID, CWnd* pOwner )
{
	_szGrpExpID = m_szExpID;
	return Groups::DoEdit( m_pGroup, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoEdit( IGroup* pGroup, CString szID, CWnd* pOwner )
{
	if( !pGroup ) return FALSE;
	if( FAILED( Groups::Find( pGroup, szID ) ) ) return FALSE;

	CString szDesc, szNotes;

	Groups::GetDescription( pGroup, szDesc );
	Groups::GetNotes( pGroup, szNotes );

	GroupDlg d( pOwner );
	d.m_szExpID = _szGrpExpID;
	d.m_szAbbr = szID;
	d.m_szDesc = szDesc;
	d.m_szNotes = szNotes;
	if( d.DoModal() == IDOK )
	{
		pGroup->Find( szID.AllocSysString() );
		pGroup->put_Description( d.m_szDesc.AllocSysString() );
		pGroup->put_Notes( d.m_szNotes.AllocSysString() );
		HRESULT hr = Groups::Modify( pGroup );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_UPD_ERR );
			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::Export( CMemFile* pFile )
{
	if( !m_pGroup || !pFile ) return FALSE;

	CString szItem;

	Groups::GetID( m_pGroup, szItem );
	if( szItem == HOLDER ) return TRUE;

	WRITE_STRING( pFile, TAG_GROUP );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
		// Desc
	Groups::GetDescription( m_pGroup, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Notes
	Groups::GetNotes( m_pGroup, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan )
{
//	if( !m_pGroup || !pFile || !pFile->m_pStream ) return FALSE;
	if( !m_pGroup || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	Groups comp;
	BOOL fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( szLine == HOLDER ) return TRUE;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Group %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );
	// Notes (if another field has ended, need to remove do-while logic below)
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	szTemp = _T("");
	char charEnd = ' ';
	do
	{
		if( szLine == EXPORT_SEPARATOR )
		{
			PutNotes( szTemp );
			comp.PutNotes( szTemp );
			goto DoImport;
		}
		charEnd = ( szLine.GetLength() > 0 ) ? szLine.GetAt( szLine.GetLength() - 1 ) : ' ';
		if( charEnd == '\r' )
		{
			szLine = szLine.Left( szLine.GetLength() - 1 );
			szLine += _T(" ");
		}
		szTemp += szLine;
		fRet = READ_STRING( pFile, szLine );
		if( !fRet ) return FALSE;
	}
	while( charEnd == '\r' );
	PutNotes( szTemp );
	comp.PutNotes( szTemp );

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_GROUP, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;

	// cleanup of comparison files if applicable
	if( fFiles )
	{
		CFileFind ff;
		if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
		if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			comp.GetNotes( szLine );
			PutNotes( szLine );
			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Groups::ForceLocal()
{
	if( !m_pGroup ) return;

	HRESULT hr = m_pGroup->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Groups::ForceRemote()
{
	if( !m_pGroup ) return;

	HRESULT hr = m_pGroup->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Groups::ForceDefault()
{
	if( !m_pGroup ) return;

	HRESULT hr = m_pGroup->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		Groups dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );

		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

void Groups::SetTemp( BOOL fVal )
{
	if( m_pGroup ) m_pGroup->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DumpRecord( CString szID, CString szFile )
{
	if( !m_pGroup ) return FALSE;
	return SUCCEEDED( m_pGroup->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Groups::RemoveTemp()
{
	if( m_pGroup ) m_pGroup->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::Questionnaire( CString szExp, CString szGrp )
{
	GrpQuestionnaireDlg d( szExp, szGrp, AfxGetApp()->m_pMainWnd );
	if( d.DoModal() == IDOK ) return TRUE;
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::DoEditDup( CString szID, CString szExpID, CWnd* pOwner )
{
	// TODO
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Groups::Duplicate( CString szNewID, CString szExpID, CWnd* pOwner )
{
	if( !m_pGroup ) return FALSE;

	CString szOrigID, szDesc, szNotes, szID, szItem;
	CStringList lstID, lstItem;
	BSTR bstr = NULL;
	short nItem = 0;
	// grp questionnaire object
	IGQuestionnaire* pGQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
								   IID_IGQuestionnaire, (LPVOID*)&pGQ );
	if( FAILED( hr ) ) return FALSE;

	// original 
	GetID( szOrigID );
	GetDescription( szDesc );
	GetNotes( szNotes );

	// set new & add (if not exists)
	if( SUCCEEDED( Find( szNewID ) ) ) return FALSE;
	PutID( szNewID );
	PutDescription( szDesc );
	PutNotes( szNotes );
	hr = Add();
	if( FAILED( hr ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Groups::Duplicate") );
		return FALSE;
	}

	// locate for group
	hr = pGQ->FindForGroup( szExpID.AllocSysString(), szOrigID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		// get id & add to list
		pGQ->get_ID( &bstr );
		szID = bstr;
		lstID.AddTail( szID );
		// get item num & add to list
		pGQ->get_ItemNum( &nItem );
		szItem.Format( _T("%d"), nItem );
		lstItem.AddTail( szItem );

		// get next
		hr = pGQ->GetNext();
	}

	// loop through lists and add to db with new id
	POSITION posID = lstID.GetHeadPosition();
	POSITION posItem = lstItem.GetHeadPosition();
	while( posID )
	{
		szID = lstID.GetNext( posID );
		szItem = lstItem.GetNext( posItem );

		pGQ->put_ID( szID.AllocSysString() );
		pGQ->put_ExperimentID( szExpID.AllocSysString() );
		pGQ->put_GroupID( szNewID.AllocSysString() );
		pGQ->put_ItemNum( (short)( atoi( szItem ) ) );
		hr = pGQ->Add();
	}

	// cleanup
	pGQ->Release();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Groups::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pGroup ) m_pGroup->GetNumRecords( &val );
	return val;
}

 ///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

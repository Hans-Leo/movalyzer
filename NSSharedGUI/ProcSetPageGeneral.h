#pragma once

// ProcSetPageGeneral.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageGeneral dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageGeneral : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageGeneral)

// Construction
public:
	ProcSetPageGeneral( IProcSetting* pPS = NULL );
	~ProcSetPageGeneral();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_GENERAL };
	BOOL			m_fQuest;
	BOOL			m_fEnd;
	BOOL			m_fSpecify;
	BOOL			m_fRandom;
	BOOL			m_fRandomT;
	BOOL			m_fRandomR;
	BOOL			m_fShowInstr;
	BOOL			m_fCondInstr;
	BOOL			m_fEnterKey;
	BOOL			m_fAcceptRedo;
	int				m_nTrialMode;
	int				m_nRedoMode;
	CColoredButton	m_chkQuest;
	CColoredButton	m_chkEnd;
	CColoredButton	m_chkSpecify;
	CColoredButton	m_chkRandom;
	CColoredButton	m_chkRandomT;
	CColoredButton	m_chkRandomR;
	CColoredButton	m_chkShowInstr;
	CColoredButton	m_chkCondInstr;
	CColoredButton	m_chkEnterKey;
	CColoredButton	m_chkAcceptRedo;
	CColoredButton	m_rdoTrialFirst;
	CColoredButton	m_rdoTrialEach;
	CColoredButton	m_rdoRedoEach;
	CColoredButton	m_rdoRedoConsis;
	CColoredButton	m_rdoRedoAuto;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnChkSpecify();
	afx_msg void OnChkRandom();
	afx_msg void OnChkRules();
	afx_msg void OnBnClickedChkQuest();
	afx_msg void OnBnClickedChkCondinstr();
	afx_msg void OnBnClickedRdoTrialAll();
	afx_msg void OnBnClickedChkEnterKey();
	afx_msg void OnBnClickedChkAcceptredo();
	afx_msg void OnBnClickedRdoTrialOnce();
	afx_msg void OnBnClickedRules();
	afx_msg void OnBnClickedOrder();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

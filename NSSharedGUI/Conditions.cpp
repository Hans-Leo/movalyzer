// Conditions.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "OverwriteDlg.h"
#include "ConditionSheet.h"
#include "Conditions.h"
#include "Experiments.h"
#include "Stimuli.h"
#include "Feedbacks.h"
#include "..\CTreeLink\DBCommon.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	LEX_ALLOW		_T("acdehijlmnotuvy-/|\\_?}].@")

///////////////////////////////////////////////////////////////////////////////
// Implementation

#define VSET_BOOL( x, val ) \
	V_VT( x ) = VT_BOOL; \
	V_BOOL( x ) = val;

#define WRITE_ITEM(a) \
	WRITE_STRING( pFile, a ); \
	WRITE_STRING( pFile, _T("\n") );

#define WRITE_ITEM_BOOL(a) \
	szItem = a ? _T("1") : _T("0"); \
	WRITE_ITEM( szItem );

#define WRITE_ITEM_INT(a) \
	szItem.Format( _T("%d"), a ); \
	WRITE_ITEM( szItem );

///////////////////////////////////////////////////////////////////////////////
// interface methods

void NSConditions::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	NSConditions::GetDescriptionWithID( m_pNSCondition, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::GetNext()
{
	return NSConditions::GetNext( m_pNSCondition );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Find( CString szID )
{
	return NSConditions::Find( m_pNSCondition, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Add()
{
	return NSConditions::Add( m_pNSCondition );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Modify()
{
	return NSConditions::Modify( m_pNSCondition );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Remove()
{
	return NSConditions::Remove( m_pNSCondition );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetDataPath( CString szPath )
{
	NSConditions::SetDataPath( m_pNSCondition, szPath );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetBackupPath( CString szPath )
{
	NSConditions::SetBackupPath( m_pNSCondition, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void NSConditions::GetID( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetDescription( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetDescriptionWithID( INSCondition* pCondition, CString& szVal, BOOL fReverse )
{
	if( pCondition )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		NSConditions::GetID( pCondition, szID );
		pCondition->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetNotes( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Notes( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetInstruction( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Instruction( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetLex( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Lex( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStrokeMin( INSCondition* pCondition, short& nVal )
{
	if( pCondition ) pCondition->get_StrokeMin( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStrokeMax( INSCondition* pCondition, short& nVal )
{
	if( pCondition ) pCondition->get_StrokeMax( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStrokeLength( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_StrokeLength( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStrokeDirection( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_StrokeDirection( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStrokeSkip( INSCondition* pCondition, short& nVal )
{
	if( pCondition ) pCondition->get_StrokeSkip( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetRangeLength( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_RangeLength( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetRangeDirection( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_RangeDirection( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetUseStimulus( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_UseStimulus( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStimulus( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Stimulus( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStimulusWarning( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_StimulusWarning( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStimulusPrecue( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_StimulusPrecue( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetPrecueDuration( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_PrecueDuration( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetPrecueLatency( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_PrecueLatency( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetWarningDuration( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_WarningDuration( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetWarningLatency( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_WarningLatency( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetRecordImmediately( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_RecordImmediately( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStopWrongTarget( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_StopWrongTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStartFirstTarget( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_StartFirstTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetStopLastTarget( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_StopLastTarget( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetRecord( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_Record( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetProcess( INSCondition* pCondition, BOOL& fVal )
{
	if( pCondition ) pCondition->get_Process( &fVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetParent( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Parent( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetWord( INSCondition* pCondition, short& nVal )
{
	if( pCondition ) pCondition->get_Word( &nVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetMagnetForce( INSCondition* pCondition, double& dVal )
{
	if( pCondition ) pCondition->get_MagnetForce( &dVal );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetCountStrokes( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_CountStrokes( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetHideFeedback( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_HideFeedback( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetHideShowAfter( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_HideShowAfter( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetHideShowAfterShow( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_HideShowAfterShow( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetHideShowAfterStrokes( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_HideShowAfterStrokes( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetHideShowCount( INSCondition* pCondition, double& Val )
{
	if( pCondition ) pCondition->get_HideShowCount( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetTransform( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_Transform( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetXgain( INSCondition* pCondition, double& Val )
{
	if( pCondition ) pCondition->get_Xgain( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetXrotation( INSCondition* pCondition, double& Val )
{
	if( pCondition ) pCondition->get_Xrotation( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetYgain( INSCondition* pCondition, double& Val )
{
	if( pCondition ) pCondition->get_Ygain( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetYrotation( INSCondition* pCondition, double& Val )
{
	if( pCondition ) pCondition->get_Yrotation( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetFlagBadTarget( INSCondition* pCondition, BOOL& Val )
{
	if( pCondition ) pCondition->get_FlagBadTarget( &Val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetFeedback( INSCondition* pCondition, CString& szVal )
{
	if( pCondition )
	{
		BSTR bstrItem;
		pCondition->get_Feedback( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetGripperTask( INSCondition* pCondition, short& nVal )
{
	if( pCondition ) pCondition->get_GripperTask( &nVal );
}


///////////////////////////////////////////////////////////////////////////////

void NSConditions::PutSoundInfo( SoundCat cat, SoundType type, bool val )
{
	NSConditions::PutSoundInfo( m_pNSCondition, cat, type, val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::PutSoundInfo( SoundCat cat, SoundType type, int val )
{
	NSConditions::PutSoundInfo( m_pNSCondition, cat, type, val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::PutSoundInfo( SoundCat cat, SoundType type, CString val )
{
	NSConditions::PutSoundInfo( m_pNSCondition, cat, type, val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetSoundInfo( SoundCat cat, SoundType type, bool& val )
{
	NSConditions::GetSoundInfo( m_pNSCondition, cat, type, val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetSoundInfo( SoundCat cat, SoundType type, int& val )
{
	NSConditions::GetSoundInfo( m_pNSCondition, cat, type, val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetSoundInfo( SoundCat cat, SoundType type, CString& val )
{
	NSConditions::GetSoundInfo( m_pNSCondition, cat, type, val );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::PutSoundInfo( INSCondition* pCondition, SoundCat cat, SoundType type, bool val )
{
	if( pCondition )
	{
		// get appropriate interface
		INSConditionSound* pCondS = NULL;
		HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) return;

		VARIANT v;
		VS_BOOL( &v, val );
		pCondS->put_SoundInfo( cat, type, v );

		pCondS->Release();
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::PutSoundInfo( INSCondition* pCondition, SoundCat cat, SoundType type, int val )
{
	if( pCondition )
	{
		// get appropriate interface
		INSConditionSound* pCondS = NULL;
		HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) return;

		VARIANT v;
		VS_I2( &v, val );
		pCondS->put_SoundInfo( cat, type, v );

		pCondS->Release();
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::PutSoundInfo( INSCondition* pCondition, SoundCat cat, SoundType type, CString val )
{
	if( pCondition )
	{
		// get appropriate interface
		INSConditionSound* pCondS = NULL;
		HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) return;

		VARIANT v;
		VS_CSTRING( &v, val );
		pCondS->put_SoundInfo( cat, type, v );

		pCondS->Release();
	}
}

///////////////////////////////////////////////////////////////////////////////

#pragma warning(disable: 4800)
void NSConditions::GetSoundInfo( INSCondition* pCondition, SoundCat cat, SoundType type, bool& val )
{
	if( pCondition )
	{
		// get appropriate interface
		INSConditionSound* pCondS = NULL;
		HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) return;

		VARIANT v;
		pCondS->get_SoundInfo( cat, type, &v );
		val = V_BOOL( &v );

		pCondS->Release();
	}
}
#pragma warning(default: 4800)

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetSoundInfo( INSCondition* pCondition, SoundCat cat, SoundType type, int& val )
{
	if( pCondition )
	{
		// get appropriate interface
		INSConditionSound* pCondS = NULL;
		HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) return;

		VARIANT v;
		pCondS->get_SoundInfo( cat, type, &v );
		val = V_I2( &v );

		pCondS->Release();
	}
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetSoundInfo( INSCondition* pCondition, SoundCat cat, SoundType type, CString& val )
{
	if( pCondition )
	{
		// get appropriate interface
		INSConditionSound* pCondS = NULL;
		HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
		if( FAILED( hr ) ) return;

		VARIANT v;
		pCondS->get_SoundInfo( cat, type, &v );
		val = V_BSTR( &v );

		pCondS->Release();
	}
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::GetSoundInfo( UINT nSound, CString szCondID, BOOL& fTone, int& nFreq,
								 int& nDur, CString& szFile, BOOL& fTrigger )
{
	NSConditions cond;

	// get values
	short nVal = 0;
	BSTR bstr = NULL;
	BOOL fUse = FALSE;
	cond.Find( szCondID );
	cond.GetSoundInfo( eSC_Use, (eSoundType)( nSound + 1 ), fUse );
	cond.GetSoundInfo( eSC_Tone, (eSoundType)( nSound + 1 ), fTone );
	cond.GetSoundInfo( eSC_Frequency, (eSoundType)( nSound + 1 ), nFreq );
	cond.GetSoundInfo( eSC_Duration, (eSoundType)( nSound + 1 ), nDur );
	cond.GetSoundInfo( eSC_Media, (eSoundType)( nSound + 1 ), szFile );
	cond.GetSoundInfo( eSC_Trigger, (eSoundType)( nSound + 1 ), fTrigger );

	return fUse;
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::put_SoundScript( SoundType type, short ScriptType, CString Script )
{
	NSConditions::put_SoundScript( m_pNSCondition, type, ScriptType, Script );
}

void NSConditions::put_SoundScript( INSCondition* pCondition, SoundType type, short ScriptType, CString Script )
{
	if( !pCondition ) return;
	INSConditionSound* pCondS = NULL;
	HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
	if( FAILED( hr ) ) return;
	pCondS->put_SoundScript( type, ScriptType, Script.AllocSysString() );
	pCondS->Release();
}

void NSConditions::get_SoundScript( SoundType type, short& ScriptType, CString& Script )
{
	NSConditions::get_SoundScript( m_pNSCondition, type, ScriptType, Script );
}

void NSConditions::get_SoundScript( INSCondition* pCondition, SoundType type, short& ScriptType, CString& Script )
{
	if( !pCondition ) return;
	INSConditionSound* pCondS = NULL;
	HRESULT hr = pCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
	if( FAILED( hr ) ) return;
	BSTR bstr = NULL;
	pCondS->get_SoundScript( type, &ScriptType, &bstr );
	Script = bstr;
	pCondS->Release();
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetFeedbackInfo(short nWhich, BOOL fUse, short nColumn,
								   short nStroke, double dMin, double dMax,
								   BOOL fSwapColors)
{
	NSConditions::SetFeedbackInfo( m_pNSCondition, nWhich, fUse, nColumn, nStroke,
								   dMin, dMax, fSwapColors );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetFeedbackInfo(INSCondition* pCondition, short nWhich, BOOL fUse, short nColumn,
								   short nStroke, double dMin, double dMax, BOOL fSwapColors)
{
	if( !pCondition ) return;
	pCondition->SetFeedbackInfo( nWhich, fUse, nColumn, nStroke, dMin, dMax, fSwapColors );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetFeedbackInfo( short nWhich, BOOL& fUse, short& nColumn, short& nStroke,
								    double& dMin, double& dMax, BOOL& fSwapColors )
{
	GetFeedbackInfo( m_pNSCondition, nWhich, fUse, nColumn, nStroke, dMin, dMax, fSwapColors );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::GetFeedbackInfo( INSCondition* pCondition, short nWhich, BOOL& fUse, short& nColumn,
								    short& nStroke, double& dMin, double& dMax, BOOL& fSwapColors )
{
	if( !pCondition ) return;
	pCondition->GetFeedbackInfo( nWhich, &fUse, &nColumn, &nStroke, &dMin, &dMax, &fSwapColors );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::ResetToStart()
{
	return NSConditions::ResetToStart( m_pNSCondition );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::ResetToStart( INSCondition* pCondition )
{
	if( pCondition ) return pCondition->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::GetNext( INSCondition* pCondition )
{
	if( pCondition ) return pCondition->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Find( INSCondition* pCondition, CString szID )
{
	if( pCondition ) return pCondition->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Add( INSCondition* pCondition )
{
	if( !pCondition ) return E_FAIL;

	if( FAILED( pCondition->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" NSConditions::Add") );

		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Modify( INSCondition* pCondition )
{
	if( !pCondition ) return E_FAIL;

	if( FAILED( pCondition->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT NSConditions::Remove( INSCondition* pCondition )
{
	if( !pCondition ) return E_FAIL;

	if( FAILED( pCondition->Remove() ) )
	{
		BCGPMessageBox( IDS_CONDDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetDataPath( INSCondition* pCondition, CString szPath )
{
	if( pCondition ) pCondition->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetBackupPath( INSCondition* pCondition, CString szPath )
{
	if( pCondition ) pCondition->SetBackupPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoAdd( CWnd* pOwner )
{
	return NSConditions::DoAdd( m_pNSCondition, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoAdd( INSCondition* pCondition, CWnd* pOwner )
{
	if( !pCondition ) return FALSE;

	CStringList lstConds;
	CString szItem, szCID, szDesc, szDelim;
	BOOL fRecord;
	::GetDelimiter( szDelim );
	{
	NSConditions cond;
	HRESULT hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetRecord( fRecord );
		if( fRecord )
		{
			cond.GetID( szCID );
			cond.GetDescription( szDesc );
			szItem.Format( _T("%s%s%s"), szDesc, szDelim, szCID );
			lstConds.AddTail( szItem );
		}

		hr = cond.GetNext();
	}
	}

	ConditionSheet d( pCondition, _T(""), TRUE, FALSE, FALSE, &lstConds, NULL, pOwner );
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = NSConditions::Add( pCondition );
		if( SUCCEEDED( hr ) ) return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoEdit( CString szID, CWnd* pOwner )
{
	return NSConditions::DoEdit( m_pNSCondition, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoEdit( INSCondition* pCondition, CString szID, CWnd* pOwner )
{
	if( !pCondition ) return FALSE;

	CStringList lstConds, lstCondsWord;
	CString szItem, szCID, szDesc, szDelim, szParent;
	BSTR bstr;
	BOOL fRecord;
	::GetDelimiter( szDelim );
	HRESULT hr = pCondition->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pCondition->get_Record( &fRecord );
		pCondition->get_ID( &bstr );
		szCID = bstr;
		if( fRecord )
		{
			pCondition->get_Description( &bstr );
			szDesc = bstr;
			szItem.Format( _T("%s%s%s"), szDesc, szDelim, szCID );
			lstConds.AddTail( szItem );
		}
		pCondition->get_Parent( &bstr );
		szParent = bstr;
		if( szParent == szID ) lstCondsWord.AddTail( szCID );

		hr = pCondition->GetNext();
	}

	if( FAILED( NSConditions::Find( pCondition, szID ) ) ) return FALSE;
	ConditionSheet d( pCondition, _T(""), FALSE, FALSE, FALSE, &lstConds,
					  &lstCondsWord, pOwner );
	if( FAILED( NSConditions::Find( pCondition, szID ) ) ) return FALSE;
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = NSConditions::Modify( pCondition );
		if( FAILED( hr ) ) return FALSE;

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoEdit( CString szID, CString szExpID, CWnd* pOwner, UINT iSelectPage, int nEvent )
{
	return NSConditions::DoEdit( m_pNSCondition, szID, szExpID, pOwner, iSelectPage, nEvent );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoEditDup( CString szID, CString szExpID, CWnd* pOwner )
{
	return NSConditions::DoEditDup( m_pNSCondition, szID, szExpID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoEdit( INSCondition* pCondition, CString szID, CString szExpID,
						   CWnd* pOwner, UINT iSelectPage, int nEvent )
{
	if( !pCondition ) return FALSE;

	CStringList lstConds, lstCondsWord;
	CString szItem, szCID, szDesc, szDelim, szParent;
	BSTR bstr;
	BOOL fRecord;
	::GetDelimiter( szDelim );
	HRESULT hr = pCondition->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pCondition->get_Record( &fRecord );
		pCondition->get_ID( &bstr );
		szCID = bstr;
		if( fRecord )
		{
			pCondition->get_Description( &bstr );
			szDesc = bstr;
			szItem.Format( _T("%s%s%s"), szDesc, szDelim, szCID );
			lstConds.AddTail( szItem );
		}
		pCondition->get_Parent( &bstr );
		szParent = bstr;
		if( szParent == szID ) lstCondsWord.AddTail( szCID );

		hr = pCondition->GetNext();
	}

	// get experiment type
	IExperiment* pExp = NULL;
	hr = CoCreateInstance( CLSID_Experiment, NULL, CLSCTX_ALL,
						   IID_IExperiment, (LPVOID*)&pExp );
	if( FAILED( hr ) ) return FALSE;
	hr = pExp->Find( szExpID.AllocSysString() );
	short nType = EXP_TYPE_HANDWRITING;
	if( SUCCEEDED( hr ) ) pExp->get_Type( &nType );
	pExp->Release();

	if( FAILED( NSConditions::Find( pCondition, szID ) ) ) return FALSE;
	ConditionSheet d( pCondition, szExpID, FALSE, FALSE, FALSE, &lstConds,
					  &lstCondsWord, pOwner, nType == EXP_TYPE_GRIPPER,
					  iSelectPage, nEvent );
	if( FAILED( NSConditions::Find( pCondition, szID ) ) ) return FALSE;
	if( d.DoModal() == IDOK )
	{
		HRESULT hr = NSConditions::Modify( pCondition );
		if( FAILED( hr ) ) return FALSE;

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoEditDup( INSCondition* pCondition, CString szID, CString szExpID, CWnd* pOwner )
{
	if( !pCondition ) return FALSE;

	CStringList lstConds, lstCondsWord;
	CString szItem, szCID, szDesc, szDelim, szParent;
	BSTR bstr;
	BOOL fRecord;
	::GetDelimiter( szDelim );
	HRESULT hr = pCondition->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pCondition->get_Record( &fRecord );
		pCondition->get_ID( &bstr );
		szCID = bstr;
		if( fRecord )
		{
			pCondition->get_Description( &bstr );
			szDesc = bstr;
			szItem.Format( _T("%s%s%s"), szDesc, szDelim, szCID );
			lstConds.AddTail( szItem );
		}
		pCondition->get_Parent( &bstr );
		szParent = bstr;
		if( szParent == szID ) lstCondsWord.AddTail( szCID );

		hr = pCondition->GetNext();
	}

	if( FAILED( NSConditions::Find( pCondition, szID ) ) ) return FALSE;

	IExperimentCondition* pEC = NULL;
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) ) return FALSE;
	short nReps = -1;
	if( SUCCEEDED( pEC->Find( szExpID.AllocSysString(), szID.AllocSysString() ) ) )
		pEC->get_Replications( &nReps );

	ConditionSheet d( pCondition, szExpID, FALSE, FALSE, TRUE, &lstConds,
					  &lstCondsWord, pOwner );
	if( d.DoModal() == IDOK )
	{
		// add condition
		HRESULT hr = NSConditions::Add( pCondition );
		if( FAILED( hr ) )
		{
			pEC->Release();
			return FALSE;
		}

		// add to experiment (if applicable)
		if( nReps >= 0 )	// -1 if item not found
		{
			CString szNewID;
			NSConditions::GetID( pCondition, szNewID );
			if( FAILED( pEC->Find( szExpID.AllocSysString(), szNewID.AllocSysString() ) ) )
			{
				pEC->put_ExperimentID( szExpID.AllocSysString() );
				pEC->put_ConditionID( szNewID.AllocSysString() );
				pEC->put_Replications( nReps );
				hr = pEC->Add();
				if( FAILED( hr ) )
				{
					pEC->Release();
					return FALSE;
				}
			}
		}

		pEC->Release();
		return TRUE;
	}
	pEC->Release();

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoReps( CString szID, CString szExpID, CWnd* pOwner )
{
	return NSConditions::DoReps( m_pNSCondition, szID, szExpID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoReps( INSCondition* pCondition, CString szID, CString szExpID, CWnd* pOwner )
{
	if( !pCondition ) return FALSE;
	if( FAILED( NSConditions::Find( pCondition, szID ) ) ) return FALSE;

	IExperimentCondition* pEC = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
								   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) ) return FALSE;

	short nReps;
	hr = pEC->Find( szExpID.AllocSysString(), szID.AllocSysString() );
	if( SUCCEEDED( hr ) )
	{
		pEC->get_Replications( &nReps );
		pEC->Release();
	}
	else
	{
		pEC->Release();
		return FALSE;
	}

	ConditionSheet d( pCondition, szExpID, FALSE, TRUE, FALSE, NULL, NULL, pOwner );
	d.m_nReps = nReps;
	if( d.DoModal() == IDOK )
	{
		pEC = NULL;
		hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
							   IID_IExperimentCondition, (LPVOID*)&pEC );
		if( SUCCEEDED( hr ) )
			hr = pEC->Find( szExpID.AllocSysString(), szID.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
//			nReps = ( d.m_nReps > 0 ) ? d.m_nReps : 1;
			// added to allow for individual condition testing
			nReps = d.m_nReps;
			pEC->put_Replications( nReps );
			hr = pEC->Modify();
			pEC->Release();
			if( FAILED( hr ) )
			{
				BCGPMessageBox( IDS_EDIT_ERR );
				return FALSE;
			}

			return TRUE;
		}
		else if( pEC ) pEC->Release();
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::Export( CMemFile* pFile, BOOL fStimuliAlso, BOOL fFirst )
{
	if( !m_pNSCondition || !pFile ) return FALSE;

	CString szItem, szStimW, szStimP, szStim;
	double dVal;
	short nVal;
	BOOL fVal;

	// get appropriate interface
	INSConditionSound* pCondS = NULL;
	HRESULT hr = m_pNSCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_COND_ERR );
		return FALSE;
	}

	NSConditions::GetID( m_pNSCondition, szItem );
	if( szItem == HOLDER ) return TRUE;

	WRITE_STRING( pFile, TAG_CONDITION );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Desc
	NSConditions::GetDescription( m_pNSCondition, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Instruction
	NSConditions::GetInstruction( m_pNSCondition, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Lex
	NSConditions::GetLex( m_pNSCondition, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Notes
	NSConditions::GetNotes( m_pNSCondition, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Range Dir
	NSConditions::GetRangeDirection( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Range Len
	NSConditions::GetRangeLength( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stroke Dir
	NSConditions::GetStrokeDirection( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stroke Len
	NSConditions::GetStrokeLength( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stroke Max
	NSConditions::GetStrokeMax( m_pNSCondition, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stroke Min
	NSConditions::GetStrokeMin( m_pNSCondition, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stroke Skip
	NSConditions::GetStrokeSkip( m_pNSCondition, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record
	NSConditions::GetRecord( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Process
	NSConditions::GetProcess( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Parent
	NSConditions::GetParent( m_pNSCondition, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Word
	NSConditions::GetWord( m_pNSCondition, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Use Stimulus
	NSConditions::GetUseStimulus( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Warning Stimulus
	NSConditions::GetStimulusWarning( m_pNSCondition, szItem );
	szStimW = szItem;
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Warning Duration
	NSConditions::GetWarningDuration( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Warning Latency
	NSConditions::GetWarningLatency( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Precue Stimulus
	NSConditions::GetStimulusPrecue( m_pNSCondition, szItem );
	szStimP = szItem;
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Precue Duration
	NSConditions::GetPrecueDuration( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Precue Latency
	NSConditions::GetPrecueLatency( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Imperative Stimulus
	NSConditions::GetStimulus( m_pNSCondition, szItem );
	szStim = szItem;
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stop on wrong target
	NSConditions::GetStopWrongTarget( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record immediately
	NSConditions::GetRecordImmediately( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Magnet Force
	NSConditions::GetMagnetForce( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Count strokes
	NSConditions::GetCountStrokes( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide feedback
	NSConditions::GetHideFeedback( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide Show After
	NSConditions::GetHideShowAfter( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide Show After Show
	NSConditions::GetHideShowAfterShow( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide Show After Strokes
	NSConditions::GetHideShowAfterStrokes( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Hide Show Count
	NSConditions::GetHideShowCount( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Transform
	NSConditions::GetTransform( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Xgain
	NSConditions::GetXgain( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Ygain
	NSConditions::GetYgain( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Xrotation
	NSConditions::GetXrotation( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Yrotation
	NSConditions::GetYrotation( m_pNSCondition, dVal );
	szItem.Format( _T("%f"), dVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Flag Bad Target
	NSConditions::GetFlagBadTarget( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Start on first target
	NSConditions::GetStartFirstTarget( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Stop on last target
	NSConditions::GetStopLastTarget( m_pNSCondition, fVal );
	szItem = fVal ? _T("1") : _T("0");
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );

	// sounds
	BSTR bstrItem = NULL;
	pCondS->get_SoundCount( &nVal );
	int nCnt = nVal;
	for( int nPos = 0; nPos < nCnt; nPos++ )
	{
		pCondS->get_SoundUse( (SoundType)( nPos + 1 ), &fVal );
		WRITE_ITEM_BOOL( fVal );
		pCondS->get_SoundTone( (SoundType)( nPos + 1 ), &fVal );
		WRITE_ITEM_BOOL( fVal );
		pCondS->get_SoundToneFreq( (SoundType)( nPos + 1 ), &nVal );
		WRITE_ITEM_INT( nVal );
		pCondS->get_SoundToneDur( (SoundType)( nPos + 1 ), &nVal );
		WRITE_ITEM_INT( nVal );
		pCondS->get_SoundMedia( (SoundType)( nPos + 1 ), &bstrItem );
		szItem = bstrItem;
		WRITE_ITEM( szItem );
		pCondS->get_SoundTrigger( (SoundType)( nPos + 1 ), &fVal );
		WRITE_ITEM_BOOL( fVal );
	}
	// Feedback
	NSConditions::GetFeedback( m_pNSCondition, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Gripper task
	NSConditions::GetGripperTask( m_pNSCondition, nVal );
	szItem.Format( _T("%d"), nVal );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Visual Feedback
	short nCol = 0, nStroke = 0;
	double dMin = 0., dMax = 0.;
	BOOL fUse = FALSE, fSwap = FALSE;
	NSConditions::GetFeedbackInfo( m_pNSCondition, 1, fUse, nCol, nStroke, dMin, dMax, fSwap );
	WRITE_ITEM_BOOL( fUse );
	WRITE_ITEM_INT( nCol );
	WRITE_ITEM_INT( nStroke );
	szItem.Format( _T("%f"), dMin );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	szItem.Format( _T("%f"), dMax );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	WRITE_ITEM_BOOL( fSwap );
	NSConditions::GetFeedbackInfo( m_pNSCondition, 2, fUse, nCol, nStroke, dMin, dMax, fSwap );
	WRITE_ITEM_BOOL( fUse );
	WRITE_ITEM_INT( nCol );
	WRITE_ITEM_INT( nStroke );
	szItem.Format( _T("%f"), dMin );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	szItem.Format( _T("%f"), dMax );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	WRITE_ITEM_BOOL( fSwap );
	// event scripts
	for( int nPos = 0; nPos < nCnt; nPos++ )
	{
		pCondS->get_SoundScript( (SoundType)( nPos + 1 ), &nVal, &bstrItem );
		WRITE_ITEM_INT( nVal );
		szItem = bstrItem;
		WRITE_STRING( pFile, szItem );
		WRITE_STRING( pFile, _T("\n") );
	}
	pCondS->Release();

	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	// NOW THE STIMULI (if applicable)
	if( fStimuliAlso )
	{
		// warning stimulus
		Stimuluss stim;
		if( SUCCEEDED( stim.Find( szStimW ) ) )
			stim.Export( pFile, fFirst );
		// precue stimulus
		if( SUCCEEDED( stim.Find( szStimP ) ) )
			stim.Export( pFile, fFirst );
		// imperative stimulus
		if( SUCCEEDED( stim.Find( szStim ) ) )
			stim.Export( pFile, fFirst );
	}
	
	// NOW THE FEEDBACK (if applicable)
	if( fStimuliAlso )
	{
		NSConditions::GetFeedback( m_pNSCondition, szItem );

		// feedback object
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szItem ) ) )
			fb.Export( pFile, fFirst );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan )
{
	if( !m_pNSCondition || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	NSConditions comp;
	BOOL fFiles = FALSE, fComp = FALSE, fUse = FALSE, fSwap = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;
	short nCol = 0, nStroke = 0;
	double dMin = 0., dMax = 0.;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( szLine == HOLDER ) return TRUE;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Condition %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );
	// Instruction
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutInstruction( szLine );
	comp.PutInstruction( szLine );
	// Lex
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutLex( szLine );
	comp.PutLex( szLine );
	// Notes
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	szTemp = szLine;
	PutNotes( szTemp );
	comp.PutNotes( szTemp );
	// Range Dir
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	double dVal = atof( szLine );
	PutRangeDirection( dVal );
	comp.PutRangeDirection( dVal );
	// Range Len
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutRangeLength( dVal );
	comp.PutRangeLength( dVal );
	// Stroke Dir
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutStrokeDirection( dVal );
	comp.PutStrokeDirection( dVal );
	// Stroke Len
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutStrokeLength( dVal );
	comp.PutStrokeLength( dVal );
	// Stroke Max
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	short nVal = atoi( szLine );
	PutStrokeMax( nVal );
	comp.PutStrokeMax( nVal );
	// Stroke Min
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutStrokeMin( nVal );
	comp.PutStrokeMin( nVal );
	// Stroke Skip
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutStrokeSkip( nVal );
	comp.PutStrokeSkip( nVal );
	// Record
	BOOL fTmp;
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutRecord( fTmp );
	comp.PutRecord( fTmp );
	// Process
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutProcess( fTmp );
	comp.PutProcess( fTmp );
	// Parent
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutParent( szLine );
	comp.PutParent( szLine );
	// Word
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	PutWord( nVal );
	comp.PutWord( nVal );
	// Use Stimulus
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	if( !::IsOA() ) fTmp = FALSE;
	PutUseStimulus( fTmp );
	comp.PutUseStimulus( fTmp );
	// Warning Stimulus
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( !::IsOA() ) szLine = _T("");
	PutStimulusWarning( szLine );
	comp.PutStimulusWarning( szLine );
	// Warning Duration
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	if( !::IsOA() ) dVal = 0.;
	PutWarningDuration( dVal );
	comp.PutWarningDuration( dVal );
	// Warning Latency
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	if( !::IsOA() ) dVal = 0.;
	PutWarningLatency( dVal );
	comp.PutWarningLatency( dVal );
	// Precue Stimulus
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( !::IsOA() ) szLine = _T("");
	PutStimulusPrecue( szLine );
	comp.PutStimulusPrecue( szLine );
	// Precue Duration
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	if( !::IsOA() ) dVal = 0.;
	PutPrecueDuration( dVal );
	comp.PutPrecueDuration( dVal );
	// Precue Latency
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	if( !::IsOA() ) dVal = 0.;
	PutPrecueLatency( dVal );
	comp.PutPrecueLatency( dVal );
	// Imperative Stimulus
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	if( !::IsOA() ) szLine = _T("");
	PutStimulus( szLine );
	comp.PutStimulus( szLine );
	// Stop on wrong target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	if( !::IsOA() ) fTmp = FALSE;
	PutStopWrongTarget( fTmp );
	comp.PutStopWrongTarget( fTmp );
	// Record immediately
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutRecordImmediately( fTmp );
	comp.PutRecordImmediately( fTmp );
	// Magnet Force
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutMagnetForce( dVal );
	comp.PutMagnetForce( dVal );
	// Count strokes
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutCountStrokes( fTmp );
	comp.PutCountStrokes( fTmp );
	// Hide feedback
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutHideFeedback( fTmp );
	comp.PutHideFeedback( fTmp );
	// Hide Show After
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutHideShowAfter( fTmp );
	comp.PutHideShowAfter( fTmp );
	// Hide Show After Show
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutHideShowAfterShow( fTmp );
	comp.PutHideShowAfterShow( fTmp );
	// Hide Show After Strokes
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutHideShowAfterStrokes( fTmp );
	comp.PutHideShowAfterStrokes( fTmp );
	// Hide Show Count
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutHideShowCount( dVal );
	comp.PutHideShowCount( dVal );
	// Transform
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutTransform( fTmp );
	comp.PutTransform( fTmp );
	// Xgain
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutXgain( dVal );
	comp.PutXgain( dVal );
	// Ygain
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutYgain( dVal );
	comp.PutYgain( dVal );
	// Xrotation
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutXrotation( dVal );
	comp.PutXrotation( dVal );
	// Yrotation
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dVal = atof( szLine );
	PutYrotation( dVal );
	comp.PutYrotation( dVal );
	// Flag Bad Target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	PutFlagBadTarget( fTmp );
	comp.PutFlagBadTarget( fTmp );
	// Start on first target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	if( !::IsOA() ) fTmp = FALSE;
	PutStartFirstTarget( fTmp );
	comp.PutStartFirstTarget( fTmp );
	// Stop on last target
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fTmp = atoi( szLine );
	if( !::IsOA() ) fTmp = FALSE;
	PutStopLastTarget( fTmp );
	comp.PutStopLastTarget( fTmp );

	// sounds
	// get appropriate interface
	INSConditionSound* pCondS = NULL, *pCondS2 = NULL;
	HRESULT hr2 = m_pNSCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
	hr2 = comp.m_pNSCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS2 );
	if( FAILED( hr2 ) )
	{
		BCGPMessageBox( IDS_COND_ERR );
		return FALSE;
	}

	pCondS->get_SoundCount( &nVal );
	int nCnt = nVal;
	for( int nPos = 0; nPos < nCnt; nPos++ )
	{
		CString szTmp;

		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		fTmp = atoi( szLine );
		pCondS->put_SoundUse( (SoundType)( nPos + 1 ), fTmp );
		pCondS2->put_SoundUse( (SoundType)( nPos + 1 ), fTmp );
		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		fTmp = atoi( szLine );
		pCondS->put_SoundTone( (SoundType)( nPos + 1 ), fTmp );
		pCondS2->put_SoundTone( (SoundType)( nPos + 1 ), fTmp );
		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		nVal = atoi( szLine );
		pCondS->put_SoundToneFreq( (SoundType)( nPos + 1 ), nVal );
		pCondS2->put_SoundToneFreq( (SoundType)( nPos + 1 ), nVal );
		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		nVal = atoi( szLine );
		pCondS->put_SoundToneDur( (SoundType)( nPos + 1 ), nVal );
		pCondS2->put_SoundToneDur( (SoundType)( nPos + 1 ), nVal );
		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		szTmp = szLine;
		pCondS->put_SoundMedia( (SoundType)( nPos + 1 ), szTmp.AllocSysString() );
		pCondS2->put_SoundMedia( (SoundType)( nPos + 1 ), szTmp.AllocSysString() );
		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		fTmp = atoi( szLine );
		pCondS->put_SoundTrigger( (SoundType)( nPos + 1 ), fTmp );
		pCondS2->put_SoundTrigger( (SoundType)( nPos + 1 ), fTmp );
	}

	// Feedback
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutFeedback( szLine );
	comp.PutFeedback( szLine );
	// Gripper task
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nVal = atoi( szLine );
	if( !::IsGA() ) dVal = 0.;
	PutGripperTask( nVal );
	comp.PutGripperTask( nVal );
	// Visual feedback
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fUse = atoi( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nCol = atoi( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nStroke = atoi( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dMin = atof( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dMax = atof( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fSwap = atoi( szLine );
	SetFeedbackInfo( 1, fUse, nCol, nStroke, dMin, dMax, fSwap );
	comp.SetFeedbackInfo( 1, fUse, nCol, nStroke, dMin, dMax, fSwap );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fUse = atoi( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nCol = atoi( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	nStroke = atoi( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dMin = atof( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	dMax = atof( szLine );
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	fSwap = atoi( szLine );
	SetFeedbackInfo( 2, fUse, nCol, nStroke, dMin, dMax, fSwap );
	comp.SetFeedbackInfo( 2, fUse, nCol, nStroke, dMin, dMax, fSwap );

	// event scripts
	for( int nPos = 0; nPos < nCnt; nPos++ )
	{
		CString szTmp;

		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) goto DoImport;
		nVal = atoi( szLine );
		fRet = READ_STRING( pFile, szLine );
		if( !fRet )
		{
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		if( szLine == EXPORT_SEPARATOR ) {
			pCondS->Release();
			pCondS2->Release();
			return FALSE;
		}
		put_SoundScript( (SoundType)( nPos + 1 ), nVal, szLine );
		comp.put_SoundScript( (SoundType)( nPos + 1 ), nVal, szLine );
	}

DoImport:
	pCondS->Release();
	pCondS2->Release();
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_CONDITION, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		CFileFind ff;
		if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
		if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			comp.GetInstruction( szLine );
			PutInstruction( szLine );
			comp.GetLex( szLine );
			PutLex( szLine );
			comp.GetNotes( szLine );
			PutNotes( szLine );
			comp.GetRangeDirection( dVal );
			PutRangeDirection( dVal );
			comp.GetRangeLength( dVal );
			PutRangeLength( dVal );
			comp.GetStrokeDirection( dVal );
			PutStrokeDirection( dVal );
			comp.GetStrokeLength( dVal );
			PutStrokeLength( dVal );
			comp.GetStrokeMax( nVal );
			PutStrokeMax( nVal );
			comp.GetStrokeMin( nVal );
			PutStrokeMin( nVal );
			comp.GetStrokeSkip( nVal );
			PutStrokeSkip( nVal );
			comp.GetRecord( fTmp );
			PutRecord( fTmp );
			comp.GetProcess( fTmp );
			PutProcess( fTmp );
			comp.GetParent( szLine );
			PutParent( szLine );
			comp.GetWord( nVal );
			PutWord( nVal );
			comp.GetUseStimulus( fTmp );
			if( !::IsOA() ) fTmp = FALSE;
			PutUseStimulus( fTmp );
			comp.GetStimulusWarning( szLine );
			if( !::IsOA() ) szLine = _T("");
			PutStimulusWarning( szLine );
			comp.GetWarningDuration( dVal );
			PutWarningDuration( dVal );
			comp.GetWarningLatency( dVal );
			PutWarningLatency( dVal );
			comp.GetStimulusPrecue( szLine );
			if( !::IsOA() ) szLine = _T("");
			PutStimulusPrecue( szLine );
			comp.GetPrecueDuration( dVal );
			PutPrecueDuration( dVal );
			comp.GetPrecueLatency( dVal );
			PutPrecueLatency( dVal );
			comp.GetStimulus( szLine );
			if( !::IsOA() ) szLine = _T("");
			PutStimulus( szLine );
			comp.GetStopWrongTarget( fTmp );
			if( !::IsOA() ) fTmp = FALSE;
			PutStopWrongTarget( fTmp );
			comp.GetRecordImmediately( fTmp );
			PutRecordImmediately( fTmp );
			comp.GetMagnetForce( dVal );
			PutMagnetForce( dVal );
			comp.GetCountStrokes( fTmp );
			PutCountStrokes( fTmp );
			comp.GetHideFeedback( fTmp );
			PutHideFeedback( fTmp );
			comp.GetHideShowAfter( fTmp );
			PutHideShowAfter( fTmp );
			comp.GetHideShowAfterShow( fTmp );
			PutHideShowAfterShow( fTmp );
			comp.GetHideShowAfterStrokes( fTmp );
			PutHideShowAfterStrokes( fTmp );
			comp.GetHideShowCount( dVal );
			PutHideShowCount( dVal );
			comp.GetTransform( fTmp );
			PutTransform( fTmp );
			comp.GetXgain( dVal );
			PutXgain( dVal );
			comp.GetYgain( dVal );
			PutYgain( dVal );
			comp.GetXrotation( dVal );
			PutXrotation( dVal );
			comp.GetYrotation( dVal );
			PutYrotation( dVal );
			comp.GetFlagBadTarget( fTmp );
			PutFlagBadTarget( fTmp );
			comp.GetStartFirstTarget( fTmp );
			if( !::IsOA() ) fTmp = FALSE;
			PutStartFirstTarget( fTmp );
			comp.GetStopLastTarget( fTmp );
			if( !::IsOA() ) fTmp = FALSE;
			PutStopLastTarget( fTmp );
					
			// sounds
			// get appropriate interface
			hr2 = m_pNSCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
			hr2 = comp.m_pNSCondition->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS2 );
			pCondS2->get_SoundCount( &nVal );
			nCnt = nVal;
			for( int nPos = 0; nPos < nCnt; nPos++ )
			{
				BSTR bstr = NULL;
				pCondS2->get_SoundUse( (SoundType)( nPos + 1 ), &fTmp );
				pCondS->put_SoundUse( (SoundType)( nPos + 1 ), fTmp );
				pCondS2->get_SoundTone( (SoundType)( nPos + 1 ), &fTmp );
				pCondS->put_SoundTone( (SoundType)( nPos + 1 ), fTmp );
				pCondS2->get_SoundToneFreq( (SoundType)( nPos + 1 ), &nVal );
				pCondS->put_SoundToneFreq( (SoundType)( nPos + 1 ), nVal );
				pCondS2->get_SoundToneDur( (SoundType)( nPos + 1 ), &nVal );
				pCondS->put_SoundToneDur( (SoundType)( nPos + 1 ), nVal );
				pCondS2->get_SoundMedia( (SoundType)( nPos + 1 ), &bstr );
				pCondS->put_SoundMedia( (SoundType)( nPos + 1 ), bstr );
				pCondS2->get_SoundTrigger( (SoundType)( nPos + 1 ), &fTmp );
				pCondS->put_SoundTrigger( (SoundType)( nPos + 1 ), fTmp );
			}
			pCondS->Release();
			pCondS2->Release();

			// Feedback
			comp.GetFeedback( szLine );
			PutFeedback( szLine );
			// Gripper task
			comp.GetGripperTask( nVal );
			PutGripperTask( nVal );
			// visual feedback
			comp.GetFeedbackInfo( 1, fUse, nCol, nStroke, dMin, dMax, fSwap );
			SetFeedbackInfo( 1, fUse, nCol, nStroke, dMin, dMax, fSwap );
			comp.GetFeedbackInfo( 2, fUse, nCol, nStroke, dMin, dMax, fSwap );
			SetFeedbackInfo( 2, fUse, nCol, nStroke, dMin, dMax, fSwap );
			for( int nPos = 0; nPos < nCnt; nPos++ ) {
				comp.get_SoundScript( (SoundType)( nPos + 1 ), nVal, szLine );
				put_SoundScript( (SoundType)( nPos + 1 ), nVal, szLine );
			}

			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::ForceLocal()
{
	if( !m_pNSCondition ) return;

	HRESULT hr = m_pNSCondition->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::ForceRemote()
{
	if( !m_pNSCondition ) return;

	HRESULT hr = m_pNSCondition->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::ForceDefault()
{
	if( !m_pNSCondition ) return;

	HRESULT hr = m_pNSCondition->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath )
{
	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		NSConditions dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );
		
		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		// stimuli
		CString szStimW, szStimP, szStim;
		GetStimulusWarning( szStimW );
		GetStimulusPrecue( szStimP );
		GetStimulus( szStim );
		Stimuluss stim;
		if( SUCCEEDED( stim.Find( szStimW ) ) )
		{
			if( fToLocal ) stim.DoCopyAll( szStimW, szUserPath, fOverwrite, TRUE );
			else stim.DoUploadAll( szStimW, fOverwrite, TRUE );
		}
		if( SUCCEEDED( stim.Find( szStimP ) ) )
		{
			if( fToLocal ) stim.DoCopyAll( szStimP, szUserPath, fOverwrite, TRUE );
			else stim.DoUploadAll( szStimP, fOverwrite, TRUE );
		}
		if( SUCCEEDED( stim.Find( szStim ) ) )
		{
			if( fToLocal ) stim.DoCopyAll( szStim, szUserPath, fOverwrite, TRUE );
			else stim.DoUploadAll( szStim, fOverwrite, TRUE );
		}

		// feedback
		CString szFB;
		GetFeedback( szFB );
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			if( fToLocal ) fb.DoCopy( szFB, szUserPath, fOverwrite );
			else fb.DoUpload( szFB, fOverwrite );
		}

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::SetTemp( BOOL fVal )
{
	if( m_pNSCondition ) m_pNSCondition->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::DumpRecord( CString szID, CString szFile )
{
	if( !m_pNSCondition ) return FALSE;
	return SUCCEEDED( m_pNSCondition->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void NSConditions::RemoveTemp()
{
	if( m_pNSCondition ) m_pNSCondition->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

BOOL NSConditions::GetInstructionFile( CString szExpID, CString szCondID, CString& szFile, BOOL fNoExt )
{
	CString szCheck;

	// path
	::GetExpNoExt( szExpID, szCheck );

	// instruction file
	szCheck += szCondID;
	szFile = szCheck;
	szCheck += _T(".rtf");

	// does file exist?
	CFileFind ff;
	BOOL fFound = ff.FindFile( szCheck );

	// add extension if specified
	if( !fNoExt ) szFile = szCheck;

	return fFound;
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG NSConditions::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pNSCondition ) m_pNSCondition->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////

#define VALIDATE_ERR()	\
	if( fNotify ) BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR ); \
	pListErrors->AddTail( szMsg ); \
	if( fFirstOnly ) return FALSE;

#define VALIDATE_ERR_MSG(msg)	\
	szMsg = msg; \
	if( fNotify ) BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR ); \
	pListErrors->AddTail( szMsg ); \
	if( fFirstOnly ) return FALSE;

BOOL NSConditions::ValidateData( CStringList* pListErrors, BOOL fFirstOnly, BOOL fNotify )
{
	ASSERT( pListErrors != NULL );
	if( !pListErrors ) return FALSE;

	CString szItem, szMsg;
	BOOL fVal = FALSE, fRecord = FALSE, fProcess = FALSE;
	short nVal = 0, nVal1 = 0, nVal2 = 0;
	double dVal = 0.;

	NSConditions::GetID( m_pNSCondition, szItem );

	szMsg.Format( _T("DATA VALIDATION: Condition %s"), szItem );
	OutputMessage( szMsg, LOG_DB );

	if( szItem == _T("") )
	{
		VALIDATE_ERR_MSG( _T("The condition ID must have a value") );
	}		
	if( szItem.GetLength() < szIDS )
	{
		szMsg.Format( IDS_ID_LEN, szIDS );
		VALIDATE_ERR();
	}
	if( szItem == _T("CON") )
	{
		VALIDATE_ERR_MSG( _T("ID cannot be \'CON\'") );
	}
	if( szItem == _T("NUL") )
	{
		VALIDATE_ERR_MSG( _T("ID cannot be \'NUL\'") );
	}
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( szItem ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		VALIDATE_ERR();
	}

	NSConditions::GetDescription( m_pNSCondition, szItem );
	if( szItem == _T("") )
	{
		VALIDATE_ERR_MSG( _T("The condition description must have a value") );
	}
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( szItem ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					   szReserved, szDelim );
		VALIDATE_ERR();
	}
	// stroke description (lex)
	NSConditions::GetLex( m_pNSCondition, szItem );
	CString szLexAllow = LEX_ALLOW;
	for( int i = 0; i < szItem.GetLength(); i++ )
	{
		CString szC = szItem.GetAt( i );
		if( szLexAllow.Find( szC ) == -1 )
		{
			szMsg.Format( _T("Only the following characters are allowed in the stroke description: %s"), szLexAllow );
			VALIDATE_ERR();
		}
	}

	// Ranges
	NSConditions::GetStrokeMin( m_pNSCondition, nVal1 );
	if( ( nVal1 < 1 ) || ( nVal1 > 1000 ) )
	{
		VALIDATE_ERR_MSG( _T("Minimum strokes must be between 1 and 1000") );
	}
	NSConditions::GetStrokeMax( m_pNSCondition, nVal2 );
	if( ( nVal2 < 1 ) || ( nVal2 > 1000 ) )
	{
		VALIDATE_ERR_MSG( _T("Maximum strokes must be between 1 and 1000") );
	}
	if( nVal1 > nVal2 )
	{
		VALIDATE_ERR_MSG( _T("Maximum strokes summarized cannot be greater than maximum strokes acceptable") );
	}
	NSConditions::GetStrokeSkip( m_pNSCondition, nVal2 );
	if( ( nVal2 < 0 ) || ( nVal2 > 1000 ) )
	{
		VALIDATE_ERR_MSG( _T("Strokes to skip must be between 0 and 1000") );
	}
	// skip < min
	if( nVal2 >= nVal1 )
	{
		VALIDATE_ERR_MSG( _T("Strokes to skip must be less than Minimum strokes") );
	}
	NSConditions::GetStrokeDirection( m_pNSCondition, dVal );
	if( ( dVal < -6.2832 ) || ( dVal > 6.2832 ) )
	{
		VALIDATE_ERR_MSG( _T("Stroke direction must be between -6.2832 and 6.2832") );
	}
	NSConditions::GetRangeDirection( m_pNSCondition, dVal );
	if( ( dVal < -6.2832 ) || ( dVal > 6.2832 ) )
	{
		VALIDATE_ERR_MSG( _T("Range direction must be between -6.2832 and 6.2832") );
	}

	// Lengths
	NSConditions::GetStrokeLength( m_pNSCondition, dVal );
	if( dVal < 0. )
	{
		VALIDATE_ERR_MSG( _T("Stroke length cannot be negative") );
	}
	NSConditions::GetRangeLength( m_pNSCondition, dVal );
	if( dVal < 0. )
	{
		VALIDATE_ERR_MSG( _T("Range length cannot be negative") );
	}

	// STIMULI
	NSConditions::GetStimulusPrecue( m_pNSCondition, szItem );
	if( szItem != _T("") )
	{
		Stimuluss stim;
		if( FAILED( stim.Find( szItem ) ) )
		{
			VALIDATE_ERR_MSG( _T("A precue stimulus is specified but does not exist") );
		}
	}
	NSConditions::GetStimulusWarning( m_pNSCondition, szItem );
	if( szItem != _T("") )
	{
		Stimuluss stim;
		if( FAILED( stim.Find( szItem ) ) )
		{
			VALIDATE_ERR_MSG( _T("A warning stimulus is specified but does not exist") );
		}
	}
	NSConditions::GetStimulus( m_pNSCondition, szItem );
	if( szItem != _T("") )
	{
		Stimuluss stim;
		if( FAILED( stim.Find( szItem ) ) )
		{
			VALIDATE_ERR_MSG( _T("An imperative stimulus is specified but does not exist") );
		}
	}

	NSConditions::GetPrecueDuration( m_pNSCondition, dVal );
	if( ( dVal < 0 ) || ( dVal > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Precue stimulus duration must be between 0 and 100") );
	}
	NSConditions::GetPrecueLatency( m_pNSCondition, dVal );
	if( ( dVal < 0 ) || ( dVal > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Precue stimulus latency must be between 0 and 100") );
	}
	NSConditions::GetWarningDuration( m_pNSCondition, dVal );
	if( ( dVal < 0 ) || ( dVal > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Warning stimulus duration must be between 0 and 100") );
	}
	NSConditions::GetWarningLatency( m_pNSCondition, dVal );
	if( ( dVal < 0 ) || ( dVal > 100 ) )
	{
		VALIDATE_ERR_MSG( _T("Warning stimulus latency must be between 0 and 100") );
	}

	// FEEDBACK
	NSConditions::GetFeedback( m_pNSCondition, szItem );
	if( szItem != _T("") )
	{
		Feedbacks fb;
		if( FAILED( fb.Find( szItem ) ) )
		{
			VALIDATE_ERR_MSG( _T("A feedback is specified but does not exist") );
		}
	}

	// Word extraction
	NSConditions::GetRecord( m_pNSCondition, fRecord );
	NSConditions::GetProcess( m_pNSCondition, fProcess );
	NSConditions::GetWord( m_pNSCondition, nVal );
	NSConditions::GetParent( m_pNSCondition, szItem );
	if( !fRecord && !fProcess )
	{
		VALIDATE_ERR_MSG( _T("This condition must record, process or extract, or both. See word extraction settings") );
	}

	if( !fRecord && fProcess && ( ( nVal <= 0 ) || ( nVal > 500 ) ) )
	{
		VALIDATE_ERR_MSG( _T("The word extraction word must be between 1 and 500") );
	}

	if( !fRecord && fProcess && ( szItem == _T("") ) )
	{
		VALIDATE_ERR_MSG( _T("A word extraction parent condition must be specified") );
	}

	// VISUAL FEEDBACK
	short nCol = 0, nStroke1 = 0, nStroke2 = 0;
	double dMin1 = 0., dMin2 = 0., dMax1 = 0., dMax2 = 0.;
	BOOL fUse1 = FALSE, fUse2 = FALSE, fSwap = FALSE;
	NSConditions::GetFeedbackInfo( m_pNSCondition, 1, fUse1, nCol, nStroke1, dMin1, dMax1, fSwap );
	NSConditions::GetFeedbackInfo( m_pNSCondition, 2, fUse2, nCol, nStroke2, dMin2, dMax2, fSwap );
	// Using Feedback (if 2, must have 1)
	if( fUse2 && !fUse1 )
	{
		VALIDATE_ERR_MSG( _T("In order to use visual feedback 2, you must also use visual feedback 1") );
	}
	// stroke 1
	if( fUse1 && ( nStroke1 < 1 ) )
	{
		VALIDATE_ERR_MSG( _T("Visual feedback 1 segment must be at least 1") );
	}
	// stroke 2
	if( fUse2 && ( nStroke2 < 1 ) )
	{
		VALIDATE_ERR_MSG( _T("Visual feedback 2 segment must be at least 1") );
	}
	// Min & Max 1
	if( fUse1 && ( dMax1 <= dMin1 ) )
	{
		VALIDATE_ERR_MSG( _T("Visual feedback 1 maximum value must be greater than minimum value") );
	}
	// Min & Max 2
	if( fUse2 && ( dMax2 <= dMin2 ) )
	{
		VALIDATE_ERR_MSG( _T("Visual feedback 2 maximum value must be greater than minimum value") );
	}

	// GRIPPER magnet force
// 	NSConditions::GetMagnetForce( m_pNSCondition, dVal );
// 	if( ( dVal < 0.1 ) || ( dVal > 20.0 ) )
// 	{
// 		VALIDATE_ERR_MSG( _T("Gripper magnet force must be between 0.1 and 20.0. If this is not a grip-force experiment, it will have no impact.") );
// 	}

	return( pListErrors->GetCount() == 0 );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

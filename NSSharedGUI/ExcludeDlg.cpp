// ExcludeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ExcludeDlg.h"
#include "..\DataMod\DataMod.h"
#include "Subjects.h"
#include "..\ProcessMod\FeatureData.h"
#include "Experiments.h"
#include "Conditions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ExcludeDlg dialog

BEGIN_MESSAGE_MAP(ExcludeDlg, CBCGPDialog)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_SELECTALL, OnBnSelectall)
	ON_BN_CLICKED(IDC_BN_SELECTNONE, OnBnSelectnone)
	ON_BN_CLICKED(IDOK, OnOK2)
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

ExcludeDlg::ExcludeDlg( CString szExpID, CWnd* pParent, BOOL fHeaders,
						BOOL fQuests, BOOL fTrials, CString szGrpID, CString szSubjID )
	: CBCGPDialog(ExcludeDlg::IDD, pParent), m_szExpID( szExpID ),
	  m_fHeaders( fHeaders ), m_fQuests( fQuests ), m_fTrials( fTrials ),
	  m_szGrpID( szGrpID ), m_szSubjID( szSubjID )
{
	m_nType = EXP_TYPE_HANDWRITING;
	m_szDesc = _T("&Highlight those subjects that you do not want included in summarization.");
}

void ExcludeDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Text(pDX, IDC_TXT_DESC, m_szDesc);
}

/////////////////////////////////////////////////////////////////////////////
// ExcludeDlg message handlers

BOOL ExcludeDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( !m_fTrials ) m_tooltip.AddTool( pWnd, _T("Select items from list not to include in summarization."), _T("List") );
	else m_tooltip.AddTool( pWnd, _T("Select trials from list to include in the chart."), _T("List") );
	pWnd = GetDlgItem( IDOK );
	m_tooltip.AddTool( pWnd, _T("Accept selections and proceed."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	m_tooltip.AddTool( pWnd, _T("Cancel operation."), _T("Cancel") );

	m_lstExclude.RemoveAll();

	// adjust title/description if necessary
	if( m_fHeaders )
	{
		m_szDesc = _T("&Highlight those columns that you do not want included in summarization.");
		UpdateData( FALSE );
		SetWindowText( _T("Extracted Stroke Features To Exclude") );
	}
	else if( m_fQuests )
	{
		m_szDesc = _T("&Highlight those questions that you do not want included in summarization.");
		UpdateData( FALSE );
		SetWindowText( _T("Questions To Exclude") );
	}
	else if( m_fTrials )
	{
		m_szDesc = _T("&Highlight those trials that you want to include in the chart.");
		UpdateData( FALSE );
		SetWindowText( _T("Chart Selected Raw Data") );
	}

	// Fill list
	if( !m_fHeaders && !m_fQuests && !m_fTrials )
	{
		IExperimentMember* pEML = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									IID_IExperimentMember, (LPVOID*)&pEML );
		if( FAILED( hr ) ) return TRUE;
		Subjects subj;
		if( !subj.IsValid() ) return TRUE;

		BSTR bstrSubjID = NULL, bstrGrpID = NULL;
		CString szSubjID, szLast, szFirst, szSubj, szGrpID, szCode;
		hr = pEML->FindForExperiment( m_szExpID.AllocSysString() );
		CString szDelim;
		::GetDelimiter( szDelim );
		while( SUCCEEDED( hr ) )
		{
			pEML->get_SubjectID( &bstrSubjID );
			szSubjID = bstrSubjID;
			pEML->get_GroupID( &bstrGrpID );
			szGrpID = bstrGrpID;

			HRESULT hr2 = subj.Find( bstrSubjID );
			if( SUCCEEDED( hr2 ) )
			{
				subj.GetCode( szCode );
				subj.GetNameLast( szLast, ::IsPrivacyOn() );
				subj.GetNameFirst( szFirst,::IsPrivacyOn() );

				szSubj.Format( _T("Group %s - %s, %s (%s)%s%s"),
							szGrpID, szLast, szFirst, szCode, szDelim, szSubjID );
				m_List.AddString( szSubj );
			}

			hr = pEML->GetNext();
		}

		pEML->Release();
	}
	else if( m_fHeaders )
	{
		// determine if submovement analysis is on
		BOOL fSubM = FALSE;
		IProcSetting* pPS = NULL;
		HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
										 IID_IProcSetting, (LPVOID*)&pPS );
		if( FAILED( hr ) ) return TRUE;
		pPS->Find( m_szExpID.AllocSysString() );
		IProcSetFlags* pPSF = NULL;
		hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
		if( FAILED( hr ) )
		{
			pPS->Release();
			return TRUE;
		}
		pPSF->get_SEG_S( &fSubM );
		pPSF->Release();
		pPS->Release();

		CStdioFile fsrc;
		if( fsrc.Open( m_szExtFile, CFile::modeRead ) )
		{
			// get 1st line of headers
			CString szLine, szItem, szItem2;
			fsrc.ReadString( szLine );
			fsrc.Close();

			int nCur = 0;
			// now parse through and add to list (skip the 1st 2 trial & segment)
			int nPos = szLine.Find( _T(" ") );
			while( nPos >= 0 )
			{
				nCur++;
				szItem = szLine.Left( nPos );
				szLine = szLine.Mid( nPos + 1 );

				if( nCur > 2 )
				{
					// IF SUBMOVEMENT ANALYSIS,
					// ...look in the featuredata list to determine which components to add
					if( fSubM )
					{
						FeatureData* pData = FindFeatureData( szItem );
						if( pData )
						{
							if( strcmp( pData->szAlt, "" ) != 0 ) szItem = pData->szAlt;
							if( pData->fStroke ) m_List.AddString( szItem );
							if( pData->fStroke1 )
							{
								szItem2 = szItem;
								szItem2 += _T("1");
								m_List.AddString( szItem2 );
							}
							if( pData->fStroke2 )
							{
								szItem2 = szItem;
								szItem2 += _T("2");
								m_List.AddString( szItem2 );
							}
						}
						// else, if not found, just add the total
						else m_List.AddString( szItem );
					}
					// otherwise just add it
					else m_List.AddString( szItem );
				}

				nPos = szLine.Find( _T(" ") );
			}
		}
	}
	else if( m_fQuests )
	{
		IMQuestionnaire* pQ = NULL;
		HRESULT hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
									   IID_IMQuestionnaire, (LPVOID*)&pQ );
		if( FAILED( hr ) ) return TRUE;
		IGQuestionnaire* pGQ = NULL;
		hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
							   IID_IGQuestionnaire, (LPVOID*)&pGQ );
		if( FAILED( hr ) )
		{
			pQ->Release();
			return TRUE;
		}

		// get groups
		CStringList lstGrps;
		IExperimentMember* pEML = NULL;
		hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							   IID_IExperimentMember, (LPVOID*)&pEML );
		if( FAILED( hr ) )
		{
			pQ->Release();
			pGQ->Release();
			return TRUE;
		}
		BSTR bstrGrpID = NULL;
		CString szGrpID;
		hr = pEML->FindForExperiment( m_szExpID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			pEML->get_GroupID( &bstrGrpID );
			szGrpID = bstrGrpID;
			if( !lstGrps.Find( szGrpID ) ) lstGrps.AddTail( szGrpID );
			hr = pEML->GetNext();
		}
		pEML->Release();

		// loop through questions, check to see if included in group, and add if so
		BSTR bstr = NULL;
		CString szQID, szQ, szItem, szDelim;
		BOOL fHdr = FALSE;
		::GetDelimiter( szDelim );
		hr = pQ->ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			// ID
			pQ->get_ID( &bstr );
			szQID = bstr;
			pQ->get_Question( &bstr );
			szQ = bstr;
			pQ->get_IsHeader( &fHdr );

			// loop through groups if not header
			if( !fHdr )
			{
				HRESULT hr2 = S_OK;
				BOOL fFound = FALSE;
				POSITION pos = lstGrps.GetHeadPosition();
				while( pos && !fFound )
				{
					szGrpID = lstGrps.GetNext( pos );
					hr2 = pGQ->FindForGroup( m_szExpID.AllocSysString(), szGrpID.AllocSysString() );
					if( SUCCEEDED( hr2 ) )
					{
						fFound = TRUE;
						szItem.Format( _T("%s%s%s"), szQID, szDelim, szQ );
						m_List.AddString( szItem );
					}
				}
			}

			// next question
			hr = pQ->GetNext();
		}

		pQ->Release();
		pGQ->Release();
	}
	else if( m_fTrials )
	{
		// LOOP THROUGH ALL FILES AND ADD TO LIST
		CString szFile;
		::GetHWRMask( m_szExpID, m_szGrpID, m_szSubjID, szFile );
		CFileFind ff;
		BOOL fCont = ff.FindFile( szFile );
		while( fCont )
		{
			fCont = ff.FindNextFile();
			szFile = ff.GetFileName();
			m_List.AddString( szFile );
		}

		// SELECT ALL BY DEFAULT
		m_List.SelItemRange( TRUE, 0, m_List.GetCount() );
	}

	// memory file
	CString szPath, szFile, szExt;
	::GetDataPathRoot( szPath );
	szExt = m_fQuests ? _T("quest") : ( m_fHeaders ? _T("what") : _T("who") );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );
	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s-%s.SUM"), m_szExpID, m_szExpID, szExt );
	else
		szFile.Format( _T("%s\\%s\\%s-%s.SUM"), szPath, m_szExpID, m_szExpID, szExt );
	CStdioFile f;
	// read file and highlight those "memorized"
	if( f.Open( szFile, CFile::modeRead ) )
	{
		CString szItem;
		int nPos;
		while( f.ReadString( szItem ) )
		{
			nPos = m_List.FindString( -1, szItem );
			if( nPos != LB_ERR ) m_List.SetSel( nPos );
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ExcludeDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void ExcludeDlg::OnBnSelectall() 
{
	int nCnt = m_List.GetCount();
	if( nCnt <= 0 ) return;

	if( !m_fTrials ) m_List.SelItemRange( TRUE, 0, nCnt );
	else m_List.SelItemRange( FALSE, 0, nCnt );
}

void ExcludeDlg::OnBnSelectnone() 
{
	int nCnt = m_List.GetCount();
	if( nCnt <= 0 ) return;

	if( !m_fTrials ) m_List.SelItemRange( FALSE, 0, nCnt );
	else m_List.SelItemRange( TRUE, 0, nCnt );
}

void ExcludeDlg::OnOK()
{
	// memory file
	CString szPath, szFile, szExt;
	::GetDataPathRoot( szPath );
	szExt = m_fQuests ? _T("quest") : ( m_fHeaders ? _T("what") : _T("who") );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );
	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s-%s.SUM"), m_szExpID, m_szExpID, szExt );
	else
		szFile.Format( _T("%s\\%s\\%s-%s.SUM"), szPath, m_szExpID, m_szExpID, szExt );
	CStdioFile f;
	// don't open if trial list
	BOOL fOpen = m_fTrials ? FALSE : f.Open( szFile, CFile::modeWrite | CFile::modeCreate );

	// Any items selected ?
	int nSelected = m_List.GetSelCount();
	if( nSelected > 0 )
	{
		// Get list of selected items
		int pIdx[ 500 ], nPos = 0;
		nSelected = m_List.GetSelItems( nSelected, pIdx );

		// Trial list
		if( m_fTrials )
		{
			// GET CHRONO LIST OF TRIALS
			// Mask
			CString szMask;
			if( m_nType == EXP_TYPE_GRIPPER ) ::GetFRDMask( m_szExpID, m_szGrpID, m_szSubjID, szMask );
			else ::GetHWRMask( m_szExpID, m_szGrpID, m_szSubjID, szMask );
			// List of trials
			CStringList lstTrials;
			::SortFilesChrono( &lstTrials, szMask );

			// If the list of chronologically sorted files does not match the count
			// of items in the dialog list, we have a problem
			if( lstTrials.GetCount() != m_List.GetCount() )
			{
				BCGPMessageBox( _T("ERROR: Trial count mismatch.\nThe number of trials on the disk do not match those in the list.") );
				return;
			}

			// loop through the list of selected and find the trial name in the chrono list
			// add those to the returned string list object
			CString szTrial, szItem;
			POSITION pos = NULL;
			for( int i = 0; i < nSelected; i++ ) {
				m_List.GetText( pIdx[ i ], szItem );
				pos = lstTrials.GetHeadPosition();
				while( pos ) {
					szTrial = lstTrials.GetNext( pos );
					if( szTrial == szItem ) {
						m_lstExclude.AddTail( szTrial );
						break;
					}
				}
			}
		}
		// All else
		else
		{
			CString szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );

			// Add to list
			CString szGrp, szSubj, szItem;
			for( int i = 0; i < nSelected; i++ )
			{
				// get selected item from list
				m_List.GetText( pIdx[ i ], szItem );

				// write to memory file
				if( fOpen )
				{
					f.WriteString( szItem );
					f.WriteString( _T("\n") );
				}

				nPos = szItem.Find( _T("-") );
				szGrp = szItem.Left( nPos - 1 );
				szSubj = szItem.Mid( nPos + 2 );

				nPos = szGrp.Find( _T(" ") );
				szItem = szGrp.Mid( nPos + 1 );
				szItem += _T(" ");
				szItem += szDelim;
				szItem += _T(" ");

				nPos = szSubj.Find( szDelim );
				szItem += szSubj.Mid( nPos + 2 );

 				m_lstExclude.AddTail( szItem );
			}
		}
	}

	CBCGPDialog::OnOK();
}

void ExcludeDlg::OnOK2()
{
	OnOK();
}

BOOL ExcludeDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("summarizing.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

BEGIN_MESSAGE_MAP(ExcludeListBox, CListBox)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void ExcludeListBox::OnLButtonUp( UINT nFlags, CPoint point )
{
	m_fMouseDown = FALSE;
	CListBox::OnLButtonUp( nFlags, point );
}

void ExcludeListBox::OnLButtonDown( UINT nFlags, CPoint point )
{
	m_fMouseDown = TRUE;
	BOOL fOutside = TRUE;
	UINT nItem = ItemFromPoint( point, fOutside );
	if( !fOutside ) m_nLastItem = nItem;
	CListBox::OnLButtonDown( nFlags, point );
}

void ExcludeListBox::OnMouseMove( UINT nFlags, CPoint point )
{
	CListBox::OnMouseMove( nFlags, point );
	if( !m_fMouseDown ) return;

	BOOL fOutside = TRUE, fSelected = FALSE;
	UINT nItem = ItemFromPoint( point, fOutside );
	if( !fOutside && ( nItem >= 0 ) && ( m_nLastItem != nItem ) )
	{
		// direction
		UINT nDir = LIST_DIRECTION_NONE;
		if( nItem > m_nLastItem ) nDir = LIST_DIRECTION_DOWN;
		else if( nItem < m_nLastItem ) nDir = LIST_DIRECTION_UP;

		// change of direction? if so, then the last item needs to be addressed
		if( m_nDir != LIST_DIRECTION_NONE )
		{
			if( nDir != m_nDir )
			{
				UINT nPrevItem = nItem + ( ( nDir == LIST_DIRECTION_UP ) ? 1 : -1 );
				if( nPrevItem >= 0 )
				{
					fSelected = ( GetSel( nPrevItem ) > 0 );
					SetSel( nPrevItem, !fSelected );
				}
			}
		}

		// get previous state
		fSelected = ( GetSel( nItem ) > 0 );
		// set new state
		SetSel( nItem, !fSelected );

		m_nDir = nDir;
		m_nLastItem = nItem;
	}
}
// FlaggingDlg.h : header file
//

#pragma once

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// FlaggingDlg dialog

class AFX_EXT_CLASS FlaggingDlg : public CBCGPDialog
{
// Construction
public:
	FlaggingDlg(CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_FLAGGING };
	BOOL		m_fFlag;
	int			m_nY;
	double		m_dMin;
	double		m_dMax;
	int			m_nYPoint;
	int			m_nYSize;
	long		m_nYColor;
	CBCGPColorButton	m_bnColor;
	NSToolTipCtrl		m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnClickBnColor();
	afx_msg void OnChkFlag();
	afx_msg void OnSelchangeCboY();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// ElementPageView.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ElementPageView dialog

interface IElement;

class AFX_EXT_CLASS ElementPageView : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ElementPageView)

	// Construction
public:
	ElementPageView( IElement* = NULL );
	~ElementPageView();

	// Dialog Data
	enum { IDD = IDD_PAGE_ELMT_VIEW };
	CComboBox		m_cboMode;
	CString			m_szText;
	int				m_nLineSize;
	CBCGPColorButton	m_bnColorL;
	CBCGPColorButton	m_bnColorBG;
	CBCGPColorButton	m_bnColorT;
	int				m_nItem;
	CString			m_szFont;
	CString			m_szSize;
	int				m_nTextSize;
	IElement*		m_pE;
	NSToolTipCtrl	m_tooltip;
	CStringList		m_lstItems;
	short*			m_pLines;
	long*			m_pColorsL;
	CString			m_szText1, m_szText2, m_szText3;
	short*			m_pSizes;
	long*			m_pColorsT;
	int				m_nLastIdx;
	long*			m_pColorsBG;
	LOGFONT			m_lf1;
	LOGFONT			m_lf2;
	LOGFONT			m_lf3;

	// Overrides
public:
	virtual BOOL OnApply();
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL IsTarget();
	BOOL DoHelp( HELPINFO* );
protected:
	// message map functions
	afx_msg void OnClickBnColorl();
	afx_msg void OnClickBnColorbg();
	afx_msg void OnClickBnColort();
	afx_msg void OnSelchangeCboMode();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnFont();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()

};

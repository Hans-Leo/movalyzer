// CondSortDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "CondSortDlg.h"
#include "..\DataMod\DataMod.h"
#include "Conditions.h"
#include "Experiments.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	GRID_ID						99

/////////////////////////////////////////////////////////////////////////////
// CondSortDlg dialog

BEGIN_MESSAGE_MAP(CondSortDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_UP, OnClickBnUp)
	ON_BN_CLICKED(IDC_BN_DOWN, OnClickBnDown)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

class ConditionUse : public CObject
{
public:
	ConditionUse() : m_nCount( 0 ) {}
	~ConditionUse() {}
	
	CString	m_szID;
	int		m_nCount;

	static ConditionUse* FindCondition( CObList* pList, CString szID )
	{
		if( !pList ) return NULL;
		ConditionUse* pCU = NULL;
		POSITION pos = pList->GetHeadPosition();
		while( pos )
		{
			ConditionUse* p = (ConditionUse*)pList->GetNext( pos );
			if( p && ( p->m_szID == szID ) )
			{
				pCU = p;
				break;
			}
		}
		return pCU;
	}
};

CondSortDlg::CondSortDlg( IProcSetting* pPS, CWnd* pParent)
	: CBCGPDialog(CondSortDlg::IDD, pParent), m_pPS( pPS ), m_fMissing( FALSE )
{
	CString szExp, szID, szDesc, szItem;
	BSTR bstr = NULL;

	// get data from database
	if( m_pPS )
	{
		// Experiment ID
		m_pPS->get_ExperimentID( &bstr );
		szExp = bstr;

		int nSorted = 0, nTotal = 0;
		short nReps = 0;
		NSConditions cond;

		// Loop through experiment conditions + replications add to ConditionUse class
		// If not, warn and add to bottom of list
		CObList lstAll;
		ConditionUse* pCU = NULL;
		IExperimentCondition* pEC = NULL;
		HRESULT hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
										 IID_IExperimentCondition, (LPVOID*)&pEC );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_PS_ERR, MB_OK | MB_ICONERROR );
			return;
		}
		hr = pEC->FindForExperiment( szExp.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			pEC->get_ConditionID( &bstr );
			szID = bstr;
			pEC->get_Replications( &nReps );
			if( SUCCEEDED( cond.Find( szID ) ) )
			{
				pCU = ConditionUse::FindCondition( &lstAll, szID );
				if( !pCU )
				{
					pCU = new ConditionUse();
					pCU->m_szID = szID;
					pCU->m_nCount = nReps;
					lstAll.AddTail( pCU );
				}
				else pCU->m_nCount += nReps;
				nTotal += nReps;
			}
			hr = pEC->GetNext();
		}
		pEC->Release();

		// SEQUENCE
		m_pPS->get_ConditionSequence( &bstr );
		m_szSequence = bstr;
		if( m_szSequence != _T("") )
		{
			CString szSeq = m_szSequence;
			// loop through each condition ID, ensure it's there, get description, and add to list
			// Look in ConditionUse list (from DB) for ID and trial count
			// If not there, we do not add it. If exceeds trial count, do not add it.
			// If there and OK, decrement trial count from ConditionUse object.
			while( szSeq != _T("") )
			{
				szID = szSeq.Left( 3 );
				pCU = ConditionUse::FindCondition( &lstAll, szID );
				if( pCU )
				{
					pCU->m_nCount--;
					if( ( pCU->m_nCount >= 0 ) && SUCCEEDED( cond.Find( szID ) ) )
					{
						cond.GetDescription( szDesc );
						m_lstSortedID.AddTail( szID );
						m_lstSortedDesc.AddTail( szDesc );
						nSorted++;
					}
				}
				szSeq = szSeq.Mid( 3 );
			}
		}

		// Now loop through ConditionUse table that was originally filled from DB
		// and adjusted for sequence list to see if we have any remaining.
		// We are adding one per count.
		POSITION pos = lstAll.GetHeadPosition();
		while( pos )
		{
			pCU = (ConditionUse*)lstAll.GetNext( pos );
			if( pCU->m_nCount > 0 )
			{
				m_fMissing = TRUE;
				if( SUCCEEDED( cond.Find( pCU->m_szID ) ) )
				{
					cond.GetDescription( szDesc );
					for( int i = 0; i < pCU->m_nCount; i++ )
					{
						m_lstSortedID.AddTail( pCU->m_szID );
						m_lstSortedDesc.AddTail( szDesc );
					}
				}
			}
		}

		// Object list cleanup
		pos = lstAll.GetHeadPosition();
		while( pos )
		{
			pCU = (ConditionUse*)lstAll.GetNext( pos );
			if( pCU ) delete pCU;
		}
		lstAll.RemoveAll();
	}
}

CondSortDlg::~CondSortDlg()
{
}

void CondSortDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BN_UP, m_bnUp);
	DDX_Control(pDX, IDC_BN_DOWN, m_bnDown);
}

void CondSortDlg::InsertColumns()
{
	m_grid.InsertColumn( 0, _T("ID"), 40 );
	m_grid.InsertColumn( 1, _T("Description"), 250 );
	m_grid.AdjustLayout();
}

void CondSortDlg::FillList()
{
	if( m_lstSortedID.IsEmpty() ) return;
	m_grid.RemoveAll();

	POSITION posI = m_lstSortedID.GetHeadPosition();
	POSITION posD = m_lstSortedDesc.GetHeadPosition();
	CString szID, szDesc, szItem;
	while( posI )
	{
		szID = m_lstSortedID.GetNext( posI );
		szDesc = m_lstSortedDesc.GetNext( posD );

		CBCGPGridRow* pRow = m_grid.CreateRow( m_grid.GetColumnCount() );
		pRow->GetItem( 0 )->SetValue( szID.GetBuffer(), FALSE );
		pRow->GetItem( 1 )->SetValue( szDesc.GetBuffer(), FALSE );
		m_grid.AddRow( pRow, FALSE );
	}
	m_grid.AdjustLayout();
	m_grid.SetCurSel( 0 );
}

/////////////////////////////////////////////////////////////////////////////
// CondSortDlg message handlers

BOOL CondSortDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();

	// button images
	m_bnUp.SetImage( IDB_UP );
	m_bnDown.SetImage( IDB_DOWN );
	EnableVisualManagerStyle( TRUE, TRUE );

	// Create grid
	CRect rectGrid;
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	pWnd->GetWindowRect( rectGrid );
	ScreenToClient( rectGrid );
	DWORD dwViewStyle =	WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | LVS_REPORT;
	if( !m_grid.Create( dwViewStyle, rectGrid, this, GRID_ID ) )
	{
		TRACE0("Failed to create users grid\n");
		return TRUE;      // fail to create
	}
	// grid settings
	m_grid.EnableHeader( FALSE );
	m_grid.EnableColumnAutoSize();
	m_grid.SetWholeRowSel( TRUE );
	m_grid.SetSingleSel( TRUE );
	m_grid.EnableMultipleSort( FALSE );
	m_grid.EnableDragHeaderItems( FALSE );
	m_grid.EnableMarkSortedColumn( FALSE );
	m_grid.EnableGroupByBox( FALSE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	pWnd = GetDlgItem( GRID_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Current sequence of conditions."), _T("Sequence List") );
	pWnd = GetDlgItem( IDC_BN_UP );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEUP, _T("Move Up") );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEDOWN, _T("Move Down") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Accept changes."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Cancel changes."), _T("Cancel") );

	// Column Headers
	InsertColumns();

	// Fill List
	FillList();

	// If there were extra conditions/trials that were missing from before, inform
	if( ( m_szSequence != _T("") ) && m_fMissing )
		BCGPMessageBox( _T("Conditions have been changed since the last sequence was set.\nNew conditions or replications have been added to the end of the list."), MB_OK | MB_ICONINFORMATION );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CondSortDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void CondSortDlg::OnOK()
{
	// loop through list of conditions (from 0 to n rows) and add IDs to sequence variable for update
	if( m_grid.GetRowCount() != 0 )
	{
		m_szSequence = _T("");

		CString szID;
		CBCGPGridItem* pItem = NULL;
		CBCGPGridRow* pRow = NULL;
		int nCount = m_grid.GetRowCount();
		for( int i = 0; i < nCount; i++ )
		{
			pRow = m_grid.GetRow( i );
			pItem = pRow ? pRow->GetItem( 0 ) : NULL;
			if( pItem )
			{
				szID = pItem->GetValue();
				m_szSequence += szID;
			}
		}
	}

	m_pPS->put_ConditionSequence( m_szSequence.AllocSysString() );
	
	CBCGPDialog::OnOK();
}

void CondSortDlg::OnMoveItem( int nDir )
{
	if( ( nDir < -1 ) || ( nDir > 1 ) ) return;

	// if 1st item and move up OR last item and move down, return
	CBCGPGridRow* pRow = m_grid.GetCurSel();
	int nIdx = pRow->GetRowId();
	if( ( nDir < 0 ) && ( nIdx == 0 ) ) return;
	if( ( nDir > 0 ) && ( nIdx == ( m_grid.GetRowCount() - 1 ) ) ) return;

	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL, pos1D = NULL, pos2D = NULL;

	pos1 = m_lstSortedID.FindIndex( nIdx );
	pos2 = m_lstSortedID.FindIndex( nIdx + nDir );
	pos1D = m_lstSortedDesc.FindIndex( nIdx );
	pos2D = m_lstSortedDesc.FindIndex( nIdx + nDir );
	if( !pos1 || !pos2 || !pos1D || !pos2D ) return;
	szItem1 = m_lstSortedID.GetAt( pos1 );
	szItem2 = m_lstSortedID.GetAt( pos2 );
	m_lstSortedID.SetAt( pos2, szItem1 );
	m_lstSortedID.SetAt( pos1, szItem2 );
	szItem1 = m_lstSortedDesc.GetAt( pos1D );
	szItem2 = m_lstSortedDesc.GetAt( pos2D );
	m_lstSortedDesc.SetAt( pos2D, szItem1 );
	m_lstSortedDesc.SetAt( pos1D, szItem2 );

	FillList();

	pRow = m_grid.GetRow( nIdx + nDir );
	if( pRow )
	{
		m_grid.SetCurSel( pRow );
		m_grid.EnsureVisible( pRow );
	}
}

void CondSortDlg::OnClickBnUp() 
{
	OnMoveItem( -1 );
}

void CondSortDlg::OnClickBnDown() 
{
	OnMoveItem( 1 );
}

BOOL CondSortDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("condition_sequence.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

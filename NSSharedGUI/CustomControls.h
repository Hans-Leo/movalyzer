// CustomControls.h - Header file
///////////////////////////////////////////////////////////////////////////////

#pragma once

class CPrivateEdit : public CEdit
{
	// construction/destruction
public:
	CPrivateEdit();
	virtual ~CPrivateEdit();
	// member vars
protected:
	COLORREF	m_crText;		// text color
	COLORREF	m_crBackGnd;	// text background
	CBrush		m_brBackGnd;	// background brush
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
};

class CColoredButton : public CBCGPButton
{
	// construction/destruction
public:
	CColoredButton();
	virtual ~CColoredButton();
	// member vars
public:
	void SetColors( COLORREF crBackGndDef, COLORREF crBackGndNonDef );
	void SetDefaultAsOff( BOOL fVal );
	void SetDefaultRadio( int nVal );
protected:
	COLORREF	m_crBackGndDef;		// background color for default value
	COLORREF	m_crBackGndNonDef;	// background color for non-default value
	BOOL		m_fDefOff;			// whether default value is off (TRUE), FALSE if default is on
	int			m_nDefRdo;			// ID of default radio button
	afx_msg LRESULT OnSetCheck( WPARAM, LPARAM );
	afx_msg LRESULT OnGetCheck( WPARAM, LPARAM );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	virtual void DoDrawItem( CDC* pDCPaint, CRect rectClient, UINT itemState );
	DECLARE_MESSAGE_MAP()
};

class CColoredEdit : public CEdit
{
	// construction/destruction
public:
	CColoredEdit();
	virtual ~CColoredEdit();
	// member vars
public:
	void SetColors( COLORREF clrText, COLORREF clrBg );
	void SetDefaultValue( CString szVal );
	void SetDefaultValue( int nVal );
	void SetDefaultValue( short nVal );
	void SetDefaultValue( long lVal );
	void SetDefaultValue( double dVal );
protected:
	COLORREF	m_crText;		// text color
	COLORREF	m_crBackGnd;	// text background
	CBrush		m_brBackGnd;	// background brush
	CString		m_szDefVal;		// default value if string
	VARIANT		vt;				// contains def. val. type + value if not string
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
};

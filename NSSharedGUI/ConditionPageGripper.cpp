// ConditionPageGripper.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ConditionPageGripper.h"
#include "Conditions.h"
#include "ConditionSheet.h"

// ConditionPageGripper dialog

IMPLEMENT_DYNCREATE(ConditionPageGripper, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageGripper, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ConditionPageGripper::ConditionPageGripper( INSCondition* pC, BOOL fNew )
	: CBCGPPropertyPage(ConditionPageGripper::IDD), m_pC( pC )
{
	m_dMagnetForce = 8.;
	m_nTask = 0;

	if( !fNew && pC )
	{
		NSConditions::GetMagnetForce( pC, m_dMagnetForce );
		NSConditions::GetGripperTask( pC, m_nTask );
	}
}

ConditionPageGripper::~ConditionPageGripper()
{
}

void ConditionPageGripper::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MAGNETFORCE, m_dMagnetForce);
	DDX_Control(pDX, IDC_CBO_TYPE, m_cboTask);
}

// ConditionPageGripper message handlers

BOOL ConditionPageGripper::OnApply() 
{
	if( !m_pC ) return FALSE;

	UpdateData( TRUE );

	if( ( m_dMagnetForce < 0.1 ) || ( m_dMagnetForce > 20.0 ) )
	{
		((ConditionSheet*)GetParent())->SetActivePage( 6 );
		BCGPMessageBox( _T("Magnet force must be between 0.1 and 20.0.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MAGNETFORCE );
		pWnd->SetFocus();
		return FALSE;
	}

	m_pC->put_MagnetForce( m_dMagnetForce );
	int nIdx = m_cboTask.GetCurSel();
	if( nIdx != CB_ERR ) m_pC->put_GripperTask( nIdx );

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageGripper::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageGripper::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_MAGNETFORCE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Magnet force in Neutons." ), _T("Magnet Force") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_CBO_TYPE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the type of gripper task."), _T("Gripper Task") );

	// fill combo box (gripper task)
	if( ( m_nTask < 0 ) || ( m_nTask > 3 ) ) m_nTask = 0;
	m_cboTask.AddString( _T("<default>") );
	m_cboTask.AddString( _T("Standard Task") );
	m_cboTask.AddString( _T("Repetitive Pressure Task") );
	m_cboTask.AddString( _T("Maximum Pressure Task") );

	// select combo box item (gripper task)
	m_cboTask.SetCurSel( m_nTask );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ConditionPageGripper::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageGripper::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_gripper.html"), this );
	return TRUE;
}

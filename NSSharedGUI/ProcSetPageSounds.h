#pragma once

// ProcSetPageSounds.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageSounds dialog

interface INSCondition;

class AFX_EXT_CLASS ProcSetPageSounds : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageSounds)

// Construction
public:
	ProcSetPageSounds( INSCondition* pCond = NULL, int nEvent = -1 );
	~ProcSetPageSounds();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_SOUND };
	CComboBox		m_cbo;
	BOOL			m_fUse;
	int				m_nFreq;
	int				m_nDur;
	CString			m_szWave;
	CBCGPComboBox	m_cboWave;
	BOOL			m_fTrigger;
	BOOL			m_fTone;
	BOOL*			m_pUse;
	BOOL*			m_pTone;
	int*			m_pFreq;
	int*			m_pDur;
	int				m_nLastSel;
	CStringList		m_lstWave;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnPlay;
	CBCGPButton		m_bnBrowse;
	BOOL*			m_pTrigger;
	int				m_nSelEvent;
	int				m_nExtTypeRaw;
	CBCGPComboBox	m_cboETRaw;
	CString			m_szExtScriptRaw;
	CBCGPComboBox	m_cboESRaw;
	CBCGPButton		m_bnEditRaw;
	BOOL			m_fInformedMatlab;
	short*			m_pScriptType;
	CStringList		m_lstScripts;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void FillSoundList();
	void SetEnables();
	void FillScriptLists();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboSound();
	afx_msg void OnChkUse();
	afx_msg void OnRdoTones();
	afx_msg void OnRdoWave();
	afx_msg void OnBnBrowse();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickBnPlay();
	afx_msg void OnKillfocusEditWave();
	afx_msg void OnChangeType();
	afx_msg void OnClickEditRaw();
	DECLARE_MESSAGE_MAP()
};

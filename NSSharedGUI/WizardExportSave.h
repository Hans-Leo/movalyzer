#pragma once

#include "NSTooltipCtrl.h"

// WizardExportSave dialog

class AFX_EXT_CLASS WizardExportSave : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardExportSave)

public:
	WizardExportSave();
	virtual ~WizardExportSave();

// Dialog Data
	enum { IDD = IDD_WIZ_EXPORT_SAVE };
	CString			m_szRemote;
	CString			m_szPath;
	CString			m_szLogin;
	CString			m_szPassword;
	CString			m_szStatus;
	NSToolTipCtrl	m_tooltip;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
	BOOL Test( BOOL fInformOnSuccess );

protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedBnTest();

	DECLARE_MESSAGE_MAP()
};

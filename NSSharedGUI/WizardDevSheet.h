#pragma once

#define UM_ADJUSTBUTTONS  (WM_USER + 1002)

// WizardDevSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardDevSheet

class AFX_EXT_CLASS WizardDevSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardDevSheet)

// Construction
public:
	WizardDevSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	virtual ~WizardDevSheet();

// Attributes
public:
	int		m_nDevice;	// IDC_RDO_MOUSE, IDC_RDO_TABLET, IDC_RDO_GRIPPER
	int		m_nRate;
	int		m_nPressure;
	double	m_dResolution;
	double	m_dHeightTablet;
	double	m_dWidthTablet;
	double	m_dHeightDisplay;
	double	m_dWidthDisplay;
	BOOL	m_fRecWin;	// true for map to rec window, false for desktop

// Operations
public:
	void AddPages();

// Overrides

// message map functions
protected:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

// ScatterOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ScatterOptionsDlg.h"
#include "..\NSShared\iolib.h"
#include "AGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ScatterOptionsDlg dialog

BEGIN_MESSAGE_MAP(ScatterOptionsDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_CHK_EXCLUDEX, OnChkExcludeX)
	ON_BN_CLICKED(IDC_CHK_EXCLUDEY, OnChkExcludeY)
	ON_CBN_SELCHANGE(IDC_CBO_DISP, OnSelchangeCboDisp)
	ON_EN_KILLFOCUS(IDC_EDIT_STRENGTH, OnKillfocusEditStrength)
	ON_BN_CLICKED(IDC_CHK_DISPX, OnChkDispx)
	ON_BN_CLICKED(IDC_CHK_DISPY, OnChkDispy)
	ON_EN_KILLFOCUS(IDC_EDIT_STRENGTH2, OnKillfocusEditStrength2)
	ON_WM_HELPINFO()
	ON_CBN_SELCHANGE(IDC_CBO_DISP2, OnChange)
	ON_CBN_SELCHANGE(IDC_CBO_GRP, OnChange)
	ON_CBN_SELCHANGE(IDC_CBO_SUBJ, OnChange)
	ON_CBN_SELCHANGE(IDC_CBO_COND, OnChange)
	ON_CBN_SELCHANGE(IDC_CBO_TRIAL, OnChange)
	ON_CBN_SELCHANGE(IDC_CBO_STROKE, OnChange)
//	ON_CBN_SELCHANGE(IDC_CBO_SUBMVMT, OnChange)
END_MESSAGE_MAP()

ScatterOptionsDlg::ScatterOptionsDlg(CString szINC, BOOL fScatter, CWnd* pParent)
	: CBCGPDialog(ScatterOptionsDlg::IDD, pParent), m_fScatter( fScatter )
{
	m_fHasChanged = TRUE;
	m_nGrp = 0;
	m_nSubj = 0;
	m_nCond = 0;
	m_nTrial = 0;
	m_nStroke = 0;
//	m_nSubMvmt = 0;
	m_dStrength = 0.0;
	m_dStrength2 = 0.0;
	m_fDispX = FALSE;
	m_fDispY = FALSE;
	m_fExcludeX = FALSE;
	m_dExcludeValX = 0.0;
	m_fExcludeY = FALSE;
	m_dExcludeValY = 0.0;
	m_nDisp = 0;
	m_nDisp2 = 0;
	m_nGrp2 = 0;
	m_nSubj2 = 0;
	m_nCond2 = 0;
	m_nTrial2 = 0;
	m_nStroke2 = 0;
//	m_nSubMvmt2 = 0;
	m_pDisps = NULL;
	m_pDisps2 = NULL;

	// Filters
	int nPos = szINC.ReverseFind( '.' );
	m_szFile = szINC.Left( nPos );
	if( fScatter ) m_szFile += _T(".TMP");
	else m_szFile += _T(".XME");
}

ScatterOptionsDlg::~ScatterOptionsDlg()
{
	if( m_pDisps ) delete [] m_pDisps;
	if( m_pDisps2 ) delete [] m_pDisps2;
}

void ScatterOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_CBO_GRP, m_nGrp);
	DDX_CBIndex(pDX, IDC_CBO_SUBJ, m_nSubj);
	DDX_CBIndex(pDX, IDC_CBO_COND, m_nCond);
	DDX_CBIndex(pDX, IDC_CBO_TRIAL, m_nTrial);
	DDX_CBIndex(pDX, IDC_CBO_STROKE, m_nStroke);
//	DDX_CBIndex(pDX, IDC_CBO_SUBMVMT, m_nSubMvmt);
	DDX_CBIndex(pDX, IDC_CBO_DISP, m_nDisp);
	DDX_Text(pDX, IDC_EDIT_STRENGTH, m_dStrength);
	DDX_Check(pDX, IDC_CHK_EXCLUDEX, m_fExcludeX);
	DDX_Text(pDX, IDC_EDIT_EXCLUDEX, m_dExcludeValX);
	DDX_Check(pDX, IDC_CHK_EXCLUDEY, m_fExcludeY);
	DDX_Text(pDX, IDC_EDIT_EXCLUDEY, m_dExcludeValY);
	DDX_CBIndex(pDX, IDC_CBO_DISP2, m_nDisp2);
	DDX_Check(pDX, IDC_CHK_DISPX, m_fDispX);
	DDX_Check(pDX, IDC_CHK_DISPY, m_fDispY);
	DDX_Text(pDX, IDC_EDIT_STRENGTH2, m_dStrength2);
}

/////////////////////////////////////////////////////////////////////////////
// ScatterOptionsDlg message handlers

BOOL ScatterOptionsDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL ScatterOptionsDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	CWaitCursor crs;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_GRP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display data for the selected group.") );
	pWnd = GetDlgItem( IDC_CBO_SUBJ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display data for the selected subject.") );
	pWnd = GetDlgItem( IDC_CBO_COND );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display data for the selected condition.") );
	pWnd = GetDlgItem( IDC_CBO_TRIAL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display data for the selected trial.") );
	pWnd = GetDlgItem( IDC_CBO_STROKE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Display data for the selected stroke.") );
// 	pWnd = GetDlgItem( IDC_CBO_SUBMVMT );
// 	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Dispaly data for the selected sub-movement.") );
	pWnd = GetDlgItem( IDC_CBO_DISP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Parameter by which fixed-value indices affect display of x data.") );
	pWnd = GetDlgItem( IDC_EDIT_STRENGTH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Strength (distance) for separation of x data across x-axis.") );
	pWnd = GetDlgItem( IDC_CBO_DISP2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Parameter by which fixed-value indices affect display of y data.") );
	pWnd = GetDlgItem( IDC_EDIT_STRENGTH2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Strength (distance) for separation of y data across y-axis.") );
	pWnd = GetDlgItem( IDC_CHK_DISPX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Disperse across the x-axis using the selected parameter and strength.") );
	pWnd = GetDlgItem( IDC_CHK_DISPY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Disperse across the y-axis using the selected parameter and strength.") );
	pWnd = GetDlgItem( IDC_CHK_EXCLUDEX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Exclude from data set items meeting a x-value condition..") );
	pWnd = GetDlgItem( IDC_EDIT_EXCLUDEX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The x-value to exclude.") );
	pWnd = GetDlgItem( IDC_CHK_EXCLUDEY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Exclude from data set items meeting a y-value condition..") );
	pWnd = GetDlgItem( IDC_EDIT_EXCLUDEY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The y-value to exclude.") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// Add to combos
	POSITION pos = NULL;
	CString szItem, szTmp;
	CComboBox* pCBOG = (CComboBox*)GetDlgItem( IDC_CBO_GRP );
	CComboBox* pCBOS = (CComboBox*)GetDlgItem( IDC_CBO_SUBJ );
	CComboBox* pCBOC = (CComboBox*)GetDlgItem( IDC_CBO_COND );
	CComboBox* pCBOT = (CComboBox*)GetDlgItem( IDC_CBO_TRIAL );
	CComboBox* pCBOR = (CComboBox*)GetDlgItem( IDC_CBO_STROKE );
//	CComboBox* pCBOSM = (CComboBox*)GetDlgItem( IDC_CBO_SUBMVMT );
	if( !pCBOS || !pCBOC || !pCBOT || !pCBOR /*|| !pCBOSM*/ ) return TRUE;
	// Groups
	int nCur = -1;
	szTmp.Format( _T("%d"), m_nGrp2 );
	pos = m_lstGrp.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstGrp.GetNext( pos );
		pCBOG->AddString( szItem );
		nCur++;
		if( szItem == szTmp ) m_nGrp = nCur;
	}
	// Subjects
	nCur = -1;
	szTmp.Format( _T("%d"), m_nSubj2 );
	pos = m_lstSubj.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstSubj.GetNext( pos );
		pCBOS->AddString( szItem );
		nCur++;
		if( szItem == szTmp ) m_nSubj = nCur;
	}
	// Conditions
	nCur = -1;
	szTmp.Format( _T("%d"), m_nCond2 );
	pos = m_lstCond.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstCond.GetNext( pos );
		pCBOC->AddString( szItem );
		nCur++;
		if( szItem == szTmp ) m_nCond = nCur;
	}
	// Trials
	nCur = -1;
	szTmp.Format( _T("%d"), m_nTrial2 );
	pos = m_lstTrial.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstTrial.GetNext( pos );
		pCBOT->AddString( szItem );
		nCur++;
		if( szItem == szTmp ) m_nTrial = nCur;
	}
	// Strokes
	nCur = -1;
	szTmp.Format( _T("%d"), m_nStroke2 );
	pos = m_lstStroke.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstStroke.GetNext( pos );
		pCBOR->AddString( szItem );
		nCur++;
		if( szItem == szTmp ) m_nStroke = nCur;
	}
	// Submovements
// 	nCur = -1;
// 	szTmp.Format( _T("%d"), m_nSubMvmt2 );
// 	pos = m_lstSubMvmt.GetHeadPosition();
// 	while( pos )
// 	{
// 		szItem = m_lstSubMvmt.GetNext( pos );
// 		pCBOSM->AddString( szItem );
// 		nCur++;
// 		if( szItem == szTmp ) m_nSubMvmt = nCur;
// 	}
	// Select appropriate items in combos
	pCBOG->SetCurSel( m_nGrp );
	pCBOS->SetCurSel( m_nSubj );
	pCBOC->SetCurSel( m_nCond );
	pCBOT->SetCurSel( m_nTrial );
	pCBOR->SetCurSel( m_nStroke );
//	pCBOSM->SetCurSel( m_nSubMvmt );

	// DISPERSION PARAMETERS
	int nCount = 0;
	CComboBox* pCBOD = (CComboBox*)GetDlgItem( IDC_CBO_DISP );
	AGraphDlg* pGD = (AGraphDlg*)m_pParentWnd;
	if( pCBOD && pGD )
	{
		CString szItem;
		POSITION pos = pGD->m_lstAxis.GetHeadPosition();
		while( pos )
		{
			szItem = pGD->m_lstAxis.GetNext( pos );
			pCBOD->AddString( szItem );
			nCount++;
		}
		pCBOD->SetCurSel( m_nDisp );
	}
	pCBOD = (CComboBox*)GetDlgItem( IDC_CBO_DISP2 );
	if( pCBOD && pGD )
	{
		CString szItem;
		POSITION pos = pGD->m_lstAxis.GetHeadPosition();
		while( pos )
		{
			szItem = pGD->m_lstAxis.GetNext( pos );
			pCBOD->AddString( szItem );
		}
		pCBOD->SetCurSel( m_nDisp2 );
	}
	// Dispersion data holders
	if( !m_pDisps )
	{
		m_pDisps = new double[ nCount ];
		for( int i = 0; i < nCount; m_pDisps[ i++ ] = 0.0 );
	}
	if( !m_pDisps2 )
	{
		m_pDisps2 = new double[ nCount ];
		for( int i = 0; i < nCount; m_pDisps2[ i++ ] = 0.0 );
	}

	// disable submovement if necessary
//	if( !pGD->m_fsma ) pCBOSM->EnableWindow( FALSE );

	UpdateData( FALSE );
	OnChkExcludeX();
	OnChkExcludeY();
	OnChkDispx();
	OnChkDispy();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ScatterOptionsDlg::OnOK()
{
	UpdateData( TRUE );

	CComboBox* pCBOG = (CComboBox*)GetDlgItem( IDC_CBO_GRP );
	CComboBox* pCBOS = (CComboBox*)GetDlgItem( IDC_CBO_SUBJ );
	CComboBox* pCBOC = (CComboBox*)GetDlgItem( IDC_CBO_COND );
	CComboBox* pCBOT = (CComboBox*)GetDlgItem( IDC_CBO_TRIAL );
	CComboBox* pCBOR = (CComboBox*)GetDlgItem( IDC_CBO_STROKE );
//	CComboBox* pCBOSM = (CComboBox*)GetDlgItem( IDC_CBO_SUBMVMT );
	if( !pCBOS || !pCBOC || !pCBOT || !pCBOR /*|| !pCBOSM*/ ) return;
	
	CString szItem;
	int nItem;

	nItem = pCBOG->GetCurSel();
	if( nItem >= 0 ) pCBOG->GetLBText( nItem, szItem );
	else szItem = _T("0");
	m_nGrp2 = atoi( szItem );

	nItem = pCBOS->GetCurSel();
	if( nItem >= 0 ) pCBOS->GetLBText( nItem, szItem );
	else szItem = _T("0");
	m_nSubj2 = atoi( szItem );

	nItem = pCBOC->GetCurSel();
	if( nItem >= 0 ) pCBOC->GetLBText( nItem, szItem );
	else szItem = _T("0");
	m_nCond2 = atoi( szItem );

	nItem = pCBOT->GetCurSel();
	if( nItem >= 0 ) pCBOT->GetLBText( nItem, szItem );
	else szItem = _T("0");
	m_nTrial2 = atoi( szItem );

	nItem = pCBOR->GetCurSel();
	if( nItem >= 0 ) pCBOR->GetLBText( nItem, szItem );
	else szItem = _T("0");
	m_nStroke2 = atoi( szItem );

// 	nItem = pCBOSM->GetCurSel();
// 	if( nItem >= 0 ) pCBOSM->GetLBText( nItem, szItem );
// 	else szItem = _T("0");
// 	m_nSubMvmt2 = atoi( szItem );
//	m_nSubMvmt2 = 0;

	EndDialog( IDOK );
}

void ScatterOptionsDlg::OnChkExcludeX() 
{
	UpdateData( TRUE );
	m_fHasChanged = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_EXCLUDEX );
	if( pWnd ) pWnd->EnableWindow( m_fExcludeX );
}

void ScatterOptionsDlg::OnChkExcludeY() 
{
	UpdateData( TRUE );
	m_fHasChanged = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_EXCLUDEY );
	if( pWnd ) pWnd->EnableWindow( m_fExcludeY );
}

void ScatterOptionsDlg::OnSelchangeCboDisp() 
{
	if( !m_pDisps ) return;
	m_fHasChanged = TRUE;

	UpdateData( TRUE );
	m_dStrength = m_pDisps[ m_nDisp ];
	UpdateData( FALSE );
}

void ScatterOptionsDlg::OnKillfocusEditStrength() 
{
	if( !m_pDisps ) return;
	m_fHasChanged = TRUE;

//	CEdit* pEdit = (CEdit*)GetDlgItem( IDC_EDIT_STRENGTH );
//	CString szTxt;
//	pEdit->GetWindowText( szTxt );
//	if( szTxt == _T("") ) pEdit->SetWindowText( _T("0") );

	UpdateData( TRUE );
	m_pDisps[ m_nDisp ] = m_dStrength;
}

void ScatterOptionsDlg::OnKillfocusEditStrength2() 
{
	if( !m_pDisps2 ) return;
	m_fHasChanged = TRUE;

	UpdateData( TRUE );
	m_pDisps2[ m_nDisp2 ] = m_dStrength2;
}

void ScatterOptionsDlg::OnChkDispx() 
{
	UpdateData( TRUE );
	m_fHasChanged = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_CBO_DISP );
	if( pWnd ) pWnd->EnableWindow( m_fDispX );
	pWnd = GetDlgItem( IDC_EDIT_STRENGTH );
	if( pWnd ) pWnd->EnableWindow( m_fDispX );
}

void ScatterOptionsDlg::OnChkDispy() 
{
	UpdateData( TRUE );
	m_fHasChanged = TRUE;

	CWnd* pWnd = GetDlgItem( IDC_CBO_DISP2 );
	if( pWnd ) pWnd->EnableWindow( m_fDispY );
	pWnd = GetDlgItem( IDC_EDIT_STRENGTH2 );
	if( pWnd ) pWnd->EnableWindow( m_fDispY );
}

BOOL ScatterOptionsDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("analysischarts.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void ScatterOptionsDlg::OnChange()
{
	m_fHasChanged = TRUE;
}

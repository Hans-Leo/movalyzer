// ProcSetPageImg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetPageImg.h"
#include "ProcSetSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageImg property page

IMPLEMENT_DYNCREATE(ProcSetPageImg, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageImg, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
	ON_WM_HELPINFO()
	ON_CONTROL(STN_CLICKED, IDC_TXT_ADVANCED, OnClickAdvanced)
END_MESSAGE_MAP()

ProcSetPageImg::ProcSetPageImg( IProcSetting* pPS )
: CBCGPPropertyPage(ProcSetPageImg::IDD), m_pPS( pPS )
{
	m_nSmoothIter = 0;
	m_nInkThreshold = 0;
	m_nImgReso = 0;

	m_editSI.SetDefaultValue( m_nSmoothIter );
	m_editIT.SetDefaultValue( m_nInkThreshold );
	m_editIR.SetDefaultValue( m_nImgReso );

	if( m_pPS )
	{
		m_pPS->get_SmoothingIter( &m_nSmoothIter );
		m_pPS->get_InkThreshold( &m_nInkThreshold );
		m_pPS->get_ImgResolution( &m_nImgReso );
	}
}

ProcSetPageImg::~ProcSetPageImg()
{
}

void ProcSetPageImg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SMOOTH, m_nSmoothIter);
	DDX_Text(pDX, IDC_EDIT_TH, m_nInkThreshold);
	DDX_Text(pDX, IDC_EDIT_RESOLUTION, m_nImgReso);
	DDX_Control(pDX, IDC_TXT_ADVANCED, m_linkAdv);

	DDX_Control(pDX, IDC_EDIT_SMOOTH, m_editSI);
	DDX_Control(pDX, IDC_EDIT_TH, m_editIT);
	DDX_Control(pDX, IDC_EDIT_RESOLUTION, m_editIR);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageImg message handlers

BOOL ProcSetPageImg::OnApply() 
{
	if( !m_pPS ) return FALSE;

	if( ( m_nSmoothIter < 0 ) || ( m_nSmoothIter > 100 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 19 );
		BCGPMessageBox( _T("Smoothing iterations must be between 0 and 100."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_SMOOTH );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nInkThreshold < 0 ) || ( m_nInkThreshold > 255 ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 19 );
		BCGPMessageBox( _T("Graylevel separating ink and paper must be between 0 and 255."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_TH );
		pWnd->SetFocus();
		return FALSE;
	}
	// we want range of resolution to be 75 - 1200, but we also allow 0 for automatic
	if( ( m_nImgReso != 0 ) && ( ( m_nImgReso < 75 ) || ( m_nImgReso > 1200 ) ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 19 );
		BCGPMessageBox( _T("Image resolution must be 0 (automatic) or between 75 and 1200 dpi.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_RESOLUTION );
		if( pWnd ) pWnd->SetFocus();
		return false;
	}

	m_pPS->put_SmoothingIter( m_nSmoothIter );
	m_pPS->put_InkThreshold( m_nInkThreshold );
	m_pPS->put_ImgResolution( m_nImgReso );

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageImg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageImg::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// button images
	m_linkAdv.SetImage( IDB_DATA );
	m_linkAdv.SizeToContent();

	EnableVisualManagerStyle();

	// default values
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	short nSmoothIter = 0, nInkThreshold = 0, nImgReso = 0;
	double drvymin2 = 0., drvymin = 0., dtimemin = 0., dabsdistmin = 0., drdistmin = 0.;
	if( pPS )
	{
		m_pPS->get_SmoothingIter( &nSmoothIter );
		m_pPS->get_InkThreshold( &nInkThreshold );
		m_pPS->get_ImgResolution( &nImgReso );

		pPS->Release();
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CString szData, szTmp;

	CWnd* pWnd = GetDlgItem( IDC_EDIT_SMOOTH );
	szData = _T("#Smoothing iterations (0=100dpi)");
	szTmp.Format( _T(" [DEFAULT=%d]"), nSmoothIter );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Smoothing Iterations") );

	pWnd = GetDlgItem( IDC_EDIT_TH );
	szData = _T("Graylevel separating ink and paper (0=Automatic)");
	szTmp.Format( _T(" [DEFAULT=%d]"), nInkThreshold );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Gray Level") );

	pWnd = GetDlgItem( IDC_EDIT_RESOLUTION );
	szData = _T("Resolution (dpi) (0=Automatic)");
	szTmp.Format( _T(" [DEFAULT=%d]"), nImgReso );
	szData += szTmp;
	if( pWnd ) m_tooltip.AddTool( pWnd, szData, _T("Resolution") );

	pWnd = GetDlgItem( IDC_BN_RESET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Set the experiment defaults for this page."), _T("Reset") );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ProcSetPageImg::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageImg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_advanced_imageprocessing.html"), this );
	return TRUE;
}

void ProcSetPageImg::OnBnReset() 
{
	m_nSmoothIter = 0;
	m_nInkThreshold = 0;
	m_nImgReso = 0;

	UpdateData( FALSE );
}

void ProcSetPageImg::OnClickAdvanced()
{
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->SetActivePage( 15 );
}

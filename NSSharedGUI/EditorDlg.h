#pragma once

#include "AutoRichEditCtrl.h"
#include "NSTooltipCtrl.h"

// EditorDlg dialog
class AFX_EXT_CLASS EditorDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(EditorDlg)

public:
	EditorDlg(CWnd* pParent = NULL);
	virtual ~EditorDlg();

// Dialog Data
	enum { IDD = IDD_EDITOR };
	CString				m_szExpID;
	int					m_nType;
	CString				m_szFile;
	CString				m_szFileName;	// no extension (unless just displaying)
	CBCGPButton			m_bnOpen;
	CBCGPButton			m_bnPaste;
	int					m_nTop;
	int					m_nLeft;
	int					m_nRightBuffer;
	int					m_nBottomBuffer;
	int					m_nTopBuffer;
	int					m_nBnBuffer;
	int					m_nBnOpenLeft;
	int					m_nBnPasteLeft;
	BOOL				m_fNew;
protected:
	CAutoRichEditCtrl	m_re;
	NSToolTipCtrl		m_tooltip;

public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	virtual void OnCancel();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClickOpen();
	afx_msg void OnClickPaste();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
};

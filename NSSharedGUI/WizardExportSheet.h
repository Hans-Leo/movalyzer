#if !defined(AFX_WIZARDExportSHEET_H__20348494_ACFE_11D3_8A64_000000000000__INCLUDED_)
#define AFX_WIZARDExportSHEET_H__20348494_ACFE_11D3_8A64_000000000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WizardExportSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardExportSheet

class AFX_EXT_CLASS WizardExportSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardExportSheet)

// Construction
public:
	WizardExportSheet( CString szTitle, CWnd* pParentWnd = NULL, UINT iSelectPage = 0 )
		: CBCGPPropertySheet( szTitle, pParentWnd, iSelectPage ) {}
	WizardExportSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0,
					   CString szExpID = _T(""), CString szSubjID = _T(""), CString szGrpID = _T("") );
	virtual ~WizardExportSheet();

// Attributes
public:
	CString	m_szExpID;
	CString	m_szSubjID;
	CString	m_szGrpID;

// Operations
public:
	virtual void AddPages();

// Overrides

protected:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_WIZARDExportSHEET_H__20348494_ACFE_11D3_8A64_000000000000__INCLUDED_)

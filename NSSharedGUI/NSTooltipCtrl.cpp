// NSToolTipCtrl.cpp : implementation file
//

#include "StdAfx.h"
#include "NSToolTipCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

COLOR_SCHEME NSToolTipCtrl::m_ColorSchemes[] =
{
	_T("Beige"),             RGB(255,255,255), RGB(242,242,223), RGB(198,195,160), RGB(0,0,0),
	_T("Blue"),              RGB(255,255,255), RGB(202,220,246), RGB(150,180,222), RGB(0,0,0),
	_T("Blue 2"),            RGB(255,255,255), RGB(228,236,248), RGB(198,214,235), RGB(0,0,0),
	_T("Blue 3"),            RGB(255,255,255), RGB(213,233,243), RGB(151,195,216), RGB(0,0,0),
	_T("Blue 4"),            RGB(255,255,255), RGB(227,235,255), RGB(102,153,255), RGB(0,0,0),
	_T("Blue Glass"),        RGB(182,226,253), RGB(137,185,232), RGB(188,244,253), RGB(0,0,0),
	_T("Blue Glass 2"),      RGB(192,236,255), RGB(147,195,242), RGB(198,254,255), RGB(0,0,0),
	_T("Blue Glass 3"),      RGB(212,255,255), RGB(167,215,255), RGB(218,255,255), RGB(0,0,0),
	_T("Blue Inverted"),     RGB(117,160,222), RGB(167,210,240), RGB(233,243,255), RGB(0,0,0),
	_T("Blue Shift"),        RGB(124,178,190), RGB(13,122,153),  RGB(0,89,116),    RGB(255,255,255),
	_T("CodeProject"),       RGB(255,250,172), RGB(255,207,157), RGB(255,153,0),   RGB(0,0,0),
	_T("Dark Gray"),         RGB(195,195,195), RGB(168,168,168), RGB(134,134,134), RGB(255,255,255),
	_T("Deep Purple"),       RGB(131,128,164), RGB(112,110,143), RGB(90,88,117),   RGB(255,255,255),
	_T("Electric Blue"),     RGB(224,233,255), RGB(135,146,251), RGB(99,109,233),  RGB(0,0,0),
	_T("Firefox"),           RGB(255,254,207), RGB(254,248,125), RGB(225,119,24),  RGB(0,0,0),
	_T("Gold"),              RGB(255,202,0),   RGB(255,202,0),   RGB(255,202,0),   RGB(0,0,0),
	_T("Gold Shift"),        RGB(178,170,107), RGB(202,180,32),  RGB(162,139,1),   RGB(255,255,255),
	_T("Gray"),              RGB(255,255,255), RGB(228,228,228), RGB(194,194,194), RGB(0,0,0),
	_T("Green"),             RGB(234,241,223), RGB(211,224,180), RGB(182,200,150), RGB(0,0,0),
	_T("Green Shift"),       RGB(129,184,129), RGB(13,185,15),   RGB(1,125,1),     RGB(255,255,255),
	_T("Light Green"),       RGB(174,251,171), RGB(145,221,146), RGB(90,176,89),   RGB(0,0,0),
	_T("NASA Blue"),         RGB(0,91,134),    RGB(0,100,150),   RGB(0,105,160),   RGB(255,255,255),
	_T("Office 2007 Blue"),  RGB(255,255,255), RGB(242,246,251), RGB(202,218,239), RGB(76,76,76),
	_T("Orange Shift"),      RGB(179,120,80),  RGB(183,92,19),   RGB(157,73,1),    RGB(255,255,255),
	_T("Outlook Green"),     RGB(236,242,208), RGB(219,230,187), RGB(195,210,155), RGB(0,0,0),
	_T("Pale Green"),        RGB(249,255,248), RGB(206,246,209), RGB(148,225,155), RGB(0,0,0),
	_T("Pink Blush"),        RGB(255,254,255), RGB(255,231,242), RGB(255,213,233), RGB(0,0,0),
	_T("Pink Shift"),        RGB(202,135,188), RGB(186,8,158),   RGB(146,2,116),   RGB(255,255,255),
	_T("Pretty Pink"),       RGB(255,240,249), RGB(253,205,217), RGB(255,150,177), RGB(0,0,0),
	_T("Red"),               RGB(255,183,176), RGB(253,157,143), RGB(206,88,78),   RGB(0,0,0),
	_T("Red Shift"),         RGB(186,102,102), RGB(229,23,9),    RGB(182,11,1),    RGB(255,255,255),
	_T("Silver"),            RGB(255,255,255), RGB(242,242,246), RGB(212,212,224), RGB(0,0,0),
	_T("Silver 2"),          RGB(255,255,255), RGB(242,242,248), RGB(222,222,228), RGB(0,0,0),
	_T("Silver Glass"),      RGB(158,158,158), RGB(255,255,255), RGB(105,105,105), RGB(0,0,0),
	_T("Silver Inverted"),   RGB(161,160,186), RGB(199,201,213), RGB(255,255,255), RGB(0,0,0),
	_T("Silver Inverted 2"), RGB(181,180,206), RGB(219,221,233), RGB(255,255,255), RGB(0,0,0),
	_T("Soylent Green"),     RGB(134,211,131), RGB(105,181,106), RGB(50,136,49),   RGB(255,255,255),
	_T("Spring Green"),      RGB(154,231,151), RGB(125,201,126), RGB(70,156,69),   RGB(255,255,255),
	_T("Too Blue"),          RGB(255,255,255), RGB(225,235,244), RGB(188,209,226), RGB(0,0,0),
	_T("Totally Green"),     RGB(190,230,160), RGB(190,230,160), RGB(190,230,160), RGB(0,0,0),
	_T("XP Blue"),           RGB(119,185,236), RGB(81,144,223),  RGB(36,76,171),   RGB(255,255,255),
	_T("Yellow"),            RGB(255,255,220), RGB(255,231,161), RGB(254,218,108), RGB(0,0,0),
	NULL, 0, 0, 0, 0	// last entry must be NULL
};

NSToolTipCtrl::NSToolTipCtrl()
{
	m_nIcon = 0;
	m_fDoNotCaptureMouse = FALSE;
	SetIgnoreEnabledStatus( FALSE );
	SetColorBk( GetSysColor( COLOR_INFOBK ) );
	SetNotify();
	SetCallbackHyperlink( AfxGetApp()->m_pMainWnd->GetSafeHwnd(), UNM_HYPERLINK_CLICKED );
	EnableHyperlink( TRUE );
	SetDelayTime( PPTOOLTIP_TIME_INITIAL, 1500 );
	SetDelayTime( PPTOOLTIP_TIME_AUTOPOP, 1000 );
	SetDelayTime( PPTOOLTIP_TIME_FADEIN, 10 );
	SetDelayTime( PPTOOLTIP_TIME_FADEOUT, 10 );
}

void NSToolTipCtrl::SetThisIcon( UINT nIconResourceID )
{
//	m_nIcon = nIconResourceID;
}

CString NSToolTipCtrl::AddDefaultTool( SUPER_TOOLTIP_INFO* pTI )
{
	if( !pTI ) return _T("");

	pTI->bSuperTooltip		= TRUE;
	pTI->nVirtualKeyCode	= VK_F1;
	pTI->nSizeX				= XSUPERTOOLTIP_DEFAULT_WIDTH;
	pTI->nBehaviour			= PPTOOLTIP_DISABLE_AUTOPOP; // |
							  /*PPTOOLTIP_NOCLOSE_OVER |*/
							  /*PPTOOLTIP_CLOSE_LEAVEWND |*/
							  /*PPTOOLTIP_MULTIPLE_SHOW;*/
	pTI->bLineAfterHeader = FALSE;
	pTI->strFooter    = _T("<a msg=\"F1\">Press F1 for more help.</a>");
	pTI->bLineBeforeFooter = TRUE;
	pTI->rgbBegin	= m_ColorSchemes[ 41 ].rgbStart;
	pTI->rgbMid		= m_ColorSchemes[ 41 ].rgbMiddle;
	pTI->rgbEnd		= m_ColorSchemes[ 41 ].rgbEnd;
	pTI->rgbText	= m_ColorSchemes[ 41 ].rgbText;

	return CXSuperTooltip::AddTool( pTI );
}

CString NSToolTipCtrl::AddDefaultTool( CString szTitle, CString szMsg, UINT nID, CWnd* pWnd, UINT nImage )
{
	SUPER_TOOLTIP_INFO sti;
	if( nImage > 0 ) sti.nBodyImage = nImage;
	else sti.nBodyImage = m_nIcon;
	sti.strHeader = szTitle;
	sti.strBody = szMsg;
	sti.nIDTool  = nID;
	sti.pWnd = pWnd;

	return AddDefaultTool( &sti );
}

CString NSToolTipCtrl::AddTool( CWnd* pWnd, LPCTSTR lpszText, LPCTSTR lpszTitle, UINT nImage )
{
	return AddDefaultTool( lpszTitle, lpszText, 0, pWnd, nImage );
}

CString NSToolTipCtrl::AddTool(CWnd *pWnd, UINT nIDText, LPCTSTR lpszTitle, UINT nImage )
{
	CString szText;
	szText.LoadString( nIDText );
	return AddTool( pWnd, szText, lpszTitle, nImage );
}

CString NSToolTipCtrl::AddTool( CWnd *pWnd, UINT nIDText, UINT nIDTitle, UINT nImage )
{
	CString szText, szTitle;
	szText.LoadString( nIDText );
	szTitle.LoadString( nIDTitle );
	return AddTool( pWnd, szText, szTitle, nImage );
}

// NSSharedGUI.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ConvCalc.h"
#include <Mshtmhst.h>
#include "Objects.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CNSSharedGUIApp

BEGIN_MESSAGE_MAP(CNSSharedGUIApp, CWinApp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNSSharedGUIApp construction

CNSSharedGUIApp::CNSSharedGUIApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CNSSharedGUIApp object

CNSSharedGUIApp theApp;

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Application/workspace (for registry)
///////////////////////////////////////////////////////////////////////////////

CBCGPWorkspace* _pApp = NULL;
BOOL			_fIsRx = FALSE;

//************************************
// Method:    SetWorkspaceApp
// FullName:  SetWorkspaceApp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CBCGPWorkspace * pApp
//************************************
void SetWorkspaceApp( CBCGPWorkspace* pApp )
{
	_pApp = pApp;
}

//************************************
// Method:    GetWorkspaceApp
// FullName:  GetWorkspaceApp
// Access:    public 
// Returns:   CBCGPWorkspace*
// Qualifier:
//************************************
CBCGPWorkspace* GetWorkspaceApp()
{
	return _pApp;
}

//************************************
// Method:    SetIsAppRx
// FullName:  SetIsAppRx
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fVal
//************************************
void SetIsAppRx( BOOL fVal )
{
	_fIsRx = fVal;
}

//************************************
// Method:    IsAppRx
// FullName:  IsAppRx
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL IsAppRx()
{
	return _fIsRx;
}

///////////////////////////////////////////////////////////////////////////////
// Security
///////////////////////////////////////////////////////////////////////////////

NSSecurity* _pSecurity = NULL;

//************************************
// Method:    SetSecurity
// FullName:  SetSecurity
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NSSecurity * pSecurity
//************************************
void SetSecurity( NSSecurity* pSecurity )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_pSecurity = pSecurity;
}

//************************************
// Method:    GetSecurity
// FullName:  GetSecurity
// Access:    public 
// Returns:   NSSecurity*
// Qualifier:
//************************************
NSSecurity* GetSecurity()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _pSecurity;
}

// process module authorization
CString	_szAuthCodeProcessMod = _T("");

//************************************
// Method:    SetAuthorizationProcessMod
// FullName:  SetAuthorizationProcessMod
// Access:    public 
// Returns:   void AFX_EXT_API
// Qualifier:
// Parameter: CString szAuthCode
//************************************
void AFX_EXT_API SetAuthorizationProcessMod( CString szAuthCode )
{
	_szAuthCodeProcessMod = szAuthCode;
}

//************************************
// Method:    GetAuthorizationProcessMod
// FullName:  GetAuthorizationProcessMod
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetAuthorizationProcessMod()
{
	return _szAuthCodeProcessMod;
}

// input module authorization
CString	_szAuthCodeInputMod = _T("");

//************************************
// Method:    SetAuthorizationInputMod
// FullName:  SetAuthorizationInputMod
// Access:    public 
// Returns:   void AFX_EXT_API
// Qualifier:
// Parameter: CString szAuthCode
//************************************
void AFX_EXT_API SetAuthorizationInputMod( CString szAuthCode )
{
	_szAuthCodeInputMod = szAuthCode;
}

//************************************
// Method:    GetAuthorizationInputMod
// FullName:  GetAuthorizationInputMod
// Access:    public 
// Returns:   CString&
// Qualifier:
//************************************
CString& GetAuthorizationInputMod()
{
	return _szAuthCodeInputMod;
}

//************************************
// Method:    LoadAndDecode
// FullName:  LoadAndDecode
// Access:    public 
// Returns:   CString
// Qualifier:
// Parameter: UINT nID
//************************************
CString LoadAndDecode( UINT nID )
{
	CString szItem;
	szItem.LoadString( nID );
	return Objects::Decode( szItem, TRUE, ENCRYPTION_BLOWFISH );
}

///////////////////////////////////////////////////////////////////////////////
// Current User object
///////////////////////////////////////////////////////////////////////////////

UserObj* _CurUser = NULL;

//************************************
// Method:    SetCurrentUserObj
// FullName:  SetCurrentUserObj
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UserObj * pObj
//************************************
void SetCurrentUserObj( UserObj* pObj )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_CurUser = pObj;
}

//************************************
// Method:    GetCurrentUserObj
// FullName:  GetCurrentUserObj
// Access:    public 
// Returns:   UserObj*
// Qualifier:
//************************************
UserObj* GetCurrentUserObj()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _CurUser;
}

///////////////////////////////////////////////////////////////////////////////
// Window Manager
///////////////////////////////////////////////////////////////////////////////

WindowManager* _WM = NULL;

void SetWindowManager( WindowManager* pWM )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	_WM = pWM;
}

WindowManager* GetWindowManager()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return _WM;
}

///////////////////////////////////////////////////////////////////////////////
// Paths
///////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    GetHelpPath
// FullName:  GetHelpPath
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString & szPath
//************************************
void GetHelpPath( CString& szPath )
{
	::GetAppPath( szPath );
	szPath += _T("\\Help\\nshelp.chm::/");
}

///////////////////////////////////////////////////////////////////////////////
// help
///////////////////////////////////////////////////////////////////////////////

HWND _hwndHelp = NULL;

//************************************
// Method:    GoToHelp
// FullName:  GoToHelp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szLoc
// Parameter: CWnd * pOwner
//************************************
void GoToHelp( CString szLoc, CWnd* pOwner )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString szPath;
	::GetHelpPath( szPath );
	szPath += szLoc;

//	HWND hWnd = pOwner ? pOwner->m_hWnd : ::GetDesktopWindow();
	HWND hWnd = ::GetDesktopWindow();
	if( !_hwndHelp )
	{
		HH_WINTYPE wt;
		memset( &wt, 0, sizeof( HH_WINTYPE ) );
		wt.cbStruct = sizeof( HH_WINTYPE );
		wt.hwndCaller = hWnd;
		wt.fsValidMembers = HHWIN_PARAM_PROPERTIES;
		wt.fsWinProperties = HHWIN_PROP_TRACKING | HHWIN_PROP_ONTOP;
		_hwndHelp = ::HtmlHelp( hWnd, szPath, HH_SET_WIN_TYPE, (DWORD)&wt );
	}
	_hwndHelp = ::HtmlHelp( hWnd, szPath, HH_DISPLAY_TOPIC, NULL );

	SendMessage( _hwndHelp, WM_SETFOCUS, 0, 0 );
}

///////////////////////////////////////////////////////////////////////////////
// Calculator
///////////////////////////////////////////////////////////////////////////////

ConvCalc*		_convCalc = NULL;

//************************************
// Method:    ViewCalc
// FullName:  ViewCalc
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pWnd
//************************************
void ViewCalc( CWnd* pWnd )
{
	if( _convCalc )
	{
		delete _convCalc;
		_convCalc = NULL;
	}
	else _convCalc = new ConvCalc( pWnd );
}

//************************************
// Method:    CalcClosed
// FullName:  CalcClosed
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CalcClosed()
{
	if( _convCalc )
	{
		delete _convCalc;
		_convCalc = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////

double missing = -1.e6;
void SetMissingDataValue( double dVal ) { missing = dVal; }
double GetMissingDataValue() { return missing; }

///////////////////////////////////////////////////////////////////////////////
// custom popup window
///////////////////////////////////////////////////////////////////////////////
CBCGPPopupWindow* _pPopup = NULL;
class RxPopupWindow : public CBCGPPopupWindow
{
	virtual void OnClose();
	DECLARE_MESSAGE_MAP()
};
BEGIN_MESSAGE_MAP( RxPopupWindow, CBCGPPopupWindow )
	ON_WM_CLOSE()
END_MESSAGE_MAP()

//************************************
// Method:    OnClose
// FullName:  RxPopupWindow::OnClose
// Access:    virtual private 
// Returns:   void
// Qualifier:
//************************************
void RxPopupWindow::OnClose()
{
	CBCGPPopupWindow::OnClose();
	_pPopup = NULL;
}

//************************************
// Method:    ShowPopupWindow
// FullName:  ShowPopupWindow
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd* pParent
// Parameter: CString szMsg
// Parameter: int nXOrigin
// Parameter: int nYOrigin
// Parameter: BOOL fCenter
// Parameter: HMENU hMenu
// Parameter: CString szURL
// Parameter: UINT nURL
// Parameter: BYTE nTransparency
// Parameter: int nTimeToClose
//************************************
void ShowPopupWindow( CWnd* pParent, CString szMsg, int nXOrigin, int nYOrigin,
					  BOOL fCenter, HMENU hMenu, CString szURL, UINT nURL,
					  BYTE nTransparency, int nTimeToClose )
{
	AFX_MANAGE_STATE( AfxGetStaticModuleState() );
	_pPopup = new RxPopupWindow;
	_pPopup->SetAnimationType( CBCGPPopupMenu::FADE );
	_pPopup->SetAnimationSpeed( 30 );
	_pPopup->SetTransparency( nTransparency );
	_pPopup->SetSmallCaption( TRUE );
	_pPopup->SetAutoCloseTime( nTimeToClose );
	CBCGPPopupWndParams params;
	params.m_hIcon = theApp.LoadIcon( IDI_OPEN );
	params.m_strText = szMsg;
	if( szURL != _T("") )
	{
		params.m_strURL = szURL;
		params.m_nURLCmdID = nURL;
	}
	CPoint ptOrigin( nXOrigin, nYOrigin );
	// center in frame if specified
	if( fCenter )
	{
		CRect rect;
		CWnd* pDesktop = CWnd::GetDesktopWindow();
		if( pDesktop )
		{
			pDesktop->GetWindowRect( &rect );
			ptOrigin.x = ( rect.Width() / 2 ) - 100;
			ptOrigin.y = ( rect.Height() / 2 ) - 100;
		}
	}
	_pPopup->Create( pParent, params, hMenu, ptOrigin );
}

//************************************
// Method:    KillPopupWindow
// FullName:  KillPopupWindow
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void KillPopupWindow()
{
	AFX_MANAGE_STATE( AfxGetStaticModuleState() );
	if( _pPopup ) _pPopup->DestroyWindow();
	_pPopup = NULL;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

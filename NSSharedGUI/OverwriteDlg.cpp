// OverwriteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "OverwriteDlg.h"
#include "RelationshipDlg.h"

char szTypes[ 15 ][ 25 ] = { "UNKNOWN", "EXPERIMENT", "EXPERIMENT SETTINGS", "CONDITION", "GROUP", "SUBJECT",
							 "STIMULUS", "ELEMENT", "CATEGORY", "QUESTIONNAIRE", "GROUP QUESTIONNAIRE",
							 "SUBJECT QUESTIONNAIRE", "FEEDBACK", "STIMULS TARGET", "USER" };

UINT OverwriteDlg::m_nLastType = 0;
BOOL OverwriteDlg::m_fShowOnly = TRUE;

// OverwriteDlg dialog

IMPLEMENT_DYNAMIC( OverwriteDlg, CBCGPDialog )

BEGIN_MESSAGE_MAP( OverwriteDlg, CBCGPDialog )
	ON_BN_CLICKED(IDC_CHK_HIDE, OnBnClickedChkHide)
	ON_BN_CLICKED(IDC_BN_REL, OnBnClickedRel)
END_MESSAGE_MAP()


OverwriteDlg::OverwriteDlg( UINT nType, CString szID, CString szFile1, CString szFile2, CWnd* pParent )
	: CBCGPDialog( OverwriteDlg::IDD, pParent ), m_szType( _T("UNKONWN") ),
	  m_nResult( OW_UNKNOWN ), m_szFileOrig( szFile1 ), m_szFileNew( szFile2 ),
	  m_szItem( szID ), m_fAll( FALSE )
{
	m_fDiffNameFirst = FALSE;
	m_fDiffNameLast = FALSE;
	m_fDiffNotesPrv = FALSE;
	m_fDiffSig = FALSE;
	m_fDiffPass = FALSE;

	m_nType = nType;
	if( nType > OW_USER ) nType = OW_USER;
	m_szType = szTypes[ nType ];
	m_szType += _T(" (ID = ");
	m_szType += szID;
	m_szType += _T(")");
}

OverwriteDlg::~OverwriteDlg()
{
}

void OverwriteDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_TYPE, m_szType);
	DDX_Control(pDX, IDC_BN_REL, m_bnRel);
	DDX_Check(pDX, IDC_CHK_FINISH, m_fAll);
	DDX_Check(pDX, IDC_CHK_HIDE, m_fShowOnly);
	DDX_Control(pDX, IDC_LIST, m_list);
}

// OverwriteDlg message handlers

BOOL OverwriteDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// relationship button image
	m_bnRel.SetImage( IDB_REL, IDB_REL, IDB_REL );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetTitle( _T("Item Exists") );
	CWnd* pWnd = GetDlgItem( IDC_RDO_OW_NO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this to maintain the original values of this item. Do NOT overwrite the original."), _T("Keep Original") );
	pWnd = GetDlgItem( IDC_RDO_OW_YES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this to overwrite the original values of this item with the new imported values."), _T("Overwrite Original") );
	pWnd = GetDlgItem( IDC_CHK_FINISH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this to either keep or overwrite (above) the original data for the remaining conflicts during this import."), _T("Do For All") );
	pWnd = GetDlgItem( IDC_BN_REL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View the item's relationships."), _T("Relationships") );
	pWnd = GetDlgItem( IDC_CHK_HIDE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to show/hide all data where there is no difference between the original and the newly imported item."), _T("Show Different") );
	pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The list of data fields for the conflicting item."), _T("Details") );

	// default option is NO
	CheckRadioButton( IDC_RDO_OW_NO, IDC_RDO_OW_YES, IDC_RDO_OW_NO );

	// if no files specified (or dont exist), disable details button
	CFileFind ff;
	if( ( m_szFileOrig == _T("") ) || ( m_szFileNew == _T("") ) ||
		!ff.FindFile( m_szFileOrig ) || !ff.FindFile( m_szFileNew ) )
	{
		CWnd* pWnd = GetDlgItem( IDC_BN_DETAILS );
		pWnd->EnableWindow( FALSE );
	}

	// if a different section (condition, group, etc.) than last, beep
	if( m_nType != OverwriteDlg::m_nLastType ) Beep( 800, 200 );
	OverwriteDlg::m_nLastType = m_nType;

	// COLUMN HEADERS
	// Column Headers
	m_list.InsertColumn( 0, _T("Field"), LVCFMT_LEFT, 100, 0 );
	m_list.InsertColumn( 1, _T("Original"), LVCFMT_LEFT, 100, 1 );
	m_list.InsertColumn( 2, _T("To Import"), LVCFMT_LEFT, 100, 2 );

	// Image list
	if( !m_ImageList.Create( IDB_TREE, 18, 0, RGB( 0, 255, 0 ) ) )
		ASSERT( FALSE );
	m_list.SetImageList( &m_ImageList, LVSIL_SMALL );

	// Fill list
	FillFileCompareList();

	// disable relationship button for those w/o support
	switch( m_nType )
	{
		case OW_UNKNOWN:
		case OW_EXPERIMENT:
		case OW_SETTINGS:
		case OW_QUEST:
		case OW_GQUEST:
		case OW_SQUEST:
		case OW_STIMTARG:
		case OW_CAT:
		case OW_USER:
		{
			pWnd = GetDlgItem( IDC_BN_REL );
			if( pWnd ) pWnd->EnableWindow( FALSE );
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL OverwriteDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent( pMsg );
	return CBCGPDialog::PreTranslateMessage( pMsg );
}

void OverwriteDlg::OnOK()
{
	UpdateData( TRUE );

	int nRes = GetCheckedRadioButton( IDC_RDO_OW_NO, IDC_RDO_OW_YES );
	switch( nRes )
	{
		case IDC_RDO_OW_YES:		m_nResult = OW_YES;		break;
		case IDC_RDO_OW_NO:			m_nResult = OW_NO;		break;
	}
	if( m_fAll )
	{
		if( m_nResult == OW_YES ) m_nResult = OW_YESALL;
		else if( m_nResult == OW_NO ) m_nResult = OW_NOALL;
	}

	CBCGPDialog::OnOK();
}

void OverwriteDlg::OnCancel()
{
	m_nResult = OW_CANCEL;
	CBCGPDialog::OnCancel();
}

void OverwriteDlg::FillFileCompareList()
{
	m_list.DeleteAllItems();

	// open files
	CStdioFile fOrig, fNew;
	if( !fOrig.Open( m_szFileOrig, CFile::modeRead ) ) return;
	if( !fNew.Open( m_szFileNew, CFile::modeRead ) ) return;
	CString szLineOrig, szLineNew, szName, szValOrig, szValNew;
	BOOL fContOrig = TRUE, fContNew = TRUE, fFlag = FALSE, fHide = FALSE,
		fDidID = FALSE, fIsID = FALSE;
	int nPos = 0, nItem = 0, nImage = -1;

	// type
	UINT nType = m_nType;

	// loop through each line, extract name & values, compare and add to list
	while( fContOrig || fContNew )
	{
		fIsID = FALSE;
		fFlag = FALSE;
		fHide = FALSE;
		szName = szValOrig = szValNew = _T("");

		if( fContOrig && !fOrig.ReadString( szLineOrig ) ) fContOrig = FALSE;
		if( fContNew && !fNew.ReadString( szLineNew ) ) fContNew = FALSE;

		// get name & original val
		if( fContOrig )
		{
			nPos = szLineOrig.Find( _T(" : ") );
			if( nPos != -1 )
			{
				szName = szLineOrig.Left( nPos );
				szValOrig = szLineOrig.Mid( nPos + 3 );
				// if a subject && a private field, hide val, but flag
				if( ( nType == OW_SUBJECT ) &&
					( ( szName == _T("LastName") ) || ( szName == _T("FirstName") ) ||
					( szName == _T("PrvNotes") ) || ( szName == _T("Signature") ) ||
					( szName == _T("Password") ) ) )
				{
					szValOrig = _T("<Encrypted>");
					fHide = TRUE;
				}
				else if( ( nType == OW_GQUEST ) && ( szName == _T("ItemNum") ) )
				{
					fFlag = FALSE;
					fHide = TRUE;
				}
				if( !fDidID && szName.Find( _T("ID") ) != -1 )
				{
					fFlag = TRUE;
					fDidID = TRUE;
					fIsID = TRUE;
				}
			}
			else fFlag = TRUE;
		}
		else fFlag = TRUE;
		// get name (if necessary) & new val
		if( fContNew )
		{
			nPos = szLineNew.Find( _T(" : ") );
			if( nPos != -1 )
			{
				if( szName == _T("") ) szName = szLineNew.Left( nPos );
				szValNew = szLineNew.Mid( nPos + 3 );
				// if a subject && a private field, hide val, but flag
				if( ( nType == OW_SUBJECT ) &&
					( ( szName == _T("LastName") ) || ( szName == _T("FirstName") ) ||
					( szName == _T("PrvNotes") ) || ( szName == _T("Signature") ) ||
					( szName == _T("Password") ) ) )
				{
					szValNew = _T("<Encrypted>");
					fHide = TRUE;
				}
				else if( ( nType == OW_GQUEST ) && ( szName == _T("ItemNum") ) )
				{
					fFlag = FALSE;
					fHide = TRUE;
				}
			}
			else fFlag = TRUE;
		}
		else fFlag = TRUE;
		// compare
		if( fHide )
		{
			if( ( ( szName == _T("LastName") ) && m_fDiffNameLast ) ||
				( ( szName == _T("FirstName") ) && m_fDiffNameFirst ) ||
				( ( szName == _T("PrvNotes") ) && m_fDiffNotesPrv ) ||
				( ( szName == _T("Signature") ) && m_fDiffSig ) ||
				( ( szName == _T("Password") ) && m_fDiffPass ) )
				fFlag = TRUE;
		}
		else if( szValOrig != szValNew ) fFlag = TRUE;
		// add to list
		if( ( fContOrig || fContNew ) && ( fFlag || ( !fFlag && !m_fShowOnly ) ) )
		{
			nImage = -1;
			if( fFlag && !fIsID ) nImage = 6;

			nItem = m_list.InsertItem( m_list.GetItemCount(), szName, nImage );
			m_list.SetItem( nItem, 1, LVIF_TEXT, szValOrig, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 2, LVIF_TEXT, szValNew, 0, 0, 0, NULL );
		}
	}
}

void OverwriteDlg::OnBnClickedChkHide()
{
	UpdateData( TRUE );
	FillFileCompareList();
}

void OverwriteDlg::OnBnClickedRel()
{
	RelationshipDlg d( this );
	d.m_szItem = m_szItem;
	switch( m_nType )
	{
		case OW_GROUP:		d.m_nType = REL_GROUP;		break;
		case OW_SUBJECT:	d.m_nType = REL_SUBJECT;	break;
		case OW_CONDITION:	d.m_nType = REL_CONDITION;	break;
		case OW_STIMULUS:	d.m_nType = REL_STIMULUS;	break;
		case OW_ELEMENT:	d.m_nType = REL_ELEMENT;	break;
		case OW_CAT:		d.m_nType = REL_CAT;		break;
		case OW_FEEDBACK:	d.m_nType = REL_FEEDBACK;	break;
		default: return;
	}
	d.DoModal();
}

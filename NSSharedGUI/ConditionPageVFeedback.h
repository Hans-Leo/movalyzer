#pragma once

#include "NSTooltipCtrl.h"

// ConditionPageVFeedback dialog

interface INSCondition;

class ConditionPageVFeedback : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(ConditionPageVFeedback)

public:
	ConditionPageVFeedback( INSCondition* = NULL, BOOL fNew = TRUE );
	virtual ~ConditionPageVFeedback();

// Dialog Data
	enum { IDD = IDD_PAGE_COND_VFEEDBACK };
	BOOL			m_fNew;
	BOOL			m_fUse1;
	BOOL			m_fUse2;
	CComboBox		m_cboFeature1;
	CComboBox		m_cboFeature2;
	int				m_nFeature1;
	int				m_nFeature2;
	int				m_nStroke1;
	int				m_nStroke2;
	double			m_dMin1;
	double			m_dMin2;
	double			m_dMax1;
	double			m_dMax2;
	BOOL			m_fSwap1;
	BOOL			m_fSwap2;
	INSCondition*	m_pC;
	NSToolTipCtrl	m_tooltip;

public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void FillFeatures();
	void EnableDisable();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedChkFb1();
	afx_msg void OnBnClickedChkFb2();
	DECLARE_MESSAGE_MAP()
};

#pragma once

// PrefPageInput.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// PrefPageInput dialog

class Preferences;

class AFX_EXT_CLASS PrefPageInput : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(PrefPageInput)

// Construction
public:
	PrefPageInput( Preferences* pPref = NULL );
	~PrefPageInput();

// Dialog Data
	enum { IDD = IDD_PAGE_PREF_INPUT };
	Preferences*	m_pPref;
	NSToolTipCtrl	m_tooltip;
	CComboBox		m_cboCom;
	CString			m_szCom;
	BOOL			m_fOnlyAvail;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void FillCommPorts();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnClickedCheckOnlyavail();
	DECLARE_MESSAGE_MAP()
};

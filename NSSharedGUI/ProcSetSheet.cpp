// ProcSetSheet.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageProp.h"
#include "ProcSetPageGeneral.h"
#include "ProcSetPageRETrial.h"
#include "ProcSetPageREChart.h"
#include "ProcSetPageRESumm.h"
#include "ProcSetPageREView.h"
#include "ProcSetPageREExt.h"
#include "ProcSetPageTF2.h"
#include "ProcSetPageS2W.h"
#include "ProcSetPageSum.h"
#include "DrawOptionsDlg.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"
#include "ProcSetPageInput.h"
#include "ProcSetPageTF.h"
#include "ProcSetPageSeg.h"
#include "ProcSetPageSeg2.h"
#include "ProcSetPageExt.h"
#include "ProcSetPageExt2.h"
#include "ProcSetPageCon.h"
#include "ProcSetPageCon2.h"
#include "ProcSetPageImg.h"
#include "ProcSetPageREView.h"
#include "ProcSetPageNormDB.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcSetSheet

IMPLEMENT_DYNAMIC(ProcSetSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(ProcSetSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ProcSetSheet::ProcSetSheet( UINT nIDCaption, CWnd* pParentWnd, BOOL fGripper,
						    UINT iSelectPage )
	:CBCGPPropertySheet( nIDCaption, pParentWnd, iSelectPage )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_fGripper = fGripper;
	m_nType = 0;
	m_dMDV = -1.e6;
	AddPages();
}

ProcSetSheet::ProcSetSheet( LPCTSTR pszCaption, CWnd* pParentWnd, BOOL fGripper,
						    UINT iSelectPage )
	:CBCGPPropertySheet( pszCaption, pParentWnd, iSelectPage )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_fGripper = fGripper;
	m_nType = 0;
	m_dMDV = -1.e6;
	AddPages();
}

ProcSetSheet::ProcSetSheet( IProcSetting* pPS, CWnd* pParentWnd, BOOL fGripper,
						    BOOL fNew, UINT iSelectPage, BOOL fDup )
	: CBCGPPropertySheet( IDS_PPS, pParentWnd, iSelectPage )
{
	SetLook( CBCGPPropertySheet::PropSheetLook_Tree, 175 /* Tree control width */ );
	SetIconsList( IDB_PROCSET, 16 /* Image width */ );

	m_nType = 0;
	m_fNew = fNew;
	m_fDup = fDup;
	m_fGripper = fGripper;
	m_dMDV = -1.e6;
	AddPages( pPS );
}

ProcSetSheet::~ProcSetSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void ProcSetSheet::AddPages( IProcSetting* pPS )
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	CBCGPPropSheetCategory* pCatProp = AddTreeCategory( _T("General"), 0, 1 );
	AddPageToTree( pCatProp, new ProcSetPageProp( m_fNew, m_fDup ), -1, 2 );

	CBCGPPropSheetCategory* pCatD = AddTreeCategory( _T("Input Device"), 0, 1 );
	AddPageToTree( pCatD, new ProcSetPageInput( m_fNew, pPS ), -1, 2 );

	CBCGPPropSheetCategory* pCatRE = AddTreeCategory( _T("Running Experiment"), 0, 1 );
	AddPageToTree( pCatRE, new ProcSetPageRETrial( pPS ), -1, 2 );
	AddPageToTree( pCatRE, new ProcSetPageGeneral( pPS ), -1, 2 );
	AddPageToTree( pCatRE, new DrawOptionsDlg( pPS ), -1, 2 );
	AddPageToTree( pCatRE, new ProcSetPageREChart( pPS ), -1, 2 );
	AddPageToTree( pCatRE, new ProcSetPageRESumm( pPS ), -1, 2 );
	AddPageToTree( pCatRE, new ProcSetPageREView( pPS ), -1, 2 );

	CBCGPPropSheetCategory* pCatP = AddTreeCategory( _T("Processing"), 0, 1 );
	AddPageToTree( pCatP, new ProcSetPageTF( pPS ), -1, 2 );
	AddPageToTree( pCatP, new ProcSetPageSeg( pPS ), -1, 2 );
	AddPageToTree( pCatP, new ProcSetPageExt( pPS ), -1, 2 );
	AddPageToTree( pCatP, new ProcSetPageCon( pPS ), -1, 2 );
	AddPageToTree( pCatP, new ProcSetPageREExt( pPS ), -1, 2 );

	CBCGPPropSheetCategory* pCatS = AddTreeCategory( _T("Summarization"), 0, 1 );
	AddPageToTree( pCatS, new ProcSetPageSum( pPS ), -1, 2 );

	CBCGPPropSheetCategory* pCatDB = AddTreeCategory( _T("Norm DB"), 0, 1 );
	AddPageToTree( pCatDB, new ProcSetPageNormDB( pPS ), -1, 2 );

	CBCGPPropSheetCategory* pCatA = AddTreeCategory( _T("Advanced"), 0, 1 );
	AddPageToTree( pCatA, new ProcSetPageTF2( pPS ), -1, 2 );
	AddPageToTree( pCatA, new ProcSetPageSeg2( pPS ), -1, 2 );
	AddPageToTree( pCatA, new ProcSetPageExt2( pPS ), -1, 2 );
	AddPageToTree( pCatA, new ProcSetPageCon2( pPS ), -1, 2 );
	AddPageToTree( pCatA, new ProcSetPageS2W( pPS ), -1, 2 );
	AddPageToTree( pCatA, new ProcSetPageImg( pPS ), -1, 2 );

	m_fModified = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetSheet message handlers

BOOL ProcSetSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageProp ) ) )
			return ((ProcSetPageProp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageGeneral ) ) )
			return ((ProcSetPageGeneral*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageRETrial ) ) )
			return ((ProcSetPageRETrial*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageREChart ) ) )
			return ((ProcSetPageREChart*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageRESumm ) ) )
			return ((ProcSetPageRESumm*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageREView ) ) )
			return ((ProcSetPageREView*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageTF ) ) )
			return ((ProcSetPageTF*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageTF2 ) ) )
			return ((ProcSetPageTF2*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageSeg ) ) )
			return ((ProcSetPageSeg*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageSeg2 ) ) )
			return ((ProcSetPageSeg2*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageS2W ) ) )
			return ((ProcSetPageS2W*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageInput ) ) )
			return ((ProcSetPageInput*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageExt ) ) )
			return ((ProcSetPageExt*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageExt2 ) ) )
			return ((ProcSetPageExt2*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageCon ) ) )
			return ((ProcSetPageCon*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageCon2 ) ) )
			return ((ProcSetPageCon2*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageSum ) ) )
			return ((ProcSetPageSum*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( DrawOptionsDlg ) ) )
			return ((DrawOptionsDlg*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageImg ) ) )
			return ((ProcSetPageImg*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( ProcSetPageREExt ) ) )
			return ((ProcSetPageREExt*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

#ifndef _OBJECTS_H_
#define	_OBJECTS_H_

#include "..\DataMod\DataMod.h"

#define	EXPORT_SEPARATOR	_T("***DO+++NOT+++REMOVE***")

// This class is intended to encapsulate all view & functionality for system objects
class AFX_EXT_CLASS Objects
{
	// Construction/destruction
public:
	Objects() : m_hrCreate( E_FAIL ), m_fPassed( FALSE ) {}
	virtual ~Objects() {}

	BOOL IsValid();

protected:
	virtual void Init() {}

	// Operations
public:
	// Create (static)
	static HRESULT CreateObject( Objects*, LPVOID* );	// caller is responsible for release
	virtual ULONGLONG GetNumRecords();
	// Upload/download to/from client-server/local
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE ) { return FALSE; }
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE ) { return FALSE; }
	virtual BOOL DoCopyAll( CString szID, CString szPath, BOOL fOverwrite = FALSE, BOOL fRecurse = FALSE ) { return FALSE; }
	virtual BOOL DoUploadAll( CString szID, BOOL fOverwrite = FALSE, BOOL fRecurse = FALSE ) { return FALSE; }
	// Data validation
	virtual BOOL ValidateData( CStringList* pListErrors, BOOL fFirstOnly = FALSE, BOOL fNotify = FALSE ) { return TRUE; }
protected:
	virtual REFCLSID GetCLSID() = 0;
	virtual REFIID GetREFIID() = 0;
public:
	// encryption/decryption (uses current encryption technology)
	static CString& Encode( CString szSource );
	// not specifying nMethod will use the default
	static CString& Decode( CString szSource, BOOL fEncrypted, short nMethod );
protected:
	static CString& DESEncode( CString szSource );
	static CString& DESDecode( CString szSource, BOOL fEncrypted );
	static CString& AESEncode( CString szSource );
	static CString& AESDecode( CString szSource );
	static CString& BFEncode( CString szSource );
	static CString& BFDecode( CString szSource );
	static CString& EncDecrypt( CString szSource,
								CString szMode, /* rijndael, blowfish, etc */
								BOOL fEncode /* false for decode */ );

	// Attributes
protected:
	HRESULT		m_hrCreate;
	BOOL		m_fPassed;
};

template <typename theinterface> class Object : public Objects
{
public:
	// construction
	Object( CLSID clsid, IID iid ) : Objects(), m_clsid( clsid ), m_iid( iid )
	{
        m_hrCreate = Objects::CreateObject( (Objects*)this, (LPVOID*)&m_pInterface );
	}
	Object( theinterface* pInterface ) : Objects()
	{
		m_pInterface = pInterface;
		if( m_pInterface )
		{
			m_clsid = pInterface->m_clsid;
			m_iid = pInterface->m_iid;
			m_fPassed = TRUE;
			m_hrCreate = S_OK;
		}
		else
		{
			m_fPassed = FALSE;
			m_hrCreate = E_FAIL;
		}
	}
	// destruction
	~Object()
	{
		if( !m_fPassed && m_pInterface ) m_pInterface->Release();
	}

// methods
public:
	HRESULT ResetToStart();
	static HRESULT ResetToStart( theinterface* );
	HRESULT GetNext();
	static HRESULT GetNext( theinterface* );
	HRESULT Find( CString szID );
	static HRESULT Find( theinterface*, CString szID );
	HRESULT Add();
	static HRESULT Add( theinterface* );
	HRESULT Modify();
	static HRESULT Modify( theinterface* );
	HRESULT Remove();
	static HRESULT Remove( theinterface* );
	void SetDataPath( CString szPath );
	static void SetDataPath( theinterface*, CString szPath );
	void SetBackupPath( CString szPath );
	static void SetBackupPath( theinterface*, CString szPath );
	BOOL DoAdd( CWnd* pOwner = NULL );
	static BOOL DoAdd( theinterface*, CWnd* pOwner = NULL );
	BOOL DoEdit( CString szID, CWnd* pOwner = NULL );
	static BOOL DoEdit( theinterface*, CString szID, CWnd* pOwner = NULL );
protected:
	virtual REFCLSID GetCLSID() { return m_clsid; }
	virtual REFIID GetREFIID() { return m_iid; }

// members
private:
	theinterface*		m_pInterface;
	CLSID				m_clsid;
	IID					m_iid;
};

//TODO: Make this into a template
#define	PUT_GET( theinterface, theitem, thetype, theput ) \
	void Put##theitem( thetype val ) { if( m_p##theinterface ) m_p##theinterface->put_##theitem( theput ); } \
	void Get##theitem( thetype& val ) { theinterface##s::Get##theitem( m_p##theinterface, val ); } \
	static void Get##theitem( I##theinterface* i, thetype& val );

#define	PUT_ONLY( theinterface, theitem, thetype, theput ) \
	void Put##theitem( thetype val ) { if( m_p##theinterface ) m_p##theinterface->put_##theitem( theput ); }

#define	GET_ONLY( theinterface, theitem, thetype, theput ) \
	void Get##theitem( thetype& val ) { theinterface##s::Get##theitem( m_p##theinterface, val ); } \
	static void Get##theitem( I##theinterface* i, thetype& val );

#define PUT_BASE( theinterface ) \
	public: \
	theinterface##s() { m_hrCreate = theinterface##s::CreateObject( (Objects*)this, (LPVOID*)&m_p##theinterface ); Init(); } \
	theinterface##s( I##theinterface* p##theinterface ) { m_p##theinterface = p##theinterface; m_fPassed = TRUE; m_hrCreate = S_OK; Init(); } \
	~theinterface##s() { if( !m_fPassed && m_p##theinterface ) m_p##theinterface->Release(); } \
	public: \
	HRESULT ResetToStart(); \
	static HRESULT ResetToStart( I##theinterface* ); \
	HRESULT GetNext(); \
	static HRESULT GetNext( I##theinterface* ); \
	HRESULT Find( CString szID ); \
	static HRESULT Find( I##theinterface*, CString szID ); \
	HRESULT Add(); \
	static HRESULT Add( I##theinterface* ); \
	HRESULT Modify(); \
	static HRESULT Modify( I##theinterface* ); \
	HRESULT Remove(); \
	static HRESULT Remove( I##theinterface* ); \
	void SetDataPath( CString szPath ); \
	static void SetDataPath( I##theinterface*, CString szPath ); \
	void SetBackupPath( CString szPath ); \
	static void SetBackupPath( I##theinterface*, CString szPath ); \
	BOOL DoAdd( CWnd* pOwner = NULL ); \
	static BOOL DoAdd( I##theinterface*, CWnd* pOwner = NULL ); \
	BOOL DoEdit( CString szID, CWnd* pOwner = NULL ); \
	static BOOL DoEdit( I##theinterface*, CString szID, CWnd* pOwner = NULL ); \
	protected: \
	virtual REFCLSID GetCLSID() { return CLSID_##theinterface; } \
	virtual REFIID GetREFIID() { return IID_I##theinterface; } \
	public: \
	I##theinterface*	m_p##theinterface;

#endif	// _OBJECTS_H_

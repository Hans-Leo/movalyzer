// GroupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "RelationshipDlg.h"
#include "NotesDlg.h"
#include "GroupDlg.h"
#include "..\CTreeLink\DBCommon.h"
#include "Groups.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GroupDlg dialog

BEGIN_MESSAGE_MAP(GroupDlg, CBCGPDialog)
	ON_EN_KILLFOCUS(IDC_EDIT_ABBR, OnKillfocusEditId)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_NOTES, OnClickNotes)
END_MESSAGE_MAP()

GroupDlg::GroupDlg(CWnd* pParent, BOOL fNew)
	: CBCGPDialog(GroupDlg::IDD, pParent), m_fNew( fNew )
{
	m_fInit = TRUE;
	m_szAbbr = _T("");
	m_szDesc = _T("");
	m_szNotes = _T("");
}

void GroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ABBR, m_szAbbr);
	DDV_MaxChars(pDX, m_szAbbr, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_Text(pDX, IDC_EDIT_NOTES, m_szNotes);
	DDV_MaxChars(pDX, m_szNotes, szNOTES);
}

/////////////////////////////////////////////////////////////////////////////
// GroupDlg message handlers

BOOL GroupDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	if( ::IsAppRx() ) {
		SetWindowText( _T("Visit") );
		CWnd* pWndID = GetDlgItem( IDC_TXT_ID );
		if( pWndID ) pWndID->SetWindowText( _T("Visit ID:") );
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_GRP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GRP_ID, _T("ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_GRP_DESC, _T("Description") );
	pWnd = GetDlgItem( IDC_EDIT_NOTES );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Any additional information you would like.", _T("Notes") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_BN_NOTES );
	if( ::IsAppRx() ) {
		if( pWnd ) m_tooltip.AddTool( pWnd, "Add some extended notes for this visit.", _T("Extended Notes") );
	} else {
		if( pWnd ) m_tooltip.AddTool( pWnd, "Add some extended notes for this group.", _T("Extended Notes") );
	}

	if( !m_fNew )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( m_szExpID == _T("") )
	{
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	if( ::IsAppRx() ) {
		pWnd = GetDlgItem( IDC_BN_NOTES );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_GRP ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	CString szPath;
	::GetDataPathRoot( szPath );
	Groups grp;
	grp.SetDataPath( szPath );
	CString szID;
	HRESULT hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetID( szID );
		m_lstItems.AddTail( szID );
		hr = grp.GetNext();
	}

	m_fInit = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL GroupDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void GroupDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szAbbr == _T("") )
	{
		BCGPMessageBox( _T("The group ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szAbbr.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("The group description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szAbbr ) ) return;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CBCGPDialog::OnOK();
}

void GroupDlg::OnKillfocusEditId() 
{
	if( !m_fInit ) return;
	UpdateData( TRUE );

	if( m_szAbbr == _T("") ) return;

	if( !::IsAppRx() )
	{
		if( m_lstItems.Find( m_szAbbr ) )
		{
			int nResp = BCGPMessageBox( _T("This ID already exists. Would you like to see how it is currently being used?"), MB_YESNO );
			if( nResp == IDYES )
			{
				m_fInit = FALSE;
				Groups grp;
				if( grp.IsValid() )
				{
					HRESULT hr = grp.Find( m_szAbbr );
					if( SUCCEEDED( hr ) )
					{
						CString szItem;
						grp.GetDescriptionWithID( szItem );

						RelationshipDlg d( this, REL_GROUP, szItem );
						d.DoModal();
					}
					else BCGPMessageBox( _T("Could not find group.") );
				}
				else BCGPMessageBox( IDS_G_ERR );
				m_fInit = TRUE;
			}
			CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
			if( pWnd )
			{
				pWnd->SetWindowText( _T("") );
				pWnd->SetFocus();
			}
		}
	}
	else
	{
		if( m_lstItems.Find( m_szAbbr ) )
		{
			BCGPMessageBox( _T("This ID already exists.") );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
			if( pWnd )
			{
				pWnd->SetWindowText( _T("") );
				pWnd->SetFocus();
				return;
			}
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szAbbr ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
}

BOOL GroupDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("subjectgroups.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void GroupDlg::OnClickNotes()
{
	if( ::IsAppRx() ) return;

	// verify ID first
	UpdateData( TRUE );
	if( m_szExpID == _T("") ) return;
	if( m_szAbbr == _T("") )
	{
		BCGPMessageBox( _T("The experiment ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	if( m_szAbbr.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szAbbr == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ABBR );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CString szData = _T("SRC: Experiment - Notes");
	LogToFile( szData, LOG_UI );

	// Check for directory structure
	if( !VerifyDirectory( m_szAbbr ) )
	{
		BCGPMessageBox( _T("Unable to create directory for files.") );
		return;
	}

	CString szPath, szFile;
	::GetDataPathRoot( szPath );
	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s\\%s%s.txt"), m_szExpID, m_szAbbr, m_szExpID, m_szAbbr );
	else
		szFile.Format( _T("%s\\%s\\%s\\%s%s.txt"), szPath, m_szExpID, m_szAbbr, m_szExpID, m_szAbbr );

	NotesDlg d( N_GROUP, m_szAbbr, szFile, this );
	d.DoModal();
}

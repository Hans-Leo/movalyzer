// FeedbackDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "Experiments.h"
#include "..\DataMod\DataMod.h"
#include "FeedbackDlg.h"
#include "..\CTreeLink\DBCommon.h"
#include "Feedbacks.h"

static CStringList lstFeatures;

// FeedbackDlg dialog

IMPLEMENT_DYNAMIC(FeedbackDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(FeedbackDlg, CBCGPDialog)
	ON_WM_HELPINFO()
	ON_EN_KILLFOCUS(IDC_EDIT_ID, OnEnKillfocusEditID)
	ON_BN_CLICKED(IDC_BN_COLORMIN, OnBnClickedBnColormin)
	ON_BN_CLICKED(IDC_BN_COLORMAX, OnBnClickedBnColormax)
END_MESSAGE_MAP()

FeedbackDlg::FeedbackDlg(IFeedback* pF, BOOL fNew, CWnd* pParent /*=NULL*/)
	: CBCGPDialog(FeedbackDlg::IDD, pParent), m_pF( pF ), m_fNew( fNew ),
	  m_nType(0), m_MinColor(16711680), m_MaxColor(255)
{
	// List of existing feedbaks
	CString szPath, szItem;
	::GetDataPathRoot( szPath );
	Feedbacks fb;
	fb.SetDataPath( szPath );
	HRESULT hr = fb.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		fb.GetID( szItem );
		m_lstItems.AddTail( szItem );
		hr = fb.GetNext();
	}
}

FeedbackDlg::~FeedbackDlg()
{
}

void FeedbackDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
	DDV_MaxChars(pDX, m_szID, szIDS);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDV_MaxChars(pDX, m_szDesc, szDESC);
	DDX_Control(pDX, IDC_CBO_TYPE, m_cboType);
	DDX_CBIndex(pDX, IDC_CBO_TYPE, m_nType);
	DDX_Control(pDX, IDC_BN_COLORMIN, m_bnMin);
	DDX_Control(pDX, IDC_BN_COLORMAX, m_bnMax);
}

// FeedbackDlg message handlers

void FeedbackDlg::OnBnClickedBnColormin()
{
	m_MinColor = m_bnMin.GetColor();
	if( m_MinColor == -1 ) m_MinColor = 16711680;
}

void FeedbackDlg::OnBnClickedBnColormax()
{
 	m_MaxColor = m_bnMax.GetColor();
	if( m_MaxColor == -1 ) m_MaxColor = 255;
}

BOOL FeedbackDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_FEEDBACK );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Unique identifier of the feedback."), _T("ID") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Description of feedback."), _T("Description") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );
	pWnd = GetDlgItem( IDC_CBO_TYPE );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select the time function for this feedback.", _T("Time Function") );
	pWnd = GetDlgItem( IDC_BN_COLORMIN );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Click to select a color for minimum values of the time function.", _T("Minimum Color") );
	pWnd = GetDlgItem( IDC_BN_COLORMAX );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Click to select a color for maximum values of the time function.", _T("Maximum Color") );

	if( !m_fNew && m_pF )
	{
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->EnableWindow( FALSE );

		Feedbacks::GetID( m_pF, m_szID );
		Feedbacks::GetDescription( m_pF, m_szDesc );
		Feedbacks::GetFeature( m_pF, m_szType );
		Feedbacks::GetMinColor( m_pF, m_MinColor );
		Feedbacks::GetMaxColor( m_pF, m_MaxColor );
		UpdateData( FALSE );
	}

	// Dialog icon
	CWinApp* pApp = AfxGetApp();
	// TODO:
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_FEEDBACK ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// button colors
	m_bnMin.SetColor( m_MinColor );
	m_bnMin.EnableAutomaticButton (_T("Default"), 16711680);
	m_bnMin.EnableOtherButton( _T("Custom...") );
	m_bnMax.SetColor( m_MaxColor );
	m_bnMax.EnableAutomaticButton (_T("Default"), 255);
	m_bnMax.EnableOtherButton( _T("Custom...") );

	// FILL FEEDBACK FEATURE LIST
	BOOL fSuccess = FALSE;
	CString szLine;
	// create temp hwr/tf to get data if we have not already done so
	if( lstFeatures.GetCount() <= 0 )
	{
		CString szPath, szRaw, szTF, szTmp;
		CStdioFile f;
		int nPos = -1;

		// temporary data files
		::GetDataPathRoot( szPath );
		if( szPath == _T("") ) szRaw = _T("temp.HWR");
		else szRaw.Format( _T("%s\\temp.HWR"), szPath );
		szTF = szRaw.Left( szRaw.GetLength() - 3 );
		szTF += _T("TF");
		// now open it and add a few lines of bogus #'s
		if( f.Open( szRaw, CFile::modeCreate | CFile::modeWrite ) )
		{
			f.WriteString( _T("1 1 1\n") );
			f.WriteString( _T("2 2 2\n") );
			f.Close();
		}
		// now call tf with this file
		Experiments::ProcessTimfun( szRaw, szTF, 12.0, 1, 0, 0.0, ::GetOutputWindow() );
		// delete temp HWR file
		CFile::Remove( szRaw );
		// now open the tf file to obtain the features (headers)
		int nSkip = 0;
		if( f.Open( szTF, CFile::modeRead ) )
		{
			// format of TF = # label
			//				  values
			while( f.ReadString( szLine ) )
			{
				nSkip++;
				// 1st line is label (we skip first 3: sec, prsmin, beta)
				if( ( nSkip > 3 ) && ( szLine.Find( _T(" ") ) != -1 ) )
				{
					szLine = szLine.Mid( szLine.Find( _T(" ") ) + 1 );
					TRIM( szLine );

					// if not spectrum (diff. # of points), add to list
					szTmp = szLine;
					szTmp.MakeUpper();
					nPos = szTmp.Find( _T("SPECTRUM") );
					if( nPos == -1 ) lstFeatures.AddTail( szLine );
				}

				// 2nd line we ignore
				if( !f.ReadString( szLine ) ) break;
			}
			f.Close();
			fSuccess = TRUE;
			// delete temp TF file
			CFile::Remove( szTF );
		}
	}
	else fSuccess = TRUE;
	// fill & select
	if( fSuccess )
	{
		// now fill list
		POSITION pos = lstFeatures.GetHeadPosition();
		while( pos )
		{
			szLine = lstFeatures.GetNext( pos );
			m_cboType.AddString( szLine );
		}
		// select one if this is an edit
		if( !m_fNew )
		{
			int nSel = m_cboType.FindStringExact( -1, m_szType );
			if( nSel >= 0 ) m_cboType.SetCurSel( nSel );
			else BCGPMessageBox( _T("Unable to locate the previously stored feature.") );
		}
	}
	else if( !fSuccess )
	{
		BCGPMessageBox( _T("Unable to fill the features list. Please contact NeuroScript for assistance.") );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL FeedbackDlg::OnHelpInfo(HELPINFO* pHelpInfo)
{
	::GoToHelp( _T("condfeedback.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

BOOL FeedbackDlg::PreTranslateMessage(MSG* pMsg)
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void FeedbackDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szID == _T("") )
	{
		BCGPMessageBox( _T("The feedback ID must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
	}		
	if( m_szID.GetLength() < szIDS )
	{
		CString szMsg;
		szMsg.Format( IDS_ID_LEN, szIDS );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("CON") )
	{
		BCGPMessageBox( _T("ID cannot be \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szID == _T("NUL") )
	{
		BCGPMessageBox( _T("ID cannot be \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("The feedback description must have a value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}		
	// no reserved nor delim in ID (msg handled in killfocus)
	if( !::VerifyStringForReserved( m_szID ) ) return;
	// no reserved nor delim in description
	if( !::VerifyStringForReserved( m_szDesc ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("Description cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_DESC );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	if( m_cboType.GetCurSel() == CB_ERR )
	{
		BCGPMessageBox( _T("A feature must be selected.") );
		CWnd* pWnd = GetDlgItem( IDC_CBO_TYPE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	// assign values
	if( m_pF )
	{
		m_pF->put_ID( m_szID.AllocSysString() );
		m_pF->put_Description( m_szDesc.AllocSysString() );
		m_cboType.GetLBText( m_nType, m_szType );
		m_pF->put_Feature( m_szType.AllocSysString() );
		m_pF->put_MinColor( m_MinColor );
		m_pF->put_MaxColor( m_MaxColor );
	}

	CBCGPDialog::OnOK();
}

void FeedbackDlg::OnEnKillfocusEditID()
{
	UpdateData( TRUE );

	if( m_szID == _T("") ) return;

	if( m_lstItems.Find( m_szID ) )
	{
		BCGPMessageBox( _T("This ID already exists.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}

	// 24Aug07 - We are now allowing all characters so long as they are not reserved (file names)
	// and not containing the delimiter
	if( !::VerifyStringForReserved( m_szID ) )
	{
		CString szReserved = RESERVED, szDelim, szMsg;
		::GetDelimiter( szDelim );
		szMsg.Format( _T("ID cannot contain reserved characters (%s) nor the tree delimiter (%s)."),
					  szReserved, szDelim );
		BCGPMessageBox( szMsg );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
		if( pWnd )
		{
			pWnd->SetWindowText( _T("") );
			pWnd->SetFocus();
		}
	}
}

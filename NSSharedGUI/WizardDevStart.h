#pragma once


// WizardDevStart dialog

class AFX_EXT_CLASS WizardDevStart : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardDevStart)

public:
	WizardDevStart();
	virtual ~WizardDevStart();

// Dialog Data
	enum { IDD = IDD_WIZ_DEV_START };

// Overrides
public:
	virtual LRESULT OnWizardNext();
	BOOL DoHelp( HELPINFO* );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnSetActive();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);

	DECLARE_MESSAGE_MAP()
};

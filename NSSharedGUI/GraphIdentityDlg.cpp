// GraphIdentityDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "GraphIdentityDlg.h"
#include "..\DataMod\DataMod.h"
#include "Subjects.h"
#include "Groups.h"
#include "Conditions.h"
#include "AGraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ITEM_COUNT			3
#define ITEM_GROUP			0
#define ITEM_SUBJECT		1
#define ITEM_CONDITION		2

/////////////////////////////////////////////////////////////////////////////
// GraphIdentityDlg dialog

BEGIN_MESSAGE_MAP(GraphIdentityDlg, CBCGPDialog)
	ON_LBN_SELCHANGE(IDC_CBO_ITEM, OnSelItem)
END_MESSAGE_MAP()

GraphIdentityDlg::GraphIdentityDlg(CStringList* pGrps, CStringList* pSubjs,
								   CStringList* pConds, CObList* pQuests, CWnd* pParent)
	: CBCGPDialog(GraphIdentityDlg::IDD, pParent), m_pGroups( pGrps ), 
	  m_pSubjects( pSubjs ), m_pConditions( pConds ), m_nCount( 0 )
{
	POSITION pos = pQuests ? pQuests->GetHeadPosition() : NULL;
	while( pos )
	{
		Question* pQ = (Question*)pQuests->GetNext( pos );
		if( pQ && !(pQ->fNum) && ( pQ->lstA.GetCount() > 0 ) ) m_Quests.AddTail( pQ );
	}
}


void GraphIdentityDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_G, m_List);
	DDX_Control(pDX, IDC_CBO_ITEM, m_cbo);
	DDX_Text(pDX, IDC_TXT_COUNT, m_nCount );
}

/////////////////////////////////////////////////////////////////////////////
// GraphIdentityDlg message handlers

BOOL GraphIdentityDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_LIST_G );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Existing identity associations."), _T("List") );
	pWnd = GetDlgItem( IDC_CBO_ITEM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the category list of identity associations to view."), _T("Items") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close dialog."), _T("Close") );

	// FILL CATEGORY COMBO
	// fixed ones first
	m_cbo.AddString( _T("Groups") );
	m_cbo.AddString( _T("Subjects") );
	m_cbo.AddString( _T("Conditions") );
	// questions (dynamic)
	BOOL fNum = FALSE;
	CString szID, szHdr, szA, szItem;
	int nIdx = 0;
	POSITION pos = m_Quests.GetHeadPosition();
	while( pos )
	{
		Question* pQ = (Question*)m_Quests.GetNext( pos );
		if( pQ && !(pQ->fNum) && ( pQ->lstA.GetCount() > 0 ) ) m_cbo.AddString( pQ->szHdr );
	}
	// select default
	m_cbo.SetCurSel( ITEM_GROUP );

	// fill list
	FillList();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL GraphIdentityDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void GraphIdentityDlg::OnSelItem()
{
	FillList();
}

void GraphIdentityDlg::FillList()
{
	// delete all items from list
	m_List.DeleteAllItems();
	// delete all columns from list
	while( m_List.DeleteColumn( 0 ) );

	// which item is checked
	int nWhich = m_cbo.GetCurSel();
	if( nWhich == CB_ERR ) return;

	// columns
	if( nWhich == ITEM_SUBJECT )
	{
		m_List.InsertColumn( 0, _T("Index"), LVCFMT_CENTER, 50, 0 );
		m_List.InsertColumn( 1, _T("ID"), LVCFMT_LEFT, 50, 1 );
		m_List.InsertColumn( 2, _T("Code"), LVCFMT_LEFT, 100, 2 );
		m_List.InsertColumn( 3, _T("Description"), LVCFMT_LEFT, 225, 3 );
	}
	else if( nWhich > ITEM_CONDITION )
	{
		m_List.InsertColumn( 0, _T("Index"), LVCFMT_CENTER, 50, 0 );
		m_List.InsertColumn( 1, _T("Answer"), LVCFMT_LEFT, 225, 2 );
	}
	else
	{
		m_List.InsertColumn( 0, _T("Index"), LVCFMT_CENTER, 50, 0 );
		m_List.InsertColumn( 1, _T("ID"), LVCFMT_LEFT, 50, 1 );
		m_List.InsertColumn( 2, _T("Description"), LVCFMT_LEFT, 225, 2 );
	}


	CString szItem, szIdx, szID, szDesc, szCode;
	POSITION pos = NULL;
	int nPos = -1, nItem = 0, nCount = 0;
	HRESULT hr;

	if( nWhich == ITEM_GROUP )
	{
		Groups grp;
		if( !m_pGroups || !grp.IsValid() ) return;
		int nCnt = (int)m_pGroups->GetCount();

		pos = m_pGroups->GetHeadPosition();
		while( pos )
		{
			szItem = m_pGroups->GetNext( pos );
			nPos = szItem.Find( _T(":") );
			if( nPos == -1 ) break;

			szIdx = szItem.Left( nPos );
			if( nCnt >= 100 )
			{
				if( atoi( szIdx ) < 10 ) szIdx.Format( _T("00%d"), atoi( szIdx ) );
				else if( atoi( szIdx ) < 100 ) szIdx.Format( _T("0%d"), atoi( szIdx ) );
			}
			else if( nCnt >= 10 )
			{
				if( atoi( szIdx ) < 10 ) szIdx.Format( _T("0%d"), atoi( szIdx ) );
			}
			szID = szItem.Mid( nPos + 1 );

			hr = grp.Find( szID );
			if( SUCCEEDED( hr ) )
			{
				grp.GetDescription( szDesc );

				nItem = m_List.InsertItem( nItem, szIdx, 0 );
				m_List.SetItem( nItem, 1, LVIF_TEXT, szID, 0, 0, 0, NULL );
				m_List.SetItem( nItem, 2, LVIF_TEXT, szDesc, 0, 0, 0, NULL );

				nItem++;
				nCount++;
			}
		}
	}
	else if( nWhich == ITEM_SUBJECT )
	{
		Subjects subj;
		if( !m_pSubjects || !subj.IsValid() ) return;
		int nCnt = (int)m_pSubjects->GetCount();

		pos = m_pSubjects->GetHeadPosition();
		while( pos )
		{
			szItem = m_pSubjects->GetNext( pos );
			nPos = szItem.Find( _T(":") );
			if( nPos == -1 ) break;

			szIdx = szItem.Left( nPos );
			if( nCnt >= 100 )
			{
				if( atoi( szIdx ) < 10 ) szIdx.Format( _T("00%d"), atoi( szIdx ) );
				else if( atoi( szIdx ) < 100 ) szIdx.Format( _T("0%d"), atoi( szIdx ) );
			}
			else if( nCnt >= 10 )
			{
				if( atoi( szIdx ) < 10 ) szIdx.Format( _T("0%d"), atoi( szIdx ) );
			}
			szID = szItem.Mid( nPos + 1 );

			hr = subj.Find( szID );
			if( SUCCEEDED( hr ) )
			{
				subj.GetName( szDesc, ::IsPrivacyOn() );
				subj.GetCode( szCode );

				nItem = m_List.InsertItem( nItem, szIdx, 0 );
				m_List.SetItem( nItem, 1, LVIF_TEXT, szID, 0, 0, 0, NULL );
				m_List.SetItem( nItem, 2, LVIF_TEXT, szCode, 0, 0, 0, NULL );
				m_List.SetItem( nItem, 3, LVIF_TEXT, szDesc, 0, 0, 0, NULL );

				nItem++;
				nCount++;
			}
		}
	}
	else if( nWhich == ITEM_CONDITION )
	{
		if( !m_pConditions ) return;
		int nCnt = (int)m_pConditions->GetCount();

		NSConditions cond;
		if( !cond.IsValid() ) return;

		pos = m_pConditions->GetHeadPosition();
		while( pos )
		{
			szItem = m_pConditions->GetNext( pos );
			nPos = szItem.Find( _T(":") );
			if( nPos == -1 ) break;

			szIdx = szItem.Left( nPos );
			if( nCnt >= 100 )
			{
				if( atoi( szIdx ) < 10 ) szIdx.Format( _T("00%d"), atoi( szIdx ) );
				else if( atoi( szIdx ) < 100 ) szIdx.Format( _T("0%d"), atoi( szIdx ) );
			}
			else if( nCnt >= 10 )
			{
				if( atoi( szIdx ) < 10 ) szIdx.Format( _T("0%d"), atoi( szIdx ) );
			}
			szID = szItem.Mid( nPos + 1 );

			if( SUCCEEDED( cond.Find( szID ) ) )
			{
				cond.GetDescriptionWithID( szDesc );

				nItem = m_List.InsertItem( nItem, szIdx, 0 );
				m_List.SetItem( nItem, 1, LVIF_TEXT, szID, 0, 0, 0, NULL );
				m_List.SetItem( nItem, 2, LVIF_TEXT, szDesc, 0, 0, 0, NULL );

				nItem++;
				nCount++;
			}
		}
	}
	else
	{
		// find question
		pos = m_Quests.FindIndex( nWhich - ITEM_COUNT );
		if( !pos ) return;

		// get question object
		Question* pQ = (Question*)m_Quests.GetAt( pos );
		if( !pQ ) return;

		// loop through question answers
		pos = pQ->lstA.GetHeadPosition();
		while( pos )
		{
			Answer* pA = (Answer*)pQ->lstA.GetNext( pos );
			if( pA )
			{
				szItem.Format( _T("%d"), pA->nIdx );
				nItem = m_List.InsertItem( nItem, szItem, 0 );
				m_List.SetItem( nItem, 1, LVIF_TEXT, pA->szA, 0, 0, 0, NULL );

				nItem++;
				nCount++;
			}
		}
	}

	m_nCount = nCount;
	UpdateData( FALSE );
}

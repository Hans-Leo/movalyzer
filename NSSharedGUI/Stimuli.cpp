// Stimuluss.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "StimulusDlg.h"
#include "OverwriteDlg.h"
#include <direct.h>
#include "Elements.h"
#include "Stimuli.h"
#include "GraphDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CStringList	_lstExportedElements;
CStringList	_lstExportedStimuli;

///////////////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////////////////////////////////////////////////
// interface methods

void Stimuluss::GetDescriptionWithID( CString& szVal, BOOL fReverse )
{
	Stimuluss::GetDescriptionWithID( m_pStimulus, szVal, fReverse );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::GetNext()
{
	return Stimuluss::GetNext( m_pStimulus );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Find( CString szID )
{
	return Stimuluss::Find( m_pStimulus, szID );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Add()
{
	return Stimuluss::Add( m_pStimulus );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Modify()
{
	return Stimuluss::Modify( m_pStimulus );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Remove()
{
	return Stimuluss::Remove( m_pStimulus );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::SetDataPath( CString szPath )
{
	Stimuluss::SetDataPath( m_pStimulus, szPath );
}

///////////////////////////////////////////////////////////////////////////////
// interface methods (static)

void Stimuluss::GetID( IStimulus* pStimulus, CString& szVal )
{
	if( pStimulus )
	{
		BSTR bstrItem;
		pStimulus->get_ID( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDescription( IStimulus* pStimulus, CString& szVal )
{
	if( pStimulus )
	{
		BSTR bstrItem;
		pStimulus->get_Description( &bstrItem );
		szVal = bstrItem;
	}
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDemo( IStimulus* pStimulus, BOOL& val )
{
	if( pStimulus ) pStimulus->get_Demo( &val );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDescriptionWithID( IStimulus* pStimulus, CString& szVal, BOOL fReverse )
{
	if( pStimulus )
	{
		CString szDelim, szID, szDesc;
		BSTR bstrItem;

		::GetDelimiter( szDelim );
		Stimuluss::GetID( pStimulus, szID );
		pStimulus->get_Description( &bstrItem );
		szDesc = bstrItem;

		if( fReverse )
			szVal.Format( _T("%s%s%s"), szID, szDelim, szDesc );
		else
			szVal.Format( _T("%s%s%s"), szDesc, szDelim, szID );
	}
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::ResetToStart()
{
	return Stimuluss::ResetToStart( m_pStimulus );
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::ResetToStart( IStimulus* pStimulus )
{
	if( pStimulus ) return pStimulus->ResetToStart();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::GetNext( IStimulus* pStimulus )
{
	if( pStimulus ) return pStimulus->GetNext();
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Find( IStimulus* pStimulus, CString szID )
{
	if( pStimulus ) return pStimulus->Find( szID.AllocSysString() );
	else return E_FAIL;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Add( IStimulus* pStimulus )
{
	if( !pStimulus ) return E_FAIL;

	if( FAILED( pStimulus->Add() ) )
	{
		CString szTempMsg;
		szTempMsg.LoadString(IDS_ADD_ERR);
		BCGPMessageBox( szTempMsg+_T(" Stimuluss::Add") );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Modify( IStimulus* pStimulus )
{
	if( !pStimulus ) return E_FAIL;

	if( FAILED( pStimulus->Modify() ) )
	{
		BCGPMessageBox( IDS_EDIT_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

HRESULT Stimuluss::Remove( IStimulus* pStimulus )
{
	if( !pStimulus ) return E_FAIL;

	if( FAILED( pStimulus->Remove() ) )
	{
		BCGPMessageBox( IDS_SUBJDEL_ERR );
		return E_FAIL;
	}

	return S_OK;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::SetDataPath( IStimulus* pStimulus, CString szPath )
{
	if( pStimulus ) pStimulus->SetDataPath( szPath.AllocSysString() );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoAdd( CWnd* pOwner )
{
	return Stimuluss::DoAdd( m_pStimulus, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoAdd( IStimulus* pStimulus, CWnd* pOwner )
{
	if( !pStimulus ) return FALSE;

	CString szID, szDesc;
	BOOL fDemo;	// TODO temp patch for animated precue
	Stimuluss::GetID( pStimulus, szID );
	Stimuluss::GetDescription( pStimulus, szDesc );
	Stimuluss::GetDemo( pStimulus, fDemo );

	StimulusDlg d( pOwner, TRUE );
	d.m_szID = szID;
	d.m_szDesc = szDesc;
	if( d.DoModal() == IDOK )
	{
		if( d.m_szID.GetLength() == 0 )
		{
			BCGPMessageBox( _T("A stimulus ID must be specified.") );
			return FALSE;
		}

		// Assign values from dialog to object
		pStimulus->put_ID( d.m_szID.AllocSysString() );
		pStimulus->put_Description( d.m_szDesc.AllocSysString() );

		HRESULT hr = Stimuluss::Add( pStimulus );
		if( FAILED( hr ) )
		{
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" Stimmulus::DoAdd") );
			return FALSE;
		}

		// Add elements to stimelement & stimtarget link tables
		CString szItem, szID, szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );
		POSITION pos;

		// Add elements to stimuluselement link table
		IStimulusElement* pSE = NULL;
		hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
								 IID_IStimulusElement, (LPVOID*)&pSE );
		if( SUCCEEDED( hr ) )
		{
			pos = d.m_lstElem.GetHeadPosition();
			while( pos )
			{
				szItem = d.m_lstElem.GetNext( pos );
				szID = szItem.Left( szItem.Find( szDelim ) - 1 );

				pSE->put_StimulusID( d.m_szID.AllocSysString() );
				pSE->put_ElementID( szID.AllocSysString() );

				hr = pSE->Add();
				if( FAILED( hr ) )
					BCGPMessageBox( _T("Unable to add element to link table.") );
			}

			pSE->Release();
		}
		else BCGPMessageBox( _T("Unable to create StimulusElement object.") );

		// Add Targets to stimulustarget link table
		IStimulusTarget* pST = NULL;
		hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
								 IID_IStimulusTarget, (LPVOID*)&pST );
		if( SUCCEEDED( hr ) )
		{
			short nSeq = 0;
			pos = d.m_lstTarg.GetHeadPosition();
			while( pos )
			{
				szItem = d.m_lstTarg.GetNext( pos );
				szID = szItem.Left( szItem.Find( szDelim ) - 1 );
				nSeq++;

				pST->put_StimulusID( d.m_szID.AllocSysString() );
				pST->put_ElementID( szID.AllocSysString() );
				pST->put_Sequence( nSeq );

				hr = pST->Add();
				if( FAILED( hr ) )
					BCGPMessageBox( _T("Unable to add Target to link table.") );
			}

			pST->Release();
		}
		else BCGPMessageBox( _T("Unable to create StimulusTarget object.") );

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoEdit( CString szID, CWnd* pOwner )
{
	return Stimuluss::DoEdit( m_pStimulus, szID, pOwner );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoEdit( IStimulus* pStimulus, CString szID, CWnd* pOwner )
{
	if( !pStimulus ) return FALSE;

	CString szDesc;
	BOOL fDemo = FALSE;	// temp patch for animated precue : OUTDATED


	StimulusDlg d( pOwner );
	if( FAILED( Stimuluss::Find( pStimulus, szID ) ) ) return FALSE;
	Stimuluss::GetDescription( pStimulus, szDesc );
	Stimuluss::GetDemo( pStimulus, fDemo );
	d.m_szID = szID;
	d.m_szDesc = szDesc;
	if( d.DoModal() == IDOK )
	{
		pStimulus->put_Description( d.m_szDesc.AllocSysString() );

		HRESULT hr = Stimuluss::Modify( pStimulus );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_UPD_ERR );
			return FALSE;
		}

		// Update link tables
		CString szItem, szID, szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );
		POSITION pos;

		// Update stimuluselement link table
		IStimulusElement* pSE = NULL;
		hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
								 IID_IStimulusElement, (LPVOID*)&pSE );
		if( SUCCEEDED( hr ) )
		{
			// Go through orig list...if item from it is not in new one, delete it
			pos = d.m_lstOrigElem.GetHeadPosition();
			while( pos )
			{
				szItem = d.m_lstOrigElem.GetNext( pos );
				szID = szItem.Left( szItem.Find( szDelim ) - 1 );

				if( !d.m_lstElem.Find( szItem ) )
				{
					hr = pSE->Find( d.m_szID.AllocSysString(), szID.AllocSysString() );
					if( SUCCEEDED( hr ) )
					{
						hr = pSE->Remove();
						if( FAILED( hr ) ) BCGPMessageBox( _T("Failed to remove element from link table.") );
					}
				}
			}

			// Go through new list...if item is not in orig, add it
			pos = d.m_lstElem.GetHeadPosition();
			while( pos )
			{
				szItem = d.m_lstElem.GetNext( pos );
				szID = szItem.Left( szItem.Find( szDelim ) - 1 );

				if( !d.m_lstOrigElem.Find( szItem ) )
				{
					pSE->put_StimulusID( d.m_szID.AllocSysString() );
					pSE->put_ElementID( szID.AllocSysString() );

					hr = pSE->Add();
					if( FAILED( hr ) )
						BCGPMessageBox( _T("Unable to add element to link table.") );
				}
			}

			pSE->Release();
		}
		else BCGPMessageBox( _T("Unable to create StimulusElement object.") );

		// Update stimulustarget link table
		IStimulusTarget* pST = NULL;
		hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
								 IID_IStimulusTarget, (LPVOID*)&pST );
		if( SUCCEEDED( hr ) )
		{
			int nSeq = 0;
			BSTR bstr = NULL;
			CString szID;

			// hack it...delete all related items first from db
			hr = pST->FindForStimulus( d.m_szID.AllocSysString() );
			while( SUCCEEDED( hr ) )
			{
				hr = pST->Remove();
				if( FAILED( hr ) )
					BCGPMessageBox( _T("Unable to delete target from link table.") );

//				hr = pST->GetNext();
				// *HACK* Can't delete from the current recordset else it loses its pointer (*shrug*)
				hr = pST->FindForStimulus( d.m_szID.AllocSysString() );
			}

			// Go through new list...if item is not in orig, add it
			pos = d.m_lstTarg.GetHeadPosition();
			while( pos )
			{
				szItem = d.m_lstTarg.GetNext( pos );
				szID = szItem.Left( szItem.Find( szDelim ) - 1 );
				nSeq++;

				pST->put_StimulusID( d.m_szID.AllocSysString() );
				pST->put_ElementID( szID.AllocSysString() );
				pST->put_Sequence( (short)nSeq );

				hr = pST->Add();
				if( FAILED( hr ) )
					BCGPMessageBox( _T("Unable to add target to link table.") );
			}

			pST->Release();
		}
		else BCGPMessageBox( _T("Unable to create StimulusTarget object.") );

		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

int Stimuluss::GenerateXYZ( double** x, double** y, double**z )
{
	return Stimuluss::GenerateXYZ( m_pStimulus, x, y, z );
}

///////////////////////////////////////////////////////////////////////////////

int Stimuluss::GenerateXYZ( CStringList* pElements, double** x, double** y, double**z )
{
	return Stimuluss::GenerateXYZ( m_pStimulus, pElements, x, y, z );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::GenerateFile( CString& szFile, BOOL fAsk )
{
	return Stimuluss::GenerateFile( m_pStimulus, szFile, fAsk );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::GenerateFile( CStringList* pElements, CString& szFile, BOOL fAsk )
{
	return Stimuluss::GenerateFile( m_pStimulus, pElements, szFile, fAsk );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetElements( CStringList* pElements, CStringList* pOtherElements )
{
	Stimuluss::GetElements( m_pStimulus, pElements, pOtherElements );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetTargets( CStringList* pTargets )
{
	Stimuluss::GetTargets( m_pStimulus, pTargets );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ViewData( BOOL fAsk )
{
	Stimuluss::ViewData( m_pStimulus, fAsk );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ChartData( BOOL fAsk )
{
	Stimuluss::ChartData( m_pStimulus, fAsk );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetElements( IStimulus* pStimulus, CStringList* pElements, CStringList* pOtherElements )
{
	if( !pStimulus || !pElements || !pOtherElements ) return;

	// List of elements
	IStimulusElement* pSEL = NULL;
	{
		HRESULT hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
										 IID_IStimulusElement, (LPVOID*)&pSEL );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create StimulusElement object.") );
			return;
		}
	}
	BSTR bstr = NULL;
	pStimulus->get_ID( &bstr );
	CString szID = bstr, szElemID, szStimID;
	Elements elem;

	// LOOPS THROUGH ALL ELEMENTS
	HRESULT hr = pSEL->FindForStimulus( bstr );
	while( SUCCEEDED( hr ) )
	{
		pSEL->get_ElementID( &bstr );
		szElemID = bstr;
		pSEL->get_StimulusID( &bstr );
		szStimID = bstr;

		if( szStimID == szID )
		{
			// add to list
			pElements->AddTail( szElemID );
			// check for hide/show elements
			if( SUCCEEDED( elem.Find( szElemID ) ) )
			{
				elem.GetRightTarget( szElemID );
				if( ( szElemID != _T("") ) && !pOtherElements->Find( szElemID ) )
					pOtherElements->AddTail( szElemID );
				elem.GetWrongTarget( szElemID );
				if( ( szElemID != _T("") ) && !pOtherElements->Find( szElemID ) )
					pOtherElements->AddTail( szElemID );
			}
		}

		hr = pSEL->GetNext();
	}
	pSEL->Release();

	// Sort list
	::SortStringListAlpha( pElements );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDemoCount( int& nCount )
{
	CStringList lst;
	GetTargets( m_pStimulus, &lst );
	nCount = (int)lst.GetCount();
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetTargets( IStimulus* pStimulus, CStringList* pTargets )
{
	if( !pStimulus || !pTargets ) return;

	// List of elements
	IStimulusTarget* pSTL = NULL;
	{
		HRESULT hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
										 IID_IStimulusTarget, (LPVOID*)&pSTL );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
			return;
		}
	}
	BSTR bstr = NULL;
	pStimulus->get_ID( &bstr );
	CString szID = bstr, szElemID, szTemp;
	short nSeq = 0;
	CStringList lstTmp;

	// LOOPS THROUGH ALL ELEMENTS
	HRESULT hr = pSTL->FindForStimulus( bstr );
	while( SUCCEEDED( hr ) )
	{
		pSTL->get_ElementID( &bstr );
		szElemID = bstr;
		pSTL->get_Sequence( &nSeq );

		if( nSeq < 10 )
			szTemp.Format( _T("0%d@%s"), nSeq, szElemID );
		else
			szTemp.Format( _T("%d@%s"), nSeq, szElemID );
		lstTmp.AddTail( szTemp );

		hr = pSTL->GetNext();
	}
	pSTL->Release();

	// Sort list
	::SortStringListAlpha( &lstTmp );

	// Refill list w/just ID
	int nPos = -1;
	POSITION pos = lstTmp.GetHeadPosition();
	while( pos )
	{
		szTemp = lstTmp.GetNext( pos );
		nPos = szTemp.Find( _T("@") );
		szElemID = szTemp.Mid( nPos + 1 );
		pTargets->AddTail( szElemID );
	}
}

///////////////////////////////////////////////////////////////////////////////

int Stimuluss::GenerateXYZ( IStimulus* pStimulus, double** x, double** y, double**z )
{
	if( !pStimulus ) return 0;

	CStringList elems, otherelems;
	Stimuluss::GetElements( pStimulus, &elems, &otherelems );

	int nRes = Stimuluss::GenerateXYZ( pStimulus, &elems, x, y, z );
	Stimuluss::GenerateXYZ( pStimulus, &otherelems, x, y, z );

	return nRes;
}

///////////////////////////////////////////////////////////////////////////////

int Stimuluss::GenerateXYZ( IStimulus* pStimulus, CStringList* pElements,
						    double** x, double** y, double**z )
{
	if( !pStimulus || !pElements ) return 0;
	if( pElements->GetCount() <= 0 ) return 0;
	int nLines = 0;

	double cx = 0., cy = 0., wx = 0., wy = 0.;
	double *dx = NULL, *dy = NULL, *dz = NULL;
	double *xx = NULL, *yy = NULL, *zz = NULL;

	BSTR bstr = NULL;
	pStimulus->get_ID( &bstr );
	CString szID = bstr, szDelim, szItem;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// LOOPS THROUGH ALL ELEMENTS
	Elements elem;
	// Sizes first (construct a list of IDs as well)
	CStringList lstElems;
	int nPos = -1;
	POSITION pos = pElements->GetHeadPosition();
	while( pos )
	{
		szItem = pElements->GetNext( pos );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
			szID = szItem.Left( szItem.Find( szDelim ) - 1 );
		else
			szID = szItem;
		if( SUCCEEDED( elem.Find( szID ) ) )
		{
			lstElems.AddTail( szID );
			nLines += elem.GenerateXYZ( NULL, NULL, NULL, TRUE );
		}
	}
	// If no lines, return
	if( nLines <= 0 ) return 0;

	// Decorations file
	CString szDecFile;
	Stimuluss::GetDecFile( pStimulus, szDecFile );
	CStdioFile f;
	if( !f.Open( szDecFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Error opening decorations file.") );
		return 0;
	}

	// Reloops through (using list) to get data and assign to our list (create)
	xx = new double[ nLines ];
	yy = new double[ nLines ];
	zz = new double[ nLines ];
	int nShift = 0, nLinesCur = 0, idx = 0;
	pos = lstElems.GetHeadPosition();
	while( pos )
	{
		szID = lstElems.GetNext( pos );
		if( SUCCEEDED( elem.Find( szID ) ) )
		{
			nLinesCur = elem.GenerateXYZ( &dx, &dy, &dz );
			elem.GenerateDecorations( &f, nLinesCur );
			for( idx = 0; idx < nLinesCur; idx++ )
			{
				xx[ nShift + idx ] = dx[ idx ];
				yy[ nShift + idx ] = dy[ idx ];
				zz[ nShift + idx ] = dz[ idx ];
			}

			if( nLinesCur > 0 )
			{
				delete [] dx;
				delete [] dy;
				delete [] dz;
			}

			nShift += nLinesCur;
		}
	}
	f.Close();

	*x = xx;
	*y = yy;
	*z = zz;

	return nLines;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetBaseFile( CString& szBase )
{
	::GetDataPathRoot( szBase );
	szBase += _T("\\stimuli\\");
	// ensure exists
	_mkdir( szBase );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetStimFile( CString& szFile )
{
	Stimuluss::GetStimFile( m_pStimulus, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetStimFile( IStimulus* pStimulus, CString& szFile )
{
	if( !pStimulus ) return;

	CString szPath, szID;
	Stimuluss::GetBaseFile( szPath );
	Stimuluss::GetID( pStimulus, szID );
	szPath += szID;
//	szPath += _T(".STI");
	szPath += _T(".NFO");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetStimFileFromID( CString szID, CString& szFile )
{
	if( szID == _T("") ) return;

	Stimuluss::GetBaseFile( szFile );
	szFile += szID;
//	szFile += _T(".STI");
	szFile += _T(".NFO");
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetViewFile( CString& szFile )
{
	Stimuluss::GetViewFile( m_pStimulus, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetViewFile( IStimulus* pStimulus, CString& szFile )
{
	if( !pStimulus ) return;

	CString szPath, szID;
	Stimuluss::GetBaseFile( szPath );
	Stimuluss::GetID( pStimulus, szID );
	szPath += szID;
	szPath += _T(".STI");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetViewFileFromID( CString szID, CString& szFile )
{
	if( szID == _T("") ) return;

	Stimuluss::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".STI");
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDecFile( CString& szFile )
{
	Stimuluss::GetDecFile( m_pStimulus, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDecFile( IStimulus* pStimulus, CString& szFile )
{
	if( !pStimulus ) return;

	CString szPath, szID;
	Stimuluss::GetBaseFile( szPath );
	Stimuluss::GetID( pStimulus, szID );
	szPath += szID;
	szPath += _T(".FMT");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetDecFileFromID( CString szID, CString& szFile )
{
	if( szID == _T("") ) return;

	Stimuluss::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".FMT");
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetTargFile( CString& szFile )
{
	Stimuluss::GetStimFile( m_pStimulus, szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetTargFile( IStimulus* pStimulus, CString& szFile )
{
	if( !pStimulus ) return;

	CString szPath, szID;
	Stimuluss::GetBaseFile( szPath );
	Stimuluss::GetID( pStimulus, szID );
	szPath += szID;
	szPath += _T(".TGT");
	szFile = szPath;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::GetTargFileFromID( CString szID, CString& szFile )
{
	if( szID == _T("") ) return;

	Stimuluss::GetBaseFile( szFile );
	szFile += szID;
	szFile += _T(".TGT");
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::GenerateFile( IStimulus* pStimulus, CString& szFile, BOOL fAsk )
{
	if( !pStimulus ) return FALSE;

	CStringList elems, otherelems;
	Stimuluss::GetElements( pStimulus, &elems, &otherelems );

	BOOL fRet = Stimuluss::GenerateFile( pStimulus, &elems, szFile, fAsk );
	Stimuluss::GenerateFile( pStimulus, &otherelems, szFile, fAsk );

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::GenerateFile( IStimulus* pStimulus, CStringList* pElements, CString& szFile, BOOL fAsk )
{
	if( !pStimulus || !pElements ) return FALSE;
	BOOL fRet = FALSE;

	// ensure directory first
	Stimuluss::GetViewFile( pStimulus, szFile );
	CString szID, szItem, szDelim;
	Stimuluss::GetID( pStimulus, szID );
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// generate data file
	double *x = NULL, *y = NULL, *z = NULL;
	int nLines = Stimuluss::GenerateXYZ( pStimulus, pElements, &x, &y, &z );
	if( nLines > 0 )
	{
		// Write file
		CStdioFile f;
		if( f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
		{
			// Write to file
			for( int p = 0; p < nLines; p++ )
			{
				szItem.Format( _T("%lf %lf %lf\n"), x[ p ], y[ p ], z[ p ] );
				f.WriteString( szItem );
			}
			f.Close();

			fRet = TRUE;
		}

		// Cleanup
		delete [] x;
		delete [] y;
		delete [] z;
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::GenerateFiles()
{
	return Stimuluss::GenerateFiles( m_pStimulus );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::GenerateFiles( IStimulus* pStimulus )
{
	if( !pStimulus ) return FALSE;

	CString szID;
	Stimuluss::GetID( pStimulus, szID );
	if( szID == _T("") ) return FALSE;

	// file
	CString szFile, szItem;
	Stimuluss::GetStimFile( pStimulus, szFile );
	CStdioFile f;
	if( !f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Error writing stimulus info file.") );
		return FALSE;
	}

	// Elements
	CStringList elems, otherelems;
	Elements elem;
	Stimuluss::GetElements( pStimulus, &elems, &otherelems );
	int nCount = (int)elems.GetCount();

	// HACK FIX: For some unknown reason, artifact elements (that no longer exist)
	// are getting exported and are causing the count of elements to be wrong
	// this will loop through the elements FIRST and reduce nCount for each missing one
	POSITION pos = elems.GetHeadPosition();
	while( pos )
	{
		szItem = elems.GetNext( pos );
		if( FAILED( elem.Find( szItem ) ) ) nCount--;
	}

	szItem.Format( _T("%d\n"), nCount );
	f.WriteString( szItem );
	pos = elems.GetHeadPosition();
	while( pos )
	{
		szItem = elems.GetNext( pos );
		if( SUCCEEDED( elem.Find( szItem ) ) )
		{
			elem.GenerateFiles();

			szItem += _T("\n");
			f.WriteString( szItem );
		}
	}
	pos = otherelems.GetHeadPosition();
	while( pos )
	{
		szItem = otherelems.GetNext( pos );
		if( SUCCEEDED( elem.Find( szItem ) ) )
			elem.GenerateFiles();
	}
	// Targets
	elems.RemoveAll();
	Stimuluss::GetTargets( pStimulus, &elems );
	nCount = (int)elems.GetCount();
	szItem.Format( _T("%d\n"), nCount );
	f.WriteString( szItem );
	pos = elems.GetHeadPosition();
	while( pos )
	{
		szItem = elems.GetNext( pos );
		szItem += _T("\n");
		f.WriteString( szItem );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ViewData( IStimulus* pStimulus, BOOL fAsk )
{
	CString szFile;
	if( !Stimuluss::GenerateFile( pStimulus, szFile, fAsk ) ) return;

	// Display file
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );

	// remove temp file
	CFileFind ff;
	if( ff.FindFile( szFile ) ) CFile::Remove( szFile );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ChartData( IStimulus* pStimulus, BOOL fAsk )
{
	CString szFile;
	if( !Stimuluss::GenerateFile( pStimulus, szFile, fAsk ) ) return;

	// Chart file
	CGraphDlg gd;
	gd.ChartHwr( _T(""), _T(""), _T(""), szFile,
				 ::GetDeviceResolution(), ::GetMinPenPressure() );

	// remove temp file
	CFileFind ff;
	if( ff.FindFile( szFile ) ) CFile::Remove( szFile );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::Export( CMemFile* pFile, BOOL fFirst )
{
	if( !m_pStimulus || !pFile ) return FALSE;

	CString szItem, szStim, szElem, szStimID, szRElem, szWElem;
	BSTR bstr;
	HRESULT hr;

	// if first time called, clear the static memory of exported elements
	Stimuluss::GetID( m_pStimulus, szItem );
	if( fFirst )
	{
		_lstExportedElements.RemoveAll();
		_lstExportedStimuli.RemoveAll();

		_lstExportedStimuli.AddTail( szItem );
	}
	// else see if on list...if so, skip
	else if( _lstExportedStimuli.Find( szItem ) ) return TRUE;

	// Stimulus
	WRITE_STRING( pFile, TAG_STIMULUS );
	WRITE_STRING( pFile, _T("\n") );
	// ID
	Stimuluss::GetID( m_pStimulus, szItem );
	szStim = szItem;
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Description
	Stimuluss::GetDescription( m_pStimulus, szItem );
	WRITE_STRING( pFile, szItem );
	WRITE_STRING( pFile, _T("\n") );
	// Record termination
	WRITE_STRING( pFile, EXPORT_SEPARATOR );
	WRITE_STRING( pFile, _T("\n") );

	// Stimulus elements
	IStimulusElement* pSEL = NULL;
	{
		HRESULT hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
										 IID_IStimulusElement, (LPVOID*)&pSEL );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create StimulusElement object.") );
			return FALSE;
		}
	}
	Elements elem;
	hr = pSEL->FindForStimulus( szStim.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pSEL->get_ElementID( &bstr );
		szElem = bstr;
		pSEL->get_StimulusID( &bstr );
		szStimID = bstr;

		if( szStimID == szStim )
		{
			// WRITE ELEMENT
			// we don't want to repeat elements in export file
			// also, grab hide/show elements and export them
			if( !_lstExportedElements.Find( szElem ) )
			{
				hr = elem.Find( szElem );
				if( SUCCEEDED( hr ) )
				{
					if( elem.Export( pFile ) )
					{
						_lstExportedElements.AddTail( szElem );

						elem.GetRightTarget( szRElem );
						elem.GetWrongTarget( szWElem );
						if( ( szRElem != _T("") ) && SUCCEEDED( elem.Find( szRElem ) ) )
						{
							if( !_lstExportedElements.Find( szRElem ) )
							{
								elem.Export( pFile );
								_lstExportedElements.AddTail( szRElem );
							}
						}
						if( ( szWElem != _T("") ) && SUCCEEDED( elem.Find( szWElem ) ) )
						{
							if( !_lstExportedElements.Find( szWElem ) )
							{
								elem.Export( pFile );
								_lstExportedElements.AddTail( szWElem );
							}
						}
#ifdef _DEBUG
						szItem.Format( _T("EXPORTED: Element %s"), szElem );
						OutputAMessage( szItem );
#endif
					}
					else
					{
#ifdef _DEBUG
						szItem.Format( _T("ERROR: Error in exporting element %s"), szElem );
						OutputAMessage( szItem );
#endif
					}
				}
				else
				{
#ifdef _DEBUG
					szItem.Format( _T("ERROR: Element %s not found in database"), szElem );
					OutputAMessage( szItem );
#endif
				}
			}

			// WRITE STIM-ELEMENT RELATIONSHIP
			WRITE_STRING( pFile, TAG_STIMELMT );
			WRITE_STRING( pFile, _T("\n") );
			WRITE_STRING( pFile, szStim );
			WRITE_STRING( pFile, _T("\n") );
			WRITE_STRING( pFile, szElem );
			WRITE_STRING( pFile, _T("\n") );
			// Record termination
			WRITE_STRING( pFile, EXPORT_SEPARATOR );
			WRITE_STRING( pFile, _T("\n") );

#ifdef _DEBUG
			szItem.Format( _T("EXPORTED: Stimulus/Element relationship %s/%s"), szStim, szElem );
			OutputAMessage( szItem );
#endif
		}

		hr = pSEL->GetNext();
	}

	pSEL->Release();

	// Stimulus targets
	IStimulusTarget* pSTL = NULL;
	hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
							 IID_IStimulusTarget, (LPVOID*)&pSTL );
	if( SUCCEEDED( hr ) )
	{
		short nSeq = 0;
		hr = pSTL->FindForStimulus( szStim.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			pSTL->get_ElementID( &bstr );
			szElem = bstr;
			pSTL->get_Sequence( &nSeq );

			// WRITE STIM-TARGET RELATIONSHIP
			if( SUCCEEDED( elem.Find( szElem ) ) )
			{
				WRITE_STRING( pFile, TAG_STIMTARG );
				WRITE_STRING( pFile, _T("\n") );
				WRITE_STRING( pFile, szStim );
				WRITE_STRING( pFile, _T("\n") );
				WRITE_STRING( pFile, szElem );
				WRITE_STRING( pFile, _T("\n") );
				if( nSeq < 10 ) szItem.Format( _T("00%d"), nSeq );
				else if( nSeq < 100 ) szItem.Format( _T("0%d"), nSeq );
				else szItem.Format( _T("%d"), nSeq );
				WRITE_STRING( pFile, szItem );
				WRITE_STRING( pFile, _T("\n") );
				// Record termination
				WRITE_STRING( pFile, EXPORT_SEPARATOR );
				WRITE_STRING( pFile, _T("\n") );

#ifdef _DEBUG
				szItem.Format( _T("EXPORTED: Stimulus/Target relationship %s/%s"), szStim, szElem );
				OutputAMessage( szItem );
#endif
			}

			hr = pSTL->GetNext();
		}
		pSTL->Release();
	}
	else BCGPMessageBox( _T("Unable to create StimulusTarget object.") );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan )
{
//	if( !m_pStimulus || !pFile || !pFile->m_pStream ) return FALSE;
	if( !m_pStimulus || !pFile ) return FALSE;

	CString szLine, szID, szFile1, szFile2, szTemp;
	Stimuluss comp;
	BOOL fFiles = FALSE, fComp = FALSE;
	BOOL fOverWrite = FALSE, fRet = FALSE;

	// ID
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	HRESULT hr = Find( szLine );
	if( SUCCEEDED( hr ) ) fComp = TRUE;
	PutID( szLine );
	comp.PutID( szLine );
	szID = szLine;
	szTemp.Format( _T("IMPORT: Stimulus %s"), szLine );
	if( !fScan ) OutputAMessage( szTemp );
	// Desc
	fRet = READ_STRING( pFile, szLine );
	if( !fRet ) return FALSE;
	if( szLine == EXPORT_SEPARATOR ) goto DoImport;
	PutDescription( szLine );
	comp.PutDescription( szLine );

DoImport:
	if( fScan ) return TRUE;
	// if a record already exists, save imported one to temp database
	// and dump records from both for file compares to present to user
	::GetDataPathRoot( szFile1 );	// original
	::GetDataPathRoot( szFile2 );	// imported
	szFile1 += _T("\\comp1.txt");
	szFile2 += _T("\\comp2.txt");
	BOOL fDuplicate = FALSE;
	if( !fOWAllYes && !fOWAllNo && fComp )
	{
		// dump found record in normal db table
		BOOL fRes = DumpRecord( szID, szFile1 );
		// add (or find) imported item into temp table (new object set as temp)
		comp.SetTemp( TRUE );
		HRESULT hr2 = comp.Find( szID );	// just in case the files were not deleted before
		if( FAILED( hr2 ) ) hr2 = comp.Add();
		// dump temp record just added
		fRes = comp.DumpRecord( szID, szFile2 );
		// remove temp db tables & clear temp flag
		comp.RemoveTemp();
		comp.SetTemp( FALSE );
		// we have files
		fFiles = TRUE;
		// compare files to see if we need to ask (might be there but might be duplicate)
		CStdioFile f1, f2;
		if( f1.Open( szFile1, CFile::modeRead ) )
		{
			if( f2.Open( szFile2, CFile::modeRead ) )
			{
				CString sz1, sz2;
				while( f1.ReadString( sz1 ) )
				{
					if( !f2.ReadString( sz2 ) ) goto Overwrite;
					if( sz1 != sz2 ) goto Overwrite;
				}
				fDuplicate = TRUE;
				f2.Close();
			}
			f1.Close();
		}
	}

	// ask to overwrite (if necessary)
Overwrite:
	if( !fOWAllYes && !fOWAllNo && SUCCEEDED( hr ) && !fDuplicate )
	{
		OverwriteDlg od( OW_STIMULUS, szID, szFile1, szFile2 );
		od.DoModal();
		if( od.m_nResult == OW_YESALL ) fOWAllYes = TRUE;
		else if( od.m_nResult == OW_NOALL ) fOWAllNo = TRUE;
		else if( od.m_nResult == OW_YES ) fOverWrite = TRUE;
		else if( od.m_nResult == OW_NO ) fOverWrite = FALSE;
		else if( od.m_nResult == OW_CANCEL ) return FALSE;
	}
	else fOverWrite = fOWAllYes;
	// cleanup of comparison files if applicable
	if( fFiles )
	{
		CFileFind ff;
		if( ff.FindFile( szFile1 ) ) CFile::Remove( szFile1 );
		if( ff.FindFile( szFile2 ) ) CFile::Remove( szFile2 );
	}
	// exists & overwrite - modify
	if( SUCCEEDED( hr ) && ( fOverWrite || fOWAllYes ) && !fDuplicate )
	{
		if( fComp )
		{
			// have to relocate(find) from orig table because of corrupted file pointers (c-tree) from 2 tables being open
			// then must snag values from temp copy (of new data) and reset orig then modify
			hr = Find( szID );
			comp.GetDescription( szLine );
			PutDescription( szLine );
			hr = Modify();
		}
		else hr = Modify();
	}
	// does not exist - add
	else if( FAILED( hr ) ) hr = Add();
	// exists & not overwrite - do nothing
	else hr = S_OK;

	if( FAILED( hr ) ) return FALSE;

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ForceLocal()
{
	if( !m_pStimulus ) return;

	HRESULT hr = m_pStimulus->ForceLocal();
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ForceRemote()
{
	if( !m_pStimulus ) return;

	HRESULT hr = m_pStimulus->ForceRemote();
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::ForceDefault()
{
	if( !m_pStimulus ) return;

	HRESULT hr = m_pStimulus->ForceDefault();
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, BOOL fIncludeElements, CString szUserPath )
{
	CString szItem;
	short nVal = 0;
	BOOL fRet = FALSE;
	BOOL fVal;

	// locate
	HRESULT hr = Find( szID );
	if( SUCCEEDED( hr ) )
	{
		// search object
		Stimuluss dest;

		// verify that we're logged in if applicable
		if( !fToLocal && !::VerifyLogin() ) return FALSE;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			dest.ForceLocal();
			dest.SetDataPath( szUserPath );

			ForceLocal();
			SetDataPath( szUserPath );
		}
		else
		{
			dest.ForceRemote();
			ForceRemote();
		}

		// does it already exist?
		hr = dest.Find( szID );
		
		// add or modify
		if( SUCCEEDED( hr ) /*found*/ && fOverwrite )
			hr = Modify();
		else if( FAILED( hr ) )
			hr = Add();

		// return operation mode to normal
		dest.ForceDefault();
		ForceDefault();

		fRet = TRUE;
	}

	// include elements?
	if( fIncludeElements )
	{
		Elements elem;
		BSTR bstr;
		CString szElem, szStimID;
		CStringList lstElem, lstSeq;
		BOOL* pExists = NULL;
		int nCount = 0, nItem;

		/////////////////////////////////////////////////////////////////////////////
		// STIMULUS ELEMENTS
		/////////////////////////////////////////////////////////////////////////////

		// Stimulus element object
		IStimulusElement *pSEL = NULL;
		hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
									IID_IStimulusElement, (LPVOID*)&pSEL );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create StimulusElement object.") );
			return FALSE;
		}
		
		// find list of elements
		hr = pSEL->FindForStimulus( szID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			// get info
			pSEL->get_ElementID( &bstr );
			szElem = bstr;
			pSEL->get_StimulusID( &bstr );
			szStimID = bstr;
			if( szStimID == szID ) lstElem.AddTail( szElem );

			hr = pSEL->GetNext();
		}

		// loop through and xfer the elements
		POSITION posE = lstElem.GetHeadPosition();
		while( posE )
		{
			szElem = lstElem.GetNext( posE );
			if( fToLocal ) elem.DoCopy( szElem, szUserPath, fOverwrite );
			else elem.DoUpload( szElem, fOverwrite );
		}

		// create exists list (hack) based upon # of items
		nCount = (int)lstElem.GetCount();
		if( nCount > 1 )
			pExists = new BOOL[ nCount ];
		else if( nCount == 1 )
			pExists = &fVal;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			pSEL->ForceLocal();
			pSEL->SetDataPath( szUserPath.AllocSysString() );
		}
		else pSEL->ForceRemote();


		// now loop through to create our hack exist list
		nItem = 0;
		posE = lstElem.GetHeadPosition();
		while( posE )
		{
			szElem = lstElem.GetNext( posE );

			// find relationship (if exists)
			hr = pSEL->Find( szID.AllocSysString(), szElem.AllocSysString() );

			if( nCount > 1 ) pExists[ nItem++ ] = SUCCEEDED( hr );
			else *pExists = SUCCEEDED( hr );
		}

		// now loop through and transfer
		nItem = 0;
		posE = lstElem.GetHeadPosition();
		while( posE )
		{
			szElem = lstElem.GetNext( posE );

			// assigns vals
			pSEL->put_ElementID( szElem.AllocSysString() );
			pSEL->put_StimulusID( szID.AllocSysString() );

			// check exists list
			if( nCount > 1 ) hr = pExists[ nItem++ ] ? S_OK : E_FAIL;
			else hr = *pExists ? S_OK : E_FAIL;

			// add if does not exist (do nothing if it exists)
			if( FAILED( hr ) ) hr = pSEL->Add();

			if( FAILED( hr ) )
			{
				CString szMsg;
				szMsg.Format( _T("Error transferring StimulusElement for stimulus %s and element %s."), szStimID, szElem );
				OutputAMessage( szMsg, TRUE );
			}
		}

		// reset operation mode
		pSEL->ForceDefault();

		// cleanup
		pSEL->Release();
		if( pExists && ( nCount > 1 ) ) delete [] pExists;
		lstElem.RemoveAll();


		/////////////////////////////////////////////////////////////////////////////
		// STIMULUS TARGETS
		/////////////////////////////////////////////////////////////////////////////

		// Stimulus target objects
		IStimulusTarget *pSTL = NULL;
		{
			hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
									 IID_IStimulusTarget, (LPVOID*)&pSTL );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
				return FALSE;
			}
		}

		// find list of targets
		hr = pSTL->FindForStimulus( szID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			// get info
			pSTL->get_ElementID( &bstr );
			szElem = bstr;
			pSTL->get_StimulusID( &bstr );
			szStimID = bstr;
			pSTL->get_Sequence( &nVal );
			szItem.Format( _T("%d"), nVal );
			if( szStimID == szID )
			{
				lstElem.AddTail( szElem );
				lstSeq.AddTail( szItem );
			}

			hr = pSTL->GetNext();
		}

		// create exists list (hack) based upon # of items
		nCount = (int)lstElem.GetCount();
		if( nCount > 1 )
			pExists = new BOOL[ nCount ];
		else if( nCount == 1 )
			pExists = &fVal;

		// force destination to local & set path or to remote
		if( fToLocal )
		{
			pSTL->ForceLocal();
			pSTL->SetDataPath( szUserPath.AllocSysString() );
		}
		else pSTL->ForceRemote();

		// now loop through to create our hack exist list
		nItem = 0;
		posE = lstElem.GetHeadPosition();
		POSITION posS = lstSeq.GetHeadPosition();
		while( posE )
		{
			szElem = lstElem.GetNext( posE );
			szItem = lstSeq.GetNext( posS );

			// find relationship (if exists)
			hr = pSTL->Find( szID.AllocSysString(), szElem.AllocSysString(), atoi( szItem ) );

			if( nCount > 1 ) pExists[ nItem++ ] = SUCCEEDED( hr );
			else *pExists = SUCCEEDED( hr );
		}

		// now loop through and transfer
		nItem = 0;
		posE = lstElem.GetHeadPosition();
		posS = lstSeq.GetHeadPosition();
		while( posE )
		{
			szElem = lstElem.GetNext( posE );
			szItem = lstSeq.GetNext( posS );

			// assigns vals
			pSTL->put_ElementID( szElem.AllocSysString() );
			pSTL->put_StimulusID( szID.AllocSysString() );
			pSTL->put_Sequence( (short)atoi( szItem ) );

			// check exists list
			if( nCount > 1 ) hr = pExists[ nItem++ ] ? S_OK : E_FAIL;
			else hr = *pExists ? S_OK : E_FAIL;

			// add if does not exist (do nothing if it exists)
			if( FAILED( hr ) ) hr = pSTL->Add();

			if( FAILED( hr ) )
			{
				CString szMsg;
				szMsg.Format( _T("Error transfering StimulusTarget for stimulus %s and element %s."), szStimID, szElem );
				OutputAMessage( szMsg, TRUE );
			}
		}

		// reset operation mode
		pSTL->ForceDefault();

		// cleanup
		pSTL->Release();
		if( pExists && ( nCount > 1 ) ) delete [] pExists;
	}

	return fRet;
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoCopy( CString szID, CString szPath, BOOL fOverwrite )
{
	return DoTransfer( TRUE, szID, fOverwrite, FALSE, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoCopyAll( CString szID, CString szPath, BOOL fOverwrite, BOOL fIncludeElements )
{
	return DoTransfer( TRUE, szID, fOverwrite, fIncludeElements, szPath );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoUpload( CString szID, BOOL fOverwrite )
{
	return DoTransfer( FALSE, szID, fOverwrite, FALSE );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DoUploadAll( CString szID, BOOL fOverwrite, BOOL fIncludeElements )
{
	return DoTransfer( FALSE, szID, fOverwrite, fIncludeElements );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::SetTemp( BOOL fVal )
{
	if( m_pStimulus ) m_pStimulus->SetTemp( fVal );
}

///////////////////////////////////////////////////////////////////////////////

BOOL Stimuluss::DumpRecord( CString szID, CString szFile )
{
	if( !m_pStimulus ) return FALSE;
	return SUCCEEDED( m_pStimulus->DumpRecord( szID.AllocSysString(), szFile.AllocSysString() ) );
}

///////////////////////////////////////////////////////////////////////////////

void Stimuluss::RemoveTemp()
{
	if( m_pStimulus ) m_pStimulus->RemoveTemp();
}

///////////////////////////////////////////////////////////////////////////////

ULONGLONG Stimuluss::GetNumRecords()
{
	ULONGLONG val = 0;
	if( m_pStimulus ) m_pStimulus->GetNumRecords( &val );
	return val;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

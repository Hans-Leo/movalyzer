#pragma once

// ProcSetPageREChart.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREChart dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageREChart : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageREChart)

// Construction
public:
	ProcSetPageREChart( IProcSetting* pS = NULL );
	~ProcSetPageREChart();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_RE_CHART };
	BOOL			m_fDisplay;
	BOOL			m_fRaw;
	BOOL			m_fTF;
	BOOL			m_fSeg;
	CColoredButton	m_chkDisplay;
	CColoredButton	m_chkRaw;
	CColoredButton	m_chkTF;
	CColoredButton	m_chkSeg;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void SetFieldStates();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnChkDisplay();
	afx_msg void OnChkTf();
	afx_msg void OnBnReset();
	DECLARE_MESSAGE_MAP()
};

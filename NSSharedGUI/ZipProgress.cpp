// Globals.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "..\Common\UserObj.h"
#include "ZipProgress.h"
#include "Progress.h"

#define	SEPARATE_THREAD		FALSE
bool _fAbort = false;

///////////////////////////////////////////////////////////////////////////////

void ZipProgress::Stop()
{
	_fAbort = true;
}

void ZipProgress::AbortCheck( bool *abort )
{
	// todo
}

void ZipProgress::WriteZipBegin()
{
	_fAbort = false;

	// enable progress
	Progress::EnableProgress( 100 );
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj ) pObj->m_nState = STATE_IMPORTEXPORT;
}

void ZipProgress::WriteZipPercentDone( long pctDone, bool* abort )
{
	// show percentange done
	CString szMsg;
	szMsg.Format( _T("File compression completion: %u%%"), pctDone );
	Progress::SetProgress( pctDone, szMsg );
	// abort flag
	*abort = _fAbort;
}

void ZipProgress::WriteZipEnd()
{
	// disable progress
	Progress::DisableProgress();
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj ) pObj->m_nState = STATE_NONE;
}

void ZipProgress::UnzipBegin()
{
	_fAbort = false;
	// enable progress
	Progress::EnableProgress( 100 );
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj ) pObj->m_nState = STATE_IMPORTEXPORT;
}

void ZipProgress::UnzipPercentDone( long pctDone, bool* abort )
{
	// show percentange done
	CString szMsg;
	szMsg.Format( _T("File decompression completion: %u%%"), pctDone );
	Progress::SetProgress( pctDone, szMsg );
	// abort flag
	*abort = _fAbort;
}

void ZipProgress::UnzipEnd()
{
	// disable progress
	Progress::DisableProgress();
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj ) pObj->m_nState = STATE_NONE;
}

#pragma once

#include "NSTooltipCtrl.h"

// GraphDlg.h : header file
//
#define SYMBOL_START_SEG	30000
#define SYMBOL_START_SUB	31000
#define TF_MENU_BASE		32000

#define	CHART_NONE			999
#define	CHART_HWR			100
#define	CHART_TF			200
#define	CHART_HWRRT			300
#define	CHART_TFRT			400
#define	CHART_HWR3D			150
#define	CHART_TF3D			250
// The following are for modeless only
#define	CHART_HWR_BASE		100
#define	CHART_HWR_XY		100
#define	CHART_HWR_XZ		101
#define	CHART_HWR_YZ		102
#define	CHART_HWR_XYt		103
#define	CHART_HWR_Zt		104
#define	CHART_TF_BASE		200
#define	CHART_TF_XY			200
#define	CHART_TF_XZ			201
#define	CHART_TF_YZ			202
#define	CHART_TF_Xt			203
#define	CHART_TF_Yt			204
#define	CHART_TF_Zt			205
#define	CHART_TF_VX			206
#define	CHART_TF_VY			207
#define	CHART_TF_VABS		208
#define	CHART_TF_AX			209
#define	CHART_TF_AY			210
#define	CHART_TF_AZ			211
#define	CHART_TF_JXt		212
#define	CHART_TF_JYt		213
#define	CHART_TF_JABSt		214
#define	CHART_TF_ASPEC		215
#define CHART_CUSTOM		500

#define	GM_REALTIMECOMPLETE	(WM_USER + 1020)

// class for managing axis types
class GraphAxis : public CObject
{
public:
	GraphAxis( int nIndex, int nID, CString szLabel, CString szTFLabel = _T("") ) : CObject()
	{
		m_nIndex = nIndex;
		m_nID = nID;
		m_szLabel = szLabel;
		m_szTFLabel = szTFLabel;
	}
	~GraphAxis() {}
public:
	int			m_nIndex;		// index for combo
	int			m_nID;			// ID for showmod
	CString		m_szLabel;		// label for combo
	CString		m_szTFLabel;	// lable for TF file
};

// class for managing graph types
class GraphSelection : public CObject
{
public:
	GraphSelection( int nIndex, int nID, CString szLabel, GraphAxis* pX, GraphAxis* pY ) : CObject()
	{
		m_nIndex = nIndex;
		m_nID = nID;
		m_szLabel = szLabel;
		m_pAxisX = pX;
		m_pAxisY = pY;
	}
	~GraphSelection() {}
public:
	int			m_nIndex;		// index for combo
	int			m_nID;			// ID for showmod
	CString		m_szLabel;		// label for combo
	GraphAxis*	m_pAxisX;		// x-axis information
	GraphAxis*	m_pAxisY;		// y-axis information
};

/////////////////////////////////////////////////////////////////////////////
// CGraphDlg dialog

interface IShowHWR;
interface IShowTF;
class WindowManager;

class AFX_EXT_CLASS CGraphDlg : public CBCGPDialog
{
// Construction
public:
	// Modal constructor
	CGraphDlg(UINT nChart = CHART_NONE, CWnd* pParent = NULL, WindowManager* pWM = NULL);
	// MODELESS DIALOGS
	// ...fFeedback indicates whether color-coded feedback will exist per point based on szFeature
	// ......which is obtained from the TF file (hence TF charts only) and use the min & max colors
	// ...fGraphOnly will not draw any labels or axes
	// ...pWndOther is if you want to provide your own window for the graph to be displayed
	// ...pWndNotify is only applicable if you use pWndOther for real-time TF graphs...
	// ......it uses the generic SendMessage of the CWnd class which you will have to override.
	// ......Upon completion of the real-time display the following will be called
	// ......pWndNotify->PostMessage( GM_REALTIMECOMPLETE, 0, 0 ) ... a hack, i know, but whatever :D
	// Modeless constructor
	CGraphDlg(double dFreq, int nPrs, CString szFile, UINT nChart, BOOL fFeedback = FALSE,
			  CString szFeature = _T(""), DWORD minColor = 0, DWORD maxColor = 0,CWnd* pParent = NULL,
			  CString szSeg = _T(""), DWORD dwPenUpColor = RGB(255,255,255), BOOL fChangePenUpColor = FALSE,
			  BOOL fGraphOnly = FALSE, CWnd* pWndOther = NULL, CWnd* pWndNotify = NULL,
			  BOOL fPassed = TRUE, BOOL fForceShowErr = FALSE );
	// Modeless constructor with multiple trials (must delete lists)
	CGraphDlg(double dFreq, int nPrs, CStringList* pFileList, CString szStart, UINT nChart,
			  BOOL fFeedback = FALSE, CString szFeature = _T(""), DWORD minColor = 0, DWORD maxColor = 0,
			  CWnd* pParent = NULL, BOOL fMultiple = FALSE, DWORD dwPenUpColor = RGB(255,255,255),
			  BOOL fChangePenUpColor = FALSE, BOOL fGraphOnly = FALSE, CWnd* pWndOther = NULL,
			  CWnd* pWndNotify = NULL, WindowManager* pWM = NULL, BOOL fPassed = TRUE );
	~CGraphDlg();

// Dialog Data
	enum { IDD = IDD_GRAPH };
	CObList				m_lstAxes;
	CObList				m_lstGraphs;
	CBCGPComboBox		m_cbo;
	CBCGPComboBox		m_cboX;
	CBCGPComboBox		m_cboY;
	CBCGPComboBox		m_cboFeature;
	BOOL				m_fAnnValues;
	BOOL				m_fProportional;
	BOOL				m_fMonochrome;
	BOOL				m_fSupressAnn;
	BOOL				m_fInverted;
	UINT				m_nChart;
	int					m_nCurChart;
	BOOL				m_fOverlay;
	HWND				m_hPE;
	CWnd*				m_pGraph;
	IShowHWR*			m_pShowHWR;
	IShowTF*			m_pShowTF;
	CString				m_szFile;
	CString				m_szFile2;
	double				m_dFrequency;
	int					m_nPenPressure;
	int					m_nTop;
	int					m_nLeft;
	int					m_nRightBuffer;
	int					m_nBottomBuffer;
	int					m_nToolbar;
	int					m_nErrTxt;
	int					m_nErrBn;
	BOOL				m_fModeless;
	NSToolTipCtrl		m_tooltip;
	CStringList*		m_pFileList;
	BOOL				m_fMultiple;
	POSITION			m_posCur;
	BOOL				m_fChangePenUpColor;
	DWORD				m_dwPenUpColor;
	BOOL				m_fOptionChanged;
	BOOL				m_fFeedback;
	CString				m_szFeature;
	static CString		_szFeature;
	int					m_nFeature;
	DWORD				m_MinColor;
	static DWORD		_MinColor;
	DWORD				m_MaxColor;
	static DWORD		_MaxColor;
	CBCGPButton			m_bnPrev;
	CBCGPButton			m_bnNext;
	CBCGPButton			m_bnPrevT;
	CBCGPButton			m_bnNextT;
	CBCGPButton			m_bnErr;
	CBCGPMenuButton		m_bnGo;
	CMenu				m_mnuGo;
	CWnd*				m_pWndOther;
	CWnd*				m_pWndNotify;
	BOOL				m_fGraphOnly;
	BOOL				m_fPassedConsis;
	short				m_nThickness;
	CString				m_szErr;
	BOOL				m_fSMA;
	WindowManager*		m_pWM;
	short				m_nSymbolSeg;
	short				m_nSymbolSubMvmt;
	BOOL				m_fShowChartData;
	static BOOL			m_fDoNotNotify;

	static CGraphDlg* GetActiveChart();

// Overrides
protected:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	IShowHWR* GetShowHWRObject();
	IShowTF* GetShowTFObject();
	void MoveNext();
	void MovePrev();
	void NextTrial();
	void PrevTrial();
	void NotifyOfTrial();
	void HWRChart( ULONG nChart );
	void TFChart( ULONG nChart );
	void HWRRTChart();
	void TFRTChart();
	void UpdateWindowTitle();
	void OnLines( int nThickness );
	void OnChart( UINT nWhich );
	void OnBnError( BOOL fInit );
	void ShowChartData();
	GraphAxis* GetAxis( int nIndex );
	GraphAxis* GetAxis( CString szLabel, BOOL fTFLabel = FALSE );
	GraphSelection* GetGraph( int nIndex );
	GraphSelection* GetGraph( GraphAxis* pAxisX, GraphAxis* pAxisY );
	void RemoveViewMenu();
public:
	void ChartTf3D( CString szExp, CString szGrp, CString szSubj, CString szTrial,
					double dFrequency, BOOL fPassed = TRUE );
	void ChartHwr3D( CString szExp, CString szGrp, CString szSubj,
					 CString szTrial, double dFrequency );
	void ChartTfRT( CString szExp, CString szGrp, CString szSubj,
					CString szTrial, double dFrequency );
	void ChartHwrRT( CString szExp, CString szGrp, CString szSubj,
					 CString szTrial, double dFrequency );
	void ChartTf( CString szExp, CString szGrp, CString szSubj, CString szTrial,
				  double dFrequency, int nMinPenPressure, BOOL fPassed = TRUE );
	void ChartTf( CString szFile, double dFrequency, CString szSeg,
				  BOOL fModeless, BOOL fPassed, BOOL fForceShowErr,
				  CWnd* pWndOwner );
	void ChartHwr( CString szExp, CString szGrp, CString szSubj,
				   CString szTrial, double dFrequency, int nMinPenPressure );

// message map functions
	afx_msg void OnActivate(UINT nState, CWnd *pWndOther, BOOL bMinimized);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeCboGraph();
	afx_msg void OnSelchangeCboX();
	afx_msg void OnSelchangeCboY();
	afx_msg void OnSelchangeCboFeature();
	afx_msg void OnChkAnn();
	afx_msg void OnChkProportional();
	afx_msg void OnChkMonochrome();
	afx_msg void OnChkInvert();
	afx_msg void OnChkSupress();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnBnReplay();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnPrint();
	afx_msg void OnBnClickedBnColormin();
	afx_msg void OnBnClickedBnColormax();
	afx_msg LRESULT OnWMMessage( WPARAM, LPARAM );
	void OnBnData( BOOL fTF = FALSE, BOOL fSeg = FALSE, BOOL fDis = FALSE, BOOL fWrd = FALSE );
	afx_msg void OnBnExport();
	void OnBnDataExt( BOOL fCon = FALSE, BOOL fErr = FALSE );
	afx_msg void OnBnLines();
	afx_msg void OnBnChart();
	afx_msg void OnBnGo();
	afx_msg void OnClickErr();
	DECLARE_MESSAGE_MAP()
};

// WizardDevDevices.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardDevDevices.h"
#include "WizardDevSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// WizardDevDevices dialog

IMPLEMENT_DYNAMIC(WizardDevDevices, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardDevDevices, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_RDO_MOUSE, OnBnClickedDevice)
	ON_BN_CLICKED(IDC_RDO_TABLET, OnBnClickedDevice)
	ON_BN_CLICKED(IDC_RDO_GRIPPER, OnBnClickedDevice)
END_MESSAGE_MAP()

WizardDevDevices::WizardDevDevices() : CBCGPPropertyPage(WizardDevDevices::IDD)
{
	m_nDevice = IDC_RDO_MOUSE;
	if( ::IsTabletModeOn() ) m_nDevice = IDC_RDO_TABLET;
	else if( ::IsGripperModeOn() ) m_nDevice = IDC_RDO_GRIPPER;
	m_nCurImg = -1;
}

WizardDevDevices::~WizardDevDevices()
{
}

void WizardDevDevices::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

// WizardDevDevices message handlers

BOOL WizardDevDevices::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_DEV_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_RDO_MOUSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the mouse as the default input device."), _T("Mouse") );
	pWnd = GetDlgItem( IDC_RDO_TABLET );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the tablet as the default input device."), _T("Tablet") );
	pWnd = GetDlgItem( IDC_RDO_GRIPPER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the gripper as the default input device."), _T("Gripper") );

	// determine which devices are available
	if( !::IsTabletInstalled() )
	{
		m_nDevice = IDC_RDO_MOUSE;
		pWnd = GetDlgItem( IDC_RDO_TABLET );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
	// If nidaq not installed, disable gripper choice
	BOOL fGripper = ::IsGripperInstalled();
	if( !fGripper )
	{
		pWnd = GetDlgItem( IDC_RDO_GRIPPER );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}

	// check appropriate device
	CheckRadioButton( IDC_RDO_TABLET, IDC_RDO_GRIPPER, m_nDevice );

	// show/hide images
	ShowHideImages();

	// Highlight the main sentence
	CStatic* pDis = (CStatic*)GetDlgItem( IDC_TXT_HELP );
	CDC* pDC = GetDC();
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_BOLD;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	pDis->SetFont( &font, TRUE );
	pDC->SelectObject( pOldFont );
	ReleaseDC( pDC );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void WizardDevDevices::ShowHideImages( UINT nID )
{
	CWnd* pWnd = GetDlgItem( IDB_MOUSE );
	if( pWnd ) pWnd->ShowWindow( ( nID != -1 ) ? ( nID == IDC_RDO_MOUSE ) : ( m_nDevice == IDC_RDO_MOUSE ) );
	pWnd = GetDlgItem( IDB_TABLET );
	if( pWnd ) pWnd->ShowWindow( ( nID != -1 ) ? ( nID == IDC_RDO_TABLET ) : ( m_nDevice == IDC_RDO_TABLET ) );
	pWnd = GetDlgItem( IDB_DAQPAD );
	if( pWnd ) pWnd->ShowWindow( ( nID != -1 ) ? ( nID == IDC_RDO_GRIPPER ) : ( m_nDevice == IDC_RDO_GRIPPER ) );

	if( nID != -1 ) m_nCurImg = nID;
	else m_nCurImg = m_nDevice;
}

void WizardDevDevices::OnMouseMove( UINT nFlags, CPoint pt )
{
	CRect rectMouse, rectTablet, rectGripper;
	CWnd* pWnd = GetDlgItem( IDC_RDO_MOUSE );
	pWnd->GetWindowRect( rectMouse );
	ScreenToClient( rectMouse );
	rectMouse.InflateRect( 2, 2, 2, 2 );
	pWnd = GetDlgItem( IDC_RDO_TABLET );
	pWnd->GetWindowRect( rectTablet );
	ScreenToClient( rectTablet );
	rectTablet.InflateRect( 2, 2, 2, 2 );
	pWnd = GetDlgItem( IDC_RDO_GRIPPER );
	pWnd->GetWindowRect( rectGripper );
	ScreenToClient( rectGripper );
	rectGripper.InflateRect( 2, 2, 2, 2 );

	if( ( pt.x >= rectMouse.left ) && ( pt.x <= rectMouse.right ) &&
		( pt.y >= rectMouse.top ) && ( pt.y <= rectMouse.bottom ) )
	{
		if( m_nCurImg != IDC_RDO_MOUSE ) ShowHideImages( IDC_RDO_MOUSE );
	}
	else if( ( pt.x >= rectTablet.left ) && ( pt.x <= rectTablet.right ) &&
		( pt.y >= rectTablet.top ) && ( pt.y <= rectTablet.bottom ) )
	{
		if( m_nCurImg != IDC_RDO_TABLET ) ShowHideImages( IDC_RDO_TABLET );
	}
	else if( ( pt.x >= rectGripper.left ) && ( pt.x <= rectGripper.right ) &&
		( pt.y >= rectGripper.top ) && ( pt.y <= rectGripper.bottom ) )
	{
		if( m_nCurImg != IDC_RDO_GRIPPER ) ShowHideImages( IDC_RDO_GRIPPER );
	}
	else if( m_nCurImg != m_nDevice ) ShowHideImages();
}

BOOL WizardDevDevices::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardDevDevices::OnWizardBack() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardDevDevices::OnSetActive() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnSetActive();
}

LRESULT WizardDevDevices::OnWizardNext() 
{
	WizardDevSheet* pParent = (WizardDevSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	m_nDevice = GetCheckedRadioButton( IDC_RDO_TABLET, IDC_RDO_GRIPPER );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardDevDevices::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("devicesetup.html"), this );
	return TRUE;
}

BOOL WizardDevDevices::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardDevDevices::OnBnClickedDevice()
{
	m_nDevice = GetCheckedRadioButton( IDC_RDO_TABLET, IDC_RDO_GRIPPER );
	ShowHideImages();
}

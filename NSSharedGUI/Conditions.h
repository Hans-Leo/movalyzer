#ifndef _CONDITIONS_H_
#define	_CONDITIONS_H_

#include "Objects.h"

#define	TAG_CONDITION	_T("[CONDITION]")

interface INSCondition;

// This class is intended to encapsulate all view & functionality for a condition
class AFX_EXT_CLASS NSConditions : public Objects
{
	PUT_BASE( NSCondition )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();

	// Interface methods
	PUT_GET( NSCondition, ID, CString, val.AllocSysString() )

	PUT_GET( NSCondition, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( INSCondition*, CString&, BOOL fReverse = FALSE );

	PUT_GET( NSCondition, Notes, CString, val.AllocSysString() )
	PUT_GET( NSCondition, Instruction, CString, val.AllocSysString() )
	PUT_GET( NSCondition, Lex, CString, val.AllocSysString() )
	PUT_GET( NSCondition, StrokeMin, short, val );
	PUT_GET( NSCondition, StrokeMax, short, val );
	PUT_GET( NSCondition, StrokeLength, double, val );
	PUT_GET( NSCondition, StrokeDirection, double, val );
	PUT_GET( NSCondition, StrokeSkip, short, val );
	PUT_GET( NSCondition, RangeLength, double, val );
	PUT_GET( NSCondition, RangeDirection, double, val );
	PUT_GET( NSCondition, Record, BOOL, val );
	PUT_GET( NSCondition, Process, BOOL, val );
	PUT_GET( NSCondition, Parent, CString, val.AllocSysString() );
	PUT_GET( NSCondition, Word, short, val );
	PUT_GET( NSCondition, UseStimulus, BOOL, val );
	PUT_GET( NSCondition, Stimulus, CString, val.AllocSysString() );
	PUT_GET( NSCondition, StimulusWarning, CString, val.AllocSysString() );
	PUT_GET( NSCondition, StimulusPrecue, CString, val.AllocSysString() );
	PUT_GET( NSCondition, PrecueDuration, double, val );
	PUT_GET( NSCondition, PrecueLatency, double, val );
	PUT_GET( NSCondition, RecordImmediately, BOOL, val );
	PUT_GET( NSCondition, StopWrongTarget, BOOL, val );
	PUT_GET( NSCondition, StartFirstTarget, BOOL, val );
	PUT_GET( NSCondition, StopLastTarget, BOOL, val );
	PUT_GET( NSCondition, WarningDuration, double, val );
	PUT_GET( NSCondition, WarningLatency, double, val );
	PUT_GET( NSCondition, MagnetForce, double, val );
	PUT_GET( NSCondition, CountStrokes, BOOL, val );
	PUT_GET( NSCondition, HideFeedback, BOOL, val );
	PUT_GET( NSCondition, HideShowAfter, BOOL, val );
	PUT_GET( NSCondition, HideShowAfterShow, BOOL, val );
	PUT_GET( NSCondition, HideShowAfterStrokes, BOOL, val );
	PUT_GET( NSCondition, HideShowCount, double, val );
	PUT_GET( NSCondition, Transform, BOOL, val );
	PUT_GET( NSCondition, Xgain, double, val );
	PUT_GET( NSCondition, Xrotation, double, val );
	PUT_GET( NSCondition, Ygain, double, val );
	PUT_GET( NSCondition, Yrotation, double, val );
	PUT_GET( NSCondition, FlagBadTarget, BOOL, val );
	PUT_GET( NSCondition, Feedback, CString, val.AllocSysString() );
	PUT_GET( NSCondition, GripperTask, short, val );

	void PutSoundInfo( SoundCat cat, SoundType type, bool val );
	void PutSoundInfo( SoundCat cat, SoundType type, int val );
	void PutSoundInfo( SoundCat cat, SoundType type, CString val );
	static void PutSoundInfo( INSCondition*, SoundCat cat, SoundType type, bool val );
	static void PutSoundInfo( INSCondition*, SoundCat cat, SoundType type, int val );
	static void PutSoundInfo( INSCondition*, SoundCat cat, SoundType type, CString val );
	void GetSoundInfo( SoundCat cat, SoundType type, bool& val );
	void GetSoundInfo( SoundCat cat, SoundType type, int& val );
	void GetSoundInfo( SoundCat cat, SoundType type, CString& val );
	static void GetSoundInfo( INSCondition*, SoundCat cat, SoundType type, bool& val );
	static void GetSoundInfo( INSCondition*, SoundCat cat, SoundType type, int& val );
	static void GetSoundInfo( INSCondition*, SoundCat cat, SoundType type, CString& val );
	static BOOL GetSoundInfo( UINT nSound, CString szCondID, BOOL& fTone, int& nFreq,
							  int& nDur, CString& szFile, BOOL& fTrigger );

	void put_SoundScript( SoundType type, short ScriptType, CString Script );
	static void put_SoundScript( INSCondition*, SoundType type, short ScriptType, CString Script );
	void get_SoundScript( SoundType type, short& ScriptType, CString& Script );
	static void get_SoundScript( INSCondition*, SoundType type, short& ScriptType, CString& Script );

	void SetFeedbackInfo( short nWhich, BOOL fUse, short nColumn, short nStroke,
						  double dMin, double dMax, BOOL fSwapColors );
	static void SetFeedbackInfo( INSCondition*, short nWhich, BOOL fUse, short nColumn,
								 short nStroke, double dMin, double dMax, BOOL fSwapColors );
	void GetFeedbackInfo( short nWhich, BOOL& fUse, short& nColumn, short& nStroke,
						  double& dMin, double& dMax, BOOL& fSwapColors );
	static void GetFeedbackInfo( INSCondition*, short nWhich, BOOL& fUse, short& nColumn,
								 short& nStroke, double& dMin, double& dMax, BOOL& fSwapColors );

	BOOL DoEdit( CString szID, CString szExpID, CWnd* pOwner = NULL,
				 UINT iSelectPage = 0, int nEvent = -1 );
	static BOOL DoEdit( INSCondition*, CString szID, CString szExpID,
						CWnd* pOwner = NULL, UINT iSelectPage = 0, int nEvent = -1 );
	BOOL DoEditDup( CString szID, CString szExpID, CWnd* pOwner = NULL );
	static BOOL DoEditDup( INSCondition*, CString szID, CString szExpID,
						   CWnd* pOwner = NULL );
	BOOL DoReps( CString szID, CString szExpID, CWnd* pOwner = NULL );
	static BOOL DoReps( INSCondition*, CString szID, CString szExpID,
						CWnd* pOwner = NULL );

	static BOOL GetInstructionFile( CString szExpID, CString szCondID,
									CString& szFile, BOOL fNoExt = FALSE );

	// Importing/Exporting
	BOOL Export( CMemFile* pFile, BOOL fStimuliAlso = FALSE, BOOL fFirst = FALSE );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );
	// Data validation
	virtual BOOL ValidateData( CStringList* pListErrors, BOOL fFirstOnly = FALSE, BOOL fNotify = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _CONDITIONS_H_

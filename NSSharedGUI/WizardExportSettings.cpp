// WizardExportSettings.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "WizardExportSettings.h"
#include "WizardExportExp.h"
#include "WizardExportSheet.h"
#include "PasswordDlg.h"
#include <direct.h>
#include "..\DataMod\DataMod.h"
#include "Experiments.h"

// WizardExportSettings dialog

BOOL WizardExportSettings::m_fFiles = FALSE;
BOOL WizardExportSettings::m_fTrials = TRUE;
BOOL WizardExportSettings::m_fShow = FALSE;
BOOL WizardExportSettings::m_fMembers = TRUE;

IMPLEMENT_DYNAMIC(WizardExportSettings, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardExportSettings, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BN_DEFAULT, &WizardExportSettings::OnBnClickedBnDefault)
	ON_BN_CLICKED( IDC_CHK_TRIALS, OnClickTrials )
END_MESSAGE_MAP()

WizardExportSettings::WizardExportSettings() : CBCGPPropertyPage( WizardExportSettings::IDD )
{
}

WizardExportSettings::~WizardExportSettings()
{
}

void WizardExportSettings::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_LOC, m_wndFolderEdit);
	DDX_Text(pDX, IDC_EDIT_LOC, m_szFolder);
	DDX_Check(pDX, IDC_CHK_FILES, WizardExportSettings::m_fFiles);
	DDX_Check(pDX, IDC_CHK_TRIALS, WizardExportSettings::m_fTrials);
	DDX_Check(pDX, IDC_CHK_MEMBERS, WizardExportSettings::m_fMembers);
	DDX_Check(pDX, IDC_CHK_PRIVACY, WizardExportSettings::m_fShow);
	DDX_Text(pDX, IDC_TXT_STATUS, m_szStatus);
}

// WizardExportSettings message handlers

BOOL WizardExportSettings::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	m_tooltip.SetTitle( _T("Export Wizard") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_LOC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the desired location for the export files."), _T("Location") );
	pWnd = GetDlgItem( IDC_CHK_FILES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Include support files for export."), _T("Support Files") );
	pWnd = GetDlgItem( IDC_CHK_TRIALS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Include trials for export."), _T("Trials") );
	pWnd = GetDlgItem( IDC_CHK_MEMBERS );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Include subjects and groups (with or without trials) for export."), _T("Members") );
		pWnd->EnableWindow( !WizardExportSettings::m_fTrials );
	}
	pWnd = GetDlgItem( IDC_CHK_PRIVACY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Include subject identity (not ***) in export."), _T("Privacy Protection") );
	pWnd = GetDlgItem( IDC_BN_DEFAULT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to reset the default values for the location and settings."), _T("Default") );

	m_wndFolderEdit.EnableFolderBrowseButton();

	// load last location from registry
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB, szFolder;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("ExportLocation");
		::GetAllUserPath( m_szFolder );
		m_szFolder += _T("Export\\");
		szFolder = m_szFolder;
		m_szFolder = app->GetString( szKey, m_szFolder );
		if( m_szFolder == _T("") ) m_szFolder = szFolder;
		app->SetRegistryBase( szRB );
	}

	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardExportSettings::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardExportSettings::OnWizardNext() 
{
	UpdateData( TRUE );
	if( m_szFolder == _T("") )
	{
		BCGPMessageBox( _T("No path has been specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LOC );
		if( pWnd ) pWnd->SetFocus();
		return 0;
	}
	if( m_szFolder[ m_szFolder.GetLength() - 1 ] == '\\' )
		m_szFolder = m_szFolder.Left( m_szFolder.GetLength() - 1 );
	CString szTest = m_szFolder;
	szTest.MakeUpper();
	int nPos = szTest.Find( _T("\\CON\\") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Path/Export cannot contain \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LOC );
		if( pWnd ) pWnd->SetFocus();
		return 0;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Path/Export cannot contain \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_LOC );
		if( pWnd ) pWnd->SetFocus();
		return 0;
	}

	CFileFind ff;
	if( ( szTest != _T("C:") ) && ( szTest != _T("C:\\") ) &&
		( !ff.FindFile( szTest ) ) )
	{
		if( BCGPMessageBox( _T("Path does not exist. Create?"), MB_YESNO ) == IDYES )
			::CreateDirectory( szTest );
		else return 0;
	}

	// Exp Member object
	HRESULT hr = E_FAIL;
	IExperimentMember*	pEML = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEML );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		return 0;
	}

	// loop through each of the subjects and export
	m_szSummary = _T("");
	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
	WizardExportExp* pExp = (WizardExportExp*)pParent->GetPage( 0 );
	POSITION pos = pExp->m_lstSubjects.GetHeadPosition();
	CString szItem, szExp, szGrp, szSubj, szMsg;
	BOOL fRes = TRUE, fExists = FALSE;
	while( pos )
	{
		fExists = FALSE;
		szItem = pExp->m_lstSubjects.GetNext( pos );
		szExp = szItem.Left( 3 );
		szGrp = szItem.Mid( 3, 3 );
		szSubj = szItem.Right( 3 );

		m_szStatus.Format( _T("Exporting subject %s of group %s"), szSubj, szGrp );
		UpdateData( FALSE );

		// check for exp file
		szTest = m_szFolder;
		szTest += _T("\\");
		szTest += szExp;
		szTest += szGrp;
		szTest += szSubj;
		szTest += _T(".mef");
		if( ff.FindFile( szTest ) ) fExists = TRUE;
		// inform user if necessary of existing export
		if( fExists )
		{
			szMsg.Format( _T("Export file exists for subject %s of group %s.\nDo you want to continue and overwrite?"), szSubj, szGrp );
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return -1;
		}

		// export
		CBCGPWorkspace* app = ::GetWorkspaceApp();
		CString szPath, szVal, szRB;
		if( app )
		{
			CString szUser;
			::GetCurrentUser( szUser );
			szVal.Format( _T("Settings\\%s"), szUser );
			szRB = app->GetRegistryBase();
			app->SetRegistryBase( szVal );
			::GetAllUserPath( szPath );
			szPath += _T("Export\\");
			szVal = app->GetString( _T("ExportLocation"), szPath );
			if( szVal != _T("") ) szPath = szVal;
		}
		else ::GetAllUserPath( szVal );
		fRes &= Experiments::Export( m_szSummary, szExp, szVal, szSubj, szGrp, TRUE,
									 m_fFiles, m_fTrials, m_fMembers, m_fShow, m_szFolder );
		if( app )
		{
			app->WriteString( _T("ExportLocation"), szVal );
			app->SetRegistryBase( szRB );
		}

		m_szSummary += _T("\n\n");

		if( SUCCEEDED( pEML->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() ) ) )
		{
			pEML->put_Exported( TRUE );
			pEML->put_ExpWhen( COleDateTime::GetCurrentTime() );
			pEML->put_ExpWhere( m_szFolder.AllocSysString() );
			HRESULT hr = pEML->Modify();
		}
	}

	if( !fRes ) BCGPMessageBox( _T("There were some errors.\nPlease review the results page for further information.") );

	pEML->Release();

	// set export location to registry
	CBCGPWorkspace* app = ::GetWorkspaceApp();
	if( app )
	{
		CString szKey, szVal, szUser, szRB;
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = app->GetRegistryBase();
		app->SetRegistryBase( szVal );
		szKey = _T("ExportLocation");
		app->WriteString( szKey, m_szFolder );
		app->SetRegistryBase( szRB );
	}

	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardExportSettings::OnSetActive() 
{
//	WizardExportSheet* pParent = (WizardExportSheet*)GetParent();
//	POSITION pos = m_list.GetFirstSelectedItemPosition();
//	if( pos ) pParent->SetWizardButtons( PSWIZB_NEXT );
//	else pParent->SetWizardButtons( 0 );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardExportSettings::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("export_wizard.html"), this );
	return TRUE;
}

BOOL WizardExportSettings::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardExportSettings::OnBnClickedBnDefault()
{
	::GetAllUserPath( m_szFolder );
	m_szFolder += _T("Export\\");
	UpdateData( FALSE );
}

void WizardExportSettings::OnClickTrials()
{
	UpdateData( TRUE );

	CWnd* pWndM = GetDlgItem( IDC_CHK_MEMBERS );
	CWnd* pWndP = GetDlgItem( IDC_CHK_PRIVACY );

	if( WizardExportSettings::m_fTrials ) WizardExportSettings::m_fMembers = TRUE;
	UpdateData( FALSE );

	pWndM->EnableWindow( !WizardExportSettings::m_fTrials );
	pWndP->EnableWindow( WizardExportSettings::m_fTrials || WizardExportSettings::m_fMembers );
}

void WizardExportSettings::OnClickMembers()
{
	UpdateData( TRUE );

	CWnd* pWndP = GetDlgItem( IDC_CHK_PRIVACY );
	pWndP->EnableWindow( WizardExportSettings::m_fTrials || WizardExportSettings::m_fMembers );
}

// ExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ExportDlg.h"
#include "PasswordDlg.h"
#include "direct.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExportDlg dialog

BOOL ExportDlg::m_fFiles = TRUE;
BOOL ExportDlg::m_fTrials = TRUE;
BOOL ExportDlg::m_fShow = FALSE;
BOOL ExportDlg::m_fMembers = TRUE;

BEGIN_MESSAGE_MAP(ExportDlg, CBCGPDialog)
	ON_BN_CLICKED( IDC_CHK_TRIALS, OnClickTrials )
	ON_BN_CLICKED( IDC_CHK_MEMBERS, OnClickMembers )
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

ExportDlg::ExportDlg(CWnd* pParent) : CBCGPDialog(ExportDlg::IDD, pParent)
{
	m_szFile = _T("");
}

void ExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_FILE, m_szFile);
	DDX_Control(pDX, IDC_EDIT_FILE, m_wndFolderEdit);
	DDX_Check(pDX, IDC_CHK_FILES, ExportDlg::m_fFiles);
	DDX_Check(pDX, IDC_CHK_TRIALS, ExportDlg::m_fTrials);
	DDX_Check(pDX, IDC_CHK_MEMBERS, ExportDlg::m_fMembers);
	DDX_Check(pDX, IDC_CHK_PRIVACY, ExportDlg::m_fShow);
}

/////////////////////////////////////////////////////////////////////////////
// ExportDlg message handlers

BOOL ExportDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify export location here."), _T("Location") );
	pWnd = GetDlgItem( IDC_CHK_FILES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Export the support files. These files include instructions, sounds, images, attachments, and sorting."), _T("Support Files") );
	pWnd = GetDlgItem( IDC_CHK_TRIALS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Include trials (and associated subjects/groups) for export."), _T("Trials") );
	pWnd = GetDlgItem( IDC_CHK_MEMBERS );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Include subjects and groups (with or without trials) for export."), _T("Members") );
		pWnd->EnableWindow( !ExportDlg::m_fTrials );
	}
	pWnd = GetDlgItem( IDC_CHK_PRIVACY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Include subject identity (not ***) in export."), _T("Privacy Protection") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Export to specified location."), _T("Export") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Cancel export."), _T("Cancel") );

	// Title
	if( m_szExp == _T("") )
	{
		SetWindowText( _T("Specify Export Location") );
	}
	else
	{
		CString szTmp;
		szTmp.Format( _T("Specify Export Location for Experiment %s"), m_szExp );
		SetWindowText( szTmp );
	}

	m_wndFolderEdit.EnableFolderBrowseButton();

	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ExportDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void ExportDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szFile == _T("") )
	{
		BCGPMessageBox( _T("No path has been specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	if( m_szFile[ m_szFile.GetLength() - 1 ] == '\\' )
		m_szFile = m_szFile.Left( m_szFile.GetLength() - 1 );
	CString szTest = m_szFile;
	szTest.MakeUpper();
	int nPos = szTest.Find( _T("\\CON\\") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Path/Export cannot contain \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos != -1 )
	{
		BCGPMessageBox( _T("Path/Export cannot contain \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	// if "show subject identity" is checked, we must turn off privacy temporarily
	if( m_fShow ) ::SetIsPrivacyOn( FALSE );

	CFileFind ff;
	if( ( szTest != _T("C:") ) && ( szTest != _T("C:\\") ) &&
		( szTest != _T("D:") ) && ( szTest != _T("D:\\") ) &&
		( szTest != _T("E:") ) && ( szTest != _T("E:\\") ) &&
		( !ff.FindFile( szTest ) ) )
	{
		if( BCGPMessageBox( _T("Path does not exist. Create?"), MB_YESNO ) == IDYES )
		{
			int nRes, nPos = 0;
			CString szTmp;

			szTest += _T("\\");
			nPos = szTest.Find( _T("\\") );
			nPos = szTest.Find( _T("\\"), nPos + 1 );
			while( nPos != -1 )
			{
				szTmp = szTest.Left( nPos );
				if( !ff.FindFile( szTmp ) )
				{
					nRes = _mkdir( szTmp );
					if( ( nRes != 0 ) && ( errno != EEXIST ) )
					{
						BCGPMessageBox( _T("Error creating directory.") );
						return;
					}
				}

				nPos = szTest.Find( _T("\\"), nPos + 1 );
			}
		}
		else return;
	}

	// check for exp file
	szTest = m_szFile;
	szTest += _T("\\");
	szTest += m_szExp;
	if( ( m_szGrp != _T("") ) && ( m_szSubj != _T("") ) )
	{
		szTest += m_szGrp;
		szTest += m_szSubj;
	}
	szTest += _T(".MEF");
	if( ff.FindFile( szTest ) )
	{
		if( BCGPMessageBox( _T("File exists.\nDo you want to continue and overwrite?"), MB_YESNO ) == IDNO )
			return;
	}

	CBCGPDialog::OnOK();
}

BOOL ExportDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("importexport.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void ExportDlg::OnClickTrials()
{
	UpdateData( TRUE );

	CWnd* pWndM = GetDlgItem( IDC_CHK_MEMBERS );
	CWnd* pWndP = GetDlgItem( IDC_CHK_PRIVACY );

	if( ExportDlg::m_fTrials ) ExportDlg::m_fMembers = TRUE;
	UpdateData( FALSE );

	pWndM->EnableWindow( !ExportDlg::m_fTrials );
	pWndP->EnableWindow( ExportDlg::m_fTrials || ExportDlg::m_fMembers );
}

void ExportDlg::OnClickMembers()
{
	UpdateData( TRUE );

	CWnd* pWndP = GetDlgItem( IDC_CHK_PRIVACY );
	pWndP->EnableWindow( ExportDlg::m_fTrials || ExportDlg::m_fMembers );
}

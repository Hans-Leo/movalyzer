// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#define _BIND_TO_CURRENT_CRT_VERSION	1
#define _BIND_TO_CURRENT_MFC_VERSION	1

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#include <afxcmn.h>			// MFC support for Windows Common Controls

#include "..\Common\Macros.h"
#include "..\Common\Deffun.h"
#include "Globals.h"
#include "..\Common\Security.h"

#define	_BCGCB_HIDE_AUTOLINK_OUTPUT_
#include "BCGCBProInc.h"	// bcg toolbars

#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")

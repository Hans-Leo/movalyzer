// ProcSetPageREExt.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ProcSetPageREExt.h"
#include "..\DataMod\DataMod.h"
#include "ProcSetSheet.h"
#include "EditorDlg.h"
#include "Experiments.h"
#include <direct.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static BOOL _fCheckedMatlab = FALSE;
static BOOL _fMatlabInstalled = FALSE;
static BOOL _fInformedMatlab = FALSE;

BOOL CheckedMatlab() { return _fCheckedMatlab; }
void SetCheckedMatlab() { _fCheckedMatlab = TRUE; }
BOOL MatlabInstalled() { return _fMatlabInstalled; }
void SetMatlabInstalled() { _fMatlabInstalled = TRUE; }

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREExt property page

IMPLEMENT_DYNCREATE(ProcSetPageREExt, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ProcSetPageREExt, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_CBN_SELCHANGE(IDC_CBO_EXTAPP_TYPE_RAW, OnChangeType)
	ON_CBN_SELCHANGE(IDC_CBO_EXTAPP_TYPE_TF, OnChangeType)
	ON_CBN_SELCHANGE(IDC_CBO_EXTAPP_TYPE_SEG, OnChangeType)
	ON_CBN_SELCHANGE(IDC_CBO_EXTAPP_TYPE_EXT, OnChangeType)
	ON_BN_CLICKED(IDC_BN_EDIT_RAW, OnClickEditRaw)
	ON_BN_CLICKED(IDC_BN_EDIT_TF, OnClickEditTF)
	ON_BN_CLICKED(IDC_BN_EDIT_SEG, OnClickEditSeg)
	ON_BN_CLICKED(IDC_BN_EDIT_EXT, OnClickEditExt)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnReset)
END_MESSAGE_MAP()

ProcSetPageREExt::ProcSetPageREExt( IProcSetting* pS )
	: CBCGPPropertyPage(ProcSetPageREExt::IDD), m_pPS( pS )
{
	m_nExtTypeRaw = 0;
	m_nExtTypeTF = 0;
	m_nExtTypeSeg = 0;
	m_nExtTypeExt = 0;

	if( m_pPS )
	{
		IProcSetRunExp* pPSRE = NULL;
		HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
		if( SUCCEEDED( hr ) )
		{
			BSTR bstr = NULL;
			ExternalAppType nVal = (ExternalAppType)0;

			pPSRE->get_ExtAppTypeRaw( &nVal );
			m_nExtTypeRaw = nVal;
			pPSRE->get_ExtAppScriptRaw( &bstr );
			m_szExtScriptRaw = bstr;

			pPSRE->get_ExtAppTypeTF( &nVal );
			m_nExtTypeTF = nVal;
			pPSRE->get_ExtAppScriptTF( &bstr );
			m_szExtScriptTF = bstr;

			pPSRE->get_ExtAppTypeSeg( &nVal );
			m_nExtTypeSeg = nVal;
			pPSRE->get_ExtAppScriptSeg( &bstr );
			m_szExtScriptSeg = bstr;

			pPSRE->get_ExtAppTypeExt( &nVal );
			m_nExtTypeExt = nVal;
			pPSRE->get_ExtAppScriptExt( &bstr );
			m_szExtScriptExt = bstr;

			pPSRE->Release();
		}
	}
}

ProcSetPageREExt::~ProcSetPageREExt()
{
}

void ProcSetPageREExt::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_EXTAPP_TYPE_RAW, m_cboETRaw);
	DDX_CBString(pDX, IDC_CBO_EXTAPP_SCRIPT_RAW, m_szExtScriptRaw);
	DDX_Control(pDX, IDC_CBO_EXTAPP_SCRIPT_RAW, m_cboESRaw);
	DDX_Control(pDX, IDC_CBO_EXTAPP_TYPE_TF, m_cboETTF);
	DDX_CBString(pDX, IDC_CBO_EXTAPP_SCRIPT_TF, m_szExtScriptTF);
	DDX_Control(pDX, IDC_CBO_EXTAPP_SCRIPT_TF, m_cboESTF);
	DDX_Control(pDX, IDC_CBO_EXTAPP_TYPE_SEG, m_cboETSeg);
	DDX_CBString(pDX, IDC_CBO_EXTAPP_SCRIPT_SEG, m_szExtScriptSeg);
	DDX_Control(pDX, IDC_CBO_EXTAPP_SCRIPT_SEG, m_cboESSeg);
	DDX_Control(pDX, IDC_CBO_EXTAPP_TYPE_EXT, m_cboETExt);
	DDX_CBString(pDX, IDC_CBO_EXTAPP_SCRIPT_EXT, m_szExtScriptExt);
	DDX_Control(pDX, IDC_CBO_EXTAPP_SCRIPT_EXT, m_cboESExt);
	DDX_Control(pDX, IDC_BN_EDIT_RAW, m_bnEditRaw);
	DDX_Control(pDX, IDC_BN_EDIT_TF, m_bnEditTF);
	DDX_Control(pDX, IDC_BN_EDIT_SEG, m_bnEditSeg);
	DDX_Control(pDX, IDC_BN_EDIT_EXT, m_bnEditExt);
}

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageREExt message handlers

BOOL ProcSetPageREExt::OnApply() 
{
	if( !m_pPS ) return FALSE;
	UpdateData( TRUE );

	// verify
	if( ( m_nExtTypeRaw != eEAT_None ) && ( m_szExtScriptRaw == _T("") ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 12 );
		BCGPMessageBox( _T("You must specify a script for the RAW file."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_RAW );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nExtTypeTF != eEAT_None ) && ( m_szExtScriptTF == _T("") ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 12 );
		BCGPMessageBox( _T("You must specify a script for the TF file."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_TF );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nExtTypeSeg != eEAT_None ) && ( m_szExtScriptSeg == _T("") ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 12 );
		BCGPMessageBox( _T("You must specify a script for the SEG file."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_SEG );
		pWnd->SetFocus();
		return FALSE;
	}
	if( ( m_nExtTypeExt != eEAT_None ) && ( m_szExtScriptExt == _T("") ) )
	{
		ProcSetSheet* pParent = (ProcSetSheet*)GetParent();
		pParent->SetActivePage( 12 );
		BCGPMessageBox( _T("You must specify a script for the EXT file."), MB_OK | MB_ICONEXCLAMATION );
		CWnd* pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_EXT );
		pWnd->SetFocus();
		return FALSE;
	}

	// save
	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( SUCCEEDED( hr ) )
	{
		pPSRE->put_ExtAppTypeRaw( (ExternalAppType)m_nExtTypeRaw );
		pPSRE->put_ExtAppScriptRaw( m_szExtScriptRaw.AllocSysString() );

		pPSRE->put_ExtAppTypeTF( (ExternalAppType)m_nExtTypeTF );
		pPSRE->put_ExtAppScriptTF( m_szExtScriptTF.AllocSysString() );

		pPSRE->put_ExtAppTypeSeg( (ExternalAppType)m_nExtTypeSeg );
		pPSRE->put_ExtAppScriptSeg( m_szExtScriptSeg.AllocSysString() );

		pPSRE->put_ExtAppTypeExt( (ExternalAppType)m_nExtTypeExt );
		pPSRE->put_ExtAppScriptExt( m_szExtScriptExt.AllocSysString() );

		pPSRE->Release();
	}

	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet ) pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ProcSetPageREExt::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ProcSetPageREExt::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_EXP );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXTAPP_TYPE_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the external application type to process the raw data file.\nSelect 'NONE' for no action."), _T("External Application Type") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_TYPE_TF );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the external application type to process the time function file.\nSelect 'NONE' for no action."), _T("External Application Type") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_TYPE_SEG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the external application type to process the segmentation file.\nSelect 'NONE' for no action."), _T("External Application Type") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_TYPE_EXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the external application type to process the feature extraction file.\nSelect 'NONE' for no action."), _T("External Application Type") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the script to run on the raw data file."), _T("Script") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_TF );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the script to run on the time function file."), _T("Script") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_SEG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the script to run on the segmentation file."), _T("Script") );
	pWnd = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_EXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the script to run on the feature extraction file."), _T("Script") );
	pWnd = GetDlgItem( IDC_BN_EDIT_RAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the raw data file processing script."), _T("Edit Script") );
	pWnd = GetDlgItem( IDC_BN_EDIT_TF );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the time function file processing script."), _T("Edit Script") );
	pWnd = GetDlgItem( IDC_BN_EDIT_SEG );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the segmentation file processing script."), _T("Edit Script") );
	pWnd = GetDlgItem( IDC_BN_EDIT_EXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to edit the feature extraction file processing script."), _T("Edit Script") );

	// button images
	m_bnEditRaw.SetImage( IDB_EDIT );
	m_bnEditTF.SetImage( IDB_EDIT );
	m_bnEditSeg.SetImage( IDB_EDIT );
	m_bnEditExt.SetImage( IDB_EDIT );

	// fill type combos
	m_cboETRaw.AddString( _T("NONE") );
	m_cboETRaw.SetItemData( 0, eEAT_None );
	m_cboETRaw.AddString( _T("Matlab") );
	m_cboETRaw.SetItemData( 1, eEAT_Matlab );
	m_cboETRaw.AddString( _T("Batch") );
	m_cboETRaw.SetItemData( 2, eEAT_Batch );
	m_cboETRaw.SetCurSel( m_nExtTypeRaw );
	m_cboETTF.AddString( _T("NONE") );
	m_cboETTF.SetItemData( 0, eEAT_None );
	m_cboETTF.AddString( _T("Matlab") );
	m_cboETTF.SetItemData( 1, eEAT_Matlab );
	m_cboETTF.AddString( _T("Batch") );
	m_cboETTF.SetItemData( 2, eEAT_Batch );
	m_cboETTF.SetCurSel( m_nExtTypeTF );
	m_cboETSeg.AddString( _T("NONE") );
	m_cboETSeg.SetItemData( 0, eEAT_None );
	m_cboETSeg.AddString( _T("Matlab") );
	m_cboETSeg.SetItemData( 1, eEAT_Matlab );
	m_cboETSeg.AddString( _T("Batch") );
	m_cboETSeg.SetItemData( 2, eEAT_Batch );
	m_cboETSeg.SetCurSel( m_nExtTypeSeg );
	m_cboETExt.AddString( _T("NONE") );
	m_cboETExt.SetItemData( 0, eEAT_None );
	m_cboETExt.AddString( _T("Matlab") );
	m_cboETExt.SetItemData( 1, eEAT_Matlab );
	m_cboETExt.AddString( _T("Batch") );
	m_cboETExt.SetItemData( 2, eEAT_Batch );
	m_cboETExt.SetCurSel( m_nExtTypeExt );

	// check for matlab installation if not already done
	_fInformedMatlab = FALSE;
	if( ::IsOA() && !_fCheckedMatlab )
	{
		if( BCGPMessageBox( _T("MovAlyzeR will now check to see if Matlab is installed.\nClick OK to begin, or Cancel to skip the check."), MB_OKCANCEL ) == IDOK )
		{
			_fMatlabInstalled = Experiments::IsMatlabInstalled();
			_fCheckedMatlab = TRUE;
			CString szMsg = _fMatlabInstalled ? _T("Matlab is installed and can be used as an external application.") :
												_T("Matlab is not installed or is an unsupported version.");
			BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		}
	}
	else if( !::IsOA() ) _fCheckedMatlab = TRUE;

	// fill script lists
	FillScriptLists();

	// enable/disable
	EnableDisable();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ProcSetPageREExt::FillScriptLists()
{
	if( !::IsOA() ) return;

	// empty combo content
	m_cboESRaw.ResetContent();
	m_cboESTF.ResetContent();
	m_cboESSeg.ResetContent();
	m_cboESExt.ResetContent();

	// base path
	CString szPath;
	::GetDataPathRoot( szPath );
	szPath += _T("\\scripts\\");
	// ensure exists
	_mkdir( szPath );

	// declare vars & setup search types
	CString szFindMatlab = szPath, szFindBatch = szPath, szFind, szFile;
	szFindMatlab += _T("*.M");
	szFindBatch += _T("*.BAT");
	int nSel = -1, nItem = -1;
	CFileFind ff;
	BOOL fCont = FALSE, fMatlab = FALSE;
	CStringList lstMatlab, lstBatch, *pList = NULL;
	POSITION pos = NULL;

	// get files for search types
	// matlab
	fCont = ff.FindFile( szFindMatlab );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		if( !ff.IsDirectory() ) lstMatlab.AddTail( szFile );
	}
	// batch
	fCont = ff.FindFile( szFindBatch );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		if( !ff.IsDirectory() ) lstBatch.AddTail( szFile );
	}

	// NOW FILL COMBOS
	// RAW
	nSel = -1;
	if( m_nExtTypeRaw == eEAT_Matlab ) { pList = &lstMatlab; fMatlab = TRUE; }
	else if( m_nExtTypeRaw == eEAT_Batch ) pList = &lstBatch;
	else pList = NULL;
	pos = pList ? pList->GetHeadPosition() : NULL;
	while( pos )
	{
		szFile = pList->GetNext( pos );
		nItem = m_cboESRaw.AddString( szFile );
		if( szFile == m_szExtScriptRaw ) nSel = nItem;
	}
	m_cboESRaw.SetCurSel( nSel );

	// TF
	nSel = -1;
	if( m_nExtTypeTF == eEAT_Matlab ) { pList = &lstMatlab; fMatlab = TRUE; }
	else if( m_nExtTypeTF == eEAT_Batch ) pList = &lstBatch;
	else pList = NULL;
	pos = pList ? pList->GetHeadPosition() : NULL;
	while( pos )
	{
		szFile = pList->GetNext( pos );
		nItem = m_cboESTF.AddString( szFile );
		if( szFile == m_szExtScriptTF ) nSel = nItem;
	}
	m_cboESTF.SetCurSel( nSel );

	// SEG
	nSel = -1;
	if( m_nExtTypeSeg == eEAT_Matlab ) { pList = &lstMatlab; fMatlab = TRUE; }
	else if( m_nExtTypeSeg == eEAT_Batch ) pList = &lstBatch;
	else pList = NULL;
	pos = pList ? pList->GetHeadPosition() : NULL;
	while( pos )
	{
		szFile = pList->GetNext( pos );
		nItem = m_cboESSeg.AddString( szFile );
		if( szFile == m_szExtScriptSeg ) nSel = nItem;
	}
	m_cboESSeg.SetCurSel( nSel );

	// EXT
	nSel = -1;
	if( m_nExtTypeExt == eEAT_Matlab ) { pList = &lstMatlab; fMatlab = TRUE; }
	else if( m_nExtTypeExt == eEAT_Batch ) pList = &lstBatch;
	else pList = NULL;
	pos = pList ? pList->GetHeadPosition() : NULL;
	while( pos )
	{
		szFile = pList->GetNext( pos );
		nItem = m_cboESExt.AddString( szFile );
		if( szFile == m_szExtScriptExt ) nSel = nItem;
	}
	m_cboESExt.SetCurSel( nSel );

	// if any have matlab scripts and matlab is not installed, inform
	if( fMatlab && !_fMatlabInstalled && !_fInformedMatlab && _fCheckedMatlab )
	{
		BCGPMessageBox( _T("At least one of your events is using a Matlab script.\nMatlab is either not installed or not accessible.\nThe scripts will produce errors during processing."), MB_OK | MB_ICONWARNING );
		_fInformedMatlab = TRUE;
	}
}

void ProcSetPageREExt::EnableDisable()
{
	CWnd* pWndT = GetDlgItem( IDC_CBO_EXTAPP_TYPE_RAW );
	CWnd* pWndS = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_RAW );
	CWnd* pWndB = GetDlgItem( IDC_BN_EDIT_RAW );
	BOOL fAvail = ::IsOA();
	BOOL fEnable = ( m_nExtTypeRaw != eEAT_None ) && fAvail;
	pWndT->EnableWindow( fAvail );
	pWndS->EnableWindow( fEnable );
	pWndB->EnableWindow( fEnable );
	if( !fEnable ) m_cboESRaw.SetCurSel( -1 );

	pWndT = GetDlgItem( IDC_CBO_EXTAPP_TYPE_TF );
	pWndS = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_TF );
	pWndB = GetDlgItem( IDC_BN_EDIT_TF );
	fEnable = ( m_nExtTypeTF != eEAT_None ) && fAvail;
	pWndT->EnableWindow( fAvail );
	pWndS->EnableWindow( fEnable );
	pWndB->EnableWindow( fEnable );
	if( !fEnable ) m_cboESTF.SetCurSel( -1 );

	pWndT = GetDlgItem( IDC_CBO_EXTAPP_TYPE_SEG );
	pWndS = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_SEG );
	pWndB = GetDlgItem( IDC_BN_EDIT_SEG );
	fEnable = ( m_nExtTypeSeg != eEAT_None ) && fAvail;
	pWndT->EnableWindow( fAvail );
	pWndS->EnableWindow( fEnable );
	pWndB->EnableWindow( fEnable );
	if( !fEnable ) m_cboESSeg.SetCurSel( -1 );

	pWndT = GetDlgItem( IDC_CBO_EXTAPP_TYPE_EXT );
	pWndS = GetDlgItem( IDC_CBO_EXTAPP_SCRIPT_EXT );
	pWndB = GetDlgItem( IDC_BN_EDIT_EXT );
	fEnable = ( m_nExtTypeExt != eEAT_None ) && fAvail;
	pWndT->EnableWindow( fAvail );
	pWndS->EnableWindow( fEnable );
	pWndB->EnableWindow( fEnable );
	if( !fEnable ) m_cboESExt.SetCurSel( -1 );
}

BOOL ProcSetPageREExt::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ProcSetPageREExt::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("experimentsettings_processing_external.html"), this );
	return TRUE;
}

void ProcSetPageREExt::OnChangeType()
{
	m_nExtTypeRaw = m_cboETRaw.GetItemData( m_cboETRaw.GetCurSel() );
	m_nExtTypeTF = m_cboETTF.GetItemData( m_cboETTF.GetCurSel() );
	m_nExtTypeSeg = m_cboETSeg.GetItemData( m_cboETSeg.GetCurSel() );
	m_nExtTypeExt = m_cboETExt.GetItemData( m_cboETExt.GetCurSel() );

	if( ( m_nExtTypeRaw == eEAT_Matlab ) || ( m_nExtTypeTF == eEAT_Matlab ) ||
		( m_nExtTypeSeg == eEAT_Matlab ) || ( m_nExtTypeExt == eEAT_Matlab ) )
	{
		if( !_fCheckedMatlab )
		{
			BCGPMessageBox( _T("The availability of Matlab has not been confirmed.\nYou will have to close the experiment properties and reopen.\nFinally, you will need to agree to checking the Matlab is installed."), MB_OK | MB_ICONWARNING );
		}
		else if( !_fMatlabInstalled && !_fInformedMatlab )
		{
			BCGPMessageBox( _T("Matlab is either not installed or not accessible.\nThe script will produce errors during processing."), MB_OK | MB_ICONWARNING );
			_fInformedMatlab = TRUE;
		}
	}

	FillScriptLists();
	EnableDisable();
}

void ProcSetPageREExt::OnClickEditRaw()
{
	UpdateData( TRUE );
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet )
	{
		EditorDlg d( this );
		d.m_szExpID = pSheet->m_szID;
		d.m_nType = m_nExtTypeRaw;
		d.m_szFile = m_szExtScriptRaw;
		if( d.DoModal() == IDOK ) FillScriptLists();
	}
}

void ProcSetPageREExt::OnClickEditTF()
{
	UpdateData( TRUE );
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet )
	{
		EditorDlg d( this );
		d.m_szExpID = pSheet->m_szID;
		d.m_nType = m_nExtTypeTF;
		d.m_szFile = m_szExtScriptTF;
		if( d.DoModal() == IDOK ) FillScriptLists();
	}
}

void ProcSetPageREExt::OnClickEditSeg()
{
	UpdateData( TRUE );
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet )
	{
		EditorDlg d( this );
		d.m_szExpID = pSheet->m_szID;
		d.m_nType = m_nExtTypeSeg;
		d.m_szFile = m_szExtScriptSeg;
		if( d.DoModal() == IDOK ) FillScriptLists();
	}
}

void ProcSetPageREExt::OnClickEditExt()
{
	UpdateData( TRUE );
	ProcSetSheet* pSheet = (ProcSetSheet*)this->GetParent();
	if( pSheet )
	{
		EditorDlg d( this );
		d.m_szExpID = pSheet->m_szID;
		d.m_nType = m_nExtTypeExt;
		d.m_szFile = m_szExtScriptExt;
		if( d.DoModal() == IDOK ) FillScriptLists();
	}
}

void ProcSetPageREExt::OnBnReset() 
{
	m_nExtTypeRaw = 0;
	m_nExtTypeTF = 0;
	m_nExtTypeSeg = 0;
	m_nExtTypeExt = 0;

	UpdateData( FALSE );
}

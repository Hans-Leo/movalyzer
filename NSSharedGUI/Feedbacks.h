#ifndef _FEEDBACKS_H_
#define	_FEEDBACKS_H_

#include "Objects.h"

#define	TAG_FEEDBACK	_T("[FEEDBACK]")

interface IFeedback;

// This class is intended to encapsulate all view & functionality for an element
class AFX_EXT_CLASS Feedbacks : public Objects
{
	PUT_BASE( Feedback )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();
	// Interface methods
	PUT_GET( Feedback, ID, CString, val.AllocSysString() )

	PUT_GET( Feedback, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( IFeedback*, CString&, BOOL fReverse = FALSE );

	PUT_GET( Feedback, Feature, CString, val.AllocSysString() )
	PUT_GET( Feedback, MinColor, DWORD, val )
	PUT_GET( Feedback, MaxColor, DWORD, val )

	// Importing/Exporting
	BOOL Export( CMemFile* pFile, BOOL fFirst = FALSE );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _FEEDBACKS_H_

#pragma once

// ProcSetPageTF.h : header file
//

#include "NSTooltipCtrl.h"
#include "CustomControls.h"

/////////////////////////////////////////////////////////////////////////////
// ProcSetPageTF dialog

interface IProcSetting;

class AFX_EXT_CLASS ProcSetPageTF : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(ProcSetPageTF)

// Construction
public:
	ProcSetPageTF( IProcSetting* pPS = NULL );
	~ProcSetPageTF();

// Dialog Data
	enum { IDD = IDD_PAGE_PS_TF };
	double			m_dFilterFreq;
	CColoredEdit	m_editFF;
	int				m_nDecimate;
	CColoredEdit	m_editDec;
	BOOL			m_fU;
	CColoredButton	m_chkU;
	BOOL			m_fR;
	BOOL			m_fJ;
	BOOL			m_fA;
	BOOL			m_fO;
	double			m_dRotBeta;
	CColoredEdit	m_editRB;
	BOOL			m_fFFT;
	double			m_dSharpness;
	CColoredEdit	m_editSharpness;
	int				m_nUpSampleFactor;
	CColoredEdit	m_editUSF;
	BOOL			m_fRemTrailLift;
	CColoredButton	m_chkRem;
	IProcSetting*	m_pPS;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_linkAdv;
	CBCGPButton		m_bnCalc;

// Overrides
public:
	virtual BOOL OnApply();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnBnReset();
	afx_msg void OnClickAdvanced();
	afx_msg void OnBnClickedRdoFft();
	afx_msg void OnBnClickedRdoNotfft();
	afx_msg void OnBnClickedBnCalc();
	DECLARE_MESSAGE_MAP()
};

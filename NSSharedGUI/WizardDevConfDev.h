#pragma once

#include "NSTooltipCtrl.h"

// WizardDevConfDev dialog

class AFX_EXT_CLASS WizardDevConfDev : public CBCGPPropertyPage
{
	DECLARE_DYNAMIC(WizardDevConfDev)

public:
	WizardDevConfDev();
	virtual ~WizardDevConfDev();

// Dialog Data
	enum { IDD = IDD_WIZ_DEV_DEVCONF };
	CString			m_szMPP;
	int				m_nRate;
	int				m_nPressure;
	double			m_dResolution;
	double			m_dHeightTablet;
	double			m_dWidthTablet;
	CString			m_szDev;
	CString			m_szDim;
	CString			m_szMaxPP;
	NSToolTipCtrl	m_tooltip;
	CBCGPButton		m_bnCalc;

public:
	virtual BOOL PreTranslateMessage( MSG*  );
	virtual LRESULT OnWizardBack();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	BOOL DoHelp( HELPINFO* );
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
protected:
	void ShowHideImages();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBnAcquire();
	afx_msg void OnBnClickedBnCalc();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);

	DECLARE_MESSAGE_MAP()
};

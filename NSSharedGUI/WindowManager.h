#pragma once

// msg type (as opposed to, eg, WM_COMMAND
#define	WM_WINDOWMANAGER			(WM_USER + 1040)

// msg types (hiword of wparam)
#define	MSG_WND_NONE				0
#define	MSG_WND_GRAPH				1

// msg subtypes (loword of wparam)
#define	MSG_WND_SUB_NONE			0
#define	MSG_WND_SUB_CURSOR			1
#define	MSG_WND_SUB_CURSOR_EXT		2

class AFX_EXT_CLASS WindowManager
{
// construction/destruction
public:
	WindowManager();
	virtual ~WindowManager();

// attributes
protected:
	CObList		m_lstItems;

// methods
public:
	// post message: hiword of msg = msg type, loword = sub-msg (wparam)
	// val = content for message: can be value or pointer (dependant upon message)
	virtual void PostMessage( WPARAM msg, LPARAM val, CWnd* pWndCaller );

	BOOL AddItem( CWnd* pWnd );
	BOOL RemoveItem( CWnd* pWnd );
	virtual void Clear( BOOL fDestroy = FALSE );
	CWnd* GetFirst() { return (CWnd*)m_lstItems.GetHeadPosition(); }
};

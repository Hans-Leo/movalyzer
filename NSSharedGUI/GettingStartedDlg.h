#pragma once

// GettingStartedDlg dialog

#include <Mshtml.h>
#include <afxdhtml.h>        // HTML Dialogs

class StartView;

class AFX_EXT_CLASS GettingStartedDlg : public CDHtmlDialog
{
	DECLARE_DYNAMIC(GettingStartedDlg)

public:
	GettingStartedDlg( CWnd* pParent = NULL, BOOL fModeless = FALSE );
	virtual ~GettingStartedDlg() {}

	// Dialog Data
	enum { IDD = IDD_HTML, IDH = IDR_GETTINGS_STARTED };
	BOOL	m_fModeless;
	BOOL	m_fShow;

	// operations
public:
	static GettingStartedDlg* GetGettingStartedDlg();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnDocumentComplete(LPDISPATCH pDisp, LPCTSTR szUrl);
	virtual void OnCancel();
	virtual void PostNcDestroy();

public:
	HRESULT OnClickShow( IHTMLElement* );
	HRESULT OnClickVidLink( IHTMLElement* );
	HRESULT OnClickTutLink( IHTMLElement* );
	DECLARE_MESSAGE_MAP()
	DECLARE_DHTML_EVENT_MAP()
};

// PasswordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "PasswordChgDlg.h"
#include "PasswordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PasswordDlg dialog

BEGIN_MESSAGE_MAP( PasswordDlg, CBCGPDialog )
	ON_BN_CLICKED( IDC_BN_PASS, OnBnPass )
END_MESSAGE_MAP()

PasswordDlg::PasswordDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(PasswordDlg::IDD, pParent)
{
	m_fSubject = FALSE;
	m_szPass = _T("");
	m_fPassChange = FALSE;
	m_fUser = FALSE;
}

void PasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PASS, m_szPass);
	DDX_Text(pDX, IDC_TXT_DEFAULT, m_szDefault);
	DDX_Control(pDX, IDC_TXT_HELP, m_txtHelp);
}

/////////////////////////////////////////////////////////////////////////////
// PasswordDlg message handlers

BOOL PasswordDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// help string URL
	m_txtHelp.SetURL( _T("http://www.neuroscript.net/forum/showthread.php?t=859") );
	m_txtHelp.SizeToContent();

	// default password (if not subject/user login)
	if( !m_fSubject )
	{
		m_szDefault = _T("DEFAULT = ");
		CString szUser;
		::GetCurrentUser( szUser );
		if( szUser.Left( 2 ) == _T("UU") ) m_szDefault += _T("empty");
		else m_szDefault += _T("\"userpass\"");
		UpdateData( FALSE );
	}

	// if user login, change title
	if( m_fUser && ( m_szUserID != _T("") ) )
	{
		CString szTitle;
		GetWindowText( szTitle );
		szTitle += _T(" for User ");
		szTitle += m_szUserID;
		SetWindowText( szTitle );
	}

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_SUBJ );
	m_tooltip.SetTitle( _T("PASSWORD") );
	CWnd* pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Accept changes."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close without saving."), _T("Cancel") );
	pWnd = GetDlgItem( IDC_EDIT_PASS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter password here."), _T("Password") );
 	pWnd = GetDlgItem( IDC_BN_PASS );
// 	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to change the current password."), _T("Change Password") );
	pWnd->ShowWindow( SW_HIDE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL PasswordDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void PasswordDlg::OnOK()
{
	UpdateData( TRUE );
	if( m_szPass != m_szConfirm )
	{
		BCGPMessageBox( _T("Invalid password.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_PASS );
		if( pWnd )
		{
			pWnd->SetFocus();
			CEdit* pEdit = (CEdit*)pWnd;
			pEdit->SetSel( 0, -1 );
		}
		return;
	}

	CBCGPDialog::OnOK();
}

void PasswordDlg::OnBnPass()
{
	Preferences		pref;
	GripperSettings	gs;
	::GetPreferences( &pref, &gs );

	PasswordChgDlg d( this );
	if( !m_fSubject ) d.m_szPass = pref.pszPass;
	else d.m_szPass = m_szConfirm;
	if( d.DoModal() != IDOK ) return;

	m_fPassChange = TRUE;
	m_szConfirm = d.m_szPassNew;

	if( !m_fSubject )
	{
		strcpy_s( pref.pszPass, 31, d.m_szPassNew );
		::SetPreferences( &pref, &gs );
		::WritePreferences( &pref, &gs );
	}
}

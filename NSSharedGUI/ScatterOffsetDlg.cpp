// ScatterOffsetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ScatterOffsetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ScatterOffsetDlg dialog

BEGIN_MESSAGE_MAP(ScatterOffsetDlg, CBCGPDialog)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

#define DEFAULT_OFFSET_X	0.0
#define DEFAULT_OFFSET_Y	0.0
#define DEFAULT_SCALE_X		1.0
#define DEFAULT_SCALE_Y		1.0

ScatterOffsetDlg::ScatterOffsetDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ScatterOffsetDlg::IDD, pParent)
{
	m_dOffset1 = DEFAULT_OFFSET_X;
	m_dOffset2 = DEFAULT_OFFSET_Y;
	m_dScale1 = DEFAULT_SCALE_X;
	m_dScale2 = DEFAULT_SCALE_Y;
	m_dOffset1Rec = -1.0;
	m_dOffset2Rec = -1.0;
	m_dScale1Rec = 0.0;
	m_dScale2Rec = 0.0;
}

BOOL ScatterOffsetDlg::IsCustom()
{
	if( m_dOffset1 != DEFAULT_OFFSET_X ) return TRUE;
	if( m_dOffset2 != DEFAULT_OFFSET_Y ) return TRUE;
	if( m_dScale1 != DEFAULT_SCALE_X ) return TRUE;
	if( m_dScale2 != DEFAULT_SCALE_Y ) return TRUE;
	return FALSE;
}

void ScatterOffsetDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_OFFSET1, m_dOffset1);
	DDX_Text(pDX, IDC_EDIT_OFFSET2, m_dOffset2);
	DDX_Text(pDX, IDC_EDIT_SCALE1, m_dScale1);
	DDX_Text(pDX, IDC_EDIT_SCALE2, m_dScale2);
}

/////////////////////////////////////////////////////////////////////////////
// ScatterOffsetDlg message handlers

BOOL ScatterOffsetDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL ScatterOffsetDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	CWaitCursor crs;

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_OFFSET1 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("ENTER: Offset X-Axis Bins = 1. - Smallest X-value or larger if you wish fewer bins.\nEXAMPLE: If the smallest X-value is 12.3, select -11.3 or larger."), _T("X-Axis offset.") );
	pWnd = GetDlgItem( IDC_EDIT_OFFSET2 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("ENTER: Offset Grouping Bins = 1. - Smallest Y-value or larger if you wish fewer bins.\nEXAMPLE: If the smallest Y-value is 12.3, select -11.3 or larger."), _T("Grouping offset.") );
	pWnd = GetDlgItem( IDC_EDIT_SCALE1 );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("ENTER: Scale X-Axis Bins = 199. / (Largest X-Value - Smallest X-Value - 1) or smaller if you wish fewer bins.\nEXAMPLE: If the smallest and largest X values are 12.3 and 512.3, resp., select 0.3988 or smaller."), _T("X-Axis scale.") );
	pWnd = GetDlgItem( IDC_EDIT_SCALE2 );
	if( pWnd ) m_tooltip.AddTool( pWnd,_T("ENTER: Scale Grouping Bins = 199. / (Largest Y-Value - Smallest Y-Value - 1) or smaller if you wish fewer bins.\nEXAMPLE: If the smallest and largest Y values are 12.3 and 512.3, resp., select 0.3988 or smaller."),  _T("Grouping scale.") );

	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// recommended offset/scalings?
	CString szVal;
	if( m_dOffset1Rec != -1. )
	{
		szVal.Format( _T("%g"), m_dOffset1Rec );
		pWnd = GetDlgItem( IDC_EDIT_OFFSET1 );
		if( pWnd ) pWnd->SetWindowText( szVal );
	}
	if( m_dOffset2Rec != -1. )
	{
		szVal.Format( _T("%g"), m_dOffset2Rec );
		pWnd = GetDlgItem( IDC_EDIT_OFFSET2 );
		if( pWnd ) pWnd->SetWindowText( szVal );
	}
	if( m_dScale1Rec != 0. )
	{
		szVal.Format( _T("%g"), m_dScale1Rec );
		pWnd = GetDlgItem( IDC_EDIT_SCALE1 );
		if( pWnd ) pWnd->SetWindowText( szVal );
	}
	if( m_dScale2Rec != 0. )
	{
		szVal.Format( _T("%g"), m_dScale2Rec );
		pWnd = GetDlgItem( IDC_EDIT_SCALE2 );
		if( pWnd ) pWnd->SetWindowText( szVal );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ScatterOffsetDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("analysischarts.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

// ConditionPageVFeedback.cpp : implementation file
//

#include "stdafx.h"
#include "NSSharedGUI.h"
#include "ConditionPageVFeedback.h"
#include "Conditions.h"
#include "ConditionSheet.h"
#include "Experiments.h"

// ConditionPageVFeedback dialog

IMPLEMENT_DYNAMIC(ConditionPageVFeedback, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(ConditionPageVFeedback, CBCGPPropertyPage)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_CHK_FB1, OnBnClickedChkFb1)
	ON_BN_CLICKED(IDC_CHK_FB2, OnBnClickedChkFb2)
END_MESSAGE_MAP()

ConditionPageVFeedback::ConditionPageVFeedback( INSCondition* pC, BOOL fNew )
	: CBCGPPropertyPage(ConditionPageVFeedback::IDD), m_pC( pC ), m_fUse1( FALSE ),
	  m_fUse2( FALSE ), m_nFeature1( 1 ), m_nFeature2( 1 ), m_nStroke1( 1 ),
	  m_nStroke2( 1 ), m_dMin1( 0. ), m_dMin2( 0. ), m_dMax1( 1. ), m_dMax2( 1. ),
	  m_fSwap1( FALSE ), m_fSwap2( FALSE ), m_fNew( fNew )
{
	if( !fNew && pC )
	{
		short nFeature = 0, nStroke = 0;
		NSConditions::GetFeedbackInfo( pC, 1, m_fUse1, nFeature, nStroke,
									   m_dMin1, m_dMax1, m_fSwap1 );
		m_nFeature1 = nFeature - 1;
		m_nStroke1 = nStroke;
		NSConditions::GetFeedbackInfo( pC, 2, m_fUse2, nFeature, nStroke,
									   m_dMin2, m_dMax2, m_fSwap2 );
		m_nFeature2 = nFeature - 1;
		m_nStroke2 = nStroke;
	}
}

ConditionPageVFeedback::~ConditionPageVFeedback()
{
}

void ConditionPageVFeedback::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_FB1, m_fUse1);
	DDX_Check(pDX, IDC_CHK_FB2, m_fUse2);
	DDX_Control(pDX, IDC_CBO_FEATURE1, m_cboFeature1);
	DDX_Control(pDX, IDC_CBO_FEATURE2, m_cboFeature2);
	DDX_CBIndex(pDX, IDC_CBO_FEATURE1, m_nFeature1);
	DDX_CBIndex(pDX, IDC_CBO_FEATURE2, m_nFeature2);
	DDX_Text(pDX, IDC_EDIT_STROKE1, m_nStroke1);
	DDX_Text(pDX, IDC_EDIT_STROKE2, m_nStroke2);
	DDX_Text(pDX, IDC_EDIT_MIN1, m_dMin1);
	DDX_Text(pDX, IDC_EDIT_MIN2, m_dMin2);
	DDX_Text(pDX, IDC_EDIT_MAX1, m_dMax1);
	DDX_Text(pDX, IDC_EDIT_MAX2, m_dMax2);
	DDX_Check(pDX, IDC_CHK_SWAP1, m_fSwap1);
	DDX_Check(pDX, IDC_CHK_SWAP2, m_fSwap2);
}

// ConditionPageVFeedback message handlers

BOOL ConditionPageVFeedback::OnApply() 
{
	if( !m_pC ) return FALSE;
	UpdateData( TRUE );

	// VERIFY
	// Using Feedback (if 2, must have 1)
	if( m_fUse2 && !m_fUse1 )
	{
		BCGPMessageBox( _T("In order to use feedback 2, you must also use feedback 1.") );
		return FALSE;
	}
	// stroke 1
	if( m_fUse1 && ( m_nStroke1 < 1 ) )
	{
		BCGPMessageBox( _T("Segment must be at least 1.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_STROKE1 );
		pWnd->SetFocus();
		return FALSE;
	}
	// stroke 2
	if( m_fUse2 && ( m_nStroke2 < 1 ) )
	{
		BCGPMessageBox( _T("Segment must be at least 1.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_STROKE2 );
		pWnd->SetFocus();
		return FALSE;
	}
	// Min & Max 1
	if( m_fUse1 && ( m_dMax1 <= m_dMin1 ) )
	{
		BCGPMessageBox( _T("Maximum value must be greater than minimum value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MAX1 );
		pWnd->SetFocus();
		return FALSE;
	}
	// Min & Max 2
	if( m_fUse2 && ( m_dMax2 <= m_dMin2 ) )
	{
		BCGPMessageBox( _T("Maximum value must be greater than minimum value.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_MAX2 );
		pWnd->SetFocus();
		return FALSE;
	}

	// UPDATE
	m_pC->SetFeedbackInfo( 1, m_fUse1, m_nFeature1 + 1, m_nStroke1,
						   m_dMin1, m_dMax1, m_fSwap1 );
	m_pC->SetFeedbackInfo( 2, m_fUse2, m_nFeature2 + 1, m_nStroke2,
						   m_dMin2, m_dMax2, m_fSwap2 );

	ConditionSheet* pSheet = (ConditionSheet*)this->GetParent();
	pSheet->m_fModified = TRUE;

	return CBCGPPropertyPage::OnApply();
}

BOOL ConditionPageVFeedback::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL ConditionPageVFeedback::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();
	EnableVisualManagerStyle();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDR_COND );
	CWnd* pWnd = GetDlgItem( IDC_CHK_FB1 );
	m_tooltip.AddTool( pWnd, _T("Use the following feedback information during recording." ), _T("Use Feedback 1") );
	pWnd = GetDlgItem( IDC_CHK_FB2 );
	m_tooltip.AddTool( pWnd, _T("Use the following feedback information during recording." ), _T("Use Feedback 2") );
	pWnd = GetDlgItem( IDC_CBO_FEATURE1 );
	m_tooltip.AddTool( pWnd, _T("Select the feature to use for feedback." ), _T("Select Feature") );
	pWnd = GetDlgItem( IDC_CBO_FEATURE2 );
	m_tooltip.AddTool( pWnd, _T("Select the feature to use for feedback." ), _T("Select Feature") );
	pWnd = GetDlgItem( IDC_EDIT_STROKE1 );
	m_tooltip.AddTool( pWnd, _T("Specify the stroke of the feature to use for feedback.\nWhen submovement analysis is checked in Experiment Settings -> Processing -> Segmentation:\nSegment 1 = Stroke 1, Primary submovement\nSegment 2 = Stroke 1, Secondary submovement\nSegment 3 = Stroke 1, Total stroke\nSegment 4 = Stroke 2, Primary submovement\netc."), _T("Stroke") );
	pWnd = GetDlgItem( IDC_EDIT_STROKE2 );
	m_tooltip.AddTool( pWnd, _T("Specify the stroke of the feature to use for feedback.\nWhen submovement analysis is checked in Experiment Settings -> Processing -> Segmentation:\nSegment 1 = Stroke 1, Primary submovement\nSegment 2 = Stroke 1, Secondary submovement\nSegment 3 = Stroke 1, Total stroke\nSegment 4 = Stroke 2, Primary submovement\netc."), _T("Stroke") );
	pWnd = GetDlgItem( IDC_EDIT_MIN1 );
	m_tooltip.AddTool( pWnd, _T("Specify the minimum range value for feedback." ), _T("Minimum Value") );
	pWnd = GetDlgItem( IDC_EDIT_MIN2 );
	m_tooltip.AddTool( pWnd, _T("Specify the minimum range value for feedback." ), _T("Minimum Value") );
	pWnd = GetDlgItem( IDC_EDIT_MAX1 );
	m_tooltip.AddTool( pWnd, _T("Specify the maximum range value for feedback." ), _T("Maximum Value") );
	pWnd = GetDlgItem( IDC_EDIT_MAX2 );
	m_tooltip.AddTool( pWnd, _T("Specify the maximum range value for feedback." ), _T("Maximum Value") );
	pWnd = GetDlgItem( IDC_CHK_SWAP1 );
	m_tooltip.AddTool( pWnd, _T("Default mode is OFF, which is to show the lower 50% of the range (min & max) as green, and the upper half as red.\nTurn ON to swap the colors." ), _T("Swap Colors") );
	pWnd = GetDlgItem( IDC_CHK_SWAP2 );
	m_tooltip.AddTool( pWnd, _T("Default mode is OFF, which is to show the lower 50% of the range (min & max) as green, and the upper half as red.\nTurn ON to swap the colors." ), _T("Swap Colors") );

	// fill the feedback combo
	FillFeatures();
	// now search for feedback to select in list

	// update enable/disable
	EnableDisable();

	return TRUE;
}

void ConditionPageVFeedback::FillFeatures()
{
	// FILL FEEDBACK FEATURE LIST
	BOOL fSuccess = FALSE;
	CString szLine;
	CStringList lstFeatures;
	// create temp hwr/tf to get data
	if( lstFeatures.GetCount() <= 0 )
	{
		CString szPath, szRaw, szTF, szSeg, szExt, szTmp;
		CStdioFile f;
		int nPos = -1;

		// temporary data files
		::GetDataPathRoot( szPath );
		if( szPath == _T("") ) szRaw = _T("temp.HWR");
		else szRaw.Format( _T("%s\\temp.HWR"), szPath );
		szTF = szRaw.Left( szRaw.GetLength() - 3 );
		szSeg = szRaw.Left( szRaw.GetLength() - 3 );
		szExt = szRaw.Left( szRaw.GetLength() - 3 );
		szTF += _T("TF");
		szSeg += _T("SEG");
		szExt += _T("EXT");
		// now open it and add a few lines of bogus #'s
		if( f.Open( szRaw, CFile::modeCreate | CFile::modeWrite ) )
		{
			f.WriteString( _T("1 1 1\n") );
			f.WriteString( _T("2 2 2\n") );
			f.Close();
		}
		// now call tf with this file
		Experiments::ProcessTimfun( szRaw, szTF, 12.0, 1, 0, 0.0, ::GetOutputWindow() );
		// delete temp HWR file
		CFile::Remove( szRaw );
		// now call seg with this file if it exists
		CFileFind ff;
		if( !ff.FindFile( szTF ) )
		{
			BCGPMessageBox( _T("Error filling feature lists.") );
			return;
		}
		Experiments::ProcessSegmen( szTF, szSeg, 0, ::GetOutputWindow() );
		// now call ext with this file if it exists
		if( !ff.FindFile( szSeg ) )
		{
			BCGPMessageBox( _T("Error filling feature lists.") );
			return;
		}
		// now call extract with tf & seg files to get features
		Experiments::ProcessExtract( szTF, szSeg, szExt, 0, ::GetOutputWindow() );
		// delete temp TF & SEG files
		CFile::Remove( szTF );
		CFile::Remove( szSeg );
		// now open the EXT file to obtain the features (headers)
		if( f.Open( szExt, CFile::modeRead ) )
		{
			f.ReadString( szLine );	// 1st line is headers
			szLine.Trim();
			nPos = szLine.Find( _T(" ") );
			while( nPos != -1 )
			{
				szTmp = szLine.Left( nPos );
				lstFeatures.AddTail( szTmp );
				szLine = szLine.Mid( nPos + 1 );
				nPos = szLine.Find( _T(" ") );
				if( nPos == -1 ) lstFeatures.AddTail( szLine );
			}
			f.Close();
			fSuccess = TRUE;
			// delete temp EXT file
			CFile::Remove( szExt );
		}
	}
	else fSuccess = TRUE;
	// fill & select
	if( fSuccess )
	{
		// now fill list
		POSITION pos = lstFeatures.GetHeadPosition();
		while( pos )
		{
			szLine = lstFeatures.GetNext( pos );
			m_cboFeature1.AddString( szLine );
			m_cboFeature2.AddString( szLine );
		}
		// select
		m_cboFeature1.SetCurSel( m_nFeature1 );
		m_cboFeature2.SetCurSel( m_nFeature2 );
	}
	else if( !fSuccess )
	{
		BCGPMessageBox( _T("Unable to fill the features lists. Please contact NeuroScript for assistance.") );
	}
}

void ConditionPageVFeedback::EnableDisable()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_FEATURE1 );
	pWnd->EnableWindow( m_fUse1 );
	pWnd = GetDlgItem( IDC_EDIT_STROKE1 );
	pWnd->EnableWindow( m_fUse1 );
	pWnd = GetDlgItem( IDC_EDIT_MIN1 );
	pWnd->EnableWindow( m_fUse1 );
	pWnd = GetDlgItem( IDC_EDIT_MAX1 );
	pWnd->EnableWindow( m_fUse1 );
	pWnd = GetDlgItem( IDC_CHK_SWAP1 );
	pWnd->EnableWindow( m_fUse1 );

	pWnd = GetDlgItem( IDC_CBO_FEATURE2 );
	pWnd->EnableWindow( m_fUse2 );
	pWnd = GetDlgItem( IDC_EDIT_STROKE2 );
	pWnd->EnableWindow( m_fUse2 );
	pWnd = GetDlgItem( IDC_EDIT_MIN2 );
	pWnd->EnableWindow( m_fUse2 );
	pWnd = GetDlgItem( IDC_EDIT_MAX2 );
	pWnd->EnableWindow( m_fUse2 );
	pWnd = GetDlgItem( IDC_CHK_SWAP2 );
	pWnd->EnableWindow( m_fUse2 );
}

BOOL ConditionPageVFeedback::DoHelp( HELPINFO* pHelpInfo )
{
	return OnHelpInfo( pHelpInfo );
}

BOOL ConditionPageVFeedback::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("conditionproperties_vfeedback.html"), this );
	return TRUE;
}

void ConditionPageVFeedback::OnBnClickedChkFb1()
{
	EnableDisable();
}

void ConditionPageVFeedback::OnBnClickedChkFb2()
{
	EnableDisable();
}

#ifndef _CATS_H_
#define	_CATS_H_

#include "Objects.h"

#define	TAG_CAT	_T("[CAT]")

interface ICat;

// This class is intended to encapsulate all view & functionality for a cat
class AFX_EXT_CLASS Cats : public Objects
{
	PUT_BASE( Cat )

	// Operations
public:
	virtual ULONGLONG GetNumRecords();

	// Interface methods
	PUT_GET( Cat, ID, CString, val.AllocSysString() )

	PUT_GET( Cat, Description, CString, val.AllocSysString() )

	void GetDescriptionWithID( CString&, BOOL fReverse = FALSE );
	static void GetDescriptionWithID( ICat*, CString&, BOOL fReverse = FALSE );

	PUT_GET( Cat, Type, CatType, val )

	// Importing/Exporting
	BOOL Export( CMemFile* pFile );
	BOOL Import( CStdioFile* pFile, BOOL& fOWAllYes, BOOL& fOWAllNo, BOOL fScan );

	// Upload/download to/from client-server/local
protected:
	// shared by both copy & upload - ToLocal TRUE is from client server to user, else FALSE
	BOOL DoTransfer( BOOL fToLocal, CString szID, BOOL fOverwrite, CString szUserPath = _T("") );
public:
	virtual BOOL DoCopy( CString szID, CString szPath, BOOL fOverwrite = FALSE );
	virtual BOOL DoUpload( CString szID, BOOL fOverwrite = FALSE );

	// database mode	(TODO: can we find a way to move this to objects?)
	void ForceLocal();
	void ForceRemote();
	void ForceDefault();
protected:
	void SetTemp( BOOL fVal );
	BOOL DumpRecord( CString szID, CString szFile );
	void RemoveTemp();
};

#endif	// _CATS_H_

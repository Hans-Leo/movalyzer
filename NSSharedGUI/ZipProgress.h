// ZipProgress.h: interface for the ZipProgress class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "..\chilkat\include\CkZipProgress.h"

class AFX_EXT_CLASS ZipProgress : public CkZipProgress
{
public:
	// construction
	ZipProgress(){}
	virtual ~ZipProgress() {}

	// abort check
	virtual void AbortCheck( bool* abort );
	static void Stop();
	// zip
	virtual void WriteZipBegin();
	virtual void WriteZipPercentDone( long pctDone, bool* abort );
	virtual void WriteZipEnd();
	// unzip
	virtual void UnzipPercentDone( long pctDone, bool* abort );
	virtual void UnzipBegin();
	virtual void UnzipEnd();
};

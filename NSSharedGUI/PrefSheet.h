#pragma once

// PrefSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PrefSheet

class AFX_EXT_CLASS PrefSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(PrefSheet)

// Construction
public:
	PrefSheet( UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	PrefSheet( LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	PrefSheet( CString szID, CWnd* pParentWnd = NULL, UINT iSelectPage = 0, BOOL fSettings = TRUE );

// Attributes
public:
	CString			m_szID;
	BOOL			m_fModified;
	Preferences		m_pref;
	GripperSettings	m_gs;
	BOOL			m_fgs;

// Operations
public:
	void AddPages();
	void GripperMode( BOOL fOn );

// Overrides

// Implementation
public:
	virtual ~PrefSheet();

// message map functions
protected:
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

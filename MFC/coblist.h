#ifndef __COBLIST_H__
#define __COBLIST_H__

class CObList {
public:
    // life-cycle
    CObList();
    ~CObList();

    int GetCount();
    POSITION GetHeadPosition();
    CObject GetNext(POSITION& pos);
    void RemoveAll();
    void AddTail(CObject* item);
};

#endif  // __COBLIST_H__

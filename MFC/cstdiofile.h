#ifndef	__CSTDIOFILE_H__
#define	__CSTDIOFILE_H__

#include "cfile.h"

typedef unsigned int   HANDLE;

class CString;

class CStdioFile: public CFile {
public:
    // life-cycle
    CStdioFile();

public:
    // properties
    FILE* m_pStream;    // stdio FILE
                        // m_hFile from base class is _fileno(m_pStream)

public:
    // operations
    bool Open(CString file, int mode);
	void Close();

	CString GetFilePath();

	void SeekToBegin();
    void SeekToEnd();

	bool ReadString(CString& line);
    void WriteString(CString line);
};

#endif

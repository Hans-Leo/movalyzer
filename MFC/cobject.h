#ifndef __COBJECT_H__
#define __COBJECT_H__

class CObject {
public:
    // life-cycle
    CObject();
    ~CObject();
};

#endif  // __COBJECT_H__

#ifndef __CSTRINGLIST_H__
#define __CSTRINGLIST_H__

class CStringList {
public:
    // life-cycle
    CStringList();
    ~CStringList();

public:
    // properties

public:
    // operations
    int GetCount();
    POSITION GetHeadPosition();
    CString GetNext(POSITION& pos);
    void RemoveAll();
    void AddTail(CString item);
    void InsertBefore(POSITION, CString);
};

#endif  // __CSTRINGLIST_H__

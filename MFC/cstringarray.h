#ifndef __CSTRINGARRAY_H__
#define __CSTRINGLIST_H__

class CString;

class CStringArray {
public:
    // life-cycle
    CStringArray();

public:
    // properties

public:
    // operations
    void Add(const CString item);
    int GetCount();
    const char* GetAt(int index);
};

#endif  // __CSTRINGARRAY_H__

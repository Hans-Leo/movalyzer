#ifndef	__CFILEFIND_H__
#define	__CFILEFIND_H__

struct timeval;

class CFileFind {
public:
    // life-cycle
    CFileFind();

public:
    // operations
	bool FindFile(const CString file);
    bool FindNextFile();

    CString GetFileName();

    void GetLastWriteTime(timeval* tv);
};

#endif  // __CFILEFIND_H__

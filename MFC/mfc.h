#ifndef	__MFC_H__
#define	__MFC_H__

#include <stdio.h>
#include <cstring>
// #include <cstddef>

#include "cobject.h"
#include "coblist.h"

#include "cstring.h"
#include "cstringlist.h"
#include "cstringarray.h"

#include "cfile.h"
#include "cstdiofile.h"
#include "cfilefind.h"


#define _T(x)           x
#define LPVOID          void*

struct __POSITION {};
typedef __POSITION* POSITION;

// TODO: Much of this will go away since it's view related

// struct COLORREF {
//    unsigned char R;
//    unsigned char G;
//    unsigned char B;
// };

#ifndef BYTE
  #define BYTE            char
  #define WORD            unsigned short
  #define DWORD           unsigned int
  typedef DWORD           COLORREF;
  typedef DWORD*          LPCOLORREF;
#endif
#ifndef RGB
  #define RGB(r,g,b)      ((COLORREF)(((BYTE)(r)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(b))<<16)))
#endif


#define LF_FACESIZE     35

struct LOGFONT {
  long  lfHeight;
  long  lfWidth;
  long  lfEscapement;
  long  lfOrientation;
  long  lfWeight;
  unsigned char  lfItalic;
  unsigned char  lfUnderline;
  unsigned char  lfStrikeOut;
  unsigned char  lfCharSet;
  unsigned char  lfOutPrecision;
  unsigned char  lfClipPrecision;
  unsigned char  lfQuality;
  unsigned char  lfPitchAndFamily;
  char lfFaceName[LF_FACESIZE];
};


// TODO: Handle debug
#include <assert.h>
#define ASSERT(val) assert(val)

#endif

#ifndef	__CSTRING_H__
#define	__CSTRING_H__

typedef char*       LPSTR;
typedef const char* LPCSTR;

class CString {
public:
    // life-cycle
	CString();
    CString(const char* txt);
    CString(const char txt);
    ~CString() {
        // TODO
    }

public:
    // properties

public:
    // operations
    void SetAt(int index, CString val);

    void Format(const char* format, ...);
    void Format(int formatResourceId, ...);

    int GetLength();
    const char GetAt(int index);

    bool IsEmpty();
    void Empty();

    void TrimLeft();
    void TrimRight();
    void Trim();

    void MakeUpper();
    void MakeLower();

    const char* Left(int count);
    const char* Mid(int start, int end = -1);
    const char* Right(int count);

    int Find(const CString str);
    int ReverseFind(const CString str);

    int CompareNoCase(const char* item);

    void LoadString(int resourceId);

public:
    // operator overloads
	operator LPSTR() {
        // TODO
        return (LPSTR)0;
	}

    operator LPCSTR() {
        // TODO
        return (LPCSTR)0;
    }

	inline char operator[](int index) const {
        // TODO
        return (char)0;
	}

    inline CString& operator=(const CString rhs) {
        // TODO
        return *this;
	}
    inline CString& operator=(const char* rhs) {
        // TODO
        return *this;
    }
    inline CString& operator=(const char rhs) {
        // TODO
        return *this;
    }

    inline CString& operator+=(const CString rhs) {
        // TODO
        return *this;
    }
    inline CString& operator+=(const char* rhs) {
        // TODO
        return *this;
    }

    inline bool operator==(const CString rhs) {
        // TODO
        return true;
    }
    inline bool operator==(const char* rhs) {
        // TODO
        return true;
    }

    inline bool operator!=(const CString rhs) {
        // TODO
        return true;
    }
    inline bool operator!=(const char* rhs) {
        // TODO
        return true;
    }

    inline bool operator<=(const CString rhs) {
        // TODO
        return true;
    }
    inline bool operator<=(const char* rhs) {
        // TODO
        return true;
    }

    inline bool operator>=(const CString rhs) {
        // TODO
        return true;
    }
    inline bool operator>=(const char* rhs) {
        // TODO
        return true;
    }

    inline bool operator<(const CString rhs) {
        // TODO
        return true;
    }
    inline bool operator<(const char* rhs) {
        // TODO
        return true;
    }

    inline bool operator>(const CString rhs) {
        // TODO
        return true;
    }
    inline bool operator>(const char* rhs) {
        // TODO
        return true;
    }
};

#endif  // __CSTRING_H__

//GraphView.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "..\InputMod\InputMod.h"
#include "..\InputMod\InputMod_i.c"
#include "..\InputMod\NSAnalogIn.h"
#include "..\DataMod\DataMod.h"
#include "GraphView.h"
#include "..\Wintab\include\wintab.h"
#include "MainFrm.h"
#include "RecView.h"
#include "LeftView.h"
#include "Conditions.h"
#include "Stimuli.h"
#include "StartView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif	// _DEBUG

#define	MAIN_TIMER			999
#define	START_TIMER			200			// ID for timer to begin recording
#define	TABLET_TIMER		300			// total recording time timer ID
#define	GRIPPER_TIMER		400			// gripper timer ID for recording
#define	PRECUE_TIMER		500			// precue timer ID
#define	LATENCY_TIMER		600			// latency timer ID
#define	VISIBILITY_TIMER	700			// timer ID for recording visibility

#define CURSOR_TIMER		800			// timer for handling cursor display
#define CURSOR_TIMEOUT		1000		// time (ms) after penup cursor is displayed (if hidden)
long	lTicksCursor	=	0L;			// tracks time for cursor timer
BOOL	fCursorTimer	=	FALSE;		// whether this timer is on

#define	TIMER_INTERVAL		250			// time inbetween timer calls (ms)
#define	START_LIMIT			10000		// default time to start (ms)
#define	RECORD_LIMIT		10000		// default time for total record (ms)

#define	HACK_TIMER			888			// hack timer to ensure gripper trial drawing stays up for a bit of time after a trial/test


#define IGNOREME			-99999.0


// Defined in globals.h
GripperSettings gs;
// Flag to indicate to gripper callback (separate thread) to terminate
BOOL _TerminateGThread = FALSE;
// global pointer to daqpad object...shared with other thread
IDAQPad* _dp = NULL;

// structure for housing "Record" arguments when a precue stimulus is used
struct RecordInfo
{
	char szExpID[ 4 ];
	char szCondID[ 4 ];
	char szFile[ _MAX_PATH ];
	char szStim[ 4 ];
	char szPStim[ 4 ];
	BOOL fTrialOnly;
	CPoint	lstPoint;
	double dMagnetForce;
};
RecordInfo _ri;

// TODO: temporary...move to global
UINT GetGlobalInterval() { return 100; }
BOOL GetDeactivateWrongTarget() { return FALSE; }

// see also test.cpp
#define IM_AUTH_CODE	"����������������"		//"IMq{1M{UaM1a4Wa9"
void GetAuthCodeIM( CString& szCode )
{
	szCode = IM_AUTH_CODE;
	::Decode( szCode );
}

/////////////////////////////////////////////////////////////////////////////
// CGraphView

IMPLEMENT_DYNCREATE(CGraphView, CView)

BEGIN_MESSAGE_MAP(CGraphView, CView)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_SETCURSOR()
	ON_WM_MOUSEMOVE()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	ON_WM_CONTEXTMENU()
	ON_MESSAGE(WT_PACKET, OnWTPacket)
	ON_MESSAGE(WM_USER+101, OnFinished)
	ON_COMMAND(IDM_E_CLEAR, Clear)
	ON_COMMAND(IDM_VIEW_STARTPAGE, CMainFrame::OnViewStartPage)
END_MESSAGE_MAP()

// Constructor
CGraphView::CGraphView() : m_pDigitizer( NULL )
{
	m_BGColor = ::GetBkColor();
	m_dTimeoutTicks = 3000;
	m_dTimeoutTicksNA = 3000;
	m_dTimeoutStart = 10;
	m_dTimeoutSecs = 10;
	m_dTimeoutPrecue = 3;
	m_dTimeoutLatency = 2;
	m_dTimeoutWarning = 3;
	m_dTimeoutLatencyW = 2;
	m_fBeganRecording = FALSE;
	m_fTest = FALSE;
	fRecording = FALSE;
	m_fInstrVis = FALSE;
	m_fStimOnly = FALSE;
	m_fPrecue = FALSE;
	m_fWarning = FALSE;
	fImmediately = FALSE;
	fStopWrongTarget = FALSE;
	fStartFirstTarget = FALSE;
	fStopLastTarget = FALSE;
	_TerminateGThread = FALSE;
	m_nTicks = 0L;
	m_nTicks2 = 0L;
	m_nTicks3 = 0L;
	m_nTicksNA = 0L;
	m_fDoWait = FALSE;
	fHideFeedback = FALSE;
	fHideShowAfter = FALSE;
	fHideShowAfterShow = FALSE;
	fHideShowAfterStrokes = FALSE;
	dHideShowCount = 0.;
	m_nTicksHideShow = 0L;
	m_fRealSize = FALSE;
	m_fCanContinue = FALSE;
	m_fTrialEnded = FALSE;
	m_pStartView = NULL;
	m_fNoFeedback = TRUE;
	m_dFBMin1 = 0.;
	m_dFBMax1 = 0.;
	m_dFBVal1 = 0.;
	m_dFBValLast1 = IGNOREME;
	m_dFBMin2 = 0.;
	m_dFBMax2 = 0.;
	m_dFBVal2 = 0.;
	m_dFBValLast2 = IGNOREME;
	m_fHideCursor = TRUE;
	m_fCursorHidden = FALSE;

	::SetDeviceResolution( 0.001 );
}

// Destructor
CGraphView::~CGraphView()
{
	// Release device interfaces (in inputmod)
	if( m_pDigitizer ) m_pDigitizer->Release();
	if( _dp ) _dp->Release();
	m_bmpMsgWait.DeleteObject();
	m_bmpMsgEnter.DeleteObject();
}

/////////////////////////////////////////////////////////////////////////////
// CGraphView sizing
void CGraphView::OnSize( UINT nType, int cx, int cy )
{
	CView::OnSize( nType, cx, cy );

	if( m_pStartView ) m_pStartView->MoveWindow( 0, 0, cx, cy, FALSE );
}

/////////////////////////////////////////////////////////////////////////////
// CGraphView drawing
void CGraphView::OnDraw( CDC* pDC )
{
	// Color of the recording window
	HBRUSH hbr = CreateSolidBrush( m_BGColor );
	HBRUSH hbrOld = (HBRUSH)pDC->SelectObject( hbr );
	RECT rect;
	GetWindowRect( &rect );
	pDC->Rectangle( 0, 0, rect.right, rect.bottom );
	hbr = (HBRUSH)pDC->SelectObject( hbrOld );
	DeleteObject( hbr );

#ifdef _PAPER
	if( fRecording ) DrawGrid( pDC );
#endif	// _PAPER

	DrawFeedback( m_fNoFeedback, 0., 0., 0., FALSE, TRUE );
	DrawFeedback( m_fNoFeedback, 0., 0., 0., TRUE, TRUE );

	// bitmap messages
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
	if( !pLV || ( pLV->GetActionState() == ACTION_NONE ) )
	{
		// do nothing
	}
	else if( pLV && ( pLV->GetActionState() == ACTION_RUNEXPERIMENT ) )
	{
		if( !m_fBeganRecording && fRecording )
		{
			CSize sz = m_bmpMsgWait.GetBitmapDimension();
			int nX = ( rect.right - rect.left ) - sz.cx - 20;
			int nY = 20;
			pDC->DrawState( CPoint( nX, nY ), sz, &m_bmpMsgWait, DST_BITMAP );
		}
		else if( m_fDoWait )
		{
			CSize sz = m_bmpMsgEnter.GetBitmapDimension();
			int nX = ( rect.right - rect.left ) - sz.cx - 20;
			int nY = 20;
			pDC->DrawState( CPoint( nX, nY ), sz, &m_bmpMsgEnter, DST_BITMAP );
		}
	}

	// if we're recording, tell the digitizer to update it's display (for stimuli generally)
	if( fRecording && m_pDigitizer ) m_pDigitizer->Draw( (VARIANT*)pDC );
}

/////////////////////////////////////////////////////////////////////////////
// CGraphView message handlers

// Message Handler for digitizer object indicating received packets of data
// When digitizer (wintab) receives a packet of data from the device, this method is called
// by the wintab framework
// This is also manually called during OnMouseMove to replicate functionality
LRESULT CGraphView::OnWTPacket( WPARAM wSerial, LPARAM hCtx )
{
	// if flag gets set to terminate the thread, end everything
	if( fRecording && _TerminateGThread )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		if( ::IsContinueWithEnter() )
		{
			CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
			pRV->UpdateInstruction( _T("Done - Press ENTER to continue") );
			m_fDoWait = TRUE;
			Invalidate();
		}
		else
		{
			if( !m_fTest ) pMF->Record();
			else pMF->OnDigTest2();
		}

		return TRUE;
	}

	// Time elapsed has exceeded total recording time
	if( !m_fStimOnly && !m_fPrecue && !m_fWarning && fRecording && !m_fDoWait &&
		( m_nTicks >= ( m_dTimeoutSecs * 1000 ) ) && !m_fTrialEnded )
	{
		// trial ends, stop recording, set flag that trial ended
		m_pDigitizer->Stop();

		if( ::IsGripperModeOn() )
		{
			// stop the device so it will record
			_dp->Stop();
		}

		if( !m_fStimOnly ) ::PlayASound( m_szCondID, SOUND_RECORDING_END, ::GetTrialRunInfo() );
		OutputAMessage( _T("Recording Timeout occurred.") );
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		if( ::IsContinueWithEnter() )
		{
			CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
			pRV->UpdateInstruction( _T("Done - Press ENTER to continue") );
			m_fDoWait = TRUE;
			m_pDigitizer->Stop();
			Invalidate();
		}
		else
		{
			m_fCanContinue = FALSE;
			m_fTrialEnded = FALSE;
			if( !m_fTest ) ((CMainFrame*)AfxGetApp()->m_pMainWnd)->Record();
			else ((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnDigTest2();
		}
	}
	// Time elapsed has exceeded total time allowed to begin trial
	else if( !m_fStimOnly && fRecording && !m_fBeganRecording && !m_fTrialEnded &&
		( m_nTicks2 >= ( m_dTimeoutStart * 1000 ) ) )
	{
		if( !m_fStimOnly ) ::PlayASound( m_szCondID, SOUND_RECORDING_END, ::GetTrialRunInfo() );
		OutputAMessage( _T("Start timeout occurred.") );
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		// if recording, record else ondigtest2...both in CMainFrame
		// which resets and stops everything
		if( !m_fTest ) pMF->Record();
		else pMF->OnDigTest2();
	}
	// Time elapsed has exceeded total time allowed for no motion of mouse or pen out of range
	// only applies if recording has already begun (not applicable for gripper)
	// similar to pen-up in-range but for mouse (or pen out of range)
	else if( !m_fPrecue && !m_fWarning && fRecording && m_fBeganRecording && !m_fDoWait && !m_fTrialEnded &&
			 ( ( !::IsTabletModeOn() && ( m_nTicks3 >= m_dTimeoutTicks ) ) ||
			   ( ::IsTabletModeOn() && ( m_nTicksNA >= m_dTimeoutTicksNA ) ) )
			 && !::IsGripperModeOn() )
	{
		if( !m_fStimOnly ) ::PlayASound( m_szCondID, SOUND_RECORDING_END, ::GetTrialRunInfo() );
		OutputAMessage( _T("No motion of mouse or pen out-of-range timeout occurred.") );
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		if( ::IsContinueWithEnter() )
		{
			CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
			pRV->UpdateInstruction( _T("Done - Press ENTER to continue") );
			m_fDoWait = TRUE;
			m_pDigitizer->Stop();
			Invalidate();
			m_pDigitizer->HideShow( FALSE );
		}
		else
		{
			if( !m_fTest ) pMF->Record();
			else pMF->OnDigTest2();
		}
	}
	// if no timeouts have occurred, then process the message as a packet
	else if( m_pDigitizer )
	{
		HRESULT hr;

		// Only if digitizer is being used
		if( ::IsTabletModeOn() )
		{
			// pass packet to digitizer (inputmod) to handle the data
			BOOL fInval = FALSE;
			CDC* pDC = GetDC();
			hr = m_pDigitizer->OnWTPacket( (long)wSerial, (long)hCtx, (VARIANT*)pDC, &fInval );
			ReleaseDC( pDC );
			if( ::IsTabletModeOn() ) m_nTicks3 = 0L;

			if( m_fCanContinue && m_fTrialEnded )
			{
				m_fCanContinue = FALSE;
				m_fTrialEnded = FALSE;
				if( !m_fTest ) ((CMainFrame*)AfxGetApp()->m_pMainWnd)->Record();
				else ((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnDigTest2();
			}

			if( hr == S_FALSE )
			{
				// trial ended, stop recording, set flags
				m_pDigitizer->Stop();
				m_fCanContinue = FALSE;
				m_fTrialEnded = TRUE;
			}
			else if( hr == S_PENDOWN )
			{
				PlayASound( m_szCondID, SOUND_PENDOWN, ::GetTrialRunInfo() );
				if( fRecording && m_fHideCursor && ::IsTabletModeOn() )
				{
					KillTimer( CURSOR_TIMER );
					lTicksCursor = 0L;
					fCursorTimer = FALSE;
					if( !m_fCursorHidden )
					{
						ShowCursor( FALSE );
						m_fCursorHidden = TRUE;
					}
				}
			}
			else if( hr == S_PENUP )
			{
				PlayASound( m_szCondID, SOUND_PENUP, ::GetTrialRunInfo() );
				m_fCanContinue = TRUE;
				if( fRecording && m_fHideCursor && ::IsTabletModeOn() )
				{
					lTicksCursor = 0L;
					fCursorTimer = TRUE;
					SetTimer( CURSOR_TIMER, TIMER_INTERVAL, NULL );
				}
			}
			else if( ( hr == S_TARGETGOOD ) || ( hr == S_TARGETGOODFIRST ) || ( hr == S_TARGETGOODLAST ) )
			{
				PlayASound( m_szCondID, SOUND_TARGET_CORRECT, ::GetTrialRunInfo() );
			}
			else if( hr == S_TARGETBAD )
			{
				PlayASound( m_szCondID, SOUND_TARGET_WRONG, ::GetTrialRunInfo() );
			}
			else if( hr == S_MAXSTROKES )
			{
				// trial has ended, stop recording, set flags
				m_pDigitizer->Stop();
				m_fCanContinue = FALSE;
				m_fTrialEnded = TRUE;

				CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
				if( ::IsContinueWithEnter() )
				{
					CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
					pRV->UpdateInstruction( _T("Done - Press ENTER to continue") );
					m_fDoWait = TRUE;
					Invalidate();
				}
			}
			else if( hr == S_OK ) m_nTicksNA = 0L;

			if( ( hr != S_FALSE ) && fInval ) Invalidate();

			// if we haven't begun recording yet, intialize the total recording timer
			// if displaying the precue, this timer is not applicable (diff timer)
			if( fRecording && !m_fBeganRecording && ( hr == S_START ) &&
				!m_fPrecue && !m_fWarning && !m_fStimOnly )
			{
				// Kill start timer
				if( !m_fStimOnly ) KillTimer( START_TIMER );

				// We only set this if we're recording, not displaying a stimulus,
				// and, obviously, if the # of secs specified is positivie
				if( !m_fStimOnly && !m_fPrecue && !m_fWarning && ( m_dTimeoutSecs > 0 ) )
				{
					SetTimer( TABLET_TIMER, TIMER_INTERVAL, NULL );
					// feedback visibility timer if appropriate && not begin recording immediately
					if( !fImmediately && fHideFeedback && fHideShowAfter &&
						!fHideShowAfterStrokes && ( dHideShowCount > 0. ) )
						SetTimer( VISIBILITY_TIMER, TIMER_INTERVAL, NULL );
				}
				m_fBeganRecording = TRUE;
				CDC* pDC = GetDC();
				OnDraw( pDC );
				ReleaseDC( pDC );

				// once recording has begun, we hide the instruction if flag is set
				CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
				CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
				if( pRV && !m_fInstrVis ) pRV->UpdateInstruction( _T("") );

				PlayASound( m_szCondID, SOUND_PENDOWN, ::GetTrialRunInfo() );
			}
		}

		// check to see if pen-lift timeout has occurred
		// this information must be obtained from the digitizer (inputmod)
		// because only it knows if pen is down or lifted
		if( !m_fPrecue && !m_fWarning && fRecording && m_fBeganRecording && !m_fDoWait && !m_fTrialEnded )
		{
			long lTicks = 0;
			m_pDigitizer->get_TimeoutTicks( &lTicks );
			if( lTicks >= m_dTimeoutTicks )
			{
				if( !m_fStimOnly ) ::PlayASound( m_szCondID, SOUND_RECORDING_END, ::GetTrialRunInfo() );
				OutputAMessage( _T("Pen out-of-range timeout occurred.") );
				CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
				if( ::IsContinueWithEnter() )
				{
					CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
					pRV->UpdateInstruction( _T("Done - Press ENTER to continue") );
					m_fDoWait = TRUE;
					m_pDigitizer->Stop();
					Invalidate();
				}
				else if( m_fCanContinue )
				{
					m_fCanContinue = FALSE;
					m_fTrialEnded = FALSE;
					if( !m_fTest ) pMF->Record();
					else pMF->OnDigTest2();
				}
			}
		}
	}

	return TRUE;
}

// resets devices by destroying their com/dcom instances & recreating
void CGraphView::Reset()
{
	CString szAppPath, szUser;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");

	if( _dp )
	{
		_dp->Release();
		_dp = NULL;
	}

	// Create object
	// NOTE: We had to add this to the initial load of the screen to solve time issues
	// This means that the gripper object is created when this window is created
	// otherwise with each trial, the initialization time required for the gripper device
	// was causing abnormal delays
	if( !_dp && ::IsGripperInstalled() && ::IsGripperModeOn() )
	{
		::OutputAMessage( _T("RESETTING DAQPad device."), TRUE );

		HRESULT hr = CoCreateInstance( CLSID_DAQPad, NULL, CLSCTX_ALL,
									   IID_IDAQPad, (LPVOID*)&_dp );
		if( FAILED( hr ) )
		{
			CString szMsg;
			szMsg.Format( _T("Error creating DAQPad object: %x."), hr );
			BCGPMessageBox( szMsg );
			return;
		}

		if( FAILED( _dp->Reset() ) )
		{
			_dp->Release();
			_dp = NULL;
			BCGPMessageBox( _T("Error initializing DAQPad object.") );
			return;
		}

		_dp->Stop();
	}

	if( m_pDigitizer )
	{
		m_pDigitizer->Release();
		m_pDigitizer = NULL;
	}
//	if( !::IsGripperModeOn() )
	{
		if( ::IsTabletInstalled() && ::IsTabletModeOn() )
			::OutputAMessage( _T("RESETTING tablet device."), TRUE );

		HRESULT hr = CoCreateInstance( CLSID_Digitizer, NULL, CLSCTX_ALL,
									IID_IDigitizer, (LPVOID*)&m_pDigitizer );
		if( FAILED( hr ) )
		{
			CString szErr;
			szErr.LoadString( IDS_GV_TABLETERR );
			m_pDigitizer = NULL;
			CMainFrame* pMF = (CMainFrame*)(AfxGetApp()->m_pMainWnd);
			if( pMF ) pMF->BadTablet();
			return;
		}
		// set authorization
		CString szAuthCode;
		GetAuthCodeIM( szAuthCode );
		m_pDigitizer->SetAuthorization( szAuthCode.AllocSysString() );
		// set the tablet window (this)...the recording window
		m_pDigitizer->put_TabletWindow( (VARIANT*)m_hWnd );
		// pass desktop window (currently) to digitizer
		// used to calculate positions relative to upper left point on desktop
		m_pDigitizer->put_TargetWindow( (VARIANT*)GetDesktopWindow()->m_hWnd );
		// Tell the digitizer whether we are logging or not
		m_pDigitizer->SetLoggingOn( ::GetLogTablet() && ::IsLoggingOn(), szAppPath.AllocSysString() );
		// Inform whether tablet maps to recording window or to entire desktop
		m_pDigitizer->TabletWindowOnly( !IsDesktopModeOn() );
		// Is window sized to tablet
		m_pDigitizer->put_RealSize( m_fRealSize );
		// inform digitizer of sampling rate specified in settings
		m_pDigitizer->put_SamplingRate( ::GetSamplingRate() );
		// inform digitizer of min pen pressure specified in settings
		m_pDigitizer->put_MinPenPressure( ::GetMinPenPressure() );
		// inform digitizer of device resolution specified in settings
		m_pDigitizer->put_DeviceResolution( ::GetDeviceResolution() );
		// set tilt recording to false
		m_pDigitizer->put_UseTilt( FALSE );
		// set max pen pressure to default
		m_pDigitizer->put_MaxPenPressure( ::GetMaxPenPressure() );
		// set discontinuity error & notification
		m_pDigitizer->put_DisError( 0.3 );
		m_pDigitizer->put_DisNotify( FALSE );
		// tablet dimensions
		double dX, dY;
		::GetTabletDimensions( dX, dY );
		m_pDigitizer->put_TabletDimension( dX, dY );
		// display dimensions
		::GetDisplayDimensions( dX, dY );
		m_pDigitizer->put_DisplayDimension( dX, dY );
		// begin recording immediately?
		m_pDigitizer->put_RecordImmediately( FALSE );
		// stop on wrong target?
		m_pDigitizer->put_StopWrongTarget( FALSE );
		// start on first correct target?
		m_pDigitizer->put_StartFirstTarget( FALSE );
		// stop on last correct target?
		m_pDigitizer->put_StopLastTarget( FALSE );
		// drawing settings
		m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ::GetLineThickness(), ::GetLineColor1(),
									::GetLineColor2(), ::GetLineColor3(), ::GetMPPColor(),
									::GetEllipseColor(), ::GetEllipseSize(), ::GetBkColor() );
		m_pDigitizer->put_VaryLineThickness( TRUE );
		m_pDigitizer->put_MaxLineThickness( ::GetMaxLineThickness() );

		// inform digitizer of set background color so its draw method will be the same
		DWORD dwBkColor = ::GetBkColor();
		m_pDigitizer->put_BkColor( dwBkColor );

		hr = m_pDigitizer->Initialize( (VARIANT*)NULL, FALSE, !::IsTabletModeOn() );
	}
}

// called to test the recording device
void CGraphView::Test()
{
	// just a check..if we're recording, stop & clear screen
	if( fRecording )
	{
		Stop();
		Clear();
	}

	// default values for testing
	m_fDoWait = FALSE;
	m_fTest = TRUE;
	m_dTimeoutStart = START_LIMIT / 1000;
	m_dTimeoutSecs = RECORD_LIMIT / 1000;
	m_dTimeoutTicks = 3000;
	m_dTimeoutTicksNA = 3000;
	m_nMinPressure = ::GetMinPenPressure();
	m_nRate = ::GetSamplingRate();
	m_dResolution = ::GetDeviceResolution();
	m_fPrecue = m_fWarning = FALSE;
	m_fRealSize = FALSE;
	m_fCanContinue = FALSE;
	m_fTrialEnded = FALSE;

	// todo: global settings or defaults?
	m_fHideCursor = TRUE;
	m_fCursorHidden = FALSE;

	// we use a generic data file for testing
	CString szFile;
	::GetDataPathRoot( szFile );
	if( szFile == _T("") ) szFile = _T("test.");
	else szFile += _T("\\test.");

	if( ::IsGripperModeOn() ) szFile += _T("frd");
	else szFile += _T("hwr");

	CFileFind ff;
	// delete it first if it exists
	if( ff.FindFile( szFile ) ) CFile::Remove( szFile );

	if( !m_pDigitizer  && !::IsGripperModeOn() )
	{
		((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnDigTest2();
		return;
	}

	// begin
	Record( _T(""), _T(""), szFile );
}

// This is the big-daddy of them all...job being to get the ball rolling
// It initializes data and devices and begins the recording process
// This is the last function called before all prior callers return to
// begin the asynchronous portion during recording/displaying
//
// Arguments:
// szExpID		- Experiment ID
// szCondID		- Condition ID
// szF			- Data file where recorded data will be stored
// szWStim		- Warning stimulus
// szPStim		- Precue stimulus
// szStim		- Imperative (main) stimulus file (.STI)
// fStimOnly	- Indicates whether we are simply displaying a stimulus & not recording
// fTrialOnly	- Only recording a single trial (eg, redo)
// dMagnetForce	- The force of the gripper magent (gripper only)
void CGraphView::Record( CString szExpID, CString szCondID, CString szF, CString szWStim,
						 CString szPStim, CString szStim, BOOL fStimOnly, BOOL fTrialOnly,
						 double dMagnetForce )
{
	m_fCanContinue = FALSE;
	m_fTrialEnded = FALSE;
	m_dFBMin1 = 0.;
	m_dFBMax1 = 0.;
	m_dFBVal1 = 0.;
	m_dFBMin2 = 0.;
	m_dFBMax2 = 0.;
	m_dFBVal2 = 0.;

	CString szAppPath, szUser;
	::GetDataPath( szAppPath, ::IsPrivateUserOn() );
	::GetCurrentUser( szUser );
	szAppPath += szUser;
	szAppPath += _T("\\Actions.log");

	// we don't support stimuli unless optilyzer has been purchased
	// this is to protect again imported experiments & those created with demo
	if( !::IsOA() )
	{
		szWStim = _T("");
		szPStim = _T("");
		szStim = _T("");
	}

	// clear the screen since we are starting fresh
	Clear();
	// if we were already recording, reset/stop
	if( fRecording ) Stop();

	// if start view is visible, hide it
	if( m_pStartView && m_pStartView->IsWindowVisible() ) m_pStartView->ShowWindow( SW_HIDE );

	// initialize vars
	m_szExpID = szExpID;
	m_szCondID = szCondID;
	m_szFile = szF;
	m_fStimOnly = fStimOnly;
	m_fBeganRecording = FALSE;
	_TerminateGThread = FALSE;
	m_nTicks = 0L;
	m_nTicks2 = 0L;
	m_nTicks3 = 0L;
	m_nTicksNA = 0L;
	m_nTicksHideShow = 0L;
	m_fDoWait = FALSE;
	_ri.lstPoint.SetPoint( 0, 0 );

	CDC* pDC = GetDC();
	OnDraw( pDC );
	ReleaseDC( pDC );

	// no timeouts if just testing stimulus or element
	if( fStimOnly )
	{
		m_dTimeoutSecs = 0.;
		m_dTimeoutStart = 0.;
	}

	// if we're doing warning or precue, store information for call to do actual recording
	if( ( szPStim != _T("") ) || ( szWStim != _T("") ) )
	{
		m_fWarning = ( szWStim != _T("") );
		m_fPrecue = ( szPStim != _T("") );
		strcpy_s( _ri.szExpID, 4, m_szExpID );
		strcpy_s( _ri.szCondID, 4, m_szCondID );
		strcpy_s( _ri.szFile, _MAX_PATH, szF );
		strcpy_s( _ri.szStim, 4, szStim );
		strcpy_s( _ri.szPStim, 4, szPStim );
		_ri.fTrialOnly = fTrialOnly;
		_ri.dMagnetForce = dMagnetForce;

		// we dont want to record during warning or precue
		szF = _T("");
	}
	// if last call was warning, reset arguments from the globals
	else if( m_fWarning )
	{
		m_fWarning = FALSE;
		m_szExpID = _ri.szExpID;
		m_szCondID = _ri.szCondID;
		szF = _ri.szFile;
		szStim = _ri.szStim;
		szPStim = _ri.szPStim;
		fTrialOnly = _ri.fTrialOnly;
		dMagnetForce = _ri.dMagnetForce;
	}
	// if last call was precue, reset the arguments from the globals
	else if( m_fPrecue )
	{
		m_fPrecue = FALSE;
		m_szExpID = _ri.szExpID;
		m_szCondID = _ri.szCondID;
		szF = _ri.szFile;
		szStim = _ri.szStim;
		szPStim = _ri.szPStim;
		fTrialOnly = _ri.fTrialOnly;
		dMagnetForce = _ri.dMagnetForce;
	}

	HRESULT hr = E_FAIL;

	// TABLET INITIALIZATION
	if( m_pDigitizer && fStimOnly )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;

		// x/y/z/stroke info in corner on
		m_pDigitizer->put_NoTracking( FALSE );
		// pass the output window pointer to the digitizer (inputmod) for messagin
		CWnd* pMsgWnd = pMF ? (CWnd*)pMF->GetMsgPane() : NULL;
		m_pDigitizer->SetOutputWindow( (VARIANT*)pMsgWnd );
		// set the tablet window (this)...the recording window
		m_pDigitizer->put_TabletWindow( (VARIANT*)m_hWnd );
		// pass desktop window (currently) to digitizer
		// used to calculate positions relative to upper left point on desktop
		m_pDigitizer->put_TargetWindow( (VARIANT*)this->GetDesktopWindow()->m_hWnd );
		// Tell the digitizer whether we are logging or not
		m_pDigitizer->SetLoggingOn( ::GetLogTablet() && ::IsLoggingOn(), szAppPath.AllocSysString() );
		// Inform whether tablet maps to recording window or to entire desktop
		m_pDigitizer->TabletWindowOnly( !IsDesktopModeOn() );
		// Is window sized to tablet
		m_pDigitizer->put_RealSize( m_fRealSize );
		// inform digitizer of device resolution specified in settings
		m_pDigitizer->put_DeviceResolution( m_dResolution );
		// set tilt to false
		m_pDigitizer->put_UseTilt( FALSE );
		// set max pen pressure to default
		m_pDigitizer->put_MaxPenPressure( ::GetMaxPenPressure() );
		// set discontinuity error & notification
		m_pDigitizer->put_DisError( 0.3 );
		m_pDigitizer->put_DisNotify( FALSE );
		// windows sizes set for maximization
		m_pDigitizer->put_LeftViewMaximizeSize( ::GetLVMaximizeSize() );
		// display dimensions
		double dX, dY;
		::GetDisplayDimensions( dX, dY );
		m_pDigitizer->put_DisplayDimension( dX, dY );
		// default drawing settings
		m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ::GetLineThickness(), ::GetLineColor1(),
									::GetLineColor2(), ::GetLineColor3(), ::GetMPPColor(),
									::GetEllipseColor(), ::GetEllipseSize(), ::GetBkColor() );
		m_pDigitizer->put_VaryLineThickness( TRUE );
		m_pDigitizer->put_MaxLineThickness( ::GetMaxLineThickness() );

		// Set the stimulus file info
		CString szPath;
		::GetDataPathRoot( szPath );
		szPath += _T("\\stimuli\\");
		hr = m_pDigitizer->SetStimulusInfo( szPath.AllocSysString(),
											szStim.AllocSysString(),
											::GetDeactivateWrongTarget() );

		// inform digitizer of set background color so its draw method will be the same
		DWORD dwBkColor = ::GetBkColor();
		m_pDigitizer->put_BkColor( dwBkColor );

		// get display device context
		CDC* pDC = GetDC();
#ifdef _PAPER
		// draw the grid in the recording window (if compiled)
		DrawGrid( pDC );
#endif	// _PAPER

		// display
		hr = m_pDigitizer->Initialize( (VARIANT*)pDC, TRUE, !IsTabletModeOn() );

		// release device context
		ReleaseDC( pDC );

		// if initialization succeeded
		if( SUCCEEDED( hr ) )
		{
			// main timer (used for messaging for stimuli)
			KillTimer( MAIN_TIMER );
			SetTimer( MAIN_TIMER, ::GetGlobalInterval(), NULL );

			// we are now recording (legacy)
			fRecording = TRUE;
		}
		// else we failed in initialization...stop & cleanup
		else
		{
			// reset everything
			Stop();
			pMF->Record( _T(""), FALSE, _T(""), _T(""), _T(""), TRUE, FALSE );
			// inform user
			if( ::IsTabletInstalled() && ::IsTabletModeOn() )
				BCGPMessageBox( _T("Error in digitizer initialization.") );
			if( pMF )
			{
				pMF->m_fContinue = FALSE;
				// reset the app windows back to normal
				pMF->ResetSizes( TRUE );
				// now no action is occurring
				CLeftView* pLV = pMF->GetLeftView();
				pLV->SetActionState( ACTION_NONE );
			}
		}
	}
	// record via digitizer/mouse
	else if( m_pDigitizer && !::IsGripperModeOn() )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;

		// hide cursor if set
		if( m_fHideCursor && ::IsTabletModeOn() )
		{
			ShowCursor( FALSE );
			m_fCursorHidden = TRUE;
		}

		// x/y/z/stroke info in corner on
		m_pDigitizer->put_NoTracking( FALSE );
		// pass the output window pointer to the digitizer (inputmod) for messagin
		CWnd* pMsgWnd = pMF ? (CWnd*)pMF->GetMsgPane() : NULL;
		m_pDigitizer->SetOutputWindow( (VARIANT*)pMsgWnd );
		// inform digitizer of data output file (can be blank)
		m_pDigitizer->put_OutputFile( szF.AllocSysString() );
		// set the tablet window (this)...the recording window
		m_pDigitizer->put_TabletWindow( (VARIANT*)m_hWnd );
		// pass desktop window (currently) to digitizer
		// used to calculate positions relative to upper left point on desktop
		m_pDigitizer->put_TargetWindow( (VARIANT*)this->GetDesktopWindow()->m_hWnd );
		// Tell the digitizer whether we are logging or not
		m_pDigitizer->SetLoggingOn( ::GetLogTablet() && ::IsLoggingOn(), szAppPath.AllocSysString() );
		// Inform whether tablet maps to recording window or to entire desktop
		m_pDigitizer->TabletWindowOnly( !IsDesktopModeOn() );
		// Is window sized to tablet
		m_pDigitizer->put_RealSize( m_fRealSize );
		// inform digitizer of sampling rate specified in settings
		m_pDigitizer->put_SamplingRate( m_nRate );
		// inform digitizer of min pen pressure specified in settings
		m_pDigitizer->put_MinPenPressure( m_nMinPressure );
		// inform digitizer of device resolution specified in settings
		m_pDigitizer->put_DeviceResolution( m_dResolution );
		// windows sizes set for maximization
		m_pDigitizer->put_LeftViewMaximizeSize( ::GetLVMaximizeSize() );
		// tablet dimensions
		double dX, dY;
		::GetTabletDimensions( dX, dY );
		m_pDigitizer->put_TabletDimension( dX, dY );
		// display dimensions
		::GetDisplayDimensions( dX, dY );
		m_pDigitizer->put_DisplayDimension( dX, dY );
		// begin recording immediately?
		m_pDigitizer->put_RecordImmediately( fImmediately );
		// stop on wrong target?
		m_pDigitizer->put_StopWrongTarget( fStopWrongTarget );
		// start on first correct target?
		m_pDigitizer->put_StartFirstTarget( fStartFirstTarget );
		// stop on last correct target?
		m_pDigitizer->put_StopLastTarget( fStopLastTarget );
		// drawing settings & tilt
		IProcSetting* pPS = NULL;
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
		if( SUCCEEDED( hr ) )
		{
			hr = pPS->Find( m_szExpID.AllocSysString() );
			if( SUCCEEDED( hr ) )
			{
				// set tilt recording
				BOOL fTilt = FALSE;
				pPS->get_UseTilt( &fTilt );
				m_pDigitizer->put_UseTilt( fTilt );

				// set max pen pressure
				short nMaxPP = 0;
				pPS->get_MaxPenPressure( &nMaxPP );
				m_pDigitizer->put_MaxPenPressure( nMaxPP );

				// set discontinuity error & notification
				BOOL fNotify = FALSE;
				double dDisError = 0;
				pPS->get_DisError( &dDisError );
				pPS->get_DisNotify( &fNotify );
				m_pDigitizer->put_DisError( dDisError );
				m_pDigitizer->put_DisNotify( fNotify );

				// drawing settings
				IProcSetRunExp* pPSRE = NULL;
				hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
				if( SUCCEEDED( hr ) )
				{
					short ls, es, lt;
					DWORD bg, l1, l2, l3, e, mpp;
					BOOL fVary;
					pPSRE->GetDrawingOptions( &ls, &es, &bg, &l1, &l2, &l3, &e, &mpp );
					m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ls, l1, l2, l3, mpp, e, es, bg );
					pPSRE->get_VaryLineThickness( &fVary );
					m_pDigitizer->put_VaryLineThickness( fVary );
					pPSRE->get_MaxLineThickness( &lt );
					m_pDigitizer->put_MaxLineThickness( lt );
					pPSRE->Release();
					m_BGColor = bg;
					Invalidate();
				}
			}
			pPS->Release();
		}
		// default if failed
		if( FAILED( hr ) )
		{
			m_pDigitizer->put_UseTilt( FALSE );
			m_pDigitizer->put_MaxPenPressure( ::GetMaxPenPressure() );
			m_pDigitizer->put_DisError( 0.3 );
			m_pDigitizer->put_DisNotify( TRUE );
			m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ::GetLineThickness(), ::GetLineColor1(),
										::GetLineColor2(), ::GetLineColor3(), ::GetMPPColor(),
										::GetEllipseColor(), ::GetEllipseSize(), ::GetBkColor() );
			m_pDigitizer->put_VaryLineThickness( TRUE );
			m_pDigitizer->put_MaxLineThickness( ::GetMaxLineThickness() );
		}

		// get condition-specific values for feedback & set
		if( !m_fTest && !m_fPrecue && !m_fWarning )
		{
			BOOL fCountStrokes = FALSE, fTransform = FALSE;
			double xgain = 0., xrotation = 0., ygain = 0., yrotation = 0.;
			short nMaxStrokes = 0;
			NSConditions cond;
			if( SUCCEEDED( cond.Find( m_szCondID ) ) )
			{
				cond.GetCountStrokes( fCountStrokes );
				cond.GetStrokeMax( nMaxStrokes );
				cond.GetHideFeedback( fHideFeedback );
				cond.GetHideShowAfter( fHideShowAfter );
				cond.GetHideShowAfterShow( fHideShowAfterShow );
				cond.GetHideShowAfterStrokes( fHideShowAfterStrokes );
				cond.GetHideShowCount( dHideShowCount );
				cond.GetTransform( fTransform );
				cond.GetXgain( xgain );
				cond.GetXrotation( xrotation );
				cond.GetYgain( ygain );
				cond.GetYrotation( yrotation );
			}
			m_pDigitizer->CountStrokes( fCountStrokes, nMaxStrokes );
			m_pDigitizer->HideShowAfterStrokes( fHideFeedback && fHideShowAfter && fHideShowAfterStrokes,
												fHideShowAfterShow, dHideShowCount );
			m_pDigitizer->Transformation( fTransform, xgain, xrotation, ygain, yrotation );
			if( fHideFeedback )
			{
				if( !fHideShowAfter )
					m_pDigitizer->HideShow( FALSE );
				else if( !fHideShowAfterStrokes && ( dHideShowCount > 0. ) )
					m_pDigitizer->HideShow( !fHideShowAfterShow );
			}
		}

		// Set the stimulus file info
		CString szPath, szStimulus;
		::GetDataPathRoot( szPath );
		szPath += _T("\\stimuli\\");
		if( m_fWarning ) szStimulus = szWStim;
		else if( m_fPrecue ) szStimulus = szPStim;
		else szStimulus = szStim;
		hr = m_pDigitizer->SetStimulusInfo( szPath.AllocSysString(),
											szStimulus.AllocSysString(),
											::GetDeactivateWrongTarget() );

		// inform digitizer of set background color so its draw method will be the same
		DWORD dwBkColor = ::GetBkColor();
		m_pDigitizer->put_BkColor( dwBkColor );

		// get display device context
		CDC* pDC = GetDC();
#ifdef _PAPER
		// draw the grid in the recording window (if compiled)
		DrawGrid( pDC );
#endif	// _PAPER

		// if not just displaying a stimulus, do the start tone/beep
		// if precue, do a different tone than one specified in settings (TODO)
		if( !fStimOnly )
		{
			if( !m_fWarning && !m_fPrecue )
				::PlayASound( m_szCondID, SOUND_RECORDING_START, ::GetTrialRunInfo() );
			else if( m_fWarning )
				::PlayASound( m_szCondID, SOUND_STIMULUSW_START, ::GetTrialRunInfo() );
			else if( m_fPrecue )
				::PlayASound( m_szCondID, SOUND_STIMULUSP_START, ::GetTrialRunInfo() );
		}

		// if we're not displaying JUST a stimulus or the warning or precue, then begin the recording
		if( !fStimOnly && !m_fWarning && !m_fPrecue )
		{
			BOOL fDoStim = m_fTest ? FALSE : ( szStim != _T("") );
			hr = m_pDigitizer->Start( (VARIANT*)pDC, fDoStim, !::IsTabletModeOn() );
		}
		// otherwise if we're displaying the warning or precue
		else if( m_fWarning || m_fPrecue )
			hr = m_pDigitizer->InitializeForStimulus( (VARIANT*)pDC, !IsTabletModeOn() );
		// otherwise just displaying a stimulus (no recording, but pen points shown)
		else hr = m_pDigitizer->Initialize( (VARIANT*)pDC, TRUE, !IsTabletModeOn() );

		// update recview display (enable)
		if( !fStimOnly && !fTrialOnly )
		{
			CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
			if( pRV ) pRV->Enable( TRUE );
		}

		// if initialization succeeded
		if( SUCCEEDED( hr ) )
		{
			// Set/begin the start or precue timer depending on which call to record
			if( !fStimOnly )
			{
				double dTimeout = m_fWarning ? m_dTimeoutWarning : m_dTimeoutPrecue;

				if( !m_fWarning && !m_fPrecue && ( m_dTimeoutStart > 0 ) )
				{
					// start timer
					SetTimer( START_TIMER, TIMER_INTERVAL, NULL );
					// feedback visibility timer
					if( fImmediately && fHideFeedback && fHideShowAfter &&
						!fHideShowAfterStrokes && ( dHideShowCount > 0. ) )
						SetTimer( VISIBILITY_TIMER, TIMER_INTERVAL, NULL );
				}
				// warning/precue timer
				else if( ( m_fWarning || m_fPrecue ) && ( dTimeout > 0 ) )
					SetTimer( PRECUE_TIMER, TIMER_INTERVAL, NULL );
			}

			// main timer (used for messaging for stimuli)
			KillTimer( MAIN_TIMER );
			SetTimer( MAIN_TIMER, ::GetGlobalInterval(), NULL );

			// we are now recording (in digitizer mode)
			fRecording = TRUE;
			OnDraw( pDC );
			// release device context
			ReleaseDC( pDC );
		}
		// else we failed in initialization...stop & cleanup
		else
		{
			// reset everything
			Stop();
			pMF->Record( _T(""), FALSE, _T(""), _T(""), _T(""), TRUE, FALSE );
			// inform user
			if( ::IsTabletInstalled() && ::IsTabletModeOn() )
				BCGPMessageBox( _T("Error in digitizer initialization.") );
			if( pMF )
			{
				pMF->m_fContinue = FALSE;
				// reset the app windows back to normal
				pMF->ResetSizes( TRUE );
				// now no action is occurring
				CLeftView* pLV = pMF->GetLeftView();
				pLV->SetActionState( ACTION_NONE );
			}
		}
	}
	// GRIPPER INITIALIZATION
	else if( ::IsGripperModeOn() )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CWnd* pMsgWnd = pMF ? (CWnd*)pMF->GetMsgPane() : NULL;
		// Create object
		if( !_dp && ::IsGripperInstalled() )
		{
			HRESULT hr = CoCreateInstance( CLSID_DAQPad, NULL, CLSCTX_ALL,
										   IID_IDAQPad, (LPVOID*)&_dp );
			if( FAILED( hr ) )
			{
				CString szMsg;
				szMsg.Format( _T("Error creating DAQPad object: %x."), hr );
				BCGPMessageBox( szMsg );
				return;
			}

			if( FAILED( _dp->Reset() ) )
			{
				_dp->Release();
				_dp = NULL;
				BCGPMessageBox( _T("Error initializing DAQPad object.") );

				// reset everything
				Stop();
				pMF->Record( _T(""), FALSE, _T(""), _T(""), _T(""), TRUE, FALSE );
				pMF->m_fContinue = FALSE;
				// reset the app windows back to normal
				pMF->ResetSizes( TRUE );
				// now no action is occurring
				CLeftView* pLV = pMF->GetLeftView();
				pLV->SetActionState( ACTION_NONE );

				return;
			}
		}

		// pass the output window pointer to the digitizer (inputmod) for messagin
		m_pDigitizer->SetOutputWindow( (VARIANT*)pMsgWnd );
		// inform digitizer of data output file (can be blank)
		m_pDigitizer->put_OutputFile( NULL );
		// set the tablet window (this)...the recording window
		m_pDigitizer->put_TabletWindow( (VARIANT*)m_hWnd );
		// pass desktop window (currently) to digitizer
		// used to calculate positions relative to upper left point on desktop
		m_pDigitizer->put_TargetWindow( (VARIANT*)this->GetDesktopWindow()->m_hWnd );
		// Tell the digitizer whether we are logging or not
		m_pDigitizer->SetLoggingOn( ::GetLogTablet() && ::IsLoggingOn(), szAppPath.AllocSysString() );
		// Inform whether tablet maps to recording window or to entire desktop
		m_pDigitizer->TabletWindowOnly( FALSE );
		// Is window sized to tablet
		m_pDigitizer->put_RealSize( FALSE );
		// inform digitizer of sampling rate specified in settings
		m_pDigitizer->put_SamplingRate( 120 );
		// inform digitizer of min pen pressure specified in settings
		m_pDigitizer->put_MinPenPressure( 1 );
		// inform digitizer of device resolution specified in settings
		m_pDigitizer->put_DeviceResolution( .001 );
		// windows sizes set for maximization
		m_pDigitizer->put_LeftViewMaximizeSize( ::GetLVMaximizeSize() );
		// tablet dimensions
		double dX, dY;
		::GetTabletDimensions( dX, dY );
		m_pDigitizer->put_TabletDimension( dX, dY );
		// display dimensions
		::GetDisplayDimensions( dX, dY );
		m_pDigitizer->put_DisplayDimension( dX, dY );
		// begin recording immediately?
		m_pDigitizer->put_RecordImmediately( TRUE );
		// stop on wrong target?
		m_pDigitizer->put_StopWrongTarget( FALSE );

		// Set the stimulus file info
		CString szPath, szStimulus = szStim;
		::GetDataPathRoot( szPath );
		szPath += _T("\\stimuli\\");
		hr = m_pDigitizer->SetStimulusInfo( szPath.AllocSysString(),
											szStimulus.AllocSysString(),
											::GetDeactivateWrongTarget() );

		// inform digitizer of set background color so its draw method will be the same
		DWORD dwBkColor = ::GetBkColor();
		m_pDigitizer->put_BkColor( dwBkColor );

		// get display device context
		CDC* pDC = GetDC();

		hr = m_pDigitizer->Initialize( (VARIANT*)pDC, TRUE, !IsTabletModeOn() );

		// release device context
		ReleaseDC( pDC );

		// drawing options
		IProcSetting* pPS = NULL;
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
		if( SUCCEEDED( hr ) )
		{
			hr = pPS->Find( szExpID.AllocSysString() );
			if( SUCCEEDED( hr ) )
			{
				IProcSetRunExp* pPSRE = NULL;
				hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
				if( SUCCEEDED( hr ) )
				{
					short ls, es, lt;
					DWORD bg, l1, l2, l3, e, mpp;
					BOOL fVary;
					pPSRE->GetDrawingOptions( &ls, &es, &bg, &l1, &l2, &l3, &e, &mpp );
					m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ls, l1, l2, l3, mpp, e, es, bg );
					pPSRE->get_VaryLineThickness( &fVary );
					m_pDigitizer->put_VaryLineThickness( fVary );
					pPSRE->get_MaxLineThickness( &lt );
					m_pDigitizer->put_MaxLineThickness( lt );
					pPSRE->Release();
					m_BGColor = bg;
					Invalidate();
				}
			}
			pPS->Release();
		}
		// default if failed
		if( FAILED( hr ) && m_pDigitizer )
		{
			m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ::GetLineThickness(), ::GetLineColor1(),
										::GetLineColor2(), ::GetLineColor3(), ::GetMPPColor(),
										::GetEllipseColor(), ::GetEllipseSize(), ::GetBkColor() );
			m_pDigitizer->put_VaryLineThickness( TRUE );
			m_pDigitizer->put_MaxLineThickness( ::GetMaxLineThickness() );
		}

		// Settings
		::GetGripperSettings( &gs );

		// x/y/z/stroke info in corner off
		if( m_pDigitizer ) m_pDigitizer->put_NoTracking( TRUE );

		// check if single-buffer mode, and if total recording time is sufficient
		// and buffer to ensure completeness
// 22Jun09: GMB: We're no longer using specified samples as to the length of the trial
//		m_dTimeoutSecs = (double)( (double)gs.lSamples / (double)gs.lScanRate ) + 1.;

		// windows sizes set for maximization
		_dp->put_LeftViewMaximizeSize( ::GetLVMaximizeSize() );
		// output file
		_dp->put_OutputFile( szF.AllocSysString() );
		// device
		_dp->put_Device( gs.nDevice );
		// # of channels
		_dp->put_Channels( gs.nChannels );
		// # of total samples
// 22Jun09: GMB: We're no longer using specified samples as to the length of the trial
//				 Instead, we're using normal recording timeout time as the basis (scan rate * time)
//		_dp->put_Samples( gs.lSamples );
		_dp->put_Samples( gs.lScanRate * (long)m_dTimeoutSecs );
		// sampling rate (Hz) ... how many times it samples per sec (each channel)
		_dp->put_SampleRate( gs.lSampleRate );
		// scan rate ... how many times it samples per sec (all chans)
		_dp->put_ScanRate( gs.lScanRate );
		// # of samples for baseline
		_dp->put_BaselineCount( gs.nBaseline );
		// which channel represents what (TODO: only for gripper now)
		_dp->put_ChannelVector( CHAN_LOWER, gs.nChanLower );
		_dp->put_ChannelVector( CHAN_UPPER, gs.nChanUpper );
		_dp->put_ChannelVector( CHAN_LOAD, gs.nChanLoad );
		// gains per channel
		_dp->put_Gain( CHAN_LOWER, (short)gs.dGainLG );
		_dp->put_Gain( CHAN_UPPER, (short)gs.dGainUG );
		_dp->put_Gain( CHAN_LOAD, (short)gs.dGainL );
		// output type
		_dp->put_OutputType( gs.fVolts ?
								eOT_Volts :
								gs.fNewtons ?
									eOT_Newtons :
									eOT_Raw );
		// goodies for Newton calculations (per channel)
		_dp->put_CalibrationConstant( CHAN_LOWER, gs.dCalibrationConstantLG );
		_dp->put_CalibrationConstant( CHAN_UPPER, gs.dCalibrationConstantUG );
		_dp->put_CalibrationConstant( CHAN_LOAD, gs.dCalibrationConstantL );
		_dp->put_ExcitationVoltage( CHAN_LOWER, gs.dExcitationVoltageLG );
		_dp->put_ExcitationVoltage( CHAN_UPPER, gs.dExcitationVoltageUG );
		_dp->put_ExcitationVoltage( CHAN_LOAD, gs.dExcitationVoltageL );
		_dp->put_FullScaleLoad( CHAN_LOWER, gs.dFullScaleLoadLG );
		_dp->put_FullScaleLoad( CHAN_UPPER, gs.dFullScaleLoadUG );
		_dp->put_FullScaleLoad( CHAN_LOAD, gs.dFullScaleLoadL );
		_dp->put_MagnetForce( dMagnetForce );
		// output window, logging on, drawing window
		_dp->SetOutputWindow( (VARIANT*)pMsgWnd );
		_dp->SetLoggingOn( ::GetLogTablet() && ::IsLoggingOn(), szAppPath.AllocSysString() );
		_dp->SetMessageWindow( (VARIANT*)this );

		// do starting tone/beep
		::PlayASound( m_szCondID, SOUND_RECORDING_START, ::GetTrialRunInfo() );

		// start timer for total recording time
		if( m_dTimeoutSecs > 0 )
			SetTimer( GRIPPER_TIMER, TIMER_INTERVAL, NULL );

		m_fBeganRecording = TRUE;

		// Start recording
		fRecording = TRUE;
		CWaitCursor crs;
		// double-buffer mode: samplecount = 1/2 # of samples before you want to being acquiring & displaying
		// single-buffer mode: samplecount = total # of samples
		if( gs.fDoubleBuffer )
			hr = _dp->Record( TRUE, gs.lDBSamples );
		else
			hr = _dp->Record( FALSE, gs.lSamples );
	}
}

// stop recording/displaying/testing
// kill any timers
// release com/dcom objects (except gripper)
// terminate any additional threads
void CGraphView::Stop()
{
	// reset recording vars
	fRecording = FALSE;
	m_fTest = FALSE;
	m_fDoWait = FALSE;

	// we're showing the cursor (if hidden)
	ShowCursor( TRUE );
	m_fCursorHidden = FALSE;
	KillTimer( CURSOR_TIMER );
	lTicksCursor = 0L;

	// related views
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
	if( pRV ) pRV->Enable( FALSE );

	if( m_pDigitizer && !::IsGripperModeOn() )
	{
		KillTimer( MAIN_TIMER );
		KillTimer( TABLET_TIMER );
		KillTimer( START_TIMER );
		KillTimer( PRECUE_TIMER );
		KillTimer( LATENCY_TIMER );

		m_pDigitizer->Stop();
		m_pDigitizer->put_UseTilt( FALSE );
		m_pDigitizer->put_MaxPenPressure( ::GetMaxPenPressure() );
		m_pDigitizer->put_DisError( 0.3 );
		m_pDigitizer->put_DisNotify( FALSE );
		m_pDigitizer->DrawSettings( ::IsLineTypeFixed(), ::GetLineThickness(), ::GetLineColor1(),
									::GetLineColor2(), ::GetLineColor3(), ::GetMPPColor(),
									::GetEllipseColor(), ::GetEllipseSize(), ::GetBkColor() );
		m_pDigitizer->put_VaryLineThickness( TRUE );
		m_pDigitizer->put_MaxLineThickness( ::GetMaxLineThickness() );
		m_BGColor = ::GetBkColor();
		m_pDigitizer->put_NoTracking( FALSE );

		CDC* pDC = GetDC();
		OnDraw( pDC );
		ReleaseDC( pDC );
	}
	else if( ::IsGripperModeOn() )
	{
		if( _dp )
		{
			CWaitCursor crs;
			_dp->Stop();
		}
		if( m_pDigitizer ) m_pDigitizer->put_NoTracking( TRUE );

		KillTimer( GRIPPER_TIMER );
		KillTimer( HACK_TIMER );
		_TerminateGThread = TRUE;
	}
}

// called to clear the display
void CGraphView::Clear()
{
	RECT rect;
	GetClientRect( &rect );
	if( !m_pStartView || !m_pStartView->IsWindowVisible() )
	{
		CWnd* pWnd = GetDesktopWindow();
		pWnd->GetClientRect( &rect );
		InvalidateRect( &rect, TRUE );
		if( m_pDigitizer ) m_pDigitizer->Clear();
		UpdateWindow();
	}
	else if( m_pStartView && m_pStartView->IsWindowVisible() )
	{
		m_pStartView->MoveWindow( &rect, TRUE );
	}
}

// called by framework when timer(s) have been started every n milliseconds
void CGraphView::OnTimer(UINT nIDEvent) 
{
	// update variables which track the various timers

	// main/global timer
	if( ( nIDEvent == MAIN_TIMER ) && m_pDigitizer )
	{
		UINT nInt = ::GetGlobalInterval();
		CDC* pDC = GetDC();
		HRESULT hr = m_pDigitizer->DoOnTimer( nInt, (VARIANT*)pDC );
		ReleaseDC( pDC );
	}
	// total record time timer
	else if( ( nIDEvent == TABLET_TIMER ) || ( nIDEvent == GRIPPER_TIMER ) )
	{
		m_nTicks += TIMER_INTERVAL;
		m_nTicks3 += TIMER_INTERVAL;
		m_nTicksNA += TIMER_INTERVAL;
		this->PostMessage( WT_PACKET, -1, 0 );
	}
	// start of trial timer
	else if( nIDEvent == START_TIMER )
	{
		m_nTicks2 += TIMER_INTERVAL;
		this->PostMessage( WT_PACKET, -1, 0 );
	}
	// precue timer
	else if( nIDEvent == PRECUE_TIMER )
	{
		m_nTicks += TIMER_INTERVAL;
		double dTimeout = m_fWarning ? m_dTimeoutWarning : m_dTimeoutPrecue;
		double dLatency = m_fWarning ? m_dTimeoutLatencyW : m_dTimeoutLatency;

		if( m_nTicks > ( dTimeout * 1000 ) )
		{
			// we're done with displaying precue, kill this timer and begin latency timer
			::PlayASound( m_szCondID, m_fWarning ? SOUND_STIMULUSW_END : SOUND_STIMULUSP_END, ::GetTrialRunInfo() );
			m_nTicks = 0;
			KillTimer( PRECUE_TIMER );
			Clear();
			if( dLatency > 0. ) SetTimer( LATENCY_TIMER, TIMER_INTERVAL, NULL );
			else Record();
		}
	}
	else if( nIDEvent == LATENCY_TIMER )
	{
		m_nTicks += TIMER_INTERVAL;
		double dLatency = m_fWarning ? m_dTimeoutLatencyW : m_dTimeoutLatency;
		if( m_nTicks > ( dLatency * 1000 ) )
		{
			// we're done with displaying precue, kill this timer and begin the recording
			KillTimer( LATENCY_TIMER );
			Record();
		}
	}
	else if( nIDEvent == VISIBILITY_TIMER )
	{
		m_nTicksHideShow += TIMER_INTERVAL;
		if( m_nTicksHideShow > ( dHideShowCount * 1000 ) )
		{
			KillTimer( VISIBILITY_TIMER );
			if( m_pDigitizer ) m_pDigitizer->HideShow( fHideShowAfterShow );
		}
	}
	// ** HACK ALERT **
	else if( nIDEvent == HACK_TIMER )
	{
		// simply end thread (process..recording...your choice for word)
		KillTimer( HACK_TIMER );
		_TerminateGThread = TRUE;
		OnWTPacket( 0, 0 );
	}
	else if( nIDEvent == CURSOR_TIMER )
	{
		lTicksCursor += TIMER_INTERVAL;
		if( ( lTicksCursor >= CURSOR_TIMEOUT ) &&
			fRecording && m_fHideCursor && m_fCursorHidden && ::IsTabletModeOn() )
		{
			KillTimer( CURSOR_TIMER );
			lTicksCursor = 0L;
			ShowCursor( TRUE );
			m_fCursorHidden = FALSE;
		}
	}

	CView::OnTimer( nIDEvent );
}

BOOL CGraphView::OnSetCursor( CWnd* pWnd, UINT nHitTest, UINT message )
{
	if( ( !m_fCursorHidden && !fRecording ) || ( fRecording && !::IsTabletModeOn() ) ||
		( fRecording && ::IsTabletModeOn() && !m_fCursorHidden && fCursorTimer ) )
	{
		SetCursor( AfxGetApp()->LoadCursor( IDC_DRAW ) );
		return TRUE;
	}

	return FALSE;
}

void CGraphView::OnMouseMove( UINT nFlags, CPoint point ) 
{
	// hide cursor if flag set, cursor is not already hidden, is tablet mode & is recording
	// also, we need to ensure timer is not running
	if( m_fHideCursor && !m_fCursorHidden && ::IsTabletModeOn() && fRecording && !fCursorTimer )
	{
		ShowCursor( FALSE );
		m_fCursorHidden = TRUE;
	}

	// if not mouse mode, do nothing
	if( ::IsTabletModeOn() || !m_pDigitizer || ::IsGripperModeOn() )
	{
		CView::OnMouseMove( nFlags, point );
		return;
	}

	// have we moved the mouse (with button down)?
	if( ( point != _ri.lstPoint ) && ( ( nFlags & MK_LBUTTON ) != 0 ) ) m_nTicks3 = 0L;
	// if last point is the same as before, exit (glitch in duplicate data)
	if( point == _ri.lstPoint )
	{
// 25Jun07 - we need these points in situations where the recording starts with button down,
		// the mouse hasnt moved
//		CView::OnMouseMove( nFlags, point );
//		return;
	}
	_ri.lstPoint = point;

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	// pass onmousemove info to digitizer (inputmod) to process data
	// intputmod actually draws the points
	BOOL fInval = FALSE;
	CDC* pDC = GetDC();

	HRESULT hr = m_pDigitizer->OnMouseMove( (long)nFlags, (VARIANT*)&point, (VARIANT*)pDC, &fInval );
	if( FAILED( hr ) ) BCGPMessageBox( IDS_GV_MOUSEERR );

	// if we just began recording, start the total record time timer
	if( ( fRecording || m_fTest ) && !m_fBeganRecording && ( hr == S_START ) )
	{
		if( !m_fPrecue && !m_fWarning && ( m_dTimeoutSecs > 0 ) )
		{
			SetTimer( TABLET_TIMER, TIMER_INTERVAL, NULL );
			// feedback visibility timer if appropriate && not begin recording immediately
			if( !fImmediately && fHideFeedback && fHideShowAfter &&
				!fHideShowAfterStrokes && ( dHideShowCount > 0. ) )
				SetTimer( VISIBILITY_TIMER, TIMER_INTERVAL, NULL );
		}
		m_fBeganRecording = TRUE;
		OnDraw( pDC );

		// once recording has begun, we hide the instruction if flag is set
		CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
		if( pRV && !m_fInstrVis ) pRV->UpdateInstruction( _T("") );
	}
	ReleaseDC( pDC );

	if( m_fCanContinue && m_fTrialEnded )
	{
		m_fCanContinue = FALSE;
		m_fTrialEnded = FALSE;
		if( !m_fTest ) ((CMainFrame*)AfxGetApp()->m_pMainWnd)->Record();
		else ((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnDigTest2();
		CView::OnMouseMove(nFlags, point);
		return;
	}

	if( hr == S_FALSE )
	{
		// trial ended, stop recording, set flags
		m_pDigitizer->Stop();
		m_fCanContinue = FALSE;
		m_fTrialEnded = TRUE;
	}
	else if( ( fRecording || m_fTrialEnded ) && hr == S_PENDOWN )
	{
		PlayASound( m_szCondID, SOUND_PENDOWN, ::GetTrialRunInfo() );
	}
	else if( ( fRecording || m_fTrialEnded ) && hr == S_PENUP )
	{
		PlayASound( m_szCondID, SOUND_PENUP, ::GetTrialRunInfo() );
		m_fCanContinue = TRUE;
	}
	else if( ( fRecording || m_fTrialEnded ) && hr == S_TARGETGOOD )
	{
		PlayASound( m_szCondID, SOUND_TARGET_CORRECT, ::GetTrialRunInfo() );
	}
	else if( ( fRecording || m_fTrialEnded ) && hr == S_TARGETBAD )
	{
		PlayASound( m_szCondID, SOUND_TARGET_WRONG, ::GetTrialRunInfo() );
	}
	else if( ( fRecording || m_fTrialEnded ) && hr == S_MAXSTROKES )
	{
		// trial has ended, stop recording, set flags
		m_pDigitizer->Stop();
		m_fCanContinue = FALSE;
		m_fTrialEnded = TRUE;

		if( ::IsContinueWithEnter() )
		{
			CRecView* pRV = pMF ? pMF->GetSettingsPane() : NULL;
			pRV->UpdateInstruction( _T("Done - Press ENTER to continue") );
			m_fDoWait = TRUE;
			Invalidate();
			m_pDigitizer->HideShow( FALSE );
		}
	}

	// TODO: wtf am i doing here?
	if( ( hr != S_FALSE ) && fInval )
	{
		RECT rect;
		GetWindowRect( &rect );
		InvalidateRect( &rect, TRUE );
		UpdateWindow();
	}

	CView::OnMouseMove(nFlags, point);
}

// function to set the timeout values
// initially called when experiment is to begin (obtained from experiment settings)
// and called when changes in recview are done
void CGraphView::SetTimes( double dStart, double dSecs, double lTicks )
{
	m_dTimeoutStart = dStart;
	m_dTimeoutSecs = dSecs;
	if( ::IsTabletModeOn() )
		m_dTimeoutTicks = (long)( lTicks * m_nRate );
	else
		m_dTimeoutTicks = lTicks * 1000;
	m_dTimeoutTicksNA = lTicks * 1000;
}

// function to set the warning timeout values
// called for each trial, based upon condition settings
void CGraphView::SetWarningTimes( double dWarning, double dLatency )
{
	m_dTimeoutWarning = dWarning;
	m_dTimeoutLatencyW = dLatency;
}

// function to set the precue timeout values
// called for each trial, based upon condition settings
void CGraphView::SetPrecueTimes( double dPrecue, double dLatency )
{
	m_dTimeoutPrecue = dPrecue;
	m_dTimeoutLatency = dLatency;
}

// function to set flags
// called for each trial, based upon condition settings
void CGraphView::SetFlags( BOOL fImmed, BOOL fWrongTarget, BOOL fRealSize,
						   BOOL fFirstTarget, BOOL fLastTarget )
{
	fImmediately = fImmed;
	fStopWrongTarget = fWrongTarget;
	m_fRealSize = fRealSize;
	fStartFirstTarget = fFirstTarget;
	fStopLastTarget = fLastTarget;
}

// function to set the device settings
// sampling rate, minimum pen pressure, device resolution
void CGraphView::SetRecordingSettings( short nRate, short nMinPressure,
									   double dResolution )
{
	m_nRate = nRate;
	m_nMinPressure = nMinPressure;
	m_dResolution = dResolution;

	m_fPrecue = FALSE;
	m_fWarning = FALSE;
}

// if recording window is selected (given focus), help is displayed in the status bar
void CGraphView::OnSetFocus(CWnd* pOldWnd) 
{
	CView::OnSetFocus(pOldWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Recording Window: This shows the recordings during experiments.");
	pMF->ShowStatusText( szMsg );
}

// when focus leaves, status says "ready"
void CGraphView::OnKillFocus(CWnd* pNewWnd) 
{
	CView::OnKillFocus(pNewWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Ready.");
	pMF->ShowStatusText( szMsg );
}

// called by daqpad/gripper when the device has completed it's recording
afx_msg LRESULT CGraphView::OnFinished(WPARAM wParam, LPARAM lParam)
{
	// if gripper device not valid...well..u know
	if( !_dp ) return 0;

	// if single-buffer mode, draw data
	if( !gs.fDoubleBuffer )
	{
		// write/draw data
		CDC* pDC = GetDC();
		_dp->AcquireData( TRUE );
		_dp->WriteDrawPoints( (VARIANT*)pDC );
		ReleaseDC( pDC );
		SetTimer( HACK_TIMER, 4000 /* 4 seconds */, NULL );
	}
	// if double-buffer mode, just terminate
	else
	{
		_TerminateGThread = TRUE;
		OnWTPacket( 0, 0 );
	}

	return 0;
}

void CGraphView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	m_bmpMsgWait.LoadBitmap( IDB_MSG_WAIT );
	m_bmpMsgWait.SetBitmapDimension( 98, 12 );
	m_bmpMsgEnter.LoadBitmap( IDB_MSG_ENTER );
	m_bmpMsgEnter.SetBitmapDimension( 240, 25 );
	Reset();
}

// draw a paper-like grid in the recording window
#ifdef _PAPER
void CGraphView::DrawGrid( CDC* pDC )
{
	DWORD bkColor = ::GetBkColor();
	// Todo..we can adjust the lines for the background
	if( bkColor != RGB(255,255,255) ) return;

	RECT rect;
	GetWindowRect( &rect );

	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportExt(pDC->GetDeviceCaps(LOGPIXELSX),
		pDC->GetDeviceCaps(LOGPIXELSY));
	pDC->SetWindowExt(100, -100);

	// Horizontal lines
#define	HL_CNT		6
#define	HL_INC		50
#define	HL_START	200
#define	HL_CLR		RGB(83,194,255)

	CPen penHoriz;
	penHoriz.CreatePen( PS_SOLID, 1, HL_CLR );
	CPen* pOldPen = pDC->SelectObject( &penHoriz );

	for( int i = HL_START; i <= ( HL_START + HL_INC * HL_CNT ); i += HL_INC )
	{
		pDC->MoveTo( 0, rect.top - i );
		pDC->LineTo( rect.right, rect.top - i );
	}

	// Vertical lines
#define	VT_CNT		2
#define	VT_INC		900
#define	VT_START	100
#define	VT_CLR		RGB(252,105,241)
	pDC->SetMapMode(MM_TEXT);
	CPen penVert;
	penVert.CreatePen( PS_SOLID, 1, VT_CLR );
	pDC->SelectObject( &penVert );

	for( int j = VT_START; j < ( VT_START + VT_INC * VT_CNT ); j += VT_INC )
	{
		pDC->MoveTo( rect.left + j, 0 );
		pDC->LineTo( rect.left + j, rect.bottom );
	}

	pDC->SelectObject( pOldPen );
}
#endif	// _PAPER

// This was added here as well as leftview...because if recording window had focus
// leftview would not recognize that ESC key was pressed
// ESC key during a recording is the same as hitting the STOP toolbar button
// but ONLY applies to displaying a stimulus
#define	ESC_KEY		27
#define	ENTER_KEY	13
void CGraphView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	OnKeyDown( nChar );
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CGraphView::OnKeyDown(UINT nChar) 
{
	if( nChar == ESC_KEY )
	{
		if( ( CLeftView::GetActionState() == ACTION_STIMULUS ) ||
			( CLeftView::GetActionState() == ACTION_TEST ) )
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			pMF->ObjStop();
		}
	}

	// End trial (user hits key to stop viewing completed trial)
	if( fRecording && m_fDoWait &&
		( CLeftView::GetActionState() != ACTION_TEST ) &&
		( CLeftView::GetActionState() != ACTION_STIMULUS ) &&
		( nChar == ENTER_KEY ) )
	{
		CMainFrame* pWnd = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		// if recording, record else ondigtest2...both in CMainFrame
		// which resets and stops everything
		if( !m_fTest ) pWnd->Record();
		else pWnd->OnDigTest2();
	}
}

// GetDigitizerInfo		- queries the digitizer for it's info.
// Returns		- TRUE for success
// szDesc		- Tablet identification string
// lRes			- Device resolution in lines per cm (obtained from X axis)
// lX			- Range of X axis ( / res = x dimension)
// lX			- Range of Y axis ( / res = y dimension)
// nRate		- Sampling rate
BOOL CGraphView::GetDigitizerInfo( CString& szDesc, long& lRes, long& lX, long& lY, short& nRate, BOOL& fInches )
{
	if( !::IsTabletModeOn() || !::IsTabletInstalled() || ::IsGripperModeOn() )
		return FALSE;

	HRESULT hr = E_FAIL;
	if( !m_pDigitizer )
	{
		hr = CoCreateInstance( CLSID_Digitizer, NULL, CLSCTX_ALL,
							   IID_IDigitizer, (LPVOID*)&m_pDigitizer );
		if( FAILED( hr ) ) return FALSE;
	}

	// set authorization
	CString szAuthCode;
	GetAuthCodeIM( szAuthCode );
	m_pDigitizer->SetAuthorization( szAuthCode.AllocSysString() );

	BSTR bstr;
	hr = m_pDigitizer->get_TabletDesc( &bstr );
	if( SUCCEEDED( hr ) ) szDesc = bstr;

	hr = m_pDigitizer->get_TabletResolution( &lRes );
	m_pDigitizer->get_TabletXRange( &lX );
	m_pDigitizer->get_TabletYRange( &lY );
	m_pDigitizer->get_TabletRate( &nRate );
	if( hr == S_FALSE ) fInches = TRUE;
	else fInches = FALSE;

	return TRUE;
}

//************************************
// Method:    DrawFeedback
// FullName:  CGraphView::DrawFeedback
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: BOOL fStop
// Parameter: double dMin
// Parameter: double dMax
// Parameter: double dVal
// Parameter: BOOL fSecond
// Parameter: BOOL fRefresh
//************************************
void CGraphView::DrawFeedback( BOOL fStop, double dMin, double dMax, double dVal,
							   BOOL fSecond, BOOL fRefresh )
{
	// Device context of window
	CDC* pDC = GetDC();
	if( !pDC ) return;

	// if stopping, reset vars & end
	if( fStop )
	{
		m_dFBMin1 = m_dFBMax1 = m_dFBVal1 = 0.;
		m_dFBMin2 = m_dFBMax2 = m_dFBVal2 = 0.;
		m_dFBValLast1 = IGNOREME;
		m_dFBValLast2 = IGNOREME;
		return;
	}

	// if not refresh, set class vars and draw
	if( !fRefresh )
	{
		if( !fSecond )
		{
			if( m_dFBVal1 != 0. ) m_dFBValLast1 = m_dFBVal1;
			m_dFBMin1 = dMin;
			m_dFBMax1 = dMax;
			m_dFBVal1 = dVal;
		}
		else
		{
			if( m_dFBVal2 != 0. ) m_dFBValLast2 = m_dFBVal2;
			m_dFBMin2 = dMin;
			m_dFBMax2 = dMax;
			m_dFBVal2 = dVal;
		}
	}
	// otherwise, we're using the previous values
	else
	{
		if( !fSecond )
		{
			dMin = m_dFBMin1;
			dMax = m_dFBMax1;
			dVal = m_dFBVal1;
		}
		else
		{
			dMin = m_dFBMin2;
			dMax = m_dFBMax2;
			dVal = m_dFBVal2;
		}
	}

	// if min & max are 0 or value = IGNOREME, end
	if( ( ( dMin == 0. ) && ( dMax == 0. ) ) || ( dVal == IGNOREME ) ) return;
	// if val < min or val > max...
	BOOL fOutOfRange = FALSE, fOutOfRangeLast = FALSE;
	if( ( dVal < dMin ) || ( dVal > dMax ) ) fOutOfRange = TRUE;
	if( fSecond )
	{
		if( ( m_dFBValLast2 < dMin ) || ( m_dFBValLast2 > dMax ) ) fOutOfRangeLast = TRUE;
	}
	else
	{
		if( ( m_dFBValLast1 < dMin ) || ( m_dFBValLast1 > dMax ) ) fOutOfRangeLast = TRUE;
	}

	// what is the % of where the value is
	double dPoint = 0., dPointLast = 0.;
	if( dMax != dMin )
	{
		dPoint = ( ( dVal - dMin ) / ( dMax - dMin ) ) * 100.;
		dPointLast = ( ( ( fSecond ? m_dFBValLast2 : m_dFBValLast1 ) - dMin ) / ( dMax - dMin ) ) * 100.;
	}

	// dimensions & locations
#define FB_WIDTH	50
#define FB_HEIGHT	200
	RECT rect;
	GetClientRect( &rect );
// 	int nRight = rect.right + ( fSecond ? -30 : 10 );
// 	int nLeft = nRight - 10;
// 	int nBottom = -(rect.bottom - 15);
// 	int nTop = nBottom + 100;
	int nRight = ( rect.right - rect.left ) / 2 - FB_WIDTH + ( fSecond ? 2 * FB_WIDTH : 0 );
	int nLeft = nRight - FB_WIDTH;
	int nBottom = -( ( rect.bottom - rect.top ) / 2 + FB_HEIGHT / 2 );
	int nTop = nBottom + FB_HEIGHT;
	int nScale = abs( nBottom - nTop ) / 10;

	// drawing settings
	pDC->SetMapMode( MM_ANISOTROPIC );
	pDC->SetViewportExt( pDC->GetDeviceCaps( LOGPIXELSX ),
						 pDC->GetDeviceCaps( LOGPIXELSY ) );
	pDC->SetWindowExt( 100, -100 );

	// draw 10 rectangles for frame of bar
	CPen pen;
	pen.CreatePen( PS_SOLID, 1, RGB( 0, 0, 0 ) );
	CPen* pOldPen = pDC->SelectObject( &pen );
	for( int i = 1; i <= 10; i++ )
	{
		int y1 = nBottom + ( i * nScale );
		int y2 = nBottom + ( ( i - 1 ) * nScale );
		pDC->Rectangle( nLeft, y1, nRight, y2 );
	}
	pDC->SelectObject( pOldPen );
	pen.DeleteObject();

	// draw arrow for current & last (if appropriate) values
	HBRUSH hbrBlack = CreateSolidBrush( RGB( 0, 0, 0 ) );
	HBRUSH hbrOld = (HBRUSH)pDC->SelectObject( hbrBlack );
	POINT pts[ 4 ];
	// current
	int nHPos = (int)( ( dPoint / 100. ) * ( nScale * 10. ) + nBottom );
	pts[ 0 ].x = pts[ 3 ].x = nLeft - 8;
	pts[ 0 ].y = pts[ 3 ].y = nHPos + 3;
	pts[ 1 ].x = nLeft - 2;
	pts[ 1 ].y = nHPos;
	pts[ 2 ].x = nLeft - 8;
	pts[ 2 ].y = nHPos - 3;
	if( !fOutOfRange && !fStop && ( dVal > dMin ) && ( dVal < dMax ) ) pDC->Polyline( pts, 4 );
	// last
	if( !fStop && ( dVal > dMin ) && ( dVal < dMax ) &&
		( fSecond && ( m_dFBValLast2 != IGNOREME ) ) ||
		( !fSecond && ( m_dFBValLast1 != IGNOREME ) ) )
	{
		nHPos = (int)( ( dPointLast / 100. ) * ( nScale * 10. ) + nBottom );
		pts[ 0 ].x = pts[ 3 ].x = nRight + 8;
		pts[ 0 ].y = pts[ 3 ].y = nHPos + 3;
		pts[ 1 ].x = nRight + 3;
		pts[ 1 ].y = nHPos;
		pts[ 2 ].x = nRight + 8;
		pts[ 2 ].y = nHPos - 3;
		if( !fOutOfRangeLast ) pDC->Polyline( pts, 4 );
	}
	pDC->SelectObject( hbrOld );
	::DeleteObject( hbrBlack );


	// setup text font, etc.
	pDC->SetTextColor( RGB( 0, 0, 0 ) );
	pDC->SetBkColor( m_BGColor );
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_NORMAL;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	// draw text for min & max
	CString szData;
	szData.Format( _T("%.2f"), dMax );
	pDC->TextOut( nLeft - 10, nTop + 20, szData );
	szData.Format( _T("%.2f"), dMin );
	pDC->TextOut( nLeft - 10, nBottom - 5, szData );
	// draw text for title
	if( !fSecond )
	{
		szData = _T("Knowledge of Results" );
		pDC->TextOut( ( rect.right - rect.left ) / 2 - 100, nTop + 35, szData );
	}
	// draw text for FB #
	szData.Format( _T("Feedback-%d"), fSecond ? 2 : 1 );
	pDC->TextOut( nLeft - 10, nBottom - 20, szData );
	// draw text for FB value
	szData.Format( _T("= %.3f" ), dVal );
	pDC->TextOut( nLeft - 10, nBottom - 35, szData );

	// red & green brushes/pens for filling in bar
	CPen penRed, penOrange, penGreen;
	penRed.CreatePen( PS_SOLID, 1, RGB( 255, 0, 0 ) );
	penOrange.CreatePen( PS_SOLID, 1, RGB( 255, 255, 0 ) );
	penGreen.CreatePen( PS_SOLID, 1, RGB( 81, 255, 60 ) );
	HBRUSH hbrRed = CreateSolidBrush( RGB( 255, 0, 0 ) );
	HBRUSH hbrOrange = CreateSolidBrush( RGB( 255, 255, 0 ) );
	HBRUSH hbrGreen = CreateSolidBrush( RGB( 81, 255, 60 ) );
	// fill in bars
	// ...if out of range, just fill from 50% in the direction of out of range
	if( fOutOfRange )
	{
		if( dPoint < 50 )
		{
			for( int i = 1; i <= 5; i++ )
			{
				// use red since out of range
				hbrOld = (HBRUSH)pDC->SelectObject( hbrRed );
				pOldPen = pDC->SelectObject( &penRed );

				int y1 = nBottom + ( i * nScale );
				int y2 = nBottom + ( ( i - 1 ) * nScale );
				pDC->Rectangle( nLeft + 1, y1 - 1, nRight - 2, y2 + 1 );

				pDC->SelectObject( hbrOld );
				pDC->SelectObject( pOldPen );
			}
		}
		else
		{
			for( int i = 6; i <= 10; i++ )
			{
				// use red since out of range
				hbrOld = (HBRUSH)pDC->SelectObject( hbrRed );
				pOldPen = pDC->SelectObject( &penRed );

				int y1 = nBottom + ( i * nScale );
				int y2 = nBottom + ( ( i - 1 ) * nScale );
				pDC->Rectangle( nLeft + 1, y1 - 1, nRight - 2, y2 + 1 );

				pDC->SelectObject( hbrOld );
				pDC->SelectObject( pOldPen );
			}
		}
	}
	else
	{
		if( dPoint < 50 )
		{
			for( int i = 5; i >= 1; i-- )
			{
				// if we're beyond point, do nothing
				if( dPoint > ( i * 10 ) ) continue;

				// determine color
				if( dPoint >= 30 )
				{
					// green
					hbrOld = (HBRUSH)pDC->SelectObject( hbrGreen );
					pOldPen = pDC->SelectObject( &penGreen );
				}
				else if( dPoint >= 10 )
				{
					// orange
					hbrOld = (HBRUSH)pDC->SelectObject( hbrOrange );
					pOldPen = pDC->SelectObject( &penOrange );
				}
				else
				{
					// red
					hbrOld = (HBRUSH)pDC->SelectObject( hbrRed );
					pOldPen = pDC->SelectObject( &penRed );
				}

				int y1 = nBottom + ( i * nScale );
				int y2 = nBottom + ( ( i - 1 ) * nScale );
				pDC->Rectangle( nLeft + 1, y1 - 1, nRight - 2, y2 + 1 );

				pDC->SelectObject( hbrOld );
				pDC->SelectObject( pOldPen );
			}
		}
		else
		{
			for( int i = 6; i <= 10; i++ )
			{
				// if we're beyond point, do nothing
				if( dPoint < ( ( i - 1 ) * 10 ) ) continue;

				// determine color
				if( dPoint <= 70 )
				{
					// green
					hbrOld = (HBRUSH)pDC->SelectObject( hbrGreen );
					pOldPen = pDC->SelectObject( &penGreen );
				}
				else if( dPoint <= 90 )
				{
					// orange
					hbrOld = (HBRUSH)pDC->SelectObject( hbrOrange );
					pOldPen = pDC->SelectObject( &penOrange );
				}
				else
				{
					// red
					hbrOld = (HBRUSH)pDC->SelectObject( hbrRed );
					pOldPen = pDC->SelectObject( &penRed );
				}

				int y1 = nBottom + ( i * nScale );
				int y2 = nBottom + ( ( i - 1 ) * nScale );
				pDC->Rectangle( nLeft + 1, y1 - 1, nRight - 2, y2 + 1 );

				pDC->SelectObject( hbrOld );
				pDC->SelectObject( pOldPen );
			}
		}
	}

	// cleanup
	::DeleteObject( hbrRed );
	::DeleteObject( hbrOrange );
	::DeleteObject( hbrGreen );
	penRed.DeleteObject();
	penOrange.DeleteObject();
	penGreen.DeleteObject();
	ReleaseDC( pDC );
}

//************************************
// Method:    OnContextMenu
// FullName:  CGraphView::OnContextMenu
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pWnd
// Parameter: CPoint point
//************************************
void CGraphView::OnContextMenu( CWnd* pWnd, CPoint point )
{
	CMenu menu;
	if( !menu.CreatePopupMenu() ) return;
	menu.AppendMenu( MF_ENABLED | MF_STRING, IDM_VIEW_STARTPAGE, _T("View Start Page") );
	menu.AppendMenu( MF_SEPARATOR );
	menu.AppendMenu( MF_ENABLED | MF_STRING, IDM_E_CLEAR, _T("Clear Recording Window") );

	CBCGPPopupMenu* pPopupMenu = new CBCGPPopupMenu;
	if( !pPopupMenu->Create( this, point.x, point.y, (HMENU)menu.m_hMenu, FALSE, TRUE ) ) return;

	((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnShowPopupMenu( pPopupMenu );
	UpdateDialogControls( this, FALSE );

	SetFocus();
}

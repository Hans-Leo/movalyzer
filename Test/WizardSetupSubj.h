#pragma once

// WizardSetupSubj.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardSetupSubj dialog

class WizardSetupSubj : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardSetupSubj)

// Construction
public:
	WizardSetupSubj();
	~WizardSetupSubj();

// Dialog Data
	enum { IDD = IDD_WIZ_SETUP_SUBJ };
	CBCGPComboBox	m_Cbo;
	CString			m_szSubj;
	CString			m_szID;
	CString			m_szCode;
	CString			m_szName;
	CString			m_szFirst;
	CString			m_szLast;
	COleDateTime	m_DateAdded;
	CString			m_szNotes;
	CString			m_szPrvNotes;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnCreatesubj();
	afx_msg void OnSelchangeCboSubjs();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

// WizardImportCond.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportCond.h"

#include "WizardImportSheet.h"
#include "..\DataMod\DataMod.h"
#include "AddConditionDlg.h"

#include "WizardImportGenStart.h"
#include "WizardImportExp.h"
#include "WizardImportGrp.h"
#include "WizardImportFormat.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#include "Conditions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportCond property page

IMPLEMENT_DYNCREATE(WizardImportCond, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportCond, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_CONDS, OnSelchangeCboConds)
	ON_BN_CLICKED(IDC_BN_CREATECOND, OnBnCreatecond)
	ON_BN_CLICKED(IDC_CHK_RETAIN, OnChkRetain)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportCond::WizardImportCond() : CBCGPPropertyPage(WizardImportCond::IDD)
{
	m_szCond = _T("-");
	m_szID = _T("");
	m_szDesc = _T("-");
	m_fRetain = FALSE;
	m_szInstr = _T("");
	m_szLex = _T("");
	m_szMin = _T("");
	m_szMax = _T("");
	m_nMin = 1;
	m_nMax = 1;
	m_dStrokeLength = 0.0;
	m_dRangeLength = 0.0;
	m_dStrokeDirection = 0.0;
	m_dRangeDirection = 0.0;
	m_nSkip = 0;
	m_szNotes = _T("");
	m_fStimulus = FALSE;
	m_szStimulus = _T("");

	m_pParent = NULL;
	m_nExpIdx = 3;
	m_nGrpIdx = 5;
}

WizardImportCond::~WizardImportCond()
{
}

void WizardImportCond::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHK_RETAIN, m_chkRetain);
	DDX_Control(pDX, IDC_CBO_CONDS, m_Cbo);
	DDX_CBString(pDX, IDC_CBO_CONDS, m_szCond);
	DDX_Text(pDX, IDC_TXT_CONDID, m_szID);
	DDX_Text(pDX, IDC_TXT_CONDDESC, m_szDesc);
	DDX_Check(pDX, IDC_CHK_RETAIN, m_fRetain);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportCond message handlers

void WizardImportCond::OnSelchangeCboConds() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szDesc;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	NSConditions cond;
	short nVal;
	if( !cond.IsValid() ) return;
	if( FAILED( cond.Find( m_szID ) ) ) return;
	cond.GetInstruction( m_szInstr );
	cond.GetLex( m_szLex );
	cond.GetStrokeMin( nVal );
	m_nMin = nVal;
	m_szMin.Format( _T("%d"), m_nMin );
	cond.GetStrokeMax( nVal );
	m_nMax = nVal;
	m_szMax.Format( _T("%d"), m_nMin );
	cond.GetStrokeLength( m_dStrokeLength );
	cond.GetRangeLength( m_dRangeLength );
	cond.GetStrokeDirection( m_dStrokeDirection );
	cond.GetRangeDirection( m_dRangeDirection );
	cond.GetStrokeSkip( nVal );
	m_nSkip = nVal;
	cond.GetNotes( m_szNotes );
	cond.GetUseStimulus( m_fStimulus );
	cond.GetStimulus( m_szStimulus );

	m_szCond = szItem;
	UpdateData( FALSE );

	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

void WizardImportCond::OnBnCreatecond() 
{
	AddConditionDlg d( &m_existing, this, TRUE );
	if( d.DoModal() )
	{
		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( pos )
		{
			CString szItem = d.m_lstNew.GetNext( pos );
			CString szDelim;
			::GetDelimiter( szDelim );
			int nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				m_szID = szItem.Mid( nPos + 2 );
				TRIM( m_szID );
				m_szDesc = szItem.Left( nPos );
				TRIM( m_szDesc );
				UpdateData( FALSE );

				szItem.Format( _T("%s%s%s"), m_szDesc, szDelim, m_szID );
				int nSel = m_Cbo.AddString( szItem );
				m_Cbo.SetCurSel( nSel );
				m_existing.AddTail( m_szID );

				m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
			}
		}
	}
}

BOOL WizardImportCond::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing group for the selected experiment.") );
	pWnd = GetDlgItem( IDC_BN_CREATEEXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new group to use for this experiment.") );
	pWnd = GetDlgItem( IDC_CHK_RETAIN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to continue using this condition for other imports.") );

	// File/class is used in multiple wizards with different # of pages
	// DEFAULT page index # is in constructor (for import wizard)
	// if Generate data wizard, adjust index # appropriately
	m_pParent = (CBCGPPropertySheet*)GetParent();
	pWnd = m_pParent->GetPage( 0 );
	BOOL fEnable = FALSE;
	if( pWnd->IsKindOf( RUNTIME_CLASS( WizardImportGenStart ) ) )
	{
		m_nExpIdx--;
		m_nGrpIdx--;
	}
	else
	{
		WizardImportFormat* pFmt = (WizardImportFormat*)m_pParent->GetPage( 1 );
		CString szFormat = pFmt->m_szFormat;
		if( szFormat == _T("MOVALYZER") ) fEnable = TRUE;
	}

	if( fEnable )
	{
		CWnd* pWnd = GetDlgItem( IDC_CHK_RETAIN );
		pWnd->EnableWindow( TRUE );
		m_fRetain = TRUE;
		UpdateData( FALSE );
		OnChkRetain();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportCond::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardImportCond::OnSetActive() 
{
	BSTR bstrItem = NULL;
	CString szID, szDesc, szDelim, szItem;
	::GetDelimiter( szDelim );
	HRESULT hr2 = E_FAIL;

	WizardImportExp* pExp = (WizardImportExp*)m_pParent->GetPage( m_nExpIdx );
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CLeftView* pLV = pMF->GetLeftView();
	CString szExpID = pExp->m_szID;
	if( szExpID != m_szExpID )
	{
		m_Cbo.ResetContent();
		m_szExpID = szExpID;
		m_szID.Empty();
		m_szDesc.Empty();
		m_szCond.Empty();
		UpdateData( FALSE );
	}
	else
	{
		if( m_fRetain || m_szID != _T("") ) m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
		else m_pParent->SetWizardButtons( PSWIZB_BACK );
		return CBCGPPropertyPage::OnSetActive();
	}

	// Fill condition list (for selected experiment)
	NSConditions cond;
	if( !cond.IsValid() ) return TRUE;
	m_existing.RemoveAll();
	HRESULT hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetID( szID );

		if( !pExp->m_fNew )
		{
			hr2 = pLV->m_pECL->Find( szExpID.AllocSysString(), szID.AllocSysString() );
		}
		else hr2 = S_OK;
		if( SUCCEEDED( hr2 ) )
		{
			cond.GetDescriptionWithID( szItem );
			m_Cbo.AddString( szItem );
			m_existing.AddTail( szID );
		}

		hr = cond.GetNext();
	}
	// if only 1 in the list, select it
	if( m_Cbo.GetCount() == 1 )
	{
		m_Cbo.SetCurSel( 0 );
		OnSelchangeCboConds();
	}

	if( m_fRetain || m_szID != _T("") ) m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

LRESULT WizardImportCond::OnWizardNext() 
{
	WizardImportGrp* pGrp = (WizardImportGrp*)m_pParent->GetPage( m_nGrpIdx );
	if( pGrp->m_szID != _T("") )
		m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

void WizardImportCond::OnChkRetain() 
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_CBO_CONDS );
	if( pWnd ) pWnd->EnableWindow( !m_fRetain );
	pWnd = GetDlgItem( IDC_BN_CREATECOND );
	if( pWnd ) pWnd->EnableWindow( !m_fRetain );

	if( m_fRetain || m_szID != _T("") ) m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );
}

BOOL WizardImportCond::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	CBCGPPropertySheet* pParent = (CBCGPPropertySheet*)GetParent();
	if( pParent )
	{
		if( pParent->IsKindOf( RUNTIME_CLASS( WizardImportSheet ) ) )
			::GoToHelp( _T("importexport.html"), this );
		else
			::GoToHelp( _T("datageneration_wizard.html"), this );
	}

	return TRUE;
}

BOOL WizardImportCond::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

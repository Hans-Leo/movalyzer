#pragma once

// WizardSetupEnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardSetupEnd dialog

class WizardSetupEnd : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardSetupEnd)

// Construction
public:
	WizardSetupEnd();
	~WizardSetupEnd();

// Dialog Data
	enum { IDD = IDD_WIZ_SETUP_END };
	CString	m_szExpID;
	CString	m_szExpDesc;
	CString	m_szCondID;
	CString	m_szCondDesc;
	CString	m_szCondReps;
	CString	m_szGrpID;
	CString	m_szGrpDesc;
	CString	m_szSubjID;
	CString	m_szSubjName;
	CString	m_szType;
	short	m_nExpType;

// Overrides
public:
	virtual BOOL OnWizardFinish();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
// message map functions
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

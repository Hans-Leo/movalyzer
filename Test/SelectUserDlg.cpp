// SelectUserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "SelectUserDlg.h"
#include "Users.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

// SelectUserDlg dialog

IMPLEMENT_DYNAMIC(SelectUserDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(SelectUserDlg, CBCGPDialog)
END_MESSAGE_MAP()

SelectUserDlg::SelectUserDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(SelectUserDlg::IDD, pParent), m_fOverwrite( FALSE ),
	  m_fSubItems( FALSE ), m_fHasSubItems( FALSE )
{
}

SelectUserDlg::~SelectUserDlg()
{
}

void SelectUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_USERS, m_cboUsers);
	DDX_Check(pDX, IDC_CHK_OVERWRITE, m_fOverwrite);
	DDX_Check(pDX, IDC_CHK_SUBITEMS, m_fSubItems);
}

// SelectUserDlg message handlers

BOOL SelectUserDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_CBO_USERS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing user that will receive the data."), _T("Select User") );
	pWnd = GetDlgItem( IDC_CHK_OVERWRITE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select this if you'd like to copy over existing items."), _T("Overwrite") );
	pWnd = GetDlgItem( IDC_CHK_SUBITEMS );
	if( pWnd ) m_tooltip.AddTool( pWnd, "Select this if you would like this item's children to be downloaded.", _T("Include Children") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Continue with the download."), _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Terminate the download."), _T("Cancel") );

	// fill user list
	CString szID, szDesc, szItem, szDelim;
	NSMUsers u;
	::GetDelimiter( szDelim );
	HRESULT hr = u.ResetToStart();
	if( SUCCEEDED( hr ) )
	{
		while( SUCCEEDED( hr ) )
		{
			u.GetUserID( szID );
			u.GetDescription( szDesc );

			if( szID.Left( 2 ) != _T("UU") )
			{
				szItem.Format( _T("%s%s%s"), szDesc, szDelim, szID );
				m_cboUsers.AddString( szItem );
			}

			hr = u.GetNext();
		}
	}

	// has sub items?
	if( !m_fHasSubItems )
	{
		pWnd = GetDlgItem( IDC_CHK_SUBITEMS );
		pWnd->EnableWindow( FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void SelectUserDlg::OnOK()
{
	UpdateData( TRUE );

	// user
	int nUser = m_cboUsers.GetCurSel();
	if( nUser < 0 )
	{
		BCGPMessageBox( _T("You must first select a user.") );
		CWnd* pWnd = GetDlgItem( IDC_CBO_USERS );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	// extract user ID
	CString szDelim, szItem;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	m_cboUsers.GetLBText( nUser, szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szUserID = szItem.Mid( nPos + 2 );


	CBCGPDialog::OnOK();
}

BOOL SelectUserDlg::PreTranslateMessage(MSG* pMsg)
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}


BOOL SelectUserDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T(""), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

// Processing.cpp : implementation of all processing-related
// ...functions from MainFrm.cpp
//

#include "stdafx.h"
#include "Test.h"

#include "MainFrm.h"
#include "LeftView.h"
#include "MsgView.h"
#include "QuestionnaireFullDlg.h"
#include "GraphView.h"
#include "RecView.h"
#include "ExcludeDlg.h"
#include "TestDeviceDlg.h"
#include "InstructionDlg.h"
#include "../Common/Shared.h"
#include "../Common/UserObj.h"
#include "InputDeviceSettingsDlg.h"
#include "AcceptRedoDlg.h"

#include "..\ProcessMod\ProcessMod.h"
#include "..\ProcessMod\ProcessMod_i.c"
#include "..\DataMod\DataMod.h"
#include "..\DataMod\DataMod_i.c"

#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\NSSharedGUI\Experiments.h"
#include "..\NSSharedGUI\Conditions.h"
#include "..\NSSharedGUI\Stimuli.h"
#include "..\NSSharedGUI\Elements.h"
#include "..\NSSharedGUI\Subjects.h"
#include "..\NSSharedGUI\MQuests.h"
#include "..\NSSharedGUI\TSDlg.h"
#include "..\Common\UserObj.h"
#include "..\NSSharedGUI\AGraphDlg.h"

// Processing Globals
BOOL			_RunTrial2 = FALSE;
BOOL			_RedoTrial = FALSE;
TrialRunInfo	_trInfo;
BOOL			_StopCondition = FALSE;
HTREEITEM		_htreeSubject = NULL;
int				_LastTrial = 0;
int				_LastRep = 0;
int				_OldTWidth = 0;
int				_OldTHeight = 0;
int				_OldRHeight = 0;
BOOL			_fWasMaximized = FALSE;
int				_ClockSecs = 0;
double			_ClockSecsMax = 0.;
BOOL			fTrialOnly = FALSE;
BOOL			_fDidOnce = FALSE;
BOOL			fTFA, fTFJ, fTFR, fTFU, fTFO;
BOOL			fSegF, fSegL, fSegO, fSegS, fSegM, fSegA, fSegV, fSegMM, fSegJ;
BOOL			fExtS, fExt3, fExtO, fExt2;
BOOL			fConA, fConC, fConM, fConN, fConS, fConU, fConO, fConD;
double			dFF = 0.0, dBeta = 0.0;
short			nDec = 0;
CString			_stim;
BOOL			_StimOnly = FALSE;
int				_NumProcessed = 0;
BOOL			_fQuest = FALSE;
BOOL			_fQuestEnd = FALSE;
BOOL			_fMaximize = FALSE;
BOOL			_fFullScreen = FALSE;
BOOL			_fTrialFeedback1 = FALSE;
short			_nFBColumn1 = -1;
short			_nFBStroke1 = -1;
double			_dFBMin1 = 0.;
double			_dFBMax1 = 0.;
double			_dFBVal1 = 0.;
BOOL			_fTrialFeedback2 = FALSE;
short			_nFBColumn2 = -1;
short			_nFBStroke2 = -1;
double			_dFBMin2 = 0.;
double			_dFBMax2 = 0.;
double			_dFBVal2 = 0.;
BOOL			_fSwapColors1 = FALSE;
BOOL			_fSwapColors2 = FALSE;

TrialRunInfo* GetTrialRunInfo() { return &_trInfo; }

// trial-to-trial timer handler & settings
#define	TRIALTOTRIAL_TIMER		100
#define	TIMER_INTERVAL			100

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

int GetMostFrequentOccurrence( CStringList* pList, CString& szError )
{
	if( !pList )
	{
		szError = _T("");
		return 0;
	}

	// get unique ones and place in separate list
	CString szItem;
	CStringList lstDone;
	int nCount = 0, nMax = 0;
	POSITION pos = pList->GetHeadPosition();
	while( pos )
	{
		szItem = pList->GetNext( pos );
		if( !lstDone.Find( szItem ) ) lstDone.AddTail( szItem );
	}

	// now loop through unique list and count from orig list
	CString szItem2;
	POSITION pos2 = lstDone.GetHeadPosition();
	while( pos2 )
	{
		nCount = 0;
		szItem2 = lstDone.GetNext( pos2 );
		pos = pList->GetHeadPosition();
		while( pos )
		{
			szItem = pList->GetNext( pos );
			if( szItem == szItem2 ) nCount++;
		}

		if( nCount > nMax )
		{
			nMax = nCount;
			szError = szItem2;
		}
	}

	return nMax;
}

void CMainFrame::OnOQuick() 
{
	::SetDetailedOutput( !::GetDetailedOutput() );
	IProcess* pProc = Experiments::GetProcessObject( GetMsgPaneList() );
	if( pProc ) pProc->SetRapidView( !::GetDetailedOutput() );
}

void CMainFrame::DeleteProcess()
{
	Experiments::Cleanup();
}

void CMainFrame::BadTablet()
{
	m_fContinue = FALSE;
	m_fRecording = FALSE;
	_RunTrial2 = FALSE;
}

void CMainFrame::ResetSizes( BOOL fOriginal )
{
	static int oldx, oldy, oldw, oldh;
	if( fOriginal )
	{
		if( _fMaximize == WINDOW_ASIS )
		{
			// do nothing
		}
		else if( _fMaximize == TRUE )
		{
			if( !_fFullScreen )
			{
				if( _fWasMaximized ) ShowWindow( SW_MAXIMIZE );
				else ShowWindow( SW_SHOWNORMAL );
			}
			else OnViewFullScreen();
 			SetTreeSize( _OldTWidth );
			SetTreeHeight( _OldTHeight );
			SetRecSize( _OldRHeight );
		}
		else if( _fMaximize == FALSE )
		{
			if( _fWasMaximized ) ShowWindow( SW_MAXIMIZE );
			MoveWindow( oldx, oldy, oldw, oldh, TRUE );
 			SetTreeSize( _OldTWidth );
			SetTreeHeight( _OldTHeight );
			SetRecSize( _OldRHeight );
		}
	}
	else
	{
		if( _fMaximize == WINDOW_ASIS )
		{
			// do nothing
		}
		else if( _fMaximize == TRUE )
		{
			if( !_fFullScreen )
			{
				ShowWindow( SW_SHOWMAXIMIZED );
				SetTreeSize( ::GetLVMaximizeSize() );
				SetRecSize( 40 );
//				SetTreeHeight( 1000 );
				CWnd* pWnd = GetDesktopWindow();
				RECT rd;
				pWnd->GetWindowRect( &rd );
				SetTreeHeight( rd.bottom - 200 );
			}
			else
			{
				OnViewFullScreen();
				SetTreeSize( 0 );
				SetRecSize( 0 );
				CWnd* pWnd = GetDesktopWindow();
				RECT rd;
				pWnd->GetWindowRect( &rd );
				SetTreeHeight( rd.bottom );
			}
		}
		else if( _fMaximize == FALSE )
		{
			double ddx, ddy, dtx, dty;

			::GetDisplayDimensions( ddx, ddy );
			::GetTabletDimensions( dtx, dty );

			if( ( dtx > ddx ) || ( dty > ddy ) )
			{
				ShowWindow( SW_SHOWMAXIMIZED );
				SetTreeSize( ::GetLVMaximizeSize() );
				SetRecSize( 40 );
				SetTreeHeight( 1000 );
			}
			else
			{
				// get original window size & position
				GetFramePos( oldx, oldy, oldw, oldh );

				// convert tablet dimensions to display units
				long ltx, lty;
				::CMToDisplay( dtx, dty, m_hWnd, ltx, lty, TRUE, TRUE );

				// we need to accommodate menu/toolbars/status bar heights
				// for this, we ask the total window size (mainframe)
				// then we inquire for each sub window (instruction view, recording view, but NOT results view since we set it's height to 0)
				// at this point i am unsure how the sizing items impact this...thus, we are in a test phase for this
				int iwH, iwW;		// insruction window dimensions
				int rwH, rwW;		// recording window dimensions
				int twH, twW;		// tree window dimensions
				int fwH, fwW;		// tree window dimensions
				// Get main window res
					// we already have from above (oldw, oldh)
				// Get Instruction window size
				RECT rect;
				::GetWindowRect( GetSettingsPane()->m_hWnd, &rect );
				iwW = rect.right - rect.left;
				iwH = rect.bottom - rect.top;
				// Get Recording window size
				::GetWindowRect( GetRecordingPane()->m_hWnd, &rect );
				rwW = rect.right - rect.left;
				rwH = rect.bottom - rect.top;
				// Get Tree window size
				::GetWindowRect( GetLeftView()->m_hWnd, &rect );
				twW = rect.right - rect.left;
				twH = rect.bottom - rect.top;
				// Get Feedback window size
				::GetWindowRect( GetMsgPane()->m_hWnd, &rect );
				fwW = rect.right - rect.left;
				fwH = rect.bottom - rect.top;

				// calculate additional "space" required by toolbars/menubars/sliders
				long lOthersH = oldh - iwH - rwH - fwH;
				long lOthersW = oldw - rwW - twW;

				// app window width & height
				long nWidth = ::GetLVMaximizeSize() /*min tree*/ +
							ltx /*recording window*/ +
							lOthersW /*buffer for things like slides*/;
				long nHeight = lOthersH /* menu/toolbars/status/sliders */ +
							   iwH /* instruction window size */ +
							   lty /* recording intended size */ +
							   fwH /* feedback window size */ ;

				// app window upper left coordinate
				::GetWindowRect( ::GetDesktopWindow(), &rect );
				long xPos = ( rect.right - nWidth ) / 2;
				long yPos = ( rect.bottom - nHeight ) / 2;

				// set app window
				MoveWindow( xPos, yPos, nWidth, nHeight, TRUE );

				// offsets
				long lox, loy;
				::CMToDisplay( ( 640. / rect.right ), 0., m_hWnd, lox, loy, TRUE, TRUE );

				// set splitters
				// tree split = app width - necessary recording window width - offset
				m_wndSplitter2.SetColumnInfo( 0, nWidth - ltx - lox , 0 );
				// top split (tree + ctrl + rec windows)
				nHeight = iwH /*exp ctrl*/ +
						lty /*recording*/ + 
						7 /*non-accessible item total height*/ +
						loy;
				m_wndSplitter.SetRowInfo( 0, nHeight, 0 );
			}
		}
	}

	m_wndSplitter.RecalcLayout();
	m_wndSplitter2.RecalcLayout();
}

BOOL CMainFrame::RunExperimentShared( CString szExpID, CString szExp, CString szGrpID,
									  CString szSubjID, CString szSubj, IProcSetting* pPS,
									  CStringList* lstConditions, BOOL fAuto )
{
	if( !pPS ) return FALSE;
	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	CRecView* pRV = GetSettingsPane();
	CWaitCursor crs;

	// Experiment trial settings
	_trInfo.szExpID = szExpID;
	_trInfo.szExp = szExp;
	_trInfo.szSubjID = szSubjID;
	_trInfo.szSubj = szSubj;
	_trInfo.szGrpID = szGrpID;
	_trInfo.pConditions = lstConditions;
	_trInfo.szCurCond = _T("");
	IProcSetRunExp* pPSRE = NULL;
	HRESULT hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	if( FAILED( hr ) ) { pPS->Release(); return FALSE; }
	pPSRE->get_ProcImmediately( &_trInfo.fProcess );
	pPSRE->get_DisplayCharts( &_trInfo.fDisplay );
	pPSRE->get_DisplayRaw( &_trInfo.fHWR );
	pPSRE->get_DisplayTF( &_trInfo.fTF );
	pPSRE->get_DisplaySeg( &_trInfo.fSeg );
	pPSRE->get_Summarize( &_trInfo.fSumm );
	pPSRE->get_SummarizeSelect( &_trInfo.fSelectSumm );
	pPSRE->get_SummarizeOnly( &_trInfo.fOnlySumm );
	pPSRE->get_SummarizeOnlyGroup( &_trInfo.fGrpOnlySumm );
	pPSRE->get_SummarizeNormDB( &_trInfo.fNormDBSumm );
	pPSRE->get_Analyze( &_trInfo.fAnalysis );
	pPSRE->get_ViewSubjExt( &_trInfo.fExt );
	pPSRE->get_ViewExpExt( &_trInfo.fExt2 );
	pPSRE->get_ViewSubjCon( &_trInfo.fConsis );
	pPSRE->get_ViewExpCon( &_trInfo.fConsis2 );
	pPSRE->get_TimeoutStart( &_trInfo.dStart );
	pPSRE->get_TimeoutRecord( &_trInfo.dSecs );
	pPSRE->get_TimeoutPenlift( &_trInfo.lTicks );
	pPSRE->get_TimeoutLatency( &_ClockSecsMax );
	_ClockSecsMax *= 1000;
	pPSRE->get_DoQuestionnaire( &_fQuest );
	pPSRE->get_QuestAtEnd( &_fQuestEnd );
	pPSRE->get_MaxRecArea( &_fMaximize );
	pPSRE->get_FullScreen( &_fFullScreen );
	pPSRE->Release();

	// ensure not trying real-size if mapped to recording window
	if( ::IsDesktopModeOn() && ( _fMaximize != WINDOW_ASIS ) ) _fMaximize = TRUE;

	TrialsMode tm;
	pPS->get_TrialMode( &tm );
	_trInfo.nTrialMode = tm;
	::SetContinueWithEnter( ( tm == eTM_Delay ) || ( tm == eTM_DelayAD ) ||
							( tm == eTM_DelayADErr ) || ( tm == eTM_DelayErr ) ||
							( tm == eTM_FirstDelay ) || ( tm == eTM_FirstDelayAD ) ||
							( tm == eTM_FirstDelayADErr ) || ( tm == eTM_FirstDelayErr ) );

	_trInfo.pPS = pPS;
	_trInfo.nTrial = 1;
	_trInfo.hLast = NULL;

	// get appropriate interface
	IProcSetFlags* pPSF = NULL;
	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) ) return FALSE;

	// TF parameters
	_trInfo.pPS->get_FilterFrequency( &dFF );
	_trInfo.pPS->get_RotationBeta( &dBeta );
	_trInfo.pPS->get_Decimate( &nDec );
	pPSF->get_TF_A( &fTFA );
	pPSF->get_TF_J( &fTFJ );
	pPSF->get_TF_R( &fTFR );
	pPSF->get_TF_U( &fTFU );
	pPSF->get_TF_O( &fTFO );
	// Seg parameters
	pPSF->get_SEG_F( &fSegF );
	pPSF->get_SEG_L( &fSegL );
	pPSF->get_SEG_O( &fSegO );
	pPSF->get_SEG_S( &fSegS );
	pPSF->get_SEG_M( &fSegM );
	pPSF->get_SEG_A( &fSegA );
	pPSF->get_SEG_V( &fSegV );
	pPSF->get_SEG_MM( &fSegMM );
	pPSF->get_SEG_J( &fSegJ );
	// Extract parameters
	pPSF->get_EXT_S( &fExtS );
	pPSF->get_EXT_3( &fExt3 );
	pPSF->get_EXT_O( &fExtO );
	pPSF->get_EXT_2( &fExt2 );
	// Consis parameters
	pPSF->get_CON_A( &fConA );
	pPSF->get_CON_C( &fConC );
	pPSF->get_CON_M( &fConM );
	pPSF->get_CON_N( &fConN );
	pPSF->get_CON_S( &fConS );
	pPSF->get_CON_U( &fConU );
	pPSF->get_CON_O( &fConO );
	pPSF->get_CON_D( &fConD );
	pPSF->Release();

	// experiment type
	int nType = EXP_TYPE_HANDWRITING;
	if( ::IsGripperModeOn() ) nType = EXP_TYPE_GRIPPER;

	// Clear previous trial files (for subject)
	int nFileCount = Subjects::GetTrialCount( szExpID, szGrpID, szSubjID, nType );
	if( nFileCount > 0 )
	{
		CString szWarning, szTemp;
		szWarning.LoadString( IDS_TRIALDEL_SURE );
		szTemp.Format( _T("\nThere are currently %d trials completed out of the experiment's total %d trials."),
					   nFileCount, (int)lstConditions->GetCount() );
		szWarning += szTemp;
		if( BCGPMessageBox( szWarning, MB_YESNO) == IDNO ) return FALSE;
		if( nFileCount >= (int)lstConditions->GetCount() )
		{
			szWarning = _T("WARNING: This subject has completed all trials.\n\nDo you want to continue and lose the original trials?");
			if( BCGPMessageBox( szWarning, MB_YESNO | MB_ICONWARNING ) == IDNO ) return FALSE;
		}

		CString szData;
		szData.LoadString( IDS_AL_TRIALSDEL );
		OutputAMessage( szData, TRUE );

		Subjects::CleanProcessFiles( szExpID, szGrpID, szSubjID, nType,
									 TRUE, TRUE, TRUE, TRUE, TRUE, TRUE );
	}
	crs.Restore();

	// Remove from tree
	if( pLV )
	{
		CTreeCtrl& tree = pLV->GetTreeCtrl();
		if( tree.ItemHasChildren( _htreeSubject ) )
			tree.DeleteItem( tree.GetChildItem( _htreeSubject ) );
	}

	// get actual sampling rate & dev res
	double dDevRes = 0.;
	int nSampRate = 0;
	BOOL fSupportTilt = FALSE;
	BOOL fMouse = FALSE;
	{
	long lRes = 0, lX = 0, lY = 0;
	short nRate = 0, nMaxPP = 0;
	BOOL fInches = FALSE;
	CString szDev;
	if( ::GetDigitizerInfo( szDev, lRes, lX, lY, nRate, fInches, fSupportTilt, nMaxPP ) )
	{
		if( ( lX > 0 ) && ( lY > 0 ) )
		{
			nSampRate = nRate;
			dDevRes = 1.0 / ( ( lRes != 0 ) ? lRes : 1.0 );

			if( fInches ) dDevRes *= 2.54;
		}
	}
	else if( !::IsTabletModeOn() && !::IsGripperModeOn() )
	{
		nSampRate = MOUSE_RATE;
		dDevRes = MOUSE_RES;
		fMouse = TRUE;
	}
	}

	// get exp subject's samp rate & dev res
	BOOL fDoWarning = TRUE, fUpdateSubj = TRUE;
	double dSDevRes = 0.;
	short nSSampRate = 0;
	HRESULT hrs = pLV->m_pEML->Find( szExpID.AllocSysString(),
									 szGrpID.AllocSysString(),
									 szSubjID.AllocSysString() );
	if( SUCCEEDED( hrs ) )
	{
		pLV->m_pEML->get_DeviceRes( &dSDevRes );
		pLV->m_pEML->get_SamplingRate( &nSSampRate );

		if( ( dSDevRes == dDevRes ) && ( nSSampRate == nSampRate ) )
		{
			fDoWarning = FALSE;
// 07May08: GMB: We want the subject to store this info ALL the time.
//			fUpdateSubj = FALSE;
		}
	}

	// Process object and values
	IProcess* pProcess = Experiments::GetProcessObject( GetMsgPaneList(), pPS, _trInfo.szExpID, TRUE );
	if( pProcess )
	{
		short nRateVal = 0, nPressVal = 0, nDisCorrection = 0;
		double dResoVal = 0.0, dDisFactor = 0.0, dDisZInsPoint = 0.0,
			   dDisRelErr = 0.0, dDisAbsErr = 0.0, dDisError = 0.;
		BOOL fUseTilt = FALSE;

		pPS->get_SamplingRate( &nRateVal );
		pPS->get_DeviceResolution( &dResoVal );
		pPS->get_UseTilt( &fUseTilt );

		if( fMouse )
		{
			nSampRate = nRateVal;
			dDevRes = ::GetMouseResolution();
		}
		if( fUseTilt && !fSupportTilt )
		{
			CString szMsg = _T("WARNING: Experiment settings specify the recording of pen tilt which is not supported by the selected device. No tilt will be recorded.");
			OutputAMessage( szMsg, TRUE );
			fUseTilt = FALSE;
		}
		if( fDoWarning )
		{
			if( ( nRateVal != nSampRate ) || ( (float)dResoVal != (float)dDevRes ) )
			{
				CString szMsg;
				szMsg = _T("Experiment settings for the input device differ from the input device connected to this PC.\n\nYou can change the settings by clicking CANCEL and going to the Input Device settings of the experiment properties.\n\nClick YES to correct these settings for this subject.\nClick NO to continue without correcting these settings.\nClick CANCEL to end the experiment.");
				int nYNC = BCGPMessageBox( szMsg, MB_YESNOCANCEL );
				if( nYNC == IDCANCEL ) return FALSE;
				else if( nYNC == IDYES )
				{
					nRateVal = nSampRate;
					dResoVal = dDevRes;
				}
			}
		}
		else
		{
			nRateVal = nSampRate;
			dResoVal = dDevRes;
		}

		// update subject samp rate/dev res
		if( fUpdateSubj && SUCCEEDED( hrs ) )
		{
			pLV->m_pEML->put_DeviceRes( dResoVal );
			pLV->m_pEML->put_SamplingRate( nRateVal );
			hrs = pLV->m_pEML->Modify();
			if( FAILED( hrs ) )
				BCGPMessageBox( _T("Unable to update subject device resolution and sampling rate.") );
		}
		// additional (or overridden) proc settings
		pProcess->put_SamplingRate( nRateVal );
		pProcess->put_DeviceResolution( dResoVal );
		pPS->get_MinPenPressure( &nPressVal );
		pProcess->DiscardIfDiscontinuity( fConD );
		pProcess->IsFirstTrial();

		pPS->get_DisCorrection( &nDisCorrection );
		pPS->get_DisFactor( &dDisFactor );
		pPS->get_DisZInsertPoint( &dDisZInsPoint );
		pPS->get_DisRelError( &dDisRelErr );
		pPS->get_DisAbsError( &dDisAbsErr );
		pPS->get_DisError( &dDisError );
		pProcess->Discontinuity( nDisCorrection, dDisFactor, dDisZInsPoint, dDisRelErr, dDisAbsErr );
		pProcess->put_DisError( dDisError );

		if( pGV )
		{
			pGV->SetRecordingSettings( nRateVal, nPressVal, dResoVal );
			pPS->get_InstructionAlwaysVisible( &( pGV->m_fInstrVis ) );
		}
	}
	else return FALSE;

	crs.Restore();

	// if gripper experiment, we need to reset the device
	if( nType == EXP_TYPE_GRIPPER ) pGV->Reset();

	// hide start page
	if( pLV ) pLV->HideStartPage();

	crs.Restore();

	// Left view size (current)
	WINDOWPLACEMENT wp;
	memset( &wp, 0, sizeof( WINDOWPLACEMENT ) );
	GetWindowPlacement( &wp );
	_fWasMaximized = ( wp.showCmd == SW_MAXIMIZE );
	_OldTWidth = GetTreeSize();
	_OldTHeight = GetTreeHeight();
	_OldRHeight = GetRecSize();
	ResetSizes( FALSE );

	crs.Restore();

	// Questionnaire
	if( pLV && _fQuest && !_fQuestEnd ) pLV->Questionnaire( szExpID, szGrpID, szSubjID );

	crs.Restore();

	// Experiment instructions
	InstructionDlg id( _fFullScreen ? CWnd::FromHandle( ::GetDesktopWindow() ) : ::AfxGetApp()->m_pMainWnd );
	id.m_fReadOnly = TRUE;
	id.m_szExpID = szExpID;
	id.m_szFileName = szExpID;
	if( id.DoModal() != IDOK )
	{
		ResetSizes();
		return FALSE;
	}

	crs.Restore();

	// Update subject experiment execution count
	Subjects subj;
	subj.IncrementExperimentCount( szSubjID );

	// Progress Meter steps
	int nSteps = _trInfo.pConditions->GetCount() * ( _trInfo.fProcess ? 5 : 1 ) + 1;	// initial value
	// adjust for non-process NSConditions
	{
	POSITION pos = _trInfo.pConditions->GetHeadPosition();
	CString szCond;
	int nCnt = 0;
	BOOL fProcess = FALSE;
	NSConditions cond;
	while( pos )
	{
		szCond = _trInfo.pConditions->GetNext( pos );
		szCond = szCond.Left( 3 );	// condition ID
		if( SUCCEEDED( cond.Find( szCond ) ) )
		{
			cond.GetProcess( fProcess );
			if( !fProcess ) nCnt++;
		}
	}
	nSteps -= ( nCnt * 4 );
	}
	EnableProgress( nSteps );

#ifdef	_SECURITY_
#if 0
	// decrement PPU count
	NSSecurity* pSecurity = theApp.GetSecurity();
	if( pSecurity )
	{
		long nCnt = 0;
		long return_code = 0;
		char GlobalAuthorizationCodePassword[ 256 ];
		CString szPass = ::LoadAndDecode( IDS_CODEPASS );
		strcpy_s( GlobalAuthorizationCodePassword, 256, szPass );

		if( ::IsSPPU( nCnt ) )
		{
			nCnt--;
			pSecurity->SetAuthorizationDetails( 16 /*PPU Scrip*/, GlobalAuthorizationCodePassword, TRUE, nCnt, &return_code );
			if( nCnt <= 0 ) BCGPMessageBox( _T("You no longer have any remaining ScriptAlyzeR points.") );
			GetLeftView()->FillTree();
		}
		else if( ::IsGPPU( nCnt ) )
		{
			nCnt--;
			pSecurity->SetAuthorizationDetails( 17 /*PPU Grip*/, GlobalAuthorizationCodePassword, TRUE, nCnt, &return_code );
			if( nCnt <= 0 ) BCGPMessageBox( _T("You no longer have any remaining GripAlyzeR points.") );
			GetLeftView()->FillTree();
		}
		else if( ::IsOPPU( nCnt ) )
		{
			nCnt--;
			pSecurity->SetAuthorizationDetails( 18 /*PPU Mova*/, GlobalAuthorizationCodePassword, TRUE, nCnt, &return_code );
			if( nCnt <= 0 ) BCGPMessageBox( _T("You no longer have any remaining MovAlyzeR points.") );
			GetLeftView()->FillTree();
		}
	}
#endif
#endif

	return TRUE;
}

void CMainFrame::RunExperimentAuto( CString szExpID, CString szExp, CString szGrpID,
								    CString szGrp, CString szSubjID, CString szSubj )
{
#ifdef	_SECURITY_
	// Check if PPU and any available
	long nCnt = 0;
	if( !::IsSA() && !::IsGA() && !::IsOA() &&
		!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
	{
		BCGPMessageBox( _T("You no longer have any remaining points to use.") );
		OnHelpActivate();
		return;
	}
#endif

	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	CRecView* pRV = GetSettingsPane();
	if( m_fContinue || !pLV->m_pECL ) return;


	// tree item
	_htreeSubject = pLV->Find( szExpID, szGrpID, szSubjID );
	if( !_htreeSubject ) return;

	// user object
	UserObj* pObj = ::GetCurrentUserObj();
	if( !pObj ) return;
	pObj->Reset();
	pObj->m_szExpID = szExpID;
	pObj->m_szExp = szExp;
	pObj->m_szGrpID = szGrpID;
	pObj->m_szGrp = szGrp;
	pObj->m_szSubjID = szSubjID;
	pObj->m_szSubj = szSubj;

	// run experiment
	Experiments::RunExperiment( szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj, (LPVOID)_htreeSubject );
}

void CMainFrame::RunExperiment( IProcSetting* pPS, CString szExpID, CString szExp,
							    CString szSubjID, CString szSubj, CString szGrpID,
								CStringList* lstConditions, BOOL fSen2Word,
								HTREEITEM hSubject, BOOL fJustStim, BOOL fElement )
{
	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	CRecView* pRV = GetSettingsPane();
	_htreeSubject = hSubject;
	int nSteps = 0;
	UserObj* pObj = ::GetCurrentUserObj();
	if( !pObj ) return;

	if( !fJustStim )
	{
		if( !pPS || !lstConditions || m_fContinue || m_fRecording )
		{
			pObj->Reset();
			return;
		}

#ifdef	_SECURITY_
		// Check if PPU and any available
		long nCnt = 0;
		if( !::IsSA() && !::IsGA() && !::IsOA() &&
			!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
		{
			pObj->Reset();
			BCGPMessageBox( _T("You no longer have any remaining points to use.") );
			OnHelpActivate();
			return;
		}
#endif

		if( !RunExperimentShared( szExpID, szExp, szGrpID, szSubjID,
								  szSubj, pPS, lstConditions, FALSE ) )
		{
			pObj->Reset();
			return;
		}

		_StimOnly = FALSE;
		pLV->SetActionState( ACTION_RUNEXPERIMENT );
	}
	else
	{
		if( !fElement )
		{
			Stimuluss stim;
			stim.PutID( szExpID );
			if( !stim.GenerateFiles() )
			{
				pObj->Reset();
				BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				return;
			}
		}
		else
		{
			Elements elem;
			if( SUCCEEDED( elem.Find( szExpID ) ) && !elem.GenerateFiles() )
			{
				pObj->Reset();
				BCGPMessageBox( _T("Error generating element files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
				return;
			}
		}

		_StimOnly = TRUE;
		_stim = szExpID;

		InputDeviceSettingsDlg idsd( this );
		if( idsd.DoModal() == IDCANCEL )
		{
			pObj->Reset();
			return;
		}
		::SetSamplingRate( idsd.m_nSamplingRate );
		::SetMinPenPressure( idsd.m_nMinPenPressure );
		::SetDeviceResolution( idsd.m_dDevRes );
		pGV->SetRecordingSettings( ::GetSamplingRate(), ::GetMinPenPressure(), ::GetDeviceResolution() );
		pRV->UpdateInstruction( _T("Click STOP or hit ESC to end.") );

		pLV->SetActionState( ACTION_STIMULUS );
		WINDOWPLACEMENT wp;
		memset( &wp, 0, sizeof( WINDOWPLACEMENT ) );
		GetWindowPlacement( &wp );
		_fWasMaximized = ( wp.showCmd == SW_MAXIMIZE );
		_OldTWidth = GetTreeSize();
		_OldTHeight = GetTreeHeight();
		_OldRHeight = GetRecSize();
		ResetSizes( FALSE );
	}

	// Remainder of processing
	_trInfo.fSen2Word = fSen2Word;
	_fDidOnce = FALSE;
	m_fContinue = TRUE;
	_StopCondition = FALSE;
	fTrialOnly = FALSE;
	_NumProcessed = 0;

	RunExperiment2( FALSE, nSteps );
}

void CMainFrame::RunExperiment2( BOOL fSkipProcess, long lSteps )
{
	_ClockSecs = 0;

	static CString szRaw;
	CString szIdx, szCondID, szCondDesc, szInstr, szData, szTemp,
			szStimulus, szPStimulus, szWStimulus;
	int nReps = 0;
	long nCur = 0, nSteps = 0;
	double dVal = 0.0;
	POSITION pos = NULL;
	CFileFind ff;
	CRecView* pRV = GetSettingsPane();
	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	UserObj* pObj = ::GetCurrentUserObj();
	NSConditions cond;
	if( !pObj || !pLV || !pGV ) return;

	if( !_StimOnly )
	{
		// Do we need to display raw data?
		if( _RunTrial2 && _trInfo.fDisplay && _trInfo.fHWR )
		{
			szData.Format( IDS_AL_RAWDISP, szRaw );
			OutputAMessage( szData, TRUE );
			ChartHwr( szRaw, 200.0 );
		}

		// Were we recording? if so, process
		if( _RunTrial2 )
		{
			if( !fSkipProcess )
			{
				if( !RunProcess() )
				{
					_RunTrial2 = FALSE;
					pObj->Reset();
					return;
				}
			}
		}
		else if( _trInfo.nTrial > _trInfo.pConditions->GetCount() )
		{
			if( pRV ) pRV->UpdateInstruction( _T("All Trials Done") );

			m_fContinue = FALSE;

			// Cleanup
			ResetSizes();
			pObj->Reset();
			pLV->SetActionState( ACTION_NONE );
			DisableProgress();
			szData.LoadString( IDS_READY );
			ShowStatusText( szData );

			if( pRV ) pRV->Clear();

			::SystemCleanup();

			return;
		}
		else if( _RunTrial2 ) _RunTrial2 = FALSE;

		do
		{
			if( _trInfo.pConditions )
				pos = _trInfo.pConditions->FindIndex( _trInfo.nTrial - 1 );
			else
				pos = NULL;
			if( !pos ) break;
			szCondID = _trInfo.pConditions->GetAt( pos );
			if( SUCCEEDED( cond.Find( szCondID ) ) )
			{
				cond.GetDescription( szCondDesc );
				cond.GetInstruction( szInstr );
				cond.GetMagnetForce( _trInfo.dMagnetForce );
				cond.GetStimulusWarning( szWStimulus );
				cond.GetWarningDuration( _trInfo.dWDuration );
				cond.GetWarningLatency( _trInfo.dWLatency );
				cond.GetStimulusPrecue( szPStimulus );
				cond.GetPrecueDuration( _trInfo.dPDuration );
				cond.GetPrecueLatency( _trInfo.dPLatency );
				cond.GetStimulus( szStimulus );
				cond.GetRecordImmediately( _trInfo.fImmediately );
				cond.GetStopWrongTarget( _trInfo.fStopWrongTarget );
				cond.GetStartFirstTarget( _trInfo.fStartFirstTarget );
				cond.GetStopLastTarget( _trInfo.fStopLastTarget );
				cond.GetFeedbackInfo( 1, _fTrialFeedback1, _nFBColumn1, _nFBStroke1,
									  _dFBMin1, _dFBMax1, _fSwapColors1 );
				cond.GetFeedbackInfo( 2, _fTrialFeedback2, _nFBColumn2, _nFBStroke2,
									  _dFBMin2, _dFBMax2, _fSwapColors2 );
				_fTrialFeedback1 &= _trInfo.fProcess;
				_fTrialFeedback2 &= _trInfo.fProcess;
				if( !_fTrialFeedback1 ) _dFBMin1 = _dFBMax1 = 0.;
				if( !_fTrialFeedback2 ) _dFBMin2 = _dFBMax2 = 0.;
				// reset graph view for feedback
				pGV->m_fNoFeedback = !( _fTrialFeedback1 || _fTrialFeedback2 );
				pGV->DrawFeedback( FALSE );
			}

			if( _StopCondition && ( _trInfo.szCurCond == szCondID ) )
				_trInfo.nTrial++;
			else
				break;
		}
		while( TRUE );

		_StopCondition = FALSE;

		// Recording file
		if( ::IsGripperModeOn() )
			GetFRDMask( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szTemp );
		else
			GetHWRMask( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szTemp );
		nReps = 1;
		if( ff.FindFile( szTemp ) )
		{
			nReps++;
			while( ff.FindNextFile() )
				nReps++;
		}

		if( nReps > 99 )
		{
			DisableProgress();
			szData.LoadString( IDS_READY );
			ShowStatusText( szData );
			if( pRV ) pRV->Clear();
			pObj->Reset();
            return;
		}

		if( nReps < 10 ) szIdx.Format( _T("0%d"), nReps );
		else szIdx.Format( _T("%d"), nReps );
		if( ::IsGripperModeOn() )
			GetFRD( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szIdx, szRaw );
		else
			GetHWR( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szIdx, szRaw );

		// Check for directory structure
		if( !VerifyDirectory( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID ) )
		{
			BCGPMessageBox( _T("Unable to create directory for files.") );
			// Cleanup
			pObj->Reset();
			DisableProgress();
			szData.LoadString( IDS_READY );
			ShowStatusText( szData );
			if( pRV ) pRV->Clear();
			return;
		}		

		// Tracking current file (will be last next time this function is entered)
		_LastTrial = _trInfo.nTrial;
		_LastRep = _trInfo.nRep;

		// Get current state of progress meter
		if( lSteps == 0 ) GetProgress( nCur, nSteps );
		else nSteps = lSteps;

		// Check to see if we're redoing...if so, do not update progress bar except for text.
		if( !_RedoTrial ) nCur++;
		else _RedoTrial = FALSE;
		if( nSteps != 0.0 ) dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		else dVal = 0.0;
		szData.Format( IDS_RECORDING, dVal );
		SetProgress( nCur, szData );

		_trInfo.nTrial++;
		_trInfo.nRep = nReps;
		_trInfo.szCurCond = szCondID;

		// Begin recording & await stoppage
		_RunTrial2 = TRUE;
		szData.Format( IDS_AL_RECORDING, szRaw, szCondID );
		OutputAMessage( szData, TRUE );
		if( pRV ) pRV->UpdateInstruction( szInstr );
		szData.Format( IDS_AL_INSTR, szInstr );
		OutputAMessage( szData, TRUE );

		pGV->SetTimes( _trInfo.dStart, _trInfo.dSecs, _trInfo.lTicks );
		pGV->SetPrecueTimes( _trInfo.dPDuration, _trInfo.dPLatency );
		pGV->SetWarningTimes( _trInfo.dWDuration, _trInfo.dWLatency );
	}
	else
	{
		szStimulus = _stim;
		szRaw = _T("");
	}

	Record( szRaw, FALSE, szWStimulus, szPStimulus, szStimulus, FALSE, _StimOnly );
}

BOOL CMainFrame::RunProcess()
{
	CString szRaw, szTF, szSeg, szExt, szCon, szErr, szIdx, szCondID, szCondDesc,
			szTemp, szData, szLex, szBackup, szStimulus, szPStimulus, szWStimulus;
	BOOL fProcess = FALSE, fFlagBadTarget = FALSE;
	short nMin = 0, nMax = 0, nSkip = 0, nPos = 0, nGripTask = 0;
	long nCur = 0, nSteps = 0;
	double dLength = 0.0, dRange = 0.0, dVal = 0.0, dDir = 0.0, dRangeDir = 0.0, dMagnetForce = 0.;
	POSITION pos = NULL;
	CFileFind ff;
	BOOL fPassed = FALSE;
	CRecView* pRV = GetSettingsPane();
	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	NSConditions cond;
	UserObj* pObj = ::GetCurrentUserObj();
	if( !pObj ) return FALSE;

	// process object
	IProcess* pProcess = Experiments::GetProcessObject( GetMsgPaneList(), _trInfo.pPS, _trInfo.szExpID, TRUE );

	// get subject rate & res to override exp if set
	HRESULT hrs = pLV->m_pEML->Find( _trInfo.szExpID.AllocSysString(),
									 _trInfo.szGrpID.AllocSysString(),
									 _trInfo.szSubjID.AllocSysString() );
	double dSDevRes = 0.;
	short nSSampRate = 0;
	if( SUCCEEDED( hrs ) )
	{
		pLV->m_pEML->get_DeviceRes( &dSDevRes );
		pLV->m_pEML->get_SamplingRate( &nSSampRate );

		if( dSDevRes != 0. ) pProcess->put_DeviceResolution( dSDevRes );
		if( nSSampRate != 0 ) pProcess->put_SamplingRate( nSSampRate );
	}

	// Get current progress meter status
	GetProgress( nCur, nSteps );

	// Clear instruction
	if( pRV ) pRV->UpdateInstruction( _T("Trial Done") );

	pos = _trInfo.pConditions->FindIndex( _trInfo.nTrial - 2 );
	if( !pos ) return FALSE;
	szCondID = _trInfo.pConditions->GetAt( pos );
	if( SUCCEEDED( cond.Find( szCondID ) ) )
	{
		cond.GetLex( szLex );
		cond.GetStrokeMin( nMin );
		cond.GetStrokeMax( nMax );
		cond.GetStrokeLength( dLength );
		cond.GetRangeLength( dRange );
		cond.GetStrokeDirection( dDir );
		cond.GetRangeDirection( dRangeDir );
		cond.GetStrokeSkip( nSkip );
		cond.GetProcess( fProcess );
		cond.GetFlagBadTarget( fFlagBadTarget );
		cond.GetGripperTask( nGripTask );
		cond.GetMagnetForce( dMagnetForce );
	}

	int nRep = _trInfo.nRep;
	if( nRep < 10 ) szIdx.Format( _T("0%d"), nRep );
	else szIdx.Format( _T("%d"), nRep );

	CString szFileExt;
	if( ::IsGripperModeOn() )
	{
		szFileExt = _T("FRD");
		GetFRD( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szIdx, szRaw );
	}
	else
	{
		szFileExt = _T("HWR");
		GetHWR( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szIdx, szRaw );
	}
	GetTF( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szIdx, szTF );
	GetSEG( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szIdx, szSeg );
	GetEXT( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szExt );

	// accept/decline option?
	if( ( _trInfo.nTrialMode >= eTM_NoneAD ) && ( _trInfo.nTrialMode < eTM_NoneADErr ) )
	{
		if( BCGPMessageBox( _T("Click YES to accept the trial or NO to redo.\nIf you REDO, the original file will be deleted."), MB_YESNO ) == IDNO )
		{
			_RedoTrial = TRUE;
			_trInfo.nTrial--;
			if( ff.FindFile( szRaw ) ) REMOVE_FILE( szRaw );
			CString szMsg;
			szMsg.Format( _T("EXPERIMENT: Trial #%d is being redone."), _trInfo.nTrial );
			OutputAMessage( szMsg, TRUE );
			return TRUE;
		}
	}

	// Check for existence of raw data file
	if( !ff.FindFile( szRaw ) )
	{
		szData.Format( IDS_AL_FILEMISSING, szRaw );
		OutputAMessage( szData, TRUE );
		BCGPMessageBox( szData );
		m_fContinue = FALSE;
	}

	// Backup
	if( pLV )
	{
		// raw data files
		if( ::IsGripperModeOn() )
			szBackup.Format( _T("%s%s%s%s%s.FRD"), _trInfo.szExpID, _trInfo.szGrpID,
							 _trInfo.szSubjID, szCondID, szIdx );
		else
			szBackup.Format( _T("%s%s%s%s%s.HWR"), _trInfo.szExpID, _trInfo.szGrpID,
							 _trInfo.szSubjID, szCondID, szIdx );
		pLV->BackupRaw( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szBackup );

		// target info file
		nPos = szBackup.ReverseFind( '.' );
		if( nPos > 0 )
		{
			szBackup = szBackup.Left( nPos );
			szBackup += _T(".TGT");

			if( ff.FindFile( szBackup ) )
				pLV->BackupRaw( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szBackup );
		}
	}

	// get appropriate interface
	IProcSetRunExp* pPSRE = NULL;
	IProcSetFlags* pPSF = NULL;
	HRESULT hr = _trInfo.pPS ? _trInfo.pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE ) : NULL;
	hr = _trInfo.pPS ? _trInfo.pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF ) : NULL;

	// Get external processing scripts if movalyzer
	ExternalAppType eHWRScript = eEAT_None, eTFScript = eEAT_None,
		eSegScript = eEAT_None, eExtScript = eEAT_None;
	CString szHWRScript, szTFScript, szSegScript, szExtScript, szScriptRoot;
	::GetDataPathRoot( szScriptRoot );
	szScriptRoot += _T("\\scripts\\");
	BOOL fRes = FALSE;
	if( ::IsOA() && pPSRE )
	{
		BSTR bstr = NULL;
		pPSRE->get_ExtAppTypeRaw( &eHWRScript );
		if( eHWRScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptRaw( &bstr );
			szHWRScript = szScriptRoot;
			szHWRScript += bstr;
		}
		pPSRE->get_ExtAppTypeTF( &eTFScript );
		if( eTFScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptTF( &bstr );
			szTFScript = szScriptRoot;
			szTFScript += bstr;
		}
		pPSRE->get_ExtAppTypeSeg( &eSegScript );
		if( eSegScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptSeg( &bstr );
			szSegScript = szScriptRoot;
			szSegScript += bstr;
		}
		pPSRE->get_ExtAppTypeExt( &eExtScript );
		if( eExtScript != eEAT_None )
		{
			pPSRE->get_ExtAppScriptExt( &bstr );
			szExtScript = szScriptRoot;
			szExtScript += bstr;
		}
	}
	if( pPSRE ) pPSRE->Release();
	// get norm DB settings if movalyzer
	BOOL fScore = FALSE, fScoreGlobal = FALSE, fNoUpdate = FALSE;
	double dNormDBTH = 0.;
	if( ::IsOA() && pPSF ) {
		pPSF->get_NormDBCalcScore( &fScore );
		pPSF->get_NormDBCalcScoreGlobal( &fScoreGlobal );
		pPSF->get_NormDBNoUpdate( &fNoUpdate );
		pPSF->get_NormDBThreshold( &dNormDBTH );
	}
	if( pPSF ) pPSF->Release();

	// Process TF
	UINT nSwitch = eTF_None;
	if( fTFA ) nSwitch |= eTF_A;
	if( fTFJ ) nSwitch |= eTF_J;
	if( fTFR ) nSwitch |= eTF_R;
	if( fTFU ) nSwitch |= eTF_U;
	if( fTFO ) nSwitch |= eTF_O;
	if( m_fContinue && _trInfo.fProcess && fProcess )
	{
		// Process Post-Record, Pre-TF external app script, if any
		switch( eHWRScript )
		{
			case eEAT_Matlab:
			{
				fRes = Experiments::ExecuteMatlabScript( szHWRScript, _trInfo.szExpID, _trInfo.szGrpID,
														 _trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_Batch:
			{
				fRes = Experiments::ExecuteBatchScript( szHWRScript, _trInfo.szExpID, _trInfo.szGrpID,
														_trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_None: fRes = TRUE; break;
		}
		if( !fRes )
		{
			if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
				m_fContinue = FALSE;
		}

		szData.Format( IDS_AL_PROCHWR, szTF );
		OutputAMessage( szData, TRUE );
		Experiments::ProcessTimfun( szRaw, szTF, dFF, nDec, nSwitch, dBeta, GetMsgPaneList() );

		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szData.Format( _T("Progress %.1f %% - Time Functions complete"), dVal );
		SetProgress( nCur, szData );

		// Check for existence of TF file
		if( !ff.FindFile( szTF ) )
		{
			szData.Format( IDS_AL_FILEMISSING, szTF );
			OutputAMessage( szData, TRUE );
			BCGPMessageBox( szData );
			m_fContinue = FALSE;
		}
	}

	// Segment
	UINT nGripSwitch = nGripTask;
	nSwitch = eSeg_None;
	if( fSegF ) nSwitch |= eSeg_F;
	if( fSegL ) nSwitch |= eSeg_L;
	if( fSegO ) nSwitch |= eSeg_O;
	if( fSegS && ::IsOA() ) nSwitch |= eSeg_S;
	if( fSegM ) nSwitch |= eSeg_M;
	if( fSegA ) nSwitch |= eSeg_A;
	if( fSegV ) nSwitch |= eSeg_V;
	if( fSegMM ) nSwitch |= eSeg_MM;
	if( fSegJ ) nSwitch |= eSeg_J;
	if( ::GetVerbosity() ) nSwitch |= eSeg_D;
	if( m_fContinue && _trInfo.fProcess && fProcess)
	{
		// Process Post-TF, Pre-Seg external app script, if any
		switch( eTFScript )
		{
			case eEAT_Matlab:
			{
				fRes = Experiments::ExecuteMatlabScript( szTFScript, _trInfo.szExpID, _trInfo.szGrpID,
														 _trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_Batch:
			{
				fRes = Experiments::ExecuteBatchScript( szTFScript, _trInfo.szExpID, _trInfo.szGrpID,
														_trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_None: fRes = TRUE; break;
		}
		if( !fRes )
		{
			if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
				m_fContinue = FALSE;
		}

		// Do we need to display TF data?
		if( m_fContinue && _trInfo.fProcess && _trInfo.fDisplay && _trInfo.fTF && !_trInfo.fSeg && fProcess )
		{
			szData.Format( IDS_AL_TFDISP, szTF );
			OutputAMessage( szData, TRUE );
			ChartTf( szTF, 200.0 );
		}

		szData.Format( IDS_AL_PROCTF, szSeg );
		OutputAMessage( szData, TRUE );
		Experiments::ProcessSegmen( szTF, szSeg, nSwitch, GetMsgPaneList(), nGripSwitch );

		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szData.Format( _T("Progress %.1f %% - Segmentation complete"), dVal );
		SetProgress( nCur, szData );

		// Check for existence of SEG file
		if( !ff.FindFile( szSeg ) )
		{
			szData.Format( IDS_AL_FILEMISSING, szSeg );
			OutputAMessage( szData, TRUE );
			BCGPMessageBox( szData );
			m_fContinue = FALSE;
		}
	}

	// Extract
	nSwitch = eExt_None;
	if( fExtS ) nSwitch |= eExt_S;
	if( fExt3 ) nSwitch |= eExt_3;
	if( fExtO ) nSwitch |= eExt_O;
	if( fExt2 ) nSwitch |= eExt_2;
	if( m_fContinue && _trInfo.fProcess && fProcess )
	{
		// Process Post-Seg, Pre-Ext external app script, if any
		switch( eSegScript )
		{
			case eEAT_Matlab:
			{
				fRes = Experiments::ExecuteMatlabScript( szSegScript, _trInfo.szExpID, _trInfo.szGrpID,
														 _trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_Batch:
			{
				fRes = Experiments::ExecuteBatchScript( szSegScript, _trInfo.szExpID, _trInfo.szGrpID,
														_trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_None: fRes = TRUE; break;
		}
		if( !fRes )
		{
			if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
				m_fContinue = FALSE;
		}

		// Do we need to display TF data with Seg?
		if( m_fContinue && _trInfo.fProcess && _trInfo.fDisplay && _trInfo.fTF && _trInfo.fSeg && fProcess )
		{
			szData.Format( IDS_AL_TFSEGDISP, szTF, szSeg );
			OutputAMessage( szData, TRUE );
			ChartTf( szTF, 200.0, szSeg );
		}

		szData.Format( IDS_AL_PROCEXT, szExt );
		OutputAMessage( szData, TRUE );
		if( _fTrialFeedback1 ) pProcess->SetFeedbackParams( _nFBColumn1, _nFBStroke1, FALSE );
		if( _fTrialFeedback2 ) pProcess->SetFeedbackParams( _nFBColumn2, _nFBStroke2, TRUE );
		Experiments::ProcessExtract( szTF, szSeg, szExt, nSwitch, GetMsgPaneList(), nGripSwitch );

		// Process Post-Ext external app script, if any
		switch( eExtScript )
		{
			case eEAT_Matlab:
			{
				fRes = Experiments::ExecuteMatlabScript( szExtScript, _trInfo.szExpID, _trInfo.szGrpID,
														 _trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_Batch:
			{
				fRes = Experiments::ExecuteBatchScript( szExtScript, _trInfo.szExpID, _trInfo.szGrpID,
														_trInfo.szSubjID, szCondID, szIdx, szFileExt );
				break;
			}
			case eEAT_None: fRes = TRUE; break;
		}
		if( !fRes )
		{
			if( BCGPMessageBox( _T("There was an error running an external application script.\nDo you wish to continue processing?"), MB_YESNO | MB_ICONQUESTION ) == IDNO )
				m_fContinue = FALSE;
		}

		// feedback
		if( _fTrialFeedback1 )
		{
			pProcess->GetFeedback( FALSE, &_dFBVal1 );
			szData.Format( _T("FEEDBACK-ONE: Column = %d, Stroke = %d, Min = %g, Max = %g, Value = %g"),
						   _nFBColumn1, _nFBStroke1, _dFBMin1, _dFBMax1, _dFBVal1 );
			OutputAMessage( szData, TRUE );
			pGV->DrawFeedback( FALSE, _dFBMin1, _dFBMax1, _dFBVal1, FALSE );
		}
		if( _fTrialFeedback2 )
		{
			pProcess->GetFeedback( TRUE, &_dFBVal2 );
			szData.Format( _T("FEEDBACK-two: Column = %d, Stroke = %d, Min = %g, Max = %g, Value = %g"),
						   _nFBColumn2, _nFBStroke2, _dFBMin2, _dFBMax2, _dFBVal2 );
			OutputAMessage( szData, TRUE );
			pGV->DrawFeedback( FALSE, _dFBMin2, _dFBMax2, _dFBVal2, TRUE );
		}

		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szData.Format( _T("Progress %.1f %% - Extractions complete"), dVal );
		SetProgress( nCur, szData );

		// Check for existence of EXT file
		if( !ff.FindFile( szExt ) )
		{
			szData.Format( IDS_AL_FILEMISSING, szSeg );
			OutputAMessage( szData, TRUE );
			BCGPMessageBox( szData );
			m_fContinue = FALSE;
		}
	}

	// Consis (by trial)
	GetCON( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szCon );
	GetERR( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szErr );

	UINT conSwitch = eCon_None;
	if( fConA ) conSwitch |= eCon_A;
	if( fConC ) conSwitch |= eCon_C;
	if( fConM ) conSwitch |= eCon_M;
	if( fConN ) conSwitch |= eCon_N;
	if( fConS ) conSwitch |= eCon_S;
	if( fConU ) conSwitch |= eCon_U;
	if( fConO ) conSwitch |= eCon_O;

	if( _trInfo.fProcess && fProcess )
	{
		szData.Format( IDS_AL_PROCCON, szExt, szCon, szCondID );
		OutputAMessage( szData, TRUE );
		Experiments::ProcessConsis( szExt, szCon, szLex, nMin, nMax, dLength,
									dRange, dDir, dRangeDir, nSkip, fFlagBadTarget,
									szErr, szCondID, conSwitch, NULL, TRUE, fPassed,
									GetMsgPaneList(), fConD, dMagnetForce, nGripSwitch );

		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szData.Format( _T("Progress %.1f %% - Consis complete"), dVal );
		SetProgress( nCur, szData );

		if( fPassed ) OutputAMessage( IDS_AL_TRIALS, TRUE );
		else OutputAMessage( IDS_AL_TRIALF, TRUE );
	}
	else fPassed = FALSE;

	// accept/decline option?
	if( _trInfo.nTrialMode > eTM_All )
	{
		// redo if error regardless
		if( ( _trInfo.nTrialMode >= eTM_NoneErr ) && _trInfo.fProcess && fProcess )
		{
			if( !fPassed )
			{
				OutputAMessage( _T("Trial did not pass consistency testing. Redoing."), TRUE );
				_RedoTrial = TRUE;
				_trInfo.nTrial--;
				if( ff.FindFile( szRaw ) ) REMOVE_FILE( szRaw );
				return TRUE;
			}
		}
		// ask if accept/redo if error
		else if( ( _trInfo.nTrialMode >= eTM_NoneADErr ) && _trInfo.fProcess && fProcess && !fPassed )
		{
			AcceptRedoDlg arDlg( this );
			arDlg.m_szTF = szTF;
			arDlg.m_szSeg = szSeg;
			arDlg.m_dFF = dFF;
			int arRes = arDlg.DoModal();
			if( arRes == IDOK )
			{
				_RedoTrial = TRUE;
				_trInfo.nTrial--;
				if( ff.FindFile( szRaw ) ) REMOVE_FILE( szRaw );
				return TRUE;
			}
		}
	}

	if( _htreeSubject && pLV )
	{
		_trInfo.hLast = pLV->ShowTrial( _trInfo.szExpID, _trInfo.szSubjID, _trInfo.szGrpID,
										szCondID, szIdx, _htreeSubject, _T(""), fPassed,
										FALSE, !( _trInfo.fProcess && fProcess ) );
		if( !_fDidOnce )
		{
			pLV->EnsureTrialVisible( _trInfo.hLast, TRUE );
			_fDidOnce = TRUE;
		}
	}

	if( !m_fContinue )
	{
		// Cleanup
		_fTrialFeedback1 = FALSE;
		_fTrialFeedback2 = FALSE;
		pGV->m_fNoFeedback = TRUE;
		pGV->DrawFeedback( TRUE );
		pObj->Reset();
		_trInfo.pConditions = NULL;
		DisableProgress();
		szData.LoadString( IDS_READY );
		ShowStatusText( szData );
		if( pRV ) pRV->Clear();
		return FALSE;
	}

	// If all have been done, con & display
	if( _trInfo.nTrial > _trInfo.pConditions->GetCount() )
	{
		BOOL fShowEnd = TRUE;
		if( pRV ) pRV->UpdateInstruction( _T("All Trials Done") );

		m_fContinue = FALSE;

		if( _trInfo.fProcess )
		{
			// CONSIS
			// For each condition (remove the duplicates)
			CStringList lstCond;
			pos = _trInfo.pConditions->GetHeadPosition();
			while( pos )
			{
				szCondID = _trInfo.pConditions->GetNext( pos );

				if( lstCond.Find( szCondID ) == NULL )
					lstCond.AddTail( szCondID );
			}
			// for each UNIQUE condition
			pos = lstCond.GetHeadPosition();
			while( pos )
			{
				szCondID = lstCond.GetNext( pos );
				if( SUCCEEDED( cond.Find( szCondID ) ) )
				{
					cond.GetLex( szLex );
					cond.GetStrokeMin( nMin );
					cond.GetStrokeMax( nMax );
					cond.GetStrokeLength( dLength );
					cond.GetRangeLength( dRange );
					cond.GetStrokeDirection( dDir );
					cond.GetRangeDirection( dRangeDir );
					cond.GetStrokeSkip( nSkip );
					cond.GetProcess( fProcess );
					cond.GetFlagBadTarget( fFlagBadTarget );
					cond.GetMagnetForce( dMagnetForce );
				}

				GetEXT( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szExt );
				GetCON( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szCon );
				GetERR( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szErr );

				if( ff.FindFile( szCon ) ) REMOVE_FILE( szCon );
				if( ff.FindFile( szErr ) ) REMOVE_FILE( szErr );

				szData.Format( IDS_AL_PROCCON, szExt, szCon, szCondID );
				OutputAMessage( szData, TRUE );
				if( fProcess )
				{
					Experiments::ProcessConsis( szExt, szCon, szLex, nMin, nMax,
												dLength, dRange, dDir, dRangeDir,
												nSkip, fFlagBadTarget, szErr, szCondID,
												conSwitch, NULL, FALSE, fPassed,
												GetMsgPaneList(), fConD, dMagnetForce, nGripSwitch );
					_NumProcessed++;
				}
			}
			szData.Format( IDS_AL_CONDONEALL, 100 );
			SetProgress( nSteps, szData );

			// Display
			if( _trInfo.fExt )
			{
				pos = lstCond.GetHeadPosition();
				while( pos )
				{
					CString szCmd;
					szCmd.LoadString( IDS_EDITOR );
					szCondID = lstCond.GetNext( pos );
					GetEXT( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szExt );
					szCmd += szExt;
					WinExec( szCmd, SW_SHOW );
				}
			}
			if( _trInfo.fConsis )
			{
				pos = lstCond.GetHeadPosition();
				while( pos )
				{
					CString szCmd;
					szCmd.LoadString( IDS_EDITOR );
					szCondID = lstCond.GetNext( pos );
					GetERR( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, szCondID, szErr );
					szCmd += szErr;
					WinExec( szCmd, SW_SHOW );
				}
			}
		}
		else
		{
			nCur++;
			SetProgress( nCur, _T("") );
		}

		// if we had any NSConditions that need word extraction inform user (reproc subj)
		if( _trInfo.fSen2Word )
		{
			CString szMsg = _T("Word extraction is required to complete this experiment. Continue?");
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				short nRateVal = 0;
				double dResoVal = 0.;
				pProcess->get_SamplingRate( &nRateVal );
				pProcess->get_DeviceResolution( &dResoVal );
				ReprocessSubject( _trInfo.szExpID, _trInfo.szSubjID, _trInfo.szGrpID,
								  dResoVal, nRateVal, _htreeSubject, GetLeftView(),
								  GetMsgPane(), 1, 1 );
			}
			else
			{
				_trInfo.fSumm = FALSE;
				_trInfo.fAnalysis = FALSE;
			}
		}

		// Questionnaire
		if( pLV && _fQuest && _fQuestEnd )
			pLV->Questionnaire( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID );

		// Summarize
		CString szINC;
		::GetExpINC( _trInfo.szExpID, szINC );
		if( _trInfo.fSumm && ( _NumProcessed > 0 ) )
		{
			CString szSummGrp = ( _trInfo.fGrpOnlySumm && !_trInfo.fSelectSumm ) ? _trInfo.szGrpID : _T("");
			if( !Experiments::SummarizeExperiment( _trInfo.szExpID, _trInfo.fSelectSumm,
												   _trInfo.fOnlySumm, _trInfo.fExt2, 
												   _trInfo.fConsis2, _trInfo.szSubjID,
												   GetMsgPane(), FALSE, FALSE, szSummGrp ) )
			{
				// Cleanup
				_fTrialFeedback1 = FALSE;
				_fTrialFeedback2 = FALSE;
				ResetSizes();
				pLV->SetActionState( ACTION_NONE );
				pObj->Reset();
				_trInfo.pConditions = NULL;
				DisableProgress();
				szData.LoadString( IDS_READY );
				ShowStatusText( szData );
				if( pRV ) pRV->Clear();
				return FALSE;
			}
			fShowEnd = FALSE;

			// norm database update
			if( _trInfo.fNormDBSumm && ::IsOA() ) {
				// copy INC to temp INC
				nPos = szINC.ReverseFind( '.' );
				CString szINCTemp = szINC.Left( nPos );
				szINCTemp += _T("t.INC");
				::CopyFile( szINC, szINCTemp, FALSE );
				if( !AGraphDlg::UpdateNormDB( fScore, fScoreGlobal, fNoUpdate, dNormDBTH, _trInfo.szExpID, szINCTemp ) ) {
					OutputAMessage( _T("ERROR: Unable to update historic averages database."), TRUE );
				}
				if( ff.FindFile( szINCTemp ) ) REMOVE_FILE( szINCTemp );
			}
		}

		// Cleanup
		_fTrialFeedback1 = FALSE;
		_fTrialFeedback2 = FALSE;
		ResetSizes();
		pLV->SetActionState( ACTION_NONE );
		pObj->Reset();
		_trInfo.pConditions = NULL;
		DisableProgress();
		szData.LoadString( IDS_READY );
		ShowStatusText( szData );
		if( pRV ) pRV->Clear();

		// Analysis (all data)
		if( _trInfo.fAnalysis )
		{
			fShowEnd = FALSE;
			if( ff.FindFile( szINC ) ) pLV->ShowStatGraph( _trInfo.szExpID );
		}

		// Show message indicating end of experiment if summarization/analysis are not being displayed
		if( pLV ) {
			CTreeCtrl& tree = pLV->GetTreeCtrl();
			HTREEITEM hItem = tree.GetSelectedItem();
			if( hItem ) tree.EnsureVisible( hItem );
		}
		if( fShowEnd ) BCGPMessageBox( _T("The test has been completed."), MB_OK | MB_ICONINFORMATION );

		// if trial-to-trial timer is not set (=0), set no visual feedback and return FALSE
		// so no continued processing will occur
		if( _ClockSecsMax <= 0 )
		{
			pGV->m_fNoFeedback = TRUE;
			pGV->DrawFeedback( TRUE );
			pGV->Clear();
			return FALSE;
		}
		// otherwise, return true so the trial-to-trial timer will be set for the last trial
		else return TRUE;
	}

	return TRUE;
}

void CMainFrame::ReprocessSubject( CString szExpID, CString szSubjID, CString szGrpID, double dDevRes,
								   int nSampRate, HTREEITEM hItem, CLeftView* pLeft, CListView* pLV,
								   int nSubjCount, int nCurSubj, BOOL fInformIfEmpty, BOOL fSubCond,
								   BOOL fSeparateThread, BOOL fReprocAll )
{
#ifdef	_SECURITY_
	// Check if PPU and any available
	long nCnt = 0;
	if( !::IsSA() && !::IsGA() && !::IsOA() &&
		!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
	{
		BCGPMessageBox( _T("You no longer have any remaining points to use.") );
		OnHelpActivate();
		return;
	}
#endif

	Experiments::ReprocessSubject( szExpID, szSubjID, szGrpID, dDevRes, nSampRate, hItem, pLeft,
								   pLV, nSubjCount, nCurSubj, fInformIfEmpty, fSubCond,
								   fSeparateThread, fReprocAll );

#ifdef	_SECURITY_
#if 0
	// decrement PPU count
	ISSCProtectorPtr spSSCProt = GetClientProtector();
	if( spSSCProt )
	{
		long nCnt = 0;
		long return_code = 0;
		char GlobalAuthorizationCodePassword[ 256 ];
		CString szPass = ::LoadAndDecode( IDS_CODEPASS );
		strcpy_s( GlobalAuthorizationCodePassword, 256, szPass );

		if( ::IsSPPU( nCnt ) )
		{
			nCnt--;
			pSecurity->SetAuthorizationDetails( 16 /*PPU Scrip*/, GlobalAuthorizationCodePassword, TRUE, nCnt, &return_code );
			::SetIsSPPU( ( nCnt > 0 ), nCnt );
			if( nCnt <= 0 ) BCGPMessageBox( _T("You no longer have any remaining ScriptAlyzeR points.") );
			GetLeftView()->DoTitle();
		}
		else if( ::IsGPPU( nCnt ) )
		{
			nCnt--;
			pSecurity->SetAuthorizationDetails( 17 /*PPU Grip*/, GlobalAuthorizationCodePassword, TRUE, nCnt, &return_code );
			::SetIsGPPU( ( nCnt > 0 ), nCnt );
			if( nCnt <= 0 ) BCGPMessageBox( _T("You no longer have any remaining GripAlyzeR points.") );
			GetLeftView()->DoTitle();
		}
		else if( ::IsOPPU( nCnt ) )
		{
			nCnt--;
			pSecurity->SetAuthorizationDetails( 18 /*PPU Mova*/, GlobalAuthorizationCodePassword, TRUE, nCnt, &return_code );
			::SetIsOPPU( ( nCnt > 0 ), nCnt );
			if( nCnt <= 0 ) BCGPMessageBox( _T("You no longer have any remaining MovAlyzeR points.") );
			GetLeftView()->DoTitle();
		}
	}
#endif
#endif
}

void CMainFrame::SummarizeExperiment( CString szExp, CListView* pMsgWnd, BOOL fDlg, BOOL fOnly,
									  BOOL fInc, BOOL fErr, CString szSubjID, BOOL fDoConsis )
{
	// run summarize (this is not called while running an experiment...
	// ...the Experiments::SummarizeExperiment function is
	BOOL fRes = Experiments::SummarizeExperiment( szExp, fDlg, fOnly, fInc, fErr, szSubjID,
												  pMsgWnd ? pMsgWnd : GetMsgPane(), fDoConsis );

	// if summarize completed successfully...
	if( fRes )
	{
		// refresh tree
#ifndef _DEBUG
		CLeftView* pLV = GetLeftView();
		if( pLV ) pLV->Refresh();
#endif

		// if INC exists, ask if they would like to do an analysis
		CFileFind ff;
		CString szInc;
		GetExpINC( szExp, szInc );
		if( ff.FindFile( szInc ) )
		{
			if( BCGPMessageBox( _T("Would you like to run the analysis now?"), MB_YESNO ) == IDYES )
			{
				CLeftView* pLV = GetLeftView();
				if( pLV ) pLV->ShowStatGraph( szExp );
			}
		}
	}
}

void CMainFrame::OnDigReset() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	if( BCGPMessageBox( _T("Are you sure you want to reset your current input device?"), MB_YESNO ) == IDNO )
		return;

	CGraphView* pGV = GetRecordingPane();
	if( pGV ) pGV->Reset();
}

void CMainFrame::OnDigTest()
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	OnDigTest2();
}

void CMainFrame::OnTabletTimeOut()
{
	Record();
}

void CMainFrame::OnDigTest2() 
{
	static BOOL DidTestDlg = FALSE;
	static TestDeviceDlg d( this );
	InputDeviceSettingsDlg idsd( this );

	if( !DidTestDlg )
	{
		if( d.DoModal() == IDCANCEL ) return;
		else DidTestDlg = TRUE;
	}

	if( d.m_fRaw || ( d.m_fLinear && d.m_fDraw ) )
	{
		CGraphView* pWnd = GetRecordingPane();
		if( !pWnd ) return;
		CLeftView* pLV = GetLeftView();
		if( pLV ) pLV->OnDigTest();

		CString szData;
		if( !m_fRecording )
		{
			if( pWnd->m_fTest )
			{
				szData.LoadString( IDS_AL_DIGTESTSTOP );
				OutputAMessage( szData, TRUE );

				pWnd->Stop();
				m_fContinue = FALSE;
				DidTestDlg = FALSE;
				ResetSizes();

				::PlayASound( _T(""), SOUND_RECORDING_START );

				CString szFile, szFile2;
				::GetDataPathRoot( szFile );
				if( szFile == _T("") ) szFile = _T("test.");
				else szFile += _T("\\test.");
				if( ::IsGripperModeOn() ) szFile += _T("frd");
				else szFile += _T("hwr");
				szFile2 = szFile.Left( szFile.GetLength() - 3 );
				szFile2 += _T("unr");
				// Regular recording or linear test?
				if( d.m_fLinear )
				{
					BOOL fVelocity = FALSE;
					BOOL fDiff = TRUE;
					double dRotBeta = 0.;

					// Unrotate
					Experiments::ProcessUnrot( szFile, szFile2, fVelocity, fDiff, dRotBeta, GetMsgPaneList() );
				}

				// Chart the test
				CFileFind ff;
				if( ff.FindFile( szFile ) )
				{
					if( ::IsGripperModeOn() )
					{
						GripperSettings gs;
						::GetGripperSettings( &gs );
						ChartHwr( szFile, gs.lScanRate, FALSE, ::GetSoftColor(), TRUE, -100 );
					}
					else ChartHwr( szFile, ::GetSamplingRate(), FALSE, ::GetSoftColor(), TRUE, ::GetMinPenPressure() );

				}
				if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
			}
			else
			{
				szData.LoadString( IDS_AL_DIGTEST );
				OutputAMessage( szData, TRUE );

				m_fContinue = TRUE;
				_fMaximize = TRUE;

				// Left view size (current)
				WINDOWPLACEMENT wp;
				memset( &wp, 0, sizeof( WINDOWPLACEMENT ) );
				GetWindowPlacement( &wp );
				_fWasMaximized = ( wp.showCmd == SW_MAXIMIZE );
				_OldTWidth = GetTreeSize();
				_OldTHeight = GetTreeHeight();
				_OldRHeight = GetRecSize();
				ResetSizes( FALSE );

				pWnd->Test();
			}
		}
		else Record();
	}
	else
	{
		DidTestDlg = FALSE;

		CString szFile;
		::GetDataPathRoot( szFile );
		if( szFile == _T("") ) szFile = _T("test.");
		else szFile += _T("\\test.");
		if( ::IsGripperModeOn() ) szFile += _T("frd");
		else szFile += _T("hwr");
		CString szFile2 = szFile.Left( szFile.GetLength() - 3 );
		szFile2 += _T("unr");
		BOOL fVelocity = FALSE;
		BOOL fDiff = TRUE;
		double dRotBeta = 0.;

		// Unrotate
		Experiments::ProcessUnrot( szFile, szFile2, fVelocity, fDiff, dRotBeta, GetMsgPaneList() );

		// Chart the test
		CFileFind ff;
		if( ff.FindFile( szFile ) )
		{
			if( ::IsGripperModeOn() )
			{
				GripperSettings gs;
				::GetGripperSettings( &gs );
				ChartHwr( szFile, gs.lScanRate, FALSE, ::GetSoftColor(),
						  TRUE, -100 );
			}
			else
				ChartHwr( szFile, 100.0, FALSE );
		}
		if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
	}
}

void CMainFrame::Record( CString szFile, BOOL fTrial, CString szWStim, CString szPStim,
						 CString szStim, BOOL fJustQuit, BOOL fShowStim )
{
	CGraphView* pWnd = GetRecordingPane();
	if( !pWnd ) return;

	if( !fTrialOnly && fTrial ) fTrialOnly = TRUE;

	CString szData;
	if( !m_fRecording )
	{
		pWnd->DrawFeedback( TRUE );

		m_fRecording = TRUE;
		if( !fShowStim )
		{
			pWnd->SetTimes( _trInfo.dStart, _trInfo.dSecs, _trInfo.lTicks );
			pWnd->SetPrecueTimes( _trInfo.dPDuration, _trInfo.dPLatency );
			pWnd->SetWarningTimes( _trInfo.dWDuration, _trInfo.dWLatency );
			pWnd->SetFlags( _trInfo.fImmediately, _trInfo.fStopWrongTarget, ( _fMaximize == FALSE ),
							_trInfo.fStartFirstTarget, _trInfo.fStopLastTarget );
		}
		if( fShowStim || !fJustQuit )
		{
			// condition instruction?
			TrialsMode tm = (TrialsMode)_trInfo.nTrialMode;
			if( ( ( tm == eTM_All ) || ( tm == eTM_AllAD ) ||
				  ( tm == eTM_AllADErr ) || ( tm == eTM_AllErr ) ||
				  ( tm == eTM_First ) || ( tm == eTM_FirstAD ) ||
				  ( tm == eTM_FirstADErr ) || ( tm == eTM_FirstErr ) ||
				  ( tm == eTM_FirstDelay ) || ( tm == eTM_FirstDelayAD ) ||
				  ( tm == eTM_FirstDelayADErr ) || ( tm == eTM_FirstDelayErr ) ) &&
				( szFile != _T("") ) )
			{
				BOOL fDoIt = TRUE;
				if( ( ( tm != eTM_All ) && ( tm != eTM_AllAD ) &&
					  ( tm != eTM_AllADErr ) && ( tm != eTM_AllErr ) ) &&
					( atoi( szFile.Mid( szFile.GetLength() - 6, 2 ) ) != 1 ) )
					fDoIt = FALSE;

				if( fDoIt )
				{
					InstructionDlg id( _fFullScreen ? CWnd::FromHandle( ::GetDesktopWindow() ) : ::AfxGetApp()->m_pMainWnd );
					id.m_fReadOnly = TRUE;
					id.m_szExpID = _trInfo.szExpID;
					id.m_szFileName = _trInfo.szExpID;
					id.m_szFileName += _trInfo.szCurCond;
					id.m_fCond = TRUE;
					id.DoModal();
//					if( id.DoModal() != IDOK )
//					{
//						m_fRecording = FALSE;
//						OnObjStop();
//						return;
//					}
				}
			}

			pWnd->Record( _trInfo.szExpID, _trInfo.szCurCond, szFile, szWStim,
						  szPStim, szStim, fShowStim, fTrial, _trInfo.dMagnetForce );
		}
	}
	else
	{
		m_fRecording = FALSE;
		pWnd->Stop();

		if( fJustQuit || fTrialOnly )
		{
			_StimOnly = FALSE;
			if( fTrialOnly ) RerunTrial( _T(""), _T(""), _T(""), _T(""), NULL );
			OnObjStop();
			return;
		}

		// Check for validity/existence of recorded file
		if( !_RedoTrial && _RunTrial2 )
		{
			CGraphView* pGV = GetRecordingPane();
			if( pGV && !pGV->m_fBeganRecording )
			{
 				if( BCGPMessageBox( IDS_NODATA, MB_OKCANCEL ) == IDOK )
 				{
					TroubleShootingDlg d;
					d.DoModal();
 				}

				_trInfo.nTrial--;
				_RedoTrial = TRUE;
			}
		}

		// Feedback
		pWnd->DrawFeedback( !( _fTrialFeedback1 || _fTrialFeedback2 ), _dFBMin1, _dFBMax1, _dFBVal1, FALSE );
		pWnd->DrawFeedback( !( _fTrialFeedback1 || _fTrialFeedback2 ), _dFBMin2, _dFBMax2, _dFBVal2, TRUE );

		// User timer between trials?
// 		BOOL fDoTimer = FALSE;
// 		if( _trInfo.fDisplay && ( _trInfo.fHWR || _trInfo.fTF ) ) fDoTimer = FALSE;
// 		else if( _ClockSecsMax > 0 ) fDoTimer = TRUE;
		// 23Apr10: GMB: It is not logical to eliminate a trial-to-trial timer (now, since we display knowledge of results)
		// just because the graph is going to be displayed (which originally served as a trial-to-trial break anyway)
		BOOL fDoTimer = ( _ClockSecsMax > 0 );

		if( !_RedoTrial )
		{
			// Let timer handle this
			if( _RunTrial2 )
			{
				if( fDoTimer )
				{
					if( RunProcess() ) SetTimer( TRIALTOTRIAL_TIMER, TIMER_INTERVAL, NULL );
					_RunTrial2 = FALSE;
				}
				// or not if charting
				else
				{
					pWnd->Clear();

					if( !fTrialOnly ) RunExperiment2( FALSE );
					else RerunTrial( _T(""), _T(""), _T(""), _T(""), NULL );

					fTrialOnly = FALSE;
				}
			}
		}
		else
		{
			if( _RunTrial2 )
			{
				// Let timer handle this
				if( fDoTimer )
				{
					SetTimer( TRIALTOTRIAL_TIMER, TIMER_INTERVAL, NULL );
					_RunTrial2 = FALSE;
				}
				// or not if charting
				else
				{
					pWnd->Clear();

					if( !fTrialOnly ) RunExperiment2( TRUE );
					else RerunTrial( _T(""), _T(""), _T(""), _T(""), NULL );

					fTrialOnly = FALSE;
				}
			}
		}
	}
}

void CMainFrame::OnTimer( UINT nIDEvent ) 
{
	// timer for in-between trials (250 ms)
	if( nIDEvent == TRIALTOTRIAL_TIMER )
	{
		if( _ClockSecs >= _ClockSecsMax )
		{
			KillTimer( TRIALTOTRIAL_TIMER );

			CLeftView* pLV = GetLeftView();
			BOOL fCont = pLV ? ( pLV->GetActionState() == ACTION_RUNEXPERIMENT ) : FALSE;
			if( fCont )
			{
				CGraphView* pWnd = GetRecordingPane();
				if( pWnd ) pWnd->Clear();

				if( !fTrialOnly ) RunExperiment2( TRUE );
				else RerunTrial( _T(""), _T(""), _T(""), _T(""), NULL );
			}
			else
			{
				CGraphView* pGV = GetRecordingPane();
				pGV->m_fNoFeedback = TRUE;
				pGV->DrawFeedback( TRUE );
				if( pLV->GetActionState() == ACTION_NONE ) pGV->Clear();
			}


			fTrialOnly = FALSE;
		}
		_ClockSecs += TIMER_INTERVAL;
	}

	CFrameWnd::OnTimer(nIDEvent);
}

void CMainFrame::StopRecording()
{
	if( m_fRecording )
	{
		Record();
		::SystemCleanup();
	}
}

void CMainFrame::StopCondition()
{
	BOOL fRand = FALSE, fRandT = FALSE, fSpecify = FALSE;
	if( !_trInfo.pPS ) return;

	_trInfo.pPS->get_SpecifySequence( &fSpecify );
	_trInfo.pPS->get_Randomize( &fRand );
	_trInfo.pPS->get_RandomizeTrials( &fRandT );

	if( fSpecify || ( fRand && fRandT ) )
	{
		BCGPMessageBox( IDS_NOSTOPCOND );
		return;
	}
	else _StopCondition = TRUE;
}

void CMainFrame::StopExperiment()
{
	m_fContinue = FALSE;
	_RunTrial2 = FALSE;
	_fTrialFeedback1 = FALSE;
	_fTrialFeedback2 = FALSE;

	if( m_fRecording )
	{
		Record();
		ResetSizes();
		CLeftView* pLV = GetLeftView();
		CTreeCtrl& tree = pLV->GetTreeCtrl();
		if( _htreeSubject ) tree.EnsureVisible( _htreeSubject );

		UserObj* pObj = ::GetCurrentUserObj();
		if( pObj ) pObj->Reset();
		pLV->SetActionState( ACTION_NONE );
		CString szData;
		szData.LoadString( IDS_READY );
		DisableProgress();
		ShowStatusText( szData );
	}
	else
	{
		CGraphView* pWnd = GetRecordingPane();
		pWnd->DrawFeedback( TRUE );
		pWnd->Clear();
	}

	::SystemCleanup();
}

void CMainFrame::RedoTrial()
{
	CString szTemp, szIdx, szRaw;
	int nReps = 1, nTrial = 1;
	CFileFind ff;

	Experiments exp;
	if( !exp.IsValid() ) return;
	HRESULT hr = exp.Find( _trInfo.szExpID );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPFIND_ERR );
		return;
	}
	short nType = 0;
	exp.GetType( nType );
	if( nType == EXP_TYPE_HANDWRITING )
	{
		if( ::IsGripperModeOn() )
		{
			BCGPMessageBox( _T("This is a Handwriting experiment. Please select the appropriate input device in Settings.") );
			return;
		}
	}
	else if( nType == EXP_TYPE_GRIPPER )
	{
		if( !::IsGripperModeOn() )
		{
			BCGPMessageBox( _T("This is a Grip-Force experiment. Please select the appropriate input device in Settings.") );
			return;
		}
	}

	if( _trInfo.nRep > 1 )
	{
		nReps = _trInfo.nRep; // - 2;
		nTrial = _trInfo.nTrial - 1; // - 2;
		if( nTrial <= 0 ) nTrial = 1;
	}
	else
	{
		// Recording file
		if( ::IsGripperModeOn() )
			GetFRDMask( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, _trInfo.szCurCond, szTemp );
		else
			GetHWRMask( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, _trInfo.szCurCond, szTemp );
		if( ff.FindFile( szTemp ) )
		{
			while( ff.FindNextFile() )
				nReps++;
		}

		nTrial = _trInfo.nTrial - 1; // - 2;
		if( nTrial <= 0 ) nTrial = 0;
	}

	// Remove last recorded file
	if( nReps < 10 ) szIdx.Format( _T("0%d"), nReps );
	else szIdx.Format( _T("%d"), nReps );
	if( ::IsGripperModeOn() )
		GetFRD( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, _trInfo.szCurCond, szIdx, szRaw );
	else
		GetHWR( _trInfo.szExpID, _trInfo.szGrpID, _trInfo.szSubjID, _trInfo.szCurCond, szIdx, szRaw );
	if( ff.FindFile( szRaw ) ) REMOVE_FILE( szRaw );
	CString szTmp = szRaw;
	szTmp = szTmp.Left( szRaw.GetLength() - 3 );
	szTmp += _T("TF");
	if( ff.FindFile( szTmp ) ) REMOVE_FILE( szTmp );
	szTmp = szTmp.Left( szRaw.GetLength() - 2 );
	szTmp += _T("SEG");
	if( ff.FindFile( szTmp ) ) REMOVE_FILE( szTmp );

	_trInfo.nTrial = nTrial;

	_RedoTrial = TRUE;

	Record();
}

void CMainFrame::ReprocessSubjectCond( CString szExpID, CString szSubjID, CString szGrpID, CString szCondID,
									   double dDevRes, int nSampRate, HTREEITEM hItem, CLeftView* pLeft,
									   CListView* pLV, int nSubjCount, int nCurSubj, BOOL fReprocAll )
{
	CFileFind ff;
	CString szRaw, szLex, szFile, szData, szTitle;
	CString szExt, szCon, szErr;
	short nMin = 0, nMax = 0, nPos = 0, nSkip = 0, nGripTask = 0;
	double dLength = 0.0, dRange = 0.0, dDir = 0.0, dRangeDir = 0.0, dMagnetForce = 0.;
	BOOL fRecord = FALSE, fProcess = FALSE, fFlagBadTarget = FALSE;
	short nWord = 0;
	CString szParent;

	if( !hItem || !pLV || !pLeft ) return;
	CListCtrl& lst = pLV->GetListCtrl();

	szData.Format( IDS_AL_REPROC2, szSubjID, szExpID, szCondID, nCurSubj, nSubjCount );
	OutputAMessage( szData, TRUE );

	Experiments::_Reproc = TRUE;

	// Experiment Type
	Experiments exp;
	if( !exp.IsValid() ) return;
	HRESULT hr = exp.Find( szExpID );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPFIND_ERR );
		return;
	}
	short nType = 0;
	exp.GetType( nType );

	// Process Settings
	IProcSetting* pPS = NULL;
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		Experiments::_Reproc = FALSE;
		return;
	}
	pPS->Find( szExpID.AllocSysString() );
	// Use default if not found

	// Process object
	IProcess* pProcess = Experiments::GetProcessObject( GetMsgPaneList(), pPS, szExpID, TRUE );
	hr = pProcess ? pProcess->SetOutputWindow( (VARIANT*)&lst ) : E_FAIL;
	if( FAILED( hr ) )
	{
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}
	if( pProcess )
	{
		if( nSampRate == 0 )
		{
			szData = "EXPERIMENT: Subject sampling rate is not set - using experiment value.";
			OutputAMessage( szData );
		}
		if( dDevRes == 0. )
		{
			szData = "EXPERIMENT: Subject device resolution is not set - using experiment value.";
			OutputAMessage( szData );
		}

		short nRateVal = 0, nPressVal = 0;
		double dResoVal = 0.0;
		pPS->get_SamplingRate( &nRateVal );
		if( nSampRate != 0 )
		{
			if( nRateVal != nSampRate )
			{
				szData.Format( _T("EXPERIMENT: Subject sampling rate (%d) differs from experiment (%d). Using subject value."), nSampRate, nRateVal );
				OutputAMessage( szData );
			}
			nRateVal = nSampRate;
		}
		pProcess->put_SamplingRate( nRateVal );
		pPS->get_MinPenPressure( &nPressVal );
		pProcess->put_MinPenPressure( nPressVal );
		pPS->get_DeviceResolution( &dResoVal );
		if( dDevRes != 0. )
		{
			if( dDevRes != dResoVal )
			{
				szData.Format( _T("EXPERIMENT: Subject device resolution (%g) differs from experiment (%g). Using subject value."), dDevRes, dResoVal );
				OutputAMessage( szData );
			}
			dResoVal = dDevRes;
		}
		pProcess->put_DeviceResolution( dResoVal );
		short nDisCorrection = 0;
		double dDisFactor = 0.0, dDisZInsPoint = 0.0, dDisRelErr = 0.0, dDisAbsErr = 0.0, dDisError = 0.;
		pPS->get_DisCorrection( &nDisCorrection );
		pPS->get_DisFactor( &dDisFactor );
		pPS->get_DisZInsertPoint( &dDisZInsPoint );
		pPS->get_DisRelError( &dDisRelErr );
		pPS->get_DisAbsError( &dDisAbsErr );
		pProcess->Discontinuity( nDisCorrection, dDisFactor, dDisZInsPoint, dDisRelErr, dDisAbsErr );
		pPS->get_DisError( &dDisError );
		pProcess->put_DisError( dDisError );
	}
	else
	{
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}

	// get appropriate interface
	IProcSetFlags* pPSF = NULL;
	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	if( FAILED( hr ) ) { pPS->Release(); return; }

	// Consis parameters
	BOOL fConA, fConC, fConM, fConN, fConS, fConU, fConO;
	pPSF->get_CON_A( &fConA );
	pPSF->get_CON_C( &fConC );
	pPSF->get_CON_M( &fConM );
	pPSF->get_CON_N( &fConN );
	pPSF->get_CON_S( &fConS );
	pPSF->get_CON_U( &fConU );
	pPSF->get_CON_O( &fConO );
	pPSF->get_CON_D( &fConD );
	UINT conSwitch = eCon_None;
	if( fConA ) conSwitch |= eCon_A;
	if( fConC ) conSwitch |= eCon_C;
	if( fConM ) conSwitch |= eCon_M;
	if( fConN ) conSwitch |= eCon_N;
	if( fConS ) conSwitch |= eCon_S;
	if( fConU ) conSwitch |= eCon_U;
	if( fConO ) conSwitch |= eCon_O;
	pPSF->Release();

	// Condition
	NSConditions cond;
	if( !cond.IsValid() )
	{
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}
	BSTR bstrLex = NULL;
	hr = cond.Find( szCondID );
	if( SUCCEEDED( hr ) )
	{
		cond.GetLex( szLex );
		cond.GetStrokeMin( nMin );
		cond.GetStrokeMax( nMax );
		cond.GetStrokeLength( dLength );
		cond.GetRangeLength( dRange );
		cond.GetStrokeDirection( dDir );
		cond.GetRangeDirection( dRangeDir );
		cond.GetStrokeSkip( nSkip );
		cond.GetRecord( fRecord );
		cond.GetProcess( fProcess );
		cond.GetWord( nWord );
		cond.GetParent( szParent );
		cond.GetFlagBadTarget( fFlagBadTarget );
		cond.GetGripperTask( nGripTask );
		cond.GetMagnetForce( dMagnetForce );
	}
	else
	{
		BCGPMessageBox( IDS_CONDFIND_ERR );
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}
	UINT nGripSwitch = nGripTask;

	// If not a condition for processing, exit
	if( !fProcess )
	{
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}

	// SEN2WORD
	CStringList lstRaw;
	CString szSen, szIdx, szWordDis, szWordDisOut;
	POSITION pos;
	int nReps = 0;
	// TODO: Note...this is not likely the most efficient manner at doing this
	// Do we run sen2word? parent would not be null...only handwriting...
	// ...and only ones with process but not record (should be forced by dialog)
	if( ( nType == EXP_TYPE_HANDWRITING ) && fProcess && !fRecord &&
		( szParent != _T("") ) )
	{
		// Get all trials for this condition
		lstRaw.RemoveAll();
		GetHWRMask( szExpID, szGrpID, szSubjID, szParent, szFile );
		if( ff.FindFile( szFile ) )
		{
			while( ff.FindNextFile() )
			{
				szRaw = ff.GetFilePath();
				lstRaw.AddTail( szRaw );
			}
			// Last one
			szRaw = ff.GetFilePath();
			lstRaw.AddTail( szRaw );
		}

		// Loop through each trial
		pos = lstRaw.GetHeadPosition();
		while( pos )
		{
			// Trial
			szRaw = lstRaw.GetNext( pos );
			szIdx = szRaw.Mid( szRaw.GetLength() - 6, 2 );

			// Sen2word file naming convention
			GetSEN( szExpID, szGrpID, szSubjID, szParent, szIdx, szSen );

			// Run sen2word (TODO: Does sen2word return FAIL if only one word)
			Experiments::ProcessSen2wrd( szRaw, szSen, GetMsgPaneList() );

			// Copy appropriate word file (if exists)
			// Idx based upon word (3 chars = 1-999)
			szIdx.Format( _T("%03d"), nWord );
			// Construct source file name
			szSen = szSen.Left( szSen.GetLength() - 4 );
			szSen += szIdx;
			szWordDis = szRaw;
			szSen += _T(".SEN");
			szWordDis += _T(".DIS");
			// Construct target file name
			GetHWRMask( szExpID, szGrpID, szSubjID, szCondID, szData );
			szWordDisOut = szData.Left( szData.GetLength() - 3 );
			szWordDisOut += _T("DIS");
			nReps = 1;
			if( ff.FindFile( szData ) )
			{
				nReps++;
				while( ff.FindNextFile() ) nReps++;
			}
			// Copy dis file (if exists)
			if( ff.FindFile( szWordDis ) )
			{
				CopyFile( szWordDis, szWordDisOut, FALSE );
				// delete the file
				try { CFile::Remove( szWordDis ); } catch( CFileException* e ) { e->Delete(); }
			}
			szIdx.Format( _T("%02d"), nReps );
			GetHWR( szExpID, szGrpID, szSubjID, szCondID, szIdx, szData );
			// Copy file (if exists)...then delete all .SEN files
			if( ff.FindFile( szSen ) ) CopyFile( szSen, szData, TRUE );
			GetSENMask( szExpID, szGrpID, szSubjID, szParent, szData );
			if( ff.FindFile( szData ) )
			{
				while( ff.FindNextFile() )
				{
					szData = ff.GetFilePath();
					REMOVE_FILE( szData );
				}

				// last one
				szData = ff.GetFilePath();
				REMOVE_FILE( szData );
			}
		}
	}
	lstRaw.RemoveAll();

	// Delete CON/EXT/ERR files
	GetCON( szExpID, szGrpID, szSubjID, szCondID, szCon );
	if( ff.FindFile( szCon ) ) REMOVE_FILE( szCon );
	GetEXT( szExpID, szGrpID, szSubjID, szCondID, szExt );
	if( ff.FindFile( szExt ) ) REMOVE_FILE( szExt );
	GetERR( szExpID, szGrpID, szSubjID, szCondID, szErr );
	if( ff.FindFile( szErr ) ) REMOVE_FILE( szErr );

	// If rapid view is on, and current subject trials have not been expanded yet,
	// because a single condition will only be part of that subject's trial set
	// we must explicitly load all the trials first so that all + this condition's
	// trials are visible afterwards
	CTreeCtrl& tree = pLeft->GetTreeCtrl();
	if( hItem && ::GetDetailedOutput() )
	{
		if( !tree.ItemHasChildren( hItem ) )
			pLeft->DoSTrials( hItem );
	}

	// Loop through each persisted trial to get count
	HTREEITEM hTrial;
	int nSteps = 0;
	if( nType == EXP_TYPE_GRIPPER )
		GetFRDMask( szExpID, szGrpID, szSubjID, szCondID, szFile );
	else if( nType == EXP_TYPE_HANDWRITING )
		GetHWRMask( szExpID, szGrpID, szSubjID, szCondID, szFile );
	if( ff.FindFile( szFile ) )
	{
		while( ff.FindNextFile() )
		{
			szRaw = ff.GetFilePath();
			szRaw.MakeUpper();
			lstRaw.AddTail( szRaw );
			nSteps++;

			if( ::GetDetailedOutput() )
			{
				// If this file is displayed in tree, remove
				szRaw = ff.GetFileName();
				szRaw.MakeUpper();
				hTrial = pLeft->FindTrial( szRaw, szExpID );
				if( hTrial ) tree.DeleteItem( hTrial );
			}
		}
		// Last one
		szRaw = ff.GetFilePath();
		szRaw.MakeUpper();
		lstRaw.AddTail( szRaw );
		nSteps++;

		if( ::GetDetailedOutput() )
		{
			// If this file is displayed in tree, remove
			szRaw = ff.GetFileName();
			szRaw.MakeUpper();
			hTrial = pLeft->FindTrial( szRaw, szExpID );
			if( hTrial ) tree.DeleteItem( hTrial );
		}
	}
	else
	{
//		BCGPMessageBox( IDS_NOTRIALS );
		pPS->Release();
		Experiments::_Reproc = FALSE;
		return;
	}

	// Progress Meter steps
	nSteps = ( nSteps * 4 ) + 1;
	EnableProgress( nSteps );

	// Window Title
	szTitle.LoadString( AFX_IDS_APP_TITLE );
	szData.Format( _T("%s - Processing subject %s of group %s in experiment %s"), szTitle,
				   szSubjID, szGrpID, szExpID );
//	SetWindowText( szData );

	// Process
	int nCurStep = 0;
	pos = lstRaw.GetHeadPosition();
	int nRep = 1, nSkipped = 0, nToProcess = 0;
	while( pos )
	{
		szRaw = lstRaw.GetNext( pos );
		nToProcess++;
		Experiments::ProcessTrial( szExpID, szSubjID, szGrpID, szCondID, szRaw, TRUE,
								   fFlagBadTarget, pPS, pLeft, GetMsgPaneList(),
								   nSkipped, TRUE, hItem, fReprocAll, nGripSwitch );
	}

	// CONSIS
	if( nSkipped < nToProcess )
	{
		GetEXT( szExpID, szGrpID, szSubjID, szCondID, szExt );
		GetCON( szExpID, szGrpID, szSubjID, szCondID, szCon );
		GetERR( szExpID, szGrpID, szSubjID, szCondID, szErr );

		szData.Format( IDS_AL_PROCCON, szExt, szCon, szCondID );
		OutputAMessage( szData, FALSE );
		BOOL fPassed;
		Experiments::ProcessConsis( szExt, szCon, szLex, nMin, nMax, dLength,
									dRange, dDir, dRangeDir, nSkip, fFlagBadTarget,
									szErr, szCondID, conSwitch, NULL, FALSE, fPassed,
									GetMsgPaneList(), fConD, dMagnetForce, nGripSwitch );
	}

	// Cleanup
	pPS->Release();
//	SetWindowText( szTitle );

	DisableProgress();

	Experiments::_Reproc = FALSE;
}

void CMainFrame::ReprocessTrial( CString szExpID, CString szSubjID, CString szGrpID, CString szTrial,
								 double dDevRes, int nSampRate, HTREEITEM hItem, BOOL fAppend )
{
	CFileFind ff;
	CString szCondID, szRaw, szTF, szSeg, szExt, szCon, szErr, szLex, szFile,
			szData, szParent;
	short nMin = 0, nMax = 0, nPos = 0, nSkip = 0, nSteps = 0, nGripTask = 0;
	double dLength = 0.0, dRange = 0.0, dDir = 0.0, dRangeDir = 0.0, dVal = 0.0;
	BOOL fRecord = FALSE, fProcess = FALSE, fCont = FALSE, fFlagBadTarget = FALSE;
	CStringList lstRaw;

	if( !hItem ) return;

	szData.Format( IDS_AL_REPROCT, szTrial, szSubjID, szExpID );
	OutputAMessage( szData, TRUE );

	// Experiment type
	Experiments exp;
	if( !exp.IsValid() ) return;
	HRESULT hr = exp.Find( szExpID );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPFIND_ERR );
		return;
	}
	short nType = 0;
	exp.GetType( nType );

	// Process Settings
	IProcSetting* pPS = NULL;
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	pPS->Find( szExpID.AllocSysString() );
	// Use default if not found

	// Process object
	IProcess* pProcess = Experiments::GetProcessObject( GetMsgPaneList(), pPS, szExpID, TRUE );
	if( pProcess )
	{
		if( nSampRate == 0 )
		{
			szData = "EXPERIMENT: Subject sampling rate is not set - using experiment value.";
			OutputAMessage( szData );
		}
		if( dDevRes == 0. )
		{
			szData = "EXPERIMENT: Subject device resolution is not set - using experiment value.";
			OutputAMessage( szData );
		}

		short nRateVal = 0, nPressVal = 0;
		double dResoVal = 0.0;
		pPS->get_SamplingRate( &nRateVal );
		if( nSampRate != 0 )
		{
			if( nRateVal != nSampRate )
			{
				szData.Format( _T("EXPERIMENT: Subject sampling rate (%d) differs from experiment (%d). Using subject value."), nSampRate, nRateVal );
				OutputAMessage( szData );
			}
			nRateVal = nSampRate;
		}
		pProcess->put_SamplingRate( nRateVal );
		pPS->get_MinPenPressure( &nPressVal );
		pProcess->put_MinPenPressure( nPressVal );
		pPS->get_DeviceResolution( &dResoVal );
		if( dDevRes != 0. )
		{
			if( dDevRes != dResoVal )
			{
				szData.Format( _T("EXPERIMENT: Subject device resolution (%g) differs from experiment (%g). Using subject value."), dDevRes, dResoVal );
				OutputAMessage( szData );
			}
			dResoVal = dDevRes;
		}
		pProcess->put_DeviceResolution( dResoVal );
		short nDisCorrection = 0;
		double dDisFactor = 0.0, dDisZInsPoint = 0.0, dDisRelErr = 0.0, dDisAbsErr = 0.0, dDisError = 0.;
		pPS->get_DisCorrection( &nDisCorrection );
		pPS->get_DisFactor( &dDisFactor );
		pPS->get_DisZInsertPoint( &dDisZInsPoint );
		pPS->get_DisRelError( &dDisRelErr );
		pPS->get_DisAbsError( &dDisAbsErr );
		pProcess->Discontinuity( nDisCorrection, dDisFactor, dDisZInsPoint, dDisRelErr, dDisAbsErr );
		pPS->get_DisError( &dDisError );
		pProcess->put_DisError( dDisError );
	}
	else
	{
		pPS->Release();
		return;
	}

	// Extract Condition
	szData = szTrial.Mid( szExpID.GetLength() + szGrpID.GetLength() + szSubjID.GetLength() );
	szCondID = szData.Left( szData.GetLength() - ( 4 + 2 ) );

	// Extract trial # and ensure <= 99 (we can do this by ensuring the file name
	// is exactly 3(exp)+3(grp)+3(subj)+3(cond)+2(trial)+.ext chars long)
	if( szTrial.GetLength() != 18 )
	{
		pPS->Release();
		return;
	}

	// experiment condition list
	IExperimentCondition* pECL = NULL;
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pECL );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCONDL_ERR );
		Experiments::_Reproc = FALSE;
		pPS->Release();
		return;
	}
	hr = pECL->FindForExperiment( szExpID.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_NOCONDS );
		pECL->Release();
		Experiments::_Reproc = FALSE;
		pPS->Release();
		return;
	}

	// Condition
	NSConditions cond;
	if( !cond.IsValid() )
	{
		pECL->Release();
		Experiments::_Reproc = FALSE;
		pPS->Release();
		return;
	}
	hr = cond.Find( szCondID );
	if( SUCCEEDED( hr ) )
	{
		cond.GetLex( szLex );
		cond.GetStrokeMin( nMin );
		cond.GetStrokeMax( nMax );
		cond.GetStrokeLength( dLength );
		cond.GetRangeLength( dRange );
		cond.GetStrokeDirection( dDir );
		cond.GetRangeDirection( dRangeDir );
		cond.GetStrokeSkip( nSkip );
		cond.GetRecord( fRecord );
		cond.GetProcess( fProcess );
		cond.GetParent( szParent );
		cond.GetFlagBadTarget( fFlagBadTarget );
		cond.GetGripperTask( nGripTask );
	}
	else
	{
		BCGPMessageBox( IDS_CONDFIND_ERR );
		pECL->Release();
		Experiments::_Reproc = FALSE;
		pPS->Release();
		return;
	}
	UINT nGripSwitch = nGripTask;

	// If this is a sub-condition, we only process...no word extraction
	if( ( szParent == _T("") ) && fRecord && ( nType == EXP_TYPE_HANDWRITING ) )
	{
		BSTR bstrCondID = NULL;
		CString szCondID2, szParent2;
		BOOL fProcess2 = FALSE, fRecord2 = FALSE;
		// loop through NSConditions to see how many (if any) sen2word steps there are
		while( SUCCEEDED( hr ) )
		{
			pECL->get_ConditionID( &bstrCondID );
			szCondID2 = bstrCondID;

			// we obviously don't count the condition for this trial
			if( ( szCondID2 != szCondID ) && SUCCEEDED( cond.Find( bstrCondID ) ) )
			{
				cond.GetRecord( fRecord2 );
				cond.GetProcess( fProcess2 );
				cond.GetParent( szParent2 );

				// Only if process/handwriting/not record/and legit parent
				// we only want those sub-NSConditions whose parent = condition for this trial
				if( fProcess2 && !fRecord2 && ( szParent2 == szCondID ) )
					nSteps++;
			}

			// Next condition
			hr = pECL->GetNext();
		}
	}

	// word extraction if necessary
	CStringList lstCondWords;
	CString szCondWord, szWordDis, szWordDisOut;
	if( ( szParent == _T("") ) && fRecord && ( nType == EXP_TYPE_HANDWRITING ) )
	{
		BSTR bstrCondID = NULL;
		CString szCondID2, szParent2;
		BOOL fProcess2 = FALSE, fRecord2 = FALSE;
		// loop through NSConditions
		hr = pECL->FindForExperiment( szExpID.AllocSysString() );
		while( SUCCEEDED( hr ) )
		{
			pECL->get_ConditionID( &bstrCondID );
			szCondID2 = bstrCondID;

			// we obviously don't count the condition for this trial
			if( ( szCondID2 != szCondID ) && SUCCEEDED( cond.Find( bstrCondID ) ) )
			{
				short nWord = 0;

				cond.GetRecord( fRecord2 );
				cond.GetProcess( fProcess2 );
				cond.GetParent( szParent2 );
				cond.GetWord( nWord );

				// we only want those sub-NSConditions whose parent = condition for this trial
				if( fProcess2 && !fRecord2 && ( szParent2 == szCondID ) )
				{
					CString szSen, szIdx;

					// trial name
					GetHWR( szExpID, szGrpID, szSubjID, szTrial, szRaw );

					// Sen2word file naming convention
					szIdx = szTrial.Mid( szTrial.GetLength() - 6, 2 );
					GetSEN( szExpID, szGrpID, szSubjID, szParent2, szIdx, szSen );

					// Run sen2word (TODO: Does sen2word return FAIL if only one word)
					// ...if this trial for parent condition has not already been run
					szCondWord = szParent;
					szCondWord += szIdx;
					if( !lstCondWords.Find( szCondWord ) )
					{
						lstCondWords.AddTail( szCondWord );
						Experiments::ProcessSen2wrd( szRaw, szSen, GetMsgPaneList() );
					}

					// Copy appropriate word file (if exists)
					// Idx based upon word (3 chars = 1-999)
					szIdx.Format( _T("%03d"), nWord );
					// Construct source file name
					szRaw = szSen.Left( szSen.GetLength() - 4 );
					szRaw += szIdx;
					szWordDis = szRaw;
					szRaw += _T(".SEN");
					szWordDis += _T(".DIS");
					// Construct target file name
					szIdx = szTrial.Mid( szTrial.GetLength() - 6, 2 );
					GetHWR( szExpID, szGrpID, szSubjID, szCondID2, szIdx, szData );
					szWordDisOut = szData.Left( szData.GetLength() - 3 );
					szWordDisOut += _T("DIS");
					// Copy file (if exists)...then delete all .SEN files
					if( ff.FindFile( szRaw ) )
					{
						CopyFile( szRaw, szData, FALSE );
						lstRaw.AddTail( szData );
					}
					// Copy dis file (if exists)
					if( ff.FindFile( szWordDis ) )
					{
						CopyFile( szWordDis, szWordDisOut, FALSE );
						// delete the file
						try { CFile::Remove( szWordDis ); } catch( CFileException* e ) { e->Delete(); }
					}
				}
			}

			// Next condition
			hr = pECL->GetNext();
		}
	}

	// Delete any SEN files that were created
	GetSENMask( szExpID, szGrpID, szSubjID, szParent, szData );
	if( ff.FindFile( szData ) )
	{
		while( ff.FindNextFile() )
		{
			szData = ff.GetFilePath();
			REMOVE_FILE( szData );
		}

		// last one
		szData = ff.GetFilePath();
		REMOVE_FILE( szData );
	}

	POSITION pos = lstRaw.GetHeadPosition();
	if( !pos )
	{
		// we only have one trial
		GetHWR( szExpID, szGrpID, szSubjID, szTrial, szRaw );
		lstRaw.AddTail( szRaw );
	}

	// Process
	pos = lstRaw.GetHeadPosition();
	while( pos )
	{
		szRaw = lstRaw.GetNext( pos );
		CLeftView* pLV = GetLeftView();
		int nSkipped = 0;
		Experiments::ProcessTrial( szExpID, szSubjID, szGrpID, szCondID, szRaw, fAppend,
								   fFlagBadTarget, pPS, pLV, GetMsgPaneList(), nSkipped,
								   TRUE, hItem, TRUE, FALSE, nGripSwitch );
	}

	pECL->Release();
	pPS->Release();
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();

	// Optionally delete summarization/analysis files
	CString szInc;
	GetExpINC( szExpID, szInc );
	GetExpERR( szExpID, szErr );
	if( ff.FindFile( szInc ) )
	{
		if( BCGPMessageBox( IDS_CLEANUP, MB_YESNO ) == IDYES )
		{
			CString szMsg;
			if( ff.FindFile( szInc ) )
			{
				try
				{
					CFile::Remove( szInc );
				}
				catch( CFileException* e )
				{
					char pszErr[ 200 ];
					e->GetErrorMessage( pszErr, 200 );
					e->Delete();
					szMsg = pszErr;
					szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
					BCGPMessageBox( szMsg, MB_ICONERROR );
					return;
				}
				szMsg.Format( _T("Deleting file: %s"), szInc );
				OutputAMessage( szMsg, TRUE );
			}
			// Remove TMP file
			CString szTmp = szInc.Left( szInc.GetLength() - 3 );
			szTmp += _T("tmp");
			if( ff.FindFile( szTmp ) )
			{
				try
				{
					CFile::Remove( szInc );
				}
				catch( CFileException* e )
				{
					char pszErr[ 200 ];
					e->GetErrorMessage( pszErr, 200 );
					e->Delete();
					szMsg = pszErr;
					szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
					BCGPMessageBox( szMsg, MB_ICONERROR );
					return;
				}
				szMsg.Format( _T("Deleting file: %s"), szTmp );
				OutputAMessage( szMsg, TRUE );
			}
			// Remove ERR file
			if( ff.FindFile( szErr ) )
			{
				try
				{
					CFile::Remove( szInc );
				}
				catch( CFileException* e )
				{
					char pszErr[ 200 ];
					e->GetErrorMessage( pszErr, 200 );
					e->Delete();
					szMsg = pszErr;
					szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
					BCGPMessageBox( szMsg, MB_ICONERROR );
					return;
				}
				szMsg.Format( _T("Deleting file: %s"), szErr );
				OutputAMessage( szMsg, TRUE );
			}
			// Remove AXS memory file
			szTmp = szInc.Left( szInc.GetLength() - 7 );	// exp.axs
			szTmp += _T("graph.axs");
			if( ff.FindFile( szTmp ) )
			{
				try
				{
					CFile::Remove( szInc );
				}
				catch( CFileException* e )
				{
					char pszErr[ 200 ];
					e->GetErrorMessage( pszErr, 200 );
					e->Delete();
					szMsg = pszErr;
					szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
					BCGPMessageBox( szMsg, MB_ICONERROR );
					return;
				}
				szMsg.Format( _T("Deleting file: %s"), szTmp );
				OutputAMessage( szMsg, TRUE );
			}
		}
	}
}

void CMainFrame::RerunTrial( CString szExp, CString szSubj, CString szGrp, CString szTrial, HTREEITEM hItem )
{
	static CString szRaw;
	static HTREEITEM hTrial;
	CString szCondID, szTemp;
	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	CRecView* pRV = GetSettingsPane();
	if( !pLV || !pGV || !pRV ) return;

	if( hItem )
	{
		hTrial = hItem;
		if( ::IsGripperModeOn() ) GetFRD( szExp, szGrp, szSubj, szTrial, szRaw );
		else GetHWR( szExp, szGrp, szSubj, szTrial, szRaw );

		// DETERMINE CONDITION
		szTemp = szTrial.Left( szTrial.GetLength() - 4 );
		// Extract experiment
		szTemp = szTemp.Mid( szExp.GetLength() );
		// Extract group
		szTemp = szTemp.Mid( szGrp.GetLength() );
		// Extract subject
		szTemp = szTemp.Mid( szSubj.GetLength() );
		// Extract trial #
		szCondID = szTemp.Left( szTemp.GetLength() - 2 );
		// Condition description/instruction
		NSConditions cond;
		if( !cond.IsValid() ) return;
		if( FAILED( cond.Find( szCondID ) ) )
		{
			BCGPMessageBox( IDS_CONDFIND_ERR );
			return;
		}
		CString szCond, szInstr, szStimID, szPStimID, szWStimID;
		cond.GetDescription( szCond );
		cond.GetInstruction( szInstr );
		cond.GetMagnetForce( _trInfo.dMagnetForce );
		cond.GetStimulusWarning( szWStimID );
		cond.GetWarningDuration( _trInfo.dWDuration );
		cond.GetWarningLatency( _trInfo.dWLatency );
		cond.GetStimulusPrecue( szPStimID );
		cond.GetPrecueDuration( _trInfo.dPDuration );
		cond.GetPrecueLatency( _trInfo.dPLatency );
		cond.GetStimulus( szStimID );
		cond.GetRecordImmediately( _trInfo.fImmediately );
		cond.GetStopWrongTarget( _trInfo.fStopWrongTarget );
		cond.GetStartFirstTarget( _trInfo.fStartFirstTarget );
		cond.GetStopLastTarget( _trInfo.fStopLastTarget );
		cond.GetFeedbackInfo( 1, _fTrialFeedback1, _nFBColumn1, _nFBStroke1,
			_dFBMin1, _dFBMax1, _fSwapColors1 );
		cond.GetFeedbackInfo( 2, _fTrialFeedback2, _nFBColumn2, _nFBStroke2,
			_dFBMin2, _dFBMax2, _fSwapColors2 );
		// gen stimuli as necessary
		{
		Stimuluss stim;
		if( ( szWStimID != "" ) && !stim.Find( szWStimID ) && !stim.GenerateFiles() )
		{
			BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
			return;
		}
		if( ( szPStimID != "" ) && !stim.Find( szPStimID ) && !stim.GenerateFiles() )
		{
			BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
			return;
		}
		if( ( szStimID != "" ) && !stim.Find( szStimID ) && !stim.GenerateFiles() )
		{
			BCGPMessageBox( _T("Error generating stimulus files.\nPlease verify the stimuli of each condition for this experiment."), MB_OK | MB_ICONERROR );
			return;
		}
		}

		// get actual sampling rate & dev res
		double dDevRes = 0.;
		int nSampRate = 0;
		BOOL fSupportTilt = FALSE;
		BOOL fMouse = FALSE;
		{
			long lRes = 0, lX = 0, lY = 0;
			short nRate = 0, nMaxPP = 0;
			BOOL fInches = FALSE;
			CString szDev;
			if( ::GetDigitizerInfo( szDev, lRes, lX, lY, nRate, fInches, fSupportTilt, nMaxPP ) )
			{
				if( ( lX > 0 ) && ( lY > 0 ) )
				{
					nSampRate = nRate;
					dDevRes = 1.0 / ( ( lRes != 0 ) ? lRes : 1.0 );

					if( fInches ) dDevRes *= 2.54;
				}
			}
			else if( !::IsTabletModeOn() && !::IsGripperModeOn() )
			{
				nSampRate = MOUSE_RATE;
				dDevRes = MOUSE_RES;
				fMouse = TRUE;
			}
		}

		// get subject settings for rate, etc. if exists
		BOOL fDoWarning = FALSE;
		double dSDevRes = 0.;
		short nSSampRate = 0;
		HRESULT hrs = pLV->m_pEML->Find( szExp.AllocSysString(),
										 szGrp.AllocSysString(),
										 szSubj.AllocSysString() );
		if( SUCCEEDED( hrs ) )
		{
			pLV->m_pEML->get_DeviceRes( &dSDevRes );
			pLV->m_pEML->get_SamplingRate( &nSSampRate );
			if( ( dSDevRes == dDevRes ) && ( nSSampRate == nSampRate ) )
				fDoWarning = FALSE;
		}

		// get experiment settings
		BOOL fUseTilt = FALSE;
		short nRate = 120, nPressure = 1;
		double dRes = 0.001;
		TrialsMode tm = eTM_None;
		IProcSetting* pPS = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									   IID_IProcSetting, (LPVOID*)&pPS );
		if( SUCCEEDED( hr ) )
		{
			hr = pPS->Find( szExp.AllocSysString() );
			// if not found, just use default
			pPS->get_SamplingRate( &nRate );
			pPS->get_MinPenPressure( &nPressure );
			pPS->get_DeviceResolution( &dRes );
			pPS->get_UseTilt( &fUseTilt );
			pPS->get_TrialMode( &tm );
			pPS->get_InstructionAlwaysVisible( &( pGV->m_fInstrVis ) );

			// check settings
			if( fMouse )				// if mouse, we're using settings of mouse
			{
				nSampRate = nRate;
				dDevRes = ::GetMouseResolution();
			}
			if( fUseTilt && !fSupportTilt )
			{
				CString szMsg = _T("WARNING: Experiment settings specify the recording of pen tilt which is not supported by the selected device. No tilt will be recorded.");
				OutputAMessage( szMsg, TRUE );
				fUseTilt = FALSE;
			}
			if( fDoWarning )
			{
				if( ( nRate != nSampRate ) || ( (float)dRes != (float)dDevRes ) )
				{
					CString szMsg;
					szMsg = _T("Experiment settings for the input device differ from the input device connected to this PC.\n\nYou can change the settings by clicking CANCEL and going to the Input Device settings of the experiment properties.\n\nClick YES to correct these settings for this subject.\nClick NO to continue without correcting these settings.\nClick CANCEL to end the experiment.");
					int nYNC = BCGPMessageBox( szMsg, MB_YESNOCANCEL );
					if( nYNC == IDCANCEL )
					{
						pPS->Release();
						return;
					}
					else if( nYNC == IDYES )
					{
						nRate = nSampRate;
						dRes = dDevRes;

						// update subject samp rate/dev res
						pLV->m_pEML->put_DeviceRes( dDevRes );
						pLV->m_pEML->put_SamplingRate( nRate );
						hrs = pLV->m_pEML->Modify();
						if( FAILED( hrs ) ) BCGPMessageBox( _T("Unable to update subject device resolution and sampling rate.") );
					}
				}
			}
			else
			{
				nRate = nSampRate;
				dRes = dDevRes;
			}

			IProcSetRunExp* pPSRE = NULL;
			HRESULT hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				pPSRE->get_ProcImmediately( &_trInfo.fProcess );
				pPSRE->get_DisplayCharts( &_trInfo.fDisplay );
				pPSRE->get_DisplayRaw( &_trInfo.fHWR );
				pPSRE->get_DisplayTF( &_trInfo.fTF );
				pPSRE->get_TimeoutStart( &_trInfo.dStart );
				pPSRE->get_TimeoutRecord( &_trInfo.dSecs );
				pPSRE->get_TimeoutPenlift( &_trInfo.lTicks );
				pPSRE->get_TimeoutLatency( &_ClockSecsMax );
				_ClockSecsMax *= 1000;
				pPSRE->get_MaxRecArea( &_fMaximize );
				pPSRE->get_FullScreen( &_fFullScreen );
				pPSRE->Release();
			}

			// ensure not trying real-size if mapped to recording window
			if( ::IsDesktopModeOn() && ( _fMaximize != WINDOW_ASIS ) ) _fMaximize = TRUE;

			TrialsMode tm;
			pPS->get_TrialMode( &tm );
			_trInfo.nTrialMode = tm;
			::SetContinueWithEnter( ( tm == eTM_Delay ) || ( tm == eTM_DelayAD ) ||
									( tm == eTM_DelayADErr ) || ( tm == eTM_DelayErr ) ||
									( tm == eTM_FirstDelay ) || ( tm == eTM_FirstDelayAD ) ||
									( tm == eTM_FirstDelayADErr ) || ( tm == eTM_FirstDelayErr ) );

			// get appropriate interface
			IProcSetFlags* pPSF = NULL;
			hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
			if( SUCCEEDED( hr ) )
			{
				// TF parameters
				pPS->get_FilterFrequency( &dFF );
				pPS->get_RotationBeta( &dBeta );
				pPS->get_Decimate( &nDec );
				pPSF->get_TF_A( &fTFA );
				pPSF->get_TF_J( &fTFJ );
				pPSF->get_TF_R( &fTFR );
				pPSF->get_TF_U( &fTFU );
				pPSF->get_TF_O( &fTFO );
				// Seg parameters
				pPSF->get_SEG_F( &fSegF );
				pPSF->get_SEG_L( &fSegL );
				pPSF->get_SEG_O( &fSegO );
				pPSF->get_SEG_S( &fSegS );
				pPSF->get_SEG_M( &fSegM );
				pPSF->get_SEG_A( &fSegA );
				pPSF->get_SEG_V( &fSegV );
				pPSF->get_SEG_MM( &fSegMM );
				pPSF->get_SEG_J( &fSegJ );
				// Extract parameters
				pPSF->get_EXT_S( &fExtS );
				pPSF->get_EXT_3( &fExt3 );
				pPSF->get_EXT_O( &fExtO );
				pPSF->get_EXT_2( &fExt2 );
				// Consis parameters
				pPSF->get_CON_A( &fConA );
				pPSF->get_CON_C( &fConC );
				pPSF->get_CON_M( &fConM );
				pPSF->get_CON_N( &fConN );
				pPSF->get_CON_S( &fConS );
				pPSF->get_CON_U( &fConU );
				pPSF->get_CON_O( &fConO );
				pPSF->get_CON_D( &fConD );
				pPSF->Release();
			}
		}

		// update recording window settings
		pGV->SetRecordingSettings( nRate, nPressure, dRes );
		pGV->SetTimes( _trInfo.dStart, _trInfo.dSecs, _trInfo.lTicks );
		pGV->SetPrecueTimes( _trInfo.dPDuration, _trInfo.dPLatency );
		pGV->SetWarningTimes( _trInfo.dWDuration, _trInfo.dWLatency );

		// message of redoing trial
		szTemp.Format( IDS_AL_RERUNTRIAL, szTrial );
		OutputAMessage( szTemp, TRUE );

		// Begin recording
		pLV->SetActionState( ACTION_RUNEXPERIMENT );
		pRV->UpdateInstruction( szInstr );
		szTemp.Format( IDS_AL_INSTR, szInstr );
		OutputAMessage( szTemp, TRUE );

		// if gripper experiment, we need to reset the device
		if( ::IsGripperModeOn() ) pGV->Reset();

		// hide start page if shown
		pLV->HideStartPage();

		// size windows
		WINDOWPLACEMENT wp;
		memset( &wp, 0, sizeof( WINDOWPLACEMENT ) );
		GetWindowPlacement( &wp );
		_fWasMaximized = ( wp.showCmd == SW_MAXIMIZE );
		_OldTWidth = GetTreeSize();
		_OldTHeight = GetTreeHeight();
		_OldRHeight = GetRecSize();
		ResetSizes( FALSE );

		// Experiment instructions
		InstructionDlg id( _fFullScreen ? CWnd::FromHandle( ::GetDesktopWindow() ) : ::AfxGetApp()->m_pMainWnd );
		id.m_fReadOnly = TRUE;
		id.m_szExpID = szExp;
		id.m_szFileName = szExp;
		if( id.DoModal() != IDOK )
		{
			_trInfo.pPS = NULL;
			pPS->Release();
			ResetSizes();
			pLV->SetActionState( ACTION_NONE );
			return;
		}
		// condition instruction?
		if( tm != eTM_None )
		{
			id.m_fReadOnly = TRUE;
			id.m_szExpID = szExp;
			id.m_szFileName = szExp;
			id.m_szFileName += szCondID;
			id.m_fCond = TRUE;
			id.DoModal(); 
		}

		CFileFind ff;
		CString szTmp = szRaw;
		szTmp = szTmp.Left( szRaw.GetLength() - 3 );
		szTmp += _T("TF");
		if( ff.FindFile( szTmp ) ) REMOVE_FILE( szTmp );
		szTmp = szTmp.Left( szRaw.GetLength() - 2 );
		szTmp += _T("SEG");
		if( ff.FindFile( szTmp ) ) REMOVE_FILE( szTmp );

		_trInfo.szExpID = szExp;
		_trInfo.szGrpID = szGrp;
		_trInfo.szSubjID = szSubj;
		_trInfo.szCurCond = szCondID;
		_trInfo.szCurCond = szCondID;
		_trInfo.pPS = pPS;

		Record( szRaw, TRUE, szWStimID, szPStimID, szStimID, FALSE, FALSE );
	}
	else if( hTrial )
	{
		// cleanup of experiment settings object
		if( _trInfo.pPS )
		{
			_trInfo.pPS->Release();
			_trInfo.pPS = NULL;
		}

		// verify that there is a raw data file
		CFileFind ff;
		if( !ff.FindFile( szRaw ) ) return;

		// left view tree object
		CTreeCtrl& tree = pLV->GetTreeCtrl();

		// Get exp, grp, subj, cond from raw file name
		CString szExpID, szGrpID, szSubjID;
		int nPos = szRaw.ReverseFind( '\\' );
		if( nPos == -1 ) return;
		szTemp = szRaw.Mid( nPos + 1, 14 );
		szTrial = szRaw.Mid( nPos + 1 );
		// Extract experiment
		szExpID = szTemp.Left( 3 );
		szTemp = szTemp.Mid( 3 );
		// Extract group
		szGrpID = szTemp.Left( 3 );
		szTemp = szTemp.Mid( 3 );
		// Extract subject
		szSubjID = szTemp.Left( 3 );

		// samp rate & device res
		double dDevRes = 0.;
		short nSampRate = 0;
		HRESULT hr = pLV->m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
										szSubjID.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			pLV->m_pEML->get_DeviceRes( &dDevRes );
			pLV->m_pEML->get_SamplingRate( &nSampRate );
		}

		// subject htree item
		hTrial = tree.GetParentItem( hTrial );	// trial folder
		hTrial = tree.GetParentItem( hTrial );	// subject

		// process trial
		if( _trInfo.fProcess ) {
			BOOL fWasVerbose = ::GetVerbosity();
			BOOL fWasDetailed = ::GetDetailedOutput();
			::SetVerbosity( TRUE );
			::SetDetailedOutput( TRUE );
			ReprocessTrial( szExpID, szSubjID, szGrpID, szTrial, dDevRes,
							nSampRate, hTrial, TRUE );
			::SetVerbosity( fWasVerbose );
			::SetDetailedOutput( fWasDetailed );
		}
	}
}

void CMainFrame::OnViewQuest() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	CString szData;
	IMQuestionnaire* pQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
								   IID_IMQuestionnaire, (LPVOID*)&pQ );

	if( SUCCEEDED( hr ) )
	{
		szData.LoadString( IDS_AL_QUESTMASTER );
		LogToFile( szData, LOG_UI );

		QuestionnaireFullDlg d( pQ, TRUE, this );
		if( d.DoModal() == IDOK )
		{
			::DataHasChanged();

			POSITION posH = d.m_lstH->GetHeadPosition();
			POSITION posP = d.m_lstP->GetHeadPosition();
			POSITION posQ = d.m_lstQ->GetHeadPosition();
			POSITION posID = d.m_lstID->GetHeadPosition();
			POSITION posS = d.m_lstS->GetHeadPosition();
			POSITION posI = d.m_lstI->GetHeadPosition();
			POSITION posN = d.m_pListN->GetHeadPosition();
			POSITION posCH = d.m_pListCH->GetHeadPosition();
			CString szH, szQ, szID, szI, szS, szP, szN, szCH;
			BOOL fHdr = FALSE, fPrv = FALSE, fNum = FALSE;
			int nOrigItem = 0;
			int nPos = 1;

			while( posID )
			{
				szID = d.m_lstID->GetNext( posID );
				if( ( szID != _T("") ) && ( szID.GetLength() == 3 ) )
				{
					szH = d.m_lstH->GetNext( posH );
					szP = d.m_lstP->GetNext( posP );
					szQ = d.m_lstQ->GetNext( posQ );
					szS = d.m_lstS->GetNext( posS );
					szI = d.m_lstI->GetNext( posI );
					szN = d.m_pListN->GetNext( posN );
					szCH = d.m_pListCH->GetNext( posCH );
					fHdr = szH != _T("");
					fPrv = szP != _T("");
					fNum = szN != _T("");
					nOrigItem = atoi( szI );

					if( szS == Q_STATE_DEL )
					{
						hr = pQ->Find( szID.AllocSysString() );
						if( SUCCEEDED( hr ) )
						{
							hr = pQ->Remove();
							if( FAILED( hr ) )
							{
								BCGPMessageBox( IDS_DEL_ERR );
								break;
							}
							else	// remove any instances in subject/group questionnaires
							{
								// subject questionnaire
								// 17Oct07 - GMB - we're no longer deleting from subject quest
								/*
								ISQuestionnaire* pSQ = NULL;
								hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
													IID_ISQuestionnaire, (LPVOID*)&pSQ );
								if( SUCCEEDED( hr ) )
								{
									hr = pSQ->RemoveQuestion( szID.AllocSysString() );
									pSQ->Release();
								}
								else
								{
									szData.Format( IDS_QUEST_ERR, hr );
									BCGPMessageBox( szData );
								}
								*/

								// group questionnaire
								IGQuestionnaire* pQ = NULL;
								hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
													IID_IGQuestionnaire, (LPVOID*)&pQ );
								if( SUCCEEDED( hr ) )
								{
									hr = pQ->RemoveQuestion( szID.AllocSysString() );
									pQ->Release();
								}
								else
								{
									szData.Format( IDS_QUEST_ERR, hr );
									BCGPMessageBox( szData );
								}
							}
						}
					}
					else if( szS == Q_STATE_NEW )
					{
						pQ->put_ID( szID.AllocSysString() );
						pQ->put_ItemNum( (short)nPos );
						pQ->put_IsHeader( fHdr );
						pQ->put_IsPrivate( fPrv );
						pQ->put_Question( szQ.AllocSysString() );
						pQ->put_IsNumeric( fNum );
						pQ->put_ColumnHeader( szCH.AllocSysString() );
						hr = pQ->Add();
						if( FAILED( hr ) )
						{
							CString szTempMsg;
							szTempMsg.LoadString(IDS_ADD_ERR);
							BCGPMessageBox( szTempMsg+_T(" Processing::CMainFrame:1") );
							break;
						}

						nPos++;
					}
					else	// Mod anyway in case index changed (also check for add if update fails)
					{
						hr = pQ->Find( szID.AllocSysString() );
						if( SUCCEEDED( hr ) )
						{
							pQ->put_ID( szID.AllocSysString() );
							pQ->put_ItemNum( (short)nPos );
							pQ->put_IsHeader( fHdr );
							pQ->put_IsPrivate( fPrv );
							pQ->put_Question( szQ.AllocSysString() );
							pQ->put_IsNumeric( fNum );
							pQ->put_ColumnHeader( szCH.AllocSysString() );
							hr = pQ->Modify();
							if( FAILED( hr ) )
							{
								BCGPMessageBox( IDS_EDIT_ERR );
								break;
							}
						}
						else
						{
							pQ->put_ID( szID.AllocSysString() );
							pQ->put_ItemNum( (short)nPos );
							pQ->put_IsHeader( fHdr );
							pQ->put_IsPrivate( fPrv );
							pQ->put_Question( szQ.AllocSysString() );
							pQ->put_IsNumeric( fNum );
							pQ->put_ColumnHeader( szCH.AllocSysString() );
							hr = pQ->Add();
							if( FAILED( hr ) )
							{
								CString szTempMsg;
								szTempMsg.LoadString(IDS_ADD_ERR);
								BCGPMessageBox( szTempMsg+_T(" Processing::CMainFrame::2") );
								break;
							}
						}

						nPos++;
					}
				}
			}

			// repair questionnaire for item nums just in case
			{
			MQuestionnaires q;
			q.Repair();
			}
		}

		pQ->Release();
	}
	else
	{
		szData.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szData );
	}
}

BOOL CMainFrame::GetSoundInfo( UINT nSound, CString szCondID, BOOL& fTone, int& nFreq,
							   int& nDur, CString& szFile, BOOL& fTrigger )
{
	// Create condition object
	INSCondition* pCond = NULL;
	HRESULT hr = CoCreateInstance( CLSID_NSCondition, NULL, CLSCTX_ALL,
								   IID_INSCondition, (LPVOID*)&pCond );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_COND_ERR );
		return FALSE;
	}

	pCond->Find( szCondID.AllocSysString() );

	// get appropriate interface
	INSConditionSound* pCondS = NULL;
	hr = pCond->QueryInterface( IID_INSConditionSound, (LPVOID*)&pCondS );
	if( FAILED( hr ) ) { pCond->Release(); return FALSE; }

	// get values
	short nVal = 0;
	BSTR bstr = NULL;
	BOOL fUse = FALSE;

	VARIANT v;
	pCondS->get_SoundInfo( eSC_Use, (eSoundType)( nSound + 1 ), &v );
	fUse = V_BOOL( &v );
	pCondS->get_SoundInfo( eSC_Tone, (eSoundType)( nSound + 1 ), &v );
	fTone = V_BOOL( &v );
	pCondS->get_SoundInfo( eSC_Frequency, (eSoundType)( nSound + 1 ), &v );
	nFreq = V_I2( &v );
	pCondS->get_SoundInfo( eSC_Duration, (eSoundType)( nSound + 1 ), &v );
	nDur = V_I2( &v );
	pCondS->get_SoundInfo( eSC_Media, (eSoundType)( nSound + 1 ), &v );
	szFile = V_BSTR( &v );
	pCondS->get_SoundInfo( eSC_Trigger, (eSoundType)( nSound + 1 ), &v );
	fTrigger = V_BOOL( &v );

	pCondS->Release();
	pCond->Release();

	return fUse;
}

void CMainFrame::TestTrialLinearity( CString szExp, CString szSubj, CString szGrp, CString szTrial )
{
	// Ensure experiment is handwriting
	{
	Experiments exp;
	if( FAILED( exp.Find( szExp ) ) ) return;
	short nType = 0;
	exp.GetType( nType );
	if( nType != EXP_TYPE_HANDWRITING )
	{
		BCGPMessageBox( _T("Linearity tests can only be run on handwriting experiment trials.") );
		return;
	}
	}

	// create full file names (hwr & unr)
	CString szFile, szFile2;
	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );
	szFile2 = szFile.Left( szFile.GetLength() - 3 );
	szFile2 += _T("unr");

	// rapid view is turned off for unrotate...get current setting so we can put it back after
	BOOL fRV = !::GetDetailedOutput();

	// get input device settings for this experiment
	{
		IProcSetting* pPS = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									   IID_IProcSetting, (LPVOID*)&pPS );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_PS_ERR );
			return;
		}
		// if not found, use default
		pPS->Find( szExp.AllocSysString() );
		short nVal = 0;
		double dVal = 0.;
		pPS->get_MinPenPressure( &nVal );
		::SetMinPenPressure( nVal );
		pPS->get_SamplingRate( &nVal );
		::SetSamplingRate( nVal );
		pPS->get_DeviceResolution( &dVal );
		::SetDeviceResolution( dVal );
		pPS->Release();
	}

	// Run linearity test
	BOOL fVelocity = FALSE;
	BOOL fDiff = TRUE;
	double dRotBeta = 0.;
	// Unrotate
	IProcess* pProc = Experiments::GetProcessObject( GetMsgPaneList() );
	if( pProc ) pProc->SetRapidView( FALSE );
	Experiments::ProcessUnrot( szFile, szFile2, fVelocity, fDiff, dRotBeta, GetMsgPaneList() );
	if( pProc ) pProc->SetRapidView( fRV );
	// Chart the test
	CFileFind ff;
	if( ff.FindFile( szFile ) ) ChartHwr( szFile, 100.0, FALSE );
	// Remove unr file
	if( ff.FindFile( szFile2 ) ) REMOVE_FILE( szFile2 );
}

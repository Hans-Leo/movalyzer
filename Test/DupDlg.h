#pragma once

#include "..\NSSharedGUI\NSTooltipCtrl.h"

// DupDlg dialog

class DupDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(DupDlg)

public:
	DupDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DupDlg();

// Dialog Data
	enum { IDD = IDD_DUP };
	CString			m_szID;
	int				m_nCount;
	CString			m_szDesc;
	BOOL			m_fWords;
	CString			m_szParent;
	int				m_nWord;
	CBCGPComboBox	m_cboParent;
	CStringList		m_lstCond;
	NSToolTipCtrl	m_tooltip;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	afx_msg void OnBnClickedChkWords();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

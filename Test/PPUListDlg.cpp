// PPUListDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "PPUListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PPUListDlg dialog

BEGIN_MESSAGE_MAP(PPUListDlg, CBCGPDialog)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
END_MESSAGE_MAP()

PPUListDlg::PPUListDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(PPUListDlg::IDD, pParent)
{
}

void PPUListDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
}

/////////////////////////////////////////////////////////////////////////////
// PPUListDlg message handlers

void PPUListDlg::OnDblclkList() 
{
	OnOK();
}

BOOL PPUListDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// since we're using an existing resource for moving a subject to another group
	// we need to alter the title
	SetWindowText( _T("Pay-Per-Use Item For This Session") );

	// fill list
	long nSPPU = 0, nGPPU = 0, nOPPU = 0;
	BOOL fSPPU = ::IsSPPU( nSPPU );
	BOOL fGPPU = ::IsGPPU( nGPPU );
	BOOL fOPPU = ::IsOPPU( nOPPU );
	CString szTmp;
	if( fSPPU )
	{
		szTmp.Format( _T("ScriptAlyzeR - %u trials remaining"), nSPPU );
		m_list.AddString( szTmp );
	}
	if( fGPPU )
	{
		szTmp.Format( _T("GripAlyzeR - %u trials remaining"), nGPPU );
		m_list.AddString( szTmp );
	}
	if( fOPPU )
	{
		szTmp.Format( _T("MovAlyzeR - %u trials remaining"), nOPPU );
		m_list.AddString( szTmp );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PPUListDlg::OnOK()
{
	int nPos = m_list.GetCurSel();
	if( nPos == LB_ERR )
	{
		BCGPMessageBox( _T("No item has been selected.") );
		return;
	}

	// get which item is selected
	// disable the others
	CString szItem;
	m_list.GetText( nPos, szItem );
	szItem.MakeUpper();
	BOOL fScript = ( szItem.Find( _T("SCRIP") ) >= 0 );
	BOOL fGrip = ( szItem.Find( _T("GRIP") ) >= 0 );
	BOOL fMov = ( szItem.Find( _T("MOV") ) >= 0 );
	if( fScript )
	{
		::SetIsGPPU( FALSE );
		::SetIsOPPU( FALSE );
	}
	if( fGrip )
	{
		::SetIsSPPU( FALSE );
		::SetIsOPPU( FALSE );
	}
	if( fMov )
	{
		::SetIsSPPU( FALSE );
		::SetIsGPPU( FALSE );
	}

	CBCGPDialog::OnOK();
}

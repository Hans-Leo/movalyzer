// WizardRunExpGrp.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardRunExpGrp.h"
#include "WizardRunExpSheet.h"
#include "..\DataMod\DataMod.h"
#include "AddGroupDlg.h"
#include "WizardRunExpExp.h"
#include "WizardRunExpSubj.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "Groups.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpGrp property page

IMPLEMENT_DYNCREATE(WizardRunExpGrp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardRunExpGrp, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_GRPS, OnSelchangeCboGrps)
	ON_BN_CLICKED(IDC_BN_CREATEGRP, OnBnCreategrp)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardRunExpGrp::WizardRunExpGrp() : CBCGPPropertyPage(WizardRunExpGrp::IDD)
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_szGrp = _T("");
	m_fInit = FALSE;
}

WizardRunExpGrp::~WizardRunExpGrp()
{
}

void WizardRunExpGrp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_GRPS, m_Cbo);
	DDX_CBString(pDX, IDC_CBO_GRPS, m_szGrp);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szDesc);
}

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpGrp message handlers

void WizardRunExpGrp::OnSelchangeCboGrps() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szDesc;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	Groups grp;
	if( !grp.IsValid() ) return;
	if( FAILED( grp.Find( m_szID ) ) ) return;

	grp.GetNotes( m_szNotes );

	m_szGrp = szItem;
	UpdateData( FALSE );

	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

void WizardRunExpGrp::OnBnCreategrp() 
{
	AddGroupDlg d( &m_existing, this, TRUE );
	if( d.DoModal() )
	{
		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( pos )
		{
			CString szItem = d.m_lstNew.GetNext( pos );
			CString szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );
			int nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				m_szID = szItem.Mid( nPos + 2 );
				m_szDesc = szItem.Left( nPos - 1 );
				UpdateData( FALSE );

				szItem.Format( _T("%s%s%s"), m_szDesc, szDelim, m_szID );
				int nSel = m_Cbo.AddString( szItem );
				m_Cbo.SetCurSel( nSel );
				m_existing.AddTail( m_szID );

				WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
				pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
			}
		}
	}
}

BOOL WizardRunExpGrp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_EXP_RUN );
	CWnd* pWnd = GetDlgItem( IDC_CBO_GRPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing group for this experiment."), _T("Existing Group") );
	pWnd = GetDlgItem( IDC_BN_CREATEGRP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new group to use for this experiment."), _T("New Group") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardRunExpGrp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardRunExpGrp::OnSetActive() 
{
	CString szID, szDesc, szItem;
	HRESULT hr2 = E_FAIL;

	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	WizardRunExpExp* pExp = (WizardRunExpExp*)pParent->GetPage( 1 );
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CLeftView* pLV = pMF->GetLeftView();
	CString szExpID = pExp->m_szID;
	if( szExpID != m_szExpID )
	{
		m_Cbo.ResetContent();
		m_szExpID = szExpID;
		m_szID.Empty();
		m_szDesc.Empty();
		m_szGrp.Empty();
	}
	else return CBCGPPropertyPage::OnSetActive();

	// was there a last experiment run
	CString szExp, szGrp, szSubj;
	::GetLastExperimentInfo( szExp, szGrp, szSubj );

	// Fill group list (for selected experiment)
	Groups grp;
	if( !grp.IsValid() ) return TRUE;
	int nSel = -1;
	m_existing.RemoveAll();
	HRESULT hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetID( szID );

		if( !pExp->m_fNew )
		{
			hr2 = pLV->m_pEML->FindForGroup( szExpID.AllocSysString(),
											 szID.AllocSysString() );
		}
		else hr2 = S_OK;
		if( SUCCEEDED( hr2 ) )
		{
			grp.GetDescriptionWithID( szItem );

			if( szID == szGrp ) nSel = m_Cbo.AddString( szItem );
			else m_Cbo.AddString( szItem );

			m_existing.AddTail( szID );
		}

		hr = grp.GetNext();
	}

	// if not already checked
	if( !m_fInit && ( nSel != -1 ) )
	{
		m_Cbo.SetCurSel( nSel );
		OnSelchangeCboGrps();
		m_fInit = TRUE;
	}

	if( m_szID != _T("") ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

LRESULT WizardRunExpGrp::OnWizardBack() 
{
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

LRESULT WizardRunExpGrp::OnWizardNext() 
{
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	WizardRunExpSubj* pSubj = (WizardRunExpSubj*)pParent->GetPage( 3 );
	if( pSubj->m_szID != _T("") )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardRunExpGrp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("runexperiment_wizard.html"), this );
	return TRUE;
}

BOOL WizardRunExpGrp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

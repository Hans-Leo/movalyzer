// WizardImportGen.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportGen.h"
#include "WizardImportSheet.h"
#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\NSShared\GenSig.h"
#include "WizardImportGenSheet.h"
#include "WizardImportExp.h"
#include "..\DataMOD\DataMod.h"
#include "GraphDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportGen property page

IMPLEMENT_DYNCREATE(WizardImportGen, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportGen, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_SETTINGS, OnBnSettings)
	ON_BN_CLICKED(IDC_BN_VIEW, OnBnPreview)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportGen::WizardImportGen() : CBCGPPropertyPage(WizardImportGen::IDD)
{
	m_szType = _T("");
	m_szProfile = _T("");
	m_szNoise = _T("");
	m_szRound = _T("");
	m_szXStart = _T("");
	m_szXDelta = _T("");
	m_szXPhase = _T("");
	m_szXVel = _T("");
	m_szYStart = _T("");
	m_szYDelta = _T("");
	m_szYPhase = _T("");
	m_szStart = _T("");
	m_szStrokes = _T("");
	m_szTrail = _T("");
	m_szDelta = _T("");
	m_fSelected = FALSE;
	m_szSM = _T("");
	m_szAmount = _T("");
	m_szDur = _T("");
}

WizardImportGen::~WizardImportGen()
{
	// cleanup temporary file if exists
	CString szFile;
	::GetDataPathRoot( szFile );
	if( szFile == _T("") ) szFile = _T("gentest.hwr");
	else szFile += _T("\\gentest.hwr");
	CFileFind ff;
	if( ff.FindFile( szFile ) ) REMOVE_FILE( szFile );
}

void WizardImportGen::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_TYPE, m_szType);
	DDX_Text(pDX, IDC_TXT_PROFILE, m_szProfile);
	DDX_Text(pDX, IDC_TXT_NOISE, m_szNoise);
	DDX_Text(pDX, IDC_TXT_ROUND, m_szRound);
	DDX_Text(pDX, IDC_TXT_XSTARTT, m_szXStart);
	DDX_Text(pDX, IDC_TXT_XDELTAT, m_szXDelta);
	DDX_Text(pDX, IDC_TXT_XPHASE, m_szXPhase);
	DDX_Text(pDX, IDC_TXT_XVEL, m_szXVel);
	DDX_Text(pDX, IDC_TXT_YSTARTT, m_szYStart);
	DDX_Text(pDX, IDC_TXT_YDELTAT, m_szYDelta);
	DDX_Text(pDX, IDC_TXT_YPHASE, m_szYPhase);
	DDX_Text(pDX, IDC_TXT_STARTT, m_szStart);
	DDX_Text(pDX, IDC_TXT_STROKES, m_szStrokes);
	DDX_Text(pDX, IDC_TXT_TRAIL, m_szTrail);
	DDX_Text(pDX, IDC_TXT_DELTAT, m_szDelta);
	DDX_Text(pDX, IDC_TXT_SM, m_szSM);
	DDX_Text(pDX, IDC_TXT_AMOUNT, m_szAmount);
	DDX_Text(pDX, IDC_TXT_SM_DURATION, m_szDur);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportGen message handlers

void WizardImportGen::OnBnSettings() 
{
	if( m_dlgSettings.DoModal() != IDOK ) return;

	UpdateValues();
}

void WizardImportGen::OnBnPreview()
{
	GenSig gs;
	gs.m_cm = m_dlgSettings.m_cm;
	gs.m_deltat = m_dlgSettings.m_deltat;
	gs.m_deltax = m_dlgSettings.m_deltax;
	gs.m_deltay = m_dlgSettings.m_deltay;
	gs.m_integer = m_dlgSettings.m_fInt;
	gs.m_noise = m_dlgSettings.m_noise;
	gs.m_nstroke = m_dlgSettings.m_nstrokes;
	gs.m_pattern = m_dlgSettings.m_pattern;
	gs.m_phasex = m_dlgSettings.m_phasex;
	gs.m_phasey = m_dlgSettings.m_phasey;
	gs.m_prsmin = m_dlgSettings.m_prsmin;
	gs.m_sec = m_dlgSettings.m_sec;
	gs.m_startt = m_dlgSettings.m_startt;
	gs.m_startx = m_dlgSettings.m_startx;
	gs.m_starty = m_dlgSettings.m_starty;
	gs.m_trailtime = m_dlgSettings.m_trailtime;
	gs.m_xspeed = m_dlgSettings.m_xspeed;
	switch( m_dlgSettings.m_nSubMvmtModel )
	{
		case SM_GEN_NONE:
			gs.m_nSubMvmtModel = GEN_NONE;
			gs.m_subduration = 0.;
			gs.m_subdelay = 0.;
			break;
		case SM_GEN_DELAY:
			gs.m_nSubMvmtModel = GEN_DELAY;
			gs.m_subduration = m_dlgSettings.m_dur;
			gs.m_subdelay = m_dlgSettings.m_delay;
			break;
		case SM_GEN_WARP:
			gs.m_nSubMvmtModel = GEN_WARP;
			gs.m_subduration = m_dlgSettings.m_dur;
			gs.m_subdelay = m_dlgSettings.m_warp;
			break;
	}

	// Get settings from experiment
	WizardImportGenSheet* pParent = (WizardImportGenSheet*)GetParent();
	WizardImportExp* pgExp = pParent ? (WizardImportExp*)pParent->GetPage( 2 ) : NULL;
	if( !pgExp ) return;
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( pgExp->m_szID.AllocSysString() );
	short nVal = 0;
	pPS->get_DeviceResolution( &gs.m_cm );
	pPS->get_MinPenPressure( &nVal );
	gs.m_prsmin = nVal;
	pPS->get_SamplingRate( &nVal );
	gs.m_sec = nVal;
	if( gs.m_sec != 0. ) gs.m_sec = 1. / gs.m_sec;
	pPS->Release();

	// Temp trial
	CString szFile;
	::GetDataPathRoot( szFile );
	if( szFile == _T("") ) szFile = _T("gentest.hwr");
	else szFile += _T("\\gentest.hwr");

	// Generate
	if( gs.Generate( szFile ) )
	{
		WindowManager* pWM = ::GetWindowManager();
		CGraphDlg* gd = new CGraphDlg( gs.m_sec, (int)gs.m_prsmin, NULL, szFile, CHART_HWR,
			FALSE, _T(""), 0, 0, AfxGetMainWnd(), FALSE, ::GetSoftColor(), TRUE,
			FALSE, NULL, NULL, pWM );

		// add to window manager
		if( pWM ) pWM->AddItem( gd );
	}
	else BCGPMessageBox( _T("Error in data generation.") );
}

void WizardImportGen::UpdateValues() 
{
	switch( m_dlgSettings.m_nMode )
	{
		case GS_LINEAR: m_szType = _T("Diagonal Line"); break;
		case GS_ONESTROKE: m_szType = _T("One Stroke"); break;
		case GS_4LOOPS: m_szType = _T("Four Loops"); break;
		case GS_8LOOPS: m_szType = _T("Eight Loops"); break;
		case GS_CUSTOM: m_szType = _T("Custom"); break;
	}
	if( m_dlgSettings.m_pattern == 1 ) m_szProfile = _T("Harmonic");
	else m_szProfile = _T("Jerk");

	m_szNoise.Format( _T("%.2f"), m_dlgSettings.m_noise );
	m_szRound = m_dlgSettings.m_fInt ? _T("Yes") : _T("No");
	m_szStart.Format( _T("%.2f"), m_dlgSettings.m_startt );
	m_szDelta.Format( _T("%.2f"), m_dlgSettings.m_deltat );
	m_szStrokes.Format( _T("%.2f"), m_dlgSettings.m_nstrokes );
	m_szTrail.Format( _T("%.2f"), m_dlgSettings.m_trailtime );
	m_szXStart.Format( _T("%.2f"), m_dlgSettings.m_startx );
	m_szXDelta.Format( _T("%.2f"), m_dlgSettings.m_deltax );
	m_szXPhase.Format( _T("%.2f"), m_dlgSettings.m_phasex );
	m_szXVel.Format( _T("%.2f"), m_dlgSettings.m_xspeed );
	m_szYStart.Format( _T("%.2f"), m_dlgSettings.m_starty );
	m_szYDelta.Format( _T("%.2f"), m_dlgSettings.m_deltay );
	m_szYPhase.Format( _T("%.2f"), m_dlgSettings.m_phasey );
	switch( m_dlgSettings.m_nSubMvmtModel )
	{
		case SM_GEN_NONE:
			m_szSM = _T("None");
			m_szAmount = _T("0.00");
			m_szDur = _T("0.00");
			break;
		case SM_GEN_DELAY:
			m_szSM = _T("Delay");
			m_szAmount.Format( _T("%.2f"), m_dlgSettings.m_delay );
			m_szDur.Format( _T("%.2f"), m_dlgSettings.m_dur );
			break;
		case SM_GEN_WARP:
			m_szSM = _T("Time Warp");
			m_szAmount.Format( _T("%.2f"), m_dlgSettings.m_warp );
			m_szDur.Format( _T("%.2f"), m_dlgSettings.m_dur );
			break;
	}

	UpdateData( FALSE );

	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	m_fSelected = TRUE;
}

BOOL WizardImportGen::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_BN_SETTINGS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to define the settings.") );
	pWnd = GetDlgItem( IDC_BN_VIEW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to preview the results.") );

	UpdateValues();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportGen::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardImportGen::OnWizardBack() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

LRESULT WizardImportGen::OnWizardNext() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();

	if( !m_fSelected )
	{
		BCGPMessageBox( _T("You must select your generation settings first.") );
		CWnd* pWnd = GetDlgItem( IDC_BN_SETTINGS );
		if( pWnd ) pWnd->SetFocus();
		return -1;
	}

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportGen::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportGen::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

// SigGenSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "SigGenSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// made globalized for retention of settings during given session of app;
static BOOL		_fInt = TRUE;
static double	_cm = 0.001;
static double	_sec = 0.01;
static double	_prsmin = 20.0;
static double	_noise = 0.02;
static double	_startt = 0.1;
static double	_deltat = 0.2;
static double	_nstrokes = 16.8;
static double	_trailtime = 0.1;
static double	_startx = 0.1;
static double	_deltax = 1.0;
static double	_phasex = 60.0;
static double	_xspeed = 2.0;
static double	_starty = 0.3;
static double	_deltay = 1.0;
static double	_phasey = 0.0;
static int		_pattern = 1;
static UINT		_nMode = GS_8LOOPS;
static UINT		_nSubMvmtModel = SM_GEN_DELAY;
static double	_delay = 0.05;
static double	_warp = 1.0;
static double	_dur = 0.16;

/////////////////////////////////////////////////////////////////////////////
// SigGenSettingsDlg dialog

BEGIN_MESSAGE_MAP(SigGenSettingsDlg, CBCGPDialog)
	ON_CBN_SELCHANGE(IDC_CBO_PRE, OnSelchangeCboPre)
	ON_BN_CLICKED(IDC_RDO_HARMONIC, OnRdoHarmonic)
	ON_BN_CLICKED(IDC_RDO_JERK, OnRdoJerk)
	ON_BN_CLICKED(IDC_RDO_SM_NONE, OnRdoSM)
	ON_BN_CLICKED(IDC_RDO_SM_DELAY, OnRdoSM)
	ON_BN_CLICKED(IDC_RDO_SM_WARP, OnRdoSM)
END_MESSAGE_MAP()

SigGenSettingsDlg::SigGenSettingsDlg( UINT nMode, CWnd* pParent )
	: CBCGPDialog( SigGenSettingsDlg::IDD, pParent ), m_nMode( nMode )
{
	m_fInt = _fInt;
	m_cm = _cm;
	m_sec = _sec;
	m_prsmin = _prsmin;
	m_noise = _noise;
	m_startt = _startt;
	m_deltat = _deltat;
	m_nstrokes = _nstrokes;
	m_trailtime = _trailtime;
	m_startx = _startx;
	m_deltax = _deltax;
	m_phasex = _phasex;
	m_xspeed = _xspeed;
	m_starty = _starty;
	m_deltay = _deltay;
	m_phasey = _phasey;
	m_pattern = _pattern;
	m_nSubMvmtModel = _nSubMvmtModel;
	m_delay = _delay;
	m_warp = _warp;
	m_dur = _dur;

	m_cm = ::GetDeviceResolution();
	m_sec = ::GetSamplingRate();
	if( m_sec != 0. ) m_sec = 1. / m_sec;
	m_prsmin = ::GetMinPenPressure();
}

SigGenSettingsDlg::SigGenSettingsDlg( CWnd* pParent )
	: CBCGPDialog(SigGenSettingsDlg::IDD, pParent)
{
	m_fInt = _fInt;
	m_cm = _cm;
	m_sec = _sec;
	m_prsmin = _prsmin;
	m_noise = _noise;
	m_startt = _startt;
	m_deltat = _deltat;
	m_nstrokes = _nstrokes;
	m_trailtime = _trailtime;
	m_startx = _startx;
	m_deltax = _deltax;
	m_phasex = _phasex;
	m_xspeed = _xspeed;
	m_starty = _starty;
	m_deltay = _deltay;
	m_phasey = _phasey;
	m_pattern = _pattern;
	m_nMode = _nMode;
	m_nSubMvmtModel = _nSubMvmtModel;
	m_delay = _delay;
	m_warp = _warp;
	m_dur = _dur;

	m_cm = ::GetDeviceResolution();
	m_sec = ::GetSamplingRate();
	if( m_sec != 0. ) m_sec = 1. / m_sec;
	m_prsmin = ::GetMinPenPressure();
}

void SigGenSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_PRE, m_cboPre);
	DDX_Check(pDX, IDC_CHK_INT, m_fInt);
	DDX_Text(pDX, IDC_EDIT_NOISE, m_noise);
	DDX_Text(pDX, IDC_EDIT_STARTT, m_startt);
	DDX_Text(pDX, IDC_EDIT_DELTAT, m_deltat);
	DDX_Text(pDX, IDC_EDIT_NSTROKES, m_nstrokes);
	DDX_Text(pDX, IDC_EDIT_TRAIL, m_trailtime);
	DDX_Text(pDX, IDC_EDIT_STARTX, m_startx);
	DDX_Text(pDX, IDC_EDIT_DELTAX, m_deltax);
	DDX_Text(pDX, IDC_EDIT_PHASEX, m_phasex);
	DDX_Text(pDX, IDC_EDIT_XSPEED, m_xspeed);
	DDX_Text(pDX, IDC_EDIT_STARTY, m_starty);
	DDX_Text(pDX, IDC_EDIT_DELTAY, m_deltay);
	DDX_Text(pDX, IDC_EDIT_PHASEY, m_phasey);
	DDX_Text(pDX, IDC_EDIT_SM_DELAY, m_delay);
	DDX_Text(pDX, IDC_EDIT_SM_WARP, m_warp);
	DDX_Text(pDX, IDC_EDIT_SM_DURATION, m_dur);
}

/////////////////////////////////////////////////////////////////////////////
// SigGenSettingsDlg message handlers

BOOL SigGenSettingsDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetTitle( _T("Signal Generation") );
	CWnd* pWnd = GetDlgItem( IDC_CBO_PRE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select predefined signal generators or make custom."), _T("Predefined") );
	pWnd = GetDlgItem( IDC_CHK_INT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Rounding raw data to integer digitizer data vs. floating point."), _T("Rounding") );
	pWnd = GetDlgItem( IDC_RDO_HARMONIC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Stroke velocity is harmonic."), _T("Harmonic") );
	pWnd = GetDlgItem( IDC_RDO_JERK );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Stroke velocity is minimum jerk."), _T("Minimum Jerk") );
	pWnd = GetDlgItem( IDC_EDIT_NOISE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Noise amplitude of x and y in cm."), _T("Noise Amplitude") );
	pWnd = GetDlgItem( IDC_EDIT_STARTT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Start time in sec of the movement from the start of the trial."), _T("Start Time") );
	pWnd = GetDlgItem( IDC_EDIT_DELTAT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Duration in sec of one stroke."), _T("Duration") );
	pWnd = GetDlgItem( IDC_EDIT_NSTROKES );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Number of Strokes. Can be decimal (eg 2.4 is one loop plus a small upstroke)."), _T("Number of Strokes") );
	pWnd = GetDlgItem( IDC_EDIT_TRAIL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Time in sec after the movement has stopped."), _T("Trail Time") );
	pWnd = GetDlgItem( IDC_EDIT_STARTX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Start x-position in cm of the first stroke."), _T("Starting Position") );
	pWnd = GetDlgItem( IDC_EDIT_DELTAX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("X-size of a stroke in cm."), _T("Stroke Size") );
	pWnd = GetDlgItem( IDC_EDIT_PHASEX );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Phase in deg of the x movement at the start of the movement."), _T("Starting Phase") );
	pWnd = GetDlgItem( IDC_EDIT_XSPEED );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Constant x-velocity in cm/sec added during the movement to produce a baseline in a sequence of loops."), _T("Velocity") );
	pWnd = GetDlgItem( IDC_EDIT_STARTY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Start y-position in cm of the first stroke."), _T("Starting Position") );
	pWnd = GetDlgItem( IDC_EDIT_DELTAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Y-size of a stroke in cm."), _T("Stroke Size") );
	pWnd = GetDlgItem( IDC_EDIT_PHASEY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Phase in deg of the y movement at the start of the movement."), _T("Starting Phase") );
	pWnd = GetDlgItem( IDC_RDO_SM_NONE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Do not generate submovements."), _T("None") );
	pWnd = GetDlgItem( IDC_RDO_SM_DELAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Generate submovements using the specified delay around the primary submovement."), _T("Delay") );
	pWnd = GetDlgItem( IDC_RDO_SM_WARP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Generate submovements using the specified time warping amplitude."), _T("Time Warping") );
	pWnd = GetDlgItem( IDC_EDIT_SM_DELAY );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the delay around the primary submovement for submovement generation. [DEFAULT=0.05]."), _T("Delay") );
	pWnd = GetDlgItem( IDC_EDIT_SM_WARP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the time warping amplitude for submovement generation. Zero (0) = no time warmping. More than one (1) = time warping reversal. [DEFAULT=1]."), _T("Time Warp") );
	pWnd = GetDlgItem( IDC_EDIT_SM_DURATION );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the duration of the primary submovement. [DEFAULT=0.16]"), _T("Duration") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV );

	// Fill predefined combo
	m_cboPre.AddString( _T("Custom") );
	m_cboPre.AddString( _T("One Stroke") );
	m_cboPre.AddString( _T("Two Loops") );
	m_cboPre.AddString( _T("Four Loops") );
	m_cboPre.AddString( _T("Eight Loops") );
	m_cboPre.AddString( _T("Diagonal Line") );
	m_cboPre.SetCurSel( m_nMode );

	if( m_pattern == 1 ) CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_HARMONIC );
	else CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_JERK );

	CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, m_nSubMvmtModel );

	OnSelchangeCboPre();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL SigGenSettingsDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void SigGenSettingsDlg::OnSelchangeCboPre() 
{
	m_nMode = m_cboPre.GetCurSel();
	if( m_nMode != CB_ERR )
	{
		UpdateData( TRUE );
		UpdateSettings();
		UpdateData( FALSE );
	}

	BOOL fEnable = ( m_nMode == GS_CUSTOM );
	int nRdo = GetCheckedRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK );
	BOOL fRdoHarmonic = ( nRdo == IDC_RDO_HARMONIC );
	nRdo = GetCheckedRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP );
	BOOL fRdoSMDelay = ( nRdo == IDC_RDO_SM_DELAY );
	BOOL fRdoSMWarp = ( nRdo == IDC_RDO_SM_WARP );

	CWnd* pWnd = GetDlgItem( IDC_CHK_INT );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_RDO_HARMONIC );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_RDO_JERK );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_NOISE );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_STARTT );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_DELTAT );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_NSTROKES );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoHarmonic );
	pWnd = GetDlgItem( IDC_EDIT_TRAIL );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_STARTX );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_DELTAX );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_PHASEX );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoHarmonic );
	pWnd = GetDlgItem( IDC_EDIT_XSPEED );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_STARTY );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_DELTAY );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_PHASEY );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoHarmonic );
	pWnd = GetDlgItem( IDC_RDO_SM_NONE );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_RDO_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_RDO_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoSMDelay );
	pWnd = GetDlgItem( IDC_EDIT_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoSMWarp );
	pWnd = GetDlgItem( IDC_EDIT_SM_DURATION );
	if( pWnd ) pWnd->EnableWindow( fEnable && ( fRdoSMWarp || fRdoSMDelay ) );
}

void SigGenSettingsDlg::OnRdoHarmonic() 
{
	CWnd* pWnd = GetDlgItem( IDC_EDIT_PHASEX );
	if( pWnd ) pWnd->EnableWindow( TRUE );
	pWnd = GetDlgItem( IDC_EDIT_PHASEY );
	if( pWnd ) pWnd->EnableWindow( TRUE );
	pWnd = GetDlgItem( IDC_EDIT_NSTROKES );
	if( pWnd ) pWnd->EnableWindow( TRUE );

	BOOL fEnable = ( m_nMode == GS_CUSTOM );
	int nRdo = GetCheckedRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP );
	BOOL fRdoSMDelay = ( nRdo == IDC_RDO_SM_DELAY );
	BOOL fRdoSMWarp = ( nRdo == IDC_RDO_SM_WARP );

	pWnd = GetDlgItem( IDC_RDO_SM_NONE );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_RDO_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_RDO_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( fEnable );
	pWnd = GetDlgItem( IDC_EDIT_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoSMDelay );
	pWnd = GetDlgItem( IDC_EDIT_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( fEnable && fRdoSMWarp );
	pWnd = GetDlgItem( IDC_EDIT_SM_DURATION );
	if( pWnd ) pWnd->EnableWindow( fEnable && ( fRdoSMWarp || fRdoSMDelay ) );
}

void SigGenSettingsDlg::OnRdoJerk() 
{
	CWnd* pWnd = GetDlgItem( IDC_EDIT_PHASEX );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_EDIT_PHASEY );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_EDIT_NSTROKES );
	if( pWnd ) pWnd->EnableWindow( FALSE );

	pWnd = GetDlgItem( IDC_RDO_SM_NONE );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_RDO_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_RDO_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_EDIT_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_EDIT_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	pWnd = GetDlgItem( IDC_EDIT_SM_DURATION );
	if( pWnd ) pWnd->EnableWindow( FALSE );
	CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, IDC_RDO_SM_NONE );
}

void SigGenSettingsDlg::OnOK()
{
	UpdateData( TRUE );

	if( !VerifySubMvmtValues() ) return;

	_fInt = m_fInt;
	_cm = m_cm;
	_sec = m_sec;
	_prsmin = m_prsmin;
	_noise = m_noise;
	_startt = m_startt;
	_deltat = m_deltat;
	_nstrokes = m_nstrokes;
	_trailtime = m_trailtime;
	_startx = m_startx;
	_deltax = m_deltax;
	_phasex = m_phasex;
	_xspeed = m_xspeed;
	_starty = m_starty;
	_deltay = m_deltay;
	_phasey = m_phasey;
	_nMode = m_nMode;
	_nSubMvmtModel = m_nSubMvmtModel;
	_delay = m_delay;
	_warp = m_warp;
	_dur = m_dur;

	int nPattern = GetCheckedRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK );
	if( nPattern == IDC_RDO_HARMONIC ) m_pattern = 1;
	else m_pattern = 2;
	_pattern = m_pattern;

	CBCGPDialog::OnOK();
}

void SigGenSettingsDlg::UpdateSettings()
{
	if( m_nMode == GS_ONESTROKE )
	{
		m_fInt = TRUE;
		m_noise = 0.0;
		m_startt = 0.1;
		m_deltat = 1.0;
		m_nstrokes = 1.0;
		m_trailtime = 0.1;
		m_startx = 0.3;
		m_deltax = 1.0;
		m_phasex = 0.0;
		m_xspeed = 2.0;
		m_starty = 0.0;
		m_deltay = 0.0;
		m_phasey = 0.0;
		m_pattern = 1;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_HARMONIC );
		m_nSubMvmtModel = SM_GEN_DELAY;
		m_delay = 0.05;
		m_warp = 1.;
		m_dur = 0.16;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, IDC_RDO_SM_DELAY );
	}
	else if( m_nMode == GS_2LOOPS )
	{
		m_fInt = TRUE;
		m_noise = 0.02;
		m_startt = 0.1;
		m_deltat = 0.2;
		m_nstrokes = 4.2;
		m_trailtime = 0.1;
		m_startx = 0.3;
		m_deltax = 1.0;
		m_phasex = 60.0;
		m_xspeed = 2.0;
		m_starty = 0.3;
		m_deltay = 2.0;
		m_phasey = 0.0;
		m_pattern = 1;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_HARMONIC );
		m_nSubMvmtModel = SM_GEN_DELAY;
		m_delay = 0.05;
		m_warp = 1.;
		m_dur = 0.16;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, IDC_RDO_SM_DELAY );
	}
	else if( m_nMode == GS_4LOOPS )
	{
		m_fInt = TRUE;
		m_noise = 0.02;
		m_startt = 0.1;
		m_deltat = 0.2;
		m_nstrokes = 8.4;
		m_trailtime = 0.1;
		m_startx = 0.3;
		m_deltax = 1.0;
		m_phasex = 60.0;
		m_xspeed = 2.0;
		m_starty = 0.3;
		m_deltay = 2.0;
		m_phasey = 0.0;
		m_pattern = 1;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_HARMONIC );
		m_nSubMvmtModel = SM_GEN_DELAY;
		m_delay = 0.05;
		m_warp = 1.;
		m_dur = 0.16;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, IDC_RDO_SM_DELAY );
	}
	else if( m_nMode == GS_8LOOPS )
	{
		m_fInt = TRUE;
		m_noise = 0.02;
		m_startt = 0.1;
		m_deltat = 0.2;
		m_nstrokes = 16.8;
		m_trailtime = 0.1;
		m_startx = 0.3;
		m_deltax = 1.0;
		m_phasex = 60.0;
		m_xspeed = 2.0;
		m_starty = 0.3;
		m_deltay = 2.0;
		m_phasey = 0.0;
		m_pattern = 1;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_HARMONIC );
		m_nSubMvmtModel = SM_GEN_DELAY;
		m_delay = 0.05;
		m_warp = 1.;
		m_dur = 0.16;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, IDC_RDO_SM_DELAY );
	}
	else if( m_nMode == GS_LINEAR )
	{
		m_fInt = TRUE;
		m_noise = 0.02;
		m_startt = 0.1;
		m_deltat = 1.0;
		m_nstrokes = 1.0;
		m_trailtime = 0.1;
		m_startx = 0.;
		m_deltax = 5.0;
		m_phasex = 0.0;
		m_xspeed = 0.0;
		m_starty = 0.0;
		m_deltay = 5.0;
		m_phasey = 0.0;
		m_pattern = 1;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_HARMONIC, IDC_RDO_JERK, IDC_RDO_HARMONIC );
		m_nSubMvmtModel = SM_GEN_DELAY;
		m_delay = 0.05;
		m_warp = 1.;
		m_dur = 0.16;
		if( m_hWnd ) CheckRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP, IDC_RDO_SM_DELAY );
	}
}

void SigGenSettingsDlg::OnRdoSM()
{
	int nRdo = GetCheckedRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP );
	BOOL fRdoSMDelay = ( nRdo == IDC_RDO_SM_DELAY );
	BOOL fRdoSMWarp = ( nRdo == IDC_RDO_SM_WARP );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_SM_DELAY );
	if( pWnd ) pWnd->EnableWindow( fRdoSMDelay );
	pWnd = GetDlgItem( IDC_EDIT_SM_WARP );
	if( pWnd ) pWnd->EnableWindow( fRdoSMWarp );
	pWnd = GetDlgItem( IDC_EDIT_SM_DURATION );
	if( pWnd ) pWnd->EnableWindow( fRdoSMWarp || fRdoSMDelay );
}

BOOL SigGenSettingsDlg::VerifySubMvmtValues()
{
	UpdateData( TRUE );

	UINT nID = 0;
	CString szMsg;

	// if NONE selected for method, all OK
	m_nSubMvmtModel = GetCheckedRadioButton( IDC_RDO_SM_NONE, IDC_RDO_SM_WARP );
	if( m_nSubMvmtModel == IDC_RDO_SM_NONE ) return TRUE;

	// verify primary sub-movement duration
	if( m_dur < 0. )
	{
		szMsg = _T("Duration of primary submovement must be greater than 0.");
		nID = IDC_EDIT_SM_DURATION;
		goto Error;
	}
	if( m_dur >= m_deltat )
	{
		szMsg = _T("Duration of primary submovement must be less than stroke duration.");
		nID = IDC_EDIT_SM_DURATION;
		goto Error;
	}

	// verify delay if method = delay
	if( m_nSubMvmtModel == IDC_RDO_SM_DELAY )
	{
		// >= 0
		if( m_delay < 0. )
		{
			szMsg = _T("Delay around end of primary submovement must be greater than 0.");
			nID = IDC_EDIT_SM_DELAY;
			goto Error;
		}
		// Ensure: StrokeDuration > DurationPrimary + 0.5*Delay
		double dVal = m_dur + 0.5 * m_delay;
		if( m_deltat <= dVal )
		{
			szMsg = _T("The following condition is not satisfied:\nStrokeDuration > PrimaryStrokeDuration + 0.5 * Delay");
			nID = IDC_EDIT_SM_DELAY;
			goto Error;
		}
	}

	// verify warp if method = warp
	if( m_nSubMvmtModel == IDC_RDO_SM_WARP )
	{
		// >= 0
		if( m_warp < 0. )
		{
			szMsg = _T("Time warping amplitude must be greater than 0.");
			nID = IDC_EDIT_SM_WARP;
			goto Error;
		}
	}

	goto NoError;

Error:
	// message & set focus if an error was found
	BCGPMessageBox( szMsg );
	CWnd* pWnd = GetDlgItem( nID );
	if( pWnd )
	{
		pWnd->SetFocus();
		CEdit* pEdit = (CEdit*)pWnd;
		pEdit->SetSel( 0, -1 );
	}
	return FALSE;

NoError:
	return TRUE;
}

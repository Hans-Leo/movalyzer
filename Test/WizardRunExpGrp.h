#pragma once

// WizardRunExpGrp.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpGrp dialog

class WizardRunExpGrp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardRunExpGrp)

// Construction
public:
	WizardRunExpGrp();
	~WizardRunExpGrp();

// Dialog Data
	enum { IDD = IDD_WIZ_RUNEXP_GRP };
	CBCGPComboBox	m_Cbo;
	CString			m_szID;
	CString			m_szDesc;
	CString			m_szGrp;
	CString			m_szNotes;
	CString			m_szExpID;
	CStringList		m_existing;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fInit;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnSelchangeCboGrps();
	afx_msg void OnBnCreategrp();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

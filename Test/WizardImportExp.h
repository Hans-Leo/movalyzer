#pragma once

// WizardImportExp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportExp dialog

class WizardImportExp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportExp)

// Construction
public:
	WizardImportExp();
	~WizardImportExp();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_EXP };
	CBCGPComboBox	m_Cbo;
	CString			m_szID;
	CString			m_szDesc;
	CString			m_szNotes;
	short			m_nExpType;
	BOOL			m_fUpdated;
	BOOL			m_fNew;
	BOOL			m_fGenWiz;
	BOOL			m_fImage;
	BOOL			m_fGripper;
	CBCGPPropertySheet* m_pParent;
	int				m_nCondIdx;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual LRESULT OnWizardBack();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnCreateexp();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboExps();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// InputDeviceSettingsDlg dialog

class InputDeviceSettingsDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(InputDeviceSettingsDlg)

public:
	InputDeviceSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~InputDeviceSettingsDlg();

// Dialog Data
	enum { IDD = IDD_IDS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	int m_nSamplingRate;
	int m_nMinPenPressure;
	double m_dDevRes;
	afx_msg void OnBnClickedBnReset();
};

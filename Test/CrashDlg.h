#pragma once

// CrashDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CrashDlg dialog

class CrashDlg : public CBCGPDialog
{
// Construction
public:
	CrashDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_CRASH };
	CString			m_szDo;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	afx_msg void OnBnAdd();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

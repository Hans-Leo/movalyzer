// WizardSetupExp.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupExp.h"
#include "ProcSetSheet.h"
#include "WizardSetupSheet.h"
#include "Experiments.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardSetupExp property page

IMPLEMENT_DYNCREATE(WizardSetupExp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardSetupExp, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_CREATEEXP, OnBnCreateexp)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupExp::WizardSetupExp() : CBCGPPropertyPage(WizardSetupExp::IDD)
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_szType = _T("");
	m_szNotes = _T("");
	m_nType = 0;		// EXP_TYPE_HANDWRITING (default)
	m_fUpdated = FALSE;
	m_pPS = NULL;
}

WizardSetupExp::~WizardSetupExp()
{
	if( m_pPS ) m_pPS->Release();
}

void WizardSetupExp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szDesc);
	DDX_Text(pDX, IDC_TXT_EXPTYPE, m_szType);
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupExp message handlers

void WizardSetupExp::OnBnCreateexp() 
{
	HRESULT hr;
	if( !m_pPS )
	{
		hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&m_pPS );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_PS_ERR );
			return;
		}
	}
	ProcSetSheet d( m_pPS, this, FALSE, TRUE );
	d.DoModal();
	if( d.m_fModified )
	{
		m_pPS->put_ExperimentID( d.m_szID.AllocSysString() );
		m_szID = d.m_szID;
		m_szDesc = d.m_szDesc;
		m_szNotes = d.m_szNotes;
		m_nType = d.m_nType;
		switch( m_nType )
		{
			case EXP_TYPE_HANDWRITING:
				m_szType = _T("Handwriting");
				break;
			case EXP_TYPE_IMAGE:
				m_szType = _T("Handwriting Image");
				break;
			case EXP_TYPE_GRIPPER:
			{
				IProcSetRunExp* pPSRE = NULL;
				m_pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
				if( pPSRE )
				{
					pPSRE->put_MaxRecArea( TRUE );
					pPSRE->Release();
				}
				m_szType = _T("Grip-Force");
				break;
			}
			default:
				m_szType = _T("Unknown");
		}
		UpdateData( FALSE );
		WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	}
}

BOOL WizardSetupExp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_EXP_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_BN_CREATEEXP );
	m_tooltip.AddTool( pWnd, _T("Create a new experiment to use."), _T("New Experiment") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardSetupExp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardSetupExp::OnWizardBack() 
{
	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardSetupExp::OnSetActive() 
{
	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	if( m_szID != _T("") ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardSetupExp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newexperiment.html#wizard"), this );
	return TRUE;
}

BOOL WizardSetupExp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

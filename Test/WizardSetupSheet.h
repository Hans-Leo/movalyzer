#pragma once

// WizardSetupSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardSetupSheet

class WizardSetupSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardSetupSheet)

// Construction
public:
	WizardSetupSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	virtual ~WizardSetupSheet();

// Attributes
public:
	CString	m_szExpID;
	CString	m_szExpDesc;
	short	m_nExpType;

// Operations
public:
	void AddPages();

// Overrides

// message map functions
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnHelpInfo( HELPINFO* );
};

// MoveToGroupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "MoveToGroupDlg.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MoveToGroupDlg dialog

BEGIN_MESSAGE_MAP(MoveToGroupDlg, CBCGPDialog)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkListGrp)
	ON_LBN_DBLCLK(IDC_LIST2, OnDblclkListSubj)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

MoveToGroupDlg::MoveToGroupDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(MoveToGroupDlg::IDD, pParent)
{
}

void MoveToGroupDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_listGrp);
	DDX_Control(pDX, IDC_LIST2, m_listSubj);
}

/////////////////////////////////////////////////////////////////////////////
// MoveToGroupDlg message handlers

void MoveToGroupDlg::OnDblclkListGrp() 
{
	OnOK();
}

void MoveToGroupDlg::OnDblclkListSubj() 
{
	OnOK();
}

BOOL MoveToGroupDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	CString szItem;
	int nItem = LB_ERR, nItemSel = LB_ERR;
	// fill groups list
	POSITION pos = m_lstItemsGrp.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstItemsGrp.GetNext( pos );
		nItem = m_listGrp.AddString( szItem );
		if( szItem == m_szCurGrp ) nItemSel = nItem;
	}
	// select current group
	if( nItemSel != LB_ERR ) m_listGrp.SetCurSel( nItemSel );

	// fill subjects list
	nItem = LB_ERR, nItemSel = LB_ERR;
	pos = m_lstItemsSubj.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstItemsSubj.GetNext( pos );
		nItem = m_listSubj.AddString( szItem );
		if( szItem == m_szCurSubj ) nItemSel = nItem;
	}
	// select current subject
	if( nItemSel != LB_ERR ) m_listSubj.SetCurSel( nItemSel );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MoveToGroupDlg::OnOK()
{
	// get selected group
	int nPos = m_listGrp.GetCurSel();
	// verify one is selected
	if( nPos == LB_ERR )
	{
		BCGPMessageBox( _T("No group has been selected.") );
		return;
	}
	// get group text
	m_listGrp.GetText( nPos, m_szGrp );

	// get selected subject
	nPos = m_listSubj.GetCurSel();
	// verify one is selected
	if( nPos == LB_ERR )
	{
		BCGPMessageBox( _T("No subject has been selected.") );
		return;
	}
	// get subject text
	m_listSubj.GetText( nPos, m_szSubj );

	// what was changed
	BOOL fGrp = ( m_szGrp != m_szCurGrp );
	BOOL fSubj = ( m_szSubj != m_szCurSubj );

	// verify something different has been selected
	if( !fGrp && !fSubj )
	{
		BCGPMessageBox( _T("You have not selected a different group and/or subject.") );
		return;
	}

	CBCGPDialog::OnOK();
}

BOOL MoveToGroupDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("newsubject.html#move"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

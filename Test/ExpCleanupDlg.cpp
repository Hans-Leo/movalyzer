// ExpCleanupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "ExpCleanupDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExpCleanupDlg dialog

BEGIN_MESSAGE_MAP(ExpCleanupDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_CHK_HWR, OnChkHwr)
	ON_BN_CLICKED(IDC_CHK_EXT, OnChkExt)
	ON_BN_CLICKED(IDC_CHK_INC, OnChkInc)
END_MESSAGE_MAP()

ExpCleanupDlg::ExpCleanupDlg(CWnd* pParent, BOOL fSubject)
	: CBCGPDialog(ExpCleanupDlg::IDD, pParent), m_fSubject( fSubject )
{
	m_fHWR = FALSE;
	m_fTF = TRUE;
	m_fSeg = TRUE;
	m_fExt = FALSE;
	m_fCon = TRUE;
	m_fErr = TRUE;
	m_fInc = FALSE;
}

void ExpCleanupDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHK_HWR, m_fHWR);
	DDX_Check(pDX, IDC_CHK_TF, m_fTF);
	DDX_Check(pDX, IDC_CHK_SEG, m_fSeg);
	DDX_Check(pDX, IDC_CHK_EXT, m_fExt);
	DDX_Check(pDX, IDC_CHK_CON, m_fCon);
	DDX_Check(pDX, IDC_CHK_ERR, m_fErr);
	DDX_Check(pDX, IDC_CHK_INC, m_fInc);
}

/////////////////////////////////////////////////////////////////////////////
// ExpCleanupDlg message handlers

BOOL ExpCleanupDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);
	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void ExpCleanupDlg::OnChkHwr() 
{
	m_fHWR = FALSE;
	UpdateData( FALSE );
	BCGPMessageBox( _T("Recorded data must be removed manually."), MB_OK | MB_ICONINFORMATION );
}
void ExpCleanupDlg::OnChkExt() 
{
	UpdateData( TRUE );
	if( m_fExt )
	{
		if( BCGPMessageBox( _T("Are you sure you want to delete the trial result files?\nYou will need to reprocess the experiment."), MB_YESNO ) == IDNO )		{
			m_fExt = FALSE;
			UpdateData( FALSE );
		}
	}
}

void ExpCleanupDlg::OnChkInc() 
{
	UpdateData( TRUE );
	if( m_fInc )
	{
		if( BCGPMessageBox( IDS_INC_WARN, MB_YESNO ) == IDNO )
		{
			m_fInc = FALSE;
			UpdateData( FALSE );
		}
	}
}

BOOL ExpCleanupDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_CHK_HWR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL_HWR, _T("Source Files") );
	pWnd = GetDlgItem( IDC_CHK_TF );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL_TF, _T("Time Function Files") );
	pWnd = GetDlgItem( IDC_CHK_SEG );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL_SEG, _T("Segmentation Files") );
	pWnd = GetDlgItem( IDC_CHK_EXT );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL_EXT, _T("Feature Files") );
	pWnd = GetDlgItem( IDC_CHK_CON );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL_CON, _T("Consistency Files") );
	pWnd = GetDlgItem( IDC_CHK_ERR );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_DEL_ERRS, _T("Error Files") );
	pWnd = GetDlgItem( IDC_CHK_INC );
	if( pWnd )
	{
		if( m_fSubject )
		{
			m_tooltip.AddTool( pWnd, IDS_DEL_INC_NO, _T("Summary File") );
			pWnd->EnableWindow( FALSE );
		}
		else m_tooltip.AddTool( pWnd, IDS_DEL_INC, _T("Summary File") );
	}
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

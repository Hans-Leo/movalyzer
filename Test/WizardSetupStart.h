#pragma once

// WizardSetupStart.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardSetupStart dialog

class WizardSetupStart : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardSetupStart)

// Construction
public:
	WizardSetupStart();
	~WizardSetupStart();

// Dialog Data
	enum { IDD = IDD_WIZ_SETUP_START };

// Overrides
public:
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
//message map functions
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

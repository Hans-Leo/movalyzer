// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "..\Common\MessageTarget.h"
#include "..\Common\MessageManager.h"

// import the security module dll with no namespace
// also rename the SSCProt function "GetUserName" as it conflicts with a windows API
#include "..\NSSharedGUI\WindowManager.h"
#include "..\NSSharedGUI\SystemTray.h"

struct TrialRunInfo;
TrialRunInfo* GetTrialRunInfo();

class CGraphView;
class CRecView;
class CMsgView;
class CLeftView;
interface IProcSetting;
class Preferences;
struct GripperSettings;

#define ID_STATUS_PROGRESS	9191

class NSMenuBar : public CBCGPMenuBar
{
public:
	NSMenuBar() : CBCGPMenuBar() {};
	virtual ~NSMenuBar() {}
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class NSToolBar : public CBCGPToolBar
{
public:
	NSToolBar() : CBCGPToolBar() {};
	virtual ~NSToolBar() {}
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class NSStatusBar : public CBCGPRibbonStatusBar
{
public:
	NSStatusBar() : CBCGPRibbonStatusBar() {};
	virtual ~NSStatusBar() {}
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class NSSplitterWnd : public CBCGPSplitterWnd
{
public:
	NSSplitterWnd() : CBCGPSplitterWnd() {}
	virtual ~NSSplitterWnd() {}
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};
class NSCaptionBar : public CBCGPCaptionBar
{
public:
	NSCaptionBar() : CBCGPCaptionBar() {};
	virtual ~NSCaptionBar() {}
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	DECLARE_MESSAGE_MAP()
};

class CMainFrame : public CBCGPFrameWnd, public MessageTarget
{
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
protected:
	NSSplitterWnd		m_wndSplitter;
	NSSplitterWnd		m_wndSplitter2;
	NSSplitterWnd		m_wndSplitter3;
	BOOL				m_fInit;
	BOOL				m_fClearLastUser;
public:
	WindowManager		m_wm;
	CSystemTray			m_wndSysTrayIcon;
	BOOL				m_fRecording;
	BOOL				m_fContinue;
	BOOL				m_fMainToolbarMenu;
	CBitmap				m_bmpStatus;

// Operations
public:
	void OnChangeLook( BOOL bOneNoteTabs, BOOL bMDITabColors, BOOL bIsVSDotNetLook,
							 BOOL bDockTabColors, BOOL bMDITabsVS2005Look, BOOL bIsToolBoxEx );
	virtual BOOL OnShowPopupMenu( CBCGPPopupMenu* pMenuPopup );
// Overrides
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnDrawMenuImage( CDC* pDC, const CBCGPToolbarMenuButton* pMenuButton, const CRect &rectImage );
	// receive messages from messagemanager
	virtual void Message( LPARAM msg, WPARAM val, BOOL* fStop );
public:
	void OnCreateClient2();

// Implementation
public:
	virtual ~CMainFrame();
	CRecView*	GetSettingsPane();
	CMsgView*	GetMsgPane();
	CListCtrl*	GetMsgPaneList();
	CGraphView*	GetRecordingPane();
	CLeftView*	GetLeftView();
	int			GetTreeSize();
	int			GetTreeHeight();
	void		SetTreeSize( int nSize );
	void		SetTreeHeight( int nSize );
	void		ResetSizes( BOOL fOriginal = TRUE );
	int			GetMsgSize();
	void		SetMsgSize( int nSize );
	int			GetRecSize();
	void		SetRecSize( int nSize );
	void		GetFramePos( int& x, int&, int& width, int& height );
	BOOL		IsMinimized() { return m_bIsMinimized; }

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	NSStatusBar		m_wndStatusBar;
protected:  // control bar embedded members (now BCG)
	NSToolBar		m_wndToolBar;
	NSToolBar		m_wndToolBarID;
	NSToolBar		m_wndToolBarExp;
	NSToolBar		m_wndToolBarInfo;
	NSMenuBar		m_wndMenuBar;
	NSToolBar		m_wndToolBarCommon;
	NSToolBar		m_wndToolBarUpdate;
	NSCaptionBar	m_wndCaptionBar;

//Implementation
public:
	void SetAppTitle();
	void ShowPopupWindow( CString szMsg, CString szURL = _T(""), UINT nURL = 0,
						  BYTE nTransparency = 200 /* 0-255[opaque] */,
						  int nTimeToClose = 3000 /* ms */ );
	void Record( CString szFile = _T(""), BOOL fTrial = FALSE,
				 CString szWStim = _T(""), CString szPStim = _T(""),
				 CString szStim = _T(""), BOOL fJustQuit = FALSE,
				 BOOL fShowStim = FALSE );
	void ObjStop();
	void StopRecording();
	void StopCondition();
	void StopExperiment();

	void ChartHwr( CString szFile, double dFrequency, BOOL fModeless = FALSE,
		DWORD dwPenUpColor = ::GetSoftColor(), BOOL fChangePenUpColor = TRUE,
		int nMinPenPressure = 1 );
	void ChartHwr( CString szExp, CString szGrp, CString szSubj,
				   CString szTrial, double dFrequency, int nMinPenPressure );
	void ChartHwrRT( CString szExp, CString szGrp, CString szSubj,
					 CString szTrial, double dFrequency );
	void ChartHwrMultiple( CString szExp, CString szGrp, CString szSubj,
						   double dFrequency, int nMinPenPressure, CStringList* pList = NULL );
	void ChartTf( CString szFile, double dFrequency, CString szSeg = _T(""),
				  BOOL fModeless = FALSE, BOOL fPassed = TRUE, BOOL fForceShowErr = FALSE,
				  CWnd* pWndOwner = NULL );
	void ChartTf( CString szExp, CString szGrp, CString szSubj, CString szTrial,
				  double dFrequency, int nMinPenPressure, BOOL fPassed = TRUE );
	void ChartTfRT( CString szExp, CString szGrp, CString szSubj,
					CString szTrial, double dFrequency );
	void ChartHwr3D( CString szExp, CString szGrp, CString szSubj,
				   CString szTrial, double dFrequency );
	void ChartTf3D( CString szExp, CString szGrp, CString szSubj, CString szTrial,
				  double dFrequency, BOOL fPassed = TRUE );
	void OnTabletTimeOut();
	BOOL RunExperimentShared( CString szExpID, CString szExp,
							  CString szGrpID, CString szSubjID,
							  CString szSubj, IProcSetting* pPS,
							  CStringList* lstConditions, BOOL fAuto );
	void RunExperimentAuto( CString szExpID, CString szExp,
							CString szGrpID, CString szGrp,
							CString szSubjID, CString szSubj );
	void RunExperiment( IProcSetting* pPS, CString szExpID, CString szExp,
						CString szSubjID, CString szSubj, CString szGrpID,
						CStringList* lstConditions, BOOL fSen2Word, HTREEITEM hSubject = NULL,
						BOOL fJustStim = FALSE, BOOL fElement = FALSE );
	void SummarizeExperiment( CString szExp, CListView* pMsgWnd = NULL, BOOL fDlg = TRUE,
							  BOOL fOnly = FALSE, BOOL fInc = FALSE,
							  BOOL fErr = FALSE, CString szSubjID = _T(""), BOOL fDoConsis = FALSE );
	void RedoTrial();
	void RerunTrial( CString szExp, CString szSubj, CString szGrp,
					 CString szTrial, HTREEITEM hItem );
	void TestTrialLinearity( CString szExp, CString szSubj, CString szGrp, CString szTrial );
	void ReprocessSubject( CString szExpID, CString szSubjID, CString szGrpID, double dDevRes,
						   int nSampRate, HTREEITEM hItem, CLeftView* pLeft, CListView* pLV,
						   int nSubjCount, int nCurSubj, BOOL fInformIfEmpty = TRUE, BOOL fSubCond = TRUE,
						   BOOL fSeparateThread = TRUE, BOOL fReprocAll = TRUE );
	void ReprocessSubjectCond( CString szExpID, CString szSubjID, CString szGrpID,
							   CString szCondID, double dDevRes, int nSampRate,
							   HTREEITEM hItem, CLeftView* pLeft, CListView* pLV,
							   int nSubjCount, int nCurSubj, BOOL fReprocAll = TRUE );
	void ReprocessTrial( CString szExpID, CString szSubjID, CString szGrpID, CString szTrial,
						 double dDevRes, int nSampRate, HTREEITEM hItem, BOOL fAppend );
	void LegendClosed();
	void ViewCalc( CWnd* pWnd );
	void CalcClosed();
	BOOL WritePreferences( Preferences* prefs, GripperSettings* gs );
	void GetPreferences( Preferences* prefs, GripperSettings* gs );
	void SetPreferences( Preferences* prefs, GripperSettings* gs );
	BOOL OnPreferences( UINT nPage, CString szMsg );
	void OnDigTest2();
	BOOL ConfirmPrefs( BOOL fUserOnly = FALSE );
	void OnOPath2() { OnOPath(); }
	void BadTablet();
	BOOL GetSoundInfo( UINT nSound, CString szCondID, BOOL& fTone, int& nFreq,
					   int& nDur, CString& szFile, BOOL& fTrigger );
	void StimulusEditor( CString szStim = _T("") );
	void LoadUserRegistryData( CString szID, CString szPrefix );
	void DoExportWizard( CString szExpID = _T("") );

	// separate thread sets flag (default TRUE for safety from crash)
	static void CALLBACK ShowStatusText( CString szMsg );
	static void CALLBACK EnableProgress( long nTotal );
	static void CALLBACK SetProgress( long nCur, CString szTxt );
	static void CALLBACK DisableProgress();
	static void CALLBACK GetProgress( long& nCur, long& nTotal );
	static void CALLBACK ShowProgress( BOOL fShow );
	BOOL FBackupdb( BOOL fAsk = TRUE );
	BOOL OnWizDevSetup2( CWnd* pParent = NULL );
	void ShowGettingStarted();
protected:
	void RunExperiment2( BOOL fSkipProcess = FALSE, long lSteps = 0 );
	BOOL RunProcess();
	void DeleteProcess();
	void ConvertToPCX( DWORD dwType );

// message map functions
public:
	void HandleMouseMoveForRecording();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnOLogdb();
	afx_msg void OnUpdateOLogdb(CCmdUI* pCmdUI);
	afx_msg void OnOLoggraph();
	afx_msg void OnUpdateOLoggraph(CCmdUI* pCmdUI);
	afx_msg void OnOLogproc();
	afx_msg void OnUpdateOLogproc(CCmdUI* pCmdUI);
	afx_msg void OnOLogtablet();
	afx_msg void OnUpdateOLogtablet(CCmdUI* pCmdUI);
	afx_msg void OnOLogui();
	afx_msg void OnUpdateOLogui(CCmdUI* pCmdUI);
	afx_msg void OnOLogall();
	afx_msg void OnOLognone();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewLog();
	afx_msg void OnClearLog();
	afx_msg void OnObjRelationships();
	afx_msg void OnHelpHelp();
	afx_msg void OnClose();
	afx_msg void OnClearMessages();
	afx_msg void OnViewLegend();
	afx_msg void OnEClear();
	afx_msg void OnEStopexp();
	afx_msg void OnEStopcond();
	afx_msg void OnEStoprec();
	afx_msg void OnAddLog();
	afx_msg void OnSendLog();
	afx_msg void OnViewQuest();
	afx_msg void OnOQuick();
	afx_msg void OnOPath();
	afx_msg void OnOInputdevice();
	afx_msg void OnUpdateInputDevice(CCmdUI* pCmdUI);
	afx_msg void OnOInputdeviceSettings();
	afx_msg void OnODologging();
	afx_msg void OnDigTest();
	afx_msg void OnDigReset();
	afx_msg void OnObjStop();
	afx_msg void OnViewRefresh();
	afx_msg void OnUpdateViewRefresh(CCmdUI* pCmdUI);
	afx_msg void OnFileUsers();
	afx_msg void OnFileBackuphistory();
	afx_msg void OnFBackupdb();
	afx_msg void OnFRestoredb();
	afx_msg void OnWizExpsetup();
	afx_msg void OnWizDataimport();
	afx_msg void OnWizRemoteDataimport();
	afx_msg void OnWizDataExport();
	afx_msg void OnWizDatagen();
	afx_msg void OnFBackupsys();
	afx_msg void OnHelpTutorial();
	afx_msg void OnViewProgress();
	afx_msg void OnWizRunexp();
	afx_msg void OnViewPrivate();
	afx_msg void OnUpdateViewLegend(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOQuick(CCmdUI* pCmdUI);
	afx_msg void OnUpdateODologging(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjStop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewProgress(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewPrivate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewsStop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewsRunExp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewsCont(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewsUsers(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewsSettings(CCmdUI* pCmdUI);
	afx_msg void OnViewFullScreen();
	afx_msg void OnUpdateViewFullScreen(CCmdUI* pCmdUI);
	afx_msg void OnViewMenu();
	afx_msg void OnUpdateViewMenu(CCmdUI* pCmdUI);
	afx_msg void OnViewInfo();
	afx_msg void OnUpdateViewInfo(CCmdUI* pCmdUI);
	afx_msg void OnViewId();
	afx_msg void OnUpdateViewId(CCmdUI* pCmdUI);
	afx_msg void OnViewExp();
	afx_msg void OnUpdateViewExp(CCmdUI* pCmdUI);
	afx_msg void OnViewAction();
	afx_msg void OnUpdateViewAction(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewCustomize(CCmdUI* pCmdUI);
	afx_msg void OnViewTextlabels();
	afx_msg void OnUpdateViewTextlabels(CCmdUI* pCmdUI);
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarReset(WPARAM,LPARAM);
	afx_msg LRESULT OnToolbarContextMenu(WPARAM,LPARAM);
	afx_msg LRESULT OnHelpCustomizeToolbars(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnShowProgress(WPARAM, LPARAM);
	afx_msg LRESULT OnShowStatusText(WPARAM, LPARAM);
	afx_msg LRESULT OnEnableProgress(WPARAM, LPARAM);
	afx_msg LRESULT OnSetProgress(WPARAM, LPARAM);
	afx_msg LRESULT OnDisableProgress(WPARAM, LPARAM);
	afx_msg LRESULT OnTCard(WPARAM, LPARAM);
	afx_msg void OnDevice();
	afx_msg void OnUpdateDevice(CCmdUI* pCmdUI);
	afx_msg void OnOpenStimEditor();
	afx_msg void OnUpdateOpenStimEditorRx(CCmdUI* pCmdUI);
	afx_msg void OnOpenRx();
	afx_msg void OnHelpQuick();
	afx_msg void OnHelpTroubleshooting();
	afx_msg void OnHelpTroubleshootingRec();
	afx_msg void OnHelpActivate();
	afx_msg void OnHelpDeactivate();
	afx_msg void OnUpdateHelpActivate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateHelpDeactivate(CCmdUI* pCmdUI);
	afx_msg void OnHelpLicense();
	afx_msg void OnViewCalc();
	afx_msg void OnHelpPpu();
	afx_msg void OnUpdateHelpPpu(CCmdUI *pCmdUI);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnCheckForUpdates();
	afx_msg void OnUpdateCheckForUpdates(CCmdUI *pCmdUI);
	afx_msg void OnWizDevSetup();
	afx_msg void OnUpdateWizDevSetup(CCmdUI *pCmdUI);
	afx_msg void OnWebsite();
	afx_msg void OnWebsiteSupport();
	afx_msg void OnTrayMinimize();
	afx_msg void OnUpdateTrayMinimize(CCmdUI *pCmdUI);
	afx_msg void OnTrayMaximize();
	afx_msg void OnUpdateTrayMaximize(CCmdUI *pCmdUI);
	afx_msg void OnOUser();
	afx_msg void OnRecoverPass();
	afx_msg void OnUpdateRecoverPass(CCmdUI* pCmdUI);
	afx_msg void OnRestorePass();
	afx_msg void OnUpdateRestorePass(CCmdUI* pCmdUI);
	afx_msg void OnViewGettingStarted();
	afx_msg void OnViewStartPage();
	afx_msg void OnUpdateViewStartPage(CCmdUI* pCmdUI);
    afx_msg LRESULT OnHyperlinkClicked( WPARAM wParam, LPARAM lParam );
	afx_msg void OnConvertBMPtoPCX();
	afx_msg void OnConvertPNGtoPCX();
	afx_msg void OnConvertGIFtoPCX();
	afx_msg void OnConvertJPGtoPCX();
	afx_msg void OnFImport();
	afx_msg void OnUpdateFileImport(CCmdUI* pCmdUI);
	// CHART-based popup menu on-update handlers
	afx_msg void OnUpdateShowData(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSegSymbol(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSubSymbol(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFeedbackTF(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSupAnn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowXY(CCmdUI* pCmdUI);
	afx_msg void OnAppExitNoMemory();
	DECLARE_MESSAGE_MAP()
};

#ifndef GETTHETRIAL
#define GETTHETRIAL
#define	GET_TRIAL() \
	CString szPath, szFile; \
	CLeftView* pLV = GetLeftView(); \
	if( pLV && pLV->GetSelectedTrialLocation( szFile, szPath ) ) \
	{ \
		if( ( szFile != _T("") ) && ( szFile.GetLength() > 4 ) ) \
			szFile = szFile.Left( szFile.GetLength() - 4 ); \
	} \
	else \
	{ \
		::GetDataPathRoot( szPath ); \
		szFile = _T("test"); \
	}
#endif

double GetBetweenTrialTime();
void SetBetweenTrialTime( double dVal );

/////////////////////////////////////////////////////////////////////////////

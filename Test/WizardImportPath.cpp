// WizardImportPath.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportPath.h"
#include "shlobj.h" // For SHFunctions
#include "WizardImportSheet.h"
#include "WizardImportFormat.h"
#include "ImageDlg.h"
#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\CxImage\CxImage\ximage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportPath property page

IMPLEMENT_DYNCREATE(WizardImportPath, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportPath, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnBrowse)
	ON_EN_CHANGE(IDC_EDIT_PATH, OnChangeEditPath)
	ON_BN_CLICKED(IDC_BN_COLS, OnBnCols)
	ON_BN_CLICKED(IDC_BN_VIEW, OnBnView)
	ON_BN_CLICKED(IDC_CHK_REMAINDER, OnBnClickedChkRemainder)
	ON_CBN_SELCHANGE(IDC_CBO_DELIM, OnChangeDelim)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportPath::WizardImportPath() : CBCGPPropertyPage(WizardImportPath::IDD)
{
	m_szPath = _T("");
	m_nSkip = 0;
	m_fHeaders = FALSE;
	m_fRemainder = FALSE;
	m_nCount = 0;
}

WizardImportPath::~WizardImportPath()
{
}

void WizardImportPath::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_szPath);
	DDV_MaxChars(pDX, m_szPath, 255);
	DDX_Text(pDX, IDC_EDIT_SKIP, m_nSkip);
	DDV_MinMaxInt(pDX, m_nSkip, 0, 100);
	DDX_Check(pDX, IDC_CHK_HEADERS, m_fHeaders);
	DDX_Check(pDX, IDC_CHK_REMAINDER, m_fRemainder);
	DDX_Text(pDX, IDC_EDIT_COUNT, m_nCount);
	DDV_MinMaxInt(pDX, m_nCount, 0, 99);
	DDX_Control(pDX, IDC_CBO_DELIM, m_cboDelim);
	DDX_CBString(pDX, IDC_CBO_DELIM, m_szDelimName);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportPath message handlers

BOOL WizardImportPath::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the path for the recorded data to import."), _T("Path") );
	pWnd = GetDlgItem( IDC_BN_BROWSE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Browse for path of data to import."), _T("Browse For File") );
	pWnd = GetDlgItem( IDC_BN_VIEW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View raw data to import."), _T("View Import File") );
	pWnd = GetDlgItem( IDC_BN_COLS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select columns to import from file."), _T("Select Data Columns") );
	pWnd = GetDlgItem( IDC_EDIT_SKIP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify how many lines to skip from the top of the file (excluding headers if any)."), _T("Lines to Skip") );
	pWnd = GetDlgItem( IDC_CHK_HEADERS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify whether the file has column headers. If not, generic ones will be used."), _T("Column Headers") );
	pWnd = GetDlgItem( IDC_CHK_REMAINDER );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify whether multiple files with the same extension in the same directory are imported as well."), _T("Multiple Files") );
	pWnd = GetDlgItem( IDC_EDIT_COUNT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the number of trials to import in sequence starting with above."), _T("Number of Trials") );
	pWnd = GetDlgItem( IDC_CBO_DELIM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the field delimiter for delimited import files (CSV)."), _T("Delimiter") );

	m_cboDelim.AddString( _T("Comma") );
	m_cboDelim.AddString( _T("Space") );
	m_cboDelim.AddString( _T("Tab") );
	m_cboDelim.SetCurSel( 0 );
	m_szDelimName = _T("Comma");
	m_szDelim = _T(",");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportPath::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardImportPath::OnWizardBack() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

LRESULT WizardImportPath::OnWizardNext() 
{
	UpdateData( TRUE );
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();

	if( pParent->m_szLastFile == m_szPath )
	{
		if( BCGPMessageBox( _T("You just imported this file/location. Are you sure?"), MB_YESNO ) == IDNO )
			return -1;
	}

	if( m_szName != _T("GRIPPER") && m_szName != _T("CSV") )
	{
		return CBCGPPropertyPage::OnWizardNext();
	}

	if( m_dlgColSel.m_lstColNum.GetCount() == 0 )
	{
		m_dlgColSel.m_szFile = m_szPath;
		m_dlgColSel.m_nSkip = m_nSkip;
		m_dlgColSel.m_fHeaders = m_fHeaders;
		m_dlgColSel.m_szDelim = m_szDelim;
		if( m_dlgColSel.DoModal() == IDCANCEL ) return -1;
	}

	if( m_szDelimName == _T("Comma") ) m_szDelim = _T(",");
	else if( m_szDelimName == _T("Space") ) m_szDelim = _T(" ");
	else if( m_szDelimName == _T("Tab") ) m_szDelim = _T("\t");

	int nPos = m_szPath.ReverseFind( '.' );
	if( nPos == -1 )
	{
		BCGPMessageBox( _T("This does not appear to be a valid file.") );
		return -1;
	}
	m_szExt = m_szPath.Mid( nPos + 1 );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportPath::OnSetActive() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportFormat* pFmt = (WizardImportFormat*)pParent->GetPage( 1 );
	m_szName = pFmt->m_szFormat;

	if( CheckFile( TRUE ) )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else
		pParent->SetWizardButtons( PSWIZB_BACK );
	OnChangeEditPath();

	return CBCGPPropertyPage::OnSetActive();
}

void WizardImportPath::OnBnBrowse() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportFormat* pFmt = (WizardImportFormat*)pParent->GetPage( 1 );
	m_szName = pFmt->m_szFormat;

	static CString _LastExt = _T("HWR");

	CString szPath;
	::GetAppPath( szPath );

	if( ( m_szName != _T("MOVALYZER") ) && ( m_szName != _T("SINGLE TRIAL OR IMAGE") ) )
	{
		char szObj[ 500 ];
		strcpy_s( szObj, 500, szPath );
		strcat_s( szObj, 500, m_szName );
		strcat_s( szObj, 500, ".DRV" );
		HINSTANCE hMod = LoadLibrary( szObj );
		if( hMod < (HINSTANCE)HINSTANCE_ERROR )
		{
			szPath.Format( _T("Unable to load %s driver."), m_szName );
			BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
			return;
		}

		typedef LONG (CDECL* LPFNEXT) (LPSTR);
		LPFNEXT proc; 
		proc = (LPFNEXT)GetProcAddress( hMod, "GetExtension" );
		if( !proc ) 
		{
			szPath.Format( _T("Error in %s driver."), m_szName );
			BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
			return;
		}

		char szExt[ 5 ];
		LONG lVal = (*proc)( szExt );
		m_szExt = szExt;

		FreeLibrary( hMod );

		CString szFilter, szFile;
		szFilter.Format( _T("%s files (*.%s)|*.%s|Comma-delimited files (*.csv)|*.csv|All files (*.*)|*.*|"),
						 m_szName, m_szExt, m_szExt );
		CFileFind ff;
		UpdateData( TRUE );
		if( m_szPath.GetLength() > 0 )
		{
			if( m_szPath.GetAt( m_szPath.GetLength() - 1 ) == '\\' )
				szPath = m_szPath.Left( m_szPath.GetLength() - 1 );
			else szPath = m_szPath;
		}
		if( ff.FindFile( szPath ) )
		{
			ff.FindNextFile();
			if( ff.IsDirectory() ) szFile.Format( _T("%s\\*.%s"), szPath, szExt );
			else szFile = m_szPath;
		}

		CFileDialog d( TRUE, szFilter, szFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					   szFilter, this );
		if( d.DoModal() == IDCANCEL ) return;

		m_szPath = d.GetPathName();
		UpdateData( FALSE );

		if( !CheckFile() ) return;
	}
	else if( m_szName == _T("SINGLE TRIAL OR IMAGE") )
	{
		UpdateData( TRUE );
		CString szFile = m_szPath;
		CFileFind ff;

		if( !szFile.IsEmpty() )
		{
			TCHAR fname[ _MAX_FNAME ];
			_tsplitpath_s( szFile, NULL, 0, NULL, 0, fname, _MAX_FNAME, NULL, 0 );
			CString szFileName = fname;
			TRIM( szFileName );

			if( szFileName.IsEmpty() )
			{
				szFile += _T("*.");
				szFile += _LastExt;
			}
			else
			{
				if( ff.FindFile( szFile ) )
				{
					ff.FindNextFile();
					if( ff.IsDirectory() )
					{
						szFile += _T("\\*.");
						szFile += _LastExt;
					}
				}
			}
		}
		if( !szFile.IsEmpty() )
		{
			if( szFile.GetAt( szFile.GetLength() - 1 ) == '\\' )
			{
				szFile += _T("*.");
				szFile += _LastExt;
			}
		}

		CString szFilter;
		if( _LastExt == _T("HWR") )
			szFilter = _T("Handwriting Trials (*.HWR)|*.HWR|Handwriting Images (*.PCX;*.PNG;*.BMP;*.GIF;*.JPG)|*.PCX; *.PNG; *.BMP; *.GIF; *.JPG|Grip-Force Trials (*.FRD)|*.FRD|");
		else if( _LastExt == _T("FRD") )
			szFilter = _T("Grip-Force Trials (*.FRD)|*.FRD|Handwriting Images (*.PCX;*.PNG;*.BMP;*.GIF;*.JPG)|*.PCX; *.PNG; *.BMP; *.GIF; *.JPG|Handwriting Trials (*.HWR)|*.HWR|");
		else if( ( _LastExt == _T("PCX") ) || ( _LastExt == _T("BMP") ) || ( _LastExt == _T("PNG") ) ||
				 ( _LastExt == _T("GIF") ) || ( _LastExt == _T("JPG") ) )
			szFilter = _T("Handwriting Images (*.PCX;*.PNG;*.BMP;*.GIF;*.JPG)|*.PCX; *.PNG; *.BMP; *.GIF; *.JPG|Handwriting Trials (*.HWR)|*.HWR|Grip-Force Trials (*.FRD)|*.FRD|");

		CFileDialog d( TRUE, szFilter, szFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					   szFilter, this );
		if( d.DoModal() == IDCANCEL ) return;

		m_szPath = d.GetPathName();
		UpdateData( FALSE );

		_LastExt = d.GetFileExt();
		_LastExt.MakeUpper();
	}
	else
	{
		BROWSEINFO bi;
		memset( (LPVOID)&bi, 0, sizeof( bi ) );
		TCHAR szDisplayName[ _MAX_PATH ];
		szDisplayName[ 0 ] = '\0';
		bi.hwndOwner = GetSafeHwnd();
		bi.pidlRoot = NULL;
		bi.pszDisplayName = szDisplayName;
		bi.lpszTitle = _T("Select the folder of the data files to be imported:");
		bi.ulFlags = BIF_RETURNONLYFSDIRS;

		LPITEMIDLIST pIIL = ::SHBrowseForFolder( &bi );

		TCHAR szInitialDir[ _MAX_PATH ];
		BOOL bRet = ::SHGetPathFromIDList( pIIL, (char*)&szInitialDir );
		if( bRet )
		{
			if( szInitialDir != _T("") ) m_szPath = szInitialDir;

			LPMALLOC pMalloc;
			HRESULT hr = SHGetMalloc( &pMalloc );
			pMalloc->Free( pIIL );
			pMalloc->Release();
		}
	}

	UpdateData( FALSE );
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	OnChangeEditPath();
}

void WizardImportPath::OnChangeEditPath() 
{
	UpdateData( TRUE );
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	CWnd* pWnd = NULL;

	if( CheckFile() )
	{
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
		pWnd = GetDlgItem( IDC_BN_COLS );
		if( pWnd ) pWnd->EnableWindow( m_szName == _T("GRIPPER") || m_szName == _T("CSV") );
		pWnd = GetDlgItem( IDC_BN_VIEW );
		if( pWnd ) pWnd->EnableWindow( m_szName != _T("MOVALYZER") );
		pWnd = GetDlgItem( IDC_EDIT_SKIP );
		if( pWnd ) pWnd->EnableWindow( m_szName == _T("GRIPPER") || m_szName == _T("CSV") );
		pWnd = GetDlgItem( IDC_CHK_HEADERS );
		if( pWnd ) pWnd->EnableWindow( m_szName == _T("GRIPPER") || m_szName == _T("CSV") );
		pWnd = GetDlgItem( IDC_CHK_REMAINDER );
		if( pWnd ) pWnd->EnableWindow( m_szName == _T("GRIPPER") || m_szName == _T("CSV") );
		pWnd = GetDlgItem( IDC_EDIT_COUNT );
		if( pWnd ) pWnd->EnableWindow( ( m_szName == _T("GRIPPER") || m_szName == _T("CSV") ) && m_fRemainder );
		pWnd = GetDlgItem( IDC_CBO_DELIM );
		if( pWnd ) pWnd->EnableWindow( m_szName == _T("CSV") );
	}
	else
	{
		pParent->SetWizardButtons( PSWIZB_BACK );
		pWnd = GetDlgItem( IDC_BN_COLS );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_BN_VIEW );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_SKIP );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_HEADERS );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CHK_REMAINDER );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_EDIT_COUNT );
		if( pWnd ) pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_CBO_DELIM );
		if( pWnd ) pWnd->EnableWindow( FALSE );
	}
}

BOOL WizardImportPath::CheckFile( BOOL fNoMsg )
{
	if( m_szPath != _T("") )
	{
		CString szTest = m_szPath, szExt;
		szTest.MakeUpper();
		int nPos = szTest.Find( _T("\\CON\\") );
		if( nPos == -1 ) szTest.Find( _T("\\CON") );
		if( nPos != -1 )
		{
			if( !fNoMsg ) BCGPMessageBox( _T("Path cannot contain \'CON\'.") );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
			if( pWnd ) pWnd->SetFocus();
			return FALSE;
		}
		nPos = szTest.Find( _T("\\NUL\\") );
		if( nPos == -1 ) nPos = szTest.Find( _T("\\NUL") );
		if( nPos != -1 )
		{
			if( !fNoMsg ) BCGPMessageBox( _T("Path cannot contain \'NUL\'.") );
			CWnd* pWnd = GetDlgItem( IDC_EDIT_PATH );
			if( pWnd ) pWnd->SetFocus();
			return FALSE;
		}
		nPos = szTest.ReverseFind( '.' );
		if( nPos == -1 ) return FALSE;
		szExt = szTest.Mid( nPos + 1 );

		CFileFind ff;
		if( ff.FindFile( m_szPath ) )
		{
			ff.FindNextFile();
			if( ff.IsDirectory() ) return FALSE;

			if( m_szName == _T("SINGLE TRIAL OR IMAGE") )
			{
				if( ( szExt == _T("PCX") ) || ( szExt == _T("BMP") ) || ( szExt == _T("PNG") ) ||
					( szExt == _T("GIF") ) || ( szExt == _T("JPG") ) )
				{
					// verify image
					DWORD dwType = CXIMAGE_FORMAT_PCX;
					if( szExt == _T("BMP") ) dwType = CXIMAGE_FORMAT_BMP;
					else if( szExt == _T("PNG") ) dwType = CXIMAGE_FORMAT_PNG;
					else if( szExt == _T("GIF") ) dwType = CXIMAGE_FORMAT_GIF;
					else if( szExt == _T("JPG") ) dwType = CXIMAGE_FORMAT_JPG;

					CxImage img( m_szPath, dwType );
					if( !img.IsValid() )
					{
						if( !fNoMsg ) BCGPMessageBox( _T("This is not a valid image file.") );
						return FALSE;
					}

					if( dwType == CXIMAGE_FORMAT_PCX )
					{
						long lXRes = img.GetXDPI();
						bool fGrayScale = img.IsGrayScale();
						if( lXRes < 300 )
						{
							if( !fNoMsg && BCGPMessageBox( _T("Image resolution is below 300 dpi.\nProcessing can be inaccurate.\n\nContinue?"), MB_YESNO ) == IDNO )
								return FALSE;
						}
						if( !fGrayScale )
						{
// 14Apr10: GMB: Removed, as it seems unimportant to inform user...just decrease the color depth
// 							if( !fNoMsg && BCGPMessageBox( _T("Image is not gray scale.\nProcessing can be inaccurate.\n\nContinue with color depth adjustment?"), MB_YESNO ) == IDNO )
// 								return FALSE;
							img.DecreaseBpp( 8, TRUE );
						}
					}
				}
			}

			return TRUE;
		}
		else return FALSE;
	}
	else return FALSE;
}

void WizardImportPath::OnBnCols() 
{
	UpdateData( TRUE );
	if( CheckFile() )
	{
		m_dlgColSel.m_szFile = m_szPath;
		m_dlgColSel.m_nSkip = m_nSkip;
		m_dlgColSel.m_fHeaders = m_fHeaders;
		m_dlgColSel.m_szDelim = m_szDelim;
		m_dlgColSel.DoModal();
	}
}

void WizardImportPath::OnBnView() 
{
	CFileFind f;
	if( !f.FindFile( m_szPath ) ) return;

	CString szExt = m_szPath.Right( 3 );
	szExt.MakeUpper();
	if( ( szExt != _T("PCX") ) && ( szExt != _T("BMP") ) && ( szExt != _T("JPG") ) &&
		( szExt != _T("GIF") ) && ( szExt != _T("PNG") ) )
	{
		CString szCmd;
		szCmd.LoadString( IDS_EDITOR );
		szCmd += m_szPath;
		WinExec( szCmd, SW_SHOW );
	}
	else
	{
		CWaitCursor crs;
		ImageDlg d( m_szPath, this );
		d.DoModal();
	}
}

void WizardImportPath::OnBnClickedChkRemainder()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_COUNT );
	pWnd->EnableWindow( m_fRemainder );
}

BOOL WizardImportPath::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportPath::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

void WizardImportPath::OnChangeDelim()
{
	UpdateData( TRUE );
	if( m_szDelimName == _T("Comma") ) m_szDelim = _T(",");
	else if( m_szDelimName == _T("Space") ) m_szDelim = _T(" ");
	else if( m_szDelimName == _T("Tab") ) m_szDelim = _T("\t");
}

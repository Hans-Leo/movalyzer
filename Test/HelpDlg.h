//{{AFX_INCLUDES()
#include "webbrowser.h"
//}}AFX_INCLUDES
#if !defined(AFX_HELPDLG_H__B7AEE624_DFEF_11D3_8B06_00104BC7E2C8__INCLUDED_)
#define AFX_HELPDLG_H__B7AEE624_DFEF_11D3_8B06_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HelpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// HelpDlg dialog

class HelpDlg : public CDialog
{
// Construction
public:
	HelpDlg( BOOL fTutorial = FALSE, CWnd* pParent = NULL );
	HelpDlg( CString szURL, BOOL fTutorial = FALSE, CWnd* pParent = NULL );
	~HelpDlg();

// Dialog Data
	//{{AFX_DATA(HelpDlg)
	enum { IDD = IDD_HELP };
	CWebBrowser	m_wb;
	CString	m_szURL;
	//}}AFX_DATA
	BOOL	m_fTutorial;

// Overrides
	//{{AFX_VIRTUAL(HelpDlg)
	protected:
	virtual void OnCancel();
	virtual void PostNcDestroy();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	void Refresh();
protected:
	//{{AFX_MSG(HelpDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClickBnBack();
	afx_msg void OnClickBnForward();
	afx_msg void OnClickBnRefresh();
	afx_msg void OnBnGo();
	afx_msg void OnNavigateComplete2Browser(LPDISPATCH pDisp, VARIANT FAR* URL);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HELPDLG_H__B7AEE624_DFEF_11D3_8B06_00104BC7E2C8__INCLUDED_)

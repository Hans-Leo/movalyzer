#pragma once

// WizardRunExpSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpSheet

class WizardRunExpSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardRunExpSheet)

// Construction
public:
	WizardRunExpSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	virtual ~WizardRunExpSheet();

// Attributes
public:
	CString	m_szExpID;
	CString	m_szExp;
	CString	m_szGrpID;
	CString	m_szGrp;
	CString	m_szSubjID;
	CString	m_szSubj;

// Operations
public:
	void AddPages();

// Overrides

// message map functions
protected:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

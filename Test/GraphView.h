#if !defined(AFX_GRAPHVIEW_H__8BCE5CD3_9495_11D3_8A24_00104BC7E2C8__INCLUDED_)
#define AFX_GRAPHVIEW_H__8BCE5CD3_9495_11D3_8A24_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GraphView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGraphView view
//
// This class manages the recording window...currently only used for digitizer
// recordings and stimulus display

interface IDigitizer;
class StartView;

class CGraphView : public CView
{
	DECLARE_DYNCREATE(CGraphView)
protected:
	CGraphView();           // protected constructor used by dynamic creation
	virtual ~CGraphView();

// Attributes
public:
	StartView*	m_pStartView;		// HTML start page shown here
	IDigitizer*	m_pDigitizer;		// our digitizer object (interface)
	double		m_dTimeoutStart;	// how long allowed to start before timeout (secs)
	double		m_dTimeoutTicks;	// how long allowed after pen lifted before
									// timeout (secs) - once recording has started
	double		m_dTimeoutSecs;		// how long allowed for entire trial (secs)
	double		m_dTimeoutPrecue;	// how long precue stimulus is displayed
	double		m_dTimeoutLatency;	// how long latency is before imperative stimulus
									// & recording beings (secs)
	double		m_dTimeoutWarning;	// how long warning stimulus is displayed
	double		m_dTimeoutLatencyW;	// how long latency is before precue stimulus
	double		m_dTimeoutTicksNA;	// how long allowed before pen out of range timeout

	COLORREF	m_BGColor;

	BOOL		m_fBeganRecording;	// flag to indicate whether recording has begun
									// set when pen is first down
	long		m_nTicks;			// used to track current status of the timers
	long		m_nTicks2;
	long		m_nTicks3;
	long		m_nTicksNA;
	short		m_nRate;			// sampling rate of device
	short		m_nMinPressure;		// minimum pen pressure of digitizer
	double		m_dResolution;		// device resolution
	BOOL		m_fTest;			// flag to indicate whether we are just testing device
	BOOL		m_fInstrVis;		// flag to indicate whether instruction is visible
	CString		m_szFile;			// stores recording file name/location
	BOOL		m_fStimOnly;		// flag indicating if only displaying a stimulus/not recording
	BOOL		m_fWarning;			// flag indicating whether warning is currently being displayed
	BOOL		m_fPrecue;			// flag indicating whether precue is currently being displayed
	BOOL		fRecording;			// flag to indicate whether we are currently recording
	BOOL		fImmediately;		// Begin recording immediately, else on pen down
	BOOL		fStopWrongTarget;	// Stop recording if wrong target is hit
	BOOL		fStartFirstTarget;	// Start recording if reach first correct target
	BOOL		fStopLastTarget;	// Stop recording if reach last correct target
	CString		m_szExpID;			// Experiment for current trial
	CString		m_szCondID;			// Condition for current trial
	BOOL		m_fDoWait;			// After a trial, set this flag to allow user to watch results
									// and wait for a particular keypress to continue
	BOOL		fHideFeedback;		// visibility/feedback options
	BOOL		fHideShowAfter;
	BOOL		fHideShowAfterShow;
	BOOL		fHideShowAfterStrokes;
	double		dHideShowCount;
	long		m_nTicksHideShow;
	BOOL		m_fRealSize;

	BOOL		m_fCanContinue;		// can continue with next trial? (requires pen up)
	BOOL		m_fTrialEnded;		// has the previous trial ended?

	BOOL		m_fHideCursor;		// whether to hide mouse cursor during recordings (tablet only)
	BOOL		m_fCursorHidden;	// is cursor currently hidden

	// feedback min/max/values
	BOOL		m_fNoFeedback;
	double		m_dFBMin1;
	double		m_dFBMax1;
	double		m_dFBVal1;
	double		m_dFBValLast1;
	double		m_dFBMin2;
	double		m_dFBMax2;
	double		m_dFBVal2;
	double		m_dFBValLast2;

	// infor images
	CBitmap		m_bmpMsgWait;
	CBitmap		m_bmpMsgEnter;

// Operations
public:
	// Reset device
	void Reset();
	// Test device
	void Test();
	// Begin recording or display stimulus
	void Record( CString szExpID = _T(""), CString szCondID = _T(""),
				 CString szF = _T(""), CString szWStim = _T(""),
				 CString szPStim = _T(""), CString szStim = _T(""),
				 BOOL fStimOnly = FALSE, BOOL fTrialOnly = FALSE,
				 double dMagnetForce = 0.0 );
	// Stop recording or displaying stimulus
	void Stop();
	// Clear the recording window
	void Clear();
	// Set timeouts: starting, total, pen-lift, and precue
	void SetTimes( double dStart, double dSecs, double lTicks );
	void SetWarningTimes( double dWarning, double dLatency );
	void SetPrecueTimes( double dPrecue, double dLatency );
	void SetFlags( BOOL fImmed, BOOL fWrongTarget, BOOL fRealSize,
				   BOOL fFirstTarget, BOOL fLastTarget );
	// Set the recording settings: sampling rate, min pen pressure, and device resolution
	void SetRecordingSettings( short nRate, short nMinPressure,
							   double dResolution );
	void OnKeyDown(UINT nChar);	// for external calls

	// Get the device info
	BOOL GetDigitizerInfo( CString& szDesc,	// the device description
						   long& lRes,		// device resolution
						   long& lX,		// the x range (dimensions)
						   long& lY,		// the y range (dimensions)
						   short& nRate,	// sampling rate
						   BOOL& fInches );	// are the values in inches

// Overrides
public:
	virtual void OnInitialUpdate();
protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnSize( UINT nType, int cx, int cy );

// Implementation
public:
	void DrawFeedback( BOOL fStop = FALSE, double dMin = 0., double dMax = 0.,
					   double dVal = 0., BOOL fSecond = FALSE, BOOL fRefresh = FALSE );
protected:
#ifdef	_PAPER
	// if desired, this will draw a grid in the recording window during recording
	void DrawGrid( CDC* );
#endif	// _PAPER
	afx_msg LRESULT OnWTPacket(WPARAM, LPARAM);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnContextMenu( CWnd* pWnd, CPoint point );
	DECLARE_MESSAGE_MAP()
	// gripper callbacks
    afx_msg LRESULT OnFinished(WPARAM wParam, LPARAM lParam);
};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_GRAPHVIEW_H__8BCE5CD3_9495_11D3_8A24_00104BC7E2C8__INCLUDED_)

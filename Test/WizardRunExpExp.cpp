// WizardRunExpExp.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardRunExpExp.h"
#include "WizardRunExpSheet.h"
#include "WizardRunExpGrp.h"
#include "..\DataMod\DataMod.h"
#include "WizardSetupSheet.h"
#include "Experiments.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpExp property page

IMPLEMENT_DYNCREATE(WizardRunExpExp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardRunExpExp, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_CREATEEXP, OnBnCreateexp)
	ON_CBN_SELCHANGE(IDC_CBO_EXPS, OnSelchangeCboExps)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardRunExpExp::WizardRunExpExp() : CBCGPPropertyPage(WizardRunExpExp::IDD)
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_szType = _T("");
	m_nType = 0;		// EXP_TYPE_HANDWRITING (default)
	m_fUpdated = FALSE;
	m_fNew = FALSE;
}

WizardRunExpExp::~WizardRunExpExp()
{
}

void WizardRunExpExp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_EXPS, m_Cbo);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szDesc);
	DDX_Text(pDX, IDC_TXT_TYPE, m_szType);
}

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpExp message handlers

void WizardRunExpExp::OnBnCreateexp() 
{
	WizardSetupSheet d( this );
	if( d.DoModal() != ID_WIZFINISH ) return;

	m_szID = d.m_szExpID;
	m_szDesc = d.m_szExpDesc;
	CString szItem, szDelim;
	::GetDelimiter( szDelim );
	szItem.Format( _T("%s%s%s"), m_szDesc, szDelim, m_szID );
	m_Cbo.SetCurSel( m_Cbo.AddString( szItem ) );
	m_nType = d.m_nExpType;
	switch( m_nType )
	{
		case EXP_TYPE_HANDWRITING:
			m_szType = _T("Handwriting");
			break;
		case EXP_TYPE_GRIPPER:
			m_szType = _T("Grip-Force");
			break;
		case EXP_TYPE_IMAGE:
			m_szType = _T("Handwriting Image");
			break;
		default:
			m_szType = _T("Unknown");
	}

	UpdateData( FALSE );
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

BOOL WizardRunExpExp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_EXP_RUN );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing experiment."), _T("Existing Experiment") );
	pWnd = GetDlgItem( IDC_BN_CREATEEXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new experiment to use."), _T("New Experiment") );

	// was there a last experiment run
	CString szExp, szGrp, szSubj;
	::GetLastExperimentInfo( szExp, szGrp, szSubj );

	// Fill experiment list
	Experiments exp;
	if( !exp.IsValid() ) return TRUE;
	CString szID, szItem;
	int nSel = -1;
	short nType = EXP_TYPE_HANDWRITING;
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		exp.GetID( szID );
		exp.GetDescriptionWithID( szItem );
		exp.GetType( nType );

		if( nType != EXP_TYPE_IMAGE )
		{
			if( szID == szExp ) nSel = m_Cbo.AddString( szItem );
			else m_Cbo.AddString( szItem );
		}

		hr = exp.GetNext();
	}

	if( nSel != -1 )
	{
		m_Cbo.SetCurSel( nSel );
		OnSelchangeCboExps();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardRunExpExp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardRunExpExp::OnWizardBack() 
{
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardRunExpExp::OnSetActive() 
{
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	if( m_szID != _T("") ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

void WizardRunExpExp::OnSelchangeCboExps() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szDesc;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	Experiments exp;
	if( !exp.IsValid() ) return;
	HRESULT hr = exp.Find( m_szID );
	if( FAILED( hr ) )
	{
		m_fNew = TRUE;
		return;
	}
	m_fNew = FALSE;
	exp.GetNotes( m_szNotes );
	short nVal;
	exp.GetType( nVal );
	m_nType = nVal;
	switch( m_nType )
	{
		case EXP_TYPE_HANDWRITING:
			m_szType = _T("Handwriting");
			break;
		case EXP_TYPE_GRIPPER:
			m_szType = _T("Grip-Force");
			break;
		case EXP_TYPE_IMAGE:
			m_szType = _T("Handwriting Image");
			break;
		default:
			m_szType = _T("Unknown");
	}

	UpdateData( FALSE );

	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

LRESULT WizardRunExpExp::OnWizardNext() 
{
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	WizardRunExpGrp* pGrp = (WizardRunExpGrp*)pParent->GetPage( 2 );
	if( pGrp->m_szID != _T("") )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardRunExpExp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("runexperiment_wizard.html"), this );
	return TRUE;
}

BOOL WizardRunExpExp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

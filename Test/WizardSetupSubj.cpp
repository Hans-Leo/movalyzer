// WizardSetupSubj.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupSubj.h"
#include "WizardSetupSheet.h"
#include "..\DataMod\DataMod.h"
#include "AddSubjectDlg.h"
#include "PasswordDlg.h"
#include "Subjects.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardSetupSubj property page

IMPLEMENT_DYNCREATE(WizardSetupSubj, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardSetupSubj, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_CREATESUBJ, OnBnCreatesubj)
	ON_CBN_SELCHANGE(IDC_CBO_SUBJS, OnSelchangeCboSubjs)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupSubj::WizardSetupSubj() : CBCGPPropertyPage(WizardSetupSubj::IDD)
{
	m_szID = _T("");
	m_szName = _T("");
}

WizardSetupSubj::~WizardSetupSubj()
{
}

void WizardSetupSubj::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_SUBJS, m_Cbo);
	DDX_CBString(pDX, IDC_CBO_SUBJS, m_szSubj);
	DDX_Text(pDX, IDC_TXT_SUBJID, m_szID);
	DDX_Text(pDX, IDC_TXT_SUBJNAME, m_szName);
	DDX_Text(pDX, IDC_TXT_SUBJCODE, m_szCode);
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupSubj message handlers

void WizardSetupSubj::OnBnCreatesubj() 
{
	CStringList existing;
	CString szID;
	Subjects subj;

	HRESULT hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetID( szID );
		existing.AddTail( szID );
		hr = subj.GetNext();
	}

	AddSubjectDlg d( &existing, this, TRUE );
	if( d.DoModal() )
	{
		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( pos )
		{
			CString szItem = d.m_lstNew.GetNext( pos );
			CString szDelim;
			::GetDelimiter( szDelim );
			int nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				m_szID = szItem.Mid( nPos + 2 );
				TRIM( m_szID );
				if( SUCCEEDED( subj.Find( m_szID ) ) )
				{
					subj.GetCode( m_szCode );
					subj.GetNameFirst( m_szFirst );
					subj.GetNameLast( m_szLast );
					subj.GetName( m_szName );

					UpdateData( FALSE );
					m_Cbo.SetCurSel( -1 );

					WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
					pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
				}
			}
		}
	}
}

void WizardSetupSubj::OnSelchangeCboSubjs() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szName;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );

	Subjects subj;
	if( !subj.IsValid() ) return;
	if( FAILED( subj.Find( m_szID ) ) ) return;

	subj.GetCode( m_szCode );
	subj.GetNameFirst( m_szFirst );
	subj.GetNameLast( m_szLast );
	subj.GetName( m_szName );
	subj.GetNotes( m_szNotes );
	subj.GetPrvNotes( m_szPrvNotes );
	subj.GetDateAdded( m_DateAdded );

	m_szSubj = szItem;
	UpdateData( FALSE );

	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

BOOL WizardSetupSubj::OnSetActive() 
{
	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardSetupSubj::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_EXP_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_CBO_SUBJS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing subject for this group."), _T("Existing Subject") );
	pWnd = GetDlgItem( IDC_BN_CREATESUBJ );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new subject to use for this group."), _T("New Subject") );

	// Fill subject list
	Subjects subj;
	if( !subj.IsValid() ) return TRUE;
	CString szItem;
	HRESULT hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetNameWithCodeID( szItem, ::IsPrivacyOn() );
		m_Cbo.AddString( szItem );

		hr = subj.GetNext();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardSetupSubj::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPPropertyPage::PreTranslateMessage(pMsg);
}

BOOL WizardSetupSubj::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newexperiment.html#wizard"), this );
	return TRUE;
}

BOOL WizardSetupSubj::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

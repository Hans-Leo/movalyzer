// WizardRunExpEnd.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardRunExpEnd.h"
#include <direct.h>

#include "WizardRunExpSheet.h"
#include "..\DataMod\DataMod.h"
#include "WizardRunExpExp.h"
#include "WizardRunExpGrp.h"
#include "WizardRunExpSubj.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "Subjects.h"
#include "Groups.h"
#include "Experiments.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

WizardRunExpExp* _pExp = NULL;
WizardRunExpGrp* _pGrp = NULL;
WizardRunExpSubj* _pSubj = NULL;

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpEnd property page

IMPLEMENT_DYNCREATE(WizardRunExpEnd, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardRunExpEnd, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardRunExpEnd::WizardRunExpEnd() : CBCGPPropertyPage(WizardRunExpEnd::IDD)
{
	m_szExpID = _T("");
	m_szExpDesc = _T("");
	m_szGrpID = _T("");
	m_szGrpDesc = _T("");
	m_szSubjID = _T("");
	m_szSubjName = _T("");
}

WizardRunExpEnd::~WizardRunExpEnd()
{
}

void WizardRunExpEnd::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szExpID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szExpDesc);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szGrpID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szGrpDesc);
	DDX_Text(pDX, IDC_TXT_SUBJID, m_szSubjID);
	DDX_Text(pDX, IDC_TXT_SUBJNAME, m_szSubjName);
	DDX_Text(pDX, IDC_TXT_TYPE, m_szExpType);
}

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpEnd message handlers

BOOL WizardRunExpEnd::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	_pExp = (WizardRunExpExp*)pParent->GetPage( 1 );
	_pGrp = (WizardRunExpGrp*)pParent->GetPage( 2 );
	_pSubj = (WizardRunExpSubj*)pParent->GetPage( 3 );

	m_szExpID = _pExp->m_szID;
	pParent->m_szExpID = m_szExpID;
	m_szExpDesc = _pExp->m_szDesc;
	pParent->m_szExp = m_szExpDesc;
	m_szExpType = _pExp->m_szType;
	m_szGrpID = _pGrp->m_szID;
	pParent->m_szGrpID = m_szGrpID;
	m_szGrpDesc = _pGrp->m_szDesc;
	pParent->m_szGrp = m_szGrpDesc;
	m_szSubjID = _pSubj->m_szID;
	pParent->m_szSubjID = m_szSubjID;
	if( ::IsPrivacyOn() )
		m_szSubjName.Format( _T("%s %s (%s)"), PVT_NAME_FIRST, PVT_NAME_LAST, _pSubj->m_szCode );
	else
		m_szSubjName.Format( _T("%s %s (%s)"), _pSubj->m_szFirst, _pSubj->m_szLast, _pSubj->m_szCode );
	pParent->m_szSubj = m_szSubjName;

	UpdateData( FALSE );

	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardRunExpEnd::ConfirmCreate()
{
	CFileFind ff, ff2;
	BOOL fCont = TRUE, fRefresh = FALSE;
	int nCnt = 0;
	CString szFile, szPath, szTemp, szNum;

	// Data root path
	::GetDataPathRoot( szPath );
	if( szPath != _T("") ) szPath += _T("\\");

	// Ensure directories exists
	szTemp.Format( _T("%s\\%s"), szPath, m_szExpID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	szTemp.Format( _T("%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	szTemp.Format( _T("%s\\%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID, m_szSubjID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}

	// Create new/add exp, cond, grp, subjects where necessary
	HRESULT hr;
	// ...Experiment
	if( _pExp->m_fNew )
	{
		Experiments exp;
		if( !exp.IsValid() ) return FALSE;

		exp.PutID( m_szExpID );
		exp.PutDescription( m_szExpDesc );
		hr = exp.Add();
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPADD_ERR );
			return FALSE;
		}

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new experiment %s."), m_szExpID );
		OutputAMessage( szTemp );
	}
	// ...Group
	Groups grp;
	if( !grp.IsValid() ) return FALSE;
	hr = grp.Find( m_szGrpID );
	if( FAILED( hr ) )
	{
		grp.PutID( m_szGrpID );
		grp.PutDescription( m_szGrpDesc );
		grp.PutNotes( _pGrp->m_szNotes );

		hr = grp.Add();
		if( FAILED( hr ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new group %s."), m_szGrpID );
		OutputAMessage( szTemp );
	}
	// ...Subject
	Subjects subj;
	if( !subj.IsValid() ) return FALSE;
	hr = subj.Find( m_szSubjID );
	if( FAILED( hr ) )
	{
		subj.PutID( m_szSubjID );
		subj.PutCode( _pSubj->m_szCode );
		subj.PutNameLast( _pSubj->m_szLast );
		subj.PutNameFirst( _pSubj->m_szFirst );
		subj.PutNotes( _pSubj->m_szNotes );
		subj.PutDateAdded( COleDateTime::GetCurrentTime() );

		hr = subj.Add();
		if( FAILED( hr ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new subject %s."), m_szSubjID );
		OutputAMessage( szTemp );
	}
	// ...ExperimentMember
	IExperimentMember* pEM = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return FALSE;
	}
	hr = pEM->Find( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString(),
					m_szSubjID.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->put_ExperimentID( m_szExpID.AllocSysString() );
		pEM->put_GroupID( m_szGrpID.AllocSysString() );
		pEM->put_SubjectID( m_szSubjID.AllocSysString() );
		hr = pEM->Add();
		if( FAILED( hr ) )
		{
			pEM->Release();
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" WizardRunExpEnd::ConfirmCreate") );
			return FALSE;
		}
		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new group %s and subject %s to experiment %s."),
					   m_szGrpID, m_szSubjID, m_szExpID );
		OutputAMessage( szTemp );
	}
	pEM->Release();

	if( fRefresh )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
		if( pLV ) pLV->Refresh();
	}

	return TRUE;
}

BOOL WizardRunExpEnd::OnWizardFinish() 
{
	if( !ConfirmCreate() ) return FALSE;

	// update last experiment run info
	::SetLastExperimentInfo( m_szExpID, m_szGrpID, m_szSubjID );

	return CBCGPPropertyPage::OnWizardFinish();
}

BOOL WizardRunExpEnd::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("runexperiment_wizard.html"), this );
	return TRUE;
}

BOOL WizardRunExpEnd::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

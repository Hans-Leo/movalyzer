// WizardImportSubj.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportSubj.h"
#include "WizardImportSheet.h"
#include "..\DataMod\DataMod.h"
#include "AddSubjectDlg.h"
#include "WizardImportGenStart.h"
#include "WizardImportExp.h"
#include "WizardImportGrp.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "PasswordDlg.h"
#include "Subjects.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportSubj property page

IMPLEMENT_DYNCREATE(WizardImportSubj, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportSubj, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_CREATESUBJ, OnBnCreatesubj)
	ON_CBN_SELCHANGE(IDC_CBO_SUBJS, OnSelchangeCboSubjs)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportSubj::WizardImportSubj() : CBCGPPropertyPage(WizardImportSubj::IDD)
{
	m_szID = _T("");
	m_szName = _T("");
	m_szSubj = _T("");
	m_pParent = NULL;
	m_nExpIdx = 3;
	m_nGrpIdx = 5;
}

WizardImportSubj::~WizardImportSubj()
{
}

void WizardImportSubj::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_SUBJS, m_Cbo);
	DDX_CBString(pDX, IDC_CBO_SUBJS, m_szSubj);
	DDX_Text(pDX, IDC_TXT_SUBJID, m_szID);
	DDX_Text(pDX, IDC_TXT_SUBJNAME, m_szName);
	DDX_Text(pDX, IDC_TXT_SUBJCODE, m_szCode);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportSubj message handlers

void WizardImportSubj::OnBnCreatesubj() 
{
	AddSubjectDlg d( &m_existing, this, TRUE );
	if( d.DoModal() == IDOK )
	{
		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( pos )
		{
			CString szItem = d.m_lstNew.GetNext( pos );
			CString szDelim;
			::GetDelimiter( szDelim );
			int nPos = szItem.Find( szDelim );
			int nPos2 = szItem.Find( _T(")") );
			if( ( nPos != -1 ) && ( nPos2 != -1 ) )
			{
				m_szID = szItem.Mid( nPos + 2 );
				TRIM( m_szID );
				m_szName = szItem.Mid( nPos2 + 2, nPos - nPos2 - 2 );
				nPos = m_szName.Find( "," );
				m_szLast = m_szName.Left( nPos );
				m_szFirst = m_szName.Mid( nPos + 2 );
				nPos = szItem.Find( ")" );
				m_szCode = szItem.Mid( 1, nPos - 1 );

				UpdateData( FALSE );

				if( ::IsPrivacyOn() )
					szItem.Format( _T("(%s) %s, %s%s%s"), m_szCode, PVT_NAME_LAST, PVT_NAME_FIRST, szDelim, m_szID );
				else
					szItem.Format( _T("(%s) %s, %s%s%s"), m_szCode, m_szLast, m_szFirst, szDelim, m_szID );
				int nSel = m_Cbo.AddString( szItem );
				m_Cbo.SetCurSel( nSel );
				m_existing.AddTail( m_szID );

				m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
			}
		}
	}
}

void WizardImportSubj::OnSelchangeCboSubjs() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szName;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );

	Subjects subj;
	if( !subj.IsValid() ) return;

	if( FAILED( subj.Find( m_szID ) ) ) return;

	subj.GetCode( m_szCode );
	subj.GetNameFirst( m_szFirst );
	subj.GetNameLast( m_szLast );
	subj.GetName( m_szName );
	subj.GetNotes( m_szNotes );
	subj.GetPrvNotes( m_szPrvNotes );
	subj.GetDateAdded( m_DateAdded );

	m_szSubj = szItem;
	UpdateData( FALSE );

	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

BOOL WizardImportSubj::OnSetActive() 
{
	CString szID, szFirst, szLast, szDelim, szItem;
	::GetDelimiter( szDelim );
	HRESULT hr2 = E_FAIL;

	WizardImportExp* pExp = (WizardImportExp*)m_pParent->GetPage( m_nExpIdx );
	WizardImportGrp* pGrp = (WizardImportGrp*)m_pParent->GetPage( m_nGrpIdx );
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CLeftView* pLV = pMF->GetLeftView();
	CString szExpID = pExp->m_szID;
	CString szGrpID = pGrp->m_szID;
	if( ( szExpID != m_szExpID ) || ( szGrpID != m_szGrpID ) )
	{
		m_Cbo.ResetContent();
		m_szExpID = szExpID;
		m_szGrpID = szGrpID;
		m_szID.Empty();
		m_szCode.Empty();
		m_szName.Empty();
		m_szSubj.Empty();
	}
	else return CBCGPPropertyPage::OnSetActive();

	// Fill subject list
	Subjects subj;
	if( !subj.IsValid() ) return TRUE;

	m_existing.RemoveAll();
	HRESULT hr = subj.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		subj.GetID( szID );

		if( !pExp->m_fNew )
		{
			hr2 = pLV->m_pEML->Find( szExpID.AllocSysString(),
									 szGrpID.AllocSysString(),
									 szID.AllocSysString() );
		}
		else hr2 = S_OK;
		if( SUCCEEDED( hr2 ) )
		{
			subj.GetNameWithCodeID( szItem, ::IsPrivacyOn() );
			m_Cbo.AddString( szItem );
			m_existing.AddTail( szID );
		}

		hr = subj.GetNext();
	}

	if( m_szID != _T("") ) m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardImportSubj::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing subject for the selected group.") );
	pWnd = GetDlgItem( IDC_BN_CREATEEXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new subject to use for this group.") );

	// File/class is used in multiple wizards with different # of pages
	// DEFAULT page index # is in constructor (for import wizard)
	// if Generate data wizard, adjust index # appropriately
	m_pParent = (CBCGPPropertySheet*)GetParent();
	pWnd = m_pParent->GetPage( 0 );
	if( pWnd->IsKindOf( RUNTIME_CLASS( WizardImportGenStart ) ) )
	{
		m_nExpIdx--;
		m_nGrpIdx--;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportSubj::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardImportSubj::OnWizardBack() 
{
	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardImportSubj::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	CBCGPPropertySheet* pParent = (CBCGPPropertySheet*)GetParent();
	if( pParent )
	{
		if( pParent->IsKindOf( RUNTIME_CLASS( WizardImportSheet ) ) )
			::GoToHelp( _T("importexport.html"), this );
		else
			::GoToHelp( _T("datageneration_wizard.html"), this );
	}

	return TRUE;
}

BOOL WizardImportSubj::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

// ColumnSelectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "ColumnSelectDlg.h"
#include "OrderColDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	COLS_NEEDED			3

/////////////////////////////////////////////////////////////////////////////
// ColumnSelectDlg dialog

BEGIN_MESSAGE_MAP(ColumnSelectDlg, CBCGPDialog)
END_MESSAGE_MAP()

ColumnSelectDlg::ColumnSelectDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(ColumnSelectDlg::IDD, pParent)
{
	m_nSkip = 0;
	m_fHeaders = FALSE;
	m_szDelim = _T(",");
}

void ColumnSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
}

/////////////////////////////////////////////////////////////////////////////
// ColumnSelectDlg message handlers

BOOL ColumnSelectDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select/highlight the desired columns."), _T("Columns") );
	pWnd = GetDlgItem( IDOK );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_ACCEPT, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_CLOSE_NOSAV, _T("Cancel") );

	// Currently only for gripper data
	if( m_szFile != _T("") )
	{
		CStdioFile f;
		if( !f.Open( m_szFile, CFile::modeRead | CFile::typeText ) )
		{
			CString szMsg;
			szMsg.Format( _T("Unable to open file %s for reading."), m_szFile );
			BCGPMessageBox( szMsg );
			return TRUE;
		}

		CString szLine;
		// skip m_nSkip # of lines
		for( int i = 0; i < m_nSkip; i++ )
		{
			if( !f.ReadString( szLine ) )
			{
				BCGPMessageBox( _T("Error reading column information.") );
				return TRUE;
			}
		}
		// Columns (delimiter is set from parent dialog)
		if( m_fHeaders && !f.ReadString( szLine ) )
		{
			BCGPMessageBox( _T("Error reading column information.") );
			return TRUE;
		}
		int nItem = 0, nPos = 0;
		CString szItem;
		// determine # of columns & create generic headers if no headers selected
		// read next line which should be the data
		if( !m_fHeaders && !f.ReadString( szLine ) )
		{
			BCGPMessageBox( _T("Error reading column information.") );
			return TRUE;
		}
		nPos = szLine.Find( m_szDelim );
		while( nPos != -1 )
		{
			nItem++;
			if( m_fHeaders ) szItem = szLine.Left( nPos );
			else szItem.Format( _T("Column %d"), nItem );
			szLine = szLine.Mid( nPos + 1 );
			m_list.AddString( szItem );

			nPos = szLine.Find( m_szDelim );
		}
		// Last one
		szItem.Format( _T("Column %d"), ++nItem );
		m_list.AddString( m_fHeaders ? szLine : szItem );

		if( m_list.GetCount() <= 1 )
		{
			BCGPMessageBox( _T("Unable to parse columns. Please check delimiter.") );
			CBCGPDialog::OnCancel();
		}
	}

	if( m_lstColNum.GetCount() > 0 )
	{
		CString szItem;
		int nItem;
		POSITION pos = m_lstColName.GetHeadPosition();
		while( pos )
		{
			szItem = m_lstColName.GetNext( pos );
			nItem = m_list.FindStringExact( -1, szItem );
			if( nItem != LB_ERR ) m_list.SetSel( nItem );
		}
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL ColumnSelectDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void ColumnSelectDlg::OnOK()
{
	m_lstColNum.RemoveAll();
	m_lstColName.RemoveAll();

	int nCnt = m_list.GetSelCount();
	if( nCnt != COLS_NEEDED )
	{
		CString szMsg;
		szMsg.Format( _T("Please select %d columns (x,y,z)."), COLS_NEEDED );
		BCGPMessageBox( szMsg );
		return;
	}

	CString szItem;
	int nItems[ COLS_NEEDED ];
	m_list.GetSelItems( COLS_NEEDED, nItems );

	for( int i = 0; i < COLS_NEEDED; i++ )
	{
		szItem.Format( _T("%d"), nItems[ i ] + 1 );
		m_lstColNum.AddTail( szItem );

		m_list.GetText( nItems[ i ], szItem );
		m_lstColName.AddTail( szItem );
	}

	OrderColDlg dlgOrder( &m_lstColNum, &m_lstColName, this );
	if( dlgOrder.DoModal() == IDCANCEL ) return;

	CBCGPDialog::OnOK();
}

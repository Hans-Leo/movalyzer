#pragma once

// WizardImportSubj.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportSubj dialog

class WizardImportSubj : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportSubj)

// Construction
public:
	WizardImportSubj();
	~WizardImportSubj();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_SUBJ };
	CBCGPComboBox	m_Cbo;
	CString			m_szID;
	CString			m_szCode;
	CString			m_szName;
	CString			m_szSubj;
	CString			m_szFirst;
	CString			m_szLast;
	COleDateTime	m_DateAdded;
	CString			m_szNotes;
	CString			m_szPrvNotes;
	CString			m_szExpID;
	CString			m_szGrpID;
	CStringList		m_existing;
	CBCGPPropertySheet* m_pParent;
	int				m_nExpIdx;
	int				m_nGrpIdx;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardBack();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnCreatesubj();
	afx_msg void OnSelchangeCboSubjs();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

// AppLookDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "AppLookDlg.h"
#include "MainFrm.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAppLookDlg dialog

BEGIN_MESSAGE_MAP(CAppLookDlg, CBCGPDialog)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	ON_CBN_SELENDOK(IDC_APP_LOOK, OnAppLook)
	ON_CBN_SELENDOK(IDC_STYLE, OnStyle)
	ON_BN_CLICKED(IDC_BN_CUSTOMIZE, OnCustomize)
END_MESSAGE_MAP()

CAppLookDlg::CAppLookDlg( BOOL bStartup, CWnd* pParent )
	: CBCGPDialog( CAppLookDlg::IDD, pParent ), m_bStartup( bStartup )
{
	m_bShowAtStartup = FALSE;
	m_bOneNoteTabs = TRUE;
	m_bDockTabColors = TRUE;
	m_bRoundedTabs = TRUE;
	m_bCustomTooltips = TRUE;
	m_nAppLook = 5;
	m_nStyle = 0;
	m_clrCustom = (COLORREF)-1;

	m_nAppLook = theApp.GetInt( _T("AppLook"), 5 );
	m_nStyle = theApp.GetInt( _T("AppStyle"), 0 );
	m_clrCustom = theApp.GetInt( _T("LookColor"), m_clrCustom );

//	m_bShowAtStartup = theApp.GetInt (_T("ShowAppLookAtStartup"), TRUE);
	m_bOneNoteTabs = theApp.GetInt (_T("OneNoteTabs"), TRUE);
	m_bDockTabColors = theApp.GetInt (_T("DockTabColors"), TRUE);
	m_bRoundedTabs = theApp.GetInt (_T("RoundedTabs"), TRUE);
	m_bCustomTooltips = theApp.GetInt (_T("CustomTooltips"), TRUE);

	m_dlgCal.m_CS.m_appbg1 = theApp.GetDWORD( _T("CustomApp1"), m_dlgCal.m_CS.m_appbg1 );
	m_dlgCal.m_CS.m_appbg2 = theApp.GetDWORD( _T("CustomApp2"), m_dlgCal.m_CS.m_appbg2 );
	m_dlgCal.m_CS.m_barbg1 = theApp.GetDWORD( _T("CustomBG1"), m_dlgCal.m_CS.m_barbg1 );
	m_dlgCal.m_CS.m_barbg2 = theApp.GetDWORD( _T("CustomBG2"), m_dlgCal.m_CS.m_barbg2 );
	m_dlgCal.m_CS.m_barcaptionbg1 = theApp.GetDWORD( _T("CustomCaption1"), m_dlgCal.m_CS.m_barcaptionbg1 );
	m_dlgCal.m_CS.m_barcaptionbg2 = theApp.GetDWORD( _T("CustomCaption2"), m_dlgCal.m_CS.m_barcaptionbg2 );
	m_dlgCal.m_CS.m_gripper1 = theApp.GetDWORD( _T("CustomGripper1"), m_dlgCal.m_CS.m_gripper1 );
	m_dlgCal.m_CS.m_gripper2 = theApp.GetDWORD( _T("CustomGripper2"), m_dlgCal.m_CS.m_gripper2 );
	m_dlgCal.m_CS.m_taberase1 = theApp.GetDWORD( _T("CustomTabErase1"), m_dlgCal.m_CS.m_taberase1 );
	m_dlgCal.m_CS.m_taberase2 = theApp.GetDWORD( _T("CustomTabErase2"), m_dlgCal.m_CS.m_taberase2 );
	m_dlgCal.m_CS.m_splitter1 = theApp.GetDWORD( _T("CustomSplitter1"), m_dlgCal.m_CS.m_splitter1 );
	m_dlgCal.m_CS.m_splitter2 = theApp.GetDWORD( _T("CustomSplitter2"), m_dlgCal.m_CS.m_splitter2 );
	m_dlgCal.m_CS.m_menuhl1 = theApp.GetDWORD( _T("CustomMenuHighlight1"), m_dlgCal.m_CS.m_menuhl1 );
	m_dlgCal.m_CS.m_menuhl2 = theApp.GetDWORD( _T("CustomMenuHighlight2"), m_dlgCal.m_CS.m_menuhl2 );
	m_dlgCal.m_CS.m_menubartext = theApp.GetDWORD( _T("CustomMenuBarText"), m_dlgCal.m_CS.m_menubartext );
}


void CAppLookDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STYLE, m_wndStyle);
	DDX_Control(pDX, IDC_ROUNDED_TABS, m_wndRoundedTabs);
	DDX_Control(pDX, IDC_DOCK_TAB_COLORS, m_wndDockTabColors);
	DDX_Control(pDX, IDC_ONENOTE_TABS, m_wndOneNoteTabs);
	DDX_Check(pDX, IDC_ONENOTE_TABS, m_bOneNoteTabs);
	DDX_Check(pDX, IDC_DOCK_TAB_COLORS, m_bDockTabColors);
	DDX_Check(pDX, IDC_ROUNDED_TABS, m_bRoundedTabs);
	DDX_Check(pDX, IDC_CUSTOM_TOOLTIPS, m_bCustomTooltips);
	DDX_CBIndex(pDX, IDC_APP_LOOK, m_nAppLook);
	DDX_CBIndex(pDX, IDC_STYLE, m_nStyle);
}

/////////////////////////////////////////////////////////////////////////////
// CAppLookDlg message handlers

BOOL CAppLookDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// fill combo boxes
	CBCGPComboBox* pCB = (CBCGPComboBox*)GetDlgItem( IDC_APP_LOOK );
	if( pCB )
	{
		pCB->ResetContent();
		pCB->AddString( _T("Microsoft� Windows� XP") );
		pCB->AddString( _T("Microsoft� Visual Studio� 97") );
		pCB->AddString( _T("Microsoft� Visual Studio� 2003") );
		pCB->AddString( _T("Microsoft� Visual Studio� 2005") );
		pCB->AddString( _T("Microsoft� Office 2003") );
		pCB->AddString( _T("Microsoft� Office 2007") );
		pCB->AddString( _T("Microsoft� Visual Studio� 2008") );
		pCB->AddString( _T("Microsoft� Visual Studio� 2010") );
		pCB->AddString( _T("Microsoft� Office 2010") );
		pCB->AddString( _T("Carbon") );
		pCB->AddString( _T("Custom...") );
	}
	pCB = (CBCGPComboBox*)GetDlgItem( IDC_STYLE );
	if( pCB )
	{
		pCB->ResetContent();
		pCB->AddString( _T("Blue") );
		pCB->AddString( _T("Black") );
		pCB->AddString( _T("Silver") );
		if( m_nAppLook == 5 )
		{
			pCB->AddString( _T("Aqua") );
			pCB->AddString( _T("Custom...") );
		}
	}

	UpdateData (FALSE);
	OnAppLook ();

	if( m_bStartup )
	{
		SetLook ();

		if (!m_bShowAtStartup)
		{
			EndDialog (IDCANCEL);
			return TRUE;
		}

		CRect rectBtn;

		// Hide "Cancel" button:
		CWnd* pWnd = GetDlgItem( IDCANCEL );
		pWnd->GetWindowRect (rectBtn);
		ScreenToClient (rectBtn);
		pWnd->EnableWindow( FALSE );
		pWnd->ShowWindow( SW_HIDE );

		pWnd = GetDlgItem( IDOK );
		pWnd->MoveWindow( rectBtn );
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAppLookDlg::OnOK() 
{
	SetLook ();
	CBCGPDialog::OnOK();
}

void CAppLookDlg::SetLook(  BOOL fExternal, CBCGPFrameWnd* pMainWnd )
{
	CWaitCursor wait;

	if( !fExternal ) UpdateData ();

	CMainFrame* pMainFrame = (CMainFrame*)pMainWnd;
	if( !pMainWnd ) pMainFrame = DYNAMIC_DOWNCAST( CMainFrame, AfxGetMainWnd() );
	if( pMainFrame != NULL ) pMainFrame->LockWindowUpdate();

	CBCGPTabbedControlBar::m_StyleTabWnd = CBCGPTabWnd::STYLE_3D;
	CBCGPVisualManager2007::SetCustomColor( (COLORREF)-1 );

	switch( m_nAppLook )
	{
	case 0:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPWinXPVisualManager));
		CBCGPWinXPVisualManager::m_b3DTabsXPTheme = TRUE;
		break;

	case 1:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManager));
		break;

	case 2:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManagerXP));
		break;

	case 3:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManagerVS2005));

		if (m_bRoundedTabs)
		{
			CBCGPTabbedControlBar::m_StyleTabWnd = CBCGPTabWnd::STYLE_3D_ROUNDED;
		}

		CBCGPVisualManagerVS2005::m_bRoundedAutohideButtons = m_bRoundedTabs;
		break;

	case 4:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManager2003));
		break;

	case 5:
		switch (m_nStyle)
		{
			case 0:
				CBCGPVisualManager2007::SetStyle (CBCGPVisualManager2007::VS2007_LunaBlue);
				break;

			case 1:
				CBCGPVisualManager2007::SetStyle (CBCGPVisualManager2007::VS2007_ObsidianBlack);
				break;

			case 2:
				CBCGPVisualManager2007::SetStyle (CBCGPVisualManager2007::VS2007_Silver);
				break;

			case 3:
				CBCGPVisualManager2007::SetStyle (CBCGPVisualManager2007::VS2007_Aqua);
				break;

			case 4:
				theApp.m_clrCustom = m_clrCustom;
				theApp.WriteInt( _T("LookColor"), m_clrCustom );
				CBCGPVisualManager2007::SetStyle (CBCGPVisualManager2007::VS2007_LunaBlue);
				CBCGPVisualManager2007::SetCustomColor(
					theApp.m_clrCustom == (COLORREF)-1 ? theApp.m_clrCustomDef : theApp.m_clrCustom );
				break;
		}
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManager2007));
		break;
	case 6:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManagerVS2008));
		break;

	case 7:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManagerVS2010));
		break;

	case 8:
		switch (m_nStyle)
		{
			case 0:
				CBCGPVisualManager2010::SetStyle (CBCGPVisualManager2010::VS2010_Blue);
				break;

			case 1:
				CBCGPVisualManager2010::SetStyle (CBCGPVisualManager2010::VS2010_Black);
				break;

			case 2:
				CBCGPVisualManager2010::SetStyle (CBCGPVisualManager2010::VS2010_Silver);
				break;
		}
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManager2010));
		break;

	case 9:
		CBCGPVisualManager::SetDefaultManager (RUNTIME_CLASS (CBCGPVisualManagerCarbon));
		break;

	case 10:
		CCustomStyle::SetColors( m_dlgCal.m_CS );
		CBCGPVisualManager::SetDefaultManager( RUNTIME_CLASS( CCustomStyle ) );

		// save to registry
		theApp.WriteDWORD( _T("CustomApp1"), m_dlgCal.m_CS.m_appbg1 );
		theApp.WriteDWORD( _T("CustomApp2"), m_dlgCal.m_CS.m_appbg2 );
		theApp.WriteDWORD( _T("CustomBG1"), m_dlgCal.m_CS.m_barbg1 );
		theApp.WriteDWORD( _T("CustomBG2"), m_dlgCal.m_CS.m_barbg2 );
		theApp.WriteDWORD( _T("CustomCaption1"), m_dlgCal.m_CS.m_barcaptionbg1 );
		theApp.WriteDWORD( _T("CustomCaption2"), m_dlgCal.m_CS.m_barcaptionbg2 );
		theApp.WriteDWORD( _T("CustomGripper1"), m_dlgCal.m_CS.m_gripper1 );
		theApp.WriteDWORD( _T("CustomGripper2"), m_dlgCal.m_CS.m_gripper2 );
		theApp.WriteDWORD( _T("CustomTabErase1"), m_dlgCal.m_CS.m_taberase1 );
		theApp.WriteDWORD( _T("CustomTabErase2"), m_dlgCal.m_CS.m_taberase2 );
		theApp.WriteDWORD( _T("CustomSplitter1"), m_dlgCal.m_CS.m_splitter1 );
		theApp.WriteDWORD( _T("CustomSplitter2"), m_dlgCal.m_CS.m_splitter2 );
		theApp.WriteDWORD( _T("CustomMenuHighlight1"), m_dlgCal.m_CS.m_menuhl1 );
		theApp.WriteDWORD( _T("CustomMenuHighlight2"), m_dlgCal.m_CS.m_menuhl2 );
		theApp.WriteDWORD( _T("CustomMenuBarText"), m_dlgCal.m_CS.m_menubartext );
	}


	theApp.WriteInt (_T("AppLook"), m_nAppLook);
	theApp.WriteInt (_T("ShowAppLookAtStartup"), m_bShowAtStartup);
	theApp.WriteInt (_T("OneNoteTabs"), m_bOneNoteTabs);
	theApp.WriteInt (_T("DockTabColors"), m_bDockTabColors);
	theApp.WriteInt (_T("RoundedTabs"), m_bRoundedTabs);
	theApp.WriteInt (_T("CustomTooltips"), m_bCustomTooltips);
	theApp.WriteInt (_T("AppStyle"), m_nStyle);

	switch (m_nAppLook)
	{
		case 2:	// Office 2003
		case 3:	// VS.NET 2005
		case 4:	// Windows XP
		case 5:	// Office 2007
		case 6: // VS 2008
		case 7: // VS 2010
		case 8: // Office 2010
		{
			CWindowDC dc (NULL);
			theApp.m_bHiColorIcons = dc.GetDeviceCaps (BITSPIXEL) >= 16;

			if (globalData.bIsWindows9x && dc.GetDeviceCaps (BITSPIXEL) == 16)
			{
				theApp.m_bHiColorIcons = FALSE;
			}

			CBCGPDockManager::SetDockMode (BCGP_DT_SMART);
		}
		break;

	default:
		theApp.m_bHiColorIcons = FALSE;
	}

	CBCGPTabbedControlBar::ResetTabs ();

	if (m_bCustomTooltips)
	{
		CBCGPToolTipParams params;
		params.m_bVislManagerTheme = TRUE;

		theApp.GetTooltipManager ()->SetTooltipParams (
			BCGP_TOOLTIP_TYPE_ALL,
			RUNTIME_CLASS (CBCGPToolTipCtrl),
			&params);
	}
	else
	{
		theApp.GetTooltipManager ()->SetTooltipParams (
			BCGP_TOOLTIP_TYPE_ALL,
			NULL,
			NULL);
	}

	if (pMainFrame != NULL)
	{
		pMainFrame->OnChangeLook (
					m_bOneNoteTabs/* OneNote tabs */,
					m_bOneNoteTabs /* MDI tab colors*/,
					m_nAppLook != 0 /* VS.NET look */,
					m_bDockTabColors /* Dock tab colors*/,
					m_nAppLook == 3	/* VS.NET 2005 MDI tabs */,
					m_nAppLook == 4 || m_nAppLook == 2 || m_nAppLook == 3 || m_nAppLook == 5	/* Extended toolbox */);

		pMainFrame->UnlockWindowUpdate ();
		pMainFrame->RedrawWindow ();
	}
}

void CAppLookDlg::OnApply() 
{
	SetLook ();

	if (!m_bStartup)
	{
		CWnd* pWnd = GetDlgItem( IDCANCEL );
		pWnd->SetWindowText( _T("Close") );
	}
}

void CAppLookDlg::OnAppLook() 
{
	UpdateData();

	m_wndRoundedTabs.EnableWindow( m_nAppLook == 3 );
	m_wndStyle.EnableWindow( ( m_nAppLook == 5 ) || ( m_nAppLook == 8 ) );

	CBCGPComboBox* pCB = (CBCGPComboBox*)GetDlgItem( IDC_STYLE );
	if( pCB )
	{
		pCB->ResetContent();
		pCB->AddString( _T("Blue") );
		pCB->AddString( _T("Black") );
		pCB->AddString( _T("Silver") );
		if( m_nAppLook == 5 )
		{
			pCB->AddString( _T("Aqua") );
			pCB->AddString( _T("Custom...") );
		}
	}
	UpdateData( FALSE );

	CWnd* pWnd = GetDlgItem( IDC_BN_CUSTOMIZE );
	pWnd->EnableWindow( ( m_nAppLook == 10 ) || ( m_nAppLook == 5 ) );
}

BOOL CAppLookDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("applicationlook.html"), this );
	return CBCGPDialog::OnHelpInfo( pHelpInfo );
}

void CAppLookDlg::OnCustomize()
{
	UpdateData();
	if( m_nAppLook == 9 )
	{
		if( m_dlgCal.DoModal() == IDOK ) CCustomStyle::SetColors( m_dlgCal.m_CS );
	}
	else if( m_nAppLook == 5 )
	{
		CBCGPColorDialog d( theApp.m_clrCustom );
		if( d.DoModal() == IDOK )
		{
			m_clrCustom = d.GetColor();
			CBCGPComboBox* pCB = (CBCGPComboBox*)GetDlgItem( IDC_STYLE );
			if( pCB ) pCB->SetCurSel( pCB->GetCount() - 1 );
		}

	}
}

void CAppLookDlg::OnStyle()
{
// 	UpdateData();
// 	if( m_nStyle == 5 )
// 	{
// 		CBCGPColorDialog d( theApp.m_clrCustom );
// 		if( d.DoModal() == IDOK ) m_clrCustom = d.GetColor();
// 	}
}

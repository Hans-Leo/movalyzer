// WizardImportGenEnd.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportGenEnd.h"
#include <direct.h>

#include "WizardImportGenSheet.h"
#include "..\DataMod\DataMod.h"
#include "WizardImportExp.h"
#include "WizardImportCond.h"
#include "WizardImportGrp.h"
#include "WizardImportSubj.h"
#include "WizardImportGen.h"
#include "SigGenSettingsDlg.h"
#include "..\NSShared\GenSig.h"

#include "MainFrm.h"
#include "LeftView.h"

#include "Subjects.h"
#include "Groups.h"
#include "Experiments.h"
#include "Conditions.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

WizardImportExp* _pgExp = NULL;
WizardImportCond* _pgCond = NULL;
WizardImportGrp* _pgGrp = NULL;
WizardImportSubj* _pgSubj = NULL;

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenEnd property page

IMPLEMENT_DYNCREATE(WizardImportGenEnd, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportGenEnd, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_CONTINUE, OnChkContinue)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportGenEnd::WizardImportGenEnd() : CBCGPPropertyPage(WizardImportGenEnd::IDD)
{
	m_szExpID = _T("");
	m_szExpDesc = _T("");
	m_szGrpID = _T("");
	m_szGrpDesc = _T("");
	m_szSubjID = _T("");
	m_szSubjName = _T("");
	m_szFormat = _T("");
	m_fCont = FALSE;
	m_fRetain = FALSE;
}

WizardImportGenEnd::~WizardImportGenEnd()
{
}

void WizardImportGenEnd::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_FORMAT, m_szFormat);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szExpID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szExpDesc);
	DDX_Text(pDX, IDC_TXT_CONDID, m_szCondID);
	DDX_Text(pDX, IDC_TXT_CONDDESC, m_szCondDesc);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szGrpID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szGrpDesc);
	DDX_Text(pDX, IDC_TXT_SUBJID, m_szSubjID);
	DDX_Text(pDX, IDC_TXT_SUBJNAME, m_szSubjName);
	DDX_Check(pDX, IDC_CHK_CONTINUE, m_fCont);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenEnd message handlers

BOOL WizardImportGenEnd::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_CONTINUE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check if completed. Uncheck to continue.") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportGenEnd::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardImportGenEnd::OnWizardNext() 
{
	DoNext();

	WizardImportGenSheet* pParent = (WizardImportGenSheet*)GetParent();
	if( pParent ) pParent->SetActivePage( 1 );

	return -1;
}

BOOL WizardImportGenEnd::OnWizardFinish() 
{
	DoNext();

	return CBCGPPropertyPage::OnWizardFinish();
}

void WizardImportGenEnd::DoNext()
{
	CString szPath = _T("Beginning data generation.");
	OutputAMessage( szPath );

	if( !ConfirmCreate() ) return;

	::GetDataPathRoot( szPath );

	// Get generate settings
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportGen* pGen = (WizardImportGen*)pParent->GetPage( 1 );
	GenSig gs;
	gs.m_cm = pGen->m_dlgSettings.m_cm;
	gs.m_deltat = pGen->m_dlgSettings.m_deltat;
	gs.m_deltax = pGen->m_dlgSettings.m_deltax;
	gs.m_deltay = pGen->m_dlgSettings.m_deltay;
	gs.m_integer = pGen->m_dlgSettings.m_fInt;
	gs.m_noise = pGen->m_dlgSettings.m_noise;
	gs.m_nstroke = pGen->m_dlgSettings.m_nstrokes;
	gs.m_pattern = pGen->m_dlgSettings.m_pattern;
	gs.m_phasex = pGen->m_dlgSettings.m_phasex;
	gs.m_phasey = pGen->m_dlgSettings.m_phasey;
	gs.m_prsmin = pGen->m_dlgSettings.m_prsmin;
	gs.m_sec = pGen->m_dlgSettings.m_sec;
	gs.m_startt = pGen->m_dlgSettings.m_startt;
	gs.m_startx = pGen->m_dlgSettings.m_startx;
	gs.m_starty = pGen->m_dlgSettings.m_starty;
	gs.m_trailtime = pGen->m_dlgSettings.m_trailtime;
	gs.m_xspeed = pGen->m_dlgSettings.m_xspeed;
	switch( pGen->m_dlgSettings.m_nSubMvmtModel )
	{
		case SM_GEN_NONE:
			gs.m_nSubMvmtModel = GEN_NONE;
			gs.m_subduration = 0.;
			gs.m_subdelay = 0.;
			break;
		case SM_GEN_DELAY:
			gs.m_nSubMvmtModel = GEN_DELAY;
			gs.m_subduration = pGen->m_dlgSettings.m_dur;
			gs.m_subdelay = pGen->m_dlgSettings.m_delay;
			break;
		case SM_GEN_WARP:
			gs.m_nSubMvmtModel = GEN_WARP;
			gs.m_subduration = pGen->m_dlgSettings.m_dur;
			gs.m_subdelay = pGen->m_dlgSettings.m_warp;
			break;
	}

	// Get settings from experiment
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	pPS->Find( m_szExpID.AllocSysString() );
	// use default if not found

	short nVal = 0;
	pPS->get_DeviceResolution( &gs.m_cm );
	pPS->get_MinPenPressure( &nVal );
	gs.m_prsmin = nVal;
	pPS->get_SamplingRate( &nVal );
	gs.m_sec = nVal;
	if( gs.m_sec != 0. ) gs.m_sec = 1. / gs.m_sec;
	pPS->Release();

	// Get trial
	int nTrial = 1;
	CString szTrial, szDest, szExt = _T("HWR");
	BOOL fFound = FALSE;

	CFileFind ff;
	while( !fFound )
	{
		if( nTrial < 10 ) szTrial.Format( _T("0%d"), nTrial );
		else szTrial.Format( _T("%d"), nTrial );
		szDest.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.%s"), szPath, m_szExpID,
					   m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
					   m_szCondID, szTrial, szExt );
		if( !ff.FindFile( szDest ) ) fFound = TRUE;
		else nTrial++;
	}

	// Generate
	if( gs.Generate( szDest ) )
	{
		BCGPMessageBox( _T("Data generation complete.") );
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
		if( pLV ) pLV->Refresh();
	}
	else BCGPMessageBox( _T("Error in data generation.") );
}

BOOL WizardImportGenEnd::ConfirmCreate()
{
	CFileFind ff, ff2;
	BOOL fCont = TRUE, fRefresh = FALSE;
	int nCnt = 0;
	CString szFile, szPath, szTemp, szNum;

	// Data root path
	::GetDataPathRoot( szPath );
	if( szPath != _T("") ) szPath += _T("\\");

	// Ensure directories exists
	szTemp.Format( _T("%s\\%s"), szPath, m_szExpID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	szTemp.Format( _T("%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	szTemp.Format( _T("%s\\%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID, m_szSubjID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}

	// Create new/add exp, cond, grp, subjects where necessary
	HRESULT hr;
	// ...Experiment
	if( _pgExp->m_fNew )
	{
		Experiments exp;
		if( !exp.IsValid() ) return FALSE;

		exp.PutID( m_szExpID );
		exp.PutDescription( m_szExpDesc );
		hr = exp.Add();
		if( FAILED( exp.Add() ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new experiment %s."), m_szExpID );
		OutputAMessage( szTemp );
	}
	if( !m_fRetain )
	{
		// ...Condition
		NSConditions cond;
		if( !cond.IsValid() ) return FALSE;
		hr = cond.Find( m_szCondID );
		if( FAILED( hr ) )
		{
			cond.PutID( m_szCondID );
			cond.PutDescription( m_szCondDesc );
			cond.PutLex( _pgCond->m_szLex );
			cond.PutInstruction( _pgCond->m_szInstr );
			cond.PutNotes( _pgCond->m_szNotes );
			cond.PutRangeDirection( _pgCond->m_dRangeDirection );
			cond.PutRangeLength( _pgCond->m_dRangeLength );
			cond.PutStrokeDirection( _pgCond->m_dStrokeDirection );
			cond.PutStrokeLength( _pgCond->m_dStrokeLength );
			cond.PutStrokeMax( _pgCond->m_nMax );
			cond.PutStrokeMin( _pgCond->m_nMin );
			cond.PutStrokeSkip( _pgCond->m_nSkip );
			cond.PutUseStimulus( _pgCond->m_fStimulus );
			cond.PutStimulus( _pgCond->m_szStimulus );
			if( FAILED( cond.Add() ) ) return FALSE;

			::DataHasChanged();
			fRefresh = TRUE;

			szTemp.Format( _T("Added a new condition %s."), m_szCondID );
			OutputAMessage( szTemp );
		}
		// ...ExperimentCondition
		IExperimentCondition* pEC = NULL;
		hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
							   IID_IExperimentCondition, (LPVOID*)&pEC );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPCOND_ERR );
			return FALSE;
		}
		hr = pEC->Find( m_szExpID.AllocSysString(), m_szCondID.AllocSysString() );
		if( FAILED( hr ) )
		{
			pEC->put_ExperimentID( m_szExpID.AllocSysString() );
			pEC->put_ConditionID( m_szCondID.AllocSysString() );
			pEC->put_Replications( 1 );
			hr = pEC->Add();
			if( FAILED( hr ) )
			{
				pEC->Release();
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" WizardImportGenEnd::ConfirmCreate:1") );
				return FALSE;
			}
			::DataHasChanged();
			fRefresh = TRUE;
			szTemp.Format( _T("Added a new condition %s to experiment %s with 1 replication."),
						   m_szCondID, m_szExpID );
			OutputAMessage( szTemp );
		}
		pEC->Release();
	}
	// ...Group
	Groups grp;
	if( !grp.IsValid() ) return FALSE;
	hr = grp.Find( m_szGrpID );
	if( FAILED( hr ) )
	{
		grp.PutID( m_szGrpID );
		grp.PutDescription( _pgGrp->m_szDesc );
		grp.PutNotes( _pgGrp->m_szNotes );

		hr = grp.Add();
		if( FAILED( hr ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new group %s."), m_szGrpID );
		OutputAMessage( szTemp );
	}
	// ...Subject
	Subjects subj;
	if( !subj.IsValid() ) return FALSE;
	hr = subj.Find( m_szSubjID );
	if( FAILED( hr ) )
	{
		subj.PutID( m_szSubjID );
		subj.PutNameLast( _pgSubj->m_szLast );
		subj.PutNameFirst( _pgSubj->m_szFirst );
		subj.PutNotes( _pgSubj->m_szNotes );
		subj.PutDateAdded( COleDateTime::GetCurrentTime() );

		hr = subj.Add();
		if( FAILED( hr ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new subject %s."), m_szSubjID );
		OutputAMessage( szTemp );
	}
	// ...ExperimentMember
	IExperimentMember* pEM = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return FALSE;
	}
	hr = pEM->Find( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString(),
					m_szSubjID.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->put_ExperimentID( m_szExpID.AllocSysString() );
		pEM->put_GroupID( m_szGrpID.AllocSysString() );
		pEM->put_SubjectID( m_szSubjID.AllocSysString() );
		hr = pEM->Add();
		if( FAILED( hr ) )
		{
			pEM->Release();
			CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" WizardImportGenEnd::ConfirmCreate:2") );
			return FALSE;
		}
		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new group %s and subject %s to experiment %s."),
					   m_szGrpID, m_szSubjID, m_szExpID );
		OutputAMessage( szTemp );
	}
	pEM->Release();

	if( fRefresh )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
		if( pLV ) pLV->Refresh();
	}

	return TRUE;
}

void WizardImportGenEnd::OnChkContinue() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	CWnd* pWnd = GetDlgItem( IDC_CHK_CONTINUE );
	if( pWnd ) pWnd->EnableWindow( FALSE );
}

BOOL WizardImportGenEnd::OnSetActive() 
{
	WizardImportGenSheet* pParent = (WizardImportGenSheet*)GetParent();
	WizardImportGen *pGen = (WizardImportGen*)pParent->GetPage( 1 );
	_pgExp = (WizardImportExp*)pParent->GetPage( 2 );
	_pgCond = (WizardImportCond*)pParent->GetPage( 3 );
	_pgGrp = (WizardImportGrp*)pParent->GetPage( 4 );
	_pgSubj = (WizardImportSubj*)pParent->GetPage( 5 );

	m_szFormat = pGen->m_szType;
	m_szExpID = _pgExp->m_szID;
	m_szExpDesc = _pgExp->m_szDesc;
	m_szCondID = _pgCond->m_szID;
	m_szCondDesc = _pgCond->m_szDesc;
	m_fRetain = _pgCond->m_fRetain;
	m_szGrpID = _pgGrp->m_szID;
	m_szGrpDesc = _pgGrp->m_szDesc;
	m_szSubjID = _pgSubj->m_szID;
	if( ::IsPrivacyOn() )
		m_szSubjName.Format( _T("%s %s (%s)"), PVT_NAME_LAST, PVT_NAME_FIRST, _pgSubj->m_szCode );
	else
		m_szSubjName.Format( _T("%s %s (%s)"), _pgSubj->m_szFirst, _pgSubj->m_szLast, _pgSubj->m_szCode );

	UpdateData( FALSE );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardImportGenEnd::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportGenEnd::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

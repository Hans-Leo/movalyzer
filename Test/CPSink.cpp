// CPSink.cpp : implementation file
//

#include "stdafx.h"
#ifndef	_WRITALYZER_
	#include "Test.h"
	#include "MainFrm.h"
#else
	#include "..\Writalyzer\Writalyzer.h"
	#include "..\Writalyzer\WritalyzerDlg.h"
#endif
#include "CPSink.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCPSink

IMPLEMENT_DYNCREATE(CCPSink, CCmdTarget)

BEGIN_MESSAGE_MAP(CCPSink, CCmdTarget)
	//{{AFX_MSG_MAP(CCPSink)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CCPSink, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CCPSink)
	DISP_FUNCTION(CCPSink, "OnMissingLicenseFile", OnMissingLicenseFile, VT_EMPTY, VTS_BSTR VTS_PBSTR)
	DISP_FUNCTION(CCPSink, "OnAttempReleaseCP", OnAttempReleaseCP, VT_EMPTY, VTS_BSTR VTS_PBSTR)
	DISP_FUNCTION(CCPSink, "OnAttemptDeactivate", OnAttemptDeactivate, VT_EMPTY, VTS_PI2)
	DISP_FUNCTION(CCPSink, "OnAttemptRecover", OnAttemptRecover, VT_EMPTY, VTS_BSTR VTS_PBSTR)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

CCPSink::CCPSink()
{
	EnableAutomation();
}

CCPSink::~CCPSink()
{
}


void CCPSink::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

// Note: we add support for IID_ICPSink to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {A2DBC1D3-5737-4F43-9742-076628D55598}
static const IID IID_ICPSink =
{ 0xa2dbc1d3, 0x5737, 0x4f43, { 0x97, 0x42, 0x7, 0x66, 0x28, 0xd5, 0x55, 0x98 } };

BEGIN_INTERFACE_MAP(CCPSink, CCmdTarget)
	INTERFACE_PART(CCPSink, __uuidof(ISSCProtectorEvents), Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCPSink message handlers
// These message handlers will simply delegate the jobs back to the main form.
// an alternative approach would have the main form multiply inherit from CCmdTarget as well as CDialog  
// and directly impliment the event handlers through an INTERFACE_MAP / DISPATCH_MAP
void CCPSink::OnMissingLicenseFile( LPCTSTR ExpectedFileNamePath, BSTR FAR* ActualFileNamePath ) 
{
	if( m_pRefMainForm )
		m_pRefMainForm->OnMissingLicenseFile( ExpectedFileNamePath, ActualFileNamePath );
}

void CCPSink::OnAttempReleaseCP( LPCTSTR ReleaseCPAuthRequestCode, BSTR FAR* InputActivationCode ) 
{
	if( m_pRefMainForm )
		m_pRefMainForm->OnAttemptReleaseCP( ReleaseCPAuthRequestCode, InputActivationCode );
}

void CCPSink::OnAttemptRecover( LPCTSTR RecoverAuthRequestCode, BSTR FAR* InputActivationCode ) 
{
	if( m_pRefMainForm )
		m_pRefMainForm->OnAttemptRecover( RecoverAuthRequestCode, InputActivationCode );
}

void CCPSink::OnAttemptDeactivate( short FAR* AllowDeactivate ) 
{
	if( m_pRefMainForm )
		m_pRefMainForm->OnAttemptDeactivate( AllowDeactivate ) ;
}

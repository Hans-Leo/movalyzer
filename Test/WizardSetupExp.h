#pragma once

// WizardSetupExp.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardSetupExp dialog
interface IProcSetting;

class WizardSetupExp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardSetupExp)

// Construction
public:
	WizardSetupExp();
	~WizardSetupExp();

// Dialog Data
	enum { IDD = IDD_WIZ_SETUP_EXP };
	CString			m_szID;
	CString			m_szDesc;
	CString			m_szType;
	CString			m_szNotes;
	int				m_nType;
	IProcSetting*	m_pPS;
	BOOL			m_fUpdated;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual LRESULT OnWizardBack();
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnCreateexp();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

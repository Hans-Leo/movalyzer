#pragma once

// WizardRunExpSubj.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpSubj dialog

class WizardRunExpSubj : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardRunExpSubj)

// Construction
public:
	WizardRunExpSubj();
	~WizardRunExpSubj();

// Dialog Data
	enum { IDD = IDD_WIZ_RUNEXP_SUBJ };
	CBCGPComboBox	m_Cbo;
	CString			m_szID;
	CString			m_szCode;
	CString			m_szName;
	CString			m_szSubj;
	CString			m_szFirst;
	CString			m_szLast;
	COleDateTime	m_DateAdded;
	CString			m_szNotes;
	CString			m_szPrvNotes;
	CString			m_szExpID;
	CString			m_szGrpID;
	CStringList		m_existing;
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fInit;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardBack();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnCreatesubj();
	afx_msg void OnSelchangeCboSubjs();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

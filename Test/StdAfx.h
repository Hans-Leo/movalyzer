// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#pragma once

#define _BIND_TO_CURRENT_CRT_VERSION	1
#define _BIND_TO_CURRENT_MFC_VERSION	1

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxcview.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxhtml.h>

#include "..\Common\DefFun.h"
#include "..\Common\Macros.h"
#include "Globals.h"		// global vars/methods
#define	_BCGCB_HIDE_AUTOLINK_OUTPUT_
#include "BCGCBProInc.h"	// bcg toolbars
#include <afxdlgs.h>

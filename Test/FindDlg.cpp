// FindDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "FindDlg.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FindDlg dialog

BEGIN_MESSAGE_MAP(FindDlg, CBCGPDialog)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

FindDlg::FindDlg(CString szTitle, CWnd* pParent /*=NULL*/)
	: CBCGPDialog(FindDlg::IDD, pParent), m_szTitle( szTitle )
{
	m_fExactPartial = FALSE;
	m_fPartial = FALSE;
}

FindDlg::FindDlg(UINT nResourceID, CWnd* pParent /*=NULL*/)
	: CBCGPDialog(FindDlg::IDD, pParent)
{
	m_szTitle.LoadString( nResourceID );
	m_fExactPartial = FALSE;
	m_fPartial = FALSE;
}

void FindDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ID, m_szID);
}

/////////////////////////////////////////////////////////////////////////////
// FindDlg message handlers

BOOL FindDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Set window title
	SetWindowText( m_szTitle );

	// window icon
	HICON hIcon = AfxGetApp()->LoadIcon( IDI_FIND );
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_FIND );
	m_tooltip.SetTitle( _T("FIND") );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_ID );
	m_tooltip.AddTool( pWnd, IDS_FIND_ID, _T("Search Text") );
	pWnd = GetDlgItem( IDOK );
	m_tooltip.AddTool( pWnd, IDS_FIND, _T("OK") );
	pWnd = GetDlgItem( IDCANCEL );
	m_tooltip.AddTool( pWnd, IDS_FIND_CLOSE, _T("Cancel") );
	pWnd = GetDlgItem( IDC_RDO_EXACT );
	m_tooltip.AddTool( pWnd, _T("Select to locate those items that match the search text exactly."), _T("Exact Match") );
	pWnd = GetDlgItem( IDC_RDO_PARTIAL );
	m_tooltip.AddTool( pWnd, _T("Select to locate those items that match the search text, even if only partially."), _T("Partial Match") );

	// disable exact/partial if specified
	if( !m_fExactPartial )
	{
		m_fPartial = FALSE;
		pWnd = GetDlgItem( IDC_RDO_EXACT );
		pWnd->EnableWindow( FALSE );
		pWnd = GetDlgItem( IDC_RDO_PARTIAL );
		pWnd->EnableWindow( FALSE );
	}
	else
	{
		// default is exact
		if( m_fPartial )
			CheckRadioButton( IDC_RDO_EXACT, IDC_RDO_PARTIAL, IDC_RDO_PARTIAL );
		else
			CheckRadioButton( IDC_RDO_EXACT, IDC_RDO_PARTIAL, IDC_RDO_EXACT );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FindDlg::OnOK()
{
	int nSel = GetCheckedRadioButton( IDC_RDO_EXACT, IDC_RDO_PARTIAL );
	if( nSel == IDC_RDO_PARTIAL ) m_fPartial = TRUE;
	else m_fPartial = FALSE;

	CBCGPDialog::OnOK();
}

BOOL FindDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL FindDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("find.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

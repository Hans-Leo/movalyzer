// RangeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "RangeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RangeDlg dialog

BEGIN_MESSAGE_MAP(RangeDlg, CDialog)
	//{{AFX_MSG_MAP(RangeDlg)
	ON_BN_CLICKED(IDC_CHK_XRANGE, OnChkXRange)
	ON_BN_CLICKED(IDC_CHK_YRANGE, OnChkYRange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

RangeDlg::RangeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RangeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RangeDlg)
	m_fXRange = FALSE;
	m_dXMin = 0.0;
	m_dXMax = 0.0;
	m_fYRange = FALSE;
	m_dYMin = 0.0;
	m_dYMax = 0.0;
	//}}AFX_DATA_INIT
}

void RangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RangeDlg)
	DDX_Check(pDX, IDC_CHK_XRANGE, m_fXRange);
	DDX_Check(pDX, IDC_CHK_YRANGE, m_fYRange);
	DDX_Text(pDX, IDC_EDIT_XMIN, m_dXMin);
	DDX_Text(pDX, IDC_EDIT_XMAX, m_dXMax);
	DDX_Text(pDX, IDC_EDIT_YMIN, m_dYMin);
	DDX_Text(pDX, IDC_EDIT_YMAX, m_dYMax);
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// RangeDlg message handlers

void RangeDlg::OnChkXRange() 
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_XMIN );
	if( pWnd ) pWnd->EnableWindow( m_fXRange );
	pWnd = GetDlgItem( IDC_EDIT_XMAX );
	if( pWnd ) pWnd->EnableWindow( m_fXRange );
}

void RangeDlg::OnChkYRange() 
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_EDIT_YMIN );
	if( pWnd ) pWnd->EnableWindow( m_fYRange );
	pWnd = GetDlgItem( IDC_EDIT_YMAX );
	if( pWnd ) pWnd->EnableWindow( m_fYRange );
}

BOOL RangeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd* pWnd = GetDlgItem( IDC_EDIT_XMIN );
	if( pWnd ) pWnd->EnableWindow( m_fXRange );
	pWnd = GetDlgItem( IDC_EDIT_XMAX );
	if( pWnd ) pWnd->EnableWindow( m_fXRange );
	pWnd = GetDlgItem( IDC_EDIT_YMIN );
	if( pWnd ) pWnd->EnableWindow( m_fYRange );
	pWnd = GetDlgItem( IDC_EDIT_YMAX );
	if( pWnd ) pWnd->EnableWindow( m_fYRange );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

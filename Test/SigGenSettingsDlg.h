#pragma once

// SigGenSettingsDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

// Predefined Settings
#define	GS_CUSTOM		0
#define	GS_ONESTROKE	1
#define	GS_2LOOPS		2
#define	GS_4LOOPS		3
#define	GS_8LOOPS		4
#define	GS_LINEAR		5

// Submovement Generation Method
#define SM_GEN_NONE		IDC_RDO_SM_NONE
#define SM_GEN_DELAY	IDC_RDO_SM_DELAY
#define SM_GEN_WARP		IDC_RDO_SM_WARP

/////////////////////////////////////////////////////////////////////////////
// SigGenSettingsDlg dialog

class SigGenSettingsDlg : public CBCGPDialog
{
// Construction
public:
	SigGenSettingsDlg( UINT nMode, CWnd* pParent = NULL );
	SigGenSettingsDlg( CWnd* pParent = NULL );

// Dialog Data
	enum { IDD = IDD_SIGGEN };
	CComboBox	m_cboPre;
	BOOL	m_fInt;
	double	m_cm;
	double	m_sec;
	double	m_prsmin;
	double	m_noise;
	double	m_startt;
	double	m_deltat;
	double	m_nstrokes;
	double	m_trailtime;
	double	m_startx;
	double	m_deltax;
	double	m_phasex;
	double	m_xspeed;
	double	m_starty;
	double	m_deltay;
	double	m_phasey;
	int		m_pattern;
	UINT	m_nMode;
	UINT	m_nSubMvmtModel;
	double	m_delay;
	double	m_warp;
	double	m_dur;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	void UpdateSettings();
protected:
	BOOL VerifySubMvmtValues();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboPre();
	afx_msg void OnRdoHarmonic();
	afx_msg void OnRdoJerk();
	afx_msg void OnRdoSM();
	DECLARE_MESSAGE_MAP()
};

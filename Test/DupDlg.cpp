// DupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "DupDlg.h"
#include "..\NSSharedGUI\Conditions.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

// DupDlg dialog

IMPLEMENT_DYNAMIC(DupDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(DupDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_CHK_PROCESS, OnBnClickedChkWords)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

DupDlg::DupDlg( CWnd* pParent )
	: CBCGPDialog( DupDlg::IDD, pParent ), m_szID( _T("") ), m_nCount( 1 )
	, m_szDesc(_T("")), m_fWords( FALSE ), m_nWord( 1 )
{
}

DupDlg::~DupDlg()
{
}

void DupDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_ID, m_szID);
	DDX_Text(pDX, IDC_EDIT_REPS, m_nCount);
	DDX_Text(pDX, IDC_EDIT_DESC, m_szDesc);
	DDX_Check(pDX, IDC_CHK_PROCESS, m_fWords);
	DDX_Control(pDX, IDC_CBO_PARENT, m_cboParent);
	DDX_Text(pDX, IDC_EDIT_WORD, m_nWord);
}

// DupDlg message handlers

BOOL DupDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_REPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the number of conditions to create.\nThe IDs of the new conditions will be incremented by 1 from the ID above."), _T("Condition Count") );
	pWnd = GetDlgItem( IDC_EDIT_DESC );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the description that you want each new condition to share.\nThe condition # will be appended to the description."), _T("Description") );
	pWnd = GetDlgItem( IDC_CHK_PROCESS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to make each new condition a process-only condition for word extraction."), _T("Word Extraction") );
	pWnd = GetDlgItem( IDC_CBO_PARENT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select the recordable condition to make as the parent condition for the process-only new conditions."), _T("Parent Condition") );
	pWnd = GetDlgItem( IDC_EDIT_WORD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Specify the starting word # of the first generated process-only condition.\nSubsequent conditions will be incremented by 1."), _T("Word Number") );

	// determine if condition is process-only for word extraction purposes
	// if so, they we can auto-select the word-ext checkbox
	// if record only, then disable it
	CString szID, szDesc, szParent, szSearch;
	short nWord = 1;
	BOOL fRecord = FALSE, fProcess = FALSE;
	NSConditions c;
	if( SUCCEEDED( c.Find( m_szID ) ) )
	{
		c.GetDescription( m_szDesc );
		c.GetRecord( fRecord );
		c.GetProcess( fProcess );
		c.GetParent( szParent );
		c.GetWord( nWord );
		if( !fRecord && fProcess )
		{
			m_fWords = TRUE;
			m_nWord = nWord + 1;
			m_szParent = szParent;
		}
		else if( fRecord && !fProcess)
		{
			pWnd = GetDlgItem( IDC_CHK_PROCESS );
			pWnd->EnableWindow( FALSE );
			m_fWords = FALSE;
		}
	}
	else
	{
		pWnd = GetDlgItem( IDC_CHK_PROCESS );
		pWnd->EnableWindow( FALSE );
		m_fWords = FALSE;
	}

	// enable disable fields
	UpdateData( FALSE );
	pWnd = GetDlgItem( IDC_CBO_PARENT );
	pWnd->EnableWindow( m_fWords );
	pWnd = GetDlgItem( IDC_EDIT_WORD );
	pWnd->EnableWindow( m_fWords );

	// fill parent condition list (also add all condition IDs to master list)
	HRESULT hr = c.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		c.GetID( szID );
		m_lstCond.AddTail( szID );
		c.GetDescriptionWithID( szDesc );
		c.GetRecord( fRecord );
		if( ( szID != m_szID ) && fRecord ) m_cboParent.AddString( szDesc );

		hr = c.GetNext();
	}
	if( m_szParent != _T("") && SUCCEEDED( c.Find( m_szParent ) ) )
	{
		c.GetDescriptionWithID( szSearch );
		m_cboParent.SelectString( -1, szSearch );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL DupDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL DupDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("condition_generate.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void DupDlg::OnBnClickedChkWords()
{
	UpdateData( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_PARENT );
	pWnd->EnableWindow( m_fWords );
	pWnd = GetDlgItem( IDC_EDIT_WORD );
	pWnd->EnableWindow( m_fWords ) ;
}

void DupDlg::OnOK()
{
// 	NSConditions c;
// 	CString szID1, szID2;
// 	for( int q = 1; q <= 99; q++ )
// 	{
// 		szID1.Format( _T("T%02d"), q );
// 		szID2.Format( _T("F%02d"), q );
// 		if( SUCCEEDED( c.Find( szID1 ) ) ) c.Remove();
// 		if( SUCCEEDED( c.Find( szID2 ) ) ) c.Remove();
// 	}
// 	return;

	CWnd* pWnd = NULL;

	// Verify we have the data we need
	UpdateData( TRUE );
	if( m_szDesc == _T("") )
	{
		BCGPMessageBox( _T("You need to specify a description."), MB_OK | MB_ICONERROR );
		pWnd = GetDlgItem( IDC_EDIT_DESC );
		pWnd->SetFocus();
		return;
	}
	if( ( m_nCount <= 0 ) || ( m_nCount > 999 ) )
	{
		BCGPMessageBox( _T("Condition count must be between 1 and 999."), MB_OK | MB_ICONERROR );
		pWnd = GetDlgItem( IDC_EDIT_REPS );
		pWnd->SetFocus();
		return;
	}
	if( m_fWords )
	{
		int nSel = m_cboParent.GetCurSel();
		if( nSel >= 0 )
		{
			CString szTemp;
			m_cboParent.GetLBText( nSel, szTemp );
			m_szParent = szTemp.Right( 3 );
		}
		else m_szParent = _T("");
		if( m_szParent == _T("") )
		{
			BCGPMessageBox( _T("A parent condition must be selected."), MB_OK | MB_ICONERROR );
			pWnd = GetDlgItem( IDC_CBO_PARENT );
			pWnd->SetFocus();
			return;
		}
		if( ( m_nWord <= 0 ) || ( m_nWord > 999 ) )
		{
			BCGPMessageBox( _T("Word number must be between 1 and 999."), MB_OK | MB_ICONERROR );
			pWnd = GetDlgItem( IDC_EDIT_WORD );
			pWnd->SetFocus();
			return;
		}
	}

	// Ask user if they are sure...
	CString szMsg;
	szMsg.Format( _T("You are about to create %d new conditions duplicated from condition %s.\n\nContinue?"), m_nCount, m_szID );
	if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	// now generate the tentative condition ID list
	// starting ID is the current + 1, end, we assume, is ZZZ
	CStringList lstNewCond;
	CString szCur = m_szID, szStart = m_szID, szEnd = _T("ZZZ");
	for( int i = 0; i < m_nCount; i++ )
	{
		if( NextID( szCur, szStart, szEnd, FALSE ) )
		{
			lstNewCond.AddTail( szCur );
		}
		else
		{
			szMsg.Format( _T("There is an insufficient number of available IDs for this many conditions starting with ID: %s\nYou will need to reduce the number of conditions or choose a different ID for the condition to duplicate."), m_szID );
			BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
			return;
		}
	}

	// now verify that all IDs are available
	POSITION pos = lstNewCond.GetHeadPosition();
	while( pos )
	{
		szCur = lstNewCond.GetNext( pos );
		if( m_lstCond.Find( szCur ) )
		{
			BCGPMessageBox( _T("The specified number of conditions cannot be generated.\nOne ore more conditions already exist with an ID that would be used.\nYou will need to reduce the number of conditions or choose a different ID for the condition to duplicate."), MB_OK | MB_ICONERROR );
			return;
		}
	}

	// CREATE NEW CONDITIONS
	CWaitCursor crs;
	// verify source
	NSConditions c;
	if( FAILED( c.Find( m_szID ) ) )
	{
		BCGPMessageBox( _T("Unable to continue.\nUnable to locate the source condition."), MB_OK | MB_ICONERROR );
		return;
	}
	// loop through list of new IDs, modify ID, desc, parent, and word as appropriate
	CString szDesc;
	int nWord = m_nWord;
	pos = lstNewCond.GetHeadPosition();
	while( pos )
	{
		szCur = lstNewCond.GetNext( pos );
		if( m_nCount < 9 ) szDesc.Format( _T("%s %d"), m_szDesc, nWord );
		else if( m_nCount < 99 ) szDesc.Format( _T("%s %02d"), m_szDesc, nWord );
		else if( m_nCount < 999 ) szDesc.Format( _T("%s %03d"), m_szDesc, nWord );
		c.PutID( szCur );
		c.PutDescription( szDesc );
		if( m_fWords )
		{
			c.PutParent( m_szParent );
			c.PutWord( nWord++ );
		}
		if( FAILED( c.Add() ) )
		{
			BCGPMessageBox( _T("There was an error adding the condition to the database."), MB_OK | MB_ICONERROR );
			return;
		}
	}

	BCGPMessageBox( _T("The new conditions have been generated."), MB_OK | MB_ICONINFORMATION );
	CBCGPDialog::OnOK();
}

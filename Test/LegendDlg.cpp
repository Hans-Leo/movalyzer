// LegendDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "LegendDlg.h"
#include "MainFrm.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LegendDlg dialog

BEGIN_MESSAGE_MAP(LegendDlg, CBCGPDialog)
	ON_WM_HELPINFO()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

LegendDlg::LegendDlg(CWnd* pParent /*=NULL*/) : CBCGPDialog()
{
	if( Create( LegendDlg::IDD, pParent ) ) ShowWindow( SW_SHOW );
}

void LegendDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
// LegendDlg message handlers

BOOL LegendDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Center window in parent
	CenterWindow();

	CStatic* pDis = (CStatic*)GetDlgItem( IDC_TXT_PATH );
	CDC* pDC = GetDC();
	LOGFONT lf;
	lf.lfHeight= -MulDiv( 8, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
	lf.lfWidth = 0;
	lf.lfWeight = FW_NORMAL;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;
	CFont font;
	font.CreateFontIndirect( &lf );
	CFont* pOldFont = pDC->SelectObject( &font );
	pDis->SetFont( &font, TRUE );
	pDC->SelectObject( pOldFont );
	ReleaseDC( pDC );


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH LegendDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBCGPDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if( ( nCtlColor == CTLCOLOR_STATIC ) && ( GetDlgItem( IDC_TXT_PATH ) == pWnd ) )
		pDC->SetTextColor( RGB( 255, 0, 0 ) );

	return hbr;

}

void LegendDlg::OnOK()
{
	OnCancel();
}

void LegendDlg::OnCancel()
{
	DestroyWindow();
}

void LegendDlg::PostNcDestroy()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->LegendClosed();
}

BOOL LegendDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("symbollegend.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

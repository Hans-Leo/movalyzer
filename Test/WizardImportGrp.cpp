// WizardImportGrp.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportGrp.h"

#include "WizardImportSheet.h"
#include "..\DataMod\DataMod.h"
#include "AddGroupDlg.h"

#include "WizardImportGenStart.h"
#include "WizardImportExp.h"
#include "WizardImportSubj.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "Groups.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportGrp property page

IMPLEMENT_DYNCREATE(WizardImportGrp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportGrp, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_GRPS, OnSelchangeCboGrps)
	ON_BN_CLICKED(IDC_BN_CREATEGRP, OnBnCreategrp)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportGrp::WizardImportGrp() : CBCGPPropertyPage(WizardImportGrp::IDD)
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_szGrp = _T("");
	m_pParent = NULL;
	m_nExpIdx = 3;
	m_nSubjIdx = 6;
}

WizardImportGrp::~WizardImportGrp()
{
}

void WizardImportGrp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_GRPS, m_Cbo);
	DDX_CBString(pDX, IDC_CBO_GRPS, m_szGrp);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szDesc);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportGrp message handlers

void WizardImportGrp::OnSelchangeCboGrps() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szDesc;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	Groups grp;
	if( !grp.IsValid() ) return;
	if( FAILED( grp.Find( m_szID ) ) ) return;
	grp.GetNotes( m_szNotes );

	m_szGrp = szItem;
	UpdateData( FALSE );

	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

void WizardImportGrp::OnBnCreategrp() 
{
	AddGroupDlg d( &m_existing, this, TRUE );
	if( d.DoModal() )
	{
		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( pos )
		{
			CString szItem = d.m_lstNew.GetNext( pos );
			CString szDelim;
			::GetDelimiter( szDelim );
			int nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				m_szID = szItem.Mid( nPos + 2 );
				TRIM( m_szID );
				m_szDesc = szItem.Left( nPos );
				TRIM( m_szDesc );
				UpdateData( FALSE );

				szItem.Format( _T("%s%s%s"), m_szDesc, szDelim, m_szID );
				int nSel = m_Cbo.AddString( szItem );
				m_Cbo.SetCurSel( nSel );
				m_existing.AddTail( m_szID );

				m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
			}
		}
	}
}

BOOL WizardImportGrp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing group for the selected experiment.") );
	pWnd = GetDlgItem( IDC_BN_CREATEEXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new group to use for this experiment.") );

	// File/class is used in multiple wizards with different # of pages
	// DEFAULT page index # is in constructor (for import wizard)
	// if Generate data wizard, adjust index # appropriately
	m_pParent = (CPropertySheet*)GetParent();
	pWnd = m_pParent->GetPage( 0 );
	if( pWnd->IsKindOf( RUNTIME_CLASS( WizardImportGenStart ) ) )
	{
		m_nExpIdx--;
		m_nSubjIdx--;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportGrp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardImportGrp::OnSetActive() 
{
	CString szID, szDesc, szItem;
	HRESULT hr2 = E_FAIL;

	WizardImportExp* pExp = (WizardImportExp*)m_pParent->GetPage( m_nExpIdx );
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CLeftView* pLV = pMF->GetLeftView();
	CString szExpID = pExp->m_szID;
	if( szExpID != m_szExpID )
	{
		m_Cbo.ResetContent();
		m_szExpID = szExpID;
		m_szID.Empty();
		m_szDesc.Empty();
		m_szGrp.Empty();
	}
	else return CBCGPPropertyPage::OnSetActive();

	// Fill group list (for selected experiment)
	Groups grp;
	if( !grp.IsValid() ) return TRUE;
	m_existing.RemoveAll();
	HRESULT hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetID( szID );

		if( !pExp->m_fNew )
		{
			hr2 = pLV->m_pEML->FindForGroup( szExpID.AllocSysString(),
											 szID.AllocSysString() );
		}
		else hr2 = S_OK;
		if( SUCCEEDED( hr2 ) )
		{
			grp.GetDescriptionWithID( szItem );
			m_Cbo.AddString( szItem );
			m_existing.AddTail( szID );
		}

		hr = grp.GetNext();
	}
	// if only 1 in the list, select it
	if( m_Cbo.GetCount() == 1 )
	{
		m_Cbo.SetCurSel( 0 );
		OnSelchangeCboGrps();
	}

	if( m_szID != _T("") ) m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

LRESULT WizardImportGrp::OnWizardBack() 
{
	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

LRESULT WizardImportGrp::OnWizardNext() 
{
	WizardImportSubj* pSubj = (WizardImportSubj*)m_pParent->GetPage( m_nSubjIdx );
	if( pSubj->m_szID != _T("") )
		m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportGrp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	CPropertySheet* pParent = (CPropertySheet*)GetParent();
	if( pParent )
	{
		if( pParent->IsKindOf( RUNTIME_CLASS( WizardImportSheet ) ) )
			::GoToHelp( _T("importexport.html"), this );
		else
			::GoToHelp( _T("datageneration_wizard.html"), this );
	}

	return TRUE;
}

BOOL WizardImportGrp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

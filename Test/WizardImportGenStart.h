#pragma once

// WizardImportGenStart.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenStart dialog

class WizardImportGenStart : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportGenStart)

// Construction
public:
	WizardImportGenStart();
	~WizardImportGenStart();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_GENSTART };

// Overrides
public:
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

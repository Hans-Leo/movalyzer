.// Test.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Test.h"

#include "MainFrm.h"
#include "TestDoc.h"
#include "LeftView.h"
#include "Splash.h"
#include "AppLookDlg.h"
#include "..\Common\ErrorReport.h"
#include "..\NSSharedGUI\ActivateHTMLDlg.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// TODO: Rename this from Test to something else...been 3 years already lol


#define PM_AUTH_CODE	"©ńµ©ū©ł©ęż"	// QBa9}La3aM1aU.5R
// see also graphview.cpp
#define IM_AUTH_CODE	"¹³ł³©ł©ü©ń"		// IMq{1M{UaM1a4Wa9

/////////////////////////////////////////////////////////////////////////////
// CTestApp

BEGIN_MESSAGE_MAP(CTestApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_VIEW_APP_LOOK, OnViewAppLook)
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestApp construction

void GetAuthCodePMShared( CString& szCode )
{
	szCode = PM_AUTH_CODE;
	::Decode( szCode );
}

void GetAuthCodeIMShared( CString& szCode )
{
	szCode = IM_AUTH_CODE;
	::Decode( szCode );
}

CTestApp::CTestApp() : CBCGPWorkspace( TRUE )
{
	::SetUnhandledExceptionFilter( &NSUnhandledExceptionFilter );

	m_pDocTemplateMain = NULL;
	m_bSaveState = FALSE;
	m_fBackgroundImage = TRUE;
	m_bHiColorIcons = TRUE;

	// Init custom theme colors:
	m_lstCustomColors.AddTail( RGB( 210, 240, 179 ) );
	m_lstCustomColors.AddTail( RGB( 151, 192, 234 ) );
	m_lstCustomColors.AddTail( RGB( 203, 160, 224 ) );
	m_lstCustomColors.AddTail( RGB( 223, 159, 191 ) );
	m_lstCustomColors.AddTail( RGB( 243, 224, 185 ) );
	m_lstCustomColors.AddTail( RGB( 190, 178, 240 ) );
	m_lstCustomColors.AddTail( RGB( 232, 232, 216 ) );
	m_lstCustomColors.AddTail( RGB( 247, 218, 214 ) );
	m_lstCustomColors.AddTail( RGB( 189, 231, 224 ) );
	m_lstCustomColors.AddTail( RGB( 214, 234, 218 ) );

	m_clrCustomDef = RGB( 193, 223, 192 );
	m_clrCustom = (COLORREF)-1;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTestApp object

CTestApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTestApp initialization

BOOL CheckForFiles()
{
	// Check for necessary files
	CString szPath;
	CFileFind ff;
	CString szFile, szTemp;
	char szWS[ _MAX_PATH ];
	::GetSystemDirectory( szWS, _MAX_PATH );
	
	// Shared dir files
	::GetCommonPath( szPath );

	// Files
	szFile.Format( _T("%sctreelink.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sctloclib.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sdatamod.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sadminmod.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%spegrp32c.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sprocessmod.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sshowmod.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sinputmod.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}

	// App dir files
	::GetAppPath( szPath );
#ifdef	_SECURITY_
	szFile.Format( _T("%sSSCProt.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
#endif
	szFile.Format( _T("%scdrvdl32.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%scdrvhf32.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%sCdrvLDlgs.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}
	szFile.Format( _T("%scdrvxf32.dll"), szPath );
	if( !ff.FindFile( szFile ) )
	{
		szTemp.Format( _T("The following file was not found: %s."), szFile );
		BCGPMessageBox( szTemp);
		return FALSE;
	}

	// check for module type library version #s
	if( !::ConfirmModuleVersions( VER_ALL, szTemp ) )
	{
		CString szMsg = _T("MovAlyzeR cannot run because there are old modules installed.");
		szMsg += _T("\nPlease go to the download page at http://www.neuroscript.net to upgrade MovAlyzeR.");
		szMsg += _T("\n\nThe following modules are outdated and are required:\n\n");
		szMsg += szTemp;
		BCGPMessageBox( szMsg );
		return FALSE;
	}

	return TRUE;
}

BOOL CTestApp::InitInstance()
{
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);

	AfxEnableControlContainer();
	AfxInitRichEdit2();

	// verify all files are present to run the application
	CString szPath = AfxGetApp()->m_pszHelpFilePath;
	int nPos = szPath.ReverseFind( '\\' );
	szPath = szPath.Left( nPos + 1 );
	::SetAppPath( szPath );
	if( !CheckForFiles() ) return FALSE;

	// application pointer for modules
	::SetWorkspaceApp( this );

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	// Change the registry key under which our settings are stored.
	SetRegistryKey( _T("NeuroScript") );
	SetRegistryBase( _T("Settings" ) );

	LoadStdProfileSettings( 0 );  // Load standard INI file options (including MRU)

	// Initialize all bcg-related manager for usage
	InitContextMenuManager();
	InitKeyboardManager();
	InitSkinManager();
	InitShellManager();
	globalData.SetDPIAware();

	InitTooltipManager();
	CBCGPTooltipManager* pTTM = GetTooltipManager();
	if( pTTM )
	{
		CBCGPToolTipParams params;
		params.m_bVislManagerTheme = TRUE;
		pTTM->SetTooltipParams( BCGP_TOOLTIP_TYPE_ALL,
								RUNTIME_CLASS( CBCGPToolTipCtrl ), &params);
	}

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	m_pDocTemplateMain = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS( CTestDoc ),
		RUNTIME_CLASS( CMainFrame ),       // main SDI frame window
		RUNTIME_CLASS( CLeftView ) );
	AddDocTemplate( m_pDocTemplateMain );

	// Initialize OLE libraries
	if( !AfxOleInit() )
	{
		BCGPMessageBox( _T("Failed initializing OLE") );
		return FALSE;
	}

	// security
	NSSecurity* pSecurity = GetSecurity();
	if( pSecurity )
	{
		::SetSecurity( pSecurity );

		// license file (application) type
		pSecurity->SetApplication( APP_SECURITY_MOVALYZER );

		BOOL fExpired = FALSE, fMsgGiven = FALSE;
		if( !pSecurity->Initialize( fExpired, fMsgGiven ) )
		{
			// handle if expired and nothing activated
			if( fExpired )
			{
				long nTmp = 0;
				ActivateHTMLDlg d( m_pMainWnd, FALSE );
				d.m_nMode = ACT_MODE_EXPIRED;
				d.DoModal();
				if( !::IsSA( FALSE ) && !::IsOA( FALSE ) && !::IsGA( FALSE ) &&
					!::IsSPPU( nTmp ) && !::IsGPPU( nTmp ) && !::IsOPPU( nTmp ) )
				{
					pSecurity->EarlyTerm();
					CString szMsg = ::LoadAndDecode( IDS_MSG_THANKYOU );
					BCGPMessageBox( szMsg );
					return FALSE;
				}
			}
			else
			{
				pSecurity->EarlyTerm();
				if( !fMsgGiven ) BCGPMessageBox( _T("You are not authorized to run this application.\nPlease contact NeuroScript for assistance at support@neuroscriptsoftware.com.") );
				return FALSE;
			}
		}
	}

	// Set ProcessMod authorization code
	CString szAuthCode;
	GetAuthCodePMShared( szAuthCode );
	::SetAuthorizationProcessMod( szAuthCode );
	// Set InputMod authorization code
	GetAuthCodeIMShared( szAuthCode );
	::SetAuthorizationInputMod( szAuthCode );

	CString m_strFileName;

	// valid only for FilePrintTo
	CString m_strPrinterName;
	CString m_strDriverName;
	CString m_strPortName;

	// Dispatch commands specified on the command line
	if( !ProcessShellCommand( cmdInfo ) )
	{
		BCGPMessageBox( "Error in processing shell commands." );
		return FALSE;
	}

	// Save instance to Registry
	// the handle of the current app main window
	HWND hWnd = m_pMainWnd->GetSafeHwnd();	
	CString szHandle;
	szHandle.Format( "%ld", hWnd );
	WriteProfileString( "Control", "MainWndHwnd", szHandle );

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->DragAcceptFiles();
	m_pMainWnd->ShowWindow( SW_SHOW );
	m_pMainWnd->UpdateWindow();

	// window title
	((CMainFrame*)m_pMainWnd)->SetAppTitle();

	return TRUE;
}

int CTestApp::ExitInstance() 
{
	WriteProfileString( "Control", "MainWndHwnd", "" ); 

	CloseLogFile();
	::BCGCBProCleanUp();

	return CWinApp::ExitInstance();
}

//************************************
// Method:    GetSecurity
// FullName:  CRxApp::GetSecurity
// Access:    public 
// Returns:   NSSecurity*
// Qualifier:
//************************************
NSSecurity* CTestApp::GetSecurity()
{
#if defined _SECURITY_
	return &m_security;
#else
	return NULL;
#endif
}

/////////////////////////////////////////////////////////////////////////////
// Customization load/save methods

void CTestApp::LoadCustomState ()
{
	m_fBackgroundImage = GetInt( _T("BackgroundImage"), TRUE );
}

void CTestApp::SaveCustomState ()
{
	WriteInt ( _T("BackgroundImage"), m_fBackgroundImage );
}

/////////////////////////////////////////////////////////////////////////////
// CTestApp message handlers


BOOL CTestApp::PreTranslateMessage(MSG* pMsg)
{
	if( CSplashWnd::PreTranslateAppMessage( pMsg ) )
		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

void CTestApp::DoAbout()
{
	OnAppAbout();
}

void CTestApp::OnViewAppLook() 
{
	CAppLookDlg dlg (FALSE, m_pMainWnd);
	dlg.DoModal ();
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CBCGPDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };
	CString		m_szVersion;
	CString		m_szLicense;
	CBCGPURLLinkButton	m_eSNS;
	CBCGPURLLinkButton	m_eSNS2;
	CBCGPURLLinkButton	m_www;
	CBCGPURLLinkButton	m_TellUs;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CAboutDlg, CBCGPDialog)
END_MESSAGE_MAP()

CAboutDlg::CAboutDlg() : CBCGPDialog(CAboutDlg::IDD)
{
	m_szVersion = _T("Version x.x.x.x");
	m_szLicense = _T("Evaluation");
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_VERSION, m_szVersion);
	DDX_Text(pDX, IDC_TXT_LICENSE, m_szLicense);
	DDX_Control(pDX, IDC_TXT_E_SUPP_NS, m_eSNS);
	DDX_Control(pDX, IDC_TXT_E_SUPP_NS2, m_eSNS2);
	DDX_Control(pDX, IDC_TXT_WWW, m_www);
	DDX_Control(pDX, IDC_TXT_TELLUS, m_TellUs);
}

// App command to run the dialog
void CTestApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

BOOL CAboutDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Version
	int nVer = ::GetAppVersion();
	int nTh = nVer / 1000;
	int nH = ( nVer - ( nTh * 1000 ) ) / 100;
	int nT = ( nVer - ( nTh * 1000 ) - ( nH * 100 ) ) / 10;
	int nO = nVer - ( nTh * 1000 ) - ( nH * 100 ) - ( nT * 10 );
	m_szVersion.Format( _T("Version %d.%d.%d.%d"), nTh, nH, nT, nO );

	// Set HyperLinks
	m_www.SetURL( _T("HTTP://www.neuroscript.net") );
	m_www.SizeToContent();

	m_eSNS.SetURL( _T("http://www.neuroscript.net/support.php") );
	m_eSNS.SizeToContent();

	m_eSNS2.SetURL( _T("mailto:sales@neuroscript.net") );
	m_eSNS2.SizeToContent();

	m_TellUs.SetURL( _T("http://www.neuroscript.net/forum/forumdisplay.php?f=6") );
	m_TellUs.SizeToContent();

	BOOL fAdded = FALSE;
	CString szCur, szTemp;
	long nCnt = 0;
	int nDays = 0;
	if( ::IsD( &nDays ) && !::IsSA( FALSE ) && !::IsOA( FALSE ) && !::IsGA( FALSE ) &&
		!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
	{
		if( nDays > 0 )
			szTemp.Format( _T("MovAlyzeR (TRIAL - %d days)"), nDays );
		else
			szTemp = _T("MovAlyzeR (TRIAL - EXPIRED)");
		szCur += szTemp;
	}
	else
	{
		if( ::IsSA( FALSE ) && !::IsOA( FALSE ) )
		{
			szCur += _T("ScriptAlyzeR");
			fAdded = TRUE;
		}
		if( ::IsOA( FALSE ) )
		{
			szCur += _T("MovAlyzeR");
			fAdded = TRUE;
		}
		if( ::IsGA( FALSE ) )
		{
			if( fAdded ) szCur += _T(" && ");
			szCur += _T("GripAlyzeR");
			fAdded = TRUE;
		}
		if( !fAdded )
		{
			if( ::IsSPPU( nCnt ) )
			{
				szTemp.Format( _T("ScriptAlyzeR (%u points)"), nCnt );
				szCur += szTemp;
			}
			else if( ::IsGPPU( nCnt ) )
			{
				szTemp.Format( _T("GripAlyzeR (%u points)"), nCnt );
				szCur += szTemp;
			}
			else if( ::IsOPPU( nCnt ) )
			{
				szTemp.Format( _T("MovAlyzeR (%u points)"), nCnt );
				szCur += szTemp;
			}
		}
		if( fAdded && ::IsCS() ) szCur += _T(" [NETWORK VERSION]");
	}
	m_szLicense = szCur;

	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

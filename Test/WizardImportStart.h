#pragma once

// WizardImportStart.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportStart dialog

class WizardImportStart : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportStart)

// Construction
public:
	WizardImportStart();
	~WizardImportStart();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_START };

// Overrides
public:
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// WizardImportFormat.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportFormat dialog

class WizardImportFormat : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportFormat)

// Construction
public:
	WizardImportFormat();
	~WizardImportFormat();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_FORMAT };
	CBCGPComboBox	m_cboFormat;
	BOOL			m_fCustom;
	CString			m_szFormat;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnChkCustom();
	afx_msg void OnBnCustom();
	afx_msg void OnSelchangeCboFmt();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// BackupHistoryDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// BackupHistoryDlg dialog

class BackupHistoryDlg : public CBCGPDialog
{
// Construction
public:
	BackupHistoryDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_BACKUPS };
	CBCGPListCtrl	m_list;
	BOOL			m_fDat;
	CString			m_szPath;
	int				m_nType;
	CString			m_szExp;
	CString			m_szGrp;
	CString			m_szSubj;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	void FillList();
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnRestore();
	afx_msg void OnBnClear();
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnClickDB();
	DECLARE_MESSAGE_MAP()
};

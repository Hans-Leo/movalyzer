// WizardSetupCond.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupCond.h"
#include "WizardSetupSheet.h"
#include "..\DataMod\DataMod.h"
#include "ConditionSheet.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/////////////////////////////////////////////////////////////////////////////
// WizardSetupCond property page

IMPLEMENT_DYNCREATE(WizardSetupCond, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardSetupCond, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_CONDS, OnSelchangeCboConds)
	ON_BN_CLICKED(IDC_BN_CREATECOND, OnBnCreatecond)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupCond::WizardSetupCond() : CBCGPPropertyPage(WizardSetupCond::IDD)
{
	m_szCond = _T("");
	m_szID = _T("");
	m_szDesc = _T("");
	m_nReps = 2;
}

WizardSetupCond::~WizardSetupCond()
{
}

void WizardSetupCond::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_CONDS, m_Cbo);
	DDX_CBString(pDX, IDC_CBO_CONDS, m_szCond);
	DDX_Text(pDX, IDC_TXT_CONDID, m_szID);
	DDX_Text(pDX, IDC_TXT_CONDDESC, m_szDesc);
	DDX_Text(pDX, IDC_EDIT_REPS, m_nReps);
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupCond message handlers

void WizardSetupCond::OnSelchangeCboConds() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szDesc;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	NSConditions cond;
	if( !cond.IsValid() ) return;
	if( FAILED( cond.Find( m_szID ) ) ) return;
	short nVal = 0;

	m_szCond = szItem;
	UpdateData( FALSE );

	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

void WizardSetupCond::OnBnCreatecond() 
{
	ConditionSheet d( m_cond.m_pNSCondition, "", TRUE, FALSE, FALSE, NULL, NULL, this, FALSE, 0 );
	if( d.DoModal() == IDOK )
	{
		UpdateData( TRUE );
		m_cond.GetID( m_szID );
		m_cond.GetDescription( m_szDesc );
		UpdateData( FALSE );
		m_Cbo.SetCurSel( -1 );

		WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	}
}

BOOL WizardSetupCond::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_EXP_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_CBO_CONDS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing condition for this experiment."), _T("Existing Condition") );
	pWnd = GetDlgItem( IDC_BN_CREATECOND );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new condition to use for this experiment."), _T("New Condition") );
	pWnd = GetDlgItem( IDC_EDIT_REPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Enter the number of times this condition will be used within an experiment."), _T("Number of Trials") );

	// Fill condition list
	NSConditions cond;
	if( !cond.IsValid() ) return TRUE;
	CString szItem;
	HRESULT hr = cond.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cond.GetDescriptionWithID( szItem );
		m_Cbo.AddString( szItem );
		hr = cond.GetNext();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardSetupCond::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardSetupCond::OnSetActive() 
{
	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	if( m_szID != _T("") ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardSetupCond::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newexperiment.html#wizard"), this );
	return TRUE;
}

BOOL WizardSetupCond::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

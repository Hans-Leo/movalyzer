// BackupHistoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "BackupHistoryDlg.h"
#include "..\DataMod\DataMod.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BackupHistoryDlg dialog

BEGIN_MESSAGE_MAP(BackupHistoryDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_RESTORE, OnBnRestore)
	ON_BN_CLICKED(IDC_BN_CLEAR, OnBnClear)
	ON_BN_CLICKED(IDC_CHK_DAT, OnClickDB)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

BackupHistoryDlg::BackupHistoryDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(BackupHistoryDlg::IDD, pParent)
{
	m_fDat = TRUE;
}

void BackupHistoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Check(pDX, IDC_CHK_DAT, m_fDat);
}

/////////////////////////////////////////////////////////////////////////////
// BackupHistoryDlg message handlers

BOOL BackupHistoryDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

BOOL BackupHistoryDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Contains the history of all backups performed by this user."), _T("History") );
	pWnd = GetDlgItem( IDC_BN_RESTORE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Restore the data files for the selected item in the list."), _T("Restore") );
	pWnd = GetDlgItem( IDC_BN_CLEAR );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Empty the list of all backup histories."), _T("Clear History") );
	pWnd = GetDlgItem( IDC_CHK_DAT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check this to include database files during a restore."), _T("Include Database") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close without restoring."), _T("Cancel") );

	// list settings
	m_list.SendMessage( LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT );

	// List column headers
	m_list.InsertColumn( 0, _T("User"), LVCFMT_LEFT, 40, 0 );
	m_list.InsertColumn( 1, _T("Date"), LVCFMT_LEFT, 125, 1 );
	m_list.InsertColumn( 2, _T("Path"), LVCFMT_LEFT, 225, 2 );
	m_list.InsertColumn( 3, _T("Exp"), LVCFMT_LEFT, 50, 3 );
	m_list.InsertColumn( 4, _T("Grp"), LVCFMT_LEFT, 50, 4 );
	m_list.InsertColumn( 5, _T("Subj"), LVCFMT_LEFT, 50, 5 );
	m_list.InsertColumn( 6, _T("Comments"), LVCFMT_LEFT, 200, 6 );

	// Fill list
	FillList();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void BackupHistoryDlg::FillList()
{
	UpdateData( TRUE );
	m_list.DeleteAllItems();

	IBackup* pB = NULL;
	HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
		IID_IBackup, (LPVOID*)&pB );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create backup object.") );
		return;
	}

	BSTR bstrItem = NULL;
	CString szUser, szDate, szPath, szExp, szGrp, szSubj, szNotes;
	int nItem = 0;
	BackupType bt;

	hr = pB->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		pB->get_UserID( &bstrItem );
		szUser = bstrItem;
		pB->get_BackupDate( &bstrItem );
		szDate = bstrItem;
		pB->get_Path( &bstrItem );
		szPath = bstrItem;
		pB->get_BackupType( &bt );
		pB->get_Exp( &bstrItem );
		szExp = bstrItem;
		pB->get_Grp( &bstrItem );
		szGrp = bstrItem;
		pB->get_Subj( &bstrItem );
		szSubj = bstrItem;
		pB->get_Notes( &bstrItem );
		szNotes = bstrItem;

		if( ( m_fDat && ( bt == eB_DB ) ) || ( !m_fDat && ( bt != eB_DB ) ) )
		{
			nItem = m_list.InsertItem( nItem, szUser, 0 );
			m_list.SetItem( nItem, 1, LVIF_TEXT, szDate, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 2, LVIF_TEXT, szPath, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 3, LVIF_TEXT, szExp, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 4, LVIF_TEXT, szGrp, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 5, LVIF_TEXT, szSubj, 0, 0, 0, NULL );
			m_list.SetItem( nItem, 6, LVIF_TEXT, szNotes, 0, 0, 0, NULL );
		}

		hr = pB->GetNext();
	}
	pB->Release();
}

void BackupHistoryDlg::OnBnRestore() 
{
	UpdateData( TRUE );

	if( m_list.GetSelectedCount() == 0 )
	{
		BCGPMessageBox( _T("No item is selected.") );
		return;
	}

	int nIdx = m_list.GetNextItem( -1, LVNI_SELECTED );
	m_szPath = m_list.GetItemText( nIdx, 2 );
	m_szExp = m_list.GetItemText( nIdx, 3 );
	m_szGrp = m_list.GetItemText( nIdx, 4 );
	m_szSubj = m_list.GetItemText( nIdx, 5 );

	if( m_fDat ) m_nType = eB_DB;
	else if( m_szSubj != _T("") ) m_nType = eB_Subject;
	else if( m_szGrp != _T("") ) m_nType = eB_Group;
	else m_nType = eB_Experiment;

	CBCGPDialog::OnOK();
}

void BackupHistoryDlg::OnBnClear() 
{
	if( BCGPMessageBox( _T("Are you sure you want to clear the history? This cannot be undone."), MB_YESNO ) == IDNO )
		return;

	IBackup* pB = NULL;
	HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
								   IID_IBackup, (LPVOID*)&pB );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create backup object.") );
		return;
	}

	hr = pB->ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		hr = pB->Remove();
		if( FAILED( hr ) )
			BCGPMessageBox( _T("Unable to delete item.") );

		hr = pB->GetNext();
	}
	pB->Release();

	m_list.DeleteAllItems();
}

void BackupHistoryDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnBnRestore();

	*pResult = 0;
}

BOOL BackupHistoryDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("backup.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

void BackupHistoryDlg::OnClickDB()
{
	FillList();
}
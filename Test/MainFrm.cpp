// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Test.h"
#include <direct.h>
#include <afxinet.h>
#include <Mshtmhst.h>

#include "MainFrm.h"
#include "LeftView.h"
#include "RecView.h"
#include "MsgView.h"
#include "GraphView.h"
#include "RelationshipDlg.h"
#include "LegendDlg.h"
#include "Splash.h"
#include "CommentDlg.h"
#include "CrashDlg.h"
#include "ConvCalc.h"
#include "InstructionDlg.h"
#include "ImageDlg.h"
#include "AppLookDlg.h"

#include "WizardSetupSheet.h"
#include "WizardImportSheet.h"
#include "WizardImportGenSheet.h"
#include "WizardRunExpSheet.h"
#include "WizardDevSheet.h"
#include "WizardExportSheet.h"
#include "WizardImportRemoteSheet.h"

#include "PasswordDlg.h"
#include "Hyperlink.h"
#include "htmlhelp.h"
#include "Mail.h"
#include "HtmlDlg.h"
#include "Ver.h"
#include "PPUListDlg.h"
#include "AfxCtl.h"

#include "..\Common\UserObj.h"

#include "..\NSSharedGUI\TSDlg.h"
#include "..\NSShared\Crc32Static.h"

#include "..\CxImage\CxImage\ximage.h"

#include "..\NSSharedGUI\ActivateHTMLDlg.h"
#include "..\NSSharedGUI\GraphDlg.h"
#include "..\NSSharedGUI\SiteLoginDlg.h"
#include "..\NSSharedGUI\LoginDlg.h"
#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\NSSharedGUI\UserMessages.h"
#include "..\NSSharedGUI\Progress.h"
#include "..\NSSharedGUI\GettingStartedDlg.h"

#include "..\AdminMod\AdminMod.h"
#include "..\AdminMod\AdminMod_i.c"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define	IS_EXP_RUNNING() \
	CGraphView* pGV = GetRecordingPane(); \
	BOOL fRunExp = pGV ? !pGV->fRecording : FALSE; \
	CLeftView* pLV = GetLeftView(); \
	fRunExp &= pLV ? ( pLV->GetActionState() == ACTION_NONE ) : FALSE;

#define	ID_GROUP_SHORTCUTS	99999

LegendDlg*		_LegendDlg = NULL;
ConvCalc*		_convCalc = NULL;
CListCtrl*		_MsgPaneList = NULL;
CLeftView*		_LeftView = NULL;

HWND	_hwndHelp = NULL;
//status pane globals (for use between threads)
HWND	_hWndMain = NULL;
char	_pszStatusText[ 500 ];
long	_lProgress = 0;
long	_lProgressTotal = 0;

const int  iMaxUserToolbars		= 10;
const UINT uiFirstUserToolBarId	= AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId	= uiFirstUserToolBarId + iMaxUserToolbars - 1;

#define	UM_SHOWPROGRESS				(WM_USER + 1001)
#define	UM_SETSTATUSPROGRESS		(WM_USER + 1002)
#define	UM_SETSTATUSTEXT			(WM_USER + 1003)
#define	UM_ENABLESTATUSPROGRESS		(WM_USER + 1004)
#define	UM_DISABLESTATUSPROGRESS	(WM_USER + 1005)

#define REGISTRY_ROOT	_T("SOFTWARE\\NeuroScript\\MovAlyzeR\\");
#define CONTROL_BARS	_T("ControlBars\\");


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CBCGPFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CBCGPFrameWnd)
	ON_WM_TIMER()
	ON_WM_HELPINFO()
	ON_WM_CREATE()
	ON_WM_MOUSEMOVE()
	ON_WM_CLOSE()
	ON_COMMAND(IDM_VIEW_LOG, OnViewLog)
	ON_COMMAND(IDM_CLEAR_LOG, OnClearLog)
	ON_COMMAND(IDM_OBJ_RELATIONSHIPS, OnObjRelationships)
	ON_COMMAND(IDM_HELP_HELP, OnHelpHelp)
	ON_COMMAND(IDM_CLEAR_MESSAGES, OnClearMessages)
	ON_COMMAND(IDM_VIEW_LEGEND, OnViewLegend)
	ON_COMMAND(IDM_E_CLEAR, OnEClear)
	ON_COMMAND(IDM_E_STOPEXP, OnEStopexp)
	ON_COMMAND(IDM_E_STOPCOND, OnEStopcond)
	ON_COMMAND(IDM_E_STOPREC, OnEStoprec)
	ON_COMMAND(IDM_ADD_LOG, OnAddLog)
	ON_COMMAND(IDM_SEND_LOG, OnSendLog)
	ON_COMMAND(IDM_VIEW_QUEST, OnViewQuest)
	ON_COMMAND(IDM_O_QUICK, OnOQuick)
	ON_COMMAND(IDM_O_PATH, OnOPath)
	ON_COMMAND(IDM_O_INPUTDEVICE, OnOInputdevice)
	ON_UPDATE_COMMAND_UI(IDM_O_INPUTDEVICE, OnUpdateInputDevice)
	ON_COMMAND(IDM_O_INPUTDEVICESETTINGS, OnOInputdeviceSettings)
	ON_COMMAND(IDM_O_DOLOGGING, OnODologging)
	ON_COMMAND(IDM_DIG_TEST, OnDigTest)
	ON_COMMAND(IDM_DIG_RESET, OnDigReset)
	ON_COMMAND(IDM_OBJ_STOP, OnObjStop)
	ON_COMMAND(IDM_VIEW_REFRESH, OnViewRefresh)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_REFRESH, OnUpdateViewRefresh)
	ON_COMMAND(IDM_FILE_USERS, OnFileUsers)
	ON_COMMAND(IDM_FILE_BACKUPHISTORY, OnFileBackuphistory)
	ON_COMMAND(IDM_F_BACKUPDB, OnFBackupdb)
	ON_COMMAND(IDM_F_RESTOREDB, OnFRestoredb)
	ON_COMMAND(IDM_WIZ_EXPSETUP, OnWizExpsetup)
	ON_COMMAND(IDM_WIZ_DATAIMPORT, OnWizDataimport)
	ON_COMMAND(IDM_WIZ_REMOTEDATAIMPORT, OnWizRemoteDataimport)
	ON_COMMAND(IDM_F_EXPORT, OnWizDataExport)
	ON_COMMAND(IDM_WIZ_DATAGEN, OnWizDatagen)
	ON_COMMAND(IDM_F_BACKUPSYS, OnFBackupsys)
	ON_COMMAND(IDM_O_LOGDB, OnOLogdb)
	ON_COMMAND(IDM_O_LOGGRAPH, OnOLoggraph)
	ON_COMMAND(IDM_O_LOGPROC, OnOLogproc)
	ON_COMMAND(IDM_O_LOGTABLET, OnOLogtablet)
	ON_COMMAND(IDM_O_LOGUI, OnOLogui)
	ON_COMMAND(IDM_O_LOGALL, OnOLogall)
	ON_COMMAND(IDM_O_LOGNONE, OnOLognone)
	ON_COMMAND(IDM_HELP_QUICK, OnHelpQuick)
	ON_COMMAND(IDM_HELP_TUTORIAL, OnHelpTutorial)
	ON_COMMAND(IDM_WIZ_RUNEXP, OnWizRunexp)
	ON_COMMAND(ID_VIEW_PRIVATE, OnViewPrivate)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_LEGEND, OnUpdateViewLegend)
	ON_UPDATE_COMMAND_UI(IDM_O_QUICK, OnUpdateOQuick)
	ON_UPDATE_COMMAND_UI(IDM_O_DOLOGGING, OnUpdateODologging)
	ON_UPDATE_COMMAND_UI(IDM_OBJ_STOP, OnUpdateObjStop)
	ON_UPDATE_COMMAND_UI(IDM_O_LOGDB, OnUpdateOLogdb)
	ON_UPDATE_COMMAND_UI(IDM_O_LOGGRAPH, OnUpdateOLoggraph)
	ON_UPDATE_COMMAND_UI(IDM_O_LOGPROC, OnUpdateOLogproc)
	ON_UPDATE_COMMAND_UI(IDM_O_LOGTABLET, OnUpdateOLogtablet)
	ON_UPDATE_COMMAND_UI(IDM_O_LOGUI, OnUpdateOLogui)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PRIVATE, OnUpdateViewPrivate)
	ON_UPDATE_COMMAND_UI(IDM_E_STOPEXP, OnUpdateViewsStop)
	ON_UPDATE_COMMAND_UI(IDM_F_BACKUPDB, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_FILE_BACKUPHISTORY, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_O_INPUTDEVICE, OnUpdateViewsCont)
	ON_UPDATE_COMMAND_UI(IDM_O_INPUTDEVICESETTINGS, OnUpdateViewsSettings)
	ON_COMMAND(ID_VIEW_FULLSCREEN, OnViewFullScreen)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FULLSCREEN, OnUpdateViewFullScreen)
 	ON_COMMAND(ID_VIEW_MENU, OnViewMenu)
 	ON_UPDATE_COMMAND_UI(ID_VIEW_MENU, OnUpdateViewMenu)
	ON_COMMAND(ID_VIEW_INFO, OnViewInfo)
	ON_UPDATE_COMMAND_UI(ID_VIEW_INFO, OnUpdateViewInfo)
	ON_COMMAND(ID_VIEW_ID, OnViewId)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ID, OnUpdateViewId)
	ON_COMMAND(ID_VIEW_EXP, OnViewExp)
	ON_UPDATE_COMMAND_UI(ID_VIEW_EXP, OnUpdateViewExp)
	ON_COMMAND(ID_VIEW_TOOLBAR, OnViewAction)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBAR, OnUpdateViewAction)
	ON_COMMAND(ID_VIEW_CUSTOMIZE, OnViewCustomize)
	ON_UPDATE_COMMAND_UI(ID_VIEW_CUSTOMIZE, OnUpdateViewCustomize)
	ON_UPDATE_COMMAND_UI(IDM_E_STOPCOND, OnUpdateViewsStop)
	ON_UPDATE_COMMAND_UI(IDM_E_STOPREC, OnUpdateViewsStop)
	ON_UPDATE_COMMAND_UI(IDM_FILE_USERS, OnUpdateViewsUsers)
	ON_UPDATE_COMMAND_UI(IDM_O_PATH, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_F_RESTOREDB, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_WIZ_EXPSETUP, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_WIZ_DATAIMPORT, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_WIZ_REMOTEDATAIMPORT, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_F_EXPORT, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_QUEST, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_WIZ_DATAGEN, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_F_BACKUPSYS, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_WIZ_RUNEXP, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_DIG_TEST, OnUpdateViewsRunExp)
	ON_UPDATE_COMMAND_UI(IDM_DIG_RESET, OnUpdateViewsRunExp)
	ON_COMMAND(ID_VIEW_TEXTLABELS, OnViewTextlabels)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TEXTLABELS, OnUpdateViewTextlabels)
	ON_REGISTERED_MESSAGE(BCGM_RESETTOOLBAR, OnToolbarReset)
	ON_REGISTERED_MESSAGE(BCGM_TOOLBARMENU, OnToolbarContextMenu)
	ON_REGISTERED_MESSAGE(BCGM_CUSTOMIZEHELP, OnHelpCustomizeToolbars)
	ON_MESSAGE(UM_ENABLESTATUSPROGRESS, OnEnableProgress)
	ON_MESSAGE(UM_SHOWPROGRESS, OnShowProgress)
	ON_MESSAGE(UM_SETSTATUSPROGRESS, OnSetProgress)
	ON_MESSAGE(UM_DISABLESTATUSPROGRESS, OnDisableProgress)
	ON_MESSAGE(UM_SETSTATUSTEXT, OnShowStatusText)
	ON_MESSAGE(WM_TCARD, OnTCard)
	ON_COMMAND(IDM_DEVICE, OnDevice)
	ON_UPDATE_COMMAND_UI(IDM_DEVICE, OnUpdateDevice)
	ON_CBN_SELENDOK(IDM_DEVICE, OnDevice)
	ON_COMMAND(IDM_HELP_TROUBLESHOOTING, OnHelpTroubleshooting)
	ON_COMMAND(IDM_HELP_TROUBLESHOOTING_REC, OnHelpTroubleshootingRec)
	ON_COMMAND(IDM_HELP_ACTIVATE, OnHelpActivate)
	ON_UPDATE_COMMAND_UI(IDM_HELP_ACTIVATE, OnUpdateHelpActivate)
	ON_COMMAND(IDM_HELP_DEACTIVATE, OnHelpDeactivate)
	ON_UPDATE_COMMAND_UI(IDM_HELP_DEACTIVATE, OnUpdateHelpDeactivate)
	ON_COMMAND(IDM_HELP_LICENSE, OnHelpLicense)
	ON_COMMAND(IDM_VIEW_CALC, OnViewCalc)
	ON_COMMAND(IDM_HELP_PPU, OnHelpPpu)
	ON_UPDATE_COMMAND_UI(IDM_HELP_PPU, OnUpdateHelpPpu)
	ON_WM_DROPFILES()
	ON_COMMAND(ID_SUPPORT_CHECKFORUPDATES, OnCheckForUpdates)
	ON_UPDATE_COMMAND_UI(ID_SUPPORT_CHECKFORUPDATES, OnUpdateCheckForUpdates)
	ON_COMMAND(IDM_STIM_EDITOR, OnOpenStimEditor)
	ON_COMMAND(IDM_RUN_RX, OnOpenRx)
	ON_UPDATE_COMMAND_UI(IDM_STIM_EDITOR, OnUpdateOpenStimEditorRx)
	ON_COMMAND(IDM_WIZ_DEVSETUP, OnWizDevSetup)
	ON_COMMAND(UM_DEVICE_SETUP_WIZ, OnWizDevSetup)
	ON_UPDATE_COMMAND_UI(IDM_WIZ_DEVSETUP, OnUpdateWizDevSetup)
	ON_COMMAND(IDM_NS_WEBSITE, OnWebsite)
	ON_COMMAND(IDM_NS_SUP_WEBSITE, OnWebsiteSupport)
	ON_COMMAND(ID_TRAYPOPUP_MINIMIZE, OnTrayMinimize)
	ON_UPDATE_COMMAND_UI(ID_TRAYPOPUP_MINIMIZE, OnUpdateTrayMinimize)
	ON_COMMAND(ID_TRAYPOPUP_MAXIMIZE, OnTrayMaximize)
	ON_UPDATE_COMMAND_UI(ID_TRAYPOPUP_MAXIMIZE, OnUpdateTrayMaximize)
	ON_COMMAND(IDM_O_USER, OnOUser)
	ON_COMMAND(IDM_O_RECOVERPASS, OnRecoverPass)
	ON_UPDATE_COMMAND_UI(IDM_O_RECOVERPASS, OnUpdateRecoverPass)
	ON_COMMAND(IDM_O_RESTOREPASS, OnRestorePass)
	ON_UPDATE_COMMAND_UI(IDM_O_RESTOREPASS, OnUpdateRestorePass)
	ON_COMMAND(IDM_VIEW_GETTINGSTARTED, OnViewGettingStarted)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_GETTINGSTARTED, OnUpdateViewStartPage)
	ON_COMMAND(IDM_VIEW_STARTPAGE, OnViewStartPage)
	ON_UPDATE_COMMAND_UI(IDM_VIEW_STARTPAGE, OnUpdateViewStartPage)
	ON_MESSAGE( UNM_HYPERLINK_CLICKED, OnHyperlinkClicked )
	ON_COMMAND( IDM_FILE_CONVERTBMPTOPCX, OnConvertBMPtoPCX)
	ON_COMMAND( IDM_FILE_CONVERTPNGTOPCX, OnConvertPNGtoPCX)
	ON_COMMAND( IDM_FILE_CONVERTGIFTOPCX, OnConvertGIFtoPCX)
	ON_COMMAND( IDM_FILE_CONVERTJPGTOPCX, OnConvertJPGtoPCX)
	ON_COMMAND(IDM_F_IMPORT, OnFImport)
	ON_UPDATE_COMMAND_UI(IDM_F_IMPORT, OnUpdateFileImport)
	ON_COMMAND(ID_APP_EXIT_NOMEMORY, OnAppExitNoMemory)
	// CHART-based popup menu on-update handlers
	ON_UPDATE_COMMAND_UI(IDC_BN_VIEW, OnUpdateShowData)
	ON_UPDATE_COMMAND_UI_RANGE( IDM_CHART_SYMBOL_SEG_0, IDM_CHART_SYMBOL_SEG_36, OnUpdateSegSymbol )
	ON_UPDATE_COMMAND_UI_RANGE( IDM_CHART_SYMBOL_SUB_0, IDM_CHART_SYMBOL_SUB_36, OnUpdateSubSymbol )
	ON_UPDATE_COMMAND_UI_RANGE( TF_MENU_BASE, TF_MENU_BASE + 15, OnUpdateFeedbackTF )
	ON_UPDATE_COMMAND_UI(IDC_CHK_SUP, OnUpdateSupAnn)
	ON_UPDATE_COMMAND_UI(IDC_CHK_ANN, OnUpdateShowXY)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame() : MessageTarget()
{
	m_fRecording = FALSE;
	::CoInitialize( NULL );
	m_fContinue = FALSE;
	m_fInit = FALSE;
	m_fMainToolbarMenu = FALSE;
	m_fClearLastUser = FALSE;
}

CMainFrame::~CMainFrame()
{
	m_wndSplitter.DestroyWindow();
	m_wndSplitter2.DestroyWindow();
	m_wndSplitter3.DestroyWindow();

	DeleteProcess();

	MessageManager::Destroy();

	::CoUninitialize();
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CBCGPFrameWnd::OnCreate( lpCreateStruct ) == -1 )
		return -1;

	_hWndMain = m_hWnd;

	// APPEARANCE
	CAppLookDlg dlg( TRUE, this );
	dlg.SetLook( TRUE, this );

	// Set toolbar and menu image size:
	CBCGPToolBar::SetSizes( CSize( 36, 28 ), CSize( 22, 20 ) );
	CBCGPToolBar::SetMenuSizes( CSize( 22, 21 ), CSize( 16, 15 ) );

	CList<UINT, UINT>	lstBasicCommands;
	lstBasicCommands.AddTail( IDM_FILE_USERS );
	lstBasicCommands.AddTail( IDM_FILE_IMPORT );
	lstBasicCommands.AddTail( IDM_F_IMPORT );
	lstBasicCommands.AddTail( IDM_F_EXPORT );
	lstBasicCommands.AddTail( IDM_WIZ_DEVSETUP );
	lstBasicCommands.AddTail( IDM_WIZ_EXPSETUP );
	lstBasicCommands.AddTail( IDM_F_BACKUPSYS );
	lstBasicCommands.AddTail( ID_APP_EXIT );
	lstBasicCommands.AddTail( IDM_O_DOLOGGING );
	lstBasicCommands.AddTail( IDM_HELP_HELP );
	lstBasicCommands.AddTail( IDM_HELP_TROUBLESHOOTING );
	lstBasicCommands.AddTail( IDM_HELP_ACTIVATE );
	lstBasicCommands.AddTail( IDM_HELP_PPU );
	lstBasicCommands.AddTail( ID_SUPPORT_CHECKFORUPDATES );
	lstBasicCommands.AddTail( ID_APP_ABOUT );
	lstBasicCommands.AddTail( IDM_HELP_LICENSE );
	lstBasicCommands.AddTail( IDM_O_QUICK );
	lstBasicCommands.AddTail( IDM_OBJ_RELATIONSHIPS );
	lstBasicCommands.AddTail( IDM_VIEW_CALC );
	lstBasicCommands.AddTail( IDM_VIEW_REFRESH );
	lstBasicCommands.AddTail( ID_VIEW_TEXTLABELS );
	lstBasicCommands.AddTail( IDM_O_INPUTDEVICE );
	lstBasicCommands.AddTail( IDM_O_LOGALL );
	lstBasicCommands.AddTail( IDM_O_LOGNONE );
	lstBasicCommands.AddTail( IDM_DIG_TEST );
	lstBasicCommands.AddTail( IDM_VIEW_QUEST );
	lstBasicCommands.AddTail( IDM_EF_ADD );
	lstBasicCommands.AddTail( IDM_E_ADDGROUP );
	lstBasicCommands.AddTail( IDM_E_ADDCONDITION );
	lstBasicCommands.AddTail( IDM_E_REPROCESS );
	lstBasicCommands.AddTail( IDM_E_SUM );
	lstBasicCommands.AddTail( IDM_E_A_ALL );
	lstBasicCommands.AddTail( IDM_E_EDIT );
	lstBasicCommands.AddTail( IDM_GF_ADD );
	lstBasicCommands.AddTail( IDM_G_ADDSUBJ );
	lstBasicCommands.AddTail( IDM_G_EDIT );
	lstBasicCommands.AddTail( IDM_G_REPROCESS );
	lstBasicCommands.AddTail( IDM_SF_ADD );
	lstBasicCommands.AddTail( IDM_S_EXP );
	lstBasicCommands.AddTail( IDM_S_REPROCESS );
	lstBasicCommands.AddTail( IDM_S_EDIT );
	lstBasicCommands.AddTail( IDM_CF_ADD );
	lstBasicCommands.AddTail( IDM_C_REPROCESS );
	lstBasicCommands.AddTail( IDM_C_REPS );
	lstBasicCommands.AddTail( IDM_C_EDIT );
	lstBasicCommands.AddTail( IDM_T_VIEW );
	lstBasicCommands.AddTail( IDM_T_CHART_TF );
	lstBasicCommands.AddTail( IDM_STF_ADD );
	lstBasicCommands.AddTail( IDM_ST_ADDELEMENTS );
	lstBasicCommands.AddTail( IDM_ST_DISPLAY );
	lstBasicCommands.AddTail( IDM_ST_EDIT );
	lstBasicCommands.AddTail( IDM_ELF_ADD );
	lstBasicCommands.AddTail( IDM_EL_DISPLAY );
	lstBasicCommands.AddTail( IDM_EL_DUP );
	lstBasicCommands.AddTail( IDM_EL_EDIT );
	lstBasicCommands.AddTail( IDM_CATF_ADD );
	lstBasicCommands.AddTail( IDM_CAT_EDIT );
	lstBasicCommands.AddTail( IDM_OBJ_STOP );
//	CBCGPToolBar::SetBasicCommands( lstBasicCommands );
	// uncomment to do MS style "hide not common stuff"

	// Create menu bar (replaces the standard menu):
	if( !m_wndMenuBar.Create ( this, dwDefaultToolbarStyle ) )
	{
		TRACE0("Failed to create menubar\n");
		return -1;      // fail to create
	}
	m_wndMenuBar.SetBarStyle( m_wndMenuBar.GetBarStyle() | WS_CHILD | WS_VISIBLE | CBRS_TOP |
							 CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC | CBRS_HIDE_INPLACE |
							 CBRS_GRIPPER | CBRS_BORDER_3D );
	m_wndMenuBar.SetWindowText( _T("MovAlyzeR Menu") );
	CBCGPPopupMenu::SetForceMenuFocus( FALSE );

	// Main toolbar
	if (!m_wndToolBar.CreateEx( this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
								| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC,
								CRect( 1, 1, 1, 1 ), IDR_MAINFRAME ) ||
		!m_wndToolBar.LoadToolBar( IDR_MAINFRAME ) )
	{
		TRACE0("Failed to create main toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolBar.SetWindowText( _T("Main Toolbar") );


	// Info toolbar
	if (!m_wndToolBarInfo.Create( this, WS_CHILD | WS_VISIBLE | CBRS_TOP |
										CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC | CBRS_HIDE_INPLACE |
										CBRS_GRIPPER | CBRS_BORDER_3D, IDR_TB_INFO ) ||
		!m_wndToolBarInfo.LoadToolBar( IDR_TB_INFO ))
	{
		TRACE0("Failed to create info toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolBarInfo.SetWindowText( _T("Info Toolbar") );

	// Experiment toolbar
	if (!m_wndToolBarExp.Create( this, WS_CHILD | WS_VISIBLE | CBRS_TOP |
									CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC | CBRS_HIDE_INPLACE |
									CBRS_GRIPPER | CBRS_BORDER_3D, IDR_TB_EXP ) ||
		!m_wndToolBarExp.LoadToolBar( IDR_TB_EXP ))
	{
		TRACE0("Failed to create experiment toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolBarExp.SetWindowText( _T("Experiment Toolbar") );

	// Update toolbar (NOT SHOWN) - used for menu icon only
	OSVERSIONINFO osv;
	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	if( GetVersionEx( &osv ) )
	{
		// only show if Vista or later
		if( osv.dwMajorVersion == 6 )
		{
			if( !m_wndToolBarUpdate.Create( this, WS_CHILD, IDR_TB_UPDATE ) ||
				!m_wndToolBarUpdate.LoadToolBar( IDR_TB_UPDATE ) )
			{
				TRACE0("Failed to create update toolbar\n");
				return -1;      // fail to create
			}
		}
	}

	// Input Device toolbar
	if (!m_wndToolBarID.Create( this, WS_CHILD | WS_VISIBLE | CBRS_TOP |
									CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC | CBRS_HIDE_INPLACE |
									CBRS_GRIPPER | CBRS_BORDER_3D, IDR_TB_SETTINGS ) ||
		!m_wndToolBarID.LoadToolBar( IDR_TB_SETTINGS ))
	{
		TRACE0("Failed to create device toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolBarID.SetWindowText( _T("Device Toolbar") );

	// common tool bar for menu (not displayed as toolbar)
	if (!m_wndToolBarCommon.Create( this, WS_CHILD | WS_VISIBLE | CBRS_TOP |
									CBRS_TOOLTIPS | CBRS_FLYBY |CBRS_SIZE_DYNAMIC | CBRS_HIDE_INPLACE |
									CBRS_GRIPPER | CBRS_BORDER_3D, IDR_TB_COMMON ) ||
		!m_wndToolBarCommon.LoadToolBar( IDR_TB_COMMON ))
	{
		TRACE0("Failed to create device toolbar\n");
		return -1;      // fail to create
	}
	m_wndToolBarCommon.SetWindowText( _T("Common Actions Toolbar") );

	ShowControlBar( &m_wndToolBarCommon, FALSE, FALSE, TRUE );

	m_wndToolBar.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, _T("Customize..."));
	m_wndToolBarInfo.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, _T("Customize..."));
	m_wndToolBarExp.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, _T("Customize..."));
	m_wndToolBarID.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, _T("Customize..."));

	// Docking
	EnableDocking( CBRS_ALIGN_ANY );
	m_wndMenuBar.EnableDocking( CBRS_ALIGN_ANY );
	m_wndToolBar.EnableDocking( CBRS_ALIGN_ANY );
	m_wndToolBarInfo.EnableDocking( CBRS_ALIGN_ANY );
	m_wndToolBarExp.EnableDocking( CBRS_ALIGN_ANY );
	m_wndToolBarID.EnableDocking( CBRS_ALIGN_ANY );
	EnableAutoHideBars( CBRS_ALIGN_ANY );

	// Initial/default docking structure (will be updated by registry)
	DockControlBar( &m_wndMenuBar );
	DockControlBar( &m_wndToolBarExp );
	DockControlBarLeftOf( &m_wndToolBarInfo, &m_wndToolBarExp );
	DockControlBarLeftOf( &m_wndToolBarID, &m_wndToolBarInfo );
	DockControlBarLeftOf( &m_wndToolBar, &m_wndToolBarID );

	// Allow user-defined toolbar operations:
	InitUserToobars( NULL, uiFirstUserToolBarId, uiLastUserToolBarId );
#define ID_VIEW_TOOLBARS                99999
	EnableControlBarMenu( TRUE, ID_VIEW_CUSTOMIZE, _T("Customize..."),
						  ID_VIEW_TOOLBARS, FALSE, TRUE );

	// Create status bar
	if( !m_wndStatusBar.Create( this ) )
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	// add menu bar show option
	HICON hIcon = (HICON)::LoadImage( AfxGetResourceHandle(),
		MAKEINTRESOURCE( IDI_MENU ),
		IMAGE_ICON,
		::GetSystemMetrics( SM_CXSMICON ),
		::GetSystemMetrics( SM_CYSMICON ),
		LR_SHARED );
	m_wndStatusBar.AddElement( new CBCGPRibbonStatusBarPane( ID_VIEW_MENU, _T(""),
		FALSE, hIcon ), _T("View Menu") );
	// separator
	m_wndStatusBar.AddSeparator();
	// website
	m_wndStatusBar.AddElement( new CBCGPRibbonHyperlink( IDM_NS_WEBSITE,
														 _T("www.neuroscript.net"),
														 _T("http://www.neuroscript.net") ),
							   _T("Link to website") );
	// separator
	m_wndStatusBar.AddSeparator();
	// website
	m_wndStatusBar.AddElement( new CBCGPRibbonHyperlink( IDM_NS_SUP_WEBSITE,
														_T("support"),
														_T("http://www.neuroscript.net/support.php") ),
							   _T("Link to website support") );
	// separator
	m_wndStatusBar.AddSeparator();
	// current user
	hIcon = (HICON)::LoadImage( AfxGetResourceHandle(),
									  MAKEINTRESOURCE( IDI_USER ),
									  IMAGE_ICON,
									  ::GetSystemMetrics( SM_CXSMICON ),
									  ::GetSystemMetrics( SM_CYSMICON ),
									  LR_SHARED );
	m_wndStatusBar.AddElement( new CBCGPRibbonStatusBarPane( IDM_FILE_USERS, _T("USER"),
															 FALSE, hIcon ),
							   _T("Current User") );
	// separator
	m_wndStatusBar.AddSeparator();
	// input device
	hIcon = (HICON)::LoadImage( AfxGetResourceHandle(),
								MAKEINTRESOURCE( IDR_EXP ),
								IMAGE_ICON,
								::GetSystemMetrics( SM_CXSMICON ),
								::GetSystemMetrics( SM_CYSMICON ),
								LR_SHARED );
	m_wndStatusBar.AddElement( new CBCGPRibbonStatusBarPane( IDM_WIZ_DEVSETUP, _T("MOUSE"),
															 FALSE, hIcon ),
							   _T("Input Device") );
	// Logging
	hIcon = (HICON)::LoadImage( AfxGetResourceHandle(),
		MAKEINTRESOURCE( IDI_LOGGING ),
		IMAGE_ICON,
		::GetSystemMetrics( SM_CXSMICON ),
		::GetSystemMetrics( SM_CYSMICON ),
		LR_SHARED );
	m_wndStatusBar.AddExtendedElement( new CBCGPRibbonStatusBarPane( IDM_O_DOLOGGING, _T(""),
																	 FALSE, hIcon ), _T("Toggle Logging") );
	// Rapid View
	hIcon = (HICON)::LoadImage( AfxGetResourceHandle(),
		MAKEINTRESOURCE( IDI_RAPID ),
		IMAGE_ICON,
		::GetSystemMetrics( SM_CXSMICON ),
		::GetSystemMetrics( SM_CYSMICON ),
		LR_SHARED );
	m_wndStatusBar.AddExtendedElement( new CBCGPRibbonStatusBarPane( IDM_O_QUICK, _T(""),
																	 FALSE, hIcon ), _T("Toggle Detailed Output") );
	// refresh
	// 	hIcon = (HICON)::LoadImage( AfxGetResourceHandle(),
	// 								MAKEINTRESOURCE( IDI_REFRESH ),
	// 								IMAGE_ICON,
	// 								::GetSystemMetrics( SM_CXSMICON ),
	// 								::GetSystemMetrics( SM_CYSMICON ),
	// 								LR_SHARED );
	// 	m_wndStatusBar.AddExtendedElement( new CBCGPRibbonStatusBarPane( IDM_VIEW_REFRESH, _T(""),
	// 																	 FALSE, hIcon ), _T("Refresh Tree") );

	// create caption bar
// 	if( !m_wndCaptionBar.Create( WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, this, -1, -1 ) )
// 	{
// 		TRACE0("Failed to create caption bar\n");
// 		return -1;      // fail to create
// 	}
// 	m_wndCaptionBar.SetFlatBorder();
// 	m_wndCaptionBar.SetButton( "Login", IDM_FILE_USERS, CBCGPCaptionBar::ALIGN_LEFT );
// 	m_wndCaptionBar.SetButtonToolTip( _T("Login"), _T("Login as the specified user.") );
// 	m_wndCaptionBar.m_clrBarBackground = RGB( 208, 72, 73 );
// 	m_wndCaptionBar.m_clrBarText = RGB( 255, 255, 255 );

	// set to global var
	Progress::SetShowProgressCB( ShowProgress );
	Progress::SetSetStatusTextCB( ShowStatusText );
	Progress::SetEnableProgressCB( EnableProgress );
	Progress::SetSetProgressCB( SetProgress );
	Progress::SetDisableProgressCB( DisableProgress );

	// window manager
	::SetWindowManager( &m_wm );

	// Output window
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	// system tray
#define	WM_ICON_NOTIFY			WM_APP + 10
#ifndef SYSTEMTRAY_USEW2K
#define NIIF_WARNING 0
#endif
    hIcon = ::LoadIcon( AfxGetResourceHandle(), MAKEINTRESOURCE( IDR_MAINFRAME ) );
	if( !m_wndSysTrayIcon.Create(
                        NULL,									// Let icon deal with its own messages
                        WM_ICON_NOTIFY,							// Icon notify message to use
                        _T("MovAlyzeR - Right-click for menu"),	// tooltip
                        hIcon,
                        IDR_SYSTRAY ) )							// ID of tray icon
    {
		TRACE0("Failed to create system tray icon\n");
		return -1;      // fail to create
    }
    m_wndSysTrayIcon.SetMenuDefaultItem( ID_TRAYPOPUP_MINIMIZE, FALSE );

	// allow full screen mode (w/o menu) and assign to menu item
	EnableFullScreenMainMenu( FALSE );
	EnableFullScreenMode( ID_VIEW_FULLSCREEN );

	m_fInit = TRUE;

	// CRASH DETECTION
	CString szCrashFile;
	::GetDataPath( szCrashFile );
	szCrashFile += _T("mova.zzz");
	// Check for existence of crash file - means at some point the software crashed
	CFileFind ff;
	BOOL fCrashed = ff.FindFile( szCrashFile );
	// if we find a crash, we need to inform the user and provide the option to report it
	if( fCrashed )
	{
		// hide the splash screen
		CSplashWnd::RemoveSplashScreen();

		// open file to get user
		CStdioFile f;
		CString szUser;
		if( f.Open( szCrashFile, CFile::modeRead ) )
		{
			f.ReadString( szUser );
			f.Close();
		}
		if( szUser == _T("") ) szUser = _T("UNKNOWN");
		CString szMsg = _T("There was a previous unhandled crash that occurred with user ");
		szMsg += szUser;
		szMsg += _T(".\n\nYou will now be taken to the problem reporting system.");
		BCGPMessageBox( szMsg, MB_OK | MB_ICONWARNING );
		// delete crash file
		try
		{
			CFile::Remove( szCrashFile );
		}
		catch( CFileException* e )
		{
			char pszErr[ 200 ];
			e->GetErrorMessage( pszErr, 200 );
			e->Delete();
			szMsg = pszErr;
			::OutputAMessage( szMsg, TRUE );
		}
		// Open dialog for reporting
		CrashDlg cd( this );
		if( cd.DoModal() == IDOK ) OnSendLog();
	}

	// ****IMPORTANT****
	// Do NOT uncomment this out unless you want to crash the program (for testing purposes)
//	*(int*)0 = 1;
	// ****IMPORTANT****

	return 0;
}

BOOL CMainFrame::OnCreateClient( LPCREATESTRUCT, CCreateContext* pContext )
{
	// Application window icon
	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_MAINFRAME ) : NULL;
	if( hIcon )
	{
		SetIcon( hIcon, TRUE );
		SetIcon( hIcon, FALSE );
	}

	// create splitter windows
	if( !m_wndSplitter.CreateStatic( this, 2, 1 ) )
		return FALSE;
	if( !m_wndSplitter2.CreateStatic( &m_wndSplitter, 1, 2,
									  WS_CHILD | WS_VISIBLE | WS_BORDER,
									  m_wndSplitter.IdFromRowCol( 0, 0 ) ) )
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}
	if( !m_wndSplitter3.CreateStatic( &m_wndSplitter2, 2, 1,
									  WS_CHILD | WS_VISIBLE | WS_BORDER,
									  m_wndSplitter2.IdFromRowCol( 0, 1 ) ) )
	{
		m_wndSplitter.DestroyWindow();
		m_wndSplitter2.DestroyWindow();
		return FALSE;
	}
	if( !m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CLeftView), CSize(0, 0), pContext) ||
		!m_wndSplitter3.CreateView(0, 0, RUNTIME_CLASS(CRecView), CSize(0, 0), pContext) ||
		!m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CGraphView), CSize(0, 0), pContext) ||
		!m_wndSplitter.CreateView(1, 0, RUNTIME_CLASS(CMsgView), CSize(0,0), pContext) )
	{
		m_wndSplitter.DestroyWindow();
		m_wndSplitter2.DestroyWindow();
		m_wndSplitter3.DestroyWindow();
		return FALSE;
	}

	// default window widths
	m_wndSplitter2.SetColumnInfo( 0, 270, 0 );
	m_wndSplitter3.SetRowInfo( 0, 40, 0 );
	m_wndSplitter.SetRowInfo( 0, 375, 0 );
	MoveWindow( 0, 0, 580, 450, TRUE );

	// last user
	CString szVal, szKey;
	szKey = _T("LastUser");
	szVal = theApp.GetString( szKey, _T("") );
	::SetCurrentUser( szVal );

	OnCreateClient2();

	// check for updates if auto-update is on
	if( ::IsAutoUpdateOn() ) OnCheckForUpdates();

	// Splash Screen.
	CSplashWnd::ShowSplashScreen( this );

	return TRUE;
}

void CMainFrame::OnCreateClient2()
{
	// WINDOW WIDTHS
	int nWidth = 640, nHeight = 480, nTWidth = 220, nTHeight = 325, nMHeight = 0,
		nRHeight = 40, x = 0, y = 0;
	TreeMemory tm;
	CFile f;
	CString szMem;

	// File for reading memory
	if( ConfirmPrefs( TRUE ) )
	{
		::GetDataPathRoot( szMem );
		szMem += _T("\\Mem.sta");
		if( f.Open( szMem, CFile::modeRead | CFile::typeBinary ) )
		{
			// Read
			f.Read( (LPVOID)&nWidth, sizeof( int ) );
			f.Read( (LPVOID)&tm, sizeof( tm ) );
			// Main window
			f.Read( (LPVOID)&x, sizeof( int ) );
			f.Read( (LPVOID)&y, sizeof( int ) );
			f.Read( (LPVOID)&nWidth, sizeof( int ) );
			f.Read( (LPVOID)&nHeight, sizeof( int ) );
			// Tree width
			f.Read( (LPVOID)&nTWidth, sizeof( int ) );
			// Tree height
			f.Read( (LPVOID)&nTHeight, sizeof( int ) );
			// Msg View height
			f.Read( (LPVOID)&nMHeight, sizeof( int ) );
			// Rec View height
			f.Read( (LPVOID)&nRHeight, sizeof( int ) );
		}
	}

	if( nTWidth < 10 ) nTWidth = 150;
	if( nRHeight < 10 ) nRHeight = 40;
	if( nTHeight < 10 ) nTHeight = 325;
	x = MAX( x, 0 );
	y = MAX( y, 0 );
	if( nWidth < 20 ) nWidth = 640;
	if( nHeight < 10 ) nHeight = 480;
	m_wndSplitter2.SetColumnInfo( 0, nTWidth, 0 );
	m_wndSplitter3.SetRowInfo( 0, nRHeight, 0 );
	m_wndSplitter.SetRowInfo( 0, nTHeight, 0 );
	MoveWindow( x, y, nWidth, nHeight, TRUE );
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~(FWS_ADDTOTITLE);

	if( !CBCGPFrameWnd::PreCreateWindow( cs ) ) return FALSE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CBCGPFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CBCGPFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

CRecView* CMainFrame::GetSettingsPane()
{
	if( !this ) return NULL;
	CWnd* pWnd = m_wndSplitter3.GetPane(0, 0);
	CRecView* pView = DYNAMIC_DOWNCAST(CRecView, pWnd);
	return pView;
}

CMsgView* CMainFrame::GetMsgPane()
{
	if( !this ) return NULL;
	CWnd* pWnd = m_wndSplitter.GetPane( 1, 0 );
	CMsgView* pView = DYNAMIC_DOWNCAST(CMsgView, pWnd);
	return pView;
}

CListCtrl* CMainFrame::GetMsgPaneList()
{
	if( !_MsgPaneList )
	{
		CListView* pMsgWnd = (CListView*)GetMsgPane();
		CListCtrl& lst = pMsgWnd->GetListCtrl();
		_MsgPaneList = &lst;
	}

	return _MsgPaneList;
}

CGraphView* CMainFrame::GetRecordingPane()
{
	if( !this ) return NULL;
	CWnd* pWnd = m_wndSplitter3.GetPane( 1, 0 );
	CGraphView* pView = DYNAMIC_DOWNCAST(CGraphView, pWnd);
	return pView;
}

CLeftView* CMainFrame::GetLeftView()
{
	if( !_LeftView )
	{
		if( !this ) return NULL;
		CWnd* pWnd = m_wndSplitter2.GetPane( 0, 0 );
		CLeftView* pView = DYNAMIC_DOWNCAST(CLeftView, pWnd);
		_LeftView = pView;
	}

	return _LeftView;
}

int CMainFrame::GetTreeSize()
{
	if( !this ) return 0;
	int nSize = 0, nMin = 0;
	m_wndSplitter2.GetColumnInfo( 0, nSize, nMin );

	return nSize;
}

void CMainFrame::SetTreeSize( int nSize )
{
	if( !this ) return;
	m_wndSplitter2.SetColumnInfo( 0, nSize, 0 );
}

int CMainFrame::GetTreeHeight()
{
	if( !this ) return 0;
	int nSize = 0, nMin = 0;
	m_wndSplitter.GetRowInfo( 0, nSize, nMin );

	return nSize;
}

void CMainFrame::SetTreeHeight( int nSize )
{
	if( !this ) return;
	m_wndSplitter.SetRowInfo( 0, MAX( nSize, 0 ), 0 );
}

int CMainFrame::GetMsgSize()
{
	if( !this ) return 0;
	int nSize = 0, nMin = 0;
	m_wndSplitter.GetRowInfo( 1, nSize, nMin );

	return nSize;
}

void CMainFrame::SetMsgSize( int nSize )
{
	if( !this ) return;
	m_wndSplitter.SetRowInfo( 1, MAX( nSize, 0 ), 0 );
}

int CMainFrame::GetRecSize()
{
	if( !this ) return 0;
	int nSize = 0, nMin = 0;
	m_wndSplitter3.GetRowInfo( 0, nSize, nMin );

	return nSize;
}

void CMainFrame::SetRecSize( int nSize )
{
	if( !this ) return;
	m_wndSplitter3.SetRowInfo( 0, MAX( nSize, 0 ), 0 );
}

void CMainFrame::GetFramePos( int& x, int& y, int& width, int& height )
{
	RECT rect;
	GetWindowRect( &rect );

	x = rect.left;
	y = rect.top;
	width = abs( rect.left - rect.right );
	height = abs( rect.bottom - rect.top );
}

void CMainFrame::OnViewLog() 
{
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");

	CFileFind ff;
	if( !ff.FindFile( szAppPath ) )
	{
		BCGPMessageBox( IDS_NOAL );
		return;
	}

	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szAppPath;
	OutputAMessage( szCmd, TRUE );
	WinExec( szCmd, SW_SHOW );
}

void CMainFrame::OnClearLog() 
{
	if( BCGPMessageBox( IDS_ALDEL_SURE, MB_YESNO ) == IDNO )
		return;

	OutputAMessage( IDS_AL_DEL, TRUE );

	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");

	CFileFind ff;
	if( ff.FindFile( szAppPath ) )
		CFile::Remove( szAppPath );
}

void CALLBACK CMainFrame::ShowStatusText( CString szMsg )
{
	::PostMessage( _hWndMain, UM_SETSTATUSTEXT, 0, 0 );
}

LRESULT CMainFrame::OnShowStatusText( WPARAM, LPARAM )
{
	return 0;
}

void CALLBACK CMainFrame::ShowProgress( BOOL fShow )
{
	::PostMessage( _hWndMain, UM_SHOWPROGRESS, fShow, 0 );
}

LRESULT CMainFrame::OnShowProgress(WPARAM wp, LPARAM)
{
	BOOL fShow = (BOOL)wp;
	if( fShow )
	{
		int cxFree = m_wndStatusBar.GetSpace();
		if( cxFree < 20 ) return 0;	// not enough space

		int cxProgress = min( cxFree, 150 );
		CBCGPRibbonProgressBar* pProgressBar = 
			new CBCGPRibbonProgressBar( ID_STATUS_PROGRESS, cxProgress );
		pProgressBar->SetInfiniteMode( FALSE );	// true looks like an unknown ending time
		pProgressBar->SetRange( 0, _lProgressTotal );
		m_wndStatusBar.AddDynamicElement( pProgressBar );
		_lProgress = 0;
	}
	else
	{
		m_wndStatusBar.RemoveElement( ID_STATUS_PROGRESS );
		_lProgress = 0;
	}

	m_wndStatusBar.RecalcLayout();
	m_wndStatusBar.RedrawWindow();

	CBCGPPopupMenu::UpdateAllShadows();

	return 0;
}

void CALLBACK CMainFrame::EnableProgress( long nTotal )
{
	::PostMessage( _hWndMain, UM_ENABLESTATUSPROGRESS, (WPARAM)nTotal, 0 );
}

LRESULT CMainFrame::OnEnableProgress( WPARAM nTotal, LPARAM )
{
	_lProgress = 0;
	_lProgressTotal = nTotal;
	ShowProgress( TRUE );
	return 0;
}

void CMainFrame::GetProgress( long& nCur, long& nTotal )
{
	nCur = _lProgress;
	nTotal = _lProgressTotal;
}

void CALLBACK CMainFrame::DisableProgress()
{
	::PostMessage( _hWndMain, UM_DISABLESTATUSPROGRESS, 0, 0 );
}

LRESULT CMainFrame::OnDisableProgress( WPARAM, LPARAM )
{
	ShowProgress( FALSE );
	m_wndStatusBar.RedrawWindow();
	return 0;
}

void CALLBACK CMainFrame::SetProgress( long nCur, CString szTxt )
{
	::PostMessage( _hWndMain, UM_SETSTATUSPROGRESS, (WPARAM)nCur, 0 );
}

LRESULT CMainFrame::OnSetProgress( WPARAM nCur, LPARAM )
{
	if( (long)nCur > _lProgressTotal ) return 0;

	_lProgress = nCur;
	CBCGPRibbonProgressBar* pProgress = DYNAMIC_DOWNCAST( CBCGPRibbonProgressBar,
		m_wndStatusBar.FindElement( ID_STATUS_PROGRESS ) );
	if( pProgress ) pProgress->SetPos( _lProgress, TRUE );
	return 0;
}

void CMainFrame::OnObjRelationships() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	RelationshipDlg d( this );
	d.DoModal();
}

void CMainFrame::OnHelpHelp() 
{
	CString szPath;
	::GetAppPath( szPath );
	szPath += _T("\\Help\\nshelp.chm");

	_hwndHelp = ::HtmlHelp( m_hWnd, szPath, HH_DISPLAY_TOPIC, NULL );
}

void CMainFrame::OnHelpQuick()
{
	InstructionDlg d( this );
	d.m_fJustDisplay = TRUE;
	CString szPath;
	::GetAppPath( szPath );
	szPath += _T("\\Help\\MovAlyzeRQuickReference.rtf");
	d.m_szFileRTF = szPath;
	CWaitCursor crs;
	d.DoModal();
}

void CMainFrame::OnHelpTutorial()
{
	::GoToHelp( _T("setup.html"), this );
}

LRESULT CMainFrame::OnTCard( WPARAM w, LPARAM l )
{
	CLeftView* pLV = GetLeftView();

	switch( w )
	{
		// create/modify
		case 101: pLV->OnEfAdd(); break;
		case 103: pLV->OnGfAdd(); break;
		case 104: pLV->OnSfAdd(); break;
		case 105: pLV->OnCfAdd(); break;
		case 106: pLV->OnStfAdd(); break;
		case 107: pLV->OnEfAdd(); break;
		// Add item
		case 201: pLV->OnEAddgroup(); break;
		case 202: pLV->OnGAddsubj(); break;
		case 203: pLV->OnEAddcondition(); break;
		case 204: pLV->OnCEdit2(); break;
		case 205: pLV->OnStAddelements(); break;
		// defaults
		default: break;
	}

	return 0;
}

void CMainFrame::HandleMouseMoveForRecording()
{
	CLeftView* pLV = GetLeftView();
	CGraphView* pGV = GetRecordingPane();
	if( !pLV || !pGV ) return;

	// if we're running an experiment or in test mode...
	UINT nState = pLV->GetActionState();
	if( ( nState == ACTION_RUNEXPERIMENT ) || ( nState == ACTION_TEST ) )
	{
		// if we're recording or in test mode and cursor is hidden...
		if( ( pGV->m_fTest || pGV->fRecording ) && pGV->m_fHideCursor && pGV->m_fCursorHidden )
		{
			// show it
			pGV->m_fCursorHidden = FALSE;
			ShowCursor( TRUE );
		}
	}
}

void CMainFrame::OnMouseMove( UINT nFlags, CPoint point ) 
{
	HandleMouseMoveForRecording();
	CBCGPFrameWnd::OnMouseMove( nFlags, point );
}

void CMainFrame::OnClose() 
{
	// ensure no processes are running
	CLeftView* pLV = GetLeftView();
	UINT nState = pLV->GetActionState();
	if( nState != ACTION_NONE )
	{
		BCGPMessageBox( _T("Please stop all processes before exiting MovAlyzeR") );
		m_fClearLastUser = FALSE;
		return;
	}

	// info to backup
	if( ::HasDataChanged() && !::IsClientServerModeOn() && !FBackupdb() )
	{
		m_fClearLastUser = FALSE;
		return;
	}

	// tree memory
	pLV->WriteMemory();

	// write last user (unless specified to clear)
	CString szVal, szKey, szID;
	szKey = _T("LastUser");
	::GetCurrentUser( szID );
	if( m_fClearLastUser ) szID = _T("");
	theApp.WriteString( szKey, szID );

	// system tray icon
	m_wndSysTrayIcon.RemoveIcon();

	// Current user object
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj )
	{
		pObj->Reset();
		delete pObj;
	}

	// Save toolbar/menu parameters:
//	CString strControlBarRegEntry = REGISTRY_ROOT;
//	strControlBarRegEntry += CONTROL_BARS;
//	CBCGPToolBar::SaveParameters( strControlBarRegEntry );

//	CDockState dockState;
//	GetDockState( dockState );
//	dockState.SaveState( strControlBarRegEntry );

	// toolbar memory
//	SaveBarState( _T("Toolbars") );

	// Save standard toolbars:
//	m_wndToolBar.SaveState( strControlBarRegEntry );
//	m_wndToolBarInfo.SaveState( strControlBarRegEntry );
//	m_wndToolBarID.SaveState( strControlBarRegEntry );
//	m_wndToolBarExp.SaveState( strControlBarRegEntry );
//	m_wndMenuBar.SaveState( strControlBarRegEntry );

	// Save user defined toolbars:
//	SaveUserToolbars();

	CBCGPFrameWnd::OnClose();
}

void CMainFrame::OnClearMessages() 
{
	CMsgView* pMV = GetMsgPane();
	if( pMV ) pMV->ClearMessages();
}

void CMainFrame::OnViewLegend() 
{
	if( _LegendDlg )
	{
		delete _LegendDlg;
		_LegendDlg = NULL;
	}
	else _LegendDlg = new LegendDlg( this );
}

void CMainFrame::LegendClosed()
{
	if( _LegendDlg )
	{
		delete _LegendDlg;
		_LegendDlg = NULL;
	}
}

void CMainFrame::ViewCalc( CWnd* pWnd )
{
	if( _convCalc )
	{
		delete _convCalc;
		_convCalc = NULL;
	}
	else _convCalc = new ConvCalc( pWnd );
}

void CMainFrame::CalcClosed()
{
	if( _convCalc )
	{
		delete _convCalc;
		_convCalc = NULL;
	}
}

void CMainFrame::OnViewCalc()
{
	ViewCalc( this );
}

void CMainFrame::OnEClear() 
{
	CRecView* pRV = GetSettingsPane();
	if( pRV ) pRV->OnClear();
}

void CMainFrame::OnEStopexp() 
{
	CRecView* pRV = GetSettingsPane();
	if( pRV ) pRV->OnStopExp();
}

void CMainFrame::OnEStopcond() 
{
	CRecView* pRV = GetSettingsPane();
	if( pRV ) pRV->OnStopCond();
}

void CMainFrame::OnEStoprec() 
{
	CRecView* pRV = GetSettingsPane();
	if( pRV ) pRV->OnStopRec();
}

void CMainFrame::OnAddLog() 
{
	CommentDlg d( this );
	d.DoModal();
}

void CMainFrame::OnSendLog() 
{
	CWaitCursor wait;

	CString szData;
	szData.LoadString( IDS_AL_SEND );
	OutputAMessage( szData, TRUE );

	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");

	// Ensure actions.log file exists & get file name/path
	CString szFile, szPath, szFull;
	CFileFind ff;
	if( !ff.FindFile( szAppPath ) )
	{
		BCGPMessageBox( IDS_NOAL );
		return;
	}
	else
	{
		ff.FindNextFile();
		szFull = ff.GetFilePath();
		int nPos = szFull.ReverseFind( '\\' );
		if( nPos != -1 )
		{
			szPath = szFull.Left( nPos + 1 );
			szFile = szFull.Mid( nPos + 1 );
		}
	}

	// Preference is to send them to our "Report Problem" web page.
	// We will present this option, but if they choose otherwise, they can still send an email.
	CString szCmd;
	CString szMsg = _T("Click YES to go to our problem reporting web page and upload your log file.");
	szMsg += _T("\nClick NO to send an email and manually attach the log file (folder will be opened).");
	szMsg += _T("\n\nInternet connection required. Login required on web page.");
	szMsg += _T("\n\nYour LOG file is located here:\n");
	szMsg += szAppPath;
	int nRes = BCGPMessageBox( szMsg, MB_YESNOCANCEL | MB_ICONQUESTION );
	if( nRes == IDYES )
	{
		szCmd = _T("http://www.neuroscript.net/problem.php");
		CHyperLink::GotoURL( szCmd );
	}
	else if( nRes == IDNO )
	{
		// Open folder containing actions.log file
		int nPos = szAppPath.ReverseFind( '\\' );
		CString szRoot = szAppPath.Left( nPos );
		szCmd.Format( _T("explorer /ROOT,%s,/SELECT,%s"), szRoot, szAppPath );
		WinExec( szCmd, SW_SHOW );
		// Destination
		CString szAddy = _T("support@neuroscriptsoftware.com");
		// Body
		CString szBody = _T("Attached is my Actions.LOG file.");
		// Send using "MAILTO"
		szCmd.Format( _T("mailto:%s?subject=ACTIONS Log Report&body=%s"), szAddy, szBody );
		CHyperLink::GotoURL( szCmd );
	}
}

void CMainFrame::ObjStop()
{
	OnObjStop();
}

void CMainFrame::OnObjStop()
{
	CLeftView* pLV = GetLeftView();
	if( pLV )
	{
		UINT nVal = pLV->GetActionState();
		if( ( nVal == ACTION_RUNEXPERIMENT ) ||
			( nVal == ACTION_STIMULUS ) )
		{
			ResetSizes();
			CString szInstr;
			szInstr.LoadString( IDS_RV_INSTR );
			CRecView* pRV = GetSettingsPane();
			if( pRV )
			{
				pRV->OnClear();
				pRV->UpdateInstruction( szInstr );
			}

			DisableProgress();
		}
		pLV->OnObjStop();
	}

	m_wndSysTrayIcon.ShowBalloon( _T("") );
	::SystemCleanup();
}

void CMainFrame::OnViewRefresh()
{
	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->Refresh();
}

void CMainFrame::OnUpdateViewRefresh( CCmdUI* pCmdUI )
{
	IS_EXP_RUNNING();
	UINT nVal = pLV->GetActionState();
	if( nVal != ACTION_NONE ) fRunExp = FALSE;
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnUpdateInputDevice( CCmdUI* pCmdUI )
{
	IS_EXP_RUNNING();
	UINT nVal = pLV->GetActionState();
	if( nVal != ACTION_NONE ) fRunExp = FALSE;
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnWizExpsetup() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	WizardSetupSheet d( this );
	d.DoModal();
}

void CMainFrame::OnWizDataimport() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	WizardImportSheet d( this );
	d.DoModal();
}

void CMainFrame::OnWizRemoteDataimport() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	WizardImportRemoteSheet d( this );
	if( d.DoModal() == ID_WIZFINISH )
	{
		CLeftView* pLV = GetLeftView();
		if( pLV ) pLV->Refresh();
	}
}

void CMainFrame::OnWizDataExport()
{
	DoExportWizard();
}

void CMainFrame::DoExportWizard( CString szExpID )
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	WizardExportSheet d( this, 0, szExpID );
	d.DoModal();
}

void CMainFrame::OnWizDatagen()
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	WizardImportGenSheet d( this );
	d.DoModal();
}

void CMainFrame::OnFBackupsys() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->BackupSystemThread();
}

BOOL CMainFrame::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("introduction.html"), this );

	return CBCGPFrameWnd::OnHelpInfo(pHelpInfo);
}

void CMainFrame::OnWizRunexp() 
{
	if( ::IsClientServerModeOn() ) return;

	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}
	if( m_bIsMinimized ) ::ShowWindow( m_hWnd, SW_RESTORE );

	WizardRunExpSheet d( this );
	if( d.DoModal() == ID_WIZFINISH )
		RunExperimentAuto( d.m_szExpID, d.m_szExp, d.m_szGrpID, d.m_szGrp,
						   d.m_szSubjID, d.m_szSubj );
}

void CMainFrame::OnViewPrivate() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}
	if( !::HasPassBeenSet() && ::IsPrivacyOn() )
	{
		PasswordDlg d( this );
		::GetUserPassword( d.m_szConfirm );
		if( d.DoModal() != IDOK ) return;
		::SetHasPassBeenSet( TRUE );
	}

	::SetIsPrivacyOn( !::IsPrivacyOn() );
	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->Refresh();
}

void CMainFrame::OnUpdateViewLegend(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( _LegendDlg != NULL );
}

void CMainFrame::OnUpdateViewsStop(CCmdUI* pCmdUI) 
{
	CGraphView* pGV = GetRecordingPane();
	if( pGV && pGV->fRecording && !pGV->m_fTest ) pCmdUI->Enable( TRUE );
	else pCmdUI->Enable( FALSE );
}

void CMainFrame::OnUpdateOQuick(CCmdUI* pCmdUI) 
{
	BOOL fCheck = ::GetDetailedOutput();
	pCmdUI->SetCheck( fCheck );
}

void CMainFrame::OnUpdateObjStop(CCmdUI* pCmdUI)
{
	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->OnUpdateObjStop( pCmdUI );
}

void CMainFrame::OnUpdateViewPrivate(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::IsPrivacyOn() );
}

void CMainFrame::OnUpdateViewsRunExp(CCmdUI*pCmdUI)
{
	IS_EXP_RUNNING();
	fRunExp &= !::IsClientServerModeOn();
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnViewFullScreen()
{
	// were we in full screen mode?
	BOOL fWasFullScreen = IsFullScreen();
	// show/hide full screen mode
	ShowFullScreen();
	// if WAS full screen, we must reset toolbars and menu
	if( fWasFullScreen )
	{
		DockControlBar( &m_wndMenuBar );
		DockControlBar( &m_wndToolBar );
		DockControlBarLeftOf( &m_wndToolBarInfo, &m_wndToolBar );
		DockControlBarLeftOf( &m_wndToolBarID, &m_wndToolBarInfo );
		DockControlBarLeftOf( &m_wndToolBarExp, &m_wndToolBarID );
		ShowControlBar( &m_wndToolBar, TRUE, FALSE, FALSE );
		ShowControlBar( &m_wndToolBarInfo, TRUE, FALSE, FALSE );
		ShowControlBar( &m_wndToolBarID, TRUE, FALSE, FALSE );
		ShowControlBar( &m_wndToolBarExp, TRUE, FALSE, FALSE );
		m_wndStatusBar.ShowWindow( SW_SHOW );
	}
	// otherwise, we need to move the full screen toolbar
	else
	{
		CRect rectFrame, rcScreen;
		GetWindowRect( &rectFrame );
		MONITORINFO mi;
		mi.cbSize = sizeof( MONITORINFO );
		if( GetMonitorInfo( MonitorFromPoint( rectFrame.TopLeft(), MONITOR_DEFAULTTONEAREST ), &mi ) )
			rcScreen = mi.rcMonitor;
		else ::SystemParametersInfo( SPI_GETWORKAREA, 0, &rcScreen, 0 );

		CBCGPToolBar* pTB = GetFullScreenToolBar();
		if( !pTB ) return;
		pTB->EnableDocking( CBRS_ALIGN_ANY );
		DockControlBar( pTB );
		pTB->FloatControlBar( CRect( rcScreen.right - 120, 8, 200, 200 ) );
	}
}

void CMainFrame::OnUpdateViewFullScreen( CCmdUI* pCmdUI )
{
	pCmdUI->SetCheck( IsFullScreen() );
}

void CMainFrame::OnViewMenu() 
{
	ShowControlBar( &m_wndMenuBar, !m_wndMenuBar.IsVisible(), FALSE, TRUE );
}

void CMainFrame::OnUpdateViewMenu(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_wndMenuBar.IsVisible() );
}

void CMainFrame::OnViewInfo() 
{
	ShowControlBar( &m_wndToolBarInfo, !m_wndToolBarInfo.IsVisible(), FALSE, TRUE );
}

void CMainFrame::OnUpdateViewInfo(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_wndToolBarInfo.IsVisible() );
}

void CMainFrame::OnViewAction() 
{
	ShowControlBar( &m_wndToolBar, !m_wndToolBar.IsVisible(), FALSE, TRUE );
}

void CMainFrame::OnUpdateViewAction(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_wndToolBar.IsVisible() );
}

void CMainFrame::OnViewId() 
{
	ShowControlBar( &m_wndToolBarID, !m_wndToolBarID.IsVisible(), FALSE, TRUE );
}

void CMainFrame::OnUpdateViewId(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_wndToolBarID.IsVisible() );
}

void CMainFrame::OnViewExp() 
{
	ShowControlBar( &m_wndToolBarExp, !m_wndToolBarExp.IsVisible(), FALSE, TRUE );
}

void CMainFrame::OnUpdateViewExp(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_wndToolBarExp.IsVisible() );
}

void CMainFrame::OnViewCustomize() 
{
	//------------------------------------
	// Create a customize toolbars dialog:
	//------------------------------------
	CBCGPToolbarCustomize* pDlgCust = new CBCGPToolbarCustomize (this,
		TRUE /* Automatic menus scaning */,
		BCGCUSTOMIZE_MENU_SHADOWS | BCGCUSTOMIZE_TEXT_LABELS | 
		BCGCUSTOMIZE_LOOK_2000 | BCGCUSTOMIZE_MENU_ANIMATIONS);

	pDlgCust->EnableUserDefinedToolbars ();

	pDlgCust->Create ();
}

void CMainFrame::OnUpdateViewCustomize(CCmdUI* pCmdUI) 
{
	IS_EXP_RUNNING();
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnDevice()
{
	CBCGPToolbarComboBoxButton* pCB = NULL;

	CObList listButtons;
	if( CBCGPToolBar::GetCommandButtons( IDM_DEVICE, listButtons ) > 0 )
	{
		for( POSITION posCombo = listButtons.GetHeadPosition (); 
			pCB == NULL && posCombo != NULL; )
		{
			CBCGPToolbarComboBoxButton* pCombo = 
				DYNAMIC_DOWNCAST( CBCGPToolbarComboBoxButton, listButtons.GetNext( posCombo ) );
 
			if( pCombo != NULL && 
				CBCGPToolBar::IsLastCommandFromButton( pCombo ) )
			{
				pCB = pCombo;
			}
		}
	}

	if( pCB != NULL )
	{
		ASSERT_VALID( pCB );

		NSSecurity* pSecurity = theApp.GetSecurity();
		ASSERT( pSecurity != NULL );

		LPCSTR lpszSelItem = pCB->GetItem ();
		CString strSelItem = (lpszSelItem == NULL) ? _T("") : lpszSelItem;

		CBCGPRibbonStatusBarPane* pStatBarDev = 
			(CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_WIZ_DEVSETUP );
		CString szPrefix, szKey, szVal, szUser, szRB, szDef;
		Preferences pref;
		GetPreferences( &pref, NULL );
		if( strSelItem == _T("Mouse") )
		{
			if( !::IsSA() && !::IsOA() )
			{
				if( !pSecurity->AO() ) OnToolbarReset( IDR_TB_SETTINGS, 1 );
				else
				{
					szPrefix = _T("Mouse_");
					pref.ip = Preferences::itMouse;
					pref.tm = Preferences::tmDesktop;
					::SetDesktopModeOn( TRUE );
					pStatBarDev->SetText( "MOUSE" );
				}
			}
			else
			{
				szPrefix = _T("Mouse_");
				pref.ip = Preferences::itMouse;
				pref.tm = Preferences::tmDesktop;
				::SetDesktopModeOn( TRUE );
				pStatBarDev->SetText( "MOUSE" );
			}
		}
		else if( strSelItem == _T("Tablet") )
		{
			if( !::IsSA() && !::IsOA() )
			{
				if( !pSecurity->AO() ) OnToolbarReset( IDR_TB_SETTINGS, 1 );
				else
				{
					szPrefix = _T("Tablet_");
					pref.ip = Preferences::itTablet;
					pStatBarDev->SetText( "TABLET" );
				}
			}
			else
			{
				szPrefix = _T("Tablet_");
				pref.ip = Preferences::itTablet;
				pStatBarDev->SetText( "TABLET" );
			}
		}
		else if( strSelItem == _T("DAQPad") )
		{
			if( !::IsGA() )
			{
				if( !pSecurity->AG() ) OnToolbarReset( IDR_TB_SETTINGS, 1 );
				{
					szPrefix = _T("Gripper_");
					pref.ip = Preferences::itGripper;
					pref.tm = Preferences::tmDesktop;
					::SetDesktopModeOn( TRUE );
					pStatBarDev->SetText( "DAQPAD" );
				}
			}
			else
			{
				szPrefix = _T("Gripper_");
				pref.ip = Preferences::itGripper;
				pref.tm = Preferences::tmDesktop;
				::SetDesktopModeOn( TRUE );
				pStatBarDev->SetText( "DAQPAD" );
			}
		}
		pStatBarDev->Redraw();

		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = theApp.GetRegistryBase();
		theApp.SetRegistryBase( szVal );
		// sampling rate
		szKey = szPrefix;
		szKey += _T("SamplingRate");
		szDef.Format( _T("%d"), ::GetSamplingRate() );
		szVal = theApp.GetString( szKey, szDef );
		::SetSamplingRate( atoi( szVal ) );
		// min pen pressure
		szKey = szPrefix;
		szKey += _T("MPP");
		szVal.Format( _T("%d"), ::GetMinPenPressure() );
		szVal = theApp.GetString( szKey, szDef );
		::SetMinPenPressure( atoi( szVal ) );
		// device resolution
		szKey = szPrefix;
		szKey += _T("Resolution");
		szVal.Format( _T("%g"), ::GetDeviceResolution() );
		szVal = theApp.GetString( szKey, szDef );
		::SetDeviceResolution( atof( szVal ) );
		theApp.SetRegistryBase( szRB );

		SetPreferences( &pref, NULL );
		WritePreferences( &pref, NULL );
	}
}

void CMainFrame::OnUpdateDevice(CCmdUI* pCmdUI)
{
	IS_EXP_RUNNING();
	pCmdUI->Enable( fRunExp );
}

afx_msg LRESULT CMainFrame::OnToolbarReset(WPARAM wp, LPARAM lp)
{
	UINT uiToolBarId = (UINT) wp;
	if( uiToolBarId == IDR_TB_SETTINGS )
	{
		if( lp == 0 )
		{
			CBCGPToolbarComboBoxButton cbb( IDM_DEVICE,
				CImageHash::GetImageOfCommand( IDM_DEVICE, FALSE ), CBS_DROPDOWNLIST, 75 );

			cbb.AddItem( _T("Mouse") );
			if( ::IsTabletInstalled() ) cbb.AddItem( _T("Tablet") );
			if( ::IsGripperInstalled() ) cbb.AddItem( _T("DAQPad") );

			m_wndToolBarID.ReplaceButton( IDM_DEVICE, cbb );
		}

		static BOOL fTold = FALSE;

		CBCGPToolbarComboBoxButton* pCombo =
			(CBCGPToolbarComboBoxButton*)m_wndToolBarID.GetButton( 1 );

		if( pCombo )
		{
			if( ::IsGripperModeOn() ||
				( ::IsGA() && !::IsSA() && !::IsOA() ) )
			{
				pCombo->SelectItem( 2 );
				::SetGripperModeOn( TRUE );
			}
			else

			if( ::IsTabletModeOn() )
			{
				pCombo->SelectItem( 1 );
				if( !fTold && ::IsDesktopModeOn() )
				{
//					BCGPMessageBox( _T("The tablet is selected and the mapping is set to the entire desktop.\n\nIf real-size is desired, you MUST set the mapping to recording window in the Input Device Settings\nand make recording window real-size in experiment settings.\n\nEntire desktop mapping is primarily geared towards pen-computers.\nIf you keep the mapping to the entire desktop, you might see multiple cursors while recording.\nTo fix this, go to the Input Device Settings and switch to recording window mapping.") );
					fTold = TRUE;
				}
			}
			else pCombo->SelectItem( 0 );
		}
	}

	return 0;
}

LRESULT CMainFrame::OnHelpCustomizeToolbars(WPARAM wp, LPARAM lp)
{
	int iPageNum = (int) wp;

	CBCGPToolbarCustomize* pDlg = (CBCGPToolbarCustomize*) lp;
	ASSERT_VALID (pDlg);

	return 0;
}

BOOL CMainFrame::OnShowPopupMenu( CBCGPPopupMenu* pMenuPopup )
{
    CBCGPFrameWnd::OnShowPopupMenu( pMenuPopup );

	if( pMenuPopup == NULL ) return TRUE;

	CBCGPPopupMenuBar* pMenuBar = pMenuPopup->GetMenuBar();
	ASSERT_VALID( pMenuBar );

	for( int i = 0; i < pMenuBar->GetCount(); i ++ )
	{
		CBCGPToolbarButton* pButton = pMenuBar->GetButton( i );
		ASSERT_VALID( pButton );
	}

	CBCGPToolbarMenuButton* pParentButton = pMenuPopup->GetParentButton();
	if( pParentButton == NULL ) return TRUE;

	return TRUE;
}

LRESULT CMainFrame::OnToolbarContextMenu( WPARAM wp, LPARAM lp )
{
	CBCGPToolBar* pToolBar = DYNAMIC_DOWNCAST( CBCGPToolBar, 
											  CWnd::FromHandlePermanent( (HWND)wp) );
	m_fMainToolbarMenu = (
		pToolBar != NULL && pToolBar->GetDlgCtrlID() == AFX_IDW_TOOLBAR );

	CPoint point( BCG_GET_X_LPARAM( lp ), BCG_GET_Y_LPARAM( lp ) );

	CMenu menu;
	VERIFY( menu.LoadMenu( IDR_POPUP_TOOLBAR ) );

	CMenu* pPopup = menu.GetSubMenu( 0 );
	ASSERT( pPopup != NULL );

	CBCGPPopupMenu* pPopupMenu = new CBCGPPopupMenu;
	pPopupMenu->Create( this, point.x, point.y, pPopup->Detach () );

	return 0;
}

void CMainFrame::OnViewTextlabels() 
{
	m_wndToolBar.EnableTextLabels( !m_wndToolBar.AreTextLabels() );
	m_wndToolBarInfo.EnableTextLabels( !m_wndToolBarInfo.AreTextLabels() );
	m_wndToolBarExp.EnableTextLabels( !m_wndToolBarExp.AreTextLabels() );
	m_wndToolBarID.EnableTextLabels( !m_wndToolBarID.AreTextLabels() );
}

void CMainFrame::OnUpdateViewTextlabels(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_wndToolBar.AreTextLabels() );
	pCmdUI->Enable( TRUE );
}

void CMainFrame::OnHelpTroubleshooting()
{
	GoToHelp( _T("troubleshooting.html"), this );
}

void CMainFrame::OnHelpTroubleshootingRec()
{
	TroubleShootingDlg d( this );
	d.DoModal();
}

void CMainFrame::OnHelpActivate()
{
	ActivateHTMLDlg d( this, FALSE );
	d.m_nMode = ACT_MODE_PURCHASE;
	d.DoModal();
}

void CMainFrame::OnUpdateHelpActivate(CCmdUI*pCmdUI)
{
	IS_EXP_RUNNING();
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnHelpDeactivate()
{
	NSSecurity* pSecurity = theApp.GetSecurity();
	if( pSecurity && pSecurity->DeactivateDemo() ) SendMessage( WM_CLOSE );
}

void CMainFrame::OnUpdateHelpDeactivate(CCmdUI*pCmdUI)
{
	pCmdUI->Enable( ::IsSA( FALSE ) ||
					::IsOA( FALSE ) ||
					::IsGA( FALSE ) );
}

void CMainFrame::OnHelpLicense()
{
	CString szPath, szFile;
	::GetAppPath( szPath );
	szFile.Format( _T("%slicense.txt"), szPath );

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("License agreement is missing.") );
		return;
	}

//	CString szCmd;
//	szCmd.LoadString( IDS_EDITOR );
//	szCmd += szFile;
//	WinExec( szCmd, SW_SHOW );
	InstructionDlg d( this );
	d.m_fJustDisplay = TRUE;
	d.m_szFileName = szFile;
	CWaitCursor crs;
	d.DoModal();
}

void CMainFrame::OnHelpPpu()
{
	NSSecurity* pSecurity = theApp.GetSecurity();
	if( pSecurity )
	{
		pSecurity->CheckMultiplePPU();
		GetLeftView()->DoTitle();
	}
}

void CMainFrame::OnUpdateHelpPpu(CCmdUI *pCmdUI)
{
	pCmdUI->Enable( TRUE );
}

void CMainFrame::OnDropFiles(HDROP hDropInfo)
{
	CLeftView* pLV = GetLeftView();
	if( !pLV ) return;

	if( hDropInfo )
	{
		// get file count
		UINT nCount = DragQueryFile( hDropInfo, 0xFFFFFFFF, NULL, 0 );
		if( nCount > 0 )
		{
			TCHAR pszFile[ _MAX_PATH ];
			CString szFile;
			CPoint pt;
			UINT nCur = 0;
			BOOL fProcess = ( nCount == 1 );

			while( nCur < nCount )
			{
				// get file
				DragQueryFile( hDropInfo, nCur, pszFile, _MAX_PATH );

				// get extension
				szFile = pszFile;
				szFile.MakeUpper();
				if( szFile.GetLength() > 3 )
				{
					// if it's on a tree item, handle that...otherwise default is to open in graph or image
					CTreeCtrl& tree = pLV->GetTreeCtrl();
					DragQueryPoint( hDropInfo, &pt );
					ClientToScreen( &pt );
					tree.ScreenToClient( &pt );
					HTREEITEM hItem = tree.HitTest( pt );
					DWORD dwType = hItem ? tree.GetItemData( hItem ) : TI_UNKNOWN;
					BOOL fOpen = ( dwType != TI_SUBJECT );

					// raw data file
					if( ( szFile.Right( 3 ) == _T("HWR") ) || ( szFile.Right( 3 ) == _T("FRD") ) ||
						( szFile.Right( 3 ) == _T("CSV") ) )
					{
						if( fOpen ) ChartHwr( szFile, ::GetSamplingRate(), TRUE );
						else pLV->OnDroppedExternal( hItem, szFile, fProcess, fProcess );
					}
					// RTF file (precedes TF file because of same last 2 letters)
					else if( szFile.Right( 3 ) == _T("RTF") )
					{
						InstructionDlg d( this );
						d.m_fReadOnly = TRUE;
						d.m_fJustDisplay = TRUE;
						d.m_szFileName = szFile;
						d.m_szFileRTF = szFile;
						CWaitCursor crs;
						d.DoModal();
					}
					// processed file
					else if( ( szFile.Right( 2 ) == _T("TF") ) || ( szFile.Right( 3 ) == _T("SEG") ) )
					{
						CString szTF, szSeg;
						if( szFile.Right( 2 ) == _T("TF") )
						{
							szTF = szFile;
							szSeg = szFile.Left( szFile.GetLength() - 2 );
							szSeg += _T("SEG");
						}
						else
						{
							szSeg = szFile;
							szTF = szFile.Left( szFile.GetLength() - 3 );
							szTF += _T("TF");
						}
						ChartTf( szTF, ::GetSamplingRate(), szSeg, TRUE );
					}
					// other process files (ext,con,err,lst)
					// ext/con/err/lst/inc/tmp/xm1/xme/ers
					else if( ( szFile.Right( 3 ) == _T("INC") ) || ( szFile.Right( 3 ) == _T("TMP") ) ||
							 ( szFile.Right( 3 ) == _T("XM1") ) || ( szFile.Right( 3 ) == _T("XME") ) ||
							 ( szFile.Right( 3 ) == _T("EXT") ) || ( szFile.Right( 3 ) == _T("CON") ) ||
							 ( szFile.Right( 3 ) == _T("ERR") ) || ( szFile.Right( 3 ) == _T("LST") ) ||
							 ( szFile.Right( 3 ) == _T("ERS") ) )
					{
						szFile.Format( _T("notepad.exe %s"), pszFile );
						WinExec( szFile, SW_SHOW );
					}
					else if( ( szFile.Right( 3 ) == _T("PCX") ) || ( szFile.Right( 3 ) == _T("BMP") ) ||
							 ( szFile.Right( 3 ) == _T("PNG") ) || ( szFile.Right( 3 ) == _T("GIF") ) ||
							 ( szFile.Right( 3 ) == _T("JPG") ) )
					{
						// open image in image dlg if tree didn't handle
						if( fOpen )
						{
							ImageDlg d( szFile, this );
							d.DoModal();
						}
						else pLV->OnDroppedExternal( hItem, szFile, fProcess, fProcess );
					}
					else BCGPMessageBox( _T("A file was dragged into MovAlyzeR for processing.\nOnly image (JPG, GIF, PNG, BMP, PCX) and recording (HWR, FRD) files can be processed."), MB_OK | MB_ICONWARNING );
				}

				// next
				nCur++;
			}
		}

		// memory cleanup
		DragFinish( hDropInfo );

		// clean up pcx-related static vars
		CLeftView::m_szDropCondID = _T("");
	}
}

void CMainFrame::OnCheckForUpdates()
{
	CString szMsg = _T("You must be connected to the internet to check for updates.");
	szMsg += _T(" If you have a slow connection, it could appear that the software is not responding.");
	szMsg += _T(" If the program is unable to connect to the online server, it will eventually time out.");
	szMsg += _T(" If there are problems connecting, you will be informed within about 30 seconds.");
	szMsg += _T("\n\nDo you wish to continue?");
	if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	IAdminElevator* pAE = NULL;
	HRESULT hr = ::CoCreateInstance( __uuidof( AdminElevator ), NULL, CLSCTX_ALL,
									 IID_IAdminElevator, (LPVOID*)&pAE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create AdminElevate object") );
		return;
	}

	// app & common paths
	CString szAppPath, szCommonPath;
	::GetAppPath( szAppPath );
	::GetCommonPath( szCommonPath );

	// do update check
	double dVer = 0.;
	int nCurVer = ::GetAppVersion();
	CWaitCursor crs;
	COleMessageFilter* pMsgFilter = AfxOleGetMessageFilter();
	if( pMsgFilter )
	{
		pMsgFilter->EnableBusyDialog( FALSE );
		pMsgFilter->EnableNotRespondingDialog( FALSE );
	}
	if( SUCCEEDED( pAE->CheckForUpdates( szAppPath.AllocSysString(), szCommonPath.AllocSysString(), nCurVer, &dVer ) ) )
	{
		if( dVer != 0. ) ::UpdateVersion( dVer );
	}

	pAE->Release();
}

void CMainFrame::OnUpdateCheckForUpdates(CCmdUI *pCmdUI)
{
	IS_EXP_RUNNING();
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::StimulusEditor( CString szStim )
{
	if( !::IsD() && !::IsOA() )
	{
		OnHelpActivate();
		return;
	}
	CString szPath;
	::GetAppPath( szPath );
	szPath += _T("StimEdit.exe");

	CString szParms;
	if( szStim != _T("") )
	{
		// FORMAT is -d datapath -u user -s stimulus (- or /)
		CString szUser, szDataPath;
		::GetDataPath( szDataPath );
		if( szDataPath[ szDataPath.GetLength() - 1 ] == '\\' )
			szDataPath = szDataPath.Left( szDataPath.GetLength() - 1 );
		::GetCurrentUser( szUser );
		szParms.Format( _T(" -d \"%s\" -u %s -s %s"), szDataPath, szUser, szStim );
	}

	::ShellExecute( NULL, _T("open"), szPath, szParms, NULL, SW_SHOW );
}

void CMainFrame::OnOpenRx()
{
	CString szPath;
	::GetAppPath( szPath );
	szPath += _T("Rx.exe");
	::ShellExecute( NULL, _T("open"), szPath, _T(""), NULL, SW_SHOW );
}

void CMainFrame::OnOpenStimEditor()
{
	StimulusEditor();
}

void CMainFrame::OnUpdateOpenStimEditorRx( CCmdUI *pCmdUI )
{
	IS_EXP_RUNNING();
	fRunExp &= !::IsClientServerModeOn();
	fRunExp &= ::IsOA( TRUE );
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnChangeLook( BOOL bOneNoteTabs, BOOL bMDITabColors, BOOL bIsVSDotNetLook,
							   BOOL bDockTabColors, BOOL bMDITabsVS2005Look, BOOL bIsToolBoxEx )
{
	CBCGPMDITabParams mdiTabParams;
	mdiTabParams.m_bTabCustomTooltips = TRUE;

	if( bMDITabsVS2005Look )
	{
		mdiTabParams.m_style = CBCGPTabWnd::STYLE_3D_VS2005;
		mdiTabParams.m_bDocumentMenu = TRUE;
	}
	else if( bOneNoteTabs )
	{
		mdiTabParams.m_style = CBCGPTabWnd::STYLE_3D_ONENOTE;
		mdiTabParams.m_bAutoColor = bMDITabColors;
	}

	CBCGPTabbedControlBar::EnableTabAutoColor( bDockTabColors );
	CBCGPMenuBar::HighlightDisabledItems( TRUE );

	RecalcLayout();
	RedrawWindow( NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE );
}

void CMainFrame::OnUpdateWizDevSetup(CCmdUI* pCmdUI)
{
	IS_EXP_RUNNING();
	fRunExp &= !::IsClientServerModeOn();
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::OnWizDevSetup()
{
	OnWizDevSetup2( this );
}

BOOL CMainFrame::OnWizDevSetup2( CWnd* pParent )
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return FALSE;
	}

	WizardDevSheet d( pParent );
	Preferences pref;
	GetPreferences( &pref, NULL );
	d.m_dHeightDisplay = pref.dD_Y;
	d.m_dWidthDisplay = pref.dD_X;
	d.m_dHeightTablet = pref.dT_Y;
	d.m_dWidthTablet = pref.dT_X;
	d.m_fRecWin = ( pref.tm == Preferences::tmTablet );
	if( d.DoModal() == ID_WIZFINISH )
	{
		CBCGPRibbonStatusBarPane* pStatBarDev = 
			(CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_WIZ_DEVSETUP );

		// update preferences
		switch( d.m_nDevice )
		{
			case IDC_RDO_TABLET:	pref.ip = Preferences::itTablet; pStatBarDev->SetText( "TABLET"); break;
			case IDC_RDO_GRIPPER:	pref.ip = Preferences::itGripper; pStatBarDev->SetText( "DAQPAD"); break;
			default:				pref.ip = Preferences::itMouse; pStatBarDev->SetText( "MOUSE"); break;
		}
		pStatBarDev->Redraw();
		if( d.m_fRecWin ) pref.tm = Preferences::tmTablet;
		else pref.tm = Preferences::tmDesktop;
		pref.dT_X = d.m_dWidthTablet;
		pref.dT_Y = d.m_dHeightTablet;
		pref.dD_X = d.m_dWidthDisplay;
		pref.dD_Y = d.m_dHeightDisplay;

		// rate, pressure, resolution to registry (depending upon selected device)
		CString szPrefix, szKey, szVal, szUser, szRB;
		if( d.m_nDevice == IDC_RDO_TABLET ) szPrefix = _T("Tablet_");
		else if( d.m_nDevice == IDC_RDO_MOUSE ) szPrefix = _T("Mouse_");
		else if( d.m_nDevice == IDC_RDO_GRIPPER ) szPrefix = _T("Gripper_");
		::GetCurrentUser( szUser );
		szVal.Format( _T("Settings\\%s"), szUser );
		szRB = theApp.GetRegistryBase();
		theApp.SetRegistryBase( szVal );
		::SetSamplingRate( d.m_nRate );
		szVal.Format( _T("%d"), d.m_nRate );
		szKey = szPrefix;
		szKey += _T("SamplingRate");
		theApp.WriteString( szKey, szVal );
		::SetMinPenPressure( d.m_nPressure );
		szVal.Format( _T("%d"), d.m_nPressure );
		szKey = szPrefix;
		szKey += _T("MPP");
		theApp.WriteString( szKey, szVal );
		::SetDeviceResolution( d.m_dResolution );
		szVal.Format( _T("%g"), d.m_dResolution );
		szKey = szPrefix;
		szKey += _T("Resolution");
		theApp.WriteString( szKey, szVal );
		::SetDeviceSetupRun( TRUE );
		szKey = _T("DeviceSetupRun");
		theApp.WriteInt( szKey, 1 );
		theApp.SetRegistryBase( szRB );

		// set/write preferences
		SetPreferences( &pref, NULL );
		WritePreferences( &pref, NULL );

		// update toolbar
		OnToolbarReset( IDR_TB_SETTINGS, 1 );

		// start view (if open) needs to be refreshed
		CHtmlDlg::ShowTheWindow( TRUE );

		return TRUE;
	}

	return FALSE;
}

void CMainFrame::OnWebsite()
{
	CBCGPRibbonHyperlink* pLink = (CBCGPRibbonHyperlink*) m_wndStatusBar.FindByID( IDM_NS_WEBSITE );
	if (pLink != NULL) pLink->OpenLink();
}

void CMainFrame::OnWebsiteSupport()
{
	CBCGPRibbonHyperlink* pLink = (CBCGPRibbonHyperlink*) m_wndStatusBar.FindByID( IDM_NS_SUP_WEBSITE );
	if (pLink != NULL) pLink->OpenLink();
}

void CMainFrame::OnTrayMinimize()
{
	CSystemTray::MinimiseToTray( this );
	m_wndSysTrayIcon.SetMenuDefaultItem( ID_TRAYPOPUP_MAXIMIZE, FALSE );
}

void CMainFrame::OnTrayMaximize()
{
	CSystemTray::MaximiseFromTray( this );
	m_wndSysTrayIcon.SetMenuDefaultItem( ID_TRAYPOPUP_MINIMIZE, FALSE );
}

void CMainFrame::OnUpdateTrayMinimize( CCmdUI* pCmdUI )
{
	if( !IsMinimized() ) pCmdUI->Enable( TRUE );
	else if( !IsIconic() && IsWindowVisible() ) pCmdUI->Enable( TRUE );
	else pCmdUI->Enable( FALSE );
}

void CMainFrame::OnUpdateTrayMaximize( CCmdUI* pCmdUI )
{
	if( IsMinimized() ) pCmdUI->Enable( TRUE );
	else if( !IsIconic() && !IsWindowVisible() ) pCmdUI->Enable( TRUE );
	else pCmdUI->Enable( FALSE );
}

//************************************
// Method:    OnDrawMenuImage
// FullName:  CMainFrame::OnDrawMenuImage
// Access:    virtual public 
// Returns:   BOOL
// Qualifier:
// Parameter: CDC * pDC
// Parameter: const CBCGPToolbarMenuButton * pMenuButton
// Parameter: const CRect & rectImage
//************************************
BOOL CMainFrame::OnDrawMenuImage( CDC* pDC, const CBCGPToolbarMenuButton* pMenuButton, const CRect &rectImage )
{
	ASSERT_VALID( pDC );
	ASSERT_VALID( pMenuButton );

	BOOL fDraw = FALSE, fDrawImage = FALSE, fDrawShape = FALSE, fDrawLine = FALSE;
	CImageList lstImages;
	CBCGPToolbarButton* pButton = NULL;
	CBCGPToolBarImages* pImages = NULL;
	BOOL fDrawButton = FALSE;
	int nImage = 0, nThickness = 0;
	int cx = rectImage.left + ( rectImage.Width() - 16 ) / 2;
	int cy = rectImage.top + ( rectImage.Height() - 16 ) / 2;

	if( pMenuButton->m_nID == UINT_MAX )
	{
		const CObList& lstMenuItems = pMenuButton->GetCommands();
		CBCGPToolbarButton* pBn = (CBCGPToolbarButton*)lstMenuItems.GetHead();
		if( pBn )
		{
			if( ( pBn->m_nID == IDM_T_CHART_HWR ) || ( pBn->m_nID == IDM_CHART_HWR ) )
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 5 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
			}
			else if( pBn->m_nID == IDM_T_VIEW )
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 4 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
			}
			else if( ( pBn->m_nID == IDM_E_REPROCESS ) || ( pBn->m_nID == IDM_G_REPROCESS ) ||
					 ( pBn->m_nID == IDM_S_REPROCESS ) || ( pBn->m_nID == IDM_C_REPROCESS ) ||
					 ( pBn->m_nID == IDM_T_REPROCESS ) )
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 3 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
			}
			else if( pBn->m_nID == IDC_BN_COLORMIN )
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				lstImages.Create( IDB_FEEDBACK, 16, 1, RGB( 255, 255, 255 ) );
			}
		}
	}
	else
	{
		switch( pMenuButton->m_nID )
		{
			case IDM_ST_CHART: case IDM_EL_CHART:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 5 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_ST_VIEW: case IDM_EL_VIEW:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 4 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_HELP_TUTORIAL:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 32 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_STIM_EDITOR: case IDM_ST_STIMEDIT:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 28 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_RUN_RX:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 29 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_E_DELETE: case IDM_G_DELETE: case IDM_S_DELETE: case IDM_C_DELETE: case IDM_T_DELETE:
			case IDM_ST_DELETE: case IDM_EL_DELETE:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 30 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_T_EXPERIMENT:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 0 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_T_CONDITION: case IDM_AGRAPH_SORT_C:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 1 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_AGRAPH_SORT_G: case IDM_E_BUILDNORMDB: case IDM_S_BUILDNORMDB:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 2 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_AGRAPH_SORT_S:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 13 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_AGRAPH_VIEW_ALL: case IDM_AXIS_CUSTOMIZE: case IDM_VIEW_ATTACH:
			case IDM_E_VIEWZHISTORYSUMMARY: case IDM_S_VIEWZHISTORYSUMMARY:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 4 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_S_EXPORT: case IDM_E_EXPORT: case IDC_BN_EXPORT: case IDM_AGRAPH_EXPORT_ALL:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 27 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDC_BN_PRINT: case IDM_AGRAPH_PRINT:
			{
				fDraw = TRUE;
				fDrawImage = TRUE;
				pButton = m_wndToolBarCommon.GetButton( 31 );
				pImages = m_wndToolBarCommon.GetImages();
				fDrawButton = TRUE;
				break;
			}
			case IDM_THICKNESS2: case IDM_THICKNESS3: case IDM_THICKNESS4:
			case IDM_THICKNESS5: case IDM_THICKNESS6: case IDM_THICKNESS7:
			{
				fDraw = TRUE;
				fDrawShape = TRUE;
				fDrawLine = TRUE;
				switch( pMenuButton->m_nID )
				{
					case IDM_THICKNESS2: nThickness = 1; break;
					case IDM_THICKNESS3: nThickness = 2; break;
					case IDM_THICKNESS4: nThickness = 3; break;
					case IDM_THICKNESS5: nThickness = 4; break;
					case IDM_THICKNESS6: nThickness = 5; break;
					case IDM_THICKNESS7: nThickness = 6; break;
				}
				break;
			}
			case IDC_BN_COLORMIN: case IDC_BN_COLORMAX:
			{
				CGraphDlg* pGD = CGraphDlg::GetActiveChart();
				if( pGD && pGD->m_hWnd )
				{
					CRect rect = rectImage;
					rect.DeflateRect( 0, 2 );
					rect.DeflateRect( 1, 0 );

					COLORREF col = ( pMenuButton->m_nID == IDC_BN_COLORMIN ) ? pGD->m_MinColor : pGD->m_MaxColor;
					CBrush br;
					br.CreateSolidBrush( col );
					CBrush* pBrushOld = pDC->SelectObject( &br );
					pDC->Rectangle( rect );
					pDC->SelectObject( pBrushOld );

					return TRUE;
				}
				break;
			}
		}
	}

	if( fDraw )
	{
		if( fDrawImage )
		{
			if( fDrawButton && pButton && pImages )
			{
				pButton->OnDraw( pDC, rectImage, pImages );
			}
			else
			{
				::ImageList_Draw( lstImages.GetSafeHandle(), nImage, pDC->GetSafeHdc(),
								  cx, cy, ILD_TRANSPARENT | ILD_FOCUS );
			}
		}
		else if( fDrawShape )
		{
			if( fDrawLine )
			{
				CRect rect = rectImage;
				rect.DeflateRect( 0, 2 );
				rect.DeflateRect( 1, 0 );
				rect.DeflateRect( 0, rect.Height() - nThickness, 0, 0 );

				COLORREF col = RGB( 0, 0, 0 );
				CBrush br;
				br.CreateSolidBrush( col );
				CBrush* pBrushOld = pDC->SelectObject( &br );
				pDC->Rectangle( rect );
				pDC->SelectObject( pBrushOld );

				return TRUE;
			}
			else
			{
				HBRUSH hbr = CreateSolidBrush( RGB( 0, 255, 0 ) );
				HBRUSH hbrOld = (HBRUSH)pDC->SelectObject( hbr );
				int nLeft = ( rectImage.right - rectImage.left ) / 2 + rectImage.left;
				int nTop = ( rectImage.bottom - rectImage.top ) / 2 + rectImage.top;
				POINT pts[ 4 ];
				pts[ 0 ].x = pts[ 3 ].x = nLeft - 2;
				pts[ 0 ].y = pts[ 3 ].y = nTop + 5;
				pts[ 1 ].x = nLeft + 4;
				pts[ 1 ].y = nTop;
				pts[ 2 ].x = nLeft - 2;
				pts[ 2 ].y = nTop - 5;
				pDC->Polygon( pts, 4 );
				pDC->SelectObject( hbrOld );
				::DeleteObject( hbr );
				return TRUE;
			}
		}
	}

	return FALSE;
}

void CMainFrame::OnViewGettingStarted()
{
	ShowGettingStarted();
}

void CMainFrame::ShowGettingStarted()
{
	GettingStartedDlg* pDialog = GettingStartedDlg::GetGettingStartedDlg();

	if( !pDialog ) {
		GettingStartedDlg* pDlg = new GettingStartedDlg( this, TRUE );
	}
	else {
		pDialog->DestroyWindow();
	}
}

void CMainFrame::OnViewStartPage()
{
	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->ShowStartPage();
}

void CMainFrame::OnUpdateViewStartPage(CCmdUI *pCmdUI)
{
	IS_EXP_RUNNING();
	pCmdUI->Enable( fRunExp );
}

void CMainFrame::ShowPopupWindow( CString szMsg, CString szURL, UINT nURL,
								  BYTE nTransparency, int nTimeToClose )
{
	CBCGPPopupWindow* pPopup = new CBCGPPopupWindow;
	pPopup->SetAnimationType( CBCGPPopupMenu::FADE );
	pPopup->SetAnimationSpeed( 30 );
	pPopup->SetTransparency( nTransparency );
	pPopup->SetSmallCaption( TRUE );
	pPopup->SetAutoCloseTime( nTimeToClose );
	CBCGPPopupWndParams params;
	params.m_hIcon = theApp.LoadIcon( IDR_MAINFRAME );
	params.m_strText = szMsg;
	if( szURL != _T("") )
	{
		params.m_strURL = szURL;
		params.m_nURLCmdID = nURL;
	}
	pPopup->Create( this, params );
}

// for hyperlinks
LRESULT CMainFrame::OnHyperlinkClicked( WPARAM wParam, LPARAM lParam )
{
	if( wParam )
	{
		// we assume active window is control for tooltip
		// parent = dialog which should handle help
		HWND hWnd = ::GetActiveWindow();
		if( hWnd )
		{
			hWnd = ::GetParent( hWnd );
			if( hWnd )
			{
				CWnd* pWnd = CWnd::FromHandle( hWnd );
				if( pWnd )
				{
					pWnd->SendMessage( WM_HELP );
					return 0;
				}
			}
		}
		// if nothing, then just do main help
		OnHelpHelp();
	}

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// handle messages as a message target
///////////////////////////////////////////////////////////////////////////////
//************************************
// Method:    Message
// FullName:  CMainFrame::Message
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: LPARAM msg
// Parameter: WPARAM val
// Parameter: BOOL * fStop
//************************************
void CMainFrame::Message( LPARAM msg, WPARAM val, BOOL* fStop )
{
	WORD wMsg = LOWORD( msg );
	WORD wSubMsg = HIWORD( msg );

	if( wMsg == MSG_ACTION )
	{
		if( wSubMsg == UM_INSTRUCTION_UPDATE )
		{
			CString szInstr = (char*)val;
			CRecView* pRV = GetSettingsPane();
			if( pRV ) pRV->UpdateInstruction( szInstr );
		}
		else if( wSubMsg == MSG_SUB_STIMEDIT ) StimulusEditor( (char*)val );
		else if( wSubMsg == MSG_SUB_DEVICESETUP ) OnWizDevSetup();
	}
}

void CMainFrame::ConvertToPCX( DWORD dwType )
{
	CString szImgExt, szMsg, szFilter;

	BOOL fBMP = ( dwType == CXIMAGE_FORMAT_BMP );
	BOOL fPNG = ( dwType == CXIMAGE_FORMAT_PNG );
	BOOL fGIF = ( dwType == CXIMAGE_FORMAT_GIF );
	BOOL fJPG = ( dwType == CXIMAGE_FORMAT_JPG );

	if( fBMP ) szImgExt = _T("BMP");
	else if( fPNG ) szImgExt = _T("PNG");
	else if( fGIF ) szImgExt = _T("GIF");
	else if( fJPG ) szImgExt = _T("JPG");

	ASSERT( szImgExt != _T("") );

	szMsg.Format( _T("Select the %s file to convert to a PCX file."), szImgExt );
	BCGPMessageBox( szMsg );

	// open file dialog to find BMP
	szFilter.Format( _T("%s Files (*.%s)|*.%s|"), szImgExt, szImgExt, szImgExt );
	CFileDialog d( TRUE, szFilter, _T(""), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this );
	if( d.DoModal() == IDCANCEL ) return;
	CString szImage = d.GetPathName();

	// image - verify OK of type
	CxImage img( szImage, dwType );
	if( !img.IsValid() )
	{
		szMsg.Format( _T("This appears to be an invalid %s file."), szImgExt );
		BCGPMessageBox( szMsg );
		return;
	}

	BCGPMessageBox( _T("Now enter the name and select the location to save the PCX.") );

	// Save as dialog
	CString szPCX = szImage.Left( szImage.GetLength() - 3 );
	szPCX += _T("PCX");
	CFileDialog d2( FALSE, __T("PCX Files (*.PCX)|*.PCX|"), szPCX, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T("PCX Files (*.PCX)|*.PCX|"), this );
	if( d2.DoModal() == IDCANCEL ) return;
	szPCX = d2.GetPathName();

	// convert
	long lXRes = img.GetXDPI();
	bool fGrayScale = img.IsGrayScale();
	if( lXRes < 300 )
	{
		if( BCGPMessageBox( _T("Image resolution is below 300 dpi.\nProcessing can be inaccurate.\n\nContinue?"), MB_YESNO ) == IDNO )
			return;
	}
	if( !fGrayScale )
	{
// 14Apr10: GMB: Removed, as it seems unimportant to inform user...just decrease the color depth
//		if( BCGPMessageBox( _T("Image is not gray scale.\nProcessing can be inaccurate.\n\nContinue with color depth adjustment?"), MB_YESNO ) == IDNO )
//			return;
		img.DecreaseBpp( 8, TRUE );
	}
	if( img.Save( szPCX, CXIMAGE_FORMAT_PCX ) ) BCGPMessageBox( _T("Conversion successfull.") );
	else BCGPMessageBox( _T("Error in converting.") );
}

void CMainFrame::OnConvertBMPtoPCX()
{
	ConvertToPCX( CXIMAGE_FORMAT_BMP );
}

void CMainFrame::OnConvertPNGtoPCX()
{
	ConvertToPCX( CXIMAGE_FORMAT_PNG );
}

void CMainFrame::OnConvertGIFtoPCX()
{
	ConvertToPCX( CXIMAGE_FORMAT_GIF );
}

void CMainFrame::OnConvertJPGtoPCX()
{
	ConvertToPCX( CXIMAGE_FORMAT_JPG );
}

//************************************
// Method:    OnFImport
// FullName:  CLeftView::OnFImport
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CMainFrame::OnFImport()
{
	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->OnFileImport();
}

//************************************
// Method:    OnUpdateFileImport
// FullName:  CLeftView::OnUpdateFileImport
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CMainFrame::OnUpdateFileImport(CCmdUI* pCmdUI)
{
	BOOL fEnable = TRUE;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnAppExitNoMemory
// FullName:  CMainFrame::OnAppExitNoMemory
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CMainFrame::OnAppExitNoMemory()
{
	m_fClearLastUser = TRUE;
	::SendMessage( GetSafeHwnd(), WM_COMMAND, ID_APP_EXIT, 0 );
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Chart specific UI update handlers
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void CMainFrame::OnUpdateShowData( CCmdUI* pCmdUI )
{
	CGraphDlg* pGD = CGraphDlg::GetActiveChart();
	if( pGD && pGD->m_hWnd ) pCmdUI->SetCheck( pGD->m_fShowChartData );
}

void CMainFrame::OnUpdateSegSymbol( CCmdUI* pCmdUI )
{
	CGraphDlg* pGD = CGraphDlg::GetActiveChart();
	if( pGD && pGD->m_hWnd ) pCmdUI->SetCheck( pCmdUI->m_nID == pGD->m_nSymbolSeg );
}

void CMainFrame::OnUpdateSubSymbol( CCmdUI* pCmdUI )
{
	CGraphDlg* pGD = CGraphDlg::GetActiveChart();
	if( pGD && pGD->m_hWnd ) pCmdUI->SetCheck( pCmdUI->m_nID == pGD->m_nSymbolSubMvmt );
}

void CMainFrame::OnUpdateFeedbackTF( CCmdUI* pCmdUI )
{
	CGraphDlg* pGD = CGraphDlg::GetActiveChart();
	if( pGD && pGD->m_hWnd ) pCmdUI->SetCheck( pCmdUI->m_nID == pGD->m_nFeature );
}

void CMainFrame::OnUpdateSupAnn( CCmdUI* pCmdUI )
{
	CGraphDlg* pGD = CGraphDlg::GetActiveChart();
	if( pGD && pGD->m_hWnd ) pCmdUI->SetCheck( pGD->m_fSupressAnn );
}

void CMainFrame::OnUpdateShowXY( CCmdUI* pCmdUI )
{
	CGraphDlg* pGD = CGraphDlg::GetActiveChart();
	if( pGD && pGD->m_hWnd ) pCmdUI->SetCheck( pGD->m_fAnnValues );
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// Custom (overriden) BCG Toolbar, Menubar & status bar
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(NSMenuBar, CBCGPMenuBar)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void NSMenuBar::OnMouseMove( UINT nFlags, CPoint point )
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CBCGPMenuBar::OnMouseMove( nFlags, point );
}

BEGIN_MESSAGE_MAP(NSToolBar, CBCGPToolBar)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void NSToolBar::OnMouseMove( UINT nFlags, CPoint point )
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CBCGPToolBar::OnMouseMove( nFlags, point );
}

BEGIN_MESSAGE_MAP(NSStatusBar, CBCGPRibbonStatusBar)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void NSStatusBar::OnMouseMove( UINT nFlags, CPoint point )
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CBCGPRibbonStatusBar::OnMouseMove( nFlags, point );
}

BEGIN_MESSAGE_MAP(NSSplitterWnd, CBCGPSplitterWnd)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void NSSplitterWnd::OnMouseMove( UINT nFlags, CPoint point )
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CBCGPSplitterWnd::OnMouseMove( nFlags, point );
}

BEGIN_MESSAGE_MAP(NSCaptionBar, CBCGPCaptionBar)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void NSCaptionBar::OnMouseMove( UINT nFlags, CPoint point )
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CBCGPCaptionBar::OnMouseMove( nFlags, point );
}

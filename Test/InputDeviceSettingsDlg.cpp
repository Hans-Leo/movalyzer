// InputDeviceSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#if defined _RX_
	#include "..\Rx\Rx.h"
	#include "..\Rx\RecordingView.h"
#else
	#include "Test.h"
	#include "MainFrm.h"
	#include "GraphView.h"
#endif
#include "InputDeviceSettingsDlg.h"

// InputDeviceSettingsDlg dialog

IMPLEMENT_DYNAMIC(InputDeviceSettingsDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(InputDeviceSettingsDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_RESET, OnBnClickedBnReset)
END_MESSAGE_MAP()

InputDeviceSettingsDlg::InputDeviceSettingsDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(InputDeviceSettingsDlg::IDD, pParent)
	, m_nSamplingRate(0)
	, m_nMinPenPressure(0)
	, m_dDevRes(0)
{
	m_nSamplingRate = ::GetSamplingRate();
	m_nMinPenPressure = ::GetMinPenPressure();
	m_dDevRes = ::GetDeviceResolution();
}

InputDeviceSettingsDlg::~InputDeviceSettingsDlg()
{
}

void InputDeviceSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_RATE, m_nSamplingRate);
	DDX_Text(pDX, IDC_EDIT_PRESSURE, m_nMinPenPressure);
	DDX_Text(pDX, IDC_EDIT_RESOLUTION, m_dDevRes);
}

BOOL InputDeviceSettingsDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );
	return TRUE;
}

// InputDeviceSettingsDlg message handlers

void InputDeviceSettingsDlg::OnBnClickedBnReset()
{
	if( ::IsGripperModeOn() )
	{
		m_nSamplingRate = GRIPPER_RATE;
		m_nMinPenPressure = GRIPPER_PRESS;
		m_dDevRes = GRIPPER_RES;
	}
	else if( ::IsTabletModeOn() )
	{
		m_nSamplingRate = TABLET_RATE;
		m_nMinPenPressure = TABLET_PRESS;
		m_dDevRes = TABLET_RES;

#if !defined _RX_
		CGraphView* pW = ((CMainFrame*)AfxGetApp()->m_pMainWnd)->GetRecordingPane();
#else
		RecordingView* pW = ((CMainFrame*)AfxGetApp()->m_pMainWnd)->GetRecordingWindow();
#endif
		long lRes = 0, lX = 0, lY = 0;
		short nRate = 0;
		BOOL fInches = FALSE;
		CString szDev;
		if( pW->GetDigitizerInfo( szDev, lRes, lX, lY, nRate, fInches ) )
		{
			m_nSamplingRate = nRate;
			m_dDevRes = 1.0 / ( ( lRes != 0 ) ? lRes : 1.0 );
			if( fInches ) m_dDevRes *= 2.54;
		}
	}
	else
	{
		m_nSamplingRate = MOUSE_RATE;
		m_nMinPenPressure = MOUSE_PRESS;
		m_dDevRes = MOUSE_RES;
	}

	UpdateData( FALSE );
}

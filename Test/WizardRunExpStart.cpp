// WizardRunExpStart.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardRunExpStart.h"
#include "WizardRunExpSheet.h"
#include "WizardRunExpExp.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpStart property page

IMPLEMENT_DYNCREATE(WizardRunExpStart, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardRunExpStart, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardRunExpStart::WizardRunExpStart() : CBCGPPropertyPage(WizardRunExpStart::IDD)
{
}

WizardRunExpStart::~WizardRunExpStart()
{
}

void WizardRunExpStart::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpStart message handlers

LRESULT WizardRunExpStart::OnWizardNext() 
{
	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	WizardRunExpExp* pExp = (WizardRunExpExp*)pParent->GetPage( 1 );
	if( pExp->m_szID != _T("") )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardRunExpStart::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	WizardRunExpSheet* pParent = (WizardRunExpSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardRunExpStart::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("runexperiment_wizard.html"), this );
	return TRUE;
}

BOOL WizardRunExpStart::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

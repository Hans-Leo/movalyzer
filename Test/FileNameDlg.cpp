// FileNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "FileNameDlg.h"
#include "shlobj.h" // For SHFunctions

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FileNameDlg dialog

BEGIN_MESSAGE_MAP(FileNameDlg, CDialog)
	//{{AFX_MSG_MAP(FileNameDlg)
	ON_BN_CLICKED(IDC_BN_BROWSE, OnBnBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

FileNameDlg::FileNameDlg(CString szFilter, CString szName,
						 CWnd* pParent, CString szPath)
	: CDialog(FileNameDlg::IDD, pParent), m_szFilter( szFilter ),
	  m_szName( szName ), m_szPath( szPath )
{
	//{{AFX_DATA_INIT(FileNameDlg)
	m_szFile = _T("");
	//}}AFX_DATA_INIT

	if( szName != _T("*") )
	{
		if( szPath == _T("") ) m_szFile.Format( _T("%s.%s"), m_szName, m_szFilter );
		else m_szFile.Format( _T("%s\\%s.%s"), m_szPath, m_szName, m_szFilter );
	}
	m_fDirOnly = FALSE;
}

FileNameDlg::FileNameDlg(CWnd* pParent)
	: CDialog(FileNameDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FileNameDlg)
	m_szFile = _T("");
	//}}AFX_DATA_INIT
	m_fDirOnly = TRUE;
}

void FileNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FileNameDlg)
	DDX_Text(pDX, IDC_EDIT_FILE, m_szFile);
	DDX_Control(pDX, IDC_BN_BROWSE, m_bnBrowse);
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// FileNameDlg message handlers

void FileNameDlg::OnBnBrowse() 
{
	if( !m_fDirOnly )
	{
		CString szFilter;
		szFilter.Format( _T("%s files (*.%s)|*.%s|"), m_szFilter, m_szFilter, m_szFilter );

		CString szFile;
		if( m_szPath == _T("") ) szFile.Format( _T("%s.%s"), m_szName, m_szFilter );
		else szFile.Format( _T("%s\\%s.%s"), m_szPath, m_szName, m_szFilter );

		CFileDialog d( TRUE, m_szFilter, szFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					   szFilter, this );
		if( d.DoModal() == IDCANCEL ) return;

		m_szFile = d.GetPathName();

		UpdateData( FALSE );
	}
	else
	{
		BROWSEINFO bi;
		memset( (LPVOID)&bi, 0, sizeof( bi ) );
		TCHAR szDisplayName[ _MAX_PATH ];
		szDisplayName[ 0 ] = '\0';
		bi.hwndOwner = GetSafeHwnd();
		bi.pidlRoot = NULL;
		bi.pszDisplayName = szDisplayName;
		bi.lpszTitle = _T("Select a folder:");
		bi.ulFlags = BIF_RETURNONLYFSDIRS;

		LPITEMIDLIST pIIL = ::SHBrowseForFolder( &bi );

		TCHAR szInitialDir[ _MAX_PATH ];
		BOOL bRet = ::SHGetPathFromIDList( pIIL, (char*)&szInitialDir );
		if( bRet )
		{
			if( szInitialDir != _T("") ) m_szFile = szInitialDir;

			LPMALLOC pMalloc;
			HRESULT hr = SHGetMalloc( &pMalloc );
			pMalloc->Free( pIIL );
			pMalloc->Release();
		}

		UpdateData( FALSE );
	}
}

void FileNameDlg::OnOK()
{
	UpdateData( TRUE );

	if( m_szFile == _T("") )
	{
		AfxMessageBox( _T("No file has been specified.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	CString szTest = m_szFile;
	szTest.MakeUpper();
	int nPos = szTest.Find( _T("\\CON\\") );
	if( nPos != -1 )
	{
		AfxMessageBox( _T("Path/filename cannot contain \'CON\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}
	nPos = szTest.Find( _T("\\NUL\\") );
	if( nPos != -1 )
	{
		AfxMessageBox( _T("Path/filename cannot contain \'NUL\'.") );
		CWnd* pWnd = GetDlgItem( IDC_EDIT_FILE );
		if( pWnd ) pWnd->SetFocus();
		return;
	}

	CDialog::OnOK();
}

BOOL FileNameDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bnBrowse.SetIcon( IDI_OPEN );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

// LeftView.cpp : implementation of the CLeftView class
//

#include "stdafx.h"
#include <direct.h>
#include "Test.h"		// main winapp
#include "errno.h"		// error codes
#include "shlwapi.h"	// For SHFunctions

// window includes
#include "LeftView.h"
#include "MainFrm.h"
#include "GraphView.h"
#include "RecView.h"

// data object includes
#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\NSSharedGUI\Progress.h"
#include "..\Common\UserObj.h"
#include "..\CxImage\CxImage\ximage.h"
#include "Subjects.h"
#include "Groups.h"
#include "Experiments.h"
#include "Conditions.h"
#include "Stimuli.h"
#include "Elements.h"
#include "Cats.h"
#include "Feedbacks.h"
#include "Users.h"
#include "MQuests.h"

// supporting dialogs
#include "FindDlg.h"
#include "AddGroupDlg.h"
#include "AddSubjectDlg.h"
#include "AddConditionDlg.h"
#include "ProcSetSheet.h"
#include "RelationshipDlg.h"
#include "GrpQuestionnaireDlg.h"
#include "SQuestionnaireFullDlg.h"
#include "ExpCleanupDlg.h"
#include "GraphDlg.h"
#include "AGraphDlg.h"
#include "SortDlg.h"
#include "MoveToGroupDlg.h"
#include "NotesDlg.h"
#include "AddelementDlg.h"
#include "DataTabDlg.h"
#include "DataTabGridDlg.h"
#include "SelectUserDlg.h"
#include "ImageDlg.h"
#include "..\NSShared\SFINX.h"
#include "ZipProgress.h"
#include "InstructionDlg.h"
#include "HtmlDlg.h"
#include "StartView.h"
#include "ExcludeDlg.h"
#include "DupDlg.h"
#include "../NSSharedGUI/ZResultsDlg.h"

// MSOffice stuff
#include "..\NSSharedGUI\MSOffice.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// maximum # of groups allowed for an experiment
// maximum # of subjects allowed for a group
#define	MAX_GROUPS		100

// MACROS
// get's currently selected item and item data
#define	GET_LV_TREE() \
	CTreeCtrl& tree = GetTreeCtrl(); \
	HTREEITEM hItem = tree.GetSelectedItem(); \
	if( !hItem ) { pCmdUI->Enable( FALSE ); return; } \
	DWORD dwType = tree.GetItemData( hItem );

// checks whether an experiment is in process
#define	IS_EXP_RUNNING() \
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd; \
	CGraphView* pGV = pMF ? pMF->GetRecordingPane() : NULL; \
	BOOL fRunExp = pGV ? !pGV->fRecording : FALSE;	\
	fRunExp &= !_Reprocessing;



// IDs of drag & drop items
CString	_DragID;
CString	_DropID;

// static var initialization
CString CLeftView::m_szDropCondID = _T("");

// support function to return integer index of a string list based upon POSITION
int FindPosition( CStringList& lst, POSITION pos );

// Thread management for experiment reprocessing
BOOL	_Reprocessing = FALSE;
UINT	ReprocessExperiment( LPVOID pParam );
UINT	ReprocessExperimentCond( LPVOID pParam );		// For condition-based
UINT	ReprocessGroup( LPVOID pParam );
CMainFrame* _MF = NULL;
CLeftView*	_LV = NULL;
CListView*	_MsgWnd = NULL;
BOOL		_TerminateThread = FALSE;
BOOL		_ReprocAll = FALSE;
CString		_CondID;
int			_nFindCount = 0;

// Action Management
UINT		_ActionState = ACTION_NONE;

// Backup Management
#include "..\NSSharedGUI\Backup.h"
UINT DoBackupExperiment( LPVOID pParam );
UINT DoBackupGroup( LPVOID pParam );
UINT DoBackupSubject( LPVOID pParam );
UINT DoBackupSystem( LPVOID pParam );

/////////////////////////////////////////////////////////////////////////////
// CLeftView

IMPLEMENT_DYNCREATE(CLeftView, CTreeView)

BEGIN_MESSAGE_MAP(CLeftView, CTreeView)
	ON_WM_CONTEXTMENU()
	ON_WM_TIMER()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_COMMAND(IDM_SF_ADD, OnSfAdd)
	ON_COMMAND(IDM_EF_ADD, OnEfAdd)
	ON_COMMAND(IDM_FILE_IMPORT, OnFileImport)
	ON_COMMAND(IDM_E_EDIT, OnEEdit2)
	ON_COMMAND(IDM_E_DELETE, OnEDelete)
	ON_COMMAND(IDM_E_ADDGROUP, OnEAddgroup)
	ON_COMMAND(IDM_GF_ADD, OnGfAdd)
	ON_COMMAND(IDM_GF_ADDGROUPS, OnGfAddgroups)
	ON_COMMAND(IDM_EF_FIND, OnEfFind)
	ON_COMMAND(IDM_GF_FIND, OnGfFind)
	ON_COMMAND(IDM_G_EDIT, OnGEdit2)
	ON_COMMAND(IDM_G_DELETE, OnGDelete)
	ON_COMMAND(IDM_G_ADDSUBJ, OnGAddsubj)
	ON_COMMAND(IDM_SF_FIND, OnSfFind)
	ON_COMMAND(IDM_S_DELETE, OnSDelete)
	ON_COMMAND(IDM_S_EDIT, OnSEdit2)
	ON_COMMAND(IDM_S_REMOVE, OnSRemove)
	ON_COMMAND(IDM_G_REMOVE, OnGRemove)
	ON_COMMAND(IDM_SF_ADDSUBJECTS, OnSfAddsubjects)
	ON_COMMAND(IDM_E_ADDCONDITION, OnEAddcondition)
	ON_COMMAND(IDM_CF_ADD, OnCfAdd)
	ON_COMMAND(IDM_CF_FIND, OnCfFind)
	ON_COMMAND(IDM_CF_ADDCONDITIONS, OnCfAddconditions)
	ON_COMMAND(IDM_C_EDIT, OnCEdit2)
	ON_COMMAND(IDM_C_DELETE, OnCDelete)
	ON_COMMAND(IDM_C_REMOVE, OnCRemove)
	ON_COMMAND(IDM_S_EXP, OnSExp)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_COMMAND(IDM_C_REPS, OnCReps)
	ON_COMMAND(IDM_E_SUM, OnESum)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_UPDATE_COMMAND_UI(IDM_EF_ADD, OnUpdateEfAdd)
	ON_UPDATE_COMMAND_UI(IDM_EF_FIND, OnUpdateEfFind)
	ON_UPDATE_COMMAND_UI(IDM_FILE_IMPORT, OnUpdateEfImport)
	ON_UPDATE_COMMAND_UI(IDM_E_EDIT, OnUpdateEEdit)
	ON_UPDATE_COMMAND_UI(IDM_E_DELETE, OnUpdateEDelete)
	ON_UPDATE_COMMAND_UI(IDM_E_ADDCONDITION, OnUpdateEAddcondition)
	ON_UPDATE_COMMAND_UI(IDM_E_ADDGROUP, OnUpdateEAddgroup)
	ON_UPDATE_COMMAND_UI(IDM_E_SUM, OnUpdateESum)
	ON_UPDATE_COMMAND_UI(IDM_GF_ADD, OnUpdateGfAdd)
	ON_UPDATE_COMMAND_UI(IDM_GF_FIND, OnUpdateGfFind)
	ON_UPDATE_COMMAND_UI(IDM_GF_ADDGROUPS, OnUpdateGfAddgroups)
	ON_UPDATE_COMMAND_UI(IDM_G_EDIT, OnUpdateGEdit)
	ON_UPDATE_COMMAND_UI(IDM_G_DELETE, OnUpdateGDelete)
	ON_UPDATE_COMMAND_UI(IDM_G_ADDSUBJ, OnUpdateGAddsubj)
	ON_UPDATE_COMMAND_UI(IDM_G_REMOVE, OnUpdateGRemove)
	ON_UPDATE_COMMAND_UI(IDM_SF_ADD, OnUpdateSfAdd)
	ON_UPDATE_COMMAND_UI(IDM_SF_ADDSUBJECTS, OnUpdateSfAddsubjects)
	ON_UPDATE_COMMAND_UI(IDM_SF_FIND, OnUpdateSfFind)
	ON_UPDATE_COMMAND_UI(IDM_S_DELETE, OnUpdateSDelete)
	ON_UPDATE_COMMAND_UI(IDM_S_EDIT, OnUpdateSEdit)
	ON_UPDATE_COMMAND_UI(IDM_S_EXP, OnUpdateSExp)
	ON_UPDATE_COMMAND_UI(IDM_S_REMOVE, OnUpdateSRemove)
	ON_UPDATE_COMMAND_UI(IDM_CF_ADD, OnUpdateCfAdd)
	ON_UPDATE_COMMAND_UI(IDM_CF_ADDCONDITIONS, OnUpdateCfAddconditions)
	ON_UPDATE_COMMAND_UI(IDM_CF_FIND, OnUpdateCfFind)
	ON_UPDATE_COMMAND_UI(IDM_C_DELETE, OnUpdateCDelete)
	ON_UPDATE_COMMAND_UI(IDM_C_EDIT, OnUpdateCEdit)
	ON_UPDATE_COMMAND_UI(IDM_C_REMOVE, OnUpdateCRemove)
	ON_UPDATE_COMMAND_UI(IDM_C_REPS, OnUpdateCReps)
	ON_UPDATE_COMMAND_UI(IDM_C_RELATIONSHIPS, OnUpdateCRelationships)
	ON_UPDATE_COMMAND_UI(IDM_G_RELATIONSHIPS, OnUpdateGRelationships)
	ON_UPDATE_COMMAND_UI(IDM_S_RELATIONSHIPS, OnUpdateSRelationships)
	ON_COMMAND(IDM_C_RELATIONSHIPS, OnCRelationships)
	ON_COMMAND(IDM_G_RELATIONSHIPS, OnGRelationships)
	ON_COMMAND(IDM_S_RELATIONSHIPS, OnSRelationships)
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBegindrag)
	ON_COMMAND(IDM_T_VIEW, OnTView)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW, OnUpdateTView)
	ON_COMMAND(IDM_T_NOTES, OnTNotes)
	ON_UPDATE_COMMAND_UI(IDM_T_NOTES, OnUpdateTNotes)
	ON_COMMAND(IDM_T_VIEW_TF, OnTViewTf)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_TF, OnUpdateTViewTf)
	ON_COMMAND(IDM_T_CHART_HWR, OnTChartHwr)
	ON_UPDATE_COMMAND_UI(IDM_T_CHART_HWR, OnUpdateTChart)
	ON_COMMAND(IDM_T_CHART_HWRR, OnTChartHwrRT)
	ON_UPDATE_COMMAND_UI(IDM_T_CHART_HWRR, OnUpdateTChart)
	ON_COMMAND(IDM_T_CHART_TFR, OnTChartTfRT)
	ON_UPDATE_COMMAND_UI(IDM_T_CHART_TFR, OnUpdateTChart)
	ON_COMMAND(IDM_T_CHART_TF, OnTChartTf)
	ON_UPDATE_COMMAND_UI(IDM_T_CHART_TF, OnUpdateTChart)
	ON_COMMAND(IDM_T_CHART_HWR3D, OnTChartHwr3D)
	ON_UPDATE_COMMAND_UI(IDM_T_CHART_HWR3D, OnUpdateTChart)
	ON_COMMAND(IDM_T_CHART_TF3D, OnTChartTf3D)
	ON_UPDATE_COMMAND_UI(IDM_T_CHART_TF3D, OnUpdateTChart)
	ON_COMMAND(IDM_T_REDO, OnTRedo)
	ON_UPDATE_COMMAND_UI(IDM_T_REDO, OnUpdateTRedo)
	ON_COMMAND(IDM_S_REPROCESS, OnSReprocess)
	ON_UPDATE_COMMAND_UI(IDM_S_REPROCESS, OnUpdateSReprocess)
	ON_COMMAND(IDM_S_PROCESS, OnSProcess)
	ON_UPDATE_COMMAND_UI(IDM_S_PROCESS, OnUpdateSProcess)
	ON_COMMAND(IDM_E_REPROCESS, OnEReprocess)
	ON_UPDATE_COMMAND_UI(IDM_E_REPROCESS, OnUpdateEReprocess)
	ON_COMMAND(IDM_E_PROCESS, OnEProcess)
	ON_UPDATE_COMMAND_UI(IDM_E_PROCESS, OnUpdateEProcess)
	ON_COMMAND(IDM_S_QUEST, OnSQuest)
	ON_UPDATE_COMMAND_UI(IDM_S_QUEST, OnUpdateSQuest)
	ON_COMMAND(IDM_G_QUEST, OnGQuest)
	ON_UPDATE_COMMAND_UI(IDM_G_QUEST, OnUpdateGQuest)
	ON_COMMAND(IDM_E_CLEAN, OnEClean)
	ON_UPDATE_COMMAND_UI(IDM_E_CLEAN, OnUpdateEClean)
	ON_COMMAND(IDM_T_VIEW_EXT, OnTViewExt)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_EXT, OnUpdateTViewExt)
	ON_COMMAND(IDM_T_VIEW_ERR, OnTViewErr)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_ERR, OnUpdateTViewErr)
	ON_COMMAND(IDM_T_VIEW_DIS, OnTViewDis)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_DIS, OnUpdateTViewErr)
	ON_COMMAND(IDM_T_VIEW_WRD, OnTViewWrd)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_WRD, OnUpdateTViewErr)
	ON_COMMAND(IDM_S_CLEAN, OnSClean)
	ON_UPDATE_COMMAND_UI(IDM_S_CLEAN, OnUpdateSClean)
	ON_COMMAND(IDM_G_REPROCESS, OnGReprocess)
	ON_UPDATE_COMMAND_UI(IDM_G_REPROCESS, OnUpdateGReprocess)
	ON_COMMAND(IDM_G_PROCESS, OnGProcess)
	ON_UPDATE_COMMAND_UI(IDM_G_PROCESS, OnUpdateGProcess)
	ON_COMMAND(IDM_E_STOPREPROCESS, OnEStopreprocess)
	ON_UPDATE_COMMAND_UI(IDM_E_STOPREPROCESS, OnUpdateEStopreprocess)
	ON_COMMAND(IDM_T_REPROCESS, OnTReprocess)
	ON_UPDATE_COMMAND_UI(IDM_T_REPROCESS, OnUpdateTReprocess)
	ON_COMMAND(IDM_E_VIEW_INC, OnEViewInc)
	ON_UPDATE_COMMAND_UI(IDM_E_VIEW_INC, OnUpdateEViewInc)
	ON_COMMAND(IDM_E_VIEW_ERR, OnEViewErr)
	ON_UPDATE_COMMAND_UI(IDM_E_VIEW_ERR, OnUpdateEViewErr)
	ON_COMMAND(IDM_E_VIEW_ERS, OnEViewErs)
	ON_UPDATE_COMMAND_UI(IDM_E_VIEW_ERS, OnUpdateEViewErs)
	ON_COMMAND(IDM_E_REPORTPROCSET, OnEReportprocset)
	ON_UPDATE_COMMAND_UI(IDM_E_REPORTPROCSET, OnUpdateEReportprocset)
	ON_COMMAND(IDM_C_REPROCESS, OnCReprocess)
	ON_UPDATE_COMMAND_UI(IDM_C_REPROCESS, OnUpdateCReprocess)
	ON_COMMAND(IDM_C_PROCESS, OnCProcess)
	ON_UPDATE_COMMAND_UI(IDM_C_PROCESS, OnUpdateCProcess)
	ON_COMMAND(IDM_E_A_AVGS, OnEAAvgs)
	ON_UPDATE_COMMAND_UI(IDM_E_A_AVGS, OnUpdateEAAvgs)
	ON_COMMAND(IDM_E_A_AVGS2, OnEAAvgs2)
	ON_UPDATE_COMMAND_UI(IDM_E_A_AVGS2, OnUpdateEAAvgs2)
	ON_COMMAND(IDM_E_A_STDDEVS, OnEAStddevs)
	ON_UPDATE_COMMAND_UI(IDM_E_A_STDDEVS, OnUpdateEAStddevs)
	ON_COMMAND(IDM_E_A_STDDEVS2, OnEAStddevs2)
	ON_UPDATE_COMMAND_UI(IDM_E_A_STDDEVS2, OnUpdateEAStddevs2)
	ON_COMMAND(IDM_E_A_TRIAL, OnEATrial)
	ON_UPDATE_COMMAND_UI(IDM_E_A_TRIAL, OnUpdateEATrial)
	ON_COMMAND(IDM_T_REPROCESS_NOEXT, OnTReprocessNoext)
	ON_UPDATE_COMMAND_UI(IDM_T_REPROCESS_NOEXT, OnUpdateTReprocessNoext)
	ON_COMMAND(IDM_E_SORTGROUPS, OnESortgroups)
	ON_COMMAND(IDM_E_SORTCONDS, OnESortconds)
	ON_COMMAND(IDM_E_SORTSUBJS, OnESortsubjs)
	ON_UPDATE_COMMAND_UI(IDM_E_SORTGROUPS, OnUpdateESortgroups)
	ON_UPDATE_COMMAND_UI(IDM_E_SORTCONDS, OnUpdateESortconds)
	ON_UPDATE_COMMAND_UI(IDM_E_SORTSUBJS, OnUpdateESortsubjs)
	ON_COMMAND(IDM_S_MOVE, OnSMove)
	ON_UPDATE_COMMAND_UI(IDM_S_MOVE, OnUpdateSMove)
	ON_COMMAND(IDM_S_SHOWTRIALS, OnSShowtrials)
	ON_UPDATE_COMMAND_UI(IDM_S_SHOWTRIALS, OnUpdateSShowtrials)
	ON_COMMAND(IDM_E_BACKUP, OnEBackup)
	ON_UPDATE_COMMAND_UI(IDM_E_BACKUP, OnUpdateEBackup)
	ON_COMMAND(IDM_G_BACKUP, OnGBackup)
	ON_UPDATE_COMMAND_UI(IDM_G_BACKUP, OnUpdateGBackup)
	ON_COMMAND(IDM_S_BACKUP, OnSBackup)
	ON_UPDATE_COMMAND_UI(IDM_S_BACKUP, OnUpdateSBackup)
	ON_COMMAND(IDM_T_CONDITION, OnTCondition)
	ON_UPDATE_COMMAND_UI(IDM_T_CONDITION, OnUpdateTCondition)
	ON_COMMAND(IDM_T_EXPERIMENT, OnTExperiment)
	ON_UPDATE_COMMAND_UI(IDM_T_EXPERIMENT, OnUpdateTCondition)
	ON_COMMAND(IDM_T_REPS, OnTConditionReps)
	ON_UPDATE_COMMAND_UI(IDM_T_REPS, OnUpdateTConditionReps)
	ON_COMMAND(IDM_E_A_ALL, OnEAAll)
	ON_UPDATE_COMMAND_UI(IDM_E_A_ALL, OnUpdateEAAll)
	ON_COMMAND(IDM_E_A_EW, OnEAEw)
	ON_UPDATE_COMMAND_UI(IDM_E_A_EW, OnUpdateEAEw)
	ON_COMMAND(IDM_E_A_UEW, OnEAUew)
	ON_UPDATE_COMMAND_UI(IDM_E_A_UEW, OnUpdateEAUew)
	ON_COMMAND(IDM_E_A_COMBO, OnEACombo)
	ON_UPDATE_COMMAND_UI(IDM_E_A_COMBO, OnUpdateEACombo)
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_NOTIFY_REFLECT(TVN_KEYDOWN, OnKeydown)
	ON_COMMAND(IDM_E_EXPORT, OnEExport)
	ON_UPDATE_COMMAND_UI(IDM_E_EXPORT, OnUpdateEExport)
	ON_COMMAND(IDM_T_DELETE, OnTDelete)
	ON_UPDATE_COMMAND_UI(IDM_T_DELETE, OnUpdateTDelete)
	ON_COMMAND(IDM_C_DUP, OnCDup)
	ON_UPDATE_COMMAND_UI(IDM_C_DUP, OnUpdateCDup)
	ON_COMMAND(IDM_C_DUP_MULT, OnCDupMulti)
	ON_UPDATE_COMMAND_UI(IDM_C_DUP_MULT, OnUpdateCDupMulti)
	ON_COMMAND(IDM_S_CHARTALL, OnSChartall)
	ON_UPDATE_COMMAND_UI(IDM_S_CHARTALL, OnUpdateSChartall)
	ON_COMMAND(IDM_EL_DELETE, OnElDelete)
	ON_UPDATE_COMMAND_UI(IDM_EL_DELETE, OnUpdateElDelete)
	ON_COMMAND(IDM_EL_EDIT, OnElEdit)
	ON_UPDATE_COMMAND_UI(IDM_EL_EDIT, OnUpdateElEdit)
	ON_COMMAND(IDM_ELF_ADD, OnElfAdd)
	ON_UPDATE_COMMAND_UI(IDM_ELF_ADD, OnUpdateElfAdd)
	ON_COMMAND(IDM_ELF_FIND, OnElfFind)
	ON_UPDATE_COMMAND_UI(IDM_ELF_FIND, OnUpdateElfFind)
	ON_COMMAND(IDM_ST_DELETE, OnStDelete)
	ON_UPDATE_COMMAND_UI(IDM_ST_DELETE, OnUpdateStDelete)
	ON_COMMAND(IDM_ST_EDIT, OnStEdit)
	ON_UPDATE_COMMAND_UI(IDM_ST_EDIT, OnUpdateStEdit)
	ON_COMMAND(IDM_STF_ADD, OnStfAdd)
	ON_UPDATE_COMMAND_UI(IDM_STF_ADD, OnUpdateStfAdd)
	ON_COMMAND(IDM_STF_FIND, OnStfFind)
	ON_UPDATE_COMMAND_UI(IDM_STF_FIND, OnUpdateStfFind)
	ON_COMMAND(IDM_ELF_ADDELEMENTS, OnElfAddelements)
	ON_UPDATE_COMMAND_UI(IDM_ELF_ADDELEMENTS, OnUpdateElfAddelements)
	ON_COMMAND(IDM_ST_ADDELEMENTS, OnStAddelements)
	ON_UPDATE_COMMAND_UI(IDM_ST_ADDELEMENTS, OnUpdateStAddelements)
	ON_COMMAND(IDM_EL_REMOVE, OnElRemove)
	ON_UPDATE_COMMAND_UI(IDM_EL_REMOVE, OnUpdateElRemove)
	ON_COMMAND(IDM_EL_VIEW, OnElView)
	ON_UPDATE_COMMAND_UI(IDM_EL_VIEW, OnUpdateElView)
	ON_COMMAND(IDM_EL_CHART, OnElChart)
	ON_UPDATE_COMMAND_UI(IDM_EL_CHART, OnUpdateElChart)
	ON_COMMAND(IDM_ST_VIEW, OnStView)
	ON_UPDATE_COMMAND_UI(IDM_ST_VIEW, OnUpdateStView)
	ON_COMMAND(IDM_ST_CHART, OnStChart)
	ON_UPDATE_COMMAND_UI(IDM_ST_CHART, OnUpdateStChart)
	ON_COMMAND(IDM_EL_RELATIONSHIPS, OnElRelationships)
	ON_UPDATE_COMMAND_UI(IDM_EL_RELATIONSHIPS, OnUpdateElRelationships)
	ON_COMMAND(IDM_ST_RELATIONSHIPS, OnStRelationships)
	ON_UPDATE_COMMAND_UI(IDM_ST_RELATIONSHIPS, OnUpdateStRelationships)
	ON_COMMAND(IDM_EL_DISPLAY, OnElDisplay)
	ON_UPDATE_COMMAND_UI(IDM_EL_DISPLAY, OnUpdateElDisplay)
	ON_COMMAND(IDM_ST_DISPLAY, OnStDisplay)
	ON_UPDATE_COMMAND_UI(IDM_ST_DISPLAY, OnUpdateStDisplay)
	ON_COMMAND(IDM_EL_DUP, OnElDup)
	ON_UPDATE_COMMAND_UI(IDM_EL_DUP, OnUpdateElDup)
	ON_COMMAND(IDM_E_REPORTSUBJECTS, OnEReportsubjects)
	ON_UPDATE_COMMAND_UI(IDM_E_REPORTSUBJECTS, OnUpdateEReportsubjects)
	ON_COMMAND(IDM_ST_DUP, OnStDup)
	ON_UPDATE_COMMAND_UI(IDM_ST_DUP, OnUpdateStDup)
	ON_COMMAND(IDM_C_TESTLEX, OnCTestlex)
	ON_UPDATE_COMMAND_UI(IDM_C_TESTLEX, OnUpdateCTestlex)
	ON_COMMAND(IDM_T_VIEW_SEG, OnTViewSeg)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_SEG, OnUpdateTViewSeg)
	ON_COMMAND(IDM_T_VIEW_CON, OnTViewCon)
	ON_UPDATE_COMMAND_UI(IDM_T_VIEW_CON, OnUpdateTViewCon)
	ON_COMMAND(IDM_STF_REFRESH, OnStfRefresh)
	ON_UPDATE_COMMAND_UI(IDM_STF_REFRESH, OnUpdateStfRefresh)
	ON_COMMAND(IDM_CAT_DELETE, OnCatDelete)
	ON_UPDATE_COMMAND_UI(IDM_CAT_DELETE, OnUpdateCatDelete)
	ON_COMMAND(IDM_CAT_EDIT, OnCatEdit)
	ON_UPDATE_COMMAND_UI(IDM_CAT_EDIT, OnUpdateCatEdit)
	ON_COMMAND(IDM_CATF_ADD, OnCatfAdd)
	ON_UPDATE_COMMAND_UI(IDM_CATF_ADD, OnUpdateCatfAdd)
	ON_COMMAND(IDM_CATF_FIND, OnCatfFind)
	ON_UPDATE_COMMAND_UI(IDM_CATF_FIND, OnUpdateCatfFind)
	ON_COMMAND(IDM_FB_DELETE, OnFbDelete)
	ON_UPDATE_COMMAND_UI(IDM_FB_DELETE, OnUpdateFbDelete)
	ON_COMMAND(IDM_FB_EDIT, OnFbEdit)
	ON_UPDATE_COMMAND_UI(IDM_FB_EDIT, OnUpdateFbEdit)
	ON_COMMAND(IDM_FBF_ADD, OnFbfAdd)
	ON_UPDATE_COMMAND_UI(IDM_FBF_ADD, OnUpdateFbfAdd)
	ON_COMMAND(IDM_FBF_FIND, OnFbfFind)
	ON_UPDATE_COMMAND_UI(IDM_FBF_FIND, OnUpdateFbfFind)
	ON_COMMAND(IDM_E_REPORTQUEST, OnEReportquest)
	ON_UPDATE_COMMAND_UI(IDM_E_REPORTQUEST, OnUpdateEReportquest)
	ON_UPDATE_COMMAND_UI(IDM_EF_COPYALL, OnUpdateEfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_E_COPYALL, OnUpdateECopyall)
	ON_UPDATE_COMMAND_UI(IDM_GF_COPYALL, OnUpdateGfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_G_COPYALL, OnUpdateGCopyall)
	ON_UPDATE_COMMAND_UI(IDM_SF_COPYALL, OnUpdateSfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_S_COPYALL, OnUpdateSCopyall)
	ON_UPDATE_COMMAND_UI(IDM_CF_COPYALL, OnUpdateCfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_C_COPYALL, OnUpdateCCopyall)
	ON_UPDATE_COMMAND_UI(IDM_STF_COPYALL, OnUpdateStfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_ST_COPYALL, OnUpdateStCopyall)
	ON_UPDATE_COMMAND_UI(IDM_ELF_COPYALL, OnUpdateElfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_EL_COPYALL, OnUpdateElCopyall)
	ON_UPDATE_COMMAND_UI(IDM_CATF_COPYALL, OnUpdateCatfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_CAT_COPYALL, OnUpdateCatCopyall)
	ON_UPDATE_COMMAND_UI(IDM_FBF_COPYALL, OnUpdateFbfCopyall)
	ON_UPDATE_COMMAND_UI(IDM_FB_COPYALL, OnUpdateFbCopyall)
	ON_COMMAND(IDM_EF_COPYALL, OnEfCopyall)
	ON_COMMAND(IDM_E_COPYALL, OnECopyall)
	ON_COMMAND(IDM_GF_COPYALL, OnGfCopyall)
	ON_COMMAND(IDM_G_COPYALL, OnGCopyall)
	ON_COMMAND(IDM_SF_COPYALL, OnSfCopyall)
	ON_COMMAND(IDM_S_COPYALL, OnSCopyall)
	ON_COMMAND(IDM_CF_COPYALL, OnCfCopyall)
	ON_COMMAND(IDM_C_COPYALL, OnCCopyall)
	ON_COMMAND(IDM_STF_COPYALL, OnStfCopyall)
	ON_COMMAND(IDM_ST_COPYALL, OnStCopyall)
	ON_COMMAND(IDM_ELF_COPYALL, OnElfCopyall)
	ON_COMMAND(IDM_EL_COPYALL, OnElCopyall)
	ON_COMMAND(IDM_CATF_COPYALL, OnCatfCopyall)
	ON_COMMAND(IDM_CAT_COPYALL, OnCatCopyall)
	ON_COMMAND(IDM_FBF_COPYALL, OnFbfCopyall)
	ON_COMMAND(IDM_FB_COPYALL, OnFbCopyall)
	ON_UPDATE_COMMAND_UI(IDM_EF_UPLOAD, OnUpdateEfUpload)
	ON_UPDATE_COMMAND_UI(IDM_E_UPLOAD, OnUpdateEUpload)
	ON_UPDATE_COMMAND_UI(IDM_GF_UPLOAD, OnUpdateGfUpload)
	ON_UPDATE_COMMAND_UI(IDM_G_UPLOAD, OnUpdateGUpload)
	ON_UPDATE_COMMAND_UI(IDM_SF_UPLOAD, OnUpdateSfUpload)
	ON_UPDATE_COMMAND_UI(IDM_S_UPLOAD, OnUpdateSUpload)
	ON_UPDATE_COMMAND_UI(IDM_CF_UPLOAD, OnUpdateCfUpload)
	ON_UPDATE_COMMAND_UI(IDM_C_UPLOAD, OnUpdateCUpload)
	ON_UPDATE_COMMAND_UI(IDM_STF_UPLOAD, OnUpdateStfUpload)
	ON_UPDATE_COMMAND_UI(IDM_ST_UPLOAD, OnUpdateStUpload)
	ON_UPDATE_COMMAND_UI(IDM_ELF_UPLOAD, OnUpdateElfUpload)
	ON_UPDATE_COMMAND_UI(IDM_EL_UPLOAD, OnUpdateElUpload)
	ON_UPDATE_COMMAND_UI(IDM_CATF_UPLOAD, OnUpdateCatfUpload)
	ON_UPDATE_COMMAND_UI(IDM_CAT_UPLOAD, OnUpdateCatUpload)
	ON_UPDATE_COMMAND_UI(IDM_FBF_UPLOAD, OnUpdateFbfUpload)
	ON_UPDATE_COMMAND_UI(IDM_FB_UPLOAD, OnUpdateFbUpload)
	ON_COMMAND(IDM_EF_UPLOAD, OnEfUpload)
	ON_COMMAND(IDM_E_UPLOAD, OnEUpload)
	ON_COMMAND(IDM_GF_UPLOAD, OnGfUpload)
	ON_COMMAND(IDM_G_UPLOAD, OnGUpload)
	ON_COMMAND(IDM_SF_UPLOAD, OnSfUpload)
	ON_COMMAND(IDM_S_UPLOAD, OnSUpload)
	ON_COMMAND(IDM_CF_UPLOAD, OnCfUpload)
	ON_COMMAND(IDM_C_UPLOAD, OnCUpload)
	ON_COMMAND(IDM_STF_UPLOAD, OnStfUpload)
	ON_COMMAND(IDM_ST_UPLOAD, OnStUpload)
	ON_COMMAND(IDM_ELF_UPLOAD, OnElfUpload)
	ON_COMMAND(IDM_EL_UPLOAD, OnElUpload)
	ON_COMMAND(IDM_CATF_UPLOAD, OnCatfUpload)
	ON_COMMAND(IDM_CAT_UPLOAD, OnCatUpload)
	ON_COMMAND(IDM_FBF_UPLOAD, OnFbfUpload)
	ON_COMMAND(IDM_FB_UPLOAD, OnFbUpload)
	ON_COMMAND(IDM_E_EXPSUM_PARAM, OnEExpsumParam)
	ON_UPDATE_COMMAND_UI(IDM_E_EXPSUM_PARAM, OnUpdateEExpsumParam)
	ON_COMMAND(IDM_E_EXPSUM_SUBM, OnEExpsumSubm)
	ON_UPDATE_COMMAND_UI(IDM_E_EXPSUM_SUBM, OnUpdateEExpsumSubm)
	ON_COMMAND(IDM_MQ_COPYALL, OnMQCopy)
	ON_COMMAND(IDM_MQ_UPLOAD, OnMQUpload)
	ON_UPDATE_COMMAND_UI(IDM_MQ_COPYALL, OnUpdateMQCopy)
	ON_UPDATE_COMMAND_UI(IDM_MQ_UPLOAD, OnUpdateMQUpload)
	ON_COMMAND(IDM_T_LINEARITY, OnTLinearity)
	ON_UPDATE_COMMAND_UI(IDM_T_LINEARITY, OnUpdateTLinearity)
	ON_COMMAND(IDM_E_REPORTSUMB, OnEReportSummaryB)
	ON_UPDATE_COMMAND_UI(IDM_E_REPORTSUMB, OnUpdateEReportSummaryB)
	ON_COMMAND(IDM_E_REPORTSUMD, OnEReportSummaryD)
	ON_UPDATE_COMMAND_UI(IDM_E_REPORTSUMD, OnUpdateEReportSummaryD)
	ON_COMMAND(IDM_I_VIEW, OnIView)
	ON_COMMAND(IDM_I_VIEWF, OnIViewF)
	ON_COMMAND(IDM_I_PROCESS, OnIProcess)
	ON_UPDATE_COMMAND_UI(IDM_I_PROCESS, OnUpdateIProcess)
	ON_COMMAND(IDM_S_EXPORT, OnSExport)
	ON_UPDATE_COMMAND_UI(IDM_S_EXPORT, OnUpdateSExport)
	ON_COMMAND(IDM_ST_STIMEDIT, OnStStimEdit)
	ON_UPDATE_COMMAND_UI(IDM_ST_STIMEDIT, OnUpdateStStimEdit)
	ON_COMMAND(IDM_S_EXPLAST, OnSLastExp)
	ON_UPDATE_COMMAND_UI(IDM_S_EXPLAST, OnUpdateSLastExp)
	ON_COMMAND(IDM_E_EXPORTWIZ, OnExportWizard)
	ON_UPDATE_COMMAND_UI(IDM_E_EXPORTWIZ, OnUpdateExportWizard)
	ON_COMMAND(IDM_FIND2, OnFind)
	ON_UPDATE_COMMAND_UI(IDM_FIND2, OnUpdateFind)
	ON_COMMAND(IDM_FIND_NEXT, OnFindNext)
	ON_UPDATE_COMMAND_UI(IDM_FIND_NEXT, OnUpdateFindNext)
	ON_COMMAND(IDM_S_RECOVERPASS, OnSRecoverPass)
	ON_UPDATE_COMMAND_UI(IDM_S_RECOVERPASS, OnUpdateSRecoverPass)
	ON_COMMAND(IDM_S_RESTOREPASS, OnSRestorePass)
	ON_UPDATE_COMMAND_UI(IDM_S_RESTOREPASS, OnUpdateSRestorePass)
	ON_COMMAND(IDM_S_CONDSEQ, OnSCondSequence)
	ON_UPDATE_COMMAND_UI(IDM_S_CONDSEQ, OnUpdateSCondSequence)
	ON_COMMAND(IDM_T_QUICK, OnTQuick)
	ON_UPDATE_COMMAND_UI(IDM_T_QUICK, OnUpdateTQuick)
	ON_COMMAND(IDM_E_EDIT_SUM, OnEEditSum)
	ON_UPDATE_COMMAND_UI(IDM_E_EDIT_SUM, OnUpdateEEdit)
	ON_COMMAND(IDM_G_EDIT_EXP, OnGEditExp)
	ON_UPDATE_COMMAND_UI(IDM_G_EDIT_EXP, OnUpdateGEditExp)
	ON_COMMAND(IDM_S_EDIT_EXP, OnSEditExp)
	ON_UPDATE_COMMAND_UI(IDM_S_EDIT_EXP, OnUpdateSEditExp)
	ON_COMMAND(IDM_C_EDIT_EXP, OnCEditExp)
	ON_UPDATE_COMMAND_UI(IDM_C_EDIT_EXP, OnUpdateCEditExp)
	ON_COMMAND(IDM_E_EXCL_SUBJ, OnEViewExclSubj)
	ON_UPDATE_COMMAND_UI(IDM_E_EXCL_SUBJ, OnUpdateEViewExcl)
	ON_COMMAND(IDM_E_EXCL_FEAT, OnEViewExclFeat)
	ON_UPDATE_COMMAND_UI(IDM_E_EXCL_FEAT, OnUpdateEViewExcl)
	ON_COMMAND(IDM_E_EXCL_QUEST, OnEViewExclQuest)
	ON_UPDATE_COMMAND_UI(IDM_E_EXCL_QUEST, OnUpdateEViewExcl)
	ON_COMMAND(IDM_VIEW_ATTACH, OnViewAttachments)
	ON_COMMAND(IDM_E_DUP, OnEDup)
	ON_UPDATE_COMMAND_UI(IDM_E_DUP, OnUpdateEDup)
	ON_COMMAND(IDM_E_VAL, OnValidateExp)
	ON_UPDATE_COMMAND_UI(IDM_E_VAL, OnUpdateValidateExp)
	ON_COMMAND(IDM_E_BUILDNORMDB, OnBuildNormDB)
	ON_UPDATE_COMMAND_UI(IDM_E_BUILDNORMDB, OnUpdateBuildNormDB)
	ON_COMMAND(IDM_E_VIEWNORMDB, OnViewNormDB)
	ON_COMMAND(IDM_S_BUILDNORMDB, OnSBuildNormDB)
	ON_UPDATE_COMMAND_UI(IDM_S_BUILDNORMDB, OnUpdateSBuildNormDB)
	ON_COMMAND(IDM_S_VIEWZSCORES, OnViewZScores)
	ON_COMMAND(IDM_E_VIEWZHISTORY, OnViewZHistory)
	ON_COMMAND(IDM_E_VIEWZHISTORYSUMMARY, OnEViewZHistorySummary)
	ON_UPDATE_COMMAND_UI(IDM_E_VIEWZHISTORYSUMMARY, OnUpdateEViewZHistorySummary)
	ON_COMMAND(IDM_S_VIEWZHISTORYSUMMARY, OnSViewZHistorySummary)
	ON_UPDATE_COMMAND_UI(IDM_S_VIEWZHISTORYSUMMARY, OnUpdateSViewZHistorySummary)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLeftView construction/destruction

//************************************
// Method:    CLeftView
// FullName:  CLeftView::CLeftView
// Access:    protected 
// Returns:   
// Qualifier: : m_hExp( NULL )*/, m_hGrp( NULL )*/, m_hSub( NULL )*/, m_hCond( NULL )*/, m_hitemDrag( NULL )*/, m_pImageList( NULL )*/, m_hitemDrop( NULL )*/, m_fDragging( FALSE )*/, m_hStim( NULL )*/, m_hElmt( NULL )*/, m_hCat( NULL )*/, m_hFb( NULL )*/, m_hNext( NULL )*/, MessageTarget()
//************************************
CLeftView::CLeftView() : m_hExp( NULL ), m_hGrp( NULL ), m_hSub( NULL ),
						 m_hCond( NULL ), m_hitemDrag( NULL ), m_pImageList( NULL ),
						 m_hitemDrop( NULL ), m_fDragging( FALSE ), m_hStim( NULL ),
						 m_hElmt( NULL ), m_hCat( NULL ), m_hFb( NULL ), m_hNext( NULL ),
						 m_hQ( NULL ), MessageTarget()
{
	m_pEML = NULL;
	m_pECL = NULL;
	m_fInit = FALSE;
	m_fFindPartial = FALSE;
	m_pStartView = NULL;
	m_wndDesktop.Attach( ::GetDesktopWindow() );
}

//************************************
// Method:    Cleanup
// FullName:  CLeftView::Cleanup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::Cleanup()
{
	if( m_pEML ) m_pEML->Release(); m_pEML = NULL;
	if( m_pECL ) m_pECL->Release(); m_pECL = NULL;
	HWND hWndDesktop = m_wndDesktop.Detach();
	if( hWndDesktop ) ::DestroyWindow( hWndDesktop );
}

//************************************
// Method:    ~CLeftView
// FullName:  CLeftView::~CLeftView
// Access:    virtual protected 
// Returns:   
// Qualifier:
//************************************
CLeftView::~CLeftView()
{
	OutputAMessage( _T("STOP"), TRUE );
	Cleanup();
}

//************************************
// Method:    PreCreateWindow
// FullName:  CLeftView::PreCreateWindow
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CREATESTRUCT & cs
//************************************
BOOL CLeftView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= TVS_HASLINES;
	cs.style |= TVS_LINESATROOT;
	cs.style |= TVS_HASBUTTONS;

	return CTreeView::PreCreateWindow(cs);
}

//************************************
// Method:    SetActionState
// FullName:  CLeftView::SetActionState
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nVal
//************************************
void CLeftView::SetActionState( UINT nVal )
{
	_ActionState = nVal;
}

//************************************
// Method:    GetActionState
// FullName:  CLeftView::GetActionState
// Access:    public 
// Returns:   UINT
// Qualifier:
//************************************
UINT CLeftView::GetActionState()
{
	return _ActionState;
}

/////////////////////////////////////////////////////////////////////////////
// Mouse handling methods
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    PreTranslateMessage
// FullName:  CLeftView::PreTranslateMessage
// Access:    virtual protected 
// Returns:   BOOL
// Qualifier:
// Parameter: MSG * pMsg
//************************************
BOOL CLeftView::PreTranslateMessage( MSG* pMsg )
{
	// Shift+F10: show pop-up menu.
	if( ( ( ( pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN ) &&	// If we hit a key and
		( pMsg->wParam == VK_F10 ) && ( GetKeyState( VK_SHIFT ) & ~1 ) ) != 0 ) ||	// it's Shift+F10 OR
		( pMsg->message == WM_CONTEXTMENU ) )										// Natural keyboard key
	{
		CRect rect;
		GetClientRect( rect );
		ClientToScreen( rect );

		CPoint point;
		GetCursorPos( &point );
		point.Offset( 5, 5 );
		OnContextMenu( NULL, point );

		return TRUE;
	}

	return CTreeView::PreTranslateMessage(pMsg);
}

/////////////////////////////////////////////////////////////////////////////
// OnContext Menu
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnContextMenu
// FullName:  CLeftView::OnContextMenu
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd *
// Parameter: CPoint point
//************************************
void CLeftView::OnContextMenu( CWnd*, CPoint point )
{
	try
	{
		// Determine type of object and get approp menu
		int nSubMenu = -1;	// Item

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.GetSelectedItem();
		if( !hItem ) return;

		DWORD dwType = tree.GetItemData( hItem );
		switch( dwType )
		{
			case TI_EXPERIMENT_FOLDER:
				nSubMenu = 0;
				break;
			case TI_EXPERIMENT:
				nSubMenu = 1;
				break;
			case TI_GROUP_FOLDER:
				nSubMenu = 2;
				break;
			case TI_GROUP:
				nSubMenu = 3;
				break;
			case TI_SUBJECT_FOLDER:
				nSubMenu = 4;
				break;
			case TI_SUBJECT:
				nSubMenu = 5;
				break;
			case TI_CONDITION_FOLDER:
				nSubMenu = 6;
				break;
			case TI_CONDITION:
				nSubMenu = 7;
				break;
			case TI_TRIAL:
				nSubMenu = 8;
				break;
			case TI_STIMULUS_FOLDER:
				nSubMenu = 9;
				break;
			case TI_STIMULUS:
				nSubMenu = 10;
				break;
			case TI_ELEMENT_FOLDER:
				nSubMenu = 11;
				break;
			case TI_ELEMENT:
				nSubMenu = 12;
				break;
			case TI_CATEGORY_FOLDER:
				nSubMenu = 13;
				break;
			case TI_CATEGORY:
				nSubMenu = 14;
				break;
			case TI_FEEDBACK_FOLDER:
				nSubMenu = 15;
				break;
			case TI_FEEDBACK:
				nSubMenu = 16;
				break;
			case TI_IMAGE:
				nSubMenu = 17;
				break;
			case TI_ATTACHMENT_FOLDER: case TI_ATTACHMENT:
				nSubMenu = 18;
				break;
		};

		if( nSubMenu == -1 ) return;

		CMenu menu;
		menu.LoadMenu( IDR_POPUP_TREE1 );

		CMenu* pPopup = menu.GetSubMenu( nSubMenu );

		if( pPopup )
		{
			CBCGPPopupMenu* pPopupMenu = new CBCGPPopupMenu;
			if( !pPopupMenu->Create( this, point.x, point.y, (HMENU)pPopup->m_hMenu, FALSE, TRUE ) )
				return;
			UpdateDialogControls( this, FALSE );
		}
	}
	catch( CMemoryException* e )
	{
		BCGPMessageBox( _T("Not enough memory to complete operation. Either close some applications or reboot your machine.") );
		e->Delete();
	}
	catch( CResourceException* e )
	{
		BCGPMessageBox( _T("Not enough system resources to complete operation. Either close some applications or rebooty your machine.") );
		e->Delete();
	}
	catch( ... )
	{
		BCGPMessageBox( _T("An unknown error has occured. Please close the application and try again.") );
	}
}

//************************************
// Method:    OnInitialUpdate
// FullName:  CLeftView::OnInitialUpdate
// Access:    virtual protected 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();

	// Set up the image list
	CTreeCtrl& tree = GetTreeCtrl();
	m_ImageList.Create( 18, 16, ILC_COLOR24, 20, 1 );
	CBitmap bmp;
	bmp.LoadBitmap( IDB_TREE );
	m_ImageList.Add( &bmp, 	RGB( 0, 255, 0 ) );
	tree.SetImageList( &m_ImageList, TVSIL_NORMAL );
	tree.ModifyStyle( 0, TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT | TVS_SHOWSELALWAYS );

	// Let's start
	OutputAMessage( _T("START"), TRUE );
	FillTree();
}

/////////////////////////////////////////////////////////////////////////////
// FillTree
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    FindPosition
// FullName:  FindPosition
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: CStringList & lst
// Parameter: POSITION pos
//************************************
int FindPosition( CStringList& lst, POSITION pos )
{
	int nIdx = -1, nPos = -1;
	CString szTmp;

	POSITION posCur = lst.GetHeadPosition();
	while( posCur )
	{
		nPos++;
		if( posCur == pos )
		{
			nIdx = nPos;
			break;
		}

		szTmp = lst.GetNext( posCur );
	}

	return nIdx;
}

//************************************
// Method:    OnTimer
// FullName:  CLeftView::OnTimer
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nIDEvent
//************************************
void CLeftView::OnTimer(UINT nIDEvent)
{
	// timer to fill tree
	if( nIDEvent != 1999 ) return;
	KillTimer( 1999 );
	m_fInit = TRUE;
	FillTree();

	// start dialog
	ShowStartPage();

	// getting started dialog
	CString szKey, szRB;
	szKey = _T("Settings");
	szRB = theApp.GetRegistryBase();
	theApp.SetRegistryBase( szKey );
	szKey = _T("StartPage");
	BOOL fShow = theApp.GetInt( szKey, TRUE );
	theApp.SetRegistryBase( szRB );
	if( fShow ) {
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		if( pMF ) pMF->ShowGettingStarted();
	}
}

//************************************
// Method:    DoTitle
// FullName:  CLeftView::DoTitle
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::DoTitle()
{
	// user in status bar
	CString szID;
	::GetCurrentUser( szID );
	CString szUser = _T("User: ");
	szUser += szID;
	CBCGPRibbonStatusBarPane* pPane = (CBCGPRibbonStatusBarPane*)(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wndStatusBar.FindByID( IDM_FILE_USERS ));
	if( pPane )
	{
		pPane->SetText( szUser );
		pPane->Redraw();
	}
	(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wndStatusBar).RecalcLayout();
	(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wndStatusBar).RedrawWindow();
}

//************************************
// Method:    FillTree
// FullName:  CLeftView::FillTree
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::FillTree()
{
	CTreeCtrl& tree = GetTreeCtrl();
	tree.DeleteAllItems();

	// set tree view background color
	if( !::IsPrivacyOn() ) tree.SetBkColor( PVT_COLOR );
	else tree.SetBkColor( ::GetSysColor( COLOR_WINDOW ) );

	CString szInput;
	long nCnt = 0;

	// application title
	DoTitle();

	// if license is not set right (& not demo), we show nothing
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() &&
		!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		pMF->OnHelpActivate();

		if( !::IsSA() && !::IsOA() && !::IsGA() &&
			!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
			return;
	}

	CWaitCursor crs;

	if( !m_fInit )
	{
		SetTimer( 1999, 1500, NULL );
		return;
	}

	crs.Restore();

	// Preferences/Settings
	CString szPath;
	::GetDataPathRoot( szPath );

	// Make sure root path exists
	{
		CFileFind ff;
		if( !ff.FindFile( szPath ) )
		{
			int nRes, nPos = 0;
			CString szTmp;

			szPath += _T("\\");
			nPos = szPath.Find( _T("\\") );
			nPos = szPath.Find( _T("\\"), nPos + 1 );
			while( nPos != -1 )
			{
				szTmp = szPath.Left( nPos );
				if( !ff.FindFile( szTmp ) )
				{
					nRes = _mkdir( szTmp );
					if( ( nRes != 0 ) && ( errno != EEXIST ) )
					{
						BCGPMessageBox( _T("Error creating root data path directory.") );
						return;
					}
				}

				nPos = szPath.Find( _T("\\"), nPos + 1 );
			}

			::GetDataPathRoot( szPath );
		}
	}

	// Output window
	crs.Restore();
	{
	Experiments exp;
	if( !exp.IsValid() ) return;
	exp.SetLoggingOn( IsLoggingOn() );
	exp.SetDataPath( szPath );
	exp.SetAppWindow( AfxGetApp()->m_pMainWnd->m_hWnd );
	::GetBackupPath( szPath );
	exp.SetBackupPath( szPath );
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = (CListView*)pMF->GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	exp.SetOutputWindow( (VARIANT*)&lst );
	}

	// Progress meter (1 step for each set of data)
//	Progress::EnableProgress( 6 + ::IsOA() ? 3 : 0 + 1 );
//	int nCur = 0;
//	::GetDataPathRoot( szPath );
//	CString szMsg;

	// Root experiment folder
//	szMsg.Format( _T("Loading experiments from database in %s"), szPath );
//	Progress::SetProgress( ++nCur, szMsg );
	crs.Restore();
	CString szData;
	szData.LoadString( IDS_EXPS );
	m_hExp = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT, TVI_SORT );
	tree.SetItemData( m_hExp, TI_EXPERIMENT_FOLDER );
	tree.SetItemState( m_hExp, TVIS_BOLD, TVIS_BOLD );
	RefreshExperiments();

	// Groups
//	szMsg.Format( _T("Loading groups from database in %s"), szPath );
//	Progress::SetProgress( ++nCur, szMsg );
	crs.Restore();
	szData.LoadString( IDS_GRPS );
	m_hGrp = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
	tree.SetItemData( m_hGrp, TI_GROUP_FOLDER );
	tree.SetItemState( m_hGrp, TVIS_BOLD, TVIS_BOLD );
	RefreshGroups();

	// Subjects
//	szMsg.Format( _T("Loading subjects from database in %s"), szPath );
//	Progress::SetProgress( ++nCur, szMsg );
	crs.Restore();
	szData.LoadString( IDS_SUBJS );
	m_hSub = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
	tree.SetItemData( m_hSub, TI_SUBJECT_FOLDER );
	tree.SetItemState( m_hSub, TVIS_BOLD, TVIS_BOLD );
	RefreshSubjects();

	// Conditions
//	szMsg.Format( _T("Loading conditions from database in %s"), szPath );
//	Progress::SetProgress( ++nCur, szMsg );
	crs.Restore();
	szData.LoadString( IDS_CONDS );
	m_hCond = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
	tree.SetItemData( m_hCond, TI_CONDITION_FOLDER );
	tree.SetItemState( m_hCond, TVIS_BOLD, TVIS_BOLD );
	RefreshConditions();

	if( ::IsOA() )
	{
		// Stimuli
//		szMsg.Format( _T("Loading stimuli from database in %s"), szPath );
//		Progress::SetProgress( ++nCur, szMsg );
		crs.Restore();
		szData = _T("Stimuli");
		m_hStim = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
		tree.SetItemData( m_hStim, TI_STIMULUS_FOLDER );
		tree.SetItemState( m_hStim, TVIS_BOLD, TVIS_BOLD );
		RefreshStimuli();

		// Elements
//		szMsg.Format( _T("Loading elements from database in %s"), szPath );
//		Progress::SetProgress( ++nCur, szMsg );
		crs.Restore();
		szData = _T("Elements");
		m_hElmt = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
		tree.SetItemData( m_hElmt, TI_ELEMENT_FOLDER );
		tree.SetItemState( m_hElmt, TVIS_BOLD, TVIS_BOLD );
		RefreshElements();

		// Categories
//		szMsg.Format( _T("Loading categories from database in %s"), szPath );
//		Progress::SetProgress( ++nCur, szMsg );
		crs.Restore();
		szData = _T("Categories");
		m_hCat = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
		tree.SetItemData( m_hCat, TI_CATEGORY_FOLDER );
		tree.SetItemState( m_hCat, TVIS_BOLD, TVIS_BOLD );
		RefreshCategories();
	}
	else
	{
		m_hStim = NULL;
		m_hElmt = NULL;
		m_hCat = NULL;
	}

	// Feedback
//	szMsg.Format( _T("Loading feedback from database in %s"), szPath );
//	Progress::SetProgress( ++nCur, szMsg );
	crs.Restore();
	szData = _T("Feedback");
	m_hFb = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
	tree.SetItemData( m_hFb, TI_FEEDBACK_FOLDER );
	tree.SetItemState( m_hFb, TVIS_BOLD, TVIS_BOLD );
	RefreshFeedbacks();

	// Quick Links
//	szMsg.Format( _T("Loading feedback from database in %s"), szPath );
//	Progress::SetProgress( ++nCur, szMsg );
	crs.Restore();
	szData = _T("Trial Quick Links");
	m_hQ = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, TVI_ROOT );
	tree.SetItemData( m_hQ, TI_QUICK_FOLDER );
	tree.SetItemState( m_hQ, TVIS_BOLD, TVIS_BOLD );
	RefreshQuickLinks();

	// Disable progress meter
//	Progress::DisableProgress();

	// Memory
	crs.Restore();
	ReadMemory();

	if( ::IsTabletModeOn() ) szInput = _T("TABLET");
	else if( ::IsGripperModeOn() ) szInput = _T("DAQPad");
	else szInput = _T("MOUSE");

	CBCGPRibbonStatusBarPane* pPane = (CBCGPRibbonStatusBarPane*)(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wndStatusBar.FindByID( IDM_O_INPUTDEVICE ));
	if( pPane )
	{
		pPane->SetText( szInput );
		pPane->Redraw();
	}
	CBCGPRibbonStatusBarPane* pStatBarDev = 
		(CBCGPRibbonStatusBarPane*)(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wndStatusBar.FindByID( IDM_WIZ_DEVSETUP ) );
	szInput.MakeUpper();
	if( pStatBarDev )
	{
		pStatBarDev->SetText( szInput );
		pStatBarDev->Redraw();
	}

	tree.SelectItem( m_hExp );

	// verify images and sounds if not already
	CString szVal, szDef, szKey, szRB, szID;
	::GetCurrentUser( szID );
	szVal.Format( _T("Settings\\%s"), szID );
	szRB = theApp.GetRegistryBase();
	theApp.SetRegistryBase( szVal );
	// verify element image paths
	int nVal = theApp.GetInt( _T("VerifiedImages"), 0 );
	if( ( nVal == 0 ) && NSMUsers::VerifyElementImages() )
		theApp.WriteInt( _T("VerifiedImages"), 1 );
	// verify condition sound paths
	nVal = theApp.GetInt( _T("VerifiedSounds"), 0 );
	if( ( nVal == 0 ) && NSMUsers::VerifyConditionSounds() )
		theApp.WriteInt( _T("VerifiedSounds"), 1 );
	theApp.SetRegistryBase( szRB );

	// if demo, open msgbox (once) to inform user how long they have left
	static BOOL fGaveMsg = FALSE;
#ifndef	_DEBUG
	if( !fGaveMsg )
	{
		int nDays = 0;
		long nCnt = 0;
		if( ::IsD( &nDays ) && !::IsSA( FALSE ) && !::IsOA( FALSE ) && !::IsGA( FALSE ) &&
			!::IsSPPU( nCnt ) && !::IsGPPU( nCnt ) && !::IsOPPU( nCnt ) )
		{
			CString szMsg;
			szMsg.Format( _T("You have %d days left in the trial period.\nWould you like to purchase a license?"), nDays );
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnHelpActivate();
			}
			fGaveMsg = TRUE;
		}

		// warn if UU1 user that any changes made will be overwritten with updates to software
		// if they want to customize any of the sample experiments, they should export it
		// and import it into a new/existing user other than UU1
		CString szID;
		::GetCurrentUser( szID );
		if( szID.Left( 2 ) == _T("UU") )
		{
			CString szMsg = _T("IMPORTANT: You are loading a NeuroScript example user.\n");
			szMsg += _T("Any changes made to the NeuroScript example users\n");
			szMsg += _T("will be lost with program updates.\n\n");
			szMsg += _T("If you choose to customize any of the example experiments and\n");
			szMsg += _T("retain these changes for the future, you should first EXPORT that\n");
			szMsg += _T("experiment and then IMPORT it into a non-NeuroScript user.");
			BCGPMessageBox( szMsg, MB_ICONEXCLAMATION );
		}

		// if device setup wizard has not been run, inform user
		if( !::WasDeviceSetupRun() )
		{
			CString szMsg;
			szMsg = _T("You have not setup your input device.\n\nRun the Input Device Setup Wizard?");
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
				if( pMF ) pMF->OnWizDevSetup();
			}
		}
	}
#endif
}

/////////////////////////////////////////////////////////////////////////////
// Show Trials (for specified exp, grp, subj)
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    ShowTrial
// FullName:  CLeftView::ShowTrial
// Access:    public 
// Returns:   HTREEITEM
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szSubj
// Parameter: CString szGrp
// Parameter: CString szCond
// Parameter: CString szTrial
// Parameter: HTREEITEM hRoot
// Parameter: CString szName
// Parameter: BOOL fValid
// Parameter: BOOL fInit
// Parameter: BOOL fNothing
// Parameter: BOOL fPCX
//************************************
HTREEITEM CLeftView::ShowTrial( CString szExp, CString szSubj, CString szGrp,
								CString szCond, CString szTrial, HTREEITEM hRoot,
								CString szName, BOOL fValid, BOOL fInit, BOOL fNothing,
								BOOL fPCX )
{
	CTreeCtrl&  tree = GetTreeCtrl();
	HTREEITEM hTrials;

	BOOL fGripper = FALSE;
	if( ( szName.GetLength() >= 3 ) && ( szName.Right( 3 ) == _T("FRD") ) ) fGripper = TRUE;

	// Extract trial # and ensure <= 99 (we can do this by ensuring the file name
	// is exactly 3(exp)+3(grp)+3(subj)+3(cond)+2(trial)+.ext chars long)
	// NOTE: this will assume we will always use a 3 character extension
	if( ( szName != _T("") ) && ( szName.GetLength() != 18 ) ) return NULL;

	if( ( szName != _T("") ) && tree.ItemHasChildren( hRoot ) )
	{
		HTREEITEM hTmp = tree.GetChildItem( hRoot );
		if( szName.Right( 3 ) == _T("HWR") )
		{
			if( tree.GetItemData( hTmp ) != TI_HWRTRIAL_FOLDER )
				hTmp = tree.GetNextSiblingItem( hTmp );
		}
		else if( szName.Right( 3 ) == _T("PCX") )
		{
			if( tree.GetItemData( hTmp ) != TI_IMAGE_FOLDER )
				hTmp = tree.GetNextSiblingItem( hTmp );
		}
		else if( szName.Right( 3 ) == _T("FRD") )
		{
			if( tree.GetItemData( hTmp ) != TI_FRDTRIAL_FOLDER )
				hTmp = tree.GetNextSiblingItem( hTmp );
		}
		hTrials = hTmp;
	}
	else
	{
		if( tree.ItemHasChildren( hRoot ) )
		{
			HTREEITEM hTmp = tree.GetChildItem( hRoot );
			if( fGripper )
			{
				if( tree.GetItemData( hTmp ) != TI_FRDTRIAL_FOLDER )
					hTmp = tree.GetNextSiblingItem( hTmp );
			}
			else
			{
				if( !fPCX )
				{
					if( tree.GetItemData( hTmp ) != TI_HWRTRIAL_FOLDER )
						hTmp = tree.GetNextSiblingItem( hTmp );
				}
				else
				{
					if( tree.GetItemData( hTmp ) != TI_IMAGE_FOLDER )
						hTmp = tree.GetNextSiblingItem( hTmp );
				}
			}
			hTrials = hTmp;
		}
		else hTrials = hRoot;
		if( hTrials == hRoot )
		{
			CString szData;
			DWORD dwData;

			if( fGripper )
			{
				szData = _T("Gripper Trials");
				dwData = TI_FRDTRIAL_FOLDER;
			}
			else
			{
				if( !fPCX )
				{
					szData = _T("Handwriting Trials");
					dwData = TI_HWRTRIAL_FOLDER;
				}
				else
				{
					szData = _T("Handwriting Images");
					dwData = TI_IMAGE_FOLDER;
				}
			}
			hTrials = tree.InsertItem( szData, IMG_FOLDER_RED, IMG_FOLDER_RED_OPEN, hTrials );
			tree.SetItemData( hTrials, dwData );
			tree.SetItemState( hTrials, TVIS_BOLD, TVIS_BOLD );
		}
	}

	CString szFind;
	if( szName == _T("") )
	{
		if( ::IsGripperModeOn() )
			::GetFRD( szExp, szGrp, szSubj, szCond, szTrial, szFind );
		else
			if( !fPCX ) ::GetHWR( szExp, szGrp, szSubj, szCond, szTrial, szFind );
			else ::GetPCX( szExp, szGrp, szSubj, szCond, szTrial, szFind );
		int nPos = szFind.ReverseFind( '\\' );
		szName = szFind.Mid( nPos + 1 );
	}
	else
	{
		if( szName.Right( 3 ) == _T("FRD") )
			GetFRD( szExp, szGrp, szSubj, szName, szFind );
		else
			if( !fPCX ) ::GetHWR( szExp, szGrp, szSubj, szName, szFind );
			else ::GetPCX( szExp, szGrp, szSubj, szName, szFind );
	}

	// TF File
	CString szTF = szFind.Left( szFind.GetLength() - 3 );
	szTF += _T("TF");

	// DIS File
	CString szDis = szFind.Left( szFind.GetLength() - 3 );
	szDis += _T("DIS");
	CFileFind ff;
	BOOL fDisExists = ff.FindFile( szDis );

	int nImage = fDisExists ? IMG_TRIAL_D : IMG_TRIAL;
	if( fPCX )
	{
		nImage = IMG_IMAGE;
	}
	else if( !fInit && !fNothing && ff.FindFile( szTF ) )
	{
		if( fValid ) nImage = fDisExists ? IMG_TRIAL_GOOD_D : IMG_TRIAL_GOOD;
		else nImage = fDisExists ? IMG_TRIAL_BAD_D : IMG_TRIAL_BAD;
	}
	else if( fInit && szName != _T("") )
	{
		if( ::HasTrialPassed( szFind ) )
		{
			nImage = fDisExists ? IMG_TRIAL_GOOD_D : IMG_TRIAL_GOOD;
		}
		else
		{
			// check if record-only
			BOOL fRecordOnly = FALSE;
			{
				// extract condition if necessary
				if( szCond == _T("") ) szCond = szName.Mid( 9, 3 );
				// check record/process
				NSConditions cond;
				CString szPath;
				::GetDataPathRoot( szPath );
				cond.SetDataPath( szPath );
				if( SUCCEEDED( cond.Find( szCond ) ) )
				{
					BOOL fRecord = FALSE, fProcess = FALSE;
					cond.GetRecord( fRecord );
					cond.GetProcess( fProcess );
					if( fRecord && !fProcess ) fRecordOnly = TRUE;
				}
			}
			if( fRecordOnly ) nImage = fDisExists ? IMG_TRIAL_BLUE_D : IMG_TRIAL_BLUE;
			else
			{
				// if TF exists, then we know it's bad, otherwise nothing
				if( ff.FindFile( szTF ) ) nImage = fDisExists ? IMG_TRIAL_BAD_D : IMG_TRIAL_BAD;
			}
		}
	}
	else if( fNothing ) nImage = fDisExists ? IMG_TRIAL_D : IMG_TRIAL;

	// If already there, delete it before reinserting
	HTREEITEM hItem = NULL;
	if( !fInit )
	{
		hItem = Find( hTrials, szName );
		if( hItem ) tree.DeleteItem( hItem );
	}
	hItem = tree.InsertItem( szName, nImage, nImage, hTrials );
	if( !fPCX ) tree.SetItemData( hItem, (DWORD)TI_TRIAL );
	else tree.SetItemData( hItem, (DWORD)TI_IMAGE );
	if( !fInit )
	{
		tree.Expand( hTrials, TVE_EXPAND );
		tree.SelectItem( hItem );
	}

	return hItem;
}

//************************************
// Method:    EnsureTrialVisible
// FullName:  CLeftView::EnsureTrialVisible
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: HTREEITEM hItem
// Parameter: BOOL fParent
//************************************
void CLeftView::EnsureTrialVisible( HTREEITEM hItem, BOOL fParent )
{
	if( !hItem ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	if( fParent )
	{
		hItem = tree.GetParentItem( hItem );
		if( !hItem ) return;
	}
	tree.EnsureVisible( hItem );
}

//************************************
// Method:    ShowTrials
// FullName:  CLeftView::ShowTrials
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szSubj
// Parameter: CString szGrp
// Parameter: HTREEITEM hRoot
// Parameter: BOOL fShow
// Parameter: BOOL fForce
// Parameter: BOOL fJustVerify
//************************************
BOOL CLeftView::ShowTrials( CString szExp, CString szSubj, CString szGrp,
							HTREEITEM hRoot, BOOL fShow, BOOL fForce, BOOL fJustVerify )
{
	CTreeCtrl& tree = GetTreeCtrl();

	if( tree.ItemHasChildren( hRoot ) )
	{
		if( !fForce ) return FALSE;

		HTREEITEM hChild = tree.GetChildItem( hRoot );
		while( hChild )
		{
			HTREEITEM hChildNext = tree.GetNextSiblingItem( hChild );
			tree.DeleteItem( hChild );
			hChild = hChildNext;
		}
	}

	// Experiment Type
	Experiments exp;
	if( !exp.IsValid() ) return FALSE;
	if( FAILED( exp.Find( szExp ) ) ) return FALSE;
	short nType = 0;	// EXP_TYPE_HANDWRITING (default)
	exp.GetType( nType );

	// Locate any files
	CString szTemp, szHWR, szPCX, szTF, szFRD;
	GetHWRMask( szExp, szGrp, szSubj, szHWR );
	GetPCXMask( szExp, szGrp, szSubj, szPCX );
	GetFRDMask( szExp, szGrp, szSubj, szFRD );

	// Sort accordingly
	CStringList lstTrialsHWR, lstTrialsPCX, lstTrialsFRD;
	if( ::IsTrialAlphaOn() )
	{
		::SortFilesAlpha( &lstTrialsHWR, szHWR );
		::SortFilesAlpha( &lstTrialsPCX, szPCX );
		::SortFilesAlpha( &lstTrialsFRD, szFRD );
	}
	else
	{
		::SortFilesChrono( &lstTrialsHWR, szHWR );
		::SortFilesChrono( &lstTrialsPCX, szPCX );
		::SortFilesChrono( &lstTrialsFRD, szFRD );
	}

	POSITION pos = NULL;
	HTREEITEM hItem = NULL;
	// DISPLAY HWR (if exp is handwriting)
	if( nType == EXP_TYPE_HANDWRITING )
	{
		pos = lstTrialsHWR.GetHeadPosition();
		// Create folder
		if( pos )
		{
			hItem = tree.InsertItem( _T("Handwriting Trials"), IMG_FOLDER_RED, IMG_FOLDER_RED_OPEN, hRoot );
			tree.SetItemData( hItem, (DWORD)TI_HWRTRIAL_FOLDER );
			tree.SetItemState( hItem, TVIS_BOLD, TVIS_BOLD );
		}
		// Trial by trial
		while( !fJustVerify && pos )
		{
			szTemp = lstTrialsHWR.GetNext( pos );
			szTemp.MakeUpper();

			ShowTrial( szExp, szSubj, szGrp, _T(""), _T(""), hRoot, szTemp, TRUE, TRUE );
		}
	}
	// DISPLAY PCX (if exp is image)
	else if( nType == EXP_TYPE_IMAGE )
	{
		pos = lstTrialsPCX.GetHeadPosition();
		// Create folder
		if( pos )
		{
			hItem = tree.InsertItem( _T("Handwriting Images"), IMG_FOLDER_RED, IMG_FOLDER_RED_OPEN, hRoot );
			tree.SetItemData( hItem, (DWORD)TI_IMAGE_FOLDER );
			tree.SetItemState( hItem, TVIS_BOLD, TVIS_BOLD );
		}
		// Trial by trial
		while( !fJustVerify && pos )
		{
			szTemp = lstTrialsPCX.GetNext( pos );
			szTemp.MakeUpper();

			ShowTrial( szExp, szSubj, szGrp, _T(""), _T(""), hRoot, szTemp, TRUE, TRUE, TRUE, TRUE );
		}
	}
	// DISPLAY FRD (if exp is gripper)
	else if( nType == EXP_TYPE_GRIPPER )
	{
		pos = lstTrialsFRD.GetHeadPosition();
		// Create folder
		hItem = NULL;
		if( pos )
		{
			hItem = tree.InsertItem( _T("Gripper Trials"), IMG_FOLDER_RED, IMG_FOLDER_RED_OPEN, hRoot );
			tree.SetItemData( hItem, (DWORD)TI_FRDTRIAL_FOLDER );
			tree.SetItemState( hItem, TVIS_BOLD, TVIS_BOLD );
		}
		// Trial by trial
		while( !fJustVerify && pos )
		{
			szTemp = lstTrialsFRD.GetNext( pos );
			szTemp.MakeUpper();

			ShowTrial( szExp, szSubj, szGrp, _T(""), _T(""), hRoot, szTemp, TRUE, TRUE );
		}
	}

	if( fJustVerify && hItem )
	{
		tree.Expand( hRoot, TVE_COLLAPSE );
		return TRUE;
	}
	else if( fJustVerify ) return FALSE;
	else if( fShow && hItem ) tree.SelectItem( hItem );

	tree.Expand( hRoot, TVE_EXPAND );

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CLeftView message handlers

/////////////////////////////////////////////////////////////////////////////
// Experiment Folder: Create New Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnEfAdd
// FullName:  CLeftView::OnEfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEfAdd() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	Experiments exp;
	if( !exp.IsValid() ) return;

	CString szData;
	szData.LoadString( IDS_AL_EXPADD );
	OutputAMessage( szData );

	CWaitCursor crs;
	if( exp.DoAdd() )
	{
		::DataHasChanged();

		CString szItem;
		exp.GetDescriptionWithID( szItem );

		short nType = EXP_TYPE_HANDWRITING;
		exp.GetType( nType );

		int nImage = IMG_EXPERIMENT;
		if( nType == EXP_TYPE_IMAGE ) nImage = IMG_EXPERIMENT_IMG;
		else if( nType == EXP_TYPE_GRIPPER ) nImage = IMG_EXPERIMENT_GRIP;

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, nImage, nImage, m_hExp );
		tree.SetItemData( hItem, TI_EXPERIMENT );

		exp.GetDescription( szItem );
		szData.Format( IDS_AL_EXPADDED, szItem );
		OutputAMessage( szData, TRUE );

		szData.Format( IDS_EXPGROUPS, szItem );
		HTREEITEM hItemGrps = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, hItem );
		tree.SetItemData( hItemGrps, TI_GROUP_FOLDER );
		tree.SetItemState( hItemGrps, TVIS_BOLD, TVIS_BOLD );

		szData.Format( IDS_EXPCONDS, szItem );
		HTREEITEM hItemConds = tree.InsertItem( szData, IMG_FOLDER, IMG_FOLDER_OPEN, hItem );
		tree.SetItemData( hItemConds, TI_CONDITION_FOLDER );
		tree.SetItemState( hItemConds, TVIS_BOLD, TVIS_BOLD );

		tree.EnsureVisible( hItemConds );
		tree.EnsureVisible( hItem );
		tree.SelectItem( hItem );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Experiment: Properties
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnEEdit2
// FullName:  CLeftView::OnEEdit2
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEEdit2()
{
	CString szData;
	szData.LoadString( IDS_AL_EXPEDIT );
	OutputAMessage( szData );

	OnEEdit();
}

//************************************
// Method:    OnEEdit
// FullName:  CLeftView::OnEEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEEdit() 
{
	Experiments exp;
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// original type
	short nOrigType = EXP_TYPE_HANDWRITING;
	if( SUCCEEDED( exp.Find( szExp ) ) )
		exp.GetType( nOrigType );

	CString szData;
	szData.Format( IDS_AL_EXPEDIT2, szExp );
	OutputAMessage( szData );

	CWaitCursor crs;
	if( exp.DoEdit( szExp, this, 0 ) )
	{
		::DataHasChanged();

		exp.Find( szExp );

		exp.GetDescriptionWithID( szItem );
		tree.SetItemText( tree.GetSelectedItem(), szItem );

		szData.Format( IDS_AL_EXPEDITED, szExp );
		OutputAMessage( szData );

		short nType = EXP_TYPE_HANDWRITING;
		exp.GetType( nType );
		if( nType != nOrigType ) RefreshExperiments();
	}
}

/////////////////////////////////////////////////////////////////////////////
// Experiment: Delete
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnEDelete
// FullName:  CLeftView::OnEDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEDelete() 
{
	CString szData;
	szData.LoadString( IDS_AL_EXPDEL );
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	// If any subject or conditions, tell them to remove first
	HTREEITEM hItem = tree.GetSelectedItem();
	if( tree.ItemHasChildren( hItem ) )
	{
		BOOL fNo = FALSE;
		HTREEITEM hTemp = tree.GetChildItem( hItem );
		if( tree.ItemHasChildren( hTemp ) ) fNo = TRUE;

		hTemp = tree.GetNextItem( hTemp, TVGN_NEXT );
		if( tree.ItemHasChildren( hTemp ) ) fNo = TRUE;

		if( fNo )
		{
			BCGPMessageBox( IDS_EXPDEL_GRPS );
			return;
		}
	}

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	szData.Format( IDS_EXPDEL_SURE, szExp );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	Experiments exp;
	if( exp.IsValid() )
	{
		HRESULT hr = exp.Find( szExp );
		if( SUCCEEDED( hr ) )
		{
			hr = exp.Remove();

			CString szData;
			szData.Format( IDS_AL_EXPDELETED, szExp );
			OutputAMessage( szData );

			if( FAILED( hr ) )
			{
				BCGPMessageBox( IDS_EXPDEL_ERR );
				return;
			}
			::DataHasChanged();

			tree.DeleteItem( hItem );

			// Experiment Settings
			IProcSetting* pPS = NULL;
			HRESULT hrps = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
											 IID_IProcSetting, (LPVOID*)&pPS );
			if( SUCCEEDED( hrps ) )
			{
				hrps = pPS->Find( szExp.AllocSysString() );
				if( SUCCEEDED( hrps ) )
				{
					hrps = pPS->Remove();
					if( FAILED( hrps ) )
						BCGPMessageBox( _T("Unable to delete experiment process settings.") );
				}

				pPS->Release();
			}

			// Remove data files & directories
			CString szExpDir, szFile, szFile2;
			::GetDataPathRoot( szExpDir );
			szExpDir += _T("\\");
			szExpDir += szExp;
			szData.Format( _T("%s\\*.*"), szExpDir );
			CFileFind ff, ff2, ff3;
			BOOL fCont, fCont2, fCont3;
			fCont = ff.FindFile( szData );
			if( !fCont ) return;
			OutputAMessage( _T("Removing data and directories for selected experiment.") );
			CWaitCursor crs;
			// Groups
			while( fCont )
			{
				fCont = ff.FindNextFile();
				szFile = ff.GetFileName();
				if( ff.IsDirectory() && ( szFile != _T(".") ) && ( szFile != _T("..") ) )
				{
					szFile = ff.GetFilePath();
					szData.Format( _T("%s\\*."), szFile );
					// Subjects
					fCont2 = ff2.FindFile( szData );
					while( fCont2 )
					{
						fCont2 = ff2.FindNextFile();
						szFile2 = ff2.GetFileName();
						if( ff2.IsDirectory() && ( szFile2 != _T(".") ) && ( szFile2 != _T("..") ) )
						{
							szFile2 = ff2.GetFilePath();
							szData.Format( _T("%s\\*.*"), szFile2 );
							// Trials
							fCont3 = ff3.FindFile( szData );
							while( fCont3 )
							{
								fCont3 = ff3.FindNextFile();
								szData = ff3.GetFilePath();
								if( !ff3.IsDirectory() ) ::SendToRecycleBin( szData );
							}
							// Delete subject directory
							_rmdir( szFile2 );
						}
					}
					// Delete group directory
					_rmdir( szFile );
				}
				else if( ( szFile != _T(".") ) && ( szFile != _T("..") ) )
				{
					szFile = ff.GetFilePath();
					::SendToRecycleBin( szFile );
				}
			}
			// Delete experiment directory
			_rmdir( szExpDir );
			OutputAMessage( _T("Experiment files have been sent to the Recycle Bin."), TRUE );
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// Experiment: Add Groups To Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnEAddgroup
// FullName:  CLeftView::OnEAddgroup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAddgroup() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem, hItemGrps, hGrp;
	DWORD dwType;
	CString szExp, szGrp, szSubj = HOLDER;
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	if( !m_fDragging )
	{
		hItem = tree.GetSelectedItem();
		if( tree.GetItemData( hItem ) == TI_GROUP_FOLDER )
			szExp = tree.GetItemText( tree.GetParentItem( hItem ) );
		else
			szExp = tree.GetItemText( hItem );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}
	else
	{
		hItem = m_hitemDrop;
		szExp = _DropID;
	}
	Experiments exp;
	if( FAILED( exp.Find( szExp.AllocSysString() ) ) ) return;

	hItemGrps = hItem;
	dwType = tree.GetItemData( hItem );
	if( ( dwType != TI_GROUP_FOLDER ) && !m_fDragging )
	{
		CString szData;
		szData.LoadString( IDS_AL_EGRPADD );
		OutputAMessage( szData );
	}
	if( dwType == TI_GROUP_FOLDER )
		hItem = tree.GetParentItem( hItem );

	// Existing groups
	CStringList exist;

	if( dwType == TI_EXPERIMENT )
	{
		hItemGrps = tree.GetNextItem( hItemGrps, TVGN_CHILD );	// Group folder ?
		if( hItemGrps ) dwType = tree.GetItemData( hItemGrps );
		else dwType = TI_UNKNOWN;

		if( dwType == TI_CONDITION_FOLDER )
		{
			hItemGrps = tree.GetNextItem( hItemGrps, TVGN_NEXT );
			if( hItemGrps ) dwType = tree.GetItemData( hItemGrps );
			else dwType = TI_UNKNOWN;
		}

		if( dwType == TI_UNKNOWN ) hItemGrps = NULL;
	}
	if( dwType == TI_GROUP_FOLDER )
	{
		hGrp = tree.GetNextItem( hItemGrps, TVGN_CHILD );
		while( hGrp )
		{
			CString szGrp = tree.GetItemText( hGrp );
			int nPos = szGrp.Find( szDelim );
			if( nPos != -1 )
			{
				szGrp = szGrp.Mid( nPos + 2 );
				exist.AddTail( szGrp );
			}
			hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );
		}
	}

	IExperimentMember* pEM = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}

	AddGroupDlg d( &exist, this );
	int nRet = m_fDragging ? IDOK : d.DoModal();
	if( d.m_fChanged ) RefreshGroups();
	if( nRet == IDOK )
	{
		CString szItem;
		int nPos;

		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( !m_fDragging && !pos ) return;

		if( !m_fDragging )
		{
			while( pos )
			{
				szItem = d.m_lstNew.GetNext( pos );
				szGrp = szItem.Mid( szItem.Find( szDelim ) + 2 );
				hGrp = tree.InsertItem( szItem, IMG_GROUP, IMG_GROUP, hItemGrps );
				tree.SetItemData( hGrp, TI_GROUP );

				pEM->put_ExperimentID( szExp.AllocSysString() );
				pEM->put_GroupID( szGrp.AllocSysString() );
				pEM->put_SubjectID( szSubj.AllocSysString() );
				pEM->Add();

				CString szData;
				szData.Format( IDS_AL_EGRPADDING, szItem, tree.GetItemText( hItem ) );
				OutputAMessage( szData, TRUE );

				szItem.Format( IDS_EXPSUBJS, szExp,
							   szItem.Mid( szItem.ReverseFind( szDelim[ 0 ] ) + 2 ) );
				HTREEITEM hItemSubjs = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hGrp );
				tree.SetItemData( hItemSubjs, TI_SUBJECT_FOLDER );
				tree.SetItemState( hItemSubjs, TVIS_BOLD, TVIS_BOLD );

				tree.EnsureVisible( hItemSubjs );
			}
		}
		else
		{
			nPos = _DragID.ReverseFind( szDelim[ 0 ] );
			szGrp = _DragID.Mid( nPos + 2 );

			// Check if in list
			HTREEITEM hItemTemp = tree.GetChildItem( hItemGrps );
			CString szTmp;
			while( hItemTemp )
			{
				szTmp = tree.GetItemText( hItemTemp );
				nPos = szTmp.Find( szDelim );
				if( nPos == -1 ) return;
				szTmp = szTmp.Mid( nPos + 2 );

				if( szTmp == szGrp )
				{
					BCGPMessageBox( _T("This group already exists in the experiment.") );
					return;
				}

				hItemTemp = tree.GetNextSiblingItem( hItemTemp );
			}

			pEM->put_ExperimentID( szExp.AllocSysString() );
			pEM->put_GroupID( szGrp.AllocSysString() );
			pEM->put_SubjectID( szSubj.AllocSysString() );

			pEM->Add();

			if( SUCCEEDED( hr ) )
			{
				::DataHasChanged();

				hGrp = tree.InsertItem( _DragID, IMG_GROUP, IMG_GROUP, hItemGrps );
				tree.SetItemData( hGrp, TI_GROUP );

				CString szData;
				szData.Format( IDS_AL_EGRPADDING, _DragID, tree.GetItemText( hItem ) );
				OutputAMessage( szData, TRUE );

				szItem.Format( IDS_EXPSUBJS, szExp,
							   _DragID.Mid( _DragID.ReverseFind( szDelim[ 0 ] ) + 2 ) );
				HTREEITEM hItemSubjs = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hGrp );
				tree.SetItemData( hItemSubjs, TI_SUBJECT_FOLDER );
				tree.SetItemState( hItemSubjs, TVIS_BOLD, TVIS_BOLD );

				tree.EnsureVisible( hItemSubjs );
			}
		}
	}
	pEM->Release();
}

//************************************
// Method:    OnGfAddgroups
// FullName:  CLeftView::OnGfAddgroups
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGfAddgroups() 
{
	CString szData = _T("SRC: Group Folder - Add Group");
	OutputAMessage( szData );

	OnEAddgroup();
}

/////////////////////////////////////////////////////////////////////////////
// Group Folder: Create New Group
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnGfAdd
// FullName:  CLeftView::OnGfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGfAdd() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	Groups grp;
	if( !grp.IsValid() ) return;

	CString szData;
	szData.LoadString( IDS_AL_GRPADD );
	OutputAMessage( szData );


	if( grp.DoAdd( this ) )
	{
		::DataHasChanged();

		CString szItem;
		grp.GetDescriptionWithID( szItem );

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_GROUP, IMG_GROUP, m_hGrp );
		tree.SetItemData( hItem, TI_GROUP );

		tree.EnsureVisible( hItem );
		tree.SelectItem( hItem );

		grp.GetID( szItem );
		szData.Format( IDS_AL_GRPADDED, szItem );
		OutputAMessage( szData, TRUE );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Generic Find Function For All Types
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    FindTrial
// FullName:  CLeftView::FindTrial
// Access:    public 
// Returns:   HTREEITEM
// Qualifier:
// Parameter: CString szTrial
// Parameter: CString szExp
//************************************
HTREEITEM CLeftView::FindTrial( CString szTrial, CString szExp )
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem, hRet = NULL, hGrp, hSubj, hNext;
	DWORD dwData;
	BOOL fFound = FALSE;
	CString szItem, szNext;
	int nPos = -1;

	// Find experiment
	hItem = Find( m_hExp, szExp );
	if( !hItem ) return NULL;

	// We want to go through each group
	hGrp = tree.GetNextItem( hItem, TVGN_CHILD );
	if( !hGrp ) return NULL;
	dwData = tree.GetItemData( hGrp );
	if( dwData != TI_GROUP_FOLDER ) hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );
	if( !hGrp ) return NULL;
	dwData = tree.GetItemData( hGrp );
	if( dwData != TI_GROUP_FOLDER ) return NULL;
	hGrp = tree.GetNextItem( hGrp, TVGN_CHILD );
	if( !hGrp ) return NULL;
	while( hGrp && !fFound )
	{
szItem = tree.GetItemText( hGrp );
		// We want to go through each subject
		hSubj = tree.GetNextItem( hGrp, TVGN_CHILD );
		if( !hSubj ) goto NextGrp;
		dwData = tree.GetItemData( hSubj );
		if( dwData != TI_SUBJECT_FOLDER ) goto NextGrp;
		hSubj = tree.GetNextItem( hSubj, TVGN_CHILD );
		if( !hSubj ) goto NextGrp;
		while( hSubj && !fFound )
		{
szItem = tree.GetItemText( hSubj );
			// Now through each trial
			hNext = tree.GetNextItem( hSubj, TVGN_CHILD );
			if( !hNext ) goto NextSubj;
			dwData = tree.GetItemData( hNext );
			if( ( dwData != TI_HWRTRIAL_FOLDER ) &&
				( dwData != TI_FRDTRIAL_FOLDER ) &&
				( dwData != TI_IMAGE_FOLDER ) )
				goto NextSubj;
			hNext = tree.GetNextItem( hNext, TVGN_CHILD );
			if( !hNext ) goto NextSubj;
			while( hNext && !fFound )
			{
				szItem = tree.GetItemText( hNext );
				szItem.MakeUpper();
				if( szItem == szTrial )
				{
					fFound = TRUE;
					hRet = hNext;
					break;
				}
				hNext = tree.GetNextItem( hNext, TVGN_NEXT );
			}

NextSubj:
			// Next subject
			hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );
		}

NextGrp:
		// Next group
		hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );
	}

	return hRet;
}

//************************************
// Method:    Find
// FullName:  CLeftView::Find
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szIDs
//************************************
void CLeftView::Find( CString szIDs )
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) hItem = m_hExp;
	HTREEITEM hNext = tree.GetNextItem( hItem, TVGN_CHILD );
	if( !hNext ) hNext = m_hExp;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	BOOL fFound = FALSE;
	CString szItem, szID, szDesc;
	int nPos = -1;

	while( TRUE )
	{
		szItem = tree.GetItemText( hNext );

		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szID = szItem.Mid( nPos + 2 );
			szDesc = szItem.Left( nPos - 1 );
			if( ( szID.CompareNoCase( szIDs ) == 0 ) || ( szDesc.CompareNoCase( szIDs ) == 0 ) )
			{
				fFound = TRUE;
				tree.SelectItem( hNext );
				break;
			}
		}

		hNext = tree.GetNextItem( hNext, TVGN_NEXT );
		if( !hNext ) break;
	}

	if( !fFound ) BCGPMessageBox( IDS_FIND_ERR );
}

/////////////////////////////////////////////////////////////////////////////
// Locate a particular item and return location
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    Find
// FullName:  CLeftView::Find
// Access:    public 
// Returns:   HTREEITEM
// Qualifier:
// Parameter: CString szExpID
// Parameter: CString szGrpID
// Parameter: CString szSubjID
//************************************
HTREEITEM CLeftView::Find( CString szExpID, CString szGrpID, CString szSubjID )
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = NULL;
	HTREEITEM hNext = tree.GetNextItem( m_hExp, TVGN_CHILD );

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	BOOL fFound = FALSE;
	CString szItem, szNext;
	int nPos = -1;
	DWORD dwData = 0;

	// Find experiment
	while( TRUE )
	{
		szItem = tree.GetItemText( hNext );

		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szNext = szItem.Mid( nPos + 2 );
			if( szNext == szExpID )
			{
				fFound = TRUE;
				break;
			}
		}

		hNext = tree.GetNextItem( hNext, TVGN_NEXT );
		if( !hNext ) break;
	}

	// Find group folder
	if( fFound ) hNext = tree.GetNextItem( hNext, TVGN_CHILD );
	while( fFound )
	{
		if( !hNext )
		{
			fFound = FALSE;
			break;
		}
		dwData = tree.GetItemData( hNext );
		if( dwData == TI_GROUP_FOLDER ) break;
		hNext = tree.GetNextItem( hNext, TVGN_NEXT );
	}

	// Find group
	if( fFound ) hNext = tree.GetNextItem( hNext, TVGN_CHILD );
	while( fFound )
	{
		if( !hNext )
		{
			fFound = FALSE;
			break;
		}
		szItem = tree.GetItemText( hNext );

		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szNext = szItem.Mid( nPos + 2 );
			if( szNext == szGrpID )
			{
				hItem = tree.GetNextItem( hNext, TVGN_CHILD );
				break;
			}
		}

		hNext = tree.GetNextItem( hNext, TVGN_NEXT );
	}

	if( !hItem ) return NULL;
	else return Find( hItem, szSubjID );
}

//************************************
// Method:    OnFind
// FullName:  CLeftView::OnFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFind()
{
	FindDlg d( _T("Find Item In Tree"), this );
	d.m_szID = m_szFindText;
	d.m_fExactPartial = TRUE;
	d.m_fPartial = m_fFindPartial;
	if( d.DoModal() == IDOK )
	{
		CTreeCtrl& tree = GetTreeCtrl();

		m_fFindPartial = d.m_fPartial;

		// we start from currently selected item. If null, root
		HTREEITEM hStart = tree.GetSelectedItem();
		if( !hStart ) hStart = tree.GetRootItem();
		if( !hStart ) return;

		CWaitCursor crs;

		_nFindCount = 0;
		m_hNext = FindItem( d.m_szID, hStart, m_fFindPartial, hStart ? TRUE : FALSE );
		if( m_hNext )
		{
			m_szFindText = d.m_szID;
			tree.SelectItem( m_hNext );
			tree.EnsureVisible( m_hNext );
		}
		else
		{
			if( hStart != tree.GetRootItem() )
			{
				CString szMsg = _T("Unable to find item. Would you like to search from the beginning?");
				if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
				{
					m_hNext = FindItem( d.m_szID, tree.GetRootItem(), m_fFindPartial, FALSE );
					if( m_hNext )
					{
						m_szFindText = d.m_szID;
						tree.SelectItem( m_hNext );
						tree.EnsureVisible( m_hNext );
					}
					else BCGPMessageBox( IDS_FIND_ERR );
				}
			}
			else BCGPMessageBox( IDS_FIND_ERR );
		}
	}
}

//************************************
// Method:    OnFindNext
// FullName:  CLeftView::OnFindNext
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFindNext()
{
	if( m_szFindText == _T("") ) return;

	CWaitCursor crs;

	// tree
	CTreeCtrl& tree = GetTreeCtrl();
	// although we track last item found, if someone clicks another tree item, we start from there
	if( m_hNext != tree.GetSelectedItem() ) m_hNext = tree.GetSelectedItem();
	// starting point
	HTREEITEM hStart = m_hNext;
	// if nothing selected nor previous, select root
	if( !m_hNext ) m_hNext = tree.GetRootItem();
	// search
	_nFindCount = 0;
	m_hNext = FindItem( m_szFindText, m_hNext, m_fFindPartial, TRUE );
	if( m_hNext )
	{
		tree.SelectItem( m_hNext );
		tree.EnsureVisible( m_hNext );
	}
	else
	{
		if( hStart != tree.GetRootItem() )
		{
			CString szMsg = _T("Unable to find item. Would you like to search from the beginning?");
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				m_hNext = FindItem( m_szFindText, tree.GetRootItem(), m_fFindPartial, FALSE );
				if( m_hNext )
				{
					tree.SelectItem( m_hNext );
					tree.EnsureVisible( m_hNext );
				}
				else BCGPMessageBox( IDS_FIND_ERR );
			}
		}
		else BCGPMessageBox( _T("No other items could be found.") );
	}
}

//************************************
// Method:    OnUpdateFind
// FullName:  CLeftView::OnUpdateFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFind( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = TRUE;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateFindNext
// FullName:  CLeftView::OnUpdateFindNext
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFindNext( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = TRUE;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= (BOOL)( m_szFindText != _T("") );
	fEnable &= ( ( m_hNext != NULL ) || ( tree.GetSelectedItem() != NULL ) );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    FindItem
// FullName:  CLeftView::FindItem
// Access:    public 
// Returns:   HTREEITEM
// Qualifier:
// Parameter: CString szItem
// Parameter: HTREEITEM hStart
// Parameter: BOOL fPartial
// Parameter: BOOL fSkipCheck
// Parameter: BOOL fSkipChildren
//************************************
HTREEITEM CLeftView::FindItem( CString szItem, HTREEITEM hStart, BOOL fPartial,
							   BOOL fSkipCheck, BOOL fSkipChildren )
{
	if( !hStart ) return NULL;

	// tree
	CTreeCtrl& tree = GetTreeCtrl();

	// only do if we're not skipping
	if( !fSkipCheck )
	{
		_nFindCount++;
		// this item's text
		CString szText = tree.GetItemText( hStart );
		// compare
		CString szDelim = ::GetDelimiter();
		int nPos = szText.Find( szDelim );
		if( nPos != -1 )	// item with ID & desc
		{
			CString szDesc = szText.Left( nPos - 1 );
			CString szID = szText.Mid( nPos + 2 );
			// partial search
			if( fPartial )
			{
				if( ( szID.MakeUpper().Find( szItem.MakeUpper() ) != -1 ) ||
					( szDesc.MakeUpper().Find( szItem.MakeUpper() ) != -1 ) )
					return hStart;
			}
			// exact search (except case)
			else
			{
				if( ( szID.CompareNoCase( szItem ) == 0 ) ||
					( szDesc.CompareNoCase( szItem ) == 0 ) )
					return hStart;
			}
		}
		else				// trial/pcx
		{
			// partial search
			if( fPartial )
			{
				CString szTemp = szItem;
				szTemp.MakeUpper();
				szText.MakeUpper();
				if( szText.Find( szTemp ) != -1 ) return hStart;
			}
			// exact search (except case)
			else if( !fPartial && ( szText.CompareNoCase( szItem ) == 0 ) ) return hStart;
		}
	}

	// search children
	HTREEITEM hItem = fSkipChildren ? NULL : FindItem( szItem, tree.GetChildItem( hStart ), fPartial );
	// search siblings
	if( !hItem ) hItem = FindItem( szItem, tree.GetNextItem( hStart, TVGN_NEXT ), fPartial );
	// start search from parent's next sibling if applicable
	int nCount = tree.GetCount();
	if( !hItem && ( _nFindCount < nCount ) ) hItem = FindItem( szItem, tree.GetParentItem( hStart ), fPartial, TRUE, TRUE );

	return hItem;
}

//************************************
// Method:    Find
// FullName:  CLeftView::Find
// Access:    public 
// Returns:   HTREEITEM
// Qualifier:
// Parameter: HTREEITEM hRoot
// Parameter: CString szIDs
//************************************
HTREEITEM CLeftView::Find( HTREEITEM hRoot, CString szIDs )
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hNext = tree.GetNextItem( hRoot, TVGN_CHILD );
	if( !hNext ) return NULL;

	HTREEITEM hItem = NULL;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	BOOL fFound = FALSE;
	CString szItem, szID, szDesc;
	int nPos = -1;

	while( TRUE )
	{
		szItem = tree.GetItemText( hNext );

		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szID = szItem.Mid( nPos + 2 );
			szDesc = szItem.Left( nPos - 1 );
		}
		// else likely a trial (with no ID nor delimiter)
		else szID = szItem;
		// is this the one?
		if( ( szID.CompareNoCase( szIDs ) == 0 ) || ( szDesc.CompareNoCase( szIDs ) == 0 ) )
		{
			fFound = TRUE;
			hItem = hNext;
			break;
		}

		hNext = tree.GetNextItem( hNext, TVGN_NEXT );
		if( !hNext ) break;
	}

	return hItem;
}

//************************************
// Method:    SelectItem
// FullName:  CLeftView::SelectItem
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: HTREEITEM hItem
//************************************
void CLeftView::SelectItem( HTREEITEM hItem )
{
	CTreeCtrl& tree = GetTreeCtrl();
	if( hItem ) tree.SelectItem( hItem );
}

/////////////////////////////////////////////////////////////////////////////
// Experiment Folder: Find Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnEfFind
// FullName:  CLeftView::OnEfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEfFind() 
{
	CString szData;
	szData.LoadString( IDS_AL_EXPFIND );
	OutputAMessage( szData );

	FindDlg d( IDS_EXP_FIND, this );
	if( d.DoModal() == IDOK ) Find( d.m_szID );
}

/////////////////////////////////////////////////////////////////////////////
// Group Folder: Find Group
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnGfFind
// FullName:  CLeftView::OnGfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGfFind() 
{
	CString szData;
	szData.LoadString( IDS_AL_GRPFIND );
	OutputAMessage( szData );

	FindDlg d( IDS_GRP_FIND, this );
	if( d.DoModal() == IDOK )
		Find( d.m_szID );
}

/////////////////////////////////////////////////////////////////////////////
// Group: Properties
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnGEdit2
// FullName:  CLeftView::OnGEdit2
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGEdit2()
{
	CString szData;
	szData.LoadString( IDS_AL_GRPEDIT );
	OutputAMessage( szData );

	OnGEdit();
}

//************************************
// Method:    OnGEdit
// FullName:  CLeftView::OnGEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGEdit() 
{
	Groups grp;
	if( !grp.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which group
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// Which experiment (if applicable)
	HTREEITEM hExp = tree.GetParentItem( tree.GetSelectedItem() );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	CString szExp;
	if( nPos != -1 ) szExp = szItem.Mid( nPos + 2 );

	CString szData;
	szData.Format( IDS_AL_GRPEDIT2, szGrp );
	OutputAMessage( szData );

	// Original text
	CString szOld = tree.GetItemText( tree.GetSelectedItem() );

	// do edit
	grp.m_szExpID = szExp;
	if( grp.DoEdit( szGrp, this ) )
	{
		::DataHasChanged();

		CString szItem;
		grp.GetDescriptionWithID( szItem );

//		tree.SetItemText( tree.GetSelectedItem(), szItem );
		ChangeAllInstanceNames( TI_GROUP, szOld, szItem );

		szData.Format( IDS_AL_GRPEDITED, szGrp );
		OutputAMessage( szData );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Group: Delete
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnGDelete
// FullName:  CLeftView::OnGDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGDelete() 
{
	// groups object
	Groups grp;
	if( !grp.IsValid() ) return;

	// inform of action
	CString szData;
	szData.LoadString( IDS_AL_GRPDEL );
	OutputAMessage( szData );

	// get tree item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();

	// if group has children (subjects), inform & return
	if( tree.ItemHasChildren( hItem ) )
	{
		BCGPMessageBox( IDS_GRPDEL_SUBJS );
		return;
	}

	// get group id
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// warning
	szData.Format( IDS_GRPDEL_SURE, szGrp );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	// associations?
	BOOL fAssoc = FALSE;
	// Now find in an experiment if can & remove
	HTREEITEM hExp = tree.GetNextItem( m_hExp, TVGN_CHILD );	// 1st experiment
	while( hExp && !fAssoc )
	{
		CString szExp = tree.GetItemText( hExp );
		nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );

		HTREEITEM hGrp = tree.GetNextItem( hExp, TVGN_CHILD );	// Group folder
		DWORD dwStyle = TI_UNKNOWN;
		if( hGrp ) dwStyle = tree.GetItemData( hGrp );
		if( ( dwStyle != TI_UNKNOWN ) && ( dwStyle != TI_GROUP_FOLDER ) )
		{
			hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );	// Retry
			if( hGrp ) dwStyle = tree.GetItemData( hGrp );
		}

		if( dwStyle == TI_GROUP_FOLDER )
		{
			hGrp = tree.GetNextItem( hGrp, TVGN_CHILD );	// 1st group
			while( hGrp && !fAssoc )
			{
				CString szGrp2 = tree.GetItemText( hGrp );
				nPos = szGrp2.Find( szDelim );
				szGrp2 = szGrp2.Mid( nPos + 2 );

				if( szGrp == szGrp2 )
				{
					fAssoc = TRUE;
					break;
				}

				hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );	// next group
			}
		}

		hExp = tree.GetNextItem( hExp, TVGN_NEXT );	// Next experiment
	}

	if( fAssoc )
	{
		CString szMsg = _T("This group is used in experiments. Those associations need to be removed first. Would you like to see how it is being used?");
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
		{
			RelationshipDlg d( this, REL_GROUP, tree.GetItemText( hItem ) );
			d.DoModal();
		}
		return;
	}

	// find group
	if( SUCCEEDED( grp.Find( szGrp ) ) )
	{
		// Delete group
		HRESULT hr = grp.Remove();
		if( SUCCEEDED( hr ) )
			tree.DeleteItem( hItem );
		// database has now been altered
		::DataHasChanged();

		// inform group was deleted
		szData.Format( IDS_AL_GRPDELETED, szGrp );
		OutputAMessage( szData );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Group: Add Subjects To Group
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnGAddsubj
// FullName:  CLeftView::OnGAddsubj
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGAddsubj() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	// get tree control
	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = NULL, hSub = NULL;
	DWORD dwType = 0;;
	CString szDelim, szGrp;

	// delimiter
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// were we dragging?
	// if so, we'll use the dropped tree item, otherwise get selected tree item
	if( !m_fDragging ) hItem = tree.GetSelectedItem();
	else hItem = m_hitemDrop;

	// get group
	hSub = hItem;
	dwType = tree.GetItemData( hItem );
	// if not subject folder, but not dragging...
	if( ( dwType != TI_SUBJECT_FOLDER ) && !m_fDragging )
	{
		CString szData;
		szData.LoadString( IDS_AL_ESUBJADD );
		OutputAMessage( szData );
		szGrp = tree.GetItemText( hItem );
		int nPos = szGrp.Find( szDelim );
		szGrp = szGrp.Mid( nPos + 2 );
	}
	// else if dragging to subject folder
	else if( dwType == TI_SUBJECT_FOLDER && m_fDragging )
	{
		hItem = tree.GetSelectedItem();
		szGrp = _DropID;
	}
	// else if dragging to group
	else if( dwType == TI_GROUP && m_fDragging )
	{
		szGrp = _DropID;
	}
	else
	{
		hItem = tree.GetParentItem( hItem );
		if( tree.GetItemData( hItem ) == TI_SUBJECT_FOLDER )
			szGrp = tree.GetItemText( tree.GetParentItem( hItem ) );
		else
			szGrp = tree.GetItemText( hItem );
		int nPos = szGrp.Find( szDelim );
		szGrp = szGrp.Mid( nPos + 2 );
	}
	Groups grp;
	if( FAILED( grp.Find( szGrp.AllocSysString() ) ) ) return;

	// Existing subjects
	if( dwType == TI_GROUP )
		hSub = tree.GetNextItem( hSub, TVGN_CHILD );	// Subject folder
	CStringList exist;
	hSub = tree.GetNextItem( hSub, TVGN_CHILD );
	while( hSub )
	{
		CString szSubj = tree.GetItemText( hSub );
		int nPos = szSubj.Find( szDelim );
		if( nPos != -1 )
		{
			szSubj = szSubj.Mid( nPos + 2 );
			exist.AddTail( szSubj );
		}
		hSub = tree.GetNextItem( hSub, TVGN_NEXT );
	}

	// add subject dialog
	AddSubjectDlg d( &exist, this );
	int nRet = m_fDragging ? IDOK : d.DoModal();
	if( d.m_fChanged ) RefreshSubjects();
	if( nRet == IDOK )
	{
		// experiment member object
		IExperimentMember* pEM = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									   IID_IExperimentMember, (LPVOID*)&pEM );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPMEM_ERR );
			return;
		}

		HTREEITEM hItemSubjs = NULL, hSubj = NULL;
		CString szItem, szSubj;

		// group
		if( !m_fDragging )
		{
			szGrp = tree.GetItemText( hItem );
			int nPos = szGrp.Find( szDelim );
			szGrp = szGrp.Mid( nPos + 2 );
		}
		else szGrp = _DropID;

		// experiment
		HTREEITEM hExp = tree.GetParentItem( hItem );
		hExp = tree.GetParentItem( hExp );
		CString szExp = tree.GetItemText( hExp );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );

		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( !m_fDragging && !pos ) return;

		HTREEITEM hChild = tree.GetNextItem( hItem, TVGN_CHILD );
		while( hChild )
		{
			dwType = tree.GetItemData( hChild );
			if( dwType == TI_SUBJECT_FOLDER )
			{
				hItemSubjs = hChild;
				break;
			}
			hChild = tree.GetNextItem( hChild, TVGN_NEXT );
		}

		if( !m_fDragging )
		{
			while( pos )
			{
				szItem = d.m_lstNew.GetNext( pos );

				nPos = szItem.Find( szDelim );
				szSubj = szItem.Mid( nPos + 2 );

				pEM->put_ExperimentID( szExp.AllocSysString() );
				pEM->put_GroupID( szGrp.AllocSysString() );
				pEM->put_SubjectID( szSubj.AllocSysString() );

				hr = pEM->Add();

				if( SUCCEEDED( hr ) )
				{
					::DataHasChanged();

					hSubj = tree.InsertItem( szItem, IMG_SUBJECT, IMG_SUBJECT, hItemSubjs );
					tree.SetItemData( hSubj, TI_SUBJECT );

					CString szData;
					szData.Format( IDS_AL_ESUBJADDING, szItem, tree.GetItemText( hItem ) );
					OutputAMessage( szData, TRUE );
				}
				else {
					CString szTempMsg;
					szTempMsg.LoadString(IDS_ADD_ERR);
					BCGPMessageBox( szTempMsg+_T(" CLeftView::1") );
				}
			}
		}
		else
		{
			szItem = _DragID;
			nPos = szItem.Find( szDelim );
			szSubj = szItem.Mid( nPos + 2 );

			// Check if in list
			HTREEITEM hItemTemp = tree.GetChildItem( hItemSubjs );
			CString szTmp;
			while( hItemTemp )
			{
				szTmp = tree.GetItemText( hItemTemp );
				nPos = szTmp.Find( szDelim );
				if( nPos == -1 ) return;
				szTmp = szTmp.Mid( nPos + 2 );

				if( szTmp == szSubj )
				{
					BCGPMessageBox( _T("This subject already exists in the group.") );
					return;
				}

				hItemTemp = tree.GetNextSiblingItem( hItemTemp );
			}

			pEM->put_ExperimentID( szExp.AllocSysString() );
			pEM->put_GroupID( szGrp.AllocSysString() );
			pEM->put_SubjectID( szSubj.AllocSysString() );

			hr = pEM->Add();

			if( SUCCEEDED( hr ) )
			{
				::DataHasChanged();

				hSubj = tree.InsertItem( szItem, IMG_SUBJECT, IMG_SUBJECT, hItemSubjs );
				tree.SetItemData( hSubj, TI_SUBJECT );

				CString szData;
				szData.Format( IDS_AL_ESUBJADDING, szItem, tree.GetItemText( hItem ) );
				OutputAMessage( szData, TRUE );
			}
			else {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" CLeftView::2") );
			}
		}

		pEM->Release();

		if( hSubj ) tree.EnsureVisible( hSubj );
	}
}

//************************************
// Method:    OnSfAddsubjects
// FullName:  CLeftView::OnSfAddsubjects
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSfAddsubjects() 
{
	CString szData;
	szData.LoadString( IDS_AL_SUBJADD );
	OutputAMessage( szData );

	OnGAddsubj();
}

/////////////////////////////////////////////////////////////////////////////
// Group: Remove From Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnGRemove
// FullName:  CLeftView::OnGRemove
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGRemove() 
{
	// log action
	CString szData;
	szData.LoadString( IDS_AL_GRPREM );
	OutputAMessage( szData );

	// get tree item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();

	// obtain group id
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// obtain parent experiment id
	HTREEITEM hExp = tree.GetParentItem( hItem );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// warning
	szData.Format( IDS_GRPREM_SURE, szGrp, szExp );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	// If no subjects, then just the "___" subject
	HTREEITEM hTemp = tree.GetChildItem( hItem );	// Subject folder
	if( tree.ItemHasChildren( hTemp ) )
	{
		BCGPMessageBox( IDS_GRPDEL_SUBJS );
		return;
	}

	// Delete from exp member db
	IExperimentMember* pEM = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}
	hr = pEM->RemoveGroup( szExp.AllocSysString(), szGrp.AllocSysString() );
	pEM->Release();

	// error?
	// 22Jul10: GMB: Tired of trying to diagnose this crap once very 3 years. Just eliminating 
	// the message, as it was simply removed from the DB already when removing a subject (varying conditions)
/*	if( FAILED( hr ) ) {
		BCGPMessageBox( IDS_REM_ERR );
		return;
	} */

	// Remove questionnaires
	IGQuestionnaire* pQ = NULL;
	hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
							IID_IGQuestionnaire, (LPVOID*)&pQ );
	if( SUCCEEDED( hr ) )
	{
		if( SUCCEEDED( pQ->FindForGroup( szExp.AllocSysString(), szGrp.AllocSysString() ) ) )
            hr = pQ->RemoveGroup( szGrp.AllocSysString() );
		pQ->Release();
	}
	else
	{
		szData.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szData );
	}

	// inform of deletion
	szData.Format( IDS_AL_GRPREMOVED, szGrp, szExp );
	OutputAMessage( szData, TRUE );

	// database altered
	::DataHasChanged();

	// delete from tree
	tree.DeleteItem( hItem );
}

/////////////////////////////////////////////////////////////////////////////
// Subject Folder: Create New
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnSfAdd
// FullName:  CLeftView::OnSfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSfAdd() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	CString szData;
	szData.LoadString( IDS_AL_SUBJADD2 );
	OutputAMessage( szData );

	Subjects subj;
	if( !subj.IsValid() ) return;

	if( subj.DoAdd( this ) )
	{
		::DataHasChanged();

		CString szItem;
		subj.GetNameWithCodeID( szItem, ::IsPrivacyOn() );

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_SUBJECT, IMG_SUBJECT, m_hSub );
		tree.SetItemData( hItem, TI_SUBJECT );

		tree.EnsureVisible( hItem );
		tree.SelectItem( hItem );

		subj.GetID( szItem );
		szData.Format( IDS_AL_SUBJADDED, szItem );
		OutputAMessage( szData, TRUE );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Subject Folder: Find Subject
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnSfFind
// FullName:  CLeftView::OnSfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSfFind() 
{
	CString szData;
	szData.LoadString( IDS_AL_SUBJFIND );
	OutputAMessage( szData );

	FindDlg d( IDS_SUBJ_FIND, this );
	if( d.DoModal() == IDOK ) Find( d.m_szID );
}

/////////////////////////////////////////////////////////////////////////////
// Subject: Delete
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnSDelete
// FullName:  CLeftView::OnSDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSDelete() 
{
	CString szData;
	szData.LoadString( IDS_AL_SUBJDEL );
	OutputAMessage( szData );

	// Selected Item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();

	// Subject
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubj = szItem.Mid( nPos + 2 );

	// Associations?
	CString szMsg = _T("Would you like to see how this subject is being used first?");
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
	{
		RelationshipDlg d( this, REL_SUBJECT, tree.GetItemText( hItem ) );
		d.DoModal();
	}

	// Message
	szData.Format( IDS_SUBJDEL_SURE, szSubj );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	// Delete
	Subjects subj;
	if( !subj.IsValid() ) return;

	if( SUCCEEDED( subj.Find( szSubj ) ) )
	{
		HRESULT hr = subj.Remove();

		// inform of deletion
		szData.Format( IDS_AL_SUBJDELETED, szSubj );
		OutputAMessage( szData );

		// database altered
		::DataHasChanged();

		// Now find in an experiment if can & remove (as well as experiment files)
		CString szPath, szFileFind, szFile;
		::GetDataPathRoot( szPath );
		CFileFind ff;
		HTREEITEM hExp = tree.GetChildItem( m_hExp );	// 1st experiment
		while( hExp )
		{
			CString szExp = tree.GetItemText( hExp );
			nPos = szExp.Find( szDelim );
			szExp = szExp.Mid( nPos + 2 );

			HTREEITEM hGrp = tree.GetChildItem( hExp );	// Group folder
			DWORD dwStyle = TI_UNKNOWN;
			if( hGrp ) dwStyle = tree.GetItemData( hGrp );
			if( ( dwStyle != TI_UNKNOWN ) && ( dwStyle != TI_GROUP_FOLDER ) )
			{
				hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );	// Retry
				if( hGrp ) dwStyle = tree.GetItemData( hGrp );
			}

			if( dwStyle == TI_GROUP_FOLDER )
			{
				hGrp = tree.GetChildItem( hGrp );				// 1st group
				while( hGrp )
				{
					CString szGrp = tree.GetItemText( hGrp );
					nPos = szGrp.Find( szDelim );
					szGrp = szGrp.Mid( nPos + 2 );

					HTREEITEM hSubj = tree.GetChildItem( hGrp );	// Subject folder
					if( hSubj ) hSubj = tree.GetChildItem( hSubj );	// 1st subject
					while( hSubj )
					{
						CString szSubj2 = tree.GetItemText( hSubj );
						nPos = szSubj2.Find( szDelim );
						szSubj2 = szSubj2.Mid( nPos + 2 );

						if( szSubj == szSubj2 )
						{
							IExperimentMember* pEM = NULL;
							hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
												   IID_IExperimentMember, (LPVOID*)&pEM );
							if( SUCCEEDED( hr ) )
							{
								hr = pEM->Find( szExp.AllocSysString(), szGrp.AllocSysString(),
												szSubj.AllocSysString() );
								if( SUCCEEDED( hr ) )
								{
									szFileFind.Format( _T("%s\\%s\\%s\\%s\\%s%s%s*.*"), szPath,
													   szExp, szGrp, szSubj, szExp, szGrp, szSubj );
									if( ff.FindFile( szFileFind ) )
									{
										while( ff.FindNextFile() )
										{
											szFile = ff.GetFilePath();
											::SendToRecycleBin( szFile );
										}
										szFile = ff.GetFilePath();
										::SendToRecycleBin( szFile );

										szFileFind.Format( _T("%s\\%s\\%s\\%s"), szPath,
														   szExp, szGrp, szSubj );
										_rmdir( szFileFind );
										OutputAMessage( _T("Subject files have been sent to the Recycle Bin."), TRUE );
									}

									hr = pEM->Remove();
									if( FAILED( hr ) ) BCGPMessageBox( IDS_LINKSUBJDEL_ERR );
									else tree.DeleteItem( hSubj );

									// Remove folder
									szFileFind.Format( _T("%s\\%s\\%s\\%s"), szPath, szExp, szGrp, szSubj );
									_rmdir( szFileFind );
								}
								else BCGPMessageBox( IDS_LINKSUBJFIND_ERR );

								pEM->Release();
							}
							else BCGPMessageBox( IDS_EXPMEM_ERR );
						}

						hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );	// Next subject
					}

					hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );	// next group
				}
			}

			hExp = tree.GetNextItem( hExp, TVGN_NEXT );	// Next experiment
		}

		tree.DeleteItem( hItem );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Subject: Properties
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnSEdit2
// FullName:  CLeftView::OnSEdit2
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSEdit2()
{
	CString szData;
	szData.LoadString( IDS_AL_SUBJEDIT );
	OutputAMessage( szData );
	OnSEdit();
}

//************************************
// Method:    OnSEdit
// FullName:  CLeftView::OnSEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSEdit() 
{
	Subjects subj;
	if( !subj.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	// which subject
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	// Which experiment/group
	CString szGrpID, szExpID;
	HTREEITEM hExp = tree.GetParentItem( tree.GetSelectedItem() ); // Subject folder
	hExp = tree.GetParentItem( hExp ); // Group
	if( hExp )
	{
		szItem = tree.GetItemText( hExp );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Mid( nPos + 2 );
			hExp = tree.GetParentItem( hExp ); // Group folder
			hExp = tree.GetParentItem( hExp ); // Experiment
			szItem = tree.GetItemText( hExp ); 
			nPos = szItem.Find( szDelim );
			if( nPos != -1 ) szExpID = szItem.Mid( nPos + 2 );
		}
	}

	CString szData;
	szData.Format( IDS_AL_SUBJEDIT2, szIDs );
	OutputAMessage( szData );

	// Original text
	CString szOld = tree.GetItemText( tree.GetSelectedItem() );

	// device info
	double dDevRes = -1.;
	short nSampRate = -1;
	HRESULT hr = m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(), szIDs.AllocSysString() );
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_DeviceRes( &dDevRes );
		m_pEML->get_SamplingRate( &nSampRate );
	}
	// if no subject info, set it to experiment values
	if( ( dDevRes == 0. ) || ( nSampRate == 0 ) )
	{
		IProcSetting* pPS = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									   IID_IProcSetting, (LPVOID*)&pPS );
		if( pPS )
		{
			if( SUCCEEDED( pPS->Find( szExpID.AllocSysString() ) ) )
			{
				pPS->get_DeviceResolution( &dDevRes );
				pPS->get_SamplingRate( &nSampRate );
			}
			pPS->Release();
		}
	}

	// do edit
	subj.m_szExpID = szExpID;
	subj.m_szGrpID = szGrpID;
	subj.m_dDevRes = dDevRes;
	subj.m_nSampRate = nSampRate;
	if( subj.DoEdit( szIDs, this ) )
	{
		::DataHasChanged();

		CString szItem;
		subj.GetNameWithCodeID( szItem, ::IsPrivacyOn() );

//		tree.SetItemText( tree.GetSelectedItem(), szItem );
		ChangeAllInstanceNames( TI_SUBJECT, szOld, szItem );

		szData.Format( IDS_AL_SUBJEDITED, szIDs );
		OutputAMessage( szData );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Subject: Remove From Group
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnSRemove
// FullName:  CLeftView::OnSRemove
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSRemove() 
{
	// output of action
	CString szData;
	szData.LoadString( IDS_AL_SUBJREM );
	OutputAMessage( szData );

	// get selected tree item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();

	// obtain subject id
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubj = szItem.Mid( nPos + 2 );

	// obtain parent group id
	HTREEITEM hGroup = tree.GetParentItem( hItem );
	hGroup = tree.GetParentItem( hGroup );
	szItem = tree.GetItemText( hGroup );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// obtain parent experiment id
	HTREEITEM hExp = tree.GetParentItem( hGroup );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// warning
	szData.Format( IDS_SUBJREM_SURE, szSubj, szGrp, szExp );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	// create experiment member object
	IExperimentMember* pEM = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}
	// find the expmember relationship
	hr = pEM->Find( szExp.AllocSysString(), szGrp.AllocSysString(),
					szSubj.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_FIND_ERR );
		pEM->Release();
		return;
	}
	// delete it
	hr = pEM->Remove();
	// cleanup object
	pEM->Release();

	// create subject questionnaire object
	ISQuestionnaire* pSQ = NULL;
	hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
							IID_ISQuestionnaire, (LPVOID*)&pSQ );
	if( SUCCEEDED( hr ) )
	{
		// remove the subject questionnaire
		if( SUCCEEDED( pSQ->FindForSubject( szExp.AllocSysString(),
											szGrp.AllocSysString(),
											szSubj.AllocSysString() ) ) )
		{
			pSQ->RemoveSubject( szSubj.AllocSysString() );
		}

		pSQ->Release();
	}
	else
	{
		szData.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szData );
	}

	// inform that subject was removed
	szData.Format( IDS_AL_SUBJREMOVED, szSubj, szGrp, szExp );
	OutputAMessage( szData, TRUE );
	// there was an error
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_REM_ERR );
		return;
	}

	// we have altered the database
	::DataHasChanged();

	// remove from the tree
	tree.DeleteItem( hItem );
}

/////////////////////////////////////////////////////////////////////////////
// Experiment: Add Conditions To Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnEAddcondition
// FullName:  CLeftView::OnEAddcondition
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAddcondition() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem, hItemConds, hCond;
	DWORD dwType;
	CString szExp, szCond;
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	if( !m_fDragging )
	{
		hItem = tree.GetSelectedItem();
		szExp = tree.GetItemText( hItem );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}
	else
	{
		hItem = m_hitemDrop;
		szExp = _DropID;
	}
	hItemConds = hItem;
	dwType = tree.GetItemData( hItem );
	if( ( dwType != TI_CONDITION_FOLDER ) && !m_fDragging )
	{
		CString szData;
		szData.LoadString( IDS_AL_ECONDADD );
		OutputAMessage( szData );
	}
	if( dwType == TI_CONDITION_FOLDER )
	{
		hItem = tree.GetParentItem( hItem );
		szExp = tree.GetItemText( hItem );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}

	// Existing conditions
	CStringList exist;

	if( dwType == TI_EXPERIMENT )
	{
		hItemConds = tree.GetNextItem( hItem, TVGN_CHILD );	// Condition folder ?
		if( hItemConds ) dwType = tree.GetItemData( hItemConds );
		else dwType = TI_UNKNOWN;

		if( dwType == TI_GROUP_FOLDER )
		{
			hItemConds = tree.GetNextItem( hItemConds, TVGN_NEXT );
			if( hItemConds ) dwType = tree.GetItemData( hItemConds );
			else dwType = TI_UNKNOWN;
		}

		if( dwType == TI_UNKNOWN ) hItemConds = NULL;
	}
	if( dwType == TI_CONDITION_FOLDER )
	{
		hCond = tree.GetNextItem( hItemConds, TVGN_CHILD );
		while( hCond )
		{
			szCond = tree.GetItemText( hCond );
			int nPos = szCond.Find( szDelim );
			if( nPos != -1 )
			{
				szCond = szCond.Mid( nPos + 2 );
				exist.AddTail( szCond );
			}
			hCond = tree.GetNextItem( hCond, TVGN_NEXT );
		}
	}

	AddConditionDlg d( &exist, this );
	int nRet = m_fDragging ? IDOK : d.DoModal();
	if( d.m_fChanged ) RefreshConditions();
	if( nRet == IDOK )
	{
		IExperimentCondition* pEC = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
									   IID_IExperimentCondition, (LPVOID*)&pEC );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPCOND_ERR );
			return;
		}

		CString szItem;
		int nPos;

		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( !m_fDragging && !pos ) return;

		if( !m_fDragging )
		{
			while( pos )
			{
				szItem = d.m_lstNew.GetNext( pos );

				nPos = szItem.Find( szDelim );
				szCond = szItem.Mid( nPos + 2 );

				pEC->put_ExperimentID( szExp.AllocSysString() );
				pEC->put_ConditionID( szCond.AllocSysString() );

				hr = pEC->Add();

				if( SUCCEEDED( hr ) )
				{
					::DataHasChanged();

					// update stimuli and elements since you can add these from within condition
					RefreshElements();
					RefreshStimuli();

					hCond = tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, hItemConds );
					tree.SetItemData( hCond, TI_CONDITION );

					CString szData;
					szData.Format( IDS_AL_ECONDADDED, szCond, szExp );
					OutputAMessage( szData, TRUE );
				}
				else {
					CString szTempMsg;
					szTempMsg.LoadString(IDS_ADD_ERR);
					BCGPMessageBox( szTempMsg+_T(" CLeftView::3") );
				}
			}
		}
		else
		{
			nPos = _DragID.ReverseFind( szDelim[ 0 ] );
			szCond = _DragID.Mid( nPos + 2 );

			// Check if in list
			HTREEITEM hItemTemp = tree.GetChildItem( hItemConds );
			CString szTmp;
			while( hItemTemp )
			{
				szTmp = tree.GetItemText( hItemTemp );
				nPos = szTmp.Find( szDelim );
				if( nPos == -1 ) return;
				szTmp = szTmp.Mid( nPos + 2 );

				if( szTmp == szCond )
				{
					BCGPMessageBox( _T("This condition already exists in the experiment.") );
					return;
				}

				hItemTemp = tree.GetNextSiblingItem( hItemTemp );
			}

			pEC->put_ExperimentID( szExp.AllocSysString() );
			pEC->put_ConditionID( szCond.AllocSysString() );

			hr = pEC->Add();

			if( SUCCEEDED( hr ) )
			{
				::DataHasChanged();
				hCond = tree.InsertItem( _DragID, IMG_CONDITION, IMG_CONDITION, hItemConds );
				tree.SetItemData( hCond, TI_CONDITION );

				CString szData;
				szData.Format( IDS_AL_ECONDADDED, szCond, szExp );
				OutputAMessage( szData, TRUE );
			}
			else {
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" CLeftView::3") );
			}
		}

		pEC->Release();

		if( hCond ) tree.EnsureVisible( hCond );
	}
}

//************************************
// Method:    OnCfAdd
// FullName:  CLeftView::OnCfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCfAdd() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	NSConditions cond;
	if( !cond.IsValid() ) return;

	CString szData;
	szData.LoadString( IDS_AL_CONDADD );
	OutputAMessage( szData );

	if( cond.DoAdd( this ) )
	{
		// flag that data has changed
		::DataHasChanged();

		// update stimuli and elements since you can add these from within condition
		RefreshElements();
		RefreshStimuli();

		// get the string (ID + desc) to add to tree
		CString szItem;
		cond.GetDescriptionWithID( szItem );

		// add it to tree and assign the appropriate data val
		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, m_hCond );
		tree.SetItemData( hItem, TI_CONDITION );

		// ensure it is visible and select it in tree
		tree.EnsureVisible( hItem );
		tree.SelectItem( hItem );

		// output message
		szData.Format( IDS_AL_CONDADDED, szItem );
		OutputAMessage( szData, TRUE );
	}
}

//************************************
// Method:    OnCfAddconditions
// FullName:  CLeftView::OnCfAddconditions
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCfAddconditions() 
{
	CString szData;
	szData.LoadString( IDS_AL_CONDADD2 );
	OutputAMessage( szData );

	OnEAddcondition();
}

/////////////////////////////////////////////////////////////////////////////
// Condition Folder: Find Condition
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnCfFind
// FullName:  CLeftView::OnCfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCfFind() 
{
	FindDlg d( IDS_COND_FIND, this );
	if( d.DoModal() == IDOK )
		Find( d.m_szID );
}

/////////////////////////////////////////////////////////////////////////////
// Condition: Properties
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnCEdit2
// FullName:  CLeftView::OnCEdit2
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCEdit2()
{
	CString szData;
	szData.LoadString( IDS_AL_CONDEDIT );
	OutputAMessage( szData );

	OnCEdit();
}

//************************************
// Method:    EditCondition
// FullName:  CLeftView::EditCondition
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: UINT iSelectPage
//************************************
void CLeftView::EditCondition( UINT iSelectPage )
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which Condition
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	// Which experiment (if applicable)
	HTREEITEM hExp = tree.GetParentItem( tree.GetSelectedItem() );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	CString szExp;
	if( nPos != -1 ) szExp = szItem.Mid( nPos + 2 );

	CString szData;
	szData.Format( IDS_AL_CONDEDIT2, szCond );
	OutputAMessage( szData );

	// Original text
	CString szOld = tree.GetItemText( tree.GetSelectedItem() );

	// Modify
	if( cond.DoEdit( szCond, szExp, this, iSelectPage ) )
	{
		::DataHasChanged();

		// update stimuli and elements since you can add these from within condition
		RefreshElements();
		RefreshStimuli();

		CString szItem;
		cond.GetDescriptionWithID( szItem );

		ChangeAllInstanceNames( TI_CONDITION, szOld, szItem );

		szData.Format( IDS_AL_CONDEDITED, szCond );
		OutputAMessage( szData );
	}
}

//************************************
// Method:    OnCEdit
// FullName:  CLeftView::OnCEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCEdit() 
{
	EditCondition();
}


/////////////////////////////////////////////////////////////////////////////
// Condition: Delete
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnCDelete
// FullName:  CLeftView::OnCDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCDelete() 
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CString szData;
	szData.LoadString( IDS_AL_CONDDEL );
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	// Associations?
	CString szMsg = _T("Would you like to see how this condition is being used first?");
	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
	{
		RelationshipDlg d( this, REL_CONDITION, tree.GetItemText( hItem ) );
		d.DoModal();
	}

	// warning
	szData.Format( IDS_CONDDEL_SURE, szCond );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	if( SUCCEEDED( cond.Find( szCond ) ) )
	{
		HRESULT hr = cond.Remove();

		szData.Format( IDS_AL_CONDDEL2, szCond );
		OutputAMessage( szData );

		::DataHasChanged();

		// Now find in an experiment if can & remove
		HTREEITEM hExp = tree.GetNextItem( m_hExp, TVGN_CHILD );	// 1st experiment
		while( hExp )
		{
			CString szExp = tree.GetItemText( hExp );
			nPos = szExp.Find( szDelim );
			szExp = szExp.Mid( nPos + 2 );

			HTREEITEM hCond = tree.GetNextItem( hExp, TVGN_CHILD );	// Condition folder
			DWORD dwStyle = TI_UNKNOWN;
			if( hCond ) dwStyle = tree.GetItemData( hCond );
			if( ( dwStyle != TI_UNKNOWN ) && ( dwStyle != TI_CONDITION_FOLDER ) )
			{
				hCond = tree.GetNextItem( hCond, TVGN_NEXT );	// Retry
				if( hCond ) dwStyle = tree.GetItemData( hCond );
			}

			if( dwStyle == TI_CONDITION_FOLDER )
			{
				hCond = tree.GetNextItem( hCond, TVGN_CHILD );	// 1st condition
				while( hCond )
				{
					CString szCond2 = tree.GetItemText( hCond );
					nPos = szCond2.Find( szDelim );
					szCond2 = szCond2.Mid( nPos + 2 );

					if( szCond == szCond2 )
					{
						IExperimentCondition* pEC = NULL;
						hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
											   IID_IExperimentCondition, (LPVOID*)&pEC );
						if( SUCCEEDED( hr ) )
						{
							hr = pEC->Find( szExp.AllocSysString(), szCond.AllocSysString() );
							if( SUCCEEDED( hr ) )
							{
								hr = pEC->Remove();
								if( FAILED( hr ) )
									BCGPMessageBox( IDS_LINKCONDDEL_ERR );
								else
									tree.DeleteItem( hCond );
							}
							else BCGPMessageBox( IDS_LINKCONDFIND_ERR );

							pEC->Release();
						}
						else BCGPMessageBox( IDS_EXPCOND_ERR );
					}

					hCond = tree.GetNextItem( hCond, TVGN_NEXT );	// Next condition
				}
			}

			hExp = tree.GetNextItem( hExp, TVGN_NEXT );	// Next experiment
		}

		tree.DeleteItem( hItem );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Condition: Remove From Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnCRemove
// FullName:  CLeftView::OnCRemove
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCRemove() 
{
	CString szData;
	szData.LoadString( IDS_AL_CONDREM );
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	HTREEITEM hExp = tree.GetParentItem( hItem );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	szData.Format( IDS_CONDREM_SURE, szCond, szExp );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	{
	NSConditions cond;
	if( cond.Find( szCond ) )
	{
		BOOL fRecord = FALSE, fProcess = FALSE;
		cond.GetRecord( fRecord );
		cond.GetProcess( fProcess );
		if( fRecord && !fProcess )
		{
			szData = _T("This condition is a RECORD-ONLY condition.\nRemoving this condition could cause the experiment to not function correctly.\n\nAre you sure you want to remove this condition?");
			if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;
		}
	}
	}

	IExperimentCondition* pEC = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
								   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCOND_ERR );
		return;
	}

	hr = pEC->Find( szExp.AllocSysString(), szCond.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_FIND_ERR );
		pEC->Release();
		return;
	}

	hr = pEC->Remove();
	pEC->Release();

	szData.Format( IDS_AL_CONDREMOVED, szCond, szExp );
	OutputAMessage( szData, TRUE );

	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_REM_ERR );
		return;
	}
	::DataHasChanged();

	tree.DeleteItem( hItem );
}

/////////////////////////////////////////////////////////////////////////////
// Tree View Right-Click Message Handler
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnRclick
// FullName:  CLeftView::OnRclick
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NMHDR * pNMHDR
// Parameter: LRESULT * pResult
//************************************
void CLeftView::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// get the item that is below cursor
	CTreeCtrl& tree = GetTreeCtrl();
	CPoint pt;
	::GetCursorPos( &pt );
	tree.ScreenToClient( &pt );
	UINT uFlags;

	HTREEITEM hItem = tree.HitTest( pt, &uFlags );
	if( hItem && ( ( uFlags & TVHT_ONITEM ) != 0 ) )
	{
		tree.SelectItem( hItem );
		MSG* pMsg = new MSG;
		pMsg->message = WM_CONTEXTMENU;
		PreTranslateMessage( pMsg );
		delete pMsg;
	}
	*pResult = 0;
}

/////////////////////////////////////////////////////////////////////////////
// Condition: Replications
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnCReps
// FullName:  CLeftView::OnCReps
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCReps() 
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CString szData;
	szData.LoadString( IDS_AL_CONDREPS );
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which condition
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	// Which experiment
	HTREEITEM hExp = tree.GetParentItem( hItem );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	if( cond.DoReps( szCond, szExp, this ) )
	{
		::DataHasChanged();

		szData.Format( IDS_AL_CONDREPSEDITED, szCond );
		OutputAMessage( szData );
	}
}

/////////////////////////////////////////////////////////////////////////////
// Subject: Run Experiment
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    GetRunExpInfo
// FullName:  CLeftView::GetRunExpInfo
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: HTREEITEM hItem
// Parameter: CString & szExpID
// Parameter: CString & szExp
// Parameter: CString & szGrpID
// Parameter: CString & szGrp
// Parameter: CString & szSubjID
// Parameter: CString & szSubj
//************************************
BOOL CLeftView::GetRunExpInfo( HTREEITEM hItem, CString& szExpID, CString& szExp,
							   CString& szGrpID, CString& szGrp, CString& szSubjID,
							   CString& szSubj )
{
	if( !hItem ) return FALSE;

	// tree & delimiter
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return FALSE;
	szSubjID = szItem.Mid( nPos + 2 );
	szSubj = szItem.Left( nPos );

	// Which experiment/group
	HTREEITEM hExp = tree.GetParentItem( hItem ); // Subject folder
	if( !hExp ) return FALSE;
	hExp = tree.GetParentItem( hExp ); // Group
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return FALSE;
	szGrpID = szItem.Mid( nPos + 2 );
	szGrp = szItem.Left( nPos );
	hExp = tree.GetParentItem( hExp ); // Group folder
	if( !hExp ) return FALSE;
	hExp = tree.GetParentItem( hExp ); // Experiment
	if( !hExp ) return FALSE;
	szItem = tree.GetItemText( hExp ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return FALSE;
	szExpID = szItem.Mid( nPos + 2 );
	szExp = szItem.Left( nPos - 1 );

	return TRUE;
}

//************************************
// Method:    OnSExp
// FullName:  CLeftView::OnSExp
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSExp() 
{
	CString szData = _T("SRC: Subject - Run Experiment");
	OutputAMessage( szData );

	// if we're already doing something, exit
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF )
	{
		OutputMessage( _T("ERROR: Run Experiment: Main Window pointer invalid."), LOG_UI );
		return;
	}
	if( pMF->m_fContinue )
	{
		OutputMessage( _T("ERROR: Run Experiment: Another experiment already running."), LOG_UI );
		return;
	}

	// exp/grp/subj info
	CString szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj;
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !GetRunExpInfo( hItem, szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj ) )
	{
		OutputMessage( _T("ERROR: Run Experiment: Unable to obtain tree information for running experiment."), LOG_UI );
		return;
	}

	// user object
	UserObj* pObj = ::GetCurrentUserObj();
	if( !pObj )
	{
		OutputMessage( _T("ERROR: Run Experiment: Invalid user object."), LOG_UI );
		return;
	}
	pObj->Reset();
	pObj->m_szExpID = szExpID;
	pObj->m_szExp = szExp;
	pObj->m_szGrpID = szGrpID;
	pObj->m_szGrp = szGrp;
	pObj->m_szSubjID = szSubjID;
	pObj->m_szSubj = szSubj;

	// run experiment
	Experiments::RunExperiment( szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj, (LPVOID)hItem );
}

/////////////////////////////////////////////////////////////////////////////
// Experiment: Summarize
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnESum
// FullName:  CLeftView::OnESum
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnESum() 
{
	CString szData;
	szData.LoadString( IDS_AL_SUMM );
	OutputAMessage( szData );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	_ActionState = ACTION_SUMMARIZE;
	CWaitCursor crs;
	pMF->SummarizeExperiment( szExp, NULL, TRUE, FALSE, FALSE, FALSE, _T(""), TRUE );
	_ActionState = ACTION_NONE;

	_TerminateThread = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Tree View Refreshing
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    Refresh
// FullName:  CLeftView::Refresh
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::Refresh()
{
	CString	szItem;
	BOOL	fInExp = FALSE;
	DWORD	dwData = 0;

	// get currently selected item info (if any)
	CString szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj;
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( hItem )
	{
		dwData = tree.GetItemData( hItem );
		// if trial(folder) need subject
		if( dwData == TI_TRIAL )
		{
			hItem = tree.GetParentItem( hItem );	// trial folder
			hItem = tree.GetParentItem( hItem );	// subject
		}
		else if( ( dwData == TI_HWRTRIAL_FOLDER ) || ( dwData == TI_FRDTRIAL_FOLDER ) || (dwData == TI_IMAGE_FOLDER ) )
		{
			hItem = tree.GetParentItem( hItem );	// subject
			dwData = TI_TRIAL;
		}
		fInExp = GetRunExpInfo( hItem, szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj );
		if( fInExp )
		{
			szItem = szExpID;
			szItem += szGrpID;
			szItem += szSubjID;
		}
		else
		{
			CString szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );
			szItem = tree.GetItemText( hItem );
			int nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				szItem = szItem.Mid( nPos + 2 );
				// not in experiment if parent is at root
				hItem = tree.GetParentItem( hItem );
				if( hItem )
				{
					hItem = tree.GetParentItem( hItem );
					if( hItem ) fInExp = TRUE;
					else fInExp = FALSE;
				}
				else szItem = _T("");
			}
			else szItem = _T("");
		}
	}

	// write the tree memory
	WriteMemory();
	// fill the tree
	FillTree();
	// read the memory
	ReadMemory();

	// now select previously selected item (if any)
	if( szItem != _T("") )
	{
		// if trial(folder) or subject, easy
		if( ( dwData == TI_TRIAL ) || ( fInExp && ( dwData == TI_SUBJECT ) ) )
		{
			hItem = Find( szExpID, szGrpID, szSubjID );
		}
		else if( fInExp )
		{
			hItem = Find( m_hExp, szItem );
		}
		else
		{
			switch( dwData )
			{
				case TI_EXPERIMENT: hItem = m_hExp; break;
				case TI_GROUP: hItem = m_hGrp; break;
				case TI_SUBJECT: hItem = m_hSub; break;
				case TI_CONDITION: hItem = m_hCond; break;
				case TI_STIMULUS: hItem = m_hStim; break;
				case TI_ELEMENT: hItem = m_hElmt; break;
				case TI_CATEGORY: hItem = m_hCat; break;
				case TI_FEEDBACK: hItem = m_hFb; break;
				default: hItem = NULL;
			}
			hItem = Find( hItem, szItem );
		}
		if( hItem ) tree.SelectItem( hItem );
	}
}

//************************************
// Method:    RefreshExperiments
// FullName:  CLeftView::RefreshExperiments
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshExperiments()
{
	CString szPath, szDelim, szData, szID, szDesc, szItem, szMsg;
	// Path & delimiter
	::GetDataPathRoot( szPath );
	::GetDelimiter( szDelim );
	BOOL fRecord = FALSE, fProcess = FALSE;
	int nPos = 0;
	long nSteps = 0, nStep = 0;
	// Tree
	CTreeCtrl& tree = GetTreeCtrl();
	// Sort mechanism
	HTREEITEM hSort = ::IsTrialAlphaOn() ? TVI_SORT : TVI_LAST;

	HTREEITEM hItem = NULL, hItemFolder = NULL, hItemItem = NULL, hItemFolder2 = NULL, hItemItem2 = NULL;
	HRESULT hr = E_FAIL, hr2 = E_FAIL;

	// Remove first
	if( m_hExp )
	{
		hItem = tree.GetNextItem( m_hExp, TVGN_CHILD );
		HTREEITEM hItem2 = hItem;
		while( hItem )
		{
			hItem2 = tree.GetNextSiblingItem( hItem );
			tree.DeleteItem( hItem );
			hItem = hItem2;
		}
	}

	// Create objects
	Experiments exp;
	Groups grp;
	Subjects subj;
	NSConditions cond;

	// Create experiment settings object
	IProcSetting* pPS = NULL;
	hr2 = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
							IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr2 ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}

	// Create Exp/Group/Subject list object
	if( m_pEML ) m_pEML->Release();
	hr2 = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
							IID_IExperimentMember, (LPVOID*)&m_pEML );
	if( FAILED( hr2 ) )
	{
		BCGPMessageBox( IDS_EM_ERR );
		pPS->Release();
		return;
	}

	// Create Exp/Condition list object
	if( m_pECL ) m_pECL->Release();
	hr2 = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
							IID_IExperimentCondition, (LPVOID*)&m_pECL );
	if( FAILED( hr2 ) )
	{
		BCGPMessageBox( IDS_EC_ERR );
		pPS->Release();
		return;
	}

	CString szGrpID, szGrp, szSubjID, szName, szRoot, szFiles, szTxt;
	BSTR bstrGrpID = NULL, bstrSubjID = NULL;
	CStringList	lstGrp;
	HTREEITEM hGrps[ MAX_GROUPS ], hSubjs[ MAX_GROUPS ];
	int nGrps = 0, nImage = 0, nCount = 0;
	short nType = 0;
	BOOL fAddedItem = FALSE;
	POSITION posHITEM = NULL;
	CStringList lstAttachments;
	CFileFind ff;

	// Loop through each experiment to get count
	Progress::EnableProgress( 2 );
	Progress::SetProgress( 1, _T("Calculating experiment load time...") );
	hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		exp.GetID( szID );
		hr2 = m_pEML->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr2 ) )
		{
			nSteps++;
			hr2 = m_pEML->GetNext();
		}
		hr2 = m_pECL->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr2 ) )
		{
			nSteps++;
			hr2 = m_pECL->GetNext();
		}
		hr = exp.GetNext();
	}
	Progress::DisableProgress();

	// Loop through each experiment
	hr = exp.ResetToStart();
	Progress::EnableProgress( nSteps );
	while( SUCCEEDED( hr ) )
	{
		// exp ID & progress meter
		exp.GetID( szID );

		lstGrp.RemoveAll();
		for( int i = 0; i < MAX_GROUPS; i++ )
		{
			hGrps[ i ] = NULL;
			hSubjs[ i ] = NULL;
		}
		nGrps = 0;

		// Get data
		exp.GetDescription( szDesc );
		exp.GetType( nType );

		// locate exp settings
		pPS->Find( szID.AllocSysString() );

		if( FAILED( hr ) || ( szID == _T("") ) )
		{
			CString szMsg;
			szMsg.Format( IDS_HEX_ERR, hr );
			BCGPMessageBox( szMsg );
			pPS->Release();
			return;
		}
		if( szID == _T("") ) break;

		// image based on experiment type
		nImage = IMG_EXPERIMENT;
		if( nType == EXP_TYPE_IMAGE ) nImage = IMG_EXPERIMENT_IMG;
		else if( nType == EXP_TYPE_GRIPPER ) nImage = IMG_EXPERIMENT_GRIP;

		// Display this on tree
		szItem.Format( _T("%s%s%s"), szDesc, szDelim, szID );
		hItem = tree.InsertItem( szItem, nImage, nImage, m_hExp, hSort );
		tree.SetItemData( hItem, TI_EXPERIMENT );

		// Now get all groups/subjects for this experiment (loop through)
		szItem.Format(IDS_EXPGROUPS, szID );
		hItemFolder = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hItem );
		tree.SetItemData( hItemFolder, TI_GROUP_FOLDER );
		tree.SetItemState( hItemFolder, TVIS_BOLD, TVIS_BOLD );
		hr2 = m_pEML->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr2 ) )
		{
			hr2 = m_pEML->get_GroupID( &bstrGrpID );
			szGrpID = bstrGrpID;

			if( SUCCEEDED( hr2 ) && ( bstrGrpID != NULL ) && ( szGrpID != _T("") ) )
			{
				// Get data
				m_pEML->get_SubjectID( &bstrSubjID );
				szSubjID = bstrSubjID;

				// progress
				nStep++;
				szMsg.Format( _T("Loading experiment %s group %s subject %s..."), szID, szGrpID, szSubjID );
				Progress::SetProgress( nStep, szMsg );

				// load group & subject
				hr2 = grp.Find( bstrGrpID );
				if( SUCCEEDED( hr2 ) ) grp.GetDescription( szGrp );
				if( szSubjID != HOLDER ) hr2 = subj.Find( bstrSubjID );
//				else hr2 = E_FAIL;
				if( FAILED( hr2 ) )
				{
					szName = _T("");
					hr2 = m_pEML->GetNext();
					continue;
				}
				subj.GetNameWithCodeID( szName, ::IsPrivacyOn() );
				TRIM( szGrpID );

				// If current group is not in list, need new tree item
				posHITEM = lstGrp.Find( szGrpID );
				if( !posHITEM )
				{
					// Display this group on tree
					szItem.Format( _T("%s%s%s"), szGrp, szDelim, szGrpID );

					hItemItem = tree.InsertItem( szItem, IMG_GROUP, IMG_GROUP, hItemFolder, hSort );
					tree.SetItemData( hItemItem, TI_GROUP );

					hItemFolder2 = NULL;

					nGrps++;
					hGrps[ nGrps - 1 ] = hItemItem;
					lstGrp.AddTail( szGrpID );
					fAddedItem = TRUE;
				}
				else
				{
					nPos = FindPosition( lstGrp, posHITEM );
					if( nPos != -1 )
					{
						hItemItem = hGrps[ nPos ];
						hItemFolder2 = hSubjs[ nPos ];
					}
					fAddedItem = FALSE;
				}

				// If no folder yet for subjects, create one for tree
				if( hItemFolder2 == NULL )
				{
					szItem.Format( IDS_EXPSUBJS, szID, szGrpID );
					hItemFolder2 = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hItemItem );
					tree.SetItemData( hItemFolder2, TI_SUBJECT_FOLDER );
					tree.SetItemState( hItemFolder2, TVIS_BOLD, TVIS_BOLD );

					hSubjs[ nGrps - 1 ] = hItemFolder2;
				}

				// Display this subject on tree
				if( szSubjID != HOLDER )
				{
					hItemItem2 = tree.InsertItem( szName, IMG_SUBJECT, IMG_SUBJECT, hItemFolder2, hSort );
					tree.SetItemData( hItemItem2, TI_SUBJECT );
					ShowTrials( szID, szSubjID, szGrpID, hItemItem2, FALSE, FALSE, TRUE );
					tree.Expand( hItemItem2, TVE_COLLAPSERESET );
				}
			}

			// Next Exp/Grp/Subj item
			hr2 = m_pEML->GetNext();
		}

		// Conditions
		BSTR bstrCondID = NULL;
		CString szCondID, szCond;
		// Now add conditions to experiment
		szItem.Format( IDS_EXPCONDS, szID );
		hItemFolder = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hItem );
		tree.SetItemData( hItemFolder, TI_CONDITION_FOLDER );
		tree.SetItemState( hItemFolder, TVIS_BOLD, TVIS_BOLD );
		hr2 = m_pECL->FindForExperiment( szID.AllocSysString() );
		while( SUCCEEDED( hr2 ) )
		{
			hr2 = m_pECL->get_ConditionID( &bstrCondID );
			szCondID = bstrCondID;

			nStep++;
			szMsg.Format( _T("Loading experiment %s condition %s..."), szID, szCond );
			Progress::SetProgress( nStep, szMsg );

			if( SUCCEEDED( hr2 ) && ( bstrCondID != NULL ) && szCondID != _T("") )
			{
				// Get data
				hr2 = cond.Find( bstrCondID );
				if( SUCCEEDED( hr2 ) )
				{
					cond.GetDescription( szCond );
					cond.GetRecord( fRecord );
					cond.GetProcess( fProcess );

					nPos = IMG_CONDITION;
					if( fRecord && !fProcess ) nPos = IMG_CONDITION_RO;
					else if( fProcess && !fRecord ) nPos = IMG_CONDITION_PO;

					// Display this condition on tree
					szItem.Format( _T("%s%s%s"), szCond, szDelim, szCondID );

					hItemItem = tree.InsertItem( szItem, nPos, nPos, hItemFolder, hSort );
					tree.SetItemData( hItemItem, TI_CONDITION );
				}
			}

			// Next condition
			hr2 = m_pECL->GetNext();
		}

		// Attachments
		nCount = 0;
		szRoot = szPath;
		lstAttachments.RemoveAll();
		// ... create folder
		szItem.Format( _T("%s Attachments"), szID );
		hItemFolder2 = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hItem, TVI_LAST );
		tree.SetItemData( hItemFolder2, TI_ATTACHMENT_FOLDER );
		tree.SetItemState( hItemFolder2, TVIS_BOLD, TVIS_BOLD );
		// ... locate any files
		if( szRoot.GetAt( szRoot.GetLength() - 1 ) != '\\' ) szRoot += _T("\\");
		szRoot += szID;
		szRoot += _T("\\attachments");
		szFiles.Format( _T("%s\\*.*"), szRoot );
		if( ff.FindFile( szRoot ) )
		{
			BOOL fCont = ff.FindFile( szFiles );
			while( fCont )
			{
				fCont = ff.FindNextFile();
				if( !ff.IsDirectory() )
				{
					lstAttachments.AddTail( ff.GetFileName() );
					nCount++;
				}
			}
		}
		// insert files if any
		posHITEM = lstAttachments.GetHeadPosition();
		while( posHITEM )
		{
			szItem = lstAttachments.GetNext( posHITEM );
			hItemItem = tree.InsertItem( szItem, 24, 24, hItemFolder2, TVI_LAST );
			tree.SetItemData( hItemItem, TI_ATTACHMENT );
		}

		// HACK: Since experiment object is used in call to show trials, it messes up DB pointer
		// so we'll just go back to the last experiment via a find call
		hr = exp.Find( szID );
		// Next experiment
		hr = exp.GetNext();

		hItemFolder = NULL;
	}
	Progress::DisableProgress();

	pPS->Release();
}

//************************************
// Method:    RefreshGroups
// FullName:  CLeftView::RefreshGroups
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshGroups()
{
	Groups grp;
	if( !m_hGrp || !grp.IsValid() ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hGrp, TVGN_CHILD ), hItem2 = hItem;
	while( hItem )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// Groups
	CString szItem, szMsg;
	HRESULT hr = grp.ResetToStart();
	long nSteps = (long)grp.GetNumRecords(), nStep = 0;
	Progress::EnableProgress( nSteps );
	while( SUCCEEDED( hr ) )
	{
		grp.GetDescriptionWithID( szItem );

		nStep++;
		szMsg.Format( _T("Loading group %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		hItem = tree.InsertItem( szItem, IMG_GROUP, IMG_GROUP, m_hGrp, TVI_SORT );
		tree.SetItemData( hItem, TI_GROUP );

		hr = grp.GetNext();
	}
	Progress::DisableProgress();
}

//************************************
// Method:    RefreshSubjects
// FullName:  CLeftView::RefreshSubjects
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshSubjects()
{
	if( !m_hSub ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hSub, TVGN_CHILD ), hItem2 = hItem;
	while( hItem )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// Create Subject object
	Subjects subj;
	if( !subj.IsValid() ) return;

	// Subjects
	CString szItem, szMsg;
	HRESULT hr = subj.ResetToStart();
	long nSteps = (long)subj.GetNumRecords(), nStep = 0;
	Progress::EnableProgress( nSteps );
	while( SUCCEEDED( hr ) )
	{
		subj.GetNameWithCodeID( szItem, ::IsPrivacyOn() );

		nStep++;
		szMsg.Format( _T("Loading subject %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		hItem = tree.InsertItem( szItem, IMG_SUBJECT, IMG_SUBJECT, m_hSub, TVI_SORT );
		tree.SetItemData( hItem, TI_SUBJECT );

		hr = subj.GetNext();
	}
	Progress::DisableProgress();
}

//************************************
// Method:    RefreshConditions
// FullName:  CLeftView::RefreshConditions
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshConditions()
{
	if( !m_hCond ) return;

	NSConditions cond;
	if( !cond.IsValid() ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hCond, TVGN_CHILD ), hItem2 = hItem;
	while( hItem )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// Conditions
	CString szItem, szMsg;
	int nImage;
	BOOL fRecord, fProcess;
	HRESULT hr = cond.ResetToStart();
	long nSteps = (long)cond.GetNumRecords(), nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		cond.GetDescriptionWithID( szItem );
		cond.GetRecord( fRecord );
		cond.GetProcess( fProcess );

		nStep++;
		szMsg.Format( _T("Loading condition %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		nImage = IMG_CONDITION;
		if( fRecord && !fProcess ) nImage = IMG_CONDITION_RO;
		else if( fProcess && !fRecord ) nImage = IMG_CONDITION_PO;

		hItem = tree.InsertItem( szItem, nImage, nImage, m_hCond, TVI_SORT );
		tree.SetItemData( hItem, TI_CONDITION );

		hr = cond.GetNext();
	}
	Progress::DisableProgress();
}

//************************************
// Method:    RefreshStimuli
// FullName:  CLeftView::RefreshStimuli
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshStimuli()
{
	// Create Exp/Condition list object
	IStimulusElement* pSEL = NULL;
	HRESULT hr = CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
								   IID_IStimulusElement, (LPVOID*)&pSEL );
	if( FAILED( hr ) ) return;

	Stimuluss stim;
	Elements elem;
	if( !m_hStim || !stim.IsValid() || !stim.IsValid() ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hStim, TVGN_CHILD ), hItem2 = hItem;
	while( hItem )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// Stimuli
	BSTR bstrElem = NULL;
	HTREEITEM hItemElems = NULL, hItemElem = NULL;
	HRESULT hr2;
	CString szItem, szIDs, szElemID, szStimID, szMsg;
	hr = stim.ResetToStart();
	long nSteps = (long)stim.GetNumRecords(), nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		stim.GetDescriptionWithID( szItem );
		stim.GetID( szIDs );

		nStep++;
		szMsg.Format( _T("Loading stimulus %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		hItem = tree.InsertItem( szItem, IMG_STIMULUS, IMG_STIMULUS, m_hStim, TVI_SORT );
		tree.SetItemData( hItem, TI_STIMULUS );

		// Now add elements to stimulus
		szItem.Format( _T("%s Elements"), szIDs );
		hItemElems = tree.InsertItem( szItem, IMG_FOLDER, IMG_FOLDER_OPEN, hItem );
		tree.SetItemData( hItemElems, TI_ELEMENT_FOLDER );

		hr2 = pSEL->FindForStimulus( szIDs.AllocSysString() );
		while( SUCCEEDED( hr2 ) )
		{
			pSEL->get_ElementID( &bstrElem );
			szElemID = bstrElem;
			pSEL->get_StimulusID( &bstrElem );
			szStimID = bstrElem;

			if( szStimID == szIDs )
			{
				// Get data
				hr2 = elem.Find( szElemID );
				if( SUCCEEDED( hr2 ) )
				{
					elem.GetDescriptionWithID( szItem );

					hItemElem = tree.InsertItem( szItem, IMG_ELEMENT, IMG_ELEMENT, hItemElems, TVI_SORT );
					tree.SetItemData( hItemElem, TI_ELEMENT );
				}
			}

			// Next condition
			hr2 = pSEL->GetNext();
		}

		// Next stimulus
		hr = stim.GetNext();
	}
	Progress::DisableProgress();

	pSEL->Release();
}

struct TreeCat
{
	TreeCat() : hItem( NULL ) {}
	CString	szIDs;
	CString	szDesc;
	HTREEITEM	hItem;
};

//************************************
// Method:    FindTreeCat
// FullName:  FindTreeCat
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: CString szIDs
// Parameter: TreeCat * pTC
// Parameter: int nCnt
//************************************
int FindTreeCat( CString szIDs, TreeCat* pTC, int nCnt )
{
	if( !pTC || ( nCnt <= 0 ) ) return -1;

	int nIdx = -1, nItem = 0;
	while( nItem < nCnt )
	{
		if( pTC[ nItem ].szIDs == szIDs )
		{
			nIdx = nItem;
			break;
		}

		nItem++;
	}

	return nIdx;
}

//************************************
// Method:    RefreshElements
// FullName:  CLeftView::RefreshElements
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshElements()
{
	Elements elem;
	if( !m_hElmt || !elem.IsValid() ) return;
	Cats cat;
	if( !cat.IsValid() ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hElmt, TVGN_CHILD ), hItem2 = hItem;
	while( hItem2 )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// CATEGORIES
	// we have to do all the mumbo jumbo below to ensure we reuse existing category folders
	// ...
	// count for element
	int nCnt = 0;
	CatType ct;
	HRESULT hr = cat.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		cat.GetType( ct );
		if( ct == eCT_Element ) nCnt++;
		hr = cat.GetNext();
	}
	// how many structures depends on count
	TreeCat* pTC = NULL;
	if( nCnt > 0 ) pTC = new TreeCat[ nCnt ];
	// fill category structure
	int nItem = 0;
	CString szIDs, szItem, szMsg;
	hr = cat.ResetToStart();
	while( pTC && SUCCEEDED( hr ) )
	{
		cat.GetType( ct );
		if( ct == eCT_Element )
		{
			cat.GetID( szIDs );
			cat.GetDescription( szItem );
			pTC[ nItem ].szIDs = szIDs;
			pTC[ nItem ].szDesc = szItem;

			nItem++;
		}
		hr = cat.GetNext();
	}

	// Elements
	hr = elem.ResetToStart();
	long nSteps = (long)elem.GetNumRecords(), nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		hItem2 = m_hElmt;
		// do we have a category?
		elem.GetCatID( szIDs );

		nStep++;
		szMsg.Format( _T("Loading element %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		nItem = FindTreeCat( szIDs, pTC, nCnt );
		// if we do, has the folder been created yet?
		if( pTC && ( nItem != -1 ) )
		{
			if( !pTC[ nItem ].hItem )
			{
				pTC[ nItem ].hItem = tree.InsertItem( pTC[ nItem ].szDesc,
													  IMG_FOLDER_RED, IMG_FOLDER_RED_OPEN,
													  m_hElmt, TVI_SORT );
				tree.SetItemData( pTC[ nItem ].hItem, TI_CATEGORY_HOLDER );
			}
			hItem2 = pTC[ nItem ].hItem;
		}

		// insert element
		elem.GetDescriptionWithID( szItem );
		hItem = tree.InsertItem( szItem, IMG_ELEMENT, IMG_ELEMENT, hItem2, TVI_SORT );
		tree.SetItemData( hItem, TI_ELEMENT );

		hr = elem.GetNext();
	}
	Progress::DisableProgress();

	if( pTC ) delete [] pTC;
}

//************************************
// Method:    RefreshCategories
// FullName:  CLeftView::RefreshCategories
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshCategories()
{
	Cats cat;
	if( !m_hCat || !cat.IsValid() ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hCat, TVGN_CHILD ), hItem2 = hItem;
	while( hItem2 )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// insert categories
	CString szItem, szMsg;
	HRESULT hr = cat.ResetToStart();
	long nSteps = (long)cat.GetNumRecords(), nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		cat.GetDescriptionWithID( szItem );

		nStep++;
		szMsg.Format( _T("Loading category %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		hItem = tree.InsertItem( szItem, IMG_CATEGORY, IMG_CATEGORY, m_hCat, TVI_SORT );
		tree.SetItemData( hItem, TI_CATEGORY );

		hr = cat.GetNext();
	}
	Progress::DisableProgress();
}

//************************************
// Method:    RefreshFeedbacks
// FullName:  CLeftView::RefreshFeedbacks
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshFeedbacks()
{
	Feedbacks fb;
	if( !m_hFb || !fb.IsValid() ) return;

	// Remove first
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetNextItem( m_hFb, TVGN_CHILD ), hItem2 = hItem;
	while( hItem2 )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	// insert feedback
	CString szItem, szMsg;
	HRESULT hr = fb.ResetToStart();
	long nSteps = (long)fb.GetNumRecords(), nStep = 0;
	while( SUCCEEDED( hr ) )
	{
		fb.GetDescriptionWithID( szItem );

		nStep++;
		szMsg.Format( _T("Loading feedback %s..."), szItem );
		Progress::SetProgress( nStep, szMsg );

		hItem = tree.InsertItem( szItem, IMG_FEEDBACK, IMG_FEEDBACK, m_hFb, TVI_SORT );
		tree.SetItemData( hItem, TI_FEEDBACK );

		hr = fb.GetNext();
	}
	Progress::DisableProgress();
}

/////////////////////////////////////////////////////////////////////////////
// Tree View Double-Click Message Handler
/////////////////////////////////////////////////////////////////////////////

//************************************
// Method:    OnDblclk
// FullName:  CLeftView::OnDblclk
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NMHDR * pNMHDR
// Parameter: LRESULT * pResult
//************************************
void CLeftView::OnDblclk(NMHDR*, LRESULT* pResult) 
{
	// Selected item
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return;

	// Check cursor location to ensure we are not double-clicking a +/-
	CPoint pt;
	::GetCursorPos( &pt );
	tree.ScreenToClient( &pt );
	UINT uFlags;
	HTREEITEM hItemCur = tree.HitTest( pt, &uFlags );
	if( hItemCur != hItem ) return;

	// Type of selected item
	DWORD dwType = tree.GetItemData( hItem );
	// Does this item have children
	BOOL fHasChildren = tree.ItemHasChildren( hItem );
	BOOL fChildHasChildren = FALSE;
	if( fHasChildren )
	{
		HTREEITEM hItemChild = tree.GetChildItem( hItem );
		fChildHasChildren = tree.ItemHasChildren( hItemChild );
	}

	CString szData;
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	CString szOrigItem = szItem;
	int nPos = szItem.Find( szDelim );
	if( nPos != -1 ) szItem = szItem.Mid( nPos + 2 );

	BOOL fEdit = FALSE;
	HTREEITEM hParent = tree.GetParentItem( hItem );
	if( tree.GetParentItem( hParent ) == NULL ) fEdit = TRUE;

	*pResult = 0;
	switch( dwType )
	{
		case TI_EXPERIMENT_FOLDER:
			szData.LoadString( IDS_AL_EF );
			OutputAMessage( szData );
			break;
		case TI_GROUP_FOLDER:
			szData.LoadString( IDS_AL_GF );
			OutputAMessage( szData );
			break;
		case TI_SUBJECT_FOLDER:
			szData.LoadString( IDS_AL_SF );
			OutputAMessage( szData );
			break;
		case TI_CONDITION_FOLDER:
			szData.LoadString( IDS_AL_CF );
			OutputAMessage( szData );
			break;
		case TI_STIMULUS_FOLDER:
			szData = _T("DC: Stimulus Folder.");
			OutputAMessage( szData );
			break;
		case TI_ELEMENT_FOLDER:
			szData = _T("DC: Element Folder.");
			OutputAMessage( szData );
			break;
		case TI_EXPERIMENT:
			szData.Format( IDS_AL_EXP, szItem );
			OutputAMessage( szData );
			break;
		case TI_GROUP:
			szData.Format( IDS_AL_GRP, szItem );
			OutputAMessage( szData );
			if( fEdit ) OnGEdit();
			break;
		case TI_SUBJECT:
			szData.Format( IDS_AL_SUBJ, szItem );
			OutputAMessage( szData );
			if( fEdit ) OnSEdit();
			else if( !fChildHasChildren ) DoSTrials( TRUE );
			*pResult = 1;
			break;
		case TI_CONDITION:
			szData.Format( IDS_AL_COND, szItem );
			OutputAMessage( szData );
			if( fEdit ) OnCEdit();
			break;
		case TI_TRIAL:
			OnTChart();
			break;
		case TI_STIMULUS:
			szData.Format( _T("DC: Stimulus %s."), szItem );
			OutputAMessage( szData );
			break;
		case TI_ELEMENT:
			szData.Format( _T("DC: Element %s."), szItem );
			OutputAMessage( szData );
			if( fEdit ) OnElEdit();
			break;
		case TI_CATEGORY:
			szData.Format( _T("DC: Category %s."), szItem );
			OutputAMessage( szData );
			if( fEdit ) OnCatEdit();
			break;
		case TI_FEEDBACK:
			szData.Format( _T("DC: Feedback %s."), szItem );
			OutputAMessage( szData );
			if( fEdit ) OnFbEdit();
			break;
		case TI_IMAGE:
		{
			BOOL fQuick = FALSE;
			CString szPath, szTemp, szFile;
			if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
			szFile.Format( _T("%s\\%s"), szPath, szTemp );
			szFile = szFile.Left( szFile.GetLength() - 4 );
			szFile += _T("-F.PCX");
			CFileFind f;
			if( f.FindFile( szFile ) ) OnIViewF();
			else OnIView();
			break;
		}
		case TI_HWRTRIAL_FOLDER: case TI_FRDTRIAL_FOLDER: case TI_IMAGE_FOLDER:
			if( !fHasChildren && ( _ActionState == ACTION_NONE ) )
			{
				if( hParent )
				{
					tree.SelectItem( hParent );
					DoSTrials( FALSE );
					// folder was deleted, so need to get the true pointer back
					hItem = Find( hParent, szOrigItem );
					if( hItem )
					{
						tree.SelectItem( hItem );
						tree.Expand( hItem, TVE_EXPAND );
					}
				}
				*pResult = 1;
			}
			break;
		default:
			*pResult = 0;
	}
}

//************************************
// Method:    OnUpdateEfAdd
// FullName:  CLeftView::OnUpdateEfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEfAdd(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEfFind
// FullName:  CLeftView::OnUpdateEfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEfFind(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEfImport
// FullName:  CLeftView::OnUpdateEfImport
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEfImport(CCmdUI* pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEEdit
// FullName:  CLeftView::OnUpdateEEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEEdit(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEDelete
// FullName:  CLeftView::OnUpdateEDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEAddcondition
// FullName:  CLeftView::OnUpdateEAddcondition
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAddcondition(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEAddgroup
// FullName:  CLeftView::OnUpdateEAddgroup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAddgroup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateESum
// FullName:  CLeftView::OnUpdateESum
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateESum(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGfAdd
// FullName:  CLeftView::OnUpdateGfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGfAdd(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );	// Experiment ?
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGfFind
// FullName:  CLeftView::OnUpdateGfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGfFind(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGfAddgroups
// FullName:  CLeftView::OnUpdateGfAddgroups
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGfAddgroups(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGEdit
// FullName:  CLeftView::OnUpdateGEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGEdit(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGDelete
// FullName:  CLeftView::OnUpdateGDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGAddsubj
// FullName:  CLeftView::OnUpdateGAddsubj
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGAddsubj(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGRemove
// FullName:  CLeftView::OnUpdateGRemove
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGRemove(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSDelete
// FullName:  CLeftView::OnUpdateSDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSfAdd
// FullName:  CLeftView::OnUpdateSfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSfAdd(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSfAddsubjects
// FullName:  CLeftView::OnUpdateSfAddsubjects
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSfAddsubjects(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSfFind
// FullName:  CLeftView::OnUpdateSfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSfFind(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	pCmdUI->Enable( dwType == TI_SUBJECT_FOLDER );
}

//************************************
// Method:    OnUpdateSEdit
// FullName:  CLeftView::OnUpdateSEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSEdit(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSExp
// FullName:  CLeftView::OnUpdateSExp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSExp(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSRemove
// FullName:  CLeftView::OnUpdateSRemove
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSRemove(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCfAdd
// FullName:  CLeftView::OnUpdateCfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCfAdd(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCfAddconditions
// FullName:  CLeftView::OnUpdateCfAddconditions
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCfAddconditions(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCfFind
// FullName:  CLeftView::OnUpdateCfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCfFind(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCDelete
// FullName:  CLeftView::OnUpdateCDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCEdit
// FullName:  CLeftView::OnUpdateCEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCEdit(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCRemove
// FullName:  CLeftView::OnUpdateCRemove
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCRemove(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCReps
// FullName:  CLeftView::OnUpdateCReps
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCReps(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCRelationships
// FullName:  CLeftView::OnUpdateCRelationships
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCRelationships(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCRelationships
// FullName:  CLeftView::OnCRelationships
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCRelationships() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );

	RelationshipDlg d( this, REL_CONDITION, szItem );
	d.DoModal();
}

//************************************
// Method:    OnUpdateGRelationships
// FullName:  CLeftView::OnUpdateGRelationships
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGRelationships(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnGRelationships
// FullName:  CLeftView::OnGRelationships
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGRelationships() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );

	RelationshipDlg d( this, REL_GROUP, szItem );
	d.DoModal();
}

//************************************
// Method:    OnUpdateSRelationships
// FullName:  CLeftView::OnUpdateSRelationships
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSRelationships(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSRelationships
// FullName:  CLeftView::OnSRelationships
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSRelationships() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );

	RelationshipDlg d( this, REL_SUBJECT, szItem );
	d.DoModal();
}

//************************************
// Method:    OnBegindrag
// FullName:  CLeftView::OnBegindrag
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NMHDR * pNMHDR
// Parameter: LRESULT * pResult
//************************************
void CLeftView::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	CTreeCtrl& tree = GetTreeCtrl();

	CPoint ptAction;
	UINT nFlags;

	// Get drag item
	GetCursorPos( &ptAction );
	ScreenToClient( &ptAction );
	ASSERT( !m_fDragging );

	// If folder no go
	m_hitemDrag = tree.HitTest( ptAction, &nFlags );
	DWORD dwType = m_hitemDrag ? tree.GetItemData( m_hitemDrag ) : TI_UNKNOWN;
	switch( dwType )
	{
		case TI_UNKNOWN:
		case TI_EXPERIMENT_FOLDER:
		case TI_EXPERIMENT:
		case TI_GROUP_FOLDER:
		case TI_SUBJECT_FOLDER:
		case TI_CONDITION_FOLDER:
		case TI_STIMULUS_FOLDER:
		case TI_ELEMENT_FOLDER:
			return;
	}

	// If not in master list then no go
	HTREEITEM hItem = tree.GetParentItem( m_hitemDrag );	// Object root folder
	hItem = tree.GetParentItem( hItem );					// Should be null (or root)
	if( hItem != NULL ) return;

	m_fDragging = TRUE;
	ASSERT( m_pImageList == NULL );
	m_pImageList = tree.CreateDragImage( m_hitemDrag );
	m_pImageList->DragShowNolock( TRUE );
	m_pImageList->SetDragCursorImage( 0, CPoint( 0, 0 ) );
	m_pImageList->BeginDrag( 0, CPoint( 0, 0 ) );
	m_pImageList->DragMove( ptAction );
	m_pImageList->DragEnter( &tree, ptAction );
	SetCapture();

	*pResult = 0;
}

//************************************
// Method:    OnDroppedExternal
// FullName:  CLeftView::OnDroppedExternal
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: HTREEITEM hItem
// Parameter: CString szFile
// Parameter: BOOL fProcess
// Parameter: BOOL fMsg
//************************************
void CLeftView::OnDroppedExternal( HTREEITEM hItem, CString szFile, BOOL fProcess, BOOL fMsg )
{
	CString szMsg;
	// tree
	CTreeCtrl& tree = GetTreeCtrl();
	// item type -- if not subject, exit
	DWORD dwType = tree.GetItemData( hItem );
	if( dwType != TI_SUBJECT ) return;
	// is this an exp subj
	HTREEITEM hParent = tree.GetParentItem( hItem );
	if( !hParent ) return;
	hParent = tree.GetParentItem( hParent );
	if( !hParent ) return;

	// select item in tree
	SelectItem( hItem );

	// Which subject?
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which experiment/group
	hItem = tree.GetParentItem( hItem );		// Subject folder
	hItem = tree.GetParentItem( hItem );		// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );		// Group folder
	hItem = tree.GetParentItem( hItem );		// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	// file extension
	CString szExt = szFile.Right( 3 );
	szExt.MakeUpper();
	BOOL fNonPCX = TRUE, fRaw = FALSE, fGripper = FALSE, fCSV = FALSE;
	dwType = CXIMAGE_FORMAT_PCX;
	if( szExt == _T("BMP") ) dwType = CXIMAGE_FORMAT_BMP;
	else if( szExt == _T("PNG") ) dwType = CXIMAGE_FORMAT_PNG;
	else if( szExt == _T("GIF") ) dwType = CXIMAGE_FORMAT_GIF;
	else if( szExt == _T("JPG") ) dwType = CXIMAGE_FORMAT_JPG;
	else if( szExt == _T("PCX") ) fNonPCX = FALSE;
	else if( szExt == _T("HWR") ) fRaw = TRUE;
	else if( szExt == _T("FRD") ) fRaw = fGripper = TRUE;
	else if( szExt == _T("CSV") ) fRaw = fCSV = TRUE;

	// verify experiment type
	short nExpType = EXP_TYPE_HANDWRITING;
	{
		Experiments exp;
		if( FAILED( exp.Find( szExp ) ) ) return;
		exp.GetType( nExpType );
		if( !fRaw && ( nExpType != EXP_TYPE_IMAGE ) )
		{
			if( fMsg ) BCGPMessageBox( _T("You can only drop image files on image experiments.") );
			else
			{
				szMsg.Format( _T("%s is an image file which can only be dropped on image experiments."), szFile );
				OutputAMessage( szMsg, TRUE );
			}
			return;
		}
		else if( fRaw && fGripper && ( nExpType != EXP_TYPE_GRIPPER ) )
		{
			if( fMsg ) BCGPMessageBox( _T("You can only drop FRD files on grip-force experiments.") );
			else
			{
				szMsg.Format( _T("%s is a GripAlyzeR file which can only be dropped on grip-force experiments."), szFile );
				OutputAMessage( szMsg, TRUE );
			}
			return;
		}
		else if( fRaw && !fGripper && ( nExpType != EXP_TYPE_HANDWRITING ) )
		{
			if( fMsg ) BCGPMessageBox( _T("You can only drop HWR files on handwriting experiments.") );
			else
			{
				szMsg.Format( _T("%s is a handwriting file which can only be dropped on handwriting experiments."), szFile );
				OutputAMessage( szMsg, TRUE );
			}
			return;
		}
	}

	// IF IMAGE
	CxImage img( szFile, dwType );
	if( !fRaw )
	{
		// verify image
		if( !img.IsValid() )
		{
			if( fMsg ) BCGPMessageBox( _T("This is not a valid image file.") );
			else
			{
				szMsg.Format( _T("%s is not a valid image file."), szFile );
				OutputAMessage( szMsg, TRUE );
			}
			return;
		}

		// verify resolution/grayscale
		long lXRes = img.GetXDPI();
		bool fGrayScale = img.IsGrayScale();
		if( lXRes < 300 )
		{
			if( fMsg )
			{
				if( BCGPMessageBox( _T("Image resolution is below 300 dpi.\nProcessing can be inaccurate.\n\nContinue?"), MB_YESNO ) == IDNO )
					return;
			}
			else
			{
				szMsg.Format( _T("%s image resolution < 300 dpi. Processing can be inaccurate."), szFile );
				OutputAMessage( szMsg, TRUE );
			}
		}
		if( !fGrayScale )
		{
			if( fMsg )
			{
// 14Apr10: GMB: Removed, as it seems unimportant to inform user...just decrease the color depth
// 				if( BCGPMessageBox( _T("Image is not gray scale.\nProcessing can be inaccurate.\n\nContinue with color depth adjustment?"), MB_YESNO ) == IDNO )
// 					return;
				img.DecreaseBpp( 8, TRUE );
			}
			else
			{
				szMsg.Format( _T("%s image is not gray scale. Adjusting color depth to 8bpp."), szFile );
				OutputAMessage( szMsg, TRUE );
				img.DecreaseBpp( 8, TRUE );
			}
		}
	}

	// Get a condition
	CStringList existing;
	{
		CString szItem;
		NSConditions cond;
		HRESULT hr = cond.ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			cond.GetID( szItem );
			if( FAILED( m_pECL->Find( szExp.AllocSysString(), szItem.AllocSysString() ) ) )
				existing.AddTail( szItem );
			hr = cond.GetNext();
		}
	}

	CString szCond;
	AddConditionDlg d( &existing, this, TRUE, TRUE );
	if( CLeftView::m_szDropCondID == _T("") )
	{
		if( d.DoModal() == IDOK )
		{
			POSITION pos = d.m_lstNew.GetHeadPosition();
			if( pos )
			{
				szCond = d.m_lstNew.GetNext( pos );
				nPos = szCond.Find( szDelim );
				if( nPos == -1 ) return;

				szCond = szCond.Mid( nPos + 2 );
				TRIM( szCond );
			}
			else return;
		}
		else return;
	}
	else szCond = CLeftView::m_szDropCondID;

	// NAME THE FILE ACCORDINGLY
	if( fCSV )
	{
		if( nExpType == EXP_TYPE_GRIPPER )
		{
			szExt = _T("FRD");
			fGripper = TRUE;
		}
		else szExt = _T("HWR");
	}
	else if( !fRaw ) szExt = _T("PCX");
	// search for the highest trial #
	CString szFileNameMask;
	szFileNameMask.Format( _T("%s%s%s%s*.%s"), szExp, szGrp, szSubj, szCond, szExt );
	CString szPath, szFileName;
	::GetDataPathRoot( szPath );
	szFileName.Format( _T("%s\\%s\\%s\\%s\\%s"), szPath, szExp, szGrp, szSubj, szFileNameMask );
	int nTrial = 1;
	CFileFind ff;
	if( ff.FindFile( szFileName ) )
	{
		CStringList lstFiles;
		BOOL fCont = TRUE;
		while( fCont )
		{
			fCont = ff.FindNextFile();
			szFileName = ff.GetFilePath();
			lstFiles.AddTail( szFileName );
		}
		::SortStringListAlpha( &lstFiles );
		szFileName = lstFiles.GetTail();
		szFileName = szFileName.Left( szFileName.GetLength() - 4 );
		int nThisTrial = atoi( szFileName.Right( 2 ) );
		nThisTrial++;
		if( nThisTrial > nTrial ) nTrial = nThisTrial;
	}
	// create new filename with path
	if( !fRaw )
	{
		if( nTrial >= 10 )
			szFileName.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%d.PCX"), szPath, szExp,
							  szGrp, szSubj, szExp, szGrp, szSubj, szCond, nTrial );
		else
			szFileName.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s0%d.PCX"), szPath, szExp,
							   szGrp, szSubj, szExp, szGrp, szSubj, szCond, nTrial );
	}
	else
	{
		if( nTrial >= 10 )
			szFileName.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%d.%s"), szPath, szExp,
							   szGrp, szSubj, szExp, szGrp, szSubj, szCond, nTrial, szExt );
		else
			szFileName.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s0%d.%s"), szPath, szExp,
							   szGrp, szSubj, szExp, szGrp, szSubj, szCond, nTrial, szExt );
	}

	// If a CSV, we need to import it first
	if( fCSV )
	{
		CString szAppPath;
		::GetAppPath( szAppPath );
		char szObj[ 500 ];
		strcpy_s( szObj, 500, szAppPath );
		strcat_s( szObj, 500, "CSV.DRV" );
		HINSTANCE hMod = LoadLibrary( szObj );
		if( hMod < (HINSTANCE)HINSTANCE_ERROR )
		{
			if( fMsg ) BCGPMessageBox( _T("Unable to load CSV import driver."), MB_ICONERROR );
			else OutputAMessage( _T("Unable to load CSV import driver."), TRUE );
			return;
		}

		// get proc address to import procedure
		typedef LONG (CDECL* LPFNIMPORT) (HWND, LPSTR, LPSTR, int);
		LPFNIMPORT proc; 
		proc = (LPFNIMPORT)GetProcAddress( hMod, "Import" );
		if( !proc ) 
		{
			if( fMsg ) BCGPMessageBox( _T("Unable to load CSV import driver."), MB_ICONERROR );
			else OutputAMessage( _T("Unable to load CSV import driver."), TRUE );
			FreeLibrary( hMod );
			return;
		}

		// Do the import
		LONG lVal = (*proc)( m_hWnd, (char*)((LPCTSTR)szFile),(char*)((LPCTSTR)szFileName), 1 );

		// free the library
		FreeLibrary( hMod );

		if( lVal != 0L )
		{
			if( fMsg ) BCGPMessageBox( _T("Unable to import file using CSV driver.") );
			else
			{
				szMsg.Format( _T("%s could not be imported by CSV driver."), szFile );
				OutputAMessage( szMsg, TRUE );
			}
			return;
		}
	}

	// COPY FILE
	if( !::VerifyDirectory( szExp, szGrp, szSubj ) ) return;
	BOOL fRes = FALSE;
	if( fCSV ) fRes = TRUE;
	else if( fRaw || !fNonPCX ) fRes = CopyFile( szFile, szFileName, TRUE );
	else fRes = img.Save( szFileName, CXIMAGE_FORMAT_PCX );
	if( fRes )
	{
		szMsg.Format( _T("Imported the following file to exp=%s, group=%s, subject=%s, cond=%s: %s."),
					  szExp, szGrp, szSubj, szCond, szFileName );
		OutputAMessage( szMsg, TRUE );

		OnSShowtrials();
		m_szDropCondID = szCond;

		// Add notes for this trial
		CString szNotesFile = szFileName.Left( szFileName.GetLength() - 3 );
		szNotesFile += _T("TXT");
		if( ff.FindFile( szNotesFile ) ) REMOVE_FILE( szNotesFile );
		CStdioFile f;
		if( f.Open( szNotesFile, CFile::modeWrite | CFile::modeCreate ) )
		{
			CString szDateTime = (COleDateTime::GetCurrentTime()).Format( _T("%Y-%m-%d %H:%M") );
			ULONGLONG nFileLength = 0LL;
			if( ff.FindFile( szFile ) )
			{
				ff.FindNextFile();
				nFileLength = ff.GetLength();
			}

			CString szNotes;
			szNotes.Format( _T("%s %I64d %s"), szDateTime, nFileLength, szFile );
			f.WriteString( szNotes );
			f.WriteString( _T("\r\n") );
		}
		else BCGPMessageBox( _T("Unable to write notes file.") );

		// process the trial (unless specified not to)
		if( fRaw && fProcess )
		{
			// trial name
			int nPos = szFileName.ReverseFind( '\\' );
			CString szTrial = ( nPos != -1 ) ? szFileName.Mid( nPos + 1 ) : szFileName;

			// tree item
			HTREEITEM hItemT = FindTrial( szTrial, szExp );
			hItem = tree.GetParentItem( hItemT );	// Trial folder
			hItem = tree.GetParentItem( hItem );			// Subject

			// select in tree
			hItem = FindTrial( szTrial, szExp );
			tree.SelectItem( hItem );
			tree.EnsureVisible( hItem );

			// process
			OnTReprocess();

			// show
			OnTChart();
		}
		else if( fProcess ) // image
		{
			// experiment object for missing data value
			CString szDataPath;
			::GetDataPathRoot( szDataPath );
			Experiments exp;
			exp.SetDataPath( szDataPath );
			double dMissing = 0.;
			if( SUCCEEDED( exp.Find( szExp ) ) ) exp.GetMissingDataValue( dMissing );

			// Experiment Settings
			IProcSetting *pPS = NULL;
			HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
				IID_IProcSetting, (LPVOID*)&pPS );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( IDS_PS_ERR );
				return;
			}
			pPS->Find( szExp.AllocSysString() );
			// image processing settings
			short nSmoothIter = 0 , nInkThreshold = 0, nImgReso = 0;
			double dMinSegLen = 0.;
			pPS->Find( szExp.AllocSysString() );
			pPS->get_SmoothingIter( &nSmoothIter );
			pPS->get_InkThreshold( &nInkThreshold );
			pPS->get_ImgResolution( &nImgReso );
			pPS->get_MinStrokeSize( &dMinSegLen );
			pPS->Release();

			// trial name
			int nPos = szFileName.ReverseFind( '\\' );
			CString szTrial = ( nPos != -1 ) ? szFileName.Mid( nPos + 1 ) : szFileName;

			// select in tree
			hItem = FindTrial( szTrial, szExp );
			tree.SelectItem( hItem );
			tree.EnsureVisible( hItem );

			// trial #
			CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 6, 2 );

			// run SFINX
			BOOL fRet = SFINX::RunSFINX( szPath, szFileName, atoi( szTrialNum ), nSmoothIter,
										 nInkThreshold, nImgReso, dMinSegLen, dMissing );

			// display processed image
			if( fRet ) OnIViewF();
		}
	}
	else
	{
		DWORD dwLastError = GetLastError();
		if( fMsg ) BCGPMessageBox( _T("Error importing image(s).") );
		else
		{
			szMsg.Format( _T("Error importing file: %s"), szFileName );
			OutputAMessage( szMsg, TRUE );
		}
	}
}

//************************************
// Method:    OnButtonUp
// FullName:  CLeftView::OnButtonUp
// Access:    protected 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnButtonUp()
{
	if( m_fDragging )
	{
		CTreeCtrl& tree = GetTreeCtrl();

		ASSERT( m_pImageList != NULL );
		m_pImageList->DragLeave( &tree );
		m_pImageList->EndDrag();
		delete m_pImageList;
		m_pImageList = NULL;

		CString szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );

		// Process
		DWORD dwDrag = m_hitemDrag ? tree.GetItemData( m_hitemDrag ) : TI_UNKNOWN;
		DWORD dwDrop = m_hitemDrop ? tree.GetItemData( m_hitemDrop ) : TI_UNKNOWN;
		_DragID = m_hitemDrag ? tree.GetItemText( m_hitemDrag ) : _T("");
		CString szDrop = m_hitemDrop ? tree.GetItemText( m_hitemDrop ) : _T("");
		int nPos = szDrop.ReverseFind( szDelim[ 0 ] );
		if( nPos != -1 ) _DropID = szDrop.Mid( nPos + 2 );

		HTREEITEM hParent = tree.GetParentItem( m_hitemDrop );
		hParent = tree.GetParentItem( hParent );
		switch( dwDrag )
		{
			case TI_GROUP:
				if( dwDrop != TI_EXPERIMENT ) goto Cleanup;
				OnEAddgroup();
				break;
			case TI_SUBJECT:
				if( dwDrop != TI_GROUP ) goto Cleanup;
				if( hParent == NULL ) goto Cleanup;
				OnGAddsubj();
				break;
			case TI_CONDITION:
				if( dwDrop != TI_EXPERIMENT ) goto Cleanup;
				OnEAddcondition();
				break;
			case TI_ELEMENT:
				if( dwDrop != TI_STIMULUS ) goto Cleanup;
				OnStAddelements();
				break;
		}

Cleanup:
		// Cleanup
		ReleaseCapture();
		m_fDragging = FALSE;
		_DropID.Empty();
		_DragID.Empty();
		m_hitemDrag = NULL;
		m_hitemDrop = NULL;
		tree.SelectDropTarget( NULL );
	}
}

//************************************
// Method:    OnLButtonUp
// FullName:  CLeftView::OnLButtonUp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nFlags
// Parameter: CPoint point
//************************************
void CLeftView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	OnButtonUp();
	CTreeView::OnLButtonUp(nFlags, point);
}

//************************************
// Method:    OnRButtonUp
// FullName:  CLeftView::OnRButtonUp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nFlags
// Parameter: CPoint point
//************************************
void CLeftView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	OnButtonUp();
	CTreeView::OnRButtonUp(nFlags, point);
}

//************************************
// Method:    OnMouseMove
// FullName:  CLeftView::OnMouseMove
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: UINT nFlags
// Parameter: CPoint point
//************************************
void CLeftView::OnMouseMove(UINT nFlags, CPoint point) 
{
	if( m_fDragging )
	{
		ASSERT( m_pImageList != NULL );
		m_pImageList->DragMove( point );

		CTreeCtrl& tree = GetTreeCtrl();
		UINT flags;
		HTREEITEM hItem = tree.HitTest( point, &flags );
		DWORD dwDrop = hItem ? tree.GetItemData( hItem ) : TI_UNKNOWN;
		DWORD dwDrag = tree.GetItemData( m_hitemDrag );

		if( ( ( dwDrag == TI_GROUP ) && ( dwDrop == TI_EXPERIMENT ) ) ||
			( ( dwDrag == TI_CONDITION ) && ( dwDrop == TI_EXPERIMENT ) ) ||
			( ( dwDrag == TI_ELEMENT ) && ( dwDrop == TI_STIMULUS ) ) ||
			( ( dwDrag == TI_SUBJECT ) && ( dwDrop == TI_GROUP ) &&
			  ( tree.GetParentItem( tree.GetParentItem( hItem ) ) != NULL ) ) )
		{
			m_pImageList->DragLeave( &tree );
			tree.SelectDropTarget( hItem );
			m_hitemDrop = hItem;
			m_pImageList->DragEnter( &tree, point );
		}
		else m_hitemDrop = NULL;
	}
	else
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		if( pMF ) pMF->HandleMouseMoveForRecording();
	}

	CTreeView::OnMouseMove(nFlags, point);
}

//************************************
// Method:    OnMouseWheel
// FullName:  CLeftView::OnMouseWheel
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: UINT nFlags
// Parameter: short zDelta
// Parameter: CPoint pt
//************************************
BOOL CLeftView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	BOOL fChild = FALSE;

	switch( nFlags )
	{
		case MK_CONTROL:
		case MK_LBUTTON:
		case MK_RBUTTON:
		case MK_SHIFT:
			fChild = TRUE;
			break;
	}

	// What is currently selected (if nothing choose root)
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hCur = tree.GetSelectedItem();
	if( !hCur ) hCur = this->m_hExp;

	// Direction
	BOOL fBack = ( zDelta > 0 );

	// Check for existence of next/prev
	HTREEITEM hItem = NULL, hParent = NULL;
	if( fChild )
	{
		if( !fBack )
		{
			// If no child, get next sibling
			hItem = tree.GetChildItem( hCur );
			if( !hItem ) hItem = tree.GetNextItem( hCur, TVGN_NEXT );
			// If neither, keep trying to find next one
			if( !hItem )
			{
				hItem = hCur;
				do
				{
					// Parent
					hParent = tree.GetParentItem( hItem );
					if( hParent )
					{
						// Parent's sibling
						hItem = tree.GetNextItem( hParent, TVGN_NEXT );
						if( hItem ) break;
						else hItem = hParent;
					}
					else break;
				}
				while( hItem );
			}
		}
		else
		{
			// If no previous sibling, get parent
			hItem = tree.GetNextItem( hCur, TVGN_PREVIOUS );
			if( !hItem ) hItem = tree.GetParentItem( hCur );
		}
	}
	else	// sibling
	{
		if( !fBack ) hItem = tree.GetNextItem( hCur, TVGN_NEXT );
		else hItem = tree.GetNextItem( hCur, TVGN_PREVIOUS );
	}

	// Select item
	if( hItem ) tree.SelectItem( hItem );

	CString szData, szWho, szWhere;
	szWho.LoadString( fChild ? IDS_AL_WHEELCSP : IDS_AL_WHEELS );
	szWhere.LoadString( fBack ? IDS_UP : IDS_DOWN );
	szData.Format( IDS_AL_WHEEL, szWho, szWhere );
	OutputAMessage( szData );

	return CTreeView::OnMouseWheel(nFlags, zDelta, pt);
}

//************************************
// Method:    OnTView
// FullName:  CLeftView::OnTView
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTView() 
{
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );

	// adjust extension if image file
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return;
	DWORD dwType = tree.GetItemData( hItem );
	if( dwType == TI_IMAGE )
	{
		szFile = szFile.Left( szFile.GetLength() - 3 );
		szFile += _T("HWR");
	}

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CWaitCursor crs;
	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateTView
// FullName:  CLeftView::OnUpdateTView
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTView(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTNotes
// FullName:  CLeftView::OnTNotes
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTNotes() 
{
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );

	// adjust extension for notes
	szFile = szFile.Left( szFile.GetLength() - 3 );
	szFile += _T("TXT");

	// file name (w/o ext)
	TCHAR fname[ _MAX_FNAME ];
	_tsplitpath_s( szFile, NULL, 0, NULL, 0, fname, _MAX_FNAME, NULL, 0 );
	CString szName = fname;

	// open notes dialog
	NotesDlg d( N_TRIAL, szName, szFile, this );
	d.DoModal();
}

//************************************
// Method:    OnUpdateTNotes
// FullName:  CLeftView::OnUpdateTNotes
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTNotes(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTViewTf
// FullName:  CLeftView::OnTViewTf
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewTf() 
{
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szTemp = szTemp.Left( szTemp.GetLength() - 4 );
	szFile.Format( _T("%s\\%s.TF"), szPath, szTemp );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

// 	InstructionDlg d( this );
// 	d.m_fJustDisplay = TRUE;
// 	d.m_szFileName = szFile;
// 	CWaitCursor crs;
// 	d.DoModal();

	CWaitCursor crs;
	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateTViewTf
// FullName:  CLeftView::OnUpdateTViewTf
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTViewTf(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTChart
// FullName:  CLeftView::OnTChart
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChart()
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial, szTrialTF;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	HTREEITEM hItem = NULL;
	if( !fQuick )
	{
		// Which file?
		hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );
		szTrialTF = szTrial.Left( szTrial.GetLength() - 3 );
		szTrialTF += _T("TF");

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );

		hItem = tree.GetSelectedItem();
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
		szTrialTF = szTrial.Left( szTrial.GetLength() - 3 );
		szTrialTF += _T("TF");

		hItem = FindItem( szTemp, m_hExp );
		if( !hItem ) return;
	}

	// Get sampling rate from ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	double dRate = 200.0;
	short nPrs = 1, nVal = 0;
	if( SUCCEEDED( hr ) )
	{
		pPS->get_SamplingRate( &nVal );
		dRate = nVal;
		pPS->get_MinPenPressure( &nPrs );
	}
	pPS->Release();
	hr = m_pEML->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_SamplingRate( &nVal );
		if( nVal != 0 ) dRate = nVal;
	}

	// has trial passed consistency?
	BOOL fPassed = FALSE;
	int nImage = 0, nSelImage = 0;
	if( tree.GetItemImage( tree.GetSelectedItem(), nImage, nSelImage ) )
	{
		if( nImage == IMG_TRIAL_GOOD ) fPassed = TRUE;
	}

	CFileFind ff;
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	// Try TF file first
	GetTF( szExp, szGrp, szSubj, szTrialTF, szFile );
	if( pMF && !ff.FindFile( szFile ) )
	{
		// else HWR file
		GetHWR( szExp, szGrp, szSubj, szTrial, szFile );
		pMF->ChartHwr( szExp, szGrp, szSubj, szFile, dRate, nPrs );
	}
	else if( pMF ) pMF->ChartTf( szExp, szGrp, szSubj, szFile, dRate, nPrs, fPassed );
}

//************************************
// Method:    OnTChartHwr
// FullName:  CLeftView::OnTChartHwr
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChartHwr() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	// Get sampling rate & pen pressure from ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	double dRate = 200.0;
	short nPrs = 1, nVal = 0 ;
	if( SUCCEEDED( hr ) )
	{
		pPS->get_SamplingRate( &nVal );
		dRate = nVal;
		pPS->get_MinPenPressure( &nPrs );
	}
	pPS->Release();
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_SamplingRate( &nVal );
		if( nVal != 0 ) dRate = nVal;
	}

	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );
//	szFile.MakeUpper();
	CString szExt = szFile.Right( 3 );
	szExt.MakeUpper();
	if( szExt == _T("PCX") )
	{
		szFile = szFile.Left( szFile.GetLength() - 3 );
		szFile += _T("HWR");
		CFileFind ff;
		if( !ff.FindFile( szFile ) )
		{
			BCGPMessageBox( _T("The raw data file cannot be found.\nThe trial must be processed before you can view the raw data chart." ) );
			return;
		}
	}

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartHwr( szExp, szGrp, szSubj, szFile, dRate, nPrs );
}

//************************************
// Method:    OnUpdateTChart
// FullName:  CLeftView::OnUpdateTChart
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTChart(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTChartHwrRT
// FullName:  CLeftView::OnTChartHwrRT
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChartHwrRT() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	// Get sampling rate from ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	double dRate = 200.0;
	short nVal = 0;
	if( SUCCEEDED( hr ) )
	{
		pPS->get_SamplingRate( &nVal );
		dRate = nVal;
	}
	pPS->Release();
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_SamplingRate( &nVal );
		if( nVal != 0 ) dRate = nVal;
	}

	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartHwrRT( szExp, szGrp, szSubj, szFile, dRate );
}

//************************************
// Method:    OnTChartHwr3D
// FullName:  CLeftView::OnTChartHwr3D
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChartHwr3D() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	// Get sampling rate & pen pressure from ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	double dRate = 200.0;
	short nPrs = 1, nVal = 0;
	if( SUCCEEDED( hr ) )
	{
		pPS->get_SamplingRate( &nVal );
		dRate = nVal;
		pPS->get_MinPenPressure( &nPrs );
	}
	pPS->Release();
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_SamplingRate( &nVal );
		if( nVal != 0 ) dRate = nVal;
	}

	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartHwr3D( szExp, szGrp, szSubj, szFile, dRate );
}

//************************************
// Method:    OnTChartTfRT
// FullName:  CLeftView::OnTChartTfRT
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChartTfRT() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which trial?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );
		szTrial = szTrial.Left( szTrial.GetLength() - 3 );
		szTrial += _T("TF");

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
		szTrial = szTrial.Left( szTrial.GetLength() - 3 );
		szTrial += _T("TF");
	}

	// get sampling rate from subject, if set
	short nRate = 0;
	if( SUCCEEDED( m_pEML->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() ) ) )
	{
		m_pEML->get_SamplingRate( &nRate );
	}
	// Get sampling rate from ProcSettings object (if necessary)
	if( nRate == 0 )
	{
		IProcSetting* pPS = NULL;
		HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									   IID_IProcSetting, (LPVOID*)&pPS );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_PS_ERR );
			return;
		}
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) ) pPS->get_SamplingRate( &nRate );
		pPS->Release();
	}

	GetTF( szExp, szGrp, szSubj, szTrial, szFile );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartTfRT( szExp, szGrp, szSubj, szFile, (double)nRate );
}

//************************************
// Method:    OnTChartTf
// FullName:  CLeftView::OnTChartTf
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChartTf() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which trial?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );
		szTrial = szTrial.Left( szTrial.GetLength() - 3 );
		szTrial += _T("TF");

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
		szTrial = szTrial.Left( szTrial.GetLength() - 3 );
		szTrial += _T("TF");
	}

	// get sampling rate from subject, if set
	short nRate = 0;
	if( SUCCEEDED( m_pEML->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() ) ) )
	{
		m_pEML->get_SamplingRate( &nRate );
	}
	// Get sampling rate from ProcSettings object (if necessary)
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	short nPrs = 1;
	if( SUCCEEDED( hr ) )
	{
		if( nRate == 0 ) pPS->get_SamplingRate( &nRate );
		pPS->get_MinPenPressure( &nPrs );
	}
	pPS->Release();

	GetTF( szExp, szGrp, szSubj, szTrial, szFile );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartTf( szExp, szGrp, szSubj, szFile, (double)nRate, nPrs );
}

//************************************
// Method:    OnTChartTf3D
// FullName:  CLeftView::OnTChartTf3D
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTChartTf3D() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which trial?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );
		szTrial = szTrial.Left( szTrial.GetLength() - 3 );
		szTrial += _T("TF");

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
		szTrial = szTrial.Left( szTrial.GetLength() - 3 );
		szTrial += _T("TF");
	}

	// get sampling rate from subject, if set
	short nRate = 0;
	if( SUCCEEDED( m_pEML->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() ) ) )
	{
		m_pEML->get_SamplingRate( &nRate );
	}
	// Get sampling rate from ProcSettings object (if necessary)
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	short nPrs = 1;
	if( SUCCEEDED( hr ) )
	{
		if( nRate == 0 ) pPS->get_SamplingRate( &nRate );
		pPS->get_MinPenPressure( &nPrs );
	}
	pPS->Release();

	GetTF( szExp, szGrp, szSubj, szTrial, szFile );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartTf3D( szExp, szGrp, szSubj, szFile, (double)nRate );
}

//************************************
// Method:    OnTRedo
// FullName:  CLeftView::OnTRedo
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTRedo() 
{
	if( BCGPMessageBox( IDS_AL_WRITEOVER, MB_YESNO ) == IDNO ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which trial?
	HTREEITEM hTrial = tree.GetSelectedItem();			// Trial
	CString szTrial = tree.GetItemText( hTrial );

	// Which subject?
	HTREEITEM hItem = tree.GetParentItem( hTrial );		// Trial folder
	hItem = tree.GetParentItem( hItem );				// Subject
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which experiment/group
	hItem = tree.GetParentItem( hItem );				// Subject folder
	hItem = tree.GetParentItem( hItem );				// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );				// Group folder
	hItem = tree.GetParentItem( hItem );				// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->RerunTrial( szExp, szSubj, szGrp, szTrial, hTrial );
}

//************************************
// Method:    OnUpdateTRedo
// FullName:  CLeftView::OnUpdateTRedo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTRedo(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_TRIAL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    HideTrials
// FullName:  CLeftView::HideTrials
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExpID
//************************************
void CLeftView::HideTrials( CString szExpID, CString szGrpID, CString szSubjID )
{
	if( !m_hExp ) return;

	// if Exp is empty, do all experiments
	// if grp is empty, do all groups
	// if subj is empty, do all subjects
	// otherwise, hide the trials of the specified objects
	BOOL fAllExp = ( szExpID == _T("") );
	BOOL fAllGrp = ( szGrpID == _T("") );
	BOOL fAllSubj = ( szSubjID == _T("") );

	HTREEITEM hItemE = NULL, hItemG = NULL, hItemS = NULL;
	CString szExp, szGrp, szSubj, szDelim;
	DWORD dwType = 0;
	int nPos = 0;

	// delimiter
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// tree object
	CTreeCtrl& tree = GetTreeCtrl();
	// 1st experiment
	HTREEITEM hItemExp = tree.GetChildItem( m_hExp );
	while( hItemExp )
	{
		// get next item now to handle 'continue'
		hItemE = hItemExp;
		hItemExp = tree.GetNextSiblingItem( hItemExp );

		// ensure this is an experiment
		dwType = tree.GetItemData( hItemE );
		if( dwType != TI_EXPERIMENT ) continue;
		// if not all exps & cur exp = param, then process
		szExp = tree.GetItemText( hItemE ); 
		nPos = szExp.Find( szDelim );
		if( nPos == -1 ) continue;
		szExp = szExp.Mid( nPos + 2 );
		if( fAllExp || ( szExp == szExpID ) )
		{
			// get group folder
			HTREEITEM hItemGrp = tree.GetChildItem( hItemE );
			if( !hItemGrp ) continue;
			// get 1st group
			hItemGrp = tree.GetChildItem( hItemGrp );
			while( hItemGrp )
			{
				// get next item now to handle 'continue'
				hItemG = hItemGrp;
				hItemGrp = tree.GetNextSiblingItem( hItemGrp );

				// ensure this is a group
				dwType = tree.GetItemData( hItemG );
				if( dwType != TI_GROUP ) continue;
				// if not all grps & cur grp = param, then process
				szGrp = tree.GetItemText( hItemG );
				nPos = szGrp.Find( szDelim );
				if( nPos == -1 ) continue;
				szGrp = szGrp.Mid( nPos + 2 );
				if( fAllGrp || ( szGrp == szGrpID ) )
				{
					// get subject folder
					HTREEITEM hItemSubj = tree.GetChildItem( hItemG );
					if( !hItemSubj ) continue;
					// get 1st subj
					hItemSubj = tree.GetChildItem( hItemSubj );
					while( hItemSubj )
					{
						// get next item now to handle 'continue'
						hItemS = hItemSubj;
						hItemSubj = tree.GetNextSiblingItem( hItemSubj );

						// ensure this is a subject
						dwType = tree.GetItemData( hItemS );
						if( dwType != TI_SUBJECT ) continue;
						// if not all subjs & cur subj = param, then process
						szSubj = tree.GetItemText( hItemS );
						nPos = szSubj.Find( szDelim );
						if( nPos == -1 ) continue;
						szSubj = szSubj.Mid( nPos + 2 );
						if( fAllSubj || ( szSubj == szSubjID ) )
						{
							// get trial folder: if null, do nothing
							HTREEITEM hItemF = tree.GetChildItem( hItemS );
							if( !hItemF ) continue;
							// remove all trials
							// tree.DeleteItem( hItemF );
							HTREEITEM hItemT = tree.GetChildItem( hItemF );
							while( hItemT )
							{
								HTREEITEM hItemTNext = tree.GetNextSiblingItem( hItemT );
								tree.DeleteItem( hItemT );
								hItemT = hItemTNext;
							}
						}
					}
				}
			}
		}
	}
}

//************************************
// Method:    ProcessSubject
// FullName:  CLeftView::ProcessSubject
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: HTREEITEM hItem
// Parameter: CString szExpID
// Parameter: CString szGrpID
// Parameter: CString szSubjID
// Parameter: BOOL fReProcess
//************************************
void CLeftView::ProcessSubject( HTREEITEM hItem, CString szExpID, CString szGrpID,
								CString szSubjID, BOOL fReProcess, BOOL fSumm )
{
	// main frame & list view
 	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
 	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	// delimiter
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	// tree
	CTreeCtrl& tree = GetTreeCtrl();

	// Remove the trials (unless flag set)
	if( tree.ItemHasChildren( hItem ) && ::GetDetailedOutput() )
	{
		HTREEITEM hItem2 = tree.GetChildItem( hItem );
		if( hItem2 ) tree.DeleteItem( hItem2 );
	}

	// get subject sampling rate & device res
	double dDevRes = 0.;
	int nSampRate = 0;
	HRESULT hr = m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
							   szSubjID.AllocSysString() );
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_DeviceRes( &dDevRes );
		short nTemp = 0;
		m_pEML->get_SamplingRate( &nTemp );
		nSampRate = nTemp;
	}
	// if they are 0, use experiments
	if( ( dDevRes == 0. ) || ( nSampRate == 0 ) )
	{
		IProcSetting* pPS = NULL;
		hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								 IID_IProcSetting, (LPVOID*)&pPS );
		if( SUCCEEDED( hr ) )
		{
			pPS->Find( szExpID.AllocSysString() );
			pPS->get_DeviceResolution( &dDevRes );
			short nTemp = 0;
			pPS->get_SamplingRate( &nTemp );
			nSampRate = nTemp;

			pPS->Release();
		}
	}

	// reprocess
	_ActionState = ACTION_REPROCESS;
	_Reprocessing = TRUE;
// 	BOOL fWasVerbose = ::GetVerbosity();
// 	BOOL fWasDetailed = ::GetDetailedOutput();
// 	::SetVerbosity( TRUE );
// 	::SetDetailedOutput( FALSE );
	pMF->ReprocessSubject( szExpID, szSubjID, szGrpID, dDevRes, nSampRate, hItem, this,
						   pMsgWnd, 1, 1, TRUE, TRUE, FALSE, fReProcess );
// 	::SetVerbosity( fWasVerbose );
// 	::SetDetailedOutput( fWasDetailed );
	_Reprocessing = FALSE;
	_ActionState = ACTION_NONE;

	// Optionally delete summarization/analysis files
	CString szInc, szErr, szMsg, szTmp;
	CFileFind ff;
	GetExpINC( szExpID, szInc );
	GetExpERR( szExpID, szErr );
	if( fSumm && BCGPMessageBox( IDS_CLEANUP, MB_YESNO ) == IDYES )
	{
		if( ff.FindFile( szInc ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				return;
			}
			szMsg.Format( _T("Deleting file: %s"), szInc );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove TMP file
		szTmp = szInc.Left( szInc.GetLength() - 3 );
		szTmp += _T("tmp");
		if( ff.FindFile( szTmp ) )
		{
			REMOVE_FILE( szTmp );
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove ERR file
		if( ff.FindFile( szErr ) )
		{
			REMOVE_FILE( szErr );
			szMsg.Format( _T("Deleting file: %s"), szErr );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove AXS memory file
		szTmp = szInc.Left( szInc.GetLength() - 7 );	// exp.axs
		szTmp += _T("graph.axs");
		if( ff.FindFile( szTmp ) )
		{
			REMOVE_FILE( szTmp );
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}

		// summarize
		pMF->SummarizeExperiment( szExpID, pMsgWnd );
	}
}

//************************************
// Method:    OnSReprocess
// FullName:  CLeftView::OnSReprocess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSReprocess() 
{
	// delimiter
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );
	// Which experiment
	HTREEITEM hExp = tree.GetParentItem( hItem );	// Subject folder
	hExp = tree.GetParentItem( hExp );				// Group
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	CString szGrpID = szItem.Mid( nPos + 2 );
	hExp = tree.GetParentItem( hExp );				// Group folder
	hExp = tree.GetParentItem( hExp );				// Experiment
	szItem = tree.GetItemText( hExp ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	// reprocess subject
	ProcessSubject( hItem, szExpID, szGrpID, szSubjID, TRUE );
}

//************************************
// Method:    OnUpdateSReprocess
// FullName:  CLeftView::OnUpdateSReprocess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSReprocess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSProcess
// FullName:  CLeftView::OnSProcess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSProcess() 
{
	// delimiter
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );
	// Which experiment
	HTREEITEM hExp = tree.GetParentItem( hItem );	// Subject folder
	hExp = tree.GetParentItem( hExp );				// Group
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	CString szGrpID = szItem.Mid( nPos + 2 );
	hExp = tree.GetParentItem( hExp );				// Group folder
	hExp = tree.GetParentItem( hExp );				// Experiment
	szItem = tree.GetItemText( hExp ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	// reprocess subject
	ProcessSubject( hItem, szExpID, szGrpID, szSubjID, FALSE );
}

//************************************
// Method:    OnUpdateSProcess
// FullName:  CLeftView::OnUpdateSProcess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSProcess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEReprocess
// FullName:  CLeftView::OnEReprocess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEReprocess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_ReprocAll = TRUE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	HWND hWndStatus = pMF->m_wndStatusBar.m_hWnd;
	CWinThread* pThread = AfxBeginThread( ReprocessExperiment, hWndStatus );
}

//************************************
// Method:    OnUpdateEReprocess
// FullName:  CLeftView::OnUpdateEReprocess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEReprocess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= tree.ItemHasChildren( hItem );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEProcess
// FullName:  CLeftView::OnEProcess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEProcess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_ReprocAll = FALSE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	HWND hWndStatus = pMF->m_wndStatusBar.m_hWnd;
	CWinThread* pThread = AfxBeginThread( ReprocessExperiment, hWndStatus );
}

//************************************
// Method:    OnUpdateEProcess
// FullName:  CLeftView::OnUpdateEProcess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEProcess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= tree.ItemHasChildren( hItem );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    MoveAndFind
// FullName:  CLeftView::MoveAndFind
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szItem
// Parameter: BOOL fEnsureVisible
// Parameter: BOOL fFindParent
//************************************
BOOL CLeftView::MoveAndFind( CString szItem, BOOL fEnsureVisible, BOOL fFindParent )
{
	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = NULL;

	if( fFindParent )
	{
		if( szItem.GetLength() == 18 )
		{
			CString szExp, szGrp, szSubj, szTemp;
			szExp = szItem.Left( 3 );
			szGrp = szItem.Mid( 3, 3 );
			szSubj = szItem.Mid( 6, 3 );
			HTREEITEM hItemSubj = Find( szExp, szGrp, szSubj );
			if( hItemSubj )
			{
				hItem = tree.GetChildItem( hItemSubj );	// trial folder
				if( hItem )
				{
					// is expanded?
					if( !tree.ItemHasChildren( hItem ) )
					{
						DoSTrials( hItemSubj );
						hItem = tree.GetChildItem( hItemSubj );
						tree.Expand( hItem, TVE_EXPAND );
					}
					hItem = tree.GetChildItem( hItem );
				}
			}
		}
	}
	else
	{
		hItem = tree.GetSelectedItem();
		hItem = tree.GetParentItem( hItem );
		hItem = tree.GetChildItem( hItem );
	}

	BOOL fFound = FALSE;
	CString szNext;

	while( TRUE )
	{
		szNext = tree.GetItemText( hItem );
		if( szNext == szItem )
		{
			tree.SelectItem( hItem );
			if( fEnsureVisible ) tree.EnsureVisible( hItem );
			break;
		}
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
		if( !hItem ) break;
	}

	// has trial passed consistency?
	BOOL fPassed = FALSE;
	int nImage = 0, nSelImage = 0;
	if( hItem && tree.GetItemImage( hItem, nImage, nSelImage ) )
	{
		if( nImage == IMG_TRIAL_GOOD ) fPassed = TRUE;
	}

	return fPassed;
}

//************************************
// Method:    Questionnaire
// FullName:  CLeftView::Questionnaire
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
//************************************
void CLeftView::Questionnaire( CString szExp, CString szGrp, CString szSubj )
{
	ISQuestionnaire* pQ = NULL;
	HRESULT hr = CoCreateInstance( CLSID_SQuestionnaire, NULL, CLSCTX_ALL,
								   IID_ISQuestionnaire, (LPVOID*)&pQ );
	if( SUCCEEDED( hr ) )
	{
		CString szData;
		szData.Format( IDS_AL_QUEST, szSubj );
		OutputAMessage( szData );

		//  Master Questionnaire check
		IMQuestionnaire* pMQ = NULL;
		hr = CoCreateInstance( CLSID_MQuestionnaire, NULL, CLSCTX_ALL,
							   IID_IMQuestionnaire, (LPVOID*)&pMQ );
		if( SUCCEEDED( hr ) )
		{
CheckMaster:
			hr = pMQ->ResetToStart();
			if( FAILED( hr ) )
			{
				if( BCGPMessageBox( _T("There are no questions in the master questionnaire.\nWould you like to set it up?"), MB_YESNO ) == IDNO )
				{
					pMQ->Release();
					pQ->Release();
					return;
				}
				((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnViewQuest();
				goto CheckMaster;
			}

			pMQ->Release();
		}
		// Find questionnaire for subject
//		hr = pQ->FindForSubject( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );

		// If no questions yet, fill from group
//		if( FAILED( hr ) )
//		{
			IGQuestionnaire* pGQ = NULL;
			hr = CoCreateInstance( CLSID_GQuestionnaire, NULL, CLSCTX_ALL,
								   IID_IGQuestionnaire, (LPVOID*)&pGQ );
			if( FAILED( hr ) ) return;
SearchForGroup:
			hr = pGQ->FindForGroup( szExp.AllocSysString(), szGrp.AllocSysString() );
			if( FAILED( hr ) )
			{
				if( BCGPMessageBox( IDS_NOGRP_QUEST, MB_YESNO ) == IDYES )
				{
					GrpQuestionnaireDlg gqd( szExp, szGrp );
					if( gqd.DoModal() == IDOK ) goto SearchForGroup;
				}
				pQ->Release();
				pGQ->Release();
				return;
			}

			BSTR bstrID = NULL;
			CString szID2;
			CStringList ID;

			while( SUCCEEDED( hr ) )
			{
				pGQ->get_ID( &bstrID );
				szID2 = bstrID;
				ID.AddTail( szID2 );

				hr = pGQ->GetNext();
			}

			pGQ->Release();

			POSITION pos = ID.GetHeadPosition();
			while( pos )
			{
				szID2 = ID.GetNext( pos );
				if( FAILED( pQ->Find( szExp.AllocSysString(), szGrp.AllocSysString(),
									  szSubj.AllocSysString(), szID2.AllocSysString() ) ) )
				{
					pQ->put_ExperimentID( szExp.AllocSysString() );
					pQ->put_GroupID( szGrp.AllocSysString() );
					pQ->put_SubjectID( szSubj.AllocSysString() );
					pQ->put_ID( szID2.AllocSysString() );
					CString szA;
					pQ->put_Answer( szA.AllocSysString() );
					hr = pQ->Add();
					::DataHasChanged();
				}
			}

			hr = pQ->FindForSubject( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );
//		}

		SQuestionnaireFullDlg d( pQ, szExp, szGrp, this );
		if( d.DoModal() == IDOK )
		{
			// TODO: To avoid early complications, I'm simply deleting all for subject & re-adding
			if( SUCCEEDED( hr ) )
			{
				hr = pQ->ClearQuestionnaire( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );
				if( FAILED( hr ) )
				{
					BCGPMessageBox( IDS_Q_DELALLERR );
					pQ->Release();
					return;
				}
			}

			POSITION posID = d.m_lstID->GetHeadPosition();
			POSITION posA = d.m_lstA->GetHeadPosition();
			CString szIDs, szA;

			while( posID )
			{
				szIDs = d.m_lstID->GetNext( posID );
				szA = d.m_lstA->GetNext( posA );

				pQ->put_ExperimentID( szExp.AllocSysString() );
				pQ->put_GroupID( szGrp.AllocSysString() );
				pQ->put_SubjectID( szSubj.AllocSysString() );
				pQ->put_ID( szIDs.AllocSysString() );
				pQ->put_Answer( szA.AllocSysString() );
				hr = pQ->Add();
				if( FAILED( hr ) )
				{
					CString szTempMsg;
					szTempMsg.LoadString(IDS_ADD_ERR);
					BCGPMessageBox( szTempMsg+_T(" CLeftView::4") );
					break;
				}
				::DataHasChanged();
			}
		}

		pQ->Release();
	}
	else
	{
		CString szItem;
		szItem.Format( IDS_QUEST_ERR, hr );
		BCGPMessageBox( szItem );
	}
}

//************************************
// Method:    OnSQuest
// FullName:  CLeftView::OnSQuest
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSQuest() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get subject
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubj = szItem.Mid( nPos + 2 );

	// password check
	{
		Subjects subj;
		if( FAILED( subj.Find( szSubj ) ) )
		{
			BCGPMessageBox( _T("Unable to locate user in database.") );
			return;
		}
		CString szUserPass = ::GetUserPassword(), szPass;
		subj.GetPassword( szPass, FALSE );
		if( szPass == _T("") ) szPass = ::GetUserPassword();
		if( szPass != szUserPass )
		{
			if( !CheckPass( TRUE, szPass ) ) return;
		}
		else if( !CheckPass() ) return;
	}

	// Get group
	hItem = tree.GetParentItem( hItem );	// Subjets folder
	hItem = tree.GetParentItem( hItem );	// Group
	szItem = tree.GetItemText( hItem );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// Get experiment
	HTREEITEM hExp = tree.GetParentItem( hItem );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	Questionnaire( szExp, szGrp, szSubj );
}

//************************************
// Method:    OnUpdateSQuest
// FullName:  CLeftView::OnUpdateSQuest
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSQuest(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnGQuest
// FullName:  CLeftView::OnGQuest
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGQuest() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return;
	DWORD dwType = tree.GetItemData( hItem );

	if( dwType != TI_GROUP ) return;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrpID = szItem.Mid( nPos + 2 );
	if( szGrpID == _T("") ) return;

	HTREEITEM hExp = tree.GetParentItem( hItem );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	GrpQuestionnaireDlg d( szExp, szGrpID, this );
	d.DoModal();
}

//************************************
// Method:    OnUpdateGQuest
// FullName:  CLeftView::OnUpdateGQuest
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGQuest(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEClean
// FullName:  CLeftView::OnEClean
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEClean() 
{
	ExpCleanupDlg d( this );
	if( d.DoModal() == IDCANCEL ) return;

	if( BCGPMessageBox( _T("Deleting process files cannot be undone.\n\nContinue?"), MB_YESNO ) == IDNO ) return;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment ID
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	CString szExp = szItem.Mid( nPos + 2 );

	CString szData;
	szData.Format( IDS_AL_CLEAN, szExp );
	OutputAMessage( szData, TRUE );

	// REMOVE FILES ACCORDINGLY
	CString szPath;
	::GetDataPathRoot( szPath );
	Experiments::CleanProcessFiles( szPath, szExp, d.m_fHWR, d.m_fTF,
									d.m_fSeg, d.m_fExt, d.m_fCon, d.m_fErr, d.m_fInc );
}

//************************************
// Method:    OnUpdateEClean
// FullName:  CLeftView::OnUpdateEClean
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEClean(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !_Reprocessing;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSClean
// FullName:  CLeftView::OnSClean
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSClean() 
{
	ExpCleanupDlg d( this, TRUE );
	if( d.DoModal() == IDCANCEL ) return;

	if( BCGPMessageBox( _T("Deleting process files cannot be undone.\n\nContinue?"), MB_YESNO ) == IDNO ) return;

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get exp, grp, subj ID
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );		// Subject
	int nPos = szItem.Find( szDelim );
	CString szSubj = szItem.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );			// Subject folder
	hItem = tree.GetParentItem( hItem );			// Group
	szItem = tree.GetItemText( hItem );
	nPos = szItem.Find( szDelim );
	CString szGrp = szItem.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );			// Group folder
	hItem = tree.GetParentItem( hItem );			// Experiment
	szItem = tree.GetItemText( hItem );
	nPos = szItem.Find( szDelim );
	CString szExp = szItem.Mid( nPos + 2 );

	CString szData;
	szData.Format( IDS_AL_CLEAN_SUBJ, szSubj, szGrp, szExp );
	OutputAMessage( szData, TRUE );

	// Experiment Type
	Experiments exp;
	if( !exp.IsValid() ) return;
	if( FAILED( exp.Find( szExp ) ) ) return;
	short nType = 0;
	exp.GetType( nType );

	// REMOVE FILES ACCORDINGLY
	Subjects::CleanProcessFiles( szExp, szGrp, szSubj, nType, d.m_fHWR,
								 d.m_fTF, d.m_fSeg, d.m_fExt, d.m_fCon, d.m_fErr );
}

//************************************
// Method:    OnUpdateSClean
// FullName:  CLeftView::OnUpdateSClean
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSClean(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
//	fEnable &= tree.ItemHasChildren( hItem );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTViewExt
// FullName:  CLeftView::OnTViewExt
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewExt() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	szTrial = szTrial.Left( szTrial.GetLength() - 6 );
	szTrial += _T(".EXT");
	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("Extracted data file does not exist. Ensure you have processed this trial.\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWEXT, szTrial );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;
	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateTViewExt
// FullName:  CLeftView::OnUpdateTViewExt
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTViewExt(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTViewErr
// FullName:  CLeftView::OnTViewErr
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewErr() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	szTrial = szTrial.Left( szTrial.GetLength() - 6 );
	szTrial += _T(".ERR");
	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWERR, szTrial );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;

// 	InstructionDlg d( this );
// 	d.m_fJustDisplay = TRUE;
// 	d.m_szFileName = szFile;
// 	d.DoModal();

	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateTViewErr
// FullName:  CLeftView::OnUpdateTViewErr
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTViewErr(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_TRIAL );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTViewDis
// FullName:  CLeftView::OnTViewDis
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewDis() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	szTrial = szTrial.Left( szTrial.GetLength() - 3 );
	szTrial += _T("DIS");
	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWERR, szTrial );
	OutputAMessage( szData, TRUE );

 	CWaitCursor crs;
 	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnTViewWrd
// FullName:  CLeftView::OnTViewWrd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewWrd() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	szTrial = szTrial.Left( szTrial.GetLength() - 3 );
	szTrial += _T("WRD");
	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWERR, szTrial );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnGReprocess
// FullName:  CLeftView::OnGReprocess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGReprocess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_ReprocAll = TRUE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	HWND hWndStatus = pMF->m_wndStatusBar.m_hWnd;
	CWinThread* pThread = AfxBeginThread( ReprocessGroup, hWndStatus );
}

//************************************
// Method:    OnUpdateGReprocess
// FullName:  CLeftView::OnUpdateGReprocess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGReprocess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	fEnable &= tree.ItemHasChildren( hItem );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnGProcess
// FullName:  CLeftView::OnGProcess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGProcess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_ReprocAll = FALSE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	HWND hWndStatus = pMF->m_wndStatusBar.m_hWnd;
	CWinThread* pThread = AfxBeginThread( ReprocessGroup, hWndStatus );
}

//************************************
// Method:    OnUpdateGProcess
// FullName:  CLeftView::OnUpdateGProcess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGProcess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	fEnable &= tree.ItemHasChildren( hItem );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    ReprocessExperiment
// FullName:  ReprocessExperiment
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT ReprocessExperiment( LPVOID pParam )
{
	CMainFrame* pMF = _MF;
	CLeftView* pLV = _LV;
	CListView* pMsgWnd = _MsgWnd;
	if( !pMF || !pLV || !pMsgWnd )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	CTreeCtrl& tree = pLV->GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	HTREEITEM hExp = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hExp ); 
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 )
	{
		_Reprocessing = FALSE;
		return 0;
	}
	CString szExpID = szItem.Mid( nPos + 2 );

	// Hide trials if not detailed output
	if( !::GetDetailedOutput() ) pLV->HideTrials( szExpID );

	// Delete ERR file
	CString szErr;
	GetExpERR( szExpID, szErr );
	CFileFind ff;
	if( ff.FindFile( szErr ) ) REMOVE_FILE( szErr );

	// Loop through all subjects in experiment to get a count of total subjects
	int nCount = 0;
	HTREEITEM hItem = tree.GetNextItem( hExp, TVGN_CHILD );	// Group folder
	DWORD dwData = tree.GetItemData( hItem );
	if( dwData != TI_GROUP_FOLDER )
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	hItem = tree.GetNextItem( hItem, TVGN_CHILD );			// 1st group
	HTREEITEM hSubj = NULL;

	while( hItem )						// loop through each group
	{
		hSubj = tree.GetNextItem( hItem, TVGN_CHILD );		// subject folder
		hSubj = tree.GetNextItem( hSubj, TVGN_CHILD );		// 1st subject
		while( hSubj )
		{
			nCount++;
			hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );
		}

		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// Loop through all subjects in experiment
	hItem = tree.GetNextItem( hExp, TVGN_CHILD );	// Group folder
	dwData = tree.GetItemData( hItem );
	if( dwData != TI_GROUP_FOLDER )
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	hItem = tree.GetNextItem( hItem, TVGN_CHILD );			// 1st group
	CString szGrpID;
	int nCur = 0;

	HRESULT hRes = CoInitialize( NULL );
	_ASSERTE( SUCCEEDED( hRes ) );

	CLeftView::SetActionState( ACTION_REPROCESS );
	Progress::UseCallbacks( TRUE );
	while( hItem && !_TerminateThread )						// loop through each group
	{
		szItem = tree.GetItemText( hItem );
		nPos = szItem.Find( szDelim );
		if( nPos == -1 ) break;
		szGrpID = szItem.Mid( nPos + 2 );
		hSubj = tree.GetNextItem( hItem, TVGN_CHILD );		// subject folder
		hSubj = tree.GetNextItem( hSubj, TVGN_CHILD );		// 1st subject
		while( hSubj && !_TerminateThread )
		{
			// Remove the trials (unless flag set)
			if( tree.ItemHasChildren( hSubj ) && ::GetDetailedOutput() )
			{
				HTREEITEM hItem2 = tree.GetNextItem( hSubj, TVGN_CHILD );
				if( hItem2 ) tree.DeleteItem( hItem2 );
			}

			// Reprocess this subject
			szItem = tree.GetItemText( hSubj );
			nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				szItem = szItem.Mid( nPos + 2 );
				// get subject sampling rate & device res
				double dDevRes = 0.;
				int nSampRate = 0;
				HRESULT hr = pLV->m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
												szItem.AllocSysString() );
				if( SUCCEEDED( hr ) )
				{
					pLV->m_pEML->get_DeviceRes( &dDevRes );
					short nTemp = 0;
					pLV->m_pEML->get_SamplingRate( &nTemp );
					nSampRate = nTemp;
				}

				nCur++;
				pMF->ReprocessSubject( szExpID, szItem, szGrpID, dDevRes, nSampRate,
									   hSubj, pLV, pMsgWnd, nCount, nCur, FALSE, TRUE, TRUE, _ReprocAll );

				if( ::GetDetailedOutput() )
				{
					HTREEITEM hTrials = tree.GetChildItem( hSubj );
					if( hTrials ) tree.Expand( hTrials, TVE_COLLAPSE );
				}
			}

			hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );
		}

		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}

	BOOL fMsg = FALSE;
	// inform that some trials might not be visible
	if( ::GetDetailedOutput() && !_ReprocAll )
	{
		BCGPMessageBox( _T("Some trials might not be visible in the tree view.\nTo show all trials, View -> Refresh.") );
		fMsg = TRUE;
	}

	// Optionally delete summarization/analysis files
	CString szInc;
	GetExpINC( szExpID, szInc );
	GetExpERR( szExpID, szErr );
	if( BCGPMessageBox( IDS_CLEANUP, MB_YESNO ) == IDYES )
	{
		CString szMsg;
		if( ff.FindFile( szInc ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szInc );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove TMP file
		CString szTmp = szInc.Left( szInc.GetLength() - 3 );
		szTmp += _T("tmp");
		if( ff.FindFile( szTmp ) )
		{
			try
			{
				CFile::Remove( szTmp );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove ERR file
		if( ff.FindFile( szErr ) )
		{
			try
			{
				CFile::Remove( szErr );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szErr );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove AXS memory file
		szTmp = szInc.Left( szInc.GetLength() - 7 );	// exp.axs
		szTmp += _T("graph.axs");
		if( ff.FindFile( szTmp ) )
		{
			try
			{
				CFile::Remove( szTmp );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}

		// summarize
		pMF->SummarizeExperiment( szExpID, pMsgWnd );
	
		fMsg = TRUE;
	}

	// message user processing is done if no other message presented
	if( !fMsg ) BCGPMessageBox( _T("Processing complete.") );

	// cleanup
	CLeftView::SetActionState( ACTION_NONE );
	Progress::UseCallbacks( FALSE );
	_Reprocessing = FALSE;
	_TerminateThread = FALSE;
	CoUninitialize();
	return 0;
}

//************************************
// Method:    OnEStopreprocess
// FullName:  CLeftView::OnEStopreprocess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEStopreprocess() 
{
	if( _Reprocessing )
	{
		CString szMsg;
		szMsg.LoadString( IDS_AL_STOPREPROC );
		OutputAMessage( szMsg, TRUE );
		_TerminateThread = TRUE;
//		_ActionState = ACTION_NONE;
//		_Reprocessing = FALSE;
	}
}

//************************************
// Method:    OnUpdateEStopreprocess
// FullName:  CLeftView::OnUpdateEStopreprocess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEStopreprocess(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( _Reprocessing );
}

//************************************
// Method:    TerminateThread
// FullName:  CLeftView::TerminateThread
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL CLeftView::TerminateThread()
{
	return _TerminateThread;
}

//************************************
// Method:    ReprocessGroup
// FullName:  ReprocessGroup
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT ReprocessGroup( LPVOID pParam )
{
	CMainFrame* pMF = _MF;
	CLeftView* pLV = _LV;
	CListView* pMsgWnd = _MsgWnd;
	if( !pMF || !pLV || !pMsgWnd )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	CTreeCtrl& tree = pLV->GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which group
	HTREEITEM hGrp = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hGrp ); 
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 )
	{
		_Reprocessing = FALSE;
		return 0;
	}
	CString szGrpID = szItem.Mid( nPos + 2 );

	// Which experiment
	HTREEITEM hExp = tree.GetParentItem( hGrp );	// Group folder
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 )
	{
		_Reprocessing = FALSE;
		return 0;
	}
	CString szExpID = szItem.Mid( nPos + 2 );

	// Hide trials if not detailed output
	if( !::GetDetailedOutput() ) pLV->HideTrials( szExpID, szGrpID );

	// Loop through all subjects in group to get a count of total subjects
	int nCount = 0;
	HTREEITEM hItem = tree.GetNextItem( hGrp, TVGN_CHILD );	// Subject folder
	hItem = tree.GetNextItem( hItem, TVGN_CHILD );			// 1st subjet
	while( hItem )
	{
		nCount++;
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// Loop through all subjects in group
	HTREEITEM hSubj = tree.GetNextItem( hGrp, TVGN_CHILD );	// Subject folder
	hSubj = tree.GetNextItem( hSubj, TVGN_CHILD );			// 1st subject

	HRESULT hRes = CoInitialize( NULL );
	_ASSERTE( SUCCEEDED( hRes ) );

	int nCur = 0;
	CLeftView::SetActionState( ACTION_REPROCESS );
	Progress::UseCallbacks( TRUE );
	while( hSubj && !_TerminateThread )
	{
		// Remove the trials (unless flag set)
		if( tree.ItemHasChildren( hSubj ) && ::GetDetailedOutput() )
		{
			HTREEITEM hItem = tree.GetNextItem( hSubj, TVGN_CHILD );
			if( hItem ) tree.DeleteItem( hItem );
		}

		// Reprocess this subject
		szItem = tree.GetItemText( hSubj );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szItem = szItem.Mid( nPos + 2 );
			// get subject sampling rate & device res
			double dDevRes = 0.;
			int nSampRate = 0;
			HRESULT hr = pLV->m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
											szItem.AllocSysString() );
			if( SUCCEEDED( hr ) )
			{
				pLV->m_pEML->get_DeviceRes( &dDevRes );
				short nTemp = 0;
				pLV->m_pEML->get_SamplingRate( &nTemp );
				nSampRate = nTemp;
			}

			nCur++;
			pMF->ReprocessSubject( szExpID, szItem, szGrpID, dDevRes, nSampRate,
								   hSubj, pLV, pMsgWnd, nCount, nCur, FALSE, TRUE,
								   TRUE, _ReprocAll );

			if( ::GetDetailedOutput() )
			{
				HTREEITEM hTrials = tree.GetChildItem( hSubj );
				if( hTrials ) tree.Expand( hTrials, TVE_COLLAPSE );
			}
		}

		hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );
	}

	BOOL fMsg = FALSE;
	// inform that some trials might not be visible
	if( ::GetDetailedOutput() && !_ReprocAll )
	{
		BCGPMessageBox( _T("Some trials might not be visible in the tree view.\nTo show all trials, View -> Refresh.") );
		fMsg = TRUE;
	}

	// Optionally delete summarization/analysis files
	CString szInc, szErr;
	CFileFind ff;
	GetExpINC( szExpID, szInc );
	GetExpERR( szExpID, szErr );
	if( BCGPMessageBox( IDS_CLEANUP, MB_YESNO ) == IDYES )
	{
		CString szMsg;
		if( ff.FindFile( szInc ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szInc );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove TMP file
		CString szTmp = szInc.Left( szInc.GetLength() - 3 );
		szTmp += _T("tmp");
		if( ff.FindFile( szTmp ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove ERR file
		if( ff.FindFile( szErr ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szErr );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove AXS memory file
		szTmp = szInc.Left( szInc.GetLength() - 7 );	// exp.axs
		szTmp += _T("graph.axs");
		if( ff.FindFile( szTmp ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				_TerminateThread = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}

		// summarize
		pMF->SummarizeExperiment( szExpID, pMsgWnd );

		fMsg = TRUE;
	}

	// message user processing is done if no other message presented
	if( !fMsg ) BCGPMessageBox( _T("Processing complete.") );

	// cleanup
	CLeftView::SetActionState( ACTION_NONE );
	Progress::UseCallbacks( FALSE );
	_Reprocessing = FALSE;
	_TerminateThread = FALSE;
	CoUninitialize();
	return 0;
}

//************************************
// Method:    OnTReprocess
// FullName:  CLeftView::OnTReprocess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTReprocess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( pMF && pMsgWnd )
	{
		CString szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );

		// Trial
		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItemT = tree.GetSelectedItem();
		CString szTrial = tree.GetItemText( hItemT );
		// Which subject
		HTREEITEM hItem = tree.GetParentItem( hItemT );	// Trial folder
		hItem = tree.GetParentItem( hItem );			// Subject
		CString szItem = tree.GetItemText( hItem );
		int nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return;
		CString szSubjID = szItem.Mid( nPos + 2 );
		// Which experiment
		HTREEITEM hItem2 = tree.GetParentItem( hItem );	// Subject folder
		hItem2 = tree.GetParentItem( hItem2 );			// Group
		szItem = tree.GetItemText( hItem2 );
		nPos = szItem.Find( szDelim );
		CString szGrpID = szItem.Mid( nPos + 2 );
		hItem2 = tree.GetParentItem( hItem2 );			// Group folder
		hItem2 = tree.GetParentItem( hItem2 );			// Experiment
		szItem = tree.GetItemText( hItem2 ); 
		nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return;
		CString szExpID = szItem.Mid( nPos + 2 );
		// Remove the trial
//		if( ::GetDetailedOutput() ) tree.DeleteItem( hItemT );

		// get subject sampling rate & device res
		double dDevRes = 0.;
		int nSampRate = 0;
		HRESULT hr = m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
								   szSubjID.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			m_pEML->get_DeviceRes( &dDevRes );
			short nTemp = 0;
			m_pEML->get_SamplingRate( &nTemp );
			nSampRate = nTemp;
		}

		_Reprocessing = TRUE;
		pMF->ReprocessTrial( szExpID, szSubjID, szGrpID, szTrial, dDevRes, nSampRate, hItem, TRUE );
		_Reprocessing = FALSE;
	}
}

//************************************
// Method:    OnUpdateTReprocess
// FullName:  CLeftView::OnUpdateTReprocess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTReprocess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_TRIAL );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEViewInc
// FullName:  CLeftView::OnEViewInc
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewInc() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	GetExpINC( szExp, szFile );

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("INC file does not exist. Generate by summarizing the experiment (MENU: Exp->summarize)."), MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWINC, szExp );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;
	DataTabGridDlg* d = new DataTabGridDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateEViewInc
// FullName:  CLeftView::OnUpdateEViewInc
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEViewInc(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEViewErr
// FullName:  CLeftView::OnEViewErr
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewErr() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	GetExpERR( szExp, szFile );

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("Error file does not exist."), MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWERR, szExp );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;
//	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnUpdateEViewErr
// FullName:  CLeftView::OnUpdateEViewErr
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEViewErr(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEViewErs
// FullName:  CLeftView::OnEViewErs
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewErs() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	GetExpERS( szExp, szFile );

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("Error summary file does not exist."), MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWERR, szExp );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;
//	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnUpdateEViewErs
// FullName:  CLeftView::OnUpdateEViewErs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEViewErs(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEReportprocset
// FullName:  CLeftView::OnEReportprocset
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEReportprocset() 
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	CString szData;
	szData.Format( IDS_AL_REPORTPROCSET, szExpID );
	OutputAMessage( szData );

	// experiment type
	short nExpType = EXP_TYPE_HANDWRITING;
	{
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExpID ) ) ) exp.GetType( nExpType );
	}

	// Process Settings (one for experiment one default)
	IProcSetting *pPS = NULL, *pPS2 = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
						   IID_IProcSetting, (LPVOID*)&pPS2 );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	pPS->Find( szExpID.AllocSysString() );
	// pPS2 will use defaults
	// Use default if not found
	IProcSetFlags *pPSF = NULL, *pPSF2 = NULL;
	hr = pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF );
	hr = pPS2->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF2 );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		pPS->Release();
		pPS2->Release();
		return;
	}
	IProcSetRunExp *pPSRE = NULL, *pPSRE2 = NULL;
	hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
	hr = pPS2->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE2 );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		pPSF->Release();
		pPSF2->Release();
		pPS->Release();
		pPS2->Release();
		return;
	}

	// File already there?
	CFileFind ff;
	CString szSET;
	GetExpSET( szExpID, szSET );
	BOOL fRedo = FALSE;
	if( !::VerifyDirectory( szExpID ) )
	{
		pPS->Remove();
		pPSF->Release();
		pPSRE->Release();
		pPS->Release();
		return;
	}
	if( ff.FindFile( szSET ) )
	{
		if( BCGPMessageBox( IDS_PS_WRITTEN, MB_YESNO ) == IDYES ) fRedo = TRUE;
	}
	else fRedo = TRUE;
	if( fRedo )
	{
		CStdioFile f;
		if( !f.Open( szSET, CFile::modeWrite | CFile::modeCreate ) )
		{
			pPSF->Release();
			pPSRE->Release();
			pPS->Release();
			pPS->Release();
			return;
		}

		BOOL fVal, fVal2;
		double dVal, dVal2;
		short nVal, nVal2;

		// 1st line = user
		f.WriteString( _T("User: ") );
		::GetCurrentUser( szData );
		f.WriteString( szData );
		f.WriteString( _T(" - ") );
		::GetCurrentUserDesc( szData );
		f.WriteString( szData );
		f.WriteString( _T("\n\n") );

		// Device Settings (user-dependent, not experiment)
		f.WriteString( _T("DEVICE SETTINGS\n") );
		f.WriteString( _T("________________________\n") );
		fVal = ::IsTabletModeOn();
		if( fVal ) f.WriteString( _T("Device = Tablet\n") );
		else if( ::IsGripperModeOn() ) f.WriteString( _T("Device = Gripper\n") );
		else f.WriteString( _T("Device = Mouse\n") );
		fVal = ::IsDesktopModeOn();
		if( fVal ) f.WriteString( _T("Mapping = Desktop\n") );
		else f.WriteString( _T("Mapping = Recording Window\n") );
		double dx = 0., dy = 0.;
		::GetDisplayDimensions( dx, dy );
		szData.Format( _T("Display Dimensions = %.2f cm (width) x %.2f cm (height)\n"), dx, dy );
		f.WriteString( szData );
		::GetTabletDimensions( dx, dy );
		szData.Format( _T("Tablet Dimensions = %.2f cm (width) x %.2f cm (height)\n\n\n"), dx, dy );
		f.WriteString( szData );

		// Experiment
		szData.Format( _T("Process settings for experiment %s"), szExpID );
		f.WriteString( szData );	// what experiment we're exporting settings of
		f.WriteString( _T("\n") );
		f.WriteString( _T("\n") );

		// Trials
		f.WriteString( _T("TRIALS\n") );
		f.WriteString( _T("________________________\n") );
		pPSRE->get_TimeoutStart( &dVal );
		pPSRE2->get_TimeoutStart( &dVal2 );
		szData.Format( _T("Time allowed to begin a trial (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPSRE->get_TimeoutRecord( &dVal );
		pPSRE2->get_TimeoutRecord( &dVal2 );
		szData.Format( _T("Total time allowed to record during a trial (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPSRE->get_TimeoutPenlift( &dVal );
		pPSRE2->get_TimeoutPenlift( &dVal2 );
		szData.Format( _T("Once recording has begun, time allowed for pen to be lifted from digitizer (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPSRE->get_TimeoutLatency( &dVal );
		pPSRE2->get_TimeoutLatency( &dVal2 );
		szData.Format( _T("Time between successive trials (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPSRE->get_ProcImmediately( &fVal );
		pPSRE2->get_ProcImmediately( &fVal2 );
		szData.Format( _T("Process immediately after recording: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_MaxRecArea( &fVal );
		pPSRE2->get_MaxRecArea( &fVal2 );
		szData.Format( _T("Make recording window real size: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("Maximize recording window: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_FullScreen( &fVal );
		pPSRE2->get_FullScreen( &fVal2 );
		szData.Format( _T("Maximize to full screen: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("Leave recording window as-is: %s [DEFAULT=%s]\n"),
					   ( fVal == 2 ) ? _T("TRUE") : _T("FALSE"), ( fVal2 == 2 ) ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Charting
		f.WriteString( _T("CHARTING\n") );
		f.WriteString( _T("________________________\n") );
		pPSRE->get_DisplayCharts( &fVal );
		pPSRE2->get_DisplayCharts( &fVal2 );
		szData.Format( _T("Display charts after each trial: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_DisplayRaw( &fVal );
		pPSRE2->get_DisplayRaw( &fVal2 );
		szData.Format( _T("Chart raw data: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_DisplayTF( &fVal );
		pPSRE2->get_DisplayTF( &fVal2 );
		szData.Format( _T("Chart data as a function of time: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_DisplaySeg( &fVal );
		pPSRE2->get_DisplaySeg( &fVal2 );
		szData.Format( _T("\tInclude segmentation: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Summary/Analysis
		f.WriteString( _T("SUMMARIZATION/ANALYSIS\n") );
		f.WriteString( _T("________________________\n") );
		pPSRE->get_Summarize( &fVal );
		pPSRE2->get_Summarize( &fVal2 );
		szData.Format( _T("Summarize data after running experiment: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_SummarizeOnly( &fVal );
		pPSRE2->get_SummarizeOnly( &fVal2 );
		szData.Format( _T("\tSummarize this subject only: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_SummarizeSelect( &fVal );
		pPSRE2->get_SummarizeSelect( &fVal2 );
		szData.Format( _T("\tSelect which subjects to include/exclude: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_Analyze( &fVal );
		pPSRE2->get_Analyze( &fVal2 );
		szData.Format( _T("Analyze experiment: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Viewing data
		f.WriteString( _T("DATA VIEWING\n") );
		f.WriteString( _T("________________________\n") );
		pPSRE->get_ViewSubjExt( &fVal );
		pPSRE2->get_ViewSubjExt( &fVal2 );
		szData.Format( _T("View subject's extracted data: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_ViewSubjCon( &fVal );
		pPSRE2->get_ViewSubjCon( &fVal2 );
		szData.Format( _T("View subject's consistency errors: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_ViewExpExt( &fVal );
		pPSRE2->get_ViewExpExt( &fVal2 );
		szData.Format( _T("View experiment's extracted data: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_ViewExpCon( &fVal );
		pPSRE2->get_ViewExpCon( &fVal2 );
		szData.Format( _T("View experiment's consistency errors: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Procedure
		f.WriteString( _T("RECORDING PROCEDURE\n") );
		f.WriteString( _T("________________________\n") );
		pPSRE->get_DoQuestionnaire( &fVal );
		pPSRE2->get_DoQuestionnaire( &fVal2 );
		szData.Format( _T("View/edit this subject's questionnaire, [prior to] running the experiment: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSRE->get_QuestAtEnd( &fVal );
		pPSRE2->get_QuestAtEnd( &fVal2 );
		szData.Format( _T("View/edit this subject's questionnaire after running the experiment: %s [DEFAULT=%s]\n"),
						fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_SpecifySequence( &fVal );
		pPS2->get_SpecifySequence( &fVal2 );
		szData.Format( _T("Specify condition order: %s [DEFAULT=%s]\n"),
						fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_Randomize( &fVal );
		pPS2->get_Randomize( &fVal2 );
		szData.Format( _T("Randomize condition order: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_RandomizeTrials( &fVal );
		pPS2->get_RandomizeTrials( &fVal2 );
		szData.Format( _T("Randomize replications as well: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_RandWithRules( &fVal );
		pPS2->get_RandWithRules( &fVal2 );
		szData.Format( _T("Randomize with rules: %s [DEFAULT=%s]\n"),
			fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_InstructionAlwaysVisible( &fVal );
		pPS2->get_InstructionAlwaysVisible( &fVal2 );
		szData.Format( _T("Instruction constantly visible during recording: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		TrialsMode tm, tm2;
		BOOL fCondInstr = FALSE, fCondInstr2 = FALSE, fEnterKey = FALSE, fEnterKey2 = FALSE,
			 fAcceptRedo = FALSE, fAcceptRedo2 = FALSE;
		int nRedoMode = 0, nRedoMode2 = 0, nTrialMode = 0, nTrialMode2 = 0;
		pPS->get_TrialMode( &tm );
		pPS2->get_TrialMode( &tm2 );
		switch( tm )
		{
			case eTM_None: 
				break;
			case eTM_Delay:
				fEnterKey = TRUE;
				break;
			case eTM_First:
				fCondInstr = TRUE;
				break;
			case eTM_FirstDelay:
				fCondInstr = TRUE;
				fEnterKey = TRUE;
				break;
			case eTM_All:
				fCondInstr = TRUE;
				nTrialMode = IDC_RDO_TRIAL_ALL;
				break;
			case eTM_NoneAD:
				fAcceptRedo = TRUE;
				break;
			case eTM_DelayAD:
				fEnterKey = TRUE;
				fAcceptRedo = TRUE;
				break;
			case eTM_FirstAD:
				fCondInstr = TRUE;
				fAcceptRedo = TRUE;
				break;
			case eTM_FirstDelayAD:
				fCondInstr = TRUE;
				fEnterKey = TRUE;
				fAcceptRedo = TRUE;
				break;
			case eTM_AllAD:
				fCondInstr = TRUE;
				nTrialMode = IDC_RDO_TRIAL_ALL;
				fAcceptRedo = TRUE;
				break;
			case eTM_NoneADErr:
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_DelayADErr:
				fEnterKey = TRUE;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_FirstADErr:
				fCondInstr = TRUE;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_FirstDelayADErr:
				fCondInstr = TRUE;
				fEnterKey = TRUE;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_AllADErr:
				fCondInstr = TRUE;
				nTrialMode = IDC_RDO_TRIAL_ALL;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_ERR;
				break;
			case eTM_NoneErr:
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_DelayErr:
				fEnterKey = TRUE;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_FirstErr:
				fCondInstr = TRUE;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_FirstDelayErr:
				fCondInstr = TRUE;
				fEnterKey = TRUE;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_REDO;
				break;
			case eTM_AllErr:
				fCondInstr = TRUE;
				nTrialMode = IDC_RDO_TRIAL_ALL;
				fAcceptRedo = TRUE;
				nRedoMode = IDC_RDO_AD_REDO;
				break;
		}
		switch( tm2 )
		{
			case eTM_None: 
				break;
			case eTM_Delay:
				fEnterKey2 = TRUE;
				break;
			case eTM_First:
				fCondInstr2 = TRUE;
				break;
			case eTM_FirstDelay:
				fCondInstr2 = TRUE;
				fEnterKey2 = TRUE;
				break;
			case eTM_All:
				fCondInstr2 = TRUE;
				nTrialMode2 = IDC_RDO_TRIAL_ALL;
				break;
			case eTM_NoneAD:
				fAcceptRedo2 = TRUE;
				break;
			case eTM_DelayAD:
				fEnterKey2 = TRUE;
				fAcceptRedo2 = TRUE;
				break;
			case eTM_FirstAD:
				fCondInstr2 = TRUE;
				fAcceptRedo2 = TRUE;
				break;
			case eTM_FirstDelayAD:
				fCondInstr2 = TRUE;
				fEnterKey2 = TRUE;
				fAcceptRedo2 = TRUE;
				break;
			case eTM_AllAD:
				fCondInstr2 = TRUE;
				nTrialMode2 = IDC_RDO_TRIAL_ALL;
				fAcceptRedo2 = TRUE;
				break;
			case eTM_NoneADErr:
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_ERR;
				break;
			case eTM_DelayADErr:
				fEnterKey2 = TRUE;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_ERR;
				break;
			case eTM_FirstADErr:
				fCondInstr2 = TRUE;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_ERR;
				break;
			case eTM_FirstDelayADErr:
				fCondInstr2 = TRUE;
				fEnterKey2 = TRUE;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_ERR;
				break;
			case eTM_AllADErr:
				fCondInstr2 = TRUE;
				nTrialMode2 = IDC_RDO_TRIAL_ALL;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_ERR;
				break;
			case eTM_NoneErr:
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_REDO;
				break;
			case eTM_DelayErr:
				fEnterKey2 = TRUE;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_REDO;
				break;
			case eTM_FirstErr:
				fCondInstr2 = TRUE;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_REDO;
				break;
			case eTM_FirstDelayErr:
				fCondInstr2 = TRUE;
				fEnterKey2 = TRUE;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_REDO;
				break;
			case eTM_AllErr:
				fCondInstr2 = TRUE;
				nTrialMode2 = IDC_RDO_TRIAL_ALL;
				fAcceptRedo2 = TRUE;
				nRedoMode2 = IDC_RDO_AD_REDO;
				break;
		}
		f.WriteString( _T("AT THE BEGINNING OF A TRIAL:\n") );
		szData.Format( _T("\tShow condition instruction for the first trial per condition: %s\n"),
					   fCondInstr ? _T("TRUE") : _T("FALSE"), fCondInstr2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("\tShow condition instruction per trial: %s [DEFAULT=%s]\n"),
					   ( fCondInstr && ( nTrialMode == IDC_RDO_TRIAL_ALL ) ) ? _T("TRUE") : _T("FALSE"),
					   ( fCondInstr2 && ( nTrialMode2 == IDC_RDO_TRIAL_ALL ) ) ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("AT THE END OF A TRIAL:\n") );
		szData.Format( _T("\tDelay next trial until ENTER key is pressed: %s [DEFAULT=%s]\n"),
					   fEnterKey ? _T("TRUE") : _T("FALSE"), fEnterKey2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("\tAccept/Redo trial: %s [DEFAULT=%s]\n"),
					   fAcceptRedo ? _T("TRUE") : _T("FALSE"), fAcceptRedo2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("\t\tAsk for every trial: %s [DEFAULT=%s]\n"),
					   ( fAcceptRedo && ( nRedoMode == IDC_RDO_AD_YES ) ) ? _T("TRUE") : _T("FALSE"),
					   ( fAcceptRedo2 && ( nRedoMode2 == IDC_RDO_AD_YES ) ) ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("\t\tAsk for consistency error: %s [DEFAULT=%s]\n"),
					   ( fAcceptRedo && ( nRedoMode == IDC_RDO_AD_ERR ) ) ? _T("TRUE") : _T("FALSE"),
					   ( fAcceptRedo2 && ( nRedoMode2 == IDC_RDO_AD_ERR ) ) ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		szData.Format( _T("\t\tRedo trial automatically if consistency error: %s [DEFAULT=%s]\n"),
					   ( fAcceptRedo && ( nRedoMode == IDC_RDO_AD_REDO ) ) ? _T("TRUE") : _T("FALSE"),
					   ( fAcceptRedo2 && ( nRedoMode2 == IDC_RDO_AD_REDO ) ) ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Input Device
		f.WriteString( _T("INPUT DEVICE\n") );
		f.WriteString( _T("________________________\n") );
		pPS->get_SamplingRate( &nVal );
		pPS2->get_SamplingRate( &nVal2 );
		szData.Format( _T("Sampling Rate: %d Hz [DEFAULT=%d]\n"), nVal, nVal2 );
		f.WriteString( szData );
		pPS->get_MinPenPressure( &nVal );
		pPS2->get_MinPenPressure( &nVal2 );
		szData.Format( _T("Minimum Pen Pressure or lift force (N) or Z Position (cm): %d [DEFAULT=%d]\n"), nVal, nVal2 );
		f.WriteString( szData );
		pPS->get_MaxPenPressure( &nVal );
		pPS2->get_MaxPenPressure( &nVal2 );
		szData.Format( _T("Maximum Pen Pressure for notification: %d [DEFAULT=%d]\n"), nVal, nVal2 );
		f.WriteString( szData );
		pPS->get_DeviceResolution( &dVal );
		pPS2->get_DeviceResolution( &dVal2 );
		szData.Format( _T("Device Resolution: %g (cm or N) [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_UseTilt( &fVal );
		pPS2->get_UseTilt( &fVal2 );
		szData.Format( _T("Record Tilt: %s [DEFAULT=%s]\n"),
			fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Image Settings
		if( nExpType == EXP_TYPE_IMAGE )
		{
			f.WriteString( _T("IMAGE PROCESSING\n") );
			f.WriteString( _T("________________________\n") );
			pPS->get_SmoothingIter( &nVal );
			pPS2->get_SmoothingIter( &nVal2 );
			szData.Format( _T("Number of smoothing iterations (0=100dpi): %d [DEFAULT=%d]\n"), nVal, nVal2 );
			f.WriteString( szData );
			pPS->get_InkThreshold( &nVal );
			pPS2->get_InkThreshold( &nVal2 );
			szData.Format( _T("Graylevel separating ink and paper (0=Automatic): %d [DEFAULT=%d]\n"), nVal, nVal2 );
			f.WriteString( szData );
			pPS->get_ImgResolution( &nVal );
			pPS2->get_ImgResolution( &nVal2 );
			szData.Format( _T("Resolution (dpi) (0=Automatic): %d [DEFAULT=%d]\n"), nVal, nVal2 );
			f.WriteString( szData );
			f.WriteString( _T("\n") );
		}

		// Word Extraction
		if( nExpType == EXP_TYPE_HANDWRITING )
		{
			f.WriteString( _T("WORD EXTRACTION\n") );
			f.WriteString( _T("________________________\n") );
			pPS->get_MinWordWidth( &dVal );
			pPS2->get_MinWordWidth( &dVal2 );
			szData.Format( _T("Minimum word width (cm): %g [DEFAULT=%g]\n"), dVal, dVal2 );
			f.WriteString( szData );
			pPS->get_MinLeftSpacing( &dVal );
			pPS2->get_MinLeftSpacing( &dVal2 );
			szData.Format( _T("Minimum leftward distance between words (cm): %g [DEFAULT=%g]\n"), dVal, dVal2 );
			f.WriteString( szData );
			pPS->get_MinLeftPenup( &dVal );
			pPS2->get_MinLeftPenup( &dVal2 );
			szData.Format( _T("Minimum leftward penlift movement between words (cm): %g [DEFAULT=%g]\n"), dVal, dVal2 );
			f.WriteString( szData );
			pPS->get_MinDownSpacing( &dVal );
			pPS2->get_MinDownSpacing( &dVal2 );
			szData.Format( _T("Minimum downward distance between word beginnings (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
			f.WriteString( szData );
			pPS->get_MinTimePenup( &dVal );
			pPS2->get_MinTimePenup( &dVal2 );
			szData.Format( _T("Minimum duration of penlifts between words (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
			f.WriteString( szData );
			pPS->get_MaxTimePenup( &dVal );
			pPS2->get_MaxTimePenup( &dVal2 );
			szData.Format( _T("Maximum duration of penlifts within a word (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
			f.WriteString( szData );
			f.WriteString( _T("\n") );
		}

		// TimFun
		f.WriteString( _T("TIME FUNCTION\n") );
		f.WriteString( _T("________________________\n") );
		pPS->get_FFT( &fVal );
		pPS2->get_FFT( &fVal2 );
		szData.Format( _T("Filter Method: %s Low Pass [DEFAULT=%s]\n"), fVal ? _T("FFT") : _T("Butterworth"),
					   fVal2 ? _T("FFT") : _T("Butterworth") );
		f.WriteString( szData );
		pPS->get_FilterFrequency( &dVal );
		pPS2->get_FilterFrequency( &dVal2 );
		szData.Format( _T("Filter frequency: %g Hz [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_Sharpness( &dVal );
		pPS2->get_Sharpness( &dVal2 );
		szData.Format( _T("Sharpness: %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_UpSampleFactor( &nVal2 );
		pPS2->get_UpSampleFactor( &nVal2 );
		szData.Format( _T("Sharpness: %d [DEFAULT=%d]\n"), nVal, nVal2 );
		f.WriteString( szData );
		pPS->get_Decimate( &nVal );
		pPS2->get_Decimate( &nVal2 );
		szData.Format( _T("Read only every specified decimate-th sample: %d [DEFAULT=%d]\n"), nVal, nVal2 );
		f.WriteString( szData );
		pPS->get_RemoveTrailingPenlift( &fVal );
		pPS2->get_RemoveTrailingPenlift( &fVal2 );
		szData.Format( _T("Remove trailing penlift: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_TF_J( &fVal );
		pPSF2->get_TF_J( &fVal2 );
		szData.Format( _T("Calculate x, y, jerk and append to .tf file: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_TF_R( &fVal );
		pPSF2->get_TF_R( &fVal2 );
		szData.Format( _T("Spectrum of raw signal instead of velocity: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_TF_A( &fVal );
		pPSF2->get_TF_A( &fVal2 );
		szData.Format( _T("Spectrum of acceleration instead of velocity: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_TF_U( &fVal );
		pPSF2->get_TF_U( &fVal2 );
		szData.Format( _T("Unrotate automatically: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_RotationBeta( &dVal );
		pPS2->get_RotationBeta( &dVal2 );
		szData.Format( _T("\tAdditional rotation: %.2f radians [DEFAULT=%.2f]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_DisCorrection( &nVal );
		pPS2->get_DisCorrection( &nVal2 );
		if( nVal == 0 ) szData = _T("Discontinuity Correction - Discard all samples after discontinuity");
		else if( nVal == 1 ) szData = _T("Discontinuity Correction - Insert running average velocity line");
		else if( nVal == 2 ) szData = _T("Discontinuity Correction - Move towards start of discontinuity");
		else if( nVal == 3 ) szData = _T("Discontinuity Correction - Accept discontinuity");
		szData += _T(" [DEFAULT=Discard all samples after discontinuity]\n");
		f.WriteString( szData );
		pPS->get_DisFactor( &dVal );
		pPS2->get_DisFactor( &dVal2 );
		szData.Format( _T("Integration constant for running sample difference: %.2f [DEFAULT=%.2f]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_DisZInsertPoint( &dVal );
		pPS2->get_DisZInsertPoint( &dVal2 );
		szData.Format( _T("Set pressure of inserted or first moved sample negative to mark raw data manipulation: %.2f [DEFAULT=%.2f]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_DisRelError( &dVal );
		pPS2->get_DisRelError( &dVal2 );
		szData.Format( _T("Maximum relative intersample distance: %.2f [DEFAULT=%.2f]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_DisAbsError( &dVal );
		pPS2->get_DisAbsError( &dVal2 );
		szData.Format( _T("Maximum absolute intersample distance: %.2f [DEFAULT=%.2f]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_DisError( &dVal );
		pPS2->get_DisError( &dVal2 );
		szData.Format( _T("Relative intersample period error: %.2f [DEFAULT=%.2f]\n"), dVal, dVal2 );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Segmentation
		f.WriteString( _T("SEGMENTATION\n") );
		f.WriteString( _T("________________________\n") );
		pPSF->get_SEG_F( &fVal );
		pPSF2->get_SEG_F( &fVal2 );
		szData.Format( _T("Add first segment at any rate: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_SEG_L( &fVal );
		pPSF2->get_SEG_L( &fVal2 );
		szData.Format( _T("Add last segment at any rate: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_SEG_O( &fVal );
		pPSF2->get_SEG_O( &fVal2 );
		szData.Format( _T("The entire trial is one stroke: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_SEG_V( &fVal );
		pPSF2->get_SEG_V( &fVal2 );
		szData.Format( _T("Move segmentation point to nearest pendown if on a penlift: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_SEG_MM( &fVal );
		pPSF2->get_SEG_MM( &fVal2 );
		szData.Format( _T("Move segmentation point to nearest absolute velocity minima within stroke: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_SEG_S( &fVal );
		pPSF2->get_SEG_S( &fVal2 );
		szData.Format( _T("Submovement analysis: Add before each stroke primary and secondary submovements: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_SEG_A( &fVal );
		pPSF2->get_SEG_A( &fVal2 );
		BOOL fA = fVal, fA2 = fVal2;
		pPSF->get_SEG_M( &fVal );
		pPSF2->get_SEG_M( &fVal2 );
		BOOL fM = fVal, fM2 = fVal2;
		pPSF->get_SEG_J( &fVal );
		pPSF2->get_SEG_J( &fVal2 );
		BOOL fJ = fVal, fJ2 = fVal2;
		if( fA ) fA = fM = fJ = FALSE;
		if( fA2 ) fA2 = fM2 = fJ2 = FALSE;
		szData.Format( _T("Segmentation Method: %s [DEFAULT=%s]\n"),
					   fJ ? _T("At pendown trajectories") :
							  ( fM ? _T("At absolute velocity minima") : _T("At vertical velocity zero crossings") ),
					   fJ2 ? _T("At pendown trajectories") :
							  ( fM2 ? _T("At absolute velocity minima") : _T("At vertical velocity zero crossings") ) );
		f.WriteString( szData );
		pPS->get_MinStrokeSize( &dVal );
		pPS2->get_MinStrokeSize( &dVal2 );
		szData.Format( _T("Min. stroke size (cm): %g [DEFAULT=%g]\n" ), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MinStrokeSizeRelativeToMax( &dVal );
		pPS2->get_MinStrokeSizeRelativeToMax( &dVal2 );
		szData.Format( _T("Min. stroke size relative to max stroke: %g [DEFAULT=%g]\n" ), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MinStrokeDuration( &dVal );
		pPS2->get_MinStrokeDuration( &dVal2 );
		szData.Format( _T("Min. stroke duration (s): %g [DEFAULT=%g]\n" ), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MinVertVelocity( &dVal );
		pPS2->get_MinVertVelocity( &dVal2 );
		szData.Format( _T("Min. velocity of first/last stroke (relative to abs. peak): %g [DEFAULT=%g]\n" ), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MaxVertVelocity( &dVal );
		pPS2->get_MaxVertVelocity( &dVal2 );
		szData.Format( _T("Max. noise velocity of first/last stroke (cm/s): %g [DEFAULT=%g]\n" ), dVal, dVal2 );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Extraction
		f.WriteString( _T("EXTRACTION\n") );
		f.WriteString( _T("________________________\n") );
		pPSF->get_EXT_O( &fVal );
		pPSF2->get_EXT_O( &fVal2 );
		szData.Format( _T("Onestroke analysis combines all strokes as if it were one: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_EXT_2( &fVal );
		pPSF2->get_EXT_2( &fVal2 );
		szData.Format( _T("Calculate loop area between every consecutive 2 strokes, instead of every consecutive stroke: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_StrokeBeginning( &dVal );
		pPS2->get_StrokeBeginning( &dVal2 );
		szData.Format( _T("Initial part of stroke for departure of initial (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MaxFrequency( &dVal );
		pPS2->get_MaxFrequency( &dVal2 );
		szData.Format( _T("Maximum frequency of acceleration and deceleration: %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Consis
		f.WriteString( _T("CONSISTENCY CHECKING\n") );
		f.WriteString( _T("________________________\n") );
		pPSF->get_CON_A( &fVal );
		pPSF2->get_CON_A( &fVal2 );
		szData.Format( _T("Use absolute measures instead of relative measures: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_CON_M( &fVal );
		pPSF2->get_CON_M( &fVal2 );
		szData.Format( _T("Discard if more than the max number of strokes: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_CON_U( &fVal );
		pPSF2->get_CON_U( &fVal2 );
		szData.Format( _T("Remove initial downstroke: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_CON_D( &fVal );
		pPSF2->get_CON_D( &fVal2 );
		szData.Format( _T("Discard if discontinuity: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_CON_C( &fVal );
		pPSF2->get_CON_C( &fVal2 );
		szData.Format( _T("Discard trial if first stroke starts within the reaction time: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_MinLoopArea( &dVal );
		pPS2->get_MinLoopArea( &dVal2 );
		szData.Format( _T( "Minimum loop area (cm2): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );

		pPS->get_SpiralIncreaseFactor( &dVal );
		pPS2->get_SpiralIncreaseFactor( &dVal2 );
		szData.Format( _T( "Minimum relative stroke size increase ratio between succesive strokes in outward spiral: %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_SlantError( &dVal );
		pPS2->get_SlantError( &dVal2 );
		szData.Format( _T( "Maximum slant error for strokes of a certain direction (rad): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MaxStraightErrorStraight( &dVal );
		pPS2->get_MaxStraightErrorStraight( &dVal2 );
		szData.Format( _T( "Maximum straightness error for straight segments: %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_MinStraightErrorCurved( &dVal );
		pPS2->get_MinStraightErrorCurved( &dVal2 );
		szData.Format( _T( "Minimum straightness departure for curved segments: %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );

		pPS->get_DiscardLTMin( &fVal );
		pPS2->get_DiscardLTMin( &fVal2 );
		szData.Format( _T("Discard trial if reaction time is < Min. reaction time: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_MinReactionTime( &dVal );
		pPS2->get_MinReactionTime( &dVal2 );
		szData.Format( _T("Min. reaction time (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		pPS->get_DiscardGTMax( &fVal );
		pPS2->get_DiscardGTMax( &fVal2 );
		szData.Format( _T("Discard trial if reaction time is > Max. reaction time: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_MaxReactionTime( &dVal );
		pPS2->get_MaxReactionTime( &dVal2 );
		szData.Format( _T("Max. reaction time (s): %g [DEFAULT=%g]\n"), dVal, dVal2 );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// External apps
		ExternalAppType eat;
		BSTR bstr = NULL;
		f.WriteString( _T("EXTERNAL APPLICATION PROCESSING\n") );
		f.WriteString( _T("________________________\n") );
		pPSRE->get_ExtAppTypeRaw( &eat );
		pPSRE->get_ExtAppScriptRaw( &bstr );
		szItem = bstr;
		if( szItem == _T("") ) { szItem = _T("NONE"); bstr = szItem.AllocSysString(); }
		switch( eat )
		{
			case eEAT_Matlab: szItem = _T("Matlab"); break;
			case eEAT_Batch: szItem = _T("Batch File"); break;
			case eEAT_None: szItem = _T("NOTHING");
		}
		szData.Format( _T("Run external application processing on RAW data file using: %s with script: %s\n"),
					   szItem, CString( bstr ) );
		f.WriteString( szData );
		pPSRE->get_ExtAppTypeTF( &eat );
		pPSRE->get_ExtAppScriptTF( &bstr );
		szItem = bstr;
		if( szItem == _T("") ) { szItem = _T("NONE"); bstr = szItem.AllocSysString(); }
		switch( eat )
		{
			case eEAT_Matlab: szItem = _T("Matlab"); break;
			case eEAT_Batch: szItem = _T("Batch File"); break;
			case eEAT_None: szItem = _T("NOTHING");
		}
		szData.Format( _T("Run external application processing on TF data file using: %s with script: %s\n"),
						szItem, CString( bstr ) );
		f.WriteString( szData );
		pPSRE->get_ExtAppTypeSeg( &eat );
		pPSRE->get_ExtAppScriptSeg( &bstr );
		szItem = bstr;
		if( szItem == _T("") ) { szItem = _T("NONE"); bstr = szItem.AllocSysString(); }
		switch( eat )
		{
			case eEAT_Matlab: szItem = _T("Matlab"); break;
			case eEAT_Batch: szItem = _T("Batch File"); break;
			case eEAT_None: szItem = _T("NOTHING");
		}
		szData.Format( _T("Run external application processing on SEG data file using: %s with script: %s\n"),
						szItem, CString( bstr ) );
		f.WriteString( szData );
		pPSRE->get_ExtAppTypeExt( &eat );
		pPSRE->get_ExtAppScriptExt( &bstr );
		szItem = bstr;
		if( szItem == _T("") ) { szItem = _T("NONE"); bstr = szItem.AllocSysString(); }
		switch( eat )
		{
			case eEAT_Matlab: szItem = _T("Matlab"); break;
			case eEAT_Batch: szItem = _T("Batch File"); break;
			case eEAT_None: szItem = _T("NOTHING");
		}
		szData.Format( _T("Run external application processing on EXT data file using: %s with script: %s\n"),
						szItem, CString( bstr ) );
		f.WriteString( szData );
		f.WriteString( _T("\n") );

		// Summarization
		f.WriteString( _T("SUMMARIZATION\n") );
		f.WriteString( _T("________________________\n") );
		pPSF->get_ST_A( &fVal );
		pPSF2->get_ST_A( &fVal2 );
		szData.Format( _T("Absolute values: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_ST_R( &fVal );
		pPSF2->get_ST_R( &fVal2 );
		szData.Format( _T("Relative data = data / mean_across_strokes: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_ST_Z( &fVal );
		pPSF2->get_ST_Z( &fVal2 );
		szData.Format( _T("Output missing strokes as zeroes: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_ST_T( &fVal );
		pPSF2->get_ST_T( &fVal2 );
		szData.Format( _T("Output missing trials as zeroes: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_ST_S( &fVal );
		pPSF2->get_ST_S( &fVal2 );
		szData.Format( _T("Substitute missing trials: %s [DEFAULT=%s]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPSF->get_ST_D( &fVal );
		pPSF2->get_ST_D( &fVal2 );
		pPSF->get_DiscardAfter( &nVal );
		pPSF2->get_DiscardAfter( &nVal2 );
		szData.Format( _T("Discard substituted trials: %s after #: %d [DEFAULT=%s, %d]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), nVal, fVal2 ? _T("TRUE") : _T("FALSE"), nVal2 );
		f.WriteString( szData );
		pPSF->get_ST_D2( &fVal );
		pPSF2->get_ST_D2( &fVal2 );
		pPSF->get_DiscardAfter2( &nVal );
		pPSF2->get_DiscardAfter2( &nVal2 );
		szData.Format( _T("Discard trials: %s after #: %d [DEFAULT=%s, %d]\n"),
					   fVal ? _T("TRUE") : _T("FALSE"), nVal, fVal2 ? _T("TRUE") : _T("FALSE"), nVal2 );
		f.WriteString( szData );
		pPSF->get_DiscardPenDownDurMin( &fVal );
		pPSF2->get_DiscardPenDownDurMin( &fVal2 );
		pPSF->get_PenDownDurMin( &dVal );
		pPSF2->get_PenDownDurMin( &dVal2 );
		szData.Format( _T("Discard strokes with pen-lift: %s < : %.2f [DEFAULT=%s, %.2f]\n"),
			fVal ? _T("TRUE") : _T("FALSE"), dVal, fVal2 ? _T("TRUE") : _T("FALSE"), dVal2 );
		f.WriteString( szData );
		pPS->get_CollapseUDStrokes( &fVal );
		pPS2->get_CollapseUDStrokes( &fVal2 );
		szData.Format( _T("Collapse Up/Down Strokes: %s [DEFAULT=%s]\n"),
			fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_CollapseStrokes( &fVal );
		pPS2->get_CollapseStrokes( &fVal2 );
		szData.Format( _T("Collapse Strokes: %s [DEFAULT=%s]\n"),
			fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_CollapseTrials( &fVal );
		pPS2->get_CollapseTrials( &fVal2 );
		szData.Format( _T("Collapse Trials: %s [DEFAULT=%s]\n"),
			fVal ? _T("TRUE") : _T("FALSE"), fVal2 ? _T("TRUE") : _T("FALSE") );
		f.WriteString( szData );
		pPS->get_CollapseMedian( &fVal );
		pPS2->get_CollapseMedian( &fVal2 );
		szData.Format( _T("Collapse Using: %s [DEFAULT=%s]\n"),
			fVal ? _T("Median") : _T("Averages"), fVal2 ? _T("Median") : _T("Averages") );
		f.WriteString( szData );
	}

	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szSET;
	WinExec( szCmd, SW_SHOW );

	pPSF->Release();
	pPSF2->Release();
	pPSRE->Release();
	pPSRE2->Release();
	pPS->Release();
	pPS2->Release();
}

//************************************
// Method:    OnUpdateEReportprocset
// FullName:  CLeftView::OnUpdateEReportprocset
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEReportprocset(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCReprocess
// FullName:  CLeftView::OnCReprocess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCReprocess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	// Which condition
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	_CondID = szItem.Mid( nPos + 2 );

	_Reprocessing = TRUE;
	_ReprocAll = TRUE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	CString szData;
	szData.Format( IDS_AL_REPROCC, _CondID );
	OutputAMessage( szData );

	CWinThread* pThread = AfxBeginThread( ReprocessExperimentCond, 0 );
}

//************************************
// Method:    OnUpdateCReprocess
// FullName:  CLeftView::OnUpdateCReprocess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCReprocess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCProcess
// FullName:  CLeftView::OnCProcess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCProcess() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	// Which condition
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	_CondID = szItem.Mid( nPos + 2 );

	_Reprocessing = TRUE;
	_ReprocAll = FALSE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	CString szData;
	szData.Format( IDS_AL_REPROCC, _CondID );
	OutputAMessage( szData );

	CWinThread* pThread = AfxBeginThread( ReprocessExperimentCond, 0 );
}

//************************************
// Method:    OnUpdateCProcess
// FullName:  CLeftView::OnUpdateCProcess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCProcess(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    ReprocessExperimentCond
// FullName:  ReprocessExperimentCond
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT ReprocessExperimentCond( LPVOID pParam )
{
	CMainFrame* pMF = _MF;
	CLeftView* pLV = _LV;
	CListView* pMsgWnd = _MsgWnd;
	if( !pMF || !pLV || !pMsgWnd )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	CTreeCtrl& tree = pLV->GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	HTREEITEM hExp = tree.GetSelectedItem();	// Condition
	hExp = tree.GetParentItem( hExp );						// Condition folder
	hExp = tree.GetParentItem( hExp );						// Experiment
	CString szItem = tree.GetItemText( hExp ); 
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 )
	{
		_Reprocessing = FALSE;
		return 0;
	}
	CString szExpID = szItem.Mid( nPos + 2 );

	// Hide trials if not detailed output
	if( !::GetDetailedOutput() ) pLV->HideTrials( szExpID );

	// Loop through all subjects in experiment to get count
	HTREEITEM hItem = tree.GetNextItem( hExp, TVGN_CHILD );	// Group folder
	DWORD dwData = tree.GetItemData( hItem );
	if( dwData != TI_GROUP_FOLDER )
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	hItem = tree.GetNextItem( hItem, TVGN_CHILD );			// 1st group
	HTREEITEM hSubj = NULL;
	int nCount = 0;

	CLeftView::SetActionState( ACTION_REPROCESS );
	while( hItem )						// loop through each group
	{
		hSubj = tree.GetNextItem( hItem, TVGN_CHILD );		// subject folder
		hSubj = tree.GetNextItem( hSubj, TVGN_CHILD );		// 1st subject
		while( hSubj )
		{
			nCount++;
			hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );
		}

		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// Loop through all subjects in experiment
	hItem = tree.GetNextItem( hExp, TVGN_CHILD );	// Group folder
	dwData = tree.GetItemData( hItem );
	if( dwData != TI_GROUP_FOLDER )
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	hItem = tree.GetNextItem( hItem, TVGN_CHILD );			// 1st group
	CString szGrpID;
	int nCur = 0;

	HRESULT hRes = CoInitialize( NULL );
	_ASSERTE( SUCCEEDED( hRes ) );

	CLeftView::SetActionState( ACTION_REPROCESS );
	Progress::UseCallbacks( TRUE );
	while( hItem && !_TerminateThread )						// loop through each group
	{
		szItem = tree.GetItemText( hItem );
		nPos = szItem.Find( szDelim );
		if( nPos == -1 ) break;
		szGrpID = szItem.Mid( nPos + 2 );
		hSubj = tree.GetNextItem( hItem, TVGN_CHILD );		// subject folder
		hSubj = tree.GetNextItem( hSubj, TVGN_CHILD );		// 1st subject
		while( hSubj && !_TerminateThread )
		{
			// Reprocess this subject
			szItem = tree.GetItemText( hSubj );
			nPos = szItem.Find( szDelim );
			if( nPos != -1 )
			{
				szItem = szItem.Mid( nPos + 2 );
				// get subject sampling rate & device res
				double dDevRes = 0.;
				int nSampRate = 0;
				HRESULT hr = pLV->m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
												szItem.AllocSysString() );
				if( SUCCEEDED( hr ) )
				{
					pLV->m_pEML->get_DeviceRes( &dDevRes );
					short nTemp = 0;
					pLV->m_pEML->get_SamplingRate( &nTemp );
					nSampRate = nTemp;
				}

				nCur++;
				pMF->ReprocessSubjectCond( szExpID, szItem, szGrpID, _CondID, dDevRes,
										   nSampRate, hSubj, pLV, pMsgWnd, nCount, nCur, _ReprocAll );

				HTREEITEM hTrials = tree.GetChildItem( hSubj );
				if( hTrials ) tree.Expand( hTrials, TVE_COLLAPSE );
			}

			hSubj = tree.GetNextItem( hSubj, TVGN_NEXT );
		}

		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}

	BOOL fMsg = FALSE;
	// inform that some trials might not be visible
	if( ::GetDetailedOutput() && !_ReprocAll )
	{
		BCGPMessageBox( _T("Some trials might not be visible in the tree view.\nTo show all trials, View -> Refresh.") );
		fMsg = TRUE;
	}

	// Ask to summarize
	CFileFind ff;
	CString szInc, szErr;
	GetExpINC( szExpID, szInc );
	GetExpERR( szExpID, szErr );
	if( BCGPMessageBox( IDS_CLEANUP, MB_YESNO ) == IDYES )
	{
		CString szMsg;
		if( ff.FindFile( szInc ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szInc );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove TMP file
		CString szTmp = szInc.Left( szInc.GetLength() - 3 );
		szTmp += _T("tmp");
		if( ff.FindFile( szTmp ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove ERR file
		if( ff.FindFile( szErr ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szErr );
			OutputAMessage( szMsg, TRUE );
		}
		// Remove AXS memory file
		szTmp = szInc.Left( szInc.GetLength() - 7 );	// exp.axs
		szTmp += _T("graph.axs");
		if( ff.FindFile( szTmp ) )
		{
			try
			{
				CFile::Remove( szInc );
			}
			catch( CFileException* e )
			{
				char pszErr[ 200 ];
				e->GetErrorMessage( pszErr, 200 );
				e->Delete();
				szMsg = pszErr;
				szMsg += _T("\n\nSummarize cannot continue.\nPlease ensure there all windows and applications viewing this file are closed.");
				BCGPMessageBox( szMsg, MB_ICONERROR );
				CLeftView::SetActionState( ACTION_NONE );
				Progress::UseCallbacks( FALSE );
				_Reprocessing = FALSE;
				CoUninitialize();
				return 0;
			}
			szMsg.Format( _T("Deleting file: %s"), szTmp );
			OutputAMessage( szMsg, TRUE );
		}

		// summarize
		pMF->SummarizeExperiment( szExpID, pMsgWnd );

		fMsg = TRUE;
	}

	// message user processing is done if no other message presented
	if( !fMsg ) BCGPMessageBox( _T("Processing complete.") );

	// cleanup
	CLeftView::SetActionState( ACTION_NONE );
	Progress::UseCallbacks( FALSE );
	_Reprocessing = FALSE;
	CoUninitialize();
	return 0;
}

//************************************
// Method:    OnObjStop
// FullName:  CLeftView::OnObjStop
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnObjStop() 
{
	switch( _ActionState )
	{
		case ACTION_IMPORTEXPORT:
			ZipProgress::Stop();
			break;
		case ACTION_STIMULUS:
		case ACTION_RUNEXPERIMENT:
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			if( pMF )
			{
				pMF->StopExperiment();
				CString szData = _T("RECORDING: Stopping experiment.");
				OutputAMessage( szData, TRUE );
				_ActionState = ACTION_NONE;
			}
			break;
		}
		case ACTION_REPROCESS:
			OnEStopreprocess();
			OutputAMessage( _T("Reprocessing stopped...will complete current condition."), TRUE );
			break;
		case ACTION_SUMMARIZE:
			_TerminateThread = TRUE;
			_ActionState = ACTION_NONE;
			OutputAMessage( _T("Summarization stopped...will complete current subject."), TRUE );
			break;
		case ACTION_TEST:
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			if( pMF ) pMF->OnDigTest2();
			break;
		}
		case ACTION_BACKUP:
			_TerminateThread = TRUE;
			break;
	}
}

//************************************
// Method:    OnUpdateObjStop
// FullName:  CLeftView::OnUpdateObjStop
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateObjStop(CCmdUI* pCmdUI) 
{
	if( _ActionState == ACTION_NONE )
		pCmdUI->Enable( FALSE );
	else
		pCmdUI->Enable( TRUE );
}

//************************************
// Method:    ShowStatGraph
// FullName:  CLeftView::ShowStatGraph
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: CString szExpID
//************************************
void CLeftView::ShowStatGraph( CString szExpID )
{
	ShowStatGraph( szExpID, FALSE, MODE_COMBO );
}

//************************************
// Method:    ShowStatGraph
// FullName:  CLeftView::ShowStatGraph
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: CString szExpID
// Parameter: BOOL fEqual
// Parameter: UINT nMode
//************************************
void CLeftView::ShowStatGraph( CString szExpID, BOOL fEqual, UINT nMode )
{
	// INC file (verify existence)
	CString szINC;
	::GetExpINC( szExpID, szINC );
	CFileFind ff;
	if( !ff.FindFile( szINC ) )
	{
		if( BCGPMessageBox( _T("Summary file (INC) missing.\n\nSummarize?"), MB_YESNO ) == IDYES )
		{
			OnESum();
			// verify again
			if( !ff.FindFile( szINC ) ) return;
		}
		else return;
	}

	CString szMsg;
	if( nMode == MODE_SCATTER )
	{
		szMsg.LoadString( IDS_AL_STATGRAPH );
		OutputAMessage( szMsg, TRUE );
	}
	else
	{
		szMsg.LoadString( IDS_AL_SCATGRAPH );
		OutputAMessage( szMsg, TRUE );
	}
//	AGraphDlg* d = new AGraphDlg( szExpID, szINC, fEqual, nMode, this );
	AGraphDlg d( szExpID, szINC, fEqual, nMode, this );
	d.DoModal();
}

//************************************
// Method:    ShowStatGraph
// FullName:  CLeftView::ShowStatGraph
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: BOOL fEqual
// Parameter: UINT nMode
//************************************
void CLeftView::ShowStatGraph( BOOL fEqual, UINT nMode )
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	HTREEITEM hExp = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hExp ); 
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	ShowStatGraph( szExpID, fEqual, nMode );
}

//************************************
// Method:    OnUpdateStatGraph
// FullName:  CLeftView::OnUpdateStatGraph
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStatGraph( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEAAvgs
// FullName:  CLeftView::OnEAAvgs
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAAvgs() 
{
	ShowStatGraph( FALSE, MODE_AVERAGE );
}

//************************************
// Method:    OnUpdateEAAvgs
// FullName:  CLeftView::OnUpdateEAAvgs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAAvgs(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEAAvgs2
// FullName:  CLeftView::OnEAAvgs2
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAAvgs2() 
{
	ShowStatGraph( TRUE, MODE_AVERAGE );
}

//************************************
// Method:    OnUpdateEAAvgs2
// FullName:  CLeftView::OnUpdateEAAvgs2
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAAvgs2(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEAStddevs
// FullName:  CLeftView::OnEAStddevs
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAStddevs() 
{
	ShowStatGraph( FALSE, MODE_STDDEV );
}

//************************************
// Method:    OnUpdateEAStddevs
// FullName:  CLeftView::OnUpdateEAStddevs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAStddevs(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEAStddevs2
// FullName:  CLeftView::OnEAStddevs2
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAStddevs2() 
{
	ShowStatGraph( TRUE, MODE_STDDEV );
}

//************************************
// Method:    OnUpdateEAStddevs2
// FullName:  CLeftView::OnUpdateEAStddevs2
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAStddevs2(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEATrial
// FullName:  CLeftView::OnEATrial
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEATrial() 
{
	ShowStatGraph( FALSE, MODE_STDDEVN );
}

//************************************
// Method:    OnUpdateEATrial
// FullName:  CLeftView::OnUpdateEATrial
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEATrial(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnTReprocessNoext
// FullName:  CLeftView::OnTReprocessNoext
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTReprocessNoext() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( pMF && pMsgWnd )
	{
		CString szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );

		// Trial
		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItemT = tree.GetSelectedItem();
		CString szTrial = tree.GetItemText( hItemT );
		// Which subject
		HTREEITEM hItem = tree.GetParentItem( hItemT );	// Trial folder
		hItem = tree.GetParentItem( hItem );			// Subject
		CString szItem = tree.GetItemText( hItem );
		int nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return;
		CString szSubjID = szItem.Mid( nPos + 2 );
		// Which experiment
		HTREEITEM hItem2 = tree.GetParentItem( hItem );	// Subject folder
		hItem2 = tree.GetParentItem( hItem2 );			// Group
		szItem = tree.GetItemText( hItem2 );
		nPos = szItem.Find( szDelim );
		CString szGrpID = szItem.Mid( nPos + 2 );
		hItem2 = tree.GetParentItem( hItem2 );			// Group folder
		hItem2 = tree.GetParentItem( hItem2 );			// Experiment
		szItem = tree.GetItemText( hItem2 ); 
		nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return;
		CString szExpID = szItem.Mid( nPos + 2 );
		// Remove the trial
//		tree.DeleteItem( hItemT );

		// get subject sampling rate & device res
		double dDevRes = 0.;
		int nSampRate = 0;
		HRESULT hr = m_pEML->Find( szExpID.AllocSysString(), szGrpID.AllocSysString(),
								   szSubjID.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			m_pEML->get_DeviceRes( &dDevRes );
			short nTemp = 0;
			m_pEML->get_SamplingRate( &nTemp );
			nSampRate = nTemp;
		}

		_Reprocessing = TRUE;
		pMF->ReprocessTrial( szExpID, szSubjID, szGrpID, szTrial, dDevRes, nSampRate, hItem, FALSE );
		_Reprocessing = FALSE;
	}
}

//************************************
// Method:    OnUpdateTReprocessNoext
// FullName:  CLeftView::OnUpdateTReprocessNoext
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTReprocessNoext(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_TRIAL );
	fEnable &= !_Reprocessing;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    GetSelectedTrialLocation
// FullName:  CLeftView::GetSelectedTrialLocation
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString & szFile
// Parameter: CString & szPath
//************************************
BOOL CLeftView::GetSelectedTrialLocation( CString& szFile, CString& szPath, BOOL& fQuick )
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	fQuick = FALSE;

	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return FALSE;
	DWORD dwType = tree.GetItemData( hItem );
	if( ( dwType != TI_TRIAL ) && ( dwType != TI_IMAGE ) ) return FALSE;

	int nPos = -1;
	CString szSubj, szGrp, szExp;
	CString szItem = tree.GetItemText( hItem );	// Trial
	hItem = tree.GetParentItem( hItem );		// Trial folder (or Quick Link)
	hItem = tree.GetParentItem( hItem );		// Subject (if not quick link)
	if( hItem )
	{
		szSubj = tree.GetItemText( hItem );
		nPos = szSubj.Find( szDelim );
		szSubj = szSubj.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );	// Subject folder
		hItem = tree.GetParentItem( hItem );	// Group
		szGrp = tree.GetItemText( hItem );
		nPos = szGrp.Find( szDelim );
		szGrp = szGrp.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );	// Group folder
		hItem = tree.GetParentItem( hItem );	// Experiment
		szExp = tree.GetItemText( hItem );
		nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szItem.Left( 3 );
		szGrp = szItem.Mid( 3, 3 );
		szSubj = szItem.Mid( 6, 3 );
		fQuick = TRUE;
	}

	szFile = szItem;
	GetHWR( szExp, szGrp, szSubj, szItem, szPath );
	nPos = szPath.Find( szFile );
	szPath = szPath.Left( nPos - 1 );

	return TRUE;
}

//************************************
// Method:    OnDigTest
// FullName:  CLeftView::OnDigTest
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnDigTest()
{
	if( ( _ActionState != ACTION_NONE ) && ( _ActionState != ACTION_TEST ) ) return;
	if( _ActionState == ACTION_NONE ) _ActionState = ACTION_TEST;
	else _ActionState = ACTION_NONE;
}

//************************************
// Method:    OnESortgroups2
// FullName:  CLeftView::OnESortgroups2
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
//************************************
BOOL CLeftView::OnESortgroups2( CString szExp ) 
{
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return FALSE;

	// if szExp (ID) is not provided, we assume this has been activated from tree
	if( szExp == _T("") )
	{
		szExp = tree.GetItemText( hItem );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}

	// Get the groups
	CStringList lst;
	if( !m_pEML ) return FALSE;
	HRESULT hr = m_pEML->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) ) return FALSE;

	BSTR bstrID = NULL;
	CString szItem;
	while( SUCCEEDED( hr ) )
	{
		m_pEML->get_GroupID( &bstrID );
		szItem = bstrID;
		if( !lst.Find( szItem ) ) lst.AddTail( szItem );

		hr = m_pEML->GetNext();
	}
	if( lst.GetCount() == 0 )
	{
		BCGPMessageBox( _T("There are no groups to sort.") );
		return FALSE;
	}

	OutputAMessage( _T("Viewing/changing analysis graphing sort order of groups."), TRUE );

	SortDlg d( SORT_GROUPS, szExp, &lst, this );
	if( d.DoModal() == IDOK ) return TRUE;
	else return FALSE;
}

//************************************
// Method:    OnESortgroups
// FullName:  CLeftView::OnESortgroups
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnESortgroups() 
{
	OnESortgroups2();
}

//************************************
// Method:    OnESortconds2
// FullName:  CLeftView::OnESortconds2
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
//************************************
BOOL CLeftView::OnESortconds2( CString szExp ) 
{
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return FALSE;

	// if szExp (ID) is not provided, we assume this has been activated from tree
	if( szExp == _T("") )
	{
		szExp = tree.GetItemText( hItem );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}

	// Get the conditions
	CStringList lst;
	if( !m_pECL ) return FALSE;
	HRESULT hr = m_pECL->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) ) return FALSE;

	BSTR bstrID = NULL;
	CString szItem;
	while( SUCCEEDED( hr ) )
	{
		m_pECL->get_ConditionID( &bstrID );
		szItem = bstrID;
		if( !lst.Find( szItem ) ) lst.AddTail( szItem );

		hr = m_pECL->GetNext();
	}
	if( lst.GetCount() == 0 )
	{
		BCGPMessageBox( _T("There are no conditions to sort.") );
		return FALSE;
	}

	OutputAMessage( _T("Viewing/changing analysis graphing sort order of conditions."), TRUE );

	SortDlg d( SORT_CONDITIONS, szExp, &lst, this );
	if( d.DoModal() == IDOK ) return TRUE;
	else return FALSE;
}

//************************************
// Method:    OnESortconds
// FullName:  CLeftView::OnESortconds
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnESortconds() 
{
	OnESortconds2();
}

//************************************
// Method:    OnESortsubjs2
// FullName:  CLeftView::OnESortsubjs2
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
//************************************
BOOL CLeftView::OnESortsubjs2( CString szExp ) 
{
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return FALSE;

	// if szExp (ID) is not provided, we assume this has been activated from tree
	if( szExp == _T("") )
	{
		szExp = tree.GetItemText( hItem );
		int nPos = szExp.Find( szDelim );
		szExp = szExp.Mid( nPos + 2 );
	}

	// Get the subjects
	CStringList lst;
	if( !m_pEML ) return FALSE;
	HRESULT hr = m_pEML->FindForExperiment( szExp.AllocSysString() );
	if( FAILED( hr ) ) return FALSE;

	BSTR bstrID = NULL;
	CString szItem;
	while( SUCCEEDED( hr ) )
	{
		m_pEML->get_SubjectID( &bstrID );
		szItem = bstrID;
		if( !lst.Find( szItem ) ) lst.AddTail( szItem );

		hr = m_pEML->GetNext();
	}
	if( lst.GetCount() == 0 )
	{
		BCGPMessageBox( _T("There are no subjects to sort.") );
		return FALSE;
	}

	OutputAMessage( _T("Viewing/changing analysis graphing sort order of subjects."), TRUE );

	SortDlg d( SORT_SUBJECTS, szExp, &lst, this );
	if( d.DoModal() == IDOK ) return TRUE;
	else return FALSE;
}

//************************************
// Method:    OnESortsubjs
// FullName:  CLeftView::OnESortsubjs
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnESortsubjs() 
{
	OnESortsubjs2();
}

//************************************
// Method:    OnUpdateESortsubjs
// FullName:  CLeftView::OnUpdateESortsubjs
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateESortsubjs(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateESortgroups
// FullName:  CLeftView::OnUpdateESortgroups
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateESortgroups(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateESortconds
// FullName:  CLeftView::OnUpdateESortconds
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateESortconds(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSMove
// FullName:  CLeftView::OnUpdateSMove
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSMove(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSMove
// FullName:  CLeftView::OnSMove
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSMove() 
{
	// Log info
	CString szData = _T("SRC: Subject - Move");
	OutputAMessage( szData, TRUE );

	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();

	// Subject ID
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubj = szItem.Mid( nPos + 2 );
	CString szSubjDisp = szItem;

	// Group ID
	HTREEITEM hGroup = tree.GetParentItem( hItem );
	hGroup = tree.GetParentItem( hGroup );
	szItem = tree.GetItemText( hGroup );
	CString szGrpDisp = szItem;
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// Experiment ID
	HTREEITEM hExp = tree.GetParentItem( hGroup );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// Show list of available groups (if any)
	MoveToGroupDlg d( this );
	d.m_szCurGrp = szGrpDisp;
	d.m_szCurSubj = szSubjDisp;
	hGroup = tree.GetParentItem( hGroup );		// Group folder
	hGroup = tree.GetNextItem( hGroup, TVGN_CHILD );
	while( hGroup )
	{
		szItem = tree.GetItemText( hGroup );
		nPos = szItem.Find( szDelim );
		d.m_lstItemsGrp.AddTail( szItem );
		hGroup = tree.GetNextItem( hGroup, TVGN_NEXT );
	}

	// list of subjects
	{
		CString szPath, szID;
		::GetDataPathRoot( szPath );
		Subjects subj;
		subj.SetDataPath( szPath );
		HRESULT hr = subj.ResetToStart();
		while( SUCCEEDED( hr ) )
		{
			subj.GetID( szID );
			subj.GetNameWithCodeID( szID, ::IsPrivacyOn() );
			d.m_lstItemsSubj.AddTail( szID );
			hr = subj.GetNext();
		}
	}

	// If no other groups and subjects, just return
	if( ( d.m_lstItemsGrp.GetCount() == 0 ) && ( d.m_lstItemsSubj.GetCount() == 0 ) )
	{
		BCGPMessageBox( _T("There are no others groups or subjects.") );
		return;
	}

	// Ask if sure
	szData.Format( IDS_SUBJMOVE_SURE, szSubj, szGrp, szExp );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	// show dialog
	if( d.DoModal() == IDCANCEL ) return;

	// Get ID of new group (if changed)
	nPos = d.m_szGrp.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrpNew = d.m_szGrp.Mid( nPos + 2 );
	BOOL fNewGrp = ( szGrpNew != szGrp );
	// Get ID of new subject (if changed)
	nPos = d.m_szSubj.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjNew = d.m_szSubj.Mid( nPos + 2 );
	BOOL fNewSubj = ( szSubjNew != szSubj );

	// notify
	if( fNewGrp && fNewSubj )
	{
		szData.Format( _T("Moving subject %s from group %s to group %s with new ID %s in experiment %s."),
					   szSubj, szGrp, szGrpNew, szSubjNew, szExp );
	}
	else if( fNewGrp )
	{
		szData.Format( _T("Moving subject %s from group %s to group %s in experiment %s."),
					   szSubj, szGrp, szGrpNew, szExp );
	}
	else if( fNewSubj )
	{

		szData.Format( _T("Moving subject ID %s to subject ID %s."), szSubj, szSubjNew );
	}
	else return;
	OutputAMessage( szData, TRUE );

	// EXPERIMENT/MEMBER Associations
	IExperimentMember* pEM = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
								   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}
	// Ensure the subject does not exist already in the new group
	szData.Format( _T("Ensuring subject %s does not already exist in group %s in experiment %s."),
				   szSubjNew, szGrpNew, szExp );
	OutputAMessage( szData, TRUE );
	hr = pEM->Find( szExp.AllocSysString(), szGrpNew.AllocSysString(),
					szSubjNew.AllocSysString() );
	if( SUCCEEDED( hr ) )
	{
		szData.Format( _T("This subject (%s) is already in the selected group (%s)."), szSubjNew, szGrpNew );
		BCGPMessageBox( szData, MB_OK | MB_ICONINFORMATION );
		pEM->Release();
		return;
	}

	// Delete original association from ExpMember DB
	szData.Format( _T("Deleting original association of subject %s to group %s in experiment %s."),
				   szSubj, szGrp, szExp );
	OutputAMessage( szData, TRUE );
	hr = pEM->Find( szExp.AllocSysString(), szGrp.AllocSysString(),
					szSubj.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_FIND_ERR );
		pEM->Release();
		return;
	}
	hr = pEM->Remove();
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to remove original association.") );
		pEM->Release();
		return;
	}
	::DataHasChanged();

	// Add new association to DB
	szData.Format( _T("Creating new association of subject %s with group %s in experiment %s."),
				   szSubjNew, szGrpNew, szExp );
	OutputAMessage( szData, TRUE );
	pEM->put_GroupID( szGrpNew.AllocSysString() );
	pEM->put_SubjectID( szSubjNew.AllocSysString() );
	hr = pEM->Add();
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to add new association.") );
		pEM->Release();
		return;
	}
	pEM->Release();

	// Move & rename all data files
	CFileFind ff;
	CString szPath, szSearch, szFile, szNew;
	::GetDataPathRoot( szPath );

	// Create new directory(ies) first
	szSearch.Format( _T("%s\\%s"), szPath, szExp );
	if( !ff.FindFile( szSearch ) ) _mkdir( szSearch );
	szSearch.Format( _T("%s\\%s\\%s"), szPath, szExp, szGrpNew );
	if( !ff.FindFile( szSearch ) ) _mkdir( szSearch );
	szSearch.Format( _T("%s\\%s\\%s\\%s"), szPath, szExp, szGrpNew, szSubjNew );
	szData.Format( _T("Creating directory %s if not already present."), szPath );
	OutputAMessage( szData, TRUE );
	if( !ff.FindFile( szSearch ) ) _mkdir( szSearch );

	// Now go through all and move
	OutputAMessage( _T("Moving files to new location."), TRUE );
	szSearch.Format( _T("%s\\%s\\%s\\%s\\*.*"), szPath, szExp, szGrp, szSubj );
	BOOL fFiles = FALSE;
	if( ff.FindFile( szSearch ) )
	{
		fFiles = TRUE;
		while( ff.FindNextFile() )
		{
			if( !ff.IsDirectory() )
			{
				// full file name & path
				szFile = ff.GetFilePath();
				// get just file name
				nPos = szFile.ReverseFind( '\\' );
				szSearch = szFile.Mid( nPos + 1 );
				if( szSearch.GetLength() > 9 )
				{
					// eliminate exp, group & subj codes (9 chars)
					szSearch = szSearch.Mid( 9 );
					// new destination
					szNew.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s"), szPath, szExp, szGrpNew,
								  szSubjNew, szExp, szGrpNew, szSubjNew, szSearch );
					// move file
					MoveFile( szFile, szNew );
				}
			}
		}
		if( !ff.IsDirectory() )
		{
			// full file name & path
			szFile = ff.GetFilePath();
			// get just file name
			nPos = szFile.ReverseFind( '\\' );
			szSearch = szFile.Mid( nPos + 1 );
			if( szSearch.GetLength() > 9 )
			{
				// eliminate exp, group & subj codes (9 chars)
				szSearch = szSearch.Mid( 9 );
				// new destination
				szNew.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s"), szPath, szExp, szGrpNew,
							  szSubjNew, szExp, szGrpNew, szSubjNew, szSearch );
				// move file
				MoveFile( szFile, szNew );
			}
		}
	}
	// Cleanup (process files & directories)
	OutputAMessage( _T("Deleting old files from original location."), TRUE );
	szSearch.Format( _T("%s\\%s\\%s\\%s\\*.*"), szPath, szExp, szGrp, szSubj );
	if( ff.FindFile( szSearch ) )
	{
		while( ff.FindNextFile() )
		{
			if( !ff.IsDirectory() )
			{
				szFile = ff.GetFilePath();
				REMOVE_FILE( szFile );
			}
		}
		if( !ff.IsDirectory() )
		{
			szFile = ff.GetFilePath();
			REMOVE_FILE( szFile );
		}
	}
	ff.Close();
 	szSearch.Format( _T("%s\\%s\\%s\\%s"), szPath, szExp, szGrp, szSubj );
 	_rmdir( szSearch );

	// We already have TREE location of subject to remove
	// ...now we need TREE location of new group...we have experiment
	hGroup = tree.GetChildItem( hExp );
	if( hGroup )
	{
		DWORD dwVal = tree.GetItemData( hGroup );
		if( dwVal == TI_CONDITION_FOLDER )
		{
			hGroup = tree.GetNextItem( hGroup, TVGN_NEXT );
			dwVal = hGroup ? tree.GetItemData( hGroup ) : TI_UNKNOWN;
			if( dwVal != TI_GROUP_FOLDER ) hGroup = NULL;
		}
	}
	if( !hGroup )
	{
		Refresh();
		return;
	}
	// Find new group
	hGroup = tree.GetChildItem( hGroup );
	if( !hGroup )
	{
		Refresh();
		return;
	}
	CString szComp = tree.GetItemText( hGroup );
	nPos = szComp.Find( szDelim );
	szComp = szComp.Mid( nPos + 2 );
	while( hGroup && ( szComp != szGrpNew ) )
	{
		hGroup = tree.GetNextItem( hGroup, TVGN_NEXT );
		szComp = tree.GetItemText( hGroup );
		nPos = szComp.Find( szDelim );
		if( nPos != -1 ) szComp = szComp.Mid( nPos + 2 );
		else hGroup = NULL;
	}
	if( hGroup )
	{
		hGroup = tree.GetChildItem( hGroup );
	}
	else
	{
		Refresh();
		return;
	}
	// Delete and readd
	{
		CString szPath, szID;
		::GetDataPathRoot( szPath );
		Subjects subj;
		subj.SetDataPath( szPath );
		HRESULT hr = subj.Find( szSubjNew.AllocSysString() );
		if( SUCCEEDED( hr ) ) subj.GetNameWithCodeID( szSubjDisp );
	}
	tree.DeleteItem( hItem );
	hItem = tree.InsertItem( szSubjDisp, IMG_SUBJECT, IMG_SUBJECT, hGroup );
	tree.SetItemData( hItem, TI_SUBJECT );
	tree.SelectItem( hItem );
	ShowTrials( szExp, szSubjNew, szGrpNew, hItem, FALSE, FALSE, TRUE );
	tree.Expand( hItem, TVE_EXPAND );
	tree.Expand( hItem, TVE_COLLAPSE );

	BCGPMessageBox( _T("Move complete.\n\nYou will need to summarize the experiment.") );
}

//************************************
// Method:    DoSTrials
// FullName:  CLeftView::DoSTrials
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: BOOL fDblClick
//************************************
void CLeftView::DoSTrials( BOOL fDblClick )
{
	CWaitCursor crs;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject?
	HTREEITEM hItem = tree.GetSelectedItem();			// Subject
	HTREEITEM hSubj = hItem;
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which experiment/group
	hItem = tree.GetParentItem( hItem );				// Subject folder
	hItem = tree.GetParentItem( hItem );				// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );				// Group folder
	hItem = tree.GetParentItem( hItem );				// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	ShowTrials( szExp, szSubj, szGrp, hSubj, !fDblClick );
}

//************************************
// Method:    DoSTrials
// FullName:  CLeftView::DoSTrials
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: HTREEITEM hItem
//************************************
void CLeftView::DoSTrials( HTREEITEM hItem )
{
	if( !hItem ) return;

	CWaitCursor crs;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject?
	HTREEITEM hSubj = hItem;
	DWORD dwData = tree.GetItemData( hSubj );
	if( dwData != TI_SUBJECT ) return;
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which experiment/group
	hItem = tree.GetParentItem( hItem );				// Subject folder
	hItem = tree.GetParentItem( hItem );				// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );				// Group folder
	hItem = tree.GetParentItem( hItem );				// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	ShowTrials( szExp, szSubj, szGrp, hSubj, FALSE );
}

//************************************
// Method:    OnSShowtrials
// FullName:  CLeftView::OnSShowtrials
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSShowtrials() 
{
	DoSTrials( FALSE );
}

//************************************
// Method:    OnUpdateSShowtrials
// FullName:  CLeftView::OnUpdateSShowtrials
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSShowtrials(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( ( dwType == TI_SUBJECT ) && ( _ActionState == ACTION_NONE ) );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEBackup
// FullName:  CLeftView::OnUpdateEBackup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEBackup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGBackup
// FullName:  CLeftView::OnUpdateGBackup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGBackup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_GROUP );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSBackup
// FullName:  CLeftView::OnUpdateSBackup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSBackup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTCondition
// FullName:  CLeftView::OnTCondition
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTCondition() 
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	// Extract condition
	szTrial = szTrial.Mid( 3 /*exp*/ + 3/*grp*/ + 3 /*subj*/ );
	CString szCond = szTrial.Left( 3 );

	if( cond.DoEdit( szCond, szExp, this ) )
	{
		::DataHasChanged();

		szTrial.Format( IDS_AL_CONDEDITED, szCond );
		OutputAMessage( szTrial );
	}
}

//************************************
// Method:    OnTExperiment
// FullName:  CLeftView::OnTExperiment
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTExperiment() 
{
	Experiments exp;
	if( !exp.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	// experiment properties
	if( exp.DoEdit( szExp, this, 0 ) )
	{
		::DataHasChanged();

		szTrial.Format( IDS_AL_EXPEDITED, szExp );
		OutputAMessage( szTrial );
	}
}

//************************************
// Method:    OnUpdateTCondition
// FullName:  CLeftView::OnUpdateTCondition
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTCondition(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTConditionReps
// FullName:  CLeftView::OnTConditionReps
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTConditionReps() 
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	// Extract condition
	szTrial = szTrial.Mid( 3 /*exp*/ + 3/*grp*/ + 3 /*subj*/ );
	CString szCond = szTrial.Left( 3 );

	if( cond.DoReps( szCond, szExp, this ) )
	{
		::DataHasChanged();

		szTrial.Format( IDS_AL_CONDEDITED, szCond );
		OutputAMessage( szTrial );
	}
}

//************************************
// Method:    OnUpdateTConditionReps
// FullName:  CLeftView::OnUpdateTConditionReps
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTConditionReps(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    VerifyBackupPath
// FullName:  CLeftView::VerifyBackupPath
// Access:    public 
// Returns:   BOOL
// Qualifier:
//************************************
BOOL CLeftView::VerifyBackupPath()
{
	if( !NSMUsers::VerifyBackupPath() )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		pMF->OnOPath2();
	}
	return TRUE;
}

//************************************
// Method:    BackupRaw
// FullName:  CLeftView::BackupRaw
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szGrp
// Parameter: CString szSubj
// Parameter: CString szRaw
//************************************
BOOL CLeftView::BackupRaw( CString szExp, CString szGrp, CString szSubj, CString szRaw )
{
	return NSMUsers::BackupRaw( szExp, szGrp, szSubj, szRaw );
}

//************************************
// Method:    OnSBackup
// FullName:  CLeftView::OnSBackup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSBackup() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	CWaitCursor crs;
	CWinThread* pThread = AfxBeginThread( DoBackupSubject, 0 );
}

//************************************
// Method:    DoBackupSubject
// FullName:  DoBackupSubject
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT DoBackupSubject( LPVOID pParam )
{
	CMainFrame* pMF = _MF;
	CLeftView* pLV = _LV;
	if( !pMF || !pLV )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	if( !NSMUsers::VerifyBackupPath() )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	NotesDlg nd( N_BACKUP, _T(""), _T(""), pLV );
	if( nd.DoModal() == IDCANCEL )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	CString szData = _T("SRC: Subject - Backup");
	OutputAMessage( szData, TRUE );

	CTreeCtrl& tree = pLV->GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject?
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which group?
	hItem = tree.GetParentItem( hItem );	// Subject Folder
	hItem = tree.GetParentItem( hItem );	// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );

	// Which experiment?
	hItem = tree.GetParentItem( hItem );	// Group Folder
	hItem = tree.GetParentItem( hItem );	// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	// BACKUP
	CFileFind ff;
	BOOL fCopy = FALSE;
	CString szFile, szBackup;
	int nCount = 0, nCur = 0;
	CLeftView::SetActionState( ACTION_BACKUP );

	::GetBackupPath( szBackup );

	CWaitCursor crs;

BeginHere:
	if( fCopy )
	{
		// enable progress
		pMF->EnableProgress( nCount );
		nCount = 0;
	}
	// Verify experiment directory
	szFile.Format( _T("%s\\%s"), szBackup, szExp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}
	}
	// Verify group directory
	szFile.Format( _T("%s\\%s\\%s"), szBackup, szExp, szGrp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}
	}

	// Backup
	if( ::BackupSubject( szExp, szGrp, szSubj, fCopy, nCount, TRUE ) != ACTION_BACKUP )
	{
		CLeftView::SetActionState( ACTION_NONE );
		_Reprocessing = FALSE;
		return 0;
	}

	if( !fCopy )
	{
		fCopy = TRUE;
		goto BeginHere;
	}
	else
	{
		HRESULT hRes = CoInitialize( NULL );
		_ASSERTE( SUCCEEDED( hRes ) );

		IBackup* pB = NULL;
		HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
									   IID_IBackup, (LPVOID*)&pB );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create backup object.") );
			// stop progress / cleanup
			pMF->DisableProgress();
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}

		CString szItem;
		::GetCurrentUser( szItem );
		pB->put_UserID( szItem.AllocSysString() );
		COleDateTime d = COleDateTime::GetCurrentTime();
		szItem = d.Format( _T("%m-%d-%Y %H:%M:%S") );
		pB->put_BackupDate( szItem.AllocSysString() );
		::GetBackupPath( szItem );
		pB->put_Path( szItem.AllocSysString() );
		pB->put_BackupType( eB_Subject );
		pB->put_Exp( szExp.AllocSysString() );
		pB->put_Grp( szGrp.AllocSysString() );
		pB->put_Subj( szSubj.AllocSysString() );
		pB->put_Notes( nd.m_szNotes.AllocSysString() );
		hr = pB->Add();
		if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to add backup to backup history table.") );
		pB->Release();
		CoUninitialize();
	}

	// stop progress / cleanup
	pMF->DisableProgress();
	CLeftView::SetActionState( ACTION_NONE );
	_Reprocessing = FALSE;

	BCGPMessageBox( _T("Subject backup completed successfully.") );

	return 0;
}

//************************************
// Method:    OnGBackup
// FullName:  CLeftView::OnGBackup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGBackup() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	CWaitCursor crs;
	CWinThread* pThread = AfxBeginThread( DoBackupGroup, 0 );
}

//************************************
// Method:    DoBackupGroup
// FullName:  DoBackupGroup
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT DoBackupGroup( LPVOID pParam )
{
	CMainFrame* pMF = _MF;
	CLeftView* pLV = _LV;
	if( !pMF || !pLV )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	if( !NSMUsers::VerifyBackupPath() )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	NotesDlg nd( N_BACKUP, _T(""), _T(""), pLV );
	if( nd.DoModal() == IDCANCEL )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	CString szData = _T("SRC: Group - Backup");
	OutputAMessage( szData, TRUE );

	CTreeCtrl& tree = pLV->GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which group?
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );

	// Which experiment?
	hItem = tree.GetParentItem( hItem );	// Group Folder
	hItem = tree.GetParentItem( hItem );	// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	// BACKUP
	CFileFind ff;
	BOOL fCopy = FALSE;
	CString szBackup, szFile;
	int nCount = 0;
	CLeftView::SetActionState( ACTION_BACKUP );

	::GetBackupPath( szBackup );

	CWaitCursor crs;

BeginHere:
	if( fCopy )
	{
		// Progress Meter steps
		pMF->EnableProgress( nCount );
		nCount = 0;
	}
	// Verify experiment directory
	szFile.Format( _T("%s\\%s"), szBackup, szExp );
	if( !ff.FindFile( szFile ) )
	{
		if( _mkdir( szFile ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}
	}

	// Backup
	if( ::BackupGroup( szExp, szGrp, fCopy, nCount, TRUE ) != ACTION_BACKUP )
	{
		CLeftView::SetActionState( ACTION_NONE );
		_Reprocessing = FALSE;
		return 0;
	}

	if( !fCopy )
	{
		fCopy = TRUE;
		goto BeginHere;
	}
	else
	{
		HRESULT hRes = CoInitialize( NULL );
		_ASSERTE( SUCCEEDED( hRes ) );

		IBackup* pB = NULL;
		HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
									   IID_IBackup, (LPVOID*)&pB );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create backup object.") );
			// stop progress / cleanup
			pMF->DisableProgress();
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}

		CString szItem;
		::GetCurrentUser( szItem );
		pB->put_UserID( szItem.AllocSysString() );
		COleDateTime d = COleDateTime::GetCurrentTime();
		szItem = d.Format( _T("%m-%d-%Y %H:%M:%S") );
		pB->put_BackupDate( szItem.AllocSysString() );
		::GetBackupPath( szItem );
		pB->put_Path( szItem.AllocSysString() );
		pB->put_BackupType( eB_Group );
		pB->put_Exp( szExp.AllocSysString() );
		pB->put_Grp( szGrp.AllocSysString() );
		pB->put_Notes( nd.m_szNotes.AllocSysString() );
		hr = pB->Add();
		if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to add backup to backup history table.") );
		pB->Release();
		CoUninitialize();
	}

	// stop progress / cleanup
	pMF->DisableProgress();

	CLeftView::SetActionState( ACTION_NONE );
	_Reprocessing = FALSE;

	BCGPMessageBox( _T("Group backup completed successfully.") );

	return 0;
}

//************************************
// Method:    OnEBackup
// FullName:  CLeftView::OnEBackup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEBackup() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
	if( !pMF || !pMsgWnd ) return;

	_Reprocessing = TRUE;
	_MF = pMF;
	_LV = this;
	_MsgWnd = pMsgWnd;
	_TerminateThread = FALSE;

	CWaitCursor crs;
	CWinThread* pThread = AfxBeginThread( DoBackupExperiment, 0 );
}

//************************************
// Method:    DoBackupExperiment
// FullName:  DoBackupExperiment
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT DoBackupExperiment( LPVOID pParam )
{
	CMainFrame* pMF = _MF;
	CLeftView* pLV = _LV;
	if( !pMF || !pLV )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	if( !NSMUsers::VerifyBackupPath() )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	NotesDlg nd( N_BACKUP, _T(""), _T(""), pLV );
	if( nd.DoModal() == IDCANCEL )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	CString szData = _T("SRC: Experiment - Backup");
	OutputAMessage( szData, TRUE );

	CTreeCtrl& tree = pLV->GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment?
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	// BACKUP
	BOOL fCopy = FALSE;
	CString szFile;
	int nCount = 0;
	CLeftView::SetActionState( ACTION_BACKUP );

BeginHere:
	if( fCopy )
	{
		// Progress Meter steps
		pMF->EnableProgress( nCount );
		nCount = 0;
	}

	// Backup
	if( ::BackupExperiment( szExp, fCopy, nCount, TRUE ) != ACTION_BACKUP )
	{
		CLeftView::SetActionState( ACTION_NONE );
		_Reprocessing = FALSE;
		return 0;
	}

	if( !fCopy )
	{
		fCopy = TRUE;
		goto BeginHere;
	}
	else
	{
		HRESULT hRes = CoInitialize( NULL );
		_ASSERTE( SUCCEEDED( hRes ) );

		IBackup* pB = NULL;
		HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
									   IID_IBackup, (LPVOID*)&pB );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create backup object.") );
			// stop progress / cleanup
			pMF->DisableProgress();
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}

		CString szItem;
		::GetCurrentUser( szItem );
		pB->put_UserID( szItem.AllocSysString() );
		COleDateTime d = COleDateTime::GetCurrentTime();
		szItem = d.Format( _T("%m-%d-%Y %H:%M:%S") );
		pB->put_BackupDate( szItem.AllocSysString() );
		::GetBackupPath( szItem );
		pB->put_Path( szItem.AllocSysString() );
		pB->put_BackupType( eB_Group );
		pB->put_Exp( szExp.AllocSysString() );
		pB->put_Notes( nd.m_szNotes.AllocSysString() );
		hr = pB->Add();
		if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to add backup to backup history table.") );
		pB->Release();
		CoUninitialize();
	}

	// stop progress / cleanup
	pMF->DisableProgress();
	CLeftView::SetActionState( ACTION_NONE );
	_Reprocessing = FALSE;

	BCGPMessageBox( _T("Experiment backup completed successfully.") );

	return 0;
}

//************************************
// Method:    BackupSystemThread
// FullName:  CLeftView::BackupSystemThread
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::BackupSystemThread()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	_Reprocessing = TRUE;
	_MF = pMF;
	_LV = this;
	_TerminateThread = FALSE;

	CWaitCursor crs;
	CWinThread* pThread = AfxBeginThread( DoBackupSystem, 0 );
}

//************************************
// Method:    DoBackupSystem
// FullName:  DoBackupSystem
// Access:    public 
// Returns:   UINT
// Qualifier:
// Parameter: LPVOID pParam
//************************************
UINT DoBackupSystem( LPVOID pParam )
{
	CMainFrame* pMF = _MF;

	if( !pMF )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	if( !NSMUsers::VerifyBackupPath() )
	{
		_Reprocessing = FALSE;
		return 0;
	}

	OutputAMessage( _T("Backup system."), TRUE );
	CLeftView::SetActionState( ACTION_BACKUP );

	HRESULT hRes = CoInitialize( NULL );
	_ASSERTE( SUCCEEDED( hRes ) );

	Experiments exp;
	if( !exp.IsValid() ) return 0;

	CString szIDs, szFile;
	BOOL fCopy = FALSE;
	int nCount = 0;
	UINT nRes = ACTION_BACKUP;

BeginHere:
	if( fCopy )
	{
		// Progress Meter steps
		pMF->EnableProgress( nCount );
		nCount = 0;
	}

	// Backup
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) && ( nRes == ACTION_BACKUP ) && !_TerminateThread )
	{
		exp.GetID( szIDs );

		nRes = BackupExperiment( szIDs, fCopy, nCount, TRUE );

		hr = exp.GetNext();
	}

	if( !fCopy )
	{
		fCopy = TRUE;
		goto BeginHere;
	}
	else
	{
		IBackup* pB = NULL;
		HRESULT hr = CoCreateInstance( CLSID_Backup, NULL, CLSCTX_ALL,
									   IID_IBackup, (LPVOID*)&pB );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create backup object.") );
			// stop progress / cleanup
			pMF->DisableProgress();
			CLeftView::SetActionState( ACTION_NONE );
			_Reprocessing = FALSE;
			return 0;
		}

		// add to backup history
		CString szItem;
		::GetCurrentUser( szItem );
		pB->put_UserID( szItem.AllocSysString() );
		COleDateTime d = COleDateTime::GetCurrentTime();
		szItem = d.Format( _T("%m-%d-%Y %H:%M:%S") );
		pB->put_BackupDate( szItem.AllocSysString() );
		::GetBackupPath( szItem );
		pB->put_Path( szItem.AllocSysString() );
		pB->put_BackupType( eB_Group );
		pB->put_Exp( szIDs.AllocSysString() );
		szFile = _T("System backup.");
		pB->put_Notes( szFile.AllocSysString() );
		hr = pB->Add();
		if( FAILED( hr ) ) BCGPMessageBox( _T("Unable to add backup to backup history table.") );
		pB->Release();

		// now backup the databases
		pMF->FBackupdb( FALSE );
	}

	CoUninitialize();

	if( !_TerminateThread )
		BCGPMessageBox( _T("System backup completed successfully.") );
	else
		BCGPMessageBox( _T("Backup terminated abnormally.") );

	// stop progress / cleanup
	pMF->DisableProgress();
	CLeftView::SetActionState( ACTION_NONE );
	_Reprocessing = FALSE;

	return 0;
}

//************************************
// Method:    OnEAAll
// FullName:  CLeftView::OnEAAll
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAAll() 
{
	ShowStatGraph( FALSE, MODE_ALL );
}

//************************************
// Method:    OnUpdateEAAll
// FullName:  CLeftView::OnUpdateEAAll
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAAll(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEAEw
// FullName:  CLeftView::OnEAEw
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAEw() 
{
	ShowStatGraph( FALSE, MODE_AVERAGE );
}

//************************************
// Method:    OnUpdateEAEw
// FullName:  CLeftView::OnUpdateEAEw
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAEw(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEAUew
// FullName:  CLeftView::OnEAUew
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEAUew() 
{
	ShowStatGraph( TRUE, MODE_AVERAGE );
}

//************************************
// Method:    OnUpdateEAUew
// FullName:  CLeftView::OnUpdateEAUew
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEAUew(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnEACombo
// FullName:  CLeftView::OnEACombo
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEACombo() 
{
	ShowStatGraph( FALSE, MODE_COMBO );
}

//************************************
// Method:    OnUpdateEACombo
// FullName:  CLeftView::OnUpdateEACombo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEACombo(CCmdUI* pCmdUI) 
{
	OnUpdateStatGraph( pCmdUI );
}

//************************************
// Method:    OnSetFocus
// FullName:  CLeftView::OnSetFocus
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pOldWnd
//************************************
void CLeftView::OnSetFocus(CWnd* pOldWnd) 
{
	CTreeView::OnSetFocus(pOldWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Leftview Window: Right-click on an item to see that item's menu.");
	pMF->ShowStatusText( szMsg );
	
}

//************************************
// Method:    OnKillFocus
// FullName:  CLeftView::OnKillFocus
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CWnd * pNewWnd
//************************************
void CLeftView::OnKillFocus(CWnd* pNewWnd) 
{
	CTreeView::OnKillFocus(pNewWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Ready.");
	pMF->ShowStatusText( szMsg );
	
}

#define	INS_KEY		45
#define	DEL_KEY		46
#define	ESC_KEY		27
#define	ENTER_KEY	13

//************************************
// Method:    OnKeydown
// FullName:  CLeftView::OnKeydown
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: NMHDR * pNMHDR
// Parameter: LRESULT * pResult
//************************************
void CLeftView::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_KEYDOWN* pTVKeyDown = (TV_KEYDOWN*)pNMHDR;
	*pResult = 0;

	char c = (char)pTVKeyDown->wVKey;
	CString szKey = c;
	szKey.MakeUpper();

	if( ( pTVKeyDown->wVKey != INS_KEY ) &&
		( pTVKeyDown->wVKey != DEL_KEY ) &&
		( pTVKeyDown->wVKey != ESC_KEY ) &&
		( pTVKeyDown->wVKey != ENTER_KEY ) )
		return;

	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return;
	DWORD dwType = tree.GetItemData( hItem );
	BOOL fRoot = TRUE;

	HTREEITEM hTmp = tree.GetParentItem( hItem );
	if( hTmp )
	{
		hTmp = tree.GetParentItem( hTmp );
		if( hTmp ) fRoot = FALSE;
	}

	if( pTVKeyDown->wVKey == ESC_KEY )
	{
		if( ( CLeftView::GetActionState() == ACTION_STIMULUS ) ||
			( CLeftView::GetActionState() == ACTION_TEST ) )
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			pMF->ObjStop();
		}
	}
	else if( pTVKeyDown->wVKey == INS_KEY )
	{
		if( dwType == TI_GROUP_FOLDER )
		{
			if( fRoot ) OnGfAdd();
			else OnGfAddgroups();
		}
		else if( dwType == TI_SUBJECT_FOLDER )
		{
			if( fRoot ) OnSfAdd();
			else OnSfAddsubjects();
		}
		else if( dwType == TI_CONDITION_FOLDER )
		{
			if( fRoot ) OnCfAdd();
			else OnCfAddconditions();
		}
		else if( dwType == TI_EXPERIMENT_FOLDER )
		{
			OnEfAdd();
		}
		else if( dwType == TI_STIMULUS_FOLDER )
		{
			OnStfAdd();
		}
		else if( dwType == TI_ELEMENT_FOLDER )
		{
			if( fRoot ) OnElfAdd();
			else OnStAddelements();
		}
		else if( dwType == TI_CATEGORY_FOLDER )
		{
			OnCatfAdd();
		}
		else if( dwType == TI_FEEDBACK_FOLDER )
		{
			OnFbfAdd();
		}
	}
	else if( pTVKeyDown->wVKey == DEL_KEY )
	{
		if( dwType == TI_SUBJECT )
		{
			if( fRoot ) OnSDelete();
			else OnSRemove();
		}
		else if( dwType == TI_GROUP )
		{
			if( fRoot ) OnGDelete();
			else OnGRemove();
		}
		else if( dwType == TI_CONDITION )
		{
			if( fRoot ) OnCDelete();
			else OnCRemove();
		}
		else if( dwType == TI_EXPERIMENT )
		{
			OnEDelete();
		}
		else if( dwType == TI_STIMULUS )
		{
			OnStDelete();
		}
		else if( dwType == TI_ELEMENT )
		{
			if( fRoot ) OnElDelete();
			else
			{
				hTmp = tree.GetParentItem( hItem );
				DWORD dwType2 = hTmp ? tree.GetItemData( hTmp ) : TI_UNKNOWN;
				if( dwType2 == TI_CATEGORY_HOLDER ) OnElDelete();
				else OnElRemove();
			}
		}
		else if( dwType == TI_CATEGORY )
		{
			OnCatDelete();
		}
		else if( dwType == TI_FEEDBACK )
		{
			OnFbDelete();
		}
	}
	else if( pTVKeyDown->wVKey == ENTER_KEY )
	{
		if( CLeftView::GetActionState() == ACTION_NONE )
		{
			LRESULT lres = 0 ;
			OnDblclk( NULL, &lres );
		}
		else
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			CGraphView* pGV = pMF->GetRecordingPane();
			// End trial (user hits key to stop viewing completed trial)
			if( pGV ) pGV->OnKeyDown( c );
		}
	}
}

//************************************
// Method:    OnUpdateEExport
// FullName:  CLeftView::OnUpdateEExport
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEExport(CCmdUI* pCmdUI)
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSExport
// FullName:  CLeftView::OnUpdateSExport
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSExport(CCmdUI* pCmdUI)
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEExport
// FullName:  CLeftView::OnEExport
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEExport()
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment ID
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	CString szExp = szItem.Mid( nPos + 2 );

	// export
	CString szMsg;
	Export( szMsg, szExp );
}

//************************************
// Method:    OnSExport
// FullName:  CLeftView::OnSExport
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSExport()
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );

	// Which experiment/group
	HTREEITEM hExp = tree.GetParentItem( hItem ); // Subject folder
	hExp = tree.GetParentItem( hExp ); // Group
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrpID = szItem.Mid( nPos + 2 );
	hExp = tree.GetParentItem( hExp ); // Group folder
	hExp = tree.GetParentItem( hExp ); // Experiment
	szItem = tree.GetItemText( hExp ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	// export
	CString szMsg;
	Export( szMsg, szExpID, szSubjID, szGrpID );
}

//************************************
// Method:    Export
// FullName:  CLeftView::Export
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString & szWMsg
// Parameter: CString szExp
// Parameter: CString szSubjOnly
// Parameter: CString szSubjGroup
// Parameter: BOOL fWizard
// Parameter: BOOL fWFiles
// Parameter: BOOL fWTrials
// Parameter: BOOL fWMembers
// Parameter: BOOL fWShow
// Parameter: CString szWPath
//************************************
BOOL CLeftView::Export( CString& szWMsg, CString szExp, CString szSubjOnly, CString szSubjGroup,
						BOOL fWizard, BOOL fWFiles, BOOL fWTrials, BOOL fWMembers,
						BOOL fWShow, CString szWPath )
{
	// default export location
	CString szUser, szVal, szRB, szPath;
	::GetCurrentUser( szUser );
	szVal.Format( _T("Settings\\%s"), szUser );
	szRB = theApp.GetRegistryBase();
	theApp.SetRegistryBase( szVal );
	::GetAllUserPath( szPath );
	szPath += _T("Export\\");
	szVal = theApp.GetString( _T("ExportLocation"), szPath );
	// export
	BOOL fRes = Experiments::Export( szWMsg, szExp, szVal, szSubjOnly, szSubjGroup, fWizard,
									 fWFiles, fWTrials, fWMembers, fWShow, szWPath );
	// write export location
	if( fRes ) theApp.WriteString( _T("ExportLocation"), szVal );
	theApp.SetRegistryBase( szRB );

	return fRes;
}

//************************************
// Method:    OnFileImport
// FullName:  CLeftView::OnFileImport
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFileImport()
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		pMF->OnHelpActivate();
		return;
	}

	CString szExp;
	if( OnEImport( szExp ) )
	{
		Find( m_hExp, szExp );
		if( !((CMainFrame*)AfxGetMainWnd())->IsMinimized() )::ShowWindow( m_hWnd, SW_RESTORE );
	}
}

//************************************
// Method:    OnEImport
// FullName:  CLeftView::OnEImport
// Access:    public 
// Returns:   BOOL
// Qualifier:
// Parameter: CString & szExpID
//************************************
BOOL CLeftView::OnEImport( CString& szExpID )
{
	BOOL fTrials = FALSE;
	CStringList lstImp;

	// do the import
	if( Experiments::Import( szExpID, fTrials, &lstImp, TRUE ) )
	{
		// were there any subjects imported?
		if( lstImp.GetCount() > 0 )
		{
			int nPos = 0;
			short nExpType = EXP_TYPE_HANDWRITING;
			{
				Experiments exp;
				if( SUCCEEDED( exp.Find( szExpID ) ) ) exp.GetType( nExpType );
			}
			CString szItem, szExpID, szGrpID, szSubjID, szMsg;
			CStringList lstExp;
			CString szDelim = ::GetDelimiter();
			szDelim.Trim();

			// loop through the list of subjects, construct message
			POSITION pos = lstImp.GetHeadPosition();
			while( pos )
			{
				// each item is EXP::GRP::SUBJ
				// get next item
				szItem = lstImp.GetNext( pos );
				// extract exp ID
				nPos = szItem.Find( szDelim );
				if( nPos != -1 )
				{
					szExpID = szItem.Left( nPos );
					szItem = szItem.Mid( nPos + 1 );
				}
				else break;
				// extract grp ID
				nPos = szItem.Find( szDelim );
				if( nPos != -1 )
				{
					szGrpID = szItem.Left( nPos );
					szItem = szItem.Mid( nPos + 1 );
					szItem.Trim();
				}
				else break;
				// extract subj ID
				if( szItem != _T("") )
				{
					szSubjID = szItem;
				}
				else break;

				// add to lists
				if( !lstExp.Find( szExpID ) ) lstExp.AddTail( szExpID );
			}

			Refresh();

			// inform & ask
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			CListView* pMsgWnd = pMF ? (CListView*)pMF->GetMsgPane() : NULL;
			szMsg.Format( _T("There were %d subjects imported.\n\nYou can now process the new trials of the imported experiment.\nThis can be a lengthy process, but you can stop it by clicking the stop button in the toolbar.\nIf you stop the process, it will end after the current subject being processed.\n\nDo you want to process new trials?"), lstImp.GetCount() );
			if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
			{
				// get experiment and process
				POSITION posExp = lstExp.GetHeadPosition();
				if( posExp )
				{
					// get exp, grp & subj IDs
					szExpID = lstExp.GetNext( posExp );
					// locate tree item
					HTREEITEM hItem = Find( m_hExp, szExpID );
					// verify that item was found
					if( !hItem )
					{
						BCGPMessageBox( _T("Unable to locate experiment in tree."), MB_OK | MB_ICONERROR );
						return FALSE;
					}
					CTreeCtrl& tree = GetTreeCtrl();
					tree.SelectItem( hItem );

					_Reprocessing = TRUE;
					_ReprocAll = FALSE;
					_MF = pMF;
					_LV = this;
					_MsgWnd = pMsgWnd;
					_TerminateThread = FALSE;

					HWND hWndStatus = pMF->m_wndStatusBar.m_hWnd;
					CWinThread* pThread = AfxBeginThread( ReprocessExperiment, hWndStatus );
				}
			}
		}
		else Refresh();

		return TRUE;
	}

	return FALSE;
}

//************************************
// Method:    OnTDelete
// FullName:  CLeftView::OnTDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTDelete() 
{
	CTreeCtrl& tree = GetTreeCtrl();

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	// ask
	CString szMsg = _T("Are you sure you want to delete this trial? (It can be restored from the Recycle Bin)");
	if( fQuick ) szMsg = _T("Are you sure you want to remove this trial from the Quick Links?");
	int nRes = BCGPMessageBox( szMsg, MB_YESNO );
	if( nRes == IDNO ) return;

	if( !fQuick )
	{
		// format to .* to include tf, seg, etc
		szTemp = szTemp.Left( szTemp.GetLength() - 3 );
		szTemp += _T("*");
		szFile.Format( _T("%s\\%s"), szPath, szTemp );

		// inform user
		szTemp.Format( _T("Trial %s sent to the Recycle Bin."), szFile );
		OutputAMessage( szTemp, TRUE );

		::SendToRecycleBin( szFile );
	}
	else
	{
		POSITION pos = m_lstQuick.Find( szTemp );
		if( pos ) m_lstQuick.RemoveAt( pos );
	}

	// update tree
	HTREEITEM hSel = tree.GetSelectedItem();
	tree.DeleteItem( hSel );
}

//************************************
// Method:    OnUpdateTDelete
// FullName:  CLeftView::OnUpdateTDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCDup
// FullName:  CLeftView::OnCDup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCDup() 
{
	NSConditions cond;
	if( !cond.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which Condition
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	// Which experiment (if applicable)
	HTREEITEM hCondFolder = tree.GetParentItem( tree.GetSelectedItem() );
	HTREEITEM hExp = tree.GetParentItem( hCondFolder );
	CString szExp;
	if( hExp )
	{
		szItem = tree.GetItemText( hExp );
		nPos = szItem.Find( szDelim );
		if( nPos != -1 ) szExp = szItem.Mid( nPos + 2 );
	}

	CString szData = _T("Duplicating condition.");
	OutputAMessage( szData );

	BCGPMessageBox( _T("Condition instructions and extended notes will NOT be duplicated.") );
	if( cond.DoEditDup( szCond, szExp, this ) )
	{
		::DataHasChanged();

		CString szItem;
		cond.GetDescriptionWithID( szItem );

		// insert in master list
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, m_hCond );
		tree.SetItemData( hItem, TI_CONDITION );

		// insert in experiment (if applicable)
		if( hExp )
		{
			hItem = tree.InsertItem( szItem, IMG_CONDITION, IMG_CONDITION, hCondFolder );
			tree.SetItemData( hItem, TI_CONDITION );
		}

		tree.EnsureVisible( hItem );

		szData.Format( IDS_AL_CONDADDED, szItem );
		OutputAMessage( szData, TRUE );
	}
}

//************************************
// Method:    OnUpdateCDup
// FullName:  CLeftView::OnUpdateCDup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCDup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSChartall
// FullName:  CLeftView::OnSChartall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSChartall() 
{
// 	CString szMsg = _T("This provides a \"rough\" visualization of all of the trials for the subject.\nDepending upon the # of trials and the speed of your computer, this could take longer than expected.\n\nContinue?");
// 	if( BCGPMessageBox( szMsg, MB_YESNO ) == IDNO ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject?
	HTREEITEM hItem = tree.GetSelectedItem();	// Trial
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which experiment/group
	hItem = tree.GetParentItem( hItem );		// Subject folder
	hItem = tree.GetParentItem( hItem );		// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );		// Group folder
	hItem = tree.GetParentItem( hItem );		// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	ExcludeDlg dlgEx( szExp, this, FALSE, FALSE, TRUE, szGrp, szSubj );
	if( dlgEx.DoModal() == IDCANCEL ) return;

	// Get sampling rate from ProcSettings object
	IProcSetting* pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
								   IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	hr = pPS->Find( szExp.AllocSysString() );
	double dRate = 200.0;
	short nPrs = 1, nVal = 0;
	if( SUCCEEDED( hr ) )
	{
		pPS->get_SamplingRate( &nVal );
		dRate = nVal;
		pPS->get_MinPenPressure( &nPrs );
	}
	pPS->Release();
	hr = m_pEML->Find( szExp.AllocSysString(), szGrp.AllocSysString(), szSubj.AllocSysString() );
	if( SUCCEEDED( hr ) )
	{
		m_pEML->get_SamplingRate( &nVal );
		if( nVal != 0 ) dRate = nVal;
	}

	CWaitCursor crs;
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->ChartHwrMultiple( szExp, szGrp, szSubj, dRate, nPrs, &(dlgEx.m_lstExclude) );
}

//************************************
// Method:    OnUpdateSChartall
// FullName:  CLeftView::OnUpdateSChartall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSChartall(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElDelete
// FullName:  CLeftView::OnElDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElDelete() 
{
	Elements elem;
	if( !elem.IsValid() ) return;

	CString szData;
	szData = _T("SRC: Element - Delete.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szElem = szItem.Mid( nPos + 2 );

	szData.Format( _T("Are you sure you want to permanently DELETE element %s?"), szElem );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	if( SUCCEEDED( elem.Find( szElem ) ) )
	{
		// Delete element
		HRESULT hr = elem.Remove();

		::DataHasChanged();

		szData.Format( _T("Deleted element %s."), szElem );
		OutputAMessage( szData );

		// Now find in a stimulus if can & remove
		HTREEITEM hStim = tree.GetNextItem( m_hStim, TVGN_CHILD );		// 1st stimulus
		while( hStim )
		{
			CString szStim = tree.GetItemText( hStim );
			nPos = szStim.Find( szDelim );
			szStim = szStim.Mid( nPos + 2 );

			HTREEITEM hElem = tree.GetNextItem( hStim, TVGN_CHILD );	// Elements folder
			hElem = tree.GetNextItem( hElem, TVGN_CHILD );				// 1st element
			while( hElem )
			{
				CString szElem2 = tree.GetItemText( hElem );
				nPos = szElem2.Find( szDelim );
				szElem2 = szElem2.Mid( nPos + 2 );

				if( szElem == szElem2 )
				{
					// Elements
					IStimulusElement* pSE = NULL;
					hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
											 IID_IStimulusElement, (LPVOID*)&pSE );
					if( SUCCEEDED( hr ) )
					{
						hr = pSE->Find( szStim.AllocSysString(), szElem.AllocSysString() );
						if( SUCCEEDED( hr ) ) hr = pSE->Remove();
						if( FAILED( hr ) )
							BCGPMessageBox( _T("Unable to remove element from link table.") );
						else
							tree.DeleteItem( hElem );

						pSE->Release();
					}
					else BCGPMessageBox( _T("Unable to create StimulusElement object.") );
				}

				hElem = tree.GetNextItem( hElem, TVGN_NEXT );			// next element
			}

			// remove as target too (if exists)
			IStimulusTarget* pST = NULL;
			hr = CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
								IID_IStimulusTarget, (LPVOID*)&pST );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
				return;
			}

			BSTR bstrItem;
			hr = pST->FindForStimulus( szStim.AllocSysString() );
			while( SUCCEEDED( hr ) )
			{
				pST->get_StimulusID( &bstrItem );
				szItem = bstrItem;
				if( szItem == szStim )
				{
					pST->get_ElementID( &bstrItem );
					szItem = bstrItem;
					if( szItem == szElem ) hr = pST->Remove();
				}

				hr = pST->GetNext();
			}
			pST->Release();

			hStim = tree.GetNextItem( hStim, TVGN_NEXT );				// Next stimulus
		}

		tree.DeleteItem( hItem );
	}
}

//************************************
// Method:    OnUpdateElDelete
// FullName:  CLeftView::OnUpdateElDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	if( hParent )
	{
		DWORD dwType2 = tree.GetItemData( hParent );
		hParent = tree.GetParentItem( hParent );
		if( dwType2 == TI_CATEGORY_HOLDER ) fEnable &= TRUE;
		else fEnable &= ( hParent == NULL );
		IS_EXP_RUNNING();
		fEnable &= fRunExp;
	}
	else fEnable = FALSE;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElEdit
// FullName:  CLeftView::OnElEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElEdit() 
{
	CString szData;
	szData = _T("SRC: Element - Properties.");
	OutputAMessage( szData );

	Elements elem;
	if( !elem.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szElemID = szItem.Mid( nPos + 2 );
	CString szElem = szItem.Left( nPos - 1 );

	szData.Format( _T("View element %s properties."), szElem );
	OutputAMessage( szData );

	// Original text
	CString szOld = tree.GetItemText( tree.GetSelectedItem() );

	if( elem.DoEdit( szElemID, this ) )
	{
		::DataHasChanged();

		elem.GetDescriptionWithID( szItem );

		ChangeAllInstanceNames( TI_ELEMENT, szOld, szItem );

		szData.Format( _T("Modified element %s."), szElem );
		OutputAMessage( szData );
	}
}

//************************************
// Method:    OnUpdateElEdit
// FullName:  CLeftView::OnUpdateElEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElEdit(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElfAdd
// FullName:  CLeftView::OnElfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElfAdd() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	Elements elem;
	if( !elem.IsValid() ) return;

	CString szData;
	szData = _T("SRC: Element Folder - New Element.");
	OutputAMessage( szData );

	if( elem.DoAdd( this ) )
	{
		::DataHasChanged();

		CString szItem;
		elem.GetDescriptionWithID( szItem );

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_ELEMENT, IMG_ELEMENT, m_hElmt );
		tree.SetItemData( hItem, TI_ELEMENT );

		tree.EnsureVisible( hItem );
		tree.SelectItem( hItem );

		elem.GetID( szItem );
		szData.Format( _T("Added new element %s."), szItem );
		OutputAMessage( szData, TRUE );
	}
}

//************************************
// Method:    OnUpdateElfAdd
// FullName:  CLeftView::OnUpdateElfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElfAdd(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElfFind
// FullName:  CLeftView::OnElfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElfFind() 
{
	CString szData = _T("SRC: Element Folder - Find.");
	OutputAMessage( szData );

	FindDlg d( _T("Find Element"), this );
	if( d.DoModal() == IDOK ) Find( d.m_szID );
}

//************************************
// Method:    OnUpdateElfFind
// FullName:  CLeftView::OnUpdateElfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElfFind(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStDelete
// FullName:  CLeftView::OnStDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStDelete() 
{
	CString szData = _T("SRC: Stimulus - Delete.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	// If any elements, tell them to remove first
	HTREEITEM hItem = tree.GetSelectedItem();
	if( tree.ItemHasChildren( hItem ) )
	{
		BOOL fNo = FALSE;
		HTREEITEM hTemp = tree.GetChildItem( hItem );
		if( tree.ItemHasChildren( hTemp ) ) fNo = TRUE;

		if( fNo )
		{
			BCGPMessageBox( _T("You must remove all elements first.") );
			return;
		}
	}

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szStim = szItem.Mid( nPos + 2 );

	szData.Format( _T("Are you sure you want to permanently DELETE stimulus %s?"), szStim );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	Stimuluss stim;
	if( stim.IsValid() )
	{
		HRESULT hr = stim.Find( szStim );
		if( SUCCEEDED( hr ) )
		{
			hr = stim.Remove();

			CString szData;
			szData.Format( _T("Deleted stimulus %s."), szStim );
			OutputAMessage( szData );

			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Unable to delete stimulus.") );
				return;
			}
			::DataHasChanged();

			tree.DeleteItem( hItem );
		}
	}
}

//************************************
// Method:    OnUpdateStDelete
// FullName:  CLeftView::OnUpdateStDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStDelete(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStEdit
// FullName:  CLeftView::OnStEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStEdit() 
{
	CString szData;
	szData = _T("SRC: Stimulus - Properties.");
	OutputAMessage( szData );

	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szStim = szItem.Mid( nPos + 2 );

	szData.Format( _T("View stimulus %s properties."), szStim );
	OutputAMessage( szData );

	if( stim.DoEdit( szStim, this ) )
	{
		::DataHasChanged();

		RefreshStimuli();
		RefreshElements();

		Find( szStim );

		CString szItem;
		stim.GetDescriptionWithID( szItem );

		szData.Format( _T("Modified stimulus %s."), szStim );
		OutputAMessage( szData );
	}
}

//************************************
// Method:    OnUpdateStEdit
// FullName:  CLeftView::OnUpdateStEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStEdit(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStfAdd
// FullName:  CLeftView::OnStfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStfAdd() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	Stimuluss stim;
	if( !stim.IsValid() ) return;

	CString szData;
	szData = _T("SRC: Stimulus Folder - New Stimulus.");
	OutputAMessage( szData );

	if( stim.DoAdd( this ) )
	{
		::DataHasChanged();

		CString szItem;
		stim.GetDescriptionWithID( szItem );

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_STIMULUS, IMG_STIMULUS, m_hStim );
		tree.SetItemData( hItem, TI_STIMULUS );

		RefreshStimuli();

		stim.GetID( szItem );
		Find( szItem );

		szData.Format( _T("Added new stimulus %s."), szItem );
		OutputAMessage( szData, TRUE );
	}
	RefreshElements();
}

//************************************
// Method:    OnUpdateStfAdd
// FullName:  CLeftView::OnUpdateStfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStfAdd(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStfFind
// FullName:  CLeftView::OnStfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStfFind() 
{
	CString szData = _T("SRC: Stimulus Folder - Find.");
	OutputAMessage( szData );

	FindDlg d( _T("Find Stimulus"), this );
	if( d.DoModal() == IDOK ) Find( d.m_szID );
}

//************************************
// Method:    OnUpdateStfFind
// FullName:  CLeftView::OnUpdateStfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStfFind(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElfAddelements
// FullName:  CLeftView::OnElfAddelements
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElfAddelements() 
{
	CString szData = _T("SRC: Element Folder - Add Element");
	OutputAMessage( szData );

	OnStAddelements();
}

//************************************
// Method:    OnUpdateElfAddelements
// FullName:  CLeftView::OnUpdateElfAddelements
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElfAddelements(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_ELEMENT_FOLDER );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	if( hParent )
	{
		fEnable &= ( hParent != NULL );
		IS_EXP_RUNNING();
		fEnable &= fRunExp;
	}
	else fEnable = FALSE;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStAddelements
// FullName:  CLeftView::OnStAddelements
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStAddelements() 
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem, hItemElems, hElem;
	DWORD dwType;
	CString szStim, szElem = HOLDER;
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	if( !m_fDragging )
	{
		hItem = tree.GetSelectedItem();
		if( tree.GetItemData( hItem ) == TI_ELEMENT_FOLDER )
			szStim = tree.GetItemText( tree.GetParentItem( hItem ) );
		else
			szStim = tree.GetItemText( hItem );
		int nPos = szStim.Find( szDelim );
		szStim = szStim.Mid( nPos + 2 );
	}
	else
	{
		hItem = m_hitemDrop;
		szStim = _DropID;
	}
	hItemElems = hItem;
	dwType = tree.GetItemData( hItem );
	if( ( dwType != TI_ELEMENT_FOLDER ) && !m_fDragging )
	{
		CString szData = _T("SRC: Experiment - Add Group(s)");
		OutputAMessage( szData );
	}
	if( dwType == TI_ELEMENT_FOLDER )
		hItem = tree.GetParentItem( hItem );

	// Existing elements
	CStringList exist;

	if( dwType == TI_STIMULUS )
	{
		hItemElems = tree.GetNextItem( hItemElems, TVGN_CHILD );	// Element folder
		if( hItemElems ) dwType = tree.GetItemData( hItemElems );
		else dwType = TI_UNKNOWN;

		if( dwType == TI_UNKNOWN ) hItemElems = NULL;
	}
	if( dwType == TI_ELEMENT_FOLDER )
	{
		hElem = tree.GetNextItem( hItemElems, TVGN_CHILD );
		while( hElem )
		{
			szElem = tree.GetItemText( hElem );
			int nPos = szElem.Find( szDelim );
			if( nPos != -1 )
			{
				szElem = szElem.Mid( nPos + 2 );
				exist.AddTail( szElem );
			}
			hElem = tree.GetNextItem( hElem, TVGN_NEXT );
		}
	}

	HRESULT hr;
	IStimulusElement* pSE = NULL;
	hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
							 IID_IStimulusElement, (LPVOID*)&pSE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusElement object.") );
		return;
	}

	AddElementDlg d( &exist, this );
	int nRet = m_fDragging ? IDOK : d.DoModal();
	if( d.m_fChanged ) RefreshElements();
	if( nRet == IDOK )
	{
		CString szItem, szTmp;
		int nPos;

		POSITION pos = d.m_lstNew.GetHeadPosition();
		if( !m_fDragging && !pos ) return;

		if( !m_fDragging )
		{
			while( pos )
			{
				szItem = d.m_lstNew.GetNext( pos );
				szElem = szItem.Left( szItem.Find( szDelim ) - 1 );
				szTmp.Format( _T("%s %s %s"), szItem.Mid( szItem.Find( szDelim ) + 2 ), szDelim, szElem );
				hElem = tree.InsertItem( szTmp, IMG_ELEMENT, IMG_ELEMENT, hItemElems );
				tree.SetItemData( hElem, TI_ELEMENT );
				::DataHasChanged();

				pSE->put_StimulusID( szStim.AllocSysString() );
				pSE->put_ElementID( szElem.AllocSysString() );
				pSE->Add();

				CString szData;
				szData.Format( _T("Adding element %s to stimulus %s."), szItem, tree.GetItemText( hItem ) );
				OutputAMessage( szData, TRUE );

				tree.EnsureVisible( hItemElems );
			}
		}
		else
		{
			szItem = _DragID;
			szElem = szItem.Mid( szItem.Find( szDelim ) + 2 );

			// Check if in list
			HTREEITEM hItemTemp = tree.GetChildItem( hItemElems );
			CString szTmp;
			while( hItemTemp )
			{
				szTmp = tree.GetItemText( hItemTemp );
				nPos = szTmp.Find( szDelim );
				if( nPos == -1 ) return;
				szTmp = szTmp.Mid( nPos + 2 );

				if( szTmp == szElem )
				{
					BCGPMessageBox( _T("This element already exists in the stimulus.") );
					return;
				}

				hItemTemp = tree.GetNextSiblingItem( hItemTemp );
			}

			pSE->put_StimulusID( szStim.AllocSysString() );
			pSE->put_ElementID( szElem.AllocSysString() );
			hr = pSE->Add();

			if( SUCCEEDED( hr ) )
			{
				::DataHasChanged();

				CString szData;
				szData.Format( _T("Adding element %s to stimulus %s."), _DragID, tree.GetItemText( hItem ) );
				OutputAMessage( szData, TRUE );

				hElem = tree.InsertItem( _DragID, IMG_ELEMENT, IMG_ELEMENT, hItemElems );
				tree.SetItemData( hElem, TI_ELEMENT );

				tree.EnsureVisible( hItemElems );
			}
		}
	}
	pSE->Release();
}

//************************************
// Method:    OnUpdateStAddelements
// FullName:  CLeftView::OnUpdateStAddelements
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStAddelements(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElRemove
// FullName:  CLeftView::OnElRemove
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElRemove() 
{
	CString szData = _T("SRC: Element - Remove.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szElem = szItem.Mid( nPos + 2 );

	HTREEITEM hStim = tree.GetParentItem( hItem );
	hStim = tree.GetParentItem( hStim );
	szItem = tree.GetItemText( hStim );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szStim = szItem.Mid( nPos + 2 );

	szData.Format( _T("Are you sure you want to remove element %s from stimulus %s?"), szElem, szStim );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	// remove from stim/element table
	IStimulusElement* pSE = NULL;
	HRESULT hr = CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
								   IID_IStimulusElement, (LPVOID*)&pSE );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusElement object.") );
		return;
	}

	hr = pSE->Find( szStim.AllocSysString(), szElem.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_FIND_ERR );
		pSE->Release();
		return;
	}

	hr = pSE->Remove();
	pSE->Release();

	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_REM_ERR );
		return;
	}

	// remove as target too (if exists)
	IStimulusTarget* pST = NULL;
	hr = CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
						   IID_IStimulusTarget, (LPVOID*)&pST );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
		return;
	}

	BSTR bstrItem;
	hr = pST->FindForStimulus( szStim.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pST->get_StimulusID( &bstrItem );
		szItem = bstrItem;
		if( szItem == szStim )
		{
			pST->get_ElementID( &bstrItem );
			szItem = bstrItem;
			if( szItem == szElem ) hr = pST->Remove();
		}

		hr = pST->GetNext();
	}
	pST->Release();

	szData.Format( _T("Removed element %s from stimulus %s."), szElem, szStim );
	OutputAMessage( szData, TRUE );

	::DataHasChanged();

	tree.DeleteItem( hItem );
}

//************************************
// Method:    OnUpdateElRemove
// FullName:  CLeftView::OnUpdateElRemove
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElRemove(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_ELEMENT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	if( hParent )
	{
		DWORD dwType2 = tree.GetItemData( hParent );
		hParent = tree.GetParentItem( hParent );
		if( dwType2 == TI_CATEGORY_HOLDER ) fEnable &= FALSE;
		else fEnable &= ( hParent != NULL );
		IS_EXP_RUNNING();
		fEnable &= fRunExp;
	}
	else fEnable = FALSE;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElView
// FullName:  CLeftView::OnElView
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElView() 
{
	CString szData = _T("SRC: Element - View Data.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szElem = szItem.Mid( nPos + 2 );

	Elements elem;
	if( SUCCEEDED( elem.Find( szElem.AllocSysString() ) ) )
	{
		elem.ViewData();
	}
}

//************************************
// Method:    OnUpdateElView
// FullName:  CLeftView::OnUpdateElView
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElView(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElChart
// FullName:  CLeftView::OnElChart
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElChart() 
{
	CString szData = _T("SRC: Element - Chart Data.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szElem = szItem.Mid( nPos + 2 );

	Elements elem;
	if( SUCCEEDED( elem.Find( szElem.AllocSysString() ) ) )
	{
		elem.ChartData();
	}
}

//************************************
// Method:    OnUpdateElChart
// FullName:  CLeftView::OnUpdateElChart
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElChart(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStView
// FullName:  CLeftView::OnStView
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStView() 
{
	CString szData = _T("SRC: Stimulus - View Data.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	Stimuluss stim;
	if( SUCCEEDED( stim.Find( szIDs.AllocSysString() ) ) )
	{
		stim.ViewData( FALSE );
	}
}

//************************************
// Method:    OnUpdateStView
// FullName:  CLeftView::OnUpdateStView
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStView(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStChart
// FullName:  CLeftView::OnStChart
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStChart() 
{
	CString szData = _T("SRC: Stimulus - Chart Data.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	Stimuluss stim;
	if( SUCCEEDED( stim.Find( szIDs.AllocSysString() ) ) )
	{
		stim.ChartData( FALSE );
	}
}

//************************************
// Method:    OnUpdateStChart
// FullName:  CLeftView::OnUpdateStChart
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStChart(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElRelationships
// FullName:  CLeftView::OnElRelationships
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElRelationships() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );

	RelationshipDlg d( this, REL_ELEMENT, szItem );
	d.DoModal();
}

//************************************
// Method:    OnUpdateElRelationships
// FullName:  CLeftView::OnUpdateElRelationships
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElRelationships(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStRelationships
// FullName:  CLeftView::OnStRelationships
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStRelationships() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );

	RelationshipDlg d( this, REL_STIMULUS, szItem );
	d.DoModal();
}

//************************************
// Method:    OnUpdateStRelationships
// FullName:  CLeftView::OnUpdateStRelationships
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStRelationships(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElDisplay
// FullName:  CLeftView::OnElDisplay
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElDisplay() 
{
	CString szData = _T("SRC: Element - Display.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	pMF->RunExperiment( NULL, szIDs, _T(""), _T(""), _T(""), _T(""), NULL, FALSE, hItem, TRUE, TRUE );
}

//************************************
// Method:    OnUpdateElDisplay
// FullName:  CLeftView::OnUpdateElDisplay
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElDisplay(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
//	fEnable &= IsTabletModeOn();
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStDisplay
// FullName:  CLeftView::OnStDisplay
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStDisplay() 
{
	CString szData = _T("SRC: Stimulus - Display.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	pMF->RunExperiment( NULL, szIDs, _T(""), _T(""), _T(""), _T(""), NULL, FALSE, hItem, TRUE );
}

//************************************
// Method:    OnUpdateStDisplay
// FullName:  CLeftView::OnUpdateStDisplay
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStDisplay(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
//	fEnable &= IsTabletModeOn();
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnElDup
// FullName:  CLeftView::OnElDup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElDup() 
{
	CString szData = _T("SRC: Element - Duplicate.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	Elements elem;
	if( SUCCEEDED( elem.Find( szIDs ) ) )
	{
		if( elem.DoAdd() )
		{
			::DataHasChanged();

			CString szItem;
			elem.GetDescriptionWithID( szItem );

			CTreeCtrl& tree = GetTreeCtrl();
			HTREEITEM hItem = tree.InsertItem( szItem, IMG_ELEMENT, IMG_ELEMENT, m_hElmt );
			tree.SetItemData( hItem, TI_ELEMENT );

			tree.EnsureVisible( hItem );

			elem.GetID( szItem );
			szData.Format( _T("Added new element %s."), szItem );
			OutputAMessage( szData, TRUE );

			tree.SelectItem( hItem );
		}
	}
}

//************************************
// Method:    OnUpdateElDup
// FullName:  CLeftView::OnUpdateElDup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElDup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    GetSubjectTrialInfo
// FullName:  CLeftView::GetSubjectTrialInfo
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExpID
// Parameter: CString szGrpID
// Parameter: CString szSubjID
// Parameter: CStringList * pLstCond
// Parameter: short & nTrials
// Parameter: short & nGoodTrials
// Parameter: short & nBadTrials
// Parameter: COleDateTime & dtExpStart
// Parameter: COleDateTime & dtExpEnd
// Parameter: COleDateTime & dtProcessed
// Parameter: BOOL & fInSummarize
//************************************
void CLeftView::GetSubjectTrialInfo( CString szExpID, CString szGrpID, CString szSubjID,
									 CStringList* pLstCond, short& nTrials, short& nGoodTrials,
									 short& nBadTrials, COleDateTime& dtExpStart, COleDateTime& dtExpEnd,
									 COleDateTime& dtProcessed, BOOL& fInSummarize )
{
	// TODO - remove this from leftview to experiment object w/o dependency on view

	CString szTrial, szPath, szCond, szTemp, szTF;
	CStringList lstConsConds, lstAllConds;
	BSTR bstr = NULL;
	COleDateTime dtTemp;
	CTime ctTime;
	CFileFind ff;
	int nImage = 0, nSelImage = 0;

	if( !pLstCond ) return;

	// initialize
	pLstCond->RemoveAll();
	nTrials = nGoodTrials = nBadTrials = 0;
	dtExpStart = dtExpEnd = dtProcessed = DATE( 0 );
	fInSummarize = TRUE;

	// tree ctrl
	CTreeCtrl& tree = GetTreeCtrl();

	// find subject in experiment
	HTREEITEM hSubj = Find( szExpID, szGrpID, szSubjID );
	if( !hSubj ) return;

	// fill 'all conditions' list
	HRESULT hr = m_pECL->FindForExperiment( szExpID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		m_pECL->get_ConditionID( &bstr );
		szCond = bstr;
		if( !lstAllConds.Find( szCond ) ) lstAllConds.AddTail( szCond );

		hr = m_pECL->GetNext();
	}

	// expand folder for trials
//	if( !tree.ItemHasChildren( hSubj ) )
//	{
		tree.SelectItem( hSubj );
		DoSTrials( TRUE );
//	}

	// gripper exp?
	BOOL fGripper = FALSE;
	{
		short nType = EXP_TYPE_HANDWRITING;
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExpID ) ) ) exp.GetType( nType );
		fGripper = ( nType == EXP_TYPE_GRIPPER );
	}

	// get HWR/FRD folder tree item
	HTREEITEM hItem = tree.GetChildItem( hSubj );
	// if NULL, then no trials
	if( hItem )
	{
		if( !fGripper )
		{
			// if not hwr, then pcx
			if( tree.GetItemData( hItem ) != TI_HWRTRIAL_FOLDER )
			{
				hItem = tree.GetNextSiblingItem( hItem );
				if( !hItem && ( tree.GetItemData( hItem ) != TI_HWRTRIAL_FOLDER ) ) return;
			}
		}
		else
		{
			// if not frd, then pcs
			if( tree.GetItemData( hItem ) != TI_FRDTRIAL_FOLDER )
			{
				hItem = tree.GetNextSiblingItem( hItem );
				if( !hItem && ( tree.GetItemData( hItem ) != TI_FRDTRIAL_FOLDER ) ) return;
			}
		}
	}

	// loop through each trial
	if( hItem ) hItem = tree.GetChildItem( hItem );
	while( hItem )
	{
		// trial count
		nTrials++;
		// trial name
		szTrial = tree.GetItemText( hItem );
		// trial path
		::GetHWR( szExpID, szGrpID, szSubjID, szTrial, szPath );
		// TF file (& path)
		szTF = szPath.Left( szPath.GetLength() - 4 );		// removes ext + .
		szTF += _T(".TF");
		// trial condition
		szTemp = szTrial.Left( szTrial.GetLength() - 4 );	// removes ext + .
		szTemp = szTemp.Left( szTemp.GetLength() - 2 );		// remove trial # (2 chars)
		szCond = szTemp.Right( 3 );	// TODO - remove ID length fixed #
		// trial consistency
		if( tree.GetItemImage( hItem, nImage, nSelImage ) )
		{
			if( nImage == IMG_TRIAL_GOOD )
			{
				if( !lstConsConds.Find( szCond ) ) lstConsConds.AddTail( szCond );
				nGoodTrials++;
			}
			else if( nImage == IMG_TRIAL_BAD )
				nBadTrials++;
		}
		// get HWR record date
		if( ff.FindFile( szPath ) )
		{
			ff.FindNextFile();
			ff.GetLastWriteTime( ctTime );
			dtTemp = ctTime.GetTime();
			if( dtExpStart == 0 ) dtExpStart = dtTemp;
			if( dtTemp < dtExpStart ) dtExpStart = dtTemp;
			if( dtTemp > dtExpEnd ) dtExpEnd = dtTemp;
		}
		// get TF date
		if( ff.FindFile( szTF ) )
		{
			ff.FindNextFile();
			ff.GetLastWriteTime( ctTime );
			dtTemp = ctTime.GetTime();
			if( dtTemp > dtProcessed ) dtProcessed = dtTemp;
		}

		// next trial
		hItem = tree.GetNextSiblingItem( hItem );
	}

	// now find the conditions with no consistent trials
	POSITION pos = lstAllConds.GetHeadPosition();
	while( pos )
	{
		szCond = lstAllConds.GetNext( pos );
		if( !lstConsConds.Find( szCond ) )
		{
			if( !pLstCond->Find( szCond ) ) pLstCond->AddTail( szCond );
		}
	}

	// now parse the summarize exclude file for this subject (of this group)
	CString szFile;
	::GetDataPathRoot( szPath );
	szFile.Format( _T("%s\\%s\\%s-who.SUM"), szPath, szExpID, szExpID );
	if( ff.FindFile( szFile ) )
	{
		CStdioFile f;
		if( f.Open( szFile, CFile::modeRead ) )
		{
			int nPos1 = -1, nPos2 = -1;
			CString szLine, szGrp, szSubj, szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );

			while( f.ReadString( szLine ) )
			{
				nPos1 = szLine.Find( _T(" ") );
				nPos2 = szLine.Find( _T("-") );
				if( ( nPos1 != -1 ) && ( nPos2 != -1 ) )
				{
					szGrp = szLine.Mid( nPos1 + 1, nPos2 - nPos1 - 1 );
					TRIM( szGrp );
					nPos1 = szLine.Find( szDelim );
					if( nPos1 != -1 )
					{
						szSubj = szLine.Mid( nPos1 + 1 );
						TRIM( szSubj );

						if( ( szGrp == szGrpID ) && ( szSubj == szSubjID ) )
						{
							fInSummarize = FALSE;
							break;
						}
					}
				}
			}
			f.Close();
		}
	}
}

//************************************
// Method:    OnEReportsubjects
// FullName:  CLeftView::OnEReportsubjects
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEReportsubjects() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	Experiments exp;
	if( FAILED( exp.Find( szExp ) ) )
	{
		BCGPMessageBox( _T("Unable to locate experiment.") );
		return;
	}
	CWaitCursor crs;
	if( exp.ReportSubjects( szFile ) )
	{
		CFileFind f;
		if( !f.FindFile( szFile ) ) return;

		CString szData;
		szData.Format( _T("View subject report for experiment "), szExp );
		OutputAMessage( szData, TRUE );

		if( !Excel::OpenDelimitedText( szFile, EXCEL_FORMAT_SUBJECTREPORT ) )
		{
			CString szCmd;
			szCmd.LoadString( IDS_EDITOR );
			szCmd += szFile;
			WinExec( szCmd, SW_SHOW );
		}
	}
	else BCGPMessageBox( _T("There is no data to produce a subject report.") );
}

//************************************
// Method:    OnUpdateEReportsubjects
// FullName:  CLeftView::OnUpdateEReportsubjects
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEReportsubjects(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStDup
// FullName:  CLeftView::OnStDup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStDup() 
{
	CString szData = _T("SRC: Stimulus - Duplicate.");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szIDs = szItem.Mid( nPos + 2 );

	Stimuluss stim;
	if( SUCCEEDED( stim.Find( szIDs ) ) )
	{
		if( stim.DoAdd() )
		{
			::DataHasChanged();

			CString szItem;
			stim.GetDescriptionWithID( szItem );

			CTreeCtrl& tree = GetTreeCtrl();
			HTREEITEM hItem = tree.InsertItem( szItem, IMG_STIMULUS, IMG_STIMULUS, m_hStim );
			tree.SetItemData( hItem, TI_STIMULUS );

			tree.EnsureVisible( hItem );

			RefreshStimuli();

			stim.GetID( szItem );
			szData.Format( _T("Added new stimulus %s."), szItem );
			OutputAMessage( szData, TRUE );

			tree.SelectItem( hItem );
		}
	}
}

//************************************
// Method:    OnUpdateStDup
// FullName:  CLeftView::OnUpdateStDup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStDup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    ChangeAllInstanceNames
// FullName:  CLeftView::ChangeAllInstanceNames
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: DWORD dwType
// Parameter: CString szOld
// Parameter: CString szNew
//************************************
void CLeftView::ChangeAllInstanceNames( DWORD dwType, CString szOld, CString szNew )
{
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hRoot, hItem, hFolder, hExp, hGrp;
	CString szItem;
	DWORD dwStyle;

	// Which root
	switch( dwType )
	{
		case TI_EXPERIMENT: hRoot = m_hExp; break;
		case TI_GROUP: hRoot = m_hGrp; break;
		case TI_SUBJECT: hRoot = m_hSub; break;
		case TI_CONDITION: hRoot = m_hCond; break;
		case TI_STIMULUS: hRoot = m_hStim; break;
		case TI_ELEMENT: hRoot = m_hElmt; break;
		default: return;
	}

	// Remove from root folder
	hItem = tree.GetChildItem( hRoot );
	while( hItem )
	{
		szItem = tree.GetItemText( hItem );
		if( szItem == szOld ) tree.SetItemText( hItem, szNew );

		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}

	// For stimulus
	if( dwType == TI_STIMULUS ) return;	// do nothing

	// For elements
	if( dwType == TI_ELEMENT )
	{
		hFolder = tree.GetNextItem( m_hStim, TVGN_CHILD );	// 1st stimulus
		while( hFolder )
		{
			hItem = tree.GetNextItem( hFolder, TVGN_CHILD );	// elements folder
			hItem = tree.GetNextItem( hItem, TVGN_CHILD );		// 1st element
			while( hItem )
			{
				szItem = tree.GetItemText( hItem );
				if( szItem == szOld ) tree.SetItemText( hItem, szNew );

				hItem = tree.GetNextItem( hItem, TVGN_NEXT );	// next element
			}

			hFolder = tree.GetNextItem( hFolder, TVGN_NEXT );	// next stimulus
		}

		return;
	}

	// Now find in an experiment if can & remove (not for stimulus or element)
	hExp = tree.GetNextItem( m_hExp, TVGN_CHILD );	// 1st experiment
	while( hExp )
	{
		hFolder = tree.GetNextItem( hExp, TVGN_CHILD );	// Condition or group folder
		while( hFolder )
		{
			dwStyle = tree.GetItemData( hFolder );

			// For conditions
			if( ( dwStyle == TI_CONDITION_FOLDER ) && ( dwType == TI_CONDITION ) )
			{
				hItem = tree.GetNextItem( hFolder, TVGN_CHILD );	// 1st condition
				while( hItem )
				{
					szItem = tree.GetItemText( hItem );
					if( szItem == szOld ) tree.SetItemText( hItem, szNew );

					hItem = tree.GetNextItem( hItem, TVGN_NEXT );	// Next condition
				}
			}

			// For groups/subjects
			if( ( dwStyle == TI_GROUP_FOLDER ) &&
				( ( dwType == TI_GROUP ) || ( dwType == TI_SUBJECT ) ) )
			{
				hGrp = tree.GetNextItem( hFolder, TVGN_CHILD );		// 1st group
				while( hGrp )
				{
					// for groups
					if( dwType == TI_GROUP )
					{
						szItem = tree.GetItemText( hGrp );
						if( szItem == szOld ) tree.SetItemText( hGrp, szNew );
					}
					// for subjects
					else
					{
						hItem = tree.GetNextItem( hGrp, TVGN_CHILD );	// subject folder
						hItem = tree.GetNextItem( hItem, TVGN_CHILD );	// 1st subject
						while( hItem )
						{
							szItem = tree.GetItemText( hItem );
							if( szItem == szOld ) tree.SetItemText( hItem, szNew );

							hItem = tree.GetNextItem( hItem, TVGN_NEXT );	// next subject
						}
					}

					hGrp = tree.GetNextItem( hGrp, TVGN_NEXT );	// next group
				}
			}

			hFolder = tree.GetNextItem( hFolder, TVGN_NEXT );	// Next cond/grp folder
		}

		hExp = tree.GetNextItem( hExp, TVGN_NEXT );	// Next experiment
	}
}

//************************************
// Method:    OnCTestlex
// FullName:  CLeftView::OnCTestlex
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCTestlex() 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CRecView* pRV = pMF->GetSettingsPane();
	if( !pRV ) return;

	NSConditions cond;
	if( !cond.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which Condition
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	// apply instruction
	if( SUCCEEDED( cond.Find( szCond ) ) )
	{
		CString szInstr;
		cond.GetInstruction( szInstr );
		pRV->UpdateInstruction( szInstr );
	}
}

//************************************
// Method:    OnUpdateCTestlex
// FullName:  CLeftView::OnUpdateCTestlex
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCTestlex(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTViewSeg
// FullName:  CLeftView::OnTViewSeg
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewSeg() 
{
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );

	szFile = szFile.Left( szFile.GetLength() - 3 );
	szFile += _T("SEG");

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

// 	InstructionDlg d( this );
// 	d.m_fJustDisplay = TRUE;
// 	d.m_szFileName = szFile;
// 	d.DoModal();

	CWaitCursor crs;
	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateTViewSeg
// FullName:  CLeftView::OnUpdateTViewSeg
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTViewSeg(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( ( dwType == TI_TRIAL ) || ( dwType == TI_IMAGE ) );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTViewCon
// FullName:  CLeftView::OnTViewCon
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTViewCon() 
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// ensure it's a trial
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile, szTrial;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;

	CString szSubj, szGrp, szExp;
	if( !fQuick )
	{
		// Which file?
		HTREEITEM hItem = tree.GetSelectedItem();	// Trial
		szTrial = tree.GetItemText( hItem );

		// Which subject?
		hItem = tree.GetParentItem( hItem );		// Trial folder
		hItem = tree.GetParentItem( hItem );		// Subject
		CString szText = tree.GetItemText( hItem );
		int nPos = szText.ReverseFind( szDelim[ 0 ] );
		szSubj = szText.Mid( nPos + 2 );

		// Which experiment/group
		hItem = tree.GetParentItem( hItem );		// Subject folder
		hItem = tree.GetParentItem( hItem );		// Group
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szGrp = szText.Mid( nPos + 2 );
		hItem = tree.GetParentItem( hItem );		// Group folder
		hItem = tree.GetParentItem( hItem );		// Experiment
		szText = tree.GetItemText( hItem );
		nPos = szText.ReverseFind( szDelim[ 0 ] );
		szExp = szText.Mid( nPos + 2 );
	}
	else
	{
		// extract from file name
		szExp = szTemp.Left( 3 );
		szGrp = szTemp.Mid( 3, 3 );
		szSubj = szTemp.Mid( 6, 3 );
		szTrial = szTemp;
	}

	szTrial = szTrial.Left( szTrial.GetLength() - 6 );
	szTrial += _T(".CON");
	GetHWR( szExp, szGrp, szSubj, szTrial, szFile );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n\n%s"), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CString szData;
	szData.Format( IDS_AL_VIEWEXT, szTrial );
	OutputAMessage( szData, TRUE );

	CWaitCursor crs;
	DataTabDlg* d = new DataTabDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnUpdateTViewCon
// FullName:  CLeftView::OnUpdateTViewCon
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTViewCon(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_TRIAL );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnStfRefresh
// FullName:  CLeftView::OnStfRefresh
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStfRefresh()
{
	int nRes = BCGPMessageBox( _T("This will undo all manual changes that you have made to the stimuli files. Are you sure?"), MB_YESNO );
	if( nRes == IDNO ) return;

	CString szPath;
	::GetDataPathRoot( szPath );
	szPath += _T("\\stimuli\\*.*");

	CString szFile;
	CFileFind ff;
	BOOL fCont = ff.FindFile( szPath );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFilePath();
		if( !ff.IsReadOnly() && !ff.IsDirectory() ) REMOVE_FILE( szFile );
	}
}

//************************************
// Method:    OnUpdateStfRefresh
// FullName:  CLeftView::OnUpdateStfRefresh
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStfRefresh(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCatDelete
// FullName:  CLeftView::OnCatDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatDelete()
{
	Cats cat;
	if( !cat.IsValid() ) return;

	CString szData = _T("SRC: Category - Delete");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCat = szItem.Mid( nPos + 2 );

	szData.Format( _T("Are you sure you want to permanently delete category %s?"), szCat );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	if( SUCCEEDED( cat.Find( szCat ) ) )
	{
		HRESULT hr = cat.Remove();

		szData.Format( _T("Deleting category %s."), szCat );
		OutputAMessage( szData );

		::DataHasChanged();

		// TODO: how we address items who use this category?

		tree.DeleteItem( hItem );
	}
}

//************************************
// Method:    OnUpdateCatDelete
// FullName:  CLeftView::OnUpdateCatDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatDelete(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCatEdit
// FullName:  CLeftView::OnCatEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatEdit()
{
	CString szData;
	szData = _T("SRC: Category - Properties.");
	OutputAMessage( szData );

	Cats cat;
	if( !cat.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCat = szItem.Mid( nPos + 2 );

	szData.Format( _T("View category %s properties."), szCat );
	OutputAMessage( szData );

	if( cat.DoEdit( szCat, this ) )
	{
		::DataHasChanged();

		RefreshCategories();

		Find( szCat );

		CString szItem;
		cat.GetDescriptionWithID( szItem );

		szData.Format( _T("Modified category %s."), szCat );
		OutputAMessage( szData );
	}
}

//************************************
// Method:    OnUpdateCatEdit
// FullName:  CLeftView::OnUpdateCatEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatEdit(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCatfAdd
// FullName:  CLeftView::OnCatfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatfAdd()
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	Cats cat;
	if( !cat.IsValid() ) return;

	CString szData;
	szData = _T("SRC: Category Folder - New Category.");
	OutputAMessage( szData );

	if( cat.DoAdd( this ) )
	{
		::DataHasChanged();

		CString szItem;
		cat.GetDescriptionWithID( szItem );

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_CATEGORY, IMG_CATEGORY, m_hCat );
		tree.SetItemData( hItem, TI_CATEGORY );

		RefreshCategories();

		cat.GetID( szItem );
		Find( szItem );

		szData.Format( _T("Added new category %s."), szItem );
		OutputAMessage( szData, TRUE );
	}
}

//************************************
// Method:    OnUpdateCatfAdd
// FullName:  CLeftView::OnUpdateCatfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatfAdd(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCatfFind
// FullName:  CLeftView::OnCatfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatfFind()
{
	CString szData = _T("SRC: Category Folder - Find.");
	OutputAMessage( szData );

	FindDlg d( _T("Find Category"), this );
	if( d.DoModal() == IDOK ) Find( d.m_szID );
}

//************************************
// Method:    OnUpdateCatfFind
// FullName:  CLeftView::OnUpdateCatfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatfFind(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnFbDelete
// FullName:  CLeftView::OnFbDelete
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbDelete()
{
	Feedbacks fb;
	if( !fb.IsValid() ) return;

	CString szData = _T("SRC: Feedback - Delete");
	OutputAMessage( szData );

	CTreeCtrl& tree = GetTreeCtrl();

	HTREEITEM hItem = tree.GetSelectedItem();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szFb = szItem.Mid( nPos + 2 );

	szData.Format( _T("Are you sure you want to permanently delete feedback %s?"), szFb );
	if( BCGPMessageBox( szData, MB_YESNO ) == IDNO ) return;

	if( SUCCEEDED( fb.Find( szFb ) ) )
	{
		HRESULT hr = fb.Remove();

		szData.Format( _T("Deleting feedback %s."), szFb );
		OutputAMessage( szData );

		::DataHasChanged();

		// TODO: how we address items who use this feedback?

		tree.DeleteItem( hItem );
	}
}

//************************************
// Method:    OnUpdateFbDelete
// FullName:  CLeftView::OnUpdateFbDelete
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbDelete(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnFbEdit
// FullName:  CLeftView::OnFbEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbEdit()
{
	CString szData;
	szData = _T("SRC: Feedback - Properties.");
	OutputAMessage( szData );

	CString szFb;
	BOOL fOK = FALSE;
	{
	Feedbacks fb;
	if( !fb.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	szFb = szItem.Mid( nPos + 2 );

	szData.Format( _T("View feedback %s properties."), szFb );
	OutputAMessage( szData );

	fOK = fb.DoEdit( szFb, this );
	if( fOK )
	{
		CString szItem;
		fb.GetDescriptionWithID( szItem );

		tree.SetItemText( tree.GetSelectedItem(), szItem );

		::DataHasChanged();

		szData.Format( _T("Modified feedback %s."), szFb );
		OutputAMessage( szData );
	}
	}
}

//************************************
// Method:    OnUpdateFbEdit
// FullName:  CLeftView::OnUpdateFbEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbEdit(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnFbfAdd
// FullName:  CLeftView::OnFbfAdd
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbfAdd()
{
	CString szUser;
	::GetCurrentUser( szUser );
	if( szUser == _T("UU1") )
	{
		szUser = _T("All data that is added to this user (UU1) will be lost when installing a new version.\nDo you wish to continue?");
		if( BCGPMessageBox( szUser, MB_YESNO | MB_ICONWARNING ) == IDNO ) return;
	}

	Feedbacks fb;
	if( !fb.IsValid() ) return;

	CString szData;
	szData = _T("SRC: Feedback Folder - New Feedback.");
	OutputAMessage( szData );

	if( fb.DoAdd( this ) )
	{
		::DataHasChanged();

		CString szItem;
		fb.GetDescriptionWithID( szItem );

		CTreeCtrl& tree = GetTreeCtrl();
		HTREEITEM hItem = tree.InsertItem( szItem, IMG_FEEDBACK, IMG_FEEDBACK, m_hFb );
		tree.SetItemData( hItem, TI_FEEDBACK );

		RefreshFeedbacks();

		fb.GetID( szItem );
		Find( szItem );

		szData.Format( _T("Added new feedback %s."), szItem );
		OutputAMessage( szData, TRUE );
	}
}

//************************************
// Method:    OnUpdateFbfAdd
// FullName:  CLeftView::OnUpdateFbfAdd
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbfAdd(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnFbfFind
// FullName:  CLeftView::OnFbfFind
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbfFind()
{
	CString szData = _T("SRC: Feedback Folder - Find.");
	OutputAMessage( szData );

	FindDlg d( _T("Find Feedback"), this );
	if( d.DoModal() == IDOK ) Find( d.m_szID );
}

//************************************
// Method:    OnUpdateFbfFind
// FullName:  CLeftView::OnUpdateFbfFind
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbfFind(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK_FOLDER );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    GetElementList
// FullName:  CLeftView::GetElementList
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CStringList * lst
//************************************
void CLeftView::GetElementList( CStringList* lst )
{
	if( !m_hElmt ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szItem;
	HTREEITEM hItem = tree.GetChildItem( m_hElmt ), hItem2 = NULL;
	while( hItem )
	{
		// if this is a category for some elements
		if( tree.GetItemData( hItem ) == TI_CATEGORY_HOLDER )
		{
			// loop through all of this categories children
			hItem2 = tree.GetChildItem( hItem );
			while( hItem2 )
			{
				szItem = tree.GetItemText( hItem2 );
				lst->AddTail( szItem );

				hItem2 = tree.GetNextItem( hItem2, TVGN_NEXT );
			}
		}
		// this element was not in a category
		else
		{
			szItem = tree.GetItemText( hItem );
			lst->AddTail( szItem );
		}

		hItem = tree.GetNextItem( hItem, TVGN_NEXT );
	}
}

//************************************
// Method:    OnEReportquest
// FullName:  CLeftView::OnEReportquest
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEReportquest()
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	Experiments exp;
	if( FAILED( exp.Find( szExp ) ) )
	{
		BCGPMessageBox( _T("Unable to locate experiment.") );
		return;
	}
	if( exp.ReportQuestionnaires( szFile ) )
	{
		CFileFind f;
		if( !f.FindFile( szFile ) ) return;

		CString szData;
		szData.Format( _T("View questionnaire report for experiment "), szExp );
		OutputAMessage( szData, TRUE );

		if( !Excel::OpenDelimitedText( szFile, EXCEL_FORMAT_QUESTIONNAIRE ) )
		{
			CString szCmd;
			szCmd.LoadString( IDS_EDITOR );
			szCmd += szFile;
			WinExec( szCmd, SW_SHOW );
		}
	}
	else BCGPMessageBox( _T("There is no data to produce a questionnaire report.") );
}

//************************************
// Method:    OnUpdateEReportquest
// FullName:  CLeftView::OnUpdateEReportquest
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEReportquest(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEfCopyall
// FullName:  CLeftView::OnUpdateEfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateECopyall
// FullName:  CLeftView::OnUpdateECopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateECopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGfCopyall
// FullName:  CLeftView::OnUpdateGfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGCopyall
// FullName:  CLeftView::OnUpdateGCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSfCopyall
// FullName:  CLeftView::OnUpdateSfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSCopyall
// FullName:  CLeftView::OnUpdateSCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCfCopyall
// FullName:  CLeftView::OnUpdateCfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCCopyall
// FullName:  CLeftView::OnUpdateCCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateStfCopyall
// FullName:  CLeftView::OnUpdateStfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateStCopyall
// FullName:  CLeftView::OnUpdateStCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateElfCopyall
// FullName:  CLeftView::OnUpdateElfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateElCopyall
// FullName:  CLeftView::OnUpdateElCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCatfCopyall
// FullName:  CLeftView::OnUpdateCatfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCatCopyall
// FullName:  CLeftView::OnUpdateCatCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateFbfCopyall
// FullName:  CLeftView::OnUpdateFbfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbfCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK_FOLDER );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateFbCopyall
// FullName:  CLeftView::OnUpdateFbCopyall
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbCopyall(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK );
	fEnable &= ( ::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    DoCopy
// FullName:  CLeftView::DoCopy
// Access:    protected 
// Returns:   BOOL
// Qualifier:
// Parameter: Objects * pObj
// Parameter: BOOL & fCancel
// Parameter: CString szID
// Parameter: BOOL fRecurse
// Parameter: BOOL fFirst
//************************************
BOOL CLeftView::DoCopy( Objects* pObj, BOOL& fCancel, CString szID, BOOL fRecurse, BOOL fFirst )
{
	if( !pObj ) return FALSE;

	fCancel = FALSE;

	// get the id (if not provided)
	if( szID == _T("") )
	{
		CTreeCtrl& tree = GetTreeCtrl();
		CString szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );
		CString szItem = tree.GetItemText( tree.GetSelectedItem() );
		int nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return FALSE;
		szID = szItem.Mid( nPos + 2 );
	}

	// get the user
	static CString szUserID;
	static BOOL fOverwrite = FALSE;
	static BOOL fSubItems = FALSE;
	SelectUserDlg d( this );
	if( fFirst )
	{
		d.m_fHasSubItems = fRecurse;
		if( d.DoModal() == IDCANCEL )
		{
			fCancel = TRUE;
			return FALSE;
		}
		szUserID = d.m_szUserID;
		fOverwrite = d.m_fOverwrite;
		fSubItems = d.m_fSubItems;
	}

	// get user's root data path
	CString szPath;
	{
	NSMUsers u;
	if( SUCCEEDED( u.Find( szUserID ) ) )
	{
		u.GetRootPath( szPath );
	}
	else
	{
		BCGPMessageBox( _T("Unable to locate user.") );
		return FALSE;
	}
	}

	CWaitCursor crs;

	// do the download
	CString szMsg;
	if( fSubItems )
	{
		if( pObj->DoCopyAll( szID, szPath, fOverwrite, fSubItems ) )
		{
			szMsg.Format( _T("Download operation on item %s completed successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
		else
		{
			szMsg.Format( _T("Download operation on item %s did NOT complete successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
	}
	else
	{
		if( pObj->DoCopy( szID, szPath, fOverwrite ) )
		{
			szMsg.Format( _T("Download operation on item %s completed successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
		else
		{
			szMsg.Format( _T("Download operation on item %s did NOT complete successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
	}

	return d.m_fOverwrite;
}

//************************************
// Method:    FolderTransfer
// FullName:  CLeftView::FolderTransfer
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: Objects * pObj
// Parameter: HTREEITEM hRoot
// Parameter: BOOL fRecurse
// Parameter: BOOL fDoCopy
//************************************
void CLeftView::FolderTransfer( Objects* pObj, HTREEITEM hRoot, BOOL fRecurse, BOOL fDoCopy )
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szItem, szID, szDelim;
	int nPos, nSteps = 0, nCur = 0;
	DWORD dwType;
	BOOL fOverwrite = FALSE, fDidOnce = FALSE, fCancel = FALSE, fHadCatFolder = FALSE;
	HTREEITEM hLastNonCat = NULL;
	double dVal;
	
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// traverse all items to get count for progress meter
	HTREEITEM hItem = tree.GetChildItem( hRoot );
	while( hItem )
	{
		dwType = tree.GetItemData( hItem );

		// if category folder
		if( dwType == TI_CATEGORY_HOLDER )
		{
			// remember last non-categorized item
			hLastNonCat = hItem;
			// indicate we're withing a cat folder
			fHadCatFolder = TRUE;

			// get first element within cat folder (always will have children)
			hItem = tree.GetChildItem( hItem );
		}

		// increment steps
		nSteps++;

		// get next
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );

		// if we were within a cat folder, we need to adjust
		if( !hItem && fHadCatFolder )
		{
			// no longer within cat folder
			fHadCatFolder = FALSE;
			// reset back to last non-cat (works even if 1st was a cat)
			hItem = hLastNonCat;
			// now redo 'get next'
			hItem = tree.GetNextItem( hItem, TVGN_NEXT );
		}
	}

	// if 0 items, do nothing
	if( nSteps <= 0 ) return;

	// enable progress meter
	pMF->EnableProgress( nSteps );

	// traverse all items and transfer each
	hItem = tree.GetChildItem( hRoot );
	while( hItem )
	{
		// get item info
		szItem = tree.GetItemText( hItem );
		dwType = tree.GetItemData( hItem );

		// if category folder
		if( dwType == TI_CATEGORY_HOLDER )
		{
			// remember last non-categorized item
			hLastNonCat = hItem;
			// indicate we're withing a cat folder
			fHadCatFolder = TRUE;

			// get first element within cat folder (always will have children)
			hItem = tree.GetChildItem( hItem );
			szItem = tree.GetItemText( hItem );
		}

		// get ID
		nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return;
		szID = szItem.Mid( nPos + 2 );

		// transfer
		if( fDoCopy )
			fOverwrite = DoCopy( pObj, fCancel, szID, fRecurse, !fDidOnce );
		else
			fOverwrite = DoUpload( pObj, fCancel, szID, fOverwrite, fDidOnce ? FALSE : TRUE, fRecurse );
		if( !fDidOnce ) fDidOnce = TRUE;
		if( fCancel ) break;

		// update progress meter
		nCur++;
		dVal = ( ( 1.0 * nCur ) / ( 1.0 * nSteps ) ) * 100.0;
		szItem.Format( _T("%.1f %% - Transferred item %s"), dVal, szID );
		pMF->SetProgress( nCur, szItem );

		// get next
		hItem = tree.GetNextItem( hItem, TVGN_NEXT );

		// if we were within a cat folder, we need to adjust
		if( !hItem && fHadCatFolder )
		{
			// no longer within cat folder
			fHadCatFolder = FALSE;
			// reset back to last non-cat (works even if 1st was a cat)
			hItem = hLastNonCat;
			// now redo 'get next'
			hItem = tree.GetNextItem( hItem, TVGN_NEXT );
		}
	}

	// reset progress meter
	pMF->DisableProgress();
	szItem.LoadString( IDS_READY );
	pMF->ShowStatusText( szItem );
}

//************************************
// Method:    FolderCopy
// FullName:  CLeftView::FolderCopy
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: Objects * pObj
// Parameter: HTREEITEM hRoot
// Parameter: BOOL fRecurse
//************************************
void CLeftView::FolderCopy( Objects* pObj, HTREEITEM hRoot, BOOL fRecurse )
{
	FolderTransfer( pObj, hRoot, fRecurse, TRUE );
}

//************************************
// Method:    FolderUpload
// FullName:  CLeftView::FolderUpload
// Access:    protected 
// Returns:   void
// Qualifier:
// Parameter: Objects * pObj
// Parameter: HTREEITEM hRoot
// Parameter: BOOL fRecurse
//************************************
void CLeftView::FolderUpload( Objects* pObj, HTREEITEM hRoot, BOOL fRecurse )
{
	FolderTransfer( pObj, hRoot, fRecurse, FALSE );
}

//************************************
// Method:    OnEfCopyall
// FullName:  CLeftView::OnEfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEfCopyall()
{
	Experiments item;
	FolderCopy( &item, m_hExp, TRUE );
}

//************************************
// Method:    OnECopyall
// FullName:  CLeftView::OnECopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnECopyall()
{
	Experiments item;
	BOOL fCancel;
	DoCopy( &item, fCancel, _T(""), TRUE );
}

//************************************
// Method:    OnGfCopyall
// FullName:  CLeftView::OnGfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGfCopyall()
{
	Groups item;
	FolderCopy( &item, m_hGrp );
}

//************************************
// Method:    OnGCopyall
// FullName:  CLeftView::OnGCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGCopyall()
{
	Groups item;
	BOOL fCancel;
	DoCopy( &item, fCancel );
}

//************************************
// Method:    OnSfCopyall
// FullName:  CLeftView::OnSfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSfCopyall()
{
	Subjects item;
	FolderCopy( &item, m_hSub );
}

//************************************
// Method:    OnSCopyall
// FullName:  CLeftView::OnSCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSCopyall()
{
	Subjects item;
	BOOL fCancel;
	DoCopy( &item, fCancel );
}

//************************************
// Method:    OnCfCopyall
// FullName:  CLeftView::OnCfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCfCopyall()
{
	NSConditions item;
	FolderCopy( &item, m_hCond );
}

//************************************
// Method:    OnCCopyall
// FullName:  CLeftView::OnCCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCCopyall()
{
	NSConditions item;
	BOOL fCancel;
	DoCopy( &item, fCancel );
}

//************************************
// Method:    OnStfCopyall
// FullName:  CLeftView::OnStfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStfCopyall()
{
	Stimuluss item;
	FolderCopy( &item, m_hStim, TRUE );
}

//************************************
// Method:    OnStCopyall
// FullName:  CLeftView::OnStCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStCopyall()
{
	Stimuluss item;
	BOOL fCancel;
	DoCopy( &item, fCancel, _T(""), TRUE );
}

//************************************
// Method:    OnElfCopyall
// FullName:  CLeftView::OnElfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElfCopyall()
{
	Elements item;
	FolderCopy( &item, m_hElmt );
}

//************************************
// Method:    OnElCopyall
// FullName:  CLeftView::OnElCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElCopyall()
{
	Elements item;
	BOOL fCancel;
	DoCopy( &item, fCancel );
}

//************************************
// Method:    OnCatfCopyall
// FullName:  CLeftView::OnCatfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatfCopyall()
{
	Cats item;
	FolderCopy( &item, m_hCat );
}

//************************************
// Method:    OnCatCopyall
// FullName:  CLeftView::OnCatCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatCopyall()
{
	Cats item;
	BOOL fCancel;
	DoCopy( &item, fCancel );
}

//************************************
// Method:    OnFbfCopyall
// FullName:  CLeftView::OnFbfCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbfCopyall()
{
	Feedbacks item;
	FolderCopy( &item, m_hCat );
}

//************************************
// Method:    OnFbCopyall
// FullName:  CLeftView::OnFbCopyall
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbCopyall()
{
	Feedbacks item;
	BOOL fCancel;
	DoCopy( &item, fCancel );
}

//************************************
// Method:    OnMQCopy
// FullName:  CLeftView::OnMQCopy
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnMQCopy()
{
	// get the user
	BOOL fOverwrite = FALSE;
	SelectUserDlg d( this );
	d.m_fHasSubItems = FALSE;
	if( d.DoModal() == IDCANCEL ) return;
	CString szUserID = d.m_szUserID;
	fOverwrite = d.m_fOverwrite;

	// get user's root data path
	CString szPath;
	{
		NSMUsers u;
		if( SUCCEEDED( u.Find( szUserID ) ) )
		{
			u.GetRootPath( szPath );
		}
		else
		{
			BCGPMessageBox( _T("Unable to locate user.") );
			return;
		}
	}

	// verify that we're logged in if applicable
	if( !::VerifyLogin() ) return;
	CWaitCursor crs;

	// iterate through to get the IDs
	CStringList lstID;
	CString szID, szMsg;
	MQuestionnaires q;
	HRESULT hr = q.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		q.GetID( szID );
		lstID.AddTail( szID );

		hr = q.GetNext();
	}

	// now loop through IDs and upload each
	POSITION pos = lstID.GetHeadPosition();
	while( pos )
	{
		szID = lstID.GetNext( pos );

		if( q.DoCopy( szID, szPath, fOverwrite ) )
		{
			szMsg.Format( _T("Download operation on item %s completed successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
		else
		{
			szMsg.Format( _T("Download operation on item %s did NOT complete successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
	}
}

//************************************
// Method:    OnMQUpload
// FullName:  CLeftView::OnMQUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnMQUpload()
{
	// ask user about overwrite
	int nRes = BCGPMessageBox( _T("Do you want to replace any questions if they already exist?"), MB_YESNOCANCEL );
	BOOL fOverwrite = ( nRes == IDYES );
	if( nRes == IDCANCEL ) return;

	// verify that we're logged in if applicable
	if( !::VerifyLogin() ) return;

	CWaitCursor crs;

	// iterate through to get the IDs
	CStringList lstID;
	CString szID, szMsg;
	MQuestionnaires q;
	HRESULT hr = q.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		q.GetID( szID );
		lstID.AddTail( szID );

		hr = q.GetNext();
	}

	// now loop through IDs and copy each
	POSITION pos = lstID.GetHeadPosition();
	while( pos )
	{
		szID = lstID.GetNext( pos );

		if( q.DoUpload( szID, fOverwrite ) )
		{
			szMsg.Format( _T("Upload operation on item %s completed successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
		else
		{
			szMsg.Format( _T("Upload operation on item %s did NOT complete successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
	}
}

//************************************
// Method:    OnUpdateMQCopy
// FullName:  CLeftView::OnUpdateMQCopy
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateMQCopy(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = TRUE;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable && !::IsClientServerModeOn() && ::IsCS() );
}

//************************************
// Method:    OnUpdateMQUpload
// FullName:  CLeftView::OnUpdateMQUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateMQUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = TRUE;
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable && !::IsClientServerModeOn() && ::IsCS() );
}

//************************************
// Method:    OnUpdateEfUpload
// FullName:  CLeftView::OnUpdateEfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateEUpload
// FullName:  CLeftView::OnUpdateEUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGfUpload
// FullName:  CLeftView::OnUpdateGfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateGUpload
// FullName:  CLeftView::OnUpdateGUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSfUpload
// FullName:  CLeftView::OnUpdateSfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSUpload
// FullName:  CLeftView::OnUpdateSUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCfUpload
// FullName:  CLeftView::OnUpdateCfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCUpload
// FullName:  CLeftView::OnUpdateCUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateStfUpload
// FullName:  CLeftView::OnUpdateStfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateStUpload
// FullName:  CLeftView::OnUpdateStUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_STIMULUS );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateElfUpload
// FullName:  CLeftView::OnUpdateElfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateElUpload
// FullName:  CLeftView::OnUpdateElUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateElUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_ELEMENT );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCatfUpload
// FullName:  CLeftView::OnUpdateCatfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateCatUpload
// FullName:  CLeftView::OnUpdateCatUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCatUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CATEGORY );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateFbfUpload
// FullName:  CLeftView::OnUpdateFbfUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbfUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK_FOLDER );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateFbUpload
// FullName:  CLeftView::OnUpdateFbUpload
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateFbUpload(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_FEEDBACK );
	fEnable &= ( !::IsClientServerModeOn() && ::IsCS() );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    DoUpload
// FullName:  CLeftView::DoUpload
// Access:    protected 
// Returns:   BOOL
// Qualifier:
// Parameter: Objects * pObj
// Parameter: BOOL & fCancel
// Parameter: CString szID
// Parameter: BOOL fOverwrite
// Parameter: BOOL fAsk
// Parameter: BOOL fRecurse
//************************************
BOOL CLeftView::DoUpload( Objects* pObj, BOOL& fCancel, CString szID, BOOL fOverwrite, BOOL fAsk, BOOL fRecurse )
{
	if( !pObj ) return FALSE;

	fCancel = FALSE;

	// ask user about overwrite
	int nRes = fAsk ?
		BCGPMessageBox( _T("Do you want to replace if it already exists?"), MB_YESNOCANCEL ) :
		fOverwrite ? IDYES : IDNO;
	if( nRes == IDCANCEL )
	{
		fCancel = TRUE;
		return FALSE;
	}

	// recurse sub-items?
	static BOOL fDoSubItems = FALSE;
	if( fAsk && fRecurse )
	{
		int nR = BCGPMessageBox( _T("Upload sub-items as well?"), MB_YESNOCANCEL );
		if( nR == IDCANCEL )
		{
			fCancel = TRUE;
			return FALSE;
		}
		if( nR == IDYES ) fDoSubItems = TRUE;
		else fDoSubItems = FALSE;
	}

	// get the id (if not provided)
	if( szID == _T("") )
	{
		CTreeCtrl& tree = GetTreeCtrl();
		CString szDelim;
		::GetDelimiter( szDelim );
		TRIM( szDelim );
		CString szItem = tree.GetItemText( tree.GetSelectedItem() );
		int nPos = szItem.Find( szDelim );
		if( nPos == -1 ) return FALSE;
		szID = szItem.Mid( nPos + 2 );
	}

	CWaitCursor crs;

	// do the upload
	CString szMsg;
	if( fRecurse )
	{
		if( pObj->DoUploadAll( szID, ( nRes == IDYES ), fDoSubItems ) )
		{
			szMsg.Format( _T("Upload operation on item %s completed successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
		else
		{
			szMsg.Format( _T("Upload operation on item %s did NOT complete successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
	}
	else
	{
		if( pObj->DoUpload( szID, nRes == IDYES ) )
		{
			szMsg.Format( _T("Upload operation on item %s completed successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
		else
		{
			szMsg.Format( _T("Upload operation on item %s did NOT complete successfully."), szID );
			OutputAMessage( szMsg, TRUE, LOG_DB );
		}
	}

	return( nRes == IDYES );
}

//************************************
// Method:    OnEfUpload
// FullName:  CLeftView::OnEfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEfUpload()
{
	Experiments item;
	FolderUpload( &item, m_hExp, TRUE );
}

//************************************
// Method:    OnEUpload
// FullName:  CLeftView::OnEUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEUpload()
{
	Experiments item;
	BOOL fCancel;
	DoUpload( &item, fCancel, _T(""), FALSE, TRUE, TRUE );
}

//************************************
// Method:    OnGfUpload
// FullName:  CLeftView::OnGfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGfUpload()
{
	Groups item;
	FolderUpload( &item, m_hGrp, FALSE );
}

//************************************
// Method:    OnGUpload
// FullName:  CLeftView::OnGUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGUpload()
{
	Groups item;
	BOOL fCancel;
	DoUpload( &item, fCancel );
}

//************************************
// Method:    OnSfUpload
// FullName:  CLeftView::OnSfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSfUpload()
{
	Subjects item;
	FolderUpload( &item, m_hSub, FALSE );
}

//************************************
// Method:    OnSUpload
// FullName:  CLeftView::OnSUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSUpload()
{
	Subjects item;
	BOOL fCancel;
	DoUpload( &item, fCancel );
}

//************************************
// Method:    OnCfUpload
// FullName:  CLeftView::OnCfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCfUpload()
{
	NSConditions item;
	FolderUpload( &item, m_hCond, FALSE );
}

//************************************
// Method:    OnCUpload
// FullName:  CLeftView::OnCUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCUpload()
{
	NSConditions item;
	BOOL fCancel;
	DoUpload( &item, fCancel );
}

//************************************
// Method:    OnStfUpload
// FullName:  CLeftView::OnStfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStfUpload()
{
	Stimuluss item;
	FolderUpload( &item, m_hStim, TRUE );
}

//************************************
// Method:    OnStUpload
// FullName:  CLeftView::OnStUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStUpload()
{
	Stimuluss item;
	BOOL fCancel;
	DoUpload( &item, fCancel, _T(""), FALSE, TRUE, TRUE );
}

//************************************
// Method:    OnElfUpload
// FullName:  CLeftView::OnElfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElfUpload()
{
	Elements item;
	FolderUpload( &item, m_hElmt, FALSE );
}

//************************************
// Method:    OnElUpload
// FullName:  CLeftView::OnElUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnElUpload()
{
	Elements item;
	BOOL fCancel;
	DoUpload( &item, fCancel );
}

//************************************
// Method:    OnCatfUpload
// FullName:  CLeftView::OnCatfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatfUpload()
{
	Cats item;
	FolderUpload( &item, m_hCat, FALSE );
}

//************************************
// Method:    OnCatUpload
// FullName:  CLeftView::OnCatUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCatUpload()
{
	Cats item;
	BOOL fCancel;
	DoUpload( &item, fCancel );
}

//************************************
// Method:    OnFbfUpload
// FullName:  CLeftView::OnFbfUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbfUpload()
{
	Feedbacks item;
	FolderUpload( &item, m_hCat, FALSE );
}

//************************************
// Method:    OnFbUpload
// FullName:  CLeftView::OnFbUpload
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnFbUpload()
{
	Feedbacks item;
	BOOL fCancel;
	DoUpload( &item, fCancel );
}

//************************************
// Method:    OnEExpsumParam
// FullName:  CLeftView::OnEExpsumParam
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEExpsumParam()
{
	ExportSummarization( EXP_SUMM_PARAM );
}

//************************************
// Method:    OnUpdateEExpsumParam
// FullName:  CLeftView::OnUpdateEExpsumParam
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEExpsumParam(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEExpsumSubm
// FullName:  CLeftView::OnEExpsumSubm
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEExpsumSubm()
{
	ExportSummarization( EXP_SUMM_SUBM );
}

//************************************
// Method:    OnUpdateEExpsumSubm
// FullName:  CLeftView::OnUpdateEExpsumSubm
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEExpsumSubm(CCmdUI *pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    ExportSummarization
// FullName:  CLeftView::ExportSummarization
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: int nGroupMode
//************************************
void CLeftView::ExportSummarization( int nGroupMode )
{
	if( ( nGroupMode != EXP_SUMM_PARAM ) && ( nGroupMode != EXP_SUMM_SUBM ) )
		return;

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	// do it!
	if( !AGraphDlg::ExportSummarization( szExp, nGroupMode ) )
		BCGPMessageBox( _T("Error in completing summarization export process.") );
}

//************************************
// Method:    OnTLinearity
// FullName:  CLeftView::OnTLinearity
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTLinearity()
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which trial?
	HTREEITEM hItem = tree.GetSelectedItem();			// Trial
	CString szTrial = tree.GetItemText( hItem );

	// Which subject?
	hItem = tree.GetParentItem( hItem );		// Trial folder
	hItem = tree.GetParentItem( hItem );				// Subject
	CString szText = tree.GetItemText( hItem );
	int nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szSubj = szText.Mid( nPos + 2 );

	// Which experiment/group
	hItem = tree.GetParentItem( hItem );				// Subject folder
	hItem = tree.GetParentItem( hItem );				// Group
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szGrp = szText.Mid( nPos + 2 );
	hItem = tree.GetParentItem( hItem );				// Group folder
	hItem = tree.GetParentItem( hItem );				// Experiment
	szText = tree.GetItemText( hItem );
	nPos = szText.ReverseFind( szDelim[ 0 ] );
	CString szExp = szText.Mid( nPos + 2 );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->TestTrialLinearity( szExp, szSubj, szGrp, szTrial );
}

//************************************
// Method:    OnUpdateTLinearity
// FullName:  CLeftView::OnUpdateTLinearity
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTLinearity(CCmdUI *pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_TRIAL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEReportSummaryB
// FullName:  CLeftView::OnEReportSummaryB
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEReportSummaryB()
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	Experiments exp;
	if( FAILED( exp.Find( szExp ) ) )
	{
		BCGPMessageBox( _T("Unable to locate experiment.") );
		return;
	}

	// Path
	CString szID, szDesc;
	exp.GetID( szID );
	exp.GetDescription( szDesc );
	::GetExpNoExt( szID, szFile );

	// ensure path is created
	CString szPath;
	nPos = szFile.ReverseFind( '\\' );
	if( nPos != -1 )
	{
		szPath = szFile.Left( nPos );
		_mkdir( szPath );
	}
	szFile += _T("SumRpt.RPT");

	// Create list of grp/members
	CString szGrpID, szSubjID, szItem;
	CStringList lst;
	BSTR bstr;
	IExperimentMember* pEM = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
									 IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return;
	}

	hr = pEM->FindForExperiment( szID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEM->get_GroupID( &bstr );
		szGrpID = bstr;
		pEM->get_SubjectID( &bstr );
		szSubjID = bstr;

		szItem.Format( _T("%s%s%s"), szGrpID, szDelim, szSubjID );
		lst.AddTail( szItem );

		hr = pEM->GetNext();
	}
	pEM->Release();

	if( lst.GetCount() <= 0 )
	{
		BCGPMessageBox( _T("There is no data to produce a summary report.") );
		return;
	}

	// Sort list
	::SortStringListAlpha( &lst );

	// Loop through list...get data...write to file
	CStdioFile f;
	CString szGrp, szSubj;
	BOOL fPrv = ::IsPrivacyOn();
	
	if( !f.Open( szFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		BCGPMessageBox( _T("Unable to open file for writing.") );
		return;
	}

	// 1st line = user
	CString szData;
	f.WriteString( _T("User: ") );
	::GetCurrentUser( szData );
	f.WriteString( szData );
	f.WriteString( _T(" - ") );
	::GetCurrentUserDesc( szData );
	f.WriteString( szData );
	f.WriteString( _T("\n") );
	f.WriteString( _T("\n") );

	// experiment
	szData.Format( _T("Summary (brief) report for experiment %s - %s\n\n"), szID, szDesc );
	f.WriteString( szData );

	// experiment instructions
	{
		CString szFileI = szPath;
		szFileI += _T("\\");
		szFileI += szID;
		szFileI += _T(".ins");
		CStdioFile fI;
		if( fI.Open( szFileI, CFile::modeRead ) )
		{
			f.WriteString( _T("________________________\n") );
			f.WriteString( _T("EXPERIMENT INSTRUCTIONS\n" ) );
			while( fI.ReadString( szData ) )
			{
				f.WriteString( szData );
				f.WriteString( _T("\n") );
			}
		}
	}

	// groups & subjects
	Groups grp;
	Subjects subj;
	POSITION pos = lst.GetHeadPosition();
	CString szLastGrp = _T("");
	while( pos )
	{
		szItem = lst.GetNext( pos );

		// parse out id's
		szGrpID = _T("");
		szSubjID = _T("");
		nPos = szItem.Find( szDelim );
		if( nPos != -1 )
		{
			szGrpID = szItem.Left( nPos );
			szSubjID = szItem.Mid( nPos + 1 );
		}

		// group info
		hr = grp.Find( szGrpID );
		if( SUCCEEDED( hr ) )
		{
			grp.GetDescription( szGrp );

			hr = subj.Find( szSubjID );
			if( SUCCEEDED( hr ) ) subj.GetName( szSubj, fPrv );
		}

		// if new group, write group
		if( SUCCEEDED( hr ) && ( szLastGrp != szGrpID ) )
		{
			f.WriteString( _T("________________________\n") );
			szItem.Format( _T("GROUP %s - %s.\n"), szGrpID, szGrp );
			f.WriteString( szItem );
		}
		if( SUCCEEDED( hr ) )
		{
			// update last group if necessary
			if( szLastGrp != szGrpID ) szLastGrp = szGrpID;

			// write subject
			szItem.Format( _T("\tSUBJECT %s - %s\n"), szSubjID, szSubj );
			f.WriteString( szItem );
		}
	}

	// Conditions and their stimuli (+elements) & feedback
	CString szCondID, szCond, szStimID, szStim, szElemID, szElem, szFbID, szFb;
	short nSeq = 0;
	IExperimentCondition* pEC = NULL;
	hr = ::CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
							 IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCOND_ERR );
		return;
	}
	IStimulusElement* pSEL = NULL;
	hr = ::CoCreateInstance( CLSID_StimulusElement, NULL, CLSCTX_ALL,
							 IID_IStimulusElement, (LPVOID*)&pSEL );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusElement object.") );
		pEC->Release();
		return;
	}
	IStimulusTarget* pSTL = NULL;
	hr = ::CoCreateInstance( CLSID_StimulusTarget, NULL, CLSCTX_ALL,
							 IID_IStimulusTarget, (LPVOID*)&pSTL );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create StimulusTarget object.") );
		pEC->Release();
		pSEL->Release();
		return;
	}

	NSConditions cond;
	Stimuluss stim;
	Elements elem;
	Feedbacks fb;
	hr = pEC->FindForExperiment( szID.AllocSysString() );
	while( SUCCEEDED( hr ) )
	{
		pEC->get_ConditionID( &bstr );
		szCondID = bstr;
		if( SUCCEEDED( cond.Find( szCondID ) ) )
		{
			cond.GetDescription( szCond );
			f.WriteString( _T("________________________\n") );
			szItem.Format( _T("CONDITION %s - %s\n"), szCondID, szCond );
			f.WriteString( szItem );

			// condition instructions
			{
				CString szFileI = szPath;
				szFileI += _T("\\");
				szFileI += szID;
				szFileI += szCondID;
				szFileI += _T(".ins");
				CStdioFile fI;
				if( fI.Open( szFileI, CFile::modeRead ) )
				{
					f.WriteString( _T("\t________________________\n") );
					f.WriteString( _T("\tINSTRUCTIONS\n" ) );
					while( fI.ReadString( szData ) )
					{
						f.WriteString( _T("\t") );
						f.WriteString( szData );
						f.WriteString( _T("\n\n") );
					}
				}
			}

			// stimuli
			cond.GetStimulusWarning( szStimID );
			if( SUCCEEDED( stim.Find( szStimID ) ) )
			{
				stim.GetDescription( szStim );
				f.WriteString( _T("\t________________________\n") );
				szItem.Format( _T("\tWARNING STIMULUS %s - %s\n"), szStimID, szStim );
				f.WriteString( szItem );

				// elements
				hr = pSEL->FindForStimulus( szStimID.AllocSysString() );
				while( SUCCEEDED( hr ) )
				{
					pSEL->get_ElementID( &bstr );
					szElemID = bstr;
					if( SUCCEEDED( elem.Find( szElemID ) ) )
					{
						elem.GetDescription( szElem );
						szItem.Format( _T("\t\tELEMENT %s - %s\n"), szElemID, szElem );
						f.WriteString( szItem );
					}

					hr = pSEL->GetNext();
				}

				// target sequence
				lst.RemoveAll();
				hr = pSTL->FindForStimulus( szStimID.AllocSysString() );
				if( SUCCEEDED( hr ) ) f.WriteString( _T("\n") );
				while( SUCCEEDED( hr ) )
				{
					pSTL->get_ElementID( &bstr );
					szElemID = bstr;
					pSTL->get_Sequence( &nSeq );

					if( nSeq < 10 ) szItem.Format( _T("0%d%s"), nSeq, szElemID );
					else szItem.Format( _T("%d%s"), nSeq, szElemID );

					lst.AddTail( szItem );

					hr = pSTL->GetNext();
				}
				::SortStringListAlpha( &lst );
				pos = lst.GetHeadPosition();
				while( pos )
				{
					szItem = lst.GetNext( pos );
					szElemID = szItem.Right( 3 );
					if( SUCCEEDED( elem.Find( szElemID ) ) )
					{
						elem.GetDescription( szElem );
						szItem.Format( _T("\t\tTARGET %s - %s\n"), szElemID, szElem );
						f.WriteString( szItem );
					}
				}
			}
			cond.GetStimulusPrecue( szStimID );
			if( SUCCEEDED( stim.Find( szStimID ) ) )
			{
				stim.GetDescription( szStim );
				f.WriteString( _T("\t________________________\n") );
				szItem.Format( _T("\tPRECUE STIMULUS %s - %s\n"), szStimID, szStim );
				f.WriteString( szItem );

				// elements
				hr = pSEL->FindForStimulus( szStimID.AllocSysString() );
				while( SUCCEEDED( hr ) )
				{
					pSEL->get_ElementID( &bstr );
					szElemID = bstr;
					if( SUCCEEDED( elem.Find( szElemID ) ) )
					{
						elem.GetDescription( szElem );
						szItem.Format( _T("\t\tELEMENT %s - %s\n"), szElemID, szElem );
						f.WriteString( szItem );
					}

					hr = pSEL->GetNext();
				}

				// target sequence
				lst.RemoveAll();
				hr = pSTL->FindForStimulus( szStimID.AllocSysString() );
				if( SUCCEEDED( hr ) ) f.WriteString( _T("\n") );
				while( SUCCEEDED( hr ) )
				{
					pSTL->get_ElementID( &bstr );
					szElemID = bstr;
					pSTL->get_Sequence( &nSeq );

					if( nSeq < 10 ) szItem.Format( _T("0%d%s"), nSeq, szElemID );
					else szItem.Format( _T("%d%s"), nSeq, szElemID );

					lst.AddTail( szItem );

					hr = pSTL->GetNext();
				}
				::SortStringListAlpha( &lst );
				pos = lst.GetHeadPosition();
				while( pos )
				{
					szItem = lst.GetNext( pos );
					szElemID = szItem.Right( 3 );
					if( SUCCEEDED( elem.Find( szElemID ) ) )
					{
						elem.GetDescription( szElem );
						szItem.Format( _T("\t\tTARGET %s - %s\n"), szElemID, szElem );
						f.WriteString( szItem );
					}
				}
			}
			cond.GetStimulus( szStimID );
			if( SUCCEEDED( stim.Find( szStimID ) ) )
			{
				stim.GetDescription( szStim );
				f.WriteString( _T("\t________________________\n") );
				szItem.Format( _T("\tIMPERATIVE STIMULUS %s - %s\n"), szStimID, szStim );
				f.WriteString( szItem );

				// elements
				hr = pSEL->FindForStimulus( szStimID.AllocSysString() );
				while( SUCCEEDED( hr ) )
				{
					pSEL->get_ElementID( &bstr );
					szElemID = bstr;
					if( SUCCEEDED( elem.Find( szElemID ) ) )
					{
						elem.GetDescription( szElem );
						szItem.Format( _T("\t\tELEMENT %s - %s\n"), szElemID, szElem );
						f.WriteString( szItem );
					}

					hr = pSEL->GetNext();
				}

				// target sequence
				lst.RemoveAll();
				hr = pSTL->FindForStimulus( szStimID.AllocSysString() );
				if( SUCCEEDED( hr ) ) f.WriteString( _T("\n") );
				while( SUCCEEDED( hr ) )
				{
					pSTL->get_ElementID( &bstr );
					szElemID = bstr;
					pSTL->get_Sequence( &nSeq );

					if( nSeq < 10 ) szItem.Format( _T("0%d%s"), nSeq, szElemID );
					else szItem.Format( _T("%d%s"), nSeq, szElemID );

					lst.AddTail( szItem );

					hr = pSTL->GetNext();
				}
				::SortStringListAlpha( &lst );
				pos = lst.GetHeadPosition();
				while( pos )
				{
					szItem = lst.GetNext( pos );
					szElemID = szItem.Right( 3 );
					if( SUCCEEDED( elem.Find( szElemID ) ) )
					{
						elem.GetDescription( szElem );
						szItem.Format( _T("\t\tTARGET %s - %s\n"), szElemID, szElem );
						f.WriteString( szItem );
					}
				}
			}

			// feedback
			cond.GetFeedback( szFbID );
			if( SUCCEEDED( fb.Find( szFbID ) ) )
			{
				f.WriteString( _T("\n") );
				fb.GetDescription( szFb );
				szItem.Format( _T("\tFEEDBACK %s - %s\n"), szFbID, szFb );
				f.WriteString( szItem );
			}
		}

		hr = pEC->GetNext();
	}

	pSTL->Release();
	pSEL->Release();
	pEC->Release();

	f.Close();

	// display
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnUpdateEReportSummaryB
// FullName:  CLeftView::OnUpdateEReportSummaryB
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEReportSummaryB(CCmdUI* pCmdUI)
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnEReportSummaryD
// FullName:  CLeftView::OnEReportSummaryD
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEReportSummaryD()
{
	// todo
}

//************************************
// Method:    OnUpdateEReportSummaryD
// FullName:  CLeftView::OnUpdateEReportSummaryD
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEReportSummaryD(CCmdUI* pCmdUI)
{
	// todo
	pCmdUI->Enable( FALSE );
}

//************************************
// Method:    OnIView
// FullName:  CLeftView::OnIView
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnIView()
{
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );

	CWaitCursor crs;
	ImageDlg d( szFile, this );
	d.DoModal();
}

//************************************
// Method:    OnIViewF
// FullName:  CLeftView::OnIViewF
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnIViewF()
{
	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );

	szFile = szFile.Left( szFile.GetLength() - 4 );
	szFile += _T("-F.PCX");

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		CString szMsg;
		szMsg.Format( _T("The following file does not exist:\n%s\n\nPlease ensure you have processed this image."), szFile );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONINFORMATION );
		return;
	}

	CWaitCursor crs;
	ImageDlg d( szFile, this );
	d.DoModal();
}

//************************************
// Method:    OnIProcess
// FullName:  CLeftView::OnIProcess
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnIProcess()
{
	if( BCGPMessageBox( _T("Are you sure you want to process this image?\nAll previously processed files will be replaced."), MB_YESNO ) == IDNO )
		return;

	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );

	CFileFind f;
	if( !f.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("Unable to locate file to process.") );
		return;
	}

	// Trial
	CString szDelim = ::GetDelimiter();
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItemT = tree.GetSelectedItem();
	CString szTrial = tree.GetItemText( hItemT );
	// Which subject
	HTREEITEM hItem = tree.GetParentItem( hItemT );	// Trial folder
	hItem = tree.GetParentItem( hItem );			// Subject
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );
	// Which experiment
	HTREEITEM hItem2 = tree.GetParentItem( hItem );	// Subject folder
	hItem2 = tree.GetParentItem( hItem2 );			// Group
	szItem = tree.GetItemText( hItem2 );
	nPos = szItem.Find( szDelim );
	CString szGrpID = szItem.Mid( nPos + 2 );
	hItem2 = tree.GetParentItem( hItem2 );			// Group folder
	hItem2 = tree.GetParentItem( hItem2 );			// Experiment
	szItem = tree.GetItemText( hItem2 ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	// experiment object for missing data value
	CString szDataPath;
	::GetDataPathRoot( szDataPath );
	Experiments exp;
	exp.SetDataPath( szDataPath );
	double dMissing = 0.;
	if( SUCCEEDED( exp.Find( szExpID ) ) ) exp.GetMissingDataValue( dMissing );

	// Experiment Settings
	IProcSetting *pPS = NULL;
	HRESULT hr = CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
		IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_PS_ERR );
		return;
	}
	pPS->Find( szExpID.AllocSysString() );
	// image processing settings
	short nSmoothIter = 0 , nInkThreshold = 0, nImgReso = 0;
	double dMinSegLen = 0.;
	pPS->Find( szExpID.AllocSysString() );
	pPS->get_SmoothingIter( &nSmoothIter );
	pPS->get_InkThreshold( &nInkThreshold );
	pPS->get_ImgResolution( &nImgReso );
	pPS->get_MinStrokeSize( &dMinSegLen );
	pPS->Release();

	// trial #
	CString szTrialNum = szFile.Mid( szFile.GetLength() - 6, 2 );

	// run SFINX
	BOOL fRet = SFINX::RunSFINX( szPath, szFile, atoi( szTrialNum ), nSmoothIter,
								 nInkThreshold, nImgReso, dMinSegLen, dMissing );

	// inform user
	if( !fRet ) BCGPMessageBox( _T("There was an error processing the image.") );
	else BCGPMessageBox( _T("Image processing complete.") );
}

//************************************
// Method:    OnUpdateIProcess
// FullName:  CLeftView::OnUpdateIProcess
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateIProcess( CCmdUI* pCmdUI )
{
	IS_EXP_RUNNING()
	fRunExp &= !::IsClientServerModeOn();
	fRunExp &= ::IsOA( TRUE );
	pCmdUI->Enable( fRunExp );
}

//************************************
// Method:    OnStStimEdit
// FullName:  CLeftView::OnStStimEdit
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnStStimEdit()
{
	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szStim = szItem.Mid( nPos + 2 );

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->StimulusEditor( szStim );
}

//************************************
// Method:    OnUpdateStStimEdit
// FullName:  CLeftView::OnUpdateStStimEdit
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateStStimEdit(CCmdUI* pCmdUI)
{
	IS_EXP_RUNNING();
	fRunExp &= !::IsClientServerModeOn();
	fRunExp &= ::IsOA( TRUE );
	pCmdUI->Enable( fRunExp );
}

//************************************
// Method:    OnSLastExp
// FullName:  CLeftView::OnSLastExp
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSLastExp()
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );
	CString szSubj = szItem.Left( nPos );

	// Which experiment/group
	HTREEITEM hExp = tree.GetParentItem( hItem ); // Subject folder
	hExp = tree.GetParentItem( hExp ); // Group
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrpID = szItem.Mid( nPos + 2 );
	hExp = tree.GetParentItem( hExp ); // Group folder
	hExp = tree.GetParentItem( hExp ); // Experiment
	szItem = tree.GetItemText( hExp ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );
	CString szExp = szItem.Left( nPos - 1 );

	// locate experiment
	Experiments exp;
	if( FAILED( exp.Find( szExpID ) ) )
	{
		BCGPMessageBox( _T("Unable to locate experiment.") );
		return;
	}

	// if this is an image experiment, no info
	short nType = EXP_TYPE_HANDWRITING;
	exp.GetType( nType );
	if( nType == EXP_TYPE_IMAGE )
	{
		BCGPMessageBox( _T("Image experiments cannot be run for subjects.") );
		return;
	}

	// get last run date (this group)
	CTime tmLast = exp.LastRun( szSubjID, szGrpID );
	if( tmLast != 0 )
	{
		CString szDateTime = tmLast.Format( _T("%m/%d/%Y %H:%M") );
		CString szMsg;
		szMsg.Format( _T("This subject was last run for group %s of experiment %s on: %s."),
					  szGrpID, szExpID, szDateTime );
		BCGPMessageBox( szMsg );
	}
	else BCGPMessageBox( _T("This subject has not been run for the experiment.") );
}

//************************************
// Method:    OnUpdateSLastExp
// FullName:  CLeftView::OnUpdateSLastExp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSLastExp(CCmdUI* pCmdUI)
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnExportWizard
// FullName:  CLeftView::OnExportWizard
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnExportWizard()
{
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	CMainFrame* pMF = (CMainFrame*)AfxGetMainWnd();
	if( pMF ) pMF->DoExportWizard( szExp );
}

//************************************
// Method:    OnUpdateExportWizard
// FullName:  CLeftView::OnUpdateExportWizard
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateExportWizard(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSRecoverPass
// FullName:  CLeftView::OnSRecoverPass
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSRecoverPass()
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );

	Subjects subj;
	if( SUCCEEDED( subj.Find( szSubjID ) ) )
	{
		CString szSig = ::GetSignature();
		CString szSiteID = ::GetCurrentUserSiteID();
		CString szSiteDesc = ::GetCurrentUserSiteDesc();
		CString szFilePath, szFile, szUserID;
		::GetDataPathRoot( szFilePath );
		::GetCurrentUser( szUserID );
		if( subj.RecoverPassword( szUserID, szSiteID, szSiteDesc, szSig, szFilePath, szFile ) )
		{
			CString szMsg;
			szMsg.Format( _T("Subject password recovery file successfully created in:\n\n%s.\n\nEmail this file to support@neuroscript.net with subject \"PASSWORD RECOVERY\".\nNeuroScript will respond with details on proceeding."), szFile );
			BCGPMessageBox( szMsg );
		}
		else OutputAMessage( _T("Error in recovering password.") );

	}
}

//************************************
// Method:    OnUpdateSRecoverPass
// FullName:  CLeftView::OnUpdateSRecoverPass
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSRecoverPass( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSRestorePass
// FullName:  CLeftView::OnSRestorePass
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSRestorePass()
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );

	Subjects subj;
	if( SUCCEEDED( subj.Find( szSubjID ) ) )
	{
		CString szSig = ::GetSignature();
		CString szSiteID = ::GetCurrentUserSiteID();
		CString szSiteDesc = ::GetCurrentUserSiteDesc();
		CString szUserID;
		::GetCurrentUser( szUserID );
		if( subj.RestorePassword( szUserID, szSiteID, szSiteDesc, szSig ) )
		{
			CString szMsg = _T("Subject password restoration successfully completed.\nYou can now begin using your newly assigned password.");
			BCGPMessageBox( szMsg );
		}
		else OutputAMessage( _T("Error in restoring password.") );

	}
}

//************************************
// Method:    OnUpdateSRestorePass
// FullName:  CLeftView::OnUpdateSRestorePass
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSRestorePass( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    ShowStartPage
// FullName:  CLeftView::ShowStartPage
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::ShowStartPage()
{
	m_pStartView = CHtmlDlg::ShowTheWindow();
//	CHtmlDlg *pStartDlg = new CHtmlDlg( &m_wndDesktop, TRUE, &m_pStartView );
}

//************************************
// Method:    HideStartPage
// FullName:  CLeftView::HideStartPage
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::HideStartPage()
{
	if( m_pStartView && m_pStartView->IsWindowVisible() )
		m_pStartView->ShowWindow( SW_HIDE );
}

///////////////////////////////////////////////////////////////////////////////
// handle messages as a message target
///////////////////////////////////////////////////////////////////////////////
//************************************
// Method:    Message
// FullName:  CLeftView::Message
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: LPARAM msg
// Parameter: WPARAM val
// Parameter: BOOL * fStop
//************************************
void CLeftView::Message( LPARAM msg, WPARAM val, BOOL* fStop )
{
	WORD wMsg = LOWORD( msg );
	WORD wSubMsg = HIWORD( msg );

	if( wMsg == MSG_RUN_EXPERIMENT )
	{
		if( wSubMsg == MSG_SUB_RUN_EXPERIMENT )
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			if( !pMF ) return;

			UserObj* pObj = ::GetCurrentUserObj();
			if( !pObj ) return;

			HTREEITEM hParam = Find( pObj->m_szExpID, pObj->m_szGrpID, pObj->m_szSubjID );
			pMF->RunExperiment( pObj->m_pPS, pObj->m_szExpID, pObj->m_szExp,
								pObj->m_szSubjID, pObj->m_szSubj, pObj->m_szGrpID,
								pObj->m_pTRI->pConditions, pObj->m_pTRI->fSen2Word, hParam );
		}
	}
	else if( wMsg == MSG_ACTION )
	{
		if( wSubMsg == MSG_SUB_TRIAL_SELECT )
		{
			CString szTrial = (char*)val;
			int nPos = szTrial.ReverseFind( '\\' );
			if( nPos != -1 )
			{
				szTrial = szTrial.Mid( nPos + 1 );
				MoveAndFind( szTrial, TRUE, TRUE );
			}
		}
		else if( wSubMsg == MSG_SUB_SHOWTRIAL )
		{
			CString szDelim;
			::GetDelimiter( szDelim );
			TRIM( szDelim );
			CString szData = (char*)val, szItem;

			CString szExp, szSubj, szGrp, szCond, szTrial, szName;
			BOOL fValid = FALSE, fInit = FALSE, fNothing = FALSE, fPCX = FALSE;

			// experiment id
			int nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			szExp = szData.Left( nPos );
			szData = szData.Mid( nPos + 1 );
			// subject id
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			szSubj = szData.Left( nPos );
			szData = szData.Mid( nPos + 1 );
			// group id
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			szGrp = szData.Left( nPos );
			szData = szData.Mid( nPos + 1 );
			// condition id
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			szCond = szData.Left( nPos );
			szData = szData.Mid( nPos + 1 );
			// trial
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			szTrial = szData.Left( nPos );
			TRIM( szTrial );
			szData = szData.Mid( nPos + 1 );
			// name
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			szName = szData.Left( nPos );
			szData = szData.Mid( nPos + 1 );
			// valid/passed
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			fValid = atoi( szData.Left( nPos ) );
			szData = szData.Mid( nPos + 1 );
			// init
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			fInit = atoi( szData.Left( nPos ) );
			szData = szData.Mid( nPos + 1 );
			// nothing
			nPos = szData.Find( szDelim );
			if( nPos == -1 ) return;
			fNothing = atoi( szData.Left( nPos ) );
			szData = szData.Mid( nPos + 1 );
			// valid/passed
			fPCX = atoi( szData );

			ShowTrial( szExp, szSubj, szGrp, szCond, szTrial, Experiments::_hItem,
				       szName, fValid, fInit, fNothing, fPCX );
		}
	}
}

//************************************
// Method:    OnSCondSequence
// FullName:  CLeftView::OnSCondSequence
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSCondSequence()
{
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which subject
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szSubjID = szItem.Mid( nPos + 2 );

	// Which experiment/group
	HTREEITEM hExp = tree.GetParentItem( hItem ); // Subject folder
	hExp = tree.GetParentItem( hExp ); // Group
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrpID = szItem.Mid( nPos + 2 );
	hExp = tree.GetParentItem( hExp ); // Group folder
	hExp = tree.GetParentItem( hExp ); // Experiment
	szItem = tree.GetItemText( hExp ); 
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExpID = szItem.Mid( nPos + 2 );

	// Experiment type
	short nType = EXP_TYPE_IMAGE;
	Experiments exp;
	if( SUCCEEDED( exp.Find( szExpID.AllocSysString() )) ) exp.GetType( nType );
	if( nType == EXP_TYPE_IMAGE )
	{
		BCGPMessageBox( _T("There is no sequence for image experiments.") );
		return;
	}

	// FILE LOCATION
	// we'll use the ERR file to avoid writing more code
	::GetERR( szExpID, szGrpID, szSubjID, _T(""), szItem );
	// remove the extension and add "SEQ"
	szItem = szItem.Left( szItem.GetLength() - 3 );
	szItem += _T("SEQ");

	// Does file exit? If not inform that we are generating the file
	CFileFind ff;
	if( !ff.FindFile( szItem ) )
	{
		OutputAMessage( _T("INFO: Trial sequence file missing. GENERATING."), TRUE );
		Subjects::GenerateTrialSequence( szExpID, szGrpID, szSubjID, nType );
	}

	// Does file exit? If not there after generation, inform and end
	if( !ff.FindFile( szItem ) )
	{
		BCGPMessageBox( _T("No trial sequence file exists.") );
		return;
	}

	// Open
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szItem;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnUpdateSCondSequence
// FullName:  CLeftView::OnUpdateSCondSequence
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSCondSequence(CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnTQuick
// FullName:  CLeftView::OnTQuick
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnTQuick()
{
	if( !m_hQ ) return;

	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return;
	CString szItem = tree.GetItemText( hItem );	// Trial

	// Locate if in list already
	CString szItemSib;
	HTREEITEM hSib = tree.GetChildItem( m_hQ );
	while( hSib )
	{
		szItemSib = tree.GetItemText( hSib );
		if( szItemSib == szItem )
		{
			BCGPMessageBox( _T("This trial is already in the Trial Quick Links.") );
			return;
		}
		hSib = tree.GetNextSiblingItem( hSib );
	}

	BOOL fQuick = FALSE;
	CString szPath, szTemp, szFile;
	if( !GetSelectedTrialLocation( szTemp, szPath, fQuick ) ) return;
	szFile.Format( _T("%s\\%s"), szPath, szTemp );
	BOOL fPCX = ( szFile.Right( 3 ) == _T("PCX") );
	CString szTF = szFile.Left( szFile.GetLength() - 3 );
	szTF += _T("TF");

	// consistency passing
	int nImage = IMG_TRIAL;
	CFileFind ff;
	if( fPCX )
	{
		nImage = IMG_IMAGE;
	}
	else if( ff.FindFile( szTF ) )
	{
		if( ::HasTrialPassed( szFile ) ) nImage = IMG_TRIAL_GOOD;
		else nImage = IMG_TRIAL_BAD;
	}
	else nImage = IMG_TRIAL_BLUE;

	// add
	m_lstQuick.AddTail( szItem );
	hItem = tree.InsertItem( szItem, nImage, nImage, m_hQ, TVI_SORT );
	tree.SetItemData( hItem, TI_TRIAL );

	// expand tree
	tree.Expand( m_hQ, TVE_EXPAND );
}

//************************************
// Method:    OnUpdateTQuick
// FullName:  CLeftView::OnUpdateTQuick
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateTQuick( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_TRIAL );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    RefreshQuickLinks
// FullName:  CLeftView::RefreshQuickLinks
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::RefreshQuickLinks()
{
	if( !m_hQ ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	// Remove first
	HTREEITEM hItem = tree.GetNextItem( m_hQ, TVGN_CHILD ), hItem2 = hItem;
	while( hItem2 )
	{
		hItem2 = tree.GetNextSiblingItem( hItem );
		tree.DeleteItem( hItem );
		hItem = hItem2;
	}

	CString szItem;
	POSITION pos = m_lstQuick.GetHeadPosition();
	while( pos )
	{
		szItem = m_lstQuick.GetNext( pos );

		CString szExp, szGrp, szSubj, szFile;
		szExp = szItem.Left( 3 );
		szGrp = szItem.Mid( 3, 3 );
		szSubj = szItem.Mid( 6, 3 );

		BOOL fPCX = ( szItem.Right( 3 ) == _T("PCX") );
		BOOL fFRD = ( szItem.Right( 3 ) == _T("FRD") );

		if( fFRD ) ::GetHWR( szExp, szGrp, szSubj, szItem, szFile );
		else ::GetFRD( szExp, szGrp, szSubj, szItem, szFile );

		CString szTF = szFile.Left( szFile.GetLength() - 3 );
		szTF += _T("TF");

		// consistency passing
		int nImage = IMG_TRIAL;
		CFileFind ff;
		if( fPCX )
		{
			nImage = IMG_IMAGE;
		}
		else if( ff.FindFile( szTF ) )
		{
			if( ::HasTrialPassed( szFile ) ) nImage = IMG_TRIAL_GOOD;
			else nImage = IMG_TRIAL_BAD;
		}
		else nImage = IMG_TRIAL_BLUE;

		hItem = tree.InsertItem( szItem, nImage, nImage, m_hQ, TVI_SORT );
		tree.SetItemData( hItem, TI_TRIAL );
	}
}

//************************************
// Method:    OnEEditSum
// FullName:  CLeftView::OnEEditSum
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEEditSum()
{
	Experiments exp;
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// original type
	short nOrigType = EXP_TYPE_HANDWRITING;
	if( SUCCEEDED( exp.Find( szExp ) ) ) exp.GetType( nOrigType );

	if( exp.DoEdit( szExp, this, 13 ) )
	{
		::DataHasChanged();

		exp.Find( szExp );

		exp.GetDescriptionWithID( szItem );
		tree.SetItemText( tree.GetSelectedItem(), szItem );

		short nType = EXP_TYPE_HANDWRITING;
		exp.GetType( nType );
		if( nType != nOrigType ) RefreshExperiments();
	}
}

//************************************
// Method:    OnGEditExp
// FullName:  CLeftView::OnGEditExp
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnGEditExp()
{
	Experiments exp;
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which group
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szGrp = szItem.Mid( nPos + 2 );

	// Which experiment
	HTREEITEM hExp = tree.GetParentItem( tree.GetSelectedItem() );
	hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	CString szExp;
	if( nPos != -1 ) szExp = szItem.Mid( nPos + 2 );

	// original type
	short nOrigType = EXP_TYPE_HANDWRITING;
	if( SUCCEEDED( exp.Find( szExp ) ) ) exp.GetType( nOrigType );

	if( exp.DoEdit( szExp, this, 8 ) )
	{
		::DataHasChanged();

		exp.Find( szExp );

		exp.GetDescriptionWithID( szItem );
		tree.SetItemText( hExp, szItem );

		short nType = EXP_TYPE_HANDWRITING;
		exp.GetType( nType );
		if( nType != nOrigType ) RefreshExperiments();
	}
}

//************************************
// Method:    OnUpdateGEditExp
// FullName:  CLeftView::OnUpdateGEditExp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateGEditExp( CCmdUI *pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_GROUP );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnSEditExp
// FullName:  CLeftView::OnSEditExp
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSEditExp()
{
	Experiments exp;
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Subject
	HTREEITEM hExp = tree.GetSelectedItem();
	if( !hExp ) return;
	// Subject folder
	hExp = tree.GetParentItem( hExp );
	if( !hExp ) return;
	// Group
	hExp = tree.GetParentItem( hExp );
	if( !hExp ) return;
	// Group folder
	hExp = tree.GetParentItem( hExp );
	if( !hExp ) return;
	// Experiment
	hExp = tree.GetParentItem( hExp );
	if( !hExp ) return;
	CString szItem = tree.GetItemText( hExp );
	int nPos = szItem.Find( szDelim );
	CString szExp;
	if( nPos != -1 ) szExp = szItem.Mid( nPos + 2 );

	// original type
	short nOrigType = EXP_TYPE_HANDWRITING;
	if( SUCCEEDED( exp.Find( szExp ) ) ) exp.GetType( nOrigType );

	if( exp.DoEdit( szExp, this, 8 ) )
	{
		::DataHasChanged();

		exp.Find( szExp );

		exp.GetDescriptionWithID( szItem );
		tree.SetItemText( hExp, szItem );

		short nType = EXP_TYPE_HANDWRITING;
		exp.GetType( nType );
		if( nType != nOrigType ) RefreshExperiments();
	}
}

//************************************
// Method:    OnUpdateSEditExp
// FullName:  CLeftView::OnUpdateSEditExp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSEditExp( CCmdUI *pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCEditExp
// FullName:  CLeftView::OnCEditExp
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCEditExp()
{
	Experiments exp;
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Condition
	HTREEITEM hExp = tree.GetSelectedItem();
	if( !hExp ) return;
	// Condition folder
	hExp = tree.GetParentItem( hExp );
	if( !hExp ) return;
	// Experiment
	hExp = tree.GetParentItem( hExp );
	if( !hExp ) return;
	CString szItem = tree.GetItemText( hExp );
	int nPos = szItem.Find( szDelim );
	CString szExp;
	if( nPos != -1 ) szExp = szItem.Mid( nPos + 2 );

	// original type
	short nOrigType = EXP_TYPE_HANDWRITING;
	if( SUCCEEDED( exp.Find( szExp ) ) ) exp.GetType( nOrigType );

	if( exp.DoEdit( szExp, this, 8 ) )
	{
		::DataHasChanged();

		exp.Find( szExp );

		exp.GetDescriptionWithID( szItem );
		tree.SetItemText( hExp, szItem );

		short nType = EXP_TYPE_HANDWRITING;
		exp.GetType( nType );
		if( nType != nOrigType ) RefreshExperiments();
	}
}

//************************************
// Method:    OnUpdateCEditExp
// FullName:  CLeftView::OnUpdateCEditExp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCEditExp( CCmdUI *pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent != NULL );
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    ViewExcludes
// FullName:  CLeftView::ViewExcludes
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CString szExp
// Parameter: CString szWhich
//************************************
void CLeftView::ViewExcludes( CString szExp, CString szWhich )
{
	CString szPath, szFile;
	::GetDataPathRoot( szPath );
	if( ( szPath != _T("") ) && ( szPath.GetAt( szPath.GetLength() - 1 ) == '\\') )
		szPath = szPath.Left( szPath.GetLength() - 1 );
	if( szPath == _T("") )
		szFile.Format( _T("%s\\%s-%s.SUM"), szExp, szExp, szWhich);
	else
		szFile.Format( _T("%s\\%s\\%s-%s.SUM"), szPath, szExp, szExp, szWhich );

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("There are no excluded items.") );
		return;
	}

	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

//************************************
// Method:    OnEViewExclSubj
// FullName:  CLeftView::OnEViewExclSubj
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewExclSubj()
{
	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );
	ViewExcludes( szExp, _T("who") );
}

//************************************
// Method:    OnEViewExclFeat
// FullName:  CLeftView::OnEViewExclFeat
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewExclFeat()
{
	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );
	ViewExcludes( szExp, _T("what") );
}

//************************************
// Method:    OnEViewExclQuest
// FullName:  CLeftView::OnEViewExclQuest
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewExclQuest()
{
	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );
	ViewExcludes( szExp, _T("quest") );
}

//************************************
// Method:    OnUpdateEViewExclSubj
// FullName:  CLeftView::OnUpdateEViewExclSubj
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEViewExcl( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnViewAttachments
// FullName:  CLeftView::OnViewAttachments
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnViewAttachments()
{
	CString szDelim, szItem, szExpID;
	int nPos = -1;

	CTreeCtrl& tree = GetTreeCtrl();
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which item
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !hItem ) return;
	DWORD dwData = tree.GetItemData( hItem );
	if( ( dwData != TI_ATTACHMENT ) && ( dwData != TI_ATTACHMENT_FOLDER ) ) return;

	// Which experiment
	HTREEITEM hExp = tree.GetParentItem( hItem );
	dwData = tree.GetItemData( hExp );
	if( dwData != TI_EXPERIMENT ) hExp = tree.GetParentItem( hExp );
	szItem = tree.GetItemText( hExp );
	nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	szExpID = szItem.Mid( nPos + 2 );

	// open attachments folder (create if does not exist)
	CString szRoot, szTemp;
	::GetDataPathRoot( szRoot );
	if( szRoot.GetAt( szRoot.GetLength() - 1 ) != '\\' ) szRoot += _T("\\");
	szRoot += szExpID;
	szRoot += _T("\\attachments");
	CFileFind ff;
	if( !ff.FindFile( szRoot ) ) _mkdir( szRoot );
	szTemp.Format( _T("explorer /ROOT,%s"), szRoot );
	WinExec( szTemp, SW_SHOW );
}

//************************************
// Method:    OnEDup
// FullName:  CLeftView::OnEDup
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEDup() 
{
	Experiments exp;
	if( !exp.IsValid() ) return;

	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which experiment
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	CString szData = _T("Duplicating experiment.");
	OutputAMessage( szData );

	BCGPMessageBox( _T("Experiment instructions, extended notes, and attachments will NOT be duplicated.") );
	if( exp.DoEditDup( szExp, this ) )
	{
		::DataHasChanged();

		CString szItem;
		exp.GetDescriptionWithID( szItem );

		// insert in master list
		RefreshExperiments();
		CString szID;
		exp.GetID( szID );
		HTREEITEM hItem = Find( m_hExp, szID );
		if( hItem )
		{
			tree.SelectItem( hItem );
			tree.EnsureVisible( hItem );
		}

		szData.Format( IDS_AL_EXPADDED, szItem );
		OutputAMessage( szData, TRUE );
	}
}

//************************************
// Method:    OnUpdateEDup
// FullName:  CLeftView::OnUpdateEDup
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEDup(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnCDeupMult
// FullName:  CLeftView::OnCDeupMult
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnCDupMulti()
{
	CTreeCtrl& tree = GetTreeCtrl();

	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Which Condition
	CString szItem = tree.GetItemText( tree.GetSelectedItem() );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szCond = szItem.Mid( nPos + 2 );

	// open multi dup dialog
	DupDlg d( this );
	d.m_szID = szCond;
	if( d.DoModal() == IDCANCEL ) return;

	// refresh conditions
	RefreshConditions();

	// select original condition
	HTREEITEM hItem = this->FindItem( szCond, m_hCond );
	if( hItem )
	{
		tree.SelectItem( hItem );
		tree.EnsureVisible( hItem );
	}
}

//************************************
// Method:    OnUpdateCDupMulti
// FullName:  CLeftView::OnUpdateCDupMulti
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateCDupMulti(CCmdUI* pCmdUI) 
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_CONDITION );
	HTREEITEM hParent = tree.GetParentItem( hItem );
	hParent = tree.GetParentItem( hParent );
	fEnable &= ( hParent == NULL );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnValidateExp
// FullName:  CLeftView::OnValidateExp
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnValidateExp()
{
	CString szData = _T("SRC: Experiment - Validate Experiment Settings");
	OutputAMessage( szData );

	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// VALIDATION
	CStringList lstErrors;
	CString szTmp, szCondID, szMsg;
	POSITION pos = NULL;
	BOOL fDO = TRUE;
	int nErr = 0;
	// validate experiment
	CWaitCursor crs;
	Experiments exp;
	if( SUCCEEDED( exp.Find( szExp ) ) )
	{
		if( !exp.ValidateData( &lstErrors ) )
		{
			fDO = ::GetDetailedOutput();
			::SetDetailedOutput( TRUE );
			szTmp.Format( _T("VALIDATION ERROR(s) detected for experiment %s. Detailing error(s):"), szExp );
			OutputMessage( szTmp, LOG_DB );
			pos = lstErrors.GetHeadPosition();
			while( pos )
			{
				szTmp = lstErrors.GetNext( pos );
				szMsg.Format( _T("%3d. %s"), ++nErr, szTmp );
				OutputMessage( szMsg, LOG_DB );
			}
			::SetDetailedOutput( fDO );
			szTmp.Format( _T("Experiment %s failed validation. Some settings are invalid.\nPlease check the output window for details on the error(s)."), szExp );
			BCGPMessageBox( szTmp, MB_OK | MB_ICONERROR );
			return;
		}
	}
	else BCGPMessageBox( _T("Unable to locate experiment in database."), MB_OK | MB_ICONERROR );
	// loop through conditions and validate
	CStringList lstConds;
	if( Experiments::GetListOfConditions( szExp, &lstConds ) )
	{
		POSITION pos = lstConds.GetHeadPosition();
		while( pos )
		{
			szTmp = lstConds.GetNext( pos );
			nPos = szTmp.Find( szDelim );
			szCondID = szTmp.Left( nPos );

			NSConditions cond;
			if( SUCCEEDED( cond.Find( szCondID.AllocSysString() ) ) )
			{
				if( !cond.ValidateData( &lstErrors ) )
				{
					fDO = ::GetDetailedOutput();
					::SetDetailedOutput( TRUE );
					szTmp.Format( _T("VALIDATION ERROR(s) detected for condition %s. Detailing error(s):"), szCondID );
					OutputMessage( szTmp, LOG_DB );
					pos = lstErrors.GetHeadPosition();
					while( pos )
					{
						szTmp = lstErrors.GetNext( pos );
						szMsg.Format( _T("%3d. %s"), ++nErr, szTmp );
						OutputMessage( szMsg, LOG_DB );
					}
					::SetDetailedOutput( fDO );
					szTmp.Format( _T("Condition %s failed validation. Some settings are invalid.\nPlease check the output window for details on the error(s)."), szCondID );
					BCGPMessageBox( szTmp, MB_OK | MB_ICONERROR );
					return;
				}
			}
			else
			{
				szTmp.Format( _T("Unable to locate condition %s in database."), szCondID );
				BCGPMessageBox( szTmp, MB_OK | MB_ICONERROR );
			}
		}
	}

	BCGPMessageBox( _T("The experiment and its conditions have successfully passed the validation."), MB_OK | MB_ICONEXCLAMATION );
}

//************************************
// Method:    OnUpdateValidateExp
// FullName:  CLeftView::OnUpdateValidateExp
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateValidateExp( CCmdUI* pCmdUI )
{
	GET_LV_TREE();

	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnBuildNormDB
// FullName:  CLeftView::OnBuildNormDB
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnBuildNormDB()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	CString szMsg = _T("Rebuilding the norm DB will require a complete re-summarization of the experiment.\nThe existing norm DB will be replaced. Continue?");
	if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	// summarize
	BOOL fRes = FALSE;
	_ActionState = ACTION_SUMMARIZE;
	CWaitCursor crs;
	fRes = Experiments::SummarizeExperiment( szExp, TRUE, FALSE, FALSE, FALSE, _T(""),
											 (CWnd*)(pMF->GetMsgPane()), TRUE, TRUE );
	if( !fRes ) {
		BCGPMessageBox( _T("There was an error creating the norm database: Summarize failed."), MB_OK | MB_ICONINFORMATION );
		_ActionState = ACTION_NONE;
		return;
	}

	// build norm DB
	CString szInc;
	::GetExpINC( szExp, szInc );
	CFileFind ff;
	if( !ff.FindFile( szInc ) ) {
		BCGPMessageBox( _T("There was an error creating the norm DB: INC file not created."), MB_OK | MB_ICONINFORMATION );
		_ActionState = ACTION_NONE;
		return;
	}
	fRes = AGraphDlg::BuildNormDB( szExp, szInc );
	if( !fRes ) {
		BCGPMessageBox( _T("There was an error creating the norm DB."), MB_OK | MB_ICONINFORMATION );
		_ActionState = ACTION_NONE;
		return;
	}

	_ActionState = ACTION_NONE;
	BCGPMessageBox( _T("Norm DB creation successful."), MB_OK | MB_ICONINFORMATION );
}

//************************************
// Method:    OnSBuildNormDB
// FullName:  CLeftView::OnSBuildNormDB
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSBuildNormDB()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	CString szMsg = _T("If you have already added this subject to the averages database, he/she will be added again.\nAre you sure you want to add this subject?");
	if( BCGPMessageBox( szMsg, MB_YESNO | MB_ICONQUESTION ) == IDNO ) return;

	// Which experiment
	CString szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj;
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !GetRunExpInfo( hItem, szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj ) )
	{
		OutputMessage( _T("ERROR: Run Experiment: Unable to obtain tree information for running experiment."), LOG_UI );
		return;
	}

	// get exp. settings
	BOOL fScore = FALSE, fScoreGlobal = FALSE, fNoUpdate = FALSE;
	double dTH = 0.;
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL, IID_IProcSetting, (LPVOID*)&pPS );
	if( FAILED( hr ) ) {
		szMsg = _T("Unable to create ProcSettings object.");
		OutputAMessage( szMsg, TRUE, LOG_PROC );
		BCGPMessageBox( szMsg, MB_OK | MB_ICONERROR );
		return;
	}
	pPS->Find( szExp.AllocSysString() );
	IProcSetFlags* pPSF = NULL;
	if( SUCCEEDED( pPS->QueryInterface( IID_IProcSetFlags, (LPVOID*)&pPSF ) ) && pPSF ) {
		pPSF->get_NormDBCalcScore( &fScore );
		pPSF->get_NormDBCalcScoreGlobal( &fScoreGlobal );
		pPSF->get_NormDBNoUpdate( &fNoUpdate );
		pPSF->get_NormDBThreshold( &dTH );
		pPSF->Release();
	}
	pPS->Release();

	// summarize
	BOOL fRes = FALSE;
	_ActionState = ACTION_SUMMARIZE;
	CWaitCursor crs;
	fRes = Experiments::SummarizeExperiment( szExpID, FALSE, TRUE, FALSE, FALSE, szSubjID,
											 (CWnd*)( pMF->GetMsgPane()), TRUE, TRUE, szGrpID );
	if( !fRes ) {
		BCGPMessageBox( _T("There was an error creating the norm database: Summarize failed."), MB_OK | MB_ICONINFORMATION );
		_ActionState = ACTION_NONE;
		return;
	}

	// BUILD NORM DB
	// get inc file & verify existence
	CString szInc;
	::GetExpINC( szExpID, szInc );
	CFileFind ff;
	if( !ff.FindFile( szInc ) ) {
		BCGPMessageBox( _T("There was an error creating the norm database: INC file not created."), MB_OK | MB_ICONINFORMATION );
		_ActionState = ACTION_NONE;
		return;
	}
	// copy INC to temp INC
	int nPos = szInc.ReverseFind( '.' );
	CString szINCTemp = szInc.Left( nPos );
	szINCTemp += _T("t.INC");
	::CopyFile( szInc, szINCTemp, FALSE );
	// run update
	fRes = AGraphDlg::UpdateNormDB( fScore, fScoreGlobal, fNoUpdate, dTH, szExpID, szINCTemp, TRUE );
	if( ff.FindFile( szINCTemp ) ) REMOVE_FILE( szINCTemp );
	if( !fRes ) {
		BCGPMessageBox( _T("There was an error creating the norm DB."), MB_OK | MB_ICONINFORMATION );
		_ActionState = ACTION_NONE;
		return;
	}

	_ActionState = ACTION_NONE;
	BCGPMessageBox( _T("Norm DB update completed successfully."), MB_OK | MB_ICONINFORMATION );
}

//************************************
// Method:    OnUpdateBuildNormDB
// FullName:  CLeftView::OnUpdateBuildNormDB
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateBuildNormDB( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSBuildNormDB
// FullName:  CLeftView::OnUpdateSBuildNormDB
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSBuildNormDB( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnViewNormDB
// FullName:  CLeftView::OnViewNormDB
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnViewNormDB()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	GetExpINC( szExp, szFile );
	nPos = szFile.ReverseFind( '.' );
	CString szTmp = szFile.Left( nPos );
	szTmp += _T(".XMD");
	szFile = szTmp;

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("Norm DB file does not exist.\nTo build, right-click experiment->analyze->build norm DB."), MB_OK | MB_ICONINFORMATION );
		return;
	}

	CWaitCursor crs;
	DataTabGridDlg* d = new DataTabGridDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnViewZScores
// FullName:  CLeftView::OnViewZScores
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnViewZScores() {
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	// Which experiment
	CString szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj;
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !GetRunExpInfo( hItem, szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj ) )
	{
		OutputMessage( _T("ERROR: Unable to obtain tree information for subject information."), LOG_UI );
		return;
	}

	CString szFile, szTemp;
	::GetHWR( szExpID, szGrpID, szSubjID, _T(""), _T(""), szTemp );
	int nPos = szTemp.ReverseFind( '.' );
	szFile = szTemp.Left( nPos );
	szFile += _T(".XMZ");

	CFileFind ff;
	if( !ff.FindFile( szFile ) ) {
		BCGPMessageBox( _T("Z-Score table does not exist.\nWas the norm DB built?"), MB_OK | MB_ICONINFORMATION );
		return;
	}

	CWaitCursor crs;
	DataTabGridDlg* d = new DataTabGridDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnViewZHistory
// FullName:  CLeftView::OnViewZHistory
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnViewZHistory()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	CTreeCtrl& tree = GetTreeCtrl();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	// Get experiment
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szExp = tree.GetItemText( hItem );
	int nPos = szExp.Find( szDelim );
	szExp = szExp.Mid( nPos + 2 );

	CString szFile;
	GetExpINC( szExp, szFile );
	nPos = szFile.ReverseFind( '.' );
	CString szTmp = szFile.Left( nPos );
	szTmp += _T(".XMH");
	szFile = szTmp;

	CFileFind ff;
	if( !ff.FindFile( szFile ) )
	{
		BCGPMessageBox( _T("Z-score history file does not exist.\nWas the norm DB built?"), MB_OK | MB_ICONINFORMATION );
		return;
	}

	CWaitCursor crs;
	DataTabGridDlg* d = new DataTabGridDlg( szFile, this, &(((CMainFrame*)AfxGetApp()->m_pMainWnd)->m_wm) );
}

//************************************
// Method:    OnEViewZHistorySummary
// FullName:  CLeftView::OnEViewZHistorySummary
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnEViewZHistorySummary()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	// Which experiment
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	CString szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );
	CString szItem = tree.GetItemText( hItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	CString szExp = szItem.Mid( nPos + 2 );

	ZResultsDlg d( szExp, _T(""), AfxGetApp()->m_pMainWnd );
	d.m_fWideDateRange = TRUE;
	d.DoModal();
}

//************************************
// Method:    OnSViewZHistorySummary
// FullName:  CLeftView::OnSViewZHistorySummary
// Access:    public 
// Returns:   void
// Qualifier:
//************************************
void CLeftView::OnSViewZHistorySummary()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( !pMF ) return;

	if( !::IsOA() ) {
		pMF->OnHelpActivate();
		return;
	}

	// Which experiment
	CString szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj;
	CTreeCtrl& tree = GetTreeCtrl();
	HTREEITEM hItem = tree.GetSelectedItem();
	if( !GetRunExpInfo( hItem, szExpID, szExp, szGrpID, szGrp, szSubjID, szSubj ) )
	{
		OutputMessage( _T("ERROR: Unable to obtain tree information for subject information."), LOG_UI );
		return;
	}

	ZResultsDlg d( szExpID, szSubjID, AfxGetApp()->m_pMainWnd );
	d.m_fWideDateRange = TRUE;
	d.DoModal();
}

//************************************
// Method:    OnUpdateEViewZHistorySummary
// FullName:  CLeftView::OnUpdateEViewZHistorySummary
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateEViewZHistorySummary( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_EXPERIMENT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

//************************************
// Method:    OnUpdateSViewZHistorySummary
// FullName:  CLeftView::OnUpdateSViewZHistorySummary
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: CCmdUI * pCmdUI
//************************************
void CLeftView::OnUpdateSViewZHistorySummary( CCmdUI* pCmdUI )
{
	GET_LV_TREE();
	BOOL fEnable = ( dwType == TI_SUBJECT );
	IS_EXP_RUNNING();
	fEnable &= fRunExp;
	fEnable &= !::IsClientServerModeOn();
	pCmdUI->Enable( fEnable );
}

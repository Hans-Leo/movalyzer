// WizardImportEnd.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportEnd.h"
#include <direct.h>

#include "WizardImportSheet.h"
#include "..\DataMod\DataMod.h"
#include "WizardImportPath.h"
#include "WizardImportExp.h"
#include "WizardImportCond.h"
#include "WizardImportGrp.h"
#include "WizardImportSubj.h"
#include "WizardImportFormat.h"

#include "MainFrm.h"
#include "LeftView.h"

#include "Subjects.h"
#include "Groups.h"
#include "Experiments.h"
#include "Conditions.h"
#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\CxImage\CxImage\ximage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

WizardImportExp* _pExp = NULL;
WizardImportCond* _pCond = NULL;
WizardImportGrp* _pGrp = NULL;
WizardImportSubj* _pSubj = NULL;

/////////////////////////////////////////////////////////////////////////////
// WizardImportEnd property page

IMPLEMENT_DYNCREATE(WizardImportEnd, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportEnd, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_CONTINUE, OnChkContinue)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportEnd::WizardImportEnd() : CBCGPPropertyPage(WizardImportEnd::IDD)
{
	m_szPath = _T("");
	m_szExpID = _T("");
	m_szExpDesc = _T("");
	m_szGrpID = _T("");
	m_szGrpDesc = _T("");
	m_szSubjID = _T("");
	m_szSubjName = _T("");
	m_szFormat = _T("");
	m_fCont = FALSE;
	m_szCols = _T("");
	m_fRetain = FALSE;
}

WizardImportEnd::~WizardImportEnd()
{
}

void WizardImportEnd::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_PATH, m_szPath);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szExpID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szExpDesc);
	DDX_Text(pDX, IDC_TXT_CONDID, m_szCondID);
	DDX_Text(pDX, IDC_TXT_CONDDESC, m_szCondDesc);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szGrpID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szGrpDesc);
	DDX_Text(pDX, IDC_TXT_SUBJID, m_szSubjID);
	DDX_Text(pDX, IDC_TXT_SUBJNAME, m_szSubjName);
	DDX_Text(pDX, IDC_TXT_FORMAT, m_szFormat);
	DDX_Check(pDX, IDC_CHK_CONTINUE, m_fCont);
	DDX_Text(pDX, IDC_TXT_COLS, m_szCols);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportEnd message handlers

BOOL WizardImportEnd::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CHK_CONTINUE );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check if completed. Uncheck to continue.") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportEnd::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT WizardImportEnd::OnWizardNext() 
{
	DoNext();

	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	if( pParent ) pParent->SetActivePage( 2 );

	return -1;
}

BOOL WizardImportEnd::OnWizardFinish() 
{
	DoNext();

	return CBCGPPropertyPage::OnWizardFinish();
}

void WizardImportEnd::DoNext()
{
	// inform user
	CString szPath;
	szPath.Format( _T("Beginning import of file %s using %s driver."),
				   m_szPath, m_szFormat );
	OutputAMessage( szPath );

	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportPath* pPath = (WizardImportPath*)pParent->GetPage( 2 );
	LONG lVal = 0L;

	// set lastfile
	pParent->m_szLastFile = m_szPath;

	// If our own...
	if( m_szFormat == _T("MOVALYZER") )
	{
		DoOwn();
		return;
	}
	// else if gripper
	else if( m_szFormat == _T("GRIPPER") )
	{
		DoGripper();
		return;
	}
	// else if single trial/image
	else if( m_szFormat == _T("SINGLE TRIAL OR IMAGE") )
	{
		DoSingle();
		return;
	}

	// confirm the creation of directory/file
	if( !ConfirmCreate() ) return;

	// application path
	::GetAppPath( szPath );

	// open appropriate driver
	char szObj[ 500 ];
	strcpy_s( szObj, 500, szPath );
	strcat_s( szObj, 500, m_szFormat );
	strcat_s( szObj, 500, ".DRV" );
	HINSTANCE hMod = LoadLibrary( szObj );
	if( hMod < (HINSTANCE)HINSTANCE_ERROR )
	{
		szPath.Format( _T("Unable to load %s driver."), m_szFormat );
		BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
		return;
	}

	// get proc address to import procedure
	typedef LONG (CDECL* LPFNIMPORT) (HWND, LPSTR, LPSTR, int);
	LPFNIMPORT proc; 
	proc = (LPFNIMPORT)GetProcAddress( hMod, "Import" );
	if( !proc ) 
	{
		szPath.Format( _T("Error in %s driver."), m_szFormat );
		BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
		FreeLibrary( hMod );
		return;
	}

	// IF CSV.....
	if( m_szFormat == _T("CSV") )
	{
		// get proc addresses for setcolumncount, setcolumn, setskiplines, sethasheaders, and setdelimiter
		typedef LONG (CDECL* LPFNSETCOUNT) (int);
		LPFNSETCOUNT procCnt = (LPFNSETCOUNT)GetProcAddress( hMod, "SetColumnCount" );
		typedef LONG (CDECL* LPFNSETCOL) (int, int);
		LPFNSETCOL procCol = (LPFNSETCOL)GetProcAddress( hMod, "SetColumn" );
		typedef LONG (CDECL* LPFNSETSKIP) (int);
		LPFNSETSKIP procSkip = (LPFNSETSKIP)GetProcAddress( hMod, "SetSkipLines" );
		typedef LONG (CDECL* LPFNSETHDRS) (bool);
		LPFNSETHDRS procHdrs = (LPFNSETHDRS)GetProcAddress( hMod, "SetHasHeaders" );
		typedef LONG (CDECL* LPFNSETDELIM) (char);
		LPFNSETDELIM procDelim = (LPFNSETDELIM)GetProcAddress( hMod, "SetDelimiter" );

		if( !procCnt || !procCol || !procSkip || !procHdrs || !procDelim ) 
		{
			szPath.Format( _T("Error in %s driver."), m_szFormat );
			BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
			FreeLibrary( hMod );
			return;
		}

		// set column count
		lVal = (*procCnt)( pPath->m_dlgColSel.m_lstColNum.GetCount() );

		// set which columns & what order
		CString szItem;
		int nItem, nPos = 0;
		POSITION pos = pPath->m_dlgColSel.m_lstColNum.GetHeadPosition();
		while( pos )
		{
			szItem = pPath->m_dlgColSel.m_lstColNum.GetNext( pos );
			nItem = atoi( szItem );
			lVal = (*procCol)( nPos, nItem );

			nPos++;
		}

		// set # lines to skip
		lVal = (*procSkip)( pPath->m_dlgColSel.m_nSkip );

		// set if has headers
		lVal = (*procHdrs)( pPath->m_fHeaders ? 1 : 0 );

		// set delimiter (TODO)
		lVal = (*procDelim)( pPath->m_szDelim.GetAt( 0 ) );
	}

	// root data path
	::GetDataPathRoot( szPath );

	int nTrial = 1;
	CString szTrial, szDest, szExt;
	BOOL fFound = FALSE;

	// type of trial
	szExt = _T("HWR");

	// determine next trial #
	CFileFind ff;
	while( !fFound )
	{
		if( nTrial > 99 )
		{
			BCGPMessageBox( _T("Maximum number of trials (99) reached. Cannot continue.") );
			return;
		}

		if( nTrial < 10 ) szTrial.Format( _T("0%d"), nTrial );
		else szTrial.Format( _T("%d"), nTrial );
		szDest.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.%s"), szPath, m_szExpID,
					   m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
					   m_szCondID, szTrial, szExt );
		if( !ff.FindFile( szDest ) ) fFound = TRUE;
		else nTrial++;
	}

	// get flags/settings related to multiple trials to be imported
	// assuming file name last two characters (before extension) are trial #
	// we start with selected file and continue with nCount sequentially
	// if the fRemainder flag is set
	CString szSourceExt = pPath->m_szExt;
	BOOL fRemainder = pPath->m_fRemainder;
	int nCount = pPath->m_nCount;
	if( nCount <= 0 ) fRemainder = FALSE;

	// if multiple, determine the base file name
	CString szFile, szBase;
	int nStartTrial = 0, nPos = -1;
	if( fRemainder && ( nCount > 0 ) )
	{
		nPos = m_szPath.ReverseFind( '.' );
		if( nPos == -1 )
		{
			BCGPMessageBox( _T("Error in importation. Inappropriate file name.") );
			FreeLibrary( hMod );
			return;
		}
		szBase = m_szPath.Left( nPos - 2 );
		nStartTrial = atoi( m_szPath.Mid( nPos - 2, 2 ) );
	}

	// loop through (if necessary - if lVal > 0) and import file
	int nTrialNum = 0;
	CString szItem;
	CWaitCursor crs;
	lVal = (*proc)( m_hWnd, (char*)((LPCTSTR)m_szPath),(char*)((LPCTSTR)szDest), nTrialNum );
	if( !fRemainder && ( lVal == 0 ) ) nTrialNum++;
	while( fRemainder && ( lVal == 0 ) )
	{
		nTrialNum++;
		nTrial = 1;
		fFound = FALSE;

		// have we done as many as requested?
		if( nTrialNum > nCount ) break;

		while( !fFound )
		{
			if( nTrial > 99 )
			{
				BCGPMessageBox( _T("Maximum number of trials (99) reached. Cannot continue.") );
				return;
			}

			if( nTrial < 10 ) szTrial.Format( _T("0%d"), nTrial );
			else szTrial.Format( _T("%d"), nTrial );
			szDest.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.%s"), szPath, m_szExpID,
						m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
						m_szCondID, szTrial, szExt );
			if( !ff.FindFile( szDest ) ) fFound = TRUE;
			else nTrial++;
		}

		// next source file to be used
		szFile = szBase;
		szItem.Format( ( ( nStartTrial + nTrialNum ) >= 10 ) ? _T("%d") : _T("0%d"),
						 nStartTrial + nTrialNum );
		szFile += szItem;
		szFile += _T(".");
		szFile += szSourceExt;

		crs.Restore();
		lVal = (*proc)( m_hWnd, (char*)((LPCTSTR)szFile),(char*)((LPCTSTR)szDest), nTrialNum );
	}

	// free the library
	FreeLibrary( hMod );

	// inform user & refresh display (tree) if successful
	if( lVal == 0L )
	{
		szPath.Format( _T("Import complete. %d trials imported."), nTrialNum );
		BCGPMessageBox( szPath );
		if( m_szFormat != _T("MOVALYZER") )
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
			if( pLV ) pLV->Refresh();
		}
	}
	else BCGPMessageBox( _T("Error in import.") );
}

void WizardImportEnd::DoGripper()
{
	// confirm creation of directory/file
	if( !ConfirmCreate() ) return;

	// get application path
	CString szPath;
	::GetAppPath( szPath );

	// open appropriate driver
	char szObj[ 500 ];
	strcpy_s( szObj, 500, szPath );
	strcat_s( szObj, 500, m_szFormat );
	strcat_s( szObj, 500, ".DRV" );
	HINSTANCE hMod = LoadLibrary( szObj );
	if( hMod < (HINSTANCE)HINSTANCE_ERROR )
	{
		szPath.Format( _T("Unable to load %s driver."), m_szFormat );
		BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
		return;
	}

	// get proc address for import
	typedef LONG (CDECL* LPFNIMPORT) (HWND, LPSTR, LPSTR, int);
	LPFNIMPORT proc; 
	proc = (LPFNIMPORT)GetProcAddress( hMod, "Import" );
	if( !proc ) 
	{
		szPath.Format( _T("Error in %s driver."), m_szFormat );
		BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
		FreeLibrary( hMod );
		return;
	}

	// root data path
	::GetDataPathRoot( szPath );

	int nTrial = 1;
	CString szTrial, szDest, szExt;
	BOOL fFound = FALSE;

	// this is a gripper experiment
	szExt = _T("FRD");

	// determine next trial #
	CFileFind ff;
	while( !fFound )
	{
		if( nTrial > 99 )
		{
			BCGPMessageBox( _T("Maximum number of trials (99) reached. Cannot continue.") );
			return;
		}

		if( nTrial < 10 ) szTrial.Format( _T("0%d"), nTrial );
		else szTrial.Format( _T("%d"), nTrial );
		szDest.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.%s"), szPath, m_szExpID,
					   m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
					   m_szCondID, szTrial, szExt );
		if( !ff.FindFile( szDest ) ) fFound = TRUE;
		else nTrial++;
	}

	// get flags/settings related to multiple trials to be imported
	// assuming file name last two characters (before extension) are trial #
	// we start with selected file and continue with nCount sequentially
	// if the fRemainder flag is set
	LONG lVal = 0L;
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportPath* pPath = (WizardImportPath*)pParent->GetPage( 2 );
	CString szSourceExt = pPath->m_szExt;
	BOOL fRemainder = pPath->m_fRemainder;
	int nCount = pPath->m_nCount;
	if( nCount <= 0 ) fRemainder = FALSE;

	// get proc addresses for setcolumncount and setcolumn
	// this tells the driver how many columns to import and which and what order
	typedef LONG (CDECL* LPFNSETCOUNT) (int);
	LPFNSETCOUNT procCnt; 
	procCnt = (LPFNSETCOUNT)GetProcAddress( hMod, "SetColumnCount" );
	typedef LONG (CDECL* LPFNSETCOL) (int, int);
	LPFNSETCOL procCol; 
	procCol = (LPFNSETCOL)GetProcAddress( hMod, "SetColumn" );
	if( !procCnt || !procCol ) 
	{
		szPath.Format( _T("Error in %s driver."), m_szFormat );
		BCGPMessageBox( szPath, MB_ICONEXCLAMATION );
		FreeLibrary( hMod );
		return;
	}

	// set column count
	lVal = (*procCnt)( pPath->m_dlgColSel.m_lstColNum.GetCount() );

	// set which columns & what order
	CString szItem;
	int nItem, nPos = 0;
	POSITION pos = pPath->m_dlgColSel.m_lstColNum.GetHeadPosition();
	while( pos )
	{
		szItem = pPath->m_dlgColSel.m_lstColNum.GetNext( pos );
		nItem = atoi( szItem );
		lVal = (*procCol)( nPos, nItem );

		nPos++;
	}

	// if multiple, determine the base file name
	CString szFile, szBase;
	int nStartTrial = 0;
	if( fRemainder && ( nCount > 0 ) )
	{
		nPos = m_szPath.ReverseFind( '.' );
		if( nPos == -1 )
		{
			BCGPMessageBox( _T("Error in importation. Inappropriate file name.") );
			FreeLibrary( hMod );
			return;
		}
		szBase = m_szPath.Left( nPos - 2 );
		nStartTrial = atoi( m_szPath.Mid( nPos - 2, 2 ) );
	}

	int nTrialNum = 0;
	lVal = (*proc)( m_hWnd, (char*)((LPCTSTR)m_szPath),(char*)((LPCTSTR)szDest), 0 );
	while( fRemainder && ( lVal == 0 ) )
	{
		nTrialNum++;
		nTrial = 1;
		fFound = FALSE;

		// have we done as many as requested?
		if( nTrialNum > nCount ) break;

		// get next new file name to be used
		while( !fFound )
		{
			if( nTrial > 99 )
			{
				BCGPMessageBox( _T("Maximum number of trials (99) reached. Cannot continue.") );
				return;
			}

			if( nTrial < 10 ) szTrial.Format( _T("0%d"), nTrial );
			else szTrial.Format( _T("%d"), nTrial );
			szDest.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%s.%s"), szPath, m_szExpID,
						m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
						m_szCondID, szTrial, szExt );
			if( !ff.FindFile( szDest ) ) fFound = TRUE;
			else nTrial++;
		}

		// next source file to be used
		szFile = szBase;
		szItem.Format( ( ( nStartTrial + nTrialNum ) >= 10 ) ? _T("%d") : _T("0%d"),
					   nStartTrial + nTrialNum );
		szFile += szItem;
		szFile += _T(".");
		szFile += szSourceExt;

		lVal = (*proc)( m_hWnd, (char*)((LPCTSTR)szFile),(char*)((LPCTSTR)szDest), nTrialNum );
	}

	// free library
	FreeLibrary( hMod );

	// inform user & refresh display (tree) if successful
	if( lVal == 0L )
	{
		szPath.Format( _T("Import complete. %d trials imported."), nTrialNum );
		BCGPMessageBox( szPath );
		if( m_szFormat != _T("MOVALYZER") )
		{
			CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
			CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
			if( pLV ) pLV->Refresh();
		}
	}
	else BCGPMessageBox( _T("Error in import.") );
}

void WizardImportEnd::DoOwn()
{
	CFileFind ff;
	BOOL fCont = TRUE, fRefresh = FALSE;
	int nCnt = 0;
	CString szFile, szPath, szTemp, szNum, szCond;

	// Data root path
	::GetDataPathRoot( szPath );
	if( szPath != _T("") ) szPath += _T("\\");

	// Loop through HWR files in specified location
	szFile.Format( _T("%s\\*.HWR"), m_szPath );
	if( ff.FindFile( szFile ) )
	{
		if( !ConfirmCreate() ) return;

		// Add data files
		OutputAMessage( _T("Searching for trials to import..."), TRUE );
		CWaitCursor crs;
		HRESULT hr;

		while( fCont )
		{
			nCnt++;
			fCont = ff.FindNextFile();
			szFile = ff.GetFilePath();
			if( !ff.IsDirectory() )
			{
				fRefresh = FALSE;
				if( m_fRetain )
				{
					// Extract condition + trial
					szNum = szFile.Mid( szFile.GetLength() - 9 );
					szCond = szFile.Mid( szFile.GetLength() - 9, 3 );
					// Rename file maintaining original condition
					szTemp.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s"),
								   szPath, m_szExpID, m_szGrpID, m_szSubjID,
								   m_szExpID, m_szGrpID, m_szSubjID, szNum );

					// ...Condition
					NSConditions cond;
					if( !cond.IsValid() ) return;
					hr = cond.Find( szCond );
					if( FAILED( hr ) )
					{
						cond.PutID( szCond );
						cond.PutDescription( m_szCondDesc );
						cond.PutLex( _pCond->m_szLex );
						cond.PutInstruction( _pCond->m_szInstr );
						cond.PutNotes( _pCond->m_szNotes );
						cond.PutRangeDirection( _pCond->m_dRangeDirection );
						cond.PutRangeLength( _pCond->m_dRangeLength );
						cond.PutStrokeDirection( _pCond->m_dStrokeDirection );
						cond.PutStrokeLength( _pCond->m_dStrokeLength );
						cond.PutStrokeMax( _pCond->m_nMax );
						cond.PutStrokeMin( _pCond->m_nMin );
						cond.PutStrokeSkip( _pCond->m_nSkip );
						cond.PutUseStimulus( _pCond->m_fStimulus );
						cond.PutStimulus( _pCond->m_szStimulus );
						if( FAILED( cond.Add() ) ) return;

						::DataHasChanged();
						fRefresh = TRUE;

						szTemp.Format( _T("Added a new condition %s."), m_szCondID );
						OutputAMessage( szTemp );
					}
					// ...ExperimentCondition
					IExperimentCondition* pEC = NULL;
					hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
										   IID_IExperimentCondition, (LPVOID*)&pEC );
					if( FAILED( hr ) )
					{
						BCGPMessageBox( IDS_EXPCOND_ERR );
						return;
					}
					hr = pEC->Find( m_szExpID.AllocSysString(), szCond.AllocSysString() );
					if( FAILED( hr ) )
					{
						pEC->put_ExperimentID( m_szExpID.AllocSysString() );
						pEC->put_ConditionID( szCond.AllocSysString() );
						pEC->put_Replications( 1 );
						hr = pEC->Add();
						if( FAILED( hr ) )
						{
							pEC->Release();
							CString szTempMsg;
							szTempMsg.LoadString(IDS_ADD_ERR);
							BCGPMessageBox( szTempMsg+_T(" WizardImportEnd::DoOwn") );
							return;
						}
						::DataHasChanged();
						fRefresh = TRUE;

						szTemp.Format( _T("Added a new condition %s to experiment %s with 1 replication."),
									   m_szCondID, m_szExpID );
						OutputAMessage( szTemp );
					}
					pEC->Release();

					if( fRefresh )
					{
						CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
						CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
						if( pLV ) pLV->Refresh();
					}
				}
				else
				{
					// Rename file with selected condition
					szTemp.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s"),
								   szPath, m_szExpID, m_szGrpID, m_szSubjID,
								   m_szExpID, m_szGrpID, m_szSubjID, m_szCondID );
					if( nCnt < 10 ) szNum.Format( _T("0%d.HWR"), nCnt );
					else szNum.Format( _T("%d.HWR"), nCnt );
					szTemp += szNum;
				}

				// Copy data file
				CopyFile( szFile, szTemp, FALSE );
			}
		}

		// Refresh screen
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
		if( pLV ) pLV->Refresh();
	}
}

void WizardImportEnd::DoSingle()
{
	// file extension
	int nPos = m_szPath.ReverseFind( '.' );
	if( nPos == -1 ) return;
	CString szExt = m_szPath.Mid( nPos + 1 );
	szExt.MakeUpper();

	// any of these images?
	BOOL fBMP = ( szExt == _T("BMP") );
	BOOL fPNG = ( szExt == _T("PNG") );
	BOOL fGIF = ( szExt == _T("GIF") );
	BOOL fJPG = ( szExt == _T("JPG") );
	BOOL fPCX = ( szExt == _T("PCX") );
	// proper extension for searching
	CString szExtSearch = szExt;
	if( fBMP || fPNG || fGIF || fJPG ) szExtSearch = _T("PCX");

	// NAME THE FILE ACCORDINGLY
	// search for the highest trial #
	CString szFileNameMask;
	szFileNameMask.Format( _T("%s%s%s%s*.%s"), m_szExpID, m_szGrpID, m_szSubjID, m_szCondID, szExtSearch );
	CString szPath, szFileName;
	::GetDataPathRoot( szPath );
	szFileName.Format( _T("%s\\%s\\%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID, m_szSubjID, szFileNameMask );
	int nTrial = 1;
	CFileFind ff;
	if( ff.FindFile( szFileName ) )
	{
		CStringList lstFiles;
		BOOL fCont = TRUE;
		while( fCont )
		{
			fCont = ff.FindNextFile();
			szFileName = ff.GetFilePath();
			lstFiles.AddTail( szFileName );
		}
		::SortStringListAlpha( &lstFiles );
		szFileName = lstFiles.GetTail();
		szFileName = szFileName.Left( szFileName.GetLength() - 4 );
		int nThisTrial = atoi( szFileName.Right( 2 ) );
		nThisTrial++;
		if( nThisTrial > nTrial ) nTrial = nThisTrial;
	}
	// create new filename with path
	if( nTrial >= 10 )
		szFileName.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s%d.%s"), szPath, m_szExpID,
						   m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
						   m_szCondID, nTrial, m_szPath.Right( 3 ) );
	else
		szFileName.Format( _T("%s\\%s\\%s\\%s\\%s%s%s%s0%d.%s"), szPath, m_szExpID,
						   m_szGrpID, m_szSubjID, m_szExpID, m_szGrpID, m_szSubjID,
						   m_szCondID, nTrial, m_szPath.Right( 3 ) );

	// COPY FILE
	if( !ConfirmCreate() ) return;
	CopyFile( m_szPath, szFileName, FALSE );

	// if image file, we need to check if it's a PCX...if not, then we have to convert to PCX
	if( fBMP || fPNG || fGIF || fJPG )
	{
		DWORD dwType = CXIMAGE_FORMAT_PCX;
		if( fBMP ) dwType = CXIMAGE_FORMAT_BMP;
		else if( fPNG ) dwType = CXIMAGE_FORMAT_PNG;
		else if( fGIF ) dwType = CXIMAGE_FORMAT_GIF;
		else if( fJPG ) dwType = CXIMAGE_FORMAT_JPG;

		CxImage img( szFileName, dwType );
		CString szPCX = szFileName.Left( szFileName.GetLength() - 3 );
		szPCX += _T("PCX");
		img.SetXDPI( 600 );
		img.SetYDPI( 600 );
		img.GrayScale();
		img.Save( szPCX, CXIMAGE_FORMAT_PCX );

		REMOVE_FILE( szFileName );
	}

	// Add notes for this trial
	CString szNotesFile = szFileName.Left( szFileName.GetLength() - 3 );
	szNotesFile += _T("TXT");
	if( ff.FindFile( szNotesFile ) ) REMOVE_FILE( szNotesFile );
	CStdioFile f;
	if( f.Open( szNotesFile, CFile::modeWrite | CFile::modeCreate ) )
	{
		CString szDateTime = (COleDateTime::GetCurrentTime()).Format( _T("%Y-%m-%d %H:%M") );
		ULONGLONG nFileLength = 0LL;
		if( ff.FindFile( m_szPath ) )
		{
			ff.FindNextFile();
			nFileLength = ff.GetLength();
		}

		CString szNotes;
		szNotes.Format( _T("%s %I64d %s"), szDateTime, nFileLength, m_szPath );
		f.WriteString( szNotes );
		f.WriteString( _T("\r\n") );
	}
	else BCGPMessageBox( _T("Unable to write notes file.") );

	// INFORM USER (if not continuing)
	if( m_fCont ) BCGPMessageBox( _T("Import complete. You will need to refresh the subject trials.") );
}

BOOL WizardImportEnd::ConfirmCreate()
{
	CFileFind ff, ff2;
	BOOL fCont = TRUE, fRefresh = FALSE;
	int nCnt = 0;
	CString szFile, szPath, szTemp, szNum;

	// Data root path
	::GetDataPathRoot( szPath );
	if( szPath != _T("") ) szPath += _T("\\");

	// Ensure directories exists
	szTemp.Format( _T("%s\\%s"), szPath, m_szExpID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	szTemp.Format( _T("%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}
	szTemp.Format( _T("%s\\%s\\%s\\%s"), szPath, m_szExpID, m_szGrpID, m_szSubjID );
	if( !ff2.FindFile( szTemp ) )
	{
		if( _mkdir( szTemp ) != 0 )
		{
			BCGPMessageBox( _T("Error in writing files. Unable to create directory.") );
			return FALSE;
		}
	}

	// confirm experiment type with file type
	if( ( m_szFormat == _T("GRIPPER") ) &&
		( _pExp->m_nExpType != EXP_TYPE_GRIPPER ) )
		BCGPMessageBox( _T("WARNING: You are importing grip-force data into a non-grip-force experiment. The trials will not be visible until the experiment type is changed to grip-force.") );
	else if( ( m_szFormat != _T("GRIPPER") ) &&
			 ( _pExp->m_nExpType == EXP_TYPE_GRIPPER ) )
		BCGPMessageBox( _T("WARNING: You are importing handwriting data into a non-handwriting experiment. The trials will not be visible until the experiment type is changed to handwriting.") );

	// Create new/add exp, cond, grp, subjects where necessary
	HRESULT hr;
	// ...Experiment
	if( _pExp->m_fNew )
	{
		Experiments exp;
		if( !exp.IsValid() ) return FALSE;

		exp.PutID( m_szExpID );
		exp.PutDescription( m_szExpDesc );
		hr = exp.Add();
		if( FAILED( exp.Add() ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new experiment %s."), m_szExpID );
		OutputAMessage( szTemp );
	}
	if( !m_fRetain )
	{
		// ...Condition
		NSConditions cond;
		if( !cond.IsValid() ) return FALSE;
		hr = cond.Find( m_szCondID );
		if( FAILED( hr ) )
		{
			cond.PutID( m_szCondID );
			cond.PutDescription( m_szCondDesc );
			cond.PutLex( _pCond->m_szLex );
			cond.PutInstruction( _pCond->m_szInstr );
			cond.PutNotes( _pCond->m_szNotes );
			cond.PutRangeDirection( _pCond->m_dRangeDirection );
			cond.PutRangeLength( _pCond->m_dRangeLength );
			cond.PutStrokeDirection( _pCond->m_dStrokeDirection );
			cond.PutStrokeLength( _pCond->m_dStrokeLength );
			cond.PutStrokeMax( _pCond->m_nMax );
			cond.PutStrokeMin( _pCond->m_nMin );
			cond.PutStrokeSkip( _pCond->m_nSkip );
			cond.PutUseStimulus( _pCond->m_fStimulus );
			cond.PutStimulus( _pCond->m_szStimulus );
			if( FAILED( cond.Add() ) ) return FALSE;

			::DataHasChanged();
			fRefresh = TRUE;

			szTemp.Format( _T("Added a new condition %s."), m_szCondID );
			OutputAMessage( szTemp );
		}
		// ...ExperimentCondition
		IExperimentCondition* pEC = NULL;
		hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
							   IID_IExperimentCondition, (LPVOID*)&pEC );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_EXPCOND_ERR );
			return FALSE;
		}
		hr = pEC->Find( m_szExpID.AllocSysString(), m_szCondID.AllocSysString() );
		if( FAILED( hr ) )
		{
			pEC->put_ExperimentID( m_szExpID.AllocSysString() );
			pEC->put_ConditionID( m_szCondID.AllocSysString() );
			pEC->put_Replications( 1 );
			hr = pEC->Add();
			if( FAILED( hr ) )
			{
				pEC->Release();
				CString szTempMsg;
				szTempMsg.LoadString(IDS_ADD_ERR);
				BCGPMessageBox( szTempMsg+_T(" WizardImportEnd::ConfirmCreate:1") );
				return FALSE;
			}
			::DataHasChanged();
			fRefresh = TRUE;
			szTemp.Format( _T("Added a new condition %s to experiment %s with 1 replication."),
						   m_szCondID, m_szExpID );
			OutputAMessage( szTemp );
		}
		pEC->Release();
	}
	// ...Group
	Groups grp;
	if( !grp.IsValid() ) return FALSE;
	hr = grp.Find( m_szGrpID );
	if( FAILED( hr ) )
	{
		grp.PutID( m_szGrpID );
		grp.PutDescription( _pGrp->m_szDesc );
		grp.PutNotes( _pGrp->m_szNotes );

		hr = grp.Add();
		if( FAILED( hr ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new group %s."), m_szGrpID );
		OutputAMessage( szTemp );
	}
	// ...Subject
	Subjects subj;
	if( !subj.IsValid() ) return FALSE;
	hr = subj.Find( m_szSubjID );
	if( FAILED( hr ) )
	{
		subj.PutCode( _pSubj->m_szCode );
		subj.PutID( m_szSubjID );
		subj.PutNameLast( _pSubj->m_szLast );
		subj.PutNameFirst( _pSubj->m_szFirst );
		subj.PutNotes( _pSubj->m_szNotes );
		subj.PutDateAdded( COleDateTime::GetCurrentTime() );

		hr = subj.Add();
		if( FAILED( hr ) ) return FALSE;

		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new subject %s."), m_szSubjID );
		OutputAMessage( szTemp );
	}
	// ...ExperimentMember
	IExperimentMember* pEM = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return FALSE;
	}
	hr = pEM->Find( m_szExpID.AllocSysString(), m_szGrpID.AllocSysString(),
					m_szSubjID.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->put_ExperimentID( m_szExpID.AllocSysString() );
		pEM->put_GroupID( m_szGrpID.AllocSysString() );
		pEM->put_SubjectID( m_szSubjID.AllocSysString() );
		hr = pEM->Add();
		if( FAILED( hr ) )
		{
			pEM->Release();
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" WizardImportEnd::ConfirmCreate:2") );
			return FALSE;
		}
		::DataHasChanged();
		fRefresh = TRUE;
		szTemp.Format( _T("Added a new group %s and subject %s to experiment %s."),
					   m_szGrpID, m_szSubjID, m_szExpID );
		OutputAMessage( szTemp );
	}
	pEM->Release();

	if( fRefresh )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
		if( pLV ) pLV->Refresh();
	}

	return TRUE;
}

void WizardImportEnd::OnChkContinue() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );

// 	CWnd* pWnd = pParent->GetDlgItem( ID_WIZNEXT );
// 	if( pWnd ) pWnd->SetWindowText( _T("Finish") );
// 	pWnd = pParent->GetDlgItem( ID_WIZBACK );
// 	if( pWnd ) pWnd->ShowWindow( SW_HIDE );

	CWnd* pWnd = GetDlgItem( IDC_CHK_CONTINUE );
	if( pWnd ) pWnd->EnableWindow( FALSE );

	UpdateData( TRUE );
}

BOOL WizardImportEnd::OnSetActive() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportFormat* pFmt = (WizardImportFormat*)pParent->GetPage( 1 );
	WizardImportPath* pPath = (WizardImportPath*)pParent->GetPage( 2 );
	_pExp = (WizardImportExp*)pParent->GetPage( 3 );
	_pCond = (WizardImportCond*)pParent->GetPage( 4 );
	_pGrp = (WizardImportGrp*)pParent->GetPage( 5 );
	_pSubj = (WizardImportSubj*)pParent->GetPage( 6 );

	m_szPath = pPath->m_szPath;
	m_szExpID = _pExp->m_szID;
	m_szExpDesc = _pExp->m_szDesc;
	m_szCondID = _pCond->m_szID;
	m_szCondDesc = _pCond->m_szDesc;
	m_fRetain = _pCond->m_fRetain;
	m_szGrpID = _pGrp->m_szID;
	m_szGrpDesc = _pGrp->m_szDesc;
	m_szSubjID = _pSubj->m_szID;
	if( ::IsPrivacyOn() )
		m_szSubjName.Format( _T("%s %s (%s)"), PVT_NAME_LAST, PVT_NAME_FIRST, _pSubj->m_szCode );
	else
		m_szSubjName.Format( _T("%s %s (%s)"), _pSubj->m_szFirst, _pSubj->m_szLast, _pSubj->m_szCode );
	m_szFormat = pFmt->m_szFormat;

	if( m_szFormat != _T("GRIPPER") )
	{
		CWnd* pWnd = GetDlgItem( IDC_TXT_COL );
		if( pWnd ) pWnd->ShowWindow( FALSE );
		pWnd = GetDlgItem( IDC_TXT_COLS );
		if( pWnd ) pWnd->ShowWindow( FALSE );
	}
	else
	{
		m_szCols = _T("");
		POSITION pos = pPath->m_dlgColSel.m_lstColName.GetHeadPosition();
		while( pos )
		{
			m_szCols += pPath->m_dlgColSel.m_lstColName.GetNext( pos );
			if( pos ) m_szCols += _T("; ");
		}
	}

	UpdateData( FALSE );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardImportEnd::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportEnd::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

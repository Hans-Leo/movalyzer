#pragma once

// MsgView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMsgView form view

class CMsgView : public CListView
{
	DECLARE_DYNCREATE(CMsgView)
protected: // create from serialization only
	CMsgView();
public:
	virtual ~CMsgView();

// Attributes
public:

// Operations
public:
	void ClearMessages();

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
protected:

// message map
protected:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnContextMenu( CWnd* pWnd, CPoint point );
	afx_msg void OnEditCopy();
	afx_msg void OnSelectAll();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};

#pragma once

// WizardImportGrp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportGrp dialog

class WizardImportGrp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportGrp)

// Construction
public:
	WizardImportGrp();
	~WizardImportGrp();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_GRP };
	CBCGPComboBox		m_Cbo;
	CString			m_szID;
	CString			m_szDesc;
	CString			m_szGrp;
	CString			m_szNotes;
	CString			m_szExpID;
	CStringList		m_existing;
	CPropertySheet* m_pParent;
	int				m_nExpIdx;
	int				m_nSubjIdx;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnSelchangeCboGrps();
	afx_msg void OnBnCreategrp();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

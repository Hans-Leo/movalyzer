#pragma once

// WizardImportGenEnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenEnd dialog

class WizardImportGenEnd : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportGenEnd)

// Construction
public:
	WizardImportGenEnd();
	~WizardImportGenEnd();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_GENEND };
	CString			m_szFormat;
	CString			m_szExpID;
	CString			m_szExpDesc;
	CString			m_szCondID;
	CString			m_szCondDesc;
	CString			m_szGrpID;
	CString			m_szGrpDesc;
	CString			m_szSubjID;
	CString			m_szSubjName;
	BOOL			m_fCont;
	BOOL			m_fRetain;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual LRESULT OnWizardNext();
	virtual BOOL OnWizardFinish();
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void DoNext();
	BOOL ConfirmCreate();
	virtual BOOL OnInitDialog();
	afx_msg void OnChkContinue();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

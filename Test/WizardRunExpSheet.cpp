// WizardRunExpSheet.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardRunExpSheet.h"

#include "WizardRunExpStart.h"
#include "WizardRunExpExp.h"
#include "WizardRunExpGrp.h"
#include "WizardRunExpSubj.h"
#include "WizardRunExpEnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpSheet

IMPLEMENT_DYNAMIC(WizardRunExpSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardRunExpSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardRunExpSheet::WizardRunExpSheet( CWnd* pParentWnd, UINT iSelectPage )
	: CBCGPPropertySheet( _T("Run-Experiment Wizard"), pParentWnd, iSelectPage )
{
	AddPages();
}

WizardRunExpSheet::~WizardRunExpSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void WizardRunExpSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	AddPage( new WizardRunExpStart );
	AddPage( new WizardRunExpExp );
	AddPage( new WizardRunExpGrp );
	AddPage( new WizardRunExpSubj );
	AddPage( new WizardRunExpEnd );

	SetWizardMode();
}

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpSheet message handlers

BOOL WizardRunExpSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardRunExpStart ) ) )
			return ((WizardRunExpStart*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardRunExpExp ) ) )
			return ((WizardRunExpExp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardRunExpGrp ) ) )
			return ((WizardRunExpGrp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardRunExpSubj ) ) )
			return ((WizardRunExpSubj*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardRunExpEnd ) ) )
			return ((WizardRunExpEnd*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

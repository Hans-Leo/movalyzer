// WizardImportFormat.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportFormat.h"
#include "WizardImportSheet.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportFormat property page

IMPLEMENT_DYNCREATE(WizardImportFormat, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportFormat, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_CHK_CUSTOM, OnChkCustom)
	ON_BN_CLICKED(IDC_BN_CUSTOM, OnBnCustom)
	ON_CBN_SELCHANGE(IDC_CBO_FMT, OnSelchangeCboFmt)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportFormat::WizardImportFormat() : CBCGPPropertyPage(WizardImportFormat::IDD)
{
	m_fCustom = FALSE;
}

WizardImportFormat::~WizardImportFormat()
{
}

void WizardImportFormat::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_FMT, m_cboFormat);
	DDX_Check(pDX, IDC_CHK_CUSTOM, m_fCustom);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportFormat message handlers

BOOL WizardImportFormat::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_FMT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select from an available format.") );
	pWnd = GetDlgItem( IDC_CHK_CUSTOM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Check to allow a custom format.") );
	pWnd = GetDlgItem( IDC_BN_CUSTOM );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Click to define a custom format.") );

	// Fill Format Combo
	CString szPath, szFile;
	::GetAppPath( szPath );
	szPath += _T("\\*.drv");
	CFileFind ff;
	BOOL fCont = ff.FindFile( szPath );
	while( fCont )
	{
		fCont = ff.FindNextFile();
		szFile = ff.GetFileName();
		szFile = szFile.Left( szFile.GetLength() - 4 );
		szFile.MakeUpper();

		if( ( szFile == _T("GRIPPER") ) && !::IsGA() )
			;	// do nothing
		else
			m_cboFormat.AddString( szFile );
	}
	if( ::IsSA() || ::IsOA() )
		m_cboFormat.AddString( _T("MOVALYZER") );
	if( ::IsSA() || ::IsOA() || ::IsGA() )
		m_cboFormat.AddString( _T("SINGLE TRIAL OR IMAGE") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportFormat::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

void WizardImportFormat::OnChkCustom() 
{
	UpdateData( TRUE );

	CWnd* pWnd = GetDlgItem( IDC_BN_CUSTOM );
	if( pWnd ) pWnd->EnableWindow( m_fCustom );

	pWnd = GetDlgItem( IDC_CBO_FMT );
	if( pWnd ) pWnd->EnableWindow( !m_fCustom );

	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	if( m_fCustom || m_szFormat != _T("") )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );
}

void WizardImportFormat::OnBnCustom() 
{
	// TODO: Add custom column info
	BCGPMessageBox( _T("This has yet to be implemented.") );
}

void WizardImportFormat::OnSelchangeCboFmt() 
{
	int nSel = m_cboFormat.GetCurSel();
	if( nSel < 0 ) return;

	m_cboFormat.GetLBText( nSel, m_szFormat );

	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

BOOL WizardImportFormat::OnSetActive() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	if( m_fCustom || m_szFormat != _T("") )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

LRESULT WizardImportFormat::OnWizardBack() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

LRESULT WizardImportFormat::OnWizardNext() 
{
	m_cboFormat.GetLBText( m_cboFormat.GetCurSel(), m_szFormat );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportFormat::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportFormat::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

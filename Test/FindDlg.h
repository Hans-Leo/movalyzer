#pragma once

// FindDlg.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// FindDlg dialog

class FindDlg : public CBCGPDialog
{
// Construction
public:
	FindDlg(CString szTitle = _T("Find"), CWnd* pParent = NULL);   // standard constructor
	FindDlg(UINT nResourceID, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_FIND };
	CString			m_szID;
	CString			m_szTitle;
	BOOL			m_fPartial;
	BOOL			m_fExactPartial;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	virtual void OnOK();
protected:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

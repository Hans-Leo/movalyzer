#pragma once

// PPUListDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PPUListDlg dialog

class PPUListDlg : public CBCGPDialog
{
// Construction
public:
	PPUListDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_PPULIST };
	CBCGPListBox	m_list;

// Overrides
protected:
	virtual void OnOK();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	afx_msg void OnDblclkList();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

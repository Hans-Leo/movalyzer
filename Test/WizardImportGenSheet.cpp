// WizardImportGenSheet.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportGenSheet.h"

#include "WizardImportGenStart.h"
#include "WizardImportGen.h"
#include "WizardImportExp.h"
#include "WizardImportCond.h"
#include "WizardImportGrp.h"
#include "WizardImportSubj.h"
#include "WizardImportGenEnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenSheet

IMPLEMENT_DYNAMIC(WizardImportGenSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardImportGenSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportGenSheet::WizardImportGenSheet( CWnd* pParentWnd, UINT iSelectPage )
	: CBCGPPropertySheet( _T("Data Generate Wizard"), pParentWnd, iSelectPage )
{
	AddPages();
}

WizardImportGenSheet::~WizardImportGenSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void WizardImportGenSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	AddPage( new WizardImportGenStart );
	AddPage( new WizardImportGen );
	AddPage( new WizardImportExp );
	AddPage( new WizardImportCond );
	AddPage( new WizardImportGrp );
	AddPage( new WizardImportSubj );
	AddPage( new WizardImportGenEnd );

	SetWizardMode();
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenSheet message handlers

BOOL WizardImportGenSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportGenStart ) ) )
			return ((WizardImportGenStart*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportGen ) ) )
			return ((WizardImportGen*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportExp ) ) )
			return ((WizardImportExp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportCond ) ) )
			return ((WizardImportCond*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportGrp ) ) )
			return ((WizardImportGrp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportSubj ) ) )
			return ((WizardImportSubj*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportGenEnd ) ) )
			return ((WizardImportGenEnd*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

// StartView.cpp : implementation file
//

#include "StdAfx.h"
#include "Test.h"
#include "StartView.h"
#include "MainFrm.h"
#include "LeftView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString	_szURL;

/////////////////////////////////////////////////////////////////////////////
// StartView

IMPLEMENT_DYNCREATE( StartView, CHtmlView )

BEGIN_MESSAGE_MAP( StartView, CHtmlView )
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

StartView::StartView()
{
	m_fInit = FALSE;
}

StartView::~StartView()
{
}

/////////////////////////////////////////////////////////////////////////////
// StartView message handlers

CString StartView::ResourceToURL( CString szFile )
{
	_szURL = _T("");
	HINSTANCE hInstance = AfxGetResourceHandle();
	LPTSTR lpszModule = new TCHAR[_MAX_PATH];
	if( GetModuleFileName( hInstance, lpszModule, _MAX_PATH ) )
		_szURL.Format(_T("res://%s/%s"), lpszModule, szFile );
	delete []lpszModule;
	return _szURL;
}

void StartView::ShowPage( CString szURL )
{
	Navigate2( szURL );
	UpdateWindow();
}

void StartView::OnInitialUpdate()
{
	CHtmlView::OnInitialUpdate();
	m_fInit = TRUE;
	OnShowWindow( TRUE, 0 );
}

void StartView::OnDestroy() 
{
	CHtmlView::OnDestroy();
	CView::OnDestroy();		// Fixes CHtmlView bug
}

void StartView::OnShowWindow( BOOL bShow, UINT nStatus )
{
	CHtmlView::OnShowWindow( bShow, nStatus );
	if( !m_fInit || !bShow ) return;

	CString szPage = ResourceToURL( _T("main_sm.html") );

// 	CString szParms = _T("?");
// 	int nParms = 0;
	// need to run device setup wizard?
// 	if( !::WasDeviceSetupRun() )
// 	{
// 		nParms++;
// 		szParms += _T("wiz_ds");
// 	}
	// if no parms, clear the var
// 	if( nParms == 0 ) szParms = _T("");
// 	szPage += szParms;

	ShowPage( szPage );
}

void StartView::OnBeforeNavigate2( LPCTSTR lpszURL, DWORD nFlags, LPCTSTR lpszTargetFrameName,
								   CByteArray& baPostedData, LPCTSTR lpszHeaders, BOOL* pbCancel ) 
{
	if( m_fInit )
	{
		// do NOT navigate unless shows a legit HTML file
		*pbCancel = TRUE;

		// extract actual file name or command
		CString szURL = lpszURL;
		int nPos = szURL.ReverseFind( '/' );
		if( nPos == -1 ) return;
		CString szFile = szURL.Mid( nPos + 1 );

		// have an extension? Is it HTML or HTM?
		BOOL fURL = FALSE;
		if( ( szFile.GetLength() >= 9 ) && ( szFile.Left( 9 ) == _T("main.html") ) )
		{
			fURL = TRUE;
		}
		else if( szURL.GetLength() >= 4 )
		{
			CString szProt = szURL.Left( 3 );
			szProt.MakeUpper();
			if( szProt == _T("RES") )
			{
				szProt = szURL.Right( 4 );
				szProt.MakeUpper();
				if( szProt == _T("HTML") ) fURL = TRUE;
			}
			szProt = szURL.Left( 4 );
			szProt.MakeUpper();
			if( szProt == _T("HTTP") ) fURL = TRUE;
			if( szProt == _T("FILE") )
			{
				szProt = szURL.Right( 4 );
				szProt.MakeUpper();
				if( szProt == _T("HTML") ) fURL = TRUE;
			}
			szProt = szURL.Left( 5 );
			szProt.MakeUpper();
			if( szProt == _T("HTTPS") ) fURL = TRUE;
		}
		nPos = szFile.ReverseFind( '.' );

		// process either command or url
		if( !fURL )	// command
		{
			// full start page
			if( szFile == _T("win_full") )
			{
				CString szPage = ResourceToURL( _T("main.html") );
				CString szParms = _T("?");
				int nParms = 0;

				// need to run device setup wizard?
				if( !::WasDeviceSetupRun() )
				{
					nParms++;
					szParms += _T("wiz_ds");
				}

				// if no parms, clear the var
				if( nParms == 0 ) szParms = _T("");

				szPage += szParms;
				ShowPage( szPage );
			}
			// device setup wizard
			else if( szFile == _T("wiz_ds") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_WIZ_DEVSETUP, 0 );
			// experiment setup wizard
			else if( szFile == _T("wiz_es") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_WIZ_EXPSETUP, 0 );
			// run experiment wizard
			else if( szFile == _T("wiz_re") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_WIZ_RUNEXP, 0 );
			// export wizard
			else if( szFile == _T("wiz_export") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_F_EXPORT, 0 );
			// import
			else if( szFile == _T("import") )
				::PostMessage( ((CMainFrame*)AfxGetMainWnd())->GetLeftView()->m_hWnd,
							   WM_COMMAND, IDM_FILE_IMPORT, 0 );
			// import wizard
			else if( szFile == _T("wiz_import") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_WIZ_DATAIMPORT, 0 );
			// data generation wizard
			else if( szFile == _T("sim_data") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_WIZ_DATAGEN, 0 );
			// test
			else if( szFile == _T("test") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_DIG_TEST, 0 );
			// new user
			else if( szFile == _T("new_user") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_FILE_USERS, 0 );
			// help
			else if( szFile == _T("help_app") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_HELP_HELP, 0 );
			// tutorial
			else if( szFile == _T("help_tut") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_HELP_TUTORIAL, 0 );
			// troubleshooting
			else if( szFile == _T("help_ts") )
				::PostMessage( AfxGetMainWnd()->m_hWnd, WM_COMMAND, IDM_HELP_TROUBLESHOOTING, 0 );
			// close window
			else if( szFile == _T("win_close") ) ShowWindow( SW_HIDE );
		}
		else *pbCancel = FALSE;
	}
}

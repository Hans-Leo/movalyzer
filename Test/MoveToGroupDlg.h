#pragma once

// MoveToGroupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// MoveToGroupDlg dialog

class MoveToGroupDlg : public CBCGPDialog
{
// Construction
public:
	MoveToGroupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_GLIST };
	CBCGPListBox	m_listGrp;
	CBCGPListBox	m_listSubj;
	CStringList		m_lstItemsGrp;
	CStringList		m_lstItemsSubj;
	CString			m_szGrp;
	CString			m_szSubj;
	CString			m_szCurGrp;
	CString			m_szCurSubj;

// Overrides
protected:
	virtual void OnOK();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	afx_msg void OnDblclkListGrp();
	afx_msg void OnDblclkListSubj();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

// WizardSetupGrp.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupGrp.h"
#include "WizardSetupSheet.h"
#include "..\DataMod\DataMod.h"
#include "GroupDlg.h"
#include "Groups.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardSetupGrp property page

IMPLEMENT_DYNCREATE(WizardSetupGrp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardSetupGrp, CBCGPPropertyPage)
	ON_CBN_SELCHANGE(IDC_CBO_GRPS, OnSelchangeCboGrps)
	ON_BN_CLICKED(IDC_BN_CREATEGRP, OnBnCreategrp)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupGrp::WizardSetupGrp() : CBCGPPropertyPage(WizardSetupGrp::IDD)
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_szGrp = _T("");
	m_fUpdated = FALSE;
}

WizardSetupGrp::~WizardSetupGrp()
{
}

void WizardSetupGrp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_GRPS, m_Cbo);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szDesc);
	DDX_CBString(pDX, IDC_CBO_GRPS, m_szGrp);
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupGrp message handlers

void WizardSetupGrp::OnSelchangeCboGrps() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szID, szDesc, szDelim;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	Groups grp;
	if( !grp.IsValid() ) return;
	if( FAILED( grp.Find( m_szID ) ) ) return;
	grp.GetNotes( m_szNotes );

	m_szGrp = szItem;
	UpdateData( FALSE );

	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

void WizardSetupGrp::OnBnCreategrp() 
{
	GroupDlg d( this, TRUE );
	if( d.DoModal() == IDOK )
	{
		m_szID = d.m_szAbbr;
		m_szDesc = d.m_szDesc;
		m_szNotes = d.m_szNotes;
		UpdateData( FALSE );

		WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	}
}

BOOL WizardSetupGrp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetThisIcon( IDI_WIZ_EXP_SETUP );
	CWnd* pWnd = GetDlgItem( IDC_CBO_GRPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing group for this experiment."), _T("Existing Group") );
	pWnd = GetDlgItem( IDC_BN_CREATEGRP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new group to use for this experiment."), _T("New Group") );

	// Fill group list
	Groups grp;
	if( !grp.IsValid() ) return TRUE;
	CString szItem;
	HRESULT hr = grp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		grp.GetDescriptionWithID( szItem );
		m_Cbo.AddString( szItem );
		hr = grp.GetNext();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardSetupGrp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL WizardSetupGrp::OnSetActive() 
{
	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	if( m_szID != _T("") ) pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnSetActive();
}

BOOL WizardSetupGrp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newexperiment.html#wizard"), this );
	return TRUE;
}

BOOL WizardSetupGrp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

#if !defined(AFX_FILENAMEDLG_H__93FCD953_9BB1_11D3_8A30_00104BC7E2C8__INCLUDED_)
#define AFX_FILENAMEDLG_H__93FCD953_9BB1_11D3_8A30_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileNameDlg.h : header file
//
#include "BtnSt.h"

/////////////////////////////////////////////////////////////////////////////
// FileNameDlg dialog

class FileNameDlg : public CDialog
{
// Construction
public:
	FileNameDlg(CString szFilter, CString szName = _T("*"),
				CWnd* pParent = NULL, CString szPath = _T(""));

	FileNameDlg(CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(FileNameDlg)
	enum { IDD = IDD_FILENAME };
	CString	m_szFile;
	//}}AFX_DATA
	CString	m_szFilter;
	CString	m_szName;
	CString	m_szPath;
	BOOL	m_fDirOnly;
	CButtonST	m_bnBrowse;

// Overrides
	//{{AFX_VIRTUAL(FileNameDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(FileNameDlg)
	afx_msg void OnBnBrowse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILENAMEDLG_H__93FCD953_9BB1_11D3_8A30_00104BC7E2C8__INCLUDED_)

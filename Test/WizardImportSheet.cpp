// WizardImportSheet.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportSheet.h"

#include "WizardImportStart.h"
#include "WizardImportPath.h"
#include "WizardImportExp.h"
#include "WizardImportCond.h"
#include "WizardImportGrp.h"
#include "WizardImportSubj.h"
#include "WizardImportFormat.h"
#include "WizardImportEnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportSheet

IMPLEMENT_DYNAMIC(WizardImportSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardImportSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportSheet::WizardImportSheet( CWnd* pParentWnd, UINT iSelectPage )
	: CBCGPPropertySheet( _T("Data Import Wizard"), pParentWnd, iSelectPage )
{
	AddPages();
}

WizardImportSheet::~WizardImportSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void WizardImportSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	AddPage( new WizardImportStart );
	AddPage( new WizardImportFormat );
	AddPage( new WizardImportPath );
	AddPage( new WizardImportExp );
	AddPage( new WizardImportCond );
	AddPage( new WizardImportGrp );
	AddPage( new WizardImportSubj );
	AddPage( new WizardImportEnd );

	SetWizardMode();
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportSheet message handlers

BOOL WizardImportSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportStart ) ) )
			return ((WizardImportStart*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportPath ) ) )
			return ((WizardImportPath*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportExp ) ) )
			return ((WizardImportExp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportCond ) ) )
			return ((WizardImportCond*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportGrp ) ) )
			return ((WizardImportGrp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportSubj ) ) )
			return ((WizardImportSubj*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportFormat ) ) )
			return ((WizardImportFormat*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardImportEnd ) ) )
			return ((WizardImportEnd*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

// CrashDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "CrashDlg.h"
#include <Time.h>
#include <sys/timeb.h>
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CrashDlg dialog

BEGIN_MESSAGE_MAP(CrashDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_ADD, OnBnAdd)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

CrashDlg::CrashDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(CrashDlg::IDD, pParent)
{
	m_szDo = _T("");
}
void CrashDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DO, m_szDo);
}

/////////////////////////////////////////////////////////////////////////////
// CrashDlg message handlers

BOOL CrashDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Provide a detailed description of what you were doing at the time of the application crash."), _T("Events Leading to Crash") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add your comments to the actions log and submit to NeuroScript."), _T("Add") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close the dialog without adding your comments."), _T("Close") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CrashDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void CrashDlg::OnBnAdd() 
{
	UpdateData( TRUE );

	// verify something was entered
	if( m_szDo == _T("") )
	{
		BCGPMessageBox( _T("Please enter the events leading up to the crash."), MB_OK | MB_ICONEXCLAMATION );
		return;
	}

	CStdioFile	LogFile;

	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");

	if( !LogFile.Open( szAppPath,
					   CFile::modeWrite | CFile::modeCreate |
					   CFile::shareDenyNone | CFile::modeNoTruncate ) )
	{
		BCGPMessageBox( IDS_LOG_ERR );
		return;
	}

	CString szLine, szTemp;

	szTemp = (COleDateTime::GetCurrentTime()).Format( (UINT)IDS_LOG_FMT );
	szLine += szTemp;

	struct _timeb tstruct;
	_ftime64_s( &tstruct );
	szTemp.Format( IDS_LOG_FMT2, tstruct.millitm );
	szLine += szTemp;

	szTemp.Format( _T("CRASH: %s\n"), m_szDo );
	szLine += szTemp;

	LogFile.SeekToEnd();
	LogFile.WriteString( szLine );
	LogFile.Close();

	EndDialog( IDOK );
}

BOOL CrashDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("crash.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

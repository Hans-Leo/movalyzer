// ImportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "ImportDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ImportDlg dialog

BEGIN_MESSAGE_MAP(ImportDlg, CDialog)
	//{{AFX_MSG_MAP(ImportDlg)
	ON_WM_HELPINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

ImportDlg::ImportDlg(BOOL fCopy, CWnd* pParent /*=NULL*/)
	: CDialog(ImportDlg::IDD, pParent), m_pLstExp( NULL ), m_pLstGrp( NULL ),
	  m_pLstSubj( NULL ), m_pLstCond( NULL ), m_fCopyEn( fCopy )
{
	//{{AFX_DATA_INIT(ImportDlg)
	m_fCopy = FALSE;
	//}}AFX_DATA_INIT
}

void ImportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ImportDlg)
	DDX_Control(pDX, IDC_CHK_COPY, m_chkCopy);
	DDX_Control(pDX, IDC_LST_COND, m_lstCond);
	DDX_Control(pDX, IDC_LST_SUBJ, m_lstSubj);
	DDX_Control(pDX, IDC_LST_GRP, m_lstGrp);
	DDX_Control(pDX, IDC_LST_EXP, m_lstExp);
	DDX_Check(pDX, IDC_CHK_COPY, m_fCopy);
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// ImportDlg message handlers

BOOL ImportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd* pWnd = GetDlgItem( IDC_CHK_COPY );
	if( pWnd ) pWnd->EnableWindow( m_fCopyEn );
	m_fCopy = m_fCopyEn;
	UpdateData( FALSE );

	// FILL LISTS
	POSITION pos = NULL;
	CString szItem;
	// Experiments
	pos = m_pLstExp ? m_pLstExp->GetHeadPosition() : NULL;
	while( pos )
	{
		szItem = m_pLstExp->GetNext( pos );
		m_lstExp.AddString( szItem );
	}
	// Groups
	pos = m_pLstGrp ? m_pLstGrp->GetHeadPosition() : NULL;
	while( pos )
	{
		szItem = m_pLstGrp->GetNext( pos );
		m_lstGrp.AddString( szItem );
	}
	// Subjects
	pos = m_pLstSubj ? m_pLstSubj->GetHeadPosition() : NULL;
	while( pos )
	{
		szItem = m_pLstSubj->GetNext( pos );
		m_lstSubj.AddString( szItem );
	}
	// Conditions
	pos = m_pLstCond ? m_pLstCond->GetHeadPosition() : NULL;
	while( pos )
	{
		szItem = m_pLstCond->GetNext( pos );
		m_lstCond.AddString( szItem );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ImportDlg::OnOK()
{
	// REMOVE HIGHLIGHTED ITEMS FROM LISTS
	POSITION pos = NULL;
	CString szItem;
	int* indices = NULL, i = 0;
	// Experiments
	if( m_pLstExp && ( m_lstExp.GetSelCount() > 0 ) )
	{
		indices = new int[ m_lstExp.GetSelCount() ];
		m_lstExp.GetSelItems( m_lstExp.GetSelCount(), indices );
		for( i = 0; i < m_lstExp.GetSelCount(); i++ )
		{
			m_lstExp.GetText( indices[ i ], szItem );
			pos = m_pLstExp->Find( szItem );
			if( pos ) m_pLstExp->RemoveAt( pos );
		}
		delete [] indices;
	}
	// Groups
	if( m_pLstGrp && ( m_lstGrp.GetSelCount() > 0 ) )
	{
		indices = new int[ m_lstGrp.GetSelCount() ];
		m_lstGrp.GetSelItems( m_lstGrp.GetSelCount(), indices );
		for( i = 0; i < m_lstGrp.GetSelCount(); i++ )
		{
			m_lstGrp.GetText( indices[ i ], szItem );
			pos = m_pLstGrp->Find( szItem );
			if( pos ) m_pLstGrp->RemoveAt( pos );
		}
		delete [] indices;
	}
	// Subjects
	if( m_pLstSubj && ( m_lstSubj.GetSelCount() > 0 ) )
	{
		indices = new int[ m_lstSubj.GetSelCount() ];
		m_lstSubj.GetSelItems( m_lstSubj.GetSelCount(), indices );
		for( i = 0; i < m_lstSubj.GetSelCount(); i++ )
		{
			m_lstSubj.GetText( indices[ i ], szItem );
			pos = m_pLstSubj->Find( szItem );
			if( pos ) m_pLstSubj->RemoveAt( pos );
		}
		delete [] indices;
	}
	// Conditions
	if( m_pLstCond && ( m_lstCond.GetSelCount() > 0 ) )
	{
		indices = new int[ m_lstCond.GetSelCount() ];
		m_lstCond.GetSelItems( m_lstCond.GetSelCount(), indices );
		for( i = 0; i < m_lstCond.GetSelCount(); i++ )
		{
			m_lstCond.GetText( indices[ i ], szItem );
			pos = m_pLstCond->Find( szItem );
			if( pos ) m_pLstCond->RemoveAt( pos );
		}
		delete [] indices;
	}

	CDialog::OnOK();
}

BOOL ImportDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->GoToHelp( _T("importexport.html"), this );

	return CDialog::OnHelpInfo(pHelpInfo);
}

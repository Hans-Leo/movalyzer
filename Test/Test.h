// Test.h : main header file for the TEST application
//
#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#pragma warning( disable: 4005 )  // macro redefinition
#include "..\NSSharedGUI\resource.h"
#pragma warning( default: 4005 )
#include "..\Common\Security.h"

/////////////////////////////////////////////////////////////////////////////
// CTestApp:
// See Test.cpp for the implementation of this class
//

class CTestApp : public CWinApp, public CBCGPWorkspace
{
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CTestApp();

	CSingleDocTemplate*		m_pDocTemplateMain;

	NSSecurity	m_security;
	BOOL		m_fBackgroundImage;
	BOOL		m_bHiColorIcons;
	CList<COLORREF, COLORREF>	m_lstCustomColors;
	COLORREF					m_clrCustomDef;
	COLORREF					m_clrCustom;

	void LoadCustomState();
	void SaveCustomState();
	void DoAbout();

public:
	BOOL WriteSectionDWORD( LPCTSTR lpszSubSection, LPCTSTR lpszEntry, DWORD dwValue )
	{
		ASSERT( lpszSubSection );
		ASSERT( lpszEntry );
		CString strSection = GetRegSectionPath( lpszSubSection );
		CBCGPRegistrySP regSP;
		CBCGPRegistry& reg = regSP.Create( FALSE, FALSE );
		if( reg.CreateKey( strSection ) ) return reg.Write( lpszEntry, dwValue );
		else return FALSE;
	}
	BOOL WriteDWORD( LPCTSTR lpszEntry, DWORD dwValue )
	{ return WriteSectionDWORD( _T(""), lpszEntry, dwValue ); }

	DWORD GetSectionDWORD( LPCTSTR lpszSubSection, LPCTSTR lpszEntry, DWORD dwDefault = 0 )
	{
		ASSERT( lpszSubSection );
		ASSERT( lpszEntry );
		DWORD dwRet = dwDefault;
		CString strSection = GetRegSectionPath( lpszSubSection );
		CBCGPRegistrySP regSP;
		CBCGPRegistry& reg = regSP.Create( FALSE, TRUE );
		if( reg.Open( strSection ) ) reg.Read( lpszEntry, dwRet );
		return dwRet;
	}
	DWORD GetDWORD( LPCTSTR lpszEntry, DWORD dwDefault = 0 )
	{ return GetSectionDWORD( _T(""), lpszEntry, dwDefault ); }

// Overrides
public:
	// get security object
	NSSecurity* GetSecurity();

	// initialization/cleanup
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();
	afx_msg void OnViewAppLook();
	DECLARE_MESSAGE_MAP()
};

extern CTestApp theApp;

#pragma once

// WizardSetupGrp.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardSetupGrp dialog

class WizardSetupGrp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardSetupGrp)

// Construction
public:
	WizardSetupGrp();
	~WizardSetupGrp();

// Dialog Data
	enum { IDD = IDD_WIZ_SETUP_GRP };
	CBCGPComboBox	m_Cbo;
	CString			m_szID;
	CString			m_szDesc;
	CString			m_szGrp;
	CString			m_szNotes;
	BOOL			m_fUpdated;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnSelchangeCboGrps();
	afx_msg void OnBnCreategrp();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

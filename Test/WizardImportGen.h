#pragma once

// WizardImportGen.h : header file
//

#include "SigGenSettingsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// WizardImportGen dialog

class WizardImportGen : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportGen)

// Construction
public:
	WizardImportGen();
	~WizardImportGen();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_GEN };
	CString			m_szType;
	CString			m_szProfile;
	CString			m_szNoise;
	CString			m_szRound;
	CString			m_szXStart;
	CString			m_szXDelta;
	CString			m_szXPhase;
	CString			m_szXVel;
	CString			m_szYStart;
	CString			m_szYDelta;
	CString			m_szYPhase;
	CString			m_szStart;
	CString			m_szStrokes;
	CString			m_szTrail;
	CString			m_szDelta;
	CString			m_szSM;
	CString			m_szAmount;
	CString			m_szDur;
	BOOL			m_fSelected;
	SigGenSettingsDlg	m_dlgSettings;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void UpdateValues();
	afx_msg void OnBnSettings();
	afx_msg void OnBnPreview();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

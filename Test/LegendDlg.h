#pragma once

// LegendDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// LegendDlg dialog

class LegendDlg : public CBCGPDialog
{
// Construction
public:
	LegendDlg(CWnd* pParent = NULL);

// Dialog Data
	enum { IDD = IDD_LEGEND };

// Overrides
protected:
	virtual void OnOK();
	virtual void OnCancel();
	virtual void PostNcDestroy();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

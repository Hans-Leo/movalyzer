// TestDeviceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "TestDeviceDlg.h"
#include "SigGenSettingsDlg.h"
#include "GenSig.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TestDeviceDlg dialog

BEGIN_MESSAGE_MAP(TestDeviceDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_RDO_RAWT, SetRadioStates)
	ON_BN_CLICKED(IDC_RDO_LINEAR, SetRadioStates)
	ON_BN_CLICKED(IDC_RDO_LINDRAW, SetRadioStates)
	ON_BN_CLICKED(IDC_RDO_LINGEN, SetRadioStates)
	ON_BN_CLICKED(IDC_BN_SETTINGS, OnBnSettings)
	ON_BN_CLICKED(IDC_BN_EDIT, OnBnEdit)
	ON_BN_CLICKED(IDC_BN_GO, OnBnGo)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

TestDeviceDlg::TestDeviceDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(TestDeviceDlg::IDD, pParent), dlgSettings( NULL )
{
	m_fRaw = TRUE;
	m_fLinear = FALSE;
	m_fDraw = TRUE;
	m_fGen = FALSE;
	m_fDidGen = FALSE;
	m_fDidSet = FALSE;

	dlgSettings = new SigGenSettingsDlg( GS_LINEAR, this );
}

TestDeviceDlg::~TestDeviceDlg()
{
	if( dlgSettings ) delete dlgSettings;
}

void TestDeviceDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
// TestDeviceDlg message handlers

BOOL TestDeviceDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.SetTitle( _T("Test Device") );
	CWnd* pWnd = GetDlgItem( IDC_RDO_RAWT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Test input device by hand.") );
	pWnd = GetDlgItem( IDC_RDO_LINEAR );
	if( pWnd )
	{
		m_tooltip.AddTool( pWnd, _T("Test digitizer for linearity accuracy.") );
		pWnd->EnableWindow( !::IsGripperModeOn() );
	}
	pWnd = GetDlgItem( IDC_RDO_LINDRAW );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Manually test linearity by drawing a diagonal line.") );
	pWnd = GetDlgItem( IDC_RDO_LINGEN );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Test linearity via a generated diagonal line.") );
	pWnd = GetDlgItem( IDC_BN_SETTINGS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Change line generation settings.") );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("View/edit generated line raw data file.") );
	pWnd = GetDlgItem( IDC_BN_GO);
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Test device now.") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Cancel test.") );

	CheckRadioButton( IDC_RDO_RAWT, IDC_RDO_LINEAR, IDC_RDO_RAWT );
	CheckRadioButton( IDC_RDO_LINDRAW, IDC_RDO_LINGEN, IDC_RDO_LINDRAW );
	SetRadioStates();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL TestDeviceDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void TestDeviceDlg::SetRadioStates()
{
	int nRaw = GetCheckedRadioButton( IDC_RDO_RAWT, IDC_RDO_LINEAR );
	int nDraw = GetCheckedRadioButton( IDC_RDO_LINDRAW, IDC_RDO_LINGEN );

	m_fRaw = ( nRaw == IDC_RDO_RAWT );
	m_fLinear = !m_fRaw;
	m_fDraw = ( nDraw == IDC_RDO_LINDRAW );
	m_fGen = !m_fDraw;

	CWnd* pWnd = GetDlgItem( IDC_RDO_LINEAR );
	if( pWnd ) pWnd->EnableWindow( !::IsGripperModeOn() );
	pWnd = GetDlgItem( IDC_RDO_LINDRAW );
	if( pWnd ) pWnd->EnableWindow( m_fLinear );
	pWnd = GetDlgItem( IDC_RDO_LINGEN );
	if( pWnd ) pWnd->EnableWindow( m_fLinear );
	pWnd = GetDlgItem( IDC_BN_SETTINGS );
	if( pWnd ) pWnd->EnableWindow( m_fGen );
	pWnd = GetDlgItem( IDC_BN_EDIT );
	if( pWnd ) pWnd->EnableWindow( m_fGen );
}

void TestDeviceDlg::OnBnSettings() 
{
	if( dlgSettings->DoModal() == IDOK )
	{
		m_fDidGen = FALSE;
		m_fDidSet = TRUE;
	}
}

void TestDeviceDlg::OnBnEdit() 
{
	CString szFile;
	::GetDataPathRoot( szFile );
	szFile += _T("\\test.hwr");

	// Generate first if needed
	if( !m_fDidGen )
	{
		GenSig gs;
		dlgSettings->m_nMode = GS_LINEAR;
		if( !m_fDidSet ) dlgSettings->UpdateSettings();
		gs.m_cm = dlgSettings->m_cm;
		gs.m_deltat = dlgSettings->m_deltat;
		gs.m_deltax = dlgSettings->m_deltax;
		gs.m_deltay = dlgSettings->m_deltay;
		gs.m_integer = dlgSettings->m_fInt;
		gs.m_noise = dlgSettings->m_noise;
		gs.m_nstroke = dlgSettings->m_nstrokes;
		gs.m_pattern = dlgSettings->m_pattern;
		gs.m_phasex = dlgSettings->m_phasex;
		gs.m_phasey = dlgSettings->m_phasey;
		gs.m_prsmin = dlgSettings->m_prsmin;
		gs.m_sec = dlgSettings->m_sec;
		gs.m_startt = dlgSettings->m_startt;
		gs.m_startx = dlgSettings->m_startx;
		gs.m_starty = dlgSettings->m_starty;
		gs.m_trailtime = dlgSettings->m_trailtime;
		gs.m_xspeed = dlgSettings->m_xspeed;

		if( !gs.Generate( szFile ) ) return;

		m_fDidGen = TRUE;
	}

	// View in notepad
	CString szCmd;
	szCmd.LoadString( IDS_EDITOR );
	szCmd += szFile;
	WinExec( szCmd, SW_SHOW );
}

void TestDeviceDlg::OnBnGo() 
{
	// Generate first if needed
	if( !m_fDidGen && !::IsGripperModeOn() )
	{
		CString szFile;
		::GetDataPathRoot( szFile );
		szFile += _T("\\test.hwr");

		GenSig gs;
		dlgSettings->m_nMode = GS_LINEAR;
		if( !m_fDidSet ) dlgSettings->UpdateSettings();
		gs.m_cm = dlgSettings->m_cm;
		gs.m_deltat = dlgSettings->m_deltat;
		gs.m_deltax = dlgSettings->m_deltax;
		gs.m_deltay = dlgSettings->m_deltay;
		gs.m_integer = dlgSettings->m_fInt;
		gs.m_noise = dlgSettings->m_noise;
		gs.m_nstroke = dlgSettings->m_nstrokes;
		gs.m_pattern = dlgSettings->m_pattern;
		gs.m_phasex = dlgSettings->m_phasex;
		gs.m_phasey = dlgSettings->m_phasey;
		gs.m_prsmin = dlgSettings->m_prsmin;
		gs.m_sec = dlgSettings->m_sec;
		gs.m_startt = dlgSettings->m_startt;
		gs.m_startx = dlgSettings->m_startx;
		gs.m_starty = dlgSettings->m_starty;
		gs.m_trailtime = dlgSettings->m_trailtime;
		gs.m_xspeed = dlgSettings->m_xspeed;

		if( !gs.Generate( szFile ) ) return;

		m_fDidGen = TRUE;
	}

	SetRadioStates();

	CBCGPDialog::OnOK();
}

BOOL TestDeviceDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("testingdigitizer.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

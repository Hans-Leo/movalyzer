#pragma once

// WizardRunExpStart.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpStart dialog

class WizardRunExpStart : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardRunExpStart)

// Construction
public:
	WizardRunExpStart();
	~WizardRunExpStart();

// Dialog Data
	enum { IDD = IDD_WIZ_RUNEXP_START };

// Overrides
public:
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
// message map functions
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

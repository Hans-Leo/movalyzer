// ProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "ProgressDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProgressDlg dialog

BEGIN_MESSAGE_MAP(ProgressDlg, CDialog)
	//{{AFX_MSG_MAP(ProgressDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

ProgressDlg::ProgressDlg(CWnd* pParent /*=NULL*/)
	: CDialog()
{
	//{{AFX_DATA_INIT(ProgressDlg)
	//}}AFX_DATA_INIT

	if( Create( ProgressDlg::IDD, pParent ) )
		ShowWindow( SW_SHOW );
}

void ProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ProgressDlg)
	DDX_Control(pDX, IDC_PROGRESS, m_prog);
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// ProgressDlg message handlers

BOOL ProgressDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_prog.SetRange( 0, 100 );
	SetWindowPos( &wndTopMost, 0, 0, 0, 0, SWP_NOSIZE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ProgressDlg::OnCancel()
{
	DestroyWindow();
}

void ProgressDlg::PostNcDestroy()
{
	delete this;
}

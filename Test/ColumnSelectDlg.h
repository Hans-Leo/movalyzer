#pragma once

// ColumnSelectDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// ColumnSelectDlg dialog

class ColumnSelectDlg : public CBCGPDialog
{
// Construction
public:
	ColumnSelectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_SELLIST };
	CBCGPListBox	m_list;
	CString			m_szFile;
	CStringList		m_lstColNum;
	CStringList		m_lstColName;
	NSToolTipCtrl	m_tooltip;
	int				m_nSkip;
	BOOL			m_fHeaders;
	CString			m_szDelim;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

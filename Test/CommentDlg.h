#pragma once

// CommentDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CommentDlg dialog

class CommentDlg : public CBCGPDialog
{
// Construction
public:
	CommentDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_COMMENT };
	CString			m_szDo;
	CString			m_szHap;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	afx_msg void OnBnAdd();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

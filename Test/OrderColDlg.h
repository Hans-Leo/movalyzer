#pragma once

// OrderColDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// OrderColDlg dialog

class OrderColDlg : public CBCGPDialog
{
// Construction
public:
	OrderColDlg(CStringList* pLstNums, CStringList* pLstNames, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_ORDERLIST };
	CBCGPListBox	m_list;
	NSToolTipCtrl	m_tooltip;
	CStringList*	m_pLstNums;
	CStringList*	m_pLstNames;
	CBCGPButton		m_bnUp;
	CBCGPButton		m_bnDown;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void FillList();
	afx_msg void OnClickBnUp();
	afx_msg void OnClickBnDown();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

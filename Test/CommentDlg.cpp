// CommentDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "CommentDlg.h"
#include <Time.h>
#include <sys/timeb.h>
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CommentDlg dialog

BEGIN_MESSAGE_MAP(CommentDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_ADD, OnBnAdd)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

CommentDlg::CommentDlg(CWnd* pParent /*=NULL*/)
	: CBCGPDialog(CommentDlg::IDD, pParent)
{
	m_szDo = _T("");
	m_szHap = _T("");
}
void CommentDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DO, m_szDo);
	DDX_Text(pDX, IDC_EDIT_HAP, m_szHap);
}

/////////////////////////////////////////////////////////////////////////////
// CommentDlg message handlers

BOOL CommentDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// Tooltip support
	m_tooltip.Create( this );
	CWnd* pWnd = GetDlgItem( IDC_EDIT_DO );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Explain your issue/action here."), _T("Subject") );
	pWnd = GetDlgItem( IDC_EDIT_HAP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Describe the problem here."), _T("Detail") );
	pWnd = GetDlgItem( IDC_BN_ADD );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Add this issue to the actions log."), _T("Add") );
	pWnd = GetDlgItem( IDCANCEL );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Close dialog."), _T("Close") );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CommentDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void CommentDlg::OnBnAdd() 
{
	UpdateData( TRUE );

	// verify something was entered
	if( m_szDo == _T("") || m_szHap == _T("") )
	{
		BCGPMessageBox( _T("Please enter both pieces of requested information."), MB_OK | MB_ICONEXCLAMATION );
		return;
	}

	CStdioFile	LogFile;

	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");

	if( !LogFile.Open( szAppPath,
					   CFile::modeWrite | CFile::modeCreate |
					   CFile::shareDenyNone | CFile::modeNoTruncate ) )
	{
		BCGPMessageBox( IDS_LOG_ERR );
		return;
	}

	CString szLine, szTemp;

	szTemp = (COleDateTime::GetCurrentTime()).Format( (UINT)IDS_LOG_FMT );
	szLine += szTemp;

	struct _timeb tstruct;
	_ftime64_s( &tstruct );
	szTemp.Format( IDS_LOG_FMT2, tstruct.millitm );
	szLine += szTemp;

	szTemp.Format( IDS_LOG_COMMENT, m_szDo, m_szHap );
	szLine += szTemp;

	LogFile.SeekToEnd();
	LogFile.WriteString( szLine );

	m_szDo = _T("");
	m_szHap = _T("");
	UpdateData( FALSE );

	BCGPMessageBox( _T("Your comment has been added.\nYou can now add additional comments if desired."), MB_OK | MB_ICONINFORMATION );
}

BOOL CommentDlg::OnHelpInfo( HELPINFO* pHelpInfo )
{
	::GoToHelp( _T("log.html"), this );
	return CBCGPDialog::OnHelpInfo(pHelpInfo);
}

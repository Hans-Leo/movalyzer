// HelpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "HelpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// HelpDlg dialog

BEGIN_MESSAGE_MAP(HelpDlg, CDialog)
	//{{AFX_MSG_MAP(HelpDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_GO, OnBnGo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(HelpDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(HelpDlg)
	ON_EVENT(HelpDlg, IDC_BN_BACK, -600 /* Click */, OnClickBnBack, VTS_NONE)
	ON_EVENT(HelpDlg, IDC_BN_FORWARD, -600 /* Click */, OnClickBnForward, VTS_NONE)
	ON_EVENT(HelpDlg, IDC_BN_REFRESH, -600 /* Click */, OnClickBnRefresh, VTS_NONE)
	ON_EVENT(HelpDlg, IDC_BROWSER, 252 /* NavigateComplete2 */, OnNavigateComplete2Browser, VTS_DISPATCH VTS_PVARIANT)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

HelpDlg::HelpDlg( BOOL fTutorial, CWnd* pParent )
	: CDialog( HelpDlg::IDD, pParent )
{
	//{{AFX_DATA_INIT(HelpDlg)
	m_szURL = _T("");
	//}}AFX_DATA_INIT
	m_fTutorial = fTutorial;

	if( ::GetHelpOpen() )
	{
		HelpDlg* pDlg = (HelpDlg*)::GetHelpWindow();
		if( pDlg )
		{
			pDlg->m_szURL = _T("");
			pDlg->Refresh();
			pDlg->SetFocus();
		}
		::AddHelpWindow();
		delete this;
		return;
	}

	if( Create( HelpDlg::IDD, pParent ) )
	{
		::AddHelpWindow();
		::SetHelpWindow( this );
		ShowWindow( SW_SHOW );
	}
}

HelpDlg::HelpDlg( CString szURL, BOOL fTutorial, CWnd* pParent )
	: CDialog( HelpDlg::IDD, pParent ), m_szURL( szURL )
{
	m_fTutorial = fTutorial;

	if( ::GetHelpOpen() )
	{
		HelpDlg* pDlg = (HelpDlg*)::GetHelpWindow();
		if( pDlg )
		{
			pDlg->m_szURL = szURL;
			pDlg->Refresh();
			pDlg->SetFocus();
		}
		::AddHelpWindow();
		delete this;
		return;
	}

	if( Create( HelpDlg::IDD, pParent ) )
	{
		::AddHelpWindow();
		::SetHelpWindow( this );
		ShowWindow( SW_SHOW );
	}
}

HelpDlg::~HelpDlg()
{
	::RemoveHelpWindow();
}

void HelpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HelpDlg)
	DDX_Control(pDX, IDC_BROWSER, m_wb);
	DDX_Text(pDX, IDC_EDIT_URL, m_szURL);
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// HelpDlg message handlers

BOOL HelpDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CFileFind f;
	BOOL fFound = FALSE;
	CString szPath = AfxGetApp()->m_pszHelpFilePath, szTemp;
	int nPos = szPath.ReverseFind( '\\' );
	szPath = szPath.Left( nPos );

	CString szHF = szPath;
	if( m_fTutorial )
	{
		szHF += _T("\\Help\\test.html");
		if( f.FindFile( szHF ) )
		{
			szTemp.Format( _T("Opening application tutorial = %s."), szHF );
			OutputAMessage( szTemp, TRUE );
			f.FindNextFile();
			m_szURL = f.GetFilePath();
			m_wb.Navigate( m_szURL, NULL, NULL, NULL, NULL );

			UpdateData( FALSE );
			fFound = TRUE;
		}
		else
		{
			szTemp.Format( _T("Unable to open application tutorial = %s."), szHF );
			OutputAMessage( szTemp, TRUE );
		}
	}
	else
	{
		szHF += _T("\\Help\\NSHelp.html");
		if( f.FindFile( szHF ) )
		{
			szTemp.Format( _T("Opening application help = %s."), szHF );
			OutputAMessage( szTemp, TRUE );
			f.FindNextFile();
			if( m_szURL != _T("") )
			{
				CString szTemp = f.GetFilePath();
				szTemp += _T("#");
				szTemp += m_szURL;
				m_szURL = szTemp;
			}
			else m_szURL = f.GetFilePath();

			m_wb.Navigate( m_szURL, NULL, NULL, NULL, NULL );

			UpdateData( FALSE );
			fFound = TRUE;
		}
		else
		{
			szTemp.Format( _T("Unable to open application help = %s."), szHF );
			OutputAMessage( szTemp, TRUE );
		}
	}

	if( !fFound )
	{
		EndDialog( IDOK );
		return TRUE;
	}

	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_HELP ) : NULL;
	if( hIcon )
	{
		SetIcon( hIcon, TRUE );
		SetIcon( hIcon, FALSE );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void HelpDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	if( this->IsWindowVisible() )
	{
		m_wb.SetHeight( cy );
		m_wb.SetWidth( cx );
	}
}

void HelpDlg::OnCancel()
{
	DestroyWindow();
}

void HelpDlg::PostNcDestroy()
{
	delete this;
}

void HelpDlg::OnClickBnBack() 
{
	m_wb.GoBack();
}

void HelpDlg::OnClickBnForward() 
{
	m_wb.GoForward();
}

void HelpDlg::OnClickBnRefresh() 
{
	m_wb.Refresh();
}

void HelpDlg::OnBnGo() 
{
	UpdateData( TRUE );

	if( m_szURL != _T("") )
	{
		m_wb.Navigate( m_szURL, NULL, NULL, NULL, NULL );
	}
}

void HelpDlg::OnNavigateComplete2Browser(LPDISPATCH pDisp, VARIANT FAR* URL) 
{
	m_szURL = m_wb.GetLocationURL();
	UpdateData( FALSE );
}

void HelpDlg::Refresh()
{
	CFileFind f;
	if( f.FindFile( _T("Help\\NSHelp.html") ) )
	{
		f.FindNextFile();
		if( m_szURL != _T("") )
		{
			CString szTemp = f.GetFilePath();
			szTemp += _T("#");
			szTemp += m_szURL;
			m_szURL = szTemp;
		}
		else m_szURL = f.GetFilePath();

		m_wb.Navigate( m_szURL, NULL, NULL, NULL, NULL );

		UpdateData( FALSE );
	}
}

#pragma once

// WizardImportSheet.h : header file
//

#include "WizardImportSheet.h"

/////////////////////////////////////////////////////////////////////////////
// WizardImportSheet

class WizardImportGenSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardImportGenSheet)

// Construction
public:
	WizardImportGenSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	virtual ~WizardImportGenSheet();

// Operations
public:
	virtual void AddPages();

// Overrides

protected:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

// WizardImportGenStart.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportGenStart.h"
#include "WizardImportGenSheet.h"
#include "WizardImportGen.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenStart property page

IMPLEMENT_DYNCREATE(WizardImportGenStart, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportGenStart, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportGenStart::WizardImportGenStart() : CBCGPPropertyPage(WizardImportGenStart::IDD)
{
}

WizardImportGenStart::~WizardImportGenStart()
{
}

void WizardImportGenStart::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportGenStart message handlers

LRESULT WizardImportGenStart::OnWizardNext() 
{
	WizardImportGenSheet* pParent = (WizardImportGenSheet*)GetParent();
	WizardImportGen* pGen = (WizardImportGen*)pParent->GetPage( 1 );
	if( pGen->m_fSelected )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else
		pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportGenStart::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	WizardImportGenSheet* pParent = (WizardImportGenSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportGenStart::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportGenStart::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

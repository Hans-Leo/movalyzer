// HtmlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "test.h"
#include "HtmlDlg.h"
#include "StartView.h"
#include "TestDoc.h"
#include "MainFrm.h"
#include "GraphView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CHtmlDlg dialog

StartView* _pStartView = NULL;

StartView* CHtmlDlg::ShowTheWindow( BOOL fJustRefresh )
{
	CGraphView* pParent = ((CMainFrame*)AfxGetMainWnd())->GetRecordingPane();
	if( !pParent ) return NULL;
	CRect rectWindow;
	pParent->GetClientRect( &rectWindow );
	int nCmdShow = SW_SHOW;

	if( !_pStartView )
	{
		CCreateContext pContext;
		CFrameWnd* pFrameWnd = (CFrameWnd*)AfxGetMainWnd();
		pContext.m_pCurrentDoc = new CTestDoc;
		pContext.m_pNewViewClass = RUNTIME_CLASS( StartView );
		_pStartView = (StartView*)pFrameWnd->CreateView( &pContext );
		_pStartView->SetParent( pParent );
		pParent->m_pStartView = _pStartView;
		_pStartView->OnInitialUpdate();
	}
	else if( _pStartView->IsWindowVisible() ) nCmdShow = SW_HIDE;

	if( fJustRefresh )
	{
		// if hidden, do nothing as it will be shown
		// if visible, we need to hide and have it reshown
		if( nCmdShow == SW_HIDE )
		{
			_pStartView->ShowWindow( nCmdShow );
			nCmdShow = SW_SHOW;
		}
	}

	_pStartView->ShowWindow( nCmdShow );
	_pStartView->MoveWindow( rectWindow );

	return _pStartView;
}

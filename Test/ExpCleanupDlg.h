#pragma once

#include "..\NSSharedGUI\NSTooltipCtrl.h"

// ExpCleanupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ExpCleanupDlg dialog

class ExpCleanupDlg : public CBCGPDialog
{
// Construction
public:
	ExpCleanupDlg(CWnd* pParent = NULL, BOOL fSubject = FALSE);

// Dialog Data
	enum { IDD = IDD_CLEAN };
	BOOL			m_fHWR;
	BOOL			m_fTF;
	BOOL			m_fSeg;
	BOOL			m_fExt;
	BOOL			m_fCon;
	BOOL			m_fErr;
	BOOL			m_fInc;
	BOOL			m_fSubject;
	NSToolTipCtrl	m_tooltip;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	afx_msg void OnChkHwr();
	afx_msg void OnChkExt();
	afx_msg void OnChkInc();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
};

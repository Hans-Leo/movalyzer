#if !defined(AFX_HELPVIEW_H__0F796E3D_FC8E_45AB_AB20_728DE5077CCF__INCLUDED_)
#define AFX_HELPVIEW_H__0F796E3D_FC8E_45AB_AB20_728DE5077CCF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// StartView html view

class StartView : public CHtmlView
{
	DECLARE_DYNCREATE( StartView )
protected:
	StartView();
	virtual ~StartView();

// Operations
public:
	void ShowPage( CString szURL );
	CString ResourceToURL( CString szFile );

// Overrides
public:
	virtual void OnInitialUpdate();
	virtual void OnBeforeNavigate2( LPCTSTR lpszURL, DWORD nFlags, LPCTSTR lpszTargetFrameName, CByteArray& baPostedData, LPCTSTR lpszHeaders, BOOL* pbCancel );
	virtual void OnShowWindow( BOOL bShow, UINT nStatus );

// Implementation
protected:
	BOOL	m_fInit;

// message map
protected:
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_HELPVIEW_H__0F796E3D_FC8E_45AB_AB20_728DE5077CCF__INCLUDED_)

#pragma once

// WizardImportSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportSheet

class WizardImportSheet : public CBCGPPropertySheet
{
	DECLARE_DYNAMIC(WizardImportSheet)

// Construction
public:
	WizardImportSheet( CString szTitle, CWnd* pParentWnd = NULL, UINT iSelectPage = 0 )
		: CBCGPPropertySheet( szTitle, pParentWnd, iSelectPage ) {}
	WizardImportSheet( CWnd* pParentWnd = NULL, UINT iSelectPage = 0 );
	virtual ~WizardImportSheet();

// Attributes
public:
	CString	m_szLastFile;

// Operations
public:
	virtual void AddPages();

// Overrides

protected:
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	DECLARE_MESSAGE_MAP()
};

// WizardSetupSheet.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupSheet.h"

#include "WizardSetupStart.h"
#include "WizardSetupExp.h"
#include "WizardSetupCond.h"
#include "WizardSetupGrp.h"
#include "WizardSetupSubj.h"
#include "WizardSetupEnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardSetupSheet

IMPLEMENT_DYNAMIC(WizardSetupSheet, CBCGPPropertySheet)

BEGIN_MESSAGE_MAP(WizardSetupSheet, CBCGPPropertySheet)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupSheet::WizardSetupSheet( CWnd* pParentWnd, UINT iSelectPage )
	: CBCGPPropertySheet( IDS_WIZ_EXPSETUP, pParentWnd, iSelectPage )
{
	AddPages();
}

WizardSetupSheet::~WizardSetupSheet()
{
	int nNumPages = GetPageCount();
	for( int i = 0; i < nNumPages; i++ )
	{
		CPropertyPage* pPage = GetPage( i );
		if( pPage ) delete pPage;
	}
}

void WizardSetupSheet::AddPages()
{
	m_psh.dwFlags &= ~(PSH_HASHELP);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	EnableVisualManagerStyle( TRUE, TRUE );

	AddPage( new WizardSetupStart );
	AddPage( new WizardSetupExp );
	AddPage( new WizardSetupCond );
	AddPage( new WizardSetupGrp );
//	AddPage( new WizardSetupSubj );
	AddPage( new WizardSetupEnd );

	SetWizardMode();
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupSheet message handlers

BOOL WizardSetupSheet::OnHelpInfo(HELPINFO* pHelpInfo)
{
	CPropertyPage* pPage = GetActivePage();
	if( pPage )
	{
		if( pPage->IsKindOf( RUNTIME_CLASS( WizardSetupStart ) ) )
			return ((WizardSetupStart*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardSetupExp ) ) )
			return ((WizardSetupExp*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardSetupCond ) ) )
			return ((WizardSetupCond*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardSetupGrp ) ) )
			return ((WizardSetupGrp*)pPage)->DoHelp( pHelpInfo );
//		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardSetupSubj ) ) )
//			return ((WizardSetupSubj*)pPage)->DoHelp( pHelpInfo );
		else if( pPage->IsKindOf( RUNTIME_CLASS( WizardSetupEnd ) ) )
			return ((WizardSetupEnd*)pPage)->DoHelp( pHelpInfo );
	}

	return CBCGPPropertySheet::OnHelpInfo(pHelpInfo);
}

// WizardImportStart.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportStart.h"
#include "WizardImportSheet.h"
#include "WizardImportFormat.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportStart property page

IMPLEMENT_DYNCREATE(WizardImportStart, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportStart, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportStart::WizardImportStart() : CBCGPPropertyPage(WizardImportStart::IDD)
{
}

WizardImportStart::~WizardImportStart()
{
}

void WizardImportStart::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportStart message handlers

LRESULT WizardImportStart::OnWizardNext() 
{
	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	WizardImportFormat* pFmt = (WizardImportFormat*)pParent->GetPage( 1 );
	if( pFmt->m_szFormat != _T("") )
		pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportStart::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	WizardImportSheet* pParent = (WizardImportSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportStart::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("importexport.html"), this );
	return TRUE;
}

BOOL WizardImportStart::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

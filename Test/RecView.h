#pragma once

// RecView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRecView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CRecView : public CBCGPFormView
{
	DECLARE_DYNCREATE(CRecView)
protected:
	CRecView();           // protected constructor used by dynamic creation
protected:
	virtual ~CRecView();

// Form Data
public:
	enum { IDD = IDD_RECVIEW };
	CEdit		m_Instr;
	CString		m_szInstr;

// Attributes
public:

// Operations
public:
	void UpdateInstruction( CString szInstr );
	void Enable( BOOL fEnable = TRUE );
	void Clear();

// Overrides
public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnDraw( CDC* pDC );

// Implementation
public:
	void OnClear();
	void OnStopExp();
	void OnStopCond();
	void OnStopRec();
protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize( UINT nType, int cx, int cy );
	DECLARE_MESSAGE_MAP()
};

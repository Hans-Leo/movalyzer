// LeftView.h : interface of the CLeftView class
//

#pragma once

#include "..\Common\MessageTarget.h"
#include "..\Common\MessageManager.h"

interface IExperimentMember;
interface IExperimentCondition;
interface IStimulusElement;
class Objects;
class StartView;

// Tree item types
#define	TI_UNKNOWN					0
#define	TI_EXPERIMENT_FOLDER		1
#define	TI_EXPERIMENT				2
#define	TI_GROUP_FOLDER				3
#define	TI_GROUP					4
#define	TI_SUBJECT_FOLDER			5
#define	TI_SUBJECT					6
#define	TI_CONDITION_FOLDER			7
#define	TI_CONDITION				8
#define	TI_HWRTRIAL_FOLDER			9
#define	TI_FRDTRIAL_FOLDER			10
#define	TI_TRIAL					11
#define	TI_STIMULUS_FOLDER			12
#define	TI_STIMULUS					13
#define	TI_ELEMENT_FOLDER			14
#define	TI_ELEMENT					15
#define	TI_CATEGORY_FOLDER			16
#define	TI_CATEGORY					17
#define	TI_CATEGORY_HOLDER			18
#define	TI_FEEDBACK_FOLDER			19
#define	TI_FEEDBACK					20
#define	TI_IMAGE_FOLDER				21
#define	TI_IMAGE					22
#define TI_QUICK_FOLDER				23
#define TI_ATTACHMENT_FOLDER		24
#define TI_ATTACHMENT				25

// Action Management
#define	ACTION_NONE					0
#define	ACTION_RUNEXPERIMENT		1
#define	ACTION_REPROCESS			2
#define	ACTION_SUMMARIZE			3
#define	ACTION_TEST					4
#define	ACTION_BACKUP				5
#define	ACTION_STIMULUS				6
#define	ACTION_IMPORTEXPORT			7

class CLeftView : public CTreeView, public MessageTarget
{
	DECLARE_DYNCREATE(CLeftView)
protected: // create from serialization only
	CLeftView();
	virtual ~CLeftView();

// Attributes
public:
	// handles to tree folder for each category
	HTREEITEM	m_hExp, m_hGrp, m_hSub, m_hCond, m_hStim, m_hElmt, m_hCat, m_hFb, m_hQ;
	// image list for tree
	CImageList	m_ImageList;
	// persistent * to experiment subjects object
	IExperimentMember*		m_pEML;
	// persistent * to experiment conditions cobject
	IExperimentCondition*	m_pECL;
	// handles to items being dragged from tree to handle of item where dropped on tree
	HTREEITEM   m_hitemDrag, m_hitemDrop;
	// are we dragging a tree item?
	BOOL		m_fDragging;
	// image list for drag & drop
	CImageList*	m_pImageList;
	// have we initialized from start of app?
	BOOL		m_fInit;
	// For searching - to start from next & last searched text
	HTREEITEM	m_hNext;
	CString		m_szFindText;
	BOOL		m_fFindPartial;
	// start page
	CWnd		m_wndDesktop;
	StartView*	m_pStartView;
	// drag-n-drop condition var
	static CString m_szDropCondID;
	// list of quick links
	CStringList	m_lstQuick;

// Operations
public:
	// show startup
	void ShowStartPage();
	void HideStartPage();
	// cleanup of system when exiting
	void Cleanup();
	// function to determine what should be in title bar of app
	void DoTitle();
	// fill the tree with all the data
	void FillTree();
	// refresh the tree
	void Refresh();
	// refresh the experiments in the tree
	void RefreshExperiments();
	// refresh the groups in the tree
	void RefreshGroups();
	// refresh the subjects in the tree
	void RefreshSubjects();
	// refresh the conditions in the tree
	void RefreshConditions();
	// refresh the stimuli in the tree
	void RefreshStimuli();
	// refresh the elements in the tree
	void RefreshElements();
	// refresh the categories in the tree
	void RefreshCategories();
	// refresh the feedback in the tree
	void RefreshFeedbacks();
	// refresh the quick links
	void RefreshQuickLinks();
	// function to display a trial of a subject in the tree
	HTREEITEM ShowTrial( CString szExp, CString szSubj, CString szGrp,
						 CString szCond, CString szTrial,
						 HTREEITEM hRoot, CString szName = _T(""), BOOL fValid = TRUE,
						 BOOL fInit = FALSE, BOOL fNothing = FALSE, BOOL fPCX = FALSE );
	// function to ensure a given trial is visible (usually during running an experiment)
	void EnsureTrialVisible( HTREEITEM hItem, BOOL fParent = FALSE );
	// show the trials of a given subject in a given experiment/group
	BOOL ShowTrials( CString szExp, CString szSubj, CString szGrp,
					 HTREEITEM hRoot, BOOL fShow = FALSE,
					 BOOL fForce = TRUE, BOOL fJustVerify = FALSE );
	// subject trial info (summary)
	void GetSubjectTrialInfo( CString szExpID, CString szGrpID, CString szSubjID,
							  CStringList* pLstCond, short& nTrials, short& nGoodTrials,
							  short& nBadTrials, COleDateTime& dtExpStart, COleDateTime& dtExpEnd,
							  COleDateTime& dtProcessed, BOOL& fInSummarize );
	// remove all trial folders (of subjects) for an experiment (generally used for reproc)
	void HideTrials( CString szExpID = _T(""), CString szGrpID = _T(""), CString szSubjID = _T("") );
	// write tree memory to file (when exiting or leaving a user)
	void WriteMemory();
	// read tree memory from file (when opening app or changing user)
	void ReadMemory();
	// used by graph dialog to synchronize the trial in the tree with the
	// trial being viewed in the graph dialog
	BOOL MoveAndFind( CString szItem, BOOL fEnsureVisible = FALSE, BOOL fFindParent = FALSE );
	// do subject questionnaire
	void Questionnaire( CString szExp, CString szGrp, CString szSubj );
	// has the thread been terminated for experiment or group reprocessing?
	BOOL TerminateThread();
	// obtains the physical location of the selected trial in the tree
	BOOL GetSelectedTrialLocation( CString& szFile, CString& szPath, BOOL& fQuick );
	// testing input device
	void OnDigTest();
	// stopping some process that's occuring (like running exp. or reprocessing)
	void OnUpdateObjStop(CCmdUI* pCmdUI);
	void OnObjStop();
	// sort groups & conditions for analysis graph
	BOOL OnESortgroups2( CString szExp = _T("") );
	BOOL OnESortconds2( CString szExp = _T("") );
	BOOL OnESortsubjs2( CString szExp = _T("") );
	// backup raw trial
	BOOL BackupRaw( CString szExp, CString szGrp, CString szSubj,
					CString szRaw );
	// backup entire system
	void BackupSystem();
	void BackupSystemThread();
	// function to verify that the backup path exists & is valid
	static BOOL VerifyBackupPath();
	// action state of system: recording, testing input device, etc.
	static void SetActionState( UINT nVal );
	static UINT GetActionState();
	// display statistics/analysis graph for an experiment
	void ShowStatGraph( CString szExpID );
	BOOL GetRunExpInfo( HTREEITEM hItem, CString& szExpID, CString& szExp,
						CString& szGrpID, CString& szGrp, CString& szSubjID,
						CString& szSubj );
	// find & return tree handle of the first item in the tree with id = szID starting from hRoot
	HTREEITEM Find( HTREEITEM hRoot, CString szID );
	// find & return tree handle of a specific experiment's group's subject
	HTREEITEM Find( CString szExpID, CString szGrpID, CString szSubjID );
	// find & return tree handle of the trial of specified experiment
	HTREEITEM FindTrial( CString szTrial, CString szExp );
	// find & return tree handle of an item starting at hStart - partial text search allowed
	// ...will search complete text, not just ID or desc, if partial
	HTREEITEM FindItem( CString szItem, HTREEITEM hStart = NULL, BOOL fPartial = FALSE,
						BOOL fSkipCheck = FALSE, BOOL fSkipChildren = FALSE );
	// select the specified item in the tree (used by external classes)
	void SelectItem( HTREEITEM hItem );
	// import an experiment
	BOOL OnEImport( CString& szExpID );
	// show a subject's trials in the tree
	void DoSTrials( HTREEITEM hItem );
	// function to change visible text descriptions of all items in tree matching criteria
	void ChangeAllInstanceNames( DWORD dwType, CString szOld, CString szNew );
	// find an item in the tree
	void Find( CString szID );
	// get the list of elements from the tree
	void GetElementList( CStringList* lst );
	// an external file was dropped on left view
	void OnDroppedExternal( HTREEITEM hItem, CString szFile, BOOL fProcess = TRUE, BOOL fMsg = TRUE );
protected:
	// show a subject's trials in the tree
	void DoSTrials( BOOL fDblClick );
	// message that mouse button released during tree drag & drop operation
	void OnButtonUp();
	// display analysis chart
	void ShowStatGraph( BOOL fEqual, UINT nMode );
	void ShowStatGraph( CString szExpID, BOOL fEqual, UINT nMode );
	void OnUpdateStatGraph( CCmdUI* pCmdUI );
	// edit selected condition
	void EditCondition( UINT iSelectPage = 0 );
	// subject reprocessing
	void ProcessSubject( HTREEITEM hItem, CString szExpID, CString szGrpID,
						 CString szSubjID, BOOL fReProcess, BOOL fSumm = TRUE );

// Overrides
	virtual BOOL PreTranslateMessage( MSG* );
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	// receive messages from messagemanager
	virtual void Message( LPARAM msg, WPARAM val, BOOL* fStop );
protected:
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
protected:
	// client-server upload and download of data
	BOOL DoCopy( Objects* pObj, BOOL& fCancel, CString szID = _T(""), BOOL fRecurse = FALSE, BOOL fFirst = TRUE );
	BOOL DoUpload( Objects* pObj, BOOL& fCancel, CString szID = _T(""),
				   BOOL fOverwrite = FALSE, BOOL fAsk = TRUE, BOOL fRecurse = FALSE );
	void FolderUpload( Objects* pObj, HTREEITEM hRoot, BOOL fRecurse = FALSE );
	void FolderCopy( Objects* pObj, HTREEITEM hRoot, BOOL fRecurse = FALSE );
	// fDoCopy = TRUE, copy from server to client, otherwise upload to server
	void FolderTransfer( Objects* pObj, HTREEITEM hRoot, BOOL fRecurse, BOOL fDoCopy );

	// export analysis data for import into statistical packages
	public:
	void ExportSummarization( int nGroupMode );
	BOOL Export( CString& szWMsg, CString szExp, CString szSubjOnly = _T(""), CString szSubjGroup = _T(""),
				 BOOL fWizard = FALSE, BOOL fWFiles = FALSE, BOOL fWTrials = FALSE, BOOL fWMembers = FALSE,
				 BOOL fWShow = FALSE, CString szWPath = _T("") );

// message map functions
public:
	afx_msg void OnContextMenu( CWnd*, CPoint );
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSfAdd();
	afx_msg void OnEfAdd();
	afx_msg void OnFileImport();
	afx_msg void OnEEdit2();
	afx_msg void OnEDelete();
	afx_msg void OnEAddgroup();
	afx_msg void OnGfAdd();
	afx_msg void OnGfAddgroups();
	afx_msg void OnEfFind();
	afx_msg void OnGfFind();
	afx_msg void OnGEdit2();
	afx_msg void OnGDelete();
	afx_msg void OnGAddsubj();
	afx_msg void OnSfFind();
	afx_msg void OnSDelete();
	afx_msg void OnSEdit2();
	afx_msg void OnSRemove();
	afx_msg void OnGRemove();
	afx_msg void OnSfAddsubjects();
	afx_msg void OnEAddcondition();
	afx_msg void OnCfAdd();
	afx_msg void OnCfFind();
	afx_msg void OnCfAddconditions();
	afx_msg void OnCEdit2();
	afx_msg void OnCDelete();
	afx_msg void OnCRemove();
	afx_msg void OnSExp();
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCReps();
	afx_msg void OnESum();
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateEfAdd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEfFind(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEfImport(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEAddcondition(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEAddgroup(CCmdUI* pCmdUI);
	afx_msg void OnUpdateESum(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGfAdd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGfFind(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGfAddgroups(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGAddsubj(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGRemove(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSfAdd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSfAddsubjects(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSfFind(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSExp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSRemove(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCfAdd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCfAddconditions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCfFind(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCDelete(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCRemove(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCReps(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCRelationships(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGRelationships(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSRelationships(CCmdUI* pCmdUI);
	afx_msg void OnCRelationships();
	afx_msg void OnGRelationships();
	afx_msg void OnSRelationships();
	afx_msg void OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnTView();
	afx_msg void OnUpdateTView(CCmdUI* pCmdUI);
	afx_msg void OnTNotes();
	afx_msg void OnUpdateTNotes(CCmdUI* pCmdUI );
	afx_msg void OnTViewTf();
	afx_msg void OnUpdateTViewTf(CCmdUI* pCmdUI);
	afx_msg void OnTChartHwr();
	afx_msg void OnTChartTf();
	afx_msg void OnTChartHwrRT();
	afx_msg void OnTChartTfRT();
	afx_msg void OnTChartHwr3D();
	afx_msg void OnTChartTf3D();
	afx_msg void OnUpdateTChart(CCmdUI* pCmdUI);
	afx_msg void OnTRedo();
	afx_msg void OnUpdateTRedo(CCmdUI* pCmdUI);
	afx_msg void OnSReprocess();
	afx_msg void OnUpdateSReprocess(CCmdUI* pCmdUI);
	afx_msg void OnSProcess();
	afx_msg void OnUpdateSProcess(CCmdUI* pCmdUI);
	afx_msg void OnEReprocess();
	afx_msg void OnUpdateEReprocess(CCmdUI* pCmdUI);
	afx_msg void OnEProcess();
	afx_msg void OnUpdateEProcess(CCmdUI* pCmdUI);
	afx_msg void OnSQuest();
	afx_msg void OnUpdateSQuest(CCmdUI* pCmdUI);
	afx_msg void OnGQuest();
	afx_msg void OnUpdateGQuest(CCmdUI* pCmdUI);
	afx_msg void OnEClean();
	afx_msg void OnUpdateEClean(CCmdUI* pCmdUI);
	afx_msg void OnTViewExt();
	afx_msg void OnUpdateTViewExt(CCmdUI* pCmdUI);
	afx_msg void OnTViewErr();
	afx_msg void OnUpdateTViewErr(CCmdUI* pCmdUI);
	afx_msg void OnTViewDis();
	afx_msg void OnTViewWrd();
	afx_msg void OnSClean();
	afx_msg void OnUpdateSClean(CCmdUI* pCmdUI);
	afx_msg void OnGReprocess();
	afx_msg void OnUpdateGReprocess(CCmdUI* pCmdUI);
	afx_msg void OnGProcess();
	afx_msg void OnUpdateGProcess(CCmdUI* pCmdUI);
	afx_msg void OnEStopreprocess();
	afx_msg void OnUpdateEStopreprocess(CCmdUI* pCmdUI);
	afx_msg void OnTReprocess();
	afx_msg void OnUpdateTReprocess(CCmdUI* pCmdUI);
	afx_msg void OnEViewInc();
	afx_msg void OnUpdateEViewInc(CCmdUI* pCmdUI);
	afx_msg void OnEViewErr();
	afx_msg void OnUpdateEViewErr(CCmdUI* pCmdUI);
	afx_msg void OnEViewErs();
	afx_msg void OnUpdateEViewErs(CCmdUI* pCmdUI);
	afx_msg void OnEReportprocset();
	afx_msg void OnUpdateEReportprocset(CCmdUI* pCmdUI);
	afx_msg void OnCReprocess();
	afx_msg void OnUpdateCReprocess(CCmdUI* pCmdUI);
	afx_msg void OnCProcess();
	afx_msg void OnUpdateCProcess(CCmdUI* pCmdUI);
	afx_msg void OnEAAvgs();
	afx_msg void OnUpdateEAAvgs(CCmdUI* pCmdUI);
	afx_msg void OnEAAvgs2();
	afx_msg void OnUpdateEAAvgs2(CCmdUI* pCmdUI);
	afx_msg void OnEAStddevs();
	afx_msg void OnUpdateEAStddevs(CCmdUI* pCmdUI);
	afx_msg void OnEAStddevs2();
	afx_msg void OnUpdateEAStddevs2(CCmdUI* pCmdUI);
	afx_msg void OnEATrial();
	afx_msg void OnUpdateEATrial(CCmdUI* pCmdUI);
	afx_msg void OnTReprocessNoext();
	afx_msg void OnUpdateTReprocessNoext(CCmdUI* pCmdUI);
	afx_msg void OnESortgroups();
	afx_msg void OnESortconds();
	afx_msg void OnESortsubjs();
	afx_msg void OnUpdateESortgroups(CCmdUI* pCmdUI);
	afx_msg void OnUpdateESortconds(CCmdUI* pCmdUI);
	afx_msg void OnUpdateESortsubjs(CCmdUI* pCmdUI);
	afx_msg void OnSMove();
	afx_msg void OnUpdateSMove(CCmdUI* pCmdUI);
	afx_msg void OnSShowtrials();
	afx_msg void OnUpdateSShowtrials(CCmdUI* pCmdUI);
	afx_msg void OnEBackup();
	afx_msg void OnUpdateEBackup(CCmdUI* pCmdUI);
	afx_msg void OnGBackup();
	afx_msg void OnUpdateGBackup(CCmdUI* pCmdUI);
	afx_msg void OnSBackup();
	afx_msg void OnUpdateSBackup(CCmdUI* pCmdUI);
	afx_msg void OnTCondition();
	afx_msg void OnUpdateTCondition(CCmdUI* pCmdUI);
	afx_msg void OnTExperiment();
	afx_msg void OnTConditionReps();
	afx_msg void OnUpdateTConditionReps(CCmdUI* pCmdUI);
	afx_msg void OnEAAll();
	afx_msg void OnUpdateEAAll(CCmdUI* pCmdUI);
	afx_msg void OnEAEw();
	afx_msg void OnUpdateEAEw(CCmdUI* pCmdUI);
	afx_msg void OnEAUew();
	afx_msg void OnUpdateEAUew(CCmdUI* pCmdUI);
	afx_msg void OnEACombo();
	afx_msg void OnUpdateEACombo(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEExport();
	afx_msg void OnUpdateEExport(CCmdUI* pCmdUI);
	afx_msg void OnTDelete();
	afx_msg void OnUpdateTDelete(CCmdUI* pCmdUI);
	afx_msg void OnCDup();
	afx_msg void OnUpdateCDup(CCmdUI* pCmdUI);
	afx_msg void OnSChartall();
	afx_msg void OnUpdateSChartall(CCmdUI* pCmdUI);
	afx_msg void OnElDelete();
	afx_msg void OnUpdateElDelete(CCmdUI* pCmdUI);
	afx_msg void OnElEdit();
	afx_msg void OnUpdateElEdit(CCmdUI* pCmdUI);
	afx_msg void OnElfAdd();
	afx_msg void OnUpdateElfAdd(CCmdUI* pCmdUI);
	afx_msg void OnElfFind();
	afx_msg void OnUpdateElfFind(CCmdUI* pCmdUI);
	afx_msg void OnStDelete();
	afx_msg void OnUpdateStDelete(CCmdUI* pCmdUI);
	afx_msg void OnStEdit();
	afx_msg void OnUpdateStEdit(CCmdUI* pCmdUI);
	afx_msg void OnStfAdd();
	afx_msg void OnUpdateStfAdd(CCmdUI* pCmdUI);
	afx_msg void OnStfFind();
	afx_msg void OnUpdateStfFind(CCmdUI* pCmdUI);
	afx_msg void OnElfAddelements();
	afx_msg void OnUpdateElfAddelements(CCmdUI* pCmdUI);
	afx_msg void OnStAddelements();
	afx_msg void OnUpdateStAddelements(CCmdUI* pCmdUI);
	afx_msg void OnElRemove();
	afx_msg void OnUpdateElRemove(CCmdUI* pCmdUI);
	afx_msg void OnElView();
	afx_msg void OnUpdateElView(CCmdUI* pCmdUI);
	afx_msg void OnElChart();
	afx_msg void OnUpdateElChart(CCmdUI* pCmdUI);
	afx_msg void OnStView();
	afx_msg void OnUpdateStView(CCmdUI* pCmdUI);
	afx_msg void OnStChart();
	afx_msg void OnUpdateStChart(CCmdUI* pCmdUI);
	afx_msg void OnElRelationships();
	afx_msg void OnUpdateElRelationships(CCmdUI* pCmdUI);
	afx_msg void OnStRelationships();
	afx_msg void OnUpdateStRelationships(CCmdUI* pCmdUI);
	afx_msg void OnElDisplay();
	afx_msg void OnUpdateElDisplay(CCmdUI* pCmdUI);
	afx_msg void OnStDisplay();
	afx_msg void OnUpdateStDisplay(CCmdUI* pCmdUI);
	afx_msg void OnElDup();
	afx_msg void OnUpdateElDup(CCmdUI* pCmdUI);
	afx_msg void OnEReportsubjects();
	afx_msg void OnUpdateEReportsubjects(CCmdUI* pCmdUI);
	afx_msg void OnStDup();
	afx_msg void OnUpdateStDup(CCmdUI* pCmdUI);
	afx_msg void OnCTestlex();
	afx_msg void OnUpdateCTestlex(CCmdUI* pCmdUI);
	afx_msg void OnTViewSeg();
	afx_msg void OnUpdateTViewSeg(CCmdUI* pCmdUI);
	afx_msg void OnTViewCon();
	afx_msg void OnUpdateTViewCon(CCmdUI* pCmdUI);
	void OnEEdit();
	void OnGEdit();
	void OnSEdit();
	void OnCEdit();
	void OnTChart();
	afx_msg void OnStfRefresh();
	afx_msg void OnUpdateStfRefresh(CCmdUI *pCmdUI);
	afx_msg void OnCatDelete();
	afx_msg void OnUpdateCatDelete(CCmdUI *pCmdUI);
	afx_msg void OnCatEdit();
	afx_msg void OnUpdateCatEdit(CCmdUI *pCmdUI);
	afx_msg void OnCatfAdd();
	afx_msg void OnUpdateCatfAdd(CCmdUI *pCmdUI);
	afx_msg void OnCatfFind();
	afx_msg void OnUpdateCatfFind(CCmdUI *pCmdUI);
	afx_msg void OnFbDelete();
	afx_msg void OnUpdateFbDelete(CCmdUI *pCmdUI);
	afx_msg void OnFbEdit();
	afx_msg void OnUpdateFbEdit(CCmdUI *pCmdUI);
	afx_msg void OnFbfAdd();
	afx_msg void OnUpdateFbfAdd(CCmdUI *pCmdUI);
	afx_msg void OnFbfFind();
	afx_msg void OnUpdateFbfFind(CCmdUI *pCmdUI);
	afx_msg void OnEReportquest();
	afx_msg void OnUpdateEReportquest(CCmdUI *pCmdUI);
	afx_msg void OnUpdateEfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateECopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateGfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateGCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateStfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateStCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateElfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateElCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCatfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCatCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFbfCopyall(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFbCopyall(CCmdUI *pCmdUI);
	afx_msg void OnEfCopyall();
	afx_msg void OnECopyall();
	afx_msg void OnGfCopyall();
	afx_msg void OnGCopyall();
	afx_msg void OnSfCopyall();
	afx_msg void OnSCopyall();
	afx_msg void OnCfCopyall();
	afx_msg void OnCCopyall();
	afx_msg void OnStfCopyall();
	afx_msg void OnStCopyall();
	afx_msg void OnElfCopyall();
	afx_msg void OnElCopyall();
	afx_msg void OnCatfCopyall();
	afx_msg void OnCatCopyall();
	afx_msg void OnFbfCopyall();
	afx_msg void OnFbCopyall();
	afx_msg void OnUpdateEfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateEUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateGfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateGUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateStfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateStUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateElfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateElUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCatfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateCatUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFbfUpload(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFbUpload(CCmdUI *pCmdUI);
	afx_msg void OnEfUpload();
	afx_msg void OnEUpload();
	afx_msg void OnGfUpload();
	afx_msg void OnGUpload();
	afx_msg void OnSfUpload();
	afx_msg void OnSUpload();
	afx_msg void OnCfUpload();
	afx_msg void OnCUpload();
	afx_msg void OnStfUpload();
	afx_msg void OnStUpload();
	afx_msg void OnElfUpload();
	afx_msg void OnElUpload();
	afx_msg void OnCatfUpload();
	afx_msg void OnCatUpload();
	afx_msg void OnFbfUpload();
	afx_msg void OnFbUpload();
	afx_msg void OnEExpsumParam();
	afx_msg void OnUpdateEExpsumParam(CCmdUI *pCmdUI);
	afx_msg void OnEExpsumSubm();
	afx_msg void OnUpdateEExpsumSubm(CCmdUI *pCmdUI);
	afx_msg void OnMQCopy();
	afx_msg void OnMQUpload();
	afx_msg void OnUpdateMQCopy(CCmdUI *pCmdUI);
	afx_msg void OnUpdateMQUpload(CCmdUI *pCmdUI);
	afx_msg void OnTLinearity();
	afx_msg void OnUpdateTLinearity(CCmdUI *pCmdUI);
	afx_msg void OnEReportSummaryB();
	afx_msg void OnUpdateEReportSummaryB(CCmdUI* pCmdUI);
	afx_msg void OnEReportSummaryD();
	afx_msg void OnUpdateEReportSummaryD(CCmdUI* pCmdUI);
	afx_msg void OnIView();
	afx_msg void OnIViewF();
	afx_msg void OnIProcess();
	afx_msg void OnUpdateIProcess(CCmdUI* pCmdUI);
	afx_msg void OnSExport();
	afx_msg void OnUpdateSExport(CCmdUI* pCmdUI);
	afx_msg void OnStStimEdit();
	afx_msg void OnUpdateStStimEdit(CCmdUI* pCmdUI);
	afx_msg void OnSLastExp();
	afx_msg void OnUpdateSLastExp(CCmdUI* pCmdUI);
	afx_msg void OnExportWizard();
	afx_msg void OnUpdateExportWizard(CCmdUI* pCmdUI);
	afx_msg void OnFind();
	afx_msg void OnUpdateFind(CCmdUI* pCmdUI);
	afx_msg void OnFindNext();
	afx_msg void OnUpdateFindNext(CCmdUI* pCmdUI);
	afx_msg void OnSRecoverPass();
	afx_msg void OnUpdateSRecoverPass(CCmdUI* pCmdUI);
	afx_msg void OnSRestorePass();
	afx_msg void OnUpdateSRestorePass(CCmdUI* pCmdUI);
	afx_msg void OnSCondSequence();
	afx_msg void OnUpdateSCondSequence(CCmdUI* pCmdUI );
	afx_msg void OnTQuick();
	afx_msg void OnUpdateTQuick(CCmdUI* pCmdUI);
	afx_msg void OnEEditSum();
	afx_msg void OnGEditExp();
	afx_msg void OnUpdateGEditExp(CCmdUI*pCmdUI);
	afx_msg void OnSEditExp();
	afx_msg void OnUpdateSEditExp(CCmdUI*pCmdUI);
	afx_msg void OnCEditExp();
	afx_msg void OnUpdateCEditExp(CCmdUI*pCmdUI);
	void ViewExcludes( CString szExp, CString szWhich );
	afx_msg void OnEViewExclSubj();
	afx_msg void OnEViewExclFeat();
	afx_msg void OnEViewExclQuest();
	afx_msg void OnUpdateEViewExcl(CCmdUI*pCmdUI);
	afx_msg void OnViewAttachments();
	afx_msg void OnEDup();
	afx_msg void OnUpdateEDup(CCmdUI* pCmdUI);
	afx_msg void OnCDupMulti();
	afx_msg void OnUpdateCDupMulti(CCmdUI* pCmdUI);
	afx_msg void OnValidateExp();
	afx_msg void OnUpdateValidateExp(CCmdUI* pCmdUI);
	afx_msg void OnBuildNormDB();
	afx_msg void OnUpdateBuildNormDB(CCmdUI* pCmdUI);
	afx_msg void OnViewNormDB();
	afx_msg void OnSBuildNormDB();
	afx_msg void OnUpdateSBuildNormDB(CCmdUI* pCmdUI);
	afx_msg void OnViewZScores();
	afx_msg void OnViewZHistory();
	afx_msg void OnEViewZHistorySummary();
	afx_msg void OnUpdateEViewZHistorySummary(CCmdUI* pCmdUI);
	afx_msg void OnSViewZHistorySummary();
	afx_msg void OnUpdateSViewZHistorySummary(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()
};

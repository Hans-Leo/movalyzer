#pragma once

// WizardImportPath.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportPath dialog

#include "ColumnSelectDlg.h"

class WizardImportPath : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportPath)

// Construction
public:
	WizardImportPath();
	~WizardImportPath();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_PATH };
	CString			m_szPath;
	BOOL			m_fHeaders;
	int				m_nSkip;
	BOOL			m_fRemainder;
	int				m_nCount;
	CString			m_szName;
	CString			m_szExt;
	ColumnSelectDlg	m_dlgColSel;
	NSToolTipCtrl	m_tooltip;
	CBCGPComboBox	m_cboDelim;
	CString			m_szDelimName;
	CString			m_szDelim;

// Overrides
public:
	virtual LRESULT OnWizardBack();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	BOOL CheckFile( BOOL fNoMsg = FALSE );
	virtual BOOL OnInitDialog();
	afx_msg void OnBnBrowse();
	afx_msg void OnChangeEditPath();
	afx_msg void OnBnCols();
	afx_msg void OnBnView();
	afx_msg void OnBnClickedChkRemainder();
	afx_msg void OnChangeDelim();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

#include "..\NSSharedGUI\NSTooltipCtrl.h"

// SelectUserDlg dialog

class SelectUserDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(SelectUserDlg)

public:
	SelectUserDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~SelectUserDlg();

// Dialog Data
	enum { IDD = IDD_SELUSER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);

public:
	CBCGPComboBox	m_cboUsers;
	BOOL			m_fOverwrite;
	BOOL			m_fSubItems;
	BOOL			m_fHasSubItems;
	CString			m_szUserID;
	NSToolTipCtrl	m_tooltip;

public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
};

// CG: This file was added by the Splash Screen component.

#ifndef _SPLASH_SCRN_
#define _SPLASH_SCRN_

// Splash.h : header file
//

/////////////////////////////////////////////////////////////////////////////
//   Splash Screen class

class CSplashWnd : public CWnd
{
// Construction
protected:
	CSplashWnd();
public:
	~CSplashWnd();
	virtual void PostNcDestroy();

// Attributes:
public:
	CBitmap m_bitmap;

// Operations
public:
	static void EnableSplashScreen(BOOL bEnable = TRUE);
	static void ShowSplashScreen(CWnd* pParentWnd = NULL);
	static void RemoveSplashScreen();
	static BOOL PreTranslateAppMessage(MSG* pMsg);

// Implementation
	void HideSplashScreen();
protected:
	BOOL Create(CWnd* pParentWnd = NULL);
	static BOOL c_bShowSplashWnd;
	static CSplashWnd* c_pSplashWnd;

// message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()
};

#endif

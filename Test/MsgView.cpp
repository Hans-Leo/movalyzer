// TestView.cpp : implementation of the CMsgView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "MsgView.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "GraphView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMsgView

IMPLEMENT_DYNCREATE(CMsgView, CListView)

BEGIN_MESSAGE_MAP(CMsgView, CListView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	ON_WM_CONTEXTMENU()
	ON_WM_MOUSEMOVE()
	ON_COMMAND( ID_SELECTALL, OnSelectAll )
	ON_COMMAND( ID_EDIT_COPY, OnEditCopy )
	ON_COMMAND( IDM_CLEAR_MESSAGES, ClearMessages )
	ON_COMMAND( IDM_O_LOGDB, CMainFrame::OnOLogdb )
	ON_UPDATE_COMMAND_UI( IDM_O_LOGDB, CMainFrame::OnUpdateOLogdb )
	ON_COMMAND( IDM_O_LOGGRAPH, CMainFrame::OnOLoggraph )
	ON_UPDATE_COMMAND_UI( IDM_O_LOGGRAPH, CMainFrame::OnUpdateOLoggraph )
	ON_COMMAND( IDM_O_LOGPROC, CMainFrame::OnOLogproc )
	ON_UPDATE_COMMAND_UI( IDM_O_LOGPROC, CMainFrame::OnUpdateOLogproc )
	ON_COMMAND( IDM_O_LOGTABLET, CMainFrame::OnOLogtablet )
	ON_UPDATE_COMMAND_UI( IDM_O_LOGTABLET, CMainFrame::OnUpdateOLogtablet )
	ON_COMMAND( IDM_O_LOGUI, CMainFrame::OnOLogui )
	ON_UPDATE_COMMAND_UI( IDM_O_LOGUI, CMainFrame::OnUpdateOLogui )
	ON_COMMAND( IDM_O_LOGALL, CMainFrame::OnOLogall )
	ON_COMMAND( IDM_O_LOGNONE, CMainFrame::OnOLognone )
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMsgView construction/destruction

CMsgView::CMsgView()
{
}

CMsgView::~CMsgView()
{
}

BOOL CMsgView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CListView::PreCreateWindow(cs);
}

void CMsgView::ClearMessages()
{
	CListCtrl& refCtrl = GetListCtrl();
	refCtrl.DeleteAllItems();
}

/////////////////////////////////////////////////////////////////////////////
// CMsgView drawing

void CMsgView::OnDraw(CDC* pDC)
{
}

void CMsgView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	CListCtrl& lst = GetListCtrl();
	lst.SetExtendedStyle( LVS_EX_FULLROWSELECT );
	ModifyStyle( LVS_TYPEMASK, LVS_REPORT | LVS_NOCOLUMNHEADER );

	CString szTemp;
	szTemp.LoadString( IDS_COL_RESULTS );
	int nWidth = 5000;
	lst.InsertColumn( 0, szTemp, LVCFMT_LEFT, nWidth );
}

/////////////////////////////////////////////////////////////////////////////
// CMsgView message handlers

void CMsgView::OnContextMenu( CWnd* pWnd, CPoint point )
{
	// if nothing in list control or nothing selected, flag it
	BOOL fNothing = FALSE;
	CListCtrl &lst = GetListCtrl();
	if( ( lst.GetItemCount() == 0 ) || ( lst.GetSelectedCount() <= 0 ) ) fNothing = TRUE;

	CMenu menu;
	menu.LoadMenu( IDR_POPUP_OUTPUT );
	CMenu* pSubMenu = menu.GetSubMenu( 0 );
	if( fNothing ) pSubMenu->RemoveMenu( ID_EDIT_COPY, MF_BYCOMMAND );
	if( AfxGetApp()->m_pMainWnd->IsKindOf( RUNTIME_CLASS( CMainFrame ) ) )
	{
		CBCGPPopupMenu* pPopupMenu = new CBCGPPopupMenu;
		if( !pPopupMenu->Create( this, point.x, point.y, (HMENU)pSubMenu->m_hMenu, FALSE, TRUE ) )
			return;

		((CMainFrame*)AfxGetApp()->m_pMainWnd)->OnShowPopupMenu( pPopupMenu );
		UpdateDialogControls( this, FALSE );
	}

	SetFocus();
}

void CMsgView::OnEditCopy()
{
	HGLOBAL hGlobal;	// global memory handle
	LPSTR lpszData;	// pointer to clipboard data
	ULONG nSize = 0;	// size of clipboard data

	// if nothing in list control or nothing selected, do nothing
	CListCtrl &lst = GetListCtrl();
	if( ( lst.GetItemCount() == 0 ) || ( lst.GetSelectedCount() <= 0 ) )
	{
		BCGPMessageBox( _T("You must select what you want to copy.") );
		return;
	}

	// determine the # of items to copy and the total data size
	POSITION pos = lst.GetFirstSelectedItemPosition();
	CString szItem, szData;
	char c;
	int nIdx = 0;
	while( pos )
	{
		nIdx = lst.GetNextSelectedItem( pos );
		if( nIdx != -1 )
		{
			szItem = lst.GetItemText( nIdx, 0 );
			c = 13;		// carrieage return
			szItem += c;
			c = 10;		// linefeed
			szItem += c;
			szData += szItem;
		}
	}
	nSize = szData.GetLength();

	// open & empty the clipboard
	OpenClipboard();
	EmptyClipboard();

	// allocate memory for data
	hGlobal = GlobalAlloc( GMEM_ZEROINIT, nSize + 1 );
	if( hGlobal == NULL ) return;

	// lock global handle to access it
	lpszData = (LPSTR)GlobalLock( hGlobal );

	// copy data to buffer
	for( UINT i = 0; i < nSize; i++ )
		*(lpszData + i ) = szData.GetAt( i );

	// unlock global, set clipboard data & close
	GlobalUnlock( hGlobal );
	SetClipboardData( CF_TEXT, hGlobal );
	CloseClipboard();
}

void CMsgView::OnSize(UINT nType, int cx, int cy) 
{
	CListView::OnSize(nType, cx, cy);
}

void CMsgView::OnSetFocus(CWnd* pOldWnd) 
{
	CListView::OnSetFocus(pOldWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Results Window: This shows all user activities and results.");
	pMF->ShowStatusText( szMsg );
}

void CMsgView::OnKillFocus(CWnd* pNewWnd) 
{
	CListView::OnKillFocus(pNewWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Ready.");
	pMF->ShowStatusText( szMsg );
}

#define	ENTER_KEY	13
void CMsgView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if( nChar == ENTER_KEY )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CGraphView* pGV = pMF->GetRecordingPane();
		// End trial (user hits key to stop viewing completed trial)
		if( pGV ) pGV->OnKeyDown( nChar );
	}
}

void CMsgView::OnSelectAll()
{
	CListCtrl &lst = GetListCtrl();
	int nCount = lst.GetItemCount();
	for( int i = 0; i < nCount; lst.SetItemState( i++, LVIS_SELECTED, LVIS_SELECTED ) );
}

void CMsgView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CListView::OnMouseMove( nFlags, point );
}

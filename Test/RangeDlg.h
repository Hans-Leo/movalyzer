#if !defined(AFX_RANGEDLG_H__5F8B77D3_56A5_11D4_8BA4_00104BC7E2C8__INCLUDED_)
#define AFX_RANGEDLG_H__5F8B77D3_56A5_11D4_8BA4_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RangeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// RangeDlg dialog

class RangeDlg : public CDialog
{
// Construction
public:
	RangeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RangeDlg)
	enum { IDD = IDD_RANGES };
	BOOL	m_fXRange;
	double	m_dXMin;
	double	m_dXMax;
	BOOL	m_fYRange;
	double	m_dYMin;
	double	m_dYMax;
	//}}AFX_DATA

// Overrides
	//{{AFX_VIRTUAL(RangeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(RangeDlg)
	afx_msg void OnChkXRange();
	afx_msg void OnChkYRange();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RANGEDLG_H__5F8B77D3_56A5_11D4_8BA4_00104BC7E2C8__INCLUDED_)

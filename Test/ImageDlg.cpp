// ImageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "ImageDlg.h"
#include "..\NSSharedGUI\MemDC.h"
#include "..\Common\Shared.h"
#include <WinSpool.h>

#define IDM_IMG_PRINT		0x0010
#define IDM_IMG_EXP_BMP		0x0011
#define IDM_IMG_EXP_PNG		0x0012
#define IDM_IMG_EXP_GIF		0x0013
#define IDM_IMG_EXP_JPG		0x0014

///////////////////////////////////////////////////////////////////////////////
// ImageDlg dialog

IMPLEMENT_DYNAMIC(ImageDlg, CBCGPDialog)

BEGIN_MESSAGE_MAP(ImageDlg, CBCGPDialog)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()

ImageDlg::ImageDlg( CWnd* pParent ) : CBCGPDialog( ImageDlg::IDD, pParent )
{
	m_pPCX = NULL;
	m_nX = 0;
	m_nY = 0;
}

ImageDlg::ImageDlg( CString szImage, CWnd* pParent ) : CBCGPDialog( ImageDlg::IDD, pParent )
{
	m_szImage = szImage;
	m_pPCX = NULL;
	m_nX = 0;
	m_nY = 0;
}

ImageDlg::~ImageDlg()
{
	if( m_pPCX )
	{
		m_pPCX->SetEscape( 1 );
		delete m_pPCX;
	}
	if( m_bmpPrint.m_hObject ) m_bmpPrint.DeleteObject();
}

void ImageDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE, m_st1);
	DDX_Control(pDX, IDC_SCROLLBAR_V, m_vbar);
	DDX_Control(pDX, IDC_SCROLLBAR_H, m_hbar);
}

void ImageDlg::LoadImage( CString szExt )
{
	// TypeID from CxImage library
	DWORD dwType = CxImage::GetTypeIdFromName( szExt );
	m_pPCX = new CxImage( m_szImage, dwType );

	// image height/width
	DWORD dwWidth = m_pPCX->GetWidth();
	DWORD dwHeight = m_pPCX->GetHeight();

	// resize window based upon size of image...restricting to resolution of display + buffer
	CRect rc;
	::GetWindowRect( ::GetDesktopWindow(), &rc );
#define DTBUFFER	20
	int nDTWidth = rc.right - rc.left;
	int nDTHeight = rc.bottom - rc.top;
	int nWinWidth = ( (int)dwWidth > nDTWidth ) ? nDTWidth - DTBUFFER : dwWidth;
	int nWinHeight = ( (int)dwHeight > nDTHeight ) ? nDTHeight - DTBUFFER : dwHeight;
	SetWindowPos( NULL, 0, 0, nWinWidth, nWinHeight, SWP_NOMOVE );
	CenterWindow();

	// set the scroll bar size/positions
	int cyHScroll = GetSystemMetrics( SM_CYHSCROLL );
	int cxVScroll = GetSystemMetrics( SM_CXVSCROLL );
	GetWindowRect( &rc );
	ScreenToClient( &rc );
	OnSize( SIZE_RESTORED, rc.right + rc.left, rc.bottom + rc.left );

	m_st1.GetClientRect( &rectStaticClient );
	rectStaticClient.NormalizeRect();
	m_size.cx = rectStaticClient.Size().cx;
	m_size.cy = rectStaticClient.Size().cy;
	m_size.cx = rectStaticClient.Width();    // zero based
	m_size.cy = rectStaticClient.Height();    // zero based

	horz.cbSize = sizeof( SCROLLINFO );
	horz.fMask = SIF_ALL;
	horz.nMin = 0;
	horz.nMax = (int)dwWidth - m_size.cx;
	horz.nPage = horz.nMax / 10;
	horz.nPos = 0;
	horz.nTrackPos = 0;
	m_hbar.SetScrollInfo( &horz );

	vert.cbSize = sizeof( SCROLLINFO );
	vert.fMask = SIF_ALL;
	vert.nMin = 0;
	vert.nMax = MAX( 0, (int)dwHeight - m_size.cy );
	vert.nPage = vert.nMax / 10;
	vert.nPos = 0;
	vert.nTrackPos = 0;
	m_vbar.SetScrollInfo( &vert );
}

// ImageDlg message handlers

BOOL ImageDlg::OnInitDialog()
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// set dialog icon
	CWinApp* pApp = AfxGetApp();
	HICON hIcon = pApp ? pApp->LoadIcon( IDR_CAT ) : NULL;
	SetIcon( hIcon, TRUE );
	SetIcon( hIcon, FALSE );

	// add to control/system menu
	ASSERT((IDM_IMG_PRINT & 0xFFF0) == IDM_IMG_PRINT);
	ASSERT(IDM_IMG_PRINT < 0xF000);
	CMenu* pSysMenu = GetSystemMenu( FALSE );
	if( pSysMenu )
	{
		m_mnuConvert.CreatePopupMenu();
		m_mnuConvert.AppendMenu( MF_STRING, IDM_IMG_EXP_BMP, _T("&BMP") );
		m_mnuConvert.AppendMenu( MF_STRING, IDM_IMG_EXP_PNG, _T("&PNG") );
		m_mnuConvert.AppendMenu( MF_STRING, IDM_IMG_EXP_GIF, _T("&GIF") );
		m_mnuConvert.AppendMenu( MF_STRING, IDM_IMG_EXP_JPG, _T("&JPG") );

		pSysMenu->InsertMenu( 0, MF_BYPOSITION | MF_SEPARATOR );
		pSysMenu->InsertMenu( 0, MF_BYPOSITION | MF_POPUP, (UINT_PTR)m_mnuConvert.m_hMenu, _T("&Convert To") );
 		pSysMenu->InsertMenu( 0, MF_BYPOSITION, IDM_IMG_PRINT, "&Print Image..." );
 		m_bmpPrint.LoadBitmap( IDB_PRINT );
 		pSysMenu->SetMenuItemBitmaps( 0, MF_BYPOSITION, &m_bmpPrint, &m_bmpPrint );
	}

	// Title
	CString szTitle;
	GetWindowText( szTitle );
	szTitle += _T(" - ");
	szTitle += m_szImage;
	SetWindowText( szTitle );

	// draw image
	if( m_szImage.GetLength() > 3 )
	{
		m_szImage.MakeUpper();
		CString szExt = m_szImage.Right( 3 );
		LoadImage( szExt );
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void ImageDlg::DrawResize()
{
	CRect rectHScroll, rectVScroll, rc;
	GetWindowRect( &rc );
	ScreenToClient( &rc );
	m_hbar.GetWindowRect( rectHScroll );
	ScreenToClient( &rectHScroll );
	m_vbar.GetWindowRect( rectVScroll );
	ScreenToClient( &rectVScroll );

	int nScrollHeight = rectHScroll.Height();
	int nScrollWidth = rectVScroll.Width();
	int cx = rc.right + rc.left;
	int cy = rc.bottom + rc.left;
	CRect rectSizeBox( cx - nScrollWidth, cy - nScrollHeight, cx, cy );

	CDC* pDC = GetDC();
	CBrush br;
	br.CreateSolidBrush( globalData.clrBarFace );
	pDC->FillRect( rectSizeBox, &br );
	CBCGPVisualManager::GetInstance()->OnDrawStatusBarSizeBox( pDC, NULL, rectSizeBox );
	ReleaseDC( pDC );
}

void ImageDlg::OnPaint()
{
	CBCGPDialog::OnPaint();
	DrawResize();
}

void ImageDlg::OnSize( UINT nType, int cx, int cy )
{
	CBCGPDialog::OnSize( nType, cx, cy );

	if( m_st1.GetSafeHwnd() == NULL ) return;

	// rectangles of scrollbars & associated height/width
	CRect rectHScroll, rectVScroll;
	m_hbar.GetWindowRect( rectHScroll );
	m_vbar.GetWindowRect( rectVScroll );
	int nScrollHeight = rectHScroll.Height();
	int nScrollWidth = rectVScroll.Width();

	// move the image frame to the size of the window - the scrollbar thicknesses
	m_st1.MoveWindow( 0, 0, cx - nScrollWidth, cy - nScrollHeight, FALSE );

	// move scrollbars
	m_hbar.MoveWindow( 0, cy - nScrollHeight, cx - nScrollWidth, nScrollHeight, FALSE );
	m_vbar.MoveWindow( cx - nScrollWidth, 0, nScrollWidth, cy - nScrollHeight + 1, FALSE );

	// image height/width
	if( m_pPCX )
	{
		DWORD dwWidth = m_pPCX->GetWidth();
		DWORD dwHeight = m_pPCX->GetHeight();

		// new image frame rect/sizes
		m_st1.GetClientRect( &rectStaticClient );
		rectStaticClient.NormalizeRect();
		m_size.cx = rectStaticClient.Size().cx;
		m_size.cy = rectStaticClient.Size().cy;
		m_size.cx = rectStaticClient.Width();    // zero based
		m_size.cy = rectStaticClient.Height();    // zero based

		// adjust horiz scroll bar range
		horz.nMax = (int)dwWidth - m_size.cx;
		horz.nPage = horz.nMax / 10;
		m_hbar.SetScrollInfo( &horz );

		// adjust vert scroll bar range
		vert.nMax = MAX( 0, (int)dwHeight - m_size.cy );
		vert.nPage = vert.nMax / 10;
		m_vbar.SetScrollInfo( &vert );
	}

	// redraw
	RedrawWindow();
}

BOOL ImageDlg::OnEraseBkgnd( CDC* pDC )
{
	if( m_pPCX )
	{
		CMemDC* pMemDC = NULL;
		pDC = pMemDC = new CMemDC( pDC );
		m_pPCX->Draw( pDC->GetSafeHdc(), m_nX, m_nY );
		delete pMemDC;
	}
	else return CBCGPDialog::OnEraseBkgnd( pDC );

	return TRUE;
}

void ImageDlg::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
   // Get the minimum and maximum scroll-bar positions.
   int minpos;
   int maxpos;
   pScrollBar->GetScrollRange(&minpos, &maxpos); 
   maxpos = pScrollBar->GetScrollLimit();

   // Get the current position of scroll box.
   int curpos = pScrollBar->GetScrollPos();

   // Determine the new position of scroll box.
   switch (nSBCode)
   {
	   case SB_LEFT:      // Scroll to far left.
		  curpos = minpos;
		  break;

	   case SB_RIGHT:      // Scroll to far right.
		  curpos = maxpos;
		  break;

	   case SB_ENDSCROLL:   // End scroll.
		  break;

	   case SB_LINELEFT:      // Scroll left.
		  if (curpos > minpos)
			 curpos--;
		  break;

	   case SB_LINERIGHT:   // Scroll right.
		  if (curpos < maxpos)
			 curpos++;
		  break;

	   case SB_PAGELEFT:    // Scroll one page left.
	   {
		  // Get the page size. 
		  SCROLLINFO   info;
		  pScrollBar->GetScrollInfo(&info, SIF_ALL);
	   
		  if (curpos > minpos)
		  curpos = max(minpos, curpos - (int) info.nPage);
	   }
      break;

	   case SB_PAGERIGHT:      // Scroll one page right.
	   {
		  // Get the page size. 
		  SCROLLINFO   info;
		  pScrollBar->GetScrollInfo(&info, SIF_ALL);

		  if (curpos < maxpos)
			 curpos = min(maxpos, curpos + (int) info.nPage);
	   }
      break;

	   case SB_THUMBPOSITION: // Scroll to absolute position. nPos is the position
		  curpos = nPos;      // of the scroll box at the end of the drag operation.
		  break;

	   case SB_THUMBTRACK:   // Drag scroll box to specified position. nPos is the
		  curpos = nPos;     // position that the scroll box has been dragged to.
		  break;
   }

	// Set the new position of the thumb (scroll box).
	pScrollBar->SetScrollPos( curpos );
	m_nX = -curpos;
	InvalidateRect( &rectStaticClient );

	CBCGPDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void ImageDlg::OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
   // Get the minimum and maximum scroll-bar positions.
   int minpos;
   int maxpos;
   pScrollBar->GetScrollRange(&minpos, &maxpos); 
   maxpos = pScrollBar->GetScrollLimit();

   // Get the current position of scroll box.
   int curpos = pScrollBar->GetScrollPos();

   // Determine the new position of scroll box.
   switch (nSBCode)
   {
	   case SB_LEFT:      // Scroll to far left.
		  curpos = minpos;
		  break;

	   case SB_RIGHT:      // Scroll to far right.
		  curpos = maxpos;
		  break;

	   case SB_ENDSCROLL:   // End scroll.
		  break;

	   case SB_LINELEFT:      // Scroll left.
		  if (curpos > minpos)
			 curpos--;
		  break;

	   case SB_LINERIGHT:   // Scroll right.
		  if (curpos < maxpos)
			 curpos++;
		  break;

	   case SB_PAGELEFT:    // Scroll one page left.
	   {
		  // Get the page size. 
		  SCROLLINFO   info;
		  pScrollBar->GetScrollInfo(&info, SIF_ALL);
	   
		  if (curpos > minpos)
		  curpos = max(minpos, curpos - (int) info.nPage);
	   }
      break;

	   case SB_PAGERIGHT:      // Scroll one page right.
	   {
		  // Get the page size. 
		  SCROLLINFO   info;
		  pScrollBar->GetScrollInfo(&info, SIF_ALL);

		  if (curpos < maxpos)
			 curpos = min(maxpos, curpos + (int) info.nPage);
	   }
      break;

	   case SB_THUMBPOSITION: // Scroll to absolute position. nPos is the position
		  curpos = nPos;      // of the scroll box at the end of the drag operation.
		  break;

	   case SB_THUMBTRACK:   // Drag scroll box to specified position. nPos is the
		  curpos = nPos;     // position that the scroll box has been dragged to.
		  break;
   }

   // Set the new position of the thumb (scroll box).
	pScrollBar->SetScrollPos( curpos );
	m_nY = -curpos;
	InvalidateRect( &rectStaticClient );

	CBCGPDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

void ImageDlg::OnSysCommand( UINT nID, LPARAM lParam )
{
	if( nID == IDM_IMG_PRINT ) OnPrint();
	else if( ( nID >= IDM_IMG_EXP_BMP ) && ( nID <= IDM_IMG_EXP_JPG ) ) OnConvert( nID );
	else CBCGPDialog::OnSysCommand( nID, lParam );
}

void ImageDlg::OnPrint()
{
	if( !m_pPCX || !m_pPCX->IsValid() )
	{
		BCGPMessageBox( _T("Image is invalid."), MB_ICONERROR );
		return;
	}

	CPrintDialog printDlg( FALSE );
	if( printDlg.DoModal() == IDCANCEL ) return;

	CDC dc;
	if( !dc.Attach( printDlg.GetPrinterDC() ) )
	{
		BCGPMessageBox( _T("No printer found!"), MB_ICONERROR );
		return;
	} 

	HBITMAP hbm = m_pPCX->MakeBitmap( dc );
	CBitmap* pBitmap = CBitmap::FromHandle( hbm );
	if( !pBitmap )
	{
		BCGPMessageBox( _T("Unable to create an image to print."), MB_ICONERROR );
		return;
	}

	CWaitCursor crs;
	dc.m_bPrinting = TRUE;

	// Initialise print document details
	DOCINFO di;    
	::ZeroMemory( &di, sizeof( DOCINFO ) );
	di.cbSize = sizeof( DOCINFO );
	di.lpszDocName = m_szImage;
	BOOL bPrintingOK = dc.StartDoc( &di ); // Begin a new print job 

	// Get the printing extents
	// and store in the m_rectDraw field of a 
	// CPrintInfo object

	CPrintInfo Info;
	Info.SetMaxPage( 1 ); // just one page
	CONST LPDEVMODE pDevMode = printDlg.GetDevMode(); // pointer to structure with device data
	if( pDevMode ) pDevMode->dmOrientation = DMORIENT_LANDSCAPE;

	for( UINT page = Info.GetMinPage(); page <= Info.GetMaxPage() && bPrintingOK; page++ )
	{
		dc.StartPage();    // begin new page

		Info.m_nCurPage = page;
		BITMAP bm;
		pBitmap->GetBitmap( &bm );
		int w = bm.bmWidth; 
		int h = bm.bmHeight; 
		Info.m_rectDraw.SetRect( 0, 0, w, h ); 
		// create memory device context
		CDC memDC; 
		memDC.CreateCompatibleDC( &dc );
		CBitmap *pBmp = memDC.SelectObject( pBitmap );
		memDC.SetMapMode( dc.GetMapMode() );
		dc.SetStretchBltMode( HALFTONE );
		// now copy
		dc.BitBlt( 0, 0, w, h, &memDC, 0, 0, SRCCOPY );
		// clean up
		memDC.SelectObject( pBmp );
		bPrintingOK = ( dc.EndPage() > 0 );   // end page
	} 
	if( bPrintingOK ) dc.EndDoc(); // end a print job
	else dc.AbortDoc();           // abort job. 

	crs.Restore();
}

void ImageDlg::OnConvert( UINT nID )
{
	CFileFind ff;
	if( !m_pPCX || ( m_szImage == "" ) || !ff.FindFile( m_szImage ) ) return;

	CString szImgExt, szMsg, szFilter;
	DWORD dwType = 0;

	BOOL fBMP = ( nID == IDM_IMG_EXP_BMP );
	BOOL fPNG = ( nID == IDM_IMG_EXP_PNG );
	BOOL fGIF = ( nID == IDM_IMG_EXP_GIF );
	BOOL fJPG = ( nID == IDM_IMG_EXP_JPG );

	if( fBMP ) { szImgExt = _T("BMP"); dwType = CXIMAGE_FORMAT_BMP; }
	else if( fPNG ) { szImgExt = _T("PNG"); dwType = CXIMAGE_FORMAT_PNG; }
	else if( fGIF ) { szImgExt = _T("GIF"); dwType = CXIMAGE_FORMAT_GIF; }
	else if( fJPG ) { szImgExt = _T("JPG"); dwType = CXIMAGE_FORMAT_JPG; }

	ASSERT( szImgExt != _T("") );

	// Save as dialog
	CString szImage = m_szImage.Left( m_szImage.GetLength() - 3 );
	szImage += szImgExt;
	szFilter.Format( _T("%s Files (*.%s)|*.%s|"), szImgExt, szImgExt, szImgExt );
	CFileDialog d( FALSE, szFilter, szImage, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
				   szFilter, this );
	if( d.DoModal() == IDCANCEL ) return;
	szImage = d.GetPathName();

	// convert
	DWORD dwOrigType = CxImage::GetTypeIdFromName( m_szImage );
	CxImage img( m_szImage, dwOrigType );
	if( fJPG )
	{
		if( !img.IsGrayScale() ) img.IncreaseBpp( 24 );
		img.SetJpegQuality( 80 );
	}
	bool fRes = img.Save( szImage, dwType );
	if( fRes )
	{
		if( BCGPMessageBox( _T("Conversion completed.\n\nWould you like to open windows explorer to this file?"), MB_YESNO ) == IDYES )
		{
			int nPos = szImage.ReverseFind( '\\' );
			if( nPos != -1 )
			{
				CString szRoot = szImage.Left( nPos ), szTemp;
				szTemp.Format( _T("explorer /ROOT,%s,/SELECT,%s"), szRoot, szImage );
				WinExec( szTemp, SW_SHOW );
			}
		}
	}
	else BCGPMessageBox( _T("Error in converting.") );
}

// OrderColDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "OrderColDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// OrderColDlg dialog

BEGIN_MESSAGE_MAP(OrderColDlg, CBCGPDialog)
	ON_BN_CLICKED(IDC_BN_UP, OnClickBnUp)
	ON_BN_CLICKED(IDC_BN_DOWN, OnClickBnDown)
END_MESSAGE_MAP()

OrderColDlg::OrderColDlg(CStringList* pLstNums, CStringList* pLstNames, CWnd* pParent /*=NULL*/)
	: CBCGPDialog(OrderColDlg::IDD, pParent), m_pLstNums( pLstNums ), m_pLstNames( pLstNames )
{
}

void OrderColDlg::DoDataExchange(CDataExchange* pDX)
{
	CBCGPDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST, m_list);
	DDX_Control(pDX, IDC_BN_UP, m_bnUp);
	DDX_Control(pDX, IDC_BN_DOWN, m_bnDown);
}

/////////////////////////////////////////////////////////////////////////////
// OrderColDlg message handlers

void OrderColDlg::OnClickBnUp() 
{
	int nIdx = m_list.GetCurSel();
	if( nIdx == LB_ERR ) return;
	if( nIdx <= 0 ) return;

	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL;

	pos1 = m_pLstNums->FindIndex( nIdx );
	pos2 = m_pLstNums->FindIndex( nIdx - 1 );
	szItem1 = m_pLstNums->GetAt( pos1 );
	szItem2 = m_pLstNums->GetAt( pos2 );
	m_pLstNums->SetAt( pos2, szItem1 );
	m_pLstNums->SetAt( pos1, szItem2 );

	pos1 = m_pLstNames->FindIndex( nIdx );
	pos2 = m_pLstNames->FindIndex( nIdx - 1 );
	szItem1 = m_pLstNames->GetAt( pos1 );
	szItem2 = m_pLstNames->GetAt( pos2 );
	m_pLstNames->SetAt( pos2, szItem1 );
	m_pLstNames->SetAt( pos1, szItem2 );

	FillList();

	m_list.SetCurSel( nIdx - 1 );
}

void OrderColDlg::OnClickBnDown() 
{
	int nIdx = m_list.GetCurSel();
	if( nIdx == LB_ERR ) return;
	int nCnt = m_list.GetCount();
	if( nIdx >= ( nCnt - 1 ) ) return;

	CString szItem1, szItem2;
	POSITION pos1 = NULL, pos2 = NULL;

	pos1 = m_pLstNums->FindIndex( nIdx );
	pos2 = m_pLstNums->FindIndex( nIdx + 1 );
	szItem1 = m_pLstNums->GetAt( pos1 );
	szItem2 = m_pLstNums->GetAt( pos2 );
	m_pLstNums->SetAt( pos2, szItem1 );
	m_pLstNums->SetAt( pos1, szItem2 );

	pos1 = m_pLstNames->FindIndex( nIdx );
	pos2 = m_pLstNames->FindIndex( nIdx + 1 );
	szItem1 = m_pLstNames->GetAt( pos1 );
	szItem2 = m_pLstNames->GetAt( pos2 );
	m_pLstNames->SetAt( pos2, szItem1 );
	m_pLstNames->SetAt( pos1, szItem2 );

	FillList();

	m_list.SetCurSel( nIdx + 1 );
}

BOOL OrderColDlg::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CBCGPDialog::PreTranslateMessage(pMsg);
}

void OrderColDlg::FillList()
{
	if( !m_pLstNums || !m_pLstNames ) return;

	m_list.ResetContent();

	CString szItem;
	POSITION pos = m_pLstNames->GetHeadPosition();
	while( pos )
	{
		szItem = m_pLstNames->GetNext( pos );
		m_list.AddString( szItem );
	}
}

BOOL OrderColDlg::OnInitDialog() 
{
	CBCGPDialog::OnInitDialog();
	EnableVisualManagerStyle( TRUE, TRUE );

	// button images
	m_bnUp.SetImage( IDB_UP );
	m_bnDown.SetImage( IDB_DOWN );

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_BN_UP );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEUP, _T("Move Up") );
	pWnd = GetDlgItem( IDC_BN_DOWN );
	if( pWnd ) m_tooltip.AddTool( pWnd, IDS_MOVEDOWN, _T("Move Down") );
	pWnd = GetDlgItem( IDC_LIST );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("The list shows the order of your items: x, then y, then z."), _T("List") );

	FillList();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

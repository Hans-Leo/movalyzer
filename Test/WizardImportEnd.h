#pragma once

// WizardImportEnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportEnd dialog

class WizardImportEnd : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportEnd)

// Construction
public:
	WizardImportEnd();
	~WizardImportEnd();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_END };
	CString			m_szPath;
	CString			m_szExpID;
	CString			m_szExpDesc;
	CString			m_szCondID;
	CString			m_szCondDesc;
	CString			m_szGrpID;
	CString			m_szGrpDesc;
	CString			m_szSubjID;
	CString			m_szSubjName;
	CString			m_szFormat;
	BOOL			m_fCont;
	CString			m_szCols;
	BOOL			m_fRetain;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual LRESULT OnWizardNext();
	virtual BOOL OnWizardFinish();
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	void DoNext();
	void DoOwn();
	void DoGripper();
	void DoSingle();
	BOOL ConfirmCreate();
	virtual BOOL OnInitDialog();
	afx_msg void OnChkContinue();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#pragma once

// WizardImportCond.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardImportCond dialog

class WizardImportCond : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardImportCond)

// Construction
public:
	WizardImportCond();
	~WizardImportCond();

// Dialog Data
	enum { IDD = IDD_WIZ_IMPORT_COND };
	CBCGPButton		m_chkRetain;
	CBCGPComboBox	m_Cbo;
	CString			m_szCond;
	CString			m_szID;
	CString			m_szDesc;
	BOOL			m_fRetain;
	CString			m_szInstr;
	CString			m_szLex;
	CString			m_szMin;
	CString			m_szMax;
	int				m_nMin;
	int				m_nMax;
	double			m_dStrokeLength;
	double			m_dRangeLength;
	double			m_dStrokeDirection;
	double			m_dRangeDirection;
	int				m_nSkip;
	CString			m_szNotes;
	BOOL			m_fStimulus;
	CString			m_szStimulus;
	CString			m_szExpID;
	CStringList		m_existing;
	CBCGPPropertySheet* m_pParent;
	int				m_nExpIdx;
	int				m_nGrpIdx;
	CToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnSelchangeCboConds();
	afx_msg void OnBnCreatecond();
	virtual BOOL OnInitDialog();
	afx_msg void OnChkRetain();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

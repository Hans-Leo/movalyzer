// WizardImportExp.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardImportExp.h"
#include "WizardImportFormat.h"
#include "WizardImportPath.h"
#include "WizardImportSheet.h"
#include "WizardImportGenStart.h"
#include "WizardImportCond.h"
#include "..\DataMod\DataMod.h"
#include "WizardSetupSheet.h"
#include "Experiments.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardImportExp property page

IMPLEMENT_DYNCREATE(WizardImportExp, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardImportExp, CBCGPPropertyPage)
	ON_BN_CLICKED(IDC_BN_CREATEEXP, OnBnCreateexp)
	ON_CBN_SELCHANGE(IDC_CBO_EXPS, OnSelchangeCboExps)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardImportExp::WizardImportExp() : CBCGPPropertyPage(WizardImportExp::IDD)
{
	m_szID = _T("");
	m_szDesc = _T("");
	m_fUpdated = FALSE;
	m_fNew = FALSE;
	m_pParent = NULL;
	m_fGenWiz = FALSE;
	m_fImage = FALSE;
	m_fGripper = FALSE;
	m_nCondIdx = 4;
}

WizardImportExp::~WizardImportExp()
{
}

void WizardImportExp::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBO_EXPS, m_Cbo);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szDesc);
}

/////////////////////////////////////////////////////////////////////////////
// WizardImportExp message handlers

BOOL WizardImportExp::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	// Tooltip support
	m_tooltip.Create( this );
	m_tooltip.Activate( TRUE );
	CWnd* pWnd = GetDlgItem( IDC_CBO_EXPS );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Select an existing experiment.") );
	pWnd = GetDlgItem( IDC_BN_CREATEEXP );
	if( pWnd ) m_tooltip.AddTool( pWnd, _T("Create a new experiment to use.") );

	// File/class is used in multiple wizards with different # of pages
	// DEFAULT page index # is in constructor (for import wizard)
	// if Generate data wizard, adjust index # appropriately
	// also, format page is not applicable if gen wiz
	m_pParent = (CBCGPPropertySheet*)GetParent();
	pWnd = m_pParent->GetPage( 0 );
	if( pWnd->IsKindOf( RUNTIME_CLASS( WizardImportGenStart ) ) )
	{
		m_fGenWiz = TRUE;
		m_nCondIdx--;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardImportExp::PreTranslateMessage( MSG* pMsg )
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);

	return CDialog::PreTranslateMessage(pMsg);
}

void WizardImportExp::OnBnCreateexp() 
{
	WizardSetupSheet d( this );
	if( d.DoModal() != ID_WIZFINISH ) return;

	if( ( m_fGripper && ( d.m_nExpType != EXP_TYPE_GRIPPER ) ) ||
		( m_fImage && ( d.m_nExpType != EXP_TYPE_IMAGE ) ) )
	{
		CString szMsg = _T("Newly created experiment type does not match import file type.\nWould you like to convert the experiment to type ");
		if( m_fGripper ) szMsg += _T("Grip-Force?");
		else if( m_fImage ) szMsg += _T("Image?");
		szMsg += _T("\n\nIf you do not, you will need to adjust the experiment type to view the imported item.");
		if( BCGPMessageBox( szMsg, MB_YESNO ) == IDYES )
		{
			Experiments exp;
			if( SUCCEEDED( exp.Find( d.m_szExpID ) ) )
			{
				short nType = m_fGripper ? EXP_TYPE_GRIPPER : EXP_TYPE_IMAGE;
				exp.PutType( nType );
				exp.Modify();
			}
		}
	}
	m_szID = d.m_szExpID;
	m_szDesc = d.m_szExpDesc;
	m_nExpType = d.m_nExpType;
	CString szItem, szDelim;
	::GetDelimiter( szDelim );
	szItem.Format( _T("%s%s%s"), m_szDesc, szDelim, m_szID );
	m_Cbo.SetCurSel( m_Cbo.AddString( szItem ) );

	UpdateData( FALSE );
	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

LRESULT WizardImportExp::OnWizardBack() 
{
	m_pParent->SetWizardButtons( PSWIZB_NEXT );

	return CBCGPPropertyPage::OnWizardBack();
}

BOOL WizardImportExp::OnSetActive() 
{
	if( m_szID != _T("") ) m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	// Get file extension to determine type of experiments to filter
	if( !m_fGenWiz )
	{
		WizardImportFormat* pFmt = (WizardImportFormat*)m_pParent->GetPage( 1 );
		if( pFmt->m_szFormat == _T("SINGLE TRIAL OR IMAGE") )
		{
			WizardImportPath* pPath = (WizardImportPath*)m_pParent->GetPage( 2 );
			CString szExt = pPath->m_szPath.Right( 3 );
			szExt.MakeUpper();
			if( ( szExt == _T("PCX") ) || ( szExt == _T("BMP") ) || ( szExt == _T("JPG") ) ||
				( szExt == _T("GIF") ) || ( szExt == _T("PNG") ) )
				m_fImage = TRUE;
			else if( szExt == _T("FRD") )
				m_fGripper = TRUE;
		}
	}

	// FILL EXPERIMENT LIST
	CString szItem, szID, szSel;
	short nType = EXP_TYPE_HANDWRITING;
	// empty
	m_Cbo.ResetContent();
	// exp object
	Experiments exp;
	if( !exp.IsValid() ) return TRUE;
	// loop through exp and filter as necessary
	HRESULT hr = exp.ResetToStart();
	while( SUCCEEDED( hr ) )
	{
		// descript + ID
		exp.GetID( szID );
		exp.GetDescriptionWithID( szItem );
		// exp type
		exp.GetType( nType );
		// filter
		if( ( m_fGripper && ( nType == EXP_TYPE_GRIPPER ) ) ||
			( m_fImage && ( nType == EXP_TYPE_IMAGE ) ) ||
			( !m_fImage && !m_fGripper ) )
		{
			m_Cbo.AddString( szItem );
			if( szID == m_szID ) szSel = szItem;
		}
		// next exp
		hr = exp.GetNext();
	}
	// select from combo if already an exp from before
	if( szSel != _T("") ) m_Cbo.SelectString( -1, szSel );
	// otherwise, if only 1 in the list, select it
	else if( m_Cbo.GetCount() == 1 )
	{
		m_Cbo.SetCurSel( 0 );
		OnSelchangeCboExps();
	}

	return CBCGPPropertyPage::OnSetActive();
}

void WizardImportExp::OnSelchangeCboExps() 
{
	if( m_Cbo.GetCurSel() < 0 ) return;

	CString szItem, szDelim, szID, szDesc;
	::GetDelimiter( szDelim );
	TRIM( szDelim );

	m_Cbo.GetLBText( m_Cbo.GetCurSel(), szItem );
	int nPos = szItem.Find( szDelim );
	if( nPos == -1 ) return;
	m_szID = szItem.Mid( nPos + 2 );
	m_szDesc = szItem.Left( nPos - 1 );

	Experiments exp;
	if( !exp.IsValid() ) return;
	HRESULT hr = exp.Find( m_szID );
	if( FAILED( hr ) )
	{
		m_fNew = TRUE;
		return;
	}
	m_fNew = FALSE;
	exp.GetNotes( m_szNotes );
	exp.GetType( m_nExpType );

	UpdateData( FALSE );

	m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
}

LRESULT WizardImportExp::OnWizardNext() 
{
	WizardImportCond* pCond = (WizardImportCond*)m_pParent->GetPage( m_nCondIdx );
	if( pCond->m_szID != _T("") )
		m_pParent->SetWizardButtons( PSWIZB_BACK | PSWIZB_NEXT );
	else m_pParent->SetWizardButtons( PSWIZB_BACK );

	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardImportExp::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	CBCGPPropertySheet* pParent = (CBCGPPropertySheet*)GetParent();
	if( pParent )
	{
		if( pParent->IsKindOf( RUNTIME_CLASS( WizardImportSheet ) ) )
			::GoToHelp( _T("importexport.html"), this );
		else
			::GoToHelp( _T("datageneration_wizard.html"), this );
	}

	return TRUE;
}

BOOL WizardImportExp::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

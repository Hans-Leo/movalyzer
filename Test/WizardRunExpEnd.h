#pragma once

// WizardRunExpEnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpEnd dialog

class WizardRunExpEnd : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardRunExpEnd)

// Construction
public:
	WizardRunExpEnd();
	~WizardRunExpEnd();

// Dialog Data
	enum { IDD = IDD_WIZ_RUNEXP_END };
	CString	m_szExpID;
	CString	m_szExpDesc;
	CString	m_szExpType;
	CString	m_szGrpID;
	CString	m_szGrpDesc;
	CString	m_szSubjID;
	CString	m_szSubjName;

// Overrides
public:
	virtual BOOL OnWizardFinish();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	BOOL DoHelp( HELPINFO* );
protected:
	BOOL ConfirmCreate();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

#if !defined(AFX_IMPORTDLG_H__E1DAADA3_64C1_11D4_8BAA_00104BC7E2C8__INCLUDED_)
#define AFX_IMPORTDLG_H__E1DAADA3_64C1_11D4_8BAA_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ImportDlg dialog

class ImportDlg : public CDialog
{
// Construction
public:
	ImportDlg(BOOL fCopy = FALSE, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ImportDlg)
	enum { IDD = IDD_IMPORT };
	CButton	m_chkCopy;
	CListBox	m_lstCond;
	CListBox	m_lstSubj;
	CListBox	m_lstGrp;
	CListBox	m_lstExp;
	BOOL	m_fCopy;
	//}}AFX_DATA
	CStringList*	m_pLstExp;
	CStringList*	m_pLstGrp;
	CStringList*	m_pLstSubj;
	CStringList*	m_pLstCond;
	BOOL			m_fCopyEn;

// Overrides
	//{{AFX_VIRTUAL(ImportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(ImportDlg)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTDLG_H__E1DAADA3_64C1_11D4_8BAA_00104BC7E2C8__INCLUDED_)

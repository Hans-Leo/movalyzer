// Processing.cpp : implementation of all graphing-related
// ...functions from MainFrm.cpp
//

#include "stdafx.h"
#include "Test.h"
#include "..\Common\DefFun.h"

#include "MainFrm.h"
#include "LeftView.h"
#include "GraphDlg.h"
#include "Experiments.h"
#include "Conditions.h"
#include "Feedbacks.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

void CMainFrame::ChartHwr( CString szFile, double dFrequency, BOOL fModeless,
						   DWORD dwPenUpColor, BOOL fChangePenUpColor,
						   int nMinPenPressure )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;
	szData.Format( _T("Charting %s."), szFile );
	OutputAMessage( szData, TRUE );

	if( !fModeless )
	{
		CGraphDlg dlg( CHART_HWR, this, &m_wm );
		dlg.m_szFile = szFile;
		dlg.m_dFrequency = dFrequency;
		dlg.m_fChangePenUpColor = fChangePenUpColor;
		dlg.m_dwPenUpColor = dwPenUpColor;
		dlg.m_nPenPressure = nMinPenPressure;
		dlg.m_fFeedback = FALSE;
		dlg.DoModal();
	}
	else
	{
		CGraphDlg* gd = new CGraphDlg( dFrequency, nMinPenPressure, szFile, CHART_HWR,
									   FALSE, _T(""), 0, 0, this, _T(""), dwPenUpColor,
									   fChangePenUpColor );
		if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );
	}
}

void CMainFrame::ChartHwr( CString szExp, CString szGrp, CString szSubj,
						   CString szTrial, double dFrequency, int nMinPenPressure )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;

	// Main list to present to graph dlg
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// File structure
	if( szTrial.Right( 3 ) == _T("HWR") )
		GetHWRMask( szExp, szGrp, szSubj, szData );
	else if( szTrial.Right( 3 ) == _T("FRD") )
		GetFRDMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() )
		::SortFilesAlpha( pFileList, szData );
	else
		::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		if( szTrial.Right( 3 ) == _T("HWR") )
			GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		else if( szTrial.Right( 3 ) == _T("FRD") )
			GetFRD( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 5, 2 );
	szData.Format( IDS_AL_CHARTHWR, szExp, szGrp, szSubj, szCond, atoi( szTrialNum ) );
	OutputAMessage( szData, TRUE );

	CGraphDlg* gd = new CGraphDlg( dFrequency, nMinPenPressure, pFileList, szTrial, CHART_HWR,
								   FALSE, _T(""), 0, 0, this, FALSE, clrPenup, TRUE,
								   FALSE, NULL, NULL, &m_wm );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );

	// add to window manager
	m_wm.AddItem( gd );
}

void CMainFrame::ChartHwr3D( CString szExp, CString szGrp, CString szSubj,
							 CString szTrial, double dFrequency )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;

	// Main list to present to graph dlg
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// File structure
	if( szTrial.Right( 3 ) == _T("HWR") )
		GetHWRMask( szExp, szGrp, szSubj, szData );
	else if( szTrial.Right( 3 ) == _T("FRD") )
		GetFRDMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() )
		::SortFilesAlpha( pFileList, szData );
	else
		::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		if( szTrial.Right( 3 ) == _T("HWR") )
			GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		else if( szTrial.Right( 3 ) == _T("FRD") )
			GetFRD( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 5, 2 );
	szData.Format( IDS_AL_CHARTHWR, szExp, szGrp, szSubj, szCond, atoi( szTrialNum ) );
	OutputAMessage( szData, TRUE );

	CGraphDlg* gd = new CGraphDlg( dFrequency, 0, pFileList, szTrial, CHART_HWR3D,
								   FALSE, _T(""), 0, 0, this, FALSE, clrPenup, TRUE,
								   FALSE, NULL, NULL, &m_wm );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );

	// add to window manager
	m_wm.AddItem( gd );
}

void CMainFrame::ChartHwrRT( CString szExp, CString szGrp, CString szSubj,
							 CString szTrial, double dFrequency )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;

	// Main list to present to graph dlg
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	short nPrs = 1;
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			pPS->get_MinPenPressure( &nPrs );
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// File structure
	GetHWRMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() )
		::SortFilesAlpha( pFileList, szData );
	else
		::SortFilesChrono( pFileList, szData );

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 5, 2 );
	szData.Format( IDS_AL_CHARTHWR, szExp, szGrp, szSubj, szCond, atoi( szTrialNum ) );
	OutputAMessage( szData, TRUE );

	CGraphDlg* gd = new CGraphDlg( dFrequency, nPrs, pFileList, szTrial, CHART_HWRRT, FALSE,
								   _T(""), 0, 0, this, FALSE, clrPenup, TRUE );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );
}

void CMainFrame::ChartHwrMultiple( CString szExp, CString szGrp, CString szSubj,
								   double dFrequency, int nMinPenPressure, CStringList* pList )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;
	szData.Format( _T("Charting raw data for exp=%s subject=%s"), szExp, szSubj );
	OutputAMessage( szData, TRUE );

	// Main list to present to graph dlg
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// Was list a valid parameter? If so, use it
	if( pList )
	{
		// copy the data from input list to this list
		// (because it will be deleted after creating the graph)
		CString szTrial;
		POSITION pos = pList->GetHeadPosition();
		while( pos )
		{
			szTrial = pList->GetNext( pos );
			pFileList->AddTail( szTrial );
		}
	}
	// else generate
	else
	{
		// File structure
		short nType = EXP_TYPE_HANDWRITING;
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExp.AllocSysString() ) ) ) exp.GetType( nType );
		if( nType == EXP_TYPE_IMAGE )
		{
			delete pFileList;
			return;
		}

		if( nType == EXP_TYPE_HANDWRITING )
			GetHWRMask( szExp, szGrp, szSubj, szData );
		else
			GetFRDMask( szExp, szGrp, szSubj, szData );

		// Sort accordingly
		::SortFilesChrono( pFileList, szData );
	}

	// Are there trials?
	if( pFileList->GetCount() == 0 )
	{
		delete pFileList;
		BCGPMessageBox( _T("No trials to chart.") );
		return;
	}

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		GetHWR( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 4 );
		pFileList->SetAt( pLastPos, szData );
	}

	CGraphDlg* gd = new CGraphDlg( dFrequency, nMinPenPressure, pFileList, _T(""), CHART_HWR,
								   FALSE, _T(""), 0, 0, this, TRUE, clrPenup, TRUE );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );
}

void CMainFrame::ChartTf( CString szFile, double dFrequency, CString szSeg,
						  BOOL fModeless, BOOL fPassed, BOOL fForceShowErr, CWnd* pWndOwner )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	// extract condition from file name
	// ending part of file is CCC##.TF
	// so condition (CCC) should be last 3 chars after next statement
	if( szFile.GetLength() < 8 ) return;
	CString szTmp = szFile.Left( szFile.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) ) cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}

	CString szData;
	szData.Format( IDS_AL_CHARTTFSPEC, szFile );
	OutputAMessage( szData, TRUE );

	if( !fModeless )
	{
		CGraphDlg dlg( CHART_TF, pWndOwner );
		dlg.m_szFile = szFile;
		dlg.m_szFile2 = szSeg;
		dlg.m_dFrequency = dFrequency;
		dlg.m_fFeedback = ( szFB != _T("") ) ? TRUE : FALSE;
		dlg.m_szFeature = szFeature;
		dlg.m_MinColor = dwMin;
		dlg.m_MaxColor = dwMax;
		dlg.m_fPassedConsis = fPassed;
		dlg.DoModal();
	}
	else
	{
		CGraphDlg* gd = new CGraphDlg( dFrequency, 1, szFile, CHART_TF,
									   ( szFB!= _T("") ) ? TRUE : FALSE, szFeature, dwMin, dwMax,
									   this, szSeg, ::GetSoftColor(), TRUE, NULL, NULL, NULL,
									   fPassed, fForceShowErr );
		if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );
	}
}

void CMainFrame::ChartTf( CString szExp, CString szGrp, CString szSubj,
						  CString szTrial, double dFrequency, int nMinPenPressure,
						  BOOL fPassed )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// Sort accordingly
	if( ::IsTrialAlphaOn() )
	{
		// File structure
		GetTFMask( szExp, szGrp, szSubj, szData );
		// Sort
		::SortFilesAlpha( pFileList, szData );
	}
	else
	{
		// File structure (HWR since we need the trial sequence)
		short nType = EXP_TYPE_HANDWRITING;
		{
		Experiments exp;
		if( SUCCEEDED( exp.Find( szExp.AllocSysString() ) ) ) exp.GetType( nType );
		}
		if( nType == EXP_TYPE_HANDWRITING )
			GetHWRMask( szExp, szGrp, szSubj, szData );
		else
			GetFRDMask( szExp, szGrp, szSubj, szData );
		// Sort
		::SortFilesChrono( pFileList, szData );
		// Now we must adjust the HWR ext files to TF and remove the missing ones
		CStringList lstTemp;
		CString szHWR, szTF, szFile;
		CFileFind ff;
		POSITION pos = pFileList->GetHeadPosition();
		while( pos )
		{
			szHWR = pFileList->GetNext( pos );
			szTF = szHWR.Left( szHWR.GetLength() - 3 );
			szTF += _T("TF");
			GetTF( szExp, szGrp, szSubj, szTF, szFile );
			if( ff.FindFile( szFile ) ) lstTemp.AddTail( szTF );
		}
		// now place adjusted-name files back in original list
		pFileList->RemoveAll();
		pos = lstTemp.GetHeadPosition();
		while( pos )
		{
			szTF = lstTemp.GetNext( pos );
			pFileList->AddTail( szTF );
		}
	}

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		GetTF( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 3 );
		pFileList->SetAt( pLastPos, szData );
	}

	// extract condition from file name
	// ending part of file is CCC##.TF
	// so condition (CCC) should be last 3 chars after next statement
	if( szTrial.GetLength() < 8 ) return;
	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 5, 2 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) )
			cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}

	szData.Format( IDS_AL_CHARTTF, szExp, szGrp, szSubj, szCond, atoi( szTrialNum ) );
	OutputAMessage( szData, TRUE );

	CGraphDlg* gd = new CGraphDlg( dFrequency, nMinPenPressure, pFileList, szTrial, CHART_TF,
								   ( szFB != _T("") ) ? TRUE : FALSE, szFeature, dwMin,
								   dwMax, this, FALSE, clrPenup, TRUE, FALSE,
								   NULL, NULL, &m_wm, fPassed );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );

	// add to window manager
	m_wm.AddItem( gd );
}

void CMainFrame::ChartTf3D( CString szExp, CString szGrp, CString szSubj,
							CString szTrial, double dFrequency, BOOL fPassed )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// File structure
	GetTFMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() )
	{
		// File structure
		GetTFMask( szExp, szGrp, szSubj, szData );
		// Sort
		::SortFilesAlpha( pFileList, szData );
	}
	else
	{
		// File structure (HWR since we need the trial sequence)
		GetHWRMask( szExp, szGrp, szSubj, szData );
		// Sort
		::SortFilesChrono( pFileList, szData );
		// Now we must adjust the HWR ext files to TF and remove the missing ones
		CStringList lstTemp;
		CString szHWR, szTF, szFile;
		CFileFind ff;
		POSITION pos = pFileList->GetHeadPosition();
		while( pos )
		{
			szHWR = pFileList->GetNext( pos );
			szTF = szHWR.Left( szHWR.GetLength() - 3 );
			szTF += _T("TF");
			GetTF( szExp, szGrp, szSubj, szTF, szFile );
			if( ff.FindFile( szFile ) ) lstTemp.AddTail( szTF );
		}
		// now place adjusted-name files back in original list
		pFileList->RemoveAll();
		pos = lstTemp.GetHeadPosition();
		while( pos )
		{
			szTF = lstTemp.GetNext( pos );
			pFileList->AddTail( szTF );
		}
	}

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		GetTF( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 3 );
		pFileList->SetAt( pLastPos, szData );
	}

	// extract condition from file name
	// ending part of file is CCC###.HWR or .FRD
	// so condition (CCC) should be last 3 chars after next statement
	if( szTrial.GetLength() < 8 ) return;
	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 5, 2 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) )
			cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}

	szData.Format( IDS_AL_CHARTTF, szExp, szGrp, szSubj, szCond, atoi( szTrialNum ) );
	OutputAMessage( szData, TRUE );

	CGraphDlg* gd = new CGraphDlg( dFrequency, 0, pFileList, szTrial, CHART_TF3D,
								   ( szFB != _T("") ) ? TRUE : FALSE, szFeature, dwMin,
								   dwMax, this, FALSE, clrPenup, TRUE, FALSE,
								   NULL, NULL, &m_wm, fPassed );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );

	// add to window manager
	m_wm.AddItem( gd );
}

void CMainFrame::ChartTfRT( CString szExp, CString szGrp, CString szSubj,
							CString szTrial, double dFrequency )
{
	CListView* pMsgWnd = (CListView*)GetMsgPane();
	CListCtrl& lst = pMsgWnd->GetListCtrl();
	SetOutputWindow( &lst );

	CString szData;

	// Main list to present to graph dlg
	CStringList* pFileList = new CStringList;
	if( !pFileList ) return;

	// get penup color
	short nPrs = 1;
	COLORREF clrPenup = ::GetSoftColor();
	IProcSetting* pPS = NULL;
	HRESULT hr = ::CoCreateInstance( CLSID_ProcSetting, NULL, CLSCTX_ALL,
									 IID_IProcSetting, (LPVOID*)&pPS );
	if( SUCCEEDED( hr ) )
	{
		hr = pPS->Find( szExp.AllocSysString() );
		if( SUCCEEDED( hr ) )
		{
			pPS->get_MinPenPressure( &nPrs );
			IProcSetRunExp* pPSRE = NULL;
			hr = pPS->QueryInterface( IID_IProcSetRunExp, (LPVOID*)&pPSRE );
			if( SUCCEEDED( hr ) )
			{
				short nLSize = 0, nESize = 0;
				DWORD bc = 0, lc1 = 0, lc2 = 0, lc3 = 0, ec = 0;
				pPSRE->GetDrawingOptions( &nLSize, &nESize, &bc, &lc1, &lc2, &lc3, &ec, &clrPenup );
				pPSRE->Release();
			}
		}
		pPS->Release();
	}

	// File structure
	GetTFMask( szExp, szGrp, szSubj, szData );

	// Sort accordingly
	if( ::IsTrialAlphaOn() )
	{
		// File structure
		GetTFMask( szExp, szGrp, szSubj, szData );
		// Sort
		::SortFilesAlpha( pFileList, szData );
	}
	else
	{
		// File structure (HWR since we need the trial sequence)
		GetHWRMask( szExp, szGrp, szSubj, szData );
		// Sort
		::SortFilesChrono( pFileList, szData );
		// Now we must adjust the HWR ext files to TF and remove the missing ones
		CStringList lstTemp;
		CString szHWR, szTF, szFile;
		CFileFind ff;
		POSITION pos = pFileList->GetHeadPosition();
		while( pos )
		{
			szHWR = pFileList->GetNext( pos );
			szTF = szHWR.Left( szHWR.GetLength() - 3 );
			szTF += _T("TF");
			GetTF( szExp, szGrp, szSubj, szTF, szFile );
			if( ff.FindFile( szFile ) ) lstTemp.AddTail( szTF );
		}
		// now place adjusted-name files back in original list
		pFileList->RemoveAll();
		pos = lstTemp.GetHeadPosition();
		while( pos )
		{
			szTF = lstTemp.GetNext( pos );
			pFileList->AddTail( szTF );
		}
	}

	// Redo directory structure
	POSITION pos = pFileList->GetHeadPosition();
	POSITION pLastPos = NULL;
	while( pos )
	{
		pLastPos = pos;
		GetTF( szExp, szGrp, szSubj, pFileList->GetNext( pos ), szData );
		szData = szData.Left( szData.GetLength() - 3 );
		pFileList->SetAt( pLastPos, szData );
	}

	CString szTmp = szTrial.Left( szTrial.GetLength() - 5 );
	CString szCond = szTmp.Right( 3 );
	CString szTrialNum = szTrial.Mid( szTrial.GetLength() - 5, 2 );
	CString szFB, szFeature;
	DWORD dwMin = 0, dwMax = 0;
	// find condition and obtain feedback
	{
		NSConditions cond;
		if( SUCCEEDED( cond.Find( szCond ) ) )
			cond.GetFeedback( szFB );
	}
	// get feedback info (if selected
	if( szFB != _T("") )
	{
		Feedbacks fb;
		if( SUCCEEDED( fb.Find( szFB ) ) )
		{
			fb.GetFeature( szFeature );
			fb.GetMinColor( dwMin );
			fb.GetMaxColor( dwMax );
		}
	}

	szData.Format( IDS_AL_CHARTTF, szExp, szGrp, szSubj, szCond, atoi( szTrialNum ) );
	OutputAMessage( szData, TRUE );

	CGraphDlg* gd = new CGraphDlg( dFrequency, nPrs, pFileList, szTrial, CHART_TFRT,
								   ( szFB != _T("") ) ? TRUE : FALSE, szFeature, dwMin,
								   dwMax, this, FALSE, clrPenup, TRUE );
	if( gd ) gd->PostMessage( WM_SETFOCUS, 0, 0 );
}

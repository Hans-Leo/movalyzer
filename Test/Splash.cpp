// Splash.cpp : implementation file
//

#include "StdAfx.h"
#include "Test.h"
#include "Splash.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
//   Splash Screen class

BOOL CSplashWnd::c_bShowSplashWnd;
CSplashWnd* CSplashWnd::c_pSplashWnd;

BEGIN_MESSAGE_MAP(CSplashWnd, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()

CSplashWnd::CSplashWnd()
{
}

CSplashWnd::~CSplashWnd()
{
	c_pSplashWnd = NULL;
}

void CSplashWnd::EnableSplashScreen(BOOL bEnable /*= TRUE*/)
{
	c_bShowSplashWnd = bEnable;
}

void CSplashWnd::ShowSplashScreen(CWnd* pParentWnd /*= NULL*/)
{
	if( !c_bShowSplashWnd || c_pSplashWnd != NULL ) return;

	// Allocate a new splash screen, and create the window.
	c_pSplashWnd = new CSplashWnd;
	if (!c_pSplashWnd->Create(pParentWnd)) delete c_pSplashWnd;
	else c_pSplashWnd->UpdateWindow();
}

void CSplashWnd::RemoveSplashScreen()
{
	if( c_pSplashWnd != NULL ) c_pSplashWnd->HideSplashScreen();
}

BOOL CSplashWnd::PreTranslateAppMessage(MSG* pMsg)
{
	if (c_pSplashWnd == NULL) return FALSE;

	// If we get a keyboard or mouse message, hide the splash screen.
	if (pMsg->message == WM_KEYDOWN ||
	    pMsg->message == WM_SYSKEYDOWN ||
	    pMsg->message == WM_LBUTTONDOWN ||
	    pMsg->message == WM_RBUTTONDOWN ||
	    pMsg->message == WM_MBUTTONDOWN ||
	    pMsg->message == WM_NCLBUTTONDOWN ||
	    pMsg->message == WM_NCRBUTTONDOWN ||
	    pMsg->message == WM_NCMBUTTONDOWN)
	{
		c_pSplashWnd->HideSplashScreen();
		return TRUE;	// message handled here
	}

	return FALSE;	// message not handled
}

BOOL CSplashWnd::Create(CWnd* pParentWnd /*= NULL*/)
{
	if( !m_bitmap.LoadBitmap( IDB_SPLASH ) ) return FALSE;

	BITMAP bm;
	m_bitmap.GetBitmap( &bm );

	return CreateEx(0,
		AfxRegisterWndClass(0, AfxGetApp()->LoadStandardCursor(IDC_ARROW)),
		NULL, WS_POPUP | WS_VISIBLE, 0, 0, bm.bmWidth, bm.bmHeight, pParentWnd->GetSafeHwnd(), NULL);
}

void CSplashWnd::HideSplashScreen()
{
	DestroyWindow();
}

void CSplashWnd::PostNcDestroy()
{
	delete this;
}

int CSplashWnd::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	if( CWnd::OnCreate( lpCreateStruct ) == -1 ) return -1;

	// Center the window.
	CenterWindow();

	// Set a timer to destroy the splash screen.
	SetTimer( 1, 3000, NULL );

	return 0;
}

void CSplashWnd::OnPaint()
{
	CPaintDC dc( this );

	CDC dcImage;
	if( !dcImage.CreateCompatibleDC( &dc ) ) return;

	BITMAP bm;
	m_bitmap.GetBitmap( &bm );

	// Paint the image.
	CBitmap* pOldBitmap = dcImage.SelectObject(&m_bitmap);

	// which app?
	BOOL fScripta = ::IsSA( FALSE ) && !::IsOA( FALSE );
	BOOL fGripa = ::IsGA( FALSE );
	BOOL fMova = ::IsOA( FALSE );
	int nDays = 0;
	BOOL fDemo = ::IsD( &nDays );
	if( fScripta || fGripa || fMova ) fDemo = FALSE;

	// font structure (default)
	LOGFONT lf1, lf2;
	lf1.lfWeight = FW_NORMAL;
	lf2.lfWeight = FW_NORMAL;
	lf1.lfWidth = 0;
	lf2.lfWidth = 0;
	lf1.lfItalic = FALSE;
	lf2.lfItalic = FALSE;
	lf1.lfOrientation = 0;
	lf2.lfOrientation = 0;
	lf1.lfEscapement = 0;
	lf2.lfEscapement = 0;
	lf1.lfUnderline = FALSE;
	lf2.lfUnderline = FALSE;
	lf1.lfStrikeOut = FALSE;
	lf2.lfStrikeOut = FALSE;
	lf1.lfCharSet = ANSI_CHARSET;
	lf2.lfCharSet = ANSI_CHARSET;
	lf1.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf2.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf1.lfQuality = ANTIALIASED_QUALITY;
	lf2.lfQuality = ANTIALIASED_QUALITY;
	lf1.lfPitchAndFamily = FF_SWISS | DEFAULT_PITCH;
	lf2.lfPitchAndFamily = FF_SWISS | DEFAULT_PITCH;
	// set font info to what we want
	dcImage.SetTextColor( RGB( 215, 218, 229 ) );	// font color
	dcImage.SetBkColor( RGB( 255, 255, 255 ) );		// white
	dcImage.SetBkMode( TRANSPARENT );				// transparent
	strcpy_s( lf1.lfFaceName, _T("Arial") );		// font face
	strcpy_s( lf2.lfFaceName, _T("Arial") );		// font face
	// font size
	lf1.lfHeight= -MulDiv( 12, dcImage.GetDeviceCaps( LOGPIXELSY ), 72 );
	lf2.lfHeight= -MulDiv( 8, dcImage.GetDeviceCaps( LOGPIXELSY ), 72 );
	lf1.lfWeight = 700;				// bold
	lf2.lfWeight = 200;				// bold
	lf1.lfItalic = 0;				// italics
	lf2.lfItalic = 0;				// italics
	lf1.lfUnderline = 0;			// underline
	lf2.lfUnderline = 0;			// underline
	lf1.lfStrikeOut = 0;			// strikeout
	lf2.lfStrikeOut = 0;			// strikeout
	lf1.lfPitchAndFamily = 34;		// pitch (dependant upon font)
	lf2.lfPitchAndFamily = 34;		// pitch (dependant upon font)
	CFont font1, font2;
	// create font
	if( font1.CreateFontIndirect( &lf1 ) && font2.CreateFontIndirect( &lf2 ) )
	{
		// select font into dc
		CFont* pOldFont = dcImage.SelectObject( &font1 );
		// font alignment
		dcImage.SetTextAlign( TA_BASELINE );

		// write app text
		CString szLine;
		if( fDemo ) szLine = _T("TRIAL Version");
		else if( fMova && fGripa ) szLine = _T("Includes GripAlyzeR");
		else if( fMova ) szLine = _T("Includes interactive stimuli and submovement analysis");
		else if( fScripta ) szLine = _T("ScriptAlyzeR Edition ");
		else if( fGripa ) szLine = _T("GripAlyzeR Edition ");
		dcImage.ExtTextOut( 12, 285, ETO_OPAQUE, NULL, szLine, NULL );

		// write details about scriptalyzer has no stimuli, etc. (if not movalyzer)
		if( fScripta || fGripa || fDemo )
		{
			dcImage.SelectObject( pOldFont );
			if( fDemo ) szLine = _T("(includes all features of MovAlyzeR®)");
			else if( fMova ) szLine = _T("");
			else if( fScripta ) szLine = _T("(no interactive stimuli nor submovement analysis)");
			else if( fGripa ) szLine = _T("(bi-manual grip-force control)");
			else if( fDemo ) szLine = _T("(includes all features of MovAlyzeR®)");
			pOldFont = dcImage.SelectObject( &font2 );
			dcImage.ExtTextOut( fScripta ? 190 : fGripa ? 180 : 145, 285, ETO_OPAQUE, NULL, szLine, NULL );
		}

		// cleanup
		dcImage.SelectObject( pOldFont );
	}

	dc.BitBlt( 0, 0, bm.bmWidth, bm.bmHeight, &dcImage, 0, 0, SRCCOPY );
	dcImage.SelectObject( pOldBitmap );
}

void CSplashWnd::OnTimer(UINT nIDEvent)
{
	HideSplashScreen();
}

#pragma once

// AppLookDlg.h : header file
//

#include "..\NSSharedGUI\CustAppLookDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CAppLookDlg dialog

class CAppLookDlg : public CBCGPDialog
{
// Construction
public:
	CAppLookDlg(BOOL bStartup, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	enum { IDD = IDD_APP_LOOK };
	CBCGPComboBox	m_wndStyle;
	CBCGPButton		m_wndRoundedTabs;
	CBCGPButton		m_wndDockTabColors;
	CBCGPButton		m_wndOneNoteTabs;
	BOOL			m_bShowAtStartup;
	BOOL			m_bOneNoteTabs;
	BOOL			m_bDockTabColors;
	BOOL			m_bRoundedTabs;
	BOOL			m_bCustomTooltips;
	int				m_nAppLook;
	int				m_nStyle;
	CCustAppLookDlg	m_dlgCal;
	COLORREF		m_clrCustom;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	// message map
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnApply();
	afx_msg void OnAppLook();
	afx_msg void OnStyle();
	afx_msg BOOL OnHelpInfo( HELPINFO* );
	afx_msg void OnCustomize();
	DECLARE_MESSAGE_MAP()

	const BOOL	m_bStartup;
public:
	void SetLook( BOOL fExternal = FALSE, CBCGPFrameWnd* pMainWnd = NULL );
};

#pragma once

#include "..\CxImage\CxImage\ximage.h"

// ImageDlg dialog
class ImageDlg : public CBCGPDialog
{
	DECLARE_DYNAMIC(ImageDlg)

public:
	ImageDlg( CWnd* pParent = NULL );   // standard constructor
	ImageDlg( CString szImage, CWnd* pParent = NULL );   // standard constructor
	virtual ~ImageDlg();

// Dialog Data
public:
	enum { IDD = IDD_IMAGE };
	// image object
	CxImage*	m_pPCX;
	// image file
	CString		m_szImage;
	// for drawing & scrolling
	CRect		rectStaticClient;
	SCROLLINFO	horz;
	SCROLLINFO	vert;
	CStatic		m_st1;
	CBCGPScrollBar	m_vbar;
	CBCGPScrollBar	m_hbar;
	CSize		m_size;
	int			m_nX;
	int			m_nY;
	CBitmap		m_bmpPrint;
	CMenu		m_mnuConvert;

// Operations
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnSize( UINT nType, int cx, int cy );
	void LoadImage( CString szExt );
	void DrawResize();
	void OnPrint();
	void OnConvert( UINT nID );

public:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnVScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnPaint();
	afx_msg void OnSysCommand( UINT nID, LPARAM lParam );
	DECLARE_MESSAGE_MAP()
};

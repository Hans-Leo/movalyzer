// Memory.cpp : implementation of all memory(file)-related
// ...functions from MainFrm.cpp and LeftView.cpp
//

#include "stdafx.h"
#include "Test.h"
#include <direct.h>

#include "MainFrm.h"
#include "LeftView.h"
#include "GraphView.h"
#include "..\NSSharedGUI\UsersDlg.h"
#include "..\NSSharedGUI\NotesDlg.h"
#include "BackupHistoryDlg.h"
#include "..\Common\Shared.h"

#include "..\ProcessMod\ProcessMod.h"
#include "..\DataMod\DataMod.h"

#include "..\NSSharedGUI\NSSharedGUI.h"
#include "..\Common\UserObj.h"
#include "..\NSSharedGUI\Subjects.h"
#include "..\NSSharedGUI\Groups.h"
#include "..\NSSharedGUI\Experiments.h"
#include "..\NSSharedGUI\Conditions.h"
#include "..\NSSharedGUI\Users.h"

#define	MEM_WRITE() \
	nVal = tm.hItem ? tree.GetItemState( tm.hItem, TVIS_EXPANDED ) : 0; \
	tm.fOpen = ( ( nVal & TVIS_EXPANDED ) == TVIS_EXPANDED ); \
	tm.fChildren = tm.hItem ? tree.ItemHasChildren( tm.hItem ) : 0; \
	f.Write( (LPVOID)&tm, sizeof( tm ) );

#define MEM_WRITE_NODE(hitem) \
	tm.hItem = hitem; \
	MEM_WRITE(); \
	hItem = tree.GetChildItem( tm.hItem ); \
	while( hItem ) \
	{ \
		tm.hItem = hItem; \
		MEM_WRITE(); \
		hItem = tree.GetNextSiblingItem( hItem ); \
	}

BOOL LoadPreferences();
BOOL UpdatePreferences( INSMUser* pUser );

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

BOOL CMainFrame::ConfirmPrefs( BOOL fUserOnly )
{
	CString szPath;
	::GetDataPath( szPath, FALSE, TRUE );

	// encrypt all users if they haven't been
	int nVal = theApp.GetInt( _T("VerifiedUsers"), 0 );
	if( nVal == 0 )
	{
		NSMUsers::EncryptAll();
		theApp.WriteInt( _T("VerifiedUsers"), 1 );
	}

	UsersDlg d( this );
	CString szID;
	::GetCurrentUser( szID );
StartAgain:
	if( szID == _T("") )	// No user selected yet
	{
		// If only one user defined, select that one
		BOOL fDoDlg = FALSE;
		INSMUser* pUser = NULL;
		HRESULT hr = CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
									   IID_INSMUser, (LPVOID*)&pUser );
		if( FAILED( hr ) )
		{
			BCGPMessageBox( _T("Unable to create User object. Closing application.") );
			AfxGetApp()->ExitInstance();
			exit( EXIT_FAILURE );
		}
		pUser->SetDataPath( szPath.AllocSysString() );
		hr = pUser->ResetToStart();
		pUser->get_Virgin( &d.m_fVirgin );
		int nUserCount = 0;
		if( SUCCEEDED( hr ) )
		{
			nUserCount++;
			hr = pUser->GetNext();
			if( SUCCEEDED( hr ) )
			{
				nUserCount++;
				hr = pUser->GetNext();
				if( SUCCEEDED( hr ) ) nUserCount++;
				if( nUserCount > 2 )
				{
					fDoDlg = TRUE;
					pUser->Release();
					pUser = NULL;
				}
				else pUser->ResetToStart();
			}
		}
		else
		{
			pUser->Release();
			pUser = NULL;
			fDoDlg = TRUE;
		}

		// Open up user dialog (if more than one)
		if( fDoDlg && ( d.DoModal() == IDCANCEL ) )
		{
			BCGPMessageBox( _T("No user selected. Defaulting to example user.") );
			hr = CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
								   IID_INSMUser, (LPVOID*)&pUser );
			if( FAILED( hr ) )
			{
				BCGPMessageBox( _T("Unable to create User object. Closing application.") );
				AfxGetApp()->ExitInstance();
				exit( EXIT_FAILURE );
			}
			CString szTmp = _T("UU1");
			if( FAILED( pUser->Find( szTmp.AllocSysString() ) ) )
			{
				BCGPMessageBox( _T("Unable to locate user. Closing application.") );
				AfxGetApp()->ExitInstance();
				exit( EXIT_FAILURE );
			}
			fDoDlg = FALSE;
		}
		if( fDoDlg ) pUser = d.m_pUser;

		// user lock/reference file
		CString szLck;
		BSTR bstr = NULL;
		pUser->get_RootPath( &bstr );
		szLck = bstr;
		szLck += _T("\\user.lck");
		// does file already exist
		CFileFind ff;
		if( !fDoDlg )
		{
			// is this user already being used (by another instance)
			// if so, warn & provide option to continue or not
			if( ff.FindFile( szLck ) )
			{
				int nRes = BCGPMessageBox( _T("This user is already in use. Continuing could potentially result in file access conflicts. Continue?"), MB_YESNO );
				if( nRes == IDNO )
				{
					pUser->Release();
					pUser = NULL;
					PostQuitMessage( 0 );
				}
			}
		}

		// does file already exist
		// if exists
		if( !::IsClientServerModeOn() && ff.FindFile( szLck ) )
		{
			// open file and read ref count, rewrite with incremented count
			CStdioFile lf;
			if( lf.Open( szLck, CFile::modeRead ) )
			{
				// read count line
				CString szLine;
				lf.ReadString( szLine );
				// convert to numeric and increment
				int nCount = atoi( szLine );
				if( nCount <= 0 ) nCount = 0;
				nCount++;
				szLine.Format( _T("%d"), nCount );
				// close file & delete
				lf.Close();
				REMOVE_FILE( szLck );

				// recreate file with new ref count
				if( lf.Open( szLck, CFile::modeWrite | CFile::modeCreate ) )
					lf.WriteString( szLine );
			}
		}
		// if not exists, create with ref count of 1
		else if( !::IsClientServerModeOn() )
		{
			CStdioFile lf;
			if( lf.Open( szLck, CFile::modeWrite | CFile::modeCreate ) )
				lf.WriteString( _T("1") );
		}

		// preferences
		CWaitCursor crs;
		if( !::IsClientServerModeOn() && pUser && !::UpdatePreferences( pUser ) )
		{
			BCGPMessageBox( _T("Unable to set preferences.") );
			if( !fDoDlg && pUser ) pUser->Release();
			exit( EXIT_FAILURE );
		}
		else if( !pUser && fUserOnly )
		{
			return FALSE;
		}
		else if( !fDoDlg && pUser )
		{
			pUser->Release();
			pUser = NULL;
		}

		// get ID if user object
		if( pUser )
		{
			pUser->get_UserID( &bstr );
			szID = bstr;
		}
	}
	else if( !LoadPreferences() )
	{
		szID = _T("");
		::SetCurrentUser( _T("") );
		goto StartAgain;
	}

	// device
	CBCGPRibbonStatusBarPane* pStatBarDev = 
		(CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_WIZ_DEVSETUP );
	CString szPrefix;
	if( ::IsTabletModeOn() )
	{
		if( pStatBarDev ) pStatBarDev->SetText( _T("TABLET") );
		szPrefix = _T("Tablet_");
	}
	else if( ::IsGripperModeOn() )
	{
		if( pStatBarDev ) pStatBarDev->SetText( _T("DAQPAD") );
		szPrefix = _T("Gripper_");
	}
	else
	{
		if( pStatBarDev ) pStatBarDev->SetText( _T("MOUSE") );
		szPrefix = _T("Mouse_");
	}
	if( pStatBarDev ) pStatBarDev->Redraw();

	// load any user-specific registry data
	LoadUserRegistryData( szID, szPrefix );

	return TRUE;
}

BOOL LoadPreferences()
{
	CString szID;
	::GetCurrentUser( szID );
	if( szID == _T("") ) return FALSE;

	// verify user database exists first
	CString szPath;
	::GetAllUserPath( szPath );
	CString szUserDB = szPath;
	szUserDB += _T("user.dat");
	CFileFind ff;
	if( !ff.FindFile( szUserDB ) ) return FALSE;

	//CString szTempMsg;
	//szTempMsg.LoadString(IDS_ADD_ERR);
	//BCGPMessageBox( szTempMsg+_T(" LoadPreferences") ); // Jerry

	INSMUser* pUser = NULL;
	HRESULT hr = CoCreateInstance( CLSID_NSMUser, NULL, CLSCTX_ALL,
								   IID_INSMUser, (LPVOID*)&pUser );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to create user object.") );
		return FALSE;
	}
	pUser->SetDataPath( szPath.AllocSysString() );
	hr = pUser->Find( szID.AllocSysString() );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( _T("Unable to locate user.") );
		pUser->Release();
		return FALSE;
	}
	if( !UpdatePreferences( pUser ) )
	{
		BCGPMessageBox( _T("Unable to set preferences.") );
		pUser->Release();
		return FALSE;
	}

	pUser->Release();
	return TRUE;
}

BOOL UpdatePreferences( INSMUser* pUser )
{
	if( !pUser ) return FALSE;

	BSTR bstrItem = NULL;
	CString szItem, szRootPath;
	short nItem;
	BOOL fItem;

	// New Settings
	pUser->get_UserID( &bstrItem );
	::SetCurrentUser( bstrItem );
	pUser->get_Description( &bstrItem );
	::SetCurrentUserDesc( bstrItem );
	pUser->get_RootPath( &bstrItem );
	::SetDataPathRoot( bstrItem );
	szRootPath = bstrItem;
	pUser->get_BackupPath( &bstrItem );
	::SetBackupPath( bstrItem );
	pUser->get_Delimiter( &bstrItem );
	::SetDelimiter( bstrItem );
	pUser->get_InputType( &nItem );
	::SetTabletModeOn( nItem == Preferences::itTablet );
	::SetGripperModeOn( nItem == Preferences::itGripper );
	pUser->get_TabletMode( &nItem );
	::SetDesktopModeOn( nItem == Preferences::tmDesktop );
	pUser->get_Private( &fItem );
	::SetPrivateUserOn( fItem );
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");
	pUser->get_Log( &fItem );
	::SetLoggingOn( fItem, szAppPath );
	pUser->get_LogUI( &fItem );
	::SetLogUI( fItem );
	pUser->get_LogDB( &fItem );
	::SetLogDB( fItem );
	pUser->get_LogProc( &fItem );
	::SetLogProc( fItem );
	pUser->get_LogGraph( &fItem );
	::SetLogGraph( fItem );
	pUser->get_LogTablet( &fItem );
	::SetLogTablet( fItem );
	pUser->get_Alpha( &fItem );
	::SetTrialAlphaOn( fItem );
	pUser->get_EntireTablet( &fItem );
	::SetTabletRemapOn( fItem );

	NSMUsers::GetSysPass( pUser, szItem );
	::SetUserPassword( szItem );
	// update subjects if they haven't been
	BOOL fRes = Subjects::UpdateMissingPasswords( szRootPath, szItem );
	if( !fRes ) BCGPMessageBox( _T("Error updating subject passwords") );

	double dX, dY;
	pUser->get_AspectRatioWidth( &dX );
	pUser->get_TabletWidth( &dX );
	pUser->get_TabletHeight( &dY );
	::SetTabletDimensions( dX, dY );
	pUser->get_DisplayWidth( &dX );
	pUser->get_DisplayHeight( &dY );
	::SetDisplayDimensions( dX, dY );
	::SetDisplayDimension( dX, dY );	// for shared.cpp
	pUser->get_CommPort( &bstrItem );
	::SetCommPort( bstrItem );
	pUser->get_AutoUpdate( &fItem );
	::SetAutoUpdateOn( fItem );
	pUser->get_SiteID( &bstrItem );
	::SetCurrentUserSiteID( bstrItem );
	pUser->get_SiteDesc( &bstrItem );
	::SetCurrentUserSiteDesc( bstrItem );
	NSMUsers::GetSignature( pUser, szItem );
	::SetSignature( szItem );

	// Gripper Settings
	GripperSettings gs;
	gs.nDevice = 1;
	gs.nChannels = 3;
	gs.lSamples = 1000L;
	gs.lSampleRate = 100000L;
	gs.lScanRate = 100L;
	gs.nBaseline = 50;
	gs.nChanLower = 2;
	gs.nChanUpper = 0;
	gs.nChanLoad = 1;
	gs.dGainLG = 1.;
	gs.dGainUG = 1.;
	gs.dGainL = 1.;
	gs.dCalibrationConstantLG = 1000.;
	gs.dCalibrationConstantUG = 1000.;
	gs.dCalibrationConstantL = 1000.;
	gs.dExcitationVoltageLG = 5.;
	gs.dExcitationVoltageUG = 5.;
	gs.dExcitationVoltageL = 5.;
	gs.dFullScaleLoadLG = 25.;
	gs.dFullScaleLoadUG = 25.;
	gs.dFullScaleLoadL = 25.;
	gs.fVolts = FALSE;
	gs.fDoubleBuffer = TRUE;
	gs.lDBSamples = 20;
	gs.fNewtons = TRUE;
	CFile f;

	CString szWin;
	::GetAllUserPath( szWin );
	CString szGS = szWin;
	szGS += _T("\\gripper.dat");
	if( f.Open( szGS, CFile::modeRead ) )
	{
		f.Read( &gs, sizeof( gs ) );
		f.Close();
	}
	::SetGripperSettings( &gs );

	return TRUE;
}

void CMainFrame::GetPreferences( Preferences* prefs, GripperSettings* gs )
{
	::GetPreferences( prefs, gs );
}

void CMainFrame::SetPreferences( Preferences* prefs, GripperSettings* gs )
{
	if( prefs )
	{
		::SetPreferences( prefs, gs );

		// reset input device
		CGraphView* pGV = GetRecordingPane();
		if( pGV )
		{
			CWaitCursor crs;
			pGV->Reset();
		}

		// select proper combo item
		CBCGPToolbarComboBoxButton* pCombo =
			(CBCGPToolbarComboBoxButton*)m_wndToolBarID.GetButton( 1 );

		if( pCombo && pCombo->IsWindowVisible() )
		{
			if( ::IsGripperModeOn() ) pCombo->SelectItem( 2 );
			else
				if( ::IsTabletModeOn() ) pCombo->SelectItem( 1 );
			else pCombo->SelectItem( 0 );
		}

		// app title adjustment
		CString szInput;
		if( ::IsTabletModeOn() ) szInput = _T("TABLET");
		else if( ::IsGripperModeOn() ) szInput = _T("DAQPad");
		else szInput = _T("MOUSE");
		CBCGPRibbonStatusBarPane* pPane = (CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_O_INPUTDEVICE );
		if( pPane )
		{
			pPane->SetText( szInput );
			pPane->Redraw();
		}
		CBCGPRibbonStatusBarPane* pStatBarDev = 
			(CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_WIZ_DEVSETUP );
		szInput.MakeUpper();
		if( pStatBarDev )
		{
			pStatBarDev->SetText( szInput );
			pStatBarDev->Redraw();
		}
		m_wndStatusBar.RecalcLayout();
		m_wndStatusBar.RedrawWindow();
	}

	// Gripper Settings
	if( ::IsGripperInstalled() ) ::SetGripperSettings( gs );
}

BOOL CMainFrame::WritePreferences( Preferences* prefs, GripperSettings* gs )
{
	return ::WritePreferences( prefs, gs );
}

BOOL CMainFrame::OnPreferences( UINT nPage, CString szMsg )
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return FALSE;
	}

	OutputAMessage( szMsg, TRUE );

	BOOL fPassSet = ::HasPassBeenSet();
	BOOL fPrivOn = ::IsPrivacyOn();
	CString szDelim = ::GetDelimiter();
	CString szPass = ::GetUserPassword();

	CString szID;
	::GetCurrentUser( szID );
	NSMUsers user;
	user.m_nPage = nPage;
	if( user.DoEdit( szID, this ) )
	{
		CString szDelimNew = ::GetDelimiter();
		CString szPassNew = ::GetUserPassword();
		// if pass hasn't changed, restore pass being set & privacy settings
		if( szPass == szPassNew )
		{
			::SetHasPassBeenSet( fPassSet );
			::SetIsPrivacyOn( fPrivOn );
		}
		// if any of these items has changed, we need to refresh left view
		if( ( szDelim != szDelimNew ) || ( szPass != szPassNew ) )
		{
			CLeftView* pLV = GetLeftView();
			if( pLV ) pLV->Refresh();
		}
		CGraphView* pGV = GetRecordingPane();
		if( pGV )
		{
			CWaitCursor crs;
			pGV->Reset();
		}

		OnToolbarReset( IDR_TB_SETTINGS, 1 );

		return TRUE;
	}

	return FALSE;
}

void CMainFrame::OnOUser()
{
	OnPreferences( 0, _T("Viewing/modifying user information/password.") );
}

void CMainFrame::OnOPath() 
{
	OnPreferences( 1, _T("Viewing/modifying root data path.") );
}

void CMainFrame::OnOInputdevice() 
{
	OnPreferences( 2, _T("Viewing/modifying input device.") );
}

void CMainFrame::OnOInputdeviceSettings() 
{
	OnPreferences( 3, _T("Viewing/modifying input device settings.") );
}

void CMainFrame::OnODologging() 
{
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");
	::SetLoggingOn( !::IsLoggingOn(), szAppPath );

	Preferences pref;
	GetPreferences( &pref, NULL );
	pref.fLog = ::IsLoggingOn();
	WritePreferences( &pref, NULL );

	::SetLogDB( pref.fLogDB );
	::SetLogGraph( pref.fLogGraph );
	::SetLogProc( pref.fLogProc );
	::SetLogTablet( pref.fLogTablet );
	::SetLogUI( pref.fLogUI );

	CLeftView* pLV = GetLeftView();
	IExperimentMember* pEML = pLV ? pLV->m_pEML : NULL;
	if( pEML ) pEML->SetLoggingOn( pref.fLog && pref.fLogDB, szAppPath.AllocSysString() );
	IProcess* pProc = Experiments::GetProcessObject( GetMsgPaneList() );
	if( pProc ) pProc->SetLoggingOn( pref.fLog && pref.fLogProc, szAppPath.AllocSysString() );
}

void CMainFrame::OnOLogall()
{
	BOOL fOn = TRUE;

	Preferences pref;
	GetPreferences( &pref, NULL );

	::SetLogDB( fOn );
	pref.fLogDB = fOn;
	::SetLogGraph( fOn );
	pref.fLogGraph = fOn;
	::SetLogProc( fOn );
	pref.fLogProc = fOn;
	::SetLogTablet( fOn );
	pref.fLogTablet = fOn;
	::SetLogUI( fOn );
	pref.fLogUI = fOn;

	CLeftView* pLV = GetLeftView();
	IExperimentMember* pEML = pLV ? pLV->m_pEML : NULL;
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");
	if( pEML ) pEML->SetLoggingOn( fOn, szAppPath.AllocSysString() );
	IProcess* pProc = Experiments::GetProcessObject( GetMsgPaneList() );
	if( pProc ) pProc->SetLoggingOn( fOn, szAppPath.AllocSysString() );

	WritePreferences( &pref, NULL );
}

void CMainFrame::OnOLognone()
{
	BOOL fOn = FALSE;

	Preferences pref;
	GetPreferences( &pref, NULL );

	::SetLogDB( fOn );
	pref.fLogDB = fOn;
	::SetLogGraph( fOn );
	pref.fLogGraph = fOn;
	::SetLogProc( fOn );
	pref.fLogProc = fOn;
	::SetLogTablet( fOn );
	pref.fLogTablet = fOn;
	::SetLogUI( fOn );
	pref.fLogUI = fOn;

	CLeftView* pLV = GetLeftView();
	IExperimentMember* pEML = pLV ? pLV->m_pEML : NULL;
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");
	if( pEML ) pEML->SetLoggingOn( fOn, szAppPath.AllocSysString() );
	IProcess* pProc = Experiments::GetProcessObject( GetMsgPaneList() );
	if( pProc ) pProc->SetLoggingOn( fOn, szAppPath.AllocSysString() );

	WritePreferences( &pref, NULL );
}

void CMainFrame::OnOLogdb() 
{
	::SetLogDB( !::GetLogDB() );
	Preferences pref;
	GetPreferences( &pref, NULL );
	pref.fLogDB = ::GetLogDB();
	WritePreferences( &pref, NULL );

	CLeftView* pLV = GetLeftView();
	IExperimentMember* pEML = pLV ? pLV->m_pEML : NULL;
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");
	if( pEML ) pEML->SetLoggingOn( ::GetLogDB(), szAppPath.AllocSysString() );
}

void CMainFrame::OnOLoggraph() 
{
	::SetLogGraph( !::GetLogGraph() );
	Preferences pref;
	GetPreferences( &pref, NULL );
	pref.fLogGraph = ::GetLogGraph();
	WritePreferences( &pref, NULL );
}

void CMainFrame::OnOLogproc() 
{
	::SetLogProc( !::GetLogProc() );
	Preferences pref;
	GetPreferences( &pref, NULL );
	pref.fLogProc = ::GetLogProc();
	WritePreferences( &pref, NULL );

	IProcess* pProc = Experiments::GetProcessObject( GetMsgPaneList() );
	CString szAppPath;
	::GetDataPathRoot( szAppPath );
	szAppPath += _T("\\Actions.log");
	if( pProc ) pProc->SetLoggingOn( ::GetLogProc(), szAppPath.AllocSysString() );
}

void CMainFrame::OnOLogtablet() 
{
	::SetLogTablet( !::GetLogTablet() );
	Preferences pref;
	GetPreferences( &pref, NULL );
	pref.fLogTablet = ::GetLogTablet();
	WritePreferences( &pref, NULL );
}

void CMainFrame::OnOLogui() 
{
	::SetLogUI( !::GetLogUI() );
	Preferences pref;
	GetPreferences( &pref, NULL );
	pref.fLogUI = ::GetLogUI();
	WritePreferences( &pref, NULL );
}

void CMainFrame::LoadUserRegistryData( CString szID, CString szPrefix )
{
	CString szVal, szDef, szKey, szRB;
	// set registry base and store old in szRB
	szVal.Format( _T("Settings\\%s"), szID );
	szRB = theApp.GetRegistryBase();
	theApp.SetRegistryBase( szVal );
	// sampling rate
	szKey = szPrefix;
	szKey += _T("SamplingRate");
	szDef.Format( _T("%d"), ::GetSamplingRate() );
	szVal = theApp.GetString( szKey, szDef );
	::SetSamplingRate( atoi( szVal ) );
	// min pen pressure
	szKey = szPrefix;
	szKey += _T("MPP");
	szDef.Format( _T("%d"), ::GetMinPenPressure() );
	szVal = theApp.GetString( szKey, szDef );
	::SetMinPenPressure( atoi( szVal ) );
	// dev resolution
	szKey = szPrefix;
	szKey += _T("Resolution");
	szDef.Format( _T("%g"), ::GetDeviceResolution() );
	szVal = theApp.GetString( szKey, szDef );
	if( szVal == _T("") ) szVal = szDef;
	::SetDeviceResolution( atof( szVal ) );
	// device setup wizard run
	szKey = _T("DeviceSetupRun");
	int nVal = theApp.GetInt( szKey );
	::SetDeviceSetupRun( nVal );
	// reset registry base
	theApp.SetRegistryBase( szRB );

	// User object
	UserObj* pObj = ::GetCurrentUserObj();
	if( pObj )
	{
		delete pObj;
		pObj = NULL;
		::SetCurrentUserObj( NULL );
	}
	CString szPath;
	::GetDataPath( szPath, FALSE, TRUE );
	NSMUsers user;
	user.SetDataPath( szPath );
	if( SUCCEEDED( user.Find( szID ) ) )
	{
		CString szID, szDesc, szPass, szBackup, szSig, szDelim, szSiteID, szSiteDesc;
		BOOL fPrivate = FALSE, fProperLoc = TRUE, fWriteable = TRUE;
		user.GetUserID( szID );
		user.GetDescription( szDesc );
		user.GetSysPass( szPass );
		user.GetRootPath( szPath );
		user.GetBackupPath( szBackup );
		user.GetSignature( szSig );
		user.GetDelimiter( szDelim );
		user.GetSiteID( szSiteID );
		user.GetSiteDesc( szSiteDesc );
		user.GetPrivate( fPrivate );

		pObj = new UserObj( szID, szDesc, szPass, szPass, szBackup, szSig, szDelim, fPrivate,
			fProperLoc, fWriteable, szSiteID, szSiteDesc, ::IsPrivacyOn(), ::GetOutputWindow() );
		::SetCurrentUserObj( pObj );
	}

	// if device setup wizard has not been run, inform user
	if( IsWindowVisible() && !::WasDeviceSetupRun() )
	{
		szVal = _T("You have not setup your input device.\n\nRun the Input Device Setup Wizard?");
		if( BCGPMessageBox( szVal, MB_YESNO ) == IDYES )OnWizDevSetup();
	}
}

void CMainFrame::SetAppTitle()
{
	CString szTitle, szItem, szID, szSiteID;
	::GetCurrentUser( szID );
	szSiteID = ::GetCurrentUserSiteID();
	szItem.LoadString( AFX_IDS_APP_TITLE );
	szTitle.Format( _T("%s - %s at %s"), szItem, szID, szSiteID );
	SetWindowText( szTitle );
}

void CMainFrame::OnFileUsers() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	CString szCurPath, szCurDelim;
	::GetDataPathRoot( szCurPath );
	::GetDelimiter( szCurDelim );
	TRIM( szCurDelim );

	UsersDlg d( this );
	INT_PTR nRes = IDCANCEL;
	if( ( ( nRes = d.DoModal() ) == IDOK ) && d.m_pUser )
	{
		// Cleanup (release references) so open sessions will logout
		// we must do this before we create (use) an instance of a user
		// otherwise there will still be 1 reference count and session wont log out
		GetLeftView()->Cleanup();

		if( m_bIsMinimized ) ::ShowWindow( m_hWnd, SW_RESTORE );

		BSTR bstrItem = NULL;
		short nItem;
		BOOL fItem;
		CString szPath, szDelim, szID, szName, szItem;
		double dX, dY;

		// Save window memory first
		CLeftView* pLV = GetLeftView();
		pLV->WriteMemory();

		// New Settings
		d.m_pUser->get_UserID( &bstrItem );
		szID = bstrItem;
		::SetCurrentUser( szID );
		d.m_pUser->get_Description( &bstrItem );
		szName = bstrItem;
		::SetCurrentUserDesc( szName );
		d.m_pUser->get_RootPath( &bstrItem );
		szPath = bstrItem;
		::SetDataPathRoot( bstrItem );
		d.m_pUser->get_BackupPath( &bstrItem );
		::SetBackupPath( bstrItem );
		d.m_pUser->get_Delimiter( &bstrItem );
		szDelim = bstrItem;
		::SetDelimiter( bstrItem );
		d.m_pUser->get_InputType( &nItem );
		::SetTabletModeOn( nItem == Preferences::itTablet );
		::SetGripperModeOn( nItem == Preferences::itGripper );
		d.m_pUser->get_TabletMode( &nItem );
		::SetDesktopModeOn( nItem == Preferences::tmDesktop );
		d.m_pUser->get_Alpha( &fItem );
		::SetTrialAlphaOn( fItem );
		d.m_pUser->get_Private( &fItem );
		::SetPrivateUserOn( fItem );
		d.m_pUser->get_EntireTablet( &fItem );
		::SetTabletRemapOn( fItem );
		CString szAppPath;
		::GetDataPathRoot( szAppPath );
		szAppPath += _T("\\Actions.log");
		d.m_pUser->get_Log( &fItem );
		::SetLoggingOn( fItem, szAppPath );
		NSMUsers::GetSysPass( d.m_pUser, szItem );
		::SetUserPassword( szItem );
		d.m_pUser->get_AspectRatioWidth( &dX );
		d.m_pUser->get_TabletWidth( &dX );
		d.m_pUser->get_TabletHeight( &dY );
		::SetTabletDimensions( dX, dY );
		d.m_pUser->get_DisplayWidth( &dX );
		d.m_pUser->get_DisplayHeight( &dY );
		::SetDisplayDimensions( dX, dY );
		::SetDisplayDimension( dX, dY );
		d.m_pUser->get_CommPort( &bstrItem );
		szItem = bstrItem;
		::SetCommPort( szItem );
		d.m_pUser->get_AutoUpdate( &fItem );
		::SetAutoUpdateOn( fItem );
		d.m_pUser->get_SiteID( &bstrItem );
		::SetCurrentUserSiteID( bstrItem );
		d.m_pUser->get_SiteDesc( &bstrItem );
		::SetCurrentUserSiteDesc( bstrItem );
		NSMUsers::GetSignature( d.m_pUser, szItem );
		::SetSignature( szItem );

		szItem.Format( _T("Loading User: %s"), szID );
		OutputAMessage( szItem, TRUE );

		CString szInput, szPrefix;
		if( ::IsTabletModeOn() )
		{
			szInput = _T("TABLET");
			szPrefix = _T("Tablet_");
		}
		else if( ::IsGripperModeOn() )
		{
			szInput = _T("DAQPad");
			szPrefix = _T("Gripper_");
		}
		else
		{
			szInput = _T("MOUSE");
			szPrefix = _T("Mouse_");
		}
		CBCGPRibbonStatusBarPane* pPane = (CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_O_INPUTDEVICE );
		if( pPane )
		{
			pPane->SetText( szInput );
			pPane->Redraw();
		}
		CBCGPRibbonStatusBarPane* pStatBarDev = 
			(CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_WIZ_DEVSETUP );
		szInput.MakeUpper();
		if( pStatBarDev )
		{
			pStatBarDev->SetText( szInput );
			pStatBarDev->Redraw();
		}
		pPane = (CBCGPRibbonStatusBarPane*)m_wndStatusBar.FindByID( IDM_FILE_USERS );
		if( pPane )
		{
			pPane->SetText( szID );
			pPane->Redraw();
		}
		m_wndStatusBar.RecalcLayout();
		m_wndStatusBar.RedrawWindow();

		d.m_pUser->ResetWarningStatus();

		// load user registry information
//		LoadUserRegistryData( szID, szPrefix );

		// Refresh
		if( ( szCurPath != szPath ) || ( szCurDelim != szDelim ) )
		{
			pLV->FillTree();
			pLV->ReadMemory();
		}

		CGraphView* pGV = GetRecordingPane();
		pGV->Clear();

		// window title
		SetAppTitle();

		// reset toolbars
		OnToolbarReset( IDR_TB_SETTINGS, 1 );

		// resize
		OnCreateClient2();

		// warn if UU1 user that any changes made will be overwritten with updates to software
		// if they want to customize any of the sample experiments, they should export it
		// and import it into a new/existing user other than UU1
		::GetCurrentUser( szID );
		if( szID.Left( 2 ) == _T("UU") )
		{
			CString szMsg = _T("IMPORTANT: Any changes made to the NeuroScript example users\n");
			szMsg += _T("will be lost with program updates.\n\n");
			szMsg += _T("If you choose to customize any of the example experiments and\n");
			szMsg += _T("retain these changes for the future, you should first EXPORT that\n");
			szMsg += _T("experiment and then IMPORT it into a non-NeuroScript user.");
			BCGPMessageBox( szMsg, MB_ICONEXCLAMATION );
		}

		// nothing has changed since new user
		::DataHasNotChanged();

		// run device setup wizard if not run
//		if( !::WasDeviceSetupRun() ) OnWizDevSetup();
	}

	// if not a new user selected, but a current user was changed to affect left view, refresh
	if( ( nRes == IDOK ) && d.m_fRefresh ) GetLeftView()->Refresh();
}

void CMainFrame::OnFileBackuphistory() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	BackupHistoryDlg d( this );
	if( d.DoModal() == IDOK )
	{
		if( BCGPMessageBox( _T("Are you sure? This process cannot be undone."), MB_YESNO ) == IDNO )
			return;
		int nRes = BCGPMessageBox( _T("Do you want to backup the current database files first?"), MB_YESNOCANCEL );
		if( nRes == IDCANCEL ) return;
		if( ( nRes == IDYES ) && !FBackupdb( FALSE ) ) return;
	}
	else return;

	OutputAMessage( _T("Restoring data."), TRUE );

	// RESTORE
	CString szRoot, szOrig, szNew, szFile, szName, szGrp, szSubj;
	CFileFind ff1, ff2, ff3;
	BOOL fCont1, fCont2, fCont3, fCopy = FALSE;
	BOOL fExp = ( d.m_nType == eB_Experiment );
	BOOL fGrp = ( d.m_nType == eB_Group );
	BOOL fSubj = ( d.m_nType == eB_Subject );
	int nCount = 0, nCur = 0;
	// Root data path
	::GetDataPathRoot( szRoot );

	CWaitCursor crs;

BeginHere:
	if( fCopy )
	{
		// Progress Meter steps
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		pMF->EnableProgress( nCount );
		nCount = 0;
	}
	
	// Ensure path exists (create if need be)
	if( !ff1.FindFile( szRoot ) ) _mkdir( szRoot );
	// First...we'll restore DAT/IDX files if specified
	if( d.m_fDat )
	{
		// DAT files (not backup.dat)
		szFile.Format( _T("%s\\*.dat"), d.m_szPath );
		fCont1 = ff1.FindFile( szFile );
		while( fCont1 )
		{
			fCont1 = ff1.FindNextFile();
			szOrig = ff1.GetFilePath();
			szName = ff1.GetFileName();
			szName.MakeUpper();
			szNew.Format( _T("%s\\%s"), szRoot, szName );
			if( ( szName != _T("BACKUP.DAT") ) && fCopy )
			{
				CopyFile( szOrig, szNew, FALSE );
				nCur++;
				SetProgress( nCur, szName );
			}
			else nCount++;
		}
		// IDX files (not backup.idx)
		szFile.Format( _T("%s\\*.idx"), d.m_szPath );
		fCont1 = ff1.FindFile( szFile );
		while( fCont1 )
		{
			fCont1 = ff1.FindNextFile();
			szOrig = ff1.GetFilePath();
			szName = ff1.GetFileName();
			szName.MakeUpper();
			szNew.Format( _T("%s\\%s"), szRoot, szName );
			if( ( szName != _T("BACKUP.IDX") ) && fCopy )
			{
				CopyFile( szOrig, szNew, FALSE );
				nCur++;
				SetProgress( nCur, szName );
			}
			else nCount++;
		}
	}

	// Check exp path
	szFile.Format( _T("%s\\%s"), szRoot, d.m_szExp );
	if( !ff1.FindFile( szFile ) ) _mkdir( szFile );

	// Experiment text files
	if( fExp )
	{
		szFile.Format( _T("%s\\%s\\*.txt"), d.m_szPath, d.m_szExp );
		fCont1 = ff1.FindFile( szFile );
		while( fCont1 )
		{
			fCont1 = ff1.FindNextFile();
			szOrig = ff1.GetFilePath();
			szName = ff1.GetFileName();
			szNew.Format( _T("%s\\%s"), szRoot, szName );
			if( fCopy )
			{
				CopyFile( szOrig, szNew, FALSE );
				nCur++;
				SetProgress( nCur, szName );
			}
			else nCount++;
		}
	}
	// Loop through groups
	szFile.Format( _T("%s\\%s\\*.*"), d.m_szPath, d.m_szExp );
	fCont1 = ff1.FindFile( szFile );
	while( fCont1 )
	{
		fCont1 = ff1.FindNextFile();
		szGrp = ff1.GetFileName();
		if( ff1.IsDirectory() && ( szGrp != _T(".") ) && ( szGrp != _T("..") ) )
		{
			// Check path (if necessary)
			if( ( ( fSubj || fGrp ) && ( szGrp == d.m_szGrp ) ) ||
				fExp )
			{
				szFile.Format( _T("%s\\%s\\%s"), szRoot, d.m_szExp, szGrp );
				if( !ff2.FindFile( szFile ) ) _mkdir( szFile );
			}
			// Group text files
			if( ( fGrp && ( szGrp == d.m_szGrp ) ) || fExp )
			{
				szFile.Format( _T("%s\\%s\\%s\\*.txt"), d.m_szPath, d.m_szExp, szGrp );
				fCont2 = ff2.FindFile( szFile );
				while( fCont2 )
				{
					fCont2 = ff2.FindNextFile();
					szOrig = ff2.GetFilePath();
					szName = ff2.GetFileName();
					szNew.Format( _T("%s\\%s\\%s\\%s"), szRoot, d.m_szExp, szGrp, szName );
					if( fCopy )
					{
						CopyFile( szOrig, szNew, FALSE );
						nCur++;
						SetProgress( nCur, szName );
					}
					else nCount++;
				}
			}

			// Loop through subjects
			szFile.Format( _T("%s\\%s\\%s\\*.*"), d.m_szPath, d.m_szExp, szGrp );
			fCont2 = ff2.FindFile( szFile );
			while( fCont2 )
			{
				fCont2 = ff2.FindNextFile();
				szSubj = ff2.GetFileName();
				if( ff2.IsDirectory() && ( szSubj != _T(".") ) && ( szSubj != _T("..") ) )
				{
					if( ( fSubj && ( szSubj == d.m_szSubj ) && ( szGrp == d.m_szGrp ) ) ||
						( fGrp && ( szGrp == d.m_szGrp ) ) || 
						fExp )
					{
						// Check path
						szFile.Format( _T("%s\\%s\\%s\\%s"), szRoot, d.m_szExp, szGrp, szSubj );
						if( !ff3.FindFile( szFile ) ) _mkdir( szFile );

						// Subject text files
						szFile.Format( _T("%s\\%s\\%s\\%s\\*.txt"), d.m_szPath, d.m_szExp, szGrp, szSubj );
						fCont3 = ff3.FindFile( szFile );
						while( fCont3 )
						{
							fCont3 = ff3.FindNextFile();
							szOrig = ff3.GetFilePath();
							szName = ff3.GetFileName();
							szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szRoot, d.m_szExp, szGrp, szSubj, szName );
							if( fCopy )
							{
								CopyFile( szOrig, szNew, FALSE );
								nCur++;
								SetProgress( nCur, szName );
							}
							else nCount++;
						}
						// HWR files
						szFile.Format( _T("%s\\%s\\%s\\%s\\*.hwr"), d.m_szPath, d.m_szExp, szGrp, szSubj );
						fCont3 = ff3.FindFile( szFile );
						while( fCont3 )
						{
							fCont3 = ff3.FindNextFile();
							szOrig = ff3.GetFilePath();
							szName = ff3.GetFileName();
							szNew.Format( _T("%s\\%s\\%s\\%s\\%s"), szRoot, d.m_szExp, szGrp, szSubj, szName );
							if( fCopy )
							{
								CopyFile( szOrig, szNew, FALSE );
								nCur++;
								SetProgress( nCur, szName );
							}
							else nCount++;
						}
					}
				}
			}
		}
	}

	if( !fCopy )
	{
		fCopy = TRUE;
		goto BeginHere;
	}
	else
	{
		DisableProgress();

		BCGPMessageBox( _T("Restore has completed successfully."), MB_ICONINFORMATION | MB_OK );

		CLeftView* pLV = GetLeftView();
		if( pLV ) pLV->Refresh();
	}
}

void CMainFrame::OnFBackupdb() 
{
	FBackupdb();
}

BOOL CMainFrame::FBackupdb( BOOL fAsk ) 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return FALSE;
	}

	return NSMUsers::BackupDB( fAsk );
}

void CMainFrame::OnFRestoredb() 
{
	if( !::IsD() && !::IsSA() && !::IsOA() && !::IsGA() )
	{
		OnHelpActivate();
		return;
	}

	NSMUsers::RestoreDB();

	CLeftView* pLV = GetLeftView();
	if( pLV ) pLV->Refresh();
}

/////////////////////////////////////////////////////////////////////////////
// CLeftView

void CLeftView::WriteMemory()
{
	CTreeCtrl& tree = GetTreeCtrl();

	TreeMemory tm;
	tm.hItem = tree.GetSelectedItem();
	if( tm.hItem )
	{
		DWORD dwVal;
		dwVal = tree.GetItemData( tm.hItem );
		if( dwVal == TI_TRIAL )
		{
			tm.hItem = tree.GetParentItem( tm.hItem );
			tm.hItem = tree.GetParentItem( tm.hItem );
		}
	}

	HTREEITEM hItem = NULL, hExp = NULL, hGrps = NULL, hConds = NULL,
			  hGrp = NULL, hSubjs = NULL, hSubj = NULL, hTrials = NULL,
			  hStim = NULL, hElems = NULL, hElem = NULL;
	UINT nVal = 0;

	// File for writing memory
	CFile f;
	CString szMem;
	::GetDataPathRoot( szMem );
	szMem += _T("\\Mem.sta");
	if( !f.Open( szMem, CFile::modeWrite | CFile::modeCreate |
				 CFile::shareDenyNone | CFile::typeBinary ) )
	{
//		BCGPMessageBox( _T("Unable to write window memory file (mem.sta).") );
		return;
	}

	// Number of tree items that will be written
	int nItems = tree.GetCount();
	f.Write( (LPVOID)&nItems, sizeof( int ) );

	// Currently selected
	f.Write( (LPVOID)&tm, sizeof( tm ) );

	// WINDOW WIDTHS
	int nWidth = 200, nHeight = 0, x = 0, y = 0;
	BOOL fVal;
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	// Main Frame
	if( pMF ) pMF->GetFramePos( x, y, nWidth, nHeight );
	f.Write( (LPVOID)&x, sizeof( int ) );
	f.Write( (LPVOID)&y, sizeof( int ) );
	f.Write( (LPVOID)&nWidth, sizeof( int ) );
	f.Write( (LPVOID)&nHeight, sizeof( int ) );
	// Tree width
	if( pMF ) nWidth = pMF->GetTreeSize();
	f.Write( (LPVOID)&nWidth, sizeof( int ) );
	// Tree height
	if( pMF ) nWidth = pMF->GetTreeHeight();
	f.Write( (LPVOID)&nWidth, sizeof( int ) );
	// Msg View height
	if( pMF ) nWidth = pMF->GetMsgSize();
	f.Write( (LPVOID)&nWidth, sizeof( int ) );
	// Rec View height
	if( pMF ) nWidth = pMF->GetRecSize();
	f.Write( (LPVOID)&nWidth, sizeof( int ) );
	// Detailed output
	fVal = ::GetDetailedOutput();
	f.Write( (LPVOID)&fVal, sizeof( BOOL ) );
	// Verbosity
	fVal = ::GetVerbosity();
	f.Write( (LPVOID)&fVal, sizeof( BOOL ) );

	// Start with Experiment nodes
	// .......Root node
	tm.hItem = m_hExp;
	MEM_WRITE();
	hExp = tree.GetChildItem( tm.hItem );	// 1st experiment
	while( hExp )
	{
		tm.hItem = hExp;
		MEM_WRITE();

		// .......Groups
		hGrps = tree.GetChildItem( hExp );
		tm.hItem = hGrps;
		MEM_WRITE();
		hGrp = tree.GetChildItem( hGrps );	// 1st group
		while( hGrp )
		{
			tm.hItem = hGrp;
			MEM_WRITE();

			// Subject folder
			hSubjs = tree.GetChildItem( hGrp );
			tm.hItem = hGrps;
			MEM_WRITE();

			// Subjects
			hSubj = tree.GetChildItem( hSubjs );
			while( hSubj )
			{
				tm.hItem = hSubj;
				MEM_WRITE();

				// Trials
				hTrials = tree.GetChildItem( hSubj );
				if( hTrials )
				{
					tm.hItem = hTrials;
//					MEM_WRITE();
					// special case for trial folders
					// since we dont load trials auto
					// system gets confused
					tm.fOpen = FALSE;
					tm.fChildren = tm.hItem ? tree.ItemHasChildren( tm.hItem ) : 0;
					f.Write( (LPVOID)&tm, sizeof( tm ) );

					// Each trial
					hItem = tree.GetChildItem( hTrials );
					while( hItem )
					{
						tm.hItem = hItem;
						MEM_WRITE();

						// Next item
						hItem = tree.GetNextSiblingItem( hItem );
					}
				}

				// Next item
				hSubj = tree.GetNextSiblingItem( hSubj );
			}

			// Next item
			hGrp = tree.GetNextSiblingItem( hGrp );
		}
		// .......Conditions
		hConds = tree.GetNextSiblingItem( hGrps );
		tm.hItem = hConds;
		MEM_WRITE();
		hItem = tree.GetChildItem( hConds );	// 1st group
		while( hItem )
		{
			tm.hItem = hItem;
			MEM_WRITE();

			// Next item
			hItem = tree.GetNextSiblingItem( hItem );
		}

		// .......Next exp
		hExp = tree.GetNextSiblingItem( hExp );
	}

	// Group nodes
	MEM_WRITE_NODE( m_hGrp )

	// Subject nodes
	MEM_WRITE_NODE( m_hSub )

	// Condition nodes
	MEM_WRITE_NODE( m_hCond )

	// Stimulus nodes
	if( m_hStim )
	{
		tm.hItem = m_hStim;
		MEM_WRITE();
		hStim = tree.GetChildItem( tm.hItem );	// 1st stimulus
		while( hStim )
		{
			tm.hItem = hStim;
			MEM_WRITE();

			// .......Elements
			hElems = tree.GetChildItem( hStim );
			tm.hItem = hElems;
			MEM_WRITE();
			hElem = tree.GetChildItem( hElems );	// 1st element
			while( hElem )
			{
				tm.hItem = hElem;
				MEM_WRITE();

				// Next item
				hElem = tree.GetNextSiblingItem( hElem );
			}

			// .......Next stim
			hStim = tree.GetNextSiblingItem( hStim );
		}
	}

	// Element nodes
	if( m_hElmt ) MEM_WRITE_NODE( m_hElmt )

	// Category nodes
	if( m_hCat ) MEM_WRITE_NODE( m_hCat )

	f.Close();

	// memory Lock file
	CString szLck;
	::GetDataPathRoot( szLck );
	szLck += _T("\\ma.lck");
	CFileFind ff;
	if( ff.FindFile( szLck ) ) REMOVE_FILE( szLck );

	// user lock file
	::GetDataPathRoot( szLck );
	szLck += _T("\\user.lck");
	if( ff.FindFile( szLck ) )
	{
		// if we are the only instance of movalyzer running, just delete the file
		// this will work for a given windows user running multiple instances
		// of the app or multiple users accessing same app user...however, it is
		// unknown how this will work with "Fast User Switching" of XP (same likely)
		BOOL fOnlyOne = TRUE;
		int nCount = 0;
		CWnd* pWnd = AfxGetApp()->m_pMainWnd->GetNextWindow( GW_HWNDPREV );
		CString szTitle;
		// search backwards
		while( pWnd )
		{
			pWnd->GetWindowText( szTitle );
			szTitle.MakeUpper();
			if( szTitle.Find( _T("MOVALYZER") ) != -1 )
				nCount++;
			pWnd = pWnd->GetNextWindow( GW_HWNDPREV );
		}
		// search forward
		pWnd = AfxGetApp()->m_pMainWnd->GetNextWindow( GW_HWNDNEXT );
		while( pWnd )	// it will find itself as well
		{
			pWnd->GetWindowText( szTitle );
			szTitle.MakeUpper();
			if( szTitle.Find( _T("MOVALYZER") ) != -1 )
				nCount++;
			pWnd = pWnd->GetNextWindow( GW_HWNDNEXT );
		}
		nCount--;	// it will find itself as well
		if( nCount > 1 ) fOnlyOne = FALSE;

		// open file and read ref count, rewrite with decremented count
		CStdioFile lf;
		if( !fOnlyOne && lf.Open( szLck, CFile::modeRead ) )
		{
			// read count line
			CString szLine;
			lf.ReadString( szLine );
			// convert to numeric and increment
			int nCount = atoi( szLine );
			nCount--;
			szLine.Format( _T("%d"), nCount );
			// close file & delete
			lf.Close();
			REMOVE_FILE( szLck );

			// recreate file with new ref count (if not 0)
			if( ( nCount > 0 ) &&
				lf.Open( szLck, CFile::modeWrite | CFile::modeCreate ) )
				lf.WriteString( szLine );
		}
		else REMOVE_FILE( szLck );
	}
}

void CLeftView::ReadMemory()
{
	CTreeCtrl& tree = GetTreeCtrl();

	TreeMemory tm;
	HTREEITEM hItem = m_hExp, hLvl1 = NULL, hLvl2 = NULL, hLvl3 = NULL, hLvl4 = NULL,
			  hLvl5 = NULL, hLvl6 = NULL, hLvl7 = NULL, hRoot = NULL, hSel = NULL,
			  hLvl8 = NULL;
	UINT nVal = 0;
	int nLines = 0, nLastLvl = 0;

	// File for reading memory
	CString szMem, szLck;
	::GetDataPathRoot( szMem );
	::GetDataPathRoot( szLck );
	szMem += _T("\\Mem.sta");
	szLck += _T("\\ma.lck");
	CFileFind ff;
	if( ff.FindFile( szLck ) )
	{
//		if( ff.FindFile( szMem ) ) REMOVE_FILE( szMem );
//		return;
	}
	else
	{
		CStdioFile lf;
		if( lf.Open( szLck, CFile::modeWrite | CFile::modeCreate ) )
		{
			lf.WriteString( _T("In Use") );
			lf.Close();
		}
	}

	CFile f;
	if( !f.Open( szMem, CFile::modeRead | CFile::typeBinary ) )
		return;

	// Get number of lines
	f.Read( (LPVOID)&nLines, sizeof( int ) );

	// Selected Tree Item
	f.Read( (LPVOID)&tm, sizeof( tm ) );
	hSel = tm.hItem;

	// WINDOW WIDTHS
	int nWidth = 0, nHeight = 0, nTWidth = 0, nTHeight = 0, nMHeight = 0,
		nRHeight = 0, x = 0, y = 0;
	BOOL fVal;
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	// Main Frame
	f.Read( (LPVOID)&x, sizeof( int ) );
	f.Read( (LPVOID)&y, sizeof( int ) );
	f.Read( (LPVOID)&nWidth, sizeof( int ) );
	f.Read( (LPVOID)&nHeight, sizeof( int ) );
	// Tree width
	f.Read( (LPVOID)&nTWidth, sizeof( int ) );
	// Tree height
	f.Read( (LPVOID)&nTHeight, sizeof( int ) );
	// Msg View height
	f.Read( (LPVOID)&nMHeight, sizeof( int ) );
	// Rec View height
	f.Read( (LPVOID)&nRHeight, sizeof( int ) );
	// Detailed output
	f.Read( (LPVOID)&fVal, sizeof( BOOL ) );
	::SetDetailedOutput( fVal );
	::SetRapidProcessing( !fVal );
	IProcess* pProc = Experiments::GetProcessObject( pMF->GetMsgPaneList() );
	if( pProc ) pProc->SetRapidView( !fVal );
	// Verbosity
	f.Read( (LPVOID)&fVal, sizeof( BOOL ) );
	::SetVerbosity( fVal );

	// Loop through for each tree item
	for( UINT i = 0; ( i < (UINT)nLines ) && ( i < tree.GetCount() ); i++ )
	{
		f.Read( (LPVOID)&tm, sizeof( tm ) );
		if( tm.hItem )
		{
			if( tm.fOpen )
			{
				DWORD dwVal = tree.GetItemData( hItem );
				if( dwVal != TI_SUBJECT ) tree.Expand( hItem, TVE_EXPAND );
			}

			if( tree.ItemHasChildren( hItem ) )
			{
				switch( nLastLvl )
				{
					case 0: hRoot = hItem; break;
					case 1: hLvl1 = hItem; break;
					case 2: hLvl2 = hItem; break;
					case 3: hLvl3 = hItem; break;
					case 4: hLvl4 = hItem; break;
					case 5: hLvl5 = hItem; break;
					case 6: hLvl6 = hItem; break;
					case 7: hLvl7 = hItem; break;
					case 8: hLvl8 = hItem; break;
				}
				if( tree.ItemHasChildren( hItem ) ) nLastLvl++;
				hItem = tree.GetChildItem( hItem );
			}
			else if( i == (UINT)( nLines - 1 ) ) break;
			else
			{
				hItem = tree.GetNextSiblingItem( hItem );
				while( !hItem )
				{
					nLastLvl--;
					switch( nLastLvl )
					{
						case 0: hItem = hRoot; break;
						case 1: hItem = hLvl1; break;
						case 2: hItem = hLvl2; break;
						case 3: hItem = hLvl3; break;
						case 4: hItem = hLvl4; break;
						case 5: hItem = hLvl5; break;
						case 6: hItem = hLvl6; break;
						case 7: hItem = hLvl7; break;
						case 8: hItem = hLvl8; break;
					}
					if( hItem ) hItem = tree.GetNextSiblingItem( hItem );
					else break;
				}
			}
		}
	}

//	if( hSel ) tree.SelectItem( hSel );		// doesn't work
	tree.SelectItem( m_hExp );
}

void CMainFrame::OnUpdateViewsSettings(CCmdUI* pCmdUI)
{
	BOOL fEnable = !m_fContinue;
	fEnable &= ::IsGripperInstalled();
	fEnable &= !::IsClientServerModeOn();
	if( fEnable ) fEnable &= ::IsGripperModeOn();

	pCmdUI->Enable( fEnable );
}

void CMainFrame::OnUpdateViewsCont(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( !m_fContinue && !::IsClientServerModeOn() );
}

void CMainFrame::OnUpdateViewsUsers(CCmdUI* pCmdUI) 
{
	BOOL fEnable = !m_fContinue;
	CLeftView* pLV = GetLeftView();
	if( pLV && pLV->GetActionState() == ACTION_REPROCESS ) fEnable = FALSE;
	pCmdUI->Enable( fEnable );
}

void CMainFrame::OnUpdateODologging(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::IsLoggingOn() );
}

void CMainFrame::OnUpdateOLogdb(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::GetLogDB() );
}

void CMainFrame::OnUpdateOLoggraph(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::GetLogGraph() );
}

void CMainFrame::OnUpdateOLogproc(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::GetLogProc() );
}

void CMainFrame::OnUpdateOLogtablet(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::GetLogTablet() );
}

void CMainFrame::OnUpdateOLogui(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( ::GetLogUI() );
}

void CMainFrame::OnRecoverPass()
{
	CString szSig = ::GetSignature();
	CString szSiteID = ::GetCurrentUserSiteID();
	CString szSiteDesc = ::GetCurrentUserSiteDesc();
	CString szFilePath, szFile, szUserID;
	::GetDataPathRoot( szFilePath );
	::GetCurrentUser( szUserID );
	Subjects subj;
	if( subj.RecoverPassword( szUserID, szSiteID, szSiteDesc, szSig, szFilePath, szFile, TRUE ) )
	{
		CString szMsg;
		szMsg.Format( _T("User password recovery file successfully created in:\n\n%s.\n\nEmail this file to support@neuroscript.net with subject \"PASSWORD RECOVERY\".\nNeuroScript will respond with details on proceeding."), szFile );
		BCGPMessageBox( szMsg );
	}
	else OutputAMessage( _T("Error in recovering password.") );
}

void CMainFrame::OnUpdateRecoverPass( CCmdUI* pCmdUI )
{
	BOOL fEnable = !m_fContinue;
	CLeftView* pLV = GetLeftView();
	if( pLV && ( pLV->GetActionState() != ACTION_NONE ) ) fEnable = FALSE;
	pCmdUI->Enable( fEnable );
}

void CMainFrame::OnRestorePass()
{
	CString szSig = ::GetSignature();
	CString szSiteID = ::GetCurrentUserSiteID();
	CString szSiteDesc = ::GetCurrentUserSiteDesc();
	CString szUserID;
	::GetCurrentUser( szUserID );
	Subjects subj;
	if( subj.RestorePassword( szUserID, szSiteID, szSiteDesc, szSig, TRUE ) )
	{
		CString szMsg = _T("User password restoration successfully completed.\nYou can now begin using your newly assigned password.");
		BCGPMessageBox( szMsg );
		CLeftView* pLV = GetLeftView();
		if( pLV ) pLV->Refresh();
	}
	else OutputAMessage( _T("Error in restoring password.") );
}

void CMainFrame::OnUpdateRestorePass( CCmdUI* pCmdUI )
{
	BOOL fEnable = !m_fContinue;
	CLeftView* pLV = GetLeftView();
	if( pLV && ( pLV->GetActionState() != ACTION_NONE ) ) fEnable = FALSE;
	pCmdUI->Enable( fEnable );
}

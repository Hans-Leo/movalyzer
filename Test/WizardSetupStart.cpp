// WizardSetupStart.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupStart.h"
#include "WizardSetupSheet.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardSetupStart property page

IMPLEMENT_DYNCREATE(WizardSetupStart, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardSetupStart, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupStart::WizardSetupStart() : CBCGPPropertyPage(WizardSetupStart::IDD)
{
}

WizardSetupStart::~WizardSetupStart()
{
}

void WizardSetupStart::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupStart message handlers

LRESULT WizardSetupStart::OnWizardNext() 
{
	return CBCGPPropertyPage::OnWizardNext();
}

BOOL WizardSetupStart::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	pParent->SetWizardButtons( PSWIZB_NEXT );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardSetupStart::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newexperiment.html#wizard"), this );
	return TRUE;
}

BOOL WizardSetupStart::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

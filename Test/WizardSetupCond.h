#pragma once

// WizardSetupCond.h : header file
//

#include "Conditions.h"
#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardSetupCond dialog

class WizardSetupCond : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardSetupCond)

// Construction
public:
	WizardSetupCond();
	~WizardSetupCond();

// Dialog Data
	enum { IDD = IDD_WIZ_SETUP_COND };
	CBCGPComboBox	m_Cbo;
	CString			m_szCond;
	CString			m_szID;
	CString			m_szDesc;
	int				m_nReps;
	NSConditions	m_cond;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnSelchangeCboConds();
	afx_msg void OnBnCreatecond();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

// TestDoc.h : interface of the CTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTDOC_H__E1050F7D_8B15_11D3_8A21_00104BC7E2C8__INCLUDED_)
#define AFX_TESTDOC_H__E1050F7D_8B15_11D3_8A21_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTestDoc : public CDocument
{
public:
	CTestDoc() {}
	DECLARE_DYNCREATE(CTestDoc)

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTDOC_H__E1050F7D_8B15_11D3_8A21_00104BC7E2C8__INCLUDED_)

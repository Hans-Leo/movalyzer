// RecView.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "RecView.h"
#include "MainFrm.h"
#include "GraphView.h"
#include "LeftView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static CFont	_fnt;
static LOGFONT	_lf;
static BOOL		_SetFont = FALSE;

#define INSTR_LEFT_SHIFT	4
#define INSTR_TOP_SHIFT		4

/////////////////////////////////////////////////////////////////////////////
// CRecView

IMPLEMENT_DYNCREATE(CRecView, CBCGPFormView)

BEGIN_MESSAGE_MAP(CRecView, CBCGPFormView)
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CRecView::CRecView() : CBCGPFormView(CRecView::IDD)
{
	m_szInstr = _T("");
}

CRecView::~CRecView()
{
}

void CRecView::DoDataExchange(CDataExchange* pDX)
{
	CBCGPFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_INSTR, m_Instr);
	DDX_Text(pDX, IDC_EDIT_INSTR, m_szInstr);
}

/////////////////////////////////////////////////////////////////////////////
// CRecView message handlers

void CRecView::OnDraw( CDC* pDC )
{
	CBCGPFormView::OnDraw( pDC );

	// window background
	DWORD dw = ::GetSysColor( COLOR_WINDOW );
	HBRUSH hbr = CreateSolidBrush( dw );
	HBRUSH hbrOld = (HBRUSH)pDC->SelectObject( hbr );
	RECT rect;
	GetWindowRect( &rect );
	pDC->Rectangle( 0, 0, rect.right, rect.bottom );
	DeleteObject( hbrOld );
}

void CRecView::UpdateInstruction( CString szInstr )
{
	m_szInstr = szInstr;
	UpdateData( FALSE );

	// Font of instruction window
	if( !_SetFont )
	{
		LOGFONT lf;
		CFont* pFont = m_Instr.GetFont();
		pFont->GetLogFont( &lf );
		lf.lfHeight = 180;
		lf.lfWeight = FW_HEAVY;
		_fnt.CreatePointFontIndirect( &lf );

		m_Instr.SetFont( &_fnt, TRUE );
		m_Instr.Invalidate();

		_SetFont = TRUE;
	}
}

void CRecView::OnClear()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CGraphView* pGV = pMF ? pMF->GetRecordingPane() : NULL;
	if( pGV )
	{
		pGV->Clear();
		CString szData;
		szData.LoadString( IDS_RV_CLEAR );
		OutputAMessage( szData, TRUE );

		// if not running an exp, reset instruction
		BOOL fRunExp = !pGV->fRecording;
		CString szInstr;
		szInstr.LoadString( IDS_RV_INSTR );
		UpdateInstruction( szInstr );
	}
}

void CRecView::Enable( BOOL fEnable )
{
// 	CWnd* pWnd = GetDlgItem( IDC_EDIT_INSTR );
// 	if( pWnd ) pWnd->EnableWindow( fEnable );
}

void CRecView::OnInitialUpdate() 
{
	CBCGPFormView::OnInitialUpdate();

	if( m_Instr.GetSafeHwnd() )
	{
		CRect rectWnd;
		GetWindowRect( rectWnd );
		ScreenToClient( rectWnd );
		m_Instr.MoveWindow( INSTR_LEFT_SHIFT, INSTR_LEFT_SHIFT,
							rectWnd.Width() - INSTR_LEFT_SHIFT,
							rectWnd.Height() - INSTR_LEFT_SHIFT, FALSE );
	}

	CString szInstr;
	szInstr.LoadString( IDS_RV_INSTR );
	UpdateInstruction( szInstr );

	Enable( FALSE );
}

void CRecView::OnSize( UINT nType, int cx, int cy )
{
	CView::OnSize( nType, cx, cy );
	if( !m_Instr.GetSafeHwnd() ) return;
	m_Instr.MoveWindow( INSTR_LEFT_SHIFT, INSTR_LEFT_SHIFT,
						cx - INSTR_LEFT_SHIFT,
						cy - INSTR_LEFT_SHIFT, FALSE );
}

void CRecView::OnStopExp()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF )
	{
		pMF->StopExperiment();
		CString szData;
		szData.LoadString( IDS_RV_STOPEXP );
		OutputAMessage( szData, TRUE );

		CString szInstr;
		szInstr.LoadString( IDS_RV_INSTR );
		UpdateInstruction( szInstr );

		Enable( FALSE );
	}
}

void CRecView::OnStopCond()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF )
	{
		pMF->StopCondition();
		CString szData;
		szData.LoadString( IDS_RV_STOPCOND );
		OutputAMessage( szData, TRUE );

		CString szInstr;
		szInstr.LoadString( IDS_RV_INSTR );
		UpdateInstruction( szInstr );
	}
}

void CRecView::OnStopRec()
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF )
	{
		pMF->StopRecording();
		CString szData;
		szData.LoadString( IDS_RV_STOPREC );
		OutputAMessage( szData, TRUE );

// 		CString szInstr;
// 		szInstr.LoadString( IDS_RV_INSTR );
// 		UpdateInstruction( szInstr );
	}
}

void CRecView::Clear()
{
	UpdateData( FALSE );
	Enable( FALSE );
}

void CRecView::OnSetFocus(CWnd* pOldWnd) 
{
	CBCGPFormView::OnSetFocus(pOldWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Instruction Window: This has the experiment instruction.");
	pMF->ShowStatusText( szMsg );
}

void CRecView::OnKillFocus(CWnd* pNewWnd) 
{
	CBCGPFormView::OnKillFocus(pNewWnd);

	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CString szMsg = _T("Ready.");
	pMF->ShowStatusText( szMsg );
}

#define	ENTER_KEY	13
void CRecView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if( nChar == ENTER_KEY )
	{
		CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CGraphView* pGV = pMF->GetRecordingPane();
		if( pGV ) pGV->OnKeyDown( nChar );
	}
}

void CRecView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if( pMF ) pMF->HandleMouseMoveForRecording();
	CBCGPFormView::OnMouseMove( nFlags, point );
}

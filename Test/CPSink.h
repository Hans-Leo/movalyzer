#if !defined(AFX_CPSINK_H__9E0D3254_F9B0_4761_BC91_D4BB57E7ADB4__INCLUDED_)
#define AFX_CPSINK_H__9E0D3254_F9B0_4761_BC91_D4BB57E7ADB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CPSink.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCPSink command target

#ifndef	_WRITALYZER_
class CMainFrame;
#else
class CWritAlyzeRDlg;
#endif

class CCPSink : public CCmdTarget
{
	DECLARE_DYNCREATE(CCPSink)

public:
	CCPSink();           // protected constructor used by dynamic creation
	virtual ~CCPSink();

// Attributes
public:
#ifndef	_WRITALYZER_
	CMainFrame*	m_pRefMainForm;
#else
	CWritAlyzeRDlg* m_pRefMainForm;
#endif

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCPSink)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCPSink)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CCPSink)
	afx_msg void OnMissingLicenseFile(LPCTSTR ExpectedFileNamePath, BSTR FAR* ActualFileNamePath);
	afx_msg void OnAttempReleaseCP(LPCTSTR ReleaseCPAuthRequestCode, BSTR FAR* InputActivationCode);
	afx_msg void OnAttemptRecover(LPCTSTR RecoverAuthRequestCode, BSTR FAR* InputActivationCode);
	afx_msg void OnAttemptDeactivate(short FAR* AllowDeactivate);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CPSINK_H__9E0D3254_F9B0_4761_BC91_D4BB57E7ADB4__INCLUDED_)

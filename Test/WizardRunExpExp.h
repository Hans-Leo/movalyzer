#pragma once

// WizardRunExpExp.h : header file
//

#include "NSTooltipCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// WizardRunExpExp dialog

class WizardRunExpExp : public CBCGPPropertyPage
{
	DECLARE_DYNCREATE(WizardRunExpExp)

// Construction
public:
	WizardRunExpExp();
	~WizardRunExpExp();

// Dialog Data
	enum { IDD = IDD_WIZ_RUNEXP_EXP };
	CBCGPComboBox	m_Cbo;
	CString			m_szID;
	CString			m_szDesc;
	CString			m_szType;
	CString			m_szNotes;
	int				m_nType;
	BOOL			m_fUpdated;
	BOOL			m_fNew;
	NSToolTipCtrl	m_tooltip;

// Overrides
public:
	virtual LRESULT OnWizardBack();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
	BOOL DoHelp( HELPINFO* );
protected:
	afx_msg void OnBnCreateexp();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCboExps();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

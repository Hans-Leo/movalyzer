#pragma once

// TestDeviceDlg.h : header file
//

#include "..\NSSharedGUI\NSTooltipCtrl.h"

class SigGenSettingsDlg;

/////////////////////////////////////////////////////////////////////////////
// TestDeviceDlg dialog

class TestDeviceDlg : public CBCGPDialog
{
// Construction
public:
	TestDeviceDlg(CWnd* pParent = NULL);
	~TestDeviceDlg();

// Dialog Data
	enum { IDD = IDD_TEST };
	NSToolTipCtrl	m_tooltip;
	BOOL			m_fRaw;
	BOOL			m_fLinear;
	BOOL			m_fDraw;
	BOOL			m_fGen;
	BOOL			m_fDidGen;
	BOOL			m_fDidSet;
	SigGenSettingsDlg* dlgSettings;

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
public:
	virtual BOOL PreTranslateMessage( MSG*  );
protected:
	void SetRadioStates();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnSettings();
	afx_msg void OnBnEdit();
	afx_msg void OnBnGo();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_MESSAGE_MAP()
};

// WizardSetupEnd.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "WizardSetupEnd.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "WizardSetupSheet.h"
#include "..\DataMod\DataMod.h"
#include "WizardSetupExp.h"
#include "WizardSetupCond.h"
#include "WizardSetupGrp.h"
#include "WizardSetupSubj.h"
#include "Subjects.h"
#include "Groups.h"
#include "Experiments.h"
#include "..\NSSharedGUI\NSSharedGUI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WizardSetupEnd property page

IMPLEMENT_DYNCREATE(WizardSetupEnd, CBCGPPropertyPage)

BEGIN_MESSAGE_MAP(WizardSetupEnd, CBCGPPropertyPage)
	ON_WM_HELPINFO()
END_MESSAGE_MAP()

WizardSetupEnd::WizardSetupEnd() : CBCGPPropertyPage(WizardSetupEnd::IDD)
{
	m_szExpID = _T("");
	m_szExpDesc = _T("");
	m_szCondID = _T("");
	m_szCondDesc = _T("");
	m_szCondReps = _T("");
	m_szGrpID = _T("");
	m_szGrpDesc = _T("");
	m_szSubjID = _T("");
	m_szSubjName = _T("");
	m_szType = _T("");
}

WizardSetupEnd::~WizardSetupEnd()
{
}

void WizardSetupEnd::DoDataExchange(CDataExchange* pDX)
{
	CBCGPPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TXT_EXPID, m_szExpID);
	DDX_Text(pDX, IDC_TXT_EXPDESC, m_szExpDesc);
	DDX_Text(pDX, IDC_TXT_CONDID, m_szCondID);
	DDX_Text(pDX, IDC_TXT_CONDDESC, m_szCondDesc);
	DDX_Text(pDX, IDC_TXT_CONDREPS, m_szCondReps);
	DDX_Text(pDX, IDC_TXT_GRPID, m_szGrpID);
	DDX_Text(pDX, IDC_TXT_GRPDESC, m_szGrpDesc);
	DDX_Text(pDX, IDC_TXT_SUBJID, m_szSubjID);
	DDX_Text(pDX, IDC_TXT_SUBJNAME, m_szSubjName);
	DDX_Text(pDX, IDC_TXT_EXPTYPE, m_szType);
}

/////////////////////////////////////////////////////////////////////////////
// WizardSetupEnd message handlers

BOOL WizardSetupEnd::OnInitDialog() 
{
	CBCGPPropertyPage::OnInitDialog();

	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();
	WizardSetupExp* pExp = (WizardSetupExp*)pParent->GetPage( 1 );
	WizardSetupCond* pCond = (WizardSetupCond*)pParent->GetPage( 2 );
	WizardSetupGrp* pGrp = (WizardSetupGrp*)pParent->GetPage( 3 );
//	WizardSetupSubj* pSubj = (WizardSetupSubj*)pParent->GetPage( 4 );

	m_szExpID = pExp->m_szID;
	m_szExpDesc = pExp->m_szDesc;
	m_nExpType = pExp->m_nType;
	m_szCondID = pCond->m_szID;
	m_szCondDesc = pCond->m_szDesc;
	m_szCondReps.Format( _T("%d"), pCond->m_nReps );
	m_szGrpID = pGrp->m_szID;
	m_szGrpDesc = pGrp->m_szDesc;
// 	m_szSubjID = pSubj->m_szID;
// 	if( m_szSubjID != _T("") )
// 		m_szSubjName.Format( _T("%s %s (%s)"), pSubj->m_szFirst, pSubj->m_szLast, pSubj->m_szCode );
	switch( pExp->m_nType )
	{
		case EXP_TYPE_HANDWRITING:
			m_szType = _T("Handwriting");
			break;
		case EXP_TYPE_GRIPPER:
			m_szType = _T("Grip-Force");
			break;
		default:
			m_szType = _T("Unknown");
	}

	UpdateData( FALSE );

	pParent->SetFinishText( _T("Finish") );
#define UM_ADJUSTBUTTONS  (WM_USER + 1002)
	pParent->PostMessage( UM_ADJUSTBUTTONS );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL WizardSetupEnd::OnWizardFinish() 
{
	BSTR bstrVal = NULL;
	short nVal = 0;
	double dVal = 0.0;
	HRESULT hr;
	WizardSetupSheet* pParent = (WizardSetupSheet*)GetParent();

	// Update Experiment
	WizardSetupExp* pExpW = (WizardSetupExp*)pParent->GetPage( 1 );
	{
	Experiments exp;
	if( !exp.IsValid() ) return FALSE;
	hr = exp.Find( m_szExpID );
	if( FAILED( hr ) )
	{
		exp.PutID( m_szExpID );
		exp.PutDescription( m_szExpDesc );
		exp.PutNotes( pExpW->m_szNotes );
		exp.PutType( pExpW->m_nType );
		hr = exp.Add();
		if( FAILED( hr ) )
		{
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" WizardSetupEnd::OnWizardFinish:1") );
			return FALSE;
		}
	}
	if( pExpW->m_pPS ) pExpW->m_pPS->Add();
	}

	// Update Condition
	WizardSetupCond* pCondW = (WizardSetupCond*)pParent->GetPage( 2 );
	if( !pCondW ) return FALSE;
	{
	NSConditions cond;
	if( FAILED( cond.Find( pCondW->m_szID ) ) )
		if( FAILED( pCondW->m_cond.Modify() ) ) return FALSE;
	}

	// Update Group
	WizardSetupGrp* pGrpW = (WizardSetupGrp*)pParent->GetPage( 3 );
	{
	Groups grp;
	if( !grp.IsValid() ) return FALSE;
	hr = grp.Find( m_szGrpID );
	if( FAILED( hr ) )
	{
		grp.PutID( m_szGrpID );
		grp.PutDescription( m_szGrpDesc );
		grp.PutNotes( pGrpW->m_szNotes );
		hr = grp.Add();
		if( FAILED( hr ) ) return FALSE;
	}
	}

	// Update Subject
#if 0
	if( m_szSubjID != _T("") )
	{
		WizardSetupSubj* pSubjW = (WizardSetupSubj*)pParent->GetPage( 4 );
		Subjects subj;
		if( !subj.IsValid() ) return FALSE;
		hr = subj.Find( m_szSubjID );
		if( FAILED( hr ) )
		{
			subj.PutID( m_szSubjID );
			subj.PutCode( pSubjW->m_szCode );
			subj.PutNameFirst( pSubjW->m_szFirst );
			subj.PutNameLast( pSubjW->m_szLast );
			subj.PutNameFirst( pSubjW->m_szNotes );
			subj.PutPrvNotes( pSubjW->m_szPrvNotes );
			subj.PutDateAdded( pSubjW->m_DateAdded );
			hr = subj.Add();
			if( FAILED( hr ) ) return FALSE;
		}
	}
	else m_szSubjID = HOLDER;
#endif

	// Update Exp/Grp/Subj relationship
	IExperimentMember* pEM = NULL;
	hr = CoCreateInstance( CLSID_ExperimentMember, NULL, CLSCTX_ALL,
						   IID_IExperimentMember, (LPVOID*)&pEM );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPMEM_ERR );
		return FALSE;
	}
	if( m_szSubjID == _T("") ) m_szSubjID = HOLDER;
	hr = pEM->Find( m_szExpID.AllocSysString(),
					m_szGrpID.AllocSysString(),
					m_szSubjID.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEM->put_ExperimentID( m_szExpID.AllocSysString() );
		pEM->put_GroupID( m_szGrpID.AllocSysString() );
		pEM->put_SubjectID( m_szSubjID.AllocSysString() );
		hr = pEM->Add();
		if( FAILED( hr ) )
		{
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" WizardSetupEnd::OnWizardFinish:2") );
			pEM->Release();
			return FALSE;
		}
	}
	pEM->Release();

	// Update Exp/Cond relationship
	IExperimentCondition* pEC = NULL;
	hr = CoCreateInstance( CLSID_ExperimentCondition, NULL, CLSCTX_ALL,
						   IID_IExperimentCondition, (LPVOID*)&pEC );
	if( FAILED( hr ) )
	{
		BCGPMessageBox( IDS_EXPCOND_ERR );
		return FALSE;
	}
	hr = pEC->Find( m_szExpID.AllocSysString(),
					m_szCondID.AllocSysString() );
	if( FAILED( hr ) )
	{
		pEC->put_ExperimentID( m_szExpID.AllocSysString() );
		pEC->put_ConditionID( m_szCondID.AllocSysString() );
		pEC->put_Replications( pCondW->m_nReps );
		hr = pEC->Add();
		if( FAILED( hr ) )
		{
			CString szTempMsg;
			szTempMsg.LoadString(IDS_ADD_ERR);
			BCGPMessageBox( szTempMsg+_T(" WizardSetupEnd::OnWizardFinish:3") );
			pEC->Release();
			return FALSE;
		}
	}
	else
	{
		pEC->put_Replications( pCondW->m_nReps );
		hr = pEC->Modify();
		if( FAILED( hr ) )
		{
			BCGPMessageBox( IDS_UPD_ERR );
			pEC->Release();
			return FALSE;
		}
	}
	pEC->Release();

	// Refresh LeftView
	CMainFrame* pMF = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CLeftView* pLV = pMF ? pMF->GetLeftView() : NULL;
	if( pLV )
	{
		pLV->Refresh();
		HTREEITEM hItem = pLV->Find( pLV->m_hExp, m_szExpID );
		if( hItem ) pLV->SelectItem( hItem );
	}

	pParent->m_szExpID = m_szExpID;
	pParent->m_szExpDesc = m_szExpDesc;

	return CBCGPPropertyPage::OnWizardFinish();
}

BOOL WizardSetupEnd::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	::GoToHelp( _T("newexperiment.html#wizard"), this );
	return TRUE;
}

BOOL WizardSetupEnd::DoHelp(HELPINFO* pHelpInfo) 
{
	return OnHelpInfo( pHelpInfo );
}

#include "stdafx.h"
#include "Mail.h"

/////////////////////////////////////////////////////////////////////////////
// CMail

CMail::CMail()
{
	try
	{
		m_strSubject = _T("");
		m_strMessage = _T("");
		m_strAttachmentFilePath = _T("");
		m_strAttachmentFile = _T("");;
		m_strRecipient = _T(" ");	
		
		m_lhSession			= 0L;
		m_MAPIAddress		= NULL;
		m_MAPIDetails		= NULL;
		m_MAPIFindNext		= NULL;
		m_MAPIFreeBuffer	= NULL;
		m_MAPILogoff		= NULL;
		m_MAPILogon			= NULL;
		m_MAPIReadMail		= NULL;
		m_MAPIResolveName	= NULL;
		m_MAPISendDocuments	= NULL;
		m_MAPISendMail		= NULL;
		m_MAPISaveMail		= NULL;
		
		InitMapi();
	}
	catch(_com_error oComErr)
	{
		
		throw oComErr.Error();
	}
	catch(...)
	{
		
		throw E_FAIL;
	}
}

CMail::~CMail()
{
	
	m_lhSession			= 0L;
	m_MAPIAddress		= NULL;
	m_MAPIDetails		= NULL;
	m_MAPIFindNext		= NULL;
	m_MAPIFreeBuffer	= NULL;
	m_MAPILogoff		= NULL;
	m_MAPILogon			= NULL;
	m_MAPIReadMail		= NULL;
	m_MAPIResolveName	= NULL;
	m_MAPISendDocuments	= NULL;
	m_MAPISendMail		= NULL;
	m_MAPISaveMail		= NULL;
}


STDMETHODIMP CMail::get_strSubject(BSTR *pVal)
{
	try
	{
		*pVal = m_strSubject.Copy();
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

void CMail::InitMapi()
{
	ULONG unlResult;

	unlResult = IsMapiInstalled() ;
	if(unlResult != SUCCESS_SUCCESS) throw unlResult;

	// Get instance handle of MAPI32.DLL
	HINSTANCE			hlibMAPI		= LoadLibrary ( _T("MAPI32.dll") );	

	//  Get the addresses of all the API's supported by this object
	m_MAPILogon			= ( LPMAPILOGON			)	GetProcAddress ( hlibMAPI, "MAPILogon"			);
	m_MAPISendMail		= ( LPMAPISENDMAIL		)	GetProcAddress ( hlibMAPI, "MAPISendMail"		);
	m_MAPISendDocuments	= ( LPMAPISENDDOCUMENTS )	GetProcAddress ( hlibMAPI, "MAPISendDocuments"	);
	m_MAPIFindNext		= ( LPMAPIFINDNEXT		)	GetProcAddress ( hlibMAPI, "MAPIFindNext"		);
	m_MAPIReadMail		= ( LPMAPIREADMAIL		)	GetProcAddress ( hlibMAPI, "MAPIReadMail"		);
	m_MAPIResolveName	= ( LPMAPIRESOLVENAME	)	GetProcAddress ( hlibMAPI, "MAPIResolveName"	);
	m_MAPIAddress		= ( LPMAPIADDRESS		)	GetProcAddress ( hlibMAPI, "MAPIAddress"		);
	m_MAPILogoff		= ( LPMAPILOGOFF		)	GetProcAddress ( hlibMAPI, "MAPILogoff"			);
	m_MAPIFreeBuffer	= ( LPMAPIFREEBUFFER	)	GetProcAddress ( hlibMAPI, "MAPIFreeBuffer"		);   
	m_MAPIDetails		= ( LPMAPIDETAILS		)	GetProcAddress ( hlibMAPI, "MAPIDetails"		);
	m_MAPISaveMail		= ( LPMAPISAVEMAIL		)	GetProcAddress ( hlibMAPI, "MAPISaveMail"		);
}

ULONG CMail::IsMapiInstalled()
{
	try
	{
		DWORD  SimpleMAPIInstalled;
		char   szAppName[32];
		char   szKeyName[32];
		char   szDefault = {'0'};
		char   szReturn = {'0'};
		DWORD  nSize = 0L;
		char   szFileName[256];


		strcpy_s ( szAppName, "MAIL" );
		strcpy_s ( szKeyName, "MAPI" );

		nSize = 1;
		strcpy_s ( szFileName, "WIN.INI" );


		SimpleMAPIInstalled = GetPrivateProfileString ( (TCHAR*) szAppName, 
			(TCHAR*) szKeyName, 
			(TCHAR*)&szDefault,
			(TCHAR*)&szReturn, 
			nSize, 
			(TCHAR*)szFileName);


		if ( MAPI_INSTALLED == strcmp ( &szDefault, &szReturn ) )
		{
			throw  _T("MAPI not Installed");
		}
		else
		{
			return MAPI_INSTALLED;
		}
		
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(ULONG nlExcep)
	{
		_bstr_t oErrorMsg;
		oErrorMsg = GetMessageFromStringTable(nlExcep);
		AfxMessageBox( (char*)oErrorMsg );
		return E_FAIL;
	}
	catch(...)
	{		
		return E_FAIL;
	}
}

CComBSTR CMail::GetMessageFromStringTable(UINT unMessageID)
{
	CComBSTR strMessage;

	switch (unMessageID)
	{ 
	case MAPI_E_AMBIGUOUS_RECIPIENT:
		strMessage = (_T("Error: Ambiguous Recipient"));		
		break;
	case MAPI_E_UNKNOWN_RECIPIENT:
		strMessage = (_T("Error: Unknown Recipient"));		
		break;
	case MAPI_E_FAILURE:
		strMessage = (_T("Error: Mail Failure"));		
		break;
	case MAPI_E_INSUFFICIENT_MEMORY:
		strMessage = (_T("Error: Insufficient memory"));
		break;
	case MAPI_E_LOGIN_FAILURE:
		strMessage = (_T("Error: Login failure"));
		break;
	case MAPI_E_NOT_SUPPORTED:
		strMessage = (_T("Error: Mail not supported"));
		break;
	case MAPI_E_USER_ABORT:
		strMessage = (_T("Error: User Abort"));
		break;		
	case MAPI_E_ATTACHMENT_NOT_FOUND:
		strMessage = (_T("Error: Attachment not found"));
		break;
	case MAPI_E_ATTACHMENT_OPEN_FAILURE:
		strMessage = (_T("Error: Attachment open failure"));;
		break;
	case MAPI_E_BAD_RECIPTYPE:
		strMessage = (_T("Error: Bad recipient type"));
		break;		
	case MAPI_E_INVALID_RECIPS:
		strMessage = (_T("Error: Invalid recipients"));;
		break;		
	case MAPI_E_TEXT_TOO_LARGE:
		strMessage = (_T("Error: Text too large"));
		break;
	case MAPI_E_TOO_MANY_FILES:
		strMessage = (_T("Error: Too many files"));
		break;
	case MAPI_E_TOO_MANY_RECIPIENTS:
		strMessage = (_T("Error: Too many recipients"));
		break;
	case MAPI_E_INVALID_SESSION:
		strMessage = (_T("Error: Invalid session"));
		break;
/*	case ERROR_MAPI_NOT_INSTALLED:
		strMessage = (_T("Error: Mapi not installed"));
		break;*/		
	default:
		strMessage = (_T("Error: Unknown Error"));
	}	

	return strMessage;
}


STDMETHODIMP CMail::put_strSubject(BSTR sSubject)
{
	try
	{
		m_strSubject = sSubject;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::put_strProfileName(BSTR sProfileName)
{
	try
	{
		m_strProfileName = sProfileName;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::Logon()
{
	try
	{
		ULONG unlResult;
		FLAGS	flFlags = 0L;
		ULONG	ulReserved = 0L;

		LPSTR	lpszPassword = NULL;

		if ( !m_lhSession )	  // Always ask if there is an active session
		{
			flFlags = MAPI_NEW_SESSION;//|MAPI_LOGON_UI;  // Logon with a new session and force display of UI.

			unlResult = m_MAPILogon (
				0L, 				// Handle to parent window or 0.
				(LPTSTR)m_strProfileName.Copy(),	// Default profile name to use for MAPI session.
				lpszPassword,		// User password for MAPI session.
				flFlags,			// Various session settings
				ulReserved,		// Reserved.  Must be 0L.
				&m_lhSession		// Return handle to MAPI Session
				);

			if(unlResult != SUCCESS_SUCCESS) throw unlResult;
		}

		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(ULONG nlExcep)
	{	
		_bstr_t oErrorMsg;
		oErrorMsg = GetMessageFromStringTable(nlExcep);
		AfxMessageBox( (char*)oErrorMsg );
		return E_FAIL;
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::put_strEmailAddress(BSTR sEmailAddress)
{
	try
	{
		m_strEmailAdress = sEmailAddress;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::Send()
{
	try
	{
		ULONG unlResult=1; 
		MapiMessage oMapiMessage;
		MapiFileDesc oAttachment;

		ZeroMemory ( &oMapiMessage, sizeof ( MapiMessage ) );
		ZeroMemory ( &oAttachment, sizeof ( MapiFileDesc ) );

		lpMapiRecipDesc pRecips = NULL;
		MapiRecipDesc arMailRecipients[1], *tempRecip[1];     

		unlResult = Logon();

		if(unlResult != SUCCESS_SUCCESS) throw unlResult;

		CComBSTR bstrAddress;
		bstrAddress = "SMTP:";
		bstrAddress += m_strEmailAdress;

		_bstr_t bstrRec, bstrAdd, bstrAttFile, bstrAttPath, bstrSubj, bstrMesg;

		arMailRecipients[0].ulReserved   = 0;
		arMailRecipients[0].ulRecipClass = MAPI_TO;

		// recipient name
		bstrRec.Assign(m_strRecipient.Copy());
		arMailRecipients[0].lpszName	  = (char*)bstrRec;

		// email address
		bstrAdd.Assign(bstrAddress.Copy());
		arMailRecipients[0].lpszAddress  = (char*)bstrAdd;

		arMailRecipients[0].ulEIDSize    = 0;
		arMailRecipients[0].lpEntryID    = NULL;

		CComBSTR bstrFullPath = m_strAttachmentFilePath;
		bstrFullPath += m_strAttachmentFile;

		bstrAttFile.Assign(m_strAttachmentFile.Copy());
		oAttachment.lpszFileName = (char *)bstrAttFile;

		bstrAttPath.Assign(bstrFullPath.Copy());
		oAttachment.lpszPathName = (char *)bstrAttPath;

		oMapiMessage.nRecipCount	= 1;		// Must be set to the correct number of recipients.
		oMapiMessage.lpRecips		= arMailRecipients;	// Address of list of names returned from MAPIAddress.		
		oMapiMessage.ulReserved		= 0L;

		bstrSubj.Assign(m_strSubject.Copy());
		oMapiMessage.lpszSubject	= (char *)bstrSubj;

		bstrMesg.Assign(m_strMessage.Copy());
		oMapiMessage.lpszNoteText	= (char *)bstrMesg;

		oMapiMessage.lpOriginator	= NULL;	
		if(bstrFullPath.Length() > 0)
		{
			oMapiMessage.nFileCount		= 1;
			oMapiMessage.lpFiles		= &oAttachment;
		}
		else
		{
			oMapiMessage.nFileCount		= 0L;
			oMapiMessage.lpFiles		= NULL;
		}

		unlResult = m_MAPISendMail (	m_lhSession,	// Global session handle.
			0L,				// Parent window.  Set to 0 since console app.
			&oMapiMessage,		// Address of Message structure
			0L,		
			0L		// Reserved.  Must be 0L.
			);	

		if(unlResult != SUCCESS_SUCCESS) throw unlResult;

		m_MAPIFreeBuffer(tempRecip);  // release the recipient descriptors
		unlResult =Logoff();
		if(unlResult != SUCCESS_SUCCESS) throw unlResult;

		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(ULONG nlExcep)
	{
		_bstr_t oErrorMsg;
		oErrorMsg = GetMessageFromStringTable(nlExcep);
		AfxMessageBox( (char*)oErrorMsg );
		return E_FAIL;
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::Logoff()
{
	try
	{
		ULONG	unlResult;
		FLAGS	flFlags = 0L;
		ULONG	ulReserved = 0L;

		// Always check to make sure there is an active session
		if ( m_lhSession )	 
		{
			//  If there is a valid session handle, attempt to logoff.

			unlResult = m_MAPILogoff (
				m_lhSession,	// Global session handle.
				0L,				// Parent window.  Set to 0 since console app.
				flFlags,		// Ignored by MAPILogoff.
				ulReserved		// Reserved.  Must be 0L.								
				);				
			if(unlResult != SUCCESS_SUCCESS) throw(unlResult);
		}
		m_lhSession = NULL;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(ULONG nlExcep)
	{
		_bstr_t oErrorMsg;
		oErrorMsg = GetMessageFromStringTable(nlExcep);
		AfxMessageBox( (char*)oErrorMsg );
		return E_FAIL;
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::put_strRecipient(BSTR sRecipientEmail)
{
	try
	{
		m_strRecipient = sRecipientEmail;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::put_strMessage(BSTR sMessageText)
{
	try
	{
		m_strMessage = sMessageText;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::get_strMessage(BSTR *sMessageText)
{
	try
	{
		*sMessageText = m_strMessage.Copy();
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::put_strAttachmentFile(BSTR sAttachmentFileName)
{
	try
	{
		m_strAttachmentFile = sAttachmentFileName;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::get_strAttachmentFile(BSTR *sAttachmentFileName)
{
	try
	{
		*sAttachmentFileName = m_strAttachmentFile.Copy();
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}

	return S_OK;
}

STDMETHODIMP CMail::put_strAttachmentFilePath(BSTR sAttachmentFilePath)
{
	try
	{
		m_strAttachmentFilePath = sAttachmentFilePath;
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}

STDMETHODIMP CMail::get_strAttachmentFilePath(BSTR *sAttachmentFilePath)
{
	try
	{
		*sAttachmentFilePath = m_strAttachmentFilePath.Copy();
		return S_OK;
	}
	catch(_com_error oComErr)
	{
		return oComErr.Error();
	}
	catch(...)
	{
		return E_FAIL;
	}
}


#include "StdAfx.h"
#include <math.h>
#include "segments.h"
#include "message.h"
#include "..\Common\DefFun.h"

namespace SFINX {

#ifndef M_PI
#define M_PI		3.14159265358979324
#endif
#ifndef M_PI_2
#define M_PI_2		1.57079632679489662
#endif

#define	absdiff(a, b)	(((b) > (a)) ? ((b) - (a)) : ((a) - (b)))
#define	signdiff(a, b)	(((b) > (a)) ? (-1) : (1))
#define	sign(x)		((x < 0) ? (-1) : (1))
#define	isvertical(s)	(absdiff(s->a[0], M_PI_2) < M_PI / 4)

/* global */
static double	glb_sin_slant = 0.;
static double	glb_cos_slant = 0.;
static PIXEL	seg_noise = 8;
static PIXEL	seg_ok = 15;
	
double sqr( double a )
{
	return(a * a);
}

/* city block distance */

int cbdist(COORD x0, COORD y0, COORD x1, COORD y1)
{
	return(absdiff(x1, x0) + absdiff(y1, y0));
}

/* lsaxis() calculates least-squares-axis for part or whole of a segment
from beginp to endp. Also calculates rms, which can be used as a neasure for
striaghtness values are written to the `a' and `rms' arrays of the segment,
indexed by `index'. */

void lsaxis(segment *segm, int index, int beginp, int endp) 
{
	double	sx2 = 0.0, sy2 = 0.0, sxy = 0.0, 
		xav = 0.0, yav = 0.0, ssq = 0.0,
		sina, cosa, x, y;
	int	i, n = endp - beginp;
	
	for (i = beginp; i < endp; i++) {
		xav += segm->x[i];
		yav += segm->y[i];
	}
	xav /= n;
	yav /= n;
	for (i = beginp; i < endp; i++) {
		x = (double)segm->x[i] - xav;
		y = (double)segm->y[i] - yav;
		sxy += x * y;
		sx2 += x * x;
		sy2 += y * y;
	}
	
/* special case, no xy error */

	if (sxy == 0.0) {
		if (sy2 > sx2) segm->a[index] = M_PI_2;
		else segm->a[index] = 0.0;
	}

/* normal case, calc Yakimoffs ls axis and take arc tangent */

	else {
		y = -sx2 + sy2 + sqrt(sqr(sx2 - sy2) + sqr(2 * sxy));
		x = 2 * sxy;
		segm->a[index] = atan2(y, x);
	}
	
/* and sum the deviations from the tangent for all points to get rms */

	sina = sin(-segm->a[index]);
	cosa = cos(-segm->a[index]);
	for (i = beginp; i < endp; i++) {
		x = (double)segm->x[i] - xav;
		y = (double)segm->y[i] - yav;
		y = x * sina + y * cosa;
		ssq += y * y;
	}
	segm->rms[index] = sqrt(ssq / n);
}

/* estimate slant globally by considering all more or less upright segments
and taking the average slant */

double estim_slant(segment *segm, int ns)
{
	int		i, n = 0;
	double		slant = 0.0;
	segment	*s;
	
	for (i = 0, s = segm; i < ns; i++, s++) {
		if (s->n < 4) continue;
		lsaxis(s, 0, 0, s->n);	/* s->a[0] now has segment's slant */
		if (absdiff(s->a[0], M_PI_2) < M_PI / 4) {
			slant += s->a[0];
			n++;
		}
	}
	if (n) {
		slant /= n;
		slant -= M_PI_2;
	} else slant = 0.0;

	CString szMsg;
	szMsg.Format( "Estimated slant %0.2f rad = %0.0f deg.", slant, slant * 180.0 / M_PI );
	::OutputMessage( szMsg, LOG_PROC );
//	sprintf(last_msg, "Estimated slant %0.2f rad = %0.0f deg.", slant, slant * 180.0 / M_PI);
//	handle_msg(INFORM);

	return(slant);
}

/* cmpsegm_xyslant() to be called by qsort(); returns slant-corrected
segment order and makes noise segments go to the end of the array */

int cmpsegm_xyslant(segment *s1, segment *s2)
{
	double	x1, y1, x2, y2, d;
	int	i1, i2;
	
	i1 = SEG_NOISE(s1);
	i2 = SEG_NOISE(s2);
	if (i1 || i2) return(i1 - i2);
	x1 = (*(s1->x) + *(s1->xend)) / 2;
	y1 = (*(s1->y) + *(s1->yend)) / 2;
	x2 = (*(s2->x) + *(s2->xend)) / 2;
	y2 = (*(s2->y) + *(s2->yend)) / 2;
	d = glb_cos_slant * x1 + glb_sin_slant * y1 - (glb_cos_slant * x2 + glb_sin_slant * y2);
	if (d > 0) return(1); else return(-1);
}

/* cmpsegm_clean() to be called by qsort(); makes noise segments go to the
end of the array */

static int cmpsegm_clean(segment *s1, segment *s2)
{
	return(SEG_NOISE(s1) - SEG_NOISE(s2));
}

/* suppress_horiz_lines() should be called after estim_slant! */
/* this function subject to improvement */

//#pragma argsused /* function happens not to use img arg, but may later */
static void suppress_horiz_lines(image *img, segment *segm, int ns)
{
	int		nr = 0;
	segment	*si;
	int		i;
	
	for (i = 0, si = segm; i < ns; i++, si++) {
		if ((si->rms[0] < 2.0) && ((si->a[0] < 0.1) || (M_PI - si->a[0] < 0.1))) {
			SET_NOISE(si);
			nr++;
		}
	}

	CString szMsg;
	szMsg.Format( "%d segments identified as background noise and removed", nr );
	::OutputMessage( szMsg, LOG_PROC );
//	sprintf(last_msg, "%d segments identified as background noise and removed", nr);
//	handle_msg(INFORM);
}

static double looparea(segment *s)
{
	double	a = 0;
	int	i;
	COORD	*x = s->x, *y = s->y;
	
	for (i = 0; i < s->n - 1; i++) {
		a += (-(x[i + 1] - x[i]) * (y[i] - y[0]) +
		(y[i + 1] - y[i]) * (x[i] - x[0]));
	}
	return(0.5 * a);
}

static
void getextensions(segment *s, int *xext, int *yext)
{
	COORD	*x = s->x, *y = s->y;
	COORD	minx = 10000, maxx = 0, miny = 10000, maxy = 0, xs;
	int	i;
	
	for (i = 0; i < s->n; i++, x++, y++) {
		xs = (COORD)(glb_cos_slant * *x + glb_sin_slant * *y);
		if (xs < minx) minx = xs;
		if (xs > maxx) maxx = xs;
		if (*y < miny) miny = *y;
		if (*y > maxy) maxy = *y;
	}
	*xext = maxx - minx;
	*yext = maxy - miny;
}

static double av_linewidth(segment *s)
{
	int	i;
	PIXEL	*z;
	double	w = 0;
	
	for (i = 0, z = s->z; i < s->n; i++, z++) {
		w += *z;
	}
	return(2 * w / s->n);
}

int filtersegments(image *img, segment *segm, int ns, int minl)
{
	int		i;
	double	slant;
	segment	*si;

/* estimate global slant */

	slant = estim_slant(segm, ns);
	glb_sin_slant = sin(slant);
	glb_cos_slant = cos(slant);

/* suppress horizontal lines from writing paper */

	suppress_horiz_lines(img, segm, ns);

/* remove segments that are too short */
	
	for (i = 0, si = segm; i < ns; i++, si++)
	{
		if (si->n < minl) SET_NOISE(si);
	}

/* sort according to slant-corrected x, also moving noise segments to end of
array */

//	qsort(segm, ns, sizeof(segment), cmpsegm_xyslant);
	qsort(segm, ns, sizeof(segment), (int (__cdecl *)(const void *,const void *))cmpsegm_xyslant);

//	for (i = 0, si = segm; (i < ns) && !SEG_NOISE(si); i++, si++);
	i = ns;

	return i;	/* the number of non-noise segments */
}

int writefeatures(segment *segm, int ns, FILE *outf, char *out_echo, char *missing,
				  int resolution, BOOL fExists, int nTrialNum)
{
	int		i = 0, nSeg = 0;
	segment	*si = NULL;
	double	pix2mm = 25.4 / ( ( resolution == 0. ) ? 1.0 : resolution );
	double	rad2deg = 180.0 / M_PI;

	// headers
	if( !fExists )
		fprintf(outf, "Trial Segment AverageThickness HorizontalStart VerticalStart HorizonalSize VerticalSize TraceLength Slant StraightnessRelError LoopArea Archedness GarlandRelSize ArcadeRelSize ArchedFrequency GarlandFrequency ArcadeFrequency\n");

	for( i = 0, si = segm; i < ns; i++, si++ )
	{
		if( SEG_NOISE( si ) ) continue;

		nSeg++;
		char	v = isvertical(si);
		char	loop = (cbdist(*(si->x), *(si->y), *(si->xend), *(si->yend)) < 9);
		double	la = looparea(si), duct = 0., slant = 0., dloop = 0., gar = 0., arc = 0.;
		int		extx = 0, exty = 0, hstart = 0, vstart = 0, archfreq = 0, garfreq = 0, arcfreq = 0;
		
		getextensions(si, &extx, &exty);

		// slant
		if( v ) slant = rad2deg * si->a[0] - 90;
		// ductus
		if( !loop && !v )
		{
			if( la == 0. ) duct = 0.;
			else duct = pix2mm * sqrt(abs(la)) * sign(la) * signdiff(*(si->xend), *(si->x));
		}
		// loop
		if( loop ) dloop = pix2mm * sqrt(abs(la));
		// garland/arcade
		if( !loop && !v )
		{
			if( duct > 0 ) 
			{
				gar = duct;
				garfreq = 1;
			}
			if( duct < 0 )
			{
				arc = duct;
				arcfreq = 1;
			}
		}
		// horizontal start
		hstart = si->x[ 0 ];
		// vertical start
		vstart = si->y[ 0 ];
		// arched frequency
		if( duct == 0. ) archfreq = 0;
		else archfreq = 1;
		
		fprintf( outf, "%d %d %0.3g %d %d %0.3g %0.3g %0.3g %0.3g %0.3g %0.3g %0.3g %0.3g %0.3g %d %d %d\n", nTrialNum,
				 nSeg, pix2mm * av_linewidth( si ), hstart, vstart, pix2mm * exty,
				 pix2mm * extx, pix2mm * si->n, slant, pix2mm * si->rms[ 0 ],
				 dloop, duct, gar, arc, archfreq, garfreq, arcfreq );
	}
	return(ns);
}

}

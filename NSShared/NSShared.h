// NSShared.h : main header file for the NSShared DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNSSharedApp
// See NSShared.cpp for the implementation of this class
//

class CNSSharedApp : public CWinApp
{
public:
	CNSSharedApp();

// Overrides
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

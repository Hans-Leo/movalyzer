#ifndef __MAIL_H_
#define __MAIL_H_

#include <mapi.h>

/////////////////////////////////////////////////////////////////////////////
// CMail
#define MAPI_INSTALLED 0
class AFX_EXT_API CMail
{
protected:
	CComBSTR m_strSubject;
	CComBSTR m_strMessage;
	CComBSTR m_strAttachmentFilePath;
	CComBSTR m_strAttachmentFile;
	CComBSTR m_strRecipient;	
	CComBSTR m_strProfileName;
	CComBSTR m_strEmailAdress;

	
	// session used to send emails
	LHANDLE m_lhSession;
	
	// MAPI function pointers
	LPMAPIADDRESS m_MAPIAddress;
	LPMAPIDETAILS m_MAPIDetails;		
	LPMAPIFINDNEXT m_MAPIFindNext;		
	LPMAPIFREEBUFFER m_MAPIFreeBuffer;
	LPMAPILOGOFF m_MAPILogoff;		
	LPMAPILOGON m_MAPILogon;			
	LPMAPIREADMAIL m_MAPIReadMail;		
	LPMAPIRESOLVENAME m_MAPIResolveName;	
	LPMAPISENDDOCUMENTS m_MAPISendDocuments;	
	LPMAPISENDMAIL m_MAPISendMail;
	LPMAPISAVEMAIL m_MAPISaveMail;
	
	// to display error
	CComBSTR GetMessageFromStringTable( UINT unMessageID );

public:
	// constructor initializes using InitMapi();
	CMail();
	~CMail();
	// checks if MAPI is installed on the system
	// if you have outlook express installed, MAPI will be there
	ULONG IsMapiInstalled();
	
	// Initialize function pointers
	void InitMapi();

// IMail
public:
	// this is the email or set of email addresses (separated by ;)
	// which is actually used to send email	
	STDMETHOD(put_strEmailAddress)(/*[in]*/ BSTR sEmailAddress);
	
	// profile name is compulsory, this is the outlook profile, 
	// i used "outlook express" as configuring it is easier than "MS outlook"
	// make sure to specify the correct email sender's address for this profile
	// and make sure that outlook express or outlook is the default email client.	
	STDMETHOD(put_strProfileName)(/*[in]*/ BSTR sProfileName);
	
	STDMETHOD(get_strSubject)(/*[out,retval]*/BSTR *pVal);
	STDMETHOD(put_strSubject)(/*[in] */ BSTR sSubject);

	// used for file path as "C:\FilePath"
	STDMETHOD(get_strAttachmentFilePath)(/*[out,retval]*/BSTR* sAttachmentFilePath);
	STDMETHOD(put_strAttachmentFilePath)(/*[in]*/BSTR sAttachmentFilePath);

	// used for file such as "abc.txt" in the folder "C:\FilePath"
	STDMETHOD(get_strAttachmentFile)(/*[out,retval]*/BSTR* sAttachmentFileName);
	STDMETHOD(put_strAttachmentFile)(/*[in]*/BSTR sAttachmentFileName);
	
	// message text
	STDMETHOD(get_strMessage)(/*[out,retval]*/BSTR* sMessageText);
	STDMETHOD(put_strMessage)(/*[in]*/BSTR sMessageText);
	
	// display name of recipient
	STDMETHOD(put_strRecipient)(/*[in]*/ BSTR sRecipientEmail);
	
	STDMETHOD(Logon)();
	STDMETHOD(Logoff)();
	//send email, internally calls logon and logoff
	STDMETHOD(Send)();

};

#endif //__MAIL_H_

/*    pcx.c    	                                   */
/*                                                                    */
/*    reading / writing routines for PC Paintbrush (PCX) image        */
/*    files.                                                          */
/*                                                                    */
/*    Eric Helsper March '94, adapted from Ken Chu.                   */
/*    Modifications for byte order detection Eric Helsper March '95    */
/*                                                                    */
/*    needs img.c	                                           */

#include "StdAfx.h"
#include "img.h"
#include "message.h"
#include "pcx.h"
#include "..\Common\DefFun.h"

/*
 * defines for the PCX header and magic numbers
 */

namespace SFINX {

typedef unsigned char	byte;

#define PCX_LAST_VER	5    /* last acceptable version number */
#define PCX_RLL	    1    /* PCX RLL encoding method */
#define PCX_MAGIC	0x0a    /* first byte of a PCX image */
#define PCX_256COLORS_MAGIC    0x0c    /* first byte of a PCX 256 color map */
#define    MAX_DEPTH	8    /* PCX supports up to 8 bits per pixel */
#define PCX_MAXCOLORS	256    /* maximum number of colors in a PCX file */

/* prototypes */

int write_PCX(image *,char *);
int read_PCX_hdr(FILE *,struct pcxhdr *);
image *read_PCX(char *);
int pcx_read_line(FILE *,byte *,struct pcxhdr *);
int pcx_planes_to_pixels(byte *,byte *,struct pcxhdr *);
static PIXEL *packbits_0(PIXEL *, PIXEL *, unsigned short int );
static PIXEL *packbits_8_1(PIXEL *, PIXEL *, unsigned short int );
static PIXEL *packbits_4_1(PIXEL *, PIXEL *, unsigned short int );
static PIXEL *packbits_8_1_threshold(PIXEL *, PIXEL *, unsigned short int );
static PIXEL *packbits_4_1_threshold(PIXEL *, PIXEL *, unsigned short int );
static PIXEL *packbits_8_4(PIXEL *, PIXEL *, unsigned short int );
static PIXEL *packbits_8_4_threshold(PIXEL *, PIXEL *, unsigned short int );

/* byteorder() returns byte order of the current implementation: 0 for
most-significant-least-significant; 1 for least-significant-most-significant.
The latter format is used in the pc, and therefore also in the pcx format.
*/
#define    LSMS    1
#define    MSLS    0

static
int byteorder() {
    union {
	unsigned short int    i;
	unsigned char	a[2];
    } word;
    
    word.i = 1;
    return(word.a[0]);
}

static void flip_bytes(byte *p)
{
    register byte temp = *p;
    *p = p[1];
    p[1] = temp;
}

/************************/
/*    Write PCX file    */
/************************/

int write_PCX(image *img, char *filename)
{
    struct pcxhdr	    pcxhd;
    FILE		*f;
    unsigned short int	n = 0, y;
    unsigned short int	bytesperline;
    PIXEL		*bp, *fp, *endp, *buf = NULL;
    PIXEL		*(*packbits)(PIXEL*, PIXEL*, int);

	CString szMsg;
	szMsg.Format( "Writing '%s' (%d * %d), %d colors", filename, img->w, img->h, 1 << img->clrbits );
	::OutputMessage( szMsg, LOG_PROC );
//    sprintf(last_msg, "Writing '%s' (%d * %d), %d colors",
//	filename, img->w, img->h, 1 << img->clrbits);
 //   handle_msg(INFORM);

    if ((img->clrbits != 1) && (img->clrbits != 4) && (img->clrbits != 8)) {
//	error2(FATAL | INTERNAL, "Can only write PCX with 1, 4 or 8 bits per pixel", filename);
    }
    if (img->clrbits > img->bipp) {
//	error2(FATAL | INTERNAL, "clrbits > bitsperpixel", filename);
    }
    bytesperline = (USHORT)standard_bytesperline(img->w, img->clrbits);
    if (img->clrbits == img->bipp) packbits = (PIXEL *(__cdecl *)(SFINX::PIXEL *,SFINX::PIXEL *,int))packbits_0;
    else {
	if ((img->bipp == 8) && (img->clrbits == 4)) packbits = (PIXEL *(__cdecl *)(SFINX::PIXEL *,SFINX::PIXEL *,int))packbits_8_4_threshold;
	if ((img->bipp == 8) && (img->clrbits == 1)) packbits = (PIXEL *(__cdecl *)(SFINX::PIXEL *,SFINX::PIXEL *,int))packbits_8_1_threshold;
	if ((img->bipp == 4) && (img->clrbits == 1)) packbits = (PIXEL *(__cdecl *)(SFINX::PIXEL *,SFINX::PIXEL *,int))packbits_4_1_threshold;
	buf = (PIXEL*)myalloc(bytesperline);
    }
    
    f = fopen(filename, "wb");
    if (f == NULL) {
//	error2(MAJOR, "Cannot open for write", filename);
	return(MAJOR);
    }
    pcxhd.signature = PCX_MAGIC;
    pcxhd.version = PCX_LAST_VER;
    pcxhd.encoding = PCX_RLL;
    pcxhd.bitsperpix = img->clrbits;
    pcxhd.Xleft = 0;
    pcxhd.Ytop = 0;
    pcxhd.Xright = (USHORT)(img->w - 1);
    pcxhd.Ybottom = (USHORT)(img->h - 1);
    pcxhd.Xresolution = (USHORT)img->dpi;
    pcxhd.Yresolution = (USHORT)img->dpi;
    pcxhd.reserved1 = 0;
    pcxhd.planes = 1;
    pcxhd.linesize = bytesperline;
    if (img->clrtable != NULL) {
	pcxhd.paletteinfo = 1;
	memmove(pcxhd.PCXpalette, img->clrtable, 3*16);
    }
    else pcxhd.paletteinfo = 0;
    memset(pcxhd.reserved2, 0, sizeof(pcxhd.reserved2));

    if (byteorder() == MSLS) {
	flip_bytes((byte *)&pcxhd.Xleft);
	flip_bytes((byte *)&pcxhd.Ytop);
	flip_bytes((byte *)&pcxhd.Xright);
	flip_bytes((byte *)&pcxhd.Ybottom);
	flip_bytes((byte *)&pcxhd.Xresolution);
	flip_bytes((byte *)&pcxhd.Yresolution);
	flip_bytes((byte *)&pcxhd.linesize);
	flip_bytes((byte *)&pcxhd.paletteinfo);
    }

    if (fwrite(&pcxhd, sizeof(struct pcxhdr), 1, f) != 1) {
//	error2(MAJOR, "Writing pcx header failed", filename);
	fclose(f);
	return(MAJOR);
    }
    for (y = 0; y < img->h; y++) {
	bp = packbits(buf, img->data[y], bytesperline);
	endp = &(bp[bytesperline]);
	n = 0;
	while (bp < endp) {
	    for (n = 0, fp = bp; (n < 63) && (fp < endp); fp++, n++) {
		if (*fp != *bp) break;
	    }
	    if ((n > 2) || ((*bp & 0xc0) == 0xc0)) {
		putc(n | 0xc0, f);
		putc(*bp, f);
		n = 0;
	    }
	    while (n) {
		putc(*bp, f);
		n--;
	    }
	    bp = fp;
	}
    }
    if (buf != NULL) free(buf);
    if ((img->clrbits == 8) && (img->clrtable != NULL)) {
	putc(PCX_256COLORS_MAGIC, f);
	fwrite(img->clrtable, 256 * 3, 1, f);
    }
    fclose(f);
    return(0);
}

/************************/
/*    Read PCX file    */
/************************/

int read_PCX_hdr(FILE *f, struct pcxhdr *pcxhd)
{

/* Read pcx header & do some format verifications */

    if (fread(pcxhd, sizeof(struct pcxhdr), 1, f) != 1) {
	return(2);
    }

    if ((pcxhd->signature != PCX_MAGIC) || (pcxhd->version > PCX_LAST_VER)) {
	return(2);
    }

    if (pcxhd->encoding != PCX_RLL) {
	return(3);
    }

    if (pcxhd->planes * pcxhd->bitsperpix > MAX_DEPTH) {
	return(4);
    }
    if (byteorder() == MSLS) {
	flip_bytes((byte *)&(pcxhd->Xleft));
	flip_bytes((byte *)&(pcxhd->Ytop));
	flip_bytes((byte *)&(pcxhd->Xright));
	flip_bytes((byte *)&(pcxhd->Ybottom));
	flip_bytes((byte *)&(pcxhd->linesize));
    }
    return(0);
}

image *read_PCX(char *filename)
{
	CString szMsg;
    char    *errors[] = {
	"",
	"incomplete PCX header",
	"not a PCX file",
	"not a run-length encoded PCX file",
	"PCX image depth more than 8 bits",
	"unexpected end of file",
	"more than 4 PCX planes",
	"more than 1 bit per pixel multiplane"
    };
    pcxhdr	pcxhd;
    image	*img;
    FILE	*f;
    int	    rv;
    PIXEL	*tmp;
    register int	i;

    f = fopen(filename, "rb");
    if( f == NULL )
	{
		szMsg.Format( "SFINX: ERROR Unable to open file '%s'.", filename );
		::OutputMessage( szMsg, LOG_PROC );
		return NULL;
	}

    rv = read_PCX_hdr(f, &pcxhd);
    if( rv )
	{
		if( ( rv >= 0 ) && ( rv <= 7 ) )
			szMsg.Format( _T("SFINX: ERROR: %s"), errors[ rv ] );
		else szMsg = _T("SFINX: ERROR: Unspecified error when reading image header.");
		::OutputMessage( szMsg, LOG_PROC );
		return NULL;
	}

// 13Feb09: GMB: Added this since conversions from our CxImage library
// sets the PCX Pallete Info to 0 or 1 depending upon the # of colors used
// which does not correlate with this code
	if( pcxhd.paletteinfo == 0 ) pcxhd.paletteinfo = 2;

/* create an img variable */
    
    img = alloc_img( pcxhd.Xright - pcxhd.Xleft + 1,
					 pcxhd.Ybottom - pcxhd.Ytop + 1,
					 pcxhd.bitsperpix * pcxhd.planes );
	if( !img )
	{
		szMsg = _T( "SFINX: ERROR: Unable to allocate memory for image." );
		::OutputMessage( szMsg, LOG_PROC );
		return NULL;
	}
/*    img = myalloc(sizeof(image));
    img->h = pcxhd.Ybottom - pcxhd.Ytop + 1;
    img->w = pcxhd.Xright - pcxhd.Xleft + 1;
    img->bipp = pcxhd.bitsperpix;
    img->clrbits = img->bipp * pcxhd.planes;
    img->bypl = pcxhd.linesize * pcxhd.planes;
    img->dpi = pcxhd.Xresolution;
    img->data = myalloc(img->h * sizeof(PIXEL *));
    for (i = 0; i < img->h; i++) {
	img->data[i] = myalloc((8 + img->bypl) * sizeof(PIXEL));
    }
*/    
    img->dpi = pcxhd.Xresolution;

/* copy clrtable from header, if appropriate */

    if (((img->clrbits == 2) || (img->clrbits == 4)) && (pcxhd.paletteinfo))
	{
		img->clrtable = (colour*)myalloc(3 * (1 << img->clrbits));
		memmove( img->clrtable, pcxhd.PCXpalette, 3 * (1 << img->clrbits) );
    }

	szMsg.Format( "Reading '%s' (%u * %u), %u colors", filename, img->w, img->h, 1 << img->clrbits );
	::OutputMessage( szMsg, LOG_PROC );
    
/* Read compressed bitmap */

    if (pcxhd.planes > 1)
	{
		tmp = (PIXEL*)myalloc(pcxhd.planes * pcxhd.linesize);
		for (i = 0; i < img->h; i++)
		{
		    if (rv = pcx_read_line(f, tmp, &pcxhd))
			{
				delete [] tmp;
				delete img;
				return NULL;
			}
		    if (rv = pcx_planes_to_pixels(img->data[i], tmp, &pcxhd))
			{
				delete [] tmp;
				delete img;
				return NULL;
			}
	    }
		free(tmp);
		img->bipp = 4;
    }
    else
	{
		for (i = 0; i < img->h; i++)
		{
			if (rv = pcx_read_line(f, img->data[i], &pcxhd))
			{
				delete img;
				return NULL;
			}
		}
    }

/* Read colormap if 8 bits per pixel */

    if ((img->clrbits == 8) && (pcxhd.paletteinfo != 0) && (getc(f) == PCX_256COLORS_MAGIC))
	{
		img->clrtable = (colour*)myalloc(3 * 256);
		rv = fread(img->clrtable, 3 * 256, 1, f);
		if (rv != 1)
		{
			if (f != stdin) fclose(f);
			delete img;
			return NULL;
		}
    }

    if (img->clrtable == NULL)
	{
		if (img->clrbits == 1) img->clrtable = standard_grey_table(1);
		else img->clrtable = standard_clr_table(img->clrbits);
    }

    fclose(f);

    return img;
}

int pcx_read_line(FILE *f, byte *line, struct pcxhdr *phd)
{
    /*
     * Goes like this: Read a byte.  If the two high bits are set,
     * then the low 6 bits contain a repeat count, and the byte to
     * repeat is the next byte in the file.  If the two high bits are
     * not set, then this is the byte.
     */

    register int    count, nbytes, c;

    nbytes    = phd->linesize * phd->planes;

    while (nbytes > 0)
    {
	c = getc(f);
	if (c == EOF) {
	    return(5);
	}
	if ((c & 0xc0) != 0xc0) {
	    *line = (byte)c;
	    line++;
	    nbytes--;
	    continue;
	}

	count = c & 0x3f;
	if (count > nbytes) {
	    /* repeat count spans end of image */
	    /* this seems to occur in may pcx images */
	    /* so we'll have to tolerate it */
	    count = nbytes;
	}

	c = getc(f);
	if (c == EOF) {
	    return(5);
	}
	nbytes -= count;
	while (--count >= 0)
	    *line++ = (byte)c;
    }
    return 0;
}

/*
 * convert multi-plane format into 1 pixel per byte
 */

int pcx_planes_to_pixels(byte* pixels, byte *bitplanes, struct pcxhdr *phd)
{
    register int	    i, npixels;
    register byte	    *p;
    register unsigned int	bytesperline = phd->linesize;

    if (phd->planes > 4) {
	return(6);
    }

    if (phd->bitsperpix != 1) {
	return(7);
    }
    /*
     * clear the pixel buffer
     */
    npixels = (bytesperline * phd->planes) / phd->bitsperpix;
    p = pixels;
    while (--npixels >= 0)
	*p++ = 0;

    /*
     * do the format conversion
     */
    for (i = 0; i < phd->planes; i++) {

	register int j, lobit, hibit, bits;

	lobit = 1 << i;
	hibit = lobit << 4;
	for (p = pixels, j = 0; j < (int)bytesperline; j++) {

	    bits = *bitplanes++;
	    if (bits & 0x80) *p |= hibit;
	    if (bits & 0x40) *p |= lobit; p++;
	    if (bits & 0x20) *p |= hibit;
	    if (bits & 0x10) *p |= lobit; p++;
	    if (bits & 0x08) *p |= hibit;
	    if (bits & 0x04) *p |= lobit; p++;
	    if (bits & 0x02) *p |= hibit;
	    if (bits & 0x01) *p |= lobit; p++;
	}
    }
    return 0;
}

/* the following packing functions used by write_PCX if number of colour
bits smaller than number of bitsperpixel */

static PIXEL *packbits_0(PIXEL *dst, PIXEL *src, unsigned short int n) {
    return(src);
}

static PIXEL *packbits_8_4(PIXEL *dst, PIXEL *src, unsigned short int n) {
    register PIXEL    *dstp = dst;
    while (n--) {
	*dstp  = (*src++ & 0x0f) << 4;
	*dstp |= (*src++ & 0x0f);
	dstp++;
    }
    return(dst);
}

static PIXEL *packbits_8_4_threshold(PIXEL *dst, PIXEL *src, unsigned short int n) {
    register PIXEL    *dstp = dst;
    while (n--) {
	*dstp  = (*src++ & 0xf0);
	*dstp |= (*src++ & 0xf0) >> 4;
	dstp++;
    }
    return(dst);
}

static PIXEL *packbits_8_1(PIXEL *dst, PIXEL *src, unsigned short int n) {
    register PIXEL    *dstp = dst;
    while (n--) {
	*dstp  = (*src++ & 0x01) << 7;
	*dstp |= (*src++ & 0x01) << 6;
	*dstp |= (*src++ & 0x01) << 5;
	*dstp |= (*src++ & 0x01) << 4;
	*dstp |= (*src++ & 0x01) << 3;
	*dstp |= (*src++ & 0x01) << 2;
	*dstp |= (*src++ & 0x01) << 1;
	*dstp |= (*src++ & 0x01);
	dstp++;
    }
    return(dst);
}

static PIXEL *packbits_8_1_threshold(PIXEL *dst, PIXEL *src, unsigned short int n) {
    register PIXEL    *dstp = dst;
    while (n--) {
	*dstp  = (*src++ & 0x80);
	*dstp |= (*src++ & 0x80) >> 1;
	*dstp |= (*src++ & 0x80) >> 2;
	*dstp |= (*src++ & 0x80) >> 3;
	*dstp |= (*src++ & 0x80) >> 4;
	*dstp |= (*src++ & 0x80) >> 5;
	*dstp |= (*src++ & 0x80) >> 6;
	*dstp |= (*src++ & 0x80) >> 7;
	dstp++;
    }
    return(dst);
}

static PIXEL *packbits_4_1(PIXEL *dst, PIXEL *src, unsigned short int n) {
    register PIXEL    *dstp = dst;
    while (n--) {
	*dstp  = (*src   & 0x10) << 3;
	*dstp |= (*src++ & 0x01) << 6;
	*dstp |= (*src   & 0x10) << 1;
	*dstp |= (*src++ & 0x01) << 4;
	*dstp |= (*src   & 0x10) >> 1;
	*dstp |= (*src++ & 0x01) << 2;
	*dstp |= (*src   & 0x10) >> 3;
	*dstp |= (*src++ & 0x01);
	dstp++;
    }
    return(dst);
}

static PIXEL *packbits_4_1_threshold(PIXEL *dst, PIXEL *src, unsigned short int n) {
    register PIXEL    *dstp = dst;
    while (n--) {
	*dstp  = (*src   & 0x80);
	*dstp |= (*src++ & 0x08) << 3;
	*dstp |= (*src   & 0x80) >> 2;
	*dstp |= (*src++ & 0x08) << 1;
	*dstp |= (*src   & 0x80) >> 4;
	*dstp |= (*src++ & 0x08) >> 1;
	*dstp |= (*src   & 0x80) >> 6;
	*dstp |= (*src++ & 0x08) >> 3;
	dstp++;
    }
    return(dst);
}

}	// namespace

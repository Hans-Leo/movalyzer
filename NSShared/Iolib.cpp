/*************************************************
   Iolib - version 3.0 - hlt - 16 Feb 1995
**************************************************
   Copyright 1995 Teulings

PURPOSES:
    (1) i/o routines

UPDATES:
  13Jul94: GETHWR added
  10Aug94: GETHWR streamlined, reading now sample 0, decimate, 2*decimate
             instead of decimate-1, 2*decimate-1, ...
  25Jan95: getlinefloat() added
  16Feb95: Line overflow error detected; Optstuff in optlib.c
   7Feb96:  GETLINE from 512 to 1024
  18Mar96: In GETHWR fgets() instead of fscanf() which does not crash in binary data
  26Jun96: GETHWR quits after discontinuity.
  10Jul96: GETHWR discontinuty detection dynamic (problems in exp wr).
  10Feb97: Discontinuity criterion weakened
  13Jun97: hwrin() buffer NCHAR=40 increased to 80. Still will read
             bytes on same line after byte 80 if any.
   8Jul97: Cutoff at (not after) discontinuity
  17Sep98: Checkdiscontinuity can be switched off in GETHWR
  21Sep98: GETHWR now averages x and y when decimating and takes the minimum of z
  25Sep98: Added stdio, stdlib, added readhwr() from timfun
  19Oct99: GMB - Coversion to C++ & DLL w/use of MFC
  12Sep03: GMB - readhwr modified to return nsin if # samples > min pen pressure is 0
  24Sep03: GMB - getlinefloat modified to retun 0 if the 1st char = character
  18Sep06: GMB: Converted deprecated string/file functions for MSVS7
  31Oct06: HLT&Yi: Implemented discontinuity correction, improved output, made all discontinuity parameters user settable
  17Nov06: GMB: Added settable discontinuity vars
  02Apr07: Yi: change the size of GETLINELEN from 1024 to 2048
  09Jul08: GMB: Removed message for end of input file for getlinefloat
				Increased max line length from 2048 to 3060
  05Dec08: GMB: Added discontinuity correction using DIS file for complete duration of discontinuity
  29Jan09: GMB: Adjustments to discontinuity settings:
				Check for max # inserted was incorrect. Determination of inserted steps should be only running average (sdx/sdy).
				Added code to ensure int. constant, rel err, & abs err are not out of range
  10Jul09: GMB: Added 'destination' array to 'putdata' function.
  27Sep09: GMB: Added "Remove Trailing Penlift" option to readhwr and gethwr

TO BE DONE:

*/

/************************************************************************/

#include "../MFC/mfc.h"
#include "../Common/DefFun.h"
#include "iolib.h"
#include <math.h>
#include <ctype.h>

#define NCHARLINE							80
//#define GETLINELEN						2048
#define GETLINELEN							3060
#define BLANK								' '
#define BLANKSTR							" "
#define TABSEP								'	'
#define CRAZYBIG							900000000.

//ADDED Oct.31.06 Yi for discontinuity correction
#define INTEGRATIONFACTOR					0.2
#define RERROR								20.0
#define AERROR								100.0

#define DISCARDAFTERDISCONTINUITY			0	// Default
#define INSERTLINEARDISCONTUITY				1
#define MOVETOWARDSDISCONTINUITY			2
#define ACCEPTDISCONTINUITY					3

#define ZINSERTEDPOINT						-1.0

// discontinuity settable vars
int nDiscontinuityCorrection = DISCARDAFTERDISCONTINUITY;
void SetDiscontinuityCorrection( int nVal ) { nDiscontinuityCorrection = nVal; }
double dIntegrationFactor = INTEGRATIONFACTOR;
void SetIntegrationFactor( double dVal )
{
	// we're going to allow the error to eliminate any constants
	// user should see if this value is out of whack (will not cause a crash if 0)
//	if( dVal < 0.001 ) dVal = 0.3;
//	else if( dVal > 1. ) dVal = 0.3;
	if( ( dVal < 0.001 ) || ( dVal > 1. ) )
	{
		CString szMsg;
		szMsg.Format( _T("TIME FUNCTION: DISCONTINUITY: [GETHWR] ERROR: Max Relative Intersample Period Error (%.2f) is out-of-range (0.001 - 1.0)."), dIntegrationFactor );
		OutputMessage( szMsg, LOG_PROC );
	}
	dIntegrationFactor = dVal;
}
double dRerror = RERROR;
void SetRelativeError( double dVal ) { if( dVal < 1. ) dVal = 1.; dRerror = dVal; }
double dAerror = AERROR;
void SetAbsoluteError( double dVal ) { if( dVal < 1. ) dVal = 1.; dAerror = dVal; }
double dZInsertedPoint = ZINSERTEDPOINT;
void SetZInsertedPoint( double dVal ) { dZInsertedPoint = dVal; }


#if _MSC_VER < 1400
#define	strcpy_s	strcpy
#define	fscanf	fscanf
#define	sscanf	sscanf
int fopen_s( FILE** fp, const char * f, const char *a )
{
	*fp = fopen( f, a );
	if( *fp != NULL ) return 0;
	else return -1;
}
char* strtok_s( char* s, char* t, char** nt )
{
	return( strtok( s, t ) );
}
#endif

#pragma warning( disable: 4996 )	// disable deprecated warning

/********************************************************************/
int gethwr( CStdioFile& fpHWR, double* x, double* y, double* z,
			int nsammax, int nsamwanted, int nsamskip, int decimate,
			double prsmin, int checkdiscontinuity, bool fRemoveTrailingPenlift )
/********************************************************************/
/* PURPOSE */
/* The basic routine to read a hwr file */
// Read line by line a sample from HWR file,
// retrieve the x, y, z coordinates
// combine multiple samples in case when reading files larger than the array sizes.
// During combining the samples, discontinuities are detected.
// Options exist to detect nad correct discontinuities
// Discontinuities need to be taken care of as they will disrupt the lawpass filtering
//
// Updated
// 30.Oct.06 by Yi: Implemented discontinuity correction, improved output, made all discontinuity parameters user settable

{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( z != NULL );

	CString szLine, szMsg;
	int isamskip = 0, nsamread = 0;
	double dx = 0.0, dy = 0.0;
	double sdx = 0.0, sdy = 0.0;
	double xin = 0.0, yin = 0.0, zin = 0.0;
	double xprev = 0.0, yprev = 0.0;
	double xsum = 0.0, ysum = 0.0, zsum = 0.0;
	bool error = FALSE, lowpress = FALSE, fInitialZero = FALSE;
	bool xerror = FALSE, yerror = FALSE;
	int nZeroSamples = 0;

	// ADDED 30.Oct.06 by Yi for discontinuity correction
	int nInsertedSteps = 0;
	int nInsertedStepsTotal = 0;
	double dXLinearStep=0;
	double dYLinearStep=0;
	double xMoveDiscontinuity=0;
	double yMoveDiscontinuity=0;
	// ADDED 08Dec08 by GMB for discontinuity correction using DIS file
	CString	szDis, szDisName;
	CStdioFile fpDis;
	CFileFind ff;
	struct DisInfo
	{
	public:
		DisInfo() : nSample( 0 ), dDur( 0. ), fCompleted( FALSE ) {}
		long	nSample;
		double	dDur;
		bool	fCompleted;
		static bool Find( DisInfo* pItems, int nCount, long nCurSample, double& dDuration )
		{
			if( !pItems || ( nCount <= 0 ) ) return FALSE;
			for( int i = 0; i < nCount; i++ )
			{
				if( !( pItems[ i ].fCompleted ) )
				{
					if( nCurSample == pItems[ i ].nSample )
					{
						pItems[ i ].fCompleted = TRUE;
						dDuration = pItems[ i ].dDur;
						return TRUE;
					}
				}
			}
			return FALSE;
		}
	};
	DisInfo* pDisInfo = NULL;
	int nDisCount = 0;
	double dDuration = 0.;
	double dSampRate = ::GetSamplingRate();
	if( dSampRate <= 0. )
	{
		OutputMessage( "TIME FUNCTION: [GETHWR] ERROR: Sampling Rate<=0.", LOG_PROC );
		return 0;
	}

	// verify pointers & file streams
	if( !x || !y || !z || !fpHWR.m_hFile || !fpHWR.m_pStream ) return 0;

	if( nsamwanted > nsammax )
	{
		//IDS_GETHWR_1
		szMsg.Format("TIME FUNCTION: DISCONTINUITY: [GETHWR] ERROR: #samples wanted=% > #samples max=%d." , nsamwanted, nsammax );
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	if( decimate < 1 )
	{
		//IDS_GETHWR_2
		szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] ERROR: decimate=%d < 1.", decimate );
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	// DISCONTINUITY FILE
	// construct name from HWR
	szDis = fpHWR.GetFilePath();
	szDis = szDis.Left( szDis.GetLength() - 3 );
	szDis += _T("DIS");
	int nPos = szDis.ReverseFind( '\\' );
	if( nPos != -1 ) szDisName = szDis.Mid( nPos + 1 );
	else szDisName = szDis;
	// locate file (ignore if returned sampling rate <= 0)
	if( ff.FindFile( szDis ) )
	{
		// if we're inserting constant velocity lines for entire duration...
		if( nDiscontinuityCorrection == INSERTLINEARDISCONTUITY )
		{
			// open DIS file and read # of entries, then re-read and store
			if( !fpDis.Open( szDis, CFile::modeRead ) )
			{
				OutputMessage( _T("Unable to open discontinuity file."), LOG_PROC );
				return 0;
			}

			// get # of lines
			while( fpDis.ReadString( szLine ) ) nDisCount++;

			// create DisInfo structure to house data
			if( nDisCount > 0 ) pDisInfo = new DisInfo[ nDisCount ];

			// loop through again to obtain the data
			fpDis.SeekToBegin();
			int nCur = 0;
			while( pDisInfo && fpDis.ReadString( szLine ) && ( nCur < nDisCount ) )
			{
				if( szLine != _T("") )
				{
					// each value (sample & duration) separated by a space
					int nPos = szLine.Find( _T(" ") );
					if( nPos != -1 )
					{
						pDisInfo[ nCur ].nSample = atoi( szLine.Left( nPos ) );
						pDisInfo[ nCur ].dDur = atof( szLine.Mid( nPos + 1 ) );
						nCur++;
					}
				}
			}

			// close file
			fpDis.Close();
		}
	}

	// Initialize variables that integrate all changes during the trial
	// Change time
	nInsertedStepsTotal=0;
	// Change location
	xMoveDiscontinuity=0;
	yMoveDiscontinuity=0;


	/***************************************************/
	/* Read the following samples or as many as needed */
	/***************************************************/
	/* Read the requested number of samples */
	// CHECK: HLT 31oct06: nsamwanted is not used as it is NSMAX
	// nsamread is the number of samples read at any moment
	for( nsamread = 0; nsamread < nsamwanted; nsamread++ )
//	nsamread = 0;
//	while (TRUE)
	{

		/* Integratators */
		// THe current sample value is compiled into these variables.
		xsum = 0.;
		ysum = 0.;
		zsum = 0.;
		/******************************************/
		/* Read and Average over decimate samples */
		/******************************************/
		/* Average over decimate samples */
		/* If only the first sample of a group of decimate */
		/* samples is returned than mostly you can return one more sample */
		/* as the last decimate samples do not need to be present. */
		/* Because we average now across all decimate samples we need to */
		/* have a complete group of decimate samples at the end or the */
		/* incomplete group decimate samples will be discarded */
		// EXAMPLE:
		// Input x=10, 30, 40, 42, 40
		// decimate=2 because the record is too large to store in one array.
		// The simple way would be: 10, 40, 40 (take every 2nd smaple because decimate is 2)
		// This is not so good as not all data are used and the opportunity to lowpass filter is missed.
		// Output=(10+30)/2, (40+42)/2, (40)/1
		// Therefore all data are used and in such a way that a simple form of lowpass filtering is done.
		// TODO: There are better filters possible.
		// TODO: you could switch to completely dynamic arrays but who tells me what the max size of a recording will be?
		/* Artifact when a single penup signal */
		/* is inserted. The averaging should in fact not take place */
		/* This is because the averaging assumes a continuous signal which */
		/* is sometimes violated */
		/* This can be solved by NOT averaging when there is a <prsmin point */

		// Assume the first point is NOT a low pressure point (=pen lift) because lowpress will only be set when low pressure
		// However, you can set the recording to start immediately and then you may have a recording that starts with and air stroke, i.e., a movement above the digitizer
		// At the very first low pressure point the low pressure feature is well recognized
		lowpress = FALSE;
		// Collect decimate samples each time until end of file occurs
		// ADDED Nov.1.06 Yi, Counter the discontinutity points number
		int DisCounter=0;
		for( isamskip = 0; isamskip < decimate; isamskip++ )
		{
			/* Read a string from the file */
			if( !fpHWR.ReadString( szLine ) )
			{
// #if defined _DEBUG		// for debugging results of discontinuities
// 				CStdioFile f;
// 				f.Open( "C:\\temp.txt", CFile::modeWrite | CFile::modeCreate );
// 				for( int i = 0; i < ( nsamread + nInsertedStepsTotal ); i++ )
// 				{
// 					szMsg.Format( _T("%.0f %.0f %.0f\n"), x[ i ], y[ i ], z[ i ] );
// 					f.WriteString( szMsg );
// 				}
// #endif

				// Just return on end of file, actually neglecting the last decimate-1 or -2.. samples
				goto Finalize;
			}

			// Initialize data before reading data
			/* Remember previous point to estimate the differences between successive points */
			// Undetermined first time you enter this loop
			xprev = xin;
			yprev = yin;

			// ADDED 31oct06 by HLT
			// TEST that you can have an HWR file with only one column which is read into the x array.
			/* Some data do not have a z coordinate, for example if there is only x and y or some may miss the y coordinate or all coordinates */
			xin=0;
			yin=0;
			/* preset zin to prsmin, i.e., just pressure =pen up or down (QUESTION?)  in case zin is not read from scanf*/
			zin = prsmin;

			/* Overwrite x, y, z from the current line that has been read from the HWR file */
			// Read from the line already read so that it does not matter whether the line contains 0, 1, 2, or more coordinates
			sscanf( szLine, "%lf %lf %lf", &xin, &yin, &zin );

			// ADDED 16Jan09: GMB
			// If the first sample is x or y=0, we will flag that and wait for the first non-zero x,y point
			// at which time we will replace each of the (n-1) x,y values with the values of the 1st non-zero x,y (n) sample
			if( ( xin == 0. ) || ( yin == 0. ) )
			{
				if( nsamread == 0 ) fInitialZero = TRUE;
				nZeroSamples++;
			}
			else if( fInitialZero && ( xin != 0. ) && ( yin != 0. ) )
			{
				// notify
				szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: Samples 1-%d have x or y=0 (=unknown). Replaced by x,y of sample %d=%g,%g.", nZeroSamples, nZeroSamples + 1, xin, yin );
				OutputMessage( szMsg, LOG_PROC );

				// replace 1 - nZeroSamples samples x,y values with xin,yin
				for( int r = 0; r < nZeroSamples; r++ )
				{
					x[ r ] = xin;
					y[ r ] = yin;
				}

				// no longer need to notify
				fInitialZero = FALSE;
			}

			/* Special case: do not average when a no pressure point found */
			/* QUESTION: I do not know why we are not averaging low-pressure points */
			if( zin < prsmin )
			{
				/* Take the first prsmin point within the block of decimate points */
				// Thus low-pressure points are not averaged because
				xsum = xin;
				ysum = yin;
				zsum = zin;
				// Remember that we have now a low pressure point
				lowpress = TRUE;
			}
			else if( lowpress )
			{
				/* Do not do anything for additional low pressure points so that you will keep the FIRST low pressure point only */
			}
			else
			{
				/* Cumulate all high pressure (=pen down) points */
				xsum += xin;
				ysum += yin;
				zsum += zin;
			}

			/* Difference between current and previous sample can be estimated in all samples except the very first one */
			if( nsamread > 1 )
			{
				/* Difference between current and previous sample */
				dx = fabs (xin - xprev);
				dy = fabs (yin - yprev);
			}

			// 08Dec08: GMB: Added for .DIS file check. If a dis file, we update discontinuities if selected
			// instead of concerning ourselves whether the # of samples is > 10

			/* check for sudden changes in data sequence */
			/* You cannot detect a discontinuity in the first few, say 10 samples, though */
			// TODO: In fact this parameter 10 needs to be user settable too in the future
//			if (nsamread > 10 && checkdiscontinuity)
			if( checkdiscontinuity && ( ( nsamread > 10 ) || ( nDisCount > 0 ) ) )
			{
				/* No error or discontinuity detected yet */
				error = FALSE;
				xerror = FALSE;
				yerror = FALSE;

				/* Check for sudden change at a discontinuity */
				// sdx is the estimate of the standard deviation in x, or the average, absolute difference between successive x values
				// RERROR = Relative difference that is still acceptable compared to sdx. A normal value is 20
				// EXAMPLE (Assuming AERROR=0 for now): in a series like x=1,2,3,4,3,2, sdx=1, so sdx*RERROR = 20.
				// So if this x series is followed by a value of x=22, then dx=20 so we may just not detect a discontinuity,
				// but in case x=23, then dx=21 we just may detect a discontinuity.
				// AERROR = Absolute error is by default 100. Since these are raw data this represents an absolute size of 100*resolution (=0.0005cm) =0.05cm
				// EXAMPLE (Assuming RERROR=0 for now): In the above series of x=102 we just may not detect a discontinuity and with 102 it may detect
				// We need this absolute size to cope with cases where sdx=0. So we need both relative and absolute error
				// TODO: RERROR needs dynamical and user settable. Default is 20. Is float >0. Preferably >1.
				// TODO: AERROR needs dynamical and user settable. Default is 100. Is float >0. Less than max #levels of digitizer's long axis.
				// A discontinuity is detected if it is detected in at least x or y but not z.
				/* Do not detect z discontinuity. Also, z may be a binary signal in mouse mode */
				// Detect discontinuity in x
				// if sdx==0 you cannot really check for a discontinuity (using AERROR alone because it is likely will detect larger jumps
				// In general, after a long series of sdx=0 you almost certainly will have a discontinuity
				// TODO: Try whether sdx>0 and sdy>0 are really needed. Why is AERROR not taking care of this.


				// 08Dec08: GMB: If we have a DIS file, we are only checking whether a discontinuity is at this sample or not
				if( nDisCount > 0 )
				{
					if( DisInfo::Find( pDisInfo, nDisCount, nsamread, dDuration ) )
						error = TRUE;
				}
				else if( ( dx > ( sdx * dRerror + dAerror ) ) && ( sdx > 0. ) )
				{
					////IDS_GETHWR_3
					//szMsg.Format( "TIME FUNCTION[TIMFUN]: DISCONTINUITY: GETHWR: INFO: Sample difference dx(=%g) > Running sample difference sdx(=%3g) * Relative Error(=%g) + Absolute Error(=%g) = %.4g.", dx, sdx, RERROR, AERROR,sdx * RERROR + AERROR );
					//OutputMessage( szMsg, LOG_PROC );
					error = TRUE;
					xerror = TRUE;
				}
				// Detect discontinuity in y
				if( nDisCount > 0 )
				{
					// do nothing, as it was handled above
				}
				else if( ( dy > ( sdy * dRerror + dAerror ) ) && ( sdy > 0. ) )
				{
					////IDS_GETHWR_4
					//szMsg.Format( "TIME FUNCTION[TIMFUN]: DISCONTINUITY: GETHWR: INFO: Sample difference dy(=%g) > Running sample difference sdt(=%3g) * Relative Error(=%g) + Absolute Error(=%g) = %.4g.", dy, sdy, RERROR, AERROR,sdx * RERROR + AERROR );
					//OutputMessage( szMsg, LOG_PROC );
					error = TRUE;
					yerror = TRUE;
				}

				if( error )
				{
					/* Error detected in x or y */
					// TODO: Ensure we have only ONE definition of IDS_GETHWR_3 and _4 and drop one.
					// TODO: Change the format of the output according to the algorithms >discontinuity posting
					// nsamread-2 and nsamread-1 are just before the discontinuity

					//IDS_GETHWR_5
					szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: x and/or y discontinuous at Sample=%04d due to high pen lift?", nsamread-0 );
					OutputMessage( szMsg, LOG_PROC );
					//IDS_GETHWR_6
					// 07Oct09: GMB: In cases where the 2nd sample (index #1) has a discontinuity, the message below
					//               would be for sample -001. So, do not display if (nsamread-2)<0 [nsamread<2]
					if( nsamread >= 2 )
					{
						szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Sample=%04d: x, y, z= %g ,%g, %g.",
									  nsamread - 2, x [nsamread + nInsertedStepsTotal - 2], y [nsamread + nInsertedStepsTotal - 2], z [nsamread + nInsertedStepsTotal - 2] );
						OutputMessage( szMsg, LOG_PROC );
					}
					//IDS_GETHWR_6
					szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Sample=%04d: x, y, z= %g, %g, %g.<== Discontinuity Begins",
								  nsamread - 1, x [ nsamread + nInsertedStepsTotal - 1 ], y [nsamread + nInsertedStepsTotal - 1], z [nsamread + nInsertedStepsTotal - 1] );
					OutputMessage( szMsg, LOG_PROC );
					//IDS_GETHWR_6
					szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Sample=%04d: x, y, z= %g, %g, %g.<== Discontinuity Ends", nsamread, xin, yin, zin );
					OutputMessage( szMsg, LOG_PROC );
					if( nDisCount <= 0 )
					{
						if( xerror )
						{
							//IDS_GETHWR_3
							szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: XInterSampleDistance=%.0f > Running average XInterSampleDistance(=%3g) * Relative Error(=%g) + Absolute Error(=%g) = %.4g.",
										  dx, sdx, dRerror, dAerror,sdx * dRerror + dAerror );
							OutputMessage( szMsg, LOG_PROC );
						}
						if( yerror )
						{
							//IDS_GETHWR_4
							szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: YInterSampleDistance=%.0f > Running average YInterSampleDistance(=%3g) * Relative Error(=%g) + Absolute Error(=%g) = %.4g.",
										  dy, sdy, dRerror, dAerror, sdy * dRerror + dAerror );
							OutputMessage( szMsg, LOG_PROC );
						}
					}

					// ADDED Oct.30.2006 by Yi and HLT. All discontinuity correction options.
					if (nDiscontinuityCorrection == DISCARDAFTERDISCONTINUITY)
					{
						// TODO: If Stop at discontinuity, return here, otherwise continue
						/* Stop reading before current nsamread */

						//IDS_GETHWR_7
						szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: Correction = %s ( %s )",  "DISCARD", "Discard all samples after discontinuity.");
						OutputMessage( szMsg, LOG_PROC );

						if( pDisInfo ) delete [] pDisInfo;
						return nsamread;
					}
//					else if (nDiscontinuityCorrection == LINEARFILTEREDDISCONTINUITY)
//					{
//						szMsg.Format( "GETHWR: INFO: Correction Action = %s(%s)",  "LINEARFILTERED", "Insert a line after filtered the discontinue part.");
//						OutputMessage( szMsg, LOG_PROC );
//
//
//
//					}
					else if (nDiscontinuityCorrection == INSERTLINEARDISCONTUITY)
					{
						// Insert line between x [nsamread - 1], y [nsamread - 1], z [nsamread - 1] or xprev, yprev, zprev and xin, yin, zin


						/****************************************/
						/* Insert line to bridge discontinuity */
						/****************************************/
						// Find first point (= before discontinuity) and last point (= after discontinuity)
						// (xprev,yprev) and (xin,yin)
						// However, z will be set to 0;
						// The line will have a constant speed
						// The x-speed=sdx * RERROR + AERROR raw data units per inter-sample period
						// Another estimate is sdx but that could be close to zero
						// TODO: A proper value for the speed may be somewhere in between. Check the inserted line is ok.
						// So the number of points to insert is x-distance/x-speed
						// Similarly for the y.
						// Use the maximum number of points to insert for x and y
						// Divide the points equally BETWEEN begin and end
						// NOTE: for 2 steps we only need one point.

						//how many points need to be insert for joining linearity
						/*
						nInsertedSteps = (int) ceil (dx / (sdx * RERROR + AERROR));
						if ( nInsertedSteps < (int) ceil (dy / (sdy * RERROR + AERROR)) )
								nInsertedSteps = (int) ceil (dy / (sdy * RERROR + AERROR));
						*/

						nInsertedSteps = 0;

						// 08Dec08: GMB: # of inserted steps is calculated differently if DIS file exists
						if( nDisCount > 0 )
						{
							nInsertedSteps = (int)( ( dDuration / ( 1.0 / dSampRate ) ) + 0.5 );
						}
						else
						{
							// dx and dy are absolute
							if( sdx > 0. )
							{
								nInsertedSteps = (int)ceil( dx / sdx );
								if( ( sdy * dRerror + dAerror ) > 0. )
								{
									if( nInsertedSteps < (int)ceil( dy / sdy ) )
										nInsertedSteps = (int)ceil( dy / sdy );
								}
							}
						}

						// TODO: Assert this number is at least one. That cannot happen because the steps size sdx was found to be larger than sdx * RERROR + AERROR
						// if we need only one step we do not need a point as the number of points is one less than the number of steps
						if( nInsertedSteps > 1 )
						{
							// TODO: INFO:  Insert a linear, average speed movement. The time is lost but the distance is maintained =>WARNING: Discontinuity corrected by inserting average velocity movement
							//calculate the steps in X, Y directions
							dXLinearStep = (xin-xprev) / nInsertedSteps;
							dYLinearStep = (yin-yprev) / nInsertedSteps;

							// Generate the points to be inserted
							// iInsertedSteps=0 would be the last point before the discontinuity but that point does not need to be touched.
							// iInsertedSteps=nInsertedSteps would be the the first punt after the discontinuity and does not need to be touched.
							for ( int iInsertedSteps = 1; iInsertedSteps < nInsertedSteps; iInsertedSteps++ )
							{

								// If output array full: End without adding
								// 30Jan09: GMB: This check is incorrect as it does not account for total steps inserted
//								if( ( nsamread + iInsertedSteps ) >= nsamwanted ) return nsamread + iInsertedSteps - 1;
								if( ( nsamread + nInsertedStepsTotal + iInsertedSteps ) >= nsamwanted )
								{
									szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: # inserted samples (%d) + read samples (%d) > max allowed (%d). Experiment Properties->Advanced->Discontinuity for adjustments.", nInsertedStepsTotal, nsamread, nsamwanted );
									OutputMessage( szMsg, LOG_PROC );
									return( nsamread + nInsertedStepsTotal + iInsertedSteps - 1 );
								}

								// Insert into output array
								x[ nsamread + nInsertedStepsTotal + iInsertedSteps - 1 ] = xprev + iInsertedSteps * dXLinearStep;
								y[ nsamread + nInsertedStepsTotal + iInsertedSteps - 1 ] = yprev + iInsertedSteps * dYLinearStep;
								// Set pressure = -1 as a discontinuity correction can then be recognized
								z[ nsamread + nInsertedStepsTotal + iInsertedSteps - 1 ] = dZInsertedPoint;
								//set pressure zin to -1 for the point just after discontinuity
								// 09Dec08: GMB: we do not want to set the 1st real data point's pressure to -1...it makes no sense
//								zsum = dZInsertedPoint;
							}
							// Integrate time inserted or omitted
							nInsertedStepsTotal += nInsertedSteps - 1;

							if( nDisCount > 0 )
							{
								szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: DIS File=%s. Sample=%d. InterSamplePeriod of Discontinuity=%g s.", szDisName, nsamread, dDuration );
								OutputMessage( szMsg, LOG_PROC );
//								szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Normal InterSamplePeriod=1/SamplingRate(=%.0f)=%g. =>Missing #Samples=%d.", dSampRate, 1. / dSampRate, nInsertedSteps - 1 );
//								OutputMessage( szMsg, LOG_PROC );
//								szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Absolute InterSampleDistance at discontinuity=%.0f. =>Insert missing samples with absolute InterSampleDistance=%f..",
//											  sqrt( dx * dx + dy * dy ), sqrt( dx * dx + dy * dy ) / ( nInsertedSteps - 1 ) );
//								OutputMessage( szMsg, LOG_PROC );
							}
							else
							{
								szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Absolute InterSampleDistance at discontinuity=%.0f. Running average absolute InterSampleDistance=%.1f.",
											  sqrt( dx * dx + dy * dy ), sqrt( sdx * sdx + sdy * sdy ) );
								OutputMessage( szMsg, LOG_PROC );
								szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] INFO: Probable missing #samples=%d.", nInsertedSteps - 1 );
								OutputMessage( szMsg, LOG_PROC );
							}

							//IDS_GETHWR_7
							szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: Corrected by inserting constant-velocity line of #Samples=%d with Pressure=%g before Sample=%04d.", nInsertedSteps - 1, dZInsertedPoint, nsamread );
							OutputMessage( szMsg, LOG_PROC );
						}//end: if (nInsertedSteps > 1)

					}//end: else if (nDiscontinuityCorrection == INSERTLINEARDISCONTUITY)


					// All subsequent points x[ nsamread ] and y[ nsamread ] will be moved before output
					// Be sure to set the move distance to 0 initially
					// In case of more discontinuities the net moving distance integreates
					else if (nDiscontinuityCorrection == MOVETOWARDSDISCONTINUITY)
					{
						//IDS_GETHWR_7
						szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: Correction = %s ( %s )",  "MOVE", " Move samples after discontinuity to discontinuity and change first moved sample to Pressure of Inserted Samples.");
						OutputMessage( szMsg, LOG_PROC );

						xMoveDiscontinuity = xMoveDiscontinuity + xin - xprev;
						yMoveDiscontinuity = yMoveDiscontinuity + yin - yprev;
						//set pressure zin to -1 for the point just after discontinuity
						zsum = dZInsertedPoint;
					}
					else if (nDiscontinuityCorrection == ACCEPTDISCONTINUITY)
					{
						// Do nothing. Just generate the information message and continue if possible at all.
						// Expect the filtered signal and the spectrum to be weird around the discontinuity.
						//IDS_GETHWR_7
						szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: Correction = %s ( %s )",  "ACCEPT", "Accept the discontinuity without corrections.");
						OutputMessage( szMsg, LOG_PROC );
					}

				}//end of if (error)
			} // end of if (nsamread > 10 && checkdiscontinuity)


			/* After approving the data, use sdx, sdy to build new running average of sd */
			// dx and dy are not defined for the first sample
			if (nsamread > 1)
			{
				/* Leaky running average of differences */
				sdx = (1. - dIntegrationFactor) * sdx + dIntegrationFactor * dx;
				sdy = (1. - dIntegrationFactor) * sdy + dIntegrationFactor * dy;
			}
		}//end of for( isamskip = 0; isamskip < decimate; isamskip++ )

		// 02Mar09: GMB: Added check for out of range
		if( ( nsamread + nInsertedStepsTotal ) >= nsamwanted )
		{
			szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: # inserted samples (%d) + read samples (%d) > max allowed (%d). Experiment Properties->Advanced->Discontinuity for adjustments.", nInsertedStepsTotal, nsamread, nsamwanted );
			OutputMessage( szMsg, LOG_PROC );
			if( pDisInfo ) delete [] pDisInfo;
			return( nsamread + nInsertedStepsTotal );
		}


		/* Store averaged data point until the discontinuity */
		if (lowpress)
		{
			// UNCOMMENTED Oct.31.06 by Yi
			/* Except when low pressure point encountered */
			//x[ nsamread ] = xsum;
			//y[ nsamread ] = ysum;
			//z[ nsamread ] = zsum;

			// Oct.31.06 Yi: moving distance integrates
//			if ( nsamread > DisCounter)
//				return nsamread;
//			else
//			{
//				x[ nsamread - DisCounter ] = xsum - xMoveDiscontinuity;
//				y[ nsamread - DisCounter ] = ysum - yMoveDiscontinuity;
//				z[ nsamread - DisCounter ] = zsum;
				x[ nsamread + nInsertedStepsTotal ] = xsum - xMoveDiscontinuity;
				y[ nsamread + nInsertedStepsTotal ] = ysum - yMoveDiscontinuity;
				z[ nsamread + nInsertedStepsTotal ] = zsum;
//			}

			//DisCounter=DisCounter+2;
		}
		else
		{
			// UNCOMMENTED Oct.31.06 by Yi
			//x[ nsamread ] = xsum / decimate;
			//y[ nsamread ] = ysum / decimate;
			//z[ nsamread ] = zsum / decimate;

			// Oct.31.06 Yi: moving distance integrates
//			if ( nsamread > DisCounter)
//				return nsamread;
//			else
//			{
				x[ nsamread + nInsertedStepsTotal ] = xsum / decimate - xMoveDiscontinuity;
				y[ nsamread + nInsertedStepsTotal ] = ysum / decimate - yMoveDiscontinuity;
				z[ nsamread + nInsertedStepsTotal ] = zsum / decimate;
//			}

			//DisCounter=DisCounter+2;
		}

//		nsamread=nsamread + DisCounter;


	}//end of for( nsamread = 0; nsamread < nsamwanted; nsamread++ )

Finalize:
	//IDS_GETHWR_8
	if( nInsertedStepsTotal > 0 )
	{
		szMsg.Format( "TIME FUNCTION: DISCONTINUITY: [GETHWR] WARNING: Correction: Total number of samples inserted in this trial = %d.", nInsertedStepsTotal );
		OutputMessage( szMsg, LOG_PROC );
	}

	// cleanup
	if( pDisInfo ) delete [] pDisInfo;

	/* Return the number of samples read successfully */
	// 03Mar09: GMB: Need to add inserted total to return value
	return( nsamread + nInsertedStepsTotal );
}

/************************************************************************/
int getdata( CStdioFile& fIn, double* x, int nsmax, CString& txt, int decimate )
/************************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );

	CString szFile = fIn.GetFilePath();
	FILE* f = NULL;
	if( fopen_s( &f, szFile, _T("w") ) != 0 ) return 0;
	if( !f ) return 0;

	int nRes = getdata( f, x, nsmax, txt, decimate );
	fclose( f );

	return nRes;
}

/************************************************************************/
int getdata( FILE* fp_in, double* x, int nsmax, CString& txt, int decimate )
/************************************************************************/
/* read #data txt followed by as many double or int data */
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( fp_in != NULL );

	int is = 0, nsread = 0, nsfile = 0;
	double dTemp = 0.0;
	CString szMsg;
	char pszMsg[ 250 ];

	/* Do not require a space after nsfile because there may be nothing at all */
	if (fscanf (fp_in, "%d%[^\n]", &nsfile, pszMsg) == EOF)
	{
//		OutputMessage( "GETDATA(): INFO: End Of File.", LOG_PROC );
		return 0;
	}
	txt = pszMsg;

	if (decimate <= 0)
	{
		szMsg.Format( "GETDATA(): ERROR: Decimate=%d < 1.", decimate );
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	if (nsfile < 1)
	{
		szMsg.Format( "GETDATA(): Reading; DataLabel=%s; #Data=%d <1.", txt, nsfile );
		OutputMessage( szMsg, LOG_PROC );
	}
	else if (nsfile > (nsmax * decimate))
	{
		szMsg.Format( "GETDATA(): Reading; DataLabel=%s; #Data=%d >(nsarray*decimate=%d)=%d; Truncated.", txt, nsfile, decimate, nsmax * decimate );
		OutputMessage( szMsg, LOG_PROC );
	}
	nsread = MIN (nsfile, nsmax * decimate);

	/* read array */
	for (is = 0; is < nsread; is++)
	{
		if (fscanf (fp_in, "%lf", &dTemp) == EOF)
		{
			szMsg.Format( "GETDATA(): ERROR: Unexpected EOF at DataNr=%d. No. data does not match no. specified.", is );
			OutputMessage( szMsg, LOG_PROC );
			return 0;
		}
		// 10Sep03: GMB - Added assert to ensure validity of index
		ASSERT( ( ( is + decimate - 1 ) / decimate ) >= 0 );
		x [(is + decimate - 1) / decimate] = dTemp;
	}

	/* skip rest of array that does not fit in array */
	for (is = nsread; is < nsfile; is++)
	{
		if (fscanf (fp_in, "%*f") == EOF)
		{
			szMsg.Format( "GETDATA(): ERROR: Unexpected EOF at DataNr=%d.", is );
			OutputMessage( szMsg, LOG_PROC );
			return 0;
		}
	}

	return ((nsread - 1 + decimate - 1) / decimate + 1);
}

/************************************************************************/
void putdata( CStdioFile& fOut, double* x, int ns, unsigned int nResourceID, double factor, double* dest )
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );

	CString szTmp;
	szTmp.LoadString( nResourceID );
	putdata( fOut, x, ns, szTmp, factor, dest );
}

/************************************************************************/
void putdata( CStdioFile& fOut, double* x, int ns, CString txt, double factor, double* dest )
/************************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );

	// We assume file is open already
	// It is callers responsibility to maintain open/close state of file

	int is = 0;
	CString szMsg;

	szMsg.Format( _T("%d %s\n"), ns, txt );
	fOut.WriteString( szMsg );

	if( ns < 1 )
	{
		szMsg.Format( "PUTDATA(): Writing file=%s; #Data=%d < 1.", txt, ns );
		OutputMessage( szMsg, LOG_PROC );
	}

	for( is = 0; is < ns; is++ )
	{
		szMsg.Format( _T("%lf "), x[ is ] * factor );
		if( dest ) dest[ is ] = x[ is ] * factor;
		fOut.WriteString( szMsg );
	}

	fOut.WriteString( _T("\n") );
}

/* write #data txt followed by as many double data */
/************************************************************************/
void putdata( FILE *fp_out, double* x, int ns, unsigned int nResourceID, double factor, double* dest )
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( fp_out != NULL );

	CString szTmp;
	szTmp.LoadString( nResourceID );
	putdata( fp_out, x, ns, szTmp, factor, dest );
}

/************************************************************************/
void putdata( FILE *fp_out, double* x, int ns, CString txt, double factor, double* dest )
/************************************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( fp_out != NULL );

	int is = 0;
	CString szMsg;

	fprintf( fp_out, _T("%d %s\n"), ns, txt );
	if( ns < 1 )
	{
		szMsg.Format( "PUTDATA(): Writing file=%s; #Data=%d < 1.", txt, ns );
		OutputMessage( szMsg, LOG_PROC );
	}

	for( is = 0; is < ns; is++ )
	{
		if( fprintf( fp_out, _T("%lf "), x [is] * factor ) == EOF )
		{
			szMsg.Format( "PUTDATA(): ERROR: Writing file=%s; at DataNr=%d of #Data=%d.", txt, is, ns);
			return;
		}
		if( dest ) dest[ is ] = x[ is ] * factor;
	}

	fprintf( fp_out, _T("\n") );
}

/*************************************************/
int getlinefloat( CStdioFile& fIn, double* f, int ndatamax )
/*************************************************/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( f != NULL );

	CString szLine;
	char *p = NULL, *nexttoken = NULL;
	int ndata = 0, nLen = 0;

	if( !fIn.ReadString( szLine ) )
	{
//		OutputMessage( "GETLINEFLOAT(): Reached end of input file.", LOG_PROC );
		return 0;
	}

	nLen = szLine.GetLength();
	if( nLen >= GETLINELEN )
	{
		CString szMsg;
		szMsg.Format( "GETLINEFLOAT(): ERROR: Line length > %d.", GETLINELEN );
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	// Replace occasional tabs or other spearators by blanks
	for( ndata = 0; ndata < nLen; ndata++ )
	{
		if( szLine[ ndata ] == TABSEP )
			szLine.SetAt( ndata, BLANK );
	}

	// strtok places a NULL  terminator in front of the token, if found
	char strTemp[ GETLINELEN ];
	// strcpy_s( strTemp, GETLINELEN, szLine );
	strncpy(strTemp, szLine, GETLINELEN);
	p = strtok_s( strTemp, BLANKSTR, &nexttoken );
	if( !p ) return 0;
	if( !::isdigit( *p ) ) return 0;
	f[ 0 ] = atof( p );

	for( ndata = 1; ndata < ndatamax; ndata++ )
	{
		/* A second call to strtok using a NULL
		as the first parameter returns a pointer
		to the character following the token  */
		p = strtok_s( NULL, BLANKSTR, &nexttoken );
		if( !p ) break;
		f[ ndata ] = atof( p );
	}

	return ndata;
}

/*************************************************************************/
int readhwr( CStdioFile& fIn, CString infile, double* x, double* y, double* z,
			 int nsammax, int nsopt, int& nsarr, int nsamskip, int decimate,
			 int decimatearg, double prsmin, int checkdiscontinuity,
			 bool fRemoveTrailingPenlift )
/*************************************************************************/
/*
  Automatic hwr input routine to automatically decimate

  nsopt is the optimal number of data to be read. If fft is needed
  and nsmax is power of 2 then nsopt = nsmax / 1.25
*/
{
	// 10Sep03: GMB - Added asserts to ensure validity of pointers
	ASSERT( x != NULL );
	ASSERT( y != NULL );
	ASSERT( z != NULL );

	int nsin = 0, ns = 0;
	long nsinfile = 0L, nsfile = 0L, nsoptfile = 0L;
	int decimateopt = 0;
	CString szMsg;

reread:

	/*************************************/
	/* Read infile at specified decimate */
	/*************************************/
	szMsg.Format( "TIME FUNCTION: [READHWR()] Reading Hwr_file=%s.", infile );
	OutputMessage( szMsg, LOG_PROC );

	nsin = gethwr( fIn, x, y, z, nsammax, (int)nsopt, nsamskip, decimate,
				   prsmin, checkdiscontinuity, fRemoveTrailingPenlift );

	/**********************************************/
	/* Test trunctation: If yes increase decimate */
	/**********************************************/
	/* Chance that the last sample is exactly nsopt is neglected */
	if (nsin <= 10)
	{
		szMsg.Format( "TIME FUNCTION: [READHWR()] Read only #Samples=%d.", nsopt );
		OutputMessage( szMsg, LOG_PROC );
	}
	else if (nsin >= nsopt)
	{
		szMsg.Format( "TIME FUNCTION: [READHWR()] Truncated at #Samples=%d.", nsopt );
		OutputMessage( szMsg, LOG_PROC );
		if (decimatearg == 0)
		{
			/* Change decimate parameter; increase 20% and round up */
			decimate = (int)(decimate * 1.2 + 1.);
			szMsg.Format( "TIME FUNCTION: [READHWR()] Trying Decimate=%d.", decimate );
			OutputMessage( szMsg, LOG_PROC );
			fIn.SeekToBegin();
			goto reread;
		}
		else OutputMessage( _T("\n"), LOG_PROC );
	}

	/************************************/
	/* Test for empty file: If yes exit */
	/************************************/
	else if (nsin <= 0)
	{
		szMsg.Format( "TIME FUNCTION: [READHWR()] WARNING: Hwr_file=%s empty.", infile );
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}

	/************************/
	/* Find last penup part */
	/************************/
	// 27Sep09: GMB: If trailing pen lift is to be removed, do original (find last point > min pressure)
	// otherwise, set to last sample
	if( fRemoveTrailingPenlift )
	{
		for (ns = nsin; ns > 0; ns--)
		{
			// 03Mar09: GMB: = min pressure is pen up, so we exclude
			//if (z [ns - 1] >= prsmin) break;
			if (z [ns - 1] > prsmin) break;
		}
	}
	else ns = nsin;
	/* File may be longer than 32000 */
	nsinfile = (long) nsin * decimate;
	nsfile   = (long) ns   * decimate;
	/* Feedback */
	if (ns != nsin)
	{
		szMsg.Format( "TIME FUNCTION: [READHWR()] Trailing penlift; #Samples=%d(%ld)->%d(%ld).", nsin, nsinfile, ns, nsfile );
		OutputMessage( szMsg, LOG_PROC );
	}

	/********/
	/* Test */
	/********/
	if (nsinfile > CRAZYBIG || nsfile > CRAZYBIG)
	{
		szMsg.Format( "TIME FUNCTION: [READHWR()] ERROR: Hwr_file=%s longer than %ld samples?", infile, CRAZYBIG);
		OutputMessage( szMsg, LOG_PROC );
		return 0;
	}
	if (ns == 0)
	{
		szMsg.Format( "TIME FUNCTION: [READHWR()] WARNING: No pendown samples in Hwr_file=%s.", infile);
		OutputMessage( szMsg, LOG_PROC );
	}

	/******************************************************************/
	/* If trailing penup unnessitated some decimate steps: Read again */
	/******************************************************************/
	/* Round up for new optimal decimate if trailing penup is known */
	/* This method is not exact due to rounding and coarse penup testing */
	decimateopt = (int)(1. + 1.25 * decimate * ns / nsammax);
	if (decimateopt < decimate && decimatearg == 0)
	{
		/* New optimal ns */
		nsopt = (int)(ns * (double) decimate / decimateopt);
		nsoptfile = (long) nsopt * decimateopt;
		/* Feedback */
		szMsg.Format( "TIME FUNCTION: [READHWR()] Optimal decimate=%d, #Samples=%d(%ld).", decimateopt, nsopt, nsoptfile);
		OutputMessage( szMsg, LOG_PROC );
		decimate = decimateopt;
		szMsg.Format( "TIME FUNCTION: [READHWR()] Reading Hwr_file=%s.", infile);
		OutputMessage( szMsg, LOG_PROC );

		fIn.SeekToBegin();

		/* Read infile */
		ns = gethwr( fIn, x, y, z, nsammax, nsopt, nsamskip, decimateopt,
					 prsmin, checkdiscontinuity, fRemoveTrailingPenlift );
		/* No tests needed as they have been done already */
	}
	else if (decimate != 1)
	{
		/* Feedback if ok and decimate done */
		szMsg.Format( "TIME FUNCTION: [READHWR()] Decimate=%d, #Samples=%d(%d).", decimate, ns, ns * decimate);
	}

	/* Return arguments */
	// 12Sep03: GMB - When there are 0 samples that meet the min pen pressure
	//				  criterion, do all samples, otherwise the # of samples
	//				  meeting criterion
	//				  This was added due to crashes as a result of -1 array access
	//				  for "previous sample"
	if( ns > 0 ) nsarr = ns;
	else nsarr = nsin;

	return decimate;
}

#pragma warning( default: 4996 )

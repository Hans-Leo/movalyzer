#ifndef __IMG_H__
#define	__IMG_H__

namespace SFINX {

typedef unsigned char	PIXEL8;
typedef unsigned short	PIXEL16;
typedef unsigned char	PIXEL;

#define	MAXPIXEL8	255
#define	MAXPIXEL16	65535

#define	MAXPIXEL	MAXPIXEL8

typedef	signed short	COORD;

struct colour {
	unsigned char	r, g, b;
};

/* image: all fields are set by read_PCX except clrs and filename */
/* other routines change fields if appropriate */

struct image
{
public:
	image() : data( NULL ), clrtable( NULL ), filename( NULL ) {}
	BOOL Copy( image* );

	PIXEL			**data;		/* the image array */
	unsigned char   clrbits;	/* colour bits used */
	unsigned char	clrs;		/* clrs actually used */
	unsigned char	bipp;		/* bits per pixel */
	long int		w, h;		/* width, height */
	long int		dpi;		/* resolution in dpi */
	long int		nallocl;	/* bytes per line allocated */
	struct colour	*clrtable;	/* colour (rgb) table */
	char			*filename;
};

#define	PUT(img, x, y, c)	((img)->data[y][x]) = (c)
#define GET(img, x, y)		((img)->data[y][x])
#define PTR(img, x, y)		(&((img)->data[y][x]))
#define	OR_IMG(img, x, y, c)	((img)->data[y][x]) |= (c)
#define	AND_IMG(img, x, y, c)	((img)->data[y][x]) &= (c)

void *myrecalloc(void *,size_t ,size_t ,size_t );
long int standard_bytesperline(int, int);
image *alloc_img(int ,int ,int );
void unpack_img_8(image *);
void unpack_img_4(image *);
int clear_img(image *);
void free_img(image *);
image *read_ASC(char *);
int write_ASC(char *,image *);
void line(image *,int ,int ,int ,int ,PIXEL ,int );
void smooth(image *, int, int);
int threshold_img(image *,PIXEL ,PIXEL );
int clr2intensity(image *);
struct colour *standard_grey_table(unsigned char );
struct colour *standard_clr_table(unsigned char );
void set_img_clrbits(image *,unsigned char );
void set_img_greybits(image *,unsigned char );
int set_img_background(image *);

};	// namespace

#endif	// __IMG_H__

#pragma once

#include "img.h"

namespace SFINX {

struct pcxhdr {
    unsigned char	signature;	/*PCX file identifier*/
    unsigned char	version;	/*version compatibility level*/
    unsigned char	encoding;	/*encoding method*/
    unsigned char	bitsperpix;	/*bits per pixel, or depth*/
    unsigned short	Xleft;		/*X position of left edge*/
    unsigned short	Ytop;		/*Y position of top edge*/
    unsigned short	Xright;		/*X position of right edge*/
    unsigned short	Ybottom;	/*Y position of bottom edge*/
    unsigned short	Xresolution;	/*X screen res of source image*/
    unsigned short	Yresolution;	/*Y screen res of source image*/
    unsigned char	PCXpalette[16][3];	/*PCX color map*/
    unsigned char	reserved1;	/*should be 0, 1 if std res fax*/
    unsigned char	planes;		/*bit planes in image*/
    unsigned short	linesize;	/*byte delta between scanlines */
    unsigned short	paletteinfo;	/*0 == undef
					  1 == color
					  2 == grayscale*/
    unsigned char reserved2[58];	/*fill to struct size of 128*/
};

int write_PCX(image *, char *);
image *read_PCX(char *);

};	// namespace

#ifndef __SEGMENTS_H__
#define	__SEGMENTS_H__

#include "img.h"

namespace SFINX {

/* convention: a point is ON if p >= 128, in other words, its highest bit
is set. So the other bits are used for coding all kinds of things. */
#define	ON(p)	((p) >> 7)

#define	pix_thresh	128
#define	pix_edge	79
#define pix_eroded	73
#define	pix_fork	111

#define	SEG_NOISE(seg)	(seg->type >> 7)
#define	SET_NOISE(seg)	(seg->type |= 0x80)

struct segment
{
public:
	segment() : x( NULL ), y( NULL ), xend( NULL ), yend( NULL ), z( NULL )
	{
		memset( a, 0, 2 * sizeof( double ) );
		memset( rms, 0, 2 * sizeof( double ) );
	}
	COORD	*x, *y;		/* point coordinate arrays */
	COORD	*xend;		/* xend is set to &x[n-1] by findsegments() */
	COORD	*yend;		/* yend is set to &y[n-1] by findsegments() */
	int		n;
	char	type;		/* 1 = open loop, 2 = closed loop */
						/* bit 7 set for noise segment */
	PIXEL	*z;			/* local thickness */
	double	a[2];		/* tangent of regression line for both ends */
	double	rms[2];		/* rms of that tangent */
};

/* proto segment.c */
int auto_threshold(image *);
void clearedges(image *,PIXEL, int );
int connectedness(image *,COORD ,COORD );
segment *findforks(image *);
int markforks(image *,segment *,PIXEL ,PIXEL );
int findsegments(image *,segment **);
void paintsegments(image *,segment *,int );
void writesegments(segment *,int ,char *);

/* proto features.c */
double sqr(double );
int cbdist(COORD ,COORD ,COORD ,COORD );
void lsaxis(segment *,int ,int ,int );
double estim_slant(segment *,int );
int cmpsegm_xyslant(segment *,segment *);
int filtersegments(image *, segment *, int, int);
int writefeatures(segment *,int ,FILE *,char *, char *,int, BOOL, int);

};	// namespace

#endif	// __SEGMENTS_H__

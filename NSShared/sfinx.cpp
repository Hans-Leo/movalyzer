#include "StdAfx.h"
#include <math.h>
#include "Sfinx.h"
#include "img.h"
#include "pcx.h"
#include "segments.h"
#include "message.h"
#include "..\Common\DefFun.h"

namespace SFINX {

/* prototypes not in .h files */
int	spta(image *, PIXEL);		/* stpa.c */

void set_special_ctable_8(image *img)
{
	static struct colour tmpclr[] = {
	{255,  0,  0},
	{255, 32,  0},
	{255, 64,  0},
	{255, 96,  0},
	{255,128,  0},
	{255,160,  0},
	{255,192,  0},
	{255,224,  0},
	{255,255,  0},
	{255,255,  0},
	{224,255,  0},
	{192,255,  0},
	{160,255,  0},
	{128,255,  0},
	{ 96,255,  0},
	{ 64,255,  0},
	{ 32,255,  0},
	{  0,255,  0},
	{  0,224, 32},
	{  0,192, 64},
	{  0,160, 96},
	{  0,128,128},
	{  0, 96,160},
	{  0, 64,192},
	{  0, 32,224},
	{  0,  0,255}};
	struct colour	*c;
	int		i;

	set_img_clrbits(img, 8);
	c = &(img->clrtable[128]);
	memcpy(c, tmpclr, sizeof(tmpclr));
	/* set the remaining ones to the last clr */
	i = sizeof(tmpclr) / sizeof(struct colour);
	c = &(tmpclr[i - 1]);
	for (i += 128; i < 256; i++) {
		img->clrtable[i] = *c;
	}
}

void set_skeleton_signature(image *img)
{
	static struct colour	signature = { 254, 254, 254 };
	
	img->clrtable[(img->clrbits == 1) ? 1 : 255] = signature;
}

/* return zero if the img has the signature */
int query_skeleton_signature(image *img)
{
	static struct colour	signature = { 254, 254, 254 };
	
	return(memcmp(&(img->clrtable[(img->clrbits == 1) ? 1 : 255]), &signature, 
						sizeof(struct colour)));
}

char *basename(char *);			/* sfinx.c */
void expand_fn(char *, char *);		/* sfinx.c */

BOOL AFX_API_EXPORT RunSFINX( CString szDir, CString szPCX, int nTrialNum, int nSmoothIter,
							  int nThreshold, int nResolution, double dMinSegLen, double dMissing )
{
	int		smooth_iter = nSmoothIter;
	int		threshold = nThreshold;
	double	minsegmentlength = dMinSegLen;
	int		resolution = nResolution;
	char	missing_string[ 40 ] = "0";
	sprintf_s( missing_string, 39, "%f", dMissing );

	char	outpcx_intensity[MAX_PATH] = "*-1.PCX"; 
	char	outpcx_smooth[MAX_PATH] = "*-3.PCX"; 
	char	outpcx_threshold[MAX_PATH] = "*-2.PCX"; 
	char	outpcx_skeleton[MAX_PATH] = "*-4.PCX"; 
	char	outpcx_forks[MAX_PATH] = "*-F.PCX"; 
//	char	outpcx_segments[MAX_PATH] = "*-G.PCX"; 
	char	outpcx_segments[MAX_PATH] = "*-F.PCX"; 
	char	out_segments[MAX_PATH] = "*.HWR";
	char	out_features[MAX_PATH] = "*.EXT";
	char	out_echo[MAX_PATH] = "";
	char	inpcx_img[MAX_PATH] = "";
	FILE	*outf = NULL;
	char	*inpcx_img_basename = NULL;
	char	preproc_in = 0,	/* some internal `switches' */
			preproc_out = 0,
			colorfiles = 1,
			clip = 1,
			standard_messages = 0;
	int		nsegments, ngood;
	SFINX::image	*img1 = NULL;
	SFINX::image	*img2 = new SFINX::image;
	SFINX::segment	*segments = NULL;
	SFINX::segment	*forks = NULL;
	CString szExt, szMsg;
	BOOL fError = FALSE;

	strcpy_s( out_echo, MAX_PATH, szDir );
	strcpy_s( inpcx_img, MAX_PATH, szPCX );

	/* get basename and substitute it for '*' in other filenames */
	
	inpcx_img_basename = basename(inpcx_img);
	if (*out_echo == '\0') strcpy_s(out_echo, MAX_PATH, inpcx_img_basename);
	expand_fn(outpcx_intensity, inpcx_img_basename); 
	expand_fn(outpcx_smooth, inpcx_img_basename); 
	expand_fn(outpcx_threshold, inpcx_img_basename); 
	expand_fn(outpcx_skeleton, inpcx_img_basename); 
	expand_fn(outpcx_forks, inpcx_img_basename); 
	expand_fn(outpcx_segments, inpcx_img_basename); 
	expand_fn(out_segments, inpcx_img_basename);
	expand_fn(out_features, inpcx_img_basename);
	szExt = out_features;
	szExt = szExt.Left( szExt.GetLength() - 6 );
	szExt += _T(".EXT");
	strcpy_s( out_features, MAX_PATH, szExt );

	szMsg.Format( _T("SFINX Processing: File=%s, Trial#=%d, SmoothIter=%d, Threshold=%d, Resolution=%d, MinSegLen=%g"),
				  szPCX, nTrialNum, nSmoothIter, nThreshold, nResolution, dMinSegLen );
	::OutputMessage( szMsg, LOG_PROC );

	img1 = SFINX::read_PCX(inpcx_img);
	if( !img1 ) { fError = TRUE; goto Cleanup; }

// 07 May 09: GMB: Warning changed since it was not appropriate as auto-detect was specified with 0
	if (resolution == 0)
	{
		resolution = img1->dpi;
//		::OutputMessage( "Resolution set to autodetect (0) but input file contains no resolution field", LOG_PROC );
		szMsg.Format( _T("SFINX Processing: Auto-detected resolution=%d"), resolution );
		::OutputMessage( szMsg, LOG_PROC );
	}

	if (resolution < 300)
	{
		szMsg.Format( "The resolution of %s (%ddpi) is lower than the min. recommended (300dpi)", inpcx_img, resolution );
		::OutputMessage( szMsg, LOG_PROC );
	}

	if (img1->bipp < 8) unpack_img_8(img1);

	if (preproc_in)
	{
		if (query_skeleton_signature(img1) != 0)
		{
			::OutputMessage( "Input file for SFINX -Si should be a 'outpcx_skeleton' file produced by SFINX", LOG_PROC );
			fError = TRUE;
			goto Cleanup;
		}
		if (img1->clrbits == 1)
		{
			::OutputMessage( "input skeleton file is black & white; no thickness information will be available", LOG_PROC );
			threshold_img(img1, 1, pix_thresh);
		}
		goto preproc_in_s;
	}

	clr2intensity(img1);

//	if (*outpcx_intensity) {
//		set_img_greybits(img1, 8);
//		write_PCX(img1, outpcx_intensity);
//	}

/* we threshold the image to separate ink from paper */

	if (threshold <= 0) threshold = auto_threshold(img1);
	threshold_img(img1, threshold, pix_thresh);

//	if (*outpcx_threshold) {
//		set_img_greybits(img1, 1);
//		write_PCX(img1, outpcx_threshold);
//	}

	smooth(img1, pix_thresh, smooth_iter);
	
	::OutputMessage( "Clearing Edges", LOG_PROC );
	clearedges(img1, pix_edge, clip);

//	if (*outpcx_smooth) {
//		if (colorfiles) set_img_clrbits(img1, 4);
//		else set_img_clrbits(img1, 1);
//		write_PCX(img1, outpcx_smooth);
//	}
	/* set image frame pixels to 0 */
	clearedges(img1, 0, 0);

	spta(img1, pix_thresh);

	{	/* make eroded pixels one colour */
		int	x, y;
		
		for (x = 0; x < img1->w; x++) {
			for (y = 0; y < img1->h; y++) {
				SFINX::PIXEL	*p = PTR(img1, x, y);
				if (*p == 0) continue;
				if (*p == pix_edge) *p = 0;
				else if (!ON(*p)) *p = pix_eroded;
			}
		}
	}

//	if (*outpcx_skeleton) {
//		if (colorfiles) set_special_ctable_8(img1);
//		else set_img_clrbits(img1, 1);
//		set_skeleton_signature(img1);
//		write_PCX(img1, outpcx_skeleton);
//	}

preproc_in_s:

//	if (preproc_out) exit(0);
	
	forks = findforks(img1);

	if (*outpcx_forks)
	{
		if (colorfiles)
		{
			markforks(img1, forks, pix_fork, pix_fork);
			set_special_ctable_8(img1);
// 13Mar09: GMB: We are now outputting #6 only, not #5
//			write_PCX(img1, outpcx_forks);
		}
		else
		{
			markforks(img1, forks, pix_fork, 255);
			set_img_clrbits(img1, 1);
// 13Mar09: GMB: We are now outputting #6 only, not #5
//			write_PCX(img1, outpcx_forks);
			markforks(img1, forks, pix_fork, pix_fork);
		}
	}
	else markforks(img1, forks, pix_fork, pix_fork);

// 28May09: GMB: find/filter segments is altering img1 unnecessarily (as per Somesh)
//				 Copy contents of img1 into img2, and determine nsegments using img2.
	//	nsegments = findsegments(img1, &segments);
	if( !img2->Copy( img1 ) )
	{
		szMsg = _T("SFINX: ERROR: Unable to copy image.");
		::OutputMessage( szMsg, LOG_PROC );
		fError = TRUE;
		goto Cleanup;
	}
	nsegments = findsegments( img2, &segments );
	ngood = filtersegments( img2, segments, nsegments,
						    (int)( ( minsegmentlength * resolution + 12.7 ) / 25.4 ) );
	free_img( img2 );// no need to do an explicit delete on this object as free_img clears mem
	
	if (*out_features)
	{
		BOOL fExists = FALSE;
		FILE* fTemp = NULL;
		int nRes = fopen_s( &fTemp, out_features, "r" );
		if( fTemp && ( nRes == 0 ) )
		{
			fExists = TRUE;
			fclose( fTemp );
		}
		nRes = fopen_s( &outf, out_features, "a" );
		if( !outf || ( nRes != 0 ) )
		{
			::OutputMessage( "Cannot write to feature output file", LOG_PROC );
			fError = TRUE;
			goto Cleanup;
		}
		else
		{
			writefeatures( segments, ngood, outf, out_echo, missing_string,
						   resolution, fExists, nTrialNum );
			fclose(outf);
			CString szCon = out_features;
			szCon = szCon.Left( szCon.GetLength() - 3 );
			szCon += _T("CON");
			::CopyFile( out_features, szCon, FALSE );
		}
	}
	
	if (*outpcx_segments)
	{
		// 28May09: Somesh: Preserve color table of img1
		/*if (colorfiles) set_img_clrbits(img1, 8);
		else set_img_clrbits(img1, 1);*/
		paintsegments(img1, segments, nsegments);
		// 22May09:Somesh: Make the background white (It looks better on a black background)
		//set_img_background(img1);
		write_PCX(img1, outpcx_segments);
	}

	if (*out_segments)
		writesegments(segments, ngood, out_segments);

Cleanup:
	if( img1 )
	{
		if( img1->data )
		{
			for( int i = 0; i < img1->h; free( img1->data[ i++ ] ) );
			free( img1->data );
		}
		free( img1->clrtable );
		free( img1 );
	}
	if( forks )
	{
		if( forks->x ) free( forks->x );
		if( forks->y ) free( forks->y );
		if( forks->z ) free( forks->z );
		free( forks );
	}
	if( segments )
	{
		for( int i = 0; i < nsegments; i++ )
		{
			if( segments[ i ].x ) free( segments[ i ].x );
			if( segments[ i ].y ) free( segments[ i ].y );
			if( segments[ i ].z ) free( segments[ i ].z );
		}
		free( segments );
	}

	return !fError;
}

void expand_fn(char *fn, char *base)
{
	char	tmp[MAX_PATH], *tp, *np = fn;
	
	if (strchr(fn, '*') == NULL) return;
	strcpy_s(tmp, MAX_PATH, fn);
	for (tp = tmp; *tp; tp++)
	{
		if (*tp == '*')
		{
			strcpy_s(np, MAX_PATH, base);
			while (*np) np++;
		}
		else {
			*np = *tp;
			np++;
		}
	}
	*np = '\0';
}

char *basename(char *fullname) 
{
	static char	buffer[_MAX_PATH];
	char		*base = NULL;

//	base = strrchr(fullname, '\\');
//	if (base == NULL) base = fullname;
//	else base++;
	base = fullname;
	CString szBase = base;
	int nPos = szBase.ReverseFind( '.' );
	if( nPos != -1 ) szBase = szBase.Left( nPos );
	strcpy_s(buffer, MAX_PATH, szBase);
	return(buffer);
}

}		// namespace

// SFINX.h - header file

#pragma once

namespace SFINX {

BOOL AFX_EXT_API RunSFINX( CString szDir, CString szPCX, int nTrialNum, int nSmoothIter = 0,
						   int nThreshold = 0, int nResolution = 0, double dMinSegLen = 0.05,
						   double dMissing = 0. );

};	// namespace

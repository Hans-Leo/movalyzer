#include "StdAfx.h"
#include "message.h"

namespace SFINX {

result_t        error_boundaries[] = { 1, -1000, -2000, -3000, -4000 };
FILE            *msg_stream = stderr;
void           (*msg_handler)(short, char*, ...) = MSGDefaultHandle;

void MSGSetHandler(void (*newhandler)(short, char*, ...))
{
    msg_handler = newhandler;
}

void MSGSetLevel(int boundary, result_t value)
{
    if ((boundary >= 0) && (boundary < sizeof(error_boundaries) / sizeof(error_boundaries[1]))) {
	error_boundaries[boundary] = value;
    }
}

result_t MSGLevel(int boundary)
{
    if ((boundary >= 0) && (boundary < sizeof(error_boundaries) / sizeof(error_boundaries[1]))) {
	return(error_boundaries[boundary]);
    }
    return(0);
}

void MSGSetStream(FILE *stream)
{
//    msg_handler = stream;
}

void MSGHandle(result_t condition, char *msg, ...)
{
    if (msg_handler == NULL) return;
//    msg_handler(condition, msg, ...);
}

void MSGDefaultHandle(result_t condition, char *msg, ...)
{
//    if ((msg != NULL) && (condition <= MSGLevel(INFORM))) {
//	fprintf(msg_stream, msg, ...);
//    }
//    if (condition <= MSGLevel(FATAL)) exit(condition);
}

void *myalloc(size_t size)
{
    void    *ptr;

    ptr = malloc(size);
    if (ptr == NULL) {
	MSGHandle(MSGLevel(FATAL), "Alloc for %ld bytes failed", size);
	return(ptr);
    }
    return( memset(ptr, 0, size) );
}

void *myrealloc(void *ptr, size_t size)
{
    if (ptr == NULL) return(myalloc(size));
    ptr = realloc(ptr, size);
    if (ptr == NULL)
		MSGHandle(MSGLevel(FATAL), "Realloc for %ld bytes failed", size);
    return( ptr );
}

}	// namespace

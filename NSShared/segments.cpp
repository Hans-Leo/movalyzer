#include "img.h"
#include "segments.h"
#include "message.h"
#include "../Common/DefFun.h"

#define PxRINT_GREY_HISTO

namespace SFINX {

void addpoint(struct segment *c, COORD x, COORD y, PIXEL z)
{
	if (c->n % 100 == 0)
	{
		c->x = (COORD*)realloc(c->x, (c->n + 100) * sizeof(COORD));
		c->y = (COORD*)realloc(c->y, (c->n + 100) * sizeof(COORD));
		c->z = (PIXEL*)realloc(c->z, (c->n + 100) * sizeof(PIXEL));
	}
	c->x[c->n] = x;
	c->y[c->n] = y;
	c->z[c->n] = z;
	c->n++;
}

int auto_threshold(struct image *img)
{
	// 20May09: Somesh: Otsu's method for thresholding
	// Otsu, N., "A Threshold Selection Method from Gray-Level Histograms,"
	// IEEE Transactions on Systems, Man, and Cybernetics, Vol. 9, No. 1, 1979, pp. 62-66
	COORD		x, y;
	long unsigned	*intens;
	//int		i, paper, ink, n, maxn = 20, thres = 0, maxintens;
	int ii, ithres;
	double dmutotal = 0., dmuK = 0., dvarK = 0., dvar, dvarmax = 0.;
	double dhist [256];
	long unsigned lnumofpix;

	intens = (long unsigned*)myalloc(256 * sizeof(long unsigned));

	for (y = img->h - 1; y >= 0; y--)
	{
		for (x = img->w - 1; x >= 0; x--)
		{
			++intens[GET(img, x, y)];
		}
	}
#ifdef PRINT_GREY_HISTO
	for (i = 0; i < 256; i++) {
		printf("%3d %5d\n", i, intens[i]);
	}
	fflush(stdout);
#endif

	lnumofpix = img->w * img->h;
	if (lnumofpix <= 0)
	return(0);

	// Normalize the histogram
	for (ii = 0; ii < 256; ii++ )
	{
		dhist[ii] = (double) intens[ii] / lnumofpix;
		// Cumulative moment of the image
		dmutotal += (ii * dhist [ii]);
	}

	// Search for threshold level that minimizes the total variance of levels
	for ( ii=0; ii<256 ; ii++)
	{
		dmuK += ii * dhist [ii];
		dvarK += dhist [ii];
		if (dvarK)
			dvar = SQR (dmutotal * dvarK - dmuK) / (dvarK - SQR (dvarK));

		if (dvar > dvarmax)
		{
			dvarmax = dvar;
			ithres = ii;
		}
	}

	// 21May2010: Somesh: If there are only 2 levels; 0(paper) and 1(ink),
	// then the thresholded calculated above will be 0. In that case, we make it 1.
	if (ithres == 0)
		ithres = 1;

	free(intens);
	return(ithres);

	//Previous algorithm
	/*for (i = 0, maxintens = 0; i < 256; i++) {
		if (intens[i] >= maxintens) {
			maxintens = intens[i];
			n = 0;
		}
		else {
			n++;
			if (n > maxn) break;
		}
	}
	paper = i - maxn;
	for (i = 255, maxintens = 0; i > paper; i--) {
		if (intens[i] >= maxintens) {
			maxintens = intens[i];
			n = 0;
		}
		else {
			n++;
			if (n > maxn) break;
		}
	}
	ink = i + maxn;
	free(intens);
	i = (paper + ink) / 2;

	return(i);*/
}

/* clearing edges: there are 4 sweep-functions, down, up, left and right.
First all the outer pixels are coloured, then sweeps are called in order
until none of them reports having changed any pixel. */

int sweep_dn(struct image *img, PIXEL clr)
{
	COORD		x, y, bottom = img->h - 1, right = img->w - 1;
	unsigned	n = 0;

	for (x = 1; x < right; x++) {
		for (y = 0; y < bottom; y++) {
			if (GET(img, x, y) == clr) {
				register PIXEL	c;
				while ((y < bottom) && (c = GET(img, x, ++y))) {
					if (c != clr) {
						PUT(img, x, y, clr);
						n++;
					}
				}
			}
		}
	}
	return(n);
}

static int sweep_up(struct image *img, PIXEL clr)
{
	COORD		x, y, bottom = img->h - 1, right = img->w - 1;
	unsigned	n = 0;

	for (x = 1; x < right; x++) {
		for (y = bottom; y > 0; y--) {
			if (GET(img, x, y) == clr) {
				register PIXEL	c;
				while ((y > 0) && (c = GET(img, x, --y))) {
					if (c != clr) {
						PUT(img, x, y, clr);
						n++;
					}
				}
			}
		}
	}
	return(n);
}

static int sweep_ri(struct image *img, PIXEL clr)
{
	COORD		x, y, bottom = img->h - 1, right = img->w - 1;
	unsigned	n = 0;

	for (y = 1; y < bottom; y++) {
		for (x = 0; x < right; x++) {
			if (GET(img, x, y) == clr) {
				register PIXEL	c;
				while ((x < right) && (c = GET(img, ++x, y))) {
					if (c != clr) {
						PUT(img, x, y, clr);
						n++;
					}
				}
			}
		}
	}
	return(n);
}

static int sweep_le(struct image *img, PIXEL clr)
{
	COORD		x, y, bottom = img->h - 1, right = img->w - 1;
	unsigned	n = 0;

	for (y = 1; y < bottom; y++) {
		for (x = right; x > 0; x--) {
			if (GET(img, x, y) == clr) {
				register PIXEL	c;
				while ((x > 0) && (c = GET(img, --x, y))) {
					if (c != clr) {
						PUT(img, x, y, clr);
						n++;
					}
				}
			}
		}
	}
	return(n);
}

void clearedges(struct image *img, PIXEL clr, int flood)
{
	COORD	x, y, bottom = img->h - 1, right = img->w - 1;

	for (y = 0; y <= bottom; y++) {
		PUT(img, 0, y, clr);
		PUT(img, right, y, clr);
	}
	for (x = 0; x <= right; x++) {
		PUT(img, x, 0, clr);
		PUT(img, x, bottom, clr);
	}
	if (!flood) return;
	while ( sweep_up(img, clr) | sweep_dn(img, clr) |
		sweep_le(img, clr) | sweep_ri(img, clr) ) ;
		/* is there any best order for this? */
}


/* connectedness() returns a pixel's number of connections with other
pixels. It is in fact counting the number of black-white changes when
going round the point. This is sometimes less than the NUMBER OF black
pixels round the point, because even in a 1-pixel wide skeleton, connections
may be more than 1 pixel wide. */

int connectedness(struct image *img, COORD x, COORD y)
{
	register unsigned char	n = 0, p0, p1;

	if (!ON(GET(img, x, y))) return(-1);
	p0 = ON(GET(img, --x, y));
	p1 = ON(GET(img, x, ++y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, ++x, y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, ++x, y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, x, --y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, x, --y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, --x, y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, --x, y));
	n += (p1 > p0);
	p0 = p1;
	p1 = ON(GET(img, x, ++y));
	n += (p1 > p0);
	return(n);
}
#define	MAXF	2000	/* size of temp array in markforks */

/* markforks() finds forks in the skeleton (points with more than two
connections to othter points) and marks them as `forkclr', including the 8
neigbouring points.  Regarding the forks as background points, the
skeleton is now interrupted and consists of linear, unconnected segments,
which can be found by connected component analysis. */

struct segment *findforks(struct image *img)
{
	segment	*forks;
	COORD	x, y;
	int		nf = 0;

	forks = (segment*)myalloc(sizeof(struct segment));
	for (y = img->h - 2; y > 0; y--)
	{
		for (x = img->w - 2; x > 0; x--)
		{
			if (!ON(GET(img, x, y))) continue;
			if (connectedness(img, x, y) > 2)
			{
				addpoint(forks, x, y, (PIXEL)0);
				nf++;
			}
		}
	}
	return(forks);
}

int markforks(struct image *img, struct segment *forks,
	PIXEL innerclr, PIXEL outerclr)
{
	COORD		x, y;
	int		i;

	for (i = 0; i < forks->n; i++) {
		x = forks->x[i];
		y = forks->y[i];

		// 28May09: Somesh: Make hollow squares to mark forks
		x -= 2;
		PUT(img, x, --y, innerclr);
		PUT(img, x, --y, innerclr);
		PUT(img, ++x, y, innerclr);
		PUT(img, ++x, y, innerclr);
		PUT(img, ++x, y, innerclr);
		PUT(img, ++x, y, innerclr);
		PUT(img, x, ++y, innerclr);
		PUT(img, x, ++y, innerclr);
		PUT(img, x, ++y, innerclr);
		PUT(img, x, ++y, innerclr);
		PUT(img, --x, y, innerclr);
		PUT(img, --x, y, innerclr);
		PUT(img, --x, y, innerclr);
		PUT(img, --x, y, innerclr);
		PUT(img, x, --y, innerclr);

		/*PUT(img, x, y, innerclr);
		PUT(img, --x, y, innerclr);
		PUT(img, x, ++y, innerclr);
		PUT(img, ++x, y, innerclr);
		PUT(img, ++x, y, innerclr);
		PUT(img, x, --y, innerclr);
		PUT(img, x, --y, innerclr);
		PUT(img, --x, y, innerclr);
		PUT(img, --x, y, innerclr);

		PUT(img, --x, y, outerclr);
		PUT(img, x, ++y, outerclr);
		PUT(img, x, ++y, outerclr);
		PUT(img, x, ++y, outerclr);
		PUT(img, ++x, y, outerclr);
		PUT(img, ++x, y, outerclr);
		PUT(img, ++x, y, outerclr);
		PUT(img, ++x, y, outerclr);
		PUT(img, x, --y, outerclr);
		PUT(img, x, --y, outerclr);
		PUT(img, x, --y, outerclr);
		PUT(img, x, --y, outerclr);
		PUT(img, --x, y, outerclr);
		PUT(img, --x, y, outerclr);
		PUT(img, --x, y, outerclr);
		PUT(img, --x, y, outerclr);
		PUT(img, x, ++y, outerclr);*/
	}

	CString szMsg;
	szMsg.Format( _T("%d forks found"), i );
	::OutputMessage( szMsg, LOG_PROC );

	return(i);
}

/* Next functoins are concerned with finding the connected segments in the
image. */

static void floodsegment(struct image *img, struct segment *cc, COORD x, COORD y)
{
	for ( ; ; )
	{
		AND_IMG(img, x, y, 0x7f);
		/* clear hi bit, this is equivalent to subtracting 128 */
		addpoint(cc, x, y, GET(img, x, y));
        	if ON(GET(img, --x, y)) continue;
        	if ON(GET(img, x, ++y)) continue;
        	if ON(GET(img, ++x, y)) continue;
        	if ON(GET(img, ++x, y)) continue;
        	if ON(GET(img, x, --y)) continue;
        	if ON(GET(img, x, --y)) continue;
        	if ON(GET(img, --x, y)) continue;
        	if ON(GET(img, --x, y)) continue;
		return;
	}
}

/* findsegments() is a connected component routine tuned to find 1-pixel
wide skeleton segments, with points in connecting sequence, highest point
(smallest y-coordinate) first.  There are 2 scans: 1st for all open
segments (begin != end); 2nd for closed segments.  These are here called
type 1 and 2, respectively. Type 2 always have begin/end at the top and go
anticlockwise (like an 'o' is usually written). In skeletons from scanned
images, type 2 (smooth closed segments) are quite rare. Points are placed
in arrays associated with each segment. Returns the number of segments
found. */

#define	SEGMBS	100	/* segment array allocation block size */

int findsegments(struct image *img, struct segment **segmp)
{
	COORD	x, y, bottom = img->h - 1, right = img->w - 1;
	int		t, n = 0, n1 = 0, nalloc = SEGMBS;
	char	*segtype[] = { "", "open", "closed" };
	segment	*segm = NULL;
	CString	szMsg;

	int nSize = sizeof( segment );
	segm = (segment*)myalloc(nalloc * nSize);
	for (t = 1; t <= 2; t++)
	{
		for (y = 1; y < bottom; y++)
		{
			for (x = 1; x < right; x++)
			{
				if (connectedness(img, x, y) == t)
				{
					if (n >= nalloc)
					{
						segm = (segment*)realloc(segm, (nalloc + SEGMBS) * sizeof(struct segment));
						memset(&(segm[nalloc]), 0, SEGMBS * sizeof(struct segment));
						nalloc += SEGMBS;
					}
					floodsegment(img, &(segm[n]), x, y);
					segm[n].type = (char)t;
					segm[n].xend = &(segm[n].x[segm[n].n - 1]);
					segm[n].yend = &(segm[n].y[segm[n].n - 1]);
					n++;
				}
			}
		}

		szMsg.Format( _T("%d %s segments"), n - n1, segtype[ t ] );
		::OutputMessage( szMsg, LOG_PROC );

		n1 = n;
	}

	*segmp = (segment*)realloc(segm, n * sizeof(struct segment));
	return(n);
}

/* paintsegments() paints the different segments in the image. Segments
considered noise are made grey. Mainly for verification & demo purposes. */

void paintsegments(struct image *img, struct segment *c, int nc)
{
	struct segment	*cc = NULL;
	int				i = 0, p = 0;
	COORD			*x = NULL, *y = NULL;
	PIXEL			clr = 0;

	for( i = 0, cc = c; i < nc; i++, cc++ )
	{
		x = cc->x;
		y = cc->y;
// 13Mar09: GMB: Eliminating yellow coloring...only short segments are black
//		if( SEG_NOISE( cc ) ) clr = 0;
//		else clr = 220;
		if( SEG_NOISE( cc ) )
		{
			for( p = 0; p < cc->n; p++, x++, y++ )
			{
				PUT( img, *x, *y, clr );
			}
		}
	}
}

void writesegments( segment *segm, int ns, char *fn )
{
	int		i = 0, j = 0;
	COORD	*x = NULL, *y = NULL, *xLast = NULL, *yLast = NULL;
	PIXEL	*z = NULL;
	segment	*s = NULL;
	FILE	*f = NULL;
	BOOL	fWroteFirst = FALSE;

	if( !segm || !fn ) return;

	f = fopen( fn, "w" );
	if( !f ) return;

	for( i = 0, s = segm; i < ns; i++, s++ )
	{
		if( SEG_NOISE( s ) ) continue;

		x = s->x;
		y = s->y;
		z = s->z;

//		fprintf(f, "segment %d\n", i);
		if( !fWroteFirst && xLast && yLast )
		{
			fprintf( f, "%d %d 0\n", *xLast, -(*yLast) );
			fWroteFirst = TRUE;
		}

		for( j = 0; j < s->n; j++, x++, y++, z++ )
		{
			if( fWroteFirst )
			{
				fprintf( f, "%d %d 0\n", *x, -(*y) );
				fWroteFirst = FALSE;
			}
			fprintf( f, "%d %d %d\n", *x, -(*y), *z );
			xLast = x;
			yLast = y;
		}
	}

	fclose( f );
}

}	// namespace

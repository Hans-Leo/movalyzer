/****************************************
Gensig - v1.0 - 14 December 1993 - hlt
Gensig - v2.0 - 01 October 2002 - gmb
*****************************************/

/*
PURPOSE
  (1) Generate sinus test signal

ARGUMENTS [optional]

UPDATE:
  9Jun95: All arguments dynamic; Help if no arguments.
  23Aug95: xsize, ysize; small bugs solved; all parameters defaulted nonzero.
  30Jun97: Minimum jerk wave
   1Jun01: hlt; streamlined, phase added, min. jerk wave corrected.
  28jun01: hlt: gettime in .01s instead of time in s

************************************************************************/

#include "StdAfx.h"
#include "GenSig.h"
#include <sys/timeb.h>
#include <math.h>
#include <dos.h> // for gettime()

/************************************************************************/

#define HUNDREDEIGHTYDEGREES	180.
#define HARMONIC				1
#define MINIMUMJERK				2

/************************************************************************/
/************************************************************************/

GenSig::GenSig()
{
	// DEVICE SETTINGS AND PROPERTIES
	// Digitizer resolution as set in the Expriment Settings >Input Device
	m_cm = 0.001;		// for Wacom PL-100V at Motor Control Lab
	// Digitizer intersample period or 1/sampling rate as set in the Expriment Settings >Input Device
	m_sec = 0.01;
	// Pressure during the entire trial. Choose larger than the minprs set in the device settings
	m_prsmin = 20.;
	// Noise amplitude of x and y in cm
	m_noise = 0.02;
	// Option: Rounding raw data to integer digitizer data versus leaving them floating point
	m_integer = TRUE;
	// TIMING OF THE TOTAL MOVEMENT PATTERN IN THE X,Y PLANE
	// Start time in s of the movement from the start of the trial
	m_startt = 0.1;
	// Duration in s of one stroke
	m_deltat = 0.2;
	// Number of strokes. Can be decimal (eg 2.4 is one loop plus a small upstroke) -- only imp. for pattern=1
	m_nstroke = 8.4;
	// Time in s of after the movement has stopped
	m_trailtime = 0.1;
	// X-MOVEMENT COMPONENT OF THE PATTERN
	// Start x-position in cm of the first stroke
	m_startx = 0.3;
	// X-size of a stroke in cm (if xspeed=0)
	m_deltax = 1.;
	// Phase (in deg) of the x movement at the start of the movement
	// The difference of x and y phases determine the shape of the stroeks
	// x=y=0 = straight, diagonal line
	// x=90 deg and y=0 = counterclockwise circle
	// x=-90 deg and y=0 = clockwise circle
	// One stroke corresponds to 180 degrees
	// Only an option for Pattern=1
	m_phasex = 60.;
	// Constant x-velocity in cm/s added during the movement
	// to produce a baseline in a sequence of loops
	m_xspeed = 2.;
	// Y-MOVEMENT COMPONENT OF THE PATTERN
	// Start y-position in cm of the first stroke
	m_starty = 0.3;
	// Y-size of a stroke in cm
	m_deltay = 1.;
	// Phase of the y movement at the start of the movement
	// in rad or degrees
	// Only implemented for pattern=1
	m_phasey = 0.;
	// STROKE VELOCITY PROFILE
	// 1=harmonic, 2=minimum jerk
	m_pattern = HARMONIC;
	// 07Nov08: Somesh: Submovement
	// Duration to the delay (time from start of stroke to delay)
	// Duration of the delay (total delay time) 
	// Y size and phaze during delay 
	m_nSubMvmtModel = GEN_DELAY;
	m_subduration = 0.8 * m_deltat;
	m_subdelay = 0.05;
}

GenSig::~GenSig()
{
}

BOOL GenSig::Generate( CString szFileOut )
{
	double rstartt = 0., rdeltat = 0.;
	double rstartx = 0., rdeltax = 0.;
	double rstarty = 0., rdeltay = 0.;
	double positionx = 0., positiony = 0.;
	double rtrailtime = 0.;
	double rnoise = 0.;
	double rphasex = 0., rphasey = 0.;
	double xatt0 = 0., yatt0 = 0.;
	double x = 0., y = 0.;
	double xend = 0., yend = 0.;
	double rxspeed = 0.;
	int idelay = 0, istroke = 0, idelaybuf = 0;
	int isample = 0, jsample = 0;
	CStdioFile f;

	// 07Nov08: Somesh: Submovement generation
	double subduration = 0.0;
	double startDelay = 0.0;
	double subdelay = 0.0;
	double tout = 0.0;

	/************************************************/
	/*

	Test Signal Generator

	Rest period before start (s)  ____
	Rest period after start (s)   ____
	Stroke duration (s)           ____
	#Strokes                      ____

					   X component     Y component
	Start position (cm)  ____            ____
	Stroke size (cm)     ____            ____
	Start phase (deg)    ____            ____
	Xspeed (cm/s)        ____

	Uniform noise max error (cm)  ____
	Quantization noise            No/Yes
	Velocity profile              harmonic/minimum jerk

												  */
	/************************************************/

	// output file
	if( !f.Open( szFileOut, CFile::modeWrite | CFile::modeCreate ) )
	{
		AfxMessageBox( _T("Unable to open output file for signal generation.") );
		return FALSE;
	}

	// Convert from cm to digitizer units
	if( m_cm != 0. )
	{
		rnoise = m_noise / m_cm;
		rstartx = m_startx / m_cm;
		rdeltax = m_deltax / m_cm;
		rxspeed = m_xspeed * m_sec / m_cm;
		rstarty = m_starty / m_cm;
		rdeltay = m_deltay / m_cm;
	}
	else
	{
		rnoise = 0.;
		rstartx = 0.;
		rdeltax = 0.;
		rxspeed = 0.;
		rstarty = 0.;
		rdeltay = 0.;
	}

	// Convert from s to inter-sample periods
	if( m_sec != 0. )
	{
		rstartt = m_startt / m_sec;
		rdeltat = m_deltat / m_sec;
		rtrailtime = m_trailtime / m_sec;
		// 07Nov08: Somesh
		subduration = m_subduration / m_sec;
		subdelay = m_subdelay / m_sec;
	}
	else
	{
		rstartt = 0.;
		rdeltat = 0.;
		rtrailtime = 0.;
	}

	// Convert to from deg to rad
	rphasex = m_phasex * PI / HUNDREDEIGHTYDEGREES;
	rphasey = m_phasey * PI / HUNDREDEIGHTYDEGREES;


	/* initialize random generator for the noise */
	/* This has to be done in fact as follows */
	/* The first time you start with a proper seed value */
	/* If it were possible to retrieve the latest seed before program exit */
	/* Then that seed value should be stored and retrieved the next time this program is run */
	/* Using time is dangerous as time might be rounded off to the same on 2 consecutive runs */
	struct _timeb tstruct;
	_ftime64_s( &tstruct );
	srand( tstruct.millitm );

	/***********/
	/* latency */
	/***********/
	for( isample = 0; isample < rstartt; isample++ )
	{
		x = rstartx;
		y = rstarty;
		AddNoise( &x, &y, rnoise );
		Round( &x, &y, m_integer );
		/* Just one step lower pressure */
		// Somesh: 11Mar09: Force pressure to 99 for now
		//Output( x, y, m_prsmin, &f );
		Output( x, y, 99, &f );
	}

	/***********/
	/* pattern */
	/***********/
	if( m_pattern == HARMONIC )
	{
		isample = 0;
		// Raised cosine
		xatt0 = 0.5 * rdeltax * cos( rphasex + PI * isample / rdeltat );
		yatt0 = 0.5 * rdeltay * cos( rphasey + PI * isample / rdeltat );
		
		double rdeltatotal = rdeltat + subdelay;

		// Submovements ~ Implementation 1
		// 08Nov08: Somesh: Refer forum for working of the following algorithm
		if( ( m_nSubMvmtModel == GEN_NONE ) || ( m_nSubMvmtModel == GEN_DELAY ) )
		{
			for ( isample = 0; isample <= m_nstroke * rdeltatotal; isample++ )
			{
				idelay = (int) ((isample - subduration - 0.5 * subdelay)/rdeltatotal + 1);
				startDelay = idelay * rdeltatotal + subduration - 0.5 * subdelay;
				if (isample >= startDelay && isample < startDelay + subdelay)
					tout = startDelay - idelay * subdelay;
				else
					tout = isample - idelay * subdelay;
					
				x = rstartx + xatt0 - 0.5 * rdeltax * cos( rphasex + PI * tout / rdeltat) + tout * rxspeed;
				y = rstarty + yatt0 - 0.5 * rdeltay * cos( rphasey + PI * tout / rdeltat );	
			
				// Add white noise, quantization noise, and output 
				AddNoise( &x, &y, rnoise );
				Round( &x, &y, m_integer );
				// Somesh: 11Mar09: Force pressure to 99 for now
				//Output( x, y, m_prsmin, &f );
				Output( x, y, 99, &f );
			}
		}		
		// Submovements ~ Implementation 2
		// 08Nov08: Somesh: Refer forum for working of the following algorithm
		else if ( m_nSubMvmtModel == GEN_WARP )
		{
			subdelay = subdelay * m_sec;
			for ( isample = 0; isample <= m_nstroke * rdeltat; isample++ )
			{
				tout = isample - (rdeltat/(2.* PI)) * subdelay * sin(2 * PI * (isample - subduration) / rdeltat);
			
				x = rstartx + xatt0 - 0.5 * rdeltax * cos( rphasex + PI * tout / rdeltat) + tout * rxspeed;
				y = rstarty + yatt0 - 0.5 * rdeltay * cos( rphasey + PI * tout / rdeltat);	
			
				// Add white noise, quantization noise, and output 
				AddNoise( &x, &y, rnoise );
				Round( &x, &y, m_integer );
				// Somesh: 11Mar09: Force pressure to 99 for now
				//Output( x, y, m_prsmin, &f );
				Output( x, y, 99, &f );
			}
		}
	}
	else if( m_pattern == MINIMUMJERK )
	{
		/* BUG: Multiple strokes and phases not yet in order */
		for( istroke = 0; istroke < m_nstroke; istroke++ )
		{
			/* Accelerative phase */
			for( isample = 0; isample < 0.5 * rdeltat - 0.5; isample++ )
			{
				/* Minimum jerk movement profile  */
				positionx =  6. * TOTHEFIFTH  (isample / rdeltat)
							- 15. * TOTHEFOURTH (isample / rdeltat)
							+ 10. * TOTHETHIRD  (isample / rdeltat);

				positiony =  6. * TOTHEFIFTH  (isample / rdeltat)
							- 15. * TOTHEFOURTH (isample / rdeltat)
							+ 10. * TOTHETHIRD  (isample / rdeltat);

				x = rstartx + rdeltax * positionx;
				y = rstarty + rdeltay * positiony;
				AddNoise( &x, &y, rnoise );
				Round( &x, &y, m_integer );
				// Somesh: 11Mar09: Force pressure to 99 for now
				//Output (x, y, m_prsmin, &f);
				Output( x, y, 99, &f );
			}

			/* Decellerative phase  */
			for( isample = 0; isample < 0.5 * rdeltat+0.5; isample++)
			{
				/* Minimum jerk movement profile */
				jsample = (int)( 0.5 * rdeltat + 0.5 - isample );
				positionx =  6. * TOTHEFIFTH  (jsample / rdeltat)
							- 15. * TOTHEFOURTH (jsample / rdeltat)
							+ 10. * TOTHETHIRD  (jsample / rdeltat);

				positiony =  6. * TOTHEFIFTH  (jsample / rdeltat)
							- 15. * TOTHEFOURTH (jsample / rdeltat)
							+ 10. * TOTHETHIRD  (jsample / rdeltat);
				x = rstartx + rdeltax * ( 1. - positionx );
				y = rstarty + rdeltay * ( 1. - positiony );
				AddNoise( &x, &y, rnoise );
				Round( &x, &y, m_integer );
				// Somesh: 11Mar09: Force pressure to 99 for now
				//Output( x, y, m_prsmin, &f );
				Output( x, y, 99, &f );
			}
		}
	}

	xend = x;
	yend = y;

	/*****************/
	/* trailing time */
	/*****************/
	for( isample = 0; isample < rtrailtime; isample++ )
	{
		x = xend;
		y = yend;
		AddNoise( &x, &y, rnoise );
		Round( &x, &y, m_integer );
		// Somesh: 11Mar09: Force pressure to 99 for now
		//Output( x, y, m_prsmin, &f );
		Output( x, y, 99, &f );
	}

	return TRUE;
}

void GenSig::AddNoise( double* x, double* y, double rnoise )
{
	if( !x || !y ) return;

	if( rnoise > 0. )
	{
		/* Generates values from 0 - 2000 */
		/* The noise has a uniform distribution, not a normal distribution */
		*x += (rand()%2000 - 1000) * rnoise * 0.001;
		*y += (rand()%2000 - 1000) * rnoise * 0.001;
	}
}

void GenSig::Round( double* x, double* y, BOOL intgr )
{
	if( !x || !y ) return;

	if( intgr )
	{
		*x = (int)*x;
		*y = (int)*y;
	}
}

void GenSig::Output( double x, double y, double z, CStdioFile* f )
{
	if( !f ) return;

	CString szOut;
	szOut.Format( _T("%.7lg %.7lg %.7lg\n"), x, y, z );
	f->WriteString( szOut );
}
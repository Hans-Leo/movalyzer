/*	spta.c
*	safe point thinning algorithm
*	Naccache NJ & Shingal R, SPTA: A proposed algorithm for thinning
*	binary patterns, IEEE Trans Syst Man Cybern SMC-14 (3), 409-418
*	(1984).
*/

/* Adaptions Eric Helsper October 1995:

Uses unsigned pixel, so no negative values possible (unlike N&S).
(t is the threshold arg to spta).
Pixel meanings:
  0      background
  0..t   flagged for deletion
128      foreground
t..256   safe point (not to be deleted).

Important: The input image should have backgr pixels = 0, forground = t.
On exit p > t are skeleton pix, local thickn = p - t.
(if t == MAXPIXEL the skel pixels are MAXPIXEL and there is no thickness info.)
p < t non-skel pix.

neighbourhood coding
	5 6 7
	4 0 8
	3 2 1
*/

#include "StdAfx.h"
#include "img.h"
#include "message.h"
#include "..\Common\DefFun.h"

namespace SFINX {

	/* proto */
	PIXEL		*neighbourhood(image *,int ,int );
	static int	safe(int ,PIXEL *);
	static int	spta_scan(image *,int ,int );
	
	/* global */	
	PIXEL	glb_safe, glb_iter, glb_thresh, glb_unsafe;
	
PIXEL *neighbourhood(image *img, int x, int y)
{
	static PIXEL	p[9];

	p[0] = GET(img, x, y);
	p[1] = GET(img, ++x, ++y);
	p[2] = GET(img, --x, y);
	p[3] = GET(img, --x, y);
	p[4] = GET(img, x, --y);
	p[5] = GET(img, x, --y);
	p[6] = GET(img, ++x, y);
	p[7] = GET(img, ++x, y);
	p[8] = GET(img, x, ++y);
	return(p);
}

#define	OFF(p)		((p) < glb_thresh)
#define	ON(p)		((p) >= glb_thresh)
#define	n_ON(p)		ON(nbh[p])
#define	n_OFF(p)	OFF(nbh[p])

static int safe(int s, PIXEL nbh[])
{
	switch (s) {
	case 8:
		if (!n_ON(4)) return(1);
		if n_ON(6)
			if n_ON(2) return(0);
			else return n_ON(1);
		else if n_ON(7) return(1);
			else if n_ON(2) return(0);
				else if n_ON(1) return(1);
					else if n_ON(5) return(0);
						else return (!n_ON(3));
	case 2:
		if (!n_ON(6)) return(1);
		if n_ON(8)
			if n_ON(4) return(0);
			else return n_ON(3);
		else if n_ON(1) return(1);
			else if n_ON(4) return(0);
				else if n_ON(3) return(1);
					else if n_ON(7) return(0);
						else return (!n_ON(5));
	case 4:
		if (!n_ON(8)) return(1);
		if n_ON(2)
			if n_ON(6) return(0);
			else return n_ON(5);
		else if n_ON(3) return(1);
			else if n_ON(6) return(0);
				else if n_ON(5) return(1);
					else if n_ON(1) return(0);
						else return (!n_ON(7));
	case 6:
		if (!n_ON(2)) return(1);
		if n_ON(4)
			if n_ON(8) return(0);
			else return n_ON(7);
		else if n_ON(5) return(1);
			else if n_ON(8) return(0);
				else if n_ON(7) return(1);
					else if n_ON(3) return(0);
						else return (!n_ON(1));
	}
	return(-1);
}


static int spta_scan(image *img, int j, int k)
{
	int	x, y, w = img->w - 1, h = img->h - 1, flagd = 0;
	PIXEL	*nbh, *p;

	for (y = 1; y < h; y++) {
		for (x = 1; x < w; x++) {
			p = PTR(img, x, y);
			if OFF(*p) continue;
			nbh = neighbourhood(img, x, y);
			if ((nbh[j] < glb_iter) || (nbh[k] < glb_iter)) {
				if ((safe(j, nbh)) && (safe(k, nbh))) {
					/* label as safe */
					if (*p == glb_unsafe) *p = glb_safe;
				}
				else {
					/* flag for deletion */
					*p = glb_iter;
					flagd++;
				}
			}
		}
	}
#ifdef	DEBUG
	fprintf(stderr, "SPTA pass %d [%d, %d]: %d deleted\n", glb_iter, j, k, flagd);
#endif
	return(flagd);
}

int spta(image *img, PIXEL threshold)
{
	int	flagd48 = 1, flagd26 = 1;

	::OutputMessage( "Thinning", LOG_PROC );
//	message1(INFORM, "Thinning");

	glb_thresh = threshold;
	if (glb_thresh == MAXPIXEL) glb_unsafe = 0;
	else glb_unsafe = glb_thresh;
	for (glb_iter = 1, glb_safe = threshold; 
		glb_iter < threshold; 
		glb_iter++) {
		
		if (glb_safe < MAXPIXEL) glb_safe++;
		if (flagd48 == 0) flagd48 = -1;
		else flagd48 = spta_scan(img, 4, 8);
		if (flagd26 == 0) flagd26 = -1;
		else flagd26 = spta_scan(img, 2, 6);
		if ((flagd48 <= 0) && (flagd26 <= 0)) break;
	}
	return(glb_iter);	/* n of iterations for thinning */
}

}

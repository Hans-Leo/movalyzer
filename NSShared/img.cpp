#include "img.h"
#include "message.h"
#include "../Common/DefFun.h"

namespace SFINX {

void *myrecalloc(void *ptr, size_t size, size_t oldn, size_t newn)
{
    oldn *= size;
    newn *= size;
    if (ptr == NULL) return(myalloc(newn));
    ptr = realloc(ptr, newn);
    if (ptr == NULL) {
//	error1(FATAL, "Realloc failed");
    }
    if (newn > oldn) {
	memset(&(((char *)ptr)[oldn]), 0, newn - oldn);
    }
    return( ptr );
}

/* returns an ideal number of bytes to be allocated per line. The number
is always even, and is always at least 8 bytes more than the image data to
allow the prefix added in vgaimg.c */

long int standard_bytesperline(int w, int bpp)
{
    long int    bpl = w * bpp;

    bpl--;
    bpl >>= 3;
    bpl++;
    bpl += (bpl & 1) + 8;    /* add 1 if uneven */
    return(bpl);
}

image *alloc_img(int w, int h, int bpp)
{
    register int    i;
    image			*img = NULL;

	CString szMsg;
	szMsg.Format( "Allocating image %d * %d", w, h );
	::OutputMessage( szMsg, LOG_PROC );

    img = (image*)myalloc(sizeof(image));
    img->data = (PIXEL**)myalloc(h * sizeof(PIXEL *));
    img->nallocl = standard_bytesperline(w, bpp);
    for( i = 0; i < h; i++ )
	{
		img->data[ i ] = (PIXEL*)myalloc(img->nallocl * sizeof(PIXEL));
    }
	img->w = w;
	img->h = h;
	img->bipp = bpp;
	img->clrbits = bpp;
//	img->clrtable = standard_clr_table(clrbits);
	return img;
}

/*
 * convert packed pixel format into 1 pixel per byte
 */
void unpack_img_8(image *img)
{
    int    y, i;
    PIXEL    *sp, *dp, *dest;

    if (img->bipp == 8) return;    /* nothing to be done */

    img->nallocl = standard_bytesperline(img->w, 8);

    for (y = 0; y < img->h; y++) {
	dest = (PIXEL*)myalloc(img->nallocl * sizeof(PIXEL));
	dp = dest;
	sp = img->data[y];

	switch (img->bipp) {

	    case 4:
	    for (i = img->w / 2; i >= 0; i--) {
		*dp++ = (*sp >> 4) & 0x0f;
		*dp++ = (*sp     ) & 0x0f;
		sp++;
	    }
	    break;
	    case 2:
	    for (i = img->w / 4; i >= 0; i--) {
		*dp++ = (*sp >> 6) & 0x03;
		*dp++ = (*sp >> 4) & 0x03;
		*dp++ = (*sp >> 2) & 0x03;
		*dp++ = (*sp     ) & 0x03;
		sp++;
	    }
	    break;
	    case 1:
	    for (i = img->w / 8; i >= 0; i--) {
		*dp++ = ((*sp & 0x80) != 0);
		*dp++ = ((*sp & 0x40) != 0);
		*dp++ = ((*sp & 0x20) != 0);
		*dp++ = ((*sp & 0x10) != 0);
		*dp++ = ((*sp & 0x08) != 0);
		*dp++ = ((*sp & 0x04) != 0);
		*dp++ = ((*sp & 0x02) != 0);
		*dp++ = ((*sp & 0x01) != 0);
		sp++;
	    }
	    break;
	}
	free(img->data[y]);
	img->data[y] = dest;
    }
    img->bipp = 8;
}

void unpack_img_4(image *img)
{
    int    y, i;
    PIXEL    *sp, *dp, *dest;

    if (img->bipp >= 4) return;

    img->nallocl = standard_bytesperline(img->w, 4);

    for (y = 0; y < img->h; y++) {
	dest = (PIXEL*)myalloc(img->nallocl * sizeof(PIXEL));
	dp = dest;
	sp = img->data[y];

	switch (img->bipp) {

	    case 2:
	    for (i = img->w / 4; i >= 0; i--) {
		*dp   =  (*sp >> 2) & 0x0c;
		*dp   |= (*sp     ) & 0x03;
		dp++;
		*dp   =  (*sp >> 2) & 0x0c;
		*dp   |= (*sp     ) & 0x03;
		dp++;
		sp++;
	    }
	    break;
	    case 1:
	    for (i = img->w / 8; i >= 0; i--) {
		*dp    = ((*sp & 0x80) != 0) << 4;
		*dp   |= ((*sp & 0x40) != 0);
		dp++;
		*dp    = ((*sp & 0x20) != 0) << 4;
		*dp   |= ((*sp & 0x10) != 0);
		dp++;
		*dp    = ((*sp & 0x08) != 0) << 4;
		*dp   |= ((*sp & 0x04) != 0);
		dp++;
		*dp    = ((*sp & 0x02) != 0) << 4;
		*dp   |= ((*sp & 0x01) != 0);
		dp++;
		sp++;
	    }
	    break;
	}
	free(img->data[y]);
	img->data[y] = dest;
    }
    img->bipp = 4;
}


int clear_img(image *img)
{
    register int    i;

    for (i = 0; i < img->h; i++) {
	memset(img->data[i], 0, img->nallocl);
    }
    return(0);
}

void free_img(image *img)
{
    register int    i;

	CString szMsg;
	szMsg.Format( "Freeing image %d * %d", img->w, img->h );
	::OutputMessage( szMsg, LOG_PROC );

    for (i = 0; i < img->h; i++) {
	free(img->data[i]);
    }
    free(img->data);
    if (img->clrtable != NULL) free(img->clrtable);
    free(img);
    img = NULL;
}

image *read_ASC(char *fn)
{
    int	w, h, x, y, s;
    image    *img;
    FILE	*f;

    if (strcmp(fn, "-") == 0) f = stdin;
    else f = fopen(fn, "r");
    if (f == NULL) {
//	error2(MAJOR, "cannot read asc image file", fn);
	return(NULL);
    }
    s = fscanf_s(f, "%d %d", &w, &h);
    if (s < 2) {
//	error2(MAJOR, "no data in asc image file", fn);
	fclose(f);
	return(NULL);
    }

	CString szMsg;
	szMsg.Format( "Reading '%s' (%d * %d)", fn, w, h );
	::OutputMessage( szMsg, LOG_PROC );

    img = alloc_img(w, h, 1);    /* 1 = bitsperpixel */
    for ( ; ; ) {
	s = fscanf_s(f, "%d %d", &x, &y);
	if (s < 2) break;
	if ((x < 0) || (y < 0) || (x >= w) || (y >= h))
	    continue;    /* point outside image */
	PUT(img, x, y, 255);
    }
    if (f != stdin) fclose(f);
    return(img);
}
#define INTERNAL 0
int write_ASC(char *fn, image *img)
{
    int    x, y;
    FILE    *f;

    if (img == NULL) {
//	error2(MAJOR | INTERNAL, "trying to write NULL image", fn);
	return(MAJOR | INTERNAL);
    }
    if (strcmp(fn, "-") == 0) f = stdout;
    else f = fopen(fn, "w");
    if (f == NULL) {
//	error2(MAJOR, "cannot write asc image file", fn);
	return(MAJOR);
    }

	CString szMsg;
	szMsg.Format( "Writing '%s' (%d * %d)", fn, img->w, img->h );
	::OutputMessage( szMsg, LOG_PROC );
    fprintf(f, "%d %d\n", img->w, img->h);
    for (x = 0; x < img->w; x++) {
	for (y = 0; y < img->h; y++) {
	    if (GET(img, x, y)) fprintf(f, "%d %d\n", x, y);
	}
    }
    if (f != stdout) fclose(f);
    return(0);
}

void line(image *img, int x1, int y1, int x2, int y2, PIXEL clr, int thick)
{
    int    dx, dy, dxa, dya;
    double    x, y, fx, fy, xi, yi, xi1, yi1, xi2, yi2, t2;

    t2 = (double)thick / 2;
    dx = x2 - x1;
    dy = y2 - y1;
    dxa = dx;
    dya = dy;
    if (dxa < 0) dxa = -dxa;
    if (dya < 0) dya = -dya;
    x = x1;
    y = y1;
    if (dxa > dya) {
	fy = (double)dy / dxa;
	dx /= dxa;
	for ( ; ; ) {
	    xi1 = x - t2 + 0.5;
	    xi2 = x + t2 + 0.5;
	    yi1 = y - t2 + 0.5;
	    yi2 = y + t2 + 0.5;
	    for (xi = xi1; xi < xi2; xi++) {
		for (yi = yi1; yi < yi2; yi++) {
		    PUT(img, (int)xi, (int)yi, clr);
		}
	    }
	    if ((int)x == x2) return;
	    y += fy;
	    x += dx;
	}
    }
    else {
	if (dya) {
	    fx = (double)dx / dya;
	    dy /= dya;
	}
	for ( ; ; ) {
	    xi1 = x - t2 + 0.5;
	    xi2 = x + t2 + 0.5;
	    yi1 = y - t2 + 0.5;
	    yi2 = y + t2 + 0.5;
	    for (xi = xi1; xi < xi2; xi++) {
		for (yi = yi1; yi < yi2; yi++) {
		    PUT(img, (int)xi, (int)yi, clr);
		}
	    }
	    if ((int)y == y2) return;
	    x += fx;
	    y += dy;
	}
    }
}

/* smooth b/w image; input pixels should be p >= 128 for black, p < 128
for white.  Count n of black pixels in 8-neigbourhood of p, including p
itself. If more than 4 are black, we set the central point to black, else
to white. We use the low nibble for counting, the highest bit holds the
black/white value */

void smooth(image *img, int black, int niter)
{
    COORD    x, y, bottom = (COORD)(img->h - 1), right = (COORD)(img->w - 1);
    int    i;

	::OutputMessage( _T("Smoothing"), LOG_PROC );
//    message1(INFORM, "Smoothing");

    for (i = 0; i < niter; i++) {
	for (x = 1; x < right; x++) {
	    for (y = 1; y < bottom; y++) {
		register PIXEL    p =
			(GET(img, x, y) & 128) +
			(GET(img, x, y) >> 7) +
			(GET(img, x, --y) >> 7) +
			(GET(img, ++x, y) >> 7) +
			(GET(img, x, ++y) >> 7) +
			(GET(img, x, ++y) >> 7) +
			(GET(img, --x, y) >> 7) +
			(GET(img, --x, y) >> 7) +
			(GET(img, x, --y) >> 7) +
			(GET(img, x, --y) >> 7);
		x++;
		y++;
		PUT(img, x, y, p);
	    }
	}
	for (x = 1; x < right; x++) {
	    for (y = 1; y < bottom; y++) {
		if ((GET(img, x, y) & 15) > 4) PUT(img, x, y, black);
		else PUT(img, x, y, 0);
		}
	}
    }
}

int threshold_img(image *img, PIXEL th, PIXEL on)
{
    register int    x, y;

	CString szMsg;
	szMsg.Format( "Thresholding image at %d", th );
	::OutputMessage( szMsg, LOG_PROC );

    for (x = img->w - 1; x >= 0; x--)
	{
		for (y = img->h - 1; y >= 0; y--)
		{
			// 21May09: Somesh: Interchanged 0 and on levels to which image is thresholded.
		    if (GET(img, x, y) >= th)
			{
				PUT(img, x, y, 0);
			}
	    else PUT(img, x, y, on);
		}
    }
    return(0);
}

int clr2intensity(image *img)
{
    int	    x, y;
    struct colour	*c;
    register unsigned int    intens;

	::OutputMessage( "Translate pixels to intensity from clr table", LOG_PROC );
//    message1(INFORM, "Translate pixels to intensity from clr table");

    if (img->clrtable == NULL) {
//	error1(MAJOR | INTERNAL, "no clr table to translate from");
	return(MAJOR | INTERNAL);
    }
    for (x = img->w - 1; x >= 0; x--) {
	for (y = img->h - 1; y >= 0; y--) {
	    c = &(img->clrtable[GET(img, x, y)]);
		// 21May09: Somesh: Luminance is a weighted average of color intensities
		//intens = c->r + c->g + c->b;
	    intens = (0.3 * c->r) + (0.59 * c->g) + (0.11 * c->b);
	    //PUT(img, x, y, 255 - intens / 3);
		PUT(img, x, y, intens);
	}
    }
    return(0);
}


/*    clr and grey tables for img module		*/

struct colour *standard_grey_table(unsigned char nbits)
/* returns a copy of an appropriate grey table according to the number of
clr / grey level bits, to be assigned to a image. If the image is
freed, this copy of the clr table is also freed. */
{
    struct colour	*table, *tp;
    unsigned short int    nval, greyval, i;

    nval = 1 << nbits;
    table = (colour*)myalloc(nval * sizeof(struct colour));
    for (i = 0, tp = table; i < nval; i++, tp++) {
	greyval = (i * 255) / (nval - 1);
	tp->r = (UCHAR)greyval;
	tp->g = (UCHAR)greyval;
	tp->b = (UCHAR)greyval;
    }
    return(table);
}

struct colour *standard_clr_table(unsigned char nbits)
/* returns a copy of an appropriate colour table according to the number of
clr / grey level bits, to be assigned to a image. If the image is
freed, this copy of the clr table is also freed. */
{
    static struct colour    t1b[2] = {
    {  0,  0,  0},
    {255,255,255} };

    static struct colour    t4b[16] = {
    {0, 0, 0 },
    {0, 0, 170},
    {0, 170, 0   },
    {0, 170, 170  },
    {170, 0, 0},
    {170, 0, 170},
    {170, 170, 0},
    {170, 170, 170},
    {85,  85,  85 },
    {85,  85,  255 },
    {85, 255, 85},
    {85, 255, 255},
    {255,  85,  85},
    {255,  85, 255 },
    {255, 255,  85},
    {255, 255, 255 }};

    static struct colour    t8b[256] = {
    { 0,  0,  0},
    { 0,  0, 85},
    { 0,  0,170},
    { 0,  0,255},
    { 0, 42,  0},
    { 0, 42, 85},
    { 0, 42,170},
    { 0, 42,255},
    { 0, 85,  0},
    { 0, 85, 85},
    { 0, 85,170},
    { 0, 85,255},
    { 0,127,  0},
    { 0,127, 85},
    { 0,127,170},
    { 0,127,255},
    { 0,170,  0},
    { 0,170, 85},
    { 0,170,170},
    { 0,170,255},
    { 0,213,  0},
    { 0,213, 85},
    { 0,213,170},
    { 0,213,255},
    { 0,255,  0},
    { 0,255, 85},
    { 0,255,170},
    { 0,255,255},
    { 0,255,  0},
    { 0,255, 85},
    { 0,255,170},
    { 0,255,255},
    {42,  0,  0},
    {42,  0, 85},
    {42,  0,170},
    {42,  0,255},
    {42, 42,  0},
    {42, 42, 85},
    {42, 42,170},
    {42, 42,255},
    {42, 85,  0},
    {42, 85, 85},
    {42, 85,170},
    {42, 85,255},
    {42,127,  0},
    {42,127, 85},
    {42,127,170},
    {42,127,255},
    {42,170,  0},
    {42,170, 85},
    {42,170,170},
    {42,170,255},
    {42,213,  0},
    {42,213, 85},
    {42,213,170},
    {42,213,255},
    {42,255,  0},
    {42,255, 85},
    {42,255,170},
    {42,255,255},
    {42,255,  0},
    {42,255, 85},
    {42,255,170},
    {42,255,255},
    {85,  0,  0},
    {85,  0, 85},
    {85,  0,170},
    {85,  0,255},
    {85, 42,  0},
    {85, 42, 85},
    {85, 42,170},
    {85, 42,255},
    {85, 85,  0},
    {85, 85, 85},
    {85, 85,170},
    {85, 85,255},
    {85,127,  0},
    {85,127, 85},
    {85,127,170},
    {85,127,255},
    {85,170,  0},
    {85,170, 85},
    {85,170,170},
    {85,170,255},
    {85,213,  0},
    {85,213, 85},
    {85,213,170},
    {85,213,255},
    {85,255,  0},
    {85,255, 85},
    {85,255,170},
    {85,255,255},
    {85,255,  0},
    {85,255, 85},
    {85,255,170},
    {85,255,255},
    {127,  0,  0},
    {127,  0, 85},
    {127,  0,170},
    {127,  0,255},
    {127, 42,  0},
    {127, 42, 85},
    {127, 42,170},
    {127, 42,255},
    {127, 85,  0},
    {127, 85, 85},
    {127, 85,170},
    {127, 85,255},
    {127,127,  0},
    {127,127, 85},
    {127,127,170},
    {127,127,255},
    {127,170,  0},
    {127,170, 85},
    {127,170,170},
    {127,170,255},
    {127,213,  0},
    {127,213, 85},
    {127,213,170},
    {127,213,255},
    {127,255,  0},
    {127,255, 85},
    {127,255,170},
    {127,255,255},
    {127,255,  0},
    {127,255, 85},
    {127,255,170},
    {127,255,255},
    {170,  0,  0},
    {170,  0, 85},
    {170,  0,170},
    {170,  0,255},
    {170, 42,  0},
    {170, 42, 85},
    {170, 42,170},
    {170, 42,255},
    {170, 85,  0},
    {170, 85, 85},
    {170, 85,170},
    {170, 85,255},
    {170,127,  0},
    {170,127, 85},
    {170,127,170},
    {170,127,255},
    {170,170,  0},
    {170,170, 85},
    {170,170,170},
    {170,170,255},
    {170,213,  0},
    {170,213, 85},
    {170,213,170},
    {170,213,255},
    {170,255,  0},
    {170,255, 85},
    {170,255,170},
    {170,255,255},
    {170,255,  0},
    {170,255, 85},
    {170,255,170},
    {170,255,255},
    {213,  0,  0},
    {213,  0, 85},
    {213,  0,170},
    {213,  0,255},
    {213, 42,  0},
    {213, 42, 85},
    {213, 42,170},
    {213, 42,255},
    {213, 85,  0},
    {213, 85, 85},
    {213, 85,170},
    {213, 85,255},
    {213,127,  0},
    {213,127, 85},
    {213,127,170},
    {213,127,255},
    {213,170,  0},
    {213,170, 85},
    {213,170,170},
    {213,170,255},
    {213,213,  0},
    {213,213, 85},
    {213,213,170},
    {213,213,255},
    {213,255,  0},
    {213,255, 85},
    {213,255,170},
    {213,255,255},
    {213,255,  0},
    {213,255, 85},
    {213,255,170},
    {213,255,255},
    {255,  0,  0},
    {255,  0, 85},
    {255,  0,170},
    {255,  0,255},
    {255, 42,  0},
    {255, 42, 85},
    {255, 42,170},
    {255, 42,255},
    {255, 85,  0},
    {255, 85, 85},
    {255, 85,170},
    {255, 85,255},
    {255,127,  0},
    {255,127, 85},
    {255,127,170},
    {255,127,255},
    {255,170,  0},
    {255,170, 85},
    {255,170,170},
    {255,170,255},
    {255,213,  0},
    {255,213, 85},
    {255,213,170},
    {255,213,255},
    {255,255,  0},
    {255,255, 85},
    {255,255,170},
    {255,255,255},
    {255,255,  0},
    {255,255, 85},
    {255,255,170},
    {255,255,255},
    {255,  0,  0},
    {255,  0, 85},
    {255,  0,170},
    {255,  0,255},
    {255, 42,  0},
    {255, 42, 85},
    {255, 42,170},
    {255, 42,255},
    {255, 85,  0},
    {255, 85, 85},
    {255, 85,170},
    {255, 85,255},
    {255,127,  0},
    {255,127, 85},
    {255,127,170},
    {255,127,255},
    {255,170,  0},
    {255,170, 85},
    {255,170,170},
    {255,170,255},
    {255,213,  0},
    {255,213, 85},
    {255,213,170},
    {255,213,255},
    {255,255,  0},
    { 0,  0,  0},
    {42, 42, 42},
    {85, 85, 85},
    {127,127,127},
    {170,170,170},
    {213,213,213},
    {255,255,255}  };

    struct colour    *t = NULL;

    switch(nbits) {
	case 1: t = (colour*)myalloc(sizeof(t1b));
	    memcpy(t, t1b, sizeof(t1b));
	    break;
	case 4: t = (colour*)myalloc(sizeof(t4b));
	    memcpy(t, t4b, sizeof(t4b));
	    break;
	case 8: t = (colour*)myalloc(sizeof(t8b));
	    memcpy(t, t8b, sizeof(t8b));
	    break;
//	default: error1(MAJOR | INTERNAL, "clr table with illegal number of bits requested");
    }
    return(t);
}

void set_img_clrbits(image *img, unsigned char nbits)
{
    if (img->clrtable != NULL) free(img->clrtable);
    img->clrtable = standard_clr_table(nbits);
    img->clrbits = nbits;
}

void set_img_greybits(image *img, unsigned char nbits)
{
    if (img->clrtable != NULL) free(img->clrtable);
    img->clrtable = standard_grey_table(nbits);
    img->clrbits = nbits;
}

// 22May09: Somesh: Make the background white
int set_img_background(image *img)
{
	register int x, y;

	for (x = img->w - 1; x >= 0; x--)
	{
		for (y = img->h - 1; y >= 0; y--)
		{
			if (GET(img, x, y) == 0)
				PUT(img, x, y, 255);
		}
	}
	 return(0);
}

// 28May09: GMB: Copy function for image
BOOL image::Copy( image* src )
{
	if( !src ) return FALSE;

	this->clrbits = src->clrbits;
	this->clrs = src->clrs;
	this->bipp = src->bipp;
	this->w = src->w;
	this->h = src->h;
	this->dpi = src->dpi;
	this->nallocl = src->nallocl;
	if( src->clrtable )
	{
		int nSize = 1 << this->clrbits;
		nSize *= sizeof( struct colour );
		this->clrtable = (colour*)myalloc( nSize );
		memcpy( this->clrtable, src->clrtable, nSize );
	}
	// filename does not appear to be used
	if( src->data && *(src->data) )
	{
		this->data = (PIXEL**)myalloc( this->h * sizeof( PIXEL* ) );
		for( int i = 0; i < this->h; i++ )
		{
			this->data[ i ] = (PIXEL*)myalloc( this->nallocl * sizeof( PIXEL ) );
			for( int j = 0; j < this->w; j++ )
			{
				this->data[ i ][ j ] = src->data[ i ][ j ];
			}
		}
	}

	return TRUE;
}

}// namespace
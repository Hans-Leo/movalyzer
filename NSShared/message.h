#pragma once

namespace SFINX {

enum severities {
	DETAILED,
	INFORM,
	MINOR,
	MAJOR,
	FATAL,
};

typedef short result_t;

void MSGSetLevel(int , result_t );
result_t MSGLevel(int boundary);
void MSGSetStream(FILE *);
void MSGDefaultHandle(result_t, char*, ...);
void MSGHandle(result_t, char*, ...);
void MSGSetHandler(void (*newhandler)(result_t, char*, ...));
void *myalloc(size_t );
void *myrealloc(void *,size_t );

};	// namespace

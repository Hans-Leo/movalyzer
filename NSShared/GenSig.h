#ifndef _GENSIG_H_
#define _GENSIG_H_

// Submovement Generation Method
#define GEN_NONE		0
#define GEN_DELAY		1
#define GEN_WARP		2

class AFX_EXT_API GenSig
{
// Construction/Destruction
public:
	GenSig();
	~GenSig();

// Operations
public:
	BOOL Generate( CString szFileOut );
protected:
	void AddNoise( double* x, double* y, double rnoise );
	void Round( double* x, double* y, BOOL intgr );
	void Output( double x, double y, double z, CStdioFile* f );

// Attributes
public:
	// Tablet settings
	double	m_cm;
	double	m_sec;
	double	m_prsmin;
	double	m_noise;
	BOOL	m_integer;
	// Task time settings
	double	m_startt;
	double	m_deltat;
	double	m_nstroke;
	double	m_trailtime;
	// Task X movement
	double	m_startx;
	double	m_deltax;
	double	m_phasex;
	double	m_xspeed;
	// Task Y movement
	double	m_starty;
	double	m_deltay;
	double	m_phasey;
	// Stroke data
	int		m_pattern;
	// 07Nov08: Somesh: Submovement data
	int		m_nSubMvmtModel;
	double	m_subduration;
	double	m_subdelay;
};

#endif	// _GENSIG_H_

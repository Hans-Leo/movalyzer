/**********************************************************************
    hlib - version 2.2 - hlt - 15 Sep 1994
***********************************************************************

Purpose: General handwriting processing routines

History:
  18Apr94: Rnpol() added
  15Sep94: Means() added from msc2spss.c
  10Feb95: vars() added
   1May95: Means2() added; later removed
   4Jun96: xmis argument explicitely in means(), vars(), correl()
           correl() had bug: mean, var not based on only non-missing PAIRS
  10Jul97: RMS distance wrong: twice sqrt was taken. The one in rregr was superfluous.
           Originally it was meanqdist() this name was misleading. Now: rmsdist()
  19Oct99: GMB: Conversion to C++, moved into DLL, changed from "Main" to API,
		   removal of statics
18May00: hlt: Updated ratfor() to leave the DC of the first sample untouched

**********************************************************************/

#include <math.h>
#include "../Common/DefFun.h"
#include "hlib.h"

#if defined __PROCESSMOD__
#include "..\ProcessMod\Resource.h"
#elif defined __SHOWMOD__
#include "..\ShowMod\Resource.h"
#elif defined __MOVALYZER__
#include "..\Test\Resource.h"
#endif

/* Range of data array */
/* Integer data */
/*************************************************/
void imima (int* iarr, int n, int *min, int *max)
/*************************************************/
{
  if( !iarr || !min || !max ) return;

  int i;

  *min = iarr [0];
  *max = iarr [0];

  for (i = 1; i < n; i++) {
    if (iarr [i] > *max)
      *max = iarr [i];
    else if (iarr [i] < *min)
      *min = iarr [i];
  }
} /* End imima */

/********************************************************/
void rmima (double* arr, int n, double *min, double *max)
/********************************************************/
/* Range of data array */
/* Real data */
{
  int i;

  *min = arr [0];
  *max = arr [0];

  for(i = 1; i < n; i++) {
    if (arr [i] > *max)
      *max = arr [i];
    else if (arr [i] < *min)
      *min = arr [i];
  }
} /* End rmima */

// ADDED Oct.31.06 Yi: Add a overload function for rmima in case of small presure while pen lift
/********************************************************/
void rmima (double* arr, int n, double *min, double *max, double *zin, double prsmin)
/********************************************************/
/* Range of data array */
/* Real data */
{
  int i;

  *min = arr [0];
  *max = arr [0];

  //if pressure is smaller than the presim, then don't do anything
  for(i = 1; i < n; i++) {
	if (zin[i]>prsmin)
	{
		if (arr [i] > *max)
		  *max = arr [i];
		else if (arr [i] < *min)
		  *min = arr [i];
	}
  }

} /* End rmima */



/*******************************************************/
void indrmm (double* r, int n, int *indmin, int *indmax)
/*******************************************************/
/*  indices of min en max of double array

    input:
    r = double array
    n = length of r
    output:
    indmin = index minimum
    indmax = index maximum */

{
	if( !r || !indmin || !indmax ) return;

	int i;

    *indmin = 0;
    *indmax = 0;

    for (i = 1; i < n; ++i) {
	if (r [i] < r [*indmin])
          *indmin = i;
	else if (r [i] > r [*indmax])
          *indmax = i;
    }
    return;
} /* End indrmm */

/***********************************************************************/
double rregr (double* x, double* y, int n, double w, double *a, double *b, double *r)
/***********************************************************************/
/*
Fit regression line y = a + b * x through (x,y) points.
Weigh points using w = var(y) / var(x).
mdist is the RMS distance of all points from the fitted line.
*/
{
	if( !x || !y || !a || !b || !r ) return 0.;

    double bpart, xd, yd, xg, yg, qxx, qxy, qyy, mdist;
    double aa, bb;
    int i;

/*  bepaalt regressierechte y=a+b*x door de punten (x(i),y(i)),
    waarbij x en y variaties vertonen.

    input   x,y : beginadressen van x en y arrays
	    n   : aantal punten
	    w   : weegfactor = (var y)/(var x)
    output  a,b : coefficienten van gefitte rechte y=a+b*x
	    r   : correlatie coefficient */

    /*  bepaling xg,yg */
    if (n < 2) {
    //printf("%%RREGR: n<2\n");
    *r = 0.;
    *a = 0.;
    *b = 0.;
    return (0.);
    }
    xg = (double)0.;
    yg = (double)0.;
    for (i = 0; i < n; ++i) {
	xg += x[i];
	yg += y[i];
    }
    xg /= n;
    yg /= n;

    /*  bepaling qxx,qyy,qxy */
    qxx = (double)0.;
    qyy = (double)0.;
    qxy = (double)0.;
    for (i = 0; i < n; ++i) {
	xd = x[i] - xg;
	yd = y[i] - yg;
	qxx += xd * xd;
	qyy += yd * yd;
	qxy += xd * yd;
    }

    if (qxx == (double)0. || qyy == (double)0. || qxy == (double) 0.) {
/*
      fprintf(stderr,"%%RREGR: n, qxx, qyy, qxy = %d %g %g %g\n",
	  n, qxx, qyy, qxy);
      for (i=0;i<n;i++)
	  fprintf(stderr,"x[%d]= %f y[%d]= %f\n", i, x[i], i, y[i]);
*/
      *r = 1.;
      *a = 0.;
      *b = 0.;
      return (0.);
    }
/*
    if      (qyy == (double)0.) {
      *r = 1.;
      bpart =
    } else if (qxy == (double)0.) {
      *r = 1.;
    } else if (qxx == (double) 0.) {
	fprintf(stderr,"%%RREGR: n, qxx, qyy, qxy = %d %g %g %g\n",
	  n, qxx, qyy, qxy);
	for (ipr=0;ipr<n;ipr++) {
	    fprintf(stderr,"x[%d]= %f y[%d]= %f",
	      ipr, x[ipr], ipr, y[ipr]);
	    fprintf(stderr, "\n");
      }
      *r = 1.;
      *a = 0.;
      *b = 0.;
      return;
    } else {
      *r = qxy / (double)sqrt(qxx * qyy);
      bpart = (qyy - w * qxx) / (qxy * (double)2.);
    }
*/
    /*  bepaling a, b, r */
    *r = qxy / (double)sqrt(qxx * qyy);
    bpart = (qyy - w * qxx) / (qxy * (double)2.);
    *b = bpart + (double)sqrt(w + bpart * bpart);
    if (*r < (double)0.) {
	*b = bpart * (double)2. - *b;
    }
    *a = yg - *b * xg;

    /* addition: mean distance */
    aa = *a;
    bb = *b;
    mdist = rmsdist (x, y, n, aa, bb);

    return mdist;

} /* End rregr */

/*******************************************************************/
double rmsdist (double* x, double* y, int n, double a, double b)
/*******************************************************************/
/*
RMS distance of array of (x, y) points from line
  y = a + b * x

The perpendicular line through (x1, y1) or (x[is],y[is]) is
  (y - y1) = -1/b * (x - x1)

Solving y yields the x of the corssing point of both lines
  a + b * x = y1 - 1/b * (x - x1)
or
  x = (y1 - a - 1/b * (x - x1)) / b

*/
{
  if( !x || !y ) return 0.;

  int is;
  double qdist, xis0, yis0;

  if (n == 0) return (0.);

  qdist = 0.;
  for (is = 0; is < n; is++) {
    xis0 = (x [is] + (y [is] - a) * b) / (SQR (b) + 1.);
    yis0 = a + b * xis0;
    qdist += SQR (xis0 - x [is]) + SQR (yis0 - y [is]);
  }
  return sqrt (qdist / n);

}
/******************************************************************/
void ratfor (double* x, double* y, int n,
  double xasx, double xasy, double yasx, double yasy, int keep_dc)
/******************************************************************/
{
	if( !x || !y ) return;

    static double xasx1, xasy1, yasx1, yasy1;
    int i, nn;
    static double xi, detinv;
    static double xdc_old, ydc_old;
    static double xdc_new, ydc_new;

/*  SUBROUTINE  RATFOR                           HANS-LEO TEULINGS

    GEEFT DE COORDINATEN VAN DE PUNTEN (X(I),Y(I)) TOV EEN
    NIEUW ASSENSTELSEL, GEGEVEN DOOR DE COORDINATEN VAN DE NIEUWE
    X- EN Y-AS (XASX,XASY) EN (YASX,YASY)

    INVERSE: CALL RATFOR(X,Y,N,YASY,-XASY,-YASX,XASX)

    INPUT  X,Y       : BEGINADRESSEN VAN X EN Y ARRAY
           N         : AANTAL PUNTEN
           XASX,XASY : COORDINATEN VAN DE NIEUWE X-AS
           YASX,YASY : COORDINATEN VAN DE NIEUWE Y-AS */

	/* Correct n to prevent zero division */
    nn = n;
    if(nn < 1) nn = 1;

	/*** Estimations before rotation ***/

	/* 18May00: HLT: make option for:TIMFUN alone */
	/* o nn=n (Keep DC of whole trial constant) */
	/* o nn=1 (Keep DC of the first sample of the trial constant) */
	/* Currently nn=1 is hard coded */
	nn = 1;

	if(keep_dc) {
		/* Estimate existing DC base on nn samples */
		xdc_old = 0.0;
		ydc_old = 0.0;
		/* nn=1 is used when there are no data, which is ok */
		for( i = 0; i < nn; ++i) {
			xdc_old += (double) x[i];
			ydc_old += (double) y[i];
		}
        xdc_old = xdc_old / (double) nn;
        ydc_old = ydc_old / (double) nn;
    }


    /*** Rotation ***/
	/* Test for inappropriate axes */
    /*  DEPDPENDENT AXES */
    detinv = xasx * yasy - xasy * yasx;
    if (detinv == (double) 0.) {
        OutputMessage( _T("RATFOR: Dependent axes (no transformation)."), LOG_PROC );
        return;
    }

	/* Correct for rescaling if axes not length 1 */
	detinv = (double) 1. / detinv;
    xasx1 = detinv * xasx;
    xasy1 = detinv * xasy;
    yasx1 = detinv * yasx;
    yasy1 = detinv * yasy;

	/* Rotation transformation */
    for (i = 0; i < n; ++i) {
	xi = yasy1 * x[i] - yasx1 * y[i];
	y[i] = -xasy1 * x[i] + xasx1 * y[i];
	x[i] = xi;
    }

	/*** Corrections after rotation ***/
    if(keep_dc) {
		/* Estimate new DC based on nn samples */
		xdc_new = 0.0;
		ydc_new = 0.0;
        for( i = 0; i < nn; ++i) {
			xdc_new += (double) x[i];
			ydc_new += (double) y[i];
		}
        xdc_new = xdc_new / (double) nn;
        ydc_new = ydc_new / (double) nn;

		/* Add old DC and deduct new DC for WHOLE trial, thus n samples */
        for( i = 0; i < n; ++i) {
			x[i] = x[i] - (double) xdc_new + xdc_old;
			y[i] = y[i] - (double) ydc_new + ydc_old;
		}
    }

    return;

} /* End ratfor */

/***********************************************************************/
void shell_sortr (double* reals, int iarsiz, int* ipoint, int itype)
/***********************************************************************/

/*

Pointer sorter of input reals[] of size iarsiz
Output array ipoint [] contains the original rank numbers 0 to iarsiz-1
The sorted array is reals[ipoint[]]
itype=+1 means ascending; -1 means descending

*/

{
  if( !reals || !ipoint ) return;

  int more, i, j, igap, imax, kk;

  if (itype != 1 && itype != -1) {
    OutputMessage( _T("Shell_sortr: ERROR: itype not 1 or -1."), LOG_PROC );
    return;
  }

  /* Initialize index array IPOINT. */
  for(i = 0; i < iarsiz; i++) {
    ipoint [i] = i;
  }

	igap = iarsiz;
	while(igap > 1)
	{
		igap = igap / 2;
		imax = iarsiz - igap;
		more = 1;
		while (more)
		{
			more = 0;
			for (i = 0; i < imax; i++)
			{
				j = i+igap;
				// 10Sep03: GMB - Added asserts to ensure validity of array indices
				ASSERT( ipoint[ j ] >= 0 );
				ASSERT( ipoint[ i ] >= 0 );
				if((itype == +1 && reals [ipoint[j]] < reals [ipoint[i]]) ||
					(itype == -1 && reals [ipoint[j]] > reals [ipoint[i]]))
				{
					kk = ipoint[j];
					ipoint[j] = ipoint[i];
					ipoint[i] = kk;
					more = 1;
				}
			}
		}
	}
}

/*********************************************/
double means (double* data, int n, double xmis)
/*********************************************/
/* Mean value, leaving out zero data */
{
  if( !data ) return 0.;

  double mean;
  int nmean, i;

  nmean = 0;
  mean = 0.;
  for (i = 0; i < n; i++) {
    if (data [i] != xmis) {
      nmean++;
      mean += data [i];
    }
  }
  if (nmean > 0) mean /= nmean;

  return (mean);
}


/*********************************************/
double vars (double* data, int n, double meandata, double xmis)
/*********************************************/
/* Variance, leaving out zero data */
{
  if( !data ) return 0.;

  double var;
  int nmean, i;

  nmean = 0;
  var = 0.;
  for (i = 0; i < n; i++) {
    if (data [i] != xmis) {
      nmean++;
      var += SQR (data [i] - meandata);
    }
  }
  if (nmean > 1) var /= (nmean - 1);

  return (var);
}

/************************************************************/
double correl (double* x, double* y, int n, double xmis)
/************************************************************/
{
  if( !x || !y ) return 0.;

  int i;
  double xmean, ymean;
  double xvar, yvar;
  double cor;
  int nnonmissing;

  /* Means of nonmissing pairs */
  nnonmissing = 0;
  xmean = 0.;
  ymean = 0.;
  for (i = 0; i < n; i++) {
    if (x [i] != xmis && y [i] != xmis) {
      xmean += x [i];
      ymean += y [i];
      nnonmissing++;
    }
  }
  if (nnonmissing != 0) {
    xmean /= nnonmissing;
    ymean /= nnonmissing;
  }
  else {
    xmean = xmis;
    ymean = xmis;
  }

  /* Vars of nonmissing pairs */
  /* actually vars * n - 1 */
  xvar = 0.;
  yvar = 0.;
  for (i = 0; i < n; i++) {
    if (x [i] != xmis && y [i] != xmis) {
      xvar += SQR (x [i] - xmean);
      yvar += SQR (y [i] - ymean);
    }
  }

  //xmean = means (x, n, xmis);
  //ymean = means (y, n, xmis);

  //xvar = vars (x, n, xmean, xmis) * (n - 1.);
  //yvar = vars (y, n, ymean, xmis) * (n - 1.);

  cor = 0.;
  for (i = 0; i < n; i++) {
   if (x [i] != xmis && y [i] != xmis) {
     cor += (x [i] - xmean ) * (y [i] - ymean);
   }
  }

  if (xvar > 0. && yvar > 0.)
    cor /= sqrt (xvar * yvar);
  else
    cor = xmis;

  return cor;
}

/************************************************************/
double rnpol( double r [], int n, double rn )
/************************************************************/
/*
linear interpolation in float array

input:  r  = array of length n
        rn = float array index, 1-based (tvabotfin/tvabotini)
output: rnpol = interpolated value
*/

{
  if( !r ) return 0.;

  double d;

  int i1, i2;
  /*  test */
  if( rn < 0. || rn > n - 1 )
  {
	  CString szMsg;
	  szMsg.Format( _T("Rnpol: rn out of range: %.3g."), rn );
	  OutputMessage( szMsg, LOG_PROC );
  }

  /*  special cases */
  if (rn <= 0.) return (r [0]);
  if (rn >= n)
  {
	  // 10Sep03: GMB - Added assert to ensure validity of index
	  ASSERT( ( n - 1 ) >= 0 );
	  return (r [n - 1]);
  }

  /*  interpolate */
  i1 = (int)rn;
  i2 = i1 + 1;
  d = rn - i1;
  return ((1.0 - d) * r [i1] + d * r [i2]);

}

// ShowTF.h: Definition of the ShowTF class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"       // main symbols
#include "PEGrpApi.h"
#include "PEMessag.h"

#define	TF_UNKNOWN	0x0000	// unknown or don't care
#define	TF_XY		0x0001	// X vs Y graph
#define	TF_Xt		0x0002	// X(t)
#define	TF_Yt		0x0003	// Y(t)
#define	TF_Zt		0x0004	// Z(t)
#define	TF_vx		0x0005	// vx(t)
#define	TF_vy		0x0006	// vy(t)
#define	TF_vabs		0x0007	// vabs(t)
#define	TF_ax		0x0008	// ax(t)
#define	TF_ay		0x0009	// ay(t)
#define	TF_JXt		0x1000	// jx(t)
#define	TF_JYt		0x4000	// jy(t)
#define	TF_JABSt	0x8000	// jabs(t)
#define	TF_aspec	0x0010	// Position spectrum
#define	TF_XZ		0x0011	// X vx Z
#define	TF_YZ		0x0012	// Y vs Z
#define	TF_vz		0x0013	// vz(t)
#define	TF_az		0x0014	// az(t)
#define TF_vaspec	0x0015	// Velocity spectrum
#define TF_aaspec	0x0016	// Accel spectrum
#define	TF_3D		0x0100	// + 1 of the charts above

#define	TF_COL_X		1
#define	TF_COL_Y		2
#define	TF_COL_Z		3
#define	TF_COL_VX		4
#define	TF_COL_VY		5
#define	TF_COL_VZ		6
#define	TF_COL_AX		7
#define	TF_COL_AY		8
#define	TF_COL_AZ		9
#define	TF_COL_JX		10
#define	TF_COL_JY		11
#define	TF_COL_JZ		12
#define TF_COL_Time		13

/////////////////////////////////////////////////////////////////////////////
// ShowTF

class ShowTF : 
	public IDispatchImpl<IShowTF, &IID_IShowTF, &LIBID_SHOWMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<ShowTF,&CLSID_ShowTF>
{
public:
	ShowTF();
	~ShowTF();

BEGIN_COM_MAP(ShowTF)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IShowTF)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(ShowTF) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_ShowTF)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IShowTF
public:
	STDMETHOD(ShowChartData)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ BSTR Val);
	STDMETHOD(SetAnnotationSymbols)(/*[in]*/ short SymbolSeg, /*[in]*/ short SymbolSubMvmt);
	STDMETHOD(SetMissingDataValue)(/*[in]*/ double MDVal);
	STDMETHOD(GetStroke)(/*[out,retval]*/ short* pVal);
	STDMETHOD(Customize)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(SetYZInverted)(/*[in]*/ VARIANT* PEHandle,
							 /*[in]*/ TFGraph GraphType,
							 /*[in]*/ BOOL fOn);
	STDMETHOD(SetLineThickness)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ short Thickness);
	STDMETHOD(ExportGraph)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(SetCursorPos)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ LONG Pos);
	STDMETHOD(GetCursorPos)(/*[in]*/ VARIANT* PEHandle, /*[out, retval]*/ LONG* Pos);
	STDMETHOD(OnCursorMove)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ TFGraph GraphType);
	STDMETHOD(put_RTFrequency)(short Freq);
	STDMETHOD(put_ShowOnlyGraph)(BOOL fOn);
	STDMETHOD(put_Feedback)(BOOL fOn);
	STDMETHOD(put_Feature)(BSTR newVal);
	STDMETHOD(put_MinColor)(DWORD newVal);
	STDMETHOD(put_MaxColor)(DWORD newVal);
	STDMETHOD(SetMonochrome)(/*[in]*/ VARIANT* PEHandle,
							 /*[in]*/ TFGraph GraphType,
							 /*[in]*/ BOOL fOn);
	STDMETHOD(put_Monochrome)(/*[in]*/ BOOL fOn);
	STDMETHOD(put_SupressAnn)(/*[in]*/ BOOL fOn);
	STDMETHOD(PrintGraph)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(UseOptimalScaling)(/*[in]*/ BOOL fOn);
	STDMETHOD(put_Frequency)(/*[in]*/ double newVal);
	STDMETHOD(put_TF_InFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(put_SEG_InFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(ShowTFGraph2)(/*[in]*/ VARIANT* GraphWindow,
							/*[in]*/ TFAxis XAxis,
							/*[in]*/ TFAxis YAxis,
							/* [in]*/ VARIANT* GraphDlg,
							/*[in]*/ BOOL AnnValues,
							/*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(ShowTFGraph)(/*[in]*/ VARIANT* GraphWindow,
						   /*[in]*/ TFGraph GraphType,
						   /* [in]*/ VARIANT* GraphDlg,
						   /*[in]*/ BOOL AnnValues,
						   /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(ShowTFGraphRT)(/*[in]*/ VARIANT* GraphWindow,
							 /*[in]*/ TFGraph GraphType,
							 /*[out]*/ LONG* Cnt,
							 /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(OnTimer)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);
	STDMETHOD(put_PenUpColor)(/*[in]*/ DWORD newVal);
	STDMETHOD(put_MinPenPressure)(/*[in]*/ short newVal);

protected:
	void Init();
	BOOL ShowCustom( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowXY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowXZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowYZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowXt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowYt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowZt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowVX( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowVY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowVZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowVABS( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowAX( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowAY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowAZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowJXt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowJYt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowJABSt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE  );
	BOOL ShowAvsB( double* a, double* b, int nX, CString szX, int nY, CString szY,
				   CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowAvsB3D( double* a, double* b, int nX, CString szX, int nY, CString szY,
					 CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowByTime( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, double* vals, BOOL fOnlyColors = FALSE  );
	BOOL ShowByTime3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, double* vals, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE );
	BOOL ShowAspec( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, TFGraph gt, BOOL fOnlyColors = FALSE  );
	BOOL ShowXYRT( CWnd* pGraph, HWND* hPE );
	BOOL ShowXY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowXZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowYZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowXt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowYt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowZt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowJXt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowJYt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowJABSt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowVX3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowVY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowVZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowAX3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowAY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowAZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	BOOL ShowVABS3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE  );
	// Shared methods
	BOOL InitChart( HWND* hPE, CWnd* pGraph, CWnd* pGraphDlg = NULL, UINT nType = PECONTROL_SGRAPH );
	void DoLabels( HWND* hPE, CString szX, CString szY, CString szTitle, CString szSubTitle );
	void SetChartDefaults( HWND* hPE );	// override settings where desired

// Attributes
protected:
	CString		m_szTFFile;
	CString		m_szSegFile;
	BOOL		m_fInit;
	BOOL		m_f3D;
	TFAxis		AxisX;
	TFAxis		AxisY;
	CStringList	m_szLabels;
	double*		x;
	double*		y;
	double*		z;
	double*		vx;
	double*		vy;
	double*		vabs;
	double*		ax;
	double*		ay;
	double*		az;
	double*		jx;
	double*		jy;
	double*		jabs;
	double*		aspec;
	double*		aspecfilt;
	double*		vaspec;
	double*		vaspecfilt;
	double*		aaspec;
	double*		aaspecfilt;
	double		sec;
	double		prsmin;
	double		beta;

	// for other features
	double*		st;		// start time
	double*		d;		// duration
	int			nS;		// column of slant (will be for other features later)
	double*		s;		// slant (other features later)
	int			nVS;	// column of vert size
	double*		vs;		// vertical size
	int			nLA;	// column of loop area
	double*		la;		// loop area

	double		m_dFrequency;
	int			m_nCnt;
	int			m_nCntSpectrum;
	int			m_nCntSpectrumV;
	int			m_nCntSpectrumA;
	double*		tseg;
	double*		tseg2;
	int			m_nCntSeg;
	int			m_nCntSeg2;
	CString		m_szSubTitle;
	CString		m_szYAxis;
	BOOL		m_fOptimal;
	BOOL		m_fMonochrome;
	BOOL		m_fSupressAnn;
	BOOL		m_fInverted;
	// feedback related
	BOOL		m_fFeedback;	// are we giving it?
	CString		m_szFeature;	// which feature (from tf)?
	DWORD		m_MinColor;		// color for minimum feature value
	DWORD		m_MaxColor;		// color for max...
	double		m_dMinVal;		// min value for feature
	double		m_dMaxVal;		// max...
	BOOL		m_fGraphOnly;	// hide all but graph itself
	short		m_nRTFreq;		// real-time feedback frequency
	DWORD		m_dwPenUpColor;
	BOOL		m_fStored;

public:
	// FORMERLY STATIC/GLOBAL VARS
	struct TFData
	{
		float fX, fY, fZ;
		double dX1, dY1, dX2, dY2, dZ1, dZ2;
		int symbol, symbol2;
		DWORD col, col2;
		char label[ 50 ];
		int annPt;

		double dataxmin, dataxmax;
		double dataymin, dataymax;
		double datazmin, datazmax;
		double dataxrange, datayrange, datazrange;
		RECT ScreenRectangle;
		double scalex, scaley, scalez, scale;
		double extradataxrange, extradatayrange, extradatazrange;
		double dataxminnew, dataxmaxnew;
		double datayminnew, dataymaxnew;
		double datazminnew, datazmaxnew;
	};
	TFData _tfd;
	// using annotation values?
	BOOL _AnnValues;
	// index for real time chart
	int _Idx;
	// feedback pointer
	double*	_theFB;
	// graph window
	CWnd*	_pGraph;
	// current stroke
	int	_nTFStroke;
	// sub-movement analysis is on
	int	_fTFSubMovement;
	// total segments
	int	_nTFTotalSegments;
	// last pen pressure
	int	_nTFLastPenPressure;
	// is this a spectral graph?
	BOOL m_fSpectrum;
	// symbols for annotations
	int	m_nSymbolSeg;
	int	m_nSymbolSubMvmt;
	// whether chart data is being shown
	BOOL m_fShowData;
};

// ShowHWR.h: Definition of the ShowHWR class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHOWHWR_H__E8B28DA2_96E8_11D3_8A27_00104BC7E2C8__INCLUDED_)
#define AFX_SHOWHWR_H__E8B28DA2_96E8_11D3_8A27_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "PEGrpApi.h"
#include "PEMessag.h"

#define	HWR_XY		0x0001	// X vs Y
#define	HWR_XYt		0x0002	// X(t) & Y(t)
#define	HWR_Zt		0x0003	// Z(t)
#define	HWR_XZ		0x0004	// X vs Z
#define	HWR_YZ		0x0005	// Y vs Z
#define	HWR_XY_3D	0x0100	// X vs Y 3D

/////////////////////////////////////////////////////////////////////////////
// ShowHWR

class ShowHWR : 
	public IDispatchImpl<IShowHWR, &IID_IShowHWR, &LIBID_SHOWMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<ShowHWR,&CLSID_ShowHWR>
{
public:
	ShowHWR();
	~ShowHWR();

BEGIN_COM_MAP(ShowHWR)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IShowHWR)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(ShowHWR) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_ShowHWR)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IShowHWR
public:
	STDMETHOD(SetMissingDataValue)(/*[in]*/ double MDVal);
	STDMETHOD(Customize)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(SetYZInverted)(/*[in]*/ VARIANT* PEHandle,
							 /*[in]*/ HWRGraph GraphType,
							 /*[in]*/ BOOL fMultiple,
							 /*[in]*/ BOOL fOn);
	STDMETHOD(SetLineThickness)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ short Thickness);
	STDMETHOD(ExportGraph)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(PenPressureLineThickness)(/*[in]*/ BOOL PPLT);
	STDMETHOD(SetCursorPos)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ LONG Pos);
	STDMETHOD(GetCursorPos)(/*[in]*/ VARIANT* PEHandle, /*[out, retval]*/ LONG* Pos);
	STDMETHOD(OnCursorMove)(/*[in]*/ VARIANT* PEHandle, /*[in]*/ HWRGraph GraphType);
	STDMETHOD(SetMonochrome)(/*[in]*/ VARIANT* PEHandle,
							 /*[in]*/ HWRGraph GraphType,
							 /*[in]*/ BOOL fMultiple,
							 /*[in]*/ BOOL fOn);
	STDMETHOD(put_Monochrome)(/*[in]*/ BOOL fOn);
	STDMETHOD(PrintGraph)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(UseOptimalScaling)(/*[in]*/ BOOL fOn);
	STDMETHOD(put_PenUpColor)(/*[in]*/ DWORD newVal);
	STDMETHOD(ShowHWRMultiple)(/*[in]*/ VARIANT* GraphWindow,
							   /*[in]*/ HWRGraph GraphType,
							   /*[in]*/ VARIANT* GraphDlg,
							   /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(AddFile)(/*[in]*/ BSTR File, /*[in]*/ BOOL Clear);
	STDMETHOD(put_MinPenPressure)(/*[in]*/ short newVal);
	STDMETHOD(put_Frequency)(/*[in]*/ double newVal);
	STDMETHOD(put_HWR_InFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(ShowHWRGraph)(/*[in]*/ VARIANT* GraphWindow,
							/*[in]*/ HWRGraph GraphType,
							/*[in]*/ VARIANT* GraphDlg,
							/*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(ShowHWRGraphRT)(/*[in]*/ VARIANT* GraphWindow,
							  /*[in]*/ HWRGraph GraphType,
							  /*[out]*/ LONG* Cnt,
							  /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(OnTimer)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);

protected:
	void Init();
	BOOL ShowXY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowXZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowYZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowZt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowSingleSetBase( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, double* px, double* py,
							CString szSubTitle, CString szX, CString szY, HWRGraph gt = (HWRGraph)0,
							BOOL fOnlyColors = FALSE );
	BOOL ShowXYMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowXZMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowYZMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowZtMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowSingleSetBaseMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE,
									UINT xaxis, UINT yaxis,	// defines which axis to use
									CString szSubTitle, CString szX, CString szY,
									BOOL fOnlyColors = FALSE );

	BOOL ShowXYt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );
	BOOL ShowXYtMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE );

	BOOL ShowXYRT( CWnd* pGraph, HWND* hPE );
	BOOL ShowXY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors = FALSE, BOOL fInverted = FALSE );
	// Shared methods
	BOOL InitChart( HWND* hPE, CWnd* pGraph, CWnd* pGraphDlg = NULL, UINT nType = PECONTROL_SGRAPH );
	void SetChartDefaults( HWND* hPE, HWRGraph gt = (HWRGraph)0, BOOL fSetLineThickness = FALSE );	// override settings where desired

// Attributes
protected:
	CString		m_szFile;
	CStringList	m_Files;
	BOOL		m_fInit;
	double*		x;
	double*		y;
	double*		z;
	double*		ux;
	double*		uy;
	double*		uz;
	double		m_dFrequency;
	int			m_nCnt;
	int			m_nCnt2;
	BOOL		m_fUNR;
	DWORD		m_dwPenUpColor;
	BOOL		m_fOptimal;
	BOOL		m_fMonochrome;
	BOOL		m_fPPLT;	// pen pressure line thickness
	BOOL		m_fInverted;
	BOOL		m_fStored;

public:
	// FORMERYLY static global vars
	// current stroke
	int			_nStroke;
	// sub-movement analysis is on
	int			_fSubMovement;
	// total segments
	int			_nTotalSegments;
	// last pen pressure
	int			_nLastPenPressure;
	// min/max pen pressures
	int			_nPPMin;
	int			_nPPMax;
	// min/max x/y/z for pen pressure variation (since using annotations)
	double		_dXMin;
	double		_dXMax;
	double		_dYMin;
	double		_dYMax;
	double		_dZMin;
	double		_dZMax;
	// index for real-time chart
	int			_Idx;
	// rectangle of chart
	RECT		ScreenRectangle;
};

#endif // !defined(AFX_SHOWHWR_H__E8B28DA2_96E8_11D3_8A27_00104BC7E2C8__INCLUDED_)

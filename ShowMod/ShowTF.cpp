// ShowTF.cpp : Implementation of CShowModApp and DLL registration.

#include "stdafx.h"
#include "ShowMod.h"
#include "ShowTF.h"
#include "..\NSShared\iolib.h"
#include "..\NSShared\hlib.h"
#include "..\Common\deffun.h"
#include "..\Common\ProcSettings.h"

#define	NULLDATAVALUE		-9999.0F
#define	LINECOLOR			RGB( 63, 63, 191 )
#define	LINE2COLOR			RGB( 255, 0, 0 )
#define	MONOCOLOR			RGB( 0, 0, 0 )
#define	BGCOLOR				RGB( 255, 255, 255 )
//#define	FORECOLOR			RGB( 127, 127, 127 )
#define	FORECOLOR			RGB( 0, 0, 0 )
#define	TBL_DATA_SIZE		120
#define	PETAL_LOC			PETAL_TOP_LEFT
#define	PETAL_STR			"  Sample=%3d Stroke=%2d Segment=%2d Pressure=%.0f Norm.Jerk=%.1f Vert.Size=%.1f LoopSurface=%.1f"
#define	PETAL_STR_2			"  Sample=%3d Stroke=%2d Segment=%2d Pressure=%.0f Norm.Jerk=%g Vert.Size=%g LoopSurface=%g"
#define	PETAL_STR_SPECTRA	"  Component=%d"

// Label indices
#define	LBL_X			0
#define	LBL_Y			LBL_X + 1
#define	LBL_Z			LBL_Y + 1
#define	LBL_ASPEC		LBL_Z + 1
#define	LBL_VASPEC		LBL_ASPEC + 1
#define	LBL_AASPEC		LBL_VASPEC + 1
#define	LBL_ASPEC_FILT	LBL_AASPEC + 1
#define	LBL_VASPEC_FILT	LBL_ASPEC_FILT + 1
#define	LBL_AASPEC_FILT	LBL_VASPEC_FILT + 1
#define	LBL_VX			LBL_AASPEC_FILT + 1
#define	LBL_VY			LBL_VX + 1
#define	LBL_VABS		LBL_VY + 1
#define	LBL_AX			LBL_VABS + 1
#define	LBL_AY			LBL_AX + 1
#define	LBL_AZ			LBL_AY + 1
#define	LBL_JERK		LBL_AZ + 1

// TODO: Cleanup resources since no longer using strings in string table for labels

void GetMinMax( double* thevalues, long nCount, double& dMin, double& dMax )
{
	dMin = 999999.;
	dMax = -999999.;

	if( !thevalues ) return;

	for( long i = 0; i < nCount; i++ )
	{
		if( i == 0 )
		{
			dMin = thevalues[ i ];
			dMax = thevalues[ i ];
		}
		else
		{
			if( thevalues[ i ] < dMin ) dMin = thevalues[ i ];
			if( thevalues[ i ] > dMax ) dMax = thevalues[ i ];
		}
	}
}

BYTE GetColorValue( double dMin, double dMax, BYTE nMin, BYTE nMax, double dVal )
{
	BYTE nC = 0;

	// if no data range, take average color
	if( dMin == dMax )
		nC = (BYTE)( 0.5 * ( nMin + nMax ) );
	// if no color range, take constant color
	else if( nMin == nMax )
		nC = nMin;
	else
	{
		// handle outliers
		if( dVal <= dMin )
			nC = nMin;
		else if( dVal >= dMax )
			nC = nMax;
		// all other cases
		else
			nC = nMin + (BYTE)( ( nMax - nMin ) * ( dVal - dMin ) / ( dMax - dMin ) );
	}

	return nC;
}

COLORREF ScaleColor( double dMin, double dMax, DWORD dwMinColor, DWORD dwMaxColor,
					 double dPrevVal, double dCurVal )
{
	// take average of previous & current values...if prev val not exist, just cur val
	double dVal = ( dPrevVal == NULLDATAVALUE ) ? dCurVal : ( 0.5 * ( dPrevVal + dCurVal ) );

	BYTE nR = GetColorValue( dMin, dMax, GetRValue( dwMinColor ), GetRValue( dwMaxColor ), dVal );
	if( nR < 0 ) nR = 0; else if( nR > 255 ) nR = 255;
	BYTE nG = GetColorValue( dMin, dMax, GetGValue( dwMinColor ), GetGValue( dwMaxColor ), dVal );
	if( nG < 0 ) nG = 0; else if( nG > 255 ) nG = 255;
	BYTE nB = GetColorValue( dMin, dMax, GetBValue( dwMinColor ), GetBValue( dwMaxColor ), dVal );
	if( nB < 0 ) nB = 0; else if( nB > 255 ) nB = 255;

	return RGB( nR, nG, nB );
}

/////////////////////////////////////////////////////////////////////////////
//

ShowTF::ShowTF() : m_fInit( FALSE ), x( NULL ), y( NULL ), z( NULL ), vx( NULL ), vy( NULL ),
	vabs( NULL ), ax( NULL ), ay( NULL ), jx( NULL ), jy( NULL ), jabs( NULL ), aspec( NULL ),
	aspecfilt( NULL ), m_dFrequency( 200 ), m_nCnt( 0 ), m_nCntSpectrum( 0 ), tseg( NULL ),
	tseg2( NULL ), m_nCntSeg( 0 ), m_nCntSeg2( 0 ), az( NULL ), s( NULL ), nS( -1 ), vs( NULL ), nVS( -1 ),
	st( NULL ), d( NULL ), vaspec( NULL ), vaspecfilt( NULL ), aaspec( NULL ), aaspecfilt( NULL ),
	m_nCntSpectrumV( 0 ), m_nCntSpectrumA( 0 ), nLA( -1 ), la( NULL )
{
	m_fOptimal = FALSE;
	m_fMonochrome = FALSE;
	m_fFeedback = FALSE;
	m_szFeature = _T("");
	m_MinColor = 0;
	m_MaxColor = 0;
	m_dMinVal = 0.;
	m_dMaxVal = 0.;
	m_fGraphOnly = FALSE;
	m_nRTFreq = 0;
	m_fSupressAnn = FALSE;
	m_fInverted = FALSE;
	m_f3D = FALSE;
	AxisX = eTF_COL_NONE;
	AxisY = eTF_COL_NONE;
	m_fStored = FALSE;

	// FORMERLY STATIC/GLOBAL VARS
	// index for real-time chart
	_Idx = 0;
	// pointer to feedback
	_theFB = NULL;
	// graph window
	_pGraph = NULL;
	// current stroke
	_nTFStroke = 0;
	// sub-movement analysis is on
	_fTFSubMovement = FALSE;
	// total segments
	_nTFTotalSegments = 0;
	// last pen pressure
	_nTFLastPenPressure = -1;
	// symbols
	m_nSymbolSeg = PEGAT_DOT;
	m_nSymbolSubMvmt = PEGAT_CROSS;

	m_fSpectrum = FALSE;
	m_fShowData = TRUE;
}

ShowTF::~ShowTF()
{
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;
	if( vx ) delete [] vx;
	if( vy ) delete [] vy;
	if( vabs ) delete [] vabs;
	if( ax ) delete [] ax;
	if( ay ) delete [] ay;
	if( az ) delete [] az;
	if( jx ) delete [] jx;
	if( jy ) delete [] jy;
	if( jabs ) delete [] jabs;
	if( aspec ) delete [] aspec;
	if( aspecfilt ) delete [] aspecfilt;
	if( vaspec ) delete [] vaspec;
	if( vaspecfilt ) delete [] vaspecfilt;
	if( aaspec ) delete [] aaspec;
	if( aaspecfilt ) delete [] aaspecfilt;
	if( tseg ) delete [] tseg;
	if( tseg2 ) delete [] tseg2;
	if( s ) delete [] s;
	if( vs ) delete [] vs;
	if( la ) delete [] la;
	if( st ) delete [] st;
	if( d ) delete [] d;
}

STDMETHODIMP ShowTF::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IShowTF,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

void ShowTF::Init()
{
	CString szLastTxt;

	// initialize vars
	_nTFStroke = 0;
	_fTFSubMovement = FALSE;
	_nTFTotalSegments = 0;
	_nTFLastPenPressure = -1;
	m_dMinVal = 0.;
	m_dMaxVal = 0.;
	_theFB = NULL;

	if( m_szTFFile.IsEmpty() )
	{
		::OutputMessage( _T("No data file specified."), LOG_GRAPH );
		return;
	}

	// EXT File (if exists)
	CStdioFile sf;
	BOOL fS = FALSE;
	CString szFileExt = m_szTFFile.Left( m_szTFFile.GetLength() - 3 );	// .TF removed
	int nTrial = atoi( szFileExt.Right( 2 ) );
	szFileExt = m_szTFFile.Left( m_szTFFile.GetLength() - 5 );	// ##.TF removed
	szFileExt += _T(".EXT");
	if( sf.Open( szFileExt, CFile::modeRead ) ) fS = TRUE;

	//////////////////////////////////////////////////////////////////////
	// TF Data
	//////////////////////////////////////////////////////////////////////
	FILE* fData = NULL;
	if( ( fopen_s( &fData, m_szTFFile, "r" ) ) != 0 )
	{
		::OutputMessage( _T("Error opening source data file."), LOG_GRAPH );
		return;
	}

	CString szTxt;
	int nCnt = 0, i = 0;
	m_nCnt = 0;
	m_nCntSpectrum = 0;
	m_nCntSpectrumV = 0;
	m_nCntSpectrumA = 0;
	m_szLabels.RemoveAll();

	nCnt = getdata( fData, &sec, 1, szTxt, 1 );
	if( sec == 0.0 )
	{
		fclose( fData );
		::OutputMessage( _T("Invalid sec parameter."), LOG_GRAPH );
		return;
	}
	nCnt = getdata( fData, &prsmin, 1, szTxt, 1 );
	nCnt = getdata( fData, &beta, 1, szTxt, 1 );

	double* dTemp = new double[ NSMAX ];
	// X Data
	nCnt = getdata( fData, dTemp, NSMAX, szTxt, 1 );
	szTxt.Trim();
	if( nCnt == 0 )
	{
		::OutputMessage( _T("No source data found to graph."), LOG_GRAPH );
		fclose( fData );
		return;
	}
	else if( nCnt == 1 )
	{
		fclose( fData );
		return;	// crashing...so just display nothing
	}
	// X Label
	m_szLabels.AddTail( szTxt );

	// Cleanup data holders
	if( x ) delete [] x;
	x = NULL;
	if( y ) delete [] y;
	y = NULL;
	if( z ) delete [] z;
	z = NULL;
	if( vx ) delete [] vx;
	vx = NULL;
	if( vy ) delete [] vy;
	vy = NULL;
	if( vabs ) delete [] vabs;
	vabs = NULL;
	if( ax ) delete [] ax;
	ax = NULL;
	if( ay ) delete [] ay;
	ay = NULL;
	if( az ) delete [] az;
	az = NULL;
	if( jx ) delete [] jx;
	jx = NULL;
	if( jy ) delete [] jy;
	jy = NULL;
	if( jabs ) delete [] jabs;
	jabs = NULL;
	if( aspec ) delete [] aspec;
	aspec = NULL;
	if( aspecfilt ) delete [] aspecfilt;
	aspecfilt = NULL;
	if( vaspec ) delete [] vaspec;
	vaspec = NULL;
	if( vaspecfilt ) delete [] vaspecfilt;
	vaspecfilt = NULL;
	if( aaspec ) delete [] aaspec;
	aaspec = NULL;
	if( aaspecfilt ) delete [] aaspecfilt;
	aaspecfilt = NULL;
	if( tseg ) delete [] tseg;
	tseg = NULL;
	if( tseg2 ) delete [] tseg2;
	tseg2 = NULL;
	if( s ) delete [] s;
	s = NULL;
	nS = -1;
	if( vs ) delete [] vs;
	vs = NULL;
	nVS = -1;
	if( la ) delete [] la;
	la = NULL;
	nLA = -1;
	if( st ) delete [] st;
	st = NULL;
	if( d ) delete [] d;
	d = NULL;

	// Construct data arrays based upon count received from file
	x = new double[ nCnt ];
	memset( x, 0, sizeof( double ) * nCnt );
	y = new double[ nCnt ];
	memset( y, 0, sizeof( double ) * nCnt );
	z = new double[ nCnt ];
	memset( z, 0, sizeof( double ) * nCnt );
	vx = new double[ nCnt ];
	memset( vx, 0, sizeof( double ) * nCnt );
	vy = new double[ nCnt ];
	memset( vy, 0, sizeof( double ) * nCnt );
	vabs = new double[ nCnt ];
	memset( vabs, 0, sizeof( double ) * nCnt );
	ax = new double[ nCnt ];
	memset( ax, 0, sizeof( double ) * nCnt );
	ay = new double[ nCnt ];
	memset( ay, 0, sizeof( double ) * nCnt );
	az = new double[ nCnt ];
	memset( az, 0, sizeof( double ) * nCnt );
	jx = new double[ nCnt ];
	memset( jx, 0, sizeof( double ) * nCnt );
	jy = new double[ nCnt ];
	memset( jy , 0, sizeof( double ) * nCnt );
	jabs = new double[ nCnt ];
	memset( jabs, 0, sizeof( double ) * nCnt );
	s = new double[ NSEGMAX ];
	memset( s, 0, sizeof( double ) * NSEGMAX );
	vs = new double[ NSEGMAX ];
	memset( vs, 0, sizeof( double ) * NSEGMAX );
	la = new double[ NSEGMAX ];
	memset( la, 0, sizeof( double ) * NSEGMAX );
	st = new double[ NSEGMAX ];
	memset( st, 0, sizeof( double ) * NSEGMAX );
	d = new double[ NSEGMAX ];
	memset( d, 0, sizeof( double ) * NSEGMAX );

	m_nCnt = nCnt;

	// Assign X data
	for( i = 0; i < nCnt; i++ ) 
		x[ i ] = dTemp[ i ];
	delete [] dTemp;
	if( m_szFeature == szTxt ) _theFB = x;

	// Y
	nCnt = getdata( fData, y, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = y;

	// Z
	nCnt = getdata( fData, z, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = z;

	dTemp = new double[ NFREQMAX ];
	// X or Position spectrum
	nCnt = getdata( fData, dTemp, NFREQMAX, szTxt, 1 );
	// verify that we have the word "Spectrum" in the label
	if( szTxt.Find( _T("Spectrum") ) == -1 )
	{
		AfxMessageBox( _T("ERROR: ShowMod - ShowTF: No Spectra data found.") );
		return;
	}
	szTxt.Trim();
	szLastTxt = szTxt;
	if( nCnt > 0 )
	{
		m_szLabels.AddTail( szTxt );
		aspec = new double[ nCnt ];
		ZeroMemory( aspec, nCnt * sizeof( double ) );
		m_nCntSpectrum = nCnt;
		for( int j = 3; j < nCnt; j++ )
			aspec[ j ] = dTemp[ j ];
	}
	// Y or velocity Spectrum
	nCnt = getdata( fData, dTemp, NFREQMAX, szTxt, 1 );
	// if this label contains "Filtered" in it, then it's the old TF version prior to July 08
	if( szTxt.Find( _T("Filtered") ) != -1 )
	{
		// inform user
		AfxMessageBox( _T("The processed data is in an old format.\nYou will need to reprocess the data.") );

		// we need to move the data to the appropriate array if not the position spectrum
		if( szLastTxt == _T("V_Spectrum") )
		{
			// create velocity spectrum array
			vaspec = new double[ nCnt ];
			// fill vel spec array with pos spec array data
			for( int j = 0; j < nCnt; j++ )
				vaspec[ j ] = aspec[ j ];
			// delete pos spec array data as it is no longer needed
			delete [] aspec;
			aspec = NULL;
			// create & fill filtered vel spec array with newly read data
			vaspecfilt = new double[ nCnt ];
			for( int j = 0; j < nCnt; j++ )
				vaspecfilt[ j ] = dTemp[ j ];
			// update the count vars
			m_nCntSpectrumV = m_nCntSpectrum;
			m_nCntSpectrum = 0;
		}
		else if( szLastTxt == _T("A_Spectrum") )
		{
			// create accel spectrum array
			aaspec = new double[ nCnt ];
			// fill acc spec array with pos spec array data
			for( int j = 0; j < nCnt; j++ )
				aaspec[ j ] = aspec[ j ];
			// delete pos spec array data as it is no longer needed
			delete [] aspec;
			aspec = NULL;
			// create & fill filtered acc spec array with newly read data
			aaspecfilt = new double[ nCnt ];
			for( int j = 0; j < nCnt; j++ )
				aaspecfilt[ j ] = dTemp[ j ];
			// update the count vars
			m_nCntSpectrumA = m_nCntSpectrum;
			m_nCntSpectrum = 0;
		}
		// else, it's position, so just fill the filt pos spec array
		else
		{
			aspecfilt = new double[ nCnt ];
			for( int j = 0; j < nCnt; j++ )
				aspecfilt[ j ] = dTemp[ j ];
		}
		// we're done collecting spectra data, go to vel data
		goto OldTF;
	}
	szTxt.Trim();
	if( nCnt > 0 )
	{
		m_szLabels.AddTail( szTxt );
		vaspec = new double[ nCnt ];
		m_nCntSpectrumV = nCnt;
		for( int j = 0; j < nCnt; j++ )
			vaspec[ j ] = dTemp[ j ];
	}
	// Z or accel spectrum
	nCnt = getdata( fData, dTemp, NFREQMAX, szTxt, 1 );
	szTxt.Trim();
	if( nCnt > 0 )
	{
		m_szLabels.AddTail( szTxt );
		aaspec = new double[ nCnt ];
		m_nCntSpectrumA = nCnt;
		for( int j = 0; j < nCnt; j++ )
			aaspec[ j ] = dTemp[ j ];
	}
	// X/Pos filtered spectrum
	nCnt = getdata( fData, dTemp, NFREQMAX, szTxt, 1 );
	szTxt.Trim();
	if( nCnt > 0 )
	{
		m_szLabels.AddTail( szTxt );
		aspecfilt = new double[ nCnt ];
		ZeroMemory( aspecfilt, nCnt * sizeof( double ) );
		for( int j = 3; j < nCnt; j++ )
			aspecfilt[ j ] = dTemp[ j ];
	}
	// Y/Vel filtered spectrum
	nCnt = getdata( fData, dTemp, NFREQMAX, szTxt, 1 );
	// if this label does not contain "Spectrum" in it, then it's old gripper data prior to July 08
	if( szTxt.Find( _T("Spectrum") ) == -1 )
	{
		// inform user
		AfxMessageBox( _T("The processed data is in an old format.\nYou will need to reprocess the data.") );

		// we need to move the data to the appropriate array if not the position spectrum
		if( szLastTxt == _T("V_Spectrum") )
		{
			// create & fill filtered vel spec array with newly read data
			vaspecfilt = new double[ m_nCntSpectrumV ];
			for( int j = 0; j < m_nCntSpectrumV; j++ )
				vaspecfilt[ j ] = aspecfilt[ j ];
			// remove unused filt pos spec array
			delete [] aspecfilt;
			aspecfilt = NULL;
		}
		else if( szLastTxt == _T("A_Spectrum") )
		{
			// create & fill filtered acc spec array with newly read data
			aaspecfilt = new double[ m_nCntSpectrumA ];
			for( int j = 0; j < m_nCntSpectrumA; j++ )
				aaspecfilt[ j ] = aspecfilt[ j ];
			// remove unused filt pos spec array
			delete [] aspecfilt;
			aspecfilt = NULL;
		}
		// else, do nothing

		// we're done collecting spectra data, go to vel data
		goto OldTF;
	}
	szTxt.Trim();
	if( nCnt > 0 )
	{
		m_szLabels.AddTail( szTxt );
		vaspecfilt = new double[ nCnt ];
		for( int j = 0; j < nCnt; j++ )
			vaspecfilt[ j ] = dTemp[ j ];
	}
	// Z/Accel filtered spectrum
	nCnt = getdata( fData, dTemp, NFREQMAX, szTxt, 1 );
	szTxt.Trim();
	if( nCnt > 0 )
	{
		m_szLabels.AddTail( szTxt );
		aaspecfilt = new double[ nCnt ];
		for( int j = 0; j < nCnt; j++ )
			aaspecfilt[ j ] = dTemp[ j ];
	}
	/////
OldTF:
	delete [] dTemp;

	// VX
	nCnt = getdata( fData, vx, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = vx;

	// VY
	nCnt = getdata( fData, vy, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = vy;

	/* needed for secondary */
	// vabs (vz)
	nCnt = getdata( fData, vabs, m_nCnt, szTxt, 1 );	// also vz
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = vabs;

	// AX
	nCnt = getdata( fData, ax, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = ax;

	// AY
	nCnt = getdata( fData, ay, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = ay;

	// AZ
	nCnt = getdata( fData, az, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	m_szLabels.AddTail( szTxt );
	if( m_szFeature == szTxt ) _theFB = az;

	// Jerk (might or might not be generated...depending upon exp settings)
	int nCntTmp = getdata( fData, jx, m_nCnt, szTxt, 1 );
	szTxt.Trim();
	if( nCntTmp > 0 )
	{
		m_szLabels.AddTail( szTxt );
		if( m_szFeature == szTxt ) _theFB = jx;
		nCntTmp = getdata( fData, jy, m_nCnt, szTxt, 1 );
		szTxt.Trim();
		if( m_szFeature == szTxt ) _theFB = jy;
		nCntTmp = getdata( fData, jabs, m_nCnt, szTxt, 1 );
		szTxt.Trim();
		if( m_szFeature == szTxt ) _theFB = jabs;
	}
	else
	{
		for( int j = 0; j < nCnt; j++ )
			jx[ j ] = jy[ j ] = jabs[ j ] = 0.0;
	}

	fclose( fData );

	//////////////////////////////////////////////////////////////////////
	// SEG Data
	//////////////////////////////////////////////////////////////////////

	if( !m_szSegFile.IsEmpty() )
	{
		if( ( fopen_s( &fData, m_szSegFile, "r" ) ) == 0 )
		{
			dTemp = new double[ NSEGMAX ];
			m_nCntSeg = getdata( fData, dTemp, NSEGMAX, szTxt, 1 );
			if( m_nCntSeg > 0 ) tseg = new double[ m_nCntSeg ];
			/* convert tseg inseconds back to sample nrs */ 
			for( i = 0; i < m_nCntSeg; i++ )
				tseg[ i ] = dTemp[ i ] / sec;

			// second strokes (if applicable)
			m_nCntSeg2 = getdata( fData, dTemp, NSEGMAX, szTxt, 1 );
			if( m_nCntSeg2 > 0 ) tseg2 = new double[ m_nCntSeg2 ];
			/* convert tseg inseconds back to sample nrs */ 
			for( i = 0; i < m_nCntSeg2; i++ )
				tseg2[ i ] = dTemp[ i ] / sec;

			fclose( fData );

			delete [] dTemp;
		}
		else ::OutputMessage( _T("Error opening segment data file."), LOG_GRAPH );
	} else ::OutputMessage( _T("No segment data available."), LOG_GRAPH );

	// min/max of feature feedback
	::GetMinMax( _theFB, nCnt, m_dMinVal, m_dMaxVal );

	// extracted data
	int nST = -1, nD = -1, nPos = 0, nSegment = -1;
	CString szLine;
	BOOL fContST = TRUE, fContD = TRUE, fContS = TRUE, fContSeg = TRUE, fContVS = TRUE, fContLA = TRUE;
	if( fS )
	{
		CString szTemp;

		// 1st line = labels
		sf.ReadString( szLine );
		// find slant (dynamic later) index (NOW JERK)
		szLine.MakeUpper();
		nPos = szLine.Find( _T(" ") );
		while( nPos != -1 )
		{
			szTemp = szLine.Left( nPos );

			// start time
			if( fContST ) nST++;
			if( szTemp == _T("STARTTIME") ) fContST = FALSE;

			// duration
			if( fContD ) nD++;
			if( szTemp == _T("DURATION") ) fContD = FALSE;

			// slant (other features later) (NOW JERK)
			if( fContS ) nS++;
//			if( szTemp == _T("SLANT") ) fContS = FALSE;
			if( szTemp == _T("NORMALIZEDJERK") )
				fContS = FALSE;

			// vertical size
			if( fContVS ) nVS++;
			if( szTemp == _T("VERTICALSIZE") ) fContVS = FALSE;

			// loop area
			if( fContLA ) nLA++;
			if( szTemp == _T("LoopSurfaceACE") ) fContLA = FALSE;

			if( !fContST && !fContD && !fContS && !fContVS && !fContLA ) break;

			szLine = szLine.Mid( nPos + 1 );
			nPos = szLine.Find( _T(" ") );
		}
		// get data
		double	dSTLast = 0.;	// used to determine submovement analysis _fSubMovement
		int nCnt = 0;
		BOOL fSet = FALSE, fFound = FALSE;
		if( ( nST >= 0 ) && ( nD >= 0 ) && ( nS >= 0 ) && ( nVS >= 0 ) && ( nLA >= 0 ) )
		{
			int nSel = 0, nT = 0, nSeg = 0;
			CString szL;
			BOOL fCont = TRUE;
			while( sf.ReadString( szLine ) )
			{
				szL = szLine;
				fContST = TRUE; fContD = TRUE; fContS = TRUE; fContVS = TRUE; fContLA = TRUE;
				nSel = -1;
				nPos = szLine.Find( _T(" ") );
				fCont = TRUE;
				while( ( nPos != -1 ) && fCont )
				{
					nSel++;
					szTemp = szLine.Left( nPos );

					// 1st is trial # - must match with nTrial from above
					if( nSel == 0 ) nT = atoi( szTemp );

					// 2nd is segment # (used only for checking against appended processing)
					if( nSel == 1 ) nSeg = atoi( szTemp );

					// continue only if proper trial
					if( nT == nTrial )
					{
						if( nCnt < NSEGMAX )
						{
							fFound = TRUE;

							// just in case a trial was reprocessed individually and appended
// 29Apr09: GMB: Added check for segment 1 in addeition to checking if another trial had been detected
//				 in order to reset the counter for max seg. This should work since each new trial or
// 				 new appended trial of the same will have a segment 1 only once. Submovement analysis
// 				 is handled due to reset of nSeg to -1 after first realization.
//							if( fSet )
							if( fSet || ( nSeg == 1 ) )
							{
								nSeg = -1;
								nCnt = 0;
								fSet = FALSE;
								dSTLast = 0.;
							}

							// start time
							if( nSel == nST )
							{
								st[ nCnt ] = atof( szTemp );
								fContST = FALSE;

								// **HACK** - simple hack to check if submovement analysis is on
								// If on, start time will have a value x for primary stroke,
								// and x+y for secondary stroke, and value x for third
								// so if starttime is ever less than previous, sub-mvmt anal is on
								if( !_fTFSubMovement && ( st[ nCnt ] < dSTLast ) ) _fTFSubMovement = TRUE;
								else dSTLast = st[ nCnt ];
							}

							// duration
							if( nSel == nD )
							{
								d[ nCnt ] = atof( szTemp );
								fContD = FALSE;
							}

							// slant (other features later) (NOW JERK)
							if( nSel == nS )
							{
								s[ nCnt ] = atof( szTemp );
								fContS = FALSE;
							}

							// vertical size
							if( nSel == nVS )
							{
								vs[ nCnt ] = atof( szTemp );
								fContVS = FALSE;
							}

							// loop area
							if( nSel == nLA )
							{
								la[ nCnt ] = atof( szTemp );
								fContLA = FALSE;
							}

							szLine = szLine.Mid( nPos + 1 );

							// increment segment count
							if( !fContST && !fContD && !fContS && !fContVS && !fContLA )
							{
								nCnt++;
								fCont = FALSE;
							}
						}
						else
						{
							CString szMsg;
							szMsg.Format( _T("Total # of segmentation points has exceeded maximum (%d).\nThis is likely due to a corrupt EXT file. Reprocess the trials and try again.\nIf this problem continues to occur, contact NeuroScript."), NSEGMAX );
							::MessageBox( NULL, szMsg, _T("Segmentation Error"), MB_OK );
							return;
						}
					}
					else
					{
						if( fFound ) fSet = TRUE;
						fCont = FALSE;
					}

					if( !fContST && !fContD && !fContS && !fContVS && !fContLA ) break;

					nPos = szLine.Find( _T(" ") );
				}
			}
			_nTFTotalSegments = nCnt;
		}
		else nS = -1;
	}

	//////////////////////////////////////////////////////////////////////

	m_fInit = TRUE;
}

STDMETHODIMP ShowTF::PrintGraph(VARIANT* PEHandle)
{
	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;

	BOOL fRet = PElaunchprintdialog( *hPE, TRUE, NULL );

	return S_OK;
}

STDMETHODIMP ShowTF::ExportGraph(VARIANT* PEHandle)
{
	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;

	BOOL fRet = PElaunchexport( *hPE );

	return S_OK;
}

STDMETHODIMP ShowTF::ShowTFGraph(VARIANT *GraphWindow, TFGraph GraphType, VARIANT* GraphDlg,
								 BOOL AnnValues, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	if( !m_fInit ) Init();
	if( !m_fInit ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	_pGraph = pGraph;
	CWnd* pGraphDlg = (CWnd*)GraphDlg;
	_AnnValues = AnnValues;

	BOOL fRet = FALSE;
	m_f3D = FALSE;
	m_fSpectrum = FALSE;
	if( ( GraphType & TF_3D ) == TF_3D )
	{
		m_f3D = TRUE;
		GraphType = (TFGraph)( GraphType - eTF_3D );
		if( GraphType == TF_XY )
			fRet = ShowXY3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_XZ )
			fRet = ShowXZ3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_YZ )
			fRet = ShowYZ3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_Xt )
			fRet = ShowXt3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_Yt )
			fRet = ShowYt3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_Zt )
			fRet = ShowZt3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_JXt )
			fRet = ShowJXt3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_JYt )
			fRet = ShowJYt3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_JABSt )
			fRet = ShowJABSt3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_vx )
			fRet = ShowVX3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_vy )
			fRet = ShowVY3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_vz )
			fRet = ShowVZ3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_ax )
			fRet = ShowAX3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_ay )
			fRet = ShowAY3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_az )
			fRet = ShowAZ3D( pGraph, pGraphDlg, (HWND*)PEHandle );
		else if( GraphType == TF_vabs )
			fRet = ShowVABS3D( pGraph, pGraphDlg, (HWND*)PEHandle );
	}
	else if( GraphType == TF_XY )
		fRet = ShowXY( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_XZ )
		fRet = ShowXZ( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_YZ )
		fRet = ShowYZ( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_vz )
		fRet = ShowVZ( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_az )
		fRet = ShowAZ( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( ( GraphType == TF_aspec ) || ( GraphType == TF_vaspec ) || ( GraphType == TF_aaspec ) )
		fRet = ShowAspec( pGraph, pGraphDlg, (HWND*)PEHandle, GraphType );
	else if( GraphType == TF_Xt )
		fRet = ShowXt( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_Yt )
		fRet = ShowYt( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_Zt )
		fRet = ShowZt( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_vx )
		fRet = ShowVX( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_vy )
		fRet = ShowVY( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_vabs )
		fRet = ShowVABS( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_ax )
		fRet = ShowAX( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_ay )
		fRet = ShowAY( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_JXt )
		fRet = ShowJXt( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_JYt )
		fRet = ShowJYt( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == TF_JABSt )
		fRet = ShowJABSt( pGraph, pGraphDlg, (HWND*)PEHandle );

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowTF::ShowTFGraph2(VARIANT *GraphWindow, TFAxis XAxis, TFAxis YAxis,
								  VARIANT* GraphDlg, BOOL AnnValues, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	AxisX = XAxis;
	if( ( AxisX <= eTF_COL_NONE ) || ( AxisX > eTF_COL_Time ) ) return E_FAIL;
	AxisY = YAxis;
	if( ( AxisY <= eTF_COL_NONE ) || ( AxisY >= eTF_COL_Time ) ) return E_FAIL;

	if( !m_fInit ) Init();
	if( !m_fInit ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	_pGraph = pGraph;
	CWnd* pGraphDlg = (CWnd*)GraphDlg;
	_AnnValues = AnnValues;

	BOOL fRet = ShowCustom( pGraph, pGraphDlg, (HWND*)PEHandle );
	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowTF::put_TF_InFile(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_szTFFile = newVal;
	m_fInit = FALSE;

	return S_OK;
}

STDMETHODIMP ShowTF::put_SEG_InFile(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_szSegFile = newVal;

	return S_OK;
}

STDMETHODIMP ShowTF::put_Frequency(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dFrequency = newVal;

	return S_OK;
}

BOOL ShowTF::InitChart( HWND* hPE, CWnd* pGraph, CWnd* pGraphDlg, UINT nType )
{
	if( !hPE || !pGraph ) return FALSE;

	pGraph->GetClientRect( &_tfd.ScreenRectangle );

	HGLOBAL hGlobal = 0;
	DWORD dwSize = 0;
	void FAR* lpData = NULL;
	m_fStored = FALSE;
	if( *hPE )
	{
		PEstorepartial( *hPE, &hGlobal, &dwSize );
		m_fStored = TRUE;
		PEdestroy( *hPE );
	}
	*hPE = PEcreate( nType, WS_VISIBLE, &_tfd.ScreenRectangle,
					 pGraphDlg ? pGraphDlg->m_hWnd : pGraph->m_hWnd, 1001 );
	if( !*hPE )
	{
		::OutputMessage( _T("Error creating graphing COM/DCOM object."), LOG_GRAPH );
		return FALSE;
	}

	PEreset( pGraph->m_hWnd );
	PEnset( *hPE, PEP_bPREPAREIMAGES, TRUE );
	double dNull = -999999.99;
	if( pGraphDlg ) PEvset(*hPE, PEP_fNULLDATAVALUEX, &dNull, 1);
	PEvset(*hPE, PEP_fNULLDATAVALUE, &dNull, 1);

	if( m_fStored )
	{
		PEloadpartial( *hPE, &hGlobal );
		GlobalFree( hGlobal );
		PEreinitialize( *hPE );
		PEresetimage( *hPE, 0, 0 );
	}

	return TRUE;
}

void ShowTF::DoLabels( HWND* hPE, CString szX, CString szY, CString szTitle, CString szSubTitle )
{
	if( !hPE || !*hPE ) return;

	if( m_fGraphOnly )
	{
		szTitle = _T("");
		szSubTitle = _T("");
		PEnset(*hPE, PEP_nSHOWYAXIS, PESA_EMPTY );
		PEnset(*hPE, PEP_nSHOWXAXIS, PESA_EMPTY );
	}

	// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
	char lbl[ 50 ];
	strcpy_s( lbl, _T("") );
	PEszset(*hPE, PEP_szMAINTITLE, lbl);
	if( m_fShowData ) strcpy_s( lbl, 50, szTitle );
	PEszset(*hPE, PEP_szSUBTITLE, lbl);
	strcpy_s( lbl, 50, szY );
	PEszset(*hPE, PEP_szYAXISLABEL, lbl);
	strcpy_s( lbl, 50, szX );
	PEszset(*hPE, PEP_szXAXISLABEL, lbl);
	PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
	PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szLABELFONT, "Arial");
	PEszset(*hPE, PEP_szTABLEFONT, "Arial");
}

void ShowTF::SetChartDefaults( HWND* hPE )
{
	if( !hPE || !*hPE ) return;

	// Set colors
	// option to not show bounding rectangle
	if( m_fGraphOnly ) PEnset(*hPE, PEP_dwGRAPHFORECOLOR, BGCOLOR);
	else PEnset(*hPE, PEP_dwGRAPHFORECOLOR, FORECOLOR);
	PEnset(*hPE, PEP_dwSHADOWCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, BGCOLOR);

	// Set DataShadows to none
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	// plotting methods
	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	if( !m_fGraphOnly ) PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);
    PEnset(*hPE, PEP_bSCROLLINGHORZZOOM, TRUE);
	PEnset(*hPE, PEP_bSCROLLINGVERTZOOM, TRUE);

	// fixed font for scaling
	PEnset(*hPE, PEP_bFIXEDFONTS, TRUE);

	// color
	_tfd.col = m_fMonochrome ? MONOCOLOR : LINECOLOR;
	PEvset(*hPE, PEP_dwaSUBSETCOLORS, &_tfd.col, 1);
	
	// line type
	if( m_fGraphOnly ) _tfd.symbol = PELT_EXTRATHICKSOLID;
	else _tfd.symbol = PELT_THINSOLID;
	PEvset(*hPE, PEP_naSUBSETLINETYPES, &_tfd.symbol, 1);

	// point type
	_tfd.symbol = PEPT_DOTSOLID;
	PEvset(*hPE, PEP_naSUBSETPOINTTYPES, &_tfd.symbol, 1);

	// When set to no manual PEreinitialize delivers the min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLZ, PEMSC_NONE);
	PEreinitialize( *hPE );

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLZ, PEMSC_MINMAX);

	// Get min and max of x and y data
	PEvget(*hPE, PEP_fMANUALMINX, &_tfd.dataxmin);
	PEvget(*hPE, PEP_fMANUALMAXX, &_tfd.dataxmax);
	PEvget(*hPE, PEP_fMANUALMINY, &_tfd.dataymin);
	PEvget(*hPE, PEP_fMANUALMAXY, &_tfd.dataymax);
	PEvget(*hPE, PEP_fMANUALMINZ, &_tfd.datazmin);
	PEvget(*hPE, PEP_fMANUALMAXZ, &_tfd.datazmax);
		
	// Calculate the real data range
	_tfd.dataxrange = _tfd.dataxmax - _tfd.dataxmin;
	_tfd.datayrange = _tfd.dataymax - _tfd.dataymin;
	_tfd.datazrange = _tfd.datazmax - _tfd.datazmin;

	// Test for zero data ranges.
	if (_tfd.dataxrange == 0.) _tfd.dataxrange = 1.;
	if (_tfd.datayrange == 0.) _tfd.datayrange = 1.;
	if (_tfd.datazrange == 0.) _tfd.datazrange = 1.;

	// Get the screen positions. 
	PEvget (*hPE, PEP_rectGRAPH, &_tfd.ScreenRectangle); 

	// Calculate data to pixel scales ant take smallest scale for x and y so that
	// the graph fits
	_tfd.scalex = (_tfd.ScreenRectangle.right - _tfd.ScreenRectangle.left) / _tfd.dataxrange;
	_tfd.scaley = (_tfd.ScreenRectangle.bottom - _tfd.ScreenRectangle.top) / _tfd.datayrange;
	_tfd.scalez = (_tfd.ScreenRectangle.bottom - _tfd.ScreenRectangle.top) / _tfd.datazrange;
	_tfd.scale = ( m_fInverted || !m_f3D ) ? MIN(_tfd.scalex, _tfd.scaley ) : MIN(_tfd.scalex, _tfd.scalez );

	// Test for zero scale.
	if (_tfd.scale == 0.) _tfd.scale = 1.;

	// Estimate the extra data space for x or y data needed to assure that a square remains a square
	_tfd.extradataxrange = 0.;
	_tfd.extradatayrange = 0.;
	_tfd.extradatazrange = 0.;
	if( !m_fOptimal )	// ie..proportional
	{
		_tfd.extradataxrange = _tfd.dataxrange * (_tfd.scalex / _tfd.scale - 1.);
		if( m_fInverted || !m_f3D )
			_tfd.extradatayrange = _tfd.datayrange * (_tfd.scaley / _tfd.scale - 1.);
		else
			_tfd.extradatazrange = _tfd.datazrange * (_tfd.scalez / _tfd.scale - 1.);
	}
	// else independent optimal x & y scaling

    // Divide the extra data space so that the graph is in the middle of the window
	_tfd.dataxminnew = _tfd.dataxmin - .5 * _tfd.extradataxrange;
	_tfd.dataxmaxnew = _tfd.dataxmax + .5 * _tfd.extradataxrange;
	_tfd.datayminnew = _tfd.dataymin - .5 * _tfd.extradatayrange;
	_tfd.dataymaxnew = _tfd.dataymax + .5 * _tfd.extradatayrange;
	_tfd.datazminnew = _tfd.datazmin - .5 * _tfd.extradatazrange;
	_tfd.datazmaxnew = _tfd.datazmax + .5 * _tfd.extradatazrange;

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &_tfd.dataxminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &_tfd.dataxmaxnew, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &_tfd.datayminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &_tfd.dataymaxnew, 1);
	PEvset(*hPE, PEP_fMANUALMINZ, &_tfd.datazminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXZ, &_tfd.datazmaxnew, 1);

    //** Set up cursor **
    if( !m_fGraphOnly ) PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);
    
    //** Help see data points **
    //PEnset(*hPE, PEP_bMARKDATAPOINTS, TRUE);
    
    //** This will allow you to move cursor by clicking data point **
	if( !m_fGraphOnly )
	{
		PEnset(*hPE, PEP_bMOUSECURSORCONTROL, TRUE);
		PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, TRUE);
	    
		//** Cursor prompting in top left corner **
		PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
		PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);
	}
}

BOOL ShowTF::ShowByTime( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, double* vals, BOOL fOnlyColors )
{
	if( !vals ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// annotation table
	DWORD dwColor;
	double* xSeg = NULL, *ySeg = NULL, *xSeg2 = NULL, *ySeg2 = NULL;
	int i = 0;
	dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 80);

	if( !m_fSupressAnn )
	{
		// Segment Data
		if( m_nCntSeg > 0 )		// Including SEG data
		{
			PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
			PEnset(*hPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);
			PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
			PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);

			// Interpolate points
			xSeg = new double[ m_nCntSeg ];
			ySeg = new double[ m_nCntSeg ];
			if( tseg2 )
			{
				xSeg2 = new double[ m_nCntSeg2 ];
				ySeg2 = new double[ m_nCntSeg2 ];
			}
			for( i = 0; i < m_nCntSeg; i++ )
			{
				xSeg[ i ] = x[ (int)tseg[ i ] ];
				ySeg[ i ] = y[ (int)tseg[ i ] ];
			}
			for( i = 0; i < m_nCntSeg2; i++ )
			{
				xSeg2[ i ] = x[ (int)tseg2[ i ] ];
				ySeg2[ i ] = y[ (int)tseg2[ i ] ];
			}
		}
	}

	// Set number of Subsets and Points //
	PEnset( *hPE, PEP_nSUBSETS, 1 );			// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );		// number of data points

	// feedback colorization
	dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
	DWORD* dwColors = NULL, dwC = 0;
	if( m_fFeedback )
	{
		dwColors = new DWORD[ m_nCnt ];
		memset( dwColors, 0, m_nCnt * sizeof( DWORD ) );
	}

	for( int p = 0; p < m_nCnt; p++ )
	{
		_tfd.fX = (float)( p * sec );
		PEvsetcellEx( *hPE, PEP_faXDATA, 0, p, &_tfd.fX );
		_tfd.fY = (float)vals[ p ];
		if( vals[ p ] == ::GetMissingDataValue() ) _tfd.fY = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faYDATA, 0, p, &_tfd.fY );

		// sample/pressure/stroke/slant annotation table (NOW JERK)
		if( ( p == 0 ) && pGraphDlg )
		{
			char szTblData[ TBL_DATA_SIZE ];
			int nDeg = 0;
			sprintf_s( szTblData, PETAL_STR, 0, _nTFStroke, 0, (float)z[ p ], (double)nDeg, (double)nDeg, (double)nDeg );
			PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
		}

		// feedback colorization
		if( m_fFeedback && _theFB )
		{
			dwC = m_fMonochrome ? MONOCOLOR :
								  (DWORD)::ScaleColor( m_dMinVal, m_dMaxVal, m_MinColor, m_MaxColor, _theFB[ p ],
									( p > 0 ) ? _theFB[ p - 1 ] : NULLDATAVALUE );
			dwColors[ p ] = dwC;
		}
		else
		{
			if( _nTFLastPenPressure <= ::GetMinPenPressure() )
				dwColor = m_dwPenUpColor;
			else
				dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
		}
		_nTFLastPenPressure = (int)z[ p ];

		if( !m_fFeedback ) PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, p, &dwColor );
	}

	// Seg data
	_tfd.symbol = 0;
	_tfd.symbol2 = 0;
	_tfd.col = m_fMonochrome ? MONOCOLOR : LINE2COLOR;
	_tfd.col2 = m_fMonochrome ? MONOCOLOR : MONOCOLOR;
	_tfd.annPt = 0;
	if( !m_fGraphOnly && !m_fSupressAnn )
	{
		for( i = 0; i < m_nCntSeg; i++ )
		{
			_tfd.dX1 = (tseg[ i ]) * sec;
			_tfd.dY1 = rnpol( vals, NSMAX, tseg[ i ] );

			if( ( _tfd.dX1 < 999999 ) && ( _tfd.dX1 > -999999 ) &&
				( _tfd.dY1 < 999999 ) && ( _tfd.dY1 > -999999 ) )
			{
				_tfd.symbol = m_nSymbolSeg;
				sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX1, _tfd.dY1 );

				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX1);
				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY1);
				PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol);
				PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col);
				if( _AnnValues )
					PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

				_tfd.annPt++;
			}
		}
		for( i = 0; i < m_nCntSeg2; i++ )
		{
			_tfd.dX2 = (tseg2[ i ]) * sec;
			_tfd.dY2 = rnpol( vals, NSMAX, tseg2[ i ] );

			if( ( _tfd.dX2 < 999999 ) && ( _tfd.dX2 > -999999 ) &&
				( _tfd.dY2 < 999999 ) && ( _tfd.dY2 > -999999 ) )
			{
				_tfd.symbol2 = m_nSymbolSubMvmt;
				sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX2, _tfd.dY2 );

				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX2);
				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY2);
				PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol2);
				PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col2);
				if( _AnnValues )
					PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

				_tfd.annPt++;
			}
		}
	}

	// horizontal line annotation
	double fYLoc = 0.;
	PEvset( *hPE, PEP_faHORZLINEANNOTATION, &fYLoc, 1 );
	int nHType = PELT_THINSOLID;
	PEvset( *hPE, PEP_naHORZLINEANNOTATIONTYPE, &nHType, 1 );
	DWORD dwData = RGB( 128, 128, 128 );
	PEvset( *hPE, PEP_dwaHORZLINEANNOTATIONCOLOR, &dwData, 1 );
	int nAnnotationAxis = 0;
	PEvset( *hPE, PEP_naHORZLINEANNOTATIONAXIS, &nAnnotationAxis, 1 );

	// apply feedback colors
	if( m_fFeedback ) PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, m_nCnt );

	if( !fOnlyColors )
	{
		// Labels
		int nPos = m_szTFFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szTFFile.Mid( nPos + 1 ) : m_szTFFile;
		strcpy_s( _tfd.label, 50, szTitle );
		PEszset(*hPE, PEP_szMAINTITLE, _T(""));

		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		CString szLblX = _T("Time (s)");
		m_szSubTitle.Format( _T("%s vs. %s"), m_szYAxis, szLblX );
		strcpy_s( _tfd.label, 50, szTitle );
		PEszset(*hPE, PEP_szSUBTITLE, _tfd.label);
		strcpy_s( _tfd.label, 50, m_szYAxis );
		PEszset(*hPE, PEP_szYAXISLABEL, _tfd.label);
		strcpy_s( _tfd.label, 50, szLblX );
		PEszset(*hPE, PEP_szXAXISLABEL, _tfd.label);
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// Set defaults
		SetChartDefaults( hPE );
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// in case cursor was already in position
	OnCursorMove( (VARIANT*)hPE, eTF_XY );

	// Cleanup
	if( xSeg ) delete [] xSeg;
	if( ySeg ) delete [] ySeg;
	if( xSeg2 ) delete [] xSeg2;
	if( ySeg2 ) delete [] ySeg2;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowTF::ShowAvsB( double* a, double* b, int nX, CString szX, int nY, CString szY,
					   CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	if( !a || !b ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// annotation table
	DWORD dwColor;
	if( pGraphDlg )
	{
		dwColor = RGB( 0, 0, 0 );
		PEnset(*hPE, PEP_nWORKINGTABLE, 0);
		PEnset(*hPE, PEP_nTAROWS, 1);
		PEnset(*hPE, PEP_nTACOLUMNS, 1);
		PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
		int nTACW = 25;
		PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
		PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
		PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
		PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
		PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
		PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
		PEnset(*hPE, PEP_nTAHEADERROWS, 0);
		PEnset(*hPE, PEP_nTATEXTSIZE, 80);
	}

	// Segment Data
	double* xSeg = NULL, *ySeg = NULL, *xSeg2 = NULL, *ySeg2 = NULL;
	int i = 0;
	if( !m_fSupressAnn )
	{
		if( m_nCntSeg > 0 )		// Including SEG data
		{
			PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
			PEnset(*hPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);
			PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
			PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);

			// Interpolate points
			xSeg = new double[ m_nCntSeg ];
			ySeg = new double[ m_nCntSeg ];
			if( tseg2 )
			{
				xSeg2 = new double[ m_nCntSeg2 ];
				ySeg2 = new double[ m_nCntSeg2 ];
			}
			for( i = 0; i < m_nCntSeg; i++ )
			{
				if( (int)tseg[i] < m_nCnt ) {
					xSeg[ i ] = a[ (int)tseg[ i ] ];
					ySeg[ i ] = b[ (int)tseg[ i ] ];
				}
				else xSeg[i] = ySeg[i] = 0.;
			}
			for( i = 0; i < m_nCntSeg2; i++ )
			{
				if( (int)tseg2[i] < m_nCnt ) {
					xSeg2[ i ] = a[ (int)tseg2[ i ] ];
					ySeg2[ i ] = b[ (int)tseg2[ i ] ];
				}
				else xSeg2[i] = ySeg2[i] = 0.;
			}
		}
	}

	// Set number of Subsets and Points
	PEnset( *hPE, PEP_nSUBSETS, 1 );			// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );		// number of data points

	// feedback colorization
	dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
	DWORD* dwColors = NULL, dwC = 0;
	if( m_fFeedback )
	{
		dwColors = new DWORD[ m_nCnt ];
		memset( dwColors, 0, m_nCnt * sizeof( DWORD ) );
	}

	for( int p = 0; p < m_nCnt; p++ )
	{										  
		_tfd.fX = (float)a[ p ];
		if( a[ p ] == ::GetMissingDataValue() ) _tfd.fX = NULLDATAVALUE;
		PEvsetcell( *hPE, PEP_faXDATA, p, &_tfd.fX );
		_tfd.fY = (float)b[ p ];
		if( b[ p ] == ::GetMissingDataValue() ) _tfd.fY = NULLDATAVALUE;
		PEvsetcell( *hPE, PEP_faYDATA, p, &_tfd.fY );

		// sample/pressure/stroke/slant (NOW JERK) annotation table
		if( ( p == 0 ) && pGraphDlg )
		{
			char szTblData[ TBL_DATA_SIZE ];
			int nDeg = 0;
			sprintf_s( szTblData, PETAL_STR, 0, _nTFStroke, 0, (float)z[ p ], (double)nDeg, (double)nDeg, (double)nDeg );
			PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
		}

		// feedback colorization
		if( m_fFeedback && _theFB )
		{
			dwC = m_fMonochrome ? MONOCOLOR :
									(DWORD)::ScaleColor( m_dMinVal, m_dMaxVal, m_MinColor, m_MaxColor, _theFB[ p ],
														 ( p > 0 ) ? _theFB[ p - 1 ] : NULLDATAVALUE );
			dwColors[ p ] = dwC;
		}
		else if( z[ p ] > ::GetMinPenPressure() )
		{
			dwColor = m_fMonochrome ? MONOCOLOR : LINECOLOR;
		}
		else
		{
			if( _nTFLastPenPressure <= ::GetMinPenPressure() )
				dwColor = m_dwPenUpColor;
			else
				dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
		}
		_nTFLastPenPressure = (int)z[ p ];

		// set point to default color unless feedback desired
		if( !m_fFeedback ) PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, p, &dwColor );
	}

	// Seg data
	_tfd.symbol = 0;
	_tfd.symbol2 = 0;
	_tfd.col = m_fMonochrome ? MONOCOLOR : LINE2COLOR;
	_tfd.col2 = m_fMonochrome ? MONOCOLOR : LINECOLOR;
	_tfd.annPt = 0;
	if( !m_fGraphOnly && !m_fSupressAnn )
	{
		for( i = 0; i < m_nCntSeg; i++ )
		{
			_tfd.dX1 = rnpol( a, NSMAX, tseg[ i ] );
			_tfd.dY1 = rnpol( b, NSMAX, tseg[ i ] );
			if( ( _tfd.dX1 < 999999 ) && ( _tfd.dX1 > -999999 ) &&
				( _tfd.dY1 < 999999 ) && ( _tfd.dY1 > -999999 ) )
			{
				_tfd.symbol = m_nSymbolSeg;
				sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX1, _tfd.dY1 );

				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX1);
				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY1);
				PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol);
				PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col);
				if( _AnnValues )
					PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

				_tfd.annPt++;
			}
		}
		for( i = 0; i < m_nCntSeg2; i++ )
		{
			if( ( !m_fGraphOnly && !m_fSupressAnn ) || ( ( i > 0 ) && ( tseg2[ i ] != tseg2[ i - 1 ] ) ) )
			{
				_tfd.dX2 = rnpol( a, NSMAX, tseg2[ i ] );
				_tfd.dY2 = rnpol( b, NSMAX, tseg2[ i ] );

				if( ( _tfd.dX2 < 999999 ) && ( _tfd.dX2 > -999999 ) &&
					( _tfd.dY2 < 999999 ) && ( _tfd.dY2 > -999999 ) )
				{
					_tfd.symbol2 = m_nSymbolSubMvmt;
					sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX2, _tfd.dY2 );

					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX2);
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY2);
					PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol2);
					PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col2);
					if( _AnnValues )
						PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

					_tfd.annPt++;
				}
			}
		}
	}

	// apply feedback colors
	if( m_fFeedback ) PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, m_nCnt );

	if( !fOnlyColors )
	{
		// Labels
		int nPos = m_szTFFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szTFFile.Mid( nPos + 1 ) : m_szTFFile;
		CString szLblX, szLblY, szLbl;
		POSITION pos = m_szLabels.FindIndex( nX );
		szLblX = pos ? m_szLabels.GetAt( pos ) : szX;
		pos = m_szLabels.FindIndex( nY );
		szLblY = pos ? m_szLabels.GetAt( pos ) : szY;
		szLbl.Format( _T("%s vs. %s"), szLblX, szLblY );
		DoLabels( hPE, szLblX, szLblY, szTitle, szLbl );

		// Set defaults
		SetChartDefaults( hPE );
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// in case cursor was already in position
	OnCursorMove( (VARIANT*)hPE, eTF_XY );

	// Cleanup
	if( xSeg ) delete [] xSeg;
	if( ySeg ) delete [] ySeg;
	if( xSeg2 ) delete [] xSeg2;
	if( ySeg2 ) delete [] ySeg2;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowTF::ShowCustom( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	double* xcol = NULL, *ycol = NULL;
	int nX = 0, nY = 0;	// for labels
	BOOL fByTime = FALSE;

	switch( AxisX )
	{
		case eTF_COL_X: xcol = x; nX = LBL_X; break;
		case eTF_COL_Y: xcol = y; nX = LBL_Y; break;
		case eTF_COL_Z: xcol = z; nX = LBL_Z; break;
		case eTF_COL_VX: xcol = vx; nX = LBL_VX; break;
		case eTF_COL_VY: xcol = vy; nX = LBL_VY; break;
		case eTF_COL_VZ: xcol = vabs; nX = LBL_VABS; break;
		case eTF_COL_AX: xcol = ax; nX = LBL_AX; break;
		case eTF_COL_AY: xcol = ay; nX = LBL_AY; break;
		case eTF_COL_AZ: xcol = az; nX = LBL_AZ; break;
		case eTF_COL_JX: xcol = jx; nX = LBL_JERK; break;
		case eTF_COL_JY: xcol = jy; nX = LBL_JERK; break;
		case eTF_COL_JZ: xcol = jabs; nX = LBL_JERK; break;
		case eTF_COL_Time: fByTime = TRUE; break;
		default: return FALSE;
	}
	switch( AxisY )
	{
		case eTF_COL_X: ycol = x; nY = LBL_X; break;
		case eTF_COL_Y: ycol = y; nY = LBL_Y; break;
		case eTF_COL_Z: ycol = z; nY = LBL_Z; break;
		case eTF_COL_VX: ycol = vx; nY = LBL_VX; break;
		case eTF_COL_VY: ycol = vy; nY = LBL_VY; break;
		case eTF_COL_VZ: ycol = vabs; nY = LBL_VABS; break;
		case eTF_COL_AX: ycol = ax; nY = LBL_AX; break;
		case eTF_COL_AY: ycol = ay; nY = LBL_AY; break;
		case eTF_COL_AZ: ycol = az; nY = LBL_AZ; break;
		case eTF_COL_JX: ycol = jx; nY = LBL_JERK; break;
		case eTF_COL_JY: ycol = jy; nY = LBL_JERK; break;
		case eTF_COL_JZ: ycol = jabs; nY = LBL_JERK; break;
		default: return FALSE;
	}

	if( fByTime ) return ShowByTime( pGraphDlg, pGraphDlg, hPE, ycol, fOnlyColors );
	else return ShowAvsB( xcol, ycol, nX, _T("X-Axis"), nY, _T("Y-Axis"), pGraph, pGraphDlg, hPE, fOnlyColors );
}

BOOL ShowTF::ShowXY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowAvsB( x, y, LBL_X, _T("X position (cm)"), LBL_Y, _T("Y position (cm)"),
					 pGraph, pGraphDlg, hPE, fOnlyColors );
}

BOOL ShowTF::ShowXZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowAvsB( x, z, LBL_X, _T("X position (cm)"), LBL_Z, _T("Z Value"),
					 pGraph, pGraphDlg, hPE, fOnlyColors );
}

BOOL ShowTF::ShowYZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowAvsB( y, z, LBL_Y, _T("Y position (cm)"), LBL_Z, _T("Z Value"),
					 pGraph, pGraphDlg, hPE, fOnlyColors );
}

BOOL ShowTF::ShowAvsB3D( double* a, double* b, int nX, CString szX, int nY, CString szY,
						 CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	if( !a || !b ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg, PECONTROL_3D ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// Segment Data
	DWORD dwColor = 0;
	double* xSeg = NULL, *ySeg = NULL, *xSeg2 = NULL, *ySeg2 = NULL;
	int i = 0;
	if( !m_fSupressAnn )
	{
		if( m_nCntSeg > 0 )		// Including SEG data
		{
			PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
			PEnset(*hPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);
			PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
			PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);

			// Interpolate points
			xSeg = new double[ m_nCntSeg ];
			ySeg = new double[ m_nCntSeg ];
			if( tseg2 )
			{
				xSeg2 = new double[ m_nCntSeg2 ];
				ySeg2 = new double[ m_nCntSeg2 ];
			}
			for( i = 0; i < m_nCntSeg; i++ )
			{
				xSeg[ i ] = a[ (int)tseg[ i ] ];
				ySeg[ i ] = b[ (int)tseg[ i ] ];
			}
			for( i = 0; i < m_nCntSeg2; i++ )
			{
				xSeg2[ i ] = a[ (int)tseg2[ i ] ];
				ySeg2[ i ] = b[ (int)tseg2[ i ] ];
			}
		}
	}

	// feedback colorization
	dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
	DWORD* dwColors = NULL, dwC = 0;
	if( m_fFeedback )
	{
		dwColors = new DWORD[ m_nCnt ];
		memset( dwColors, 0, m_nCnt * sizeof( DWORD ) );
	}

	// Set number of Subsets and Points
	PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );	// number of data points

	int s = 0;
	for( int p = 0; p < m_nCnt; p++ )
	{
		// don't use sorted values (instructions say to use sorted, but plot by line does not work)
		_tfd.fX = (float)a[ p ];
		if( a[ p ] == ::GetMissingDataValue() ) _tfd.fX = NULLDATAVALUE;
		_tfd.fY = (float)b[ p ];
		if( b[ p ] == ::GetMissingDataValue() ) _tfd.fY = NULLDATAVALUE;
		_tfd.fZ = (float)z[ p ];
		if( z[ p ] == ::GetMissingDataValue() ) _tfd.fZ = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faXDATA, s, p, &_tfd.fX );
		if( m_fInverted )
		{
			PEvsetcellEx( *hPE, PEP_faYDATA, s, p, &_tfd.fY );
			PEvsetcellEx( *hPE, PEP_faZDATA, s, p, &_tfd.fZ );
		}
		else
		{
			PEvsetcellEx( *hPE, PEP_faYDATA, s, p, &_tfd.fZ );
			PEvsetcellEx( *hPE, PEP_faZDATA, s, p, &_tfd.fY );
		}

		// feedback colorization
		if( m_fFeedback && _theFB )
		{
			dwC = m_fMonochrome ? MONOCOLOR :
									(DWORD)::ScaleColor( m_dMinVal, m_dMaxVal, m_MinColor, m_MaxColor, _theFB[ p ],
														 ( p > 0 ) ? _theFB[ p - 1 ] : NULLDATAVALUE );
			dwColors[ p ] = dwC;
		}

		// set point to default color unless feedback desired
		if( !m_fFeedback ) PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, p, &dwColor );
	}

	// Seg data
	_tfd.symbol = 0;
	_tfd.symbol2 = 0;
	_tfd.col = m_fMonochrome ? MONOCOLOR : LINE2COLOR;
	_tfd.col2 = m_fMonochrome ? MONOCOLOR : LINECOLOR;
	_tfd.annPt = 0;
	if( !m_fGraphOnly && !m_fSupressAnn )
	{
		for( i = 0; i < m_nCntSeg; i++ )
		{
			_tfd.dX1 = rnpol( a, NSMAX, tseg[ i ] );
			_tfd.dY1 = rnpol( b, NSMAX, tseg[ i ] );
			_tfd.dZ1 = rnpol( z, NSMAX, tseg[ i ] );

			if( ( _tfd.dX1 < 999999 ) && ( _tfd.dX1 > -999999 ) &&
				( _tfd.dY1 < 999999 ) && ( _tfd.dY1 > -999999 ) )
			{
				_tfd.symbol = m_nSymbolSeg;
				sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX1, _tfd.dY1 );

				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX1);
				if( m_fInverted )
				{
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY1);
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dZ1);
				}
				else
				{
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dZ1);
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dY1);
				}
				PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol);
				PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col);
				if( _AnnValues )
					PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

				_tfd.annPt++;
			}
		}
		for( i = 0; i < m_nCntSeg2; i++ )
		{
			if( ( !m_fGraphOnly && !m_fSupressAnn ) || ( ( i > 0 ) && ( tseg2[ i ] != tseg2[ i - 1 ] ) ) )
			{
				_tfd.dX2 = rnpol( a, NSMAX, tseg2[ i ] );
				_tfd.dY2 = rnpol( b, NSMAX, tseg2[ i ] );
				_tfd.dZ2 = rnpol( z, NSMAX, tseg2[ i ] );

				if( ( _tfd.dX2 < 999999 ) && ( _tfd.dX2 > -999999 ) &&
					( _tfd.dY2 < 999999 ) && ( _tfd.dY2 > -999999 ) )
				{
					_tfd.symbol2 = m_nSymbolSubMvmt;
					sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX2, _tfd.dY2 );

					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX2);
					if( m_fInverted )
					{
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY2);
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dZ2);
					}
					else
					{
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dZ2);
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dY2);
					}
					PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol2);
					PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col2);
					if( _AnnValues )
						PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

					_tfd.annPt++;
				}
			}
		}
	}

	// apply feedback colors
	if( m_fFeedback ) PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, m_nCnt );

	if( !fOnlyColors || fInverted )
	{
		// Labels
		int nPos = m_szTFFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szTFFile.Mid( nPos + 1 ) : m_szTFFile;
		CString szLblX, szLblY, szLbl;
		POSITION pos = m_szLabels.FindIndex( nX );
		szLblX = pos ? m_szLabels.GetAt( pos ) : szX;
		pos = m_szLabels.FindIndex( nY );
		szLblY = pos ? m_szLabels.GetAt( pos ) : szY;
		szLbl.Format( _T("%s vs. %s"), szLblX, szLblY );
		if( m_fInverted )
		{
			DoLabels( hPE, szLblX, szLblY, szTitle, szLbl );
			PEszset(*hPE, PEP_szZAXISLABEL, _T("Pen Pressure"));
		}
		else
		{
			DoLabels( hPE, szLblX, _T("Pen Pressure"), szTitle, szLbl );
			PEszset(*hPE, PEP_szZAXISLABEL, szLblY.GetBuffer());
		}

		// Set defaults
		SetChartDefaults( hPE );

		// undo scale adjustments
		PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
		PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
		PEreinitialize( *hPE );
		// reset line thickness
		int nLineType = PELT_THINSOLID;
		PEvset(*hPE, PEP_naSUBSETLINETYPES, &nLineType, 1);
		// Non-Surface chart, set PolyMode //
		PEnset(*hPE, PEP_nPOLYMODE, PEPM_SCATTER);
		// Set Various Other Properties //
		PEnset(*hPE, PEP_bBITMAPGRADIENTMODE, FALSE);
		// Set camera position //
		if( m_fInverted )
		{
			PEnset(*hPE, PEP_nVIEWINGHEIGHT, 15);
			PEnset(*hPE, PEP_nDEGREEOFROTATION, 180);
		}
		else
		{
			PEnset(*hPE, PEP_nVIEWINGHEIGHT, 15);
			PEnset(*hPE, PEP_nDEGREEOFROTATION, 145);
		}
		// Set Plotting methods //
		PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEPLM_SURFACE);   // ' Points
		PEnset(*hPE, PEP_bCACHEBMP, TRUE);
		PEnset(*hPE, PEP_nROTATIONDETAIL, PERD_FULLDETAIL);
		PEnset(*hPE, PEP_nROTATIONSPEED, 50);
		PEnset(*hPE, PEP_nROTATIONINCREMENT, PERI_INCBY1);
		PEnset(*hPE, PEP_bSHOWLEGEND, FALSE);
		// Setting these helps improve and printing and metafile exports //
		PEnset(*hPE, PEP_nDPIX, 600);
		PEnset(*hPE, PEP_nDPIY, 600);
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// Cleanup
	if( xSeg ) delete [] xSeg;
	if( ySeg ) delete [] ySeg;
	if( xSeg2 ) delete [] xSeg2;
	if( ySeg2 ) delete [] ySeg2;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowTF::ShowXY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	return ShowAvsB3D( x, y, LBL_X, _T("X position (cm)"), LBL_Y, _T("Y position (cm)"),
					   pGraph, pGraphDlg, hPE, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowXZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	return ShowAvsB3D( x, z, LBL_X, _T("X position (cm)"), LBL_Z, _T("Z Value"),
					   pGraph, pGraphDlg, hPE, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowYZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	return ShowAvsB3D( y, z, LBL_Y, _T("X position (cm)"), LBL_Z, _T("Z Value"),
					   pGraph, pGraphDlg, hPE, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowByTime3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, double* vals, BOOL fOnlyColors, BOOL fInverted )
{
	if( !vals ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg, PECONTROL_3D ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// Segment Data
	DWORD dwColor = 0;
	double* xSeg = NULL, *ySeg = NULL, *xSeg2 = NULL, *ySeg2 = NULL;
	int i = 0;
	if( !m_fSupressAnn )
	{
		if( m_nCntSeg > 0 )		// Including SEG data
		{
			PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
			PEnset(*hPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);
			PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
			PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);

			// Interpolate points
			xSeg = new double[ m_nCntSeg ];
			ySeg = new double[ m_nCntSeg ];
			if( tseg2 )
			{
				xSeg2 = new double[ m_nCntSeg2 ];
				ySeg2 = new double[ m_nCntSeg2 ];
			}
			for( i = 0; i < m_nCntSeg; i++ )
			{
				xSeg[ i ] = x[ (int)tseg[ i ] ];
				ySeg[ i ] = y[ (int)tseg[ i ] ];
			}
			for( i = 0; i < m_nCntSeg2; i++ )
			{
				xSeg2[ i ] = x[ (int)tseg2[ i ] ];
				ySeg2[ i ] = y[ (int)tseg2[ i ] ];
			}
		}
	}

	// feedback colorization
	dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
	DWORD* dwColors = NULL, dwC = 0;
	if( m_fFeedback )
	{
		dwColors = new DWORD[ m_nCnt ];
		memset( dwColors, 0, m_nCnt * sizeof( DWORD ) );
	}

	// Set number of Subsets and Points //
	PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );	// number of data points

	int s = 0;
	for( int p = 0; p < m_nCnt; p++ )
	{
		// don't use sorted values (instructions say to use sorted, but plot by line does not work)
		_tfd.fX = (float)( p * sec );
		_tfd.fY = (float)vals[ p ];
		if( vals[ p ] == ::GetMissingDataValue() ) _tfd.fY = NULLDATAVALUE;
		_tfd.fZ = (float)z[ p ];
		if( z[ p ] == ::GetMissingDataValue() ) _tfd.fZ = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faXDATA, s, p, &_tfd.fX );
		if( m_fInverted )
		{
			PEvsetcellEx( *hPE, PEP_faYDATA, s, p, &_tfd.fY );
			PEvsetcellEx( *hPE, PEP_faZDATA, s, p, &_tfd.fZ );
		}
		else
		{
			PEvsetcellEx( *hPE, PEP_faYDATA, s, p, &_tfd.fZ );
			PEvsetcellEx( *hPE, PEP_faZDATA, s, p, &_tfd.fY );
		}

		// feedback colorization
		if( m_fFeedback && _theFB )
		{
			dwC = m_fMonochrome ? MONOCOLOR :
								  (DWORD)::ScaleColor( m_dMinVal, m_dMaxVal, m_MinColor, m_MaxColor, _theFB[ p ],
													   ( p > 0 ) ? _theFB[ p - 1 ] : NULLDATAVALUE );
			dwColors[ p ] = dwC;
		}

		if( !m_fFeedback ) PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, p, &dwColor );
	}

	// Seg data
	_tfd.symbol = 0;
	_tfd.symbol2 = 0;
	_tfd.col = m_fMonochrome ? MONOCOLOR : LINE2COLOR;
	_tfd.col2 = m_fMonochrome ? MONOCOLOR : LINECOLOR;
	_tfd.annPt = 0;
	if( !m_fGraphOnly && !m_fSupressAnn )
	{
		for( i = 0; i < m_nCntSeg; i++ )
		{
			_tfd.dX1 = (tseg[ i ]) * sec;
			_tfd.dY1 = rnpol( vals, NSMAX, tseg[ i ] );
			_tfd.dZ1 = rnpol( z, NSMAX, tseg[ i ] );

			if( ( _tfd.dX1 < 999999 ) && ( _tfd.dX1 > -999999 ) &&
				( _tfd.dY1 < 999999 ) && ( _tfd.dY1 > -999999 ) )
			{
				_tfd.symbol = m_nSymbolSeg;
				sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX1, _tfd.dY1 );

				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX1);
				if( m_fInverted )
				{
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY1);
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dZ1);
				}
				else
				{
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dZ1);
					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dY1);
				}
				PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol);
				PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col);
				if( _AnnValues )
					PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

				_tfd.annPt++;
			}
		}
		for( i = 0; i < m_nCntSeg2; i++ )
		{
			if( ( !m_fGraphOnly && !m_fSupressAnn ) || ( ( i > 0 ) && ( tseg2[ i ] != tseg2[ i - 1 ] ) ) )
			{
				_tfd.dX2 = (tseg[ i ]) * sec;
				_tfd.dY2 = rnpol( vals, NSMAX, tseg2[ i ] );
				_tfd.dZ2 = rnpol( z, NSMAX, tseg2[ i ] );

				if( ( _tfd.dX2 < 999999 ) && ( _tfd.dX2 > -999999 ) &&
					( _tfd.dY2 < 999999 ) && ( _tfd.dY2 > -999999 ) )
				{
					_tfd.symbol2 = m_nSymbolSubMvmt;
					sprintf_s( _tfd.label, _T("%.2f, %.2f"), _tfd.dX2, _tfd.dY2 );

					PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, _tfd.annPt, &_tfd.dX2);
					if( m_fInverted )
					{
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dY2);
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dZ2);
					}
					else
					{
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, _tfd.annPt, &_tfd.dZ2);
						PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONZ, _tfd.annPt, &_tfd.dY2);
					}
					PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, _tfd.annPt, &_tfd.symbol2);
					PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, _tfd.annPt, &_tfd.col2);
					if( _AnnValues )
						PEvsetcell(*hPE, PEP_szaGRAPHANNOTATIONTEXT, _tfd.annPt, _tfd.label);

					_tfd.annPt++;
				}
			}
		}
	}

	// apply feedback colors
	if( m_fFeedback ) PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, m_nCnt );

	if( !fOnlyColors || fInverted )
	{
		// Labels
		int nPos = m_szTFFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szTFFile.Mid( nPos + 1 ) : m_szTFFile;
		strcpy_s( _tfd.label, 50, szTitle );
		PEszset(*hPE, PEP_szMAINTITLE, _T(""));

		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		CString szLblX = _T("Time (s)");
		m_szSubTitle.Format( _T("%s vs. %s"), m_szYAxis, szLblX );
		strcpy_s( _tfd.label, 50, szTitle );
		PEszset(*hPE, PEP_szSUBTITLE, _tfd.label);
		strcpy_s( _tfd.label, 50, szLblX );
		PEszset(*hPE, PEP_szXAXISLABEL, _tfd.label);
		if( m_fInverted )
		{
			strcpy_s( _tfd.label, 50, m_szYAxis );
			PEszset(*hPE, PEP_szYAXISLABEL, _tfd.label);
			strcpy_s( _tfd.label, 50, _T("Pen Pressure") );
			PEszset(*hPE, PEP_szZAXISLABEL, _tfd.label);
		}
		else
		{
			strcpy_s( _tfd.label, 50, m_szYAxis );
			PEszset(*hPE, PEP_szZAXISLABEL, _tfd.label);
			strcpy_s( _tfd.label, 50, _T("Pen Pressure") );
			PEszset(*hPE, PEP_szYAXISLABEL, _tfd.label);
		}
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// Set defaults
		SetChartDefaults( hPE );

		// undo scale adjustments
		PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
		PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
		PEreinitialize( *hPE );
		// reset line thickness
		int nLineType = PELT_THINSOLID;
		PEvset(*hPE, PEP_naSUBSETLINETYPES, &nLineType, 1);
		// Non-Surface chart, set PolyMode //
		PEnset(*hPE, PEP_nPOLYMODE, PEPM_SCATTER);
		// Set Various Other Properties //
		PEnset(*hPE, PEP_bBITMAPGRADIENTMODE, FALSE);
		// Set camera position //
		if( m_fInverted )
		{
			PEnset(*hPE, PEP_nVIEWINGHEIGHT, 15);
			PEnset(*hPE, PEP_nDEGREEOFROTATION, 180);
		}
		else
		{
			PEnset(*hPE, PEP_nVIEWINGHEIGHT, 15);
			PEnset(*hPE, PEP_nDEGREEOFROTATION, 145);
		}
		// Set Plotting methods //
		PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEPLM_SURFACE);   // ' Points
		PEnset(*hPE, PEP_bCACHEBMP, TRUE);
		PEnset(*hPE, PEP_nROTATIONDETAIL, PERD_FULLDETAIL);
		PEnset(*hPE, PEP_nROTATIONSPEED, 50);
		PEnset(*hPE, PEP_nROTATIONINCREMENT, PERI_INCBY1);
		PEnset(*hPE, PEP_bSHOWLEGEND, FALSE);
		// Setting these helps improve and printing and metafile exports //
		PEnset(*hPE, PEP_nDPIX, 600);
		PEnset(*hPE, PEP_nDPIY, 600);
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// Cleanup
	if( xSeg ) delete [] xSeg;
	if( ySeg ) delete [] ySeg;
	if( xSeg2 ) delete [] xSeg2;
	if( ySeg2 ) delete [] ySeg2;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowTF::ShowXt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_X );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("X position (cm)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, x, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowYt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_Y );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Y position (cm)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, y, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowZt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_Z );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z value");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, z, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowJXt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted  )
{
	m_szSubTitle = _T("X jerk (cm/s3) versus Time(s)");
	m_szYAxis = _T("X Jerk (cm/s3)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, jx, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowJYt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted  )
{
	m_szSubTitle = _T("Y jerk (cm/s3) versus Time(s)");
	m_szYAxis = _T("Y Jerk (cm/s3)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, jy, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowJABSt3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted  )
{
	m_szSubTitle = _T("Absolute jerk (cm/s3) versus Time(s)");
	m_szYAxis = _T("Absolute Jerk (cm/s3)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, jabs, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowVX3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VX );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("X Velocity (cm/s)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, vx, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowVY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VY );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Y Velocity (cm/s)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, vy, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowVZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VABS );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z Velocity (N/s)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, vabs, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowVABS3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VABS );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z Velocity (N/s)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, vabs, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowAX3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_AX );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("X Acceleration (cm/s2)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, ax, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowAY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_AY );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Y Acceleration (cm/s2)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, ay, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowAZ3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	POSITION pos = m_szLabels.FindIndex( LBL_AZ );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z acceleration (N/s2)");

	return ShowByTime3D( pGraph, pGraphDlg, hPE, az, fOnlyColors, fInverted );
}

BOOL ShowTF::ShowXt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_X );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("X position (cm)");

	return ShowByTime( pGraph, pGraphDlg, hPE, x, fOnlyColors );
}

BOOL ShowTF::ShowYt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_Y );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Y position (cm)");

	return ShowByTime( pGraph, pGraphDlg, hPE, y, fOnlyColors );
}

BOOL ShowTF::ShowZt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_Z );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z value");

	return ShowByTime( pGraph, pGraphDlg, hPE, z, fOnlyColors );
}

BOOL ShowTF::ShowVX( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VX );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("X Velocity (cm/s)");

	return ShowByTime( pGraph, pGraphDlg, hPE, vx, fOnlyColors );
}

BOOL ShowTF::ShowVY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VY );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Y Velocity (cm/s)");

	return ShowByTime( pGraph, pGraphDlg, hPE, vy, fOnlyColors );
}

BOOL ShowTF::ShowVZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VABS );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z Velocity (N/s)");

	return ShowByTime( pGraph, pGraphDlg, hPE, vabs, fOnlyColors );
}

BOOL ShowTF::ShowVABS( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_VABS );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Absolute/Z Velocity (cm/s)");

	return ShowByTime( pGraph, pGraphDlg, hPE, vabs, fOnlyColors );
}

BOOL ShowTF::ShowAX( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_AX );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("X Acceleration (cm/s2)");

	return ShowByTime( pGraph, pGraphDlg, hPE, ax, fOnlyColors );
}

BOOL ShowTF::ShowAY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_AY );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Y Acceleration (cm/s2)");

	return ShowByTime( pGraph, pGraphDlg, hPE, ay, fOnlyColors );
}

BOOL ShowTF::ShowAZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	POSITION pos = m_szLabels.FindIndex( LBL_AZ );
	m_szYAxis = pos ? m_szLabels.GetAt( pos ) : _T("Z acceleration (N/s2)");

	return ShowByTime( pGraph, pGraphDlg, hPE, az, fOnlyColors );
}

BOOL ShowTF::ShowJXt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	m_szSubTitle = _T("X jerk (cm/s3) versus Time(s)");
	m_szYAxis = _T("X Jerk (cm/s3)");

	return ShowByTime( pGraph, pGraphDlg, hPE, jx, fOnlyColors );
}

BOOL ShowTF::ShowJYt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	m_szSubTitle = _T("Y jerk (cm/s3) versus Time(s)");
	m_szYAxis = _T("Y Jerk (cm/s3)");

	return ShowByTime( pGraph, pGraphDlg, hPE, jy, fOnlyColors );
}

BOOL ShowTF::ShowJABSt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	m_szSubTitle = _T("Absolute jerk (cm/s3) versus Time(s)");
	m_szYAxis = _T("Absolute Jerk (cm/s3)");

	return ShowByTime( pGraph, pGraphDlg, hPE, jabs, fOnlyColors );
}

BOOL ShowTF::ShowAspec( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, TFGraph gt, BOOL fOnlyColors )
{
	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// set vars based upon which spectrum
	m_fSpectrum = TRUE;
	int nCount = m_nCntSpectrum;
	double* pData = aspec;
	double* pDataFilt = aspecfilt;
	UINT nTitleID = IDS_TF_ASPSUB;
	INT_PTR nYID = LBL_ASPEC;
	if( gt == eTF_vaspec )
	{
		nCount = m_nCntSpectrumV;
		pData = vaspec;
		pDataFilt = vaspecfilt;
		nTitleID = IDS_TF_ASPSUBV;
		nYID = LBL_VASPEC;
	}
	else if( gt == eTF_aaspec )
	{
		nCount = m_nCntSpectrumA;
		pData = aaspec;
		pDataFilt = aaspecfilt;
		nTitleID = IDS_TF_ASPSUBA;
		nYID = LBL_AASPEC;
	}
	if( !pData || !pDataFilt )
	{
		PEnset( *hPE, PEP_nSUBSETS, 0 );
		PEnset( *hPE, PEP_nPOINTS, 0 );
		PEresetimage( *hPE, 0, 0 );
		::InvalidateRect( *hPE, NULL, FALSE );
		return FALSE;
	}

	// Set number of Subsets and Points //
	PEnset( *hPE, PEP_nSUBSETS, 2 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, nCount );	// number of data points

	DWORD dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
	float fX = 0.0, fY = 0.0;
	float fFactor = (float)(( ( sec > 0 ) && ( nCount > 0 ) ) ? (float)(1 / ( sec * 2.0 * nCount )) : 0.);
	_tfd.dataxmin = 100000.;
	_tfd.dataxmax = 0.;
	_tfd.dataymin = 100000.;
	_tfd.dataymax = 0.;
	for( int p = 0; p < nCount; p++ )
	{
		fX = (float)( p * fFactor );
		if( fX < _tfd.dataxmin ) _tfd.dataxmin = fX;
		if( fX > _tfd.dataxmax ) _tfd.dataxmax = fX;

		PEvsetcellEx( *hPE, PEP_faXDATA, 0, p, &fX );

		fY = (float)pData[ p ];
		if( pData[ p ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
		if( fY < _tfd.dataymin ) _tfd.dataymin = fY;
		if( fY > _tfd.dataymax ) _tfd.dataymax = fY;

		PEvsetcellEx( *hPE, PEP_faYDATA, 0, p, &fY );
		dwColor = m_fMonochrome ? MONOCOLOR : RGB( 255, 125, 125 );
		PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, p, &dwColor );

		PEvsetcellEx( *hPE, PEP_faXDATA, 1, p, &fX );

		fY = (float)pDataFilt[ p ];
		if( fY < _tfd.dataymin ) _tfd.dataymin = fY;
		if( fY > _tfd.dataymax ) _tfd.dataymax = fY;

		PEvsetcellEx( *hPE, PEP_faYDATA, 1, p, &fY );
		dwColor = m_fMonochrome ? MONOCOLOR : RGB( 0, 0, 225 );
		PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 1, p, &dwColor );
	}

	PEnset(*hPE, PEP_bSCROLLINGHORZZOOM, TRUE);
	PEnset(*hPE, PEP_bSCROLLINGVERTZOOM, TRUE);
	PEnset(*hPE, PEP_bFIXEDFONTS, TRUE);
	if( !fOnlyColors )
	{
		// Plotting methods
		PEnset(*hPE, PEP_bFOCALRECT, FALSE);
		PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
		PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
		PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);

		// Labels
		int nPos = m_szTFFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szTFFile.Mid( nPos + 1 ) : m_szTFFile;
		char pszTitle[ 50 ], pszSub[ 50 ], pszY[ 50 ];
		CString szTemp;
		szTemp.LoadString( nTitleID );
		strcpy_s( pszSub, 50, szTemp );
		POSITION pos = m_szLabels.FindIndex( nYID );
		if( pos ) szTemp = m_szLabels.GetAt( pos );
		else szTemp.LoadString( IDS_TF_ASPY );
		strcpy_s( pszY, 50, szTemp );
		strcpy_s( pszTitle, 50, szTitle );

		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		PEszset(*hPE, PEP_szMAINTITLE, _T(""));
		PEszset(*hPE, PEP_szSUBTITLE, pszTitle);
		PEszset(*hPE, PEP_szYAXISLABEL, pszY);
		PEszset(*hPE, PEP_szXAXISLABEL, "Frequency (Hz)");
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// subset labels
		szTemp.LoadString( IDS_TF_ASPSUB1 );
		strcpy_s( pszSub, 50, szTemp );
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 0, pszSub );
		szTemp.LoadString( IDS_TF_ASPSUB2 );
		strcpy_s( pszSub, 50, szTemp );
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 1, pszSub );
		if( !m_fMonochrome )
		{
			DWORD dwColors[ 2 ] = { RGB( 255, 125, 125 ), RGB( 0, 0, 225 ) };
			PEvset( *hPE, PEP_dwaSUBSETCOLORS, dwColors, 2 );
		}
		else
		{
			DWORD dwColors[ 2 ] = { MONOCOLOR, MONOCOLOR };
			PEvset( *hPE, PEP_dwaSUBSETCOLORS, dwColors, 2 );
		}
// 		float dVal = 1.0;
//		PEvset( *hPE, PEP_fFONTSIZELEGENDCNTL, &dVal, 1 );
			
		// Set colors
		PEnset(*hPE, PEP_dwGRAPHFORECOLOR, FORECOLOR);
		PEnset(*hPE, PEP_dwSHADOWCOLOR, BGCOLOR);
		PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, BGCOLOR);

		// Set DataShadows to none
		PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

		// subset line types
		int nLineTypes[] = { PELT_THINSOLID, PELT_THINSOLID };
		PEvset(*hPE, PEP_naSUBSETLINETYPES, nLineTypes, 2);

		// subset point types
		int nPointTypes[] = { PEPT_DOTSOLID, PEPT_UPTRIANGLESOLID };
		PEvset(*hPE, PEP_naSUBSETPOINTTYPES, nPointTypes, 2);

		// When set to no manual PEreinitialize delivers the min and max of x and y data
		PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
		PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
		PEreinitialize( *hPE );

		// Now enable manual setting of min and max of x and y data
		PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
		PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

		// Calculate the real data range
		_tfd.dataxrange = _tfd.dataxmax - _tfd.dataxmin;
		_tfd.datayrange = _tfd.dataymax - _tfd.dataymin;

		// Test for zero data ranges.
		if (_tfd.dataxrange == 0.) _tfd.dataxrange = 1.;
		if (_tfd.datayrange == 0.) _tfd.datayrange = 1.;

		// Get the screen positions. 
		PEvget (*hPE, PEP_rectGRAPH, &_tfd.ScreenRectangle); 

		// Divide the extra data space so that the graph is in the middle of the window
		_tfd.dataxminnew = _tfd.dataxmin;
		_tfd.dataxmaxnew = _tfd.dataxmax;
		_tfd.datayminnew = _tfd.dataymin;
		_tfd.dataymaxnew = _tfd.dataymax;

		// Set the new min and max of the x and y data
		PEvset(*hPE, PEP_fMANUALMINX, &_tfd.dataxminnew, 1);
		PEvset(*hPE, PEP_fMANUALMAXX, &_tfd.dataxmaxnew, 1);
		PEvset(*hPE, PEP_fMANUALMINY, &_tfd.datayminnew, 1);
		PEvset(*hPE, PEP_fMANUALMAXY, &_tfd.dataymaxnew, 1);

		//** Set up cursor **
		PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);

		//** Help see data points **
		//PEnset(*hPE, PEP_bMARKDATAPOINTS, TRUE);
    
		//** This will allow you to move cursor by clicking data point **
		PEnset(*hPE, PEP_bMOUSECURSORCONTROL, TRUE);
		PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, TRUE);
    
		//** Cursor prompting in top left corner **
		PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
		PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);
	}

	PEnset(*hPE, PEP_bSHOWLEGEND, !m_fMonochrome);

	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// in case cursor was already in position
	OnCursorMove( (VARIANT*)hPE, eTF_XY );
	// set starting point at 1 Hz
	long lStart = (long)( m_nCnt * 1 * sec * 2 + 0.5 );
	SetCursorPos( (VARIANT*)hPE, lStart );

	return TRUE;
}

STDMETHODIMP ShowTF::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetGraphLogOn( fOn );

	return S_OK;
}

STDMETHODIMP ShowTF::UseOptimalScaling( BOOL fOn )
{
	m_fOptimal = fOn;

	return S_OK;
}

STDMETHODIMP ShowTF::put_Monochrome(BOOL fOn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fMonochrome = fOn;

	return S_OK;
}

STDMETHODIMP ShowTF::put_SupressAnn(BOOL fOn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fSupressAnn = fOn;

	return S_OK;
}

STDMETHODIMP ShowTF::SetMonochrome(VARIANT* PEHandle, TFGraph GraphType, BOOL fOn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !PEHandle ) return E_FAIL;

	_nTFLastPenPressure = 0;

	BOOL fRet = FALSE;
	if( ( GraphType & TF_3D ) == TF_3D )
	{
		GraphType = (TFGraph)( GraphType - eTF_3D );
		if( GraphType == TF_XY )
			fRet = ShowXY3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_XZ )
			fRet = ShowXZ3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_YZ )
			fRet = ShowYZ3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_Xt )
			fRet = ShowXt3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_Yt )
			fRet = ShowYt3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_Zt )
			fRet = ShowZt3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_JXt )
			fRet = ShowJXt3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_JYt )
			fRet = ShowJYt3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_JABSt )
			fRet = ShowJABSt3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_vx )
			fRet = ShowVX3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_vy )
			fRet = ShowVY3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_vz )
			fRet = ShowVZ3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_ax )
			fRet = ShowAX3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_ay )
			fRet = ShowAY3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_az )
			fRet = ShowAZ3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == TF_vabs )
			fRet = ShowVABS3D( NULL, NULL, (HWND*)PEHandle, TRUE );
	}
	else if( GraphType == TF_XY )
		fRet = ShowXY( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_XZ )
		fRet = ShowXZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_YZ )
		fRet = ShowYZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vz )
		fRet = ShowVZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_az )
		fRet = ShowAZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( ( GraphType == TF_aspec ) || ( GraphType == TF_vaspec ) || ( GraphType == TF_aaspec ) )
		fRet = ShowAspec( NULL, NULL, (HWND*)PEHandle, GraphType, TRUE );
	else if( GraphType == TF_Xt )
		fRet = ShowXt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_Yt )
		fRet = ShowYt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_Zt )
		fRet = ShowZt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vx )
		fRet = ShowVX( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vy )
		fRet = ShowVY( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vabs )
		fRet = ShowVABS( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_ax )
		fRet = ShowAX( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_ay )
		fRet = ShowAY( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_JXt )
		fRet = ShowJXt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_JYt )
		fRet = ShowJYt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_JABSt )
		fRet = ShowJABSt( NULL, NULL, (HWND*)PEHandle, TRUE );

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowTF::put_Feedback(BOOL fOn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_fFeedback = fOn;

	return S_OK;
}

STDMETHODIMP ShowTF::put_ShowOnlyGraph(BOOL fOn)
{
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_fGraphOnly = fOn;

	return S_OK;
}
}

STDMETHODIMP ShowTF::put_Feature(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_szFeature = newVal;

	return S_OK;
}

STDMETHODIMP ShowTF::put_MinColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_MinColor = newVal;

	return S_OK;
}

STDMETHODIMP ShowTF::put_MaxColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_MaxColor = newVal;

	return S_OK;
}

STDMETHODIMP ShowTF::put_RTFrequency(short Freq )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	m_nRTFreq = Freq;

	return S_OK;
}

STDMETHODIMP ShowTF::ShowTFGraphRT(VARIANT *GraphWindow, TFGraph GraphType,
								   LONG* Cnt, VARIANT *PEHandle )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	if( !m_fInit ) Init();
	if( !m_fInit ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	// we're no longer scaling the frequency for real-time TF
//	if( m_nRTFreq > 0. ) *Cnt = (int)( ( 30. / m_nRTFreq ) * m_nCnt ) + 1;
//	else *Cnt = m_nCnt;
	*Cnt = m_nCnt;

	BOOL fRet = FALSE;
	if( ( GraphType & TF_XY ) == TF_XY )
		fRet = ShowXYRT( pGraph, (HWND*)PEHandle );

	return fRet ? S_OK : E_FAIL;
}

BOOL ShowTF::ShowXYRT( CWnd* pGraph, HWND* hPE )
{
	// Initialize
	if( !InitChart( hPE, pGraph ) ) return FALSE;

	// Set number of Subsets and Points //
	PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );		// number of data points

	// annotation table
	DWORD dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 80);

	//** Show Annotations **
	// option to not use annotations
	PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
 
    //** Clear out default data **
    float val = 0.0;
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 0, &val);
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 1, &val);
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 2, &val);
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 3, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 0, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 1, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 2, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 3, &val);

	// Set colors
	// option to not show bounding rectangle
	if( m_fGraphOnly ) PEnset(*hPE, PEP_dwGRAPHFORECOLOR, BGCOLOR);
	else PEnset(*hPE, PEP_dwGRAPHFORECOLOR, FORECOLOR);
	PEnset(*hPE, PEP_dwSHADOWCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, BGCOLOR);

	// Set DataShadows to show 3D 
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	int nPos = m_szTFFile.ReverseFind( '\\' );
	CString szTitle = ( nPos != -1 ) ? m_szTFFile.Mid( nPos + 1 ) : m_szTFFile;
	// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
	// option to not show titles
	if( m_fGraphOnly ) szTitle = _T("");
	POSITION pos = m_szLabels.FindIndex( LBL_X );
	CString szLblX = pos ? m_szLabels.GetAt( pos ) : _T("X-Axis");
	pos = m_szLabels.FindIndex( LBL_Y );
	CString szLblY = pos ? m_szLabels.GetAt( pos ) : _T("Y-Axis");
	CString szLbl;
	szLbl.Format( _T("%s vs. %s"), szLblX, szLblY );
	DoLabels( hPE, szLblX, szLblY, szTitle, szLbl );
	// settings
	PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
	PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szLABELFONT, "Arial");
	PEszset(*hPE, PEP_szTABLEFONT, "Arial");
	SetChartDefaults( hPE );

	// option to not show axes grids, values, or label
	if( m_fGraphOnly )
	{
		PEnset(*hPE, PEP_nSHOWYAXIS, PESA_EMPTY );
		PEnset(*hPE, PEP_nSHOWXAXIS, PESA_EMPTY );
	}

	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_bPREPAREIMAGES, TRUE);
	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	if( m_fFeedback && m_fGraphOnly )
	{
		int symbol = PELT_EXTRATHICKSOLID;
		PEvset(*hPE, PEP_naSUBSETLINETYPES, &symbol, 1);
	}

	// option to not use zooming
	if( !m_fGraphOnly ) PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);

	//  When set to no manual PEreinitialize delivers the min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	PEreinitialize( *hPE );

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

	// Get min and max of x and y data
	double xMin = 0.0, xMax = 0.0, yMin = 0.0, yMax = 0.0;
	for( int nIdx = 0; nIdx < m_nCnt; nIdx++ )
	{
		if( nIdx == 0 )
		{
			xMin = xMax = x[ nIdx ];
			yMin = yMax = y[ nIdx ];
		}
		else
		{
			if( x[ nIdx ] < xMin ) xMin = x[ nIdx ];
			if( x[ nIdx ] > xMax ) xMax = x[ nIdx ];
			if( y[ nIdx ] < yMin ) yMin = y[ nIdx ];
			if( y[ nIdx ] > yMax ) yMax = y[ nIdx ];
		}
	}

	double dataxmin = xMin, dataxmax = xMax, dataymin = yMin, dataymax = yMax;
		
	// Calculate the real data range
	double dataxrange = dataxmax - dataxmin;
	double datayrange = dataymax - dataymin;

	// Test for zero data ranges.
	if (dataxrange == 0.) dataxrange = 1.;
	if (datayrange == 0.) datayrange = 1.;

	// Get the screen positions. 
	RECT ScreenRectangle;
	PEvget (*hPE, PEP_rectGRAPH, &ScreenRectangle); 

	// Calculate data to pixel scales ant take smallest scale for x and y so that
	// the graph fits
	double scalex = (ScreenRectangle.right - ScreenRectangle.left) / dataxrange;
	double scaley = (ScreenRectangle.bottom - ScreenRectangle.top) / datayrange;
	double scale = MIN (scalex, scaley);

	// Test for zero scale.
	if (scale == 0.) scale = 1.;

	// Estimate the extra data space for x or y data needed to assure that a square remains a square
	double extradataxrange = 0.;
	double extradatayrange = 0.;
	if( !m_fOptimal )	// ie..proportional
	{
		extradataxrange = dataxrange * (scalex / scale - 1.);
		extradatayrange = datayrange * (scaley / scale - 1.);
	}
	// else independent optimal x & y scaling

    // Divide the extra data space so that the graph is in the middle of the window
	double dataxminnew = dataxmin - .5 * extradataxrange;
	double dataxmaxnew = dataxmax + .5 * extradataxrange;
	double datayminnew = dataymin - .5 * extradatayrange;
	double dataymaxnew = dataymax + .5 * extradatayrange;

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &dataxminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &dataxmaxnew, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &datayminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &dataymaxnew, 1);

	// no cursor
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);
	PEnset(*hPE, PEP_bMOUSECURSORCONTROL, FALSE);
	PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
	PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, FALSE);
	PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_NONE);

    PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	_Idx = 0;

	return TRUE;
}

STDMETHODIMP ShowTF::OnTimer( VARIANT* PEHandle )
{
	HWND* hPE = (HWND*)PEHandle;

	static float dPointsTotal = 0.;
	static int nPointsTotal = 0;

	if( _Idx >= m_nCnt )
	{
		dPointsTotal = 0.;
		nPointsTotal = 0;
		return S_OK;
	}

	int nPoints = 1;
	if( m_nRTFreq > 0. )
	{
// we're no longer scaling the frequency for real-time TF
//		float dPoints = (float)(m_nRTFreq / 30.);
		float dPoints = (float)1;
		dPointsTotal += dPoints;
		nPoints = (int)( dPointsTotal - nPointsTotal );
		nPointsTotal += nPoints;
	}

	for( int i = 0; ( i < nPoints ) && ( _Idx < m_nCnt ); i++ )
	{
		// point
		float fX = (float)x[ _Idx ];
		if( x[ _Idx ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faXDATA, 0, _Idx, &fX );
		float fY = (float)y[ _Idx ];
		if( y[ _Idx ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faYDATA, 0, _Idx, &fY );

		// crosshairs
		if( !m_fGraphOnly )
		{
			double dVal = (double)fX;
			PEvsetcell( *hPE, PEP_faVERTLINEANNOTATION, 0, &dVal );
			int lt = PELT_THINSOLID;
			PEvsetcell( *hPE, PEP_naVERTLINEANNOTATIONTYPE, 0, &lt );
			DWORD col = RGB(0,0,255);
			PEvsetcell( *hPE, PEP_dwaVERTLINEANNOTATIONCOLOR, 0, &col );

			dVal = (double)fY;
			PEvsetcell( *hPE, PEP_faHORZLINEANNOTATION, 0, &dVal );
			PEvsetcell( *hPE, PEP_naHORZLINEANNOTATIONTYPE, 0, &lt );
			PEvsetcell( *hPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 0, &col );
		}
	    
		// feedback colorization
		DWORD dwColor = 0;
		if( m_fMonochrome ) dwColor = MONOCOLOR;
		else if( z[ _Idx ] <= ::GetMinPenPressure() ) dwColor = ::GetSoftColor();
		else if( m_fFeedback && _theFB )
			dwColor = (DWORD)::ScaleColor( m_dMinVal, m_dMaxVal, m_MinColor, m_MaxColor, _theFB[ _Idx ],
										   ( _Idx > 0 ) ? _theFB[ _Idx - 1 ] : NULLDATAVALUE );
		else dwColor = LINECOLOR;
		PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, _Idx, &dwColor );

		_Idx++;
	}

	// no cursor
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);
	PEnset(*hPE, PEP_bMOUSECURSORCONTROL, FALSE);
	PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
	PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, FALSE);
	PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_NONE);

	//** Update image and force paint **
	PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowTF::put_PenUpColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dwPenUpColor = newVal;

	return S_OK;
}

STDMETHODIMP ShowTF::put_MinPenPressure(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	SetMinPenPressure( newVal );

	return S_OK;
}

STDMETHODIMP ShowTF::OnCursorMove( VARIANT *PEHandle, TFGraph GraphType )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;
	if( m_dFrequency == 0. ) return E_FAIL;

	// sample # (index from chart)
	long nX = PEnget( *hPE, PEP_nCURSORPOINT );
	// pressure and slant vars (NOW JERK)
	double dZ = z[ nX ], dS = 0., dVS = 0., dLA = 0.;
	// string var for table data
	char szTblData[ TBL_DATA_SIZE ];

	// translate into original sample #
	// adjust for decimation = chart sample # * sec * sampling rate
	double dX = (double)nX * sec * m_dFrequency;

	// calculate time (m_dFrequency is in Hz)
	double dTime = dX * ( 1.0 / m_dFrequency );

	// loop through start time & duration arrays to find the index
	// **HACK** - A stroke can either be represented as a full stroke or as
	// a primary, secondary, and full stroke (ie, either 1 or 3 segments per stroke)
	int nPos = -1;
	int nSeg = 0;
	BOOL fSet = FALSE, fZeroStroke = TRUE;
	while( ( nPos < NSEGMAX ) && ( nSeg <= 2 ) )
	{
		nPos++;
		// include lower boundary, exclude upper boundary
		if( ( dTime >= st[ nPos ] ) && ( dTime < ( st[ nPos ] + d[ nPos ] ) ) )
		{
			// set slant (JERK)
			dS = s[ nPos ];
			// set vert size
			dVS = vs[ nPos ];
			// set loop area
			dLA = la[ nPos ];
			// check for submovement position (if applicable)
			if( _fTFSubMovement ) fSet = TRUE;
			else break;
		}
		// if submovement, found range, and not full stroke, increment
		if( fSet ) nSeg++;
	}

	// if not set, last segment
	if( _fTFSubMovement && !fSet ) nPos = _nTFTotalSegments;
	else if( !_fTFSubMovement && !fSet && ( nPos > _nTFTotalSegments ) ) nPos = _nTFTotalSegments;

	// check to see if we're at the zero stroke
	if( dTime < st[ 0 ] ) fZeroStroke = TRUE;
	else fZeroStroke = FALSE;

	// stroke calculation (nPos = index determined above from the time range)
	if( _fTFSubMovement && !fZeroStroke ) _nTFStroke = ( nPos + 1 ) / 3;
	else if( fZeroStroke ) _nTFStroke = 0;
	else _nTFStroke = nPos + 1;

	// segment calculation is 3 x stroke in case of sub-movement...same as stroke otherwise
	nSeg = _fTFSubMovement ? _nTFStroke * 3 : _nTFStroke;

	// construct table data
	double dDeg = dS; // (JERK)
	double dSize = dVS;	// (VERTICAL SIZE)
	double dArea = dLA;	// (LOOP AREA)
	CString szTmp;
	if( !m_fSpectrum )
	{
		szTmp.Format( PETAL_STR, nX, _nTFStroke, nSeg, dZ, dDeg, dVS, dArea );
		if( szTmp.GetLength() > ( TBL_DATA_SIZE - 1 ) )
			sprintf_s( szTblData, PETAL_STR_2, nX, _nTFStroke, nSeg, dZ, dDeg, dVS, dArea );
		else
			sprintf_s( szTblData, PETAL_STR, nX, _nTFStroke, nSeg, dZ, dDeg, dVS, dArea );
	}
	else
	{
		sprintf_s( szTblData, PETAL_STR_SPECTRA, nX );
	}
	PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
	PEdrawtable( *hPE, 0, NULL );

	return S_OK;
}

STDMETHODIMP ShowTF::GetStroke( short* pVal )
{
	if( pVal )
	{
		*pVal = (short)_nTFStroke;
		return S_OK;
	}
	return E_FAIL;
}

STDMETHODIMP ShowTF::GetCursorPos( VARIANT *PEHandle, LONG* Pos )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	*Pos = PEnget( *hPE, PEP_nCURSORPOINT );
	return S_OK;
}

STDMETHODIMP ShowTF::SetCursorPos( VARIANT *PEHandle, LONG Pos )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	PEnset( *hPE, PEP_nCURSORPOINT, Pos );
	return S_OK;
}

STDMETHODIMP ShowTF::SetLineThickness( VARIANT* PEHandle, short Thickness )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	if( ( Thickness <= 0 ) || ( Thickness > 7 ) ) return E_FAIL;

	int nT[ 2 ] = { PELT_THINSOLID, PELT_THINSOLID };
	switch( Thickness )
	{
		case 1: nT[ 0 ] = nT[ 1 ] = PELT_EXTRATHINSOLID; break;
		case 2: nT[ 0 ] = nT[ 1 ] = PELT_THINSOLID; break;
		case 3: nT[ 0 ] = nT[ 1 ] = PELT_MEDIUMTHINSOLID; break;
		case 4: nT[ 0 ] = nT[ 1 ] = PELT_MEDIUMSOLID; break;
		case 5: nT[ 0 ] = nT[ 1 ] = PELT_MEDIUMTHICKSOLID; break;
		case 6: nT[ 0 ] = nT[ 1 ] = PELT_THICKSOLID; break;
		case 7: nT[ 0 ] = nT[ 1 ] = PELT_EXTRATHICKSOLID; break;
	}

	PEvset( *hPE, PEP_naSUBSETLINETYPES, nT, 2 );
	PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowTF::SetYZInverted(VARIANT* PEHandle, TFGraph GraphType, BOOL fOn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !PEHandle ) return E_FAIL;

	m_fInverted = !m_fInverted;

	BOOL fRet = FALSE;
	if( ( GraphType & TF_3D ) == TF_3D )
	{
		GraphType = (TFGraph)( GraphType - eTF_3D );
		if( GraphType == TF_XY )
			fRet = ShowXY3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_XZ )
			fRet = ShowXZ3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_YZ )
			fRet = ShowYZ3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_Xt )
			fRet = ShowXt3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_Yt )
			fRet = ShowYt3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_Zt )
			fRet = ShowZt3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_JXt )
			fRet = ShowJXt3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_JYt )
			fRet = ShowJYt3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_JABSt )
			fRet = ShowJABSt3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_vx )
			fRet = ShowVX3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_vy )
			fRet = ShowVY3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_vz )
			fRet = ShowVZ3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_ax )
			fRet = ShowAX3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_ay )
			fRet = ShowAY3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_az )
			fRet = ShowAZ3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( GraphType == TF_vabs )
			fRet = ShowVABS3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
	}
	else if( GraphType == TF_XY )
		fRet = ShowXY( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_XZ )
		fRet = ShowXZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_YZ )
		fRet = ShowYZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vz )
		fRet = ShowVZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_az )
		fRet = ShowAZ( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( ( GraphType == TF_aspec ) || ( GraphType == TF_vaspec ) || ( GraphType == TF_aaspec ) )
		fRet = ShowAspec( NULL, NULL, (HWND*)PEHandle, GraphType, TRUE );
	else if( GraphType == TF_Xt )
		fRet = ShowXt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_Yt )
		fRet = ShowYt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_Zt )
		fRet = ShowZt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vx )
		fRet = ShowVX( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vy )
		fRet = ShowVY( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_vabs )
		fRet = ShowVABS( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_ax )
		fRet = ShowAX( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_ay )
		fRet = ShowAY( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_JXt )
		fRet = ShowJXt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_JYt )
		fRet = ShowJYt( NULL, NULL, (HWND*)PEHandle, TRUE );
	else if( GraphType == TF_JABSt )
		fRet = ShowJABSt( NULL, NULL, (HWND*)PEHandle, TRUE );

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowTF::Customize( VARIANT* PEHandle )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;
	int nRes = PElaunchcustomize( *hPE );
	return( ( nRes == IDOK ) ? S_OK : S_FALSE );
}

STDMETHODIMP ShowTF::SetMissingDataValue(double MDVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	::SetMissingDataValue( MDVal );
	return S_OK;
}

STDMETHODIMP ShowTF::SetAnnotationSymbols(short SymbolSeg, short SymbolSubMvmt)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_nSymbolSeg = SymbolSeg;
	m_nSymbolSubMvmt = SymbolSubMvmt;
	return S_OK;
}

STDMETHODIMP ShowTF::ShowChartData(VARIANT* PEHandle, BSTR Val)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	char szTblData[ TBL_DATA_SIZE ];
	CString szVal = Val;
	strncpy_s( szTblData, TBL_DATA_SIZE, szVal.GetBuffer(), TBL_DATA_SIZE );

	DWORD dwColor;
	dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 1);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_BOTTOM_LEFT);
	BOOL fShow = ( szVal == _T("") ) ? FALSE : TRUE;
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, fShow);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 60);
	PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
	m_fShowData = fShow;

	// main table
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, fShow);

	// cursor
	if( !m_fGraphOnly )
	{
		if( fShow )
		{
			PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);
			PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
			PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);
		}
		else
		{
			PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);
			PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, FALSE);
			PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_NONE);
		}
	}

	//Update image and force paint
	PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

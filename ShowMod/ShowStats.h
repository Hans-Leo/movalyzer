// ShowStats.h: Definition of the ShowStats class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHOWSTATS_H__B439BE56_3B12_11D4_8B7F_00104BC7E2C8__INCLUDED_)
#define AFX_SHOWSTATS_H__B439BE56_3B12_11D4_8B7F_00104BC7E2C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

#define	LBL_SIZE	150

/////////////////////////////////////////////////////////////////////////////
// ShowStats

class ShowStats : 
	public IDispatchImpl<IShowStats, &IID_IShowStats, &LIBID_SHOWMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<ShowStats,&CLSID_ShowStats>
{
public:
	ShowStats();
	~ShowStats();

BEGIN_COM_MAP(ShowStats)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IShowStats)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(ShowStats) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_ShowStats)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IShowStats
public:
	STDMETHOD(SetMissingDataValue)(/*[in]*/ double MDVal);
	STDMETHOD(PrintGraph)(/*[in]*/ VARIANT* PEHandle);
	STDMETHOD(get_StdDevN)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_StdDevN)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SubsetName)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SubsetName)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_SubsetColumn)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_SubsetColumn)(/*[in]*/ short newVal);
	STDMETHOD(get_YStroke)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_YStroke)(/*[in]*/ short newVal);
	STDMETHOD(get_XStroke)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_XStroke)(/*[in]*/ short newVal);
	STDMETHOD(get_UseSpecificStrokes)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_UseSpecificStrokes)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_FlagColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_FlagColor)(/*[in]*/ long newVal);
	STDMETHOD(get_FlagSize)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_FlagSize)(/*[in]*/ short newVal);
	STDMETHOD(get_FlagType)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_FlagType)(/*[in]*/ short newVal);
	STDMETHOD(get_FlagMax)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_FlagMax)(/*[in]*/ double newVal);
	STDMETHOD(get_FlagMin)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_FlagMin)(/*[in]*/ double newVal);
	STDMETHOD(get_FlagIndex)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_FlagIndex)(/*[in]*/ short newVal);
	STDMETHOD(get_UseFlagging)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_UseFlagging)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_GroupingItemColor)(/*[in]*/ short Idx, /*[out, retval]*/ long *pVal);
	STDMETHOD(put_GroupingItemColor)(/*[in]*/ short Idx, /*[in]*/ long newVal);
	STDMETHOD(get_GroupingItemSize)(/*[in]*/ short Idx, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_GroupingItemSize)(/*[in]*/ short Idx, /*[in]*/ short newVal);
	STDMETHOD(get_GroupingItemType)(/*[in]*/ short Idx, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_GroupingItemType)(/*[in]*/ short Idx, /*[in]*/ short newVal);
	STDMETHOD(get_GroupingItemIndex)(/*[in]*/ short Idx, /*[out, retval]*/ short *pVal);
	STDMETHOD(put_GroupingItemIndex)(/*[in]*/ short Idx, /*[in]*/ short newVal);
	STDMETHOD(get_GroupingCount)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_GroupingCount)(/*[in]*/ short newVal);
	STDMETHOD(get_Grouping)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_Grouping)(/*[in]*/ short newVal);
	STDMETHOD(get_UseGrouping)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_UseGrouping)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ExcludeValY)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_ExcludeValY)(/*[in]*/ double newVal);
	STDMETHOD(get_ExcludeY)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ExcludeY)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_ExcludeValX)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_ExcludeValX)(/*[in]*/ double newVal);
	STDMETHOD(get_ExcludeX)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_ExcludeX)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_YDispersionStrength)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_YDispersionStrength)(/*[in]*/ double newVal);
	STDMETHOD(get_YDispersionIndex)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_YDispersionIndex)(/*[in]*/ short newVal);
	STDMETHOD(get_UseYDispersion)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_UseYDispersion)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_XDispersionStrength)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_XDispersionStrength)(/*[in]*/ double newVal);
	STDMETHOD(get_XDispersionIndex)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_XDispersionIndex)(/*[in]*/ short newVal);
	STDMETHOD(get_UseXDispersion)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_UseXDispersion)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_SubMovementFilter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_SubMovementFilter)(/*[in]*/ short newVal);
	STDMETHOD(get_StrokeFilter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_StrokeFilter)(/*[in]*/ short newVal);
	STDMETHOD(get_TrialFilter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_TrialFilter)(/*[in]*/ short newVal);
	STDMETHOD(get_ConditionFilter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_ConditionFilter)(/*[in]*/ short newVal);
	STDMETHOD(get_SubjectFilter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_SubjectFilter)(/*[in]*/ short newVal);
	STDMETHOD(get_GroupFilter)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_GroupFilter)(/*[in]*/ short newVal);
	STDMETHOD(ShowStatGraph)(/*[in]*/ VARIANT* GraphWindow, BOOL ShowScat,
							 /*[in]*/ BSTR StatFile, /*[in]*/ BSTR ScatFile,
							 /*[in]*/ BSTR Experiment, /*[in]*/ BSTR Grouping,
							 /*[in]*/ BSTR Title, /*[in]*/ BSTR SubTitle,
							 /*[in]*/ BSTR XAxisLabel, /*[in]*/ BSTR YAxisLabel,
							 /*[in]*/ short XAxisColumn, /*[in]*/ short YAxisColumn,
							 /*[in]*/ short StatColumnX, /*[in]*/ short StatColumnY,
							 /*[in]*/ short StdDevColumn,  // -1 if nothing
							 /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(ShowScatter)  (/*[in]*/ VARIANT* GraphWindow, /*[in]*/ BSTR InFile,
							 /*[in]*/ BSTR Experiment, /*[in]*/ BSTR Grouping,
							 /*[in]*/ BSTR XAxisLabel, /*[in]*/ BSTR YAxisLabel,
							 /*[in]*/ short XAxisColumn, /*[in]*/ short YAxisColumn,
							 /*[out,retval]*/ VARIANT* PEHandle  );
	STDMETHOD(ShowCombo)	(/*[in]*/ VARIANT* GraphWindow, BOOL ShowScat,
							 /*[in]*/ BSTR StatFile, /*[in]*/ BSTR ScatFile,
							 /*[in]*/ BSTR Experiment, /*[in]*/ BSTR Grouping,
							 /*[in]*/ BSTR Title, /*[in]*/ BSTR SubTitle,
							 /*[in]*/ BSTR XAxisLabel, /*[in]*/ BSTR YAxisLabel,
							 /*[in]*/ short XAxisColumn, /*[in]*/ short YAxisColumn,
							 /*[in]*/ short StatColumnX, /*[in]*/ short StatColumnY,
							 /*[in]*/ short StdDevColumn,  // -1 if nothing
							 /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(ShowClear)	(/*[in]*/ VARIANT* GraphWindow,
							 /*[out,retval]*/ VARIANT* PEHandle);
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);
protected:
	BOOL Init( int& numX, int& numY );
	BOOL InitScatter();
	void SortBySubset( int nCount, int nLvls );

// Attributes
protected:
	CString		m_szFile;
	CString		m_szExp;
	CString		m_szGrp;
	CString		m_szTitle;
	CString		m_szSub;
	CString		m_szX;
	CString		m_szY;
	CStringList	lstX;
	int			m_nX;
	int			m_nY;
	int			m_nStdDev;
	double*		x;
	double*		y;
	double*		sx;
	double*		sy;
	double*		stddev;
	int*		lvl;
	int*		lvl2;
	int*		subset;
	int*		slvl;
	int*		slvl2;
	int*		ssubset;
	BOOL*		flags;
	int			m_nCnt;

	int			m_nSubCol;
	CString		m_szSubset;

	int			m_nGrp;
	int			m_nSubj;
	int			m_nCond;
	int			m_nTrial;
	int			m_nStroke;
	int			m_nSubMvmt;

	BOOL		m_fDispX;
	int			m_nDispX;
	double		m_dStrengthX;
	BOOL		m_fDispY;
	int			m_nDispY;
	double		m_dStrengthY;

	BOOL		m_fGrouping;
	int			m_nGrouping;
	int			m_nGroupingCount;
	int*		m_pItems;
	int*		m_pTypes;
	int*		m_pSizes;
	long*		m_pColors;

	BOOL		m_fFlag;
	int			m_nFlagIdx;
	double		m_dFlagMin;
	double		m_dFlagMax;
	int			m_nFlagType;
	int			m_nFlagSize;
	long		m_nFlagColor;

	BOOL		m_fExcludeX;
	double		m_dExcludeValX;
	BOOL		m_fExcludeY;
	double		m_dExcludeValY;

	double		m_dXMin;
	double		m_dXMax;
	double		m_dYMin;
	double		m_dYMax;
	double		m_dsXMin;
	double		m_dsXMax;
	double		m_dsYMin;
	double		m_dsYMax;

	BOOL		m_fUseStrokes;
	int			m_nXStroke;
	int			m_nYStroke;

	BOOL		m_fStdDevN;

public:
	// FORMERLY STATIC/GLOBAL VARS
	struct StatData
	{
		int nCnt1, nCnt2;
		float fX, fY;
		int nSubset;
		int nSubset2;
		char label[ LBL_SIZE ];
		double dX, dY;
		DWORD dwCol;
	};
	StatData _sd;
	int _cnt;
	CStringList lstSubsets;
};

#endif // !defined(AFX_SHOWSTATS_H__B439BE56_3B12_11D4_8B7F_00104BC7E2C8__INCLUDED_)

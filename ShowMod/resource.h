//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ShowMod.rc
//
#define IDR_ShowHWR                     2200
#define IDR_ShowStats                   2201
#define IDR_ShowTF                      2202
#define IDS_HWR_SUB1                    2203
#define IDS_HWR_SUB2                    2204
#define IDS_HWR_X1                      2205
#define IDS_HWR_Y1                      2206
#define IDS_HWR_Y2                      2207
#define IDS_PROJNAME                    2208
#define IDS_RAFTOR_1                    2209
#define IDS_RNPOL_1                     2210
#define IDS_SHOWHWR_DESC                2211
#define IDS_SHOWSTATS_DESC              2212
#define IDS_SHOWTF_DESC                 2213
#define IDS_SS_1                        2214
#define IDS_TF_ASPSUB                   2215
#define IDS_TF_ASPSUB1                  2216
#define IDS_TF_ASPSUB2                  2217
#define IDS_TF_ASPSUBA                  2218
#define IDS_TF_ASPSUBV                  2219
#define IDS_TF_ASPSUBV2                 2220
#define IDS_TF_ASPY                     2221
#define IDS_TF_AX                       2222
#define IDS_TF_AXY                      2223
#define IDS_TF_AY                       2224
#define IDS_TF_AYY                      2225
#define IDS_TF_VABS                     2226
#define IDS_TF_VABSY                    2227
#define IDS_TF_VX                       2228
#define IDS_TF_VXY                      2229
#define IDS_TF_VY                       2230
#define IDS_TF_VYY                      2231
#define IDS_TF_X1                       2232
#define IDS_TF_XT                       2233
#define IDS_TF_XTY                      2234
#define IDS_TF_YT                       2235
#define IDS_TF_YTY                      2236
#define IDS_TF_ZT                       2237
#define IDS_TF_ZTY                      2238

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2239
#define _APS_NEXT_COMMAND_VALUE         32767
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           2339
#endif
#endif

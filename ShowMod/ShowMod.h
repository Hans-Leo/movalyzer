

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sat Apr 02 19:02:17 2016
 */
/* Compiler settings for .\ShowMod.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ShowMod_h__
#define __ShowMod_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IShowHWR_FWD_DEFINED__
#define __IShowHWR_FWD_DEFINED__
typedef interface IShowHWR IShowHWR;
#endif 	/* __IShowHWR_FWD_DEFINED__ */


#ifndef __IShowTF_FWD_DEFINED__
#define __IShowTF_FWD_DEFINED__
typedef interface IShowTF IShowTF;
#endif 	/* __IShowTF_FWD_DEFINED__ */


#ifndef __IShowStats_FWD_DEFINED__
#define __IShowStats_FWD_DEFINED__
typedef interface IShowStats IShowStats;
#endif 	/* __IShowStats_FWD_DEFINED__ */


#ifndef __ShowHWR_FWD_DEFINED__
#define __ShowHWR_FWD_DEFINED__

#ifdef __cplusplus
typedef class ShowHWR ShowHWR;
#else
typedef struct ShowHWR ShowHWR;
#endif /* __cplusplus */

#endif 	/* __ShowHWR_FWD_DEFINED__ */


#ifndef __ShowTF_FWD_DEFINED__
#define __ShowTF_FWD_DEFINED__

#ifdef __cplusplus
typedef class ShowTF ShowTF;
#else
typedef struct ShowTF ShowTF;
#endif /* __cplusplus */

#endif 	/* __ShowTF_FWD_DEFINED__ */


#ifndef __ShowStats_FWD_DEFINED__
#define __ShowStats_FWD_DEFINED__

#ifdef __cplusplus
typedef class ShowStats ShowStats;
#else
typedef struct ShowStats ShowStats;
#endif /* __cplusplus */

#endif 	/* __ShowStats_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_ShowMod_0000_0000 */
/* [local] */ 

#pragma once


extern RPC_IF_HANDLE __MIDL_itf_ShowMod_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_ShowMod_0000_0000_v0_0_s_ifspec;

#ifndef __IShowHWR_INTERFACE_DEFINED__
#define __IShowHWR_INTERFACE_DEFINED__

/* interface IShowHWR */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eHWRGraph
    {	eHWR_XY	= 0x1,
	eHWR_XYt	= 0x2,
	eHWR_Zt	= 0x3,
	eHWR_XZ	= 0x4,
	eHWR_YZ	= 0x5,
	eHWR_3D	= 0x100
    } 	HWRGraph;


EXTERN_C const IID IID_IShowHWR;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E8B28DA0-96E8-11D3-8A27-00104BC7E2C8")
    IShowHWR : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowHWRGraph( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ VARIANT *GraphDlg,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowHWRGraphRT( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ HWRGraph GraphType,
            /* [out] */ LONG *Cnt,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowHWRMultiple( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ VARIANT *GraphDlg,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnTimer( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddFile( 
            /* [in] */ BSTR File,
            /* [in] */ BOOL Clear) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_HWR_InFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Frequency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinPenPressure( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PenUpColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UseOptimalScaling( 
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PrintGraph( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExportGraph( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Monochrome( 
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMonochrome( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ BOOL fMultiple,
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnCursorMove( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ HWRGraph GraphType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCursorPos( 
            /* [in] */ VARIANT *PEHandle,
            /* [retval][out] */ LONG *Pos) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCursorPos( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ LONG Pos) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PenPressureLineThickness( 
            /* [in] */ BOOL PPLT) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLineThickness( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ short Thickness) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetYZInverted( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ BOOL fMultiple,
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Customize( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMissingDataValue( 
            /* [in] */ double MDVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IShowHWRVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IShowHWR * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IShowHWR * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IShowHWR * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IShowHWR * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IShowHWR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IShowHWR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IShowHWR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowHWRGraph )( 
            IShowHWR * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ VARIANT *GraphDlg,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowHWRGraphRT )( 
            IShowHWR * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ HWRGraph GraphType,
            /* [out] */ LONG *Cnt,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowHWRMultiple )( 
            IShowHWR * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ VARIANT *GraphDlg,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnTimer )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddFile )( 
            IShowHWR * This,
            /* [in] */ BSTR File,
            /* [in] */ BOOL Clear);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HWR_InFile )( 
            IShowHWR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Frequency )( 
            IShowHWR * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinPenPressure )( 
            IShowHWR * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IShowHWR * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PenUpColor )( 
            IShowHWR * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UseOptimalScaling )( 
            IShowHWR * This,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrintGraph )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ExportGraph )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Monochrome )( 
            IShowHWR * This,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMonochrome )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ BOOL fMultiple,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnCursorMove )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ HWRGraph GraphType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCursorPos )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle,
            /* [retval][out] */ LONG *Pos);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCursorPos )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ LONG Pos);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PenPressureLineThickness )( 
            IShowHWR * This,
            /* [in] */ BOOL PPLT);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLineThickness )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ short Thickness);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetYZInverted )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ HWRGraph GraphType,
            /* [in] */ BOOL fMultiple,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Customize )( 
            IShowHWR * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMissingDataValue )( 
            IShowHWR * This,
            /* [in] */ double MDVal);
        
        END_INTERFACE
    } IShowHWRVtbl;

    interface IShowHWR
    {
        CONST_VTBL struct IShowHWRVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IShowHWR_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IShowHWR_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IShowHWR_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IShowHWR_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IShowHWR_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IShowHWR_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IShowHWR_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IShowHWR_ShowHWRGraph(This,GraphWindow,GraphType,GraphDlg,PEHandle)	\
    ( (This)->lpVtbl -> ShowHWRGraph(This,GraphWindow,GraphType,GraphDlg,PEHandle) ) 

#define IShowHWR_ShowHWRGraphRT(This,GraphWindow,GraphType,Cnt,PEHandle)	\
    ( (This)->lpVtbl -> ShowHWRGraphRT(This,GraphWindow,GraphType,Cnt,PEHandle) ) 

#define IShowHWR_ShowHWRMultiple(This,GraphWindow,GraphType,GraphDlg,PEHandle)	\
    ( (This)->lpVtbl -> ShowHWRMultiple(This,GraphWindow,GraphType,GraphDlg,PEHandle) ) 

#define IShowHWR_OnTimer(This,PEHandle)	\
    ( (This)->lpVtbl -> OnTimer(This,PEHandle) ) 

#define IShowHWR_AddFile(This,File,Clear)	\
    ( (This)->lpVtbl -> AddFile(This,File,Clear) ) 

#define IShowHWR_put_HWR_InFile(This,newVal)	\
    ( (This)->lpVtbl -> put_HWR_InFile(This,newVal) ) 

#define IShowHWR_put_Frequency(This,newVal)	\
    ( (This)->lpVtbl -> put_Frequency(This,newVal) ) 

#define IShowHWR_put_MinPenPressure(This,newVal)	\
    ( (This)->lpVtbl -> put_MinPenPressure(This,newVal) ) 

#define IShowHWR_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IShowHWR_put_PenUpColor(This,newVal)	\
    ( (This)->lpVtbl -> put_PenUpColor(This,newVal) ) 

#define IShowHWR_UseOptimalScaling(This,fOn)	\
    ( (This)->lpVtbl -> UseOptimalScaling(This,fOn) ) 

#define IShowHWR_PrintGraph(This,PEHandle)	\
    ( (This)->lpVtbl -> PrintGraph(This,PEHandle) ) 

#define IShowHWR_ExportGraph(This,PEHandle)	\
    ( (This)->lpVtbl -> ExportGraph(This,PEHandle) ) 

#define IShowHWR_put_Monochrome(This,fOn)	\
    ( (This)->lpVtbl -> put_Monochrome(This,fOn) ) 

#define IShowHWR_SetMonochrome(This,PEHandle,GraphType,fMultiple,fOn)	\
    ( (This)->lpVtbl -> SetMonochrome(This,PEHandle,GraphType,fMultiple,fOn) ) 

#define IShowHWR_OnCursorMove(This,PEHandle,GraphType)	\
    ( (This)->lpVtbl -> OnCursorMove(This,PEHandle,GraphType) ) 

#define IShowHWR_GetCursorPos(This,PEHandle,Pos)	\
    ( (This)->lpVtbl -> GetCursorPos(This,PEHandle,Pos) ) 

#define IShowHWR_SetCursorPos(This,PEHandle,Pos)	\
    ( (This)->lpVtbl -> SetCursorPos(This,PEHandle,Pos) ) 

#define IShowHWR_PenPressureLineThickness(This,PPLT)	\
    ( (This)->lpVtbl -> PenPressureLineThickness(This,PPLT) ) 

#define IShowHWR_SetLineThickness(This,PEHandle,Thickness)	\
    ( (This)->lpVtbl -> SetLineThickness(This,PEHandle,Thickness) ) 

#define IShowHWR_SetYZInverted(This,PEHandle,GraphType,fMultiple,fOn)	\
    ( (This)->lpVtbl -> SetYZInverted(This,PEHandle,GraphType,fMultiple,fOn) ) 

#define IShowHWR_Customize(This,PEHandle)	\
    ( (This)->lpVtbl -> Customize(This,PEHandle) ) 

#define IShowHWR_SetMissingDataValue(This,MDVal)	\
    ( (This)->lpVtbl -> SetMissingDataValue(This,MDVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IShowHWR_INTERFACE_DEFINED__ */


#ifndef __IShowTF_INTERFACE_DEFINED__
#define __IShowTF_INTERFACE_DEFINED__

/* interface IShowTF */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eTFGraph
    {	eTF_XY	= 0x1,
	eTF_Xt	= 0x2,
	eTF_Yt	= 0x3,
	eTF_Zt	= 0x4,
	eTF_vx	= 0x5,
	eTF_vy	= 0x6,
	eTF_vabs	= 0x7,
	eTF_ax	= 0x8,
	eTF_ay	= 0x9,
	eTF_JXt	= 0x1000,
	eTF_JYt	= 0x4000,
	eTF_JABSt	= 0x8000,
	eTF_aspec	= 0x10,
	eTF_XZ	= 0x11,
	eTF_YZ	= 0x12,
	eTF_vz	= 0x13,
	eTF_az	= 0x14,
	eTF_vaspec	= 0x15,
	eTF_aaspec	= 0x16,
	eTF_3D	= 0x100
    } 	TFGraph;

typedef 
enum eTFAxis
    {	eTF_COL_NONE	= 0,
	eTF_COL_X	= 1,
	eTF_COL_Y	= 2,
	eTF_COL_Z	= 3,
	eTF_COL_VX	= 4,
	eTF_COL_VY	= 5,
	eTF_COL_VZ	= 6,
	eTF_COL_AX	= 7,
	eTF_COL_AY	= 8,
	eTF_COL_AZ	= 9,
	eTF_COL_JX	= 10,
	eTF_COL_JY	= 11,
	eTF_COL_JZ	= 12,
	eTF_COL_Time	= 13
    } 	TFAxis;


EXTERN_C const IID IID_IShowTF;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E8B28DA3-96E8-11D3-8A27-00104BC7E2C8")
    IShowTF : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowTFGraph( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ TFGraph GraphType,
            /* [in] */ VARIANT *GraphDlg,
            /* [in] */ BOOL AnnValues,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TF_InFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SEG_InFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Frequency( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UseOptimalScaling( 
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PrintGraph( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExportGraph( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Monochrome( 
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMonochrome( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ TFGraph GraphType,
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Feedback( 
            /* [in] */ BOOL pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Feature( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowTFGraphRT( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ TFGraph GraphType,
            /* [out] */ LONG *Cnt,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnTimer( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowOnlyGraph( 
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RTFrequency( 
            /* [in] */ short Freq) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_PenUpColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinPenPressure( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnCursorMove( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ TFGraph GraphType) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCursorPos( 
            /* [in] */ VARIANT *PEHandle,
            /* [retval][out] */ LONG *Pos) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetCursorPos( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ LONG Pos) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SupressAnn( 
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLineThickness( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ short Thickness) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetYZInverted( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ TFGraph GraphType,
            /* [in] */ BOOL fOn) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Customize( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetStroke( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowTFGraph2( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ TFAxis XAxis,
            /* [in] */ TFAxis YAxis,
            /* [in] */ VARIANT *GraphDlg,
            /* [in] */ BOOL AnnValues,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMissingDataValue( 
            /* [in] */ double MDVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAnnotationSymbols( 
            /* [in] */ short SymbolSeg,
            /* [in] */ short SymbolSubMvmt) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowChartData( 
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ BSTR Val) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IShowTFVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IShowTF * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IShowTF * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IShowTF * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IShowTF * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IShowTF * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IShowTF * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IShowTF * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowTFGraph )( 
            IShowTF * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ TFGraph GraphType,
            /* [in] */ VARIANT *GraphDlg,
            /* [in] */ BOOL AnnValues,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TF_InFile )( 
            IShowTF * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SEG_InFile )( 
            IShowTF * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Frequency )( 
            IShowTF * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IShowTF * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UseOptimalScaling )( 
            IShowTF * This,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrintGraph )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ExportGraph )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Monochrome )( 
            IShowTF * This,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMonochrome )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ TFGraph GraphType,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Feedback )( 
            IShowTF * This,
            /* [in] */ BOOL pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Feature )( 
            IShowTF * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinColor )( 
            IShowTF * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxColor )( 
            IShowTF * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowTFGraphRT )( 
            IShowTF * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ TFGraph GraphType,
            /* [out] */ LONG *Cnt,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnTimer )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowOnlyGraph )( 
            IShowTF * This,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RTFrequency )( 
            IShowTF * This,
            /* [in] */ short Freq);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_PenUpColor )( 
            IShowTF * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinPenPressure )( 
            IShowTF * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnCursorMove )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ TFGraph GraphType);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCursorPos )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [retval][out] */ LONG *Pos);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetCursorPos )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ LONG Pos);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SupressAnn )( 
            IShowTF * This,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLineThickness )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ short Thickness);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetYZInverted )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ TFGraph GraphType,
            /* [in] */ BOOL fOn);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Customize )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetStroke )( 
            IShowTF * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowTFGraph2 )( 
            IShowTF * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ TFAxis XAxis,
            /* [in] */ TFAxis YAxis,
            /* [in] */ VARIANT *GraphDlg,
            /* [in] */ BOOL AnnValues,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMissingDataValue )( 
            IShowTF * This,
            /* [in] */ double MDVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAnnotationSymbols )( 
            IShowTF * This,
            /* [in] */ short SymbolSeg,
            /* [in] */ short SymbolSubMvmt);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowChartData )( 
            IShowTF * This,
            /* [in] */ VARIANT *PEHandle,
            /* [in] */ BSTR Val);
        
        END_INTERFACE
    } IShowTFVtbl;

    interface IShowTF
    {
        CONST_VTBL struct IShowTFVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IShowTF_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IShowTF_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IShowTF_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IShowTF_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IShowTF_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IShowTF_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IShowTF_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IShowTF_ShowTFGraph(This,GraphWindow,GraphType,GraphDlg,AnnValues,PEHandle)	\
    ( (This)->lpVtbl -> ShowTFGraph(This,GraphWindow,GraphType,GraphDlg,AnnValues,PEHandle) ) 

#define IShowTF_put_TF_InFile(This,newVal)	\
    ( (This)->lpVtbl -> put_TF_InFile(This,newVal) ) 

#define IShowTF_put_SEG_InFile(This,newVal)	\
    ( (This)->lpVtbl -> put_SEG_InFile(This,newVal) ) 

#define IShowTF_put_Frequency(This,newVal)	\
    ( (This)->lpVtbl -> put_Frequency(This,newVal) ) 

#define IShowTF_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IShowTF_UseOptimalScaling(This,fOn)	\
    ( (This)->lpVtbl -> UseOptimalScaling(This,fOn) ) 

#define IShowTF_PrintGraph(This,PEHandle)	\
    ( (This)->lpVtbl -> PrintGraph(This,PEHandle) ) 

#define IShowTF_ExportGraph(This,PEHandle)	\
    ( (This)->lpVtbl -> ExportGraph(This,PEHandle) ) 

#define IShowTF_put_Monochrome(This,fOn)	\
    ( (This)->lpVtbl -> put_Monochrome(This,fOn) ) 

#define IShowTF_SetMonochrome(This,PEHandle,GraphType,fOn)	\
    ( (This)->lpVtbl -> SetMonochrome(This,PEHandle,GraphType,fOn) ) 

#define IShowTF_put_Feedback(This,pVal)	\
    ( (This)->lpVtbl -> put_Feedback(This,pVal) ) 

#define IShowTF_put_Feature(This,newVal)	\
    ( (This)->lpVtbl -> put_Feature(This,newVal) ) 

#define IShowTF_put_MinColor(This,newVal)	\
    ( (This)->lpVtbl -> put_MinColor(This,newVal) ) 

#define IShowTF_put_MaxColor(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxColor(This,newVal) ) 

#define IShowTF_ShowTFGraphRT(This,GraphWindow,GraphType,Cnt,PEHandle)	\
    ( (This)->lpVtbl -> ShowTFGraphRT(This,GraphWindow,GraphType,Cnt,PEHandle) ) 

#define IShowTF_OnTimer(This,PEHandle)	\
    ( (This)->lpVtbl -> OnTimer(This,PEHandle) ) 

#define IShowTF_put_ShowOnlyGraph(This,fOn)	\
    ( (This)->lpVtbl -> put_ShowOnlyGraph(This,fOn) ) 

#define IShowTF_put_RTFrequency(This,Freq)	\
    ( (This)->lpVtbl -> put_RTFrequency(This,Freq) ) 

#define IShowTF_put_PenUpColor(This,newVal)	\
    ( (This)->lpVtbl -> put_PenUpColor(This,newVal) ) 

#define IShowTF_put_MinPenPressure(This,newVal)	\
    ( (This)->lpVtbl -> put_MinPenPressure(This,newVal) ) 

#define IShowTF_OnCursorMove(This,PEHandle,GraphType)	\
    ( (This)->lpVtbl -> OnCursorMove(This,PEHandle,GraphType) ) 

#define IShowTF_GetCursorPos(This,PEHandle,Pos)	\
    ( (This)->lpVtbl -> GetCursorPos(This,PEHandle,Pos) ) 

#define IShowTF_SetCursorPos(This,PEHandle,Pos)	\
    ( (This)->lpVtbl -> SetCursorPos(This,PEHandle,Pos) ) 

#define IShowTF_put_SupressAnn(This,fOn)	\
    ( (This)->lpVtbl -> put_SupressAnn(This,fOn) ) 

#define IShowTF_SetLineThickness(This,PEHandle,Thickness)	\
    ( (This)->lpVtbl -> SetLineThickness(This,PEHandle,Thickness) ) 

#define IShowTF_SetYZInverted(This,PEHandle,GraphType,fOn)	\
    ( (This)->lpVtbl -> SetYZInverted(This,PEHandle,GraphType,fOn) ) 

#define IShowTF_Customize(This,PEHandle)	\
    ( (This)->lpVtbl -> Customize(This,PEHandle) ) 

#define IShowTF_GetStroke(This,pVal)	\
    ( (This)->lpVtbl -> GetStroke(This,pVal) ) 

#define IShowTF_ShowTFGraph2(This,GraphWindow,XAxis,YAxis,GraphDlg,AnnValues,PEHandle)	\
    ( (This)->lpVtbl -> ShowTFGraph2(This,GraphWindow,XAxis,YAxis,GraphDlg,AnnValues,PEHandle) ) 

#define IShowTF_SetMissingDataValue(This,MDVal)	\
    ( (This)->lpVtbl -> SetMissingDataValue(This,MDVal) ) 

#define IShowTF_SetAnnotationSymbols(This,SymbolSeg,SymbolSubMvmt)	\
    ( (This)->lpVtbl -> SetAnnotationSymbols(This,SymbolSeg,SymbolSubMvmt) ) 

#define IShowTF_ShowChartData(This,PEHandle,Val)	\
    ( (This)->lpVtbl -> ShowChartData(This,PEHandle,Val) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IShowTF_INTERFACE_DEFINED__ */


#ifndef __IShowStats_INTERFACE_DEFINED__
#define __IShowStats_INTERFACE_DEFINED__

/* interface IShowStats */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IShowStats;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B439BE54-3B12-11D4-8B7F-00104BC7E2C8")
    IShowStats : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowStatGraph( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ BOOL ShowScat,
            /* [in] */ BSTR StatFile,
            /* [in] */ BSTR ScatFile,
            /* [in] */ BSTR Experiment,
            /* [in] */ BSTR Grouping,
            /* [in] */ BSTR Title,
            /* [in] */ BSTR SubTitle,
            /* [in] */ BSTR XAxisLabel,
            /* [in] */ BSTR YAxisLabel,
            /* [in] */ short XAxisColumn,
            /* [in] */ short YAxisColumn,
            /* [in] */ short StatColumnX,
            /* [in] */ short StatColumnY,
            /* [in] */ short StdDevColumn,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowScatter( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ BSTR InFile,
            /* [in] */ BSTR Experiment,
            /* [in] */ BSTR Grouping,
            /* [in] */ BSTR XAxisLabel,
            /* [in] */ BSTR YAxisLabel,
            /* [in] */ short XAxisColumn,
            /* [in] */ short YAxisColumn,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupFilter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupFilter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubjectFilter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubjectFilter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ConditionFilter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ConditionFilter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TrialFilter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TrialFilter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StrokeFilter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StrokeFilter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseXDispersion( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseXDispersion( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XDispersionIndex( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_XDispersionIndex( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XDispersionStrength( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_XDispersionStrength( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseYDispersion( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseYDispersion( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YDispersionIndex( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_YDispersionIndex( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YDispersionStrength( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_YDispersionStrength( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExcludeX( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExcludeX( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExcludeValX( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExcludeValX( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExcludeY( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExcludeY( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ExcludeValY( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExcludeValY( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseGrouping( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseGrouping( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupingCount( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupingCount( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupingItemIndex( 
            /* [in] */ short Idx,
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupingItemIndex( 
            /* [in] */ short Idx,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupingItemType( 
            /* [in] */ short Idx,
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupingItemType( 
            /* [in] */ short Idx,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupingItemSize( 
            /* [in] */ short Idx,
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupingItemSize( 
            /* [in] */ short Idx,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GroupingItemColor( 
            /* [in] */ short Idx,
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_GroupingItemColor( 
            /* [in] */ short Idx,
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseFlagging( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseFlagging( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagIndex( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagIndex( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagMin( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagMin( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagMax( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagMax( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagType( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagType( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagSize( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagSize( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_FlagColor( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FlagColor( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Grouping( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Grouping( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_UseSpecificStrokes( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseSpecificStrokes( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_XStroke( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_XStroke( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_YStroke( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_YStroke( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowCombo( 
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ BOOL ShowScat,
            /* [in] */ BSTR StatFile,
            /* [in] */ BSTR ScatFile,
            /* [in] */ BSTR Experiment,
            /* [in] */ BSTR Grouping,
            /* [in] */ BSTR Title,
            /* [in] */ BSTR SubTitle,
            /* [in] */ BSTR XAxisLabel,
            /* [in] */ BSTR YAxisLabel,
            /* [in] */ short XAxisColumn,
            /* [in] */ short YAxisColumn,
            /* [in] */ short StatColumnX,
            /* [in] */ short StatColumnY,
            /* [in] */ short StdDevColumn,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubsetColumn( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubsetColumn( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubsetName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubsetName( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_StdDevN( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StdDevN( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ShowClear( 
            /* [in] */ VARIANT *GraphWindow,
            /* [retval][out] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_SubMovementFilter( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SubMovementFilter( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE PrintGraph( 
            /* [in] */ VARIANT *PEHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMissingDataValue( 
            /* [in] */ double MDVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IShowStatsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IShowStats * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IShowStats * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IShowStats * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IShowStats * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IShowStats * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IShowStats * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IShowStats * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowStatGraph )( 
            IShowStats * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ BOOL ShowScat,
            /* [in] */ BSTR StatFile,
            /* [in] */ BSTR ScatFile,
            /* [in] */ BSTR Experiment,
            /* [in] */ BSTR Grouping,
            /* [in] */ BSTR Title,
            /* [in] */ BSTR SubTitle,
            /* [in] */ BSTR XAxisLabel,
            /* [in] */ BSTR YAxisLabel,
            /* [in] */ short XAxisColumn,
            /* [in] */ short YAxisColumn,
            /* [in] */ short StatColumnX,
            /* [in] */ short StatColumnY,
            /* [in] */ short StdDevColumn,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowScatter )( 
            IShowStats * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ BSTR InFile,
            /* [in] */ BSTR Experiment,
            /* [in] */ BSTR Grouping,
            /* [in] */ BSTR XAxisLabel,
            /* [in] */ BSTR YAxisLabel,
            /* [in] */ short XAxisColumn,
            /* [in] */ short YAxisColumn,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupFilter )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupFilter )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubjectFilter )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubjectFilter )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ConditionFilter )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ConditionFilter )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TrialFilter )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TrialFilter )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StrokeFilter )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StrokeFilter )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseXDispersion )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseXDispersion )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_XDispersionIndex )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_XDispersionIndex )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_XDispersionStrength )( 
            IShowStats * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_XDispersionStrength )( 
            IShowStats * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseYDispersion )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseYDispersion )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_YDispersionIndex )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_YDispersionIndex )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_YDispersionStrength )( 
            IShowStats * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_YDispersionStrength )( 
            IShowStats * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExcludeX )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExcludeX )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExcludeValX )( 
            IShowStats * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExcludeValX )( 
            IShowStats * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExcludeY )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExcludeY )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ExcludeValY )( 
            IShowStats * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExcludeValY )( 
            IShowStats * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseGrouping )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseGrouping )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupingCount )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupingCount )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupingItemIndex )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupingItemIndex )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupingItemType )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupingItemType )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupingItemSize )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupingItemSize )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GroupingItemColor )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_GroupingItemColor )( 
            IShowStats * This,
            /* [in] */ short Idx,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseFlagging )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseFlagging )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagIndex )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagIndex )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagMin )( 
            IShowStats * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagMin )( 
            IShowStats * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagMax )( 
            IShowStats * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagMax )( 
            IShowStats * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagType )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagType )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagSize )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagSize )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FlagColor )( 
            IShowStats * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FlagColor )( 
            IShowStats * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Grouping )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Grouping )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UseSpecificStrokes )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseSpecificStrokes )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_XStroke )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_XStroke )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_YStroke )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_YStroke )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowCombo )( 
            IShowStats * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [in] */ BOOL ShowScat,
            /* [in] */ BSTR StatFile,
            /* [in] */ BSTR ScatFile,
            /* [in] */ BSTR Experiment,
            /* [in] */ BSTR Grouping,
            /* [in] */ BSTR Title,
            /* [in] */ BSTR SubTitle,
            /* [in] */ BSTR XAxisLabel,
            /* [in] */ BSTR YAxisLabel,
            /* [in] */ short XAxisColumn,
            /* [in] */ short YAxisColumn,
            /* [in] */ short StatColumnX,
            /* [in] */ short StatColumnY,
            /* [in] */ short StdDevColumn,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubsetColumn )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubsetColumn )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubsetName )( 
            IShowStats * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubsetName )( 
            IShowStats * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IShowStats * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StdDevN )( 
            IShowStats * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StdDevN )( 
            IShowStats * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ShowClear )( 
            IShowStats * This,
            /* [in] */ VARIANT *GraphWindow,
            /* [retval][out] */ VARIANT *PEHandle);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SubMovementFilter )( 
            IShowStats * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SubMovementFilter )( 
            IShowStats * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *PrintGraph )( 
            IShowStats * This,
            /* [in] */ VARIANT *PEHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMissingDataValue )( 
            IShowStats * This,
            /* [in] */ double MDVal);
        
        END_INTERFACE
    } IShowStatsVtbl;

    interface IShowStats
    {
        CONST_VTBL struct IShowStatsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IShowStats_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IShowStats_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IShowStats_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IShowStats_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IShowStats_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IShowStats_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IShowStats_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IShowStats_ShowStatGraph(This,GraphWindow,ShowScat,StatFile,ScatFile,Experiment,Grouping,Title,SubTitle,XAxisLabel,YAxisLabel,XAxisColumn,YAxisColumn,StatColumnX,StatColumnY,StdDevColumn,PEHandle)	\
    ( (This)->lpVtbl -> ShowStatGraph(This,GraphWindow,ShowScat,StatFile,ScatFile,Experiment,Grouping,Title,SubTitle,XAxisLabel,YAxisLabel,XAxisColumn,YAxisColumn,StatColumnX,StatColumnY,StdDevColumn,PEHandle) ) 

#define IShowStats_ShowScatter(This,GraphWindow,InFile,Experiment,Grouping,XAxisLabel,YAxisLabel,XAxisColumn,YAxisColumn,PEHandle)	\
    ( (This)->lpVtbl -> ShowScatter(This,GraphWindow,InFile,Experiment,Grouping,XAxisLabel,YAxisLabel,XAxisColumn,YAxisColumn,PEHandle) ) 

#define IShowStats_get_GroupFilter(This,pVal)	\
    ( (This)->lpVtbl -> get_GroupFilter(This,pVal) ) 

#define IShowStats_put_GroupFilter(This,newVal)	\
    ( (This)->lpVtbl -> put_GroupFilter(This,newVal) ) 

#define IShowStats_get_SubjectFilter(This,pVal)	\
    ( (This)->lpVtbl -> get_SubjectFilter(This,pVal) ) 

#define IShowStats_put_SubjectFilter(This,newVal)	\
    ( (This)->lpVtbl -> put_SubjectFilter(This,newVal) ) 

#define IShowStats_get_ConditionFilter(This,pVal)	\
    ( (This)->lpVtbl -> get_ConditionFilter(This,pVal) ) 

#define IShowStats_put_ConditionFilter(This,newVal)	\
    ( (This)->lpVtbl -> put_ConditionFilter(This,newVal) ) 

#define IShowStats_get_TrialFilter(This,pVal)	\
    ( (This)->lpVtbl -> get_TrialFilter(This,pVal) ) 

#define IShowStats_put_TrialFilter(This,newVal)	\
    ( (This)->lpVtbl -> put_TrialFilter(This,newVal) ) 

#define IShowStats_get_StrokeFilter(This,pVal)	\
    ( (This)->lpVtbl -> get_StrokeFilter(This,pVal) ) 

#define IShowStats_put_StrokeFilter(This,newVal)	\
    ( (This)->lpVtbl -> put_StrokeFilter(This,newVal) ) 

#define IShowStats_get_UseXDispersion(This,pVal)	\
    ( (This)->lpVtbl -> get_UseXDispersion(This,pVal) ) 

#define IShowStats_put_UseXDispersion(This,newVal)	\
    ( (This)->lpVtbl -> put_UseXDispersion(This,newVal) ) 

#define IShowStats_get_XDispersionIndex(This,pVal)	\
    ( (This)->lpVtbl -> get_XDispersionIndex(This,pVal) ) 

#define IShowStats_put_XDispersionIndex(This,newVal)	\
    ( (This)->lpVtbl -> put_XDispersionIndex(This,newVal) ) 

#define IShowStats_get_XDispersionStrength(This,pVal)	\
    ( (This)->lpVtbl -> get_XDispersionStrength(This,pVal) ) 

#define IShowStats_put_XDispersionStrength(This,newVal)	\
    ( (This)->lpVtbl -> put_XDispersionStrength(This,newVal) ) 

#define IShowStats_get_UseYDispersion(This,pVal)	\
    ( (This)->lpVtbl -> get_UseYDispersion(This,pVal) ) 

#define IShowStats_put_UseYDispersion(This,newVal)	\
    ( (This)->lpVtbl -> put_UseYDispersion(This,newVal) ) 

#define IShowStats_get_YDispersionIndex(This,pVal)	\
    ( (This)->lpVtbl -> get_YDispersionIndex(This,pVal) ) 

#define IShowStats_put_YDispersionIndex(This,newVal)	\
    ( (This)->lpVtbl -> put_YDispersionIndex(This,newVal) ) 

#define IShowStats_get_YDispersionStrength(This,pVal)	\
    ( (This)->lpVtbl -> get_YDispersionStrength(This,pVal) ) 

#define IShowStats_put_YDispersionStrength(This,newVal)	\
    ( (This)->lpVtbl -> put_YDispersionStrength(This,newVal) ) 

#define IShowStats_get_ExcludeX(This,pVal)	\
    ( (This)->lpVtbl -> get_ExcludeX(This,pVal) ) 

#define IShowStats_put_ExcludeX(This,newVal)	\
    ( (This)->lpVtbl -> put_ExcludeX(This,newVal) ) 

#define IShowStats_get_ExcludeValX(This,pVal)	\
    ( (This)->lpVtbl -> get_ExcludeValX(This,pVal) ) 

#define IShowStats_put_ExcludeValX(This,newVal)	\
    ( (This)->lpVtbl -> put_ExcludeValX(This,newVal) ) 

#define IShowStats_get_ExcludeY(This,pVal)	\
    ( (This)->lpVtbl -> get_ExcludeY(This,pVal) ) 

#define IShowStats_put_ExcludeY(This,newVal)	\
    ( (This)->lpVtbl -> put_ExcludeY(This,newVal) ) 

#define IShowStats_get_ExcludeValY(This,pVal)	\
    ( (This)->lpVtbl -> get_ExcludeValY(This,pVal) ) 

#define IShowStats_put_ExcludeValY(This,newVal)	\
    ( (This)->lpVtbl -> put_ExcludeValY(This,newVal) ) 

#define IShowStats_get_UseGrouping(This,pVal)	\
    ( (This)->lpVtbl -> get_UseGrouping(This,pVal) ) 

#define IShowStats_put_UseGrouping(This,newVal)	\
    ( (This)->lpVtbl -> put_UseGrouping(This,newVal) ) 

#define IShowStats_get_GroupingCount(This,pVal)	\
    ( (This)->lpVtbl -> get_GroupingCount(This,pVal) ) 

#define IShowStats_put_GroupingCount(This,newVal)	\
    ( (This)->lpVtbl -> put_GroupingCount(This,newVal) ) 

#define IShowStats_get_GroupingItemIndex(This,Idx,pVal)	\
    ( (This)->lpVtbl -> get_GroupingItemIndex(This,Idx,pVal) ) 

#define IShowStats_put_GroupingItemIndex(This,Idx,newVal)	\
    ( (This)->lpVtbl -> put_GroupingItemIndex(This,Idx,newVal) ) 

#define IShowStats_get_GroupingItemType(This,Idx,pVal)	\
    ( (This)->lpVtbl -> get_GroupingItemType(This,Idx,pVal) ) 

#define IShowStats_put_GroupingItemType(This,Idx,newVal)	\
    ( (This)->lpVtbl -> put_GroupingItemType(This,Idx,newVal) ) 

#define IShowStats_get_GroupingItemSize(This,Idx,pVal)	\
    ( (This)->lpVtbl -> get_GroupingItemSize(This,Idx,pVal) ) 

#define IShowStats_put_GroupingItemSize(This,Idx,newVal)	\
    ( (This)->lpVtbl -> put_GroupingItemSize(This,Idx,newVal) ) 

#define IShowStats_get_GroupingItemColor(This,Idx,pVal)	\
    ( (This)->lpVtbl -> get_GroupingItemColor(This,Idx,pVal) ) 

#define IShowStats_put_GroupingItemColor(This,Idx,newVal)	\
    ( (This)->lpVtbl -> put_GroupingItemColor(This,Idx,newVal) ) 

#define IShowStats_get_UseFlagging(This,pVal)	\
    ( (This)->lpVtbl -> get_UseFlagging(This,pVal) ) 

#define IShowStats_put_UseFlagging(This,newVal)	\
    ( (This)->lpVtbl -> put_UseFlagging(This,newVal) ) 

#define IShowStats_get_FlagIndex(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagIndex(This,pVal) ) 

#define IShowStats_put_FlagIndex(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagIndex(This,newVal) ) 

#define IShowStats_get_FlagMin(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagMin(This,pVal) ) 

#define IShowStats_put_FlagMin(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagMin(This,newVal) ) 

#define IShowStats_get_FlagMax(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagMax(This,pVal) ) 

#define IShowStats_put_FlagMax(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagMax(This,newVal) ) 

#define IShowStats_get_FlagType(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagType(This,pVal) ) 

#define IShowStats_put_FlagType(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagType(This,newVal) ) 

#define IShowStats_get_FlagSize(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagSize(This,pVal) ) 

#define IShowStats_put_FlagSize(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagSize(This,newVal) ) 

#define IShowStats_get_FlagColor(This,pVal)	\
    ( (This)->lpVtbl -> get_FlagColor(This,pVal) ) 

#define IShowStats_put_FlagColor(This,newVal)	\
    ( (This)->lpVtbl -> put_FlagColor(This,newVal) ) 

#define IShowStats_get_Grouping(This,pVal)	\
    ( (This)->lpVtbl -> get_Grouping(This,pVal) ) 

#define IShowStats_put_Grouping(This,newVal)	\
    ( (This)->lpVtbl -> put_Grouping(This,newVal) ) 

#define IShowStats_get_UseSpecificStrokes(This,pVal)	\
    ( (This)->lpVtbl -> get_UseSpecificStrokes(This,pVal) ) 

#define IShowStats_put_UseSpecificStrokes(This,newVal)	\
    ( (This)->lpVtbl -> put_UseSpecificStrokes(This,newVal) ) 

#define IShowStats_get_XStroke(This,pVal)	\
    ( (This)->lpVtbl -> get_XStroke(This,pVal) ) 

#define IShowStats_put_XStroke(This,newVal)	\
    ( (This)->lpVtbl -> put_XStroke(This,newVal) ) 

#define IShowStats_get_YStroke(This,pVal)	\
    ( (This)->lpVtbl -> get_YStroke(This,pVal) ) 

#define IShowStats_put_YStroke(This,newVal)	\
    ( (This)->lpVtbl -> put_YStroke(This,newVal) ) 

#define IShowStats_ShowCombo(This,GraphWindow,ShowScat,StatFile,ScatFile,Experiment,Grouping,Title,SubTitle,XAxisLabel,YAxisLabel,XAxisColumn,YAxisColumn,StatColumnX,StatColumnY,StdDevColumn,PEHandle)	\
    ( (This)->lpVtbl -> ShowCombo(This,GraphWindow,ShowScat,StatFile,ScatFile,Experiment,Grouping,Title,SubTitle,XAxisLabel,YAxisLabel,XAxisColumn,YAxisColumn,StatColumnX,StatColumnY,StdDevColumn,PEHandle) ) 

#define IShowStats_get_SubsetColumn(This,pVal)	\
    ( (This)->lpVtbl -> get_SubsetColumn(This,pVal) ) 

#define IShowStats_put_SubsetColumn(This,newVal)	\
    ( (This)->lpVtbl -> put_SubsetColumn(This,newVal) ) 

#define IShowStats_get_SubsetName(This,pVal)	\
    ( (This)->lpVtbl -> get_SubsetName(This,pVal) ) 

#define IShowStats_put_SubsetName(This,newVal)	\
    ( (This)->lpVtbl -> put_SubsetName(This,newVal) ) 

#define IShowStats_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IShowStats_get_StdDevN(This,pVal)	\
    ( (This)->lpVtbl -> get_StdDevN(This,pVal) ) 

#define IShowStats_put_StdDevN(This,newVal)	\
    ( (This)->lpVtbl -> put_StdDevN(This,newVal) ) 

#define IShowStats_ShowClear(This,GraphWindow,PEHandle)	\
    ( (This)->lpVtbl -> ShowClear(This,GraphWindow,PEHandle) ) 

#define IShowStats_get_SubMovementFilter(This,pVal)	\
    ( (This)->lpVtbl -> get_SubMovementFilter(This,pVal) ) 

#define IShowStats_put_SubMovementFilter(This,newVal)	\
    ( (This)->lpVtbl -> put_SubMovementFilter(This,newVal) ) 

#define IShowStats_PrintGraph(This,PEHandle)	\
    ( (This)->lpVtbl -> PrintGraph(This,PEHandle) ) 

#define IShowStats_SetMissingDataValue(This,MDVal)	\
    ( (This)->lpVtbl -> SetMissingDataValue(This,MDVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IShowStats_INTERFACE_DEFINED__ */



#ifndef __SHOWMODLib_LIBRARY_DEFINED__
#define __SHOWMODLib_LIBRARY_DEFINED__

/* library SHOWMODLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_SHOWMODLib;

EXTERN_C const CLSID CLSID_ShowHWR;

#ifdef __cplusplus

class DECLSPEC_UUID("E8B28DA1-96E8-11D3-8A27-00104BC7E2C8")
ShowHWR;
#endif

EXTERN_C const CLSID CLSID_ShowTF;

#ifdef __cplusplus

class DECLSPEC_UUID("E8B28DA4-96E8-11D3-8A27-00104BC7E2C8")
ShowTF;
#endif

EXTERN_C const CLSID CLSID_ShowStats;

#ifdef __cplusplus

class DECLSPEC_UUID("B439BE55-3B12-11D4-8B7F-00104BC7E2C8")
ShowStats;
#endif
#endif /* __SHOWMODLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



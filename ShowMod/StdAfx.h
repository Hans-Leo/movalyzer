// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#pragma once

#define _BIND_TO_CURRENT_CRT_VERSION	1
#define _BIND_TO_CURRENT_MFC_VERSION	1

#define STRICT
#define _ATL_APARTMENT_THREADED

#include <afxwin.h>
#include <afxdisp.h>

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>

void SetMissingDataValue( double dVal );
double GetMissingDataValue();

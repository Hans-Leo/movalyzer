// ShowStats.cpp : Implementation of CShowModApp and DLL registration.

#include "stdafx.h"
#include "ShowMod.h"
#include "ShowStats.h"
#include "PEGrpApi.h"
#include "PEMessag.h"
#include "..\common\deffun.h"
#include "..\NSShared\iolib.h"
#include "..\NSShared\hlib.h"

#include <math.h>

#define	NULLDATAVALUE		-9999.0F
#define NUMPOINTSMAX		300

/////////////////////////////////////////////////////////////////////////////
//

ShowStats::ShowStats() :
	x( NULL ), y( NULL ), lvl( NULL ), lvl2( NULL ), flags( NULL ), m_pTypes( NULL ),
	m_pSizes( NULL ), m_pColors( NULL ), m_pItems( NULL ), subset( NULL ), stddev( NULL ),
	sx( NULL ), sy( NULL ), slvl( NULL ), slvl2( NULL ), ssubset( NULL )
{
	m_nX = 0;
	m_nY = 0;
	m_nStdDev = -1;
	m_nCnt = 0;

	m_nSubCol = 0;
	m_nGrp = 0;
	m_nSubj = 0;
	m_nCond = 0;
	m_nTrial = 0;
	m_nStroke = 0;
	m_nSubMvmt = 0;

	m_fDispX = FALSE;
	m_nDispX = 0;
	m_dStrengthX = 0.0;
	m_fDispY = FALSE;
	m_nDispY = 0;
	m_dStrengthY = 0.0;

	m_fGrouping = FALSE;
	m_nGrouping = 0;
	m_nGroupingCount = 0;

	m_fFlag = FALSE;
	m_nFlagIdx = 0;
	m_dFlagMin = 0.0;
	m_dFlagMax = 0.0;
	m_nFlagType = 0;
	m_nFlagSize = 0;
	m_nFlagColor = 0L;

	m_fExcludeX = FALSE;
	m_dExcludeValX = 0.0;
	m_fExcludeY = FALSE;
	m_dExcludeValY = 0.0;

	m_fUseStrokes = FALSE;
	m_nXStroke = 0;
	m_nYStroke = 0;

	m_dXMin = 0.0;
	m_dXMax = 0.0;
	m_dYMin = 0.0;
	m_dYMax = 0.0;
	m_dsXMin = 0.0;
	m_dsXMax = 0.0;
	m_dsYMin = 0.0;
	m_dsYMax = 0.0;

	m_fStdDevN = FALSE;

	// FORMERLY STATIC/GLOBAL VARS
	_cnt = 0;
}

ShowStats::~ShowStats()
{
	try
	{
		if( x ) delete [] x;
		if( y ) delete [] y;
		if( stddev ) delete [] stddev;
		if( _cnt > 1 )
		{
			if( sx ) delete [] sx;
			if( sy ) delete [] sy;
			if( lvl ) delete [] lvl;
			if( lvl2 ) delete [] lvl2;
			if( subset ) delete [] subset;
			if( slvl ) delete [] slvl;
			if( slvl2 ) delete [] slvl2;
			if( ssubset ) delete [] ssubset;
			if( flags ) delete [] flags;
		}
		else
		{
			if( sx ) delete sx;
			if( sy ) delete sy;
			if( lvl ) delete lvl;
			if( lvl2 ) delete lvl2;
			if( subset ) delete subset;
			if( slvl ) delete slvl;
			if( slvl2 ) delete slvl2;
			if( ssubset ) delete ssubset;
			if( flags ) delete flags;
		}
		if( m_pItems ) delete [] m_pItems;
		if( m_pTypes ) delete [] m_pTypes;
		if( m_pSizes ) delete [] m_pSizes;
		if( m_pColors ) delete [] m_pColors;
	}
	catch(...)
	{
		// Do nothing for now
		TRACE( _T("Delete error on an array in ShowStats desctructor.") );
	}
}

STDMETHODIMP ShowStats::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IShowStats,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

BOOL ShowStats::Init( int& numX, int& numY )
{
	if( m_szFile.IsEmpty() ) return FALSE;

	CStdioFile f;
	if( !f.Open( m_szFile, CFile::modeRead ) ) return FALSE;

	CString szTemp;
	// Eliminate first line
	if( !f.ReadString( szTemp ) ) return FALSE;
	// get the count of items that are in the data file
	double dTemp[ NUMPOINTSMAX ];	// Assuming no more than NUMPOINTSMAX data points
	memset( dTemp, 0, NUMPOINTSMAX * sizeof( double ) );
	int nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	if( nCount == 0 )
	{
		::OutputMessage( _T("SHOWMOD - ERROR: No data in the input file."), LOG_GRAPH );
		return FALSE;
	}

	// verify that the # of colums of data is >= columns of data we are to display
	if( ( m_nX >= nCount ) || ( m_nY >= nCount ) )
	{
		CString szMsg;
		szMsg.Format( _T("SHOWMOD - ERROR: One or both of the columns of data requested (x=%d,y=%d) exceeds the maximum # of data points (%d)."), m_nX, m_nY, nCount );
		::OutputMessage( szMsg, LOG_GRAPH );
		::OutputMessage( _T("Please contact NeuroScript to inform of this specific error."), LOG_GRAPH );
		return FALSE;
	}

	// Get N for SD/sqrt(N) if specified (col = nCount/2 - 1)
	double dN = dTemp[ ( nCount / 2 ) - 1 ];
	if( dN < 0.) dN = 0.0;	// Negative #s generate errors?

	try
	{
		if( x ) delete [] x;
		if( y ) delete [] y;
		if( stddev ) delete [] stddev;
		if( lvl ) delete [] lvl;	// "lvl" in this case is grp, subj, cond, etc. for x-disp
		if( lvl2 ) delete [] lvl2;	// "lvl2" in this case is grp, subj, cond, etc. for y-disp
		if( subset ) delete [] subset;

		x = NULL;
		y = NULL;
		stddev = NULL;
		lvl = NULL;
		lvl2 = NULL;
		subset = NULL;
	}
	catch(...)
	{
		// Do nothing for now
		TRACE( _T("Delete error on an array in ShowStats Init.") );
	}

	// Need number of x/y points
	lstX.RemoveAll();
	int nY = 0;
	BOOL fInclude;
	m_nCnt = 0;
	while( nCount != 0 )
	{
		fInclude = TRUE;

// 20 Apr 2004 - GMB : The filters are handled in agraphdlg while generating tmp file
//		if( ( m_nGrp > 0 ) || ( m_nSubj > 0 ) || ( m_nCond > 0 ) ||
//			( m_nTrial > 0 ) || ( m_nStroke > 0 ) || ( m_nSubMvmt > 0 ) ||
//			m_fExcludeX || m_fExcludeY )
		if( m_fExcludeX || m_fExcludeY )
		{
//			if( ( m_nGrp > 0 ) && ( dTemp[ 0 ] == m_nGrp ) ) fInclude = TRUE;
//			else if( ( m_nSubj > 0 ) && ( dTemp[ 1 ] == m_nSubj ) ) fInclude = TRUE;
//			else if( ( m_nCond > 0 ) && ( dTemp[ 2 ] == m_nCond ) ) fInclude = TRUE;
//			else if( ( m_nTrial > 0 ) && ( dTemp[ 3 ] == m_nTrial ) ) fInclude = TRUE;
//			else if( ( m_nStroke > 0 ) && ( dTemp[ 4 ] == m_nStroke ) ) fInclude = TRUE;
//			else if( ( m_nSubMvmt > 0 ) && ( dTemp[ 5 ] == m_nSubMvmt ) ) fInclude = TRUE;
			if( m_fExcludeX && ( dTemp[ m_nX - 1 ] != m_dExcludeValX ) ) fInclude = TRUE;
			else if( m_fExcludeY && ( dTemp[ m_nY - 1 ] != m_dExcludeValY ) ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( m_fUseStrokes && fInclude )
		{
			if( dTemp[ 4 ] == m_nXStroke ) fInclude = TRUE;
			else if( dTemp[ 4 ] == m_nYStroke ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( dTemp[ 0 ] == 0.0 ) fInclude = FALSE;

		if( fInclude )
		{
			nY++;

			if( ( m_nSubCol > 0 ) && ( dTemp[ m_nSubCol - 1 ] != 0.0 ) )
			{
				szTemp.Format( _T("%g"), dTemp[ m_nSubCol - 1 ] );
				if( lstX.Find( szTemp ) == NULL )
				{
					lstX.AddTail( szTemp );
					m_nCnt++;
				}
			}
		}

		nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	}
	if( m_nSubCol <= 0 ) m_nCnt = 1;
	if( ( m_nCnt == 0 ) || ( nY == 0 ) ) return FALSE;

	// Data structure
	if( m_fUseStrokes && ( m_nXStroke != m_nYStroke ) ) nY /= 2;
	x = new double[ nY ];
	y = new double[ nY ];
	if( m_fDispX ) lvl = new int[ nY ];
	if( m_fDispY ) lvl2 = new int[ nY ];
	if( m_nStdDev > 0 ) stddev = new double[ nY ];

	double* xmap = NULL;
	POSITION pos = NULL;
	int nIdx = 0;
	if( m_nCnt > 1 )
	{
		subset = new int[ nY ];
		for( nCount = 0; nCount < nY; nCount++ )
		{
			subset[ nCount ] = 0;
		}

		// Index mapping
		xmap = new double[ m_nCnt ];
		for( nCount = 0; nCount < m_nCnt; nCount++ )
		{
			xmap[ nCount ] = 0;
		}

		pos = lstX.GetHeadPosition();
		nIdx = 0;
		while( pos )
		{
			szTemp = lstX.GetNext( pos );
			xmap[ nIdx++ ] = atof( szTemp );
		}
	}

	// Now reloop through file and get X/Y values
	lstSubsets.RemoveAll();
	CString szTmp;
	f.SeekToBegin();
	f.ReadString( szTemp );	// Eliminate first line
	nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	int nPos = 0;
	BOOL fOneStroke = FALSE;
	while( nCount != 0 )
	{
		fInclude = TRUE;

// 20 Apr 2004 - GMB : The filters are handled in agraphdlg while generating tmp file
//		if( ( m_nGrp > 0 ) || ( m_nSubj > 0 ) || ( m_nCond > 0 ) ||
//			( m_nTrial > 0 ) || ( m_nStroke > 0 ) || ( m_nSubMvmt > 0 ) ||
//			m_fExcludeX || m_fExcludeY )
		if( m_fExcludeX || m_fExcludeY )
		{
//			if( ( m_nGrp > 0 ) && ( dTemp[ 0 ] == m_nGrp ) ) fInclude = TRUE;
//			else if( ( m_nSubj > 0 ) && ( dTemp[ 1 ] == m_nSubj ) ) fInclude = TRUE;
//			else if( ( m_nCond > 0 ) && ( dTemp[ 2 ] == m_nCond ) ) fInclude = TRUE;
//			else if( ( m_nTrial > 0 ) && ( dTemp[ 3 ] == m_nTrial ) ) fInclude = TRUE;
//			else if( ( m_nStroke > 0 ) && ( dTemp[ 4 ] == m_nStroke ) ) fInclude = TRUE;
//			else if( ( m_nSubMvmt > 0 ) && ( dTemp[ 5 ] == m_nSubMvmt ) ) fInclude = TRUE;
			if( m_fExcludeX && ( dTemp[ m_nX - 1 ] != m_dExcludeValX ) ) fInclude = TRUE;
			else if( m_fExcludeY && ( dTemp[ m_nY - 1 ] != m_dExcludeValY ) ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( m_fUseStrokes && fInclude )
		{
			if( dTemp[ 4 ] == m_nXStroke ) fInclude = TRUE;
			else if( dTemp[ 4 ] == m_nYStroke ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( dTemp[ 0 ] == 0.0 ) fInclude = FALSE;

		if( fInclude && ( nPos < nY ) )
		{
			// subset labels (if applicable)
			if( m_nSubCol > 0 )
			{
				szTmp.Format( _T("%d"), (int)dTemp[ m_nSubCol - 1 ] );
				if( !lstSubsets.Find( szTmp ) ) lstSubsets.AddTail( szTmp );
			}

			// X & Y
			if( !m_fUseStrokes || ( m_nXStroke == m_nYStroke ) )
			{
				x[ nPos ] = dTemp[ m_nX - 1 ];
				y[ nPos ] = dTemp[ m_nY - 1 ];
				// Std. Dev
				if( m_nStdDev > 0 )
				{
					if( m_fStdDevN && ( dN == 0.0 ) )
						stddev[ nPos ] = 0.0;
					else if( m_fStdDevN )
						stddev[ nPos ] = dTemp[ m_nStdDev - 1 ] / sqrt( dN );
					else
						stddev[ nPos ] = dTemp[ m_nStdDev - 1 ];
				}
				if( nPos == 0 )
				{
					m_dXMin = m_dXMax = x[ nPos ];
					m_dYMin = m_dYMax = y[ nPos ];
				}
				else
				{
					m_dXMin = MIN( x[ nPos ], m_dXMin );
					m_dXMax = MAX( x[ nPos ], m_dXMax );
					if( m_nStdDev > 0 )
					{
						m_dYMin = MIN( y[ nPos ] - stddev[ nPos ], m_dYMin );
						m_dYMax = MAX( y[ nPos ] + stddev[ nPos ], m_dYMax );
					}
					else
					{
						m_dYMin = MIN( y[ nPos ], m_dYMin );
						m_dYMax = MAX( y[ nPos ], m_dYMax );
					}
				}
			}
			else
			{
				if( ( dTemp[ 4 ] == m_nXStroke ) && ( nPos < nY ) )
				{
					x[ nPos ] = dTemp[ m_nX - 1 ];
					// Std. Dev
					if( m_nStdDev > 0 ) stddev[ nPos ] = dTemp[ m_nStdDev - 1 ];
					if( nPos == 0 )
					{
						m_dXMin = x[ nPos ];
						m_dXMax = x[ nPos ];
					}
					else
					{
						m_dXMin = MIN( x[ nPos ], m_dXMin );
						m_dXMax = MAX( x[ nPos ], m_dXMax );
					}
					if( fOneStroke )
					{
						nPos++;
						fOneStroke = FALSE;
					}
					else fOneStroke = TRUE;
				}
				if( ( dTemp[ 4 ] == m_nYStroke ) && ( nPos < nY ) )
				{
					y[ nPos ] = dTemp[ m_nY - 1 ];
					// Std. Dev
					if( m_nStdDev > 0 ) stddev[ nPos ] = dTemp[ m_nStdDev - 1 ];
					if( nPos == 0 )
					{
						m_dYMin = y[ nPos ];
						m_dYMax = y[ nPos ];
					}
					else
					{
						m_dYMin = MIN( y[ nPos ], m_dYMin );
						m_dYMax = MAX( y[ nPos ], m_dYMax );
					}
					if( fOneStroke )
					{
						nPos++;
						fOneStroke = FALSE;
					}
					else fOneStroke = TRUE;
				}
			}
			if( nPos < nY )
			{
				// Dispersion
				if( m_fDispX ) lvl[ nPos ] = (int)dTemp[ m_nDispX - 1 ];
				if( m_fDispY ) lvl2[ nPos ] = (int)dTemp[ m_nDispY - 1 ];
				// Level
				if( m_nCnt > 1 )
				{
					for( nIdx = 0; nIdx < m_nCnt; nIdx++ )
					{
						if( dTemp[ m_nSubCol - 1 ] == xmap[ nIdx ] )
						{
							subset[ nPos ] = nIdx;
							break;
						}
					}
				}
			}

			if( !m_fUseStrokes || ( m_nXStroke == m_nYStroke ) ) nPos++;
		}

		nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	}

	numX = m_nCnt;
	numY = nY;
	try
	{
		if( xmap ) delete [] xmap;
	}
	catch(...)
	{
		// Do nothing for now
		TRACE( _T("Delete error on an array in ShowStats Init.") );
	}

	return TRUE;
}

BOOL ShowStats::InitScatter()
{
	if( m_szFile.IsEmpty() ) return FALSE;

	CStdioFile f;
	if( !f.Open( m_szFile, CFile::modeRead ) ) return FALSE;

	CString szTemp;

	double dTemp[ NUMPOINTSMAX ];	// Assuming no more than NUMPOINTSMAX data points
	memset( dTemp, 0, NUMPOINTSMAX * sizeof( double ) );
	int nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	// 30Sep03 - GMB: we added headers to tmp file...so we'll try again if 0
//	if( nCount == 0 ) return FALSE;
	if( nCount == 0 )
	{
		nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
		if( nCount == 0 ) return FALSE;
	}

	try
	{
		if( _cnt > 1 )
		{
			if( sx ) delete [] sx;
			if( sy ) delete [] sy;
			if( slvl ) delete [] slvl;		// "lvl" in this case is grp, subj, cond, etc. for x-disp
			if( slvl2 ) delete [] slvl2;	// "lvl2" in this case is grp, subj, cond, etc. for y-disp
			if( ssubset ) delete [] ssubset;
			if( flags ) delete [] flags;
		}
		else
		{
			if( sx ) delete sx;
			if( sy ) delete sy;
			if( slvl ) delete slvl;		// "lvl" in this case is grp, subj, cond, etc. for x-disp
			if( slvl2 ) delete slvl2;	// "lvl2" in this case is grp, subj, cond, etc. for y-disp
			if( ssubset ) delete ssubset;
			if( flags ) delete flags;
		}

		sx = NULL;
		sy = NULL;
		slvl = NULL;
		slvl2 = NULL;
		ssubset = NULL;
		flags = NULL;
	}
	catch(...)
	{
		// Do nothing for now
		TRACE( _T("Delete error on an array in ShowStats InitScatter.") );
	}

	// Need number of x/y points
	m_nCnt = 0;
	BOOL fInclude;
	while( nCount != 0 )
	{
		fInclude = TRUE;
// 20 Apr 2004 - GMB : The filters are handled in agraphdlg while generating tmp file
//		if( ( m_nGrp > 0 ) || ( m_nSubj > 0 ) || ( m_nCond > 0 ) ||
//			( m_nTrial > 0 ) || ( m_nStroke > 0 ) || ( m_nSubMvmt > 0 ) ||
//			m_fExcludeX || m_fExcludeY )
		if( m_fExcludeX || m_fExcludeY )
		{
//			if( ( m_nGrp > 0 ) && ( dTemp[ 0 ] == m_nGrp ) ) fInclude = TRUE;
//			else if( ( m_nSubj > 0 ) && ( dTemp[ 1 ] == m_nSubj ) ) fInclude = TRUE;
//			else if( ( m_nCond > 0 ) && ( dTemp[ 2 ] == m_nCond ) ) fInclude = TRUE;
//			else if( ( m_nTrial > 0 ) && ( dTemp[ 3 ] == m_nTrial ) ) fInclude = TRUE;
//			else if( ( m_nStroke > 0 ) && ( dTemp[ 4 ] == m_nStroke ) ) fInclude = TRUE;
//			else if( ( m_nSubMvmt > 0 ) && ( dTemp[ 5 ] == m_nSubMvmt ) ) fInclude = TRUE;
			if( m_fExcludeX && ( dTemp[ m_nX - 1 ] != m_dExcludeValX ) ) fInclude = TRUE;
			else if( m_fExcludeY && ( dTemp[ m_nY - 1 ] != m_dExcludeValY ) ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( m_fUseStrokes && fInclude )
		{
			if( dTemp[ 4 ] == m_nXStroke ) fInclude = TRUE;
			else if( dTemp[ 4 ] == m_nYStroke ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( dTemp[ 0 ] == 0.0 ) fInclude = FALSE;

		if( fInclude ) m_nCnt++;

		nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	}

	// Data structure
	if( m_fUseStrokes && ( m_nXStroke != m_nYStroke ) ) m_nCnt /= 2;
	sx = new double[ m_nCnt ];
	sy = new double[ m_nCnt ];
	if( m_fDispX ) slvl = new int[ m_nCnt ];
	if( m_fDispY ) slvl2 = new int[ m_nCnt ];
	if( m_fGrouping ) ssubset = new int[ m_nCnt ];
	if( m_fFlag ) flags = new BOOL[ m_nCnt ];
	_cnt = m_nCnt;

	// Now reloop through file and get X/Y values
	f.SeekToBegin();
	nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	// 30Sep03 - GMB: we added headers to tmp file...so we'll try again if 0
	if( nCount == 0 ) nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	int nPos = 0;
	BOOL fOneStroke = FALSE;
	while( nCount != 0 )
	{
		fInclude = TRUE;
		
// 20 Apr 2004 - GMB : The filters are handled in agraphdlg while generating tmp file
//		if( ( m_nGrp > 0 ) || ( m_nSubj > 0 ) || ( m_nCond > 0 ) ||
//			( m_nTrial > 0 ) || ( m_nStroke > 0 ) || ( m_nSubMvmt > 0 ) ||
//			m_fExcludeX || m_fExcludeY )
		if( m_fExcludeX || m_fExcludeY )
		{
//			if( ( m_nGrp > 0 ) && ( dTemp[ 0 ] == m_nGrp ) ) fInclude = TRUE;
//			else if( ( m_nSubj > 0 ) && ( dTemp[ 1 ] == m_nSubj ) ) fInclude = TRUE;
//			else if( ( m_nCond > 0 ) && ( dTemp[ 2 ] == m_nCond ) ) fInclude = TRUE;
//			else if( ( m_nTrial > 0 ) && ( dTemp[ 3 ] == m_nTrial ) ) fInclude = TRUE;
//			else if( ( m_nStroke > 0 ) && ( dTemp[ 4 ] == m_nStroke ) ) fInclude = TRUE;
//			else if( ( m_nSubMvmt > 0 ) && ( dTemp[ 5 ] == m_nSubMvmt ) ) fInclude = TRUE;
			if( m_fExcludeX && ( dTemp[ m_nX - 1 ] != m_dExcludeValX ) ) fInclude = TRUE;
			else if( m_fExcludeY && ( dTemp[ m_nY - 1 ] != m_dExcludeValY ) ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( m_fUseStrokes && fInclude )
		{
			if( dTemp[ 4 ] == m_nXStroke ) fInclude = TRUE;
			else if( dTemp[ 4 ] == m_nYStroke ) fInclude = TRUE;
			else fInclude = FALSE;
		}
		if( dTemp[ 0 ] == 0.0 ) fInclude = FALSE;

		if( fInclude )
		{
			// X & Y
			if( !m_fUseStrokes || ( m_nXStroke == m_nYStroke ) )
			{
				sx[ nPos ] = dTemp[ m_nX - 1 ];
				sy[ nPos ] = dTemp[ m_nY - 1 ];

				if( nPos == 0 )
				{
					m_dsXMin = m_dsXMax = sx[ nPos ];
					m_dsYMin = m_dsYMax = sy[ nPos ];
				}
				else
				{
					m_dsXMin = MIN( sx[ nPos ], m_dsXMin );
					m_dsXMax = MAX( sx[ nPos ], m_dsXMax );
					m_dsYMin = MIN( sy[ nPos ], m_dsYMin );
					m_dsYMax = MAX( sy[ nPos ], m_dsYMax );
				}
			}
			else
			{
				if( dTemp[ 4 ] == m_nXStroke )
				{
					sx[ nPos ] = dTemp[ m_nX - 1 ];
					if( nPos == 0 )
					{
						m_dsXMin = sx[ nPos ];
						m_dsXMax = sx[ nPos ];
					}
					else
					{
						m_dsXMin = MIN( sx[ nPos ], m_dsXMin );
						m_dsXMax = MAX( sx[ nPos ], m_dsXMax );
					}
					if( fOneStroke )
					{
						nPos++;
						fOneStroke = FALSE;
					}
					else fOneStroke = TRUE;
				}
				if( dTemp[ 4 ] == m_nYStroke )
				{
					sy[ nPos ] = dTemp[ m_nY - 1 ];
					if( nPos == 0 )
					{
						m_dsYMin = sy[ nPos ];
						m_dsYMax = sy[ nPos ];
					}
					else
					{
						m_dsYMin = MIN( sy[ nPos ], m_dsYMin );
						m_dsYMax = MAX( sy[ nPos ], m_dsYMax );
					}
					if( fOneStroke )
					{
						nPos++;
						fOneStroke = FALSE;
					}
					else fOneStroke = TRUE;
				}
			}
			if( nPos < m_nCnt )
			{
				if( m_fDispX ) slvl[ nPos ] = (int)dTemp[ m_nDispX - 1 ];
				if( m_fDispY ) slvl2[ nPos ] = (int)dTemp[ m_nDispY - 1 ];
				if( m_fGrouping ) ssubset[ nPos ] = (int)dTemp[ m_nGrouping - 1 ];
				if( m_fFlag )
				{
					if( ( dTemp[ m_nFlagIdx - 1 ] >= m_dFlagMin ) &&
						( dTemp[ m_nFlagIdx - 1 ] <= m_dFlagMax ) )
						flags[ nPos ] = TRUE;
					else flags[ nPos ] = FALSE;
				}
			}

			if( !m_fUseStrokes || ( m_nXStroke == m_nYStroke ) ) nPos++;
		}

		nCount = getlinefloat( f, dTemp, NUMPOINTSMAX );
	}

	return TRUE;
}

STDMETHODIMP ShowStats::PrintGraph(VARIANT* PEHandle)
{
	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;

	BOOL fRet = PElaunchprintdialog( *hPE, TRUE, NULL );

	return S_OK;
}

STDMETHODIMP ShowStats::ShowScatter  (VARIANT* GraphWindow, BSTR InFile,
									  BSTR Experiment, BSTR Grouping,
									  BSTR XAxisLabel, BSTR YAxisLabel,
									  short XAxisColumn, short YAxisColumn, VARIANT* PEHandle )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	m_szFile = InFile;
	m_szExp = Experiment;
	m_szGrp = Grouping;
	m_nX = XAxisColumn;
	m_nY = YAxisColumn;
	m_szX = XAxisLabel;
	m_szY = YAxisLabel;

	HWND* hPE = (HWND*)PEHandle;
	InitScatter();

	CWnd* pGraph = (CWnd*)GraphWindow;
	RECT rect;
	pGraph->GetClientRect( &rect );

	if( *hPE ) PEdestroy( *hPE );
	*hPE = PEcreate( PECONTROL_SGRAPH, WS_VISIBLE, &rect, pGraph->m_hWnd, 1001 );
	if( !*hPE ) return E_FAIL;

	PEreset( pGraph->m_hWnd );

	// Set number of subsets and points
	int nSubsets = 1;
	if( m_pItems && m_pTypes && m_pSizes && m_pColors && m_fGrouping )
		nSubsets = m_nGroupingCount;
	if( m_fFlag ) nSubsets++;
	PEnset( *hPE, PEP_nSUBSETS, nSubsets );
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );
	double dNull = NULLDATAVALUE;
	PEvset(*hPE, PEP_fNULLDATAVALUE, &dNull, 1);

	for( int i = 0; i < m_nCnt; i++ )
	{
		_sd.nSubset = 0;

		_sd.fX = (float)sx[ i ];
		if( sx[ i ] == ::GetMissingDataValue() ) _sd.fX = NULLDATAVALUE;
		_sd.fY = (float)sy[ i ];
		if( sy[ i ] == ::GetMissingDataValue() ) _sd.fY = NULLDATAVALUE;

		if( m_fGrouping && m_pItems && ssubset )
		{
			for( int j = 0; j < m_nGroupingCount; j++ )
			{
				if( ssubset[ i ] == m_pItems[ j ] )
				{
					_sd.nSubset = j;
					break;
				}
			}
		}
		if( m_fFlag && flags && flags[ i ] )
		{
			_sd.nSubset = nSubsets - 1;
		}

		if( m_fDispX && slvl )
		{
			_sd.fX += (float)( slvl[ i ] * m_dStrengthX );
			m_dsXMin = MIN( m_dsXMin, _sd.fX );
			m_dsXMax = MAX( m_dsXMax, _sd.fX );
		}
		if( m_fDispY && slvl2 )
		{
			_sd.fY += (float)( slvl2[ i ] * m_dStrengthY );
			m_dsYMin = MIN( m_dsYMin, _sd.fY );
			m_dsYMax = MAX( m_dsYMax, _sd.fY );
		}

		PEvsetcellEx( *hPE, PEP_faXDATA, _sd.nSubset, i, &_sd.fX );
		PEvsetcellEx( *hPE, PEP_faYDATA, _sd.nSubset, i, &_sd.fY );
	}

	// Set colors
//	PEnset(*hPE, PEP_dwDESKCOLOR, RGB(192, 192, 192));
//	PEnset(*hPE, PEP_dwGRAPHBACKCOLOR, RGB(255, 255, 255));
	PEnset(*hPE, PEP_dwGRAPHFORECOLOR, RGB(0, 0, 0));
	PEnset(*hPE, PEP_dwSHADOWCOLOR, RGB(255, 255, 255));
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, RGB(255, 255, 255));

	// Set DataShadows to show 3D 
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	// Summarization of data
	double r = correl( sx, sy, m_nCnt, 0 );
	double tcorr = 0.0, tcorrdf = m_nCnt - 3.0;
	if( ( r != 0.0 ) && ( r != 1.0 ) ) tcorr = r * sqrt( tcorrdf / ( 1.0 - SQR( r ) ) );
	double a = 0.0, b = 0.0, straighterr = 0.0, rr = 0.0;
	CString szOp = _T("+");
	straighterr = rregr( sx, sy, m_nCnt, 1, &a, &b, &rr );
	if( b < 0 ) { b = fabs( b ); szOp = _T("-"); }
	// Labels
	if( m_szGrp != _T("NONE") )
		sprintf_s( _sd.label, _T("%s, %s vs %s per %s"), m_szExp, m_szX, m_szY, m_szGrp );
	else
		sprintf_s( _sd.label, _T("%s, %s vs %s"), m_szExp, m_szX, m_szY );
//	sprintf_s( _sd.label, _T("%s vs %s"), m_szX, m_szY );
	// 08Oct03 - No main title now
	PEszset(*hPE, PEP_szMAINTITLE, _T(""));
// 	sprintf_s( _sd.label, _T("r=%.2g/t(%g)=%.3g/a=%.2lg/b=%.2lg/s=%.2lg"),
// 			 r, tcorrdf, tcorr, a, b, straighterr );
	sprintf_s( _sd.label, _T("r=%.2g  t(%g)=%.3g  y=%.2lg%s%.2lg*x  StraightnessErr=%.2lg"),
			   r, tcorrdf, tcorr, a, szOp, b, straighterr );
	PEszset(*hPE, PEP_szSUBTITLE, _sd.label);
	strcpy_s( _sd.label, 75, m_szX );
	PEszset(*hPE, PEP_szXAXISLABEL, _sd.label );
	strcpy_s( _sd.label, 75, m_szY );
	PEszset(*hPE, PEP_szYAXISLABEL, _sd.label );
	PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
	PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szLABELFONT, "Arial");
	PEszset(*hPE, PEP_szTABLEFONT, "Arial");

	// fixed font for scaling
	PEnset(*hPE, PEP_bFIXEDFONTS, TRUE);

	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_bPREPAREIMAGES, TRUE);
	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_POINT);
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);
		
	CString szTemp;
	if( m_fGrouping )
	{
		for( int i = 0; i < m_nCnt; i++ )
		{
			szTemp.Format( _T("%s %d"), m_szSubset, i + 1 );
			strcpy_s( _sd.label, 75, szTemp );
			PEvsetcell( *hPE, PEP_szaSUBSETLABELS, i, _sd.label );
		}
	}

    //** Disable some types of plotting methods **'
    PEnset(*hPE, PEP_bALLOWSPLINE, FALSE);
    PEnset(*hPE, PEP_bALLOWPOINTSPLUSSPLINE, FALSE);
    PEnset(*hPE, PEP_bALLOWBESTFITLINE, FALSE);
    PEnset(*hPE, PEP_bALLOWBESTFITCURVE, FALSE);
    PEnset(*hPE, PEP_bALLOWAREA, FALSE);

	// color/point type/point size
	if( m_fGrouping && m_pColors )
	{
		DWORD* pColors = new DWORD[ m_nGroupingCount + ( m_fFlag ? 1 : 0 ) ];
		int* pTypes = new int[ m_nGroupingCount + ( m_fFlag ? 1 : 0 ) ];

		for( int k = 0; k < ( m_nGroupingCount + ( m_fFlag ? 1 : 0 ) ); k++ )
		{
			if( k < m_nGroupingCount )
			{
				pColors[ k ] = m_pColors[ k ];
				pTypes[ k ] = m_pTypes[ k ];
			}
			else
			{
				pColors[ k ] = m_nFlagColor;
				pTypes[ k ] = m_nFlagType;
			}
		}
		PEvsetEx( *hPE, PEP_dwaSUBSETCOLORS, 0, m_nGroupingCount + ( m_fFlag ? 1 : 0 ), pColors, 0 );
		PEvset( *hPE, PEP_naSUBSETPOINTTYPES, pTypes, m_nGroupingCount + ( m_fFlag ? 1 : 0 ) );

		delete [] pColors;
		delete [] pTypes;

		// point size
		PEnset( *hPE, PEP_nPOINTSIZE, m_pSizes[ 0 ] );
	}
	else if( m_fFlag )
	{
		DWORD dwColors[ 2 ];
		dwColors[ 0 ] = RGB( 63, 63, 191 );
		dwColors[ 1 ] = m_nFlagColor;
		int nLineTypes[ 2 ];
		nLineTypes[ 0 ] = PEPT_DOTSOLID;
		nLineTypes[ 1 ] = m_nFlagType;

		PEvsetEx( *hPE, PEP_dwaSUBSETCOLORS, 0, 2, dwColors, 0 );
		PEvset( *hPE, PEP_naSUBSETPOINTTYPES, nLineTypes, 2 );

		// point size
		PEnset( *hPE, PEP_nPOINTSIZE, PEPS_MEDIUM );
	}
	else
	{
		DWORD dwColor = RGB( 63,63,191 );
		PEvset( *hPE, PEP_dwaSUBSETCOLORS, &dwColor, 1 );
		int nLineType = PEPT_DOTSOLID;
		PEvset( *hPE, PEP_naSUBSETPOINTTYPES, &nLineType, 1 );
		// point size
		PEnset( *hPE, PEP_nPOINTSIZE, PEPS_MEDIUM );
	}

	// When set to no manual PEreinitialize delivers the min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	PEreinitialize( *hPE );

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

	// Adjust ranges to keep data off edge of graph
	m_dsXMin -= ( ( m_dsXMax - m_dsXMin ) * 0.10 );
	m_dsXMax += ( ( m_dsXMax - m_dsXMin ) * 0.10 );
	m_dsYMin -= ( ( m_dsYMax - m_dsYMin ) * 0.10 );
	m_dsYMax += ( ( m_dsYMax - m_dsYMin ) * 0.10 );

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &m_dsXMin, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &m_dsXMax, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &m_dsYMin, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &m_dsYMax, 1);

    //** Set up cursor **
//    PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);

    //** This will allow you to move cursor by clicking data point **
    PEnset(*hPE, PEP_bMOUSECURSORCONTROL, TRUE);
    PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, TRUE);

    //** Cursor prompting in top left corner **
    PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
    PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);

	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowStats::ShowStatGraph(VARIANT *GraphWindow, BOOL ShowScat,
									  BSTR StatFile, BSTR ScatFile,
									  BSTR Experiment, BSTR Grouping,
									  BSTR Title, BSTR SubTitle, BSTR XAxisLabel,
									  BSTR YAxisLabel, short XAxisColumn, short YAxisColumn,
									  short StatColumnX, short StatColumnY,
									  short StdDevColumn, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	m_szExp = Experiment;
	m_szGrp = Grouping;
	m_nStdDev = StdDevColumn;
	m_szTitle = Title;
	m_szSub = SubTitle;
	m_szX = XAxisLabel;
	m_szY = YAxisLabel;

	m_nX = StatColumnX;
	m_nY = StatColumnY;
	m_szFile = ScatFile;
	InitScatter();
	_sd.nCnt1 = m_nCnt;

	m_nX = XAxisColumn;
	m_nY = YAxisColumn;
	m_szFile = StatFile;
	int numX = 0, numY = 0;
	Init( numX, numY );
	_sd.nCnt2 = m_nCnt;
//	if( ( numX == 0 ) || ( numY == 0 ) || ( m_nCnt == 0 ) ) return S_OK;

	CWnd* pGraph = (CWnd*)GraphWindow;
	HWND* hPE = (HWND*)PEHandle;
	RECT rect;
	pGraph->GetClientRect( &rect );

	if( *hPE ) PEdestroy( *hPE );
	*hPE = PEcreate( PECONTROL_SGRAPH, WS_VISIBLE, &rect, pGraph->m_hWnd, 1001 );
	if( !*hPE ) return E_FAIL;

	PEreset( pGraph->m_hWnd );

	// Set number of subsets and points
	PEnset( *hPE, PEP_nSUBSETS, numX );
	PEnset( *hPE, PEP_nPOINTS, numY );
	double dNull = NULLDATAVALUE;
	PEvset(*hPE, PEP_fNULLDATAVALUE, &dNull, 1);

	float fX = 0.0, fY = 0.0;
	int nLvl = 0, i = 0, j = 0;

	// Plot data points
	int nPos = 0;
	for( i = 0; i < numX; i++ )
	{
		nPos = 0;
		for( j = 0; j < numY; j++ )
		{
			nLvl = ( m_nCnt > 1 ) ? subset[ j ] : 0;
			if( nLvl == i )
			{
				fX = (float)x[ j ];
				if( fX == 0. ) fX = (float)0.00001;
				else if( x[ j ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
				fY = (float)y[ j ];
				if( y[ j ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;

				if( m_fDispX && lvl )
				{
					fX += (float)( lvl[ j ] * m_dStrengthX );
					m_dXMin = MIN( m_dXMin, fX );
					m_dXMax = MAX( m_dXMax, fX );
				}
				if( m_fDispY && lvl2 )
				{
					fY += (float)( lvl2[ j ] * m_dStrengthY );
					m_dYMin = MIN( m_dYMin, fY );
					m_dYMax = MAX( m_dYMax, fY );
				}
			}
			else
			{
				fX = 0.0;
				fY = 0.0;
			}

			PEvsetcellEx( *hPE, PEP_faXDATA, i, nPos, &fX );
			PEvsetcellEx( *hPE, PEP_faYDATA, i, nPos, &fY );

			nPos++;
		}
	}

	// Colors for lines
	DWORD* pColors = new DWORD[ m_nCnt ];
//	if( !m_fGrouping )
	{
		for( i = 0; i < m_nCnt; i++ )
		{
			pColors[ i ] = ::GetColorByIndex( i, m_nCnt );
		}
		PEvsetEx( *hPE, PEP_dwaSUBSETCOLORS, 0, m_nCnt, pColors, 0 );
	}

	// Standard Deviations
	if( ( m_nStdDev > 0 ) && stddev )
	{
		PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
		PEnset(*hPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);
		PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
		PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);
		int nSDPt = 0;
		int nSymbol;
		DWORD dwCol;
		double dX, dY1, dY2;

		for( i = 0; i < numY; i++ )
		{
			dX = x[ i ];
			fY = (float)y[ i ];
			if( !m_fGrouping )
				dwCol = subset ? pColors[ subset[ i ] ] : pColors[ 0 ];
			else if( m_pColors && subset ) dwCol = m_pColors[ subset[ i ] ];
			else dwCol = ::GetColorByIndex( 1, 1 );

			if( m_fDispX && lvl )
				dX += ( lvl[ i ] * m_dStrengthX );

			dY1 = fY + stddev[ i ];
			if( m_fDispY && lvl2 )
				dY1 += ( lvl2[ i ] * m_dStrengthY );
			m_dYMin = MIN( m_dYMin, dY1 );
			m_dYMax = MAX( m_dYMax, dY1 );
//			nSymbol = PEGAT_DOWNTRIANGLESOLID;
			nSymbol = PEGAT_DASH;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &dY1);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &nSymbol);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &dwCol);

			nSDPt++;

			dY2 = fY - stddev[ i ];
			if( m_fDispY && lvl2 )
				dY2 += ( lvl2[ i ] * m_dStrengthY );
			m_dYMin = MIN( m_dYMin, dY2 );
			m_dYMax = MAX( m_dYMax, dY2 );
//			nSymbol = PEGAT_UPTRIANGLESOLID;
			nSymbol = PEGAT_DASH;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &dY2);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &nSymbol);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &dwCol);

			nSDPt++;

			nSymbol = PEGAT_THINSOLIDLINE;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &dY2);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &nSymbol);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &dwCol);

			nSDPt++;

			nSymbol = PEGAT_LINECONTINUE;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &dY1);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &nSymbol);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &dwCol);

			nSDPt++;
		}
	}

	delete [] pColors;

	// Set colors
//	PEnset(*hPE, PEP_dwDESKCOLOR, RGB(192, 192, 192));
//	PEnset(*hPE, PEP_dwGRAPHBACKCOLOR, RGB(255, 255, 255));
	PEnset(*hPE, PEP_dwGRAPHFORECOLOR, RGB(0, 0, 0));
	PEnset(*hPE, PEP_dwSHADOWCOLOR, RGB(255, 255, 255));
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, RGB(255, 255, 255));

	// Set DataShadows to show 3D 
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	if( m_szGrp != _T("NONE") )
		sprintf_s( _sd.label, _T("%s, %s vs %s per %s"), m_szExp, m_szX, m_szY, m_szGrp );
	else
		sprintf_s( _sd.label, _T("%s, %s vs %s"), m_szExp, m_szX, m_szY );
//	strcpy_s( _sd.label, m_szTitle );
	// 08Oct03 - no main title now
	PEszset(*hPE, PEP_szMAINTITLE, _T(""));
	strcpy_s( _sd.label, 75, m_szSub );
	PEszset(*hPE, PEP_szSUBTITLE, _sd.label);
	strcpy_s( _sd.label, 75, m_szX );
	PEszset(*hPE, PEP_szXAXISLABEL, _sd.label );
	strcpy_s( _sd.label, 75, m_szY );
	PEszset(*hPE, PEP_szYAXISLABEL, _sd.label);
	PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
	PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szLABELFONT, "Arial");
	PEszset(*hPE, PEP_szTABLEFONT, "Arial");

	// fixed font for scaling
	PEnset(*hPE, PEP_bFIXEDFONTS, TRUE);

	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_bPREPAREIMAGES, TRUE);
//	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
	if( ( ( numY == 0 ) || ( numX == 0 ) ) || ( numY / numX ) == 1 )
		PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_POINT);
	else
		PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_POINTSPLUSLINE);
	PEnset(*hPE, PEP_nPOINTSIZE, PEPS_LARGE );
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);

	// subset labels
	float ftmp = 0.5F;
	PEvset(*hPE, PEP_fFONTSIZELEGENDCNTL, &ftmp, 1 );

	CString szTemp;
	CString szTmp;
	POSITION pos = lstSubsets.GetHeadPosition();
	i = 0;
	while( pos && ( i < m_nCnt ) )
	{
		szTmp = lstSubsets.GetNext( pos );
		szTemp.Format( _T("%s %s"), m_szSubset, szTmp );
		strcpy_s( _sd.label, 75, szTemp );
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, i++, _sd.label );
	}

	// subset line types
	int nLineTypes[] = { PELT_THINSOLID, PELT_THINSOLID };
	PEvset(*hPE, PEP_naSUBSETLINETYPES, nLineTypes, 2);

	// subset point types
	if( lstSubsets.GetCount() > 0 )
	{
		int* nPointTypes = new int[ lstSubsets.GetCount() ];
		int nPt = 0;
		for( int pt = 0; pt < lstSubsets.GetCount(); pt++ )
		{
			if( nPt >= 12 ) nPt = 0;
			nPointTypes[ pt ] = nPt;
			nPt++;
		}
		PEvset(*hPE, PEP_naSUBSETPOINTTYPES, nPointTypes, lstSubsets.GetCount());
		delete [] nPointTypes;
	}

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

	// Adjust ranges to keep data off edge of graph
	if( ( m_dXMax - m_dXMin ) == 0.0 )
	{
		m_dXMin -= ( m_dXMin * 0.10 );
		m_dXMax += ( m_dXMax * 0.10 );
	}
	else
	{
		m_dXMin -= ( ( m_dXMax - m_dXMin ) * 0.10 );
		m_dXMax += ( ( m_dXMax - m_dXMin ) * 0.10 );
	}
	if( ( m_dYMax - m_dYMin ) == 0.0 )
	{
		m_dYMin -= ( m_dYMin * 0.10 );
		m_dYMax += ( m_dYMax * 0.10 );
	}
	else
	{
		m_dYMin -= ( ( m_dYMax - m_dYMin ) * 0.10 );
		m_dYMax += ( ( m_dYMax - m_dYMin ) * 0.10 );
	}

	if( ShowScat )
	{
		m_dsXMin -= ( ( m_dsXMax - m_dsXMin ) * 0.10 );
		m_dsXMax += ( ( m_dsXMax - m_dsXMin ) * 0.10 );
		m_dsYMin -= ( ( m_dsYMax - m_dsYMin ) * 0.10 );
		m_dsYMax += ( ( m_dsYMax - m_dsYMin ) * 0.10 );

		m_dXMin = MIN( m_dXMin, m_dsXMin );
		m_dXMax = MAX( m_dXMax, m_dsXMax );
		m_dYMin = MIN( m_dYMin, m_dsYMin );
		m_dYMax = MAX( m_dYMax, m_dsYMax );
	}

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &m_dXMin, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &m_dXMax, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &m_dYMin, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &m_dYMax, 1);

    //** Set up cursor **
//    PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);
    
    //** Help see data points **
    //PEnset(*hPE, PEP_bMARKDATAPOINTS, TRUE);
    
    //** This will allow you to move cursor by clicking data point **
    PEnset(*hPE, PEP_bMOUSECURSORCONTROL, TRUE);
    PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, TRUE);
    
    //** Cursor prompting in top left corner **
    PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
    PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);

	// Get rid of Help menu item
// 	PEnset( *hPE, PEP_nHELPMENU, PEMC_HIDE );
// 	// Set all text
// 	char pszTmp[ 128 ];
// 	strcpy( pszTmp, "|" );
// 	PEvsetcell( *hPE, PEP_szaCUSTOMMENUTEXT, 0, pszTmp );
// 	strcpy( pszTmp, "Points" );
// 	PEvsetcell( *hPE, PEP_szaCUSTOMMENUTEXT, 1, pszTmp );
// 	strcpy( pszTmp, "Grouping" );
// 	PEvsetcell( *hPE, PEP_szaCUSTOMMENUTEXT, 2, pszTmp );
// 	// Set all menu items to bottom location
// 	int nCML[] = { PECML_BOTTOM, PECML_BOTTOM, PECML_BOTTOM };
// 	PEvset( *hPE, PEP_naCUSTOMMENULOCATION, nCML, 3 );

	// draw graph
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowStats::get_GroupFilter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nGrp;

	return S_OK;
}

STDMETHODIMP ShowStats::put_GroupFilter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nGrp = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_SubjectFilter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nSubj;

	return S_OK;
}

STDMETHODIMP ShowStats::put_SubjectFilter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nSubj = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_ConditionFilter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nCond;

	return S_OK;
}

STDMETHODIMP ShowStats::put_ConditionFilter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nCond = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_TrialFilter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nTrial;

	return S_OK;
}

STDMETHODIMP ShowStats::put_TrialFilter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nTrial = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_StrokeFilter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nStroke;

	return S_OK;
}

STDMETHODIMP ShowStats::put_StrokeFilter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nStroke = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_SubMovementFilter(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nSubMvmt;

	return S_OK;
}

STDMETHODIMP ShowStats::put_SubMovementFilter(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nSubMvmt = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_XDispersionIndex(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nDispX;

	return S_OK;
}

STDMETHODIMP ShowStats::put_XDispersionIndex(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nDispX = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_XDispersionStrength(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_dStrengthX;

	return S_OK;
}

STDMETHODIMP ShowStats::put_XDispersionStrength(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dStrengthX = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_UseXDispersion(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fDispX;

	return S_OK;
}

STDMETHODIMP ShowStats::put_UseXDispersion(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fDispX = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_YDispersionIndex(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nDispY;

	return S_OK;
}

STDMETHODIMP ShowStats::put_YDispersionIndex(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nDispY = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_YDispersionStrength(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_dStrengthY;

	return S_OK;
}

STDMETHODIMP ShowStats::put_YDispersionStrength(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dStrengthY = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_UseYDispersion(BOOL* pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fDispY;

	return S_OK;
}

STDMETHODIMP ShowStats::put_UseYDispersion(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fDispY = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_ExcludeX(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fExcludeX;

	return S_OK;
}

STDMETHODIMP ShowStats::put_ExcludeX(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fExcludeX = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_ExcludeValX(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_dExcludeValX;

	return S_OK;
}

STDMETHODIMP ShowStats::put_ExcludeValX(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dExcludeValX = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_ExcludeY(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fExcludeY;

	return S_OK;
}

STDMETHODIMP ShowStats::put_ExcludeY(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fExcludeY = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_ExcludeValY(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_dExcludeValY;

	return S_OK;
}

STDMETHODIMP ShowStats::put_ExcludeValY(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dExcludeValY = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_UseGrouping(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fGrouping;

	return S_OK;
}

STDMETHODIMP ShowStats::put_UseGrouping(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fGrouping = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_Grouping(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nGrouping;

	return S_OK;
}

STDMETHODIMP ShowStats::put_Grouping(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nGrouping = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_GroupingCount(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_nGroupingCount;

	return S_OK;
}

STDMETHODIMP ShowStats::put_GroupingCount(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nGroupingCount = newVal;
	if( m_nGroupingCount > 0 )
	{
		if( m_pItems ) delete [] m_pItems;
		m_pItems = NULL;
		if( m_pTypes ) delete [] m_pTypes;
		m_pTypes = NULL;
		if( m_pSizes ) delete [] m_pSizes;
		m_pSizes = NULL;
		if( m_pColors ) delete [] m_pColors;
		m_pColors = NULL;

		m_pItems = new int[ m_nGroupingCount ];
		m_pTypes = new int[ m_nGroupingCount ];
		m_pSizes = new int[ m_nGroupingCount ];
		m_pColors = new long[ m_nGroupingCount ];

		return S_OK;
	}

	return E_FAIL;
}

STDMETHODIMP ShowStats::get_GroupingItemIndex(short Idx, short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pItems && ( Idx < m_nGroupingCount ) )
		*pVal = (short)m_pItems[ Idx ];

	return S_OK;
}

STDMETHODIMP ShowStats::put_GroupingItemIndex(short Idx, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pItems && ( Idx < m_nGroupingCount ) )
		m_pItems[ Idx ] = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_GroupingItemType(short Idx, short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pTypes && ( Idx < m_nGroupingCount ) )
		*pVal = (short)m_pTypes[ Idx ];

	return S_OK;
}

STDMETHODIMP ShowStats::put_GroupingItemType(short Idx, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pTypes && ( Idx < m_nGroupingCount ) )
		m_pTypes[ Idx ] = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_GroupingItemSize(short Idx, short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pSizes && ( Idx < m_nGroupingCount ) )
		*pVal = (short)m_pSizes[ Idx ];

	return S_OK;
}

STDMETHODIMP ShowStats::put_GroupingItemSize(short Idx, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pSizes && ( Idx < m_nGroupingCount ) )
		m_pSizes[ Idx ] = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_GroupingItemColor(short Idx, long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pColors && ( Idx < m_nGroupingCount ) )
		*pVal = m_pColors[ Idx ];

	return S_OK;
}

STDMETHODIMP ShowStats::put_GroupingItemColor(short Idx, long newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pColors && ( Idx < m_nGroupingCount ) )
		m_pColors[ Idx ] = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_UseFlagging(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fFlag;

	return S_OK;
}

STDMETHODIMP ShowStats::put_UseFlagging(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fFlag = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_FlagIndex(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_nFlagIdx;

	return S_OK;
}

STDMETHODIMP ShowStats::put_FlagIndex(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nFlagIdx = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_FlagMin(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_dFlagMin;

	return S_OK;
}

STDMETHODIMP ShowStats::put_FlagMin(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dFlagMin = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_FlagMax(double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_dFlagMax;

	return S_OK;
}

STDMETHODIMP ShowStats::put_FlagMax(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dFlagMax = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_FlagType(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nFlagType;

	return S_OK;
}

STDMETHODIMP ShowStats::put_FlagType(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nFlagType = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_FlagSize(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nFlagSize;

	return S_OK;
}

STDMETHODIMP ShowStats::put_FlagSize(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nFlagSize = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_FlagColor(long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_nFlagColor;

	return S_OK;
}

STDMETHODIMP ShowStats::put_FlagColor(long newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nFlagColor = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_UseSpecificStrokes(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fUseStrokes;

	return S_OK;
}

STDMETHODIMP ShowStats::put_UseSpecificStrokes(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fUseStrokes = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_XStroke(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_nXStroke;

	return S_OK;
}

STDMETHODIMP ShowStats::put_XStroke(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nXStroke = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_YStroke(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_nYStroke;

	return S_OK;
}

STDMETHODIMP ShowStats::put_YStroke(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nYStroke = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::ShowCombo(VARIANT *GraphWindow, BOOL ShowScat,
								  BSTR StatFile, BSTR ScatFile,
								  BSTR Experiment, BSTR Grouping,
								  BSTR Title, BSTR SubTitle, BSTR XAxisLabel,
								  BSTR YAxisLabel, short XAxisColumn, short YAxisColumn,
								  short StatColumnX, short StatColumnY,
								  short StdDevColumn, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	m_szExp = Experiment;
	m_szGrp = Grouping;
	m_nStdDev = StdDevColumn;
	m_szTitle = Title;
	m_szSub = SubTitle;
	m_szX = XAxisLabel;
	m_szY = YAxisLabel;

	m_nX = StatColumnX;
	m_nY = StatColumnY;
	m_szFile = ScatFile;
	InitScatter();
	_sd.nCnt1 = m_nCnt;

	m_nX = XAxisColumn;
	m_nY = YAxisColumn;
	m_szFile = StatFile;
	int numX = 0, numY = 0;
	Init( numX, numY );
	_sd.nCnt2 = m_nCnt;

	CWnd* pGraph = (CWnd*)GraphWindow;
	RECT rect;
	pGraph->GetClientRect( &rect );

	HWND* hPE = (HWND*)PEHandle;
	if( *hPE ) PEdestroy( *hPE );
	*hPE = PEcreate( PECONTROL_SGRAPH, WS_VISIBLE, &rect, pGraph->m_hWnd, 1001 );
	if( !*hPE ) return E_FAIL;

	PEreset( pGraph->m_hWnd );

	// Set number of subsets and points
	int nSubsets = 1;
	if( m_pItems && m_pTypes && m_pSizes && m_pColors && m_fGrouping )
		nSubsets = m_nGroupingCount;
	if( m_fFlag ) nSubsets++;
	PEnset( *hPE, PEP_nSUBSETS, nSubsets );
	PEnset( *hPE, PEP_nPOINTS, _sd.nCnt1 );
	double dNull = NULLDATAVALUE;
	PEvset(*hPE, PEP_fNULLDATAVALUE, &dNull, 1);

	int i, j;
	for( i = 0; i < _sd.nCnt1; i++ )
	{
		_sd.nSubset = 0;

		if( m_fGrouping && m_pItems && ssubset )
		{
			for( j = 0; j < m_nGroupingCount; j++ )
			{
				if( ssubset[ i ] == m_pItems[ j ] )
				{
					_sd.nSubset = j;
					break;
				}
			}
		}
		if( m_fFlag && flags && flags[ i ] )
		{
			_sd.nSubset = nSubsets - 1;
		}

		_sd.fX = (float)sx[ i ];
		if( sx[ i ] == ::GetMissingDataValue() ) _sd.fX = NULLDATAVALUE;
		_sd.fY = (float)sy[ i ];
		if( sy[ i ] == ::GetMissingDataValue() ) _sd.fY = NULLDATAVALUE;

		if( m_fDispX && slvl )
		{
			_sd.fX += (float)( slvl[ i ] * m_dStrengthX );
			m_dsXMin = MIN( m_dsXMin, _sd.fX );
			m_dsXMax = MAX( m_dsXMax, _sd.fX );
		}
		if( m_fDispY && slvl2 )
		{
			_sd.fY += (float)( slvl2[ i ] * m_dStrengthY );
			m_dsYMin = MIN( m_dsYMin, _sd.fY );
			m_dsYMax = MAX( m_dsYMax, _sd.fY );
		}

		if( ShowScat )
		{
			PEvsetcellEx( *hPE, PEP_faXDATA, _sd.nSubset, i, &_sd.fX );
			PEvsetcellEx( *hPE, PEP_faYDATA, _sd.nSubset, i, &_sd.fY );
		}
	}

	// Averages & std. devs. are shown as annotations
	PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
	PEnset(*hPE, PEP_nGRAPHANNOTATIONTEXTSIZE, 100);
	PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
	PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);

	// Averages
	SortBySubset( numY, numX );
	int nSDPt = 0, nLastLvl = -1;
	CStringList lstColor;
	CString szColor;
	for( i = 0; i < numY; i++ )
	{
		_sd.dX = x[ i ];
		_sd.dY = y[ i ];

		if( subset ) szColor.Format( _T("%d"), subset[ i ] );
		if( !szColor.IsEmpty() && !lstColor.Find( szColor ) )
		{
			lstColor.AddTail( szColor );
			if( m_pColors )
				_sd.dwCol = subset ? m_pColors[ subset[ i ] ] : ::GetColorByIndex( 1, 1 );
			else
				_sd.dwCol = subset ? ::GetColorByIndex( subset[ i ], numX ) : ::GetColorByIndex( 1, 1 );
		}

		if( m_fDispX && lvl )
		{
			_sd.dX += ( lvl[ i ] * m_dStrengthX );
			m_dXMin = MIN( m_dXMin, _sd.dX );
			m_dXMax = MAX( m_dXMax, _sd.dX );
		}
		if( m_fDispY && lvl2 )
		{
			_sd.dY += ( lvl2[ i ] * m_dStrengthY );
			m_dYMin = MIN( m_dYMin, _sd.dY );
			m_dYMax = MAX( m_dYMax, _sd.dY );
		}
		if( subset )
		{
			if( subset[ i ] != nLastLvl )
			{
				nLastLvl = subset[ i ];
				_sd.nSubset = PEGAT_THINSOLIDLINE;
				_sd.nSubset2 = PEGAT_SMALLDOTSOLID;
			}
			else _sd.nSubset = PEGAT_LINECONTINUE;
		}
		else if( nLastLvl == -1 )
		{
			nLastLvl = 1;
			_sd.nSubset = PEGAT_THINSOLIDLINE;
			_sd.nSubset2 = PEGAT_SMALLDOTSOLID;
		}
		else _sd.nSubset = PEGAT_LINECONTINUE;

		PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &_sd.dX);
		PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &_sd.dY);
		PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &_sd.nSubset);
		PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &_sd.dwCol);

		nSDPt++;

		PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &_sd.dX);
		PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &_sd.dY);
		PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &_sd.nSubset2);
		PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &_sd.dwCol);

		nSDPt++;
	}

	// Standard Deviations
	lstColor.RemoveAll();
	szColor = _T("");
	if( ( m_nStdDev > 0 ) && stddev )
	{
		double dY2;
		for( i = 0; i < numY; i++ )
		{
			_sd.dX = x[ i ];
			_sd.fY = (float)y[ i ];

			if( subset ) szColor.Format( _T("%d"), subset[ i ] );
			if( !szColor.IsEmpty() && !lstColor.Find( szColor ) )
			{
				lstColor.AddTail( szColor );
				if( m_pColors )
					_sd.dwCol = subset ? m_pColors[ subset[ i ] ] : ::GetColorByIndex( 1, 1 );
				else
					_sd.dwCol = subset ? ::GetColorByIndex( subset[ i ], numX ) : ::GetColorByIndex( 1, 1 );
			}

			if( m_fDispX && lvl )
				_sd.dX += ( lvl[ i ] * m_dStrengthX );

			_sd.dY = _sd.fY + stddev[ i ];
			if( m_fDispY && lvl2 )
				_sd.dY += ( lvl2[ i ] * m_dStrengthY );
			m_dYMin = MIN( m_dYMin, _sd.dY );
			m_dYMax = MAX( m_dYMax, _sd.dY );
//			_sd.nSubset = PEGAT_DOWNTRIANGLESOLID;
			_sd.nSubset = PEGAT_DASH;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &_sd.dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &_sd.dY);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &_sd.nSubset);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &_sd.dwCol);

			nSDPt++;

			dY2 = _sd.fY - stddev[ i ];
			if( m_fDispY && lvl2 )
				dY2 += ( lvl2[ i ] * m_dStrengthY );
			m_dYMin = MIN( m_dYMin, dY2 );
			m_dYMax = MAX( m_dYMax, dY2 );
//			_sd.nSubset = PEGAT_UPTRIANGLESOLID;
			_sd.nSubset = PEGAT_DASH;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &_sd.dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &dY2);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &_sd.nSubset);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &_sd.dwCol);

			nSDPt++;

			_sd.nSubset = PEGAT_THINSOLIDLINE;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &_sd.dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &dY2);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &_sd.nSubset);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &_sd.dwCol);

			nSDPt++;

			_sd.nSubset = PEGAT_LINECONTINUE;
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nSDPt, &_sd.dX);
			PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nSDPt, &_sd.dY);
			PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nSDPt, &_sd.nSubset);
			PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nSDPt, &_sd.dwCol);

			nSDPt++;
		}
	}

	// Set colors
//	PEnset(*hPE, PEP_dwDESKCOLOR, RGB(192, 192, 192));
//	PEnset(*hPE, PEP_dwGRAPHBACKCOLOR, RGB(255, 255, 255));
	PEnset(*hPE, PEP_dwGRAPHFORECOLOR, RGB(0, 0, 0));
	PEnset(*hPE, PEP_dwSHADOWCOLOR, RGB(255, 255, 255));
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, RGB(255, 255, 255));

	// Set DataShadows to show 3D 
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	// Labels
	if( m_szGrp != _T("NONE") )
		sprintf_s( _sd.label, _T("%s, %s vs %s per %s"), m_szExp, m_szX, m_szY, m_szGrp );
	else
		sprintf_s( _sd.label, _T("%s, %s vs %s"), m_szExp, m_szX, m_szY );
//	strcpy_s( _sd.label, m_szTitle );
	// 08Oct03 - no main title now
	PEszset(*hPE, PEP_szMAINTITLE, _T(""));
	strcpy_s( _sd.label, 75, m_szSub );
	PEszset(*hPE, PEP_szSUBTITLE, _sd.label);
	strcpy_s( _sd.label, 75, m_szX );
	PEszset(*hPE, PEP_szXAXISLABEL, _sd.label );
	strcpy_s( _sd.label, 75, m_szY );
	PEszset(*hPE, PEP_szYAXISLABEL, _sd.label);
	PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
	PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szLABELFONT, "Arial");
	PEszset(*hPE, PEP_szTABLEFONT, "Arial");

	// fixed font for scaling
	PEnset(*hPE, PEP_bFIXEDFONTS, TRUE);

	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_bPREPAREIMAGES, TRUE);
	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_POINT);
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);
	PEnset( *hPE, PEP_nPOINTSIZE, PEPS_MICRO );

    //** Disable some types of plotting methods **'
	PEnset(*hPE, PEP_bALLOWSPLINE, FALSE);
	PEnset(*hPE, PEP_bALLOWPOINTSPLUSSPLINE, FALSE);
	PEnset(*hPE, PEP_bALLOWBESTFITLINE, FALSE);
	PEnset(*hPE, PEP_bALLOWBESTFITCURVE, FALSE);
	PEnset(*hPE, PEP_bALLOWAREA, FALSE);

	// color/point type/point size
	if( m_fGrouping && m_pColors )
	{
		DWORD* pColors = new DWORD[ m_nGroupingCount + ( m_fFlag ? 1 : 0 ) ];
		int* pTypes = new int[ m_nGroupingCount + ( m_fFlag ? 1 : 0 ) ];

		for( int k = 0; k < ( m_nGroupingCount + ( m_fFlag ? 1 : 0 ) ); k++ )
		{
			if( k < m_nGroupingCount )
			{
				pColors[ k ] = m_pColors[ k ];
				pTypes[ k ] = m_pTypes[ k ];
			}
			else
			{
				pColors[ k ] = m_nFlagColor;
				pTypes[ k ] = m_nFlagType;
			}
		}
		PEvsetEx( *hPE, PEP_dwaSUBSETCOLORS, 0, m_nGroupingCount + ( m_fFlag ? 1 : 0 ), pColors, 0 );
		PEvset( *hPE, PEP_naSUBSETPOINTTYPES, pTypes, m_nGroupingCount + ( m_fFlag ? 1 : 0 ) );

		delete [] pColors;
		delete [] pTypes;
	}
	else if( m_fFlag )
	{
		DWORD dwColors[ 2 ];
		dwColors[ 0 ] = RGB( 63, 63, 191 );
		dwColors[ 1 ] = m_nFlagColor;
		int nLineTypes[ 2 ];
		nLineTypes[ 0 ] = PEPT_DOTSOLID;
		nLineTypes[ 1 ] = m_nFlagType;

		PEvsetEx( *hPE, PEP_dwaSUBSETCOLORS, 0, 2, dwColors, 0 );
		PEvset( *hPE, PEP_naSUBSETPOINTTYPES, nLineTypes, 2 );
	}
	else
	{
		DWORD dwColor = RGB( 63,63,191 );
		PEvset( *hPE, PEP_dwaSUBSETCOLORS, &dwColor, 1 );
		int nLineType = PEPT_DOTSOLID;
		PEvset( *hPE, PEP_naSUBSETPOINTTYPES, &nLineType, 1 );
	}

	CString szTemp;
	if( m_fGrouping )
	{
		for( i = 0; i < m_nCnt; i++ )
		{
			szTemp.Format( _T("%s %d"), m_szSubset, i + 1 );
			strcpy_s( _sd.label, 75, szTemp );
			PEvsetcell( *hPE, PEP_szaSUBSETLABELS, i, _sd.label );
		}
	}

	// When set to no manual PEreinitialize delivers the min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	PEreinitialize( *hPE );

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

	// Adjust ranges to keep data off edge of graph
	if( ( m_dsXMax - m_dsXMin ) == 0.0 )
	{
		m_dsXMin -= ( m_dsXMin * 0.10 );
		m_dsXMax += ( m_dsXMax * 0.10 );
	}
	else
	{
		m_dsXMin -= ( ( m_dsXMax - m_dsXMin ) * 0.10 );
		m_dsXMax += ( ( m_dsXMax - m_dsXMin ) * 0.10 );
	}
	if( ( m_dsYMax - m_dsYMin ) == 0.0 )
	{
		m_dsYMin -= ( m_dsYMin * 0.10 );
		m_dsYMax += ( m_dsYMax * 0.10 );
	}
	else
	{
		m_dsYMin -= ( ( m_dsYMax - m_dsYMin ) * 0.10 );
		m_dsYMax += ( ( m_dsYMax - m_dsYMin ) * 0.10 );
	}

	m_dXMin = MIN( m_dXMin, m_dsXMin );
	m_dXMax = MAX( m_dXMax, m_dsXMax );
	m_dYMin = MIN( m_dYMin, m_dsYMin );
	m_dYMax = MAX( m_dYMax, m_dsYMax );

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &m_dXMin, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &m_dXMax, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &m_dYMin, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &m_dYMax, 1);

    //** Set up cursor **
//    PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);

    //** This will allow you to move cursor by clicking data point **
    PEnset(*hPE, PEP_bMOUSECURSORCONTROL, TRUE);
    PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, TRUE);

    //** Cursor prompting in top left corner **
    PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
    PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);

	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowStats::ShowClear(VARIANT *GraphWindow, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	RECT rect;
	pGraph->GetClientRect( &rect );

	HWND* hPE = (HWND*)PEHandle;
	if( *hPE ) PEdestroy( *hPE );

	PEreset( pGraph->m_hWnd );

	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowStats::get_SubsetColumn(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = (short)m_nSubCol;

	return S_OK;
}

STDMETHODIMP ShowStats::put_SubsetColumn(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nSubCol = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::get_SubsetName(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_szSubset.AllocSysString();

	return S_OK;
}

STDMETHODIMP ShowStats::put_SubsetName(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_szSubset = newVal;

	return S_OK;
}

void ShowStats::SortBySubset( int nCount, int nLvls )
{
	if( ( nCount <= 1 ) || !x || !y || !subset || !stddev ) return;

	double* x2 = new double[ nCount ];
	double* y2 = new double[ nCount ];
	double* sd2 = new double[ nCount ];
	int* l2 = new int[ nCount ];

	int i, j;
	for( i = 0; i < nCount; l2[ i++ ] = nLvls );

	for( i = 0; i < nCount; i++ )
	{
		for( j = 0; j < nCount; j++ )
		{
			if( ( subset[ i ] < l2[ j ] ) && ( l2[ j ] == nLvls ) )
			{
				l2[ j ] = subset[ i ];
				x2[ j ] = x[ i ];
				y2[ j ] = y[ i ];
				sd2[ j ] = stddev[ i ];
				break;
			}
		}
	}

	for( i = 0; i < nCount; i++ )
	{
		subset[ i ] = l2[ i ];
		x[ i ] = x2[ i ];
		y[ i ] = y2[ i ];
		stddev[ i ] = sd2[ i ];
	}

	delete [] x2;
	delete [] y2;
	delete [] sd2;
	delete [] l2;
}

STDMETHODIMP ShowStats::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetGraphLogOn( fOn );

	return S_OK;
}

STDMETHODIMP ShowStats::get_StdDevN(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = m_fStdDevN;

	return S_OK;
}

STDMETHODIMP ShowStats::put_StdDevN(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fStdDevN = newVal;

	return S_OK;
}

STDMETHODIMP ShowStats::SetMissingDataValue(double MDVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
		::SetMissingDataValue( MDVal );
	return S_OK;
}

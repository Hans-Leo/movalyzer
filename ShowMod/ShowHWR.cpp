// ShowHWR.cpp : Implementation of CShowModApp and DLL registration.

#include "stdafx.h"
#include "ShowMod.h"
#include "ShowHWR.h"
#include "..\Common\deffun.h"
#include "..\Common\ProcSettings.h"
#include "..\NSShared\hlib.h"


#define	NULLDATAVALUE		-9999.0F
#define	LINECOLOR			RGB( 63, 63, 191 )
#define	LINE2COLOR			RGB( 255, 0, 0 )
#define	MONOCOLOR			RGB( 0, 0, 0 )
#define	BGCOLOR				RGB( 255, 255, 255 )
//#define	FORECOLOR			RGB( 127, 127, 127 )
#define	FORECOLOR			RGB( 0, 0, 0 )
// this is a hack since the digitizer has been reporting bad values every n samples
// the graphs are not drawing properly because the ranges get horked as a result
#define	BAD_DATA			1000000		// this would be about a 394" tablet
#define	TBL_DATA_SIZE		50
#define	PETAL_LOC			PETAL_TOP_LEFT
#define	PETAL_STR			" Sample=%d Pressure=%.0f"

// axis definers for multiple base
#define	X_DATA				1
#define	Y_DATA				2
#define	Z_DATA				3

/////////////////////////////////////////////////////////////////////////////

// number of available sizes (currently) from PE
#define	NUM_SIZES					8
// nPressure is the pressure to scale (we've already found min/max in init)
int ScaleLineThickness( ShowHWR* pShow, int nPressure )
{
	if( !pShow ) return PEGAT_THINSOLIDLINE;
	if( ( pShow->_nPPMax - pShow->_nPPMin ) == 0 ) return PEGAT_THINSOLIDLINE;

	int theSizes[ NUM_SIZES ] = {
		PEGAT_EXTRAEXTRATHINSOLID,
		PEGAT_EXTRATHINSOLID,
		PEGAT_THINSOLIDLINE,
		PEGAT_MEDIUMTHINSOLID,
		PEGAT_MEDIUMSOLIDLINE,
		PEGAT_MEDIUMTHICKSOLID,
		PEGAT_THICKSOLIDLINE,
		PEGAT_EXTRATHICKSOLID
	};

	// index for above table
	double dIdx = NUM_SIZES * ( nPressure - pShow->_nPPMin) / ( pShow->_nPPMax - pShow->_nPPMin );
	// add 0.5 since no round function in this version of VC
	dIdx += 0.5;
	// round for integer (cast)
	int nIdx = (int)dIdx;
	if( nIdx < 1 ) nIdx = 1;

	// ensure we are not out of array bounds
	if( ( nIdx < 0 ) || ( nIdx > NUM_SIZES ) )
		return PEGAT_THINSOLIDLINE;
	else if( nPressure <= ::GetMinPenPressure() )
		return PEGAT_DOTLINE;
	else
		return theSizes[ nIdx - 1 ];
}

/////////////////////////////////////////////////////////////////////////////
//

ShowHWR::ShowHWR() : m_fInit( FALSE ), x( NULL ), y( NULL ), z( NULL ),
	m_dFrequency( 200 ), m_nCnt( 0 ), m_fUNR( FALSE ), ux( NULL ),
	uy( NULL ), uz( NULL ), m_nCnt2( 0 ), m_fPPLT( FALSE )
{
	::SetMinPenPressure( 1 );
	m_dwPenUpColor = RGB( (int)(0.64*255), (int)(0.72*255), (int)(0.77*255) );
	m_fOptimal = FALSE;
	m_fMonochrome = FALSE;
	m_fInverted = FALSE;

	// FORMERYLY static global vars
	// current stroke
	_nStroke = 0;
	// sub-movement analysis is on
	_fSubMovement = FALSE;
	// total segments
	_nTotalSegments = 0;
	// last pen pressure
	_nLastPenPressure = 0;
	// min/max pen pressures
	_nPPMin = 0;
	_nPPMax = 0;
	// min/max x/y/z for pen pressure variation (since using annotations)
	_dXMin = 0.;
	_dXMax = 0.;
	_dYMin = 0.;
	_dYMax = 0.;
	_dZMin = 0.;
	_dZMax = 0.;
	// index for real-time chart
	_Idx = 0;
}

ShowHWR::~ShowHWR()
{
	if( x ) delete [] x;
	if( y ) delete [] y;
	if( z ) delete [] z;

	if( ux ) delete [] ux;
	if( uy ) delete [] uy;
	if( uz ) delete [] uz;
}

STDMETHODIMP ShowHWR::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IShowHWR,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

#pragma warning( disable: 4996 )	// disable deprecated warning
void ShowHWR::Init()
{
	_nStroke = 0;
	_fSubMovement = FALSE;
	_nTotalSegments = 0;
	_nLastPenPressure = ::GetMinPenPressure() - 1;
	_dXMin = NULLDATAVALUE;
	_dXMax = NULLDATAVALUE;
	_dYMin = NULLDATAVALUE;
	_dYMax = NULLDATAVALUE;
	_dZMin = NULLDATAVALUE;
	_dZMax = NULLDATAVALUE;

	if( m_szFile.IsEmpty() )
	{
		::OutputMessage( _T("No data file specified."), LOG_GRAPH );
		return;
	}

	// HWR File
	CStdioFile f;
	if( !f.Open( m_szFile, CFile::modeRead ) )
	{
		::OutputMessage( _T("Error opening source data file."), LOG_GRAPH );
		return;
	}

	// EXT File (if exists)
	CStdioFile sf;
	BOOL fS = FALSE;
	CString szFileExt = m_szFile.Left( m_szFile.GetLength() - 4 );	// .HWR removed
	int nTrial = atoi( szFileExt.Right( 2 ) );
	szFileExt = m_szFile.Left( m_szFile.GetLength() - 6 );	// ##.HWR removed
	szFileExt += _T(".EXT");
	if( sf.Open( szFileExt, CFile::modeRead ) ) fS = TRUE;

	CString szLine;
	m_nCnt = 0;
	while( f.ReadString( szLine ) )
		m_nCnt++;

	if( m_nCnt == 0 )
	{
		::OutputMessage( _T("No source data found to graph."), LOG_GRAPH );
		return;
	}
	else if( m_nCnt == 1 )	// crashing...so just display nothing
	{
		m_nCnt = 0;
		return;
	}

	f.SeekToBegin();

	if( x ) delete [] x;
	x = new double[ m_nCnt ];
	memset( x, 0, sizeof( double ) * m_nCnt );
	if( y ) delete [] y;
	y = new double[ m_nCnt ];
	memset( y, 0, sizeof( double ) * m_nCnt );
	if( z ) delete [] z;
	z = new double[ m_nCnt ];
	memset( z, 0, sizeof( double ) * m_nCnt );

	float dx = 0., dy = 0., dz = 0.;
	float dtestx = 0., dtesty = 0.;	// added to test for bad data (see top of file)
	int nPos = 0;
	while( f.ReadString( szLine ) )
	{
		sscanf( szLine, "%f %f %f", &dx, &dy, &dz );

		// bad data test
		dtestx = dx;
		dtesty = dy;
		if( dtestx < 0 ) dtestx *= -1.;
		if( dtesty < 0 ) dtesty *= -1.;
		if( dtestx > BAD_DATA )
		{
			if( nPos > 0 ) dx = (float)x[ nPos - 1 ];
			else dx = 0.;
		}
		if( dtesty > BAD_DATA )
		{
			if( nPos > 0 ) dy = (float)y[ nPos - 1 ];
			else dy = 0.;
		}

		x[ nPos ] = dx;
		y[ nPos ] = dy;
		z[ nPos ] = dz;

		// min max for annotation pen pressure deviations
//		if( m_fPPLT )
		{
			if( nPos == 0 )
			{
				_dXMin = _dXMax = dx;
				_dYMin = _dYMax = dy;
				_dZMin = _dZMax = dz;
			}
			else
			{
				if( dx < _dXMin ) _dXMin = dx;
				if( dx > _dXMax ) _dXMax = dx;
				if( dy < _dYMin ) _dYMin = dy;
				if( dy > _dYMax ) _dYMax = dy;
				if( dz < _dZMin ) _dZMin = dz;
				if( dz > _dZMax ) _dZMax = dz;
			}
		}

		// Pen pressure min/max
		if( nPos == 0 ) _nPPMin = (int)dz;
		else if( dz < _nPPMin ) _nPPMin = (int)dz;
		if( dz > _nPPMax ) _nPPMax = (int)dz;

		nPos++;
	}
	f.Close();

	// unrotation info
	CString szFile2 = m_szFile.Left( m_szFile.GetLength() - 3 );
	szFile2 += _T("unr");
	CFileFind ff;
	if( ff.FindFile( szFile2 ) ) m_fUNR = TRUE;

	if( m_fUNR )
	{
		if( !f.Open( szFile2, CFile::modeRead ) )
		{
			::OutputMessage( _T("Error opening source data file (UNR)."), LOG_GRAPH );
			return;
		}

		m_nCnt2 = 0;
		nPos = 0;
		while( f.ReadString( szLine ) )
			m_nCnt2++;

		if( m_nCnt2 == 0 ) return;

		f.SeekToBegin();

		if( ux ) delete [] ux;
		ux = new double[ m_nCnt2 ];
		if( uy ) delete [] uy;
		uy = new double[ m_nCnt2 ];
		if( uz ) delete [] uz;
		uz = new double[ m_nCnt2 ];

		while( f.ReadString( szLine ) )
		{
			sscanf( szLine, "%f %f %f", &dx, &dy, &dz );

			ux[ nPos ] = dx;
			uy[ nPos ] = dy;
			uz[ nPos ] = dz;

			nPos++;
		}
	}

	m_fInit = TRUE;
}
#pragma warning( default: 4996 )	// disable deprecated warning

STDMETHODIMP ShowHWR::PrintGraph(VARIANT* PEHandle)
{
	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;

	BOOL fRet = PElaunchprintdialog( *hPE, TRUE, NULL );

	return S_OK;
}

STDMETHODIMP ShowHWR::ExportGraph(VARIANT* PEHandle)
{
	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;

	BOOL fRet = PElaunchexport( *hPE );

	return S_OK;
}

BOOL ShowHWR::InitChart( HWND* hPE, CWnd* pGraph, CWnd* pGraphDlg, UINT nType )
{
	if( !hPE || !pGraph ) return FALSE;

	pGraph->GetClientRect( &ScreenRectangle );

	HGLOBAL hGlobal = 0;
	DWORD dwSize = 0;
	void FAR* lpData = NULL;
	BOOL fStored = FALSE;
	if( *hPE )
	{
		PEstorepartial( *hPE, &hGlobal, &dwSize );
		fStored = TRUE;
		PEdestroy( *hPE );
	}
	*hPE = PEcreate( nType, WS_VISIBLE, &ScreenRectangle,
					 pGraphDlg ? pGraphDlg->m_hWnd : pGraph->m_hWnd, 1001 );
	if( !*hPE )
	{
		::OutputMessage( _T("Error creating graphing COM/DCOM object."), LOG_GRAPH );
		return FALSE;
	}

	PEreset( pGraph->m_hWnd );
	PEnset( *hPE, PEP_bPREPAREIMAGES, TRUE );
	double dNull = NULLDATAVALUE;
	if( pGraphDlg ) PEvset(*hPE, PEP_fNULLDATAVALUEX, &dNull, 1);
	PEvset(*hPE, PEP_fNULLDATAVALUE, &dNull, 1);

	if( fStored )
	{
		PEloadpartial( *hPE, &hGlobal );
		GlobalFree( hGlobal );
		PEreinitialize( *hPE );
		PEresetimage( *hPE, 0, 0 );
	}

	return TRUE;
}

void ShowHWR::SetChartDefaults( HWND* hPE, HWRGraph gt, BOOL fSetLineThickness )
{
	if( !hPE || !*hPE ) return;

	// monochrome?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);

	// Set colors
	PEnset(*hPE, PEP_dwGRAPHFORECOLOR, FORECOLOR);
	PEnset(*hPE, PEP_dwSHADOWCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, BGCOLOR);

	// Set DataShadows to none
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	// plotting methods
	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);
	PEnset(*hPE, PEP_nGRIDSTYLE, PEGS_THICK);
	PEnset(*hPE, PEP_bSCROLLINGHORZZOOM, TRUE);
	PEnset(*hPE, PEP_bSCROLLINGVERTZOOM, TRUE);

	// fixed font for scaling
	PEnset(*hPE, PEP_bFIXEDFONTS, TRUE);

	// color
	DWORD dwColor = m_fMonochrome ? MONOCOLOR : LINECOLOR;
	if( fSetLineThickness ) PEvset(*hPE, PEP_dwaSUBSETCOLORS, &dwColor, 1);
	
	// line type
	int nLineType = PELT_THINSOLID;
	if( fSetLineThickness ) PEvset(*hPE, PEP_naSUBSETLINETYPES, &nLineType, 1);

	// point type
	int nPointType = PEPT_DOTSOLID;
	PEvset(*hPE, PEP_naSUBSETPOINTTYPES, &nPointType, 1);

	// When set to no manual PEreinitialize delivers the min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLZ, PEMSC_NONE);
	PEreinitialize( *hPE );

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLZ, PEMSC_MINMAX);

	// Get min and max of x and y data
	double dataxmin = 0.0, dataxmax = 0.0, dataymin = 0.0, dataymax = 0.0;
	double datazmin = 0.0, datazmax = 0.0;
	if( fSetLineThickness )
	{
		PEvget(*hPE, PEP_fMANUALMINX, &dataxmin);
		PEvget(*hPE, PEP_fMANUALMAXX, &dataxmax);
		PEvget(*hPE, PEP_fMANUALMINY, &dataymin);
		PEvget(*hPE, PEP_fMANUALMAXY, &dataymax);
		PEvget(*hPE, PEP_fMANUALMINZ, &datazmin);
		PEvget(*hPE, PEP_fMANUALMAXZ, &datazmax);
	}
	else
	{
		// x-axis min/max based upon graph type
		if( ( gt == HWR_XY ) || ( gt == HWR_XZ ) || ( gt == HWR_XY_3D ) )
		{
			dataxmin = _dXMin;
			dataxmax = _dXMax;
		}
		else if( gt == HWR_YZ )
		{
			dataxmin = _dYMin;
			dataxmax = _dYMax;
		}
		else if( ( gt == HWR_XYt ) || ( gt == HWR_Zt ) )
		{
			dataxmin = 0;
			dataxmax = m_nCnt;
		}
		// y-axis min/max based upon graph type
		if( ( gt == HWR_XZ ) || ( gt == HWR_YZ ) || ( gt == HWR_Zt ) )
		{
			dataymin = _dZMin;
			dataymax = _dZMax;
		}
		else if( ( gt == HWR_XY ) || ( gt == HWR_XY_3D ) )
		{
			dataymin = _dYMin;
			dataymax = _dYMax;
		}
		else if( gt == HWR_XYt )
		{
			dataymin = ( _dXMin < _dYMin ) ? _dXMin : _dYMin;
			dataymax = ( _dXMax > _dYMax ) ? _dXMax : _dYMax;
		}
	}
		
	// Calculate the real data range
	double dataxrange = dataxmax - dataxmin;
	double datayrange = dataymax - dataymin;
	double datazrange = datazmax - datazmin;

	// Test for zero data ranges.
	if (dataxrange == 0.) dataxrange = 1.;
	if (datayrange == 0.) datayrange = 1.;
	if (datazrange == 0.) datazrange = 1.;

	// Get the screen positions. 
	PEvget (*hPE, PEP_rectGRAPH, &ScreenRectangle); 

	// Calculate data to pixel scales ant take smallest scale for x and y so that
	// the graph fits
	double scalex = (ScreenRectangle.right - ScreenRectangle.left) / dataxrange;
	double scaley = (ScreenRectangle.bottom - ScreenRectangle.top) / datayrange;
	double scalez = (ScreenRectangle.bottom - ScreenRectangle.top) / datazrange;
	double scale = ( m_fInverted || ( gt != HWR_XY_3D ) ) ? MIN( scalex, scaley ) : MIN( scalex, scalez );

	// Test for zero scale.
	if (scale == 0.) scale = 1.;

	// Estimate the extra data space for x or y data needed to assure that a square remains a square
	double extradataxrange = 0.;
	double extradatayrange = 0.;
	double extradatazrange = 0.;
	if( !m_fOptimal )	// ie..proportional
	{
		if( scale == 1. ) scale = 2.;	// so we dont get a divide by zero
		extradataxrange = dataxrange * (scalex / scale - 1.);
		if( m_fInverted || ( gt != HWR_XY_3D ) )
			extradatayrange = datayrange * (scaley / scale - 1.);
		else
			extradatazrange = datazrange * (scalez / scale - 1.);
	}
	// else independent optimal x & y scaling

    // Divide the extra data space so that the graph is in the middle of the window
	double dataxminnew = dataxmin - .5 * extradataxrange;
	double dataxmaxnew = dataxmax + .5 * extradataxrange;
	double datayminnew = dataymin - .5 * extradatayrange;
	double dataymaxnew = dataymax + .5 * extradatayrange;
	double datazminnew = datazmin - .5 * extradatazrange;
	double datazmaxnew = datazmax + .5 * extradatazrange;

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &dataxminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &dataxmaxnew, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &datayminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &dataymaxnew, 1);
	PEvset(*hPE, PEP_fMANUALMINZ, &datazminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXZ, &datazmaxnew, 1);

	//** Set up cursor **
    PEnset(*hPE, PEP_nCURSORMODE, PECM_DATACROSS);

	//** Help see data points **
	//PEnset(*hPE, PEP_bMARKDATAPOINTS, TRUE);

	//** This will allow you to move cursor by clicking data point **
	PEnset(*hPE, PEP_bMOUSECURSORCONTROL, TRUE);
	PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, TRUE);

	//** Cursor prompting in top left corner **
	PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, TRUE);
	PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_XYVALUES);
}

STDMETHODIMP ShowHWR::ShowHWRGraph(VARIANT *GraphWindow, HWRGraph GraphType,
								   VARIANT *GraphDlg, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	if( !m_fInit ) Init();
	if( !m_fInit ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	CWnd* pGraphDlg = (CWnd*)GraphDlg;

	BOOL fRet = FALSE;
	if( GraphType == ( HWR_XY_3D | HWR_XY ) )
		fRet = ShowXY3D( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == HWR_XY )
		fRet = ShowXY( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == HWR_XZ )
		fRet = ShowXZ( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == HWR_YZ )
		fRet = ShowYZ( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == HWR_XYt )
		fRet = ShowXYt( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( GraphType == HWR_Zt )
		fRet = ShowZt( pGraph, pGraphDlg, (HWND*)PEHandle );

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowHWR::ShowHWRMultiple(VARIANT *GraphWindow, HWRGraph GraphType, VARIANT *GraphDlg, VARIANT *PEHandle)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;
	if( m_Files.GetCount() == 0 ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	CWnd* pGraphDlg = (CWnd*)GraphDlg;

	BOOL fRet = FALSE;
	if( ( GraphType & HWR_XY ) == HWR_XY )
		fRet = ShowXYMultiple( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( ( GraphType & HWR_XZ ) == HWR_XZ )
		fRet = ShowXZMultiple( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( ( GraphType & HWR_YZ ) == HWR_YZ )
		fRet = ShowYZMultiple( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( ( GraphType & HWR_XYt ) == HWR_XYt )
		fRet = ShowXYtMultiple( pGraph, pGraphDlg, (HWND*)PEHandle );
	else if( ( GraphType & HWR_Zt ) == HWR_Zt )
		fRet = ShowZtMultiple( pGraph, pGraphDlg, (HWND*)PEHandle );

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowHWR::ShowHWRGraphRT(VARIANT *GraphWindow, HWRGraph GraphType,
									 LONG* Cnt, VARIANT *PEHandle )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !GraphWindow ) return E_FAIL;

	if( !m_fInit ) Init();
	if( !m_fInit ) return E_FAIL;

	CWnd* pGraph = (CWnd*)GraphWindow;
	*Cnt = m_nCnt;

	BOOL fRet = FALSE;
	if( ( GraphType & HWR_XY ) == HWR_XY )
		fRet = ShowXYRT( pGraph, (HWND*)PEHandle );

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowHWR::put_HWR_InFile(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_szFile = newVal;
	m_fInit = FALSE;

	return S_OK;
}

STDMETHODIMP ShowHWR::put_Frequency(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dFrequency = newVal;

	return S_OK;
}

BOOL ShowHWR::ShowSingleSetBase( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, double* px, double* py,
								 CString szSubTitle, CString szX, CString szY, HWRGraph gt,
								 BOOL fOnlyColors )
{
	if( !hPE ) return FALSE;
	if( !fOnlyColors && ( !pGraph || !pGraphDlg || !px || !py ) ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// if pen-pressure line thickness create datasets for annotations
	int* pnThickness = NULL;
	DWORD* pdwColor = NULL;
	double *pdPPx = NULL, *pdPPy = NULL;
	if( m_fPPLT )
	{
		pnThickness = new int[ m_nCnt * 2 ];		// must account for possible overrun of data (points + lineto)
		pdwColor = new DWORD[ m_nCnt * 2 ];		// must account for possible overrun of data (points + lineto)
		pdPPx = new double[ m_nCnt * 2 ];			// must account for possible overrun of data (points + lineto)
		memset( pdPPx, 0, ( m_nCnt * 2 ) * sizeof( double ) );
		pdPPy = new double[ m_nCnt * 2 ];			// must account for possible overrun of data (points + lineto)
		memset( pdPPy, 0, ( m_nCnt * 2 ) * sizeof( double ) );
	}

	// Set number of Subsets and Points
	PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );	// number of data points

	// annotation table
	DWORD dwColor;
	dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 80);

	float fX = 0.0, fY = 0.0;
	int nThickness = PELT_THINSOLID, nLastThickness = -1, nACounter = 0;
	for( int p = 0; p < m_nCnt; p++ )
	{
		// line/point color based upon pressure
		if( z[ p ] > ::GetMinPenPressure() )
		{
			dwColor = m_fMonochrome ? MONOCOLOR : LINECOLOR;
		}
		else
		{
//			if( _nLastPenPressure <= ::GetMinPenPressure() )
				dwColor = m_dwPenUpColor;
//			else
//				dwColor = m_fMonochrome ? MONOCOLOR : LINECOLOR;
		}
		_nLastPenPressure = (int)z[ p ];

		// data values for line
		fX = (float)px[ p ];
		if( px[ p ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
		fY = (float)py[ p ];
		if( py[ p ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faXDATA, 0, p, &fX );
		PEvsetcellEx( *hPE, PEP_faYDATA, 0, p, &fY );

		// pen pressure variation
		if( m_fPPLT )
		{
			// draw line to next data point (if not 0)
			if( p != 0 )
			{
				// data points
				pdPPx[ nACounter ] = fX;
				pdPPy[ nACounter ] = fY;
				// thickness
				pnThickness[ nACounter ] = PEGAT_LINECONTINUE;
				// color
				pdwColor[ nACounter ] = RGB( 0, 0, 0 );

				nACounter++;
			}

			// next thickness
			nThickness = ::ScaleLineThickness( this, (int)z[ p ] );	// returns PEGAT line thicknesses
			if( nThickness != nLastThickness )						// when p = 0, this will always be true
			{
				// data points
				pdPPx[ nACounter ] = fX;
				pdPPy[ nACounter ] = fY;
				// thickness
				pnThickness[ nACounter ] = nThickness;
				nLastThickness = nThickness;
				// color
				pdwColor[ nACounter ] = dwColor;

				nACounter++;
			}
		}

		// point color
		if( m_fPPLT ) dwColor = RGB( 0, 0, 0 );
		PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, p, &dwColor );

		// sample/pressure/stroke/slant annotation table
		if( ( p == 0 ) && !fOnlyColors )
		{
			char szTblData[ TBL_DATA_SIZE ];
			int nDeg = 0;
			sprintf_s( szTblData, PETAL_STR, 0, (float)z[ p ] );
			PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
		}
	}

	// if pen pressure variations, set annotations
	if( m_fPPLT )
	{
		// clear out property arrays (?)
		PEvset(*hPE, PEP_faGRAPHANNOTATIONX, NULL, 0);
		PEvset(*hPE, PEP_faGRAPHANNOTATIONY, NULL, 0);
		PEvset(*hPE, PEP_naGRAPHANNOTATIONTYPE, NULL, 0);
		PEvset(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, NULL, 0);

		// set property arrays
		PEvset(*hPE, PEP_faGRAPHANNOTATIONX, pdPPx, nACounter);
		PEvset(*hPE, PEP_faGRAPHANNOTATIONY, pdPPy, nACounter);
		PEvset(*hPE, PEP_naGRAPHANNOTATIONTYPE, pnThickness, nACounter);
		PEvset(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, pdwColor, nACounter);
		PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
	}

	if( !fOnlyColors )
	{
		// LABLES
		char pszText[ 50 ];
		// title
		strcpy_s( pszText, _T("") );
		PEszset(*hPE, PEP_szMAINTITLE, pszText);
		// subtitle
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszText, 50, szTitle );
		PEszset(*hPE, PEP_szSUBTITLE, pszText);
		// x axis
		strcpy_s( pszText, 50, szX );
		PEszset(*hPE, PEP_szXAXISLABEL, pszText);
		// y axis
		strcpy_s( pszText, 50, szY );
		PEszset(*hPE, PEP_szYAXISLABEL, pszText);
		// settings
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// Set defaults
		SetChartDefaults( hPE, gt, !m_fPPLT );
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// in case cursor was already in position
	OnCursorMove( (VARIANT*)hPE, eHWR_XY );

	// Cleanup
	if( pnThickness ) delete [] pnThickness;
	if( pdwColor ) delete [] pdwColor;
	if( pdPPx ) delete [] pdPPx;
	if( pdPPy ) delete [] pdPPy;

	return TRUE;
}

BOOL ShowHWR::ShowSingleSetBaseMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, UINT xaxis, UINT yaxis,
										 CString szSubTitle, CString szX, CString szY,
										 BOOL fOnlyColors )
{
	if( !hPE ) return FALSE;
	if( !fOnlyColors && ( !pGraph || !pGraphDlg ) ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// annotations
	PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
	PEnset(*hPE, PEP_bALLOWANNOTATIONCONTROL, TRUE);
	PEnset(*hPE, PEP_bANNOTATIONSONSURFACES, TRUE);

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// determine the # of total points (ridiculous way to do this, but PE needs it)
	int nCnt = 0, nSubsets = 0;
	POSITION pos = m_Files.GetHeadPosition();
	while( pos )
	{
		// next file
		m_szFile = m_Files.GetNext( pos );
		// initialize this file to get count
		Init();
		if( !m_fInit ) return E_FAIL;
		// increment total count by count of points + extra beginning and end to separate subsets
		nCnt += ( m_nCnt + 2 );
		// increment subset count
		nSubsets++;
	}
	// if no data, exit
	if( nCnt == 0 ) return TRUE;

	// Subsets & Points
	int nSubset = 0;
	PEnset( *hPE, PEP_nSUBSETS, 1 );						// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, nCnt );						// number of data points

	// arrays for the data
	float* fXData = new float[ nCnt ];
	float* fYData = new float[ nCnt ];
	DWORD* dwColors = new DWORD[ nCnt ];
	memset( fXData, 0, nCnt * sizeof( float ) );
	memset( fYData, 0, nCnt * sizeof( float ) );
	memset( dwColors, 0, nCnt * sizeof( DWORD ) );

	// extract data and assign to arrays (need to keep track of where we are in the array)
	int ptr = -1, nAnnPt = 0;
	float fX = 0.0, fY = 0.0;
	DWORD dwColor;
	pos = m_Files.GetHeadPosition();
	while( pos )
	{
		// next file & initialize
		m_szFile = m_Files.GetNext( pos );
		Init();

		for( int q = 0; q < m_nCnt; q++ )
		{
			// increment total count pointer
			ptr++;
			if( ptr >= nCnt ) break;

			// get data points
			if( xaxis == X_DATA )
			{
				fX = (float)x[ q ];
				if( x[ q ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
			}
			else if( xaxis == Y_DATA )
			{
				fX = (float)y[ q ];
				if( y[ q ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
			}
			else if( xaxis == Z_DATA )
			{
				fX = (float)z[ q ];
				if( z[ q ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
			}
			else fX = 0.0;
			if( yaxis == X_DATA )
			{
				fY = (float)x[ q ];
				if( x[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
			}
			else if( yaxis == Y_DATA )
			{
				fY = (float)y[ q ];
				if( y[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
			}
			else if( yaxis == Z_DATA )
			{
				fY = (float)z[ q ];
				if( z[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
			}
			else fY = 0.0;

			// pressure
			if( z[ q ] > ::GetMinPenPressure() )
			{
				dwColor = m_fMonochrome ? MONOCOLOR: ::GetColorByIndex( nSubset, m_Files.GetCount() );
			}
			else
			{
				if( _nLastPenPressure <= ::GetMinPenPressure() )
					dwColor = m_dwPenUpColor;
				else
					dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
			}
			_nLastPenPressure = (int)z[ q ];

			// if after the 1st subset and the 1st point,
			// add the 1st point twice, this time (first) with no color
			if( ( nSubset > 0 ) && ( q == 0 ) )
			{
				fXData[ ptr ] = fX;
				fYData[ ptr ] = fY;
				dwColors[ ptr ] = RGB( 255, 255, 255 );
				ptr++;
			}

			// annotations for 1st point of each subset
			if( q == 0 )
			{
				int nSymbol = PEGAT_DOT;
				double dX = fX;
				double dY = fY;
				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONX, nAnnPt, &dX);
				PEvsetcell(*hPE, PEP_faGRAPHANNOTATIONY, nAnnPt, &dY);
				PEvsetcell(*hPE, PEP_naGRAPHANNOTATIONTYPE, nAnnPt, &nSymbol);
				PEvsetcell(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, nAnnPt, &dwColor);
				nAnnPt++;
			}

			// set arrays
			fXData[ ptr ] = fX;
			fYData[ ptr ] = fY;
			dwColors[ ptr ] = dwColor;

			// if not the last subset and the last point,
			// add the last point again with no color
			if( ( nSubset < ( nSubsets - 1 ) ) && ( q == ( m_nCnt - 1 ) ) )
			{
				ptr++;
				fXData[ ptr ] = fX;
				fYData[ ptr ] = fY;
				dwColors[ ptr ] = RGB( 255, 255, 255 );
			}
		}

		nSubset++;
	}

	// for any missing points not calculated, set them to null value with no color
	int nMissing = nCnt - ptr;
	for( int q = 1; q < nMissing; q++ )
	{
		ptr++;
		fXData[ ptr ] = NULLDATAVALUE;
		fYData[ ptr ] = NULLDATAVALUE;
		dwColors[ ptr ] = RGB( 255, 255, 255 );
	}

	// send arrays to PE
	PEvset( *hPE, PEP_faXDATA, fXData, nCnt );
	PEvset( *hPE, PEP_faYDATA, fYData, nCnt );
	PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, nCnt );

	if( !fOnlyColors )
	{
		// LABLES
		char pszText[ 50 ];
		// title
		strcpy_s( pszText, _T("") );
		PEszset(*hPE, PEP_szMAINTITLE, pszText);
		// subtitle
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszText, 50, szTitle );
		PEszset(*hPE, PEP_szSUBTITLE, "X Coordinate vs Y Coordinate");
		// x axis
		strcpy_s( pszText, 50, szX );
		PEszset(*hPE, PEP_szXAXISLABEL, "X Coordinate");
		// y axis
		strcpy_s( pszText, 50, szY );
		PEszset(*hPE, PEP_szYAXISLABEL, "Y Coordinate");
		// settings
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// No legend (subsets)
		int nNoLegends = -1;
		PEvset(*hPE, PEP_naSUBSETSTOLEGEND, &nNoLegends, 1);

		// Set defaults
		SetChartDefaults( hPE, (HWRGraph)0, TRUE );
	}

    // Set property PEP_nSPEEDBOOST higher, default is 3
    PEnset( *hPE, PEP_nSPEEDBOOST, 50 );

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	// Cleanup
	if( fXData ) delete [] fXData;
	if( fYData ) delete [] fYData;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowHWR::ShowXY3D( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors, BOOL fInverted )
{
	double *px = x, *py = y, *pz = z;
	HWRGraph gt = (HWRGraph)HWR_XY_3D;

	if( !hPE ) return FALSE;
	if( !fOnlyColors && ( !pGraph || !pGraphDlg || !px || !py ) ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg, PECONTROL_3D ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// Set number of Subsets and Points
	DWORD dwColor = 0;
	PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );	// number of data points

	float fX = 0.0, fY = 0.0, fZ = 0.0;
	int s = 0;
	for( int p = 0; p < m_nCnt; p++ )
	{
		// don't use sorted values (instructions say to use sorted, but plot by line does not work)
		fX = (float)px[ p ];
		if( px[ p ] == ::GetMissingDataValue() ) fX = NULLDATAVALUE;
		fY = (float)py[ p ];
		if( py[ p ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
		fZ = (float)pz[ p ];
		if( pz[ p ] == ::GetMissingDataValue() ) fZ = NULLDATAVALUE;
		PEvsetcellEx( *hPE, PEP_faXDATA, s, p, &fX );
		if( m_fInverted )
		{
			PEvsetcellEx( *hPE, PEP_faYDATA, s, p, &fY );
			PEvsetcellEx( *hPE, PEP_faZDATA, s, p, &fZ );
		}
		else
		{
			PEvsetcellEx( *hPE, PEP_faYDATA, s, p, &fZ );
			PEvsetcellEx( *hPE, PEP_faZDATA, s, p, &fY );
		}

		// point color
		dwColor = m_fMonochrome ? MONOCOLOR : LINECOLOR;
		PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, s, p, &dwColor );
	}

	if( !fOnlyColors || fInverted )
	{
		// LABLES
		CString szX = _T("X Coordinate");
		CString szY = _T("Y Coordinate");
		CString szZ = _T("Pen Pressure");
		char pszText[ 50 ];
		// title
		strcpy_s( pszText, _T("") );
		PEszset(*hPE, PEP_szMAINTITLE, pszText);
		// subtitle
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszText, 50, szTitle );
		PEszset(*hPE, PEP_szSUBTITLE, pszText);
		// x axis
		strcpy_s( pszText, 50, szX );
		PEszset(*hPE, PEP_szXAXISLABEL, pszText);
		// y axis
		if( m_fInverted ) strcpy_s( pszText, 50, szY );
		else strcpy_s( pszText, 50, szZ );
		PEszset(*hPE, PEP_szYAXISLABEL, pszText);
		// z axis
		if( m_fInverted ) strcpy_s( pszText, 50, szZ );
		else strcpy_s( pszText, 50, szY );
		PEszset(*hPE, PEP_szZAXISLABEL, pszText);

		// settings
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// Set defaults
		SetChartDefaults( hPE, gt, !m_fPPLT );

		// reset line thickness
		int nLineType = PELT_THINSOLID;
		PEvset(*hPE, PEP_naSUBSETLINETYPES, &nLineType, 1);
		// Non-Surface chart, set PolyMode //
		PEnset(*hPE, PEP_nPOLYMODE, PEPM_SCATTER);
		// Set camera position //
		if( m_fInverted )
		{
			PEnset(*hPE, PEP_nVIEWINGHEIGHT, 15);
			PEnset(*hPE, PEP_nDEGREEOFROTATION, 180);
		}
		else
		{
			PEnset(*hPE, PEP_nVIEWINGHEIGHT, 15);
			PEnset(*hPE, PEP_nDEGREEOFROTATION, 145);
		}
		// Set Various Other Properties //
		PEnset(*hPE, PEP_bBITMAPGRADIENTMODE, FALSE);
		// Set Plotting methods //
		PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEPLM_SURFACE);   // ' Points
		PEnset(*hPE, PEP_bCACHEBMP, TRUE);
		PEnset(*hPE, PEP_nROTATIONDETAIL, PERD_FULLDETAIL);
		PEnset(*hPE, PEP_nROTATIONSPEED, 50);
		PEnset(*hPE, PEP_nROTATIONINCREMENT, PERI_INCBY1);
		PEnset(*hPE, PEP_bSHOWLEGEND, FALSE);
		// Setting these helps improve and printing and metafile exports //
		PEnset(*hPE, PEP_nDPIX, 600);
		PEnset(*hPE, PEP_nDPIY, 600);
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	return TRUE;
}

BOOL ShowHWR::ShowXY( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowSingleSetBase( pGraph, pGraphDlg, hPE, x, y,
							  _T("X Coordinate vs Y Coordinate"),
							  _T("X Coordinate"),
							  _T("Y Coordinate"),
							  (HWRGraph)HWR_XY,
							  fOnlyColors );
}

BOOL ShowHWR::ShowXYMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowSingleSetBaseMultiple( pGraph, pGraphDlg, hPE, X_DATA, Y_DATA,
									 _T("X Coordinate vs Y Coordinate"),
									 _T("X Coordinate"),
									 _T("Y Coordinate"),
									 fOnlyColors );
}

BOOL ShowHWR::ShowXZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowSingleSetBase( pGraph, pGraphDlg, hPE, x, z,
							  _T("X Coordinate vs Z Coordinate"),
							  _T("X Coordinate"),
							  _T("Z Coordinate"),
							  (HWRGraph)HWR_XZ,
							  fOnlyColors );
}

BOOL ShowHWR::ShowXZMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowSingleSetBaseMultiple( pGraph, pGraphDlg, hPE, X_DATA, Z_DATA,
									 _T("X Coordinate vs Z Coordinate"),
									 _T("X Coordinate"),
									 _T("Z Coordinate"),
									 fOnlyColors );
}

BOOL ShowHWR::ShowYZ( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowSingleSetBase( pGraph, pGraphDlg, hPE, y, z,
							  _T("Y Coordinate vs Z Coordinate"),
							  _T("Y Coordinate"),
							  _T("Z Coordinate"),
							  (HWRGraph)HWR_YZ,
							  fOnlyColors );
}

BOOL ShowHWR::ShowYZMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	return ShowSingleSetBaseMultiple( pGraph, pGraphDlg, hPE, Y_DATA, Z_DATA,
									 _T("Y Coordinate vs Z Coordinate"),
									 _T("Y Coordinate"),
									 _T("Z Coordinate"),
									 fOnlyColors );
}

BOOL ShowHWR::ShowXYt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// Set number of Subsets and Points //
	// if pen-pressure line thickness create datasets for annotations
	// since we have 2 subsets, we double the size of the arrays
	int* pnThickness = NULL;
	DWORD* pdwColor = NULL;
	double *pdPPx = NULL, *pdPPy = NULL;
	if( m_fPPLT )
	{
		PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
		PEnset( *hPE, PEP_nPOINTS, 1 );			// number of data points
		pnThickness = new int[ m_nCnt * 4 ];		// must account for possible overrun of data (points + lineto)
		pdwColor = new DWORD[ m_nCnt * 4 ];		// must account for possible overrun of data (points + lineto)
		pdPPx = new double[ m_nCnt * 4 ];			// must account for possible overrun of data (points + lineto)
		memset( pdPPx, 0, ( m_nCnt * 4 ) * sizeof( double ) );
		pdPPy = new double[ m_nCnt * 4 ];			// must account for possible overrun of data (points + lineto)
		memset( pdPPy, 0, ( m_nCnt * 4 ) * sizeof( double ) );
	}
	else
	{
		PEnset(*hPE, PEP_nSUBSETS, 2);		// set number of subsets
		PEnset(*hPE, PEP_nPOINTS, m_nCnt);	// number of data points
	}

	// annotation table
	DWORD dwColor;
	dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 80);

	float fX = 0.0, fY = 0.0, fS = 0.0;
	int nThickness = PELT_THINSOLID, nLastThickness = -1, nACounter = 0;
	if( !m_fUNR )
	{
		for( int p = 0; p <= 1; p++ )
		{
			_nLastPenPressure = 0;
			nLastThickness = -1;

			for( int q = 0; q < m_nCnt; q++ )
			{
				// pen pressure variation
				if( m_fPPLT )
				{
					fX = (float)q;
					if( p == 0 )
					{
						fY = (float)x[ q ];
						if( x[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
					}
					else
					{
						fY = (float)y[ q ];
						if( y[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
					}

					// line/point color based upon pressure
					if( z[ q ] > ::GetMinPenPressure() )
					{
						dwColor = m_fMonochrome ? MONOCOLOR :
												  ( p == 0 ) ? LINE2COLOR : LINECOLOR;
					}
					else
					{
						if( _nLastPenPressure <= ::GetMinPenPressure() )
							dwColor = m_dwPenUpColor;
						else
							dwColor = m_fMonochrome ? MONOCOLOR :
													( p == 0 ) ? LINE2COLOR : LINECOLOR;
					}
					_nLastPenPressure = (int)z[ q ];

					// draw line to next data point (if not 0)
					if( q != 0 )
					{
						// data points
						pdPPx[ nACounter ] = fX;
						pdPPy[ nACounter ] = fY;
						// thickness
						pnThickness[ nACounter ] = PEGAT_LINECONTINUE;
						// color
						pdwColor[ nACounter ] = RGB( 0, 0, 0 );

						nACounter++;
					}

					// next thickness
					nThickness = ::ScaleLineThickness( this, (int)z[ q ] );	// returns PEGAT line thicknesses
					if( nThickness != nLastThickness )					// when p = 0, this will always be true
					{
						// data points
						pdPPx[ nACounter ] = fX;
						pdPPy[ nACounter ] = fY;
						// thickness
						pnThickness[ nACounter ] = nThickness;
						nLastThickness = nThickness;
						// color
						pdwColor[ nACounter ] = dwColor;

						nACounter++;
					}
				}
				// normal method
				else
				{
					fX = (float)q;
					fY = (float)x[ q ];
					PEvsetcellEx( *hPE, PEP_faXDATA, 0, q, &fX );
					PEvsetcellEx (*hPE, PEP_faYDATA, 0, q, &fY );

					// line/point color based upon pressure
					if( z[ q ] > ::GetMinPenPressure() )
					{
						dwColor = m_fMonochrome ? MONOCOLOR: LINE2COLOR;
					}
					else
					{
						if( _nLastPenPressure <= ::GetMinPenPressure() )
							dwColor = m_dwPenUpColor;
						else
							dwColor = m_fMonochrome ? MONOCOLOR: LINE2COLOR;
					}
					_nLastPenPressure = (int)z[ q ];
					PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, q, &dwColor );

					fY = (float)y[ q ];
					PEvsetcellEx( *hPE, PEP_faXDATA, 1, q, &fX );
					PEvsetcellEx (*hPE, PEP_faYDATA, 1, q, &fY );

					// line/point color based upon pressure
					if( z[ q ] > ::GetMinPenPressure() )
					{
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
					}
					else
					{
						if( _nLastPenPressure <= ::GetMinPenPressure() )
							dwColor = m_dwPenUpColor;
						else
							dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
					}
					_nLastPenPressure = (int)z[ q ];
					PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 1, q, &dwColor );
				}

				// sample/pressure/stroke/slant annotation table
				if( ( q == 0 ) && !fOnlyColors )
				{
					int nDeg = (int)( fS * 180 / PI );
					char szTblData[ TBL_DATA_SIZE ];
					sprintf_s( szTblData, PETAL_STR, 0, (float)z[ p ] );
					PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
				}
			}
		}
	}
	else
	{
		for( int q = 0; q < m_nCnt2; q++ )
		{	
			// pen pressure variation
			if( m_fPPLT )
			{
			}
			// normal method
			else
			{
				fX = (float)q;
				fY = (float)ux[ q ];
				if( ux[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
				PEvsetcellEx( *hPE, PEP_faXDATA, 0, q, &fX );
				PEvsetcellEx (*hPE, PEP_faYDATA, 0, q, &fY );

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: LINE2COLOR;
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];
				PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, q, &dwColor );

				fY = (float)uy[ q ];
				PEvsetcellEx( *hPE, PEP_faXDATA, 1, q, &fX );
				PEvsetcellEx (*hPE, PEP_faYDATA, 1, q, &fY );

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];
				PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 1, q, &dwColor );
			}

			// sample/pressure/stroke/slant annotation table
			if( ( q == 0 ) && !fOnlyColors )
			{
				int nDeg = (int)( fS * 180 / PI );
				char szTblData[ TBL_DATA_SIZE ];
				sprintf_s( szTblData, PETAL_STR, 0, (float)z[ q ] );
				PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
			}
		}
	}

	// if pen pressure variations, set annotations
	if( m_fPPLT )
	{
		PEvset(*hPE, PEP_faGRAPHANNOTATIONX, pdPPx, nACounter);
		PEvset(*hPE, PEP_faGRAPHANNOTATIONY, pdPPy, nACounter);
		PEvset(*hPE, PEP_naGRAPHANNOTATIONTYPE, pnThickness, nACounter);
		PEvset(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, pdwColor, nACounter);
		PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
	}

	if( !fOnlyColors )
	{
		// labels
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		char pszTitle[ 50 ], pszSub[ 50 ], pszY[ 50 ], pszX[ 50 ];
		CString szSub, szY, szX;
		if( !m_fUNR )
		{
			szSub = _T("X AND Y vs Sample Number");
			szY.LoadString( IDS_HWR_Y1 );
			szX.LoadString( IDS_HWR_X1 );
		}
		else
		{
			szSub = _T("X AND Y vs Device Units");
			szX = _T("Sample Number");
			szY = _T("Device Units");
		}
		strcpy_s( pszTitle, _T("") );
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszSub, 50, szTitle );
		strcpy_s( pszY, 50, szY );
		strcpy_s( pszX, 50, szX );

		PEszset(*hPE, PEP_szMAINTITLE, pszTitle);
		PEszset(*hPE, PEP_szSUBTITLE, pszSub);
		PEszset(*hPE, PEP_szYAXISLABEL, pszY);
		PEszset(*hPE, PEP_szXAXISLABEL, pszX);
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// subset labels
		if( !m_fUNR )
		{
			PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 0, "X Coordinate" );
			PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 1, "Y Coordinate" );
		}
		else
		{
			PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 0, "Departure from Straight Line" );
			PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 1, "Distance between Samples" );
		}
			
		// Set defaults
		SetChartDefaults( hPE, (HWRGraph)HWR_XYt, !m_fPPLT );

		// line sizes (FALSE param 2 above)
		int linesizes[ 2 ] = { PELT_THINSOLID, PELT_THINSOLID };
		PEvset(*hPE, PEP_naSUBSETLINETYPES, linesizes, 2);
	}
	// subset label colors
	DWORD dwArray[ 2 ];
	if( m_fMonochrome )
	{
		dwArray[ 0 ] = MONOCOLOR;
		dwArray[ 1 ] = MONOCOLOR;
	}
	else
	{
		dwArray[ 0 ] = LINE2COLOR;
		dwArray[ 1 ] = LINECOLOR;
	}
	PEvsetEx( *hPE, PEP_dwaSUBSETCOLORS, 0, 2, dwArray, 0 );

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	// in case cursor was already in position
	OnCursorMove( (VARIANT*)hPE, eHWR_XY );

	// Cleanup
	if( pnThickness ) delete [] pnThickness;
	if( pdwColor ) delete [] pdwColor;
	if( pdPPx ) delete [] pdPPx;
	if( pdPPy ) delete [] pdPPy;

	return TRUE;
}

BOOL ShowHWR::ShowXYtMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	if( !hPE ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// determine the # of total points (ridiculous way to do this, but PE needs it)
	int nCnt = 0;
	POSITION pos = m_Files.GetHeadPosition();
	while( pos )
	{
		m_szFile = m_Files.GetNext( pos );
		Init();
		if( !m_fInit ) return E_FAIL;
		nCnt += m_nCnt;
	}
	if( nCnt == 0 ) return TRUE;

	// Subsets and Points
	int nSubset = 0;
	PEnset( *hPE, PEP_nSUBSETS, 2 );						// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, nCnt );						// number of data points

	// arrays for the data
	float* fXData = new float[ nCnt * 2 ];
	float* fYData = new float[ nCnt * 2 ];
	DWORD* dwColors = new DWORD[ nCnt * 2 ];
	memset( fXData, 0, nCnt * 2 * sizeof( float ) );
	memset( fYData, 0, nCnt * 2 * sizeof( float ) );
	memset( dwColors, 0, nCnt * 2 * sizeof( DWORD ) );

	// extract data and assign to arrays (need to keep track of where we are in the array)
	int ptr = -1;
	float fX = 0.0, fY = 0.0;
	DWORD dwColor;
	pos = m_Files.GetHeadPosition();
	while( pos )
	{
		m_szFile = m_Files.GetNext( pos );
		Init();

		if( !m_fUNR )
		{
			for( int q = 0; q < m_nCnt; q++ )
			{
				ptr++;
				if( ptr >= nCnt ) break;

				fX = (float)q;
				fXData[ ptr ] = fX;
				fY = (float)x[ q ];
				if( x[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
				fYData[ ptr ] = fY;

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: LINE2COLOR;
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];
				dwColors[ ptr ] = dwColor;

				fXData[ ptr + nCnt ] = fX;
				fY = (float)y[ q ];
				if( y[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
				fYData[ ptr + nCnt ] = fY;

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];
				dwColors[ ptr + nCnt ] = dwColor;
			}
		}
		else
		{
			for( int q = 0; q < m_nCnt2; q++ )
			{
				ptr++;
				if( ptr >= nCnt ) break;

				fX = (float)q;
				fXData[ ptr ] = fX;
				fY = (float)ux[ q ];
				if( ux[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
				fYData[ ptr ] = fY;

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: LINE2COLOR;
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];
				dwColors[ ptr ] = dwColor;

				fXData[ ptr + nCnt ] = fX;
				fY = (float)uy[ q ];
				if( uy[ q ] == ::GetMissingDataValue() ) fY = NULLDATAVALUE;
				fYData[ ptr + nCnt ] = fY;

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];
				dwColors[ ptr + nCnt ] = dwColor;
			}
		}

		nSubset++;
	}

	// send arrays to PE
    PEvset( *hPE, PEP_faXDATA, fXData, nCnt * 2 );
    PEvset( *hPE, PEP_faYDATA, fYData, nCnt * 2 );
	PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, nCnt * 2 );

	if( !fOnlyColors )
	{
		// labels
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		char pszTitle[ 50 ], pszSub[ 50 ], pszY[ 50 ], pszX[ 50 ];
		CString szSub, szY, szX;
		if( !m_fUNR )
		{
			szSub = _T("X AND Y vs Sample Number");
			szY.LoadString( IDS_HWR_Y1 );
			szX.LoadString( IDS_HWR_X1 );
		}
		else
		{
			szSub = _T("X AND Y vs Digitizer Units");
			szY = _T("Spacing Between Successive Samples");
			szX = _T("Departure From A Straight Line");
		}
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszTitle, _T("") );
		strcpy_s( pszSub, 50, szTitle );
		strcpy_s( pszY, 50, szY );
		strcpy_s( pszX, 50, szX );

		PEszset(*hPE, PEP_szMAINTITLE, pszTitle);
		PEszset(*hPE, PEP_szSUBTITLE, pszSub);
		PEszset(*hPE, PEP_szYAXISLABEL, pszY);
		PEszset(*hPE, PEP_szXAXISLABEL, pszX);
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// No legend (subsets)
		int nLegends[] = { 0, 1 };
		PEvset(*hPE, PEP_naSUBSETSTOLEGEND, nLegends, 2);

		// subset labels
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 0, "X Coordinate" );
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 1, "Y Coordinate" );
			
		// Set defaults
		SetChartDefaults( hPE, (HWRGraph)0, TRUE );
	}

    // Set property PEP_nSPEEDBOOST higher, default is 3
    PEnset( *hPE, PEP_nSPEEDBOOST, 50 );

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	// Cleanup
	if( fXData ) delete [] fXData;
	if( fYData ) delete [] fYData;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowHWR::ShowZt( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	if( !hPE ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// if pen-pressure line thickness create datasets for annotations
	int* pnThickness = NULL;
	DWORD* pdwColor = NULL;
	double *pdPPx = NULL, *pdPPy = NULL;
	if( m_fPPLT )
	{
		pnThickness = new int[ m_nCnt * 2 ];		// must account for possible overrun of data (points + lineto)
		pdwColor = new DWORD[ m_nCnt * 2 ];			// must account for possible overrun of data (points + lineto)
		pdPPx = new double[ m_nCnt * 2 ];			// must account for possible overrun of data (points + lineto)
		memset( pdPPx, 0, ( m_nCnt * 2 ) * sizeof( double ) );
		pdPPy = new double[ m_nCnt * 2 ];			// must account for possible overrun of data (points + lineto)
		memset( pdPPy, 0, ( m_nCnt * 2 ) * sizeof( double ) );
	}

	// Set number of Subsets and Points
	PEnset(*hPE, PEP_nSUBSETS, 1);					// set number of subsets
	PEnset(*hPE, PEP_nPOINTS, m_nCnt);				// number of data points

	// annotation table
	DWORD dwColor;
	dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 80);

	float fX = 0.0, fY = 0.0, fS = 0.0;
	int nThickness = PELT_THINSOLID, nLastThickness = -1, nACounter = 0;
	if( !m_fUNR )
	{
		for( int q = 0; q < m_nCnt; q++ )
		{
			// line/point color based upon pressure
			if( z[ q ] > ::GetMinPenPressure() )
			{
				dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
			}
			else
			{
				if( _nLastPenPressure <= ::GetMinPenPressure() )
					dwColor = m_dwPenUpColor;
				else
					dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
			}
			_nLastPenPressure = (int)z[ q ];

			// data values for line
			fX = (float)q;
			fY = (float)z[ q ];
			PEvsetcellEx( *hPE, PEP_faXDATA, 0, q, &fX );
			PEvsetcellEx (*hPE, PEP_faYDATA, 0, q, &fY );

			// sample/pressure/stroke/slant annotation table
			if( ( q == 0 ) && !fOnlyColors )
			{
				int nDeg = (int)( fS * 180 / PI );
				char szTblData[ TBL_DATA_SIZE ];
				sprintf_s( szTblData, PETAL_STR, 0, (float)z[ q ] );
				PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
			}

			// pen pressure variation
			if( m_fPPLT )
			{
				// draw line to next data point (if not 0)
				if( q != 0 )
				{
					// data points
					pdPPx[ nACounter ] = fX;
					pdPPy[ nACounter ] = fY;
					// thickness
					pnThickness[ nACounter ] = PEGAT_LINECONTINUE;
					// color
					pdwColor[ nACounter ] = RGB( 0, 0, 0 );

					nACounter++;
				}

				// next thickness
				nThickness = ::ScaleLineThickness( this, (int)z[ q ] );	// returns PEGAT line thicknesses
				if( nThickness != nLastThickness )					// when p = 0, this will always be true
				{
					// data points
					pdPPx[ nACounter ] = fX;
					pdPPy[ nACounter ] = fY;
					// thickness
					pnThickness[ nACounter ] = nThickness;
					nLastThickness = nThickness;
					// color
					pdwColor[ nACounter ] = dwColor;

					nACounter++;
				}
			}

			// point color
			if( m_fPPLT ) dwColor = RGB( 0, 0, 0 );
			PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, q, &dwColor );
		}
	}
	else
	{
		for( int q = 0; q < m_nCnt2; q++ )
		{
			// line/point color based upon pressure
			if( z[ q ] > ::GetMinPenPressure() )
			{
				dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
			}
			else
			{
				if( _nLastPenPressure <= ::GetMinPenPressure() )
					dwColor = m_dwPenUpColor;
				else
					dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
			}
			_nLastPenPressure = (int)z[ q ];

			// data values for line
			fX = (float)q;
			fY = (float)uz[ q ];
			PEvsetcellEx( *hPE, PEP_faXDATA, 0, q, &fX );
			PEvsetcellEx (*hPE, PEP_faYDATA, 0, q, &fY );

			// sample/pressure/stroke/slant annotation table
			if( ( q == 0 ) && !fOnlyColors )
			{
				int nDeg = (int)( fS * 180 / PI );
				char szTblData[ TBL_DATA_SIZE ];
				sprintf_s( szTblData, PETAL_STR, 0, (float)z[ q ] );
				PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
			}

			// pen pressure variation
			if( m_fPPLT )
			{
				// draw line to next data point (if not 0)
				if( q != 0 )
				{
					// data points
					pdPPx[ nACounter ] = fX;
					pdPPy[ nACounter ] = fY;
					// thickness
					pnThickness[ nACounter ] = PEGAT_LINECONTINUE;
					// color
					pdwColor[ nACounter ] = RGB( 0, 0, 0 );

					nACounter++;
				}

				// next thickness
				nThickness = ::ScaleLineThickness( this, (int)z[ q ] );	// returns PEGAT line thicknesses
				if( nThickness != nLastThickness )					// when p = 0, this will always be true
				{
					// data points
					pdPPx[ nACounter ] = fX;
					pdPPy[ nACounter ] = fY;
					// thickness
					pnThickness[ nACounter ] = nThickness;
					nLastThickness = nThickness;
					// color
					pdwColor[ nACounter ] = dwColor;

					nACounter++;
				}
			}

			// point color
			if( m_fPPLT ) dwColor = RGB( 0, 0, 0 );
			PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, q, &dwColor );
		}
	}

	// if pen pressure variations, set annotations
	if( m_fPPLT )
	{
		PEvset(*hPE, PEP_faGRAPHANNOTATIONX, pdPPx, nACounter);
		PEvset(*hPE, PEP_faGRAPHANNOTATIONY, pdPPy, nACounter);
		PEvset(*hPE, PEP_naGRAPHANNOTATIONTYPE, pnThickness, nACounter);
		PEvset(*hPE, PEP_dwaGRAPHANNOTATIONCOLOR, pdwColor, nACounter);
		PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
	}

	if( !fOnlyColors )
	{
		// labels
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		char pszTitle[ 50 ], pszSub[ 50 ], pszY[ 50 ], pszX[ 50 ];
		CString szSub, szY, szX;
		if( !m_fUNR )
		{
			szSub.LoadString( IDS_HWR_SUB2 );
			szY.LoadString( IDS_HWR_Y2 );
			szX.LoadString( IDS_HWR_X1 );
		}
		else
		{
			szSub = _T("Z Coordinate vs Digitizer Units");
			szX = _T("Sample Number");
			szY = _T("Missing Sample Estimator");
		}
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszTitle, _T("") );
		strcpy_s( pszSub, 50, szTitle );
		strcpy_s( pszY, 50, szY );
		strcpy_s( pszX, 50, szX );

		PEszset(*hPE, PEP_szMAINTITLE, pszTitle);
		PEszset(*hPE, PEP_szSUBTITLE, pszSub);
		PEszset(*hPE, PEP_szYAXISLABEL, pszY);
		PEszset(*hPE, PEP_szXAXISLABEL, pszX);
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// subset labels
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 0, "z(t)" );
			
		// Set defaults
		SetChartDefaults( hPE, (HWRGraph)HWR_Zt, !m_fPPLT );
	}

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	// in case cursor was already in position
	OnCursorMove( (VARIANT*)hPE, eHWR_XY );

	// Cleanup
	if( pnThickness ) delete [] pnThickness;
	if( pdwColor ) delete [] pdwColor;
	if( pdPPx ) delete [] pdPPx;
	if( pdPPy ) delete [] pdPPy;

	return TRUE;
}

BOOL ShowHWR::ShowZtMultiple( CWnd* pGraph, CWnd* pGraphDlg, HWND* hPE, BOOL fOnlyColors )
{
	if( !hPE ) return FALSE;

	// Initialize
	if( !fOnlyColors && !InitChart( hPE, pGraph, pGraphDlg ) ) return FALSE;

	// monochrome or color?
	if( m_fMonochrome ) PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_MONO);
	else PEnset(*hPE, PEP_nVIEWINGSTYLE, PEVS_COLOR);

	// determine the # of total points (ridiculous way to do this, but PE needs it)
	int nCnt = 0;
	POSITION pos = m_Files.GetHeadPosition();
	while( pos )
	{
		m_szFile = m_Files.GetNext( pos );
		Init();
		if( !m_fInit ) return E_FAIL;
		nCnt += m_nCnt;
	}
	if( nCnt == 0 ) return TRUE;

	// Subsets & Points
	int nSubset = 0;
	PEnset( *hPE, PEP_nSUBSETS, 1 );						// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, nCnt );						// number of data points

	// arrays for the data
	float* fXData = new float[ nCnt ];
	float* fYData = new float[ nCnt ];
	DWORD* dwColors = new DWORD[ nCnt ];
	memset( fXData, 0, nCnt * sizeof( float ) );
	memset( fYData, 0, nCnt * sizeof( float ) );
	memset( dwColors, 0, nCnt * sizeof( DWORD ) );

	// extract data and assign to arrays (need to keep track of where we are in the array)
	int ptr = -1;
	float fX = 0.0, fY = 0.0;
	DWORD dwColor;
	pos = m_Files.GetHeadPosition();
	while( pos )
	{
		m_szFile = m_Files.GetNext( pos );
		Init();

		if( !m_fUNR )
		{
			for( int q = 0; q < m_nCnt; q++ )
			{
				ptr++;
				if( ptr>= nCnt ) break;

				fX = (float)q;
				fY = (float)z[ q ];

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: ::GetColorByIndex( nSubset, m_Files.GetCount() );
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];

				// set arrays
				fXData[ ptr ] = fX;
				fYData[ ptr ] = fY;
				dwColors[ ptr ] = dwColor;
			}
		}
		else
		{
			for( int q = 0; q < m_nCnt2; q++ )
			{
				ptr++;
				if( ptr >= nCnt ) break;

				fX = (float)q;
				fY = (float)uz[ q ];

				if( z[ q ] > ::GetMinPenPressure() )
				{
					dwColor = m_fMonochrome ? MONOCOLOR: ::GetColorByIndex( nSubset, m_Files.GetCount() );
				}
				else
				{
					if( _nLastPenPressure <= ::GetMinPenPressure() )
						dwColor = m_dwPenUpColor;
					else
						dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
				}
				_nLastPenPressure = (int)z[ q ];

				// set array
				fXData[ ptr ] = fX;
				fYData[ ptr ] = fY;
				dwColors[ ptr ] = dwColor;
			}
		}

		nSubset++;
	}

	// send arrays to PE
	PEvset( *hPE, PEP_faXDATA, fXData, nCnt );
	PEvset( *hPE, PEP_faYDATA, fYData, nCnt );
	PEvset( *hPE, PEP_dwaPOINTCOLORS, dwColors, nCnt );

	if( !fOnlyColors )
	{
		// labels
		int nPos = m_szFile.ReverseFind( '\\' );
		CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
		char pszTitle[ 50 ], pszSub[ 50 ], pszY[ 50 ], pszX[ 50 ];
		CString szSub, szY, szX;
		if( !m_fUNR )
		{
			szSub.LoadString( IDS_HWR_SUB2 );
			szY.LoadString( IDS_HWR_Y2 );
			szX.LoadString( IDS_HWR_X1 );
		}
		else
		{
			szSub = _T("Z Coordinate vs Digitizer Units");
			szX = _T("Digitizer Unit");
			szY = _T("Number of Consecutive Samples Missing Estimator");
		}
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
		strcpy_s( pszTitle, _T("") );
		strcpy_s( pszSub, 50, szTitle );
		strcpy_s( pszY, 50, szY );
		strcpy_s( pszX, 50, szX );

		PEszset(*hPE, PEP_szMAINTITLE, pszTitle);
		PEszset(*hPE, PEP_szSUBTITLE, pszSub);
		PEszset(*hPE, PEP_szYAXISLABEL, pszY);
		PEszset(*hPE, PEP_szXAXISLABEL, pszX);
		PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
		PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
		PEszset(*hPE, PEP_szLABELFONT, "Arial");
		PEszset(*hPE, PEP_szTABLEFONT, "Arial");

		// No legend (subsets)
		int nNoLegends = -1;
		PEvset(*hPE, PEP_naSUBSETSTOLEGEND, &nNoLegends, 1);

		// subset labels
		PEvsetcell( *hPE, PEP_szaSUBSETLABELS, 0, "z(t)" );
			
		// Set defaults
		SetChartDefaults( hPE, (HWRGraph)0, TRUE );
	}

	// Set property PEP_nSPEEDBOOST higher, default is 3
	PEnset( *hPE, PEP_nSPEEDBOOST, 50 );

	// Draw chart
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	// cleanup
	if( fXData ) delete [] fXData;
	if( fYData ) delete [] fYData;
	if( dwColors ) delete [] dwColors;

	return TRUE;
}

BOOL ShowHWR::ShowXYRT( CWnd* pGraph, HWND* hPE )
{
	// Initialize
	if( !InitChart( hPE, pGraph ) ) return FALSE;

	// Set number of Subsets and Points //
	PEnset( *hPE, PEP_nSUBSETS, 1 );		// set number of subsets
	PEnset( *hPE, PEP_nPOINTS, m_nCnt );		// number of data points

    //** Show Annotations **
    PEnset(*hPE, PEP_bSHOWANNOTATIONS, TRUE);
 
	// annotation table
	DWORD dwColor;
	dwColor = RGB( 0, 0, 0 );
	PEnset(*hPE, PEP_nWORKINGTABLE, 0);
	PEnset(*hPE, PEP_nTAROWS, 1);
	PEnset(*hPE, PEP_nTACOLUMNS, 1);
	PEvsetcellEx(*hPE, PEP_dwaTACOLOR, 0, 0, &dwColor );
	int nTACW = 25;
	PEvsetcell(*hPE, PEP_naTACOLUMNWIDTH, 0, &nTACW);
	PEnset(*hPE, PEP_nTALOCATION, PETAL_LOC);
	PEnset(*hPE, PEP_bSHOWTABLEANNOTATION, TRUE);
	PEnset(*hPE, PEP_nTABORDER, PETAB_NO_BORDER);
	PEnset(*hPE, PEP_dwTABACKCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwTAFORECOLOR, RGB(0,0,0));
	PEnset(*hPE, PEP_nTAHEADERROWS, 0);
	PEnset(*hPE, PEP_nTATEXTSIZE, 80);

	//** Clear out default data **
    float val = 0.0;
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 0, &val);
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 1, &val);
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 2, &val);
	PEvsetcellEx(*hPE, PEP_faXDATA, 0, 3, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 0, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 1, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 2, &val);
	PEvsetcellEx(*hPE, PEP_faYDATA, 0, 3, &val);

	// Set colors
	PEnset(*hPE, PEP_dwGRAPHFORECOLOR, FORECOLOR);
	PEnset(*hPE, PEP_dwSHADOWCOLOR, BGCOLOR);
	PEnset(*hPE, PEP_dwMONOSHADOWCOLOR, BGCOLOR);

	// Set DataShadows to show 3D 
	PEnset(*hPE, PEP_nDATASHADOWS, PEDS_NONE);

	int nPos = m_szFile.ReverseFind( '\\' );
	CString szTitle = ( nPos != -1 ) ? m_szFile.Mid( nPos + 1 ) : m_szFile;
	char pszTitle[ 50 ];
		// 08Oct03 - We are not using the subtitle now...main title as subtitle (no main title)
	strcpy_s( pszTitle, 50, szTitle );
	PEszset(*hPE, PEP_szMAINTITLE, _T(""));
	PEszset(*hPE, PEP_szSUBTITLE, pszTitle);
	PEszset(*hPE, PEP_szYAXISLABEL, "Y Coordinate");
	PEszset(*hPE, PEP_szXAXISLABEL, "X Coordinate");
	// settings
	PEnset(*hPE, PEP_nFONTSIZE, PEFS_MEDIUM);
	PEszset(*hPE, PEP_szMAINTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szSUBTITLEFONT, "Arial");
	PEszset(*hPE, PEP_szLABELFONT, "Arial");
	PEszset(*hPE, PEP_szTABLEFONT, "Arial");
	SetChartDefaults( hPE, (HWRGraph)HWR_XY, FALSE );

	PEnset(*hPE, PEP_bFOCALRECT, FALSE);
	PEnset(*hPE, PEP_bPREPAREIMAGES, TRUE);
	PEnset(*hPE, PEP_nPLOTTINGMETHOD, PEGPM_LINE);
	PEnset(*hPE, PEP_nGRIDLINECONTROL, PEGLC_NONE);
	PEnset(*hPE, PEP_nALLOWZOOMING, PEAZ_HORZANDVERT);

	//  When set to no manual PEreinitialize delivers the min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_NONE);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_NONE);
	PEreinitialize( *hPE );

	// Now enable manual setting of min and max of x and y data
	PEnset(*hPE, PEP_nMANUALSCALECONTROLX, PEMSC_MINMAX);
	PEnset(*hPE, PEP_nMANUALSCALECONTROLY, PEMSC_MINMAX);

	// Get min and max of x and y data
	double xMin = 0.0, xMax = 0.0, yMin = 0.0, yMax = 0.0;
	for( int nIdx = 0; nIdx < m_nCnt; nIdx++ )
	{
		if( nIdx == 0 )
		{
			xMin = xMax = x[ nIdx ];
			yMin = yMax = y[ nIdx ];
		}
		else
		{
			if( x[ nIdx ] < xMin ) xMin = x[ nIdx ];
			if( x[ nIdx ] > xMax ) xMax = x[ nIdx ];
			if( y[ nIdx ] < yMin ) yMin = y[ nIdx ];
			if( y[ nIdx ] > yMax ) yMax = y[ nIdx ];
		}
	}

	double dataxmin = xMin, dataxmax = xMax, dataymin = yMin, dataymax = yMax;
		
	// Calculate the real data range
	double dataxrange = dataxmax - dataxmin;
	double datayrange = dataymax - dataymin;

	// Test for zero data ranges.
	if (dataxrange == 0.) dataxrange = 1.;
	if (datayrange == 0.) datayrange = 1.;

	// Get the screen positions. 
	PEvget (*hPE, PEP_rectGRAPH, &ScreenRectangle); 

	// Calculate data to pixel scales ant take smallest scale for x and y so that
	// the graph fits
	double scalex = (ScreenRectangle.right - ScreenRectangle.left) / dataxrange;
	double scaley = (ScreenRectangle.bottom - ScreenRectangle.top) / datayrange;
	double scale = MIN (scalex, scaley);

	// Test for zero scale.
	if (scale == 0.) scale = 1.;

	// Estimate the extra data space for x or y data needed to assure that a square remains a square
	double extradataxrange = 0.;
	double extradatayrange = 0.;
	if( !m_fOptimal )	// ie..proportional
	{
		if( scale == 1. ) scale = 2.;	// so we dont get a divide by zero
		extradataxrange = dataxrange * (scalex / scale - 1.);
		extradatayrange = datayrange * (scaley / scale - 1.);
	}
	// else independent optimal x & y scaling

    // Divide the extra data space so that the graph is in the middle of the window
	double dataxminnew = dataxmin - .5 * extradataxrange;
	double dataxmaxnew = dataxmax + .5 * extradataxrange;
	double datayminnew = dataymin - .5 * extradatayrange;
	double dataymaxnew = dataymax + .5 * extradatayrange;

	// Set the new min and max of the x and y data
	PEvset(*hPE, PEP_fMANUALMINX, &dataxminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXX, &dataxmaxnew, 1);
	PEvset(*hPE, PEP_fMANUALMINY, &datayminnew, 1);
	PEvset(*hPE, PEP_fMANUALMAXY, &dataymaxnew, 1);

	// no cursor
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);
	PEnset(*hPE, PEP_bMOUSECURSORCONTROL, FALSE);
	PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
	PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, FALSE);
	PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_NONE);

	PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect( *hPE, NULL, FALSE );

	_Idx = 0;

	return TRUE;
}

STDMETHODIMP ShowHWR::OnTimer( VARIANT* PEHandle )
{
	HWND* hPE = (HWND*)PEHandle;

	if( _Idx >= m_nCnt ) return S_OK;

	float fX = (float)x[ _Idx ];
	PEvsetcellEx( *hPE, PEP_faXDATA, 0, _Idx, &fX );
	float fY = (float)y[ _Idx ];
	PEvsetcellEx( *hPE, PEP_faYDATA, 0, _Idx, &fY );

	double dVal = (double)fX;
	PEvsetcell( *hPE, PEP_faVERTLINEANNOTATION, 0, &dVal );
	int lt = PELT_MEDIUMSOLID;
	PEvsetcell( *hPE, PEP_naVERTLINEANNOTATIONTYPE, 0, &lt );
	DWORD col = RGB(0,0,255);
	PEvsetcell( *hPE, PEP_dwaVERTLINEANNOTATIONCOLOR, 0, &col );

	dVal = (double)fY;
	PEvsetcell( *hPE, PEP_faHORZLINEANNOTATION, 0, &dVal );
	PEvsetcell( *hPE, PEP_naHORZLINEANNOTATIONTYPE, 0, &lt );
	PEvsetcell( *hPE, PEP_dwaHORZLINEANNOTATIONCOLOR, 0, &col );
    
	DWORD dwColor;
	if( z[ _Idx ] > ::GetMinPenPressure() ) dwColor = m_fMonochrome ? MONOCOLOR: LINECOLOR;
	else dwColor = m_dwPenUpColor;

	PEvsetcellEx( *hPE, PEP_dwaPOINTCOLORS, 0, _Idx, &dwColor );

	_Idx++;

	// no cursor
	PEnset(*hPE, PEP_nCURSORMODE, PECM_NOCURSOR);
	PEnset(*hPE, PEP_bMOUSECURSORCONTROL, FALSE);
	PEnset(*hPE, PEP_bALLOWDATAHOTSPOTS, FALSE);
	PEnset(*hPE, PEP_bCURSORPROMPTTRACKING, FALSE);
	PEnset(*hPE, PEP_nCURSORPROMPTSTYLE, PECPS_NONE);

	//** Update image and force paint **
	PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowHWR::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetGraphLogOn( fOn );

	return S_OK;
}

STDMETHODIMP ShowHWR::UseOptimalScaling( BOOL fOn )
{
	m_fOptimal = fOn;

	return S_OK;
}

STDMETHODIMP ShowHWR::put_MinPenPressure(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	SetMinPenPressure( newVal );

	return S_OK;
}

STDMETHODIMP ShowHWR::AddFile(BSTR File, BOOL Clear)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( Clear )
	{
		m_Files.RemoveAll();
		return S_OK;
	}

	CString szFile = File;
	m_Files.AddTail( szFile );

	return S_OK;
}

STDMETHODIMP ShowHWR::put_PenUpColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dwPenUpColor = newVal;

	return S_OK;
}

STDMETHODIMP ShowHWR::put_Monochrome(BOOL fOn)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fMonochrome = fOn;

	return S_OK;
}

STDMETHODIMP ShowHWR::SetMonochrome( VARIANT* PEHandle, HWRGraph GraphType, BOOL fMultiple, BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !PEHandle ) return E_FAIL;

	BOOL fRet = FALSE;
	if( !fMultiple )
	{
		if( GraphType == HWR_XY_3D )
			fRet = ShowXY3D( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == HWR_XY )
			fRet = ShowXY( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == HWR_XZ )
			fRet = ShowXZ( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == HWR_YZ )
			fRet = ShowYZ( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == HWR_XYt )
			fRet = ShowXYt( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( GraphType == HWR_Zt )
			fRet = ShowZt( NULL, NULL, (HWND*)PEHandle, TRUE );
	}
	else
	{
		if( ( GraphType & HWR_XY ) == HWR_XY )
			fRet = ShowXYMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_XZ ) == HWR_XZ )
			fRet = ShowXZMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_YZ ) == HWR_YZ )
			fRet = ShowYZMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_XYt ) == HWR_XYt )
			fRet = ShowXYtMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_Zt ) == HWR_Zt )
			fRet = ShowZtMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
	}

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowHWR::OnCursorMove( VARIANT *PEHandle, HWRGraph GraphType )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;
	if( m_dFrequency == 0. ) return E_FAIL;

	// sample # (index from chart)
	long nX = PEnget( *hPE, PEP_nCURSORPOINT );
	if( ( nX < 0 ) || ( nX >= m_nCnt ) ) return E_FAIL;
	// pressure and slant vars
	double dZ = z[ nX ], dS = 0.;
	// string var for table data
	char szTblData[ TBL_DATA_SIZE ];

	// construct table data
	int nDeg = (int)( dS * 180 / PI );
	sprintf_s( szTblData, PETAL_STR, nX, dZ );
	PEvsetcellEx( *hPE, PEP_szaTATEXT, 0, 0, szTblData );
	PEdrawtable( *hPE, 0, NULL );

	return S_OK;
}

STDMETHODIMP ShowHWR::GetCursorPos( VARIANT *PEHandle, LONG* Pos )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	*Pos = PEnget( *hPE, PEP_nCURSORPOINT );
	return S_OK;
}

STDMETHODIMP ShowHWR::SetCursorPos( VARIANT *PEHandle, LONG Pos )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	PEnset( *hPE, PEP_nCURSORPOINT, Pos );
	return S_OK;
}

STDMETHODIMP ShowHWR::PenPressureLineThickness( BOOL PPLT )
{
	m_fPPLT = PPLT;
	return S_OK;
}

STDMETHODIMP ShowHWR::SetLineThickness( VARIANT* PEHandle, short Thickness )
{
	HWND* hPE = (HWND*)PEHandle;
	if( !hPE ) return E_FAIL;

	if( ( Thickness <= 0 ) || ( Thickness > 7 ) ) return E_FAIL;

	int nT[ 2 ] = { PELT_THINSOLID, PELT_THINSOLID };
	switch( Thickness )
	{
		case 1: nT[ 0 ] = nT[ 1 ] = PELT_EXTRATHINSOLID; break;
		case 2: nT[ 0 ] = nT[ 1 ] = PELT_THINSOLID; break;
		case 3: nT[ 0 ] = nT[ 1 ] = PELT_MEDIUMTHINSOLID; break;
		case 4: nT[ 0 ] = nT[ 1 ] = PELT_MEDIUMSOLID; break;
		case 5: nT[ 0 ] = nT[ 1 ] = PELT_MEDIUMTHICKSOLID; break;
		case 6: nT[ 0 ] = nT[ 1 ] = PELT_THICKSOLID; break;
		case 7: nT[ 0 ] = nT[ 1 ] = PELT_EXTRATHICKSOLID; break;
	}

	PEvset( *hPE, PEP_naSUBSETLINETYPES, nT, 2 );
	PEreinitialize( *hPE );
	PEresetimage( *hPE, 0, 0 );
	::InvalidateRect(*hPE, NULL, FALSE);

	return S_OK;
}

STDMETHODIMP ShowHWR::SetYZInverted( VARIANT* PEHandle, HWRGraph GraphType, BOOL fMultiple, BOOL fOn )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !PEHandle ) return E_FAIL;

	m_fInverted = !m_fInverted;

	BOOL fRet = FALSE;
	if( !fMultiple )
	{
		if( ( GraphType & HWR_XY_3D ) == HWR_XY_3D )
			fRet = ShowXY3D( NULL, NULL, (HWND*)PEHandle, TRUE, TRUE );
		else if( ( GraphType & HWR_XY ) == HWR_XY )
			fRet = ShowXY( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_XZ ) == HWR_XZ )
			fRet = ShowXZ( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_YZ ) == HWR_YZ )
			fRet = ShowYZ( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_XYt ) == HWR_XYt )
			fRet = ShowXYt( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_Zt ) == HWR_Zt )
			fRet = ShowZt( NULL, NULL, (HWND*)PEHandle, TRUE );
	}
	else
	{
		if( ( GraphType & HWR_XY ) == HWR_XY )
			fRet = ShowXYMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_XZ ) == HWR_XZ )
			fRet = ShowXZMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_YZ ) == HWR_YZ )
			fRet = ShowYZMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_XYt ) == HWR_XYt )
			fRet = ShowXYtMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
		else if( ( GraphType & HWR_Zt ) == HWR_Zt )
			fRet = ShowZtMultiple( NULL, NULL, (HWND*)PEHandle, TRUE );
	}

	return fRet ? S_OK : E_FAIL;
}

STDMETHODIMP ShowHWR::Customize( VARIANT* PEHandle )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !PEHandle ) return E_FAIL;
	HWND* hPE = (HWND*)PEHandle;
	int nRes = PElaunchcustomize( *hPE );
	return( ( nRes == IDOK ) ? S_OK : S_FALSE );
}

STDMETHODIMP ShowHWR::SetMissingDataValue(double MDVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	::SetMissingDataValue( MDVal );
	return S_OK;
}

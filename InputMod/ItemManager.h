#ifndef __ITEMMANAGER_H__
#define	__ITEMMANAGER_H__

#include "..\CTreeLink\DBCommon.h"
#include <sys/timeb.h>

class MessageManager;
class ElementMessageManager;

class ItemManager : public CObject
{
// construction/destruction
public:
	ItemManager( MessageManager* pMM = NULL );
	virtual ~ItemManager();

// attributes
public:
	MessageManager*		m_pDaddy;
	CString				m_szID;			// id of item
	int					m_nItem;

// methods
public:
	virtual int PostMessage( LPARAM msg, WPARAM val, ULONG clock,
							 BOOL& invalidate, BOOL& terminate, WORD& returnmsg );
	virtual int PostMessage( LPARAM msg, double val, ULONG clock,
							 BOOL& invalidate, BOOL& terminate, WORD& returnmsg );
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

struct xyz
{
public:
	xyz() : x( 0. ), y( 0. ), z( 0. ) {}

	double x;
	double y;
	double z;
};

// structure to hold shape/location info
struct DataInfo
{
public:
	DataInfo() {}

	// TODO: We'll simply store the 6 lines of data for the rectangle
	// will need to be changed when we add other shapes
	xyz		points[ 6 ];
};

// structure to hold decorations info
struct DecInfo
{
public:
	DecInfo() : nLineSize( 1 ), dwLineColor( 0 ), dwBkColor( RGB(255,255,255) ),
				nSize( 0 ), dwColor( 0 ), lWeight( 0 ), bI( 0 ), bU( 0 ),
				bSO( 0 ), bPitch( 0 ), fIsImage( FALSE ), nShape( 0 )
	{ memset( szText, 0, 50 ); memset( szFont, 0, 50 ); memset( szBMP, 0, _MAX_PATH ); }

	int		nLineSize;
	DWORD	dwLineColor;
	DWORD	dwBkColor;

	char	szText[ szELEMENT_TEXT + 1 ];
	char	szFont[ 50 ];
	int		nSize;
	DWORD	dwColor;
	LONG	lWeight;
	int		bI;		// italics
	int		bU;		// underline
	int		bSO;	// strike-out
	int		bPitch;	// pitch&family

	BOOL	fIsImage;
	BOOL	fIsPattern;
	char	szBMP[ _MAX_PATH ];
	double	dCenterX;
	double	dCenterY;
	int		nShape;
};

// structure to hold behavior info
struct BehInfo
{
public:
	BehInfo() : dStart( 0. ), dDuration( 0. ), fHideRightTarget( FALSE ), fHideWrongTarget( FALSE ),
				fHideIfAnyRightTarget( FALSE ), fHideIfAnyWrongTarget( FALSE )
		{ memset( szRightTarget, 0, 4 ); memset( szWrongTarget, 0, 4 ); }

	double	dStart;
	double	dDuration;
	BOOL	fHideRightTarget;
	BOOL	fHideWrongTarget;
	BOOL	fHideIfAnyRightTarget;
	BOOL	fHideIfAnyWrongTarget;
	char	szRightTarget[ 4 ];
	char	szWrongTarget[ 4 ];
	// GMB: 2016-04-11: Added duration pen must stay in element/target: + for at least, - for up to
	double	stayInTime;
};

// structure to hold animations info
struct AnimInfo
{
public:
	AnimInfo() : fAnimate( FALSE ), dRate( 0. ), fRandom( FALSE ),  fGen( FALSE )
	{
		memset( szFile, 0, _MAX_PATH );
		memset( szExp, 0, 4 );
		memset( szGrp, 0, 4 );
		memset( szSubj, 0, 4 );
		dShiftX = dShiftY = 0.;
	}

	BOOL	fAnimate;
	double	dRate;
	char	szFile[ _MAX_PATH ];
	BOOL	fRandom;
	BOOL	fGen;
	char	szExp[ 4 ];
	char	szGrp[ 4 ];
	char	szSubj[ 4 ];
	double	dShiftX;
	double	dShiftY;
};

// structure to hold target info
struct TgtInfo
{
public:
	TgtInfo() : fReached( FALSE )
	{
		memset( &ptLL, 0, sizeof( POINT ) );
		memset( &ptUL, 0, sizeof( POINT ) );
		memset( &ptUR, 0, sizeof( POINT ) );
		memset( &ptLR, 0, sizeof( POINT ) );
	}

public:
	POINT	ptLL;
	POINT	ptUL;
	POINT	ptUR;
	POINT	ptLR;
	BOOL	fReached;
};

class ElementManager : public ItemManager
{
// construction/destruction
public:
	ElementManager( ElementMessageManager* pMM = NULL, ElementManager* prevemgr = NULL );
	virtual ~ElementManager();

// attributes
public:
	CString		m_szPath;		// path to items
	// NOTE: Above path is just the path..not a filename
	//		 Naming conventions for element files are as follows:
	//			STI : Defines shape (or pattern)
	//			FMT : Defines appearance
	//			BEH : Defines behavior
	//			TGT : Defines target range (reflecting errors)
	//			ANI : Animation instructions
	CString		m_szDataPath;	// path to store target results info
	BOOL		m_fTarget;		// am i a target
	double		m_dStart;		// start time relative to owner (s)
	double		m_dDuration;	// duration (s)
	BOOL		m_fDisplayed;	// am i already displayed? (to avoid flicker)
	BOOL		m_fDisplay;		// Do i display?
	int			m_nLines;		// # of data lines to read for display
	DataInfo	m_i;			// shape/location info
	DecInfo		m_di1;			// decoration info for normal state
	DecInfo		m_di2;			// decoration info for success
	DecInfo		m_di3;			// decoration info for failure
	BehInfo		m_bi;			// behavior info
	AnimInfo	m_ai;			// animation info
	TgtInfo		m_ti;			// target info (adjusted for error)
	BOOL		m_fReadTargetInfo;	// have i read the target info file yet
	HWND		m_hWndTarget;		// handle to target window
	HWND		m_hWndTablet;		// handle to tablet window
	BOOL		m_fUseMouse;		// are we using a mouse
	long		m_lExtX;			// digitizer x extent
	long		m_lExtY;			// digitizer y extent
	BOOL		m_fTabletOnly;		// is tablet mapped to window only (not desktop)
	BOOL		m_fRealSize;		// recording window sized to tablet
	double		m_dDisplayX;		// width of display in cm
	double		m_dDisplayY;		// height of display in cm
	RECT		m_rect;				// bounding rectangle (calculated during display)
	RECT		m_rectLast;			// last rect (for animation)

	ElementManager*		m_pPrevious;	// previous target (if null, then this = 1st)
	BOOL				m_fReached;		// have we been hit
	BOOL				m_fInAndOut;	// have we reached target & left it yet?
	BOOL				m_fStop;		// no longer test for target once reached incorrectly
	BOOL				m_fCorrect;		// was i correctly reached

	// animation stuff
	CStdioFile	m_fileAnim;		// animation file (keep open for sake of i/o)
	BOOL		m_fAnimFile;	// did we successfully open file?
	ULONG		m_ulClockAnim;	// clock for animation
	BOOL		m_f1stPoint;	// have we opened file & read first point yet?
	double*		m_thex;			// for the hwr file
	double*		m_they;
	long		m_nPoints;
	BOOL		m_fInit;		// began recording and ready to process target-related displays

	// GMB: 2016-04-14: Added timestamps (incl. ms) for entering/exiting element
	struct _timeb m_timebIn;
	struct _timeb m_timebOut;

	enum ElementState { esNormal, esSuccess, esFailure, esHidden } m_esState;

// methods
public:
	virtual int PostMessage( LPARAM msg, WPARAM val, ULONG clock,
							 BOOL& invalidate, BOOL& terminate, WORD& returnmsg );
	virtual int PostMessage( LPARAM msg, double val, ULONG clock,
							 BOOL& invalidate, BOOL& terminate, WORD& returnmsg );
	void SetPath( CString szPath ) { m_szPath = szPath; Reset(); }
	void SetDataPath( CString szPath ) { m_szDataPath = szPath; }
	BOOL AddItem( CString szID );
	BOOL ReadTargetFile();
	void GetBoundingRectangle( LPRECT pRect );
protected:
	void Reset();
	// Reading files for info (called upon set of path)
	BOOL ReadFiles();
	BOOL ReadDecorFile( int& nDataLines );
	BOOL ReadDataFile();
	BOOL ReadBehaviorFile();
	BOOL ReadAnimationFile();
	// display
	void Display( CDC* pDC, BOOL fEraseLast = FALSE, BOOL fNoRedraw = FALSE );
	void DrawShape( CDC* pDC, int nLineSize, DWORD dwLineColor, DWORD dwBkColor,
					long x1, long y1, long x2, long y2, UINT nShape, BOOL fErase = FALSE );
	void DrawShape( CDC* pDC, int nLineSize, DWORD dwLineColor,
					DWORD dwBkColor, CRect rect, UINT nShape );
	// target reach check
	BOOL InTarget( WORD x, WORD y );
	// if reached and have another element to display
	void ReachedShowElement( CString szID );

	// animation stuff
	void AnimationUpdate( ULONG clock, BOOL& invalidate );

	void CalculateTargetInfo( POINT* p, double dx, double dy );
};

#endif	// __ITEMMANAGER_H__

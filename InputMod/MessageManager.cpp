// MessageManager.cpp

#include "StdAfx.h"
#include "InputMod.h"
#include "MessageManager.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

MessageManager::MessageManager()
{
}

///////////////////////////////////////////////////////////////////////////////

MessageManager::~MessageManager()
{
	Clear();
}

///////////////////////////////////////////////////////////////////////////////

void MessageManager::Clear()
{
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		CObject* pObj = m_lstItems.GetNext( pos );
		if( pObj ) delete pObj;
	}
	m_lstItems.RemoveAll();
}

///////////////////////////////////////////////////////////////////////////////

int MessageManager::PostMessage( LPARAM msg, WPARAM val, ULONG clock, BOOL& invalidate, WORD& returnmsg )
{
	int nCnt = 0 ;

	invalidate = FALSE;
	BOOL fInval = FALSE, fTerm = FALSE;
	WORD wHigh = HIWORD( msg );
	WORD wLow = LOWORD( msg );

	ItemManager* pMgr;
	// targets are stored in sequence for messaging in order
	// however, for display purposes, we need to reverse their drawing
	// so when MSG_RESET (for drawing) is sent, we reverse the order
	POSITION pos = ( wHigh == MSG_RESET ) ? m_lstItems.GetTailPosition() :
											m_lstItems.GetHeadPosition();
	while( pos )
	{
		if( wHigh == MSG_RESET ) pMgr = (ItemManager*)m_lstItems.GetPrev( pos );
		else pMgr = (ItemManager*)m_lstItems.GetNext( pos );
		if( pMgr ) nCnt += pMgr->PostMessage( msg, val, clock, fInval, fTerm, returnmsg );
		invalidate |= fInval;

		// stop posting messages if fTerm is set to true
		if( fTerm ) break;
	}

	return nCnt;
}

///////////////////////////////////////////////////////////////////////////////

int MessageManager::PostMessage( LPARAM msg, double val, ULONG clock, BOOL& invalidate, WORD& returnmsg )
{
	int nCnt = 0 ;

	invalidate = FALSE;
	BOOL fInval = FALSE, fTerm = FALSE;

	ItemManager* pMgr;
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pMgr = (ItemManager*)m_lstItems.GetNext( pos );
		if( pMgr ) nCnt += pMgr->PostMessage( msg, val, clock, fInval, fTerm, returnmsg );
		invalidate |= fInval;

		if( fTerm ) break;
	}

	return nCnt;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MessageManager::AddItem( ItemManager* pIM )
{
	if( pIM )
	{
		m_lstItems.AddTail( (CObject*)pIM );
		return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

ItemManager* MessageManager::FindObject( CString szID )
{
	ItemManager* pFound = NULL;
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		ItemManager* pIM = (ItemManager*)m_lstItems.GetNext( pos );
		if( pIM )
		{
			if( pIM->m_szID == szID )
			{
				pFound = pIM;
				break;
			}
		}
	}

	return pFound;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MessageManager::RemoveItem( CString szID )
{
	BOOL fRes = FALSE;

	// TODO: Do we really need this?

	return fRes;
}

///////////////////////////////////////////////////////////////////////////////

BOOL MessageManager::RemoveItem( ItemManager* pIM )
{
	if( !pIM ) return FALSE;
	return RemoveItem( pIM->m_szID );
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

ElementMessageManager::ElementMessageManager()
{
}

///////////////////////////////////////////////////////////////////////////////

ElementMessageManager::~ElementMessageManager()
{
}

///////////////////////////////////////////////////////////////////////////////

void ElementMessageManager::Clear()
{
	m_lstTargets.RemoveAll();
	MessageManager::Clear();
}

///////////////////////////////////////////////////////////////////////////////

ElementManager* ElementMessageManager::AddItem( CString szID, CString szPath, CString szDataPath,
												BOOL fTarget, ElementManager* prevemgr )
{
	BOOL fRet = FALSE;

	int nCount = m_lstItems.GetCount();
	ElementManager* pEM = new ElementManager( this, prevemgr );
	pEM->m_szID = szID;
	pEM->m_nItem = nCount + 1;
	pEM->m_fTarget = fTarget;
	pEM->SetPath( szPath );
	pEM->SetDataPath( szDataPath );

	m_lstItems.AddTail( (CObject*)pEM );
	if( fTarget && !AmIOnTargetList( pEM ) ) m_lstTargets.AddTail( pEM->m_szID );

	return pEM;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementMessageManager::AmIOnTargetList( ElementManager* pKiddy )
{
	if( !pKiddy ) return FALSE;
	CString szAskerID = pKiddy->m_szID, szID;

	POSITION pos = m_lstTargets.GetHeadPosition();
	while( pos )
	{
		szID = m_lstTargets.GetNext( pos );
		if( szID == szAskerID ) return TRUE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementMessageManager::AmIFirstTarget( ElementManager* pKiddy )
{
	if( !pKiddy ) return FALSE;
	CString szAskerID = pKiddy->m_szID, szID;

	if( m_lstTargets.GetCount() <= 0 ) return TRUE;

	BOOL fTargetFound = FALSE;
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		// loop till find current EM, while checking to see if any previous were targets
		ElementManager* pEM = (ElementManager*)m_lstItems.GetNext( pos );
		// if this current item is the one passed in, break
		if( pEM == pKiddy ) break;
		// check current to see if it's a target
		if( pEM ) fTargetFound |= pEM->m_fTarget;
	}

	return !fTargetFound;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementMessageManager::AmILastTarget( ElementManager* pKiddy )
{
	if( !pKiddy ) return FALSE;
	// GMB: 2016-05-31: Adding check to ensure element we're checking is actually a target
	if( !pKiddy->m_fTarget ) return FALSE;

	// if no targets in list at all, then the answer is certainly true
	if( m_lstTargets.GetCount() <= 0 ) return TRUE;

	// ID of element we're checking
	CString szAskerID = pKiddy->m_szID, szID;

	// loop through elements to determine if there are any elements AFTER one to check that are targets
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		// loop till find current EM
		ElementManager* pEM = (ElementManager*)m_lstItems.GetNext( pos );
		// if pos == NULL, then there are no more items, so last = true
		if( !pos ) return TRUE;
		// if this current item is the one passed in, process
		if( pEM == pKiddy )
		{
			// get next one to see if it is a target
			ElementManager* pEMNext = (ElementManager*)m_lstItems.GetNext( pos );

			// GMB: 2016-05-31: The logic below will not work properly, as it assumes there will be no
			// additional items (elements) after THIS one we're checking
//			if( pEMNext && pEMNext->m_fTarget ) return FALSE;
//			else return TRUE;

			// SO: We'll continue looping through until we either don't find another target, or find one that is a target
			while( ( pEMNext != NULL ) && !pEMNext->m_fTarget ) {

				if( pos ) {

					pEMNext = (ElementManager*)m_lstItems.GetNext( pos );
				}
				// there are no others in the list, so we'll return TRUE
				else {

					return TRUE;
				}
			}

			// According to the logic above, if the next child is valid, it should be a target. So, we'll return FALSE
			if( pEMNext ) return FALSE;
			// otherwise, if it's not valid (NULL), then element we're checking against is the last one, so return TRUE
			else return TRUE;
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementMessageManager::AnySiblingsNotReached( ElementManager* pKiddy )
{
	if( !pKiddy ) return FALSE;
	CString szAskerID = pKiddy->m_szID, szID;
	ElementManager* pEM = NULL;

	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pEM = (ElementManager*)m_lstItems.GetNext( pos );
		if( !pEM ) break;

		if( pEM != pKiddy )
		{
			szID = pEM->m_szID;
			if( ( szID == szAskerID ) && !pEM->m_fReached ) return TRUE;
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementMessageManager::AnyNonReachedSiblingsBeforeMe( ElementManager* pKiddy )
{
	if( !pKiddy ) return FALSE;

	CString szAskerID = pKiddy->m_szID, szID;
	ElementManager* pEM = NULL;

	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pEM = (ElementManager*)m_lstItems.GetNext( pos );
		if( !pEM ) break;

		// anything after requester we ignore
		if( pEM == pKiddy ) break;

		szID = pEM->m_szID;
		// if this element has the same ID (ie replica of me) and
		// is NOT me and
		// he has not been reached yet and
		// he has not exited yet,
		// then i dont need to do anything
		if( szID == szAskerID )
		{
			if( !pEM->m_fReached || ( pEM->m_fReached && !pEM->m_fInAndOut ) )
				return TRUE;
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementMessageManager::HasTheNextTargetBeenReached( ElementManager* pKiddy )
{
	if( !pKiddy ) return FALSE;

	CString szAskerID = pKiddy->m_szID;
	ElementManager* pEM = NULL;

	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pEM = (ElementManager*)m_lstItems.GetNext( pos );
		if( pEM == pKiddy )
		{
			if( !pos ) return FALSE;
			pEM = (ElementManager*)m_lstItems.GetNext( pos );
			return ( pEM->m_fReached && pEM->m_fCorrect );
		}
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

void ElementMessageManager::DisplayNextSibling( ElementManager* pKiddy )
{
	if( !pKiddy ) return;

	CString szAskerID = pKiddy->m_szID, szID;
	ElementManager* pEM = NULL;
	BOOL fCheck = FALSE;

	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pEM = (ElementManager*)m_lstItems.GetNext( pos );
		if( fCheck )
		{
			szID = pEM->m_szID;
			if( szID == szAskerID )
			{
				pEM->m_fDisplay = TRUE;
				break;
			}
		}

		if( pEM == pKiddy ) fCheck = TRUE;
	}

	return;
}

///////////////////////////////////////////////////////////////////////////////

ElementManager* ElementMessageManager::InsertNewItem( ElementManager* pKiddy, CString szID )
{
	if( !pKiddy ) return NULL;

	ElementManager* pEM = NULL;
	POSITION pos = m_lstItems.GetHeadPosition();
	while( pos )
	{
		pEM = (ElementManager*)m_lstItems.GetNext( pos );
		if( pEM == pKiddy )
		{
			int nCount = m_lstItems.GetCount();
			pEM = new ElementManager( this, pKiddy );
			pEM->m_szID = szID;
			pEM->m_nItem = nCount + 1;
			pEM->SetPath( pKiddy->m_szPath );
			m_lstItems.InsertBefore( pos, (CObject*)pEM );

			// reset previous pointer
			if( pos )
			{
				ElementManager* pEM2 = (ElementManager*)m_lstItems.GetNext( pos );
				pEM2->m_pPrevious = pEM;
			}

			break;
		}
	}

	return pEM;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

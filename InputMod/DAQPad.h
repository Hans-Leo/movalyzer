// DAQPad.h : Declaration of the DAQPad

#ifndef __DAQPAD_H_
#define __DAQPAD_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// DAQPad
class CNSAnalogIn;

class DAQPad : 
	public IDispatchImpl<IDAQPad, &IID_IDAQPad, &LIBID_INPUTMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<DAQPad,&CLSID_DAQPad>
{
public:
	DAQPad();
	~DAQPad();

BEGIN_COM_MAP(DAQPad)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IDAQPad)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(DAQPad) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_DAQPad)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IDAQPad
public:
	STDMETHOD(put_LeftViewMaximizeSize)(/*[in]*/ int newVal);
	STDMETHOD(put_MagnetForce)(/*[in]*/ double newVal);
	STDMETHOD(put_OutputType)(/*[in]*/ DAQOutputType OutputType);
	STDMETHOD(WriteDrawPoints)(/*[in]*/ VARIANT* DC);	//only used for single-buffer mode after all data acquired
	STDMETHOD(put_OutputFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(put_ChannelVector)(/*[in]*/ short Index, /*[in]*/ short newVal);
	STDMETHOD(put_BaselineCount)(/*[in]*/ short newVal);
	STDMETHOD(SetMessageWindow)(/*[in]*/ VARIANT* MsgWindow);
	STDMETHOD(AcquireData)(/*[in]*/ BOOL ForceStop);
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);
	STDMETHOD(SetOutputWindow)(/*[in]*/ VARIANT* MsgWindow);
	STDMETHOD(get_DataNewtonsByChannel)(/*[in]*/ short Channel, /*[in]*/ long Index, /*[out, retval]*/ double *pVal);
	STDMETHOD(get_DataNewtons)(/*[in]*/ long Index, /*[out, retval]*/ double *pVal);
	STDMETHOD(get_DataVoltageByChannel)(/*[in]*/ short Channel, /*[in]*/ long Index, /*[out, retval]*/ double *pVal);
	STDMETHOD(get_DataVoltage)(/*[in]*/ long Index, /*[out, retval]*/ double *pVal);
	STDMETHOD(Stop)();
	// DBSamples = # of samples for double-buffer mode
	// single-buffer mode simply put in same as you use for put_Samples
	STDMETHOD(Record)(/*[in]*/ BOOL DoubleBufferMode, /*[in]*/ long DBSamples);
	STDMETHOD(Reset)();
	STDMETHOD(put_ExcitationVoltage)(/*[in]*/ short Channel, /*[in]*/ double newVal);
	STDMETHOD(put_CalibrationConstant)(/*[in]*/ short Channel, /*[in]*/ double newVal);
	STDMETHOD(put_FullScaleLoad)(/*[in]*/ short Channel, /*[in]*/ double newVal);
	STDMETHOD(put_Gain)(/*[in]*/ short Index, /*[in]*/ short newVal);
	STDMETHOD(put_ScanRate)(/*[in]*/ double newVal);
	STDMETHOD(put_SampleRate)(/*[in]*/ double newVal);
	STDMETHOD(put_Samples)(/*[in]*/ long newVal);
	STDMETHOD(put_Channels)(/*[in]*/ short newVal);
	STDMETHOD(put_Device)(/*[in]*/ short newVal);
protected:
	void CalcGains();

protected:
	short			m_nDevice;
	short			m_nChannels;
	long			m_lSamples;
	double			m_dSampleRate;
	double			m_dScanRate;
	short*			m_pGain;
	double*			m_pGainAdjust;
	short			m_nBaselineCount;
	double			m_dMagnetForce;

	double*			m_pFullScaleLoad;
	double*			m_pCalibrationConstant;
	double*			m_pExcitationVoltage;
	double*			m_pVtoN;

	BOOL			m_fLocked;
	double*			m_pData;
	HWND			m_hwnd;
	CString			m_szFile;
	CNSAnalogIn*	m_pAI;
};

#endif //__DAQPAD_H_

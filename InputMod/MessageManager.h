#ifndef __MESSAGEMANAGER_H__
#define	__MESSAGEMANAGER_H__

#include "ItemManager.h"

// msg types (hiword)
#define	MSG_NONE		0
#define	MSG_RESET		1	// reset
#define	MSG_HANDLES		2	// handles (such as windows)
#define	MSG_FLAGS		3	// flags & stuff (boolean typically)
#define	MSG_TIMER		4	// clock tick

//msg subtypes (loword)
#define	MSG_SUB_NONE	0
#define	MSG_SUB_INC		1	// increment clock

class MessageManager
{
// construction/destruction
public:
	MessageManager();
	virtual ~MessageManager();

// attributes
protected:
	CObList		m_lstItems;

// methods
public:
	// post message: hiword of msg = msg type, loword = sub-msg
	// dwval = content for message: can be value or pointer (dependant upon message)
	// clock = current clock time (in milliseconds)
	// invalidate = flag for whether display is redrawn
	// returnmsg = info for caller about what happened
	// RETURN value = # of item responding positively
	virtual int PostMessage( LPARAM msg, WPARAM val, ULONG clock,
							 BOOL& invalidate, WORD& returnmsg );
	virtual int PostMessage( LPARAM msg, double val, ULONG clock,
							 BOOL& invalidate, WORD& returnmsg );
	BOOL AddItem( ItemManager* pIM );
	BOOL RemoveItem( CString szID );	// yes this could be more generic..but currently we use 3 char IDs
	BOOL RemoveItem( ItemManager* pIM );
	virtual void Clear();
protected:
	ItemManager* FindObject( CString szID );
};


// msg types (hiword) - ELEMENT specific
#define	MSG_ELMT_DISPLAY		101	// aspect ratio for mouse
#define	MSG_TARG_LOCATION		102	// where pen/mouse is now
#define	MSG_TARG_REACHED		103	// a target has been reached
#define MSG_TARG_LEFT			104	// a target that was reached has been left (exited)
//msg subtypes (loword) - ELEMENT specific
#define	MSG_SUB_ELMT_TABLET		101	// tablet window handle
#define	MSG_SUB_ELMT_TARGET		102	// target window handle
#define	MSG_SUB_ELMT_MOUSE		111	// use mouse
#define	MSG_SUB_ELMT_ONLY		112	// tablet is mapped to recording window
#define	MSG_SUB_ELMT_REAL		113	// mapping is real size (relative to digitizer)
#define	MSG_SUB_ELMT_EXTENTX	121	// x extent for digitizer
#define	MSG_SUB_ELMT_EXTENTY	122	// y extent for digitizer
#define MSG_SUB_ELMT_NORMAL		131	// display
#define	MSG_SUB_TARG_MODE		141	// how the target reacts AFTER being selected
									// out of sequence (as an incorrect target)
#define	MSG_SUB_TARG_SUCCESS	151	// target reached successfully
#define	MSG_SUB_TARG_FAILURE	152	//				  unsuccessfully
#define	MSG_SUB_ELMT_DISPLAY_X	161	// display width in cm
#define	MSG_SUB_ELMT_DISPLAY_Y	162	// display width in cm
#define	MSG_SUB_ELMT_TABLET_X	163	// tablet width in cm
#define	MSG_SUB_ELMT_TABLET_Y	164	// tablet width in cm
#define	MSG_SUB_BEGAN_RECORDING	170	// have begun recording

// return messages
#define	MSG_STATUS_NONE				0
#define	MSG_STATUS_TARG_SUCCESS		0x0001	// target correctly reached
#define	MSG_STATUS_TARG_FAILURE		0x0002	// an incorrect target was reached
#define	MSG_STATUS_TARG_FIRST		0x0010	// reached 1st target
#define	MSG_STATUS_TARG_LAST		0x0020	// reached last larget
#define MSG_STATUS_TARG_LEFT		0x0100	// target was left (exited)
#define	MSG_STATUS_TARG_BADTIME		0x1000	// was not in target within specified time range

class ElementMessageManager : public MessageManager
{
// construction/destruction
public:
	ElementMessageManager();
	virtual ~ElementMessageManager();

// attributes
protected:
	CStringList		m_lstTargets;

// methods
public:
	virtual void Clear();
	ElementManager* AddItem( CString szID, CString szPath, CString szDataPath,
							 BOOL fTarget, ElementManager* prevemgr );
	BOOL AnySiblingsNotReached( ElementManager* pKiddy );
	BOOL AnyNonReachedSiblingsBeforeMe( ElementManager* pKiddy );
	BOOL HasTheNextTargetBeenReached( ElementManager* pKiddy );
	void DisplayNextSibling( ElementManager* pKiddy );
	ElementManager* InsertNewItem( ElementManager* pKiddy, CString szID );
	BOOL AmIOnTargetList( ElementManager* pKiddy );
	BOOL AmIFirstTarget( ElementManager* pKiddy );
	BOOL AmILastTarget( ElementManager* pKiddy );
};

#endif	// __MESSAGEMANAGER_H__

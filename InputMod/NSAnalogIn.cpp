// Utility class to wrap some of the NI-DAQ functions for analog input.

#include "StdAfx.h"
#include "InputMod.h"
#include "NSAnalogIn.h"
#include "..\Common\DefFun.h"
#include "..\Common\Shared.h"

#ifndef API
	#ifndef WINAPI
		#define API			FAR PASCAL
	#else
		#define API			WINAPI
	#endif
#endif

// 31Aug09: Somesh: Channels not connected give a value of 4095.
//#define	DAQPAD_RANGE		4000
#define	DAQPAD_RANGE		4096
#define	MVV					1000
#define	LBSN				4.44822
#define	AVG_SAMPLES			50
#define BUFFER_BOTTOM		250

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

typedef short (API *LPFNAI_CONFIGURE) (short, short, short, short, short, short);
typedef short (API* LPFNTIMEOUT_CONFIG) (short, long);
typedef short (API* LPFNSCAN_OP) (short, short, short FAR*, short FAR*, short FAR*,
								  unsigned long, double, double);
typedef short (API* LPFNSCAN_DEMUX) (short FAR*, unsigned long, short, short);
typedef short (API* LPFNDAQ_VSCALE) (short, short, short, double, double, unsigned long,
									 short FAR*, double FAR*);
typedef short (API* LPFNDAQ_CLEAR) (short);
typedef short (API* LPFNDAQ_RATE) (double, short, short FAR*, unsigned short FAR*);
typedef short (API* LPFNSCAN_SETUP) (short, short, short FAR*, short FAR*);
typedef short (API* LPFNSCAN_START) (short, short FAR*, unsigned long, short, unsigned short,
									 short, unsigned short);
typedef short (API* LPFNCONFIG_DAQ_EVENT_MESSAGE) (short, short, char FAR*, short, long, long,
									 unsigned long, unsigned long, unsigned long, HWND, short,
									 unsigned long);
typedef short (API* LPFNDAQ_DB_CONFIG) (short, short);
typedef short (API* LPFNDAQ_DB_HALFREADY) (short, short FAR*, short FAR*);
typedef short (API* LPFNDAQ_DB_TRANSFER) (short, short FAR*, unsigned long FAR*, short FAR*);
typedef short (API* LPFNDIG_LINE_CONFIG) (short, short, short, short);
typedef short (API* LPFNDIG_IN_LINE) (short, short, short, short FAR*);
typedef short (API* LPFNDIG_OUT_LINE) (short, short, short, short);
typedef short (API* LPFNAO_VWRITE) (short, short, double);
typedef short (API* LPFNDAQ_STOPTRIGGER_CONFIG) (short, short, unsigned long);

UINT DoAcquire( LPVOID pParam );

// variables for threads (double-buffer mode only)
BOOL	_TerminateThread = FALSE;	// flag for terminating any double-buffering threads
HWND	_hwnd = NULL;				// handle to drawing window
short*	_Buffer = NULL;				// pointer to class member m_TempBuffer used in acquisitions
short	_Device = 0;				// the device
short	_Channels = 0;				// # of channels of device
unsigned long	_Samples = 0;		// total # of samples
unsigned long	_SampleCount = 0;	// sample count for double-buffer
BOOL	_First = FALSE;				// have we drawn our first point yet
CString	_File;						// file to write to
int		_OutputType;				// acquired data output type (raw, V, N)
short*	_ChanVect;					// channel vectors
short*	_Gains;						// gains per channel
double*	_VtoN;						// voltage to newtons conversion
double*	_Offsets;					// offsets (for baseline)

//CNSAnalogIn*	_pThis = NULL;

//////////////////////////////////////////////////////////////////////////////////////

HINSTANCE _hPad = NULL;
HINSTANCE GetPadInstance();
void FreePadInstance();
BOOL Exist();

//////////////////////////////////////////////////////////////////////////////////////

BOOL Exist()
{
	// Load library (dll)
	char szObj[ 500 ];
	::GetSystemDirectory( szObj, 500 );
	strcat_s( szObj, "\\NiDAQ32.DLL" );
	CFileFind ff;
	return ff.FindFile( szObj );
}

HINSTANCE GetPadInstance()
{
	if( !_hPad && Exist() )
	{
		// Load library (dll)
		char szObj[ 500 ];
		::GetSystemDirectory( szObj, 500 );
		strcat_s( szObj, "\\NiDAQ32.DLL" );
		_hPad = LoadLibrary( szObj );
		if( !_hPad )
		{
			::OutputMessage( _T("Unable to load NiDAQ driver."), LOG_TABLET );
			AfxMessageBox( _T("Unable to load NiDAQ driver."), MB_ICONEXCLAMATION );
			return NULL;
		}
	}

	return _hPad;
}

void FreePadInstance()
{
	if( _hPad ) FreeLibrary( _hPad );
	_hPad = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

// default constructor
CNSAnalogIn::CNSAnalogIn( short device, short channels )
{
	short status;
	m_TempBuffer = NULL;
	m_dTempBuffer = NULL;
	m_hwnd = 0;
	m_nOutputType = OT_RAW;
	m_fGotBaseline = FALSE;
	m_dMagnetForce = 8.;
	m_nBaselineCount = AVG_SAMPLES;

	// Default to a device number 1.  If there is only a single device on a
	// machine, it will be device #1.  The device number is actually set
	// by the Measurement and Automation Explorer (MAX) that gets installed
	// with NI-DAQ.
	m_Device = device;
	m_Channels = channels;
	m_Samples = 0;

	m_pFullScaleLoad = new double[ MAX_CHANNELS ];
	m_pCalibrationConstant = new double[ MAX_CHANNELS ];
	m_pExcitationVoltage = new double[ MAX_CHANNELS ];
	m_pVtoN = new double[ MAX_CHANNELS ];
	m_pFullScaleLoad[ CHAN_LOWER ] = 25;			// Lower Grip (B04)
	//m_pCalibrationConstant[ CHAN_LOWER ] = -2.458; // 31Aug09: Somesh: Changed Calibrationdefault
	m_pCalibrationConstant[ CHAN_LOWER ] = 1001;
	m_pExcitationVoltage[ CHAN_LOWER ] = 5;
	m_pFullScaleLoad[ CHAN_UPPER ] = 25;			// Upper Grip (B03)
	//m_pCalibrationConstant[ CHAN_UPPER ] = -2.402;// 31Aug09: Somesh: Changed Calibrationdefault
	m_pCalibrationConstant[ CHAN_UPPER ] = 1002;
	m_pExcitationVoltage[ CHAN_UPPER ] = 5;
	m_pFullScaleLoad[ CHAN_LOAD ] = 20;				// Load (Y12)
	//m_pCalibrationConstant[ CHAN_LOAD ] = 275.36;// 31Aug09: Somesh: Changed Calibrationdefault
	m_pCalibrationConstant[ CHAN_LOAD ] = 1003;
	m_pExcitationVoltage[ CHAN_LOAD ] = 5;
	memset( m_pVtoN, 0, MAX_CHANNELS * sizeof( double ) );

	// these should be arguments to the constructior
	for( int i = 0; i < MAX_CHANNELS; i++ )
	{
		m_Gains[ i ] = 100;
		m_Offsets[ i ] = 0.;
		piChanVect[ i ] = i;
		m_Scale[ i ] = 1;
	}

	//TODO: Documentation needed for all these numbers
	m_Scale[ 0 ] = ( 1 / ( 2.150 * 504 / 1000 * 5 / 4.44822 / 25 ) );		// B03
	m_Scale[ 1 ] = ( 1 / ( 2.254 * 504 / 1000 * 5 / 4.44822 / 25 ) );		// B03
	m_Scale[ 2 ] = ( 1 / ( 2.245 * 504 / 1000*  5 / 4.44822 / 25 ) );		// B03

	// Configure analog input channels.  This can also be done from MAX, but this will insure the correct settings.
	// TODO: Calling this again while another app has device open yields in crash?
	// TODO: Move this to a first call instead of constructor
	m_DeviceOK = TRUE;
	HINSTANCE hMod = ::GetPadInstance();
	if( hMod )
	{
		LPFNAI_CONFIGURE proc = NULL;
		proc = (LPFNAI_CONFIGURE)GetProcAddress( hMod, "AI_Configure" );
		if( proc )
		{
			status = (*proc) ( device, -1, 1, 1, 1, 0 );
			if( status >= 0 )
			{
				LPFNDIG_LINE_CONFIG digproc = NULL;
				digproc = (LPFNDIG_LINE_CONFIG)GetProcAddress( hMod, "DIG_Line_Config" );
				if( digproc )
				{
					(*digproc) ( device, 0, MAGNET_SET, 1 );
					(*digproc) ( device, 0, MAGNET_UNSETn, 1 );
					(*digproc) ( device, 0, MAGNET_CHECK, 0 );
					(*digproc) ( device, 0, MAGNET_ENABLEn, 1 );
					MagnetOff();
				}
				else m_DeviceOK = FALSE;
			}
			else m_DeviceOK = FALSE;
		}
		else
		{
			::OutputMessage( _T("Unable to load NiDAQ library proc."), LOG_TABLET );
			FreePadInstance();
			m_DeviceOK = FALSE;
		}
	}
	else m_DeviceOK = FALSE;
}

// default constructor
CNSAnalogIn::~CNSAnalogIn()
{
	if( m_TempBuffer ) delete [] m_TempBuffer;
	if( m_dTempBuffer ) delete [] m_dTempBuffer;
	if( m_pFullScaleLoad ) delete [] m_pFullScaleLoad;
	if( m_pCalibrationConstant ) delete [] m_pCalibrationConstant;
	if( m_pExcitationVoltage ) delete [] m_pExcitationVoltage;
	if( m_pVtoN ) delete [] m_pVtoN;
	FreePadInstance();
}

void CNSAnalogIn::SetChannelVector( int nIndex, short nChannel )
{
	if( ( nIndex >= 0 ) && ( nIndex < MAX_CHANNELS ) )
		piChanVect[ nIndex ] = nChannel;
}

short CNSAnalogIn::GetChannelVector( int nIndex )
{
	if( ( nIndex >= 0 ) && ( nIndex < MAX_CHANNELS ) )
		return piChanVect[ nIndex ];
	else
		return -1;
}

void CNSAnalogIn::SetOutputType( DAQOutputType OutputType )
{
	if( OutputType == EOT_RAW ) m_nOutputType = OT_RAW;
	else if( OutputType == EOT_VOLTS ) m_nOutputType = OT_VOLTS;
	else m_nOutputType = OT_NEWTONS;
}

void CNSAnalogIn::Stop()
{
	_TerminateThread = TRUE;
	HINSTANCE hMod = ::GetPadInstance();
	if( hMod )
	{
		LPFNDAQ_CLEAR proc = NULL;
		proc = (LPFNDAQ_CLEAR)GetProcAddress( hMod, "DAQ_Clear" );
		if( proc )
		{
			(*proc) ( m_Device );
		}
		else FreePadInstance();
	}
}

void CNSAnalogIn::RunImmed( short* buffer, double scanrate, unsigned long samplecount )
{
	if( !buffer ) return;

    short iStatus = 0;

	// get dll instance
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;

	LPFNDAQ_STOPTRIGGER_CONFIG procSTC = NULL;
	procSTC = (LPFNDAQ_STOPTRIGGER_CONFIG)GetProcAddress( hMod, _T("DAQ_StopTrigger_Config") );
	if( !procSTC ) return;
	LPFNTIMEOUT_CONFIG procTO = NULL;
	procTO = (LPFNTIMEOUT_CONFIG)GetProcAddress( hMod, _T("Timeout_Config") );
	if( !procTO ) return;
	LPFNSCAN_OP procSO = NULL;
	procSO = (LPFNSCAN_OP)GetProcAddress( hMod, _T("SCAN_Op") );
	if( !procSO ) return;
	LPFNSCAN_DEMUX procSD = NULL;
	procSD = (LPFNSCAN_DEMUX)GetProcAddress( hMod, _T("SCAN_Demux") );
	if( !procSD ) return;

	// Enables the pretrigger mode of data acquisition and indicates the number of
	// data points to acquire after the stop trigger pulse is applied at the
	// the PFI1 pin of an E Series device
	(*procSTC) ( m_Device, 0, samplecount / 3 );

	// adjust to sampling rate/*channels/num samples
	iStatus = (*procTO) ( m_Device, 180 );

	// acquire data
	iStatus = (*procSO) ( m_Device, m_Channels, piChanVect, &m_Gains[ 0 ], buffer,
						  samplecount * m_Channels, 100000, scanrate );

	// rearrage data
	iStatus = (*procSD) ( buffer, samplecount * m_Channels, m_Channels, 0 );
}

void CNSAnalogIn::GetBaseline( double scanrate )
{
	short iStatus = 0;

	// acquire m_nBaselineCount and average to get a baseline
	memset( m_Offsets, 0, MAX_CHANNELS * sizeof( double ) );

	// buffer
	short* tempbuffer = new short[ m_nBaselineCount * m_Channels ];

	RunImmed( tempbuffer, scanrate, m_nBaselineCount );

	// average
	for( int i = 0; i < m_Channels; i++ )
	{
		m_Offsets[ i ] = 0.;
		for( unsigned long j = 0; j < (unsigned long)m_nBaselineCount; j++ )
		{
			m_Offsets[ i ] = m_Offsets[ i ] + (*( tempbuffer + i * AVG_SAMPLES ));
		}
		m_Offsets[ i ] = m_Offsets[ i ] / m_nBaselineCount;
	}

	// hardcode 0 offset of trigger channel
	m_Offsets[ 3 ] = 0.;

	// cleanup
	if( tempbuffer ) delete [] tempbuffer;

	m_fGotBaseline = TRUE;
}

void CNSAnalogIn::RunImmedAsynch( double scanrate, unsigned long samplecount )
{
    short iStatus = 0;
    short iSampTB = 0;
    unsigned short uSampInt = 0;
    short iScanTB = 0;
    unsigned short uScanInt = 0;

	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;

	LPFNDAQ_RATE procDR = NULL;
	procDR = (LPFNDAQ_RATE)GetProcAddress( hMod, "DAQ_Rate" );
	LPFNSCAN_SETUP procSS = NULL;
	procSS = (LPFNSCAN_SETUP)GetProcAddress( hMod, "SCAN_Setup" );
	LPFNSCAN_START procSSt = NULL;
	procSSt = (LPFNSCAN_START)GetProcAddress( hMod, "SCAN_Start" );
	LPFNCONFIG_DAQ_EVENT_MESSAGE procCD = NULL;
	procCD = (LPFNCONFIG_DAQ_EVENT_MESSAGE)GetProcAddress( hMod, "Config_DAQ_Event_Message" );

	if( !procDR || !procSS || !procSSt || !procCD )
	{
		::OutputMessage( _T("Unable to load NiDAQ library proc."), LOG_TABLET );
		FreePadInstance();
		return;
	}

	// Set magnet force
	SetForce( m_dMagnetForce );
	// Turn on magnet
	MagnetLatch();

	// Get baseline
//	if( ( ( m_nOutputType == OT_VOLTS ) || ( m_nOutputType == OT_NEWTONS ) ) &&
//		!m_fGotBaseline )
	if( !m_fGotBaseline ) GetBaseline( scanrate );

	// Convert sample rate (S/sec) to appropriate timebase and sample
	// interval values. (NOT scan interval values) */
	iStatus = (*procDR) ( m_SampleRate, 0 /* pts/s */, &iSampTB, &uSampInt );

	// Convert scan rate rate (S/sec) to appropriate timebase and
	// sample interval values. (NOT scan interval values) */
	iStatus = (*procDR) ( scanrate, 0 /* pts/s */, &iScanTB, &uScanInt );

	// Setup for scan
	iStatus = (*procSS) ( m_Device, m_Channels, piChanVect, &m_Gains[ 0 ] );

	// Description of the trigger analog channel or digital port
	// Format is : AIn or AOn where n = trigger channel (AI0:2  for multiple chans)
	// Channel is ignored with DAQEvent message = 2  ... inform when completed
	char pszChanStr[ 15 ];
	sprintf_s( pszChanStr, "AI%d", piChanVect[ 0 ] );
	/* note:  Message type 0 should be sent after the first N scans (set N to 1 for start) are completed,
	however, on the DAQ pad, this event isn;t generated until the data is transfered to the output buffer,
	which, for small acqusitions, is after acqusition is completed.  Alternatively, message type 1 send a message
	after every N scans and seems to work prior to end of acqusition. */
	// send a message when done
	iStatus = (*procCD) ( m_Device,		// Device (short)
						  1,			// Add a new msg..0 to clear...init? (short)
						  pszChanStr,	// Channel description (char FAR*)
						  2,			// DAQ Event msg..when stopped (short)
						  0,			// trigger 0 (long)
						  0,			// trigger 1 (long)
						  0,			// trig skip count (ulong)
						  0,			// pre trig scans (ulong)
						  0,			// post trig scans (ulong)
						  m_hwnd,		// handle to window receiving msgs (short)
						  WM_USER+101,	// message we want returned when triggered (short)
						  NULL );		// callback addr (ulong) 0 if none

	// raw data buffer
	if( m_TempBuffer ) delete [] m_TempBuffer;
	m_TempBuffer = new short[ samplecount * m_Channels ];
	memset( m_TempBuffer, 0, samplecount * m_Channels * sizeof( short ) );
	// volts/newtons data buffer (if necessary)
	if( m_dTempBuffer ) delete [] m_dTempBuffer;
	if( ( m_nOutputType == OT_VOLTS ) || ( m_nOutputType == OT_NEWTONS ) )
	{
		// buffer
		m_dTempBuffer = new double[ samplecount * m_Channels ];
		memset( m_dTempBuffer, 0, samplecount * m_Channels * sizeof( double ) );
		// calculate gains
		for( int i = 0; i < MAX_CHANNELS; i++ )
		{
			if( ( m_pFullScaleLoad[ i ] != 0. ) && ( m_pExcitationVoltage[ i ] != 0. ) )
			{
				m_pVtoN[ i ] = 1 / ( m_pCalibrationConstant[ i ] / MVV *
									m_pExcitationVoltage[ i ] / LBSN /
									m_pFullScaleLoad[ i ] );
			}
		}
	}
	else m_dTempBuffer = NULL;

    /* Start Acquire data from multiple channels. */
	iStatus = (*procSSt) ( m_Device, m_TempBuffer, samplecount * m_Channels,
						   iSampTB, uSampInt, iScanTB, uScanInt );
}

BOOL CNSAnalogIn::GetData( double* Buffer, unsigned long samplecount )
{
    short iStatus = 0;

	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return FALSE;

	LPFNSCAN_DEMUX procSD = NULL;
	procSD = (LPFNSCAN_DEMUX)GetProcAddress( hMod, "SCAN_Demux" );
	LPFNDAQ_VSCALE procDV = NULL;
	procDV = (LPFNDAQ_VSCALE)GetProcAddress( hMod, "DAQ_VScale" );
	if( !procSD || !procDV )
	{
		::OutputMessage( _T("Unable to load NiDAQ library proc."), LOG_TABLET );
		FreePadInstance();
		return FALSE;
	}

	iStatus = (*procSD) ( m_TempBuffer, samplecount * m_Channels, m_Channels, 0 );

	// convert to volts if necessary
	// volts are necessary for newtons as well
	if( ( m_nOutputType == OT_VOLTS ) || ( m_nOutputType == OT_NEWTONS ) )
	{
		for( int i = 0; i < m_Channels; i++ )
		{
			(*procDV) ( m_Device, i, m_Gains[ i ], 1, m_Offsets[ i ], samplecount,
						m_TempBuffer + i * samplecount, Buffer + i * samplecount );
		}

		for( unsigned long j = 0; j < samplecount * m_Channels; j++ )
			m_dTempBuffer[ j ] = Buffer[ j ];
	}

	// turn magnet off
	MagnetOff();

	return TRUE;
}

void CNSAnalogIn::WriteDrawPoints( CDC* pDC )
{
	// we write to file regardless of device context pointer

	// we obviously need data (this should be raw...TODO: what about volts etc)
	if( !m_TempBuffer ) return;
	BOOL fVorN = ( ( m_nOutputType == OT_VOLTS ) || ( m_nOutputType == OT_NEWTONS ) );
	if( fVorN && !m_dTempBuffer ) return;

	CStdioFile f;
	if( !f.Open( m_szFile, CFile::modeWrite | CFile::modeCreate ) ) return;
	CString szLine;

	RECT rect;
	POINT size;
	short x, y, z;
	double dx, dy, dz;
	long lx = 0, ly = 0, lz = 0;
	int nx = 0, ny = 0, nz = 0, nxold = 0, nyold = 0, nzold = 0;
	bool fFirst = TRUE;

	// get drawing window dimensions
	::GetWindowRect( m_hwnd, &rect );
	size.x = rect.right - rect.left;
	size.y = rect.bottom - rect.top;

	for( unsigned long i = 0; i < m_Samples; i++ )
	{
		// raw data values
		dx = m_TempBuffer[ i ];
		dy = m_TempBuffer[ m_Samples + i ];
		if( m_Channels >= 3 ) dz = m_TempBuffer[ m_Samples * 2 + i ];
		else dz = 0.;

		x = (short)dx;
		y = (short)dy;
		z = (short)dz;

		// volt/newton converted values
		if( fVorN )
		{
			dx = m_dTempBuffer[ i ];
			dy = m_dTempBuffer[ m_Samples + i ];
			if( m_Channels >= 3 ) dz = m_dTempBuffer[ m_Samples * 2 + i ];
			else dz = 0;

			if( m_nOutputType == OT_NEWTONS )
			{
				dx *= m_pVtoN[ piChanVect[ CHAN_LOWER] ];
				dy *= m_pVtoN[ piChanVect[ CHAN_UPPER ] ];
				dz *= m_pVtoN[ piChanVect[ CHAN_LOAD ] ];
			}
		}

		// we write the data depending on the desired format (raw, V, N)
		// write
		if( fVorN )
			szLine.Format( _T("%g %g %g\n"), dx, dy, dz );
		else
			szLine.Format( _T("%g %g %g\n"), (double)x, (double)y, (double)z );
		f.WriteString( szLine );

		// to draw we only use raw
		// only calc if we have legit values
		if( ( x >= 0 ) && ( x <= DAQPAD_RANGE ) &&
			( y >= 0 ) && ( y <= DAQPAD_RANGE ) &&
			( z >= 0 ) && ( z <= DAQPAD_RANGE ) )
		{
			// convert raw device values to screen
// 10Jun09: GMB: No spacing between graph lines
// 			lx = rect.bottom - (long)( ( x - m_Offsets[ CHAN_LOWER ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
// 			ly = rect.bottom - (long)( ( y - m_Offsets[ CHAN_UPPER ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
// 			lz = rect.bottom - (long)( ( z - m_Offsets[ CHAN_LOAD ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
			// 10Jun09: Somesh: All the channels need to be lowered a little bit
			lx = rect.bottom - (long)( (x - m_Offsets[ CHAN_LOAD ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
			ly = rect.bottom - (long)( (y - m_Offsets[ CHAN_LOAD ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
			lz = rect.bottom - (long)( (z - m_Offsets[ CHAN_LOAD ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;

			if( fFirst )
			{
				nxold = lx;
				nyold = ly;
				nzold = lz;
			}
			else
			{
				nxold = nx;
				nyold = ny;
				nzold = nz;
			}

			nx = lx;
			ny = ly;
			nz = lz;

			::DrawGripper( pDC, nx, ny, nz, nxold, nyold, nzold,
						   m_Samples, m_hwnd, TRUE, fFirst, FALSE, DAQPAD_RANGE );
			if( fFirst ) fFirst = FALSE;
		}
		else
			::OutputMessage( _T("Data values reported by hardware is out of range {0,4000}. Will not display."), LOG_TABLET );
	}
}

void CNSAnalogIn::RunWithMultipleBuffering( double scanrate, unsigned long samplecount )
{
	short iStatus = 0;
    short iSampTB = 0;
    unsigned short uSampInt = 0;
    short iScanTB = 0;
    unsigned short uScanInt = 0;

	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;

	LPFNDAQ_RATE procDR = NULL;
	procDR = (LPFNDAQ_RATE)GetProcAddress( hMod, "DAQ_Rate" );
	LPFNSCAN_SETUP procSS = NULL;
	procSS = (LPFNSCAN_SETUP)GetProcAddress( hMod, "SCAN_Setup" );
	LPFNSCAN_START procSSt = NULL;
	procSSt = (LPFNSCAN_START)GetProcAddress( hMod, "SCAN_Start" );
	LPFNDAQ_DB_CONFIG procDDBC = NULL;
	procDDBC = (LPFNDAQ_DB_CONFIG)GetProcAddress( hMod, "DAQ_DB_Config" );

	if( !procDR || !procSS || !procSSt || !procDDBC )
	{
		::OutputMessage( _T("Unable to load NiDAQ library proc."), LOG_TABLET );
		FreePadInstance();
		return;
	}

	// Get baseline
	//if( ( ( m_nOutputType == OT_VOLTS ) || ( m_nOutputType == OT_NEWTONS ) ) &&
	//	!m_fGotBaseline )
	if( !m_fGotBaseline ) GetBaseline( scanrate );

	// Turn on software double-buffered mode
	iStatus = (*procDDBC) ( m_Device, 1 );

	// Convert sample rate (S/sec) to appropriate timebase and sample
	// interval values. (NOT scan interval values) */
	iStatus = (*procDR) ( m_SampleRate, 0 /* pts/s */, &iSampTB, &uSampInt );

	// Convert scan rate rate (S/sec) to appropriate timebase and
	// sample interval values. (NOT scan interval values) */
	iStatus = (*procDR) ( scanrate, 0 /* pts/s */, &iScanTB, &uScanInt );

	// Setup for scan
	iStatus = (*procSS) ( m_Device, m_Channels, piChanVect, &m_Gains[ 0 ] );

	/* Start Acquire data from multiple channels. */
	// this buffer is just big enough to hold samplecount
	if( m_TempBuffer ) delete [] m_TempBuffer;
	m_TempBuffer = new short[ samplecount * m_Channels ];
	memset( m_TempBuffer, 0, samplecount * m_Channels * sizeof( short ) );
	// volts/newtons data buffer (if necessary)
	if( m_dTempBuffer ) delete [] m_dTempBuffer;
	if( ( m_nOutputType == OT_VOLTS ) || ( m_nOutputType == OT_NEWTONS ) )
	{
		// buffer
		m_dTempBuffer = new double[ samplecount * m_Channels ];
		memset( m_dTempBuffer, 0, samplecount * m_Channels * sizeof( double ) );
		// calculate gains
		for( int i = 0; i < MAX_CHANNELS; i++ )
		{
			if( ( m_pFullScaleLoad[ i ] != 0. ) && ( m_pExcitationVoltage[ i ] != 0. ) )
			{
				m_pVtoN[ i ] = 1 / ( m_pCalibrationConstant[ i ] / MVV *
									m_pExcitationVoltage[ i ] / LBSN /
									m_pFullScaleLoad[ i ] );
			}
		}
	}
	else m_dTempBuffer = NULL;

	// Start the scan
	iStatus = (*procSSt) ( m_Device, m_TempBuffer, samplecount * m_Channels,
						   iSampTB, uSampInt, iScanTB, uScanInt );

	// initialize
	_TerminateThread = FALSE;
	_hwnd = m_hwnd;
	_Buffer = m_TempBuffer;
	_Device = m_Device;
	_Channels = m_Channels;
	_Samples = m_Samples;
	_SampleCount = samplecount;
	_First = FALSE;
	_File = m_szFile;
	_OutputType = m_nOutputType;
	_Gains = m_Gains;
	_VtoN = m_pVtoN;
	_ChanVect = piChanVect;
	_Offsets = m_Offsets;
//	_pThis = this;

	// Set magnet force
	SetForce( m_dMagnetForce );
	// Turn on magnet
	MagnetLatch();

	// start the thread for acquiring data
	CWinThread* pThreadA = AfxBeginThread( DoAcquire, 0 );
}

UINT DoAcquire( LPVOID pParam )
{
	// handle to daq pad
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return 0;

	if( ( _Samples <= 1 ) || ( _Channels < 1 ) || (_SampleCount <= 1 ) )
		return 0;

	// get function pointers
	LPFNDAQ_DB_HALFREADY procHR = NULL;
	procHR = (LPFNDAQ_DB_HALFREADY)GetProcAddress( hMod, "DAQ_DB_HalfReady" );
	LPFNDAQ_DB_TRANSFER procDBT = NULL;
	procDBT = (LPFNDAQ_DB_TRANSFER)GetProcAddress( hMod, "DAQ_DB_Transfer" );
	LPFNDAQ_VSCALE procDV = NULL;
	procDV = (LPFNDAQ_VSCALE)GetProcAddress( hMod, "DAQ_VScale" );
	if( !procDBT || !procHR || !procDV ) return 0;

    short iStatus = 0;
	long lPass = 0;
	unsigned long ulCount = 0, pts = 0;
	short iStopped = 0, iIsReady = 0;
	BOOL fIsReady = FALSE;
	BOOL fStopped = FALSE;
	short*	BufferAll = NULL;
	BufferAll = new short[ _Samples * _Channels ];
	memset( BufferAll, 0, _Samples * _Channels * sizeof( short ) );

	// drawing-related vars
	RECT rect;
	POINT size;
	short x = 0, y = 0, z = 0;
	long lx = 0, ly = 0, lz = 0;
	int nx = 0, ny = 0, nz = 0, nxold = 0, nyold = 0, nzold = 0;
	static bool fFirst;
	fFirst = TRUE;

	// get drawing window dimensions
	::GetWindowRect( _hwnd, &rect );
	size.x = rect.right - rect.left;
	size.y = rect.bottom - rect.top;
	HDC hdc = ::GetDC( _hwnd );
	CDC* pdc = CDC::FromHandle( hdc );

	// continue polling until daqpad indicates it has stopped or the thread is terminated
	while( !fStopped && !_TerminateThread )
	{
		if( fIsReady )
		{
//_pThis->SetForce( _pThis->m_dMagnetForce );
//_pThis->MagnetLatch();
			// if, for some reason, the samplecount exceeds the total # of samples, break
			ulCount += ( _SampleCount / 2 );
			if( ulCount > _Samples ) break;

			// transfer the 1/2 buffer
			iStatus = (*procDBT) ( _Device, _Buffer, &pts, &iStopped );
			if( ( pts <= 0 ) || ( pts > ( _SampleCount / 2 * _Channels ) ) ) return 0;

			// loop through points returned (should be samplecount / 2)
			// and store in big (all points) buffer
			for( unsigned long i = 0; i < ( pts / _Channels ); i++ )
			{
				// scans are stored in buffer as follows
				// Channel 0 = xyzxyzxyzxyz...up to pts / channels
				// Channel 1 = xyzxyzxyzxyz...from pts / channels + 1 to 2 * pts / channels
				// Channel 2 = xyzxyzxyzxyz...from 2 * pts / channels + 1 to 3 * pts / channels

				// x
				x = BufferAll[ lPass * _SampleCount / 2 + i ] =  _Buffer[ i * _Channels ];
				// y
				if( _Channels > 1 )
					y = BufferAll[ _Samples + lPass * _SampleCount / 2 + i ] = _Buffer[ i * _Channels + 1 ];
				else y = 0;
				// z
				if( _Channels > 2 )
					z = BufferAll[ _Samples * 2 + lPass * _SampleCount / 2 + i ] = _Buffer[ i * _Channels + 2 ];
				else z = 0;

				// only calc if we have legit values
				if( ( x >= 0 ) && ( x <= DAQPAD_RANGE ) &&
					( y >= 0 ) && ( y <= DAQPAD_RANGE ) &&
					( z >= 0 ) && ( z <= DAQPAD_RANGE ) )
				{

					// convert raw device values to screen
					lx = rect.bottom - (long)( ( x - _Offsets[ CHAN_LOWER ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
					ly = rect.bottom - (long)( ( y - _Offsets[ CHAN_UPPER ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
					lz = rect.bottom - (long)( ( z - _Offsets[ CHAN_LOAD ] ) * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
					// 10Jun09: Somesh: All the channels need to be lowered a little bit
					/*lx = rect.bottom - (long)( ( x - _Offsets[ CHAN_LOAD ] - 200 ) * 2 * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
					ly = rect.bottom - (long)( ( y - _Offsets[ CHAN_LOAD ] - 200 ) * 2 * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;
					lz = rect.bottom - (long)( ( z - _Offsets[ CHAN_LOAD ]		 ) * 2 * size.y / DAQPAD_RANGE ) - BUFFER_BOTTOM;*/

					if( fFirst )
					{
						nxold = lx;
						nyold = ly;
						nzold = lz;
					}
					else
					{
						nxold = nx;
						nyold = ny;
						nzold = nz;
					}

					nx = lx;
					ny = ly;
					nz = lz;

					::DrawGripper( pdc, nx, ny, nz, nxold, nyold, nzold,
								   _Samples, _hwnd, TRUE, fFirst, FALSE, DAQPAD_RANGE );
					if( fFirst ) fFirst = FALSE;
				}
				else
					::OutputMessage( _T("Data values reported by hardware is out of range {0,4000}. Will not display."), LOG_TABLET );

			}

			lPass++;
		}

		// Ask device if it has acquired 1/2 the buffer yet
		iStatus = (*procHR) ( _Device, &iIsReady, &iStopped );
		fIsReady = ( iIsReady == 1 );
		fStopped = ( iStopped == 1 );
	}

	// conversions first (if necessary)
	BOOL fVorN = ( ( _OutputType == OT_VOLTS ) || ( _OutputType == OT_NEWTONS ) );
	BOOL fN = ( _OutputType == OT_NEWTONS );
	double*	BufferWrite = NULL;
	BufferWrite = new double[ _Samples * _Channels ];
	memset( BufferWrite, 0, _Samples * _Channels * sizeof( double ) );
	if( fVorN )
	{
		for( int i = 0; i < _Channels; i++ )
		{
			(*procDV) ( _Device, i, _Gains[ i ], 1, _Offsets[ i ], _Samples,
						BufferAll + i * _Samples, BufferWrite + i * _Samples );

			// to newtons
			if( fN )
			{
				for( unsigned long j = 0; j < _Samples; j++ )
				{
					// TODO: We are only assuming 3 channels thus far
					if( i == 0 )
						BufferWrite[ i * _Samples + j ] *= _VtoN[ _ChanVect[ CHAN_LOWER ] ];
					else if( i == 1 )
						BufferWrite[ i * _Samples + j ] *= _VtoN[ _ChanVect[ CHAN_UPPER ] ];
					else
						BufferWrite[ i * _Samples + j ] *= _VtoN[ _ChanVect[ CHAN_LOAD ] ];
				}
			}
		}
	}
	else
	{
		for( unsigned long j = 0; j < _Samples * _Channels; j++ )
			BufferWrite[ j ] = BufferAll[ j ];
	}

	// now that we're done acquiring all points, write to file
	if( ulCount > _Samples ) ulCount = _Samples;
	CString szLine;
	CStdioFile f;
	if( f.Open( _File, CFile::modeWrite | CFile::modeCreate ) )
	{
		for( unsigned long i = 0; i < ulCount; i++ )
		{
			szLine.Format( _T("%g %g %g\n"),
				(double)BufferWrite[ i ],
				( _Channels > 1 ) ? (double)BufferWrite[ _Samples + i ] : 0.0,
				( _Channels > 2 ) ? (double)BufferWrite[ _Samples * 2 + i ] : 0.0 );
			f.WriteString( szLine );
		}
	}

	// magnet off (TODO: How?)
	//MagnetOff();
	LPFNDIG_OUT_LINE proc = NULL;
	proc = (LPFNDIG_OUT_LINE)GetProcAddress( hMod, "DIG_Out_Line" );
	if( proc ) (*proc) ( _Device, 0, MAGNET_ENABLEn, 1 );

	// cleanup
	delete [] BufferAll;
	delete [] BufferWrite;

	return 0;
}

/* Magnet Control Summary
/
/  Four digital I/O lines are connected to the magnet control circuitry.  The magnet is on whenever the
/  magnet power enable bit is set.  The magnet is then set to the pre-trigger polarity by setting bit 1 and
/  then bit 0.  To arm the magnet, bit zero should then be unset.  At this time, the monitor bit should be
/  set indicating the magnet is in pre-trigger polarity.  After triggering, the monitor bit should be low indicating
/  the trigger occured.
/ 
/  The assignments are: 0 - Magnet latch
/                       1 - Magnet unlatch
/                       2 - Monitor magnet polarity
/                       3 - Enable magnet power
*/

void CNSAnalogIn::MagnetOn()
{
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;

	// Rev logic
	LPFNDIG_OUT_LINE proc = NULL;
	proc = (LPFNDIG_OUT_LINE)GetProcAddress( hMod, "DIG_Out_Line" );
	if( proc ) (*proc) ( m_Device, 0, MAGNET_ENABLEn, 0 );
}


void CNSAnalogIn::MagnetOff()
{
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;

	// Rev logic
	LPFNDIG_OUT_LINE proc = NULL;
	proc = (LPFNDIG_OUT_LINE)GetProcAddress( hMod, "DIG_Out_Line" );
	if( proc ) (*proc) ( m_Device, 0, MAGNET_ENABLEn, 1 );
}

bool CNSAnalogIn::MagnetCheck()
{
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return FALSE;

	short state = 0;

	LPFNDIG_IN_LINE proc = NULL;
	proc = (LPFNDIG_IN_LINE)GetProcAddress( hMod, "DIG_In_Line" );
	if( proc ) (*proc) ( m_Device, 0, MAGNET_CHECK, &state );

	return ( state != 0 );
}

void CNSAnalogIn::MagnetLatch()
{
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;

	// Turn the magnet on
	MagnetOn();

	LPFNDIG_OUT_LINE proc = NULL;
	proc = (LPFNDIG_OUT_LINE)GetProcAddress( hMod, "DIG_Out_Line" );
	if( proc ) (*proc) ( m_Device, 0, MAGNET_UNSETn, 1 );
	if( proc ) (*proc) ( m_Device, 0, MAGNET_SET, 1 );
	Sleep( 100 );
	if( proc ) (*proc) ( m_Device, 0, MAGNET_SET, 0 );
}


/* Force Control Summary
/
/  The force is controlled by the magnet and by monitoring the force of the magnet's load cell.
/  The load cell is monitored in hardware.  The hardware has a comparator and montinuously
/  compared the force generated at the magnet load cell and analog output 0.  Of course, the 
/  hardware works in volts, not newton's, and the # newton's per volt depends on the calibration of
/  that load cell.  Thus, if the user enters a newton value for force generation, it must be converted
/  to volts, and that value output on analog output 0.
*/

void CNSAnalogIn::SetForce( double force )
{
	HINSTANCE hMod = ::GetPadInstance();
	if( !hMod ) return;
	// note, m_Offset is approx 5 volts
	//TODO: Documentation needed for the numbers in the equations
	double trig_v = force / m_Scale [ CHAN_LOWER ] + ( m_Offsets [ CHAN_LOWER ] / 4096 * 10 );

	// write threshold for force
	LPFNAO_VWRITE proc = NULL;
	proc = (LPFNAO_VWRITE)GetProcAddress( hMod, "AO_VWrite" );
	if( proc ) (*proc) ( m_Device, 0, trig_v );
}

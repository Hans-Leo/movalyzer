

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0500 */
/* at Sat Mar 04 21:54:53 2017
 */
/* Compiler settings for .\InputMod.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __InputMod_h__
#define __InputMod_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDigitizer_FWD_DEFINED__
#define __IDigitizer_FWD_DEFINED__
typedef interface IDigitizer IDigitizer;
#endif 	/* __IDigitizer_FWD_DEFINED__ */


#ifndef __IDAQPad_FWD_DEFINED__
#define __IDAQPad_FWD_DEFINED__
typedef interface IDAQPad IDAQPad;
#endif 	/* __IDAQPad_FWD_DEFINED__ */


#ifndef __Digitizer_FWD_DEFINED__
#define __Digitizer_FWD_DEFINED__

#ifdef __cplusplus
typedef class Digitizer Digitizer;
#else
typedef struct Digitizer Digitizer;
#endif /* __cplusplus */

#endif 	/* __Digitizer_FWD_DEFINED__ */


#ifndef __DAQPad_FWD_DEFINED__
#define __DAQPad_FWD_DEFINED__

#ifdef __cplusplus
typedef class DAQPad DAQPad;
#else
typedef struct DAQPad DAQPad;
#endif /* __cplusplus */

#endif 	/* __DAQPad_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_InputMod_0000_0000 */
/* [local] */ 

#pragma once


extern RPC_IF_HANDLE __MIDL_itf_InputMod_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_InputMod_0000_0000_v0_0_s_ifspec;

#ifndef __IDigitizer_INTERFACE_DEFINED__
#define __IDigitizer_INTERFACE_DEFINED__

/* interface IDigitizer */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IDigitizer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("93FCD94F-9BB1-11D3-8A30-00104BC7E2C8")
    IDigitizer : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetOutputWindow( 
            /* [in] */ VARIANT *MsgWindow) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TabletWindow( 
            /* [in] */ VARIANT *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TargetWindow( 
            /* [in] */ VARIANT *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OutputFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Initialize( 
            /* [in] */ VARIANT *DC,
            /* [in] */ BOOL Stimulus,
            /* [defaultvalue][in] */ BOOL UseMouse = FALSE) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Draw( 
            /* [in] */ VARIANT *DC) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnWTPacket( 
            /* [in] */ long wSerial,
            /* [in] */ long hCtx,
            /* [in] */ VARIANT *DC,
            /* [retval][out] */ BOOL *invalidate) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TimeoutTicks( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Instruction( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE OnMouseMove( 
            /* [in] */ long Flags,
            /* [in] */ VARIANT *Point,
            /* [in] */ VARIANT *DC,
            /* [retval][out] */ BOOL *invalidate) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE TargetEqualsTablet( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE TabletWindowOnly( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SamplingRate( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MinPenPressure( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DeviceResolution( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InitializeForStimulus( 
            /* [in] */ VARIANT *DC,
            /* [defaultvalue][in] */ BOOL UseMouse = FALSE) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BkColor( 
            /* [in] */ DWORD newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NotifyOfBadTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RecordImmediately( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopWrongTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletDesc( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletXRange( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletYRange( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletRate( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_TabletResolution( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Start( 
            /* [in] */ VARIANT *DC,
            /* [in] */ BOOL Stimulus,
            /* [defaultvalue][in] */ BOOL UseMouse = FALSE) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Clear( void) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_TabletDimension( 
            /* [in] */ double dtX,
            /* [in] */ double dtY) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisplayDimension( 
            /* [in] */ double ddX,
            /* [in] */ double ddrY) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetStimulusInfo( 
            /* [in] */ BSTR StimPath,
            /* [in] */ BSTR StimID,
            /* [in] */ BOOL DeactivateWrongTarget) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DoOnTimer( 
            /* [in] */ long Increment,
            /* [in] */ VARIANT *DC) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Transformation( 
            /* [in] */ BOOL Transform,
            /* [in] */ double xgain,
            /* [in] */ double xrotation,
            /* [in] */ double ygain,
            /* [in] */ double yrotation) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CountStrokes( 
            /* [in] */ BOOL Count,
            /* [in] */ short MaxStrokes) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE HideShowAfterStrokes( 
            /* [in] */ BOOL HideShowAfter,
            /* [in] */ BOOL Show,
            /* [in] */ double Strokes) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE HideShow( 
            /* [in] */ BOOL Show) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LeftViewMaximizeSize( 
            /* [in] */ int newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RealSize( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_NoTracking( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StartFirstTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_StopLastTarget( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DrawSettings( 
            /* [in] */ BOOL LineFixed,
            /* [in] */ short LineSize,
            /* [in] */ DWORD LineColor1,
            /* [in] */ DWORD LineColor2,
            /* [in] */ DWORD LineColor3,
            /* [in] */ DWORD MPPColor,
            /* [in] */ DWORD EllipseColor,
            /* [in] */ short EllipseSize,
            /* [in] */ DWORD BGColor) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_VaryLineThickness( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxLineThickness( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SupportsTilt( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_UseTilt( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_MaxPenPressure( 
            /* [retval][out] */ short *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MaxPenPressure( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisError( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_DisNotify( 
            /* [in] */ BOOL newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAuthorization( 
            /* [in] */ BSTR AuthCode) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_RedoIfWrongTarget( 
            /* [in] */ BOOL newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDigitizerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDigitizer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDigitizer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDigitizer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDigitizer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDigitizer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDigitizer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDigitizer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetOutputWindow )( 
            IDigitizer * This,
            /* [in] */ VARIANT *MsgWindow);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TabletWindow )( 
            IDigitizer * This,
            /* [in] */ VARIANT *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TargetWindow )( 
            IDigitizer * This,
            /* [in] */ VARIANT *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OutputFile )( 
            IDigitizer * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Initialize )( 
            IDigitizer * This,
            /* [in] */ VARIANT *DC,
            /* [in] */ BOOL Stimulus,
            /* [defaultvalue][in] */ BOOL UseMouse);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Draw )( 
            IDigitizer * This,
            /* [in] */ VARIANT *DC);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnWTPacket )( 
            IDigitizer * This,
            /* [in] */ long wSerial,
            /* [in] */ long hCtx,
            /* [in] */ VARIANT *DC,
            /* [retval][out] */ BOOL *invalidate);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TimeoutTicks )( 
            IDigitizer * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Instruction )( 
            IDigitizer * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *OnMouseMove )( 
            IDigitizer * This,
            /* [in] */ long Flags,
            /* [in] */ VARIANT *Point,
            /* [in] */ VARIANT *DC,
            /* [retval][out] */ BOOL *invalidate);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IDigitizer * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TargetEqualsTablet )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TabletWindowOnly )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SamplingRate )( 
            IDigitizer * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MinPenPressure )( 
            IDigitizer * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DeviceResolution )( 
            IDigitizer * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InitializeForStimulus )( 
            IDigitizer * This,
            /* [in] */ VARIANT *DC,
            /* [defaultvalue][in] */ BOOL UseMouse);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BkColor )( 
            IDigitizer * This,
            /* [in] */ DWORD newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NotifyOfBadTarget )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RecordImmediately )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopWrongTarget )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IDigitizer * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletDesc )( 
            IDigitizer * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletXRange )( 
            IDigitizer * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletYRange )( 
            IDigitizer * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletRate )( 
            IDigitizer * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabletResolution )( 
            IDigitizer * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Start )( 
            IDigitizer * This,
            /* [in] */ VARIANT *DC,
            /* [in] */ BOOL Stimulus,
            /* [defaultvalue][in] */ BOOL UseMouse);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Clear )( 
            IDigitizer * This);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TabletDimension )( 
            IDigitizer * This,
            /* [in] */ double dtX,
            /* [in] */ double dtY);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisplayDimension )( 
            IDigitizer * This,
            /* [in] */ double ddX,
            /* [in] */ double ddrY);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetStimulusInfo )( 
            IDigitizer * This,
            /* [in] */ BSTR StimPath,
            /* [in] */ BSTR StimID,
            /* [in] */ BOOL DeactivateWrongTarget);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DoOnTimer )( 
            IDigitizer * This,
            /* [in] */ long Increment,
            /* [in] */ VARIANT *DC);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Transformation )( 
            IDigitizer * This,
            /* [in] */ BOOL Transform,
            /* [in] */ double xgain,
            /* [in] */ double xrotation,
            /* [in] */ double ygain,
            /* [in] */ double yrotation);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CountStrokes )( 
            IDigitizer * This,
            /* [in] */ BOOL Count,
            /* [in] */ short MaxStrokes);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *HideShowAfterStrokes )( 
            IDigitizer * This,
            /* [in] */ BOOL HideShowAfter,
            /* [in] */ BOOL Show,
            /* [in] */ double Strokes);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *HideShow )( 
            IDigitizer * This,
            /* [in] */ BOOL Show);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LeftViewMaximizeSize )( 
            IDigitizer * This,
            /* [in] */ int newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RealSize )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NoTracking )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StartFirstTarget )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_StopLastTarget )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DrawSettings )( 
            IDigitizer * This,
            /* [in] */ BOOL LineFixed,
            /* [in] */ short LineSize,
            /* [in] */ DWORD LineColor1,
            /* [in] */ DWORD LineColor2,
            /* [in] */ DWORD LineColor3,
            /* [in] */ DWORD MPPColor,
            /* [in] */ DWORD EllipseColor,
            /* [in] */ short EllipseSize,
            /* [in] */ DWORD BGColor);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_VaryLineThickness )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxLineThickness )( 
            IDigitizer * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SupportsTilt )( 
            IDigitizer * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UseTilt )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MaxPenPressure )( 
            IDigitizer * This,
            /* [retval][out] */ short *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MaxPenPressure )( 
            IDigitizer * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisError )( 
            IDigitizer * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DisNotify )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAuthorization )( 
            IDigitizer * This,
            /* [in] */ BSTR AuthCode);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_RedoIfWrongTarget )( 
            IDigitizer * This,
            /* [in] */ BOOL newVal);
        
        END_INTERFACE
    } IDigitizerVtbl;

    interface IDigitizer
    {
        CONST_VTBL struct IDigitizerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDigitizer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDigitizer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDigitizer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDigitizer_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IDigitizer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IDigitizer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IDigitizer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IDigitizer_SetOutputWindow(This,MsgWindow)	\
    ( (This)->lpVtbl -> SetOutputWindow(This,MsgWindow) ) 

#define IDigitizer_put_TabletWindow(This,newVal)	\
    ( (This)->lpVtbl -> put_TabletWindow(This,newVal) ) 

#define IDigitizer_put_TargetWindow(This,newVal)	\
    ( (This)->lpVtbl -> put_TargetWindow(This,newVal) ) 

#define IDigitizer_put_OutputFile(This,newVal)	\
    ( (This)->lpVtbl -> put_OutputFile(This,newVal) ) 

#define IDigitizer_Initialize(This,DC,Stimulus,UseMouse)	\
    ( (This)->lpVtbl -> Initialize(This,DC,Stimulus,UseMouse) ) 

#define IDigitizer_Draw(This,DC)	\
    ( (This)->lpVtbl -> Draw(This,DC) ) 

#define IDigitizer_OnWTPacket(This,wSerial,hCtx,DC,invalidate)	\
    ( (This)->lpVtbl -> OnWTPacket(This,wSerial,hCtx,DC,invalidate) ) 

#define IDigitizer_get_TimeoutTicks(This,pVal)	\
    ( (This)->lpVtbl -> get_TimeoutTicks(This,pVal) ) 

#define IDigitizer_put_Instruction(This,newVal)	\
    ( (This)->lpVtbl -> put_Instruction(This,newVal) ) 

#define IDigitizer_OnMouseMove(This,Flags,Point,DC,invalidate)	\
    ( (This)->lpVtbl -> OnMouseMove(This,Flags,Point,DC,invalidate) ) 

#define IDigitizer_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IDigitizer_TargetEqualsTablet(This,newVal)	\
    ( (This)->lpVtbl -> TargetEqualsTablet(This,newVal) ) 

#define IDigitizer_TabletWindowOnly(This,newVal)	\
    ( (This)->lpVtbl -> TabletWindowOnly(This,newVal) ) 

#define IDigitizer_put_SamplingRate(This,newVal)	\
    ( (This)->lpVtbl -> put_SamplingRate(This,newVal) ) 

#define IDigitizer_put_MinPenPressure(This,newVal)	\
    ( (This)->lpVtbl -> put_MinPenPressure(This,newVal) ) 

#define IDigitizer_put_DeviceResolution(This,newVal)	\
    ( (This)->lpVtbl -> put_DeviceResolution(This,newVal) ) 

#define IDigitizer_InitializeForStimulus(This,DC,UseMouse)	\
    ( (This)->lpVtbl -> InitializeForStimulus(This,DC,UseMouse) ) 

#define IDigitizer_put_BkColor(This,newVal)	\
    ( (This)->lpVtbl -> put_BkColor(This,newVal) ) 

#define IDigitizer_put_NotifyOfBadTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_NotifyOfBadTarget(This,newVal) ) 

#define IDigitizer_put_RecordImmediately(This,newVal)	\
    ( (This)->lpVtbl -> put_RecordImmediately(This,newVal) ) 

#define IDigitizer_put_StopWrongTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_StopWrongTarget(This,newVal) ) 

#define IDigitizer_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IDigitizer_get_TabletDesc(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletDesc(This,pVal) ) 

#define IDigitizer_get_TabletXRange(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletXRange(This,pVal) ) 

#define IDigitizer_get_TabletYRange(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletYRange(This,pVal) ) 

#define IDigitizer_get_TabletRate(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletRate(This,pVal) ) 

#define IDigitizer_get_TabletResolution(This,pVal)	\
    ( (This)->lpVtbl -> get_TabletResolution(This,pVal) ) 

#define IDigitizer_Start(This,DC,Stimulus,UseMouse)	\
    ( (This)->lpVtbl -> Start(This,DC,Stimulus,UseMouse) ) 

#define IDigitizer_Clear(This)	\
    ( (This)->lpVtbl -> Clear(This) ) 

#define IDigitizer_put_TabletDimension(This,dtX,dtY)	\
    ( (This)->lpVtbl -> put_TabletDimension(This,dtX,dtY) ) 

#define IDigitizer_put_DisplayDimension(This,ddX,ddrY)	\
    ( (This)->lpVtbl -> put_DisplayDimension(This,ddX,ddrY) ) 

#define IDigitizer_SetStimulusInfo(This,StimPath,StimID,DeactivateWrongTarget)	\
    ( (This)->lpVtbl -> SetStimulusInfo(This,StimPath,StimID,DeactivateWrongTarget) ) 

#define IDigitizer_DoOnTimer(This,Increment,DC)	\
    ( (This)->lpVtbl -> DoOnTimer(This,Increment,DC) ) 

#define IDigitizer_Transformation(This,Transform,xgain,xrotation,ygain,yrotation)	\
    ( (This)->lpVtbl -> Transformation(This,Transform,xgain,xrotation,ygain,yrotation) ) 

#define IDigitizer_CountStrokes(This,Count,MaxStrokes)	\
    ( (This)->lpVtbl -> CountStrokes(This,Count,MaxStrokes) ) 

#define IDigitizer_HideShowAfterStrokes(This,HideShowAfter,Show,Strokes)	\
    ( (This)->lpVtbl -> HideShowAfterStrokes(This,HideShowAfter,Show,Strokes) ) 

#define IDigitizer_HideShow(This,Show)	\
    ( (This)->lpVtbl -> HideShow(This,Show) ) 

#define IDigitizer_put_LeftViewMaximizeSize(This,newVal)	\
    ( (This)->lpVtbl -> put_LeftViewMaximizeSize(This,newVal) ) 

#define IDigitizer_put_RealSize(This,newVal)	\
    ( (This)->lpVtbl -> put_RealSize(This,newVal) ) 

#define IDigitizer_put_NoTracking(This,newVal)	\
    ( (This)->lpVtbl -> put_NoTracking(This,newVal) ) 

#define IDigitizer_put_StartFirstTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_StartFirstTarget(This,newVal) ) 

#define IDigitizer_put_StopLastTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_StopLastTarget(This,newVal) ) 

#define IDigitizer_DrawSettings(This,LineFixed,LineSize,LineColor1,LineColor2,LineColor3,MPPColor,EllipseColor,EllipseSize,BGColor)	\
    ( (This)->lpVtbl -> DrawSettings(This,LineFixed,LineSize,LineColor1,LineColor2,LineColor3,MPPColor,EllipseColor,EllipseSize,BGColor) ) 

#define IDigitizer_put_VaryLineThickness(This,newVal)	\
    ( (This)->lpVtbl -> put_VaryLineThickness(This,newVal) ) 

#define IDigitizer_put_MaxLineThickness(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxLineThickness(This,newVal) ) 

#define IDigitizer_SupportsTilt(This,pVal)	\
    ( (This)->lpVtbl -> SupportsTilt(This,pVal) ) 

#define IDigitizer_put_UseTilt(This,newVal)	\
    ( (This)->lpVtbl -> put_UseTilt(This,newVal) ) 

#define IDigitizer_get_MaxPenPressure(This,pVal)	\
    ( (This)->lpVtbl -> get_MaxPenPressure(This,pVal) ) 

#define IDigitizer_put_MaxPenPressure(This,newVal)	\
    ( (This)->lpVtbl -> put_MaxPenPressure(This,newVal) ) 

#define IDigitizer_put_DisError(This,newVal)	\
    ( (This)->lpVtbl -> put_DisError(This,newVal) ) 

#define IDigitizer_put_DisNotify(This,newVal)	\
    ( (This)->lpVtbl -> put_DisNotify(This,newVal) ) 

#define IDigitizer_SetAuthorization(This,AuthCode)	\
    ( (This)->lpVtbl -> SetAuthorization(This,AuthCode) ) 

#define IDigitizer_put_RedoIfWrongTarget(This,newVal)	\
    ( (This)->lpVtbl -> put_RedoIfWrongTarget(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDigitizer_INTERFACE_DEFINED__ */


#ifndef __IDAQPad_INTERFACE_DEFINED__
#define __IDAQPad_INTERFACE_DEFINED__

/* interface IDAQPad */
/* [unique][helpstring][dual][uuid][object] */ 

typedef 
enum eDAQOutputType
    {	eOT_Raw	= 0x1,
	eOT_Volts	= 0x4,
	eOT_Newtons	= 0x8
    } 	DAQOutputType;


EXTERN_C const IID IID_IDAQPad;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("063A8E52-CB4E-46BB-A3C7-9B4B0893B7D2")
    IDAQPad : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Device( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Channels( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Samples( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_SampleRate( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ScanRate( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Gain( 
            /* [in] */ short Index,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_FullScaleLoad( 
            /* [in] */ short Channel,
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_CalibrationConstant( 
            /* [in] */ short Channel,
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ExcitationVoltage( 
            /* [in] */ short Channel,
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Record( 
            /* [in] */ BOOL DoubleBufferMode,
            /* [in] */ long DBSamples) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Stop( void) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DataVoltage( 
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DataVoltageByChannel( 
            /* [in] */ short Channel,
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DataNewtons( 
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_DataNewtonsByChannel( 
            /* [in] */ short Channel,
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetOutputWindow( 
            /* [in] */ VARIANT *MsgWindow) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetLoggingOn( 
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AcquireData( 
            /* [in] */ BOOL ForceStop) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetMessageWindow( 
            /* [in] */ VARIANT *MsgWindow) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_BaselineCount( 
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ChannelVector( 
            /* [in] */ short Index,
            /* [in] */ short newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OutputFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE WriteDrawPoints( 
            /* [in] */ VARIANT *DC) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_MagnetForce( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_LeftViewMaximizeSize( 
            /* [in] */ int newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_OutputType( 
            /* [in] */ DAQOutputType OutputType) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDAQPadVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDAQPad * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDAQPad * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDAQPad * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDAQPad * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDAQPad * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDAQPad * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDAQPad * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Device )( 
            IDAQPad * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Channels )( 
            IDAQPad * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Samples )( 
            IDAQPad * This,
            /* [in] */ long newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SampleRate )( 
            IDAQPad * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ScanRate )( 
            IDAQPad * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Gain )( 
            IDAQPad * This,
            /* [in] */ short Index,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FullScaleLoad )( 
            IDAQPad * This,
            /* [in] */ short Channel,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CalibrationConstant )( 
            IDAQPad * This,
            /* [in] */ short Channel,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ExcitationVoltage )( 
            IDAQPad * This,
            /* [in] */ short Channel,
            /* [in] */ double newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IDAQPad * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Record )( 
            IDAQPad * This,
            /* [in] */ BOOL DoubleBufferMode,
            /* [in] */ long DBSamples);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            IDAQPad * This);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataVoltage )( 
            IDAQPad * This,
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataVoltageByChannel )( 
            IDAQPad * This,
            /* [in] */ short Channel,
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataNewtons )( 
            IDAQPad * This,
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataNewtonsByChannel )( 
            IDAQPad * This,
            /* [in] */ short Channel,
            /* [in] */ long Index,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetOutputWindow )( 
            IDAQPad * This,
            /* [in] */ VARIANT *MsgWindow);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetLoggingOn )( 
            IDAQPad * This,
            /* [in] */ BOOL fOn,
            /* [in] */ BSTR AppPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AcquireData )( 
            IDAQPad * This,
            /* [in] */ BOOL ForceStop);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetMessageWindow )( 
            IDAQPad * This,
            /* [in] */ VARIANT *MsgWindow);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_BaselineCount )( 
            IDAQPad * This,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ChannelVector )( 
            IDAQPad * This,
            /* [in] */ short Index,
            /* [in] */ short newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OutputFile )( 
            IDAQPad * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *WriteDrawPoints )( 
            IDAQPad * This,
            /* [in] */ VARIANT *DC);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MagnetForce )( 
            IDAQPad * This,
            /* [in] */ double newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LeftViewMaximizeSize )( 
            IDAQPad * This,
            /* [in] */ int newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OutputType )( 
            IDAQPad * This,
            /* [in] */ DAQOutputType OutputType);
        
        END_INTERFACE
    } IDAQPadVtbl;

    interface IDAQPad
    {
        CONST_VTBL struct IDAQPadVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDAQPad_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDAQPad_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDAQPad_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDAQPad_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IDAQPad_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IDAQPad_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IDAQPad_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IDAQPad_put_Device(This,newVal)	\
    ( (This)->lpVtbl -> put_Device(This,newVal) ) 

#define IDAQPad_put_Channels(This,newVal)	\
    ( (This)->lpVtbl -> put_Channels(This,newVal) ) 

#define IDAQPad_put_Samples(This,newVal)	\
    ( (This)->lpVtbl -> put_Samples(This,newVal) ) 

#define IDAQPad_put_SampleRate(This,newVal)	\
    ( (This)->lpVtbl -> put_SampleRate(This,newVal) ) 

#define IDAQPad_put_ScanRate(This,newVal)	\
    ( (This)->lpVtbl -> put_ScanRate(This,newVal) ) 

#define IDAQPad_put_Gain(This,Index,newVal)	\
    ( (This)->lpVtbl -> put_Gain(This,Index,newVal) ) 

#define IDAQPad_put_FullScaleLoad(This,Channel,newVal)	\
    ( (This)->lpVtbl -> put_FullScaleLoad(This,Channel,newVal) ) 

#define IDAQPad_put_CalibrationConstant(This,Channel,newVal)	\
    ( (This)->lpVtbl -> put_CalibrationConstant(This,Channel,newVal) ) 

#define IDAQPad_put_ExcitationVoltage(This,Channel,newVal)	\
    ( (This)->lpVtbl -> put_ExcitationVoltage(This,Channel,newVal) ) 

#define IDAQPad_Reset(This)	\
    ( (This)->lpVtbl -> Reset(This) ) 

#define IDAQPad_Record(This,DoubleBufferMode,DBSamples)	\
    ( (This)->lpVtbl -> Record(This,DoubleBufferMode,DBSamples) ) 

#define IDAQPad_Stop(This)	\
    ( (This)->lpVtbl -> Stop(This) ) 

#define IDAQPad_get_DataVoltage(This,Index,pVal)	\
    ( (This)->lpVtbl -> get_DataVoltage(This,Index,pVal) ) 

#define IDAQPad_get_DataVoltageByChannel(This,Channel,Index,pVal)	\
    ( (This)->lpVtbl -> get_DataVoltageByChannel(This,Channel,Index,pVal) ) 

#define IDAQPad_get_DataNewtons(This,Index,pVal)	\
    ( (This)->lpVtbl -> get_DataNewtons(This,Index,pVal) ) 

#define IDAQPad_get_DataNewtonsByChannel(This,Channel,Index,pVal)	\
    ( (This)->lpVtbl -> get_DataNewtonsByChannel(This,Channel,Index,pVal) ) 

#define IDAQPad_SetOutputWindow(This,MsgWindow)	\
    ( (This)->lpVtbl -> SetOutputWindow(This,MsgWindow) ) 

#define IDAQPad_SetLoggingOn(This,fOn,AppPath)	\
    ( (This)->lpVtbl -> SetLoggingOn(This,fOn,AppPath) ) 

#define IDAQPad_AcquireData(This,ForceStop)	\
    ( (This)->lpVtbl -> AcquireData(This,ForceStop) ) 

#define IDAQPad_SetMessageWindow(This,MsgWindow)	\
    ( (This)->lpVtbl -> SetMessageWindow(This,MsgWindow) ) 

#define IDAQPad_put_BaselineCount(This,newVal)	\
    ( (This)->lpVtbl -> put_BaselineCount(This,newVal) ) 

#define IDAQPad_put_ChannelVector(This,Index,newVal)	\
    ( (This)->lpVtbl -> put_ChannelVector(This,Index,newVal) ) 

#define IDAQPad_put_OutputFile(This,newVal)	\
    ( (This)->lpVtbl -> put_OutputFile(This,newVal) ) 

#define IDAQPad_WriteDrawPoints(This,DC)	\
    ( (This)->lpVtbl -> WriteDrawPoints(This,DC) ) 

#define IDAQPad_put_MagnetForce(This,newVal)	\
    ( (This)->lpVtbl -> put_MagnetForce(This,newVal) ) 

#define IDAQPad_put_LeftViewMaximizeSize(This,newVal)	\
    ( (This)->lpVtbl -> put_LeftViewMaximizeSize(This,newVal) ) 

#define IDAQPad_put_OutputType(This,OutputType)	\
    ( (This)->lpVtbl -> put_OutputType(This,OutputType) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDAQPad_INTERFACE_DEFINED__ */



#ifndef __INPUTMODLib_LIBRARY_DEFINED__
#define __INPUTMODLib_LIBRARY_DEFINED__

/* library INPUTMODLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_INPUTMODLib;

EXTERN_C const CLSID CLSID_Digitizer;

#ifdef __cplusplus

class DECLSPEC_UUID("93FCD950-9BB1-11D3-8A30-00104BC7E2C8")
Digitizer;
#endif

EXTERN_C const CLSID CLSID_DAQPad;

#ifdef __cplusplus

class DECLSPEC_UUID("D6A62B66-1457-46E6-BD69-E5B52DF4FC56")
DAQPad;
#endif
#endif /* __INPUTMODLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



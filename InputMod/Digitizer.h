// Digitizer.h: Definition of the Digitizer class
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "resource.h"
#include <afxmt.h>
#include "..\Wintab\include\wintab.h"
#include "msgpack.h"
#define PACKETDATA	(PK_CURSOR | PK_X | PK_Y | PK_BUTTONS | PK_NORMAL_PRESSURE | PK_ORIENTATION | PK_TIME)
#define PACKETMODE	1	// 07Sep00: gmb: Changed from 0 so now pressure is int not UINT
#include "..\WinTab\Include\pktdef.h"
#include "..\Common\Point.h"
#include <list>

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// Digitizer

class Digitizer : 
	public IDispatchImpl<IDigitizer, &IID_IDigitizer, &LIBID_INPUTMODLib>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<Digitizer,&CLSID_Digitizer>
{
public:
	Digitizer();
	~Digitizer();

// Attributes
protected:
	BOOL			m_fFirstPoint;
	BOOL			m_fJustDisplay;
	HWND			m_hWndTablet;
	HWND			m_hWndTarget;
	CMutex*			pWTMutex;
	POINT			csr;
	POINT			csrz;
	HCTX			hCtx;
	LOGCONTEXT		lc;
	list<point>*	pt_lst;
	list<point>*	pt_lstz;
	CString			m_szFile;
	//// target related
	BOOL			fNotifyBad;
	/////
	CStdioFile		f;
	BOOL			fWrote;
	CString			szInstr;
	BYTE			wPrsBtn;
	UINT			wActiveCsr;
	UINT			wOldCsr;
	UINT			prsYesBtnOrg;
	UINT			prsYesBtnExt;
	UINT			prsNoBtnOrg;
	UINT			prsNoBtnExt;
	BOOL			fOnTablet;
	BOOL			fWasOutOfRange;
	BOOL			fUseMouse;
	BOOL			fTargetEqualsTablet;
	BOOL			fTabletOnly;
	BOOL			fRealSize;
	BOOL			fPenUp;
	DWORD			dwBkColor;
	BOOL			fPenDownNotified;
	BOOL			fPenUpNotified;
	// Timeout related
	BOOL			fBeganRecording;
	BOOL			fAlertedBegan;
	long			nTimeoutTicks;
	// record immediately?
	BOOL			fRecordImmediately;
	// stop on wrong target?
	BOOL			fStopWrongTarget;
	// redo if wrong target?
	BOOL			fRedoWrongTarget;
	// start on first target?
	BOOL			fStartFirstTarget;
	// stop on last target?
	BOOL			fStopLastTarget;
	// bad data tracking
	long			lSamples;
	long			lDownSamples;
	long			lWrittenSamples;
	long			lBadPoints;
	long			lBadHandles;
	COleDateTime	dtStart;
	COleDateTime	dtStop;
	// if specified tablet dimensions are < actual, we need to shift the mouse cursor
	BOOL			fShiftMouse;
	// feedback
	int				nStrokeNum;
	BOOL			fCountStrokes;
	int				nMaxStrokes;
	BOOL			fHideShowAfterShow;
	BOOL			fHideShowAfterStrokes;
	double			dStrokeCount;
	BOOL			m_fSoundPlaying;
	char*			m_wavMPP;			// wave file for max pen pressure
	char*			m_wavDis;			// wave file for discontinuity
	// transformation
	BOOL			fTransform;
	BOOL			fBeginTransform;
	POINT			rotOrigin;
	double			dxx;
	double			dyx;
	double			dxy;
	double			dyy;
	BOOL			fInches;
	// background color
	COLORREF		m_BGColor;
	// high-precision timer
	LARGE_INTEGER	freq, start, stop;
	BOOL			fCheckedStartTime;
	// discontinuity
	CString			m_szDisFile;
	CStdioFile		fDis;
	DWORD			dwLastSample;
	BOOL			fDisWrote;
	double			m_dDisError;
	BOOL			m_fDisNotify;
	BOOL			m_fDisBeforeTrailingPenLift;
	// line thickness variation
	BOOL			m_fVaryLT;
	// tilt
	BOOL			m_fTilt;		// does it support tilt?
	BOOL			m_fUseTilt;		// user settable
	// max pen pressure
	short			m_nMPP;

// Implementation
protected:
	BOOL DigitizerToScreen( long dx, long dy, long& sx, long& sy );
	BOOL DigitizerToCM( long dx, long dy, double& x, double& y );
	BOOL CMToDigitizer( double cmx, double cmy, long& x, long& y );
	BOOL MouseToCM( long mx, long my, double& x, double& y );
	BOOL CMToDisplay( double cmx, double cmy, long& x, long& y );
	double GetMaxX();	// in cm
	double GetMaxY();	// in cm
	double GetMinX();
	double GetMinY();
	void InitializeFile();
	void InitializeSettings();
	void PrsInit();
	int PrsAdjust( PACKET p );
	LRESULT OnPacket( CDC* pDC, long x, long y, long pressure, DWORD dwTime,
					  double xFile, double yFile, BOOL fDown, BOOL fNoDraw, BOOL& invalidate,
					  int nAzimuth = 0, int nAltitude = 0, int nTwist = 0, BOOL fButtons = FALSE );
	LRESULT OnWTPacket2( WPARAM, LPARAM, CDC*, BOOL& invalidate );

BEGIN_COM_MAP(Digitizer)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IDigitizer)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(Digitizer) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Digitizer)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IDigitizer
public:
	STDMETHOD(put_MaxPenPressure)(/*[in]*/ short newVal);
	STDMETHOD(get_MaxPenPressure)(/*[out, retval]*/ short *pVal);
	STDMETHOD(put_DisError)(/*[in]*/ double newVal);
	STDMETHOD(put_DisNotify)(/*[in]*/ BOOL newVal);
	STDMETHOD(put_UseTilt)(/*[in]*/ BOOL newVal);
	STDMETHOD(SupportsTilt)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_VaryLineThickness)(/*[in]*/ BOOL newVal);
	STDMETHOD(put_MaxLineThickness)(/*[in]*/ short newVal);
	STDMETHOD(DrawSettings) ( BOOL LineFixed, short LineSize, DWORD LineColor1,
							  DWORD LineColor2, DWORD LineColor3, DWORD MPPColor, 
							  DWORD EllipseColor, short EllipseSize, DWORD BGColor );
	STDMETHOD(put_NoTracking)(BOOL newVal);
	STDMETHOD(put_RealSize)(BOOL newVal);
	STDMETHOD(put_LeftViewMaximizeSize)(int nSize);
	STDMETHOD(Transformation)(BOOL Transform, double xgain, double xrotation, double ygain, double yrotation);
	STDMETHOD(CountStrokes)(BOOL Count, short MaxStrokes);
	STDMETHOD(HideShowAfterStrokes)(BOOL HideShowAfter, BOOL Show, double Strokes);
	STDMETHOD(HideShow)(BOOL Show);
	STDMETHOD(put_TabletDimension)(/*[in]*/ double arX, /*[in]*/ double arY);
	STDMETHOD(put_DisplayDimension)(/*[in]*/ double ddX, /*[in]*/ double ddY);
	STDMETHOD(DoOnTimer)(long Increment, VARIANT* DC);
	STDMETHOD(SetStimulusInfo)(/*[in]*/ BSTR StimPath, /*[in]*/ BSTR StimID,
							   /*[in]*/ BOOL DeactivateWrongTarget);
	STDMETHOD(Start)(/*[in]*/ VARIANT* DC, /*[in]*/ BOOL Stimulus,
					 /*[in, defaultvalue(FALSE)]*/ BOOL UseMouse);
	STDMETHOD(get_TabletResolution)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TabletRate)(/*[out, retval]*/ short *pVal);
	STDMETHOD(get_TabletYRange)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TabletXRange)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TabletDesc)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(Stop)();
	STDMETHOD(put_StopLastTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(put_StartFirstTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(put_StopWrongTarget)(/*[in]*/ BOOL newVal);

	// GMB: 2016-04-12: Added new param: Redo if wrong target
	STDMETHOD(put_RedoIfWrongTarget)(/*[in]*/ BOOL newVal);

	STDMETHOD(put_RecordImmediately)(/*[in]*/ BOOL newVal);
	STDMETHOD(put_NotifyOfBadTarget)(/*[in]*/ BOOL newVal);
	STDMETHOD(put_BkColor)(/*[in]*/ DWORD newVal);
	STDMETHOD(InitializeForStimulus)(/*[in]*/ VARIANT* DC,
									 /*[in, defaultvalue(FALSE)]*/ BOOL UseMouse);
	STDMETHOD(put_DeviceResolution)(/*[in]*/ double newVal);
	STDMETHOD(put_MinPenPressure)(/*[in]*/ short newVal);
	STDMETHOD(put_SamplingRate)(/*[in]*/ short newVal);
	STDMETHOD(TabletWindowOnly)(/*[in]*/ BOOL newVal);
	STDMETHOD(TargetEqualsTablet)(/*[in]*/ BOOL newVal);
	STDMETHOD(SetLoggingOn)(/*[in]*/ BOOL fOn, /*[in]*/ BSTR AppPath);
	STDMETHOD(OnMouseMove)(/*[in]*/ long Flags, /*[in]*/ VARIANT* Point,
						   /*[in]*/ VARIANT* DC, /*[out,retval]*/ BOOL* invalidate );
	STDMETHOD(put_Instruction)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_TimeoutTicks)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_OutputFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(put_TargetWindow)(/*[in]*/ VARIANT* newVal);
	STDMETHOD(put_TabletWindow)(/*[in]*/ VARIANT* newVal);
	STDMETHOD(SetOutputWindow)(/*[in]*/ VARIANT* MsgWindow);
	STDMETHOD(Initialize)(/*[in]*/ VARIANT* DC, /*[in]*/ BOOL Stimulus,
						  /*[in, defaultvalue(FALSE)]*/ BOOL UseMouse);
	STDMETHOD(Draw)(/*[in]*/ VARIANT* DC );
	STDMETHOD(OnWTPacket)(/*[in]*/ long wSerial, /*[in]*/ long hCtx,
						  /*[in]*/ VARIANT* DC, /*[out,retval]*/ BOOL* invalidate );
	STDMETHOD(Clear)(void);

	STDMETHOD(SetAuthorization)(/*[in]*/ BSTR AuthCode);

	BOOL	m_fAuthorized;
	BOOL	VerifyAuthorization();
};

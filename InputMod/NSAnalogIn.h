// Utility class to wrap some of the NI-DAQ functions for analog input.

#pragma once
#define	MAX_CHANNELS	16
// TODO? Add to interface as enum?
#define	CHAN_LOWER		2
#define	CHAN_UPPER		0
#define	CHAN_LOAD		1

// magnet defines (digital)
#define MAGNET_ENABLEn	3
#define MAGNET_CHECK	2
#define MAGNET_UNSETn	1
#define MAGNET_SET		0

// OUTPUT TYPES
// enum comparison (from IDL)
#define EOT_RAW			0x0001	// Raw units (from daqpad)
#define EOT_VOLTS		0x0004	// Volts (V)
#define EOT_NEWTONS		0x0008	// Newtons (N)
// stored value
#define OT_RAW			1	// Raw units (from daqpad)
#define OT_VOLTS		2	// Volts (V)
#define OT_NEWTONS		3	// Newtons (N)

class CNSAnalogIn
{
public:
	// default constructor
	CNSAnalogIn( short device = 1, short channels = 1 );

	// destructor
	~CNSAnalogIn();

	// begin
	void RunWithMultipleBuffering( double scanrate, unsigned long samplecount );
	void RunImmedAsynch( double scanrate, unsigned long samplecount );	// voltage data back
	void RunImmed( short* buffer, double scanrate, unsigned long samplecount );
	void GetBaseline( double scanrate );

	// end
	void Stop();
	// get the data (non-double-buffered)
	BOOL GetData( double* data, unsigned long samplecount );
	// draw the data (non-double-buffered)
	void WriteDrawPoints( CDC* pDC );
	// not really used...but...anyway
	void SetChannelVector( int nIndex, short nChannel );
	short GetChannelVector( int nIndex );
	// output type
	void SetOutputType( DAQOutputType OutputType );

	// magnet control
	void MagnetOn();
	void MagnetOff();
	bool MagnetCheck();
	void MagnetLatch();

	// force control
	void SetForce( double force );

public:
	BOOL			m_DeviceOK;
	short 			m_Device;
	short 			m_Channels;
	double			m_Scale[ MAX_CHANNELS ];
	short 			m_Gains[ MAX_CHANNELS ];	// for the channel gains
	double 			m_Offsets[ MAX_CHANNELS ];	// for the channel offsets
	double 			m_SampleRate;
	short* 			m_TempBuffer;
	double*			m_dTempBuffer;
	HWND			m_hwnd;
	unsigned long	m_Samples;
	short			piChanVect[ MAX_CHANNELS ];
	CString			m_szFile;
	int				m_nOutputType;
	int				m_nBaselineCount;
	BOOL			m_fGotBaseline;
	double			m_dMagnetForce;

	double*			m_pFullScaleLoad;
	double*			m_pCalibrationConstant;
	double*			m_pExcitationVoltage;
	double*			m_pVtoN;
};

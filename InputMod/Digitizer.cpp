// Digitizer.cpp : Implementation of CInputModApp and DLL registration.

#include "StdAfx.h"
#include "InputMod.h"
#include "Digitizer.h"
#include "DigitizerConst.h"
#include "..\Common\DefFun.h"
#include "..\Common\Shared.h"
#include "MessageManager.h"
#include <math.h>
#include <mmsystem.h>

ElementMessageManager	_emm;

/////////////////////////////////////////////////////////////////////////////
// Exclusively for UMD
// Setting EEGPPT to "TRUE" will hide the output on recording window
/////////////////////////////////////////////////////////////////////////////
#define EEGPPT		TRUE
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

#define	MAX_PRESSURE	5000	// max pen pressure
#define	MAX_ELEMENTS	25		// max # of elements allowed per stimulus ???
#define	MAX_TARGETS		50		// max # of targets allowed ???
#define	CM_IN			2.54	// centimeters to inch
#define	DEFAULT_VARY	TRUE	// vary line thickness by pen pressure default val


// api defines for loadlibrary/getprocaddress for interface with wintab/digitizer
typedef UINT (API *LPFNWTINFO) (UINT, UINT, LPVOID);
typedef BOOL (API *LPFNWTCLOSE) (HCTX);
typedef HCTX (API *LPFNWTOPEN) (HWND, LPLOGCONTEXTA, BOOL);
typedef BOOL (API *LPFNWTGET) (HCTX, LPLOGCONTEXTA);
typedef int (API *LPFNWTPACKET) (HCTX, int, LPVOID);
typedef	BOOL (API *LPFNWTQUEUESIZESET) (HCTX, int);

int segment( int x, int y, int nsample );

//dll instance
HINSTANCE _hTablet = NULL;

HINSTANCE GetTabletInstance();
void FreeTabletInstance(HCTX &aContext);

// dll context
int _dllContextCount = 0;
bool OpenDeviceContext(HMODULE aHModule, HWND aWndTablet, LOGCONTEXTA &aLogContext, bool aAutoStart, HCTX &aContext );
bool CloseDeviceContext(HMODULE aHModule, HCTX &aContext);

ULONG		_TimerInc = 0;

/////////////////////////////////////////////////////////////////////////////
//

HINSTANCE GetTabletInstance()
{
	if( !_hTablet )
	{
		if (_dllContextCount==0)
		{
			// Load library (dll)
			char szObj[ 500 ];
#ifdef DEBUG
	::GetModuleFileName(NULL, szObj, 500);
	PathRemoveFileSpec(szObj);
#else
	::GetSystemDirectory( szObj, 500 );
#endif

	#ifdef INPUT_DIGITIZER_TYPE_PEN32
  			strcat_s( szObj, "\\Pen32.dll" );
	        //strcpy_s(szObj, "C:\\Projects\\MovAlyzeR\\Bin\\Pen32.dll");
	#else
			strcat_s( szObj, "\\WinTab32.DLL" );
			//strcpy_s(szObj, "C:\\Projects\\neuroscript\\Pen\\bin\\WinTab32.DLL");
	#endif
			_hTablet = LoadLibrary( szObj );
			if( !_hTablet )
			{
				::OutputMessage( _T("Unable to load tablet driver."), LOG_TABLET );
				return NULL;
			}
		}
	}


	return _hTablet;
}


//
void FreeTabletInstance(HCTX &aContext)
{
	if( _hTablet )
	{
		if (CloseDeviceContext(_hTablet, aContext))
		{
			if (_dllContextCount==0)
			{
				FreeLibrary( _hTablet );
				_hTablet = NULL;
			}
		}
	}
	
}



bool OpenDeviceContext(HMODULE aHModule, HWND aWndTablet, LOGCONTEXTA &aLogContext, bool aAutoStart, HCTX &aContext )
{
	bool Result = false;
   LPFNWTOPEN _procWTOpen = NULL;

	if (aHModule)
	{
		if (_dllContextCount >=0)
		{
			_procWTOpen = (LPFNWTOPEN)GetProcAddress( aHModule, "WTOpenA" );
			if( _procWTOpen )
			{
				if (aContext == NULL)
				{
					aContext = (*_procWTOpen) ( aWndTablet, &aLogContext, aAutoStart );
					if( aContext )
					{
						_dllContextCount ++;
						Result = true;		
					}
				}
				else
				{
					if (CloseDeviceContext(aHModule, aContext))
					{
						aContext = (*_procWTOpen) ( aWndTablet, &aLogContext, aAutoStart );
						if( aContext )
						{
							_dllContextCount ++;
							Result = true;		
						}
					}
				}
				
			}
		}
	}
	
	return Result;
}

bool CloseDeviceContext(HMODULE aHModule, HCTX &aContext)
{
	bool Result = false;
	if(aHModule)
	{
		if( aContext )
		{
			if (_dllContextCount > 0)
			{
				LPFNWTCLOSE _procWTClose = NULL;
				_procWTClose = (LPFNWTCLOSE)GetProcAddress( _hTablet, "WTClose" );
				if( _procWTClose ) 
				{
					if ((*_procWTClose) ( aContext ))
					{
						_dllContextCount --;
						aContext = NULL;
						Result = true;
					}
				}
			}
		}
		else
			Result = true;
	}

	return Result;
}

STDMETHODIMP Digitizer::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IDigitizer,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

Digitizer::Digitizer()
	: m_hWndTablet( NULL ), pt_lst( NULL ), pWTMutex( NULL ), hCtx( NULL ),
	  m_hWndTarget( NULL ), pt_lstz( NULL ), m_fAuthorized( FALSE )
{
	m_fSoundPlaying = FALSE;
	fOnTablet = FALSE;
	fWasOutOfRange = FALSE;
	fBeganRecording = FALSE;
	fAlertedBegan = FALSE;
	nTimeoutTicks = 0L;
	fUseMouse = FALSE;
	fWrote = FALSE;
	fDisWrote = FALSE;
	m_fDisBeforeTrailingPenLift = FALSE;
	dwLastSample = 0;
	fTargetEqualsTablet = FALSE;
	fTabletOnly = FALSE;
	fPenUp = TRUE;
	dwBkColor = RGB(255,255,255);	// white
	fNotifyBad = TRUE;
	fRecordImmediately = FALSE;
	fStopWrongTarget = FALSE;
	fRedoWrongTarget = FALSE;
	fStartFirstTarget = FALSE;
	fStopLastTarget = FALSE;
	fPenDownNotified = FALSE;
	fPenUpNotified = FALSE;
	m_fJustDisplay = TRUE;
	m_fFirstPoint = TRUE;
	lBadPoints = 0;
	lSamples = 0;
	lDownSamples = -1;
	lWrittenSamples = 0;
	fShiftMouse = FALSE;
	fCountStrokes = FALSE;
	nMaxStrokes = 0;
	fHideShowAfterShow = FALSE;
	fHideShowAfterStrokes = FALSE;
	dStrokeCount = 0.;
	fTransform = FALSE;
	dxx = 0.;
	dyx = 0.;
	dxy = 0.;
	dyy = 0.;
	fBeginTransform = FALSE;
	rotOrigin.x = rotOrigin.y = 0;
	fInches = FALSE;
	lBadHandles = 0;
	fRealSize = FALSE;
	m_BGColor = RGB( 255, 255, 255 );
	m_fVaryLT = DEFAULT_VARY;
	m_fTilt = FALSE;
	m_fUseTilt = FALSE;
	m_dDisError = 0.3;
	m_fDisNotify = TRUE;
	m_nMPP = 1023 - 1;	// initialize to max - 1 to ensure warning is triggered for max

	// load sound files
	CString szWav;
	char pszFile[ _MAX_PATH ];
	::SHGetFolderPath( NULL, CSIDL_COMMON_APPDATA, NULL, SHGFP_TYPE_DEFAULT, pszFile );
	szWav.Format( _T("%s\\NeuroScript\\mpp.wav"), pszFile );
	CFile fWav;
	if( fWav.Open( szWav, CFile::modeRead | CFile::typeBinary ) ) {
		UINT size = (UINT)fWav.GetLength();
		if( size > 0 ) {
			m_wavMPP = new char[ size ];
			if( fWav.Read( m_wavMPP, size ) == 0 ) {
				delete [] m_wavMPP;
				m_wavMPP = NULL;
			}
		}
		fWav.Close();
	}
	else m_wavMPP = NULL;
	szWav.Format( _T("%s\\NeuroScript\\dis.wav"), pszFile );
	if( fWav.Open( szWav, CFile::modeRead | CFile::typeBinary ) ) {
		UINT size = (UINT)fWav.GetLength();
		if( size > 0 ) {
			m_wavDis = new char[ size ];
			if( fWav.Read( m_wavDis, size ) == 0 ) {
				delete [] m_wavDis;
				m_wavDis = NULL;
			}
		}
		fWav.Close();
	}
	else m_wavDis = NULL;
}

Digitizer::~Digitizer()
{
	if( m_wavDis ) delete [] m_wavDis;
	if( m_wavMPP ) delete [] m_wavMPP;
	if( pt_lst ) delete pt_lst;
	if( pt_lstz ) delete pt_lstz;
	if( pWTMutex ) delete pWTMutex;

	FreeTabletInstance(hCtx);
	

	if( f.m_pStream )
	{
		f.Close();
		if( !fWrote ) CFile::Remove( m_szFile );
	}
	if( fDis.m_pStream )
	{
		fDis.Close();
		if( !fDisWrote || !m_fDisBeforeTrailingPenLift ) CFile::Remove( m_szDisFile );
	}
}

STDMETHODIMP Digitizer::SetAuthorization( BSTR AuthCode )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// auth code is 16 characters
	// characters 3,8,13,15 are looked at, the remainder ignored for specific characters
	// character 3 = lower b,x,q or upper D,F,W or numeral 2,5,7
	// character 8 = an uppercase vowel
	// character 13 = a number
	// character 15 = a lowercase vowel
	// total upper case letters is 6
	// total numbers is 4

	CString szAuthCode = AuthCode;
	char c3;
	char c8;
	char c13;
	char c15;
	int nCount = 0;

	// total length must be 16 chars
	if( szAuthCode.GetLength() != 16 ) goto WasError;

	// VERIFY CHARACTERS
	c3 = szAuthCode.GetAt( 2 );
	c8 = szAuthCode.GetAt( 7 );
	c13 = szAuthCode.GetAt( 12 );
	c15 = szAuthCode.GetAt( 14 );
	// verify character 15 (lowercase vowel)
	if( ( c15 != 'a' ) && ( c15 != 'e' ) && ( c15 != 'i' ) && ( c15 != 'o' ) && ( c15 != 'u' ) )
		goto WasError;
	// verify character 13 (numeral)
	if( isdigit( c13 ) == 0 ) goto WasError;
	// verify character 8 (uppercase vowel)
	if( ( c8 != 'A' ) && ( c8 != 'E' ) && ( c8 != 'I' ) && ( c8 != 'O' ) && ( c8 != 'U' ) )
		goto WasError;
	// verify character 3 (b,x,q,D,F,W,2,5,7)
	if( ( c3 != 'b' ) && ( c3 != 'x' ) && ( c3 != 'q' ) && ( c3 != 'D' ) &&
		( c3 != 'F' ) && ( c3 != 'W' ) && ( c3 != '2' ) && ( c3 != '5' ) && ( c3 != '7' ) )
		goto WasError;

	// verify total upper case letters
	for( int i = 0; i < 16; i++ )
	{
		char c = szAuthCode.GetAt( i );
		if( isalnum( c ) && isupper( c ) ) nCount++;
	}
	if( nCount != 6 ) goto WasError;

	// verify number count
	nCount = 0;
	for( int i = 0; i < 16; i++ )
	{
		char c = szAuthCode.GetAt( i );
		if( isdigit( c ) ) nCount++;
	}
	if( nCount != 4 ) goto WasError;

	// if all tests have passed, then we have a valid auth code
	m_fAuthorized = TRUE;
	return S_OK;

WasError:
	m_fAuthorized = FALSE;
	VerifyAuthorization();
	return E_FAIL;
}

BOOL Digitizer::VerifyAuthorization()
{
	if( !m_fAuthorized )
	{
		::MessageBox( NULL, _T("You are not licensed to use this input device module.\nPlease contact NeuroScript for licensing options."), _T("License Invalid"), MB_ICONEXCLAMATION );
		return FALSE;
	}
	else return TRUE;
}

STDMETHODIMP Digitizer::put_NoTracking(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fJustDisplay = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::SetOutputWindow(VARIANT *MsgWindow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CListCtrl* pWnd = (CListCtrl*)MsgWindow;
	::SetOutputWindow( pWnd );

	return S_OK;
}

STDMETHODIMP Digitizer::put_TabletWindow(VARIANT *newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_hWndTablet = (HWND)newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::put_TargetWindow(VARIANT *newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_hWndTarget = (HWND)newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::put_OutputFile(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_szFile = newVal;

	// construct discontinuity file from raw file
	int nPos = m_szFile.ReverseFind( '.' );
	if( nPos > 0 )
	{
		m_szDisFile = m_szFile.Left( nPos );
		m_szDisFile += _T(".DIS");
	}

	return S_OK;
}

STDMETHODIMP Digitizer::put_Instruction(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	szInstr = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::TargetEqualsTablet(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fTargetEqualsTablet = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::TabletWindowOnly(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fTabletOnly = newVal;

	return S_OK;
}

BOOL Digitizer::DigitizerToScreen( long dx, long dy, long& sx, long& sy )
{
	if( ( lc.lcInExtX == 0 ) || ( lc.lcInExtY == 0 ) ||
		( dx < GetMinX() ) || ( dx > GetMaxX() ) ||
		( dy < GetMinY() ) || ( dy > GetMaxY() ) )
	{
		sx = -1;
		sy = -1;
		return TRUE;
	}

	// Get window sizes
	RECT window_rect, dt_rect;
	GetWindowRect( m_hWndTablet, &window_rect );
	GetWindowRect( m_hWndTarget, &dt_rect );

	if( !fTabletOnly )
	{
		sx = ( dx * dt_rect.right ) / lc.lcInExtX;
		sy = dt_rect.bottom - ( ( dy * dt_rect.bottom ) / lc.lcInExtY );

		// If cursor is not in client, do nothing
		if( ( ( sx - window_rect.left ) < 0 ) ||
			( ( sx - window_rect.right ) > 0 ) ||
			( ( sy - window_rect.top ) < 0 ) ||
			( ( sy - window_rect.bottom ) > 0 ) )
		{
			sx = -1;
			sy = -1;
			return TRUE;
		}

		sx -= window_rect.left;
		sy -= window_rect.top;
	}
	else
	{
		POINT size;
		size.x = window_rect.right - window_rect.left;
		size.y = window_rect.bottom - window_rect.top;

		sx = ( size.x * dx ) / lc.lcInExtX;
		sy = size.y - ( size.y * dy ) / lc.lcInExtY;

		if( ( sx < 0 ) || ( sx > window_rect.right ) ||
			( sy < 0 ) || ( sy > window_rect.bottom ) )
		{
			sx = -1;
			sy = -1;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL Digitizer::DigitizerToCM( long dx, long dy, double& x, double& y )
{
	double dDevRes = ::GetDeviceResolution();

	x = dx * dDevRes;
	y = dy * dDevRes;

	if( x < 0. ) x *= -1.;
	if( y < 0. ) y *= -1.;

	return TRUE;
}

BOOL Digitizer::CMToDigitizer( double cmx, double cmy, long& x, long& y )
{
	double dDevRes = ::GetDeviceResolution();
	if( dDevRes == 0. ) return FALSE;

	x = (long)( cmx / dDevRes );
	y = (long)( cmy / dDevRes );

	return TRUE;
}

BOOL Digitizer::MouseToCM( long mx, long my, double& x, double& y )
{
	// display dimensions
	double dX, dY;
	::GetDisplayDimension( dX, dY );

	// how many pixels per cm
	RECT dt_rect;
	GetWindowRect( m_hWndTarget, &dt_rect );
	double dpX = ( dt_rect.right - dt_rect.left ) / dX;
	double dpY = ( dt_rect.bottom - dt_rect.top ) / dY;

	x = mx / dpX;
	y = my / dpY;

	if( x < 0. ) x *= -1.;
	if( y < 0. ) y *= -1.;

	return TRUE;
}

BOOL Digitizer::CMToDisplay( double cmx, double cmy, long& x, long& y )
{
	::CMToDisplay( cmx, cmy, m_hWndTablet, x, y );

	return TRUE;
}

STDMETHODIMP Digitizer::Draw( VARIANT* DC )
{
	if( !pt_lst || !pt_lstz || !DC ) return E_FAIL;

	if( !VerifyAuthorization() ) return E_FAIL;

	CDC* pDC = (CDC*)DC;
	POINT pt = {0,0}, ptLast = {0,0};
	BOOL fFirst = TRUE;
	BOOL fInval = FALSE;
	WORD retmsg;
	CString szMsg;

	_emm.PostMessage( MAKELPARAM( MSG_RESET, 0 ),
					  (WPARAM) pDC, _TimerInc, fInval, retmsg );
	if( !m_fJustDisplay && !EEGPPT ) ::DoTextOut( pDC, 0, 0, 0, 0, TRUE, 0, TRUE, m_BGColor );

	csr.x = -1;
	csrz.x = -1;

	list<point>::iterator i = pt_lst->begin();
	list<point>::iterator z = pt_lstz->begin();
	while( i != pt_lst->end() )
	{
		pt.x = abs( i->x );
		pt.y = abs( i->y );

		if( fFirst ) memcpy( &ptLast, &pt, sizeof( POINT ) );
		::DrawPoint( pDC, pt, ptLast, ( z->x > ::GetMinPenPressure() ), TRUE, fFirst, FALSE, m_fVaryLT ? z->x : -1 );
		fFirst = FALSE;

		ptLast.x = pt.x;
		ptLast.y = pt.y;

		i++;
		z++;
	}

	return S_OK;
}

STDMETHODIMP Digitizer::Initialize( VARIANT* DC, BOOL Stimulus, BOOL UseMouse )
{
	if( !VerifyAuthorization() ) return E_FAIL;

	fBeganRecording = FALSE;
	fAlertedBegan = FALSE;
	fPenDownNotified = FALSE;
	fPenUpNotified = TRUE;
	m_fJustDisplay = FALSE;
	m_fFirstPoint = TRUE;
	_TimerInc = 0;
	lBadPoints = 0;
	lSamples = 0;
	lDownSamples = -1;
	lWrittenSamples = 0;
	fInches = FALSE;
	m_fVaryLT = DEFAULT_VARY;

	fUseMouse = UseMouse;
	nTimeoutTicks = 0L;
	CDC* pDC = (CDC*)DC;

	InitializeFile();
	csr.x = -1;
	csrz.x = -1;
	if( !pt_lst ) pt_lst = new list<point>;
	if( !pt_lstz ) pt_lstz = new list<point>;

	// Check if Wintab is available
	if( !fUseMouse )
	{
		InitializeSettings();

		FreeTabletInstance(hCtx);
		HINSTANCE hMod = ::GetTabletInstance();
		if( !hMod ) return E_FAIL;

		LPFNWTINFO _procWTInfo = NULL;
		_procWTInfo = (LPFNWTINFO)GetProcAddress( hMod, "WTInfoA" );
		if( !_procWTInfo )
		{
			FreeTabletInstance(hCtx);
			return E_FAIL;
		}

		if( !((*_procWTInfo) ( 0, 0, NULL )) )
		{
			::OutputMessage( _T("WTInfo failed during initialization."), LOG_TABLET );
			return E_FAIL;
		}

		// supports tilt?
		struct tagAXIS TpOri[ 3 ]; // The capabilities of tilt
		m_fTilt = (*_procWTInfo)( WTI_DEVICES, DVC_ORIENTATION, &TpOri );
		if( m_fTilt )
		{
			// does the tablet support azimuth and altitude
			// if not, then don't support tilt
			if( !(TpOri[ 0 ].axResolution) || !(TpOri[ 1 ].axResolution) )
				m_fTilt = FALSE;
		}

		// get units
		AXIS a;
		(*_procWTInfo)( WTI_DEVICES, DVC_X, (LPVOID)&a );
		fInches = ( a.axUnits == TU_INCHES );

		// get max pen pressure
		(*_procWTInfo)( WTI_DEVICES, DVC_Z, (LPVOID)&a );
		::SetMaxPenPressure( a.axMax );

		// Get default context information
		(*_procWTInfo) ( WTI_DEFCONTEXT, 0, &lc );

		if( !pWTMutex ) pWTMutex = new CMutex( TRUE, NULL, NULL );
		hCtx = 0;

		// Open the context
		lc.lcPktData = PACKETDATA;
		lc.lcMsgBase = WT_PACKET;
		lc.lcPktMode = PACKETMODE;
		if( fTabletOnly ) lc.lcOptions = CXO_MESSAGES;
		else lc.lcOptions |= ( CXO_MESSAGES | CXO_SYSTEM );
		lc.lcMoveMask = PACKETDATA;

		if (! OpenDeviceContext(hMod, m_hWndTablet, lc, TRUE, hCtx ))
		{
			if( m_hWndTablet ) 
				::OutputMessage( _T("WTOpen failed during initialzation."), LOG_TABLET );
			FreeTabletInstance(hCtx);
			return E_FAIL;
		}

		// setup queue size of tablet
		LPFNWTQUEUESIZESET _procWTQueueSetSize = NULL;
		_procWTQueueSetSize = (LPFNWTQUEUESIZESET)GetProcAddress( hMod, "WTQueueSizeSet" );
		if( !_procWTQueueSetSize )
		{
			FreeTabletInstance(hCtx);
			return E_FAIL;
		}
		if( !((*_procWTQueueSetSize) ( hCtx, 32 )) )
		{
			// try a smaller size
			if( !((*_procWTQueueSetSize) ( hCtx, 16 )) )
			{
				// forget it
				::OutputMessage( _T("WTQueueSizeSet failed during initialzation."), LOG_TABLET );
				FreeTabletInstance(hCtx);
				return E_FAIL;
			}
		}
	}

	// display settings for manager
	BOOL fInval = FALSE;
	WORD retmsg;
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_MOUSE ),
					  MAKEWPARAM( UseMouse, 0 ), 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_ONLY ),
					  MAKEWPARAM( fTabletOnly, 0 ), 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_REAL ),
					  MAKEWPARAM( fRealSize, 0 ), 0, fInval, retmsg );
	if( !UseMouse )
	{
		_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTX ),
						  (WPARAM)lc.lcInExtX, 0, fInval, retmsg );
		_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTY ),
						  (WPARAM)lc.lcInExtY, 0, fInval, retmsg );
	}

	double dX, dY;
	::GetDisplayDimension( dX, dY );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_X ),
						dX, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_Y ),
						dY, 0, fInval, retmsg );
	::GetTabletDimension( dX, dY );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_TABLET_X ),
						dX, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_TABLET_Y ),
						dY, 0, fInval, retmsg );

	// start'em up
	_emm.PostMessage( MAKELPARAM( MSG_RESET, MSG_SUB_ELMT_NORMAL ),
					  (WPARAM) pDC, 0, fInval, retmsg );

	return S_OK;
}


STDMETHODIMP Digitizer::Start( VARIANT* DC, BOOL Stimulus, BOOL UseMouse )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !VerifyAuthorization() ) return E_FAIL;

	fBeganRecording = FALSE;
	fAlertedBegan = FALSE;
	fPenDownNotified = FALSE;
	fPenUpNotified = TRUE;
	m_fFirstPoint = TRUE;
	_TimerInc = 0;
	lBadPoints = 0;
	lBadHandles = 0;
	lSamples = 0;
	lDownSamples = -1;
	lWrittenSamples = 0;
	dtStart = COleDateTime::GetCurrentTime();
	dwLastSample = 0;

	fUseMouse = UseMouse;
	nTimeoutTicks = 0L;
	CDC* pDC = (CDC*)DC;

	InitializeFile();
	csr.x = -1;
	csrz.x = -1;

	// get frequency of high-resolution performance counter
	::QueryPerformanceFrequency( &freq );
	fCheckedStartTime = FALSE;

	// display settings for manager
	BOOL fInval = FALSE;
	WORD retmsg;
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_MOUSE ),
					  MAKEWPARAM( UseMouse, 0 ), 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_ONLY ),
					  MAKEWPARAM( fTabletOnly, 0 ), 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_REAL ),
					  MAKEWPARAM( fRealSize, 0 ), 0, fInval, retmsg );
	if( !UseMouse )
	{
		_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTX ),
						  (WPARAM)lc.lcInExtX, 0, fInval, retmsg );
		_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTY ),
						  (WPARAM)lc.lcInExtY, 0, fInval, retmsg );
	}

	double dX, dY;
	::GetDisplayDimension( dX, dY );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_X ),
						dX, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_Y ),
						dY, 0, fInval, retmsg );
	::GetTabletDimension( dX, dY );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_TABLET_X ),
						dX, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_TABLET_Y ),
						dY, 0, fInval, retmsg );

	// start'em up
	_emm.PostMessage( MAKELPARAM( MSG_RESET, MSG_SUB_ELMT_NORMAL ),
					  (WPARAM) pDC, 0, fInval, retmsg );

	return S_OK;
}

STDMETHODIMP Digitizer::Stop()
{
	if( !VerifyAuthorization() ) return E_FAIL;

	PlaySound( NULL, NULL, SND_ASYNC );

	if( f.m_pStream )
	{
		f.Close();
		if( !fWrote ) CFile::Remove( m_szFile );
	}
	if( fDis.m_pStream )
	{
		fDis.Close();
		if( !fDisWrote || !m_fDisBeforeTrailingPenLift ) CFile::Remove( m_szDisFile );
	}

#ifdef _DEBUG
	// report statistics if we're recording
	/*
	dtStop = COleDateTime::GetCurrentTime();
	if( m_szFile != _T("") )
	{
		COleDateTimeSpan dts = dtStop - dtStart;
		double dSecs = dts.GetTotalSeconds();

		CString szMsg;
		szMsg.Format( _T("DIGITIZER: There were %d bad samples of %d total samples in %g seconds."),
					  lBadPoints, lSamples, dSecs );
		OutputMessage( szMsg, LOG_TABLET );

		// report bad handles if any
		if( lBadHandles > 0 )
		{
			szMsg.Format( _T("DIGITIZER: There were %d samples sent with invalid tablet handles."), lBadHandles );
			::OutputMessage( szMsg, LOG_TABLET );
		}
	}*/
#endif

	m_szFile = _T("");
	m_szDisFile = _T("");
	m_fSoundPlaying = FALSE;
	fWrote = FALSE;
	fDisWrote = FALSE;
	m_fDisBeforeTrailingPenLift = FALSE;
	dwLastSample = 0;
	m_fJustDisplay = FALSE;
	fShiftMouse = FALSE;
	fCountStrokes = FALSE;
	nMaxStrokes = 0;
	fHideShowAfterShow = FALSE;
	fHideShowAfterStrokes = FALSE;
	fStopWrongTarget = FALSE;
	fRedoWrongTarget = FALSE;
	fStartFirstTarget = FALSE;
	fStopLastTarget = FALSE;
	dStrokeCount = 0.;
	fTransform = FALSE;
	fBeginTransform = FALSE;
	rotOrigin.x = rotOrigin.y = 0;
	dxx = 0.;
	dyx = 0.;
	dxy = 0.;
	dyy = 0.;
	m_dDisError = 0.3;
	m_fDisNotify = TRUE;
	m_nMPP = 1023 - 1;	// initialize to max - 1 to ensure warning is triggered for max
	::SetIsHidden( FALSE );

	return S_OK;
}

void Digitizer::InitializeFile()
{
	fWrote = FALSE;
	fDisWrote = FALSE;
	m_fDisBeforeTrailingPenLift = FALSE;

	if( m_szFile == _T("") ) return;

	try
	{
		if( f.m_pStream ) f.Close();
		f.Open( m_szFile, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite | CFile::osWriteThrough );

		if( fDis.m_pStream ) fDis.Close();
		if( m_szDisFile != _T("") ) fDis.Open( m_szDisFile, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite | CFile::osWriteThrough );
	}
	catch( CFileException *e )
	{
		char szMsg[ 1000 ];
		if( e->GetErrorMessage( szMsg, 1000 ) )
			OutputMessage( szMsg, LOG_TABLET );
		e->Delete();
		::OutputMessage( _T("Error in opening data file."), LOG_TABLET );
	}
}

void Digitizer::InitializeSettings()
{

	// SAVE Until later when confirm with Wacom
#if 0
	CStdioFile datFile, datFile2;
	BOOL fChanged = FALSE;

	char pszDatFile[ MAX_PATH + 1 ];
	if( _winmajor >= 4 )
		::GetSystemDirectory( pszDatFile, MAX_PATH );
	else
		::GetWindowsDirectory( pszDatFile, MAX_PATH );

	CString szDatFile = pszDatFile;
	szDatFile += _T("\\wacom.dat");

	if( datFile.Open( szDatFile, CFile::modeRead ) )
	{
		if( datFile2.Open( _T("temp.tmp"), CFile::modeCreate | CFile::modeWrite ) )
		{
			CString szLine, szTemp;
			char szItem[ 100 ], szVal[ 100 ];
			while( datFile.ReadString( szLine ) )
			{
				sscanf( szLine, _T("%s %s\n"), szItem, szVal );
				szTemp = szItem;
				szTemp.MakeUpper();
				if( szTemp == _T("InputModE") )
				{
					szLine = szItem;
					szLine += _T(" 1\n");
					fChanged = TRUE;
				} else szLine += _T("\n");
				
				datFile2.WriteString( szLine );
			}

			datFile2.Close();
		}

		datFile.Close();
	}

	if( fChanged )
	{
		CFile::Remove( szDatFile );
		CFile::Rename( _T("temp.tmp"), szDatFile );
	}
	else CFile::Remove( _T("temp.tmp") );
#endif
}

void Digitizer::PrsInit()
{
	/* browse WinTab's many info items to discover pressure handling. */
	AXIS np;
	int logBtns[ 32 ];
	UINT btnMarks[ 2 ];
	UINT size;

	/* discover the LOGICAL button generated by the pressure channel. */
	/* get the PHYSICAL button from the cursor category and run it */
	/* through that cursor's button map (usually the identity map). */
	wPrsBtn = (int)-1;
	HINSTANCE hMod = ::GetTabletInstance();
	if( !hMod ) return;
	LPFNWTINFO _procWTInfo = NULL;
	_procWTInfo = (LPFNWTINFO)GetProcAddress( hMod, "WTInfoA" );
	if( !_procWTInfo )
	{
		FreeTabletInstance(hCtx);
		return;
	}
	(*_procWTInfo) ( WTI_CURSORS + wActiveCsr, CSR_NPBUTTON, &wPrsBtn );
	size = (*_procWTInfo) ( WTI_CURSORS + wActiveCsr, CSR_BUTTONMAP, &logBtns );
	if( (UINT)wPrsBtn < size )
		wPrsBtn = logBtns[ wPrsBtn ];

	/* get the current context for its device variable. */
	LPFNWTGET _procWTGet = NULL;
	_procWTGet = (LPFNWTGET)GetProcAddress( hMod, "WTGetA" );
	if( !_procWTGet )
	{
		FreeTabletInstance(hCtx);
		return;
	}
	(*_procWTGet) ( hCtx, &lc );

	/* get the size of the pressure axis. */
	(*_procWTInfo) ( WTI_DEVICES + lc.lcDevice, DVC_NPRESSURE, &np );
	prsNoBtnOrg = (UINT)np.axMin;
	prsNoBtnExt = (UINT)np.axMax - (UINT)np.axMin;

	/* get the button marks (up & down generation thresholds) */
	/* and calculate what pressure range we get when pressure-button is down. */
	btnMarks[ 1 ] = 0; /* default if info item not present. */
	(*_procWTInfo) ( WTI_CURSORS + wActiveCsr, CSR_NPBTNMARKS, btnMarks );
	prsYesBtnOrg = btnMarks[ 1 ];
	prsYesBtnExt = (UINT)np.axMax - btnMarks[ 1 ];
}

int Digitizer::PrsAdjust( PACKET p )
{
	int wResult;

	wActiveCsr = p.pkCursor;

	if( wActiveCsr != wOldCsr )
	{
		/* re-init on cursor change. */
		PrsInit();
		wOldCsr = wActiveCsr;
	}

	// 07July09: GMB: We are no longer distinguishing button down or up...as this original, old
	// code makes little sense. We will use only the NO BUTTON logic
	wResult = p.pkNormalPressure - prsNoBtnOrg;

	/* scaling output range is 0-255 */
//	if( p.pkButtons & ( 1 << wPrsBtn ) )
//	{
		/* if pressure-activated button is down, */
		/* scale pressure output to compensate btn marks */
//		wResult = p.pkNormalPressure - prsYesBtnOrg;
		/* 07Jun04: Raji: Dont throw away z data by scaling, rather take the raw data from tablet as is*/
		//wResult = MulDiv( wResult, 255, prsYesBtnExt );
//	}
//	else
//	{
		/* if pressure-activated button is not down, */
		/* scale pressure output directly */
//		wResult = p.pkNormalPressure - prsNoBtnOrg;
		/* 07Jun04: Raji: Dont throw away z data by scaling, rather take the raw data from tablet as is*/
		//wResult = MulDiv( wResult, 255, prsNoBtnExt );
//	}

	return wResult;
}

STDMETHODIMP Digitizer::OnWTPacket(long wSerial, long hCtx, VARIANT *DC, BOOL* invalidate)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	WPARAM wp = (WPARAM)wSerial;
	LPARAM lp = (LPARAM)hCtx;
	CDC* pDC = (CDC*)DC;

	BOOL fInval = FALSE;
	LRESULT res = OnWTPacket2( wp, lp, pDC, fInval );
	*invalidate = fInval;

	if( res == E_FAIL )
	{
		::OutputMessage( _T("Digitizer packet failure."), LOG_TABLET );
		return res;
	}

	if( fBeganRecording && !fAlertedBegan )
	{
		fAlertedBegan = TRUE;
		// GMB: 2016-07-17: We need to know about 1st target for triggers
		if( res == S_TARGETGOODFIRST ) {

			return S_START_FIRST;
		}
		return S_START;
	}
	else return res;
}

double Digitizer::GetMinX()
{
	if( !fUseMouse ) return 0;
	else return -5000;
}

double Digitizer::GetMinY()
{
	if( !fUseMouse ) return 0;
	else return -5000;
}

double Digitizer::GetMaxX()
{
	double d = 0., dTemp = 0.;

	if( !fUseMouse ) return lc.lcInExtX;
	else return 5000;

	return d;
}

double Digitizer::GetMaxY()
{
	double d = 0., dTemp = 0.;

	if( !fUseMouse ) return lc.lcInExtY;
	else return 5000;

	return d;
}

LRESULT Digitizer::OnPacket( CDC* pDC, long x, long y, long pressure, DWORD dwTime,
							 double xFile, double yFile, BOOL fDown, BOOL fNoDraw,
							 BOOL& invalidate, int nAzimuth, int nAltitude, int nTwist, BOOL fButtons )
{
	static DWORD lTimeHold2 = 0;
	static int iPerSec2 = 0;
	CString szLine, szMsg;
	LRESULT res = S_OK;
	int nCount = 0;
	BOOL fS = FALSE;

	RECT window_rect;
	GetWindowRect( m_hWndTablet, &window_rect );

	// If pen is lifted or insufficient pressure, do not display normally, only record
	// ...Also, penlift timeout begins
	if( ( pressure > MAX_PRESSURE ) || ( pressure <= GetMinPenPressure() ) )
	{
		if( !fPenUpNotified )
		{
			res = S_PENUP;
			fPenDownNotified = FALSE;
			fPenUpNotified = TRUE;
		}
//		SetCursor( LoadCursor( NULL, IDC_ARROW ) );
		fOnTablet = FALSE;
		if( fBeganRecording )
		{
			nTimeoutTicks++;
			if( fUseMouse ) fPenUp = TRUE;
		}
	}
	else
	{
		fOnTablet = TRUE;
		if( !fPenDownNotified )
		{
			res = S_PENDOWN;
			fPenUpNotified = FALSE;
			fPenDownNotified = TRUE;
		}
		// if we have begun recording, a discontinuity was detected already,
		// and we see that the pen is down again, then we can set the flag
		// indicating that the discontinuities should be written
		// (i.e. we are not writing the DIS file if the only discontinuities are
		//  after the trailing pen lift)
		if( fBeganRecording && fDisWrote ) m_fDisBeforeTrailingPenLift = TRUE;
	}

	// Erase the old cursor
	if( !fNoDraw && ( csr.x >= 0 ) && !fUseMouse )
	{
		CRgn r1, r2, r3;
		r1.CreateEllipticRgn( csr.x - 4, csr.y - 4, csr.x + 4, csr.y + 4 );
		r2.CreateEllipticRgn( csr.x - 3, csr.y - 3, csr.x + 3, csr.y + 3 );
		r3.CreateEllipticRgn( csr.x - 4, csr.y - 4, csr.x + 4, csr.y + 4 );
		r3.CombineRgn( &r1, &r2, RGN_XOR );
		pDC->InvertRgn( &r3 );
	}

	// move mouse cursor if not in mouse mode and in tabletonly mode or transforming
	if( !fUseMouse )
	{
		if( fShiftMouse || fTabletOnly || ( fTransform && fBeginTransform ) )
			SetCursorPos( x + window_rect.left, y + window_rect.top );
	}

	// new point
	if( !fNoDraw )
	{
		csr.x = x;
		csr.y = y;
		csrz.x = pressure;
		csrz.y = 0;
	}

	// When the pen is on the tablet
	if( fDown )
	{
		// last point
		// 23Feb10: GMB: Cases where pen was on tablet as recording began (from stimulus), this was crashing because lst was empty
		POINT old;
		if( !pt_lst->empty() && !pt_lstz->empty() ) {
			list<point>::iterator i = pt_lst->end();
			list<point>::iterator z = pt_lstz->end();
			i--;
			z--;

			// old point (if no old point yet, values should be out of range....set to current point)
			if( m_fFirstPoint )	{
				old.x = abs( x );
				old.y = abs( y );
			}
			else {
				old.x = abs( i->x );
				old.y = abs( i->y );
			}
		}
		else {
			old.x = abs( x );
			old.y = abs( y );
		}

		// To display
		if( !fNoDraw )
		{
// 09Feb09: GMB: We want testing device to reflect global settings
//			if( fOnTablet || ( m_szFile != _T("") ) )
			if( fOnTablet || ( fStartFirstTarget && !fBeganRecording ) )
			{
				WORD retmsg = 0;

//				SetCursor( LoadCursor( NULL, IDC_CROSS ) );
				if( !fStartFirstTarget )
				{
					fBeganRecording = TRUE;
					_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_BEGAN_RECORDING ),
									MAKEWPARAM( fBeganRecording, 0 ), 0, invalidate, retmsg );
				}
				nTimeoutTicks = 0;

				// we checking strokes? if so, & > max, terminate
				lDownSamples++;
				if( m_szFile != _T("") ) fS = TRUE;
				nCount = ::segment( csr.x, csr.y, lDownSamples );
				if( fCountStrokes || fHideShowAfterStrokes )
				{
					if( fCountStrokes && ( nCount > nMaxStrokes ) )
					{
						OutputMessage( _T("DIGITIZER: Max # of strokes reached."), LOG_TABLET );
						return S_MAXSTROKES;
					}
					if( fHideShowAfterStrokes && ( nCount > dStrokeCount ) )
						::SetIsHidden( !fHideShowAfterShow );
				}

				if( !fBeganRecording ) fDown = FALSE;
				DrawPoint( pDC, csr, old, fDown, fBeganRecording, m_fFirstPoint, fWasOutOfRange, m_fVaryLT ? csrz.x : -1 );
				if( m_fFirstPoint ) m_fFirstPoint = FALSE;

				// check if reached or bad target (if applicable)
//				if( fDown || fStartFirstTarget )
//				if( fButtons && fStartFirstTarget )
//				if( ( fDown && !fStartFirstTarget ) || ( fButtons && fStartFirstTarget ) )
				if( ( ( !fStartFirstTarget && !fStopLastTarget && !fStopWrongTarget ) && fDown ) ||
					( ( fStartFirstTarget || fStopLastTarget || fStopWrongTarget ) && fButtons ) )
				{
					BOOL fInvalidate = FALSE;
#ifdef	_LOGPOINTS_
					szMsg.Format( _T("DIGITIZER: Passing x,y = (%d,%d) to MessageManager"), csr.x, csr.y );
					OutputMessage( szMsg, LOG_TABLET );
#endif
					_emm.PostMessage( MAKELPARAM( MSG_TARG_LOCATION, 0 ),
									  MAKEWPARAM( csr.x, csr.y ),
									_TimerInc, fInvalidate, retmsg );
					invalidate |= fInvalidate;
					if( ( retmsg & MSG_STATUS_TARG_SUCCESS ) != 0 )
					{
						// GMB: 2016-04-13: Check (FIRST) for LEFT/EXITED target
						if( ( retmsg & MSG_STATUS_TARG_LEFT ) != 0 )
						{
							OutputMessage( _T("DIGITIZER: Successfully exited a target."), LOG_TABLET );
							return S_TARGETGOODLEFT;
						}

						OutputMessage( _T("DIGITIZER: Successfully reached a target."), LOG_TABLET );

						// GMB: 2016-04-02: If there is only 1 element, both 'first' and 'last' conditions can be met simultaneously
						//					So, the if/else below would never handle both, so we need a first condition for both.
						//					This instance makes no sense, but we need to handle it.
						//		SOLUTION:	Add back 'last' message after handled for 'first'
						BOOL fForFirst = ( ( retmsg & MSG_STATUS_TARG_FIRST ) != 0 );
						BOOL fForLast = ( ( retmsg & MSG_STATUS_TARG_LAST ) != 0 );

						res = S_TARGETGOOD;	// moved up from below (since we changed if/else to if/if, and res is reset if conditions are met)

						if( ( retmsg & MSG_STATUS_TARG_FIRST ) != 0 )
						{
							res = S_TARGETGOODFIRST;
							fBeganRecording = TRUE;
							fStartFirstTarget = FALSE;
							_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_BEGAN_RECORDING ),
											MAKEWPARAM( fBeganRecording, 0 ), 0, fInvalidate, retmsg );
							// add back for 'last' if was present before
							if( fForLast ) retmsg |= MSG_STATUS_TARG_LAST;
							invalidate |= fInvalidate;
						}
						if( ( retmsg & MSG_STATUS_TARG_LAST ) != 0 )
						{
							if( fStopLastTarget )
							{
								_emm.PostMessage( MAKELPARAM( MSG_TIMER, MSG_SUB_INC ),
												(WPARAM) pDC, _TimerInc, fInvalidate, retmsg );
								invalidate |= fInvalidate;
								return S_FALSE;
							}
							res = S_TARGETGOODLAST;
						}
//						else res = S_TARGETGOOD;
					}
					else if( ( retmsg & MSG_STATUS_TARG_FAILURE ) != 0 )
					{
						// GMB: 2016-04-13: Check (FIRST) for LEFT/EXITED target
						BOOL fWasGoingToReturnLeftBadTarget = FALSE;
						if( ( retmsg & MSG_STATUS_TARG_LEFT ) != 0 )
						{
							OutputMessage( _T("DIGITIZER: Incorrectly exited target."), LOG_TABLET );

							fWasGoingToReturnLeftBadTarget = TRUE;

							// only return if NOT also a message for failing to meet criterion immediately below
							if( ( retmsg & MSG_STATUS_TARG_BADTIME ) == 0 ) {

								return S_TARGETBADLEFT;
							}
						}

						// GMB: 2016-04-14: Now check to see if criterion for duration within target failed
						if( ( retmsg & MSG_STATUS_TARG_BADTIME ) != 0 ) {

							// stop on wrong target? (even if correct element, failure to meet criterion is considered wrong target for stopping exp)
							if( fBeganRecording && fStopWrongTarget ) {

								_emm.PostMessage( MAKELPARAM( MSG_TIMER, MSG_SUB_INC ),
												  (WPARAM) pDC, _TimerInc, fInvalidate, retmsg );
								invalidate |= fInvalidate;

								if( fWasGoingToReturnLeftBadTarget ) {

									if( fRedoWrongTarget ) return S_TARGETBADREDO_BADTIME;
									else return S_FALSE_BADTIME;
								}
								if( fRedoWrongTarget ) return S_TARGETBADREDO;
								else return S_FALSE;
							}
							else if( fWasGoingToReturnLeftBadTarget ) {

								return S_TARGETBADLEFT;
							}
						}

						OutputMessage( _T("DIGITIZER: Reached an incorrect target."), LOG_TABLET );

						if( fBeganRecording && fStopWrongTarget )
						{
							_emm.PostMessage( MAKELPARAM( MSG_TIMER, MSG_SUB_INC ),
											(WPARAM) pDC, _TimerInc, fInvalidate, retmsg );
							invalidate |= fInvalidate;

							// GMB: 2016-04-12: Adding logic for check if redo if wrong target
							if( fRedoWrongTarget ) return S_TARGETBADREDO;
							else return S_FALSE;
						}
						else if( fBeganRecording && fStopLastTarget && ( ( retmsg & MSG_STATUS_TARG_LAST ) != 0 ) )
						{
							_emm.PostMessage( MAKELPARAM( MSG_TIMER, MSG_SUB_INC ),
											(WPARAM) pDC, _TimerInc, fInvalidate, retmsg );
							invalidate |= fInvalidate;
							return S_FALSE;
						}
						res = S_TARGETBAD;
					}
				}
			}

			pt_lst->push_back( csr );		// 8/29/02: Added to see if points were missing
			pt_lstz->push_back( csrz );

			// To file
			if( fBeganRecording && m_szFile != _T("") && f.m_pStream )
			{
				if( ( xFile <= GetMaxX() ) && ( yFile <= GetMaxY() ) &&
					( xFile >= GetMinX() ) && ( yFile >= GetMinY() ) &&
					( pressure >= 0 ) && ( pressure <= MAX_PRESSURE ) )
				{
					// mouse only
					// check to see if we've accounted for the time from the very
					// first time mouse button was down until we actually have movement
					// we are writing n seconds * sampling rate of the next point
					// to ensure we get the proper stroke count
					if( fUseMouse && !fCheckedStartTime )
					{
						// we've reached our movement
						if( lSamples > 1 )
						{
							double dTime = StopWatch( 1, start, stop, freq );
							fCheckedStartTime = TRUE;

							// sampling rate * time (s) to determine # of points to write
							int nCount = (int)( dTime * ::GetSamplingRate() );
							for( int i = 0; i < nCount; i++ )
							{
								szLine.Format( _T("%g %g %g\n"), xFile, yFile, (double)33 );
								f.WriteString( szLine );
								lWrittenSamples++;
							}
						}
						// start clock after very first point
						else StopWatch( 0, start, stop, freq );
					}
					if( m_fTilt && m_fUseTilt )
						szLine.Format( _T("%g %g %g %d %d\n"), xFile, yFile,
									  (double)pressure, nAzimuth, nAltitude );
					else
						szLine.Format( _T("%g %g %g\n"), xFile, yFile, (double)pressure );
					f.WriteString( szLine );
					fWrote = TRUE;
					lWrittenSamples++;

					// DISCONTINUITY DETECTION
					// only process if after 1st sample (& not mouse)
					if( !fUseMouse && ( dwLastSample > 0 ) )
					{
						// if this sample is +/- m_dDisError greater than samp rate, report it to discontinuity file
						int nSampRate = ::GetSamplingRate();
						double dRange = ( nSampRate > 0 ) ? ( 1. / nSampRate ) * ( 1. + m_dDisError ) : 0.;
						double dDiff = (double)( ( 1. * dwTime - 1. * dwLastSample ) / 1000. );
						// just in case bogus data gets in, limit to 10 seconds
						if( ( dDiff < 10. ) && ( dDiff > dRange ) )
						{
							szLine.Format( _T("%u %.4f\n"), lWrittenSamples - 1, dDiff );
							fDis.WriteString( szLine );
							fDisWrote = TRUE;
							if( fOnTablet ) m_fDisBeforeTrailingPenLift = TRUE;
							// only beep if notification is set
							if( m_fDisNotify && m_wavDis ) {
								// switching to wav file
								//Beep( 500, 50 );
								PlaySound( (LPCSTR)m_wavDis, NULL, SND_MEMORY | SND_NOWAIT | SND_ASYNC );
							}
							szLine.Format( _T("RECORDING: WARNING: Sample %u has intersample period=%.3f s departing more than %.g * 1 / sampling rate (%.4f s). Appended to trial discontinuity file."),
										   lWrittenSamples - 1, dDiff, m_dDisError, ( 1. / nSampRate ) );
							OutputMessage( szLine, LOG_TABLET );
						}
					}
					dwLastSample = dwTime;
				}
			}
		}
		else									// 8/29/02: Added to see if points were missing
		{
			POINT pt;
			pt.x = -csr.x;
			pt.y = -csr.y;
			pt_lst->push_back( pt );
			pt.x = pressure;
			pt.y = 0;
			pt_lstz->push_back( pt );
		}
	}
	else if( fBeganRecording )	// When not on tablet but already started recording
	{
		if( !fNoDraw )
		{
			list<point>::iterator i = pt_lst->end();
			list<point>::iterator z = pt_lstz->end();
			if( !pt_lst->empty() ) i--;
			if( !pt_lstz->empty() ) z--;

			// old point (if no old point yet, values should be out of range....set to current point)
			POINT old;
			if( m_fFirstPoint )
			{
				old.x = abs( x );
				old.y = abs( y );
			}
			else
			{
				old.x = abs( i->x );
				old.y = abs( i->y );
			}

			DrawPoint( pDC, csr, old, fDown, fBeganRecording, m_fFirstPoint, fWasOutOfRange, m_fVaryLT ? csrz.x : -1 );
			if( m_fFirstPoint ) m_fFirstPoint = FALSE;
		}

		pt_lst->push_back( csr );
		pt_lstz->push_back( csrz );

		// To file
		if( ( xFile <= GetMaxX() ) && ( yFile <= GetMaxY() ) &&
			( xFile >= GetMinX() ) && ( yFile >= GetMinY() ) &&
			( pressure >= 0 ) && ( pressure <= MAX_PRESSURE ) && f.m_pStream )
		{
			if( m_fTilt && m_fUseTilt )
				szLine.Format( _T("%g %g %g %d %d\n"), xFile, yFile,
							   (double)pressure, nAzimuth, nAltitude );
			else
				szLine.Format( _T("%g %g %g\n"), xFile, yFile, (double)pressure );
			f.WriteString( szLine );
			fWrote = TRUE;
			lWrittenSamples++;

			// DISCONTINUITY DETECTION
			// only process if after 1st sample (& not mouse)
			if( !fUseMouse && ( dwLastSample > 0 ) )
			{
				// if this sample is +/- m_dDisError greater than samp rate, report it to discontinuity file
				int nSampRate = ::GetSamplingRate();
				double dRange = ( nSampRate > 0 ) ? ( 1. / nSampRate ) * ( 1. + m_dDisError ) : 0.;
				double dDiff = (double)( ( 1. * dwTime - 1. * dwLastSample ) / 1000. );
				// just in case bogus data gets in, limit to 10 seconds
				if( ( dDiff < 10. ) && ( dDiff > dRange ) )
				{
					szLine.Format( _T("%u %.4f\n"), lWrittenSamples - 1, dDiff );
					fDis.WriteString( szLine );
					fDisWrote = TRUE;
					// only beep if notification is set
					if( m_fDisNotify && m_wavDis ) {
						// switching to wav file
						//Beep( 500, 50 );
						PlaySound( (LPCSTR)m_wavDis, NULL, SND_MEMORY | SND_NOWAIT | SND_ASYNC );
					}
					szLine.Format( _T("RECORDING: WARNING: Sample %u has intersample period=%.3f s departing more than %.g * 1 / sampling rate (%.4f s). Appended to trial discontinuity file."),
								   lWrittenSamples - 1, dDiff, m_dDisError, ( 1. / nSampRate ) );
					OutputMessage( szLine, LOG_TABLET );
				}
			}
			dwLastSample = dwTime;
		}
	}

	// text out info
	long nPosX = 0, nPosY = 0;
	if( ( xFile <= GetMaxX() ) && ( yFile <= GetMaxY() ) &&
		( xFile >= GetMinX() ) && ( yFile >= GetMinY() ) )
	{
		nPosX = (long)xFile;
		nPosY = abs((long)yFile);
	}
	iPerSec2++;
	DWORD dwTick = ::GetTickCount();
	BOOL fTC = ( ( dwTick - lTimeHold2 ) >= 1000 );
	if( !m_fJustDisplay && !EEGPPT ) ::DoTextOut( pDC, nPosX, nPosY, (int)pressure, iPerSec2, fTC, nCount, fS, m_BGColor );
	if( fTC )
	{
		iPerSec2 = 0;
		lTimeHold2 = dwTick;
	}

	// Draw a new cursor
	if( !fNoDraw && ( csr.x > 0 ) && !fUseMouse )
	{
		CRgn r1, r2, r3;
		r1.CreateEllipticRgn( csr.x - 4, csr.y - 4, csr.x + 4, csr.y + 4 );
		r2.CreateEllipticRgn( csr.x - 3, csr.y - 3, csr.x + 3, csr.y + 3 );
		r3.CreateEllipticRgn( csr.x - 4, csr.y - 4, csr.x + 4, csr.y + 4 );
		r3.CombineRgn( &r1, &r2, RGN_XOR );
		pDC->InvertRgn( &r3 );
	}

	fWasOutOfRange = fNoDraw;

	return res;
}

LRESULT Digitizer::OnWTPacket2( WPARAM wSerial, LPARAM aContext, CDC* pDC, BOOL& invalidate )
{
// TODO:
// BEGIN HACK : We're getting consistent junk data about every 15 packets
// ...currently, ignoring it...which is causing a break in the recording window
// ...temp solution is to still ignore for data file, but duplicate last values
// ...just for display purposes
	static long xLast = -1;
	static long yLast = -1;
	static int pressLast = -1;
	BOOL fHack = FALSE;
// END HACK

	// Process packets in order, one at a time
	if( pWTMutex ) CSingleLock lock( pWTMutex, TRUE );

	// Read the packet
	PACKET pkt;
	HINSTANCE hMod = ::GetTabletInstance();
	if( !hMod ) return E_FAIL;
	LPFNWTPACKET _procWTPacket = NULL;
	_procWTPacket = (LPFNWTPACKET)GetProcAddress( hMod, "WTPacket" );
	if( !_procWTPacket )
	{
		FreeTabletInstance(hCtx);
		return E_FAIL;
	}

	// sometimes we get packets with a tablet context of 0 (which is bad!)
	if( aContext == 0 )
	{
		lBadHandles++;
//		return S_OK;
		return S_BADHANDLE;
	}

	(*_procWTPacket) ( hCtx, wSerial, &pkt );

	// digitizer coordinates to windows coordinates
	LONG xPos = 0L, yPos = 0L;
	double dx = 0., dy = 0.;
	BOOL fNoDraw = FALSE;
	if( !fUseMouse )
	{
		double dX, dY;
		::GetTabletDimension( dX, dY );
		DigitizerToCM( lc.lcInExtX, lc.lcInExtY, dx, dy );
		if( dX == 0. ) dX = dx;
		if( dY == 0. ) dY = dy;
		fNoDraw = DigitizerToScreen( (long)( ( dx / dX ) * pkt.pkX ),
									 (long)( ( dy / dY ) * pkt.pkY ),
									 xPos, yPos );
		if( ( dX < dx ) || ( dY < dy ) ) fShiftMouse = TRUE;
		else fShiftMouse = FALSE;
	}
	else fNoDraw = DigitizerToScreen( pkt.pkX, pkt.pkY, xPos, yPos );

	// increment sample count
	lSamples++;

	// bad data?
	if( ( xPos == -1 ) || ( yPos == -1 ) )
	{
		lBadPoints++;
//		return S_OK;
		return S_BADHANDLE;
	}

	// we've begun recording
	if( fRecordImmediately && !fBeganRecording && !fStartFirstTarget )
	{
		fBeganRecording = TRUE;
		BOOL fInval;
		WORD retmsg;
		_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_BEGAN_RECORDING ),
						  MAKEWPARAM( fBeganRecording, 0 ), 0, fInval, retmsg );

		OutputMessage( _T("DIGITIZER: Recording has begun."), LOG_TABLET );
	}

	// 09Jul10: Somesh: Programming error lx was updated and then used in ly equation
	// Rotation of feedback was wrong
	long xPostemp;

	// transform if required
	if( fTransform && fBeginTransform )
	{
		/*xPos = (long)( rotOrigin.x +
					   ( ( xPos - rotOrigin.x ) * dyy +
						 ( yPos - rotOrigin.y ) * dxy ) );*/
		xPostemp = (long)( rotOrigin.x +
					   ( ( xPos - rotOrigin.x ) * dyy +
						 ( yPos - rotOrigin.y ) * dxy ) );
		yPos = (long)( rotOrigin.y +
					   ( ( xPos - rotOrigin.x ) * dyx +
						 ( yPos - rotOrigin.y ) * dxx ) );

		xPos = xPostemp;
	}

	// pen pressure
	long pressNew = PrsAdjust( pkt );
	if( fStartFirstTarget && !fBeganRecording ) pressNew = 0;

// TODO:
// BEGIN HACK
	if( ( pkt.pkX > GetMaxX() ) || ( pkt.pkY > GetMaxY() ) ||
		( pkt.pkX >= GetMinX() ) && ( pkt.pkY >= GetMinY() ) &&
		( xPos < 0 ) || ( yPos < 0 ) || ( pressNew > MAX_PRESSURE ) )
	{
		xPos = xLast;
		yPos = yLast;
		pressNew = pressLast;
		fNoDraw = FALSE;
	}
	else
	{
		xLast = xPos;
		yLast = yPos;
		pressLast = pressNew;
	}
// END HACK

	BOOL fDown = FALSE, fButtons = FALSE;
	// 07July09: GMB: We want to begin transformations as soon as the recording begins
	// so we check for record immediately OR buttons down
	if( fRecordImmediately || 
		( ( pkt.pkButtons == 1 ) || ( pkt.pkButtons == 3 ) || ( pkt.pkButtons == 5 ) ) )
	{
		fDown = TRUE;
		if( ( pkt.pkButtons == 1 ) || ( pkt.pkButtons == 3 ) || ( pkt.pkButtons == 5 ) ) 
fButtons = TRUE;
		// GMB: 2016-05-24: Transformation has been based on an incorrect origin, seemingly from 'ready to record'
		// vs. when the first relevant (to-be-recorded) sample is acquired
		// SOLUTION: Check whether recording began, also
		//if(  fTransform && !fBeginTransform )
		if( fBeganRecording && fTransform && !fBeginTransform )
		{
			fBeginTransform = TRUE;
			rotOrigin.x = xPos;
			rotOrigin.y = yPos;
		}
	}

	dx = pkt.pkX;
	dy = pkt.pkY;

	// if pen pressure is above max - notify
	static BOOL fGaveWarning = FALSE;
	int nPressMax = m_nMPP;
	if( pressNew > nPressMax )
	{
// This was causing major lag coupled with discontinuity reporting
//		Beep( 1000, 2 );
		if( !m_fSoundPlaying && m_wavMPP ) {
			PlaySound( (LPCSTR)m_wavMPP, NULL, SND_MEMORY | SND_NOWAIT | SND_ASYNC | SND_LOOP );
			m_fSoundPlaying = TRUE;
		}
		if( !fGaveWarning ) {
			CString szMsg;
			szMsg.Format( _T("Pen pressure %d has exceeded recommended maximum: %d."), pressNew, nPressMax );
			OutputMessage( szMsg, LOG_TABLET );
			fGaveWarning = TRUE;
		}
	}
	else {
		if( m_fSoundPlaying ) PlaySound( NULL, NULL, SND_ASYNC );
		m_fSoundPlaying = FALSE;
		fGaveWarning = FALSE;
	}

	// get tilt
	int nAz = 0, nAlt = 0, nTwist = 0;
	if( m_fTilt && m_fUseTilt )
	{
		nAz = pkt.pkOrientation.orAzimuth;
		nAlt = pkt.pkOrientation.orAltitude;
		nTwist = pkt.pkOrientation.orTwist;
	}

	return OnPacket( pDC, xPos, yPos, pressNew, pkt.pkTime, dx, dy, fDown, fNoDraw,
					 invalidate, nAz, nAlt, nTwist, fButtons );
}

STDMETHODIMP Digitizer::get_TimeoutTicks(long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	*pVal = nTimeoutTicks;

	return S_OK;
}

STDMETHODIMP Digitizer::OnMouseMove(long Flags, VARIANT* Point, VARIANT* DC, BOOL* invalidate )
{
	if( !VerifyAuthorization() ) return E_FAIL;

	CPoint* pPoint = (CPoint*)Point;
	if( !pPoint )
	{
		::OutputMessage( _T("Invalid mouse point."), LOG_TABLET );
		return E_FAIL;
	}
	CDC* pDC = (CDC*)DC;
	if( !pDC )
	{
		::OutputMessage( _T("Unable to obtain WINDOWS DC object."), LOG_TABLET );
		return E_FAIL;
	}

	// is left mouse button down
	BOOL fDown = ( ( Flags & MK_LBUTTON ) != 0 );

	// get coordinates
	long lx, ly;
	double dx, dy;
	lx = abs( pPoint->x );
	ly = pPoint->y;
	dx = lx;
	dy = -ly;

	// 09Jul10: Somesh: Programming error lx was updated and then used in ly equation
	// Rotation of feedback was wrong
	long lxtemp;
	
	// transform if required
	if( fTransform && fBeginTransform )
	{
		/*lx = (long)( rotOrigin.x +
					 ( ( lx - rotOrigin.x ) * dyy +
					   ( ly - rotOrigin.y ) * dxy ) );*/
		lxtemp = (long)( rotOrigin.x +
					 ( ( lx - rotOrigin.x ) * dyy +
					   ( ly - rotOrigin.y ) * dxy ) );
		ly = (long)( rotOrigin.y +
					 ( ( lx - rotOrigin.x ) * dyx +
					   ( ly - rotOrigin.y ) * dxx ) );

		lx = lxtemp;
	}

	// GMB: 2016-05-24: Transformation has been based on an incorrect origin, seemingly from 'ready to record'
	// vs. when the first relevant (to-be-recorded) sample is acquired
	// SOLUTION: Check whether recording began, also
//	if( fDown && fTransform && !fBeginTransform )
	if( fBeganRecording && fDown && fTransform && !fBeginTransform )
	{
		fBeginTransform = TRUE;
		rotOrigin.x = lx;
		rotOrigin.y = ly;
	}

	BOOL fInval = FALSE;
	WORD retmsg;
	if( fDown ) lSamples++;
	if( fRecordImmediately && !fBeganRecording && !fStartFirstTarget )
	{
		fBeganRecording = TRUE;
		_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_BEGAN_RECORDING ),
						  MAKEWPARAM( fBeganRecording, 0 ), 0, fInval, retmsg );
	}

	// pressure is 99 unless button not down or haven't begun recording
	long pressNew = 99;
	if( !fDown || ( fStartFirstTarget && !fBeganRecording ) ) pressNew = 0;

	LRESULT res = OnPacket( pDC, lx, ly, pressNew, 0, dx, dy, fDown, FALSE, fInval, 0, 0, 0, fDown );
	*invalidate = fInval;

	if( fBeganRecording && !fAlertedBegan )
	{
		fAlertedBegan = TRUE;
		// GMB: 2016-07-17: We need to know about 1st target for triggers
		if( res == S_TARGETGOODFIRST ) {

			return S_START_FIRST;
		}
		return S_START;
	}
	else return res;
}

STDMETHODIMP Digitizer::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetTabletLogOn( fOn );

	return S_OK;
}

STDMETHODIMP Digitizer::put_SamplingRate(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	::SetSamplingRate( (int)newVal );

	return S_OK;
}

STDMETHODIMP Digitizer::put_MinPenPressure(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	::SetMinPenPressure( newVal );

	return S_OK;
}

STDMETHODIMP Digitizer::put_VaryLineThickness(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_fVaryLT = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::put_MaxLineThickness(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	::SetMaxLineThickness( newVal );

	return S_OK;
}

STDMETHODIMP Digitizer::put_DeviceResolution(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	::SetDeviceResolution( newVal );

	return S_OK;
}

STDMETHODIMP Digitizer::put_BkColor(DWORD newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	dwBkColor = newVal;
	::SetBackgroundColor( dwBkColor );

	return S_OK;
}

STDMETHODIMP Digitizer::InitializeForStimulus(VARIANT *DC, BOOL UseMouse)
{
	if( !VerifyAuthorization() ) return E_FAIL;

	fUseMouse = UseMouse;
	nTimeoutTicks = 0L;
	m_fFirstPoint = TRUE;
	CDC* pDC = (CDC*)DC;
	_TimerInc = 0;
	lBadPoints = 0;
	lSamples = 0;

	csr.x = -1;

	// Check if Wintab is available
	if( !fUseMouse )
	{
		HINSTANCE hMod = ::GetTabletInstance();
		if( !hMod ) return E_FAIL;

		LPFNWTINFO _procWTInfo = NULL;
		_procWTInfo = (LPFNWTINFO)GetProcAddress( _hTablet, "WTInfoA" );
		if( !_procWTInfo )
		{
			FreeTabletInstance(hCtx);
			return E_FAIL;
		}

		if( !((*_procWTInfo) ( 0, 0, NULL )) )
		{
			::OutputMessage( _T("WTInfo failed during initialzation."), LOG_TABLET );
			return E_FAIL;
		}

		// Get default context information
		(*_procWTInfo) ( WTI_DEFCONTEXT, 0, &lc );
	}

	if( !pt_lst ) pt_lst = new list<point>;
	if( !pt_lstz ) pt_lstz = new list<point>;

	// display settings for manager
	BOOL fInval = FALSE;
	WORD retmsg;
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_MOUSE ),
					  MAKEWPARAM( UseMouse, 0 ), 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_ONLY ),
					  MAKEWPARAM( fTabletOnly, 0 ), 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_REAL ),
					  MAKEWPARAM( fRealSize, 0 ), 0, fInval, retmsg );
	if( !UseMouse )
	{
		_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTX ),
						  (WPARAM)lc.lcInExtX, 0, fInval, retmsg );
		_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTY ),
						  (WPARAM)lc.lcInExtY, 0, fInval, retmsg );
	}

	double dX, dY;
	::GetDisplayDimension( dX, dY );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_X ),
						dX, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_Y ),
						dY, 0, fInval, retmsg );
	::GetTabletDimension( dX, dY );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_TABLET_X ),
						dX, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_TABLET_Y ),
						dY, 0, fInval, retmsg );

	// start'em up
	_emm.PostMessage( MAKELPARAM( MSG_RESET, MSG_SUB_ELMT_NORMAL ),
					  (WPARAM) pDC, 0, fInval, retmsg );

	return S_OK;
}

STDMETHODIMP Digitizer::put_NotifyOfBadTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fNotifyBad = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::put_RecordImmediately(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fRecordImmediately = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::put_StopWrongTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fStopWrongTarget = newVal;

	return S_OK;
}

// GMB: 2016-04-12: Added new param: Redo if wrong target
STDMETHODIMP Digitizer::put_RedoIfWrongTarget(BOOL newVal) {

	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	fRedoWrongTarget = newVal;
	return S_OK;
}

STDMETHODIMP Digitizer::put_StartFirstTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fStartFirstTarget = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::put_StopLastTarget(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	fStopLastTarget = newVal;

	return S_OK;
}

STDMETHODIMP Digitizer::get_TabletDesc(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !VerifyAuthorization() ) return E_FAIL;

	HINSTANCE hMod = ::GetTabletInstance();
	if( !hMod ) return E_FAIL;

	LPFNWTINFO _procWTInfo = NULL;
	_procWTInfo = (LPFNWTINFO)GetProcAddress( _hTablet, "WTInfoA" );
	if( !_procWTInfo )
	{
		FreeTabletInstance(hCtx);
		return E_FAIL;
	}

	int nVal = (*_procWTInfo)( WTI_DEVICES, DVC_NAME, NULL );
	if( nVal == 0 )
	{
		FreeTabletInstance(hCtx);
		return E_FAIL;
	}

	char bfr[ 100 ];
//	(*_procWTInfo)( WTI_INTERFACE, IFC_WINTABID, (LPVOID)bfr );	// returns wintab
//	(*_procWTInfo)( WTI_INTERFACE, DVC_NAME, (LPVOID)bfr );	// returns wintab
	(*_procWTInfo)( WTI_DEVICES, DVC_NAME, (LPVOID)bfr );		// returns wacom
	CString szInfo = bfr;
	*pVal = szInfo.AllocSysString();

	return S_OK;
}

STDMETHODIMP Digitizer::get_TabletXRange(long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !VerifyAuthorization() ) return E_FAIL;

	HINSTANCE hMod = ::GetTabletInstance();
	if( !hMod ) return E_FAIL;

	LPFNWTINFO _procWTInfo = NULL;
	_procWTInfo = (LPFNWTINFO)GetProcAddress( hMod, "WTInfoA" );
	if( !_procWTInfo )
	{
		FreeTabletInstance(hCtx);
		return E_FAIL;
	}

	AXIS a;
	a.axUnits = TU_CENTIMETERS;
	(*_procWTInfo)( WTI_DEVICES, DVC_X, (LPVOID)&a );
	// +1 to account for the leading portion
	*pVal = a.axMax + 1;

	return S_OK;
}

STDMETHODIMP Digitizer::get_TabletYRange(long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !VerifyAuthorization() ) return E_FAIL;

	HINSTANCE hMod = ::GetTabletInstance();
	if( !hMod ) return E_FAIL;

	LPFNWTINFO _procWTInfo = NULL;
	_procWTInfo = (LPFNWTINFO)GetProcAddress( _hTablet, "WTInfoA" );
	if( !_procWTInfo )
	{
		FreeTabletInstance(hCtx);
		return E_FAIL;
	}

	AXIS a;
	a.axUnits = TU_CENTIMETERS;
	(*_procWTInfo)( WTI_DEVICES, DVC_Y, (LPVOID)&a );
	// +1 to account for the leading portion
	*pVal = a.axMax + 1;

	return S_OK;
}

STDMETHODIMP Digitizer::get_TabletRate(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !VerifyAuthorization() ) return E_FAIL;

	Initialize( NULL, FALSE, FALSE );
	*pVal = lc.lcPktRate;

	return S_OK;
}

STDMETHODIMP Digitizer::get_TabletResolution(long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !VerifyAuthorization() ) return E_FAIL;

	HINSTANCE hMod = ::GetTabletInstance();
	if( !hMod ) return E_FAIL;

	LPFNWTINFO _procWTInfo = NULL;
	_procWTInfo = (LPFNWTINFO)GetProcAddress( hMod, "WTInfoA" );
	if( !_procWTInfo )
	{
		FreeTabletInstance(hCtx);
		return E_FAIL;
	}

	AXIS a;
	(*_procWTInfo)( WTI_DEVICES, DVC_X, (LPVOID)&a );
	*pVal = INT( a.axResolution );

	if( a.axUnits == TU_CENTIMETERS ) 
		return S_OK;
	else 
		return S_FALSE;
}

STDMETHODIMP Digitizer::Clear(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( !VerifyAuthorization() ) return E_FAIL;

	_emm.Clear();
	m_fFirstPoint = TRUE;
	if( pt_lst )
	{
		delete pt_lst;
		pt_lst = new list<point>;
	}
	if( pt_lstz )
	{
		delete pt_lstz;
		pt_lstz = new list<point>;
	}

	return S_OK;
}

STDMETHODIMP Digitizer::put_DisplayDimension( double ddX, double ddY )
{
	::SetDisplayDimension( ddX, ddY );

	return S_OK;
}

STDMETHODIMP Digitizer::put_TabletDimension( double dtX, double dtY )
{
	::SetTabletDimension( dtX, dtY );

	return S_OK;
}

STDMETHODIMP Digitizer::SetStimulusInfo( BSTR StimPath, BSTR StimID, BOOL DeactivateWrongTarget )
{
	if( !VerifyAuthorization() ) return E_FAIL;

	CString szPath = StimPath, szID = StimID;
	if( ( szPath == _T("") ) || ( szID == _T("") ) ) return E_FAIL;

	// Read data files to obtain elements & targets
	CString szTheFile = szPath;
	szTheFile += szID;
	szTheFile += _T(".NFO");

	// construct target results file path from raw file
	int nPos = m_szFile.ReverseFind( '.' );
	CString szDataPath = 
		( nPos > 0 ) ? m_szFile.Left( nPos ) : _T("");
	szDataPath += _T(".TGT");
	BOOL fNoFile = ( szDataPath.CompareNoCase( _T(".TGT") ) == 0 );

	_emm.Clear();

	// open info file which tells us what elements & what targets
	// if file does not exist, then this must be just an element
	CStdioFile f;
	if( f.Open( szTheFile, CFile::modeRead ) )
	{
		// open target results file
		CStdioFile ft;
		if( !fNoFile && !ft.Open( szDataPath, CFile::modeWrite | CFile::modeCreate ) ) return FALSE;

		CString szItem, szLine;
		int nCount = 0, i = 0;
		CStringList lstElem;
		POSITION pos;

		// 1st line = # of elements
		if( !f.ReadString( szLine ) ) return FALSE;
		nCount = atoi( szLine );
		if( nCount <= 0 ) return S_OK;
		// read through each element & add to string list
		for( i = 0; i < nCount; i++ )
		{
			if( !f.ReadString( szLine ) ) return FALSE;
			if( !lstElem.Find( szLine ) ) lstElem.AddTail( szLine );
		}

		// next line = # of targets
		if( !f.ReadString( szLine ) ) return FALSE;
		nCount = atoi( szLine );
		// write this count to the target results file
		szItem.Format(_T("%d\n"), nCount );
		if( !fNoFile ) ft.WriteString( szItem );
		// read through each target & add to message manager
		// duplicates will exist where a single element is a target more than once
		ElementManager *emgr = NULL, *prevemgr = NULL;
		for( i = 0; i < nCount; i++ )
		{
			if( !f.ReadString( szLine ) ) return FALSE;
			emgr = _emm.AddItem( szLine, szPath, szDataPath, TRUE, prevemgr );
			prevemgr = emgr;
			pos = lstElem.Find( szLine );
			if( pos ) lstElem.RemoveAt( pos );

			// add to target results file
			if( !fNoFile )
			{
				ft.WriteString( szLine );
				ft.WriteString( _T("\n") );
			}
		}

		// write this count to the target results file to indicate end of list
		// since we don't know how many targets will actually be reached
		if( !fNoFile )
		{
			szItem.Format(_T("%d - this line has no meaning\n"), nCount );
			ft.WriteString( szItem );
		}

		// NOW....(sorta hack)...add those elements which are not targets to end of list
		pos = lstElem.GetHeadPosition();
		while( pos )
		{
			szLine = lstElem.GetNext( pos );
			_emm.AddItem( szLine, szPath, szDataPath, FALSE, NULL );
		}
	}
	else _emm.AddItem( szID, szPath, szDataPath, FALSE, NULL );

	BOOL fInval;
	WORD retmsg;
	// set the tablet & target windows for the elements (used for display)
	_emm.PostMessage( MAKELPARAM( MSG_HANDLES, MSG_SUB_ELMT_TABLET ),
					  (WPARAM) m_hWndTablet, 0, fInval, retmsg );
	_emm.PostMessage( MAKELPARAM( MSG_HANDLES, MSG_SUB_ELMT_TARGET ),
					  (WPARAM) m_hWndTarget, 0, fInval, retmsg );

	// incorrect target settings
	_emm.PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_TARG_MODE ),
					  MAKEWPARAM( DeactivateWrongTarget, 0 ), 0, fInval, retmsg );

	return S_OK;
}

STDMETHODIMP Digitizer::DoOnTimer( long Increment, VARIANT* DC )
{
	if( !VerifyAuthorization() ) return E_FAIL;

	_TimerInc += Increment;

	BOOL fInval = FALSE;
	WORD retmsg;
	CDC* pDC = (CDC*)DC;
	_emm.PostMessage( MAKELPARAM( MSG_TIMER, MSG_SUB_INC ),
					  (WPARAM) pDC, _TimerInc, fInval, retmsg );

	HRESULT hr = S_OK;
	if( fInval )
	{
		hr = S_INVALIDATE;
		_emm.PostMessage( MAKELPARAM( MSG_RESET, 0 ),
						  (WPARAM) pDC, _TimerInc, fInval, retmsg );
	}

	return hr;
}

STDMETHODIMP Digitizer::Transformation(BOOL Transform, double xgain, double xrotation,
									   double ygain, double yrotation)
{
	fTransform = Transform;
	dxx = ygain * cos( xrotation * PI / 180 );
	dyx = -ygain * sin( xrotation * PI / 180 );
	dxy = xgain * sin( yrotation * PI / 180 );
	dyy = xgain * cos( yrotation * PI / 180 );

	return S_OK;
}

STDMETHODIMP Digitizer::CountStrokes(BOOL Count, short MaxStrokes)
{
	fCountStrokes = Count;
	nMaxStrokes = MaxStrokes;

	return S_OK;
}

STDMETHODIMP Digitizer::HideShowAfterStrokes(BOOL HideShowAfter, BOOL Show, double Strokes)
{
	fHideShowAfterStrokes = HideShowAfter;
	fHideShowAfterShow = Show;
	dStrokeCount = Strokes;

	if( fHideShowAfterStrokes ) ::SetIsHidden( fHideShowAfterShow );

	return S_OK;
}

STDMETHODIMP Digitizer::HideShow(BOOL Show)
{
	::SetIsHidden( !Show );

	return S_OK;
}

#define NSEGMAX			200		// must be high because of undescarded zero crossings
int t_seg[ NSEGMAX ];
int x_seg[ NSEGMAX ];
int y_seg[ NSEGMAX ];

/*****************************************/
int segment( int x, int y, int nsample )
/*****************************************/
/*
UPDATES:
  20Apr95: Reset when nsample=0 at neutral value prevents inheritance between trials
*/
{
	#define LARGE_COORDINATE	32000
	#define DOWNUP_T			5
	#define UPDOWN_T			5

	static int y_old = 0;
	static int xdown = 0, ydown = 0, xup = 0, yup = 0;
	static int iupordown = 0;
	static int iup_flag = 0;
	static int idown_flag = 0;
	static int nstroke = 0;

	/*******************************************************/
	/*** End stroke reached when y velocity changes sign ***/
	/*******************************************************/
	/* Initialize last up and down points and previous x to first data point */
	/* because it is not clear whether there is first an upstroke or a downstroke */
	if( nsample == 0 )
	{
		xdown = x;
		ydown = y;
		xup = x;
		yup = y;
		y_old = y;
		/* Neutral value */
		iup_flag = -1;
		idown_flag = -1;

		nstroke = 0;
		memset( t_seg, 0, NSEGMAX * sizeof( int ) );
		memset( x_seg, 0, NSEGMAX * sizeof( int ) );
		memset( y_seg, 0, NSEGMAX * sizeof( int ) );
	}

	/* While current y larger than previous y, then the movement is upward */
	/*** Test for down to up transition ***/
	if( y > y_old )
	{
		/* Going upward */
		/* Store highest position since this stroke */
		if( y > yup )
		{
			xup = x;
			yup = y;
		}
		/* Increase upward samples counter or set to zero if negative */
		iupordown++;
		if( iupordown < 0 ) iupordown = 0;
		/* When more than UPDOWN_T consecutive upward samples */
		/* while the movement was not recognized as upward */
		if( ( iupordown > UPDOWN_T ) && ( iup_flag != TRUE ) )
		{
			/* Disable this event until upstroke is false */
			iup_flag = TRUE;
			idown_flag = FALSE;

			/* Store lowest downward point of previous stroke */
			t_seg[ nstroke ] = nsample;
			x_seg[ nstroke ] = xdown;
			y_seg[ nstroke ] = ydown;
			nstroke++;
			if( nstroke >= NSEGMAX ) nstroke = NSEGMAX - 1;

			/* Set the lowest downward point to some high value */
			xdown = LARGE_COORDINATE;
			ydown = LARGE_COORDINATE;
		}
	}
	/* Else while current y larger than previous y, then the movement is downward */
	/*** Test for up to down transition ***/
	else if( y < y_old )
	{
		/* Store lowest position since this stroke */
		if( y < ydown )
		{
			xdown = x;
			ydown = y;
		}
		/* Decrease upward samples counter or set to zero if positive */
		iupordown--;
		if( iupordown > 0 ) iupordown = 0;
		/* When more than DOWNUP_T consecutive downward samples */
		/* while the movement was not recognized as downward */
		if( ( iupordown < -DOWNUP_T ) && ( idown_flag != TRUE ) )
		{
			/* Disable this event until downstroke is false */
			//printf ("Down ");
			idown_flag = TRUE;
			iup_flag = FALSE;

			t_seg[ nstroke ] = nsample;
			x_seg[ nstroke ] = xup;
			y_seg[ nstroke ] = yup;
			nstroke++;
			if( nstroke >= NSEGMAX ) nstroke = NSEGMAX - 1;

			/* Set the highest upstroke point to some low value */
			xup = -LARGE_COORDINATE;
			yup = -LARGE_COORDINATE;
		}
	}
	/* Else zero displacement, set upward samples counter to neutral */
	else
	{
		iupordown = 0;
		/* Update last point as it may be the end of movement point */
		t_seg[ nstroke ] = nsample;
		x_seg[ nstroke ] = xup;
		y_seg[ nstroke ] = yup;
	}

	/* Current y becomes previous y */
	y_old = y;

	return nstroke;
}

STDMETHODIMP Digitizer::put_LeftViewMaximizeSize( int nSize )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	::SetLeftViewMaximizeSize( nSize );
	return S_OK;
}

STDMETHODIMP Digitizer::put_RealSize( BOOL newVal )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	fRealSize = newVal;
	return S_OK;
}
STDMETHODIMP Digitizer::DrawSettings( BOOL LineFixed, short LineSize, DWORD LineColor1,
									  DWORD LineColor2, DWORD LineColor3, DWORD MPPColor,
									  DWORD EllipseColor, short EllipseSize, DWORD BGColor )
{
	if( !VerifyAuthorization() ) return E_FAIL;

	m_BGColor = (COLORREF)BGColor;
	::DrawSettings( LineFixed, LineSize, LineColor1, LineColor2, LineColor3, MPPColor,
					EllipseColor, EllipseSize );

	return S_OK;
}

STDMETHODIMP Digitizer::SupportsTilt(BOOL *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = m_fTilt;
	return S_OK;
}

STDMETHODIMP Digitizer::put_UseTilt(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_fUseTilt = newVal;
	return S_OK;
}

STDMETHODIMP Digitizer::put_MaxPenPressure(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_nMPP = newVal;
	::SetMaxPenPressure( m_nMPP );
	return S_OK;
}

STDMETHODIMP Digitizer::get_MaxPenPressure(short *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	*pVal = (short)::GetMaxPenPressure();
	return S_OK;
}

STDMETHODIMP Digitizer::put_DisError(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_dDisError = newVal;
	return S_OK;
}

STDMETHODIMP Digitizer::put_DisNotify(BOOL newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	m_fDisNotify = newVal;
	return S_OK;
}

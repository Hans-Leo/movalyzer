// ItemManager.cpp

#include "StdAfx.h"
#include "InputMod.h"
#include "ItemManager.h"
#include "MessageManager.h"
#include "..\Common\DefFun.h"
#include "..\Common\Shared.h"
#include <math.h>

// some purdy defines for ya
#define	CM_IN			2.54	// centimeters to inch
// shapes
#define	SHAPE_RECTANGLE	1
#define	SHAPE_ELLIPSE	2

#pragma warning( disable: 4996 )	// disable deprecated warning

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

ItemManager::ItemManager( MessageManager* pMM )
	: m_pDaddy( pMM ),
	  m_szID( _T("") ),
	  m_nItem( 0 )
{
}

///////////////////////////////////////////////////////////////////////////////

ItemManager::~ItemManager()
{
}

///////////////////////////////////////////////////////////////////////////////

int ItemManager::PostMessage( LPARAM msg, WPARAM val, ULONG clock,
							  BOOL& invalidate, BOOL& terminate, WORD& returnmsg )
{
	invalidate = FALSE;
	returnmsg = MSG_STATUS_NONE;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int ItemManager::PostMessage( LPARAM msg, double val, ULONG clock,
							  BOOL& invalidate, BOOL& terminate, WORD& returnmsg )
{
	invalidate = FALSE;
	returnmsg = MSG_STATUS_NONE;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

ElementManager::ElementManager( ElementMessageManager* pMM, ElementManager* prevemgr )
	: ItemManager( pMM ) 
	, m_nLines( 0 )
	, m_hWndTarget( NULL )
	, m_hWndTablet( NULL )
	, m_fUseMouse( FALSE )
	, m_lExtX( 0 )
	, m_lExtY( 0 )
	, m_dStart( 0. )
	, m_dDuration( 0. )
	, m_fDisplayed( FALSE )
	, m_fDisplay( TRUE )
	, m_fReadTargetInfo( FALSE )
	, m_pPrevious( prevemgr )
	, m_fReached( FALSE )
	, m_fStop( FALSE )
	, m_fTarget( FALSE )
	, m_fInAndOut( FALSE )
	, m_esState( esNormal )
	, m_fCorrect( FALSE )
	, m_fAnimFile( FALSE )
	, m_ulClockAnim( 0 )
	, m_f1stPoint( FALSE )
	, m_thex( NULL )
	, m_they( NULL )
	, m_nPoints( 0 )
	, m_fInit( FALSE )
	, m_fTabletOnly( FALSE )
	, m_dDisplayX( 0. )
	, m_dDisplayY( 0. )
	, m_fRealSize( FALSE )
{
}

///////////////////////////////////////////////////////////////////////////////

ElementManager::~ElementManager()
{
	if( m_thex ) delete [] m_thex;
	if( m_they ) delete [] m_they;
}

///////////////////////////////////////////////////////////////////////////////

void ElementManager::Reset()
{
	BOOL fVal = ReadFiles();
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::ReadFiles()
{
	if( !ReadDecorFile( m_nLines ) || ( m_nLines <= 0 ) ) return FALSE;
	if( !ReadDataFile() ) return FALSE;
	if( !ReadBehaviorFile() ) return FALSE;
	return ReadAnimationFile();
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::ReadDecorFile( int& nDataLines )
{
	if( m_szID == _T("") ) return FALSE;

	// READ # n element decoration from file...specifies how many lines to read
	// from data file.

	// TODO: We need to find a shared way to know how decor file is structured

	int nPos = -1;
	CStdioFile fd;
	CString szFile = m_szPath, szLine;

	// open file for reading
	szFile += m_szID;
	szFile += _T(".FMT");
	if( !fd.Open( szFile, CFile::modeRead ) ) return FALSE;

	// element id
	if( !fd.ReadString( szLine ) ) return FALSE;
	// # of lines
	if( !fd.ReadString( szLine ) ) return FALSE;
	nDataLines = atoi( szLine );
	// line sizes
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%d %d %d", &m_di1.nLineSize, &m_di2.nLineSize, &m_di3.nLineSize );
	// line colors
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%u %u %u", &m_di1.dwLineColor, &m_di2.dwLineColor, &m_di3.dwLineColor );
	// background colors
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%u %u %u", &m_di1.dwBkColor, &m_di2.dwBkColor, &m_di3.dwBkColor );
	// text
	if( !fd.ReadString( szLine ) ) return FALSE;
	nPos = szLine.Find( _T(";") );
	if( nPos != -1 )
	{
		strcpy_s( m_di1.szText, szELEMENT_TEXT, szLine.Left( nPos ) );
		szLine = szLine.Mid( nPos + 1 );
		nPos = szLine.Find( _T(";") );
		if( nPos != -1 )
		{
			strcpy_s( m_di2.szText, szELEMENT_TEXT, szLine.Left( nPos ) );
			strcpy_s( m_di3.szText, szELEMENT_TEXT, szLine.Mid( nPos + 1 ) );
		}
	}
	// text sizes
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%d %d %d", &m_di1.nSize, &m_di2.nSize, &m_di3.nSize );
	// text colors
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%u %u %u", &m_di1.dwColor, &m_di2.dwColor, &m_di3.dwColor );
	// font info (1 set per line)
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%u %d %d %d %d", &m_di1.lWeight, &m_di1.bI, &m_di1.bU, &m_di1.bSO, &m_di1.bPitch );
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%u %d %d %d %d", &m_di2.lWeight, &m_di2.bI, &m_di2.bU, &m_di2.bSO, &m_di2.bPitch );
	if( !fd.ReadString( szLine ) ) return FALSE;
	sscanf( szLine, "%u %d %d %d %d", &m_di3.lWeight, &m_di3.bI, &m_di3.bU, &m_di3.bSO, &m_di3.bPitch );
	// text fonts
	if( !fd.ReadString( szLine ) ) return FALSE;
	nPos = szLine.Find( _T(";") );
	if( nPos != -1 )
	{
		strcpy_s( m_di1.szFont, 50, szLine.Left( nPos ) );
		szLine = szLine.Mid( nPos + 1 );
		nPos = szLine.Find( _T(";") );
		if( nPos != -1 )
		{
			strcpy_s( m_di2.szFont, 50, szLine.Left( nPos ) );
			strcpy_s( m_di3.szFont, 50, szLine.Mid( nPos + 1 ) );
		}
	}
	// Is pattern
	m_di1.fIsPattern = FALSE;
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_di1.fIsPattern = atoi( szLine );
	// Is image
	m_di1.fIsImage = FALSE;
	m_di1.dCenterX = 0.;
	m_di1.dCenterY = 0.;
	if( fd.ReadString( szLine ) )
	{
		// image file
		m_di1.fIsImage = atoi( szLine );
		if( fd.ReadString( szLine ) )
		{
			strcpy_s( m_di1.szBMP, _MAX_PATH, szLine );
			// centers
			if( !fd.ReadString( szLine ) ) return FALSE;
			sscanf( szLine, "%lf %lf", &m_di1.dCenterX, &m_di1.dCenterY );
		}
	}
	// shape
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_di1.nShape = atoi( szLine );
	m_di2.nShape = atoi( szLine );
	m_di3.nShape = atoi( szLine );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::ReadDataFile()
{
	if( m_szID == _T("") ) return FALSE;

	// we wont do this if it's a pattern/image
	if( m_di1.fIsPattern || m_di1.fIsImage ) return TRUE;

	CStdioFile f;
	CString szFile = m_szPath, szLine;
	int nLineCur = 0;

	// open file for reading
	szFile += m_szID;
	szFile += _T(".STI");
	if( !f.Open( szFile, CFile::modeRead ) ) return FALSE;

	// Read data file until we reach # of nLines (or EOF)
	while( ( nLineCur < m_nLines ) && f.ReadString( szLine ) )
	{
		nLineCur++;

		sscanf( szLine, "%lf %lf %lf",
				&m_i.points[ nLineCur - 1 ].x,
				&m_i.points[ nLineCur - 1 ].y,
				&m_i.points[ nLineCur - 1 ].z );
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::ReadBehaviorFile()
{
	if( m_szID == _T("") ) return FALSE;

	CStdioFile fd;
	CString szFile = m_szPath, szLine;

	// open file for reading
	szFile += m_szID;
	szFile += _T(".BEH");
	if( !fd.Open( szFile, CFile::modeRead ) ) return FALSE;

	// element id
	if( !fd.ReadString( szLine ) ) return FALSE;
	// is target
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_fTarget = atoi( szLine );
	// start
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_bi.dStart = atof( szLine );
	// duration
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_bi.dDuration = atof( szLine );
	// hide right target
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_bi.fHideRightTarget = atoi( szLine );
	// hide wrong target
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_bi.fHideWrongTarget = atoi( szLine );
	// hide if any right target
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_bi.fHideIfAnyRightTarget = atoi( szLine );
	// hide if any wrong target
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_bi.fHideIfAnyWrongTarget = atoi( szLine );
	// right taret
	if( !fd.ReadString( szLine ) ) return FALSE;
	szLine.Trim();
	strcpy_s( m_bi.szRightTarget, 4, szLine );
	// wrong taret
	if( !fd.ReadString( szLine ) ) return FALSE;
	szLine.Trim();
	strcpy_s( m_bi.szWrongTarget, 4, szLine );
	// GMB: 2016-04-11: Added duration pen must stay in element/target: + for at least, - for up to
	if( !fd.ReadString( szLine ) ) return FALSE;
	szLine.Trim();
	m_bi.stayInTime = atof( szLine );

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::ReadAnimationFile()
{
	if( m_szID == _T("") ) return FALSE;

	CStdioFile fd;
	CString szFile = m_szPath, szLine;
	BOOL fRead = FALSE;

	// open file for reading
	szFile += m_szID;
	szFile += _T(".ANI");
	if( !fd.Open( szFile, CFile::modeRead ) ) return FALSE;

	// element id
	if( !fd.ReadString( szLine ) ) return FALSE;
	// is animated
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_ai.fAnimate = atoi( szLine );
	// sampling rate
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_ai.dRate = atof( szLine );
	// animation file
	if( !fd.ReadString( szLine ) ) return FALSE;
	strcpy_s( m_ai.szFile, _MAX_PATH, szLine );
	if( szLine != _T("") )
	{
		CFileFind ff;
		if( ff.FindFile( szLine ) ) fRead = TRUE;
	}
	// random file
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_ai.fRandom = atoi( szLine );
	// generate random file
	if( !fd.ReadString( szLine ) ) return FALSE;
	m_ai.fGen = atoi( szLine );
	// exp/grp/subj
	if( !fd.ReadString( szLine ) ) return FALSE;
	if( ( szLine != _T("") ) && ( szLine.GetLength() != 9 ) ) return FALSE;
	strcpy_s( m_ai.szExp, szLine.Left( 3 ) );
	strcpy_s( m_ai.szGrp, szLine.Mid( 3, 3 ) );
	strcpy_s( m_ai.szSubj, szLine.Right( 3 ) );

	fd.Close();

	// read hwr file
	if( fRead )
	{
		// open file
		fd.Open( m_ai.szFile, CFile::modeRead );

		// how many lines
		int nLineCur = 0;
		while( fd.ReadString( szLine ) ) nLineCur++;
		m_nPoints = nLineCur;

		// create data structures to hold hwr info
		if( m_thex ) delete [] m_thex;
		if( m_they ) delete [] m_they;

		m_thex = new double[ nLineCur ];
		m_they = new double[ nLineCur ];

		// read & store
		fd.SeekToBegin();
		nLineCur = 0;
		double dx, dy, dz;
		while( fd.ReadString( szLine ) )
		{
			sscanf( szLine, "%lf %lf %lf", &dx, &dy, &dz );
			m_thex[ nLineCur ] = dx;
			m_they[ nLineCur ] = dy;

			nLineCur++;
		}
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::ReadTargetFile()
{
	if( m_szID == _T("") ) return FALSE;

	CStdioFile fd;
	CString szFile = m_szPath, szLine;

	// open file for reading
	szFile += m_szID;
	szFile += _T(".TGT");
	if( !fd.Open( szFile, CFile::modeRead ) ) return FALSE;

	// read bounding perimeter (TODO: currently only rectangle)
	// TODO: when we add other than rectangle, we are going to have to know how many lines to read
	// as well as change the TgtInfo structure
	// x,y,z
	double dx, dy;
	for( int i = 0; i < 4; i++ )
	{
		if( !fd.ReadString( szLine ) ) return FALSE;
		sscanf( szLine, "%lf %lf", &dx, &dy );

		POINT *p = NULL;
		switch( i )
		{
			case 0: p = &( m_ti.ptLL ); break;
			case 1: p = &( m_ti.ptUL ); break;
			case 2: p = &( m_ti.ptUR ); break;
			case 3: p = &( m_ti.ptLR ); break;
			default: p = NULL;
		}

		CalculateTargetInfo( p, dx, dy );
	}

	m_fReadTargetInfo = TRUE;
	return TRUE;
}

void ElementManager::CalculateTargetInfo( POINT* p, double dx, double dy )
{
	if( !p ) return;

	// SCALING FACTOR FOR NON-REALSIZED WINDOWS
	double dScaleX = 1., dScaleY = 1.;
	if( !m_fUseMouse && !m_fRealSize )
	{
		RECT window_rect;
		GetWindowRect( m_hWndTablet, &window_rect );
		// if not mapping tablet to recording window
		// simply is ratio of display dimensions to tablet dimensions
		if( !m_fTabletOnly )
		{
			double dtx = 0., dty = 0., ddx = 0., ddy = 0.;

			::GetTabletDimension( dtx, dty );
			::GetDisplayDimension( ddx, ddy );

			dScaleX = ddx / dtx;
			dScaleY = ddy / dty;
		}
		// if mapping tbalet to recording window
		// ratio of actual recording window size to theorectical real-size window
		else
		{
			double dX = 0., dY = 0.;
			long lRealX = 0, lRealY = 0;
			// obtain tablet dimensions (cm)
			::GetTabletDimension( dX, dY );
			// convert tablet dimensions to a mock window to get pixels
			::CMToDisplay( dX, dY, m_hWndTarget, lRealX, lRealY, FALSE, TRUE );
			// we already obtained real window size (pixels) above
			// now we scale based on mock "real-size" window compared to actual size of the window
			if( ( lRealX != 0 ) && ( lRealY != 0 ) )
			{
				dScaleX *= (double)( ( ( 1.0 * window_rect.right ) - ( 1.0 * window_rect.left ) ) / ( 1.0 * lRealX ) );
				dScaleY *= (double)( ( ( 1.0 * window_rect.bottom ) - ( 1.0 * window_rect.top ) ) / ( 1.0 * lRealY ) );
			}
		}
	}

	// convert to display units
	long xPos = 0, yPos = 0;
	::CMToDisplay( dx * dScaleX, dy * dScaleY, m_hWndTablet, xPos, yPos, !m_fTabletOnly );

	p->x = xPos;
	p->y = yPos;

	return;
}

///////////////////////////////////////////////////////////////////////////////

void ElementManager::GetBoundingRectangle( LPRECT pRect )
{
	if( !pRect ) return;
	memcpy( pRect, &m_rect, sizeof( RECT ) );
}

///////////////////////////////////////////////////////////////////////////////

void ElementManager::Display( CDC* pDC, BOOL fEraseLast, BOOL fNoRedraw )
{
	if( !pDC || !m_hWndTarget || !m_hWndTablet ) return;

	// if this is animated and 1st point has not been determined from hwr, dont display
	if( m_ai.fAnimate && !m_f1stPoint ) return;

	// if we are not suppose to display, do nothing (unless erasing)
	if( !m_fDisplay && !fEraseLast ) return;

	// are we already displayed?
	if( m_fDisplayed ) return;

	// if we are hidden, do nothing
	if( m_esState == esHidden ) return;

	// do we need to read from the file? (pattern only)
	BOOL fRead = m_di1.fIsPattern;

	// Open element data file
	CString szFile = m_szPath;
	szFile += m_szID;
	szFile += _T(".STI");
	CStdioFile f;
	if( fRead  && !f.Open( szFile, CFile::modeRead ) ) return;


	// Get client window size
	POINT pt, ptOld = {0,0};
	RECT window_rect;
	GetWindowRect( m_hWndTablet, &window_rect );
	RECT dt_rect;
	GetWindowRect( m_hWndTarget, &dt_rect );
	POINT size;
	size.x = window_rect.right - window_rect.left;
	size.y = window_rect.bottom - window_rect.top;

	// Variables
	CString szLine, szTmp;
	// decor vars
	int	nPos = 0;
	long lX = 0, lY = 0;
	// data file vars (drawing)
	int nLineCur = 0;
	double nx, ny, nz;				// for data file
	LONG xPos = 0L, yPos = 0L;		// for drawing
	LONG x1Pos = 0L, x2Pos = 0L, y1Pos = 0L, y2Pos = 0L;
	int nOldBkMode = pDC->GetBkMode();
	DWORD dwOldTextColor = pDC->GetTextColor(),
		  dwOldBkColor = pDC->GetBkColor();
	UINT nOldAlign = 0;
	TEXTMETRIC tm;
	CSize sz;
	BOOL fFirst = TRUE;
	// Device resolution
	double dDevRes = ::GetDeviceResolution();	// for drawing
	if( dDevRes <= 0 ) dDevRes = 1;
	// Font object
	CFont* pOldFont = NULL;
	LOGFONT lf;
	lf.lfWeight = FW_NORMAL;
	lf.lfWidth = 0;
	lf.lfItalic = FALSE;
	lf.lfOrientation = 0;
	lf.lfEscapement = 0;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS; 
	lf.lfQuality = PROOF_QUALITY;
	lf.lfPitchAndFamily = FF_MODERN | DEFAULT_PITCH;

	// SCALE FOR NON-REALSIZED WINDOWS
	double dx = m_di1.dCenterX, dy = m_di1.dCenterY;
	double dScaleX = 1., dScaleY = 1.;
	if( !m_fUseMouse && !m_fRealSize )
	{
		// if not mapping tablet to recording window
		// simply is ratio of display dimensions to tablet dimensions
		if( !m_fTabletOnly )
		{
			double dtx = 0., dty = 0., ddx = 0., ddy = 0.;

			::GetTabletDimension( dtx, dty );
			::GetDisplayDimension( ddx, ddy );

			dScaleX = ddx / dtx;
			dScaleY = ddy / dty;
		}
		// if mapping tablet to recording window
		// ratio of actual recording window size to theoretical real-size window
		else
		{
			double dX = 0., dY = 0.;
			long lRealX = 0, lRealY = 0;
			// obtain tablet dimensions (cm)
			::GetTabletDimension( dX, dY );
			// convert tablet dimensions to a mock window to get pixels
			::CMToDisplay( dX, dY, m_hWndTarget, lRealX, lRealY, FALSE, TRUE );
			// we already obtained real window size (pixels) above
			// now we scale based on mock "real-size" window compared to actual size of the window
			if( ( lRealX != 0 ) && ( lRealY != 0 ) )
			{
				dScaleX *= (double)( ( ( 1.0 * window_rect.right ) - ( 1.0 * window_rect.left ) ) / ( 1.0 * lRealX ) );
				dScaleY *= (double)( ( ( 1.0 * window_rect.bottom ) - ( 1.0 * window_rect.top ) ) / ( 1.0 * lRealY ) );
			}
		}
	}

	dx *= dScaleX;
	dy *= dScaleY;

	// shift based upon center points
	long xShift = 0, yShift = 0;
	double dXShift = 0., dYShift = 0.;
	::CMToDisplay( dx, dy, m_hWndTablet, xShift, yShift, !m_fTabletOnly, TRUE );

	// TODO: we need to move support for elements away from digitizer so all things can use
	// Read data file until we reach # of nLines (or EOF)
	nLineCur = 0;
	while( ( nLineCur < m_nLines ) && ( fRead ? f.ReadString( szLine ) : TRUE ) )
	{
		nLineCur++;

		// x,y,z
		if( !fRead )
		{
			nx = m_i.points[ nLineCur - 1 ].x * dScaleX;
			ny = m_i.points[ nLineCur - 1 ].y * dScaleY;
			nz = m_i.points[ nLineCur - 1 ].z;
		}
		else
		{
			sscanf( szLine, "%lf %lf %lf", &nx, &ny, &nz );
			// convert to cm
			nx *= dDevRes * dScaleX;
			ny *= dDevRes * dScaleY;
		}

		// absolute them all
		if( nx < 0 ) nx *= -1.0;
		if( ny < 0 ) ny *= -1.0;
		if( nz < 0 ) nz *= -1.0;

		// convert to display units
		// .. if pattern & first point, calculate shift..otherwise, shift rest of points
		if( m_di1.fIsPattern )
		{
			// shift based upon center points. Shift is applied after comparing to first point of pattern.
			// All points are shifted in the same direction where shift = center - 1st point.
			if( fFirst )
			{
				dXShift = dx - nx;
				dYShift = dy - ny;
			}
			nx += dXShift;
			ny += dYShift;
		}
		::CMToDisplay( nx, ny, m_hWndTablet, xPos, yPos, !m_fTabletOnly );

		// assign to point object
		pt.x = xPos;
		pt.y = yPos;

		// if pattern
		if( m_di1.fIsPattern )
		{
			if( pt.x < 0 ) pt.x = 0;
			if( pt.y < 0 ) pt.y = 0;

			::DrawPoint( pDC, pt, ptOld, ( nz > ::GetMinPenPressure() ), TRUE, fFirst, FALSE, (int)nz );
			if( fFirst ) fFirst = FALSE;
			memcpy( &ptOld, &pt, sizeof( POINT ) );
		}
		else
		{
			// To display
			if( nz > ::GetMinPenPressure() )
			{
				// TODO: Hack to test fill (since we only use rectangles now)
				if( nLineCur == 2 )
				{
					x1Pos = x2Pos = xPos;
					y1Pos = y2Pos = yPos;
				}
				else
				{
					x1Pos = MIN( x1Pos, xPos );
					y1Pos = MIN( y1Pos, yPos );
					x2Pos = MAX( x2Pos, xPos );
					y2Pos = MAX( y2Pos, yPos );
				}
			}
		}
	}


	// if it was a pattern we're done
	if( m_di1.fIsPattern )
	{
		m_fDisplayed = TRUE;
		return;
	}


	// Fill
	if( !m_di1.fIsImage && !m_di1.fIsPattern )
	{
		DecInfo* pDI = &m_di1;
		if( m_fInit && ( m_esState == esSuccess ) ) pDI = &m_di2;
		else if( m_fInit && ( m_esState == esFailure ) ) pDI = &m_di3;

		// ELEMENT
		// erase previous if specified
		if( fEraseLast && ( m_rectLast.left >= 0 ) )
		{
			DrawShape( pDC, pDI->nLineSize, ::GetBkColor( pDC->GetSafeHdc() ),
					   ::GetBkColor( pDC->GetSafeHdc() ), m_rectLast, pDI->nShape );
		}
		// If we are not supposed to redraw (i.e., just erase), then we're done
		if( fNoRedraw ) return;
		// Draw element in element DC
		DrawShape( pDC, pDI->nLineSize, pDI->dwLineColor, pDI->dwBkColor,
				   x1Pos, y1Pos, x2Pos, y2Pos, pDI->nShape );
		// Set old rect var
		m_rectLast.left = x1Pos;
		m_rectLast.top = y1Pos;
		m_rectLast.right = x2Pos;
		m_rectLast.bottom = y2Pos;

		// TEXT
		pDC->SetTextColor( pDI->dwColor );
		pDC->SetBkColor( pDI->dwBkColor );
		pDC->SetBkMode( TRANSPARENT );
		// set font info
		strcpy_s( lf.lfFaceName, LF_FACESIZE, pDI->szFont );
		lf.lfHeight= -MulDiv( pDI->nSize, pDC->GetDeviceCaps( LOGPIXELSY ), 72 );
		lf.lfWeight = pDI->lWeight;
		lf.lfItalic = pDI->bI;
		lf.lfUnderline = pDI->bU;
		lf.lfStrikeOut = pDI->bSO;
		lf.lfPitchAndFamily = pDI->bPitch;
		CFont font;
		if( font.CreateFontIndirect( &lf ) )
		{
			pOldFont = pDC->SelectObject( &font );
			nOldAlign = pDC->SetTextAlign( TA_BASELINE );

			// Width/Height of shape (rectangle only thus far)
			int dx = abs( x1Pos - x2Pos );
			int dy = abs( y1Pos - y2Pos );

			// text width based on font
			sz = pDC->GetTextExtent( pDI->szText );
			int lx = sz.cx;
			// TODO: The logical approach is not working for centering
			// ....most fonts seem to work with 0.66 * the height of the text
			// ....Figure out later what is correct approach
			// text height based on font
			pDC->GetTextMetrics( &tm );
//			int ly = tm.tmHeight + tm.tmExternalLeading + tm.tmInternalLeading;
			int ly = (int)( ( 2.0 / 3.0 ) * ( -lf.lfHeight - tm.tmInternalLeading ) );

			// new x,y positions for text
			x1Pos += ( dx - lx ) / 2;
			y1Pos += ( ( dy + ly ) / 2 );

			// draw text
			pDC->ExtTextOut( x1Pos, y1Pos, ETO_OPAQUE, NULL, pDI->szText, NULL );

			// cleanup
			pDC->SetTextAlign( nOldAlign );
			pDC->SelectObject( pOldFont );
		}
	}
	else if( m_di1.fIsImage )
	{
		// convert centers to display
// 23Feb10: GMB: Coordinates for images were based on display. Changing to reflect tablet. No scaling, for now.
//		::CMToDisplay( m_di1.dCenterX, m_di1.dCenterY, m_hWndTablet, lX, lY, !m_fTabletOnly );
		::CMToDisplay( dx, dy, m_hWndTablet, lX, lY, !m_fTabletOnly );

		HGLOBAL hdib;
		CPalette pal;
		if( ::LoadBMP( m_di1.szBMP, &hdib, &pal ) ) ::DrawDIB( pDC, hdib, &pal, lX, lY );
		GlobalFree( hdib );
	}


	// Cleanup
	pDC->SetTextColor( dwOldTextColor );
	pDC->SetBkColor( dwOldBkColor );
	pDC->SetBkMode( nOldBkMode );

	m_fDisplayed = TRUE;
}

///////////////////////////////////////////////////////////////////////////////

void ElementManager::DrawShape( CDC* pDC, int nLineSize, DWORD dwLineColor, DWORD dwBkColor,
							    long x1, long y1, long x2, long y2, UINT nShape, BOOL fErase )
{
	// Produce a shape with the pen width strictly inside the box, 
	// unless one dimension is 0, in which case we use penwidth.
	// When one dimension fills up the box, produce one line of 
	// thickness of the smallest dimension.
	// This method keeps the box in place even if the pen width is enormous

	HPEN hPen = NULL, hPenOld = NULL;
	HBRUSH hbrFill = NULL, hbrOld = NULL;

	// if width and height are 0, we do nothing
	if( ( abs( x2 - x1 ) == 0 ) && ( abs( y2 - y1 ) == 0 ) )
	{
		// do nothing
	}
	// if the width or height = 0 or 1, we just draw a line of pen width
	else if( ( abs( x2 - x1 ) == 0 ) || ( abs( y2 - y1 ) == 0 ) )
	{
		// If pen width = 1, we can just draw a line
		if( nLineSize == 1 )
		{
			hPen = CreatePen( PS_SOLID, nLineSize, dwLineColor );
			hPenOld = (HPEN)pDC->SelectObject( hPen );
			pDC->MoveTo( x1, y1 );
			pDC->LineTo( x2, y2 );
		}
		// otherwise we draw a rectangle with fill color same as pen color
		// and set pen width to 1
		else
		{
			hPen = CreatePen( PS_INSIDEFRAME, 1, dwLineColor );
			hPenOld = (HPEN)pDC->SelectObject( hPen );
			hbrFill = CreateSolidBrush( dwLineColor );
			hbrOld = (HBRUSH)pDC->SelectObject( hbrFill );
			RECT rect;
			rect.left = x1;
			rect.top = y1;
			rect.right = ( x2 == x1 ) ? x1 + nLineSize : x2;
			rect.bottom = ( y2 == y1 ) ? y1 + nLineSize : y2;
			pDC->Rectangle( &rect );
		}
	}
	// else we have a shape where the borders and internals fit within the bounding rectangle
	// if line size = 0, we do not display
	else if( nLineSize > 0 )
	{
		hPen = CreatePen( PS_INSIDEFRAME, nLineSize, dwLineColor );
		hPenOld = (HPEN)pDC->SelectObject( hPen );
		hbrFill = CreateSolidBrush( dwBkColor );
		hbrOld = (HBRUSH)pDC->SelectObject( hbrFill );
		// ...draw rectangle
		if( nShape == SHAPE_RECTANGLE ) pDC->Rectangle( x1, y1, x2, y2 );
		// ...draw circle
		else if( nShape == SHAPE_ELLIPSE ) pDC->Ellipse( x1, y1, x2, y2 );
	}

	// cleanup
	if( hPen && hPenOld )
	{
		pDC->SelectObject( hPenOld );
		DeleteObject( hPen );
	}
	if( hbrFill && hbrOld )
	{
		pDC->SelectObject( hbrOld );
		DeleteObject( hbrFill );
	}
}

void ElementManager::DrawShape( CDC* pDC, int nLineSize, DWORD dwLineColor, DWORD dwBkColor,
							    CRect rect, UINT nShape )
{
	long x1 = rect.left, x2 = rect.right;
	long y1 = rect.top, y2 = rect.bottom;
	DrawShape( pDC, nLineSize, dwLineColor, dwBkColor, x1, y1, x2, y2, nShape, TRUE );
}

///////////////////////////////////////////////////////////////////////////////

int ElementManager::PostMessage( LPARAM msg, WPARAM val, ULONG clock,
								 BOOL& invalidate, BOOL& terminate, WORD& returnmsg )
{
	WORD wMsg = LOWORD( msg );
	WORD wSubMsg = HIWORD( msg );
	WORD wVal1 = LOWORD( val );
	WORD wVal2 = HIWORD( val );
	invalidate = FALSE;
	terminate = FALSE;
	returnmsg = MSG_STATUS_NONE;

	switch( wMsg )
	{
		// reset
		case MSG_RESET:
		{
			m_fDisplayed = FALSE;
			if( clock == 0 )
				PostMessage( MAKELPARAM( MSG_TIMER, 0 ), val, clock, invalidate, terminate, returnmsg );
			return 1;
		}
		case MSG_TIMER:
		{
			// TODO: do we really need any sub-cats for this one?
			ULONG lStart = (ULONG)( m_bi.dStart * 1000 );
			ULONG lDuration = (ULONG)( m_bi.dDuration * 1000 );
			BOOL fPast = ( lDuration == 0 ) ? FALSE : clock > ( lStart + lDuration );
			// TODO: If time runs out, can we undisplay this item?
			// if it's atop another, then we'd have to redraw
			// so, it seems, that bottom line, redrawing is going to have to occur
			// I suppose a invalidate + clear + post message with coordinates
			// and everyone check to see if they are there then redraw if necessary :o
			// eek
			if( ( clock >= lStart ) && !fPast )
			{
				if( m_ai.fAnimate )
				{
					AnimationUpdate( clock, invalidate );
					Display( (CDC*) val, invalidate );
				}
				else Display( (CDC*) val );
			}
			else if( m_fDisplayed )
			{
				// TODO: do we need a reset ?
				m_fDisplay = FALSE;
				m_fDisplayed = FALSE;
				Display( (CDC*)val, TRUE, TRUE );
			}
			break;
		}
		// display
		case MSG_ELMT_DISPLAY:
		{
			switch( wSubMsg )
			{
				// digitizer extent x
				case MSG_SUB_ELMT_EXTENTX:
					m_lExtX = (long) val;
					return 1;
				// digitizer extent x
				case MSG_SUB_ELMT_EXTENTY:
					m_lExtY = (long) val;
					return 1;
				// display normally (???)
				case MSG_SUB_ELMT_NORMAL:
				{
					if( !m_fDisplayed )
					{
						Display( (CDC*) val );
						return 1;
					}
					else return 0;
				}
				// bad sub-message
				default: return 0;
			}
		}
		// windows pointers
		case MSG_HANDLES:
		{
			switch( wSubMsg )
			{
				// tablet window
				case MSG_SUB_ELMT_TABLET:
					m_hWndTablet = (HWND) val;
					return 1;
				// target window
				case MSG_SUB_ELMT_TARGET:
					m_hWndTarget = (HWND) val;
					return 1;
				// bad sub-message
				default: return 0;
			}
		}
		// flags
		case MSG_FLAGS:
		{
			switch( wSubMsg )
			{
				// are we using mouse?
				case MSG_SUB_ELMT_MOUSE:
					m_fUseMouse = (BOOL) wVal1;
					return 1;
				// incorrect selection (of target) mode
				case MSG_SUB_TARG_MODE:
				{
					m_fStop = (BOOL)wVal1;
					return 1;
				}
				// tablet only recording
				case MSG_SUB_ELMT_ONLY:
				{
					m_fTabletOnly = (BOOL)wVal1;
					return 1;
				}
				// real size display
				case MSG_SUB_ELMT_REAL:
				{
					m_fRealSize = (BOOL)wVal1;
					return 1;
				}
				// recording has begun
				case MSG_SUB_BEGAN_RECORDING:
				{
					m_fInit = TRUE;
					return 1;
				}
				// bad sub-msg
				default: return 0;
			}
		}
		// pen/mouse location
		case MSG_TARG_LOCATION:
		{
			// are we even a target?
			if( !m_fTarget ) return 0;

			// are we on target list?
			if( !((ElementMessageManager*)m_pDaddy)->AmIOnTargetList( this ) ) return 0;

			// have i been hit?
			BOOL fIn = InTarget( wVal1, wVal2 );

			// we become hidden if we've been reached & in-and-out
			// and the next target has as well
			// if we have siblings (same element)
			// and we tell (if any) next sibling to display
			if( m_pDaddy &&
				( m_esState != esHidden ) &&
				m_fReached &&
				m_fInAndOut &&
				((ElementMessageManager*)m_pDaddy)->HasTheNextTargetBeenReached( this ) &&
				((ElementMessageManager*)m_pDaddy)->AnySiblingsNotReached( this ) )
			{
				m_esState = esHidden;
				invalidate = TRUE;
				((ElementMessageManager*)m_pDaddy)->DisplayNextSibling( this );
			}

			// if we've been reached, we need to know whether we've left area yet
			// unless we were a failure before
			if( ( m_esState == esFailure ) && m_fReached && m_fInAndOut )
			{
				m_fInAndOut = FALSE;
				m_fReached = FALSE;
			}
			else if( m_fReached && m_fInAndOut )
			{
				return 0;
			}
			else if( m_fReached && !fIn )
			{
				m_fInAndOut = TRUE;

				// GMB: 2016-04-14: Element was just exited so get timestamp
				_ftime64_s( &m_timebOut );

				// GMB: 2016-04-14: If criterion was set for duration within target...
				if( this->m_bi.stayInTime != 0. ) {

					// can be: (1) stay in for at least n seconds (+ values)
					//		   (2) stay in no longer than n seconds (- values)
					BOOL atLeast = ( m_bi.stayInTime > 0. );

					// how long in?
					double secs = ( m_timebOut.time + m_timebOut.millitm / 1000. ) - ( m_timebIn.time + m_timebIn.millitm / 1000. );

					// check
					BOOL failedCheck = FALSE;
					if( atLeast ) {

						if( secs < this->m_bi.stayInTime ) failedCheck = TRUE;
					}
					else {

						if( secs > this->m_bi.stayInTime ) failedCheck = TRUE;
					}

					// if failed, notify
					if( failedCheck ) {

						terminate = TRUE;
						returnmsg = MSG_STATUS_TARG_FAILURE | MSG_STATUS_TARG_LEFT | MSG_STATUS_TARG_BADTIME;
						return 1;
					}
				}

				// GMB: 2016-04-13: Added notification for exiting target
#ifdef	_LOGPOINTS_
				CString szMsg;
				szMsg.Format( _T("TARGET: %s"), m_szID );
				OutputMessage( szMsg, LOG_TABLET );
#endif
				terminate = TRUE;
				returnmsg = MSG_STATUS_TARG_LEFT | ( m_fCorrect ? MSG_STATUS_TARG_SUCCESS : MSG_STATUS_TARG_FAILURE );
				return 1;
			}

			// am I not to be displayed because i have siblings still being displayed?
			if( m_pDaddy &&
				((ElementMessageManager*)m_pDaddy)->AnyNonReachedSiblingsBeforeMe( this ) )
			{
				m_fDisplay = FALSE;
				return 0;
			}

			// hide upon success or failure?
			if( m_bi.fHideRightTarget && ( m_esState == esSuccess ) /*&& m_fInAndOut*/ )
			{
				m_fInAndOut = TRUE;
				m_esState = esHidden;
				((ElementMessageManager*)m_pDaddy)->DisplayNextSibling( this );
				m_fDisplay = FALSE;
				invalidate = TRUE;
			}
			else if( m_bi.fHideWrongTarget && ( m_esState == esFailure ) /*&& m_fInAndOut*/ )
			{
				m_fInAndOut = TRUE;
				m_esState = esHidden;
				((ElementMessageManager*)m_pDaddy)->DisplayNextSibling( this );
				m_fDisplay = FALSE;
				invalidate = TRUE;
			}

			// TODO: Hide upon success/failure of another target

			// are we hidden?
			if( m_esState == esHidden ) return 0;

			// if we've been reached correctly
			if( m_fCorrect ) return 0;

			// if not in
			if( !fIn ) return 0;

			// if we've already been reached and flag set to deactivate
			if( m_fReached && m_fStop ) return 0;

			// if we've already been reached and we're still in the target
			if( m_fReached && fIn )
			{
				terminate = TRUE;
				return 0;
			}

			// GMB: 2016-04-14: If element reached for first time, we need to set time
			if( fIn && !m_fReached ) {

				_ftime64_s( &m_timebIn );
			}

			// we've been hit
			m_fReached = TRUE;
			terminate = TRUE;
			m_fDisplay = TRUE;
			m_fDisplayed = FALSE;

			// add myself to the target results file
			if( m_szDataPath != _T("") )
			{
				CStdioFile f;
				if( f.Open( m_szDataPath, CFile::modeWrite ) )
				{
					f.SeekToEnd();
					f.WriteString( m_szID );
					f.WriteString( _T("\n") );
				}
			}

			// are we the correct target?
			// update element status as necessary
			ElementManager* pEM = m_pPrevious;
			if( pEM ) 
			{
				while( pEM && !pEM->m_fTarget ) pEM = pEM->m_pPrevious;
				if( pEM )
				{
					if( pEM->m_fCorrect ) m_esState = esSuccess;
					else m_esState = esFailure;
				}
				else m_esState = esFailure;
			}
			else m_esState = esSuccess;
			m_fCorrect = ( m_esState == esSuccess );

			// message others that i've been reached
			if( m_fCorrect )
			{
				m_pDaddy->PostMessage( MAKELPARAM( MSG_TARG_REACHED, MSG_SUB_TARG_SUCCESS ),
									   (WPARAM)0, clock, invalidate, returnmsg );
				CString szTarg = m_bi.szRightTarget;
				if( szTarg != _T("") ) ReachedShowElement( m_bi.szRightTarget );
			}
			else
			{
				m_pDaddy->PostMessage( MAKELPARAM( MSG_TARG_REACHED, MSG_SUB_TARG_FAILURE ),
					(WPARAM)0, clock, invalidate, returnmsg );
				CString szTarg = m_bi.szWrongTarget;
				if( szTarg != _T("") ) ReachedShowElement( m_bi.szWrongTarget );
			}

			// first or last target?
			BOOL fFirst = ((ElementMessageManager*)m_pDaddy)->AmIFirstTarget( this );
			BOOL fLast = ((ElementMessageManager*)m_pDaddy)->AmILastTarget( this );

			invalidate = TRUE;
			if( m_esState == esFailure )
			{
				returnmsg = MSG_STATUS_TARG_FAILURE;
#ifdef	_LOGPOINTS_
				CString szMsg;
				szMsg.Format( _T("TARGET: %s"), m_szID );
				OutputMessage( szMsg, LOG_TABLET );
#endif
			}
			else if( m_esState == esSuccess )
			{
				returnmsg = MSG_STATUS_TARG_SUCCESS;
#ifdef	_LOGPOINTS_
				CString szMsg;
				szMsg.Format( _T("TARGET: %s"), m_szID );
				OutputMessage( szMsg, LOG_TABLET );
#endif
			}
			if( fFirst )
			{
				returnmsg |= MSG_STATUS_TARG_FIRST;
#ifdef	_LOGPOINTS_
				CString szMsg;
				szMsg.Format( _T("TARGET: %s"), m_szID );
				OutputMessage( szMsg, LOG_TABLET );
#endif
			}
			// GMB: 2016-04-02: If there is only 1 element, both 'first' and 'last' conditions can be met
			//					So, the 'else' (below) prevents that. And, 'returnmsg' can be OR'd.
//			else if( fLast )
			if( fLast )
			{
				returnmsg |= MSG_STATUS_TARG_LAST;
#ifdef	_LOGPOINTS_
				CString szMsg;
				szMsg.Format( _T("TARGET: %s"), m_szID );
				OutputMessage( szMsg, LOG_TABLET );
#endif
			}

			return 1;
		}
		// target reached
		case MSG_TARG_REACHED:
		{
			switch( wSubMsg )
			{
				// a target was successfully reached
				case MSG_SUB_TARG_SUCCESS:
				{
					if( m_bi.fHideIfAnyRightTarget )
					{
						m_esState = esHidden;
						((ElementMessageManager*)m_pDaddy)->DisplayNextSibling( this );
						invalidate = TRUE;
					}
					return 1;
				}
				// a target was unsuccessfully reached
				case MSG_SUB_TARG_FAILURE:
				{
					if( m_bi.fHideIfAnyRightTarget )
					{
						m_esState = esHidden;
						((ElementMessageManager*)m_pDaddy)->DisplayNextSibling( this );
						invalidate = TRUE;
					}
					return 1;
				}
				// bad sub-msg
				default: return 0;
			}
		}
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

int ElementManager::PostMessage( LPARAM msg, double val, ULONG clock,
								 BOOL& invalidate, BOOL& terminate, WORD& returnmsg )
{
	WORD wMsg = LOWORD( msg );
	WORD wSubMsg = HIWORD( msg );
	invalidate = FALSE;
	terminate = FALSE;
	returnmsg = MSG_STATUS_NONE;

	switch( wMsg )
	{
		// display
		case MSG_ELMT_DISPLAY:
		{
			switch( wSubMsg )
			{
				case MSG_SUB_ELMT_DISPLAY_X:
					m_dDisplayX = val;
					return 1;
				case MSG_SUB_ELMT_DISPLAY_Y:
					m_dDisplayY = val;
					return 1;
				// bad sub-message
				default: return 0;
			}
		}
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////

BOOL ElementManager::InTarget( WORD x, WORD y )
{
	BOOL fIn = FALSE;

	if( !m_fReadTargetInfo && !ReadTargetFile() ) 
		return FALSE;

	// For rectangle
	if( m_di1.nShape == SHAPE_RECTANGLE )
	{
		if( ( x >= m_ti.ptLL.x ) &&
			( x <= m_ti.ptLR.x ) )
			fIn = TRUE;
		else fIn = FALSE;

		if( fIn &&
			( y >= m_ti.ptUL.y ) &&
			( y <= m_ti.ptLL.y ) )
			fIn = TRUE;
		else fIn = FALSE;
	}
	else if( m_di1.nShape == SHAPE_ELLIPSE )
	{
		// radius of x & y
		double a = ( m_ti.ptLR.x - m_ti.ptLL.x ) / 2.0;
		double b = ( m_ti.ptLL.y - m_ti.ptUL.y ) / 2.0;
		// center points of ellipse
		double x0 = m_ti.ptLL.x + a;
		double y0 = m_ti.ptLL.y - b;

		// calculate
		double D = ( ( ( x - x0 ) * b ) * ( ( x - x0 ) * b ) )+
				   ( ( ( y - y0 ) *a ) * ( ( y - y0 ) * a ) )-
				   ( ( a * b ) * ( a * b ) );
		if( D <= 0. ) fIn = TRUE;
	}

	return fIn;
}

///////////////////////////////////////////////////////////////////////////////

void ElementManager::ReachedShowElement( CString szID )
{
	ElementManager* pEM = ((ElementMessageManager*)m_pDaddy)->InsertNewItem( this, szID );
	if( pEM )
	{
		BOOL inv, term;
		WORD retmsg;
		pEM->PostMessage( MAKELPARAM( MSG_HANDLES, MSG_SUB_ELMT_TABLET ),
						  (WPARAM) m_hWndTablet, 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_HANDLES, MSG_SUB_ELMT_TARGET ),
						  (WPARAM) m_hWndTarget, 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_MOUSE ),
						  MAKEWPARAM( m_fUseMouse, 0 ), 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_ONLY ),
						  MAKEWPARAM( m_fTabletOnly, 0 ), 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_ELMT_REAL ),
						  MAKEWPARAM( m_fRealSize, 0 ), 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTX ),
						  (WPARAM)m_lExtX, 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_EXTENTY ),
						  (WPARAM)m_lExtX, 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_X ),
						  m_dDisplayX, 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_ELMT_DISPLAY, MSG_SUB_ELMT_DISPLAY_Y ),
						  m_dDisplayY, 0, inv, term, retmsg );
		pEM->PostMessage( MAKELPARAM( MSG_FLAGS, MSG_SUB_TARG_MODE ),
						  MAKEWPARAM( m_fStop, 0 ), 0, inv, term, retmsg );
	}
}
///////////////////////////////////////////////////////////////////////////////

void ElementManager::AnimationUpdate( ULONG clock, BOOL& invalidate )
{
	static long lastspot = -1;

	// valid array?
	if( !m_thex || !m_they ) return;

	// have we started yet?
	BOOL fCalcShift = FALSE;
	if( !m_f1stPoint )
	{
		fCalcShift = TRUE;
		m_f1stPoint = TRUE;
		m_ulClockAnim = clock;
	}

	// is it time to display next based upon sampling rate
	// determine which item within the data array (we skip those we've missed)
	double dPeriod = 1. / m_ai.dRate * 1000;
	long nspot = (long)( ( clock - m_ulClockAnim ) / dPeriod );
	if( lastspot == nspot )
	{
		invalidate = FALSE;
		return;
	}
	if( nspot > m_nPoints )
	{
		invalidate = FALSE;
		return;
	}
	lastspot = nspot;

	// get x/y position (absolute)
	double nx = fabs( m_thex[ nspot ] );
	double ny = fabs( m_they[ nspot ] );

	// convert units to centimenters
	// TODO: what do we do if it's recorded from a mouse?
	nx *= ::GetDeviceResolution();
	ny *= ::GetDeviceResolution();

	// calculate shift if first point
	if( fCalcShift )
	{
		m_ai.dShiftX = m_di1.dCenterX - nx;
		m_ai.dShiftY = m_di1.dCenterY - ny;
	}

	// apply shift based upon center point and 1st point of pattern
	nx += m_ai.dShiftX;
	ny += m_ai.dShiftY;

	// widths
	double dx = ( m_i.points[ 4 ].x - m_i.points[ 5 ].x ) / 2.;
	double dy = ( m_i.points[ 2 ].y - m_i.points[ 1 ].y ) / 2.;

	// now convert position of element
	int nPoints = 5;	// TODO: 5 points for a rectangle - other shapes come later
	int nLines = nPoints + 1;

	double* px = new double[ nPoints ];
	double* py = new double[ nPoints ];
	int xmod, ymod;
	for( int i = 0; i < nPoints; i++ )
	{
		switch( i )
		{
			case 0: xmod = -1; ymod = -1; break;
			case 1: xmod = -1; ymod = 1; break;
			case 2: xmod = 1; ymod = 1; break;
			case 3: xmod = 1; ymod = -1; break;
			case 4: xmod = -1; ymod = -1; break;
		}

		px[ i ] = nx + ( xmod * dx );
		py[ i ] = ny + ( ymod * dy );
	}
	// For each point
	for( int p = 0; p < nLines; p++ )
	{
		if( p == 0 )
		{
			m_i.points[ p ].x = px[ 0 ];
			m_i.points[ p ].y = py[ 0 ];
		}
		else
		{
			m_i.points[ p ].x = px[ p - 1 ];
			m_i.points[ p ].y = py[ p - 1 ];
		}

		// update target info
		// TODO: add errors
		POINT* pt = NULL;
		switch( p )
		{
			case 1: pt = &(m_ti.ptLL); break;
			case 2: pt = &(m_ti.ptUL); break;
			case 3: pt= &(m_ti.ptUR); break;
			case 4: pt = &(m_ti.ptLR); break;
		}

		CalculateTargetInfo( pt, px[ p - 1 ], py[ p - 1 ] );
	}
	m_ti.fReached = FALSE;

	delete [] px;
	delete [] py;

	// now update screen
	invalidate = TRUE;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#pragma warning( default: 4996 )	// disable deprecated warning

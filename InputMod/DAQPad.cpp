// DAQPad.cpp : Implementation of DAQPad
#include "stdafx.h"
#include "InputMod.h"
#include "DAQPad.h"
#include "..\Common\DefFun.h"
#include "..\Common\Shared.h"
#include "NSAnalogIn.h"

#define	MVV				1000
#define	LBSN			4.44822


/////////////////////////////////////////////////////////////////////////////
// DAQPad

DAQPad::DAQPad() : m_pAI( NULL )
{
	m_hwnd = 0;
	m_nDevice = 1;
	m_nChannels = 3;
	m_lSamples = 600L;
	m_dSampleRate = 100000.;
	m_dScanRate = 200.;
	m_dMagnetForce = 8.;
	m_pGain = new short[ MAX_CHANNELS ];
	m_pFullScaleLoad = new double[ MAX_CHANNELS ];
	m_pCalibrationConstant = new double[ MAX_CHANNELS ];
	m_pExcitationVoltage = new double[ MAX_CHANNELS ];
	m_pVtoN = new double[ MAX_CHANNELS ];
	for( int i = 0; i < MAX_CHANNELS; i++ )
	{
		m_pGain[ i ] = 1;
		m_pFullScaleLoad[ i ] = 25;			// Lower Grip (B04)
		m_pCalibrationConstant[ i ] = 1260;
		m_pExcitationVoltage[ i ] = 5;
		m_pVtoN[ i ] = 1.;
	}
	CalcGains();

	m_fLocked = FALSE;
	m_pData = NULL;
}

DAQPad::~DAQPad()
{
	if( m_pAI ) delete m_pAI;
	if( m_pGain ) delete [] m_pGain;
	if( m_pFullScaleLoad ) delete [] m_pFullScaleLoad;
	if( m_pCalibrationConstant ) delete [] m_pCalibrationConstant;
	if( m_pExcitationVoltage ) delete [] m_pExcitationVoltage;
	if( m_pVtoN ) delete [] m_pVtoN;
	if( m_pData ) delete [] m_pData;
}

STDMETHODIMP DAQPad::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IDAQPad,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP DAQPad::SetOutputWindow(VARIANT *MsgWindow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CListCtrl* pWnd = (CListCtrl*)MsgWindow;
	::SetOutputWindow( pWnd );

	return S_OK;
}

STDMETHODIMP DAQPad::SetLoggingOn( BOOL fOn, BSTR AppPath )
{
	::SetLoggingOn( fOn, AppPath );
	::SetTabletLogOn( fOn );

	return S_OK;
}

STDMETHODIMP DAQPad::put_Device(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nDevice = ( newVal > 0 ) ? newVal : 1;

	return S_OK;
}

STDMETHODIMP DAQPad::put_Channels(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_fLocked )
		m_nChannels = ( ( newVal >= 1 ) && ( newVal <= MAX_CHANNELS ) ) ?
			newVal : MAX_CHANNELS;
	else
		::OutputMessage( _T("Value is locked. Reset to change."), LOG_TABLET );

	return S_OK;
}

STDMETHODIMP DAQPad::put_Samples(long newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_fLocked )
		m_lSamples = ( newVal >= 0 ) ? newVal : 0;
	else
		::OutputMessage( _T("Value is locked. Reset to change."), LOG_TABLET );

	return S_OK;
}

STDMETHODIMP DAQPad::put_SampleRate(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dSampleRate = ( newVal > 0. ) ? newVal : 100000;

	return S_OK;
}

STDMETHODIMP DAQPad::put_ScanRate(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dScanRate = ( newVal > 0. ) ? newVal : 200;

	return S_OK;
}

STDMETHODIMP DAQPad::put_MagnetForce(double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_dMagnetForce = ( newVal > 0. ) ? newVal : 8.;

	return S_OK;
}

STDMETHODIMP DAQPad::put_Gain(short Index, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pGain && ( ( Index >= 0 ) && ( Index <= MAX_CHANNELS ) ) )
		m_pGain[ Index ] = newVal;

	return S_OK;
}

void DAQPad::CalcGains()
{
	for( int i = 0; i < MAX_CHANNELS; i++ )
	{
		if( ( m_pFullScaleLoad[ i ] != 0. ) && ( m_pExcitationVoltage[ i ] != 0. ) )
		{
			m_pVtoN[ i ] = 1 / ( m_pCalibrationConstant[ i ] / MVV *
								 m_pExcitationVoltage[ i ] / LBSN /
								 m_pFullScaleLoad[ i ] );
		}
	}
}

STDMETHODIMP DAQPad::put_FullScaleLoad(short Channel, double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pFullScaleLoad && ( ( Channel >= 0 ) && ( Channel <= MAX_CHANNELS ) ) )
		m_pFullScaleLoad[ Channel ] = newVal;

	return S_OK;
}

STDMETHODIMP DAQPad::put_CalibrationConstant(short Channel, double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pCalibrationConstant && ( ( Channel >= 0 ) && ( Channel <= MAX_CHANNELS ) ) )
		m_pCalibrationConstant[ Channel ] = newVal;

	return S_OK;
}

STDMETHODIMP DAQPad::put_ExcitationVoltage(short Channel, double newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pExcitationVoltage && ( ( Channel >= 0 ) && ( Channel <= MAX_CHANNELS ) ) )
		m_pExcitationVoltage[ Channel ] = newVal;

	return S_OK;
}

STDMETHODIMP DAQPad::Reset()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// clear/delete data storage structure
	if( m_pData )
	{
		delete [] m_pData;
		m_pData = NULL;
	}
	m_fLocked = FALSE;

	// check to be sure device is OK
	if( m_pAI ) delete m_pAI;
	m_pAI = new CNSAnalogIn;

	// we are skipping the verification if in debug mode (since I do not have the hardware)
#if defined _DEBUG
	return S_OK;
#endif

	if( !m_pAI->m_DeviceOK )
	{
		::OutputMessage( _T("There is an error with the DAQPad device upon initialization."), LOG_TABLET );
		delete m_pAI;
		m_pAI = NULL;
		return E_FAIL;
	}

	return S_OK;
}

STDMETHODIMP DAQPad::Record(BOOL DoubleBufferMode, long DBSamples)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// Do not record unless reset & initialized
	if( m_fLocked )
	{
		::OutputMessage( _T("Recording is locked. Reset to record again."), LOG_TABLET );
		return E_FAIL;
	}

	// Do not record if channels not set
	if( m_nChannels <= 0 )
	{
		::OutputMessage( _T("There are no channels to record. Channel count = 0."), LOG_TABLET );
		return E_FAIL;
	}

	// Do not record if we will get 0 results
	if( m_lSamples > 0 )
	{
		if( m_pData ) delete [] m_pData;
		m_pData = new double[ m_lSamples * m_nChannels ];
		memset( m_pData, 0, ( m_lSamples * m_nChannels ) * sizeof( double ) );
	}
	else
	{
		::OutputMessage( _T("There are no samples to obtain. Sample count = 0."), LOG_TABLET );
		return E_FAIL;
	}

	// # Channels & Samples is not settable now (data count cannot be changed)
	m_fLocked = TRUE;

	// *** RECORDING HERE *** //
	// Initialize
	if( !m_pAI )
	{
		m_pAI = new CNSAnalogIn;
		if( !m_pAI->m_DeviceOK )
		{
			::OutputMessage( _T("There is an error with the DAQPad device upon initialization."), LOG_TABLET );
			delete m_pAI;
			m_pAI = NULL;
			return E_FAIL;
		}
	}
	m_pAI->m_Device = m_nDevice;
	m_pAI->m_Channels = m_nChannels;
	m_pAI->m_SampleRate = m_dSampleRate;
	m_pAI->m_Samples = m_lSamples;
	m_pAI->m_szFile = m_szFile;
	m_pAI->m_dMagnetForce = m_dMagnetForce;
	m_pAI->m_nBaselineCount = m_nBaselineCount;
	for( int i = 0; i < MAX_CHANNELS; i++ )
	{
		m_pAI->m_Gains[ i ] = m_pGain[ i ];
		m_pAI->m_pFullScaleLoad[ i ] = m_pFullScaleLoad[ i ];
		m_pAI->m_pCalibrationConstant[ i ] = m_pCalibrationConstant[ i ];
		m_pAI->m_pExcitationVoltage[ i ] = m_pExcitationVoltage[ i ];
	}
	if( m_hwnd ) m_pAI->m_hwnd = m_hwnd;
	// Begin recording (asynchronous)
	CalcGains();
	if( DoubleBufferMode )
		m_pAI->RunWithMultipleBuffering( m_dScanRate, DBSamples );
	else
		m_pAI->RunImmedAsynch( m_dScanRate, m_lSamples );

	return S_OK;
}

STDMETHODIMP DAQPad::AcquireData(BOOL ForceStop)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( m_pAI )
	{
		if( ForceStop ) Stop();

		m_pAI->GetData( m_pData, m_lSamples );
		return S_OK;
	}

	return E_FAIL;
}

STDMETHODIMP DAQPad::Stop()
{
	m_fLocked = FALSE;
    if( !m_pAI ) m_pAI = new CNSAnalogIn;
		m_pAI->Stop();

	return S_OK;
}

STDMETHODIMP DAQPad::get_DataVoltage(long Index, double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( ( Index >= 0 ) && ( Index < ( m_lSamples * m_nChannels ) ) )
	{
		*pVal = m_pData[ Index ];
	}
	else *pVal = 0.;

	return S_OK;
}

STDMETHODIMP DAQPad::get_DataVoltageByChannel(short Channel, long Index, double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( ( Channel >= 0 ) && ( Channel <= m_nChannels ) &&
		( Index >= 0 ) && ( Index < m_lSamples ) )
	{
		*pVal = m_pData[ ( Channel * m_lSamples ) + Index ];
	}
	else *pVal = 0.;

	return S_OK;
}

///////////////////////////////////////////
// TODO: Need settable gain per channel
///////////////////////////////////////////

STDMETHODIMP DAQPad::get_DataNewtons(long Index, double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( ( Index >= 0 ) && ( Index < ( m_lSamples * m_nChannels ) ) )
	{
		*pVal = m_pData[ Index ];
		int Channel = (int)( Index / m_lSamples );
		*pVal *= m_pVtoN[ Channel ];
	}
	else *pVal = 0.;

	return S_OK;
}

STDMETHODIMP DAQPad::get_DataNewtonsByChannel(short Channel, long Index, double *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( ( Channel >= 0 ) && ( Channel <= m_nChannels ) &&
		( Index >= 0 ) && ( Index < m_lSamples ) )
	{
		*pVal = m_pData[ ( Channel * m_lSamples ) + Index ];
		*pVal *= m_pVtoN[ Channel ];
	}
	else *pVal = 0.;

	return S_OK;
}

STDMETHODIMP DAQPad::SetMessageWindow(VARIANT *MsgWindow)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( MsgWindow ) m_hwnd = ((CWnd*)MsgWindow)->m_hWnd;

	return S_OK;
}

STDMETHODIMP DAQPad::put_BaselineCount(short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_nBaselineCount = newVal;

	return S_OK;
}

STDMETHODIMP DAQPad::put_ChannelVector(short Index, short newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if( !m_pAI ) m_pAI = new CNSAnalogIn;
	m_pAI->SetChannelVector( Index, newVal );

	return S_OK;
}

STDMETHODIMP DAQPad::put_OutputFile(BSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_szFile = newVal;

	return S_OK;
}

STDMETHODIMP DAQPad::WriteDrawPoints(VARIANT* DC)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	if( !m_pAI ) return E_FAIL;
	m_pAI->WriteDrawPoints( (CDC*)DC );
	return S_OK;
}

STDMETHODIMP DAQPad::put_OutputType(DAQOutputType OutputType)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	if( !m_pAI ) return E_FAIL;
	m_pAI->SetOutputType( OutputType );
	return S_OK;
}

STDMETHODIMP DAQPad::put_LeftViewMaximizeSize( int nSize )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	::SetLeftViewMaximizeSize( nSize );
	return S_OK;
}
